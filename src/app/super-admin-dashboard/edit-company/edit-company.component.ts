import { Component, OnInit } from '@angular/core';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { CompanyService } from '../../services/company.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.css']
})
export class EditCompanyComponent implements OnInit {

  companyDetails = {
    name: '',
    tradeName: '',
    employeCount: '',
    email: '',
    type: '',
    belongsToReseller: '',
    reseller: '',
    primaryContact: { name: '', phone: '', extention: '' },
    billingContact: { name: '', email: '', phone: '', extention: '' },
    address: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },
    billingAddress: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },
    isPrimaryContactIsBillingContact: false,
    isPrimaryAddressIsBillingAddress: false
  };

  constructor(
    private companyService: CompanyService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private router: Router,

  ) { }
  ngOnInit() {
    this.companyDetails = this.accessLocalStorageService.get('editCompany');
    if (!this.companyDetails.hasOwnProperty('billingAddress')) {
      this.companyDetails.billingAddress = {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      }
    }
  }

  updateCompany() {
    console.log(this.companyDetails);

    this.companyService.editCompany(this.companyDetails)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')
            this.companyDetails = {
              name: '',
              tradeName: '',
              employeCount: '',
              email: '',
              type: '',
              belongsToReseller: '',
              reseller: '',
              primaryContact: { name: '', phone: '', extention: '' },
              billingContact: { name: '', email: '', phone: '', extention: '' },
              address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
              },
              billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
              },
              isPrimaryContactIsBillingContact: true,
              isPrimaryAddressIsBillingAddress: true
            };
            this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

}


