import { Component, OnInit } from '@angular/core';
import { InvoiceService } from '../../services/invoice.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { Router } from '@angular/router';
import { SwalAlertService } from '../../services/swalAlert.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  constructor(
    private invoiceBillingService: InvoiceService,
    private accessLocalStorageService: AccessLocalStorageService,
    private router: Router,
    private swalAlertService: SwalAlertService
  ) { }

  public invoiceSearch: FormGroup
  public invoices = []
  public months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  public invoiceSelect;
  // public invoiceClientsData;
  public invoicePadiUnpaid;
  pageNo: any;
  perPage: any;
  clientType: any;
  status: any;
  fromDate: any;
  toDate: any;
  totalCount: any;
  custonWizardID: any;
  deleteId = [];
  company: any;
  billingOpen: boolean = false;
  searchQuery: string;

  ngOnInit() {
    this.pageNo = 1;
    this.perPage = 10;
    this.clientType = '';
    this.status = '';
    this.fromDate = '';
    this.toDate = '';
    this.invoicePadiUnpaid = '';
    this.invoiceSearch = new FormGroup({
      search: new FormControl("")
    })
    this.company = ''

    this.onChange();


  }


  onChange() {
    this.invoiceSearch.controls['search'].valueChanges
      .subscribe(val => {
        console.log(val);
        this.searchQuery = val;


        this.getInvoiceDetails()
      })
    console.log(this.searchQuery);


    if (this.searchQuery == '' || this.searchQuery == undefined) {
      console.log(this.searchQuery);

      this.getInvoiceDetails();
    }
  }
  
  /* Description: mat pagination for invoice list
 author : vipin reddy */
  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getInvoiceDetails();
  }

  /* Description: filters for invoice list
  author : vipin reddy */
  invoiceClientsData(event) {
    console.log(this.clientType, event);
    this.getInvoiceDetails();

  }

  /* Description: filters for invoice list
  author : vipin reddy */

  invoicePaymentData() {
    console.log(this.invoicePadiUnpaid);
    this.getInvoiceDetails();
  }
  /* Description: get invoice list
  author : vipin reddy */
  getInvoiceDetails() {
    console.log("Hiiii")
    var postData = {
      "per_page": this.perPage,
      "page_no": this.pageNo,
      "from_date": this.fromDate,
      "to_date": this.toDate,
      "search_query": this.invoiceSearch.controls['search'].value,
      "client_type": this.clientType,
      "paymentStatus": this.invoicePadiUnpaid
    }
    console.log(postData);
    this.invoices = []
    this.invoiceBillingService.getInvoiceBillingDetails(postData)
      .subscribe((res: any) => {
        console.log('responsee', res)
        this.totalCount = res.total_count
        this.invoices = []
        res.data.forEach(element => {
          if (element.company) {
            element.isChecked = false;
            this.invoices.push(element)
          }
        });

        console.log(this.invoices);


      }, err => {
        console.log("Errrr", err)
      });
  }
  /* Description: filters for invoice list    
  author : vipin reddy */

  formmdatefun() {
    console.log(this.fromDate.toISOString());
    this.fromDate.toISOString();
    this.getInvoiceDetails()

  }
   /* Description: filters for invoice list    
  author : vipin reddy */
  
  todatefun() {
    console.log(this.fromDate.toISOString());
    this.toDate.toISOString();
    this.getInvoiceDetails()
  }
   /* Description: single company billing history showing   
  author : vipin reddy */
  editClientBillingHistory(tab) {
    console.log("compnay", tab);

    if (this.billingOpen) {
      // this.accessLocalStorageService.set('editCompany', company);
      this.accessLocalStorageService.set('viewTab', tab)
      this.company = this.accessLocalStorageService.get('editCompany')
      console.log(this.company);
      if (this.company) {
        var id = this.company.userId;
        this.router.navigate(['/super-admin/super-admin-dashboard/edit-company/' + id])
      }
    }
    if (this.company == null || this.company == undefined || this.company == '') {
      console.log("vipoiv");

      this.swalAlertService.SweetAlertWithoutConfirmation('Invoice', "please select a client", 'error')
    }

  }
   /* Description: selecting single invoice 
  author : vipin reddy */

  singleTaskChoose(taskArray, event) {
    console.log(taskArray, event);
    this.billingOpen = event
    if (event == false) {
      localStorage.removeItem('editCompany')

    }
    if (event == true) {
      this.invoices.forEach(element => {
        if (element.company.userId == taskArray.userId) {

          this.accessLocalStorageService.set('editCompany', element.company)
          element.isChecked = true;
        }
        else {
          element.isChecked = false;
        }
      })

      // for (var task in this.standardOffboardTemplate["offBoardingTasks"]) {
      //     console.log(task);
      //     this.standardOffboardTemplate["offBoardingTasks"][task].forEach(element => {
      //         if (element.taskName === taskName) {
      //             element.isChecked = true;
      //             this.popupData = element;
      //             this.addShowHide = true;
      //             console.log("popdaata", this.popupData);

      //         } else {
      //             element.isChecked = false;
      //         }
      //     });
      // }
    }


  }
}
