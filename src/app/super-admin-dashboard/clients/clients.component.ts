import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ZenworkersService } from '../../services/zenworkers.service';
import { CompanyService } from '../../services/company.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { Router } from '@angular/router';
import { MessageService } from '../../services/message-service.service';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  public clientsSearch: FormGroup;
  public companyDetails;

  /* =[
    {
      "_id" : ("5c148f917e51851f83869616"),
      "createdDate" : ("2018-12-15T03:43:09.874Z"),
      "updatedDate" : ("2018-12-15T03:43:09.874Z"),
      "name" : "MTW LABS",
      "tradeName" : "MTW INNOVATIONS PVT LTD",
      "employeCount" : 50,
      "email" : "venkatesh.g@mtwlabs.com",
      "type" : "reseller",
      "primaryContact" : {
          "name" : "Venkatesh",
          "phone" : "8989898989",
          "extention" : "91"
      },
      "billingContact" : {
          "name" : "",
          "email" : "",
          "phone" : "",
          "extention" : ""
      },
      "address" : {
          "address1" : "Ayyappa Society",
          "address2" : "Madhapur",
          "city" : "State",
          "state" : "",
          "zipcode" : "530026",
          "country" : "United States"
      },
      "isPrimaryContactIsBillingContact" : true,
      "isPrimaryAddressIsBillingAddress" : true,
      "createdBy" : ("5c139b85a69f895486534058"),
      "__v" : 0,
      "userId" : ("5c148f917e51851f83869615")
  }
  ] ; */

  public pageNo = 1;
  public perPage = 10;
  public selectedClient = '';
  zenworkClients: any;
  type: any;
  acceslevel: any;
  restrictLevel1: boolean = false;
  constructor(
    private companyService: CompanyService,
    private accessLocalStorageService: AccessLocalStorageService,
    private router: Router,
    private messageService: MessageService,
    private loaderService: LoaderService,
  ) { }


  ngOnInit() {
    this.loaderService.loader(true);
    this.type = this.accessLocalStorageService.get("type")
    console.log(this.type, "2w2w");
    if (this.type == 'administrator' || this.type == 'zenworker') {
      this.acceslevel = this.accessLocalStorageService.get('zenworkerId');
      console.log(this.acceslevel);
      if (this.acceslevel.roleId.name == 'Level 1') {
        this.restrictLevel1 = true;
      }

    }

    this.clientsSearch = new FormGroup({
      search: new FormControl('')
    })

    this.messageService.sendMessage('super-admin');
    this.onChanges();
    this.getClientsData();

  }

  /* Description: clients list for filter
   author : vipin reddy */

  filterForClient() {
    console.log(this.selectedClient);
    this.getClientsData()
  }
      /* Description: mat pagination for client list
   author : vipin reddy */

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getClientsData();
  }


  onChanges() {
    this.clientsSearch.controls['search'].valueChanges
      .subscribe(val => {
        // console.log(this.zenworkersSearch.get('search').value);
        this.getClientsData();
      });
  }
      /* Description: get clients list 
   author : vipin reddy */

  getClientsData() {
    this.companyService.getAllCompanies({ searchQuery: this.clientsSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, type: this.selectedClient })
      .subscribe(
        (response: any) => {
          this.loaderService.loader(false);
          if (response.status) {
            this.zenworkClients = response.count;
            this.companyDetails = response.data;
          }
          console.log("Response", response);
        }
      )
  }

        /* Description: edit comapny routing to redirection
   author : vipin reddy */
  editCompany(company) {
    this.accessLocalStorageService.set('editCompany', company);
    this.router.navigate(['/super-admin/super-admin-dashboard/edit-company']);
  }

 /* Description: redirection to reseller dashboard
   author : vipin reddy */
  resellerDashboard(comapny_id, id, comapanyName) {
    localStorage.setItem('resellerName', comapanyName);
    localStorage.setItem('resellerId', comapny_id);
    localStorage.setItem('resellercmpnyID', id);
    this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id])
    this.messageService.sendMessage('reseller')
  }

  /* Description: redirection to cilent billing history shown
   author : vipin reddy */
   
  editClientBillingHistory(company, tab) {
    console.log("comapnay", company);

    this.accessLocalStorageService.set('editCompany', company);
    this.accessLocalStorageService.set('viewTab', tab)
    var id = company.userId;
    this.router.navigate(['/super-admin/super-admin-dashboard/edit-company/' + id])
  }

}
