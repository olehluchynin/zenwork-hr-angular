import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SuperAdminDashboardComponent } from './super-admin-dashboard.component';
/* import { SideNavComponent } from '../shared-module/side-nav-component/side-nav.component'; */
import { CommonModule } from '@angular/common';
import { ZenworkersComponent } from './zenworkers/zenworkers.component';



import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CreateZenworkerTemplateComponent } from './create-zenworker-template-component/create-zenworker-template-component.component';
import { ClientsComponent } from './clients/clients.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { EditCompanyComponent } from './edit-company/edit-company.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from '../shared-module/shared.module';
import { InvoiceComponent } from './invoice/invoice.component';
import { CreditCardChargingService } from '../services/chardgeCreditCard.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../services/tokenInterceptor.service';
import { MatIconModule } from '@angular/material';
import { MaterialModuleModule } from '../material-module/material-module.module';



const routes: Routes = [
    { path: '', redirectTo: 'super-admin-dashboard', pathMatch: "full" },
    {
        path: 'super-admin-dashboard', component: SuperAdminDashboardComponent, children: [
            { path: '', redirectTo: 'clients', pathMatch: "full" },
            { path: 'zenworkers', component: ZenworkersComponent },
            { path: 'clients', component: ClientsComponent },
            { path: 'edit-company/:id', component: CreateCompanyComponent },
            { path: 'edit-company', component: EditCompanyComponent },
            { path: 'settings', loadChildren: './package-creation/package-creation.module#PackageCreationModule' },
            { path: 'invoice', component: InvoiceComponent },
            { path: 'client-details', loadChildren: './client-details/client-details.module#ClientDetailsModule' },
            { path: 'reseller/:id', loadChildren: '../reseller-dashboard/reseller-dashboard.module#ResellerDashboardModule' },

        ]
    },


];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        SharedModule,
        ModalModule.forRoot(),
        MatSelectModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatStepperModule,
        MatInputModule,
        MatTabsModule,
        MatSlideToggleModule,
        MatDatepickerModule,
        MatNativeDateModule,
        HttpClientModule,
        MatIconModule,
        MaterialModuleModule
    ],
    declarations: [
        SuperAdminDashboardComponent,
        ZenworkersComponent,
        ClientsComponent,
        CreateCompanyComponent,
        EditCompanyComponent,
        InvoiceComponent,

    ],
    entryComponents: [
        CreateZenworkerTemplateComponent
    ],
    exports: [
        RouterModule
    ],
    providers: [CreditCardChargingService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },]

})

export class SuperAdminRouting { }