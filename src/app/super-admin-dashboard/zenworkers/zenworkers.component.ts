import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ZenworkersService } from '../../services/zenworkers.service';
import { CreateZenworkerTemplateComponent } from '../create-zenworker-template-component/create-zenworker-template-component.component';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { LoaderService } from '../../services/loader.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';


@Component({
  selector: 'app-zenworkers',
  templateUrl: './zenworkers.component.html',
  styleUrls: ['./zenworkers.component.css']
})
export class ZenworkersComponent implements OnInit {

  public clients;
  public zenworkerDetails;
  public selectedZenworkerDetails;
  public selectedZenworkRole;
  public zenworkersSearch: FormGroup;
  public perPage;
  public pages;
  public pageNo;
  public selectedRole: any;
  public allZenworkers: boolean = false;
  userProfile = [];
  selectedOne = [];
  addedData: any;
  type: any;
  acceslevel: any;
  restrictLevel1: boolean = false;
  constructor(
    public dialog: MatDialog,
    private zenworkersService: ZenworkersService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private accessLocalStorageService: AccessLocalStorageService
  ) { }

  ngOnInit() {
    this.loaderService.loader(true);
    this.type = this.accessLocalStorageService.get("type")
    console.log(this.type, "2w2w");
    if (this.type == 'administrator'|| this.type == 'zenworker') {
      this.acceslevel = this.accessLocalStorageService.get('zenworkerId');
      console.log(this.acceslevel);
      if (this.acceslevel.roleId.name == 'Level 1') {
        this.restrictLevel1 = true;
      }

    }
    this.zenworkersSearch = new FormGroup({
      search: new FormControl('')
    });
    this.selectedZenworkRole = "all";
    this.perPage = 10;
    this.pages = [10, 25, 50];
    this.pageNo = 1;

    // this.zenworkerDetails = [{ name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false },
    // { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false },
    // { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false }]
    this.selectedZenworkerDetails = { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false }
    this.onChanges();
    this.getZenworkersData();
    this.getAllRoles();
  }


  onChanges() {
    this.zenworkersSearch.controls['search'].valueChanges
      .subscribe(val => {
        // console.log(this.zenworkersSearch.get('search').value);
        this.getZenworkersData();
      });
  }

   /* Description: get zenworkers data list
  author : vipin reddy */
  getZenworkersData() {
    console.log("change", this.selectedZenworkRole)
    this.loaderService.loader(true);
    let zenworker;
    if (this.selectedZenworkRole == "all") {
      zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage }
    } else {
      this.selectedRole = this.selectedZenworkRole;
      zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, role: this.selectedZenworkRole }
    }
    console.log(zenworker);

    this.zenworkersService.getZenworkersDetails(zenworker)
      .subscribe(
        (response: any) => {
          this.loaderService.loader(false);
          if (response.status) {
            console.log(response.data);

            // this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')
            this.zenworkerDetails = response.data;
            this.zenworkerDetails.forEach(zenworker => {
              zenworker.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Zenworkers", "Data Not Found", 'info')
          }
        },
        (err: HttpErrorResponse) => {
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

   /* Description: select only one for zenworker
  author : vipin reddy */
  selectOnlyOne() {
    // this.addZenworker(this.addedData)
    if (this.selectedOne.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select only one Zenworker on to Proceed", 'error')
    } else if (this.selectedOne.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select a Zenworker to Proceed", 'error')
    }
    if (this.selectedOne.length == 1) {
      console.log("ssss");
      this.zenworkersService.getSingleZenworker(this.selectedOne[0])
        .subscribe(
          (res: any) => {
            console.log(res);
            this.addedData = res.data;
            this.addZenworker(this.addedData)

          },
          (err) => {

          })

    }
  }

   /* Description: selected one zenworker
  author : vipin reddy */
  selectOne(event, data) {

    console.log(event, data._id);
    if (event == true) {
      this.selectedOne.push(data._id)
      this.addedData = data
      // if (this.selectedOne.length > 1) {
      //   this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select only one Zenworker on to Proceed", 'error')
      // } else if (this.selectedOne.length == 0) {
      //   this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select a Zenworker to Proceed", 'error')
      // } else {
      //   this.addedData = data
      // }

    }
    if (event == false) {
      this.selectedOne.forEach(element => {
        if (data._id == element) {

          var index = this.selectedOne.indexOf(data._id)
          if (index > -1) {
            this.selectedOne.splice(index, 1)
            // delete this.selectedOne['isChecked']
          }
        }
      });
    }
    console.log(this.selectedOne);




  }

   /* Description: selecting all zenworkers
  author : vipin reddy */
  selectAllZenworkers(event) {
    // console.log("Selecting all", this.allZenworkers);
    console.log(event);
    if (event.checked == true) {
      this.zenworkerDetails.forEach(zenworker => {
        console.log(zenworker);

        this.selectedOne.push(zenworker._id)
        zenworker.isChecked = true
      });
      console.log(this.selectedOne);
    }
    if (event.checked == false) {
      this.selectedOne = []
      this.zenworkerDetails.forEach(zenworker => {
        console.log(zenworker);

        zenworker.isChecked = false
      });

    }


  }
     /* Description: deleting zenworkers multiple
  author : vipin reddy */
  deleteZenworkers() {
    var postdata: any = {
      _ids: this.selectedOne
    }
    this.zenworkersService.deleteZenworkers(postdata)
      .subscribe(
        (res: any) => {
          console.log("111111111", res);
          if (res.status == true)
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete", res.message, 'success')
          this.getZenworkersData();
        },
        (err) => {
          console.log(err);

        })

  }
/* Description: pagination for zenworkers
  author : vipin reddy */

  changePage(p) {
    this.pageNo = p;
    this.getZenworkersData();
  }
  /* Description: add zenworker popup
  author : vipin reddy */
  addZenworker(data) {
    const dialogRef = this.dialog.open(CreateZenworkerTemplateComponent, {
      height: '700px',
      width: '890px',
      data: data,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("Result", result)
      this.selectedOne = []
      this.getZenworkersData();
    });
  }
  
  /* Description: add zenworker popup
  author : vipin reddy */

  deleteZenworker(zenworker) {

  }
  /* Description: get all zenworker levels
  author : vipin reddy */
  getAllRoles() {

    this.zenworkersService.getAllroles()
      .subscribe(
        (res: any) => {
          console.log("111111111", res);
          if (res.status == true) {
            // this.loaderService.loader(false)
            this.userProfile = res.data;

          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }

}
