import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZenworkersComponent } from './zenworkers.component';

describe('ZenworkersComponent', () => {
  let component: ZenworkersComponent;
  let fixture: ComponentFixture<ZenworkersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZenworkersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZenworkersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
