import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatDatepickerModule } from '@angular/material/datepicker';
/* import { SideNavComponent } from '../shared-module/side-nav-component/side-nav.component'; */
import { CommonModule } from '@angular/common';
import { ClientDetailsComponent } from './client-details.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from '../../shared-module/shared.module';

import { CreditCardChargingService } from '../../services/chardgeCreditCard.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../../services/tokenInterceptor.service';
import { MatIconModule } from '@angular/material';
import { MaterialModuleModule } from '../../material-module/material-module.module';


const routes: Routes = [
  { path: '', component: ClientDetailsComponent }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    SharedModule,
    ModalModule.forRoot(),
    MatSelectModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatInputModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    MatIconModule,
    MaterialModuleModule
  ],
  declarations: [ClientDetailsComponent],
  providers: [CreditCardChargingService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }]
})
export class ClientDetailsModule { }
