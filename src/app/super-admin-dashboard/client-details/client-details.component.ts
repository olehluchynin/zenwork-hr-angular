import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { Router } from '@angular/router';
import { PackageAndAddOnService } from '../../services/packageAndAddon.service';
import { PurchasePackageService } from '../../services/purchasePackage.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { CreditCardChargingService } from '../../services/chardgeCreditCard.service';
import { MatStepper } from '@angular/material/stepper'
import { MatSlideToggleChange, MatCheckbox } from '@angular/material';
import { LoaderService } from '../../services/loader.service';
import * as jspdf from 'jspdf';
import { MatDialog } from '@angular/material';


import html2canvas from 'html2canvas';
import { post } from 'selenium-webdriver/http';
import { CompanySettingsService } from '../../services/companySettings.service';
import { ConfirmationComponent } from '../../shared-module/confirmation/confirmation.component';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  @ViewChild('stepper') private myStepper: MatStepper;

  isClientDetailshide: boolean = false;
  dummy: any;
  public companyDetails;
  validity: boolean = false;
  isLinear: boolean = true;
  basePrice: any;
  selectedAddons: any;
  invoiceData1 = {
    startDate: '',
    endDate: ''
  };
  paymentstatus: any;
  public selectedPackage: any = {
    clientId: '',
    name: '',
    wholeSaleDiscount: false,
    discount: 0,
    costPerEmployee: 0,
    price: 0,
    startDate: new Date(),
    adons: [


    ],
    endDate: new Date(new Date().setMonth(new Date().getMonth() + 1)),


    totalPrice: 0,
    packages: [],
    billingFrequency: 'monthly'
  };

  public companyContact = {
    name: '',
    email: '',
    phone: '',
    extention: '',
    isPrimaryContactIsBillingContact: true,
  }
  public billingContact = {
    name: '',
    email: '',
    phone: '',
    extention: ''

  }
  address = {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zipcode: '',
    country: '',
    isPrimaryAddressIsBillingAddress: true
  }
  billingAddress = {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zipcode: '',
    country: ''
  }
  dicountArray = [
    { value: 10, viewValue: '10%' },
    { value: 20, viewValue: '20%' },
    { value: 30, viewValue: '30%' },
    { value: 40, viewValue: '40%' },
    { value: 50, viewValue: '50%' },
    { value: 60, viewValue: '60%' },
    { value: 70, viewValue: '70%' },
    { value: 80, viewValue: '80%' },
    { value: 90, viewValue: '90%' },

  ];
  monthsArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  yearsArray = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028]

  public packages;
  public selectedPackageData: any = {};
  public allAddOns;
  public orderId;
  public addedCompanyData: any = {};
  discountValue: any;
  discountEnable: boolean = true;
  basePriceDiscount: any;
  hidingdropdown: boolean = false;
  tempforDiscountChange: any;
  isValid: boolean = false;
  isValidcard: boolean = false;
  paymentBtn: boolean = false;
  public
  invoiceData = {
    email: '',
    name: '',
    type: '',
    primaryContact: { phone: '', extention: '' },
    createdAt: ''

  };
  invoiceBillingCompanyAddress = {
    address1: '',
    city: '',
    state: '',
    zipcode: '',
    country: '',
  };
  resellersList = [];
  invoiceadons: any;
  taxrate: any = {
    tax_rate: '',
    _id: ''
  }
  grandPrice: any;
  addedCoupon: any;
  couponAmountValue: any;
  finalCouponValue: any;
  adonsPrice: number = 0;
  allStates = [];
  allCountryes = [];
  text: any;
  note: any;
  couponEnable: boolean = false;
  constructor(
    private companyService: CompanyService,
    private swalAlertService: SwalAlertService,
    private router: Router,
    private packageAndAddOnService: PackageAndAddOnService,
    private purchasePackageService: PurchasePackageService,
    private accessLocalStorageService: AccessLocalStorageService,
    private creditCardChargingService: CreditCardChargingService,
    private loaderService: LoaderService,
    private companySettingsService: CompanySettingsService,
    public dialog: MatDialog,
    public authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    console.log(this.orderId);
    var routeurl = this.router.url.split('/')
    console.log(routeurl[1] == 'self-on-boarding');
    this.isClientDetailshide = !(routeurl[1] == 'self-on-boarding')
    this.isLinear = true;
    // this.discountEnable = true;
    // this.discountValue = 0;
    this.companyDetails = {
      name: '',
      tradeName: '',
      employeCount: '',
      referralcompany: '',
      email: '',
      type: '',
      belongsToReseller: '',
      resellerId: ''
    }
    this.companyContact = {
      name: '',
      email: '',
      phone: '',
      extention: '',
      isPrimaryContactIsBillingContact: true,
    }
    this.billingContact = {
      name: '',
      email: '',
      phone: '',
      extention: ''

    }
    this.address = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: '',
      isPrimaryAddressIsBillingAddress: true
    }
    this.billingAddress = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },


      this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    if (this.addedCompanyData && this.addedCompanyData._id) {
      this.selectedPackage.clientId = this.addedCompanyData._id;

    }
    if (this.addedCompanyData == null) {
      this.addedCompanyData = {}
    }
    this.getAllPackages();
    this.getAllAddons();
    this.getAllresellers();
    this.getTaxRate();
    if (this.isClientDetailshide) {
      this.getAllUSstates(0);
      this.getAllCountry(0);

    }
    if (!this.isClientDetailshide) {
      this.getAllUSstates(1);
      this.getAllCountry(1);
      this.companyContact.name = this.addedCompanyData.primaryContact.name;
      this.companyContact.email = this.addedCompanyData.email;
      this.companyContact.phone = this.addedCompanyData.primaryContact.phone
    }
  }
  /* Description: coupon code enabling 
   author : vipin reddy */
  couponCode(event) {
    console.log(event);
    this.couponEnable = event.checked;
    console.log(this.couponEnable);

  }
  /* Description: get all us states from structure
 author : vipin reddy */

  getAllUSstates(id) {
    this.companySettingsService.getStuctureFields('State', id)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allStates.push(element)
              console.log("22222111111", this.allStates);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allStates.push(element)
            });
            console.log("22222", this.allStates);
          }
        },
        (err) => {
          console.log(err);

        })
  }

  /* Description: get all countryes form structure
   author : vipin reddy */

  getAllCountry(id) {
    this.companySettingsService.getStuctureFields('Country', id)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allCountryes.push(element)
              console.log("countryes", this.allCountryes);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allCountryes.push(element)
            });
            console.log("countryes", this.allCountryes);
          }
        },
        (err) => {
          console.log(err);

        })

  }
  // getAllUSstatesWithoutToken() {

  //   this.companySettingsService.getStuctureFieldsWithoutToken('State', 'withouttoken=1')
  //     .subscribe(
  //       (res: any) => {
  //         console.log(res);
  //         if (res.custom.length > 0) {
  //           res.custom.forEach(element => {
  //             this.allStates.push(element)
  //             console.log("22222111111", this.allStates);
  //           });
  //         }
  //         if (res.default.length > 0) {
  //           res.default.forEach(element => {
  //             this.allStates.push(element)
  //           });
  //           console.log("22222", this.allStates);
  //         }
  //       },
  //       (err) => {
  //         console.log(err);

  //       })
  // }
  // getAllCountryWithoutToken() {
  //   var x = 'withouttoken = 1'
  //   this.companySettingsService.getStuctureFieldsWithoutToken('Country', x)
  //     .subscribe(
  //       (res: any) => {
  //         console.log(res);
  //         if (res.custom.length > 0) {
  //           res.custom.forEach(element => {
  //             this.allCountryes.push(element)
  //             console.log("countryes", this.allCountryes);
  //           });
  //         }
  //         if (res.default.length > 0) {
  //           res.default.forEach(element => {
  //             this.allCountryes.push(element)
  //           });
  //           console.log("countryes", this.allCountryes);
  //         }
  //       },
  //       (err) => {
  //         console.log(err);

  //       })

  // }

  public cardDetails = {
    cardNumber: '',
    expMonth: '',
    expYear: '',
    cvv: '',
    cardName: ''
  }

  /* Description: payment for stripe API  
   author : vipin reddy */

  chargeCreditCard(stepper) {

    this.isValidcard = true;
    if (this.cardDetails.cardName && this.cardDetails.cardNumber && this.cardDetails.expMonth && this.cardDetails.expYear && this.cardDetails.cvv) {
      this.loaderService.loader(true);
      (<any>window).Stripe.card.createToken({
        number: this.cardDetails.cardNumber,
        exp_month: this.cardDetails.expMonth,
        exp_year: this.cardDetails.expYear,
        cvc: this.cardDetails.cvv
      }, (status: number, response: any) => {
        if (status === 200) {

          let token = response.id;
          console.log(token)
          // this.chargeCard(token);
          var id = JSON.parse(localStorage.getItem("addedCompany"))
          console.log(id)
          var amount: any = (this.selectedPackage.totalPrice + (this.taxrate.tax_rate)) / 100
          amount = Math.round(amount * 100);
          console.log(amount);

          this.creditCardChargingService.createCharge(token, id._id, amount, this.orderId)
            .subscribe(
              (response: any) => {
                console.log(response);
                if (response.status == true) {
                  this.loaderService.loader(false);
                  this.goForward(stepper);
                  this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')

                  this.invoiceData = response.company;
                  this.invoiceData1 = response.data;
                  this.paymentstatus = response.message;
                  this.invoiceBillingCompanyAddress = response.company.primaryAddress;
                  console.log("this.invoiceData", this.invoiceData);
                }
                else {
                  // this.loaderService.loader(false);
                  this.swalAlertService.SweetAlertWithoutConfirmation("Error", response.message, 'error')
                }


                // this.loaderService.loader(false);

              },
              err => {
                // this.loaderService.loader(false);
                console.log(err);
                this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
              }
            )

        } else {
          this.loaderService.loader(false);
          console.log(response.error.message);

        }
        this.companyDetails = {
          name: '',
          tradeName: '',
          employeCount: '',
          referralcompany: '',
          email: '',
          type: '',
          belongsToReseller: '',
          resellerId: ''
        };
        this.cardDetails.cardNumber = '';
        this.cardDetails.expMonth = '';
        this.cardDetails.expYear = '';
        this.cardDetails.cvv = '';
      });

    }
  }
  /* Description: get taxrate
  author : vipin reddy */
  getTaxRate() {
    this.packageAndAddOnService.getTaxRate()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.taxrate = res.data
        },
        (err) => {
          console.log(err);
        })
  }

  /* Description: coupon code apply
  author : vipin reddy */
  couponApply() {
    console.log(this.addedCoupon, "11232coupon");
    var postdata = {
      coupon_name: this.addedCoupon
    }
    this.packageAndAddOnService.validateCoupon(postdata)
      .subscribe(
        (res: any) => {
          console.log("response-client", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Coupon', res.message, 'success')
            if (res.data.couponType == 'Value') {
              this.finalCouponValue = res.data.flatDiscountAmount;
              var x = (this.selectedPackage.totalPrice + this.selectedPackage.totalPrice * (this.taxrate.tax_rate) / 100)
              // this.finalCouponValue = (x * (this.couponAmountValue)/100);
              this.grandPrice = x - this.finalCouponValue
            }
            if (res.data.couponType == 'Percentage') {
              this.couponAmountValue = res.data.percentage;
              var x = (this.selectedPackage.totalPrice + this.selectedPackage.totalPrice * (this.taxrate.tax_rate) / 100)
              this.finalCouponValue = (x * (this.couponAmountValue) / 100);
              this.grandPrice = x - this.finalCouponValue
            }
            console.log("11111", this.grandPrice);

            console.log('couponamountvalue', this.couponAmountValue);

          }

          this.addedCoupon = ''
          if (res.status == false) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Coupon', res.message, 'error')
            this.finalCouponValue = ''
          }
        },
        (err) => {
          console.log(err);

        })

  }


  /* Description: adding a company in (Add-Client)first tab function
  author : vipin reddy */

  createCompany(stepper) {

    console.log(this.companyDetails);
    if (this.companyDetails.belongsToReseller == 'true') {
      this.companyDetails.type = 'reseller-client';
    }
    console.log(this.companyDetails)
    this.companyService.createCompany(this.companyDetails)
      .subscribe(
        (res: any) => {
          console.log("response-client", res);
          if (res.status == true) {
            this.addedCompanyData = res.data;
            if (this.addedCompanyData == null) {
              this.addedCompanyData = {}
            }
            this.accessLocalStorageService.set('addedCompany', this.addedCompanyData);


            this.companyContact.email = this.addedCompanyData.email;
            this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')

            this.selectedPackage.clientId = this.addedCompanyData._id;
            this.goForward(stepper);
          }
          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error')
            // this.companyDetails = {
            //   name: '',
            //   tradeName: '',
            //   employeCount: '',
            //   referralcompany: '',
            //   email: '',
            //   type: '',
            //   belongsToReseller: '',
            //   resellerId: ''
            // };

          }
        },

        (err: HttpErrorResponse) => {
          console.log(err);

          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }
  /* Description: mat-stepper next step forward
    author : vipin reddy */
  goForward(stepper: MatStepper) {
    stepper.next();
  }
  /* Description: mat-stepper previous step 
  author : vipin reddy */
  goBack(stepper: MatStepper) {

    stepper.previous();
  }
  /* Description: get all packages
  author : vipin reddy */
  getAllPackages() {
    this.packageAndAddOnService.getAllPackages()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.packages = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
  /* Description: discount price calculation
   author : vipin reddy */
  discountBasePrice() {
    console.log(this.discountValue);
    if (this.discountValue <= 100) {
      var x = (this.tempforDiscountChange * this.discountValue / 100)
      this.selectedPackage.totalPrice = this.tempforDiscountChange - x;
      // this.getSpecificPackageDetails();
    }
    else {
      this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error')
      this.discountValue = ''
      this.getSpecificPackageDetails();
      this.addOnChange();
    }
  }
  /* Description: get all addons
 author : vipin reddy */
  getAllAddons() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            console.log(this.allAddOns);

            this.allAddOns.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
  /* Description: selecting addons after individual addon details fetching
author : vipin reddy */
  getAllAddOnDetails() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            this.allAddOns.forEach(addon => {
              addon.isSelected = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
  /* Description: addons adding into when client adding
    author : vipin reddy */
  addAnotherAddOn() {
    console.log('adons add', this.selectedPackage);

    // if (this.selectedPackage.addOns.length == 0) {
    //   this.selectedPackage.addOns = []
    // }
    this.selectedPackage.adons.push({
      id: '',
      selectedAddOn: '',
      price: 0,
      subscriptionType: '',
      index: this.selectedPackage.adons.length + 1
    })
    // this.selectedPackage.addOns = []

  }
  /* Description: addon changeing dropdown
  author : vipin reddy */
  addOnChange() {
    // console.log(addOn);

    console.log("before", this.selectedPackage.adons);

    // for(let ref of this.selectedPackage.addOns){
    //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
    // }
    // console.log("after",this.selectedPackage.addOns);

    // this.selectedPackage.totalPrice = 0
    for (let i = 0; i < this.allAddOns.length; i++) {
      this.selectedPackage.adons.forEach(addon => {


        console.log(addon, this.allAddOns[i])

        if (addon.selectedAddOn == this.allAddOns[i].name) {

          addon.price = this.allAddOns[i].price;
          addon.subscriptionType = this.allAddOns[i].subscriptionType;
          if (this.validity == false && addon.subscriptionType == 'monthly') {
            addon.price = addon.price
          }
          if (this.validity == false && addon.subscriptionType == "yearly") {
            addon.price = addon.price / 12;
          }
          if (this.validity == true && addon.subscriptionType == "monthly") {
            addon.price = addon.price * 12;
          }

          if (this.validity == true && addon.subscriptionType == "yearly") {
            addon.price = addon.price
          }


          // if (this.validity == true ) {

          //   addon.price = addon.price * 12
          // }
          addon.id = this.allAddOns[i]._id;
          console.log(addon.id, this.selectedPackage.adons);
          this.selectedAddons = this.selectedPackage.adons[i];
          // this.selectedAddons['id'] = addon.id;
          console.log(this.selectedAddons);

        }
      });
    }
    var price = 0;
    this.selectedPackage.adons.forEach(data => {
      price += data.price
    })
    console.log(price);
    // this.adonsPrice = 0;
    if (price > 0) {
      this.selectedPackage.totalPrice = this.dummy + price;
    }
    this.tempforDiscountChange = this.selectedPackage.totalPrice;
    if (this.discountValue) {
      this.discountBasePrice();
    }
    console.log(this.selectedPackage.totalPrice);

  }

  /* Description: addon remove
    author : vipin reddy */
  addonRemove(addon) {

    // if (addon == '') {
    //   this.selectedPackage.addOns = []
    // }
    console.log("addon", addon);
    // for (let i = 0; i < this.allAddOns.length; i++) {
    //   this.selectedPackage.addOns.forEach(addon => {
    //     console.log(addon, this.allAddOns[i])
    //     if (addon == this.allAddOns[i].name) {

    //    }
    //   });
    // }
    this.selectedPackage.adons.forEach(data => {
      console.log(data);

      if (data.selectedAddOn == addon) {
        console.log(data.price);

        this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - data.price;
      }
    });
    console.log(this.selectedPackage.totalPrice);


    this.selectedPackage.adons = this.selectedPackage.adons.filter(data => addon != data.selectedAddOn)


    if (this.discountValue) {
      this.discountBasePrice();
    }
    // this.addOnChange()
  }
  /* Description: get all resellers
   author : vipin reddy */
  getAllresellers() {
    this.companyService.getresellers()
      .subscribe(
        (res: any) => {
          console.log("reselerrsList", res);
          this.resellersList = res.data;
        },
        (err: any) => {

        })
  }
  /* Description: selecting reseller
   author : vipin reddy */
  selectedReseller(id) {
    console.log(id);

  }
  /* Description: get selecting package details
 author : vipin reddy */

  getSpecificPackageDetails() {
    this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    console.log(this.selectedPackage.name, this.packages, this.allAddOns, this.selectedPackage.adons);
    this.packages.forEach(packageData => {
      if (packageData.name == this.selectedPackage.name) {
        this.selectedPackage.packages = this.packages.filter(data => data.name == this.selectedPackage.name)
        console.log(this.selectedPackage.packages);

        if (this.validity == false && this.selectedPackage.packages[0].subscriptionType == 'Monthly') {

          this.selectedPackage.price = packageData.price;
        }
        if (this.validity == false && (this.selectedPackage.packages[0].subscriptionType == "Annual" || this.selectedPackage.packages[0].subscriptionType == "Yearly")) {
          this.selectedPackage.price = packageData.price / 12;
        }
        if (this.validity == true && this.selectedPackage.packages[0].subscriptionType == "Monthly") {
          this.selectedPackage.price = (packageData.price * 12);
        }
        if (this.validity == true && (this.selectedPackage.packages[0].subscriptionType == "Annual" || this.selectedPackage.packages[0].subscriptionType == "Yearly")) {
          this.selectedPackage.price = packageData.price;
        }
        if (this.validity == false)
          this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
      }
    });


    for (var i = 0; i < this.allAddOns.length; i++) {
      console.log(this.allAddOns);

      for (var j = 0; j < this.selectedPackage.adons.length; j++) {
        console.log(this.selectedPackage.adons[j]);

        if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn) {
          // if (this.validity == false) {
          if (this.validity == false && this.selectedPackage.adons[j].subscriptionType == 'monthly') {
            this.getAllAddons()
            this.adonsPrice = 0;
            if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

              this.selectedPackage.adons[j].price = this.allAddOns[i].price;
          }
          if (this.validity == false && this.selectedPackage.adons[j].subscriptionType == "yearly") {
            console.log("data come in");

            this.getAllAddons()
            this.adonsPrice = 0;
            if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

              this.selectedPackage.adons[j].price = this.allAddOns[i].price / 12;
          }
          if (this.validity == true && this.selectedPackage.adons[j].subscriptionType == "monthly") {
            this.getAllAddons()
            this.adonsPrice = 0;
            if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

              this.selectedPackage.adons[j].price = this.allAddOns[i].price * 12;
          }
          if (this.validity == true && this.selectedPackage.adons[j].subscriptionType == "yearly") {
            this.getAllAddons()
            this.adonsPrice = 0;
            if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

              this.selectedPackage.adons[j].price = this.allAddOns[i].price;
          }
          // else {

          //   this.selectedPackage.adons[j].price = this.selectedPackage.adons[j].price * 12;
          // }
          // this.selectedPackage.costPerEmployee = this.allAddOns[i].costPerEmployee;
        }
      }
    }

    //   this.allAddOns.forEach(element => {
    //   this.selectedPackage.adons.forEach(selectAdonsData => {
    //     if (element.name == selectAdonsData.selectedAddOn) {
    //       if (this.validity == false) {
    //         this.selectedPackage.adons = selectAdonsData.price;
    //       }
    //       else {
    //         this.selectedPackage.adons = (selectAdonsData.price * 12);
    //         // selectAdonsData.price

    //       }
    //     }
    //   });

    // });


    this.adonsPrice = 0;
    this.selectedPackage.adons.forEach(element => {
      console.log(element);


      this.adonsPrice = this.adonsPrice + element.price
    });
    console.log(this.adonsPrice);


    this.selectedPackage.totalPrice = this.selectedPackage.price + this.adonsPrice + this.selectedPackage.costPerEmployee * this.addedCompanyData.employeCount;
    // if (this.basePriceDiscount > 0) {
    //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
    // }
    this.discountEnable = false;
    /* this.selectedPackageData = packageData; */
    /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.selectedPackageData = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      ) */
    this.dummy = this.selectedPackage.totalPrice;
    this.tempforDiscountChange = this.selectedPackage.totalPrice;
    console.log(this.selectedPackage.totalPrice);
    
  }
  /* Description: toggle time period change monthly yearly
 author : vipin reddy */

  toggleDateChange(event: MatSlideToggleChange) {
    console.log(event.checked);
    if (event.checked == true) {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
      this.selectedPackage.billingFrequency = 'annually';
      this.selectedPackage.costPerEmployee = this.selectedPackage.costPerEmployee * 12;
      this.validity = true;
    }

    else {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
      this.selectedPackage.costPerEmployee = this.selectedPackage.costPerEmployee;

      this.validity = false;
      this.selectedPackage.billingFrequency = 'monthly';
    }

    this.getSpecificPackageDetails()
    // if (this.selectedPackage.adons.lenght > 0) {
    //   // this.get
    // }
  }
  /* Description: wholesale discount toggle setup
   author : vipin reddy */
  wholesaleDiscountChange($event) {
    console.log($event, this.companyDetails.type);

    if (this.companyDetails.type == 'reseller') {
      this.selectedPackage.wholeSaleDiscount = false;
    }
    else {
      this.selectedPackage.wholeSaleDiscount = true;
    }
  }
  /* Description: save contact details in (Add-client) 3rd tab
 author : vipin reddy */
  saveAddressDetails(stepper) {
    this.isValid = true;
    console.log("dat 1");
    console.log(this.companyContact.name, this.companyContact.email, this.companyContact.phone,
      this.companyContact.extention, this.address.address1, this.address.city,
      this.address.state, this.address.zipcode, this.address.country)
    if (this.companyContact.name && this.companyContact.email && this.companyContact.phone &&
      this.companyContact.extention && this.address.address1 && this.address.city &&
      this.address.state && this.address.zipcode && this.address.country) {
      console.log("dat 2");

      var postData = {
        _id: this.addedCompanyData._id,
        primaryContact: this.companyContact,
        isPrimaryContactIsBillingContact: this.companyContact.isPrimaryContactIsBillingContact,
        billingContact: this.billingContact,
        primaryAddress: this.address,
        isPrimaryAddressIsBillingAddress: this.address.isPrimaryAddressIsBillingAddress,
        billingAddress: this.billingAddress,

      }
      console.log("dat 1");

      this.companyService.addContactDetails(postData)
        .subscribe(
          (res: any) => {
            console.log("Response", res);
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')

              // this.companyContact = {
              //   name: '',
              //   email: '',
              //   phone: '',
              //   extention: '',
              //   isPrimaryContactIsBillingContact: true,
              // }
              // this.billingContact = {
              //   name: '',
              //   email: '',
              //   phone: '',
              //   extention: ''

              // }
              // this.address = {
              //   address1: '',
              //   address2: '',
              //   city: '',
              //   state: '',
              //   zipcode: '',
              //   country: '',
              //   isPrimaryAddressIsBillingAddress: true
              // }
              // this.billingAddress = {
              //   address1: '',
              //   address2: '',
              //   city: '',
              //   state: '',
              //   zipcode: '',
              //   country: ''
              // }
              this.goForward(stepper)
            }
          },
          (err) => {
            console.log(err);
          })
    }
  }

  /* Description: package and addons create order
   author : vipin reddy */
  savePackageDetails(stepper) {


    console.log("this.selectedPackage", this.selectedPackage);
    // this.invoiceadons = this.selectedPackage.addOns;

    // var postData = this.selectedPackage
    // delete postData.addOns
    // postData['adons'] = this.invoiceadons;

    // console.log(postData);
    if (this.orderId != undefined) {
      this.selectedPackage['_id'] = this.orderId;
    }

    this.purchasePackageService.purchasePackage(this.selectedPackage)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.orderId = res.data._id
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success')
            // this.activeTab = 'payment';
            this.goForward(stepper);
          }

          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error')

        }
      )
    // this.selectedPackage = Object.assign(this.selectedPackage,{addOns : []})
  }
  /* Description: download PDF format in fort-end
 author : vipin reddy */
  downloadPdf() {

    var data = document.getElementById('invoice');
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('Zenwork-Invoice.pdf'); // Generated PDF   
    });

  }
  /* Description: accept only numbers
author : vipin reddy */

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  /* Description: self signup cancel button before page redirection
   author : vipin reddy */
  cancel() {
    this.text = "The data entered will be lost,";
    this.note = "Would you like to proceed";
    this.openDialog()
  }
  /* Description: open modal popup for lose the data or not
 author : vipin reddy */
  openDialog(): void {

    let dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '550px',
      data: { txt: this.text, note: this.note },
      panelClass: 'confirm-class'

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true && this.isClientDetailshide) {
        this.router.navigate(["/super-admin/super-admin-dashboard/clients"])
      }
      // if (data == 'delete') {
      //   this.deleteSingleCategory(id)
      // }
      if (this.selectedPackage.packages.length >= 0 && !this.isClientDetailshide) {
        this.router.navigate(["/sign-up"])

      }


    });
  }
  /* Description:  open modal popup for lose the data or not
 author : vipin reddy */

  cancel1() {
    this.text = "The data entered will be lost,";
    this.note = "Would you like to proceed";
    localStorage.clear();
    this.openDialog()

  }
  /* Description: self signup before page redirection
   author : vipin reddy */
  backHome() {
    console.log('12wed');

    if (this.selectedPackage.packages.length > 0) {
      this.cancel();
    }
    else {
      this.router.navigate(["/sign-up"])
    }

  }
  /* Description: terms and condition redirection 
 author : vipin reddy */

  termsAndConditionCheckbox(event) {
    console.log(event.checked);
    this.paymentBtn = event.checked;
  }

  /* Description: scroll down to terms and condition
   author : vipin reddy */
  terms() {
    window.scroll(0, 0);
  }
  /* Description: update employee count in package and billing page
   author : vipin reddy */
  updateEmployeeCount() {
    
    var postData = {
      companyDetails: {
        employeCount: this.addedCompanyData.employeCount
      },
      companyId: this.addedCompanyData._id
    }
    this.authenticationService.updateCompanyDetail(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Employee Count', res.message, 'success')
            localStorage.removeItem('addedCompany');
            localStorage.setItem('addedCompany', JSON.stringify(res.data));
            this.getSpecificPackageDetails()

          }
        },
        (err) => {
          console.log(err);

        })

  }
  /* Description: change plan in final billing tab
   author : vipin reddy */
  changePlan(stepper: MatStepper) {
    console.log("plan");
    // clickButton(index: number, stepper: MatStepper) {
    stepper.selectedIndex = 0;


  }

}
