import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ZenworkersService } from '../../services/zenworkers.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { LoaderService } from '../../services/loader.service';
import { CompanySettingsService } from '../../services/companySettings.service';
import { fakeAsync } from '@angular/core/testing';
import { ConfirmationComponent } from '../../shared-module/confirmation/confirmation.component';


@Component({
  selector: 'app-create-client-component',
  templateUrl: './create-zenworker-template-component.component.html',
  styleUrls: ['./create-zenworker-template-component.component.css']
})
export class CreateZenworkerTemplateComponent implements OnInit {

  public createZenworker = {
    name: '',
    email: '',
    roleId: '',
    address: { state: '' },
    status: ''
  }
  isValid: boolean = false;
  userProfile: any;
  allStates = [];
  data1: boolean = false;
  text: any;
  note: any;
  constructor(
    public dialogRef: MatDialogRef<CreateZenworkerTemplateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private zenworkersService: ZenworkersService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private companySettingsService: CompanySettingsService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    if (this.data) {
      this.data1 = true;
      this.createZenworker = this.data;
    }
    this.getAllRoles();
    this.getAllUSstates();
  }
    /* Description: get all us-states from structure
 author : vipin reddy */
  getAllUSstates() {
    this.companySettingsService.getStuctureFields('State',0)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allStates.push(element)
              console.log("22222111111", this.allStates);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allStates.push(element)
            });
            console.log("22222", this.allStates);
          }
        },
        (err) => {
          console.log(err);

        })
  }
      /* Description: add zenworker submit
 author : vipin reddy */

  addZenworker() {
    console.log("FFFFFFF")
    this.isValid = true;
    // this.loaderService.loader(true);
    /* if (!this.createZenworker.name) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Name", 'error')
    } else if (!this.createZenworker.email) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Valid Email", 'error')
    } else if (!this.createZenworker.type) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Select Valid Zenworker Role", 'error')
    } else if (!this.createZenworker.address.state) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Add Address", 'error')
    } else if (!this.createZenworker.status) {
      this.swalAlertService.Swe
      etAlertWithoutConfirmation("Zenworker", "Please Select Status", 'error')
    } else { */
    if (this.createZenworker.name && this.createZenworker.email && this.createZenworker.address.state &&
      this.createZenworker.roleId && this.createZenworker.status) {
      if (this.createZenworker.hasOwnProperty('_id')) {
        this.zenworkersService.editZenworker(this.createZenworker)
          .subscribe(
            (res) => {
              console.log("res", res);
              this.dialogRef.close();
            },
            (err) => {
              console.log("err", err);
            }
          )
      } else {
        console.log("22222222222senddata", this.createZenworker);

        this.zenworkersService.addZenworker(this.createZenworker)
          .subscribe(
            (res: any) => {
              console.log("res", res);
              this.dialogRef.close();
              this.loaderService.loader(false);
              this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')

            },
            (err: HttpErrorResponse) => {
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          )
      }
    }
    /* } */
  }
  /* Description: get all levels (level1 & level2 & billing)
 author : vipin reddy */
  getAllRoles() {

    this.zenworkersService.getAllroles()
      .subscribe(
        (res: any) => {
          console.log("111111111", res);
          if (res.status == true) {
            // this.loaderService.loader(false)
            this.userProfile = res.data;
            this.userProfile.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }
  profileChange() {

  }
  /* Description: modal close
 author : vipin reddy */
  close() {
    this.text = "The data entered will be lost,";
    this.note = "Would you like to proceed";
    if (this.createZenworker.name == '' && this.createZenworker.email == '' && this.createZenworker.address.state == '' &&
      this.createZenworker.roleId == '' && this.createZenworker.status == '') {
        this.dialogRef.close();
      
    }
    else {
      this.openDialog()
    }
    // this.dialogRef.close();
  }

  /* Description: open popup for adding zenworker
 author : vipin reddy */
  openDialog(): void {

    let dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '550px',
      data: { txt: this.text, note: this.note },
      panelClass: 'confirm-class'

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.dialogRef.close()
      }
      // if (data == 'delete') {
      //   this.deleteSingleCategory(id)
      // }



    });
  }

}
