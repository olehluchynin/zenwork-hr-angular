import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanyService } from '../../services/company.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { Router } from '@angular/router';
import { PackageAndAddOnService } from '../../services/packageAndAddon.service';
import { PurchasePackageService } from '../../services/purchasePackage.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { CreditCardChargingService } from '../../services/chardgeCreditCard.service';
import { ActivatedRoute } from '@angular/router';
import { MatSlideToggleChange, MatCheckbox } from '@angular/material';
import { MessageService } from '../../services/message-service.service';
import { CompanySettingsService } from '../../services/companySettings.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.css']
})
export class CreateCompanyComponent implements OnInit {

  public isValid: any;


  public activeTab: any;
  public selectedPackage = {
    clientId: '',
    name: '',
    wholeSaleDiscount: false,
    discount: 0,
    costPerEmployee: 0,
    price: 0,
    startDate: new Date(),
    endDate: new Date(new Date().setMonth(new Date().getMonth() + 1)),
    adons: [],
    totalPrice: 0,
    packages: [],
    billingFrequency: 'monthly'
  };
  public companyDetails = {

    name: '',
    tradeName: '',
    employeCount: 0,
    email: '',
    type: '',
    belongsToReseller: false,
    resellerId: '',
    primaryContact: { name: '', email: '', phone: '', extention: '' },
    billingContact: { name: '', email: '', phone: '', extention: '' },
    companyDetails: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },
    primaryAddress: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },
    billingAddress: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },
    isPrimaryContactIsBillingContact: true,
    isPrimaryAddressIsBillingAddress: true

  };
  userData:any;
  public packages;
  public selectedPackageData: any = {};
  public allAddOns;
  public orderId;
  invoiceadons: any;
  public dummy2;
  public addedCompanyData: any = {};
  url: any;
  id: any;
  dummy: any;
  comapnyInactiveDate: any;
  clientStatus: any;
  billingHistory = {};
  companyNameValue: any;
  currentPlan = {

  };
  discountValue: any;
  discountEnable: boolean = true;
  basePriceDiscount: any;
  basePrice: any;
  validity: boolean = false;
  selectedAddons: any;
  billingHistoryAddons = [];
  billingHistoryPackages = [];
  currentPlanAddons = [];
  currentPlanPackage = [];
  resellersList = [];
  adonsPrice: number = 0;
  dicountArray = [
    { value: 10, viewValue: '10%' },
    { value: 20, viewValue: '20%' },
    { value: 30, viewValue: '30%' },
    { value: 40, viewValue: '40%' },
    { value: 50, viewValue: '50%' },
    { value: 60, viewValue: '60%' },
    { value: 70, viewValue: '70%' },
    { value: 80, viewValue: '80%' },
    { value: 90, viewValue: '90%' },

  ];
  monthsArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  yearsArray = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028]


  editCompamnyDetails: boolean = false;
  zipcode: any;
  country: any;
  taxrate: any = {
    tax_rate: '',
    _id: ''
  }
  allStates = [];
  isValidcard: boolean = false;
  tempforDiscountChange: any;
  allCountryes = [];
  constructor(
    private companyService: CompanyService,
    private swalAlertService: SwalAlertService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private packageAndAddOnService: PackageAndAddOnService,
    private purchasePackageService: PurchasePackageService,
    private accessLocalStorageService: AccessLocalStorageService,
    private creditCardChargingService: CreditCardChargingService,
    private messageService: MessageService,
    private companySettingsService: CompanySettingsService,
    private ngZone: NgZone
  ) {


  }

  ngOnInit() {
    this.activeTab = 'create';
    this.discountValue = '';
    this.companyDetails = {
      name: '',
      tradeName: '',
      employeCount: 0,
      email: '',
      type: '',
      belongsToReseller: false,
      resellerId: '',
      primaryContact: { name: '', email: '', phone: '', extention: '' },
      billingContact: { name: '', email: '', phone: '', extention: '' },
      companyDetails: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },
      primaryAddress: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },

      billingAddress: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },

      isPrimaryContactIsBillingContact: true,
      isPrimaryAddressIsBillingAddress: true
    }

    this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    if (this.addedCompanyData == null) {
      this.addedCompanyData = {}
    }
    this.activatedRoute.params.subscribe(params => {

      this.id = params['id'];
      console.log("iddddd", this.id, params);
      if (this.id) {
        this.editCompamnyDetails = true;
        this.activeTab = JSON.parse(localStorage.getItem('viewTab'));

      }
    })
    console.log('activeTab', this.activeTab);



    this.getSingleUser();
    this.getAllPackages();
    this.getAllAddons();
    this.getSingleCompanyBillingHistory();
    this.getSingleComapnyCurrentPlan();
    this.getAllresellers();
    this.getTaxRate();
    this.getAllUSstates();
    this.getAllCountry();
  }
    /* Description: get all us-states form dtructure
   author : vipin reddy */
  getAllUSstates() {
    this.companySettingsService.getStuctureFields('State', 0)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allStates.push(element)
              console.log("22222111111", this.allStates);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allStates.push(element)
            });
            console.log("22222", this.allStates);
          }
        },
        (err) => {
          console.log(err);

        })
  }

  /* Description: get taxrate
   author : vipin reddy */ 
  getTaxRate() {
    this.packageAndAddOnService.getTaxRate()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.taxrate = res.data
        },
        (err) => {
          console.log(err);
        })
  }

    /* Description: get single client details
   author : vipin reddy */ 
  getSingleUser() {
    this.companyService.editClient(this.id)
      .subscribe(
        (res: any) => {
          console.log("editres", res);
          this.companyDetails = res.data;

          if (this.companyDetails.type == 'reseller-client') {
            var x = this.companyDetails.type;
            this.companyDetails.type = 'company';
            this.companyDetails.belongsToReseller = true;
          }
          if (this.companyDetails.type == "company" && x != 'reseller-client') {
            this.companyDetails.type = 'company';
            this.companyDetails.belongsToReseller = false;
          }
          if (this.companyDetails.employeCount == null) {
            this.companyDetails.employeCount = 0;
          }
          if (this.companyDetails.billingAddress == undefined) {

            this.companyDetails.primaryContact = {
              name: '',
              email: '',
              phone: '',
              extention: ''
            }
            this.companyDetails.primaryAddress = {
              address1: "",
              address2: "",
              city: "",
              country: "",
              state: "",
              zipcode: ""
            }
            this.companyDetails.billingAddress = {
              address1: "",
              address2: "",
              city: "",
              country: "",
              state: "",
              zipcode: ""
            }
          }
          else {
            this.companyDetails.primaryContact = res.data.primaryContact;

            this.companyDetails.billingContact = res.data.billingAddress;
            this.companyDetails.primaryAddress = res.data.primaryAddress;
            this.companyDetails.billingAddress = res.data.billingAddress;
          }
          if (res.order) {
            var date = new Date(res.order.endDate);
            console.log(date);
            this.comapnyInactiveDate = date.toISOString();
            console.log(this.comapnyInactiveDate);
            var today = new Date().toISOString();
            if (today < this.comapnyInactiveDate) {
              this.clientStatus = "Active";
            }
            else {
              this.clientStatus = "Inactive"
            }
            if (res.order == null) {
              this.clientStatus = "Inactive"
              this.comapnyInactiveDate = "Inactive";
            }
            else {
              this.comapnyInactiveDate = res.order.endDate;
              var d = new Date(this.comapnyInactiveDate);
              this.comapnyInactiveDate = d.toLocaleDateString();
            }
          }
        },
        (err: any) => {
          console.log(err);

        })
  }
  /* Description: get all us-states
   author : vipin reddy */ 
  getAllCountry() {
    this.companySettingsService.getStuctureFields('Country', 0)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allCountryes.push(element)
              console.log("countryes", this.allCountryes);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allCountryes.push(element)
            });
            console.log("countryes", this.allCountryes);
          }
        },
        (err) => {
          console.log(err);

        })

  }

    /* Description: get a billing history of edited company
   author : vipin reddy */ 

  getSingleCompanyBillingHistory() {
    this.addedCompanyData = this.accessLocalStorageService.get('editCompany');
    console.log(this.addedCompanyData);
    this.companyNameValue = this.addedCompanyData.name
    console.log(this.companyNameValue);

    this.id = this.addedCompanyData._id;
    this.companyService.getBillingHistory(this.id)
      .subscribe(
        (res: any) => {
          console.log("billinghistory", res);
          if (res.data) {
            this.billingHistory = res.data;
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].adons.length >= 1) {
                if (res.data[i].adons.length == 1) {
                  this.billingHistoryAddons.push(res.data[i].adons);
                }
                if (res.data[i].adons.length > 1) {
                  console.log(res.data[i].adons);
                  for (var j = 0; j < res.data[i].adons.length; j++) {
                    this.billingHistoryAddons.push([res.data[i].adons[j]])
                  }

                }

              }
              if (res.data[i].packages.length >= 1) {
                this.billingHistoryPackages.push(res.data[i].packages);
              }
              console.log("adins,packages", this.billingHistoryAddons, this.billingHistoryPackages);

            }
          }
          else {
            this.billingHistoryPackages = [];
            this.billingHistoryAddons = [];
          }
        },
        (err: any) => {

        })
  }
  // this.companyDetails = {
  //   name: '',
  //   tradeName: '',
  //   employeCount: 0,
  //   email: '',
  //   type: '',
  //   belongsToReseller: false,
  //   resellerId: '',
  //   primaryContact: { name: '', email: '', phone: '', extention: '' },
  //   billingContact: { name: '', email: '', phone: '', extention: '' },
  //   
  //   primaryAddress: {
  //     address1: '',
  //     address2: '',
  //     city: '',
  //     state: '',
  //     zipcode: '',
  //     country: ''
  //   },

  //   billingAddress: {
  //     address1: '',
  //     address2: '',
  //     city: '',
  //     state: '',
  //     zipcode: '',
  //     country: ''
  //   },

  //   isPrimaryContactIsBillingContact: true,
  //   isPrimaryAddressIsBillingAddress: true
  // }

      /* Description: save edited contact details 
   author : vipin reddy */ 
  saveEditDetails() {
    this.isValid = true;
    var comapnyData = this.accessLocalStorageService.get('editCompany');
    console.log(this.companyDetails);

    this.id = comapnyData._id;
    if (this.companyDetails.name && this.companyDetails.tradeName && this.companyDetails.employeCount &&
      this.companyDetails.type && this.companyDetails.primaryContact.name && this.companyDetails.primaryContact.email &&
      this.companyDetails.primaryContact.phone && this.companyDetails.primaryContact.extention && this.companyDetails.primaryAddress.address1 &&
      this.companyDetails.primaryAddress.address2 && this.companyDetails.primaryAddress.city && this.companyDetails.primaryAddress.state &&
      this.companyDetails.primaryAddress.zipcode && this.companyDetails.primaryAddress.country) {
      this.companyService.editDataSave(this.companyDetails, this.id)
        .subscribe(
          (res: any) => {
            console.log(res);
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation("Client Details", res.message, 'success')
              this.activeTab = "package";
              console.log("this.activeTab", this.activeTab);

            }
            console.log(this.activeTab);

          },
          (err: any) => {

          })
    }
  }

    /* Description: save single comapny current plan 
   author : vipin reddy */ 

  getSingleComapnyCurrentPlan() {
    var comapnyData = this.accessLocalStorageService.get('editCompany');
    console.log(comapnyData);
    this.id = comapnyData._id;
    this.companyService.currentPlan(this.id)
      .subscribe(
        (res: any) => {
          console.log("currentPlan", res);
          if (res.data) {

            this.currentPlan = res.data;
            this.currentPlanAddons = res.data.adons;
            this.currentPlanPackage = res.data.packages;
          }
          else {
            this.currentPlan = []
            this.currentPlanAddons = []
            this.currentPlanPackage = []
          }


        },
        (err: any) => {

        })
  }


  // discountBasePrice() {
  //   console.log(this.discountValue);
  //   if (this.discountValue <= 100) {
  //     console.log(x, this.dummy2);

  //     var x = (this.dummy2 * this.discountValue / 100)
  //     this.selectedPackage.totalPrice = this.dummy2 - x;
  //     console.log(this.selectedPackage.totalPrice);

  //     // this.getSpecificPackageDetails();
  //   }

  //   else {
  //     this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error')
  //     this.discountValue = 0
  //     this.getSpecificPackageDetails();
  //     this.addOnChange();
  //   }
  // }


    /* Description: discont price when editing
   author : vipin reddy */ 
  discountBasePrice() {
    console.log(this.discountValue);
    if (this.discountValue <= 100) {
      var x = (this.tempforDiscountChange * this.discountValue / 100)
      this.selectedPackage.totalPrice = this.tempforDiscountChange - x;
      // this.getSpecificPackageDetails();
    }
    else {
      this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error')
      this.discountValue = ''
      this.getSpecificPackageDetails();
      this.addOnChange();
    }
  }

    /* Description: time-period change in packages and billing monthly and yearly
   author : vipin reddy */ 


  toggleDateChange(event: MatSlideToggleChange) {
    console.log(event.checked);
    if (event.checked == true) {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
      this.selectedPackage.billingFrequency = 'annually';
      this.selectedPackage.costPerEmployee = this.selectedPackage.costPerEmployee * 12;
      this.validity = true;
    }

    else {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
      this.selectedPackage.costPerEmployee = this.selectedPackage.costPerEmployee;

      this.validity = false;
      this.selectedPackage.billingFrequency = 'monthly';
    }

    this.getSpecificPackageDetails()
    // if (this.selectedPackage.adons.lenght > 0) {
    //   // this.get
    // }
  }


  public cardDetails = {
    cardNumber: '',
    expMonth: '',
    expYear: '',
    cvv: '',
    cardName: ''
  }
      /* Description: upgrade downgrade package payment for stripe 
   author : vipin reddy */ 

  chargeCreditCard() {
    this.isValidcard = true;
    (<any>window).Stripe.card.createToken({
      number: this.cardDetails.cardNumber,
      exp_month: this.cardDetails.expMonth,
      exp_year: this.cardDetails.expYear,
      cvc: this.cardDetails.cvv
    }, (status: number, response: any) => {
      if (status === 200) {
        let token = response.id;
        console.log(token)
        // this.chargeCard(token);
        var id = JSON.parse(localStorage.getItem("editCompany"))
        console.log(id)
        var amount: any = (this.selectedPackage.totalPrice + (this.taxrate.tax_rate)) / 100
        // var amount = this.selectedPackage.totalPrice + this.selectedPackage.totalPrice * 0.08
        amount = Math.round(amount * 100);
        console.log(amount);
        this.creditCardChargingService.createCharge(token, id._id, amount, this.orderId)
          .subscribe(
            (response: any) => {
              console.log(response);
              if (response.status == true) {
                this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')
                // this.messageService.sendMessage('super-admin');
                // localStorage.removeItem('viewTab');
                this.cardDetails.cardNumber = '';
                this.cardDetails.expMonth = '';
                this.cardDetails.expYear = '';
                this.cardDetails.cvv = '';
              this.ngZone.run(()=>{
                this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
              })  
              }

            },
            err => {
              console.log(err);
            }
          )
      } else {
        console.log(response.error.message);

      }

    });

  }





  cancel() {
    this.companyDetails = {
      name: '',
      tradeName: '',
      employeCount: 0,
      email: '',
      type: '',
      belongsToReseller: false,
      resellerId: '',
      primaryContact: { name: '', email: '', phone: '', extention: '' },
      billingContact: { name: '', email: '', phone: '', extention: '' },
      companyDetails: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },
      primaryAddress: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },

      billingAddress: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },

      isPrimaryContactIsBillingContact: true,
      isPrimaryAddressIsBillingAddress: true
    }
  }

        /* Description: get all packages
   author : vipin reddy */ 

  getAllPackages() {
    this.packageAndAddOnService.getAllPackages()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.packages = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

   /* Description: discount toggle setup
   author : vipin reddy */ 
  wholesaleDiscountChange($event) {
    console.log($event, this.companyDetails.type);

    if (this.companyDetails.type == 'reseller') {
      this.selectedPackage.wholeSaleDiscount = false;
    }
    else {
      this.selectedPackage.wholeSaleDiscount = true;
    }
  }
   /* Description: get selected package details
   author : vipin reddy */ 

  getSpecificPackageDetails() {
    this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    console.log(this.addedCompanyData);
    if (this.editCompamnyDetails) {
      console.log("sdasdas");
      this.addedCompanyData = this.accessLocalStorageService.get('editCompany');
    }

    console.log(this.selectedPackage.name, this.packages);
    this.packages.forEach(packageData => {
      if (packageData.name == this.selectedPackage.name) {
        this.selectedPackage.packages = this.packages.filter(data => data.name == this.selectedPackage.name)
        console.log(this.selectedPackage.packages);

        if (this.validity == false) {
          this.selectedPackage.price = packageData.price;
        }
        else {
          this.selectedPackage.price = (packageData.price * 12);
        }
        if (this.validity == false)
          this.selectedPackage.costPerEmployee = packageData.costPerEmployee;

      }
    }); for (var i = 0; i < this.allAddOns.length; i++) {
      console.log(this.allAddOns);

      for (var j = 0; j < this.selectedPackage.adons.length; j++) {
        console.log(this.selectedPackage.adons[j]);

        if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn) {
          if (this.validity == false) {
            this.getAllAddons()
            this.adonsPrice = 0;
            if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

              this.selectedPackage.adons[j].price = this.allAddOns[i].price;
          }
          else {

            this.selectedPackage.adons[j].price = this.selectedPackage.adons[j].price * 12;
          }
          // this.selectedPackage.costPerEmployee = this.allAddOns[i].costPerEmployee;
        }
      }
    }

    //   this.allAddOns.forEach(element => {
    //   this.selectedPackage.adons.forEach(selectAdonsData => {
    //     if (element.name == selectAdonsData.selectedAddOn) {
    //       if (this.validity == false) {
    //         this.selectedPackage.adons = selectAdonsData.price;
    //       }
    //       else {
    //         this.selectedPackage.adons = (selectAdonsData.price * 12);
    //         // selectAdonsData.price

    //       }
    //     }
    //   });

    // });


    this.adonsPrice = 0;
    this.selectedPackage.adons.forEach(element => {
      console.log(element);


      this.adonsPrice = this.adonsPrice + element.price
    });
    console.log(this.adonsPrice);


    this.selectedPackage.totalPrice = this.selectedPackage.price + this.adonsPrice + this.selectedPackage.costPerEmployee * this.addedCompanyData.employeCount;
    // if (this.basePriceDiscount > 0) {
    //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
    // }
    this.discountEnable = false;
    /* this.selectedPackageData = packageData; */
    /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.selectedPackageData = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      ) */
    this.dummy = this.selectedPackage.totalPrice;
    this.tempforDiscountChange = this.selectedPackage.totalPrice;
  }
   /* Description: get all addons
   author : vipin reddy */ 
  getAllAddons() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            console.log(this.allAddOns);

            this.allAddOns.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
     /* Description: selecting reseller edit
   author : vipin reddy */ 

  selectedReseller(id) {
    console.log(id);

  }
     /* Description: get all resellers
   author : vipin reddy */  
  getAllresellers() {
    this.companyService.getresellers()
      .subscribe(
        (res: any) => {
          console.log("reselerrsList", res);
          this.resellersList = res.data;
        },
        (err: any) => {

        })
  }

     /* Description: get all addons
   author : vipin reddy */ 

  getAllAddOnDetails() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            this.allAddOns.forEach(addon => {
              addon.isSelected = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

    /* Description: add another addon to order
   author : vipin reddy */

  addAnotherAddOn() {
    console.log('adons add', this.selectedPackage);

    this.selectedPackage.adons.push({
      id: '',
      selectedAddOn: '',
      price: 0,
      index: this.selectedPackage.adons.length + 1
    })
    // this.selectedPackage.addOns = []

  }
      /* Description: addon change 
   author : vipin reddy */
  addOnChange() {
    // console.log(addOn);

    console.log("before", this.selectedPackage.adons);

    // for(let ref of this.selectedPackage.addOns){
    //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
    // }
    // console.log("after",this.selectedPackage.addOns);

    // this.selectedPackage.totalPrice = 0
    for (let i = 0; i < this.allAddOns.length; i++) {
      this.selectedPackage.adons.forEach(addon => {
        console.log(addon, this.allAddOns[i])

        if (addon.selectedAddOn == this.allAddOns[i].name) {

          addon.price = this.allAddOns[i].price;
          if (this.validity == true) {
            addon.price = addon.price * 12
          }
          addon.id = this.allAddOns[i]._id;
          console.log(addon.id);
          this.selectedAddons = this.selectedPackage.adons[i];
          // this.selectedAddons['id'] = addon.id;
          console.log(this.selectedAddons);

        }
      });
    }
    var price = 0;
    this.selectedPackage.adons.forEach(data => {
      price += data.price
    })
    console.log(price);
    // this.adonsPrice = 0;
    if (price > 0) {
      this.selectedPackage.totalPrice = this.dummy + price;
    }
    this.tempforDiscountChange = this.selectedPackage.totalPrice;
    if (this.discountValue) {
      this.discountBasePrice();
    }
    console.log(this.selectedPackage.totalPrice);

  }
   /* Description: addon remove
   author : vipin reddy */
  addonRemove(addon) {

    // if (addon == '') {
    //   this.selectedPackage.addOns = []
    // }
    console.log("addon", addon);
    // for (let i = 0; i < this.allAddOns.length; i++) {
    //   this.selectedPackage.addOns.forEach(addon => {
    //     console.log(addon, this.allAddOns[i])
    //     if (addon == this.allAddOns[i].name) {

    //    }
    //   });
    // }
    this.selectedPackage.adons.forEach(data => {
      console.log(data);

      if (data.selectedAddOn == addon) {
        console.log(data.price);

        this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - data.price;
      }
    });
    console.log(this.selectedPackage.totalPrice);


    this.selectedPackage.adons = this.selectedPackage.adons.filter(data => addon != data.selectedAddOn)


    if (this.discountValue) {
      this.discountBasePrice();
    }
    // this.addOnChange()
  }

  /* Description: save selected packaege details
 author : vipin reddy */

  savePackageDetails(event) {
    console.log("this.selectedPackage", this.selectedPackage);
    var comapnyData = this.accessLocalStorageService.get('editCompany');

    this.selectedPackage.clientId = comapnyData._id;
    // this.invoiceadons = this.selectedPackage.adons;

    // var postData = this.selectedPackage
    // delete postData.adons
    // postData['adons'] = this.invoiceadons;

    // console.log(postData);

    this.purchasePackageService.purchasePackage(this.selectedPackage)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status == true) {

            this.orderId = res.data._id
            // localStorage.setItem('ordreId',this.orderId);
            event.target.classList.remove('active'); // To Remove
            // this.activeTab = "";

            this.activeTab = "finished";
            console.log(this.activeTab);
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success')


          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error')

        }
      )
  }
    /* Description: prinit numbers only
 author : vipin reddy */
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  // savebillingHistory(){
  //   this.activeTab = 'payment'
  // }

}
