import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SuperAdminRouting } from './super-admin-dashboard.routing';
import { SharedModule } from '../shared-module/shared.module';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CreateZenworkerTemplateComponent } from './create-zenworker-template-component/create-zenworker-template-component.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { MatIconModule } from '@angular/material';

import { MaterialModuleModule } from '../material-module/material-module.module';






@NgModule({
    declarations: [CreateZenworkerTemplateComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MatDialogModule,
        MatSelectModule,
        MatSlideToggleModule,
        HttpClientModule,
        SuperAdminRouting,
        SharedModule,
        MatCheckboxModule,
        MatIconModule,
        MaterialModuleModule
    ],
    providers: [
    ]
})
export class SuperAdminDashboardModule { }
