import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CreateUserProfileTemplateComponent } from '../create-user-profile-template/create-user-profile-template.component';
import { ZenworkersService } from '../../../services/zenworkers.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { LoaderService } from '../../../services/loader.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  userProfile: any;
  public checkAllAddOns: boolean = false;
  public selectedAddOnDetails;
  id : any;
  constructor(
    public dialog: MatDialog,
    private zenworkersService: ZenworkersService,
    private swalAlertService: SwalAlertService,
    private loaderService:LoaderService,
  ) { }

  ngOnInit() {
    this.loaderService.loader(true)
    this.getAllRoles();
 

  }


   /* Description: get all levels
  author : vipin reddy */


  getAllRoles() {
    
    this.zenworkersService.getAllroles()
      .subscribe(
        (res: any) => {
          console.log(res);
          if(res.status == true){
            this.loaderService.loader(false)
          this.userProfile = res.data;
          this.userProfile.forEach(addonsData => {
            addonsData.isChecked = false;
          });
        }
      },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }
  // this.selectedAddOnDetails = this.addons.filter(addonsData => {
  //   return addonsData.isChecked;
  // })

   /* Description: addedit level popup
  author : vipin reddy */

  addEditUser(data) {
    const dialogRef = this.dialog.open(CreateUserProfileTemplateComponent, {
      height: '700px',
      width: '890px',
      data: data,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("Result", result)
      this.getAllRoles();
    });
  }

   /* Description: single edit for level
  author : vipin reddy */


  editUserProfile() {
    this.selectedAddOnDetails = this.userProfile.filter(addonsData => {
      console.log(addonsData.isChecked);
      // this.selectedAddOnDetails[0]._id]
      return addonsData.isChecked;

    })
    if (this.selectedAddOnDetails.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select only one profile to Proceed", 'error')
    } else if (this.selectedAddOnDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select one profile to Proceed", 'error')
    }
    else {
      console.log(this.selectedAddOnDetails[0]._id);
      
      this.addEditUser(this.selectedAddOnDetails[0]._id)
    }


  }

   /* Description: select all all levels
  author : vipin reddy */


  selectAll() {
    this.userProfile.forEach(addonsData => {
      addonsData.isChecked = this.checkAllAddOns
    });

  }

}


