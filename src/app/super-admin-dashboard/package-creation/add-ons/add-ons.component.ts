import { Component, OnInit } from '@angular/core';
import { PackageAndAddOnService } from '../../../services/packageAndAddon.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { LoaderService } from '../../../services/loader.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-ons',
  templateUrl: './add-ons.component.html',
  styleUrls: ['./add-ons.component.css']
})
export class AddOnsComponent implements OnInit {

  constructor(
    private packageAndAddOnService: PackageAndAddOnService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private router: Router
  ) { }

  public addons;

  public checkAllAddOns: boolean = false;

  public selectedAddOnDetails;
  perPageforaddons: any;
  pageNoforaddons: any;
  addonsCount: any;


  ngOnInit() {
    this.perPageforaddons = 10;
    this.pageNoforaddons = 1;
    this.getAllAddons();
  }
   /* Description: mat pagination for adons list
  author : vipin reddy */
  pageEventsforAddons(event) {
    console.log(event);
    this.pageNoforaddons = event.pageIndex + 1;
    this.perPageforaddons = event.pageSize;
    this.getAllAddons();
  }
     /* Description: get all adons list
  author : vipin reddy */

  getAllAddons() {
    this.loaderService.loader(true);
    var postData = {
      page_no: this.pageNoforaddons,
      per_page: this.perPageforaddons
    }
    this.packageAndAddOnService.getAllAddOnslist(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.addons = res.data;
            this.addonsCount = res.count;
            this.addons.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }


     /* Description: edit single addon  
  author : vipin reddy */
  editAddOns() {
    this.selectedAddOnDetails = this.addons.filter(addonsData => {
      return addonsData.isChecked;
    })
    if (this.selectedAddOnDetails.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Add-on", "Select only one add-on to Proceed", 'error')
    } else if (this.selectedAddOnDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Add-on", "Select a add-on to Proceed", 'error')
    } else {
      this.router.navigate(['/super-admin/super-admin-dashboard/settings/add-ons-add/' + this.selectedAddOnDetails[0]._id])
    }
  }
       /* Description: delete multiple adons
  author : vipin reddy */
  deleteAddOns() {
    this.selectedAddOnDetails = this.addons.filter(addonsData => {
      return addonsData.isChecked;
    })

    var deletedIds = [];
    if (deletedIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Delete Addons", "Select a Addon to Proceed", 'error')
    }
    if (deletedIds.length >= 1) {
      this.selectedAddOnDetails.forEach(element => {
        deletedIds.push(element._id)
      });
      var postData = {
        _ids: deletedIds
      }
      console.log(deletedIds);
      this.packageAndAddOnService.deleteAddons(postData)
        .subscribe(
          (res: any) => {
            console.log("Response", res);
            this.swalAlertService.SweetAlertWithoutConfirmation('Addons', res.message, "success")
            this.getAllAddons()
          },
          (err) => {
            console.log(err);
            this.swalAlertService.SweetAlertWithoutConfirmation('Addons', err.error.message, "error")
          })
    }
  }

       /* Description: select all adons
  author : vipin reddy */
  selectAll() {
    this.addons.forEach(addonsData => {
      addonsData.isChecked = this.checkAllAddOns
    });

  }



}
