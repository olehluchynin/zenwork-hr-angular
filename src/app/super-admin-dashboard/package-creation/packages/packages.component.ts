import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { PackageAndAddOnService } from '../../../services/packageAndAddon.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { Router } from '@angular/router';
import { LoaderService } from '../../../services/loader.service';


@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.css']
})
export class PackagesComponent implements OnInit {

  constructor(
    private packageAndAddOnService: PackageAndAddOnService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private router: Router
  ) { }

  public packages;
  public selectedPackageDetails = []
  public coupons;
  public selectAllCoupons: boolean = false;
  public selectPackages: boolean = false;
  public selectCouponDetails;
  pageNoforPackage: any;
  perPageforPackage: any;
  perPageforCoupon: any;
  pageNoforCoupon: any;
  deleteCouponsArray = [];
  taxrate = {
    tax_rate: '',
    _id: ''
  };
  packagesCount: any;
  countforCoupon: any;
  deletedIds = [];

  ngOnInit() {
    this.pageNoforPackage = 1;
    this.perPageforPackage = 5;
    this.pageNoforCoupon = 1;
    this.perPageforCoupon = 5;
    this.getAllPackages();
    this.getAllCoupons();
    this.getTaxRate();
  }
   /* Description: mat pagination for packages
  author : vipin reddy */
  pageEventsforPackages(event) {
    console.log(event);
    this.pageNoforPackage = event.pageIndex + 1;
    this.perPageforPackage = event.pageSize;
    this.getAllPackages();
  }
     /* Description: mat pagination for coupons
  author : vipin reddy */

  pageEventsforCoupons(event) {
    console.log(event);

    this.pageNoforCoupon = event.pageIndex + 1;
    this.perPageforCoupon = event.pageSize;
    this.getAllCoupons();
  }

   /* Description: get all packages list view
  author : vipin reddy */
  getAllPackages() {
    this.loaderService.loader(true);
    var postData = {
      page_no: this.pageNoforPackage,
      per_page: this.perPageforPackage
    }
    this.packageAndAddOnService.getAllPackagesforSettings(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);

          if (res.status) {
            this.packages = res.data;
            this.packagesCount = res.count;
            this.packages.forEach(packageData => {
              packageData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
     /* Description: eidt Update package
  author : vipin reddy */

  editPackage() {
    this.selectedPackageDetails = this.packages.filter(packageData => {
      return packageData.isChecked;
    })
    if (this.selectedPackageDetails.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select only one package to Proceed", 'error')
    } else if (this.selectedPackageDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select a package to Proceed", 'error')
    } else {
      this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages-add/' + this.selectedPackageDetails[0]._id])
    }
  }


  packagesLenght() {
    this.selectedPackageDetails = this.packages.filter(packageData => {
      return packageData.isChecked;
    })

  }
     /* Description: delete package
  author : vipin reddy */
  deletePackages() {
    this.selectedPackageDetails = this.packages.filter(packageData => {
      return packageData.isChecked;
    })
    console.log("all packges ids", this.selectedPackageDetails);

    this.selectedPackageDetails.forEach(element => {
      this.deletedIds.push(element._id)
    });
    if (this.deletedIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Delete Package", "Select a package to Proceed", 'error')
    }
    if (this.deletedIds.length >= 1) {
      console.log(this.deletedIds);
      var postData = {
        "_ids": this.deletedIds
      }
      this.packageAndAddOnService.deletePackages(postData)
        .subscribe(
          (res: any) => {
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation('Package', res.message, 'success')
              this.getAllPackages();
            }
          },
          (err) => {
            this.swalAlertService.SweetAlertWithoutConfirmation('Package', err.error.message, 'error')
          })



    }
  }

   /* Description: delete multiple addons
  author : vipin reddy */
  deleteAdons() {
    this.deleteCouponsArray = this.coupons.filter(adonData => {
      return adonData.isChecked;
    })
    console.log("all packges ids", this.deleteCouponsArray);
    var deletedIds = [];
    this.deleteCouponsArray.forEach(element => {
      deletedIds.push(element._id)
    });
    console.log(deletedIds);
    if (deletedIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Delete Coupons", "Select a Coupon to Proceed", 'error')
    }
    if (deletedIds.length >= 1) {
      var postData = {
        "_ids": deletedIds
      }
      this.packageAndAddOnService.deleteCoupons(postData)
        .subscribe(
          (res: any) => {
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation('Coupons', res.message, 'success')
              this.getAllCoupons();
            }
          },
          (err) => {
            this.swalAlertService.SweetAlertWithoutConfirmation('Coupons', err.error.message, 'error')
          })

    }
  }
     /* Description: taxrate sending
  author : vipin reddy */
  taxrateSend() {
    console.log("taxrate", this.taxrate);
    var postdata = {
      tax_rate: this.taxrate.tax_rate,
      _id: this.taxrate._id
    }
    this.packageAndAddOnService.updateTaxRate(postdata)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Taxrate', res.message, 'success')
          }
        },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation('Taxrate', err.error.message, 'error')
        })

  }

     /* Description: accept only numbers
  author : vipin reddy */
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

   /* Description: get all coupons for list view
  author : vipin reddy */
  getAllCoupons() {
    this.loaderService.loader(true);
    var postData = {
      page_no: this.pageNoforCoupon,
      per_page: this.perPageforCoupon
    }
    this.packageAndAddOnService.getAllCouponsforAll(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.coupons = res.data;
            this.countforCoupon = res.count;
            this.coupons.forEach(couponData => {
              couponData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Coupons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

   /* Description: edit update coupon 
  author : vipin reddy */

  editCoupon() {
    this.selectCouponDetails = this.coupons.filter(couponData => {
      return couponData.isChecked;
    })
    if (this.selectCouponDetails.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Coupons", "Select only one coupon to Proceed", 'error')
    } else if (this.selectCouponDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Coupons", "Select a coupon to Proceed", 'error')
    } else {
      this.router.navigate(['/super-admin/super-admin-dashboard/settings/coupons-add/' + this.selectCouponDetails[0]._id])
    }
  }

   /* Description: select all packages
  author : vipin reddy */

  selectAllPackages() {
    this.packages.forEach(packageData => {
      packageData.isChecked = this.selectPackages
    })
  }

   /* Description: select all coupons
  author : vipin reddy */


  selectAll() {
    this.coupons.forEach(couponData => {
      couponData.isChecked = this.selectAllCoupons
    });
  }

   /* Description: get tax rate
  author : vipin reddy */

  getTaxRate() {
    this.packageAndAddOnService.getTaxRate()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.taxrate = res.data
        },
        (err) => {
          console.log(err);
        })
  }


}
