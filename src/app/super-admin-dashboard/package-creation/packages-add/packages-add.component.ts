import { Component, OnInit } from '@angular/core';
import { PackageAndAddOnService } from '../../../services/packageAndAddon.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { LoaderService } from '../../../services/loader.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-packages-add',
  templateUrl: './packages-add.component.html',
  styleUrls: ['./packages-add.component.css']
})
export class PackagesAddComponent implements OnInit {
  isValid: boolean = false;
  public package = {
    name: '',
    price: 0,
    subscriptionType: '',
    startDate: '',
    endDate: '',
    costPerEmployee: 0,
    status: '',
    modules: {
      companySettings: false,
      employeeManagement: false,
      requirements: false,
      benifitsManagement: false,
      performanceManagement: false,
      leaveManagement: false,
      reports: false
    }
  }
  public selectedPackageId = '';

  constructor(
    private packageAndAddOnService: PackageAndAddOnService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      this.selectedPackageId = params['id'];
    })

    if (this.selectedPackageId) {
      this.getSpecifiedPackageDetails();
    }
  }

   /* Description: package creation
  author : vipin reddy */


  addPackage() {
    this.isValid = true;
    // this.loaderService.loader(true);
    if (this.package.name && this.package.status && this.package.startDate &&
      this.package.endDate && this.package.price && this.package.subscriptionType && this.package.costPerEmployee)
      if (this.selectedPackageId) {
        this.editPackageDetails();
      } else {
        this.packageAndAddOnService.createPackage(this.package)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              this.loaderService.loader(false);
              if (res.status) {
                this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
                this.swalAlertService.SweetAlertWithoutConfirmation("Package", res.message, 'success')
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err);
              this.loaderService.loader(false);
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          )
      }

  }

  cancel() {
    this.package = {
      name: '',
      price: 0,
      subscriptionType: '',
      startDate: '',
      endDate: '',
      costPerEmployee: 0,
      status: "active",
      modules: {
        companySettings: false,
        employeeManagement: false,
        requirements: false,
        benifitsManagement: false,
        performanceManagement: false,
        leaveManagement: false,
        reports: false
      }
    }
  }


   /* Description: get single package detail
  author : vipin reddy */


  getSpecifiedPackageDetails() {
    this.packageAndAddOnService.getSpecificPackageDetails({ _id: this.selectedPackageId })
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.package = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

   /* Description: edit single package details
  author : vipin reddy */


  editPackageDetails() {
    this.packageAndAddOnService.editPackage(this.package)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
            this.swalAlertService.SweetAlertWithoutConfirmation("Package", res.message, 'success')
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

   /* Description: accept only numbers
  author : vipin reddy */

  keyPress(event: any) {
    const pattern = /[0-9 ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
