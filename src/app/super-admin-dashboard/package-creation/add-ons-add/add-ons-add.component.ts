import { Component, OnInit } from '@angular/core';
import { PackageAndAddOnService } from '../../../services/packageAndAddon.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { LoaderService } from '../../../services/loader.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-ons-add',
  templateUrl: './add-ons-add.component.html',
  styleUrls: ['./add-ons-add.component.css']
})
export class AddOnsAddComponent implements OnInit {

  public addOn = {
    name: '',
    price: 0,
    status: '',
    subscriptionType: '',
    startDate: '',
    endDate: '',
  }
  isValid: boolean = false;
  public selectedaddOnId = '';
 
  constructor(
    private packageAndAddOnService: PackageAndAddOnService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
   

    this.activatedRoute.params.subscribe((params) => {
      this.selectedaddOnId = params['id'];
    })

    if (this.selectedaddOnId) {
      this.getSpecifiedAddOnDetails();
    }

  }

       /* Description: create adon submit
  author : vipin reddy */
  createAddOn() {
    // this.loaderService.loader(true);
    this.isValid = true;
    if (this.addOn.name && this.addOn.status && this.addOn.startDate &&
      this.addOn.endDate && this.addOn.price && this.addOn.subscriptionType) {
      if (this.selectedaddOnId) {
        this.editAddOnDetails();
      } else {
        this.packageAndAddOnService.createAddOn(this.addOn)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              this.loaderService.loader(false);
              if (res.status) {
                this.router.navigate(['/super-admin/super-admin-dashboard/settings/add-ons']);
                this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'success')
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err);
              this.loaderService.loader(false);
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          )
      }
    }
  }

  cancel() {
    this.addOn = {
      name: '',
      price: 0,
      status: '',
      subscriptionType: '',
      startDate: '',
      endDate: '',

    }
  }
       /* Description: get single addon detail
  author : vipin reddy */

  getSpecifiedAddOnDetails() {
    this.packageAndAddOnService.getSpecificAddOnDetails({ _id: this.selectedaddOnId })
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.addOn = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
     /* Description: edit single addon detail
  author : vipin reddy */

  editAddOnDetails() {
    this.packageAndAddOnService.editAddOn(this.addOn)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.router.navigate(['/super-admin/super-admin-dashboard/settings/add-ons']);
            this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'success')
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
       /* Description: accept only numbers
  author : vipin reddy */
  keyPress(event: any) {
    const pattern = /[0-9]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
