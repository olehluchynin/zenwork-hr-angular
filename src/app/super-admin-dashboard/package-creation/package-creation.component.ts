import { Component, OnInit } from '@angular/core';
import { PackageAndAddOnService } from '../../services/packageAndAddon.service';
import { LoaderService } from '../../services/loader.service';
import { SwalAlertService } from '../../services/swalAlert.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-package-creation',
  templateUrl: './package-creation.component.html',
  styleUrls: ['./package-creation.component.css']
})
export class PackageCreationComponent implements OnInit {

  public package = {
    name : '',
    price : 0,
    status:'active'
  };

  public addOn = {
    name : '',
    price : 0,
    status:'active'
  };

  constructor(
    private packageAndAddOnService : PackageAndAddOnService,
    private loaderService : LoaderService,
    private swalAlertService : SwalAlertService
  ) { }

  ngOnInit() {
    // this.loaderService.loader(true);
  }

  createPakage(){
    this.packageAndAddOnService.createPackage(this.package)
      .subscribe(
        (res:any)=>{
          if(res.status){
            this.loaderService.loader(false);
            this.swalAlertService.SweetAlertWithoutConfirmation("Package",res.message,'success');
          }else{
            this.loaderService.loader(false);
            this.swalAlertService.SweetAlertWithoutConfirmation("Package",res.message,'info');
          }
        },
        (err:HttpErrorResponse)=>{
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Package",err.message,'error');
        }
      )
  }

  createAddOn(){
    this.packageAndAddOnService.createAddOn(this.addOn)
      .subscribe(
        (res:any)=>{
          if(res.status){
            this.loaderService.loader(false);
            this.swalAlertService.SweetAlertWithoutConfirmation("Add On",res.message,'success');
          }else{
            this.loaderService.loader(false);
            this.swalAlertService.SweetAlertWithoutConfirmation("Add On",res.message,'info');
          }
        },
        (err:HttpErrorResponse)=>{
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Add On",err.message,'error');
        }
      )
  }

}
