import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageCreationComponent } from '../package-creation/package-creation.component';
import { RouterModule, Routes } from '@angular/router';
import { PackagesComponent } from './packages/packages.component';
import { PackagesAddComponent } from './packages-add/packages-add.component';
import { MatSelectModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { ModulesComponent } from './modules/modules.component';
import { ModulesAddComponent } from './modules-add/modules-add.component';
import { AddOnsComponent } from './add-ons/add-ons.component';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AddOnsAddComponent } from './add-ons-add/add-ons-add.component';
import { CuponsAddComponent } from './cupons-add/cupons-add.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CreateUserProfileTemplateComponent } from './create-user-profile-template/create-user-profile-template.component';
import { MaterialModuleModule } from '../../material-module/material-module.module';
const router: Routes = [
  // { path: '', redirectTo: 'setting', pathMatch: 'full' },
  {
    path: '', component: PackageCreationComponent, children: [
      { path: '', redirectTo: 'packages', pathMatch: 'full' },
      { path: 'packages', component: PackagesComponent },
      { path: 'packages-add', component: PackagesAddComponent },
      // { path: '', redirectTo: 'modules', pathMatch: 'full' },
      { path: 'modules', component: ModulesComponent },
      { path: 'modules-add', component: ModulesAddComponent },
      // { path: '', redirectTo: 'add-ons', pathMatch: 'full' },
      { path: 'add-ons', component: AddOnsComponent },
      { path: 'add-ons-add', component: AddOnsAddComponent },
      { path: 'add-ons-add/:id', component: AddOnsAddComponent },

      { path: 'packages-add', component: PackagesAddComponent },
      { path: 'packages-add/:id', component: PackagesAddComponent },

      { path: 'coupons-add', component: CuponsAddComponent },
      { path: 'coupons-add/:id', component: CuponsAddComponent },
      { path: "user-profile", component:UserProfileComponent}
      
    ]
  },

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(router),
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MatCheckboxModule,
    MaterialModuleModule,
  ],
  entryComponents: [
    CreateUserProfileTemplateComponent
],
exports: [
    RouterModule
],

  declarations: [UserProfileComponent,
    PackageCreationComponent, 
    PackagesComponent, 
    PackagesAddComponent,
     ModulesComponent, 
     ModulesAddComponent,
      AddOnsComponent,
     AddOnsAddComponent,
     CuponsAddComponent,
     CreateUserProfileTemplateComponent],
   

})
export class PackageCreationModule { }
