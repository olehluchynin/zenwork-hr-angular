import { Component, OnInit } from '@angular/core';
import { PackageAndAddOnService } from '../../../services/packageAndAddon.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../../services/loader.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-cupons-add',
  templateUrl: './cupons-add.component.html',
  styleUrls: ['./cupons-add.component.css']
})
export class CuponsAddComponent implements OnInit {

  constructor(
    private couponAddService: PackageAndAddOnService,
    private router: Router,
    private activated: ActivatedRoute,
    private loaderService: LoaderService,
    private swalAlertService: SwalAlertService

  ) { }
  isValid: boolean = false;
  public coupon = {
    name: '',
    startDate: '',
    endDate: '',
    percentage: '',
    flatDiscountAmount: '',
    couponType: ''

  }

  public selectCouponId = '';

  ngOnInit() {
    this.activated.params.subscribe((params) => {
      this.selectCouponId = params['id']
    })

    if (this.selectCouponId) {
      this.getCouponDetails();
    }


  }

   /* Description: coupons create 
  author : vipin reddy */
  createCoupon() {
    this.isValid = true;
    if (this.coupon.name && this.coupon.startDate && this.coupon.endDate) {
      this.loaderService.loader(true);
      console.log(this.coupon);

      if (this.selectCouponId) {
        this.editCouponDetails();
      } else {
        this.couponAddService.createCoupon(this.coupon)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              this.loaderService.loader(false);
              if (res.status) {
                this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
                this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'success')
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err);
              this.loaderService.loader(false);
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          )
      }
    }

  }



  cancel() {
    this.coupon = {
      name: '',
      startDate: '',
      endDate: '',
      percentage: '',
      flatDiscountAmount: '',
      couponType: ''

    }
  }
     /* Description: get single coupon
  author : vipin reddy */

  getCouponDetails() {
    this.couponAddService.getSepcificCouponDetails({ _id: this.selectCouponId })
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.coupon = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

   /* Description: update coupon detail
  author : vipin reddy */
  editCouponDetails() {
    console.log(this.coupon);

    this.couponAddService.editCouponsData(this.coupon)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          this.loaderService.loader(false);
          if (res.status) {
            this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
            this.swalAlertService.SweetAlertWithoutConfirmation("coupons-add", res.message, 'success')
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("coupons-add", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
   /* Description: coupon type percentage to value change 
  author : vipin reddy */
  couponTypeChange() {
    console.log(this.coupon.couponType);
    if (this.coupon.couponType == 'Value') {
      delete this.coupon.percentage;
    }
    if (this.coupon.couponType == 'Percentage') {
      delete this.coupon.flatDiscountAmount;
    }

  }

     /* Description: accept only numbers 
  author : vipin reddy */

  keyPress(event: any) {
    const pattern = /[0-9]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


}
