import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuponsAddComponent } from './cupons-add.component';

describe('CuponsAddComponent', () => {
  let component: CuponsAddComponent;
  let fixture: ComponentFixture<CuponsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuponsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuponsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
