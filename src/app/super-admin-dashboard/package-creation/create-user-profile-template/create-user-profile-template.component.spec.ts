import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUserProfileTemplateComponent } from './create-user-profile-template.component';

describe('CreateUserProfileTemplateComponent', () => {
  let component: CreateUserProfileTemplateComponent;
  let fixture: ComponentFixture<CreateUserProfileTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUserProfileTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUserProfileTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
