import { Component, OnInit ,Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ZenworkersService } from '../../../services/zenworkers.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { LoaderService } from '../../../services/loader.service';
@Component({
  selector: 'app-create-user-profile-template',
  templateUrl: './create-user-profile-template.component.html',
  styleUrls: ['./create-user-profile-template.component.css']
})
export class CreateUserProfileTemplateComponent implements OnInit {
  public createZenworker = {
    name: '',
    description: '',
    access: '',

    // "_id":"5c9a2aa8c8dcc125af1816ce",
  }
  editId:any;
  constructor(
    public dialogRef: MatDialogRef<CreateUserProfileTemplateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private zenworkersService: ZenworkersService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService
  ) { }
  

  ngOnInit() {
    if (this.data) {
      this.editId = this.data;
      this.getSingleRole(this.editId)
    }

  }
       /* Description: get single level
  author : vipin reddy */
  getSingleRole(id){
    this.zenworkersService.editProfile(id)
    .subscribe(
      (res: any) => {
        if(res.status == true){
          console.log(res);
          
          this.createZenworker = res.data;
        // this.dialogRef.close();
        this.loaderService.loader(false);
        // this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')
        // this.getAllRoles()
      }
    },
      (err:any) =>{
        console.log(err);
        
      })
  }
     /* Description: add single level
  author : vipin reddy */
  addProfile(){
    // this.loaderService.loader(true);
    this.zenworkersService.addProfile(this.createZenworker)
    .subscribe(
      (res: any) => {
        if(res.status == true){
        this.dialogRef.close();
        this.loaderService.loader(false);
        this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')
        // this.getAllRoles()
      }
    },
      (err:any) =>{
        console.log(err);
        
      })
  }

}
