import { PackageCreationModule } from './package-creation.module';

describe('PackageCreationModule', () => {
  let packageCreationModule: PackageCreationModule;

  beforeEach(() => {
    packageCreationModule = new PackageCreationModule();
  });

  it('should create an instance', () => {
    expect(packageCreationModule).toBeTruthy();
  });
});
