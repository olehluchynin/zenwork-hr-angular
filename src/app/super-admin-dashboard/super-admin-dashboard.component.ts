import { Component,OnDestroy } from '@angular/core';
import { NavbarComponent } from '../shared-module/navbar/navbar.component';
import { SideNavComponent } from '../shared-module/side-nav-component/side-nav.component';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { MessageService } from '../services/message-service.service';

@Component({
  selector: 'app-super-admin-dashboard',
  templateUrl: './super-admin-dashboard.component.html',
  styleUrls: ['./super-admin-dashboard.component.css']
})
export class SuperAdminDashboardComponent implements OnDestroy {
  url: any;

  clientsSpecification : boolean = false;
  message: any;
  subscription: Subscription;
  public userData;
  constructor(private messageService:MessageService) {
    this.userData = {
      role: "super-admin"
    }
    
  }
  

 
  ngOnDestroy() {
    // this.subscription.unsubscribe();
    
  }



}
