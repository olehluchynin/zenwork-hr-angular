import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CompanySettingsService } from '../services/companySettings.service';

@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.css']
})
export class EmailVerifyComponent implements OnInit {


  public token:any;
  public id:any;
  public email:any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private companySettingsService:CompanySettingsService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['id'];
      console.log(this.token);
      
       var tokenDetails = {
        token: this.token
       }
       this.companySettingsService.updateEmailUser(tokenDetails)
       .subscribe(
         (response)=>{
           console.log(response)
         }
       )
    })
  }

}
