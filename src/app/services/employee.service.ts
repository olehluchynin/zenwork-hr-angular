import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable()

export class EmployeeService {
    constructor(
        private http: HttpClient
    ) { }

    addNewEmployee(employeeDetails) {
        return this.http.post(environment.url + "/api" + environment.version + "/employees/add", employeeDetails)
            .pipe(map(res => res));
    }

    getOnboardedEmployeeDetails(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/listEmployees", data)
            .pipe(map(res => res));
    }

    addNotes(notes) {
        return this.http.post(environment.url + "/api" + environment.version + "/employees/addNote", notes)
            .pipe(map(res => res));
    }

    getAllNotes(_id) {
        return this.http.get(environment.url + "/api" + environment.version + "/employees/allNotes/" + _id)
            .pipe(map(res => res));
    }


    updateEmployeePersonalDetails(id, data, section) {
        return this.http.put(environment.url + "/api" + environment.version + "/employees/" + id + '/' + section, data)
            .pipe(map(res => res));
    }
 
    updateEmergencyContact(id, data, section) {
        return this.http.put(environment.url + "/api" + environment.version + "/employees/" + id + '/' + section, data)
            .pipe(map(res => res));
    }

    copyService(data){
        return this.http.post (environment.url + "/api" + environment.version + "/roles/create-copy", data)
    }


}