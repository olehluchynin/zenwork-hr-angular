import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()


export class AuthenticationService {

constructor( private http:HttpClient ){
}

loginDetails(details) {
    return this.http.post(environment.url +"/api" + environment.version + "/users/login",details)
        .pipe(map(res => res));
}


register(details,data) {
    return this.http.post(environment.url +"",details,data)
        .pipe(map(res => res));
}

signup(details) {
    return this.http.post(environment.url +"/api" + environment.version + "/users/signup",details)
        .pipe(map(res => res));
}
updateCompanyDetail(data){
    return this.http.put(environment.url +"/api" + environment.version + "/companies/updateCompanyDetails",data)
    .pipe(map(res => res));
}
gooleSignIn(){
    return this.http.get(environment.url +"/api" + environment.version + "/users/login/google")
    .pipe(map(res => res));
}
contactUsformSend(data){
    return this.http.post(environment.url +"/api" + environment.version + "/contact-us/contactus",data)
    .pipe(map(res => res));
}



resetPassword(data){
    return this.http.post (environment.url + "/api" + environment.version + "/users/passwordresetlink", data)
}


forgotPassword(data){
    return this.http.post(environment.url +"/api" + environment.version + "/users/resetpasswordwithtoken", data)
}



}