import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable()

export class CompanyService {
    constructor(
        private http: HttpClient
    ) { }

    createCompany(companyDetails) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/addClientDetails", companyDetails)
            .pipe(map(res => res));
    }
    editClient(id){
        return this.http.get(environment.url + "/api" + environment.version + "/companies/getCompanyDetails/" + id)
        .pipe(map(res => res));
    }
    getBillingHistory(id){
        return this.http.get(environment.url + "/api" + environment.version + "/orders/BillingOfCompany/" + id)
        .pipe(map(res => res));
    }
    editDataSave(data,id){
        return this.http.put(environment.url + "/api" + environment.version + "/companies/" + id, data)
        .pipe(map(res => res));
    }
    currentPlan(id){
        return this.http.get(environment.url + "/api" + environment.version + "/orders/CurrentPlan/" + id)
        .pipe(map(res => res));
    }
    getAllCompanies(data) {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/all", { params: data })
            .pipe(map(res => res));
    }
    getresellers(){
        return this.http.get(environment.url + "/api" + environment.version + "/companies/resellers" )
        .pipe(map(res => res));
    }

    editCompany(companyDetails) {
        let id = companyDetails._id;
        let company = {
            name: companyDetails.name,
            tradeName: companyDetails.tradeName,
            employeCount: companyDetails.employeCount,
            email: companyDetails.email,
            type: companyDetails.type,
            belongsToReseller: companyDetails.belongsToReseller,
            reseller: companyDetails.reseller,
            primaryContact: { name: companyDetails.primaryContact.name, phone: companyDetails.primaryContact.phone, extention: companyDetails.primaryContact.extention },
            billingContact: { name: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.name, email: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.email, phone: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.phone, extention: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.extention },
            address: {
              address1: companyDetails.address.address1,
              address2: companyDetails.address.address2,
              city: companyDetails.address.city,
              state: companyDetails.address.state,
              zipcode: companyDetails.address.zipcode,
              country: companyDetails.address.country
            },
            billingAddress: {
              address1: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.address1,
              address2: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.address2,
              city: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.city,
              state: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.state,
              zipcode: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.zipcode,
              country: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.country
            },
            isPrimaryContactIsBillingContact: companyDetails.isPrimaryContactIsBillingContact,
            isPrimaryAddressIsBillingAddress: companyDetails.isPrimaryAddressIsBillingAddress
          };
        return this.http.put(environment.url + "/api" + environment.version + "/companies/" + id, company)
            .pipe(map(res => res));
    }
    addContactDetails(companyAddress){
        return this.http.post(environment.url + "/api" + environment.version + "/companies/addContactDetails", companyAddress)
        .pipe(map(res => res));
    }



}