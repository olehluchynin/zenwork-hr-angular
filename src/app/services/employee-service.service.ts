import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  constructor( private http:HttpClient) { }


getCompanyList(id){
  return this.http.get(environment.url + "/api" + environment.version + "/employee-front-page/announcement/list/" + id )
}

AddCompanyData(data){
  return this.http.post(environment.url + "/api" + environment.version + "/employee-front-page/announcement/add", data)
}

deleteCompanayData(data){
  return this.http.post(environment.url + "/api"+ environment.version + "/employee-front-page/announcement/delete",data)
}

companyEditData(id){
  return this.http.put(environment.url + "/api" + environment.version + "/employee-front-page/announcement/edit", id)
}

getManagerSettings(comp){
  return this.http.get(environment.url + "/api" + environment.version + "/employee-front-page/settings/get/" + comp )
}

managerSettingsAdd(data){
  return this.http.post(environment.url + "/api" + environment.version + "/employee-front-page/settings/addOrUpdate", data)
}

companyLinkAdding(data){
  return this.http.post(environment.url + "/api" + environment.version + "/employee-front-page/links/add", data)
}

companyLinksDeleteData(data){
  return this.http.post(environment.url + "/api" + environment.version + "/employee-front-page/links/delete", data)
}

companyLinkEditData(id){
  return this.http.put(environment.url + "/api" + environment.version + "/employee-front-page/links/edit", id)
}


}
