import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TimeScheduleService {

  constructor(private http: HttpClient) { }

getAllWorkScheduleData(id){
  return this.http.get(environment.url +"/api" + environment.version + "/scheduler/getposition/"+ id)
}

workScheduleTypeData(data){
  return this.http.post(environment.url + "/api" + environment.version + "/scheduler/getempschedule", data)
}

updateWorkScheduler(data){
  return this.http.post(environment.url + "/api" + environment.version + "/scheduler/updateposition", data)
}

getLeaveEligGroupHistory(id){
  return this.http.get(environment.url +"/api" + environment.version + "/myinfo/myinfotimeschedule/getleaveeligrouphistory/"+ id)

}

changeLeaveEligibilityGroup(data){
  return this.http.post(environment.url + "/api" + environment.version + "/myinfo/myinfotimeschedule/changeleaveeligibgroup", data)

}

getCurrentLeaveEligGroup(id){
  return this.http.get(environment.url +"/api" + environment.version + "/myinfo/myinfotimeschedule/getcurrentleaveeligroup/"+ id)

}

getTimeoffRequests(data){
  return this.http.post(environment.url + "/api" + environment.version + "/myinfo/myinfotimeschedule/gettimeoffrequests", data)

}

getTimeOffPolicies(id){
  return this.http.get(environment.url +"/api" + environment.version + "/myinfo/myinfotimeschedule/gettimeoffpolicies/"+ id)

}

applyForScheduledTimeOff(data){
  return this.http.post(environment.url + "/api" + environment.version + "/myinfo/myinfotimeschedule/applyfortimeoff", data)

}

updateScheduledTimeOff(data){
  return this.http.post(environment.url + "/api" + environment.version + "/myinfo/myinfotimeschedule/updatedscheduledtimeoff", data)

}



getHistoryData(compid,id){
  return this.http.get(environment.url + "/api" + environment.version + "/scheduler/postionhistory/" + compid + '/' + id)

}


getWorkScheduleTimeSheet(data){
  return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetmaintenance/gethistroricaltimesheetdetails", data)
}

}
