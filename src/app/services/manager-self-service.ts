import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ManagerSelfService {

  constructor(private http: HttpClient) { }

  
  jobEmployeeList(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/searchEmployee", data)
      .pipe(map(res => res));
  }

  selectUserId(cid,id){
    return this.http.get(environment.url + "/api" + environment.version + "/users/getOtherUser/" + cid + '/' + id)
  }

  getNewStatusFields(id){
    return this.http.get(environment.url + "/api" + environment.version + "/structure/getStandardAndCustomFields/" + id)
  }

  jobChangeRequest(data){
    return this.http.post(environment.url + "/api" + environment.version + "/manager-requests/submitRequest",data)
  }

  getJobChangeReason(id){
    return this.http.get(environment.url + "/api" + environment.version + "/structure/getStandardAndCustomFields/" + id)
  }

  salaryChangeRequestData(data){
    return this.http.post(environment.url + "/api" + environment.version + "/manager-requests/submitRequest",data)
  }


}