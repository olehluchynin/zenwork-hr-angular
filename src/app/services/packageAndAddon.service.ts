import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable()

export class PackageAndAddOnService {
    constructor(
        private http: HttpClient
    ) { }

    createPackage(packageDetails) {
        return this.http.post(environment.url + "/api" + environment.version + "/packages/add", packageDetails)
            .pipe(map(res => res));
    }

    editPackage(packageDetails) {
        return this.http.put(environment.url + "/api" + environment.version + "/packages/"+packageDetails._id, packageDetails)
            .pipe(map(res => res));
    }

    createAddOn(addOnDetails) {
        return this.http.post(environment.url + "/api" + environment.version + "/ad-ons/add", addOnDetails)
            .pipe(map(res => res));
    }
    getTaxRate(){
        return this.http.get(environment.url + "/api" + environment.version + "/zenwork/getTaxRate")
        .pipe(map(res => res));
    }

    editAddOn(addOnDetails) {
        return this.http.put(environment.url + "/api" + environment.version + "/ad-ons/"+addOnDetails._id, addOnDetails)
            .pipe(map(res => res));
    }

    getAllPackagesforSettings(data){
        return this.http.post(environment.url + "/api" + environment.version + "/packages/list",data)
            .pipe(map(res => res));
    }
    getAllPackages(){
        return this.http.get(environment.url + "/api" + environment.version + "/packages/all")
            .pipe(map(res => res));
    }

    getSpecificPackageDetails(packageData){
        return this.http.get(environment.url + "/api" + environment.version + "/packages/"+packageData._id)
            .pipe(map(res => res));
    }

    getAllAddOns(){
        return this.http.get(environment.url + "/api" + environment.version + "/ad-ons/all")
            .pipe(map(res => res));
    }

    getAllAddOnslist(data){
        return this.http.post(environment.url + "/api" + environment.version + "/ad-ons/all",data)
            .pipe(map(res => res));
    }

    getSpecificAddOnDetails(AddOnData){
        return this.http.get(environment.url + "/api" + environment.version + "/ad-ons/"+AddOnData._id)
            .pipe(map(res => res));
    }
    
    

    createCoupon(couponDetail){
        return this.http.post(environment.url + '/api' + environment.version + "/coupons/add",couponDetail)
        .pipe(map(res=>res));
    }

    getAllCoupons(){
        return this.http.get(environment.url + "/api" + environment.version + "/coupons/all")
            .pipe(map(res => res));
    }

    getAllCouponsforAll(data){
        return this.http.post(environment.url + "/api" + environment.version + "/coupons/all",data)
            .pipe(map(res => res));
    }


    getSepcificCouponDetails(couponDetail){
        return this.http.get(environment.url + '/api' + environment.version + "/coupons/"+couponDetail._id)
        .pipe(map(res=>res));
    }

    editCouponsData(couponDetail){
        return this.http.put (environment.url + "/api" + environment.version + "/coupons/" +couponDetail._id, couponDetail)
    }
    updateTaxRate(data){
        return this.http.put (environment.url + "/api" + environment.version + "/zenwork/updateTaxRate" , data)
        .pipe(map(res=>res));
    }

    validateCoupon(data){
        return this.http.post(environment.url + "/api" + environment.version + "/coupons/validate-coupon",data)
        .pipe(map(res => res));
    }
    deleteAddons(data){
        return this.http.post(environment.url + "/api" + environment.version + "/ad-ons/delete",data)
        .pipe(map(res => res));
    }

    deleteCoupons(data){
        return this.http.post(environment.url + "/api" + environment.version + "/coupons/delete",data)
        .pipe(map(res => res));
    }
    deletePackages(data){
        return this.http.post(environment.url + "/api" + environment.version + "/packages/delete",data)
        .pipe(map(res => res));
    }



}