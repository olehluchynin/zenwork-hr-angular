import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "../../environments/environment";

@Injectable()
export class CreditCardChargingService {

    constructor(private http: HttpClient) { }

    createCharge(token,id,amount,orderId) {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'token': token,
            'amount': amount
        });
        return this.http.post(environment.url + "/api" + environment.version + "/orders/credit-charge",{id,amount,token,orderId});
    }



}
