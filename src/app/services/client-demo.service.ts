import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable()

export class ClientDemoRegistrationService {
    constructor(
        private http: HttpClient
    ) { }

    registerClient(clientDetails) {
        return this.http.post(environment.url + environment.version + "/client/registerForDemo", clientDetails)
            .pipe(map(res => res));
    }
}