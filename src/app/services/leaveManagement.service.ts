import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()

export class LeaveManagementService {
    constructor(
        private http: HttpClient
    ) { }

   

    createLeaveEligibilityGroup(data){
        return this.http.post(environment.url + "/api" + environment.version + "/leaveEligibilityGroup/create", data)
        .pipe(map(res => res));
    }
    getAllLeaveEligibilityGroup(){
        return this.http.get(environment.url + "/api" + environment.version + "/leaveEligibilityGroup/list")
        .pipe(map(res => res));
    }
    updateLeaveEligibilityGroup(data){
        return this.http.put(environment.url + "/api" + environment.version + "/leaveEligibilityGroup/update", data)
        .pipe(map(res => res));
    }
    deleteLeaveEligibilityGroup(data){
        return this.http.post(environment.url + "/api" + environment.version + "/leaveEligibilityGroup/delete", data)
        .pipe(map(res => res));
    }
    getLeaveEligibilityGroup(id){
        return this.http.get(environment.url + "/api" + environment.version + "/leaveEligibilityGroup/get/" + id)
        .pipe(map(res => res));
    }

    createTimeOffPolicies(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/createpolicy ", data)
        .pipe(map(res => res));
    }
    createTierLevels(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/createtier ", data)
        .pipe(map(res => res));
    }
    getEmployeeList(data){
        return this.http.post(environment.url + "/api" + environment.version + "/leave-eligibility-group/paginate", data)
        .pipe(map(res => res));
    }
    getAllPolicies(id){
        return this.http.get(environment.url + "/api" + environment.version + "/time-off-policies/getpolicydetails/" + id)
        .pipe(map(res => res));
    }
    getTierLevel(company_id,tier_id){
        return this.http.get(environment.url + "/api" + environment.version + "/time-off-policies/gettierlevel/" + company_id + "/" + tier_id)
        .pipe(map(res => res));
    }
    updateTierLevel(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/updatetierlevel", data)
        .pipe(map(res => res));
    }

    getTimeOffPolicy(id){
        return this.http.get(environment.url + "/api" + environment.version + "/time-off-policies/get/" + id)
        .pipe(map(res => res));
    }

    deleteTierLevel(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/deletetierlevel", data)
        .pipe(map(res => res));
    }

    updateTimePolicy(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/updatetimepolicy", data)
        .pipe(map(res => res));
    }

    deleteTimeOffPolicies(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/delete", data)
        .pipe(map(res => res));
    }
 
    uploadCustomFile(data){
        return this.http.post(environment.url + "/api" + environment.version + "/leave-eligibility-group/uploadxlsx", data)
        .pipe(map(res => res));
    }

    downloadCustomFile(data){
        return this.http.post(environment.url+ "/api" + environment.version + "/leave-eligibility-group/exportexcel", data, { responseType:'blob' }).pipe(map(res => res));
    }

    uploadImportBalance(data){
        return this.http.post(environment.url + "/api" + environment.version + "/time-off-policies/upload", data)
        .pipe(map(res => res));
    }
    
    createHoliday(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/addHolidays", data)
        .pipe(map(res => res));
    }
    loadHolidays(year){
        return this.http.get(environment.url + "/api" + environment.version + "/scheduler/loadholidays/" + year)
        .pipe(map(res => res));
    }
    getHolidayList(year){
        return this.http.get(environment.url + "/api" + environment.version + "/scheduler/getHolidayList/" + year)
        .pipe(map(res => res));
    }
  
    getHoliday(id){
        return this.http.get(environment.url + "/api" + environment.version + "/scheduler/getHoliday/" + id)
        .pipe(map(res => res)); 
    }
    updateHoliday(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/updateHoliday", data)
        .pipe(map(res => res));
    }
    deleteHoliday(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/deleteHolidays", data)
        .pipe(map(res => res));
    }

    createEmployeeGroup(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/createBlackoutEmployeeGroup", data)
        .pipe(map(res => res));
    }

    getEmployeeGroupList(company_id){
        return this.http.get(environment.url + "/api" + environment.version + "/scheduler/getempgrp/" + company_id)
        .pipe(map(res => res));
    }

    createBlackout(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/addBlackoutDates", data)
        .pipe(map(res => res));
    }
    getBlackoutList(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/getblackout", data)
        .pipe(map(res => res));
    }
    updateBlackout(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/updateblackout", data)
        .pipe(map(res => res));
    }
    deleteBlackout(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/deleteblackout", data)
        .pipe(map(res => res));
    }

    createPosition(data){
        return this.http.post(environment.url + "/api" + environment.version + "/scheduler/createPositionSchedule", data)
        .pipe(map(res => res));
    }
    getAllPosition(){
        return this.http.get(environment.url + "/api" + environment.version + "/scheduler/listPositionSchedules")
        .pipe(map(res => res));
    }

   getPosition(id){
    return this.http.get(environment.url + "/api" + environment.version + "/scheduler/getPositionSchedule/" + id)
    .pipe(map(res => res));
   }

   updatePosition(data){
    return this.http.put(environment.url + "/api" + environment.version + "/scheduler/updatePositionSchedule", data)
    .pipe(map(res => res));
   }

   deletePosition(data){
    return this.http.post(environment.url + "/api" + environment.version + "/scheduler/deleteposition", data)
    .pipe(map(res => res));
   }

   createDepartment(data){
    return this.http.post(environment.url + "/api" + environment.version + "/scheduler/createDepartmentSchedule", data)
    .pipe(map(res => res));
   }

   getEmployeeWorkTime(data){
    return this.http.post(environment.url + "/api" + environment.version + "/scheduler/getempworktime", data)
    .pipe(map(res => res));
   }

   getAllDepartment(){
    return this.http.get(environment.url + "/api" + environment.version + "/scheduler/listDepartmentSchedules")
    .pipe(map(res => res));
   }
   getDepartmnet(id){
    return this.http.get(environment.url + "/api" + environment.version + "/scheduler/getDepartmentSchedule/" + id)
    .pipe(map(res => res));
   }

   updateDepartment(data){
    return this.http.post(environment.url + "/api" + environment.version + "/scheduler/editDepartmentSchedule", data)
    .pipe(map(res => res));
   }
   
   deleteDepartment(data){
    return this.http.post(environment.url + "/api" + environment.version + "/scheduler/deletedepartment", data)
    .pipe(map(res => res));
   }
   


    punchIn(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/punchIn", data)
            .pipe(map(res => res));
    }

    punchOut(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/punchOut", data)
            .pipe(map(res => res));
    }

    timeSheetApproval(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetapproval/requesttimesheetapproval", data)
        .pipe(map(res => res));
    }
    getTimeSheetApprovals(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetapproval/gettimesheetapprovalrequests",data)
        .pipe(map(res => res));
    }
   
    getMissingPunches(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetapproval/getmissingpunches", data)
        .pipe(map(res => res));
    }
    getTimeSheetSummary(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetapproval/gettimesheetsummary", data)
        .pipe(map(res => res));
    }

    getTimeSheetDetails(payCycle,id){
        return this.http.get(environment.url + "/api" + environment.version + "/timeSchedule/timesheetapproval/gettimesheetdetails/"+ payCycle +"/" + id)
        .pipe(map(res => res));
    }

    getHistoricalTimeshhet(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetmaintenance/gethistroricaltimesheetdetails", data)
        .pipe(map(res => res));
    }
    addMissingPunches(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/addmissingpunches", data)
        .pipe(map(res => res));
    }

    deletePunch(userId,timesheetEmployee,punchDate,payCycle){
        return this.http.delete(environment.url + "/api" + environment.version + "/timeSchedule/deleteattendancefordate/"+ userId +"/" + timesheetEmployee + "/" + punchDate +"/"+ payCycle)
        .pipe(map(res => res));
    }
    approveOrRejectTimeSheet(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetapproval/updatetimesheet", data)
        .pipe(map(res => res));
    }
    searchEmployeeInTimeSheetMaintenance(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetmaintenance/searchemployee", data)
        .pipe(map(res => res));
    }
    searchEmployeeForApproval(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetmaintenance/searchapprovalemployee", data)
        .pipe(map(res => res));
    }

    sendMissingApproval(data){
        return this.http.post(environment.url + "/api" + environment.version + "/timeSchedule/timesheetmaintenance/sendmissingapproval", data)
        .pipe(map(res => res));
    }
  
}