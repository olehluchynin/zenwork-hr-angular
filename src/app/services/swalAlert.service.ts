import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';

@Injectable()

export class SwalAlertService {
    constructor(
        private http: HttpClient
    ) { }

    SweetAlertWithoutConfirmation(title,text,type) {
        Swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: false,
            showConfirmButton :false,
            timer : 2500
        })
    }
}