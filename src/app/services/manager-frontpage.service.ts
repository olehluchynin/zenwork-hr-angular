import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ManagerFrontpageService {

  constructor( private http:HttpClient) { }

getAllData(cId,epmId){
  console.log("iddddd",epmId)
  return this.http.get(environment.url + "/api" + environment.version + "/manager-front-page/announcement/list/"+cId)
}

submitData(data){
  return this.http.post(environment.url +"/api" + environment.version + "/manager-front-page/announcement/add", data)
}

editAnnounceData(id){
  return this.http.put(environment.url + "/api" + environment.version + "/manager-front-page/announcement/edit", id)
}

deleteData(id){
  return this.http.post(environment.url +"/api" + environment.version + "/manager-front-page/announcement/delete",id)
}



getAllAdditionalData(cid){
  return this.http.get(environment.url +"/api"+ environment.version + "/manager-front-page/settings/get/" + cid )
}

additionalDataSave(data){
  return this.http.post(environment.url + "/api" + environment.version + "/manager-front-page/settings/addOrUpdate", data)
}


managerLinksAdd(data){
  return this.http.post(environment.url + "/api" + environment.version + "/manager-front-page/links/add",data)
}


managerLinkEdit(id){
  return this.http.put(environment.url + "/api" + environment.version + "/manager-front-page/links/edit",id)
}



managerLinksDelete(id){
  return this.http.post(environment.url + "/api" + environment.version + "/manager-front-page/links/delete", id)
}

managercontrols(cId){

}

}
