import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SiteAccessRolesService {

  constructor(private http: HttpClient) { }

  getStandardOnboardingtemplate(id) {
    // return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
    // .pipe(map(res => res));
  }
  getAllEmployees(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/searchEmployee", data)
      .pipe(map(res => res));
  }
  getuserRoleEmployees(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/users/" + id)
      .pipe(map(res => res));
  }
  chooseEmployeesData(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/chooseEmployeesForRoles", data)
      .pipe(map(res => res));
  }
  postEmpDatatoRole(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/addGroupsToUsers", data)
      .pipe(map(res => res));
  }
  getAlltabsData(cID,rID,category){
    return this.http.get(environment.url + "/api" + environment.version + "/roles/fields/get/" +cID+ "/" +rID+ "/" +category)
    .pipe(map(res => res));
  }



  UpdateRoleType(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/roles/update/" + data._id, data)
      .pipe(map(res => res));
  }
  getBaseRoles() {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/baseroles")
      .pipe(map(res => res));
  }
  createRoleType(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/roles/addUpdate", data)
      .pipe(map(res => res));
  }
  allPagesData(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/pages/all/" + id)
      .pipe(map(res => res));
  }
  getAllPagesForCompany(cid, rid) {
    return this.http.get(environment.url + "/api" + environment.version + "/pages/" + cid + "/" + rid)
      .pipe(map(res => res));
  }
  getSingleCustomRole(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/role/" + id)
      .pipe(map(res => res));
  }

  pagesCreation(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/pages/create/all", data)
      .pipe(map(res => res));
  }
  pagesUpdate(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/pages/update/all/pages", data)
      .pipe(map(res => res));
  }

  
	

  personalFieldsUpdate(data){
    return this.http.put(environment.url + "/api" + environment.version + "/roles/fields/update", data)
    .pipe(map(res => res));
  }
  jobFieldsUpdate(data){
    return this.http.put(environment.url + "/api" + environment.version + "/roles/fields/update", data)
    .pipe(map(res => res));
  }

  emergencyFieldsUpdate(data){
    return this.http.put(environment.url + "/api" + environment.version + "/roles/fields/update", data)
    .pipe(map(res => res));
  }

  compensationFieldsUpdate(data){
    return this.http.put(environment.url + "/api" + environment.version + "/roles/fields/update", data)
    .pipe(map(res => res));
  }



  postpersonalfields(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/roles/fields/create", data)
      .pipe(map(res => res));
  }
  updateRoles(data) {
    console.log("seerveice array", data);

    return this.http.put(environment.url + "/api" + environment.version + "/pages/update/" + data._id, data)
      .pipe(map(res => res));
  }
  updateMultipleRoles(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/pages/update/all/pages", data)
      .pipe(map(res => res));
  }
  companyId(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/company/" + id)
      .pipe(map(res => res));
  }
  deleteRole(id) {
    return this.http.delete(environment.url + "/api" + environment.version + "/roles/delete/" + id)
      .pipe(map(res => res));
  }
  getAllMyinfoFields(roleid, cId, category) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/fields/get/" + cId + "/" + roleid + "/" + category)
      .pipe(map(res => res));
  }
  getAllEmployeesforRoles(cId,Rid){
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/users-under-group/" + cId + "/" + Rid)
    .pipe(map(res => res));
  }
  getAllEmployeesUnderManager(cId,uId){
    return this.http.get(environment.url + "/api" + environment.version + "/offboarding/getAllDirectReports/" + cId + "/" + uId)
    .pipe(map(res => res));
  }
  getAllEmployeesforCustomRoles(rId){
    return this.http.get(environment.url + "/api" + environment.version + "/roles/userIds/"  + rId)
    .pipe(map(res => res));
  }
  changeSiteAccessRoleforEmp(data){
    return this.http.put(environment.url + "/api" + environment.version + "/roles/change-user-role" ,data)
    .pipe(map(res => res));
  }

}