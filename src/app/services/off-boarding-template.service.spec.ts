import { TestBed } from '@angular/core/testing';

import { OffBoardingTemplateService } from './off-boarding-template.service';

describe('OffBoardingTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OffBoardingTemplateService = TestBed.get(OffBoardingTemplateService);
    expect(service).toBeTruthy();
  });
});
