import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PayRollService {

  constructor(
    private http: HttpClient
  ) { }

  addPayrollGroup(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-groups/add", data)
      .pipe(map(res => res));
  }
  getPayFrequency() {
    return this.http.get(environment.url + "/api" + environment.version + "/payrollGroup/payfrequency")
      .pipe(map(res => res));
  }

  getPayrollGroup(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-groups/list", data)
      .pipe(map(res => res));
  }
  deletePaygroups(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-groups/delete", data)
      .pipe(map(res => res));
  }
  getSinglePayrollGroup(cId, pId) {
    return this.http.get(environment.url + "/api" + environment.version + "/payroll-groups/get/" + cId + '/' + pId)
      .pipe(map(res => res));
  }

  updatePayrollGroup(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/payroll-groups/edit", data)
      .pipe(map(res => res));
  }

  addSalaryGrade(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/salary-grades/add", data)
      .pipe(map(res => res));
  }
  updateSalaryGrade(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/salary-grades/edit", data)
      .pipe(map(res => res));
  }

  getSalrayGradeList(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/salary-grades/list", data)
      .pipe(map(res => res));
  }

  getSingleSalaryGrade(cId, sId) {
    return this.http.get(environment.url + "/api" + environment.version + "/salary-grades/get/" + cId + '/' + sId)
      .pipe(map(res => res));
  }
  deleteSalaryGrades(data) {
    return this.http.delete(environment.url + "/api" + environment.version + "/salary-grades/delete/"+ data._ids[0])
      .pipe(map(res => res));
  }
  deleteSalaryGradeBands(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/salary-grades/delete-grade-bands", data)
      .pipe(map(res => res));
  }

  previewCalander(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-calendars/get-pay-cycle-dates", data)
      .pipe(map(res => res));
  }

  singlePayrollCalanderAdd(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-calendars/add", data)
      .pipe(map(res => res));
  }
  pageEvents1(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-calendars/list", data)
      .pipe(map(res => res));
  }
  deleteCalander(data:any) {
    return this.http.delete(environment.url + "/api" + environment.version + "/payroll-calendars/delete/"+ data.companyId + '/' + data._ids[0])
      .pipe(map(res => res));

  }
  getPayrollCalender(cId, pcId) {
    return this.http.get(environment.url + "/api" + environment.version + "/payroll-calendars/get/" + cId + '/' + pcId)
      .pipe(map(res => res));

  }
  singlePayrollCalanderUpdate(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/payroll-calendars/edit", data)
      .pipe(map(res => res));
  }
  getSingleSalaraygradeBandsPagination(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/salary-grades/list-grade-bands", data)
      .pipe(map(res => res));
  }
  StartNextCycle(cId, calendarId) {
    return this.http.get(environment.url + "/api" + environment.version + "/payroll-calendars/current-and-next-cycle-details/" + cId + '/' + calendarId)
      .pipe(map(res => res));
  }
  cycleDataSend(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/payroll-calendars/start-next-cycle", data)
      .pipe(map(res => res));
  }

  reopenPreviousCycleSubmit(postaData) {
    return this.http.put(environment.url + "/api" + environment.version + "/payroll-calendars/reopen-cycle", postaData)
      .pipe(map(res => res));
  }
  reopenPreviousCycle(cId, calendarId) {
    return this.http.get(environment.url + "/api" + environment.version + "/payroll-calendars/previous-and-current-cycle-details/" + cId + '/' + calendarId)
      .pipe(map(res => res));
  }
  copyCalender(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/payroll-calendars/create-copy", data)
      .pipe(map(res => res));
  }
  getJobLinks(code){
    return this.http.get(environment.url + "/api" + environment.version + "/structure/getJobSalaryLinkDetails/" + code)
    .pipe(map(res => res));

  }
  compensationRationCalculate(data){
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/cal-compensation-ratio", data)
    .pipe(map(res => res));
  }
}
