import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResellerService {
  constructor(
    private http: HttpClient
  ) { }
  // purchasePackage(packageDetails) {
  //   return this.http.post(environment.url + "/api" + environment.version + "/orders/createOrder", packageDetails)
  //     .pipe(map(res => res));
  // }
  
  getAllCompanies(data){
    return this.http.post(environment.url + "/api" + environment.version + "/companies/resellerDetails", data )
    .pipe(map(res => res));
  }
}
