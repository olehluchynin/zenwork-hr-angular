import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable()

export class LoginService {
    constructor(
        private http: HttpClient
    ) { }

    login(clientDetails) {
        return this.http.post(environment.url + "/api" + environment.version + "/client/registerForDemo", clientDetails)
            .pipe(map(res => res));
    }
}