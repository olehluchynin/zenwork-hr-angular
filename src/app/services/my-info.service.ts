import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MyInfoService {

  constructor(private http: HttpClient) { }

  uploadPhoto(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/personal/upload", data)

  }

  getSingleTabSettingForMyinfo(tab) {
    return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/settingsOfMyInfoTab/" + tab)
      .pipe(map(res => res));
  }
  basicDetailsGetAll(data) {
    return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/settingsOfMyInfoTab/" + data)
  }

  updateUSer(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/users/update", data)

  }

  getOtherUser(company_id, user_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/users/getOtherUser" + "/" + company_id + "/" + user_id)
  }

  updateCurrentCompensation(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/myinfo/compensation/updateCurrentCompensation", data)

  }
  getStandardAndCustomStructureFields(company_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/structure/getStandardAndCustomFields/" + company_id)

  }

  addEmergenctContact(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/emergencyContact/add", data)

  }

  getAllEmergencyContacts(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/emergencyContact/getAll", data)

  }

  getEmergencyContact(company_id, user_id, ec_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/emergencyContact/get" + "/" + company_id + "/" + user_id + "/" + ec_id)

  }

  editEmergencyContact(ec_id, data) {
    return this.http.put(environment.url + "/api" + environment.version + "/myinfo/emergencyContact/edit" + "/" + ec_id, data)

  }

  deleteEmergencyContact(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/emergencyContact/delete", data)

  }

  addNotes(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/notes/add", data)

  }

  getAllNotes(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/notes/getAll", data)

  }

  getNote(company_id, user_id, note_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/notes/get" + "/" + company_id + "/" + user_id + "/" + note_id)
  }

  editNote(note_id, data) {
    return this.http.put(environment.url + "/api" + environment.version + "/myinfo/notes/edit" + "/" + note_id, data)

  }

  deleteNote(company_id, user_id, note_id) {
    return this.http.delete(environment.url + "/api" + environment.version + "/myinfo/notes/delete" + "/" + company_id + "/" + user_id + "/" + note_id)

  }

  changeStatus(note_id, data) {
    return this.http.put(environment.url + "/api" + environment.version + "/myinfo/notes/change-status" + "/" + note_id, data)

  }

  addAssets(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/assets/add", data)

  }

  getAllAssets(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/assets/getAll", data)

  }

  getAsset(company_id, user_id, asset_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/assets/get" + "/" + company_id + "/" + user_id + "/" + asset_id)

  }

  editAsset(asset_id, data) {
    return this.http.put(environment.url + "/api" + environment.version + "/myinfo/assets/edit" + "/" + asset_id, data)

  }

  deleteAsset(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/assets/delete", data)

  }
  listAssignedTrainings(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/assigned-trainings/listTrainings", data)

  }

  markAsComplete(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/assigned-trainings/mark-as-complete", data)

  }

  getAssignedTraining(company_id, user_id, id) {
    return this.http.get(environment.url + "/api" + environment.version + "/assigned-trainings/get" + "/" + company_id + "/" + user_id + "/" + id)

  }
  editAssignedTraining(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/assigned-trainings/edit", data)

  }
  deleteAssignedTrainings(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/assigned-trainings/deleteMany", data)

  }

  getOnboardingDetails(company_id, template_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/onboarding/get-onboarding-details" + "/" + company_id + "/" + template_id)

  }

  getOffboardingDetails(company_id, template_id) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/offboarding/get-offboarding-details" + "/" + company_id + "/" + template_id)

  }


  uploadDocument(formData) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/documents/upload", formData)

  }
  listDocuments(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/documents/list", data)

  }

  getDocumentDetails(company_id, user_id, id) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/documents/get" + "/" + company_id + "/" + user_id + "/" + id)

  }
  editDocuments(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/documents/edit", data)

  }
  deleteDocuments(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/documents/delete", data)

  }
  deleteEducation(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/personal/deleteEducation", data)

  }
  deleteLanguages(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/personal/deleteLanguages", data)

  }
  deleteVisaInformation(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/personal/deleteVisaInformation", data)

  }

  updateEmployeInfoHistory(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/job/updateEmploymentStatus", data)

  }
  updateJobInfoHistory(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/job/updateJobInformation", data)

  }
  getEmpJobInfo(cId, uId) {
    return this.http.get(environment.url + "/api" + environment.version + "/users/getOtherUser/" + cId + "/" + uId)
  }

  getPersonalSSNData(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/" + id)
  }
  sendingCustomService(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/myinfo/custom/addOrUpdate", data)

  }
  getcustomfieldsData(cId, uId) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/custom/" + cId + '/' + uId)

  }
  adminOffBoardingOverRide(uId) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/offboarding/adminOverride/" + uId)
  }

  adminOonBoardingOverRide(uId) {
    return this.http.get(environment.url + "/api" + environment.version + "/myinfo/onboarding/adminOverride/" + uId)

  }
  getPageACLS() {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/userAccessControls")

  }
}
