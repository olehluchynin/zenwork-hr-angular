import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OnboardingService {

  constructor(private http: HttpClient) { }



  getStandardOnboardingtemplate(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
      .pipe(map(res => res));
  }
  editDataSubit(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/editTask", data)
      .pipe(map(res => res));
  }
  addCustomOnboardTemplate(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/addEditTemplate", data)
      .pipe(map(res => res));
  }

  getAllCustomTemplates() {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getCustomTemplates")
      .pipe(map(res => res));
  }
  getStandardTemplates() {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getStandardTemplate")
      .pipe(map(res => res));
  }

  deleteCategory(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/deleteTask", data)
      .pipe(map(res => res));
  }


  getAllJobDetails() {
    return this.http.get(environment.url + "/api" + environment.version + "/structure/getFieldsForAddEmployee")
  }

  selectOnboardTemplate(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
  }

  getModulesData(data) {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplates", data)
  }
  deleteTemplate(id) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/deleteTemplate", id)
  }


  standardNewHireAllData(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/addEmployee", data)
  }

  newHireHrSummaryReviewData(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/addUpdateCustomWizard", data)
  }
  getAllNewHireHRData() {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/getAllCustomWizards")
  }
  customNewHrDelete(id) {
    return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/deleteCustomWizards", id)
  }
  getCustomNewHireWizard(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/getCustomWizard/" + id)
  }

  getSiteAccesRole(id) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/company/" + id)
  }


  getStructureFields(cID) {

    return this.http.get(environment.url + "/api" + environment.version + "/structure/getStandardAndCustomFields/" + cID)
      .pipe(map(res => res));
  }

  preEmployeeIdGeneration(cId){

    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/getEmployeeId/" + cId)
      .pipe(map(res => res));

    }




getAllWorkSchedule(data){
  return this.http.get(environment.url + "/api" + environment.version + "/scheduler/listPositionSchedules", {params : data})
}

 
standardScheduleData(data){
  return this.http.post(environment.url + "/api" + environment.version + "/scheduler/getempschedule",data)
}


createEmployeeData(data){
  return this.http.post(environment.url + "/api" + environment.version + "/scheduler/employeespecific", data)
}


}
