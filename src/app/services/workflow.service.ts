import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class WorkflowService {

  constructor(private http: HttpClient) { }

  createWorkFlow(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/workflow/create", data)
      .pipe(map(res => res));
  }

  getWorkflowservice(id, type) {
    return this.http.get(environment.url + "/api" + environment.version + "/workflow/get/" + id + '/' + type)
      .pipe(map(res => res));
  }

  getHrRoleEmployeesOnly(cid, hrID) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/users/" + cid + "/" + hrID)
      .pipe(map(res => res));
  }
  //localhost:3003/api/v1.0/roles/derived/:company_id/:role_id
  getHrRoleEmployees(cid, hId) {
    return this.http.get(environment.url + "/api" + environment.version + "/roles/derived/" + cid + '/' + hId)
      .pipe(map(res => res));
  }
  getManagerEmployees(cId, mId) {
    console.log(cId, mId);

    return this.http.get(environment.url + "/api" + environment.version + "/roles/derived/" + cId + '/' + mId)
      .pipe(map(res => res));
  }
  updateDemographicWorkflow(wId, postdata) {
    return this.http.put(environment.url + "/api" + environment.version + "/workflow/update/" + wId, postdata)
      .pipe(map(res => res));
  }
  LevelCreation(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/workflow/levels/create", data)
      .pipe(map(res => res));
  }
  gettingofHrlevels(cid) {
    return this.http.get(environment.url + "/api" + environment.version + "/workflow/levels/get/" + cid + '/' + 'HR')
      .pipe(map(res => res));
  }
  gettingofManagerlevels(cid) {
    return this.http.get(environment.url + "/api" + environment.version + "/workflow/levels/get/" + cid + '/' + 'Manager')
      .pipe(map(res => res));
  }

  findCorrespondingPersonId(cId, workflow_type, request_by, priority) {
    return this.http.get(environment.url + "/api" + environment.version + "/workflow/levels/get/" + cId + '/' + workflow_type + '/' + request_by + '/' + priority)
      .pipe(map(res => res));
  }

  checkEmpInWorkflow(uid) {
    return this.http.get(environment.url + "/api" + environment.version + "/workflow/checkPersonInWorkflows/" + uid)
      .pipe(map(res => res));
  }

}
