import { TestBed } from '@angular/core/testing';

import { ManagerFrontpageService } from './manager-frontpage.service';

describe('ManagerFrontpageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManagerFrontpageService = TestBed.get(ManagerFrontpageService);
    expect(service).toBeTruthy();
  });
});
