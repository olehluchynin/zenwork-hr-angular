import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { map} from 'rxjs/operators'

@Injectable ()

export class InvoiceService {
   
constructor (
    private http:HttpClient
){}   

getInvoiceBillingDetails(data){

    return this.http.post (environment.url+ "/api" + environment.version + '/orders/all' , data)
    .pipe(map(res=>res))
}


} 