import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class OffBoardingService  {

  constructor(private http: HttpClient) { }

  getAllTerminationEmployee(){
    return this.http.get(environment.url + "/api" + environment.version + "")
  }


  getAllOffboardTemplate(id) {
    return this.http.get(environment.url + "/api" + environment.version + "" + id)
  }

  getAllOffboardingTasks() {
    return this.http.get(environment.url + "/api" + environment.version + "")
  }

  getAllDirectReports(){
    return this.http.get(environment.url + "/api" + environment.version + "")
  }

  getAllCompanyProperty(){
    return this.http.get(environment.url + "/api" + environment.version + "")
  }

  terminationWizardSubmit(data){
    return this.http.post(environment.url + "/api" + environment.version + "",data)
  }




}