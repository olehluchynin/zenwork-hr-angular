import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable()

export class ZenworkersService {
    constructor(
        private http: HttpClient
    ) { }


    login(zenworkerDetails) {
        return this.http.post(environment.url + "/api" + environment.version + "/users/login", zenworkerDetails)
            .pipe(map(res => res));
    }
    addProfile(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/roles/addUpdate", data)
            .pipe(map(res => res));
    }
    editProfile(id) {
        return this.http.get(environment.url + "/api" + environment.version + "/roles/role/" + id)
            .pipe(map(res => res));
    }
    getAllroles() {
        return this.http.get(environment.url + "/api" + environment.version + "/roles/acls")
            .pipe(map(res => res));
    }


    getZenworkersDetails(data) {
        return this.http.get(environment.url + "/api" + environment.version + "/zenworkers/all", { params: data })
            .pipe(map(res => res));
    }

    addZenworker(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/zenworkers/add", data)
            .pipe(map(res => res));
    }

    editZenworker(data) {
        let id = data._id;
        let zenworker = {
            name: data.name,
            email: data.email,
            roleId: data.roleId,
            address: { state: data.address.state },
            status: data.status
        }
        return this.http.put(environment.url + "/api" + environment.version + "/zenworkers/" + id, zenworker)
            .pipe(map(res => res));
    }
    getSingleZenworker(id) {
        return this.http.get(environment.url + "/api" + environment.version + "/zenworkers/" + id)
            .pipe(map(res => res));
    }
    deleteZenworkers(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/zenworkers/delete", data)
            .pipe(map(res => res));
    }

}