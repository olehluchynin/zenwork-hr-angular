import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()

export class CompanySettingsService {

    constructor(private http: HttpClient) { }

    saveGeneralSettings(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companySetup/generalSettings", data)
            .pipe(map(res => res));
    }
    sendVerificationEmail(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/sendVerificationEmail", data)
            .pipe(map(res => res));
    }
    updateEmailUser(token) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/verifyEmail", token)
            .pipe(map(res => res));
    }
    getGeneralSettings() {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/getGeneralSettings")
            .pipe(map(res => res));
    }
    updateGeneralSettings(data) {
        return this.http.put(environment.url + "/api" + environment.version + "/companies/updateGeneralSettings", data)
            .pipe(map(res => res));
    }
    empSettings(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/empIdentitySettings", data)
            .pipe(map(res => res));
    }

    getShowFeilds() {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/getStandardSettings")
            .pipe(map(res => res));

    }
    postShowFeilds(data) {
        return this.http.put(environment.url + "/api" + environment.version + "/companies/settings/updateStandardSettings", data)
            .pipe(map(res => res));
    }
    addCustomFields(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/addUpdateCustomField", data)
            .pipe(map(res => res));
    }
    getAllCustomFields() {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/getAllCustomFields")
            .pipe(map(res => res));
    }
    getSingleCustomFieldData(id) {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/getCustomField/" + id)
            .pipe(map(res => res));
    }
    deleteCustomFieldData(deleteFields) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/deleteCustomField", deleteFields)
            .pipe(map(res => res));
    }



    saveCompanyContactInformation(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companySetup/companyContactInfo", data)
            .pipe(map(res => res));
    }

    getCompanyContactInfo() {
        return this.http.get(environment.url + "/api" + environment.version + "/companySetup/getCompanyContactInfo")
            .pipe(map(res => res));
    }
    controllSections() {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/settingsOfMyInfoSections")
            .pipe(map(res => res));
    }


    // Author: saiprakash, Date: 03/04/19
    // Company Structure APIs

    getFields() {
        return this.http.get(environment.url + "/api" + environment.version + "/structure/getFields")
            .pipe(map(res => res));
    }
    getFieldsForRoles() {
        return this.http.get(environment.url + "/api" + environment.version + "/structure/getFieldsForRoles")
            .pipe(map(res => res));
    }

    getStuctureFields(data, id) {

        return this.http.get(environment.url + "/api" + environment.version + "/structure/getSubDocs/" + data + `?withouttoken=${id}`)
            .pipe(map(res => res));
    }
    // getStuctureFieldsWithoutToken(data,id) {
    //     return this.http.get(environment.url + "/api" + environment.version + "/structure/getSubDocs/" + data+`?withouttoken=${id}`)
    //         .pipe(map(res => res));
    // }
    addUpdateStructureData(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/structure/addUpdate", data)
            .pipe(map(res => res));
    }
    structureUpload(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/structure/uploadxlsx", data)
            .pipe(map(res => res));
    }

    deleteStructure(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/structure/multi-del", data)
            .pipe(map(res => res));
    }

    // end company structure apis

    addTraining(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/training/add", data)
    }

    viewTrainingData(data) {
        return this.http.get(environment.url + "/api" + environment.version + "/training/getAll", { params: data })
    }

    TrainingDelete(id) {
        return this.http.post(environment.url + "/api" + environment.version + "/training/deleteMany", id)
    }

    EditTrainingData(id) {
        return this.http.get(environment.url + "/api" + environment.version + "/training/getSingleData/" + id)
    }

    updateTrainingData(data) {
        return this.http.put(environment.url + "/api" + environment.version + "/training/update/", data)
    }

    assignTrainingToEmployees(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/training/assignTraining", data)

    }
    //employee template
    addEmailTemplates(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/uploadEmailTemplate", data)
            .pipe(map(res => res));
    }
    getAllEmialTempates() {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/getEmailTemplates")
            .pipe(map(res => res));
    }
    deleteEmailTemplates(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/deleteEmailTemplates", data)
            .pipe(map(res => res));
    }
    editEmailTemplate(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/editEmailTemplate", data)
            .pipe(map(res => res));
    }
    singleEmailTemplate(id) {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/singleEmailTemplate/" + id)
            .pipe(map(res => res));
    }


    //   company policies


    uploadPolicy(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/uploadPolicy", data)
            .pipe(map(res => res));
    }
    getUploadPolicies() {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/getUploadPolicies")
            .pipe(map(res => res));
    }
    deletePolicies(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/deletePolicies", data)
            .pipe(map(res => res));
    }
    editUploadPolicy(data) {
        return this.http.post(environment.url + "/api" + environment.version + "/companies/settings/editUploadPolicy", data)
            .pipe(map(res => res));
    }
    getsinglePolicy(id) {
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/singlePolicy/" + id)
            .pipe(map(res => res));
    }


    getAllTraniningData() {
        return this.http.get(environment.url + "/api" + environment.version + "/structure/getTrainingDetails")
            .pipe(map(res => res));
    }
    getEmpIdentitySettings(id){
        return this.http.get(environment.url + "/api" + environment.version + "/companies/settings/" + id)
        .pipe(map(res => res));
    }

    prviewEmployeeList(data){
        return this.http.post(environment.url + "/api" + environment.version + "/employee-management/onboarding/hire-wizard/listEmployees", data)
        .pipe(map(res => res));
    }


    archiveTraining(data){
        return this.http.put(environment.url + "/api" + environment.version + "/training/changeStatus",data)
        .pipe(map(res => res));
    }

    unArchiveTraining(){
        return this.http.get(environment.url + "/api" + environment.version + "/training/unarchive")
        .pipe(map(res => res));
    }



}