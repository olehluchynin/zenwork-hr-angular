import { TestBed } from '@angular/core/testing';

import { ManagerReportsService } from './manager-reports.service';

describe('ManagerReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManagerReportsService = TestBed.get(ManagerReportsService);
    expect(service).toBeTruthy();
  });
});
