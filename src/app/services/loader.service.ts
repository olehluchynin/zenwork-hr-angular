import { Injectable } from "@angular/core";
import { NgxSpinnerService } from 'ngx-spinner';
import { map } from 'rxjs/operators';

@Injectable()

export class LoaderService {
    showLoading: boolean = false;

    constructor(
        private spinner: NgxSpinnerService
    ) { }

    loader(show) {
        if (show) {
            this.showLoading = show;
            // this.spinner.show();
        } else {
            this.showLoading = show;
            // this.spinner.hide();
        }
    }
//   hide() {
//     console.log("Loading bar : hide", );
//     // this.showLoadingOb.next(false);
//     this.showLoading = false;
//   }

//   show() {
//     console.log("Loading bar : Show");
//     this.showLoading = true;

//     console.log("Loading bar jvh: ", this.showLoading);
//   }
}