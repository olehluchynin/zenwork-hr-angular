import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ManagerApprovalService {

  constructor(private http: HttpClient) { }

  getAllPendingData(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/manager-requests/requestsList", data)
  }
  approveOrRejectRequest(data){
    return this.http.post(environment.url + "/api" + environment.version + "/manager-requests/edit", data)

  }

  editCompensationPending(cId, mId) {
    return this.http.get(environment.url + "/api" + environment.version + "/manager-requests/get/" + cId + '/' + mId)
  }

}
