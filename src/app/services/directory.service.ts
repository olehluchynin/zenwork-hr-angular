import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private http: HttpClient) { }

  getDirectorySettings(companyId) {
    return this.http.get(environment.url + "/api" + environment.version + "/employee-management/directory-settings/get/" + companyId)
      .pipe(map(res => res));
  }

  updateDirectorySettings(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/employee-management/directory-settings/update", data)
      .pipe(map(res => res));
  }

}
