import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OffBoardingTemplateService {

  constructor(private http: HttpClient) { }

  getStandardOffboardingTemplate() {

    return this.http.get(environment.url + "/api" + environment.version + "/offboarding/template/base/get")
      .pipe(map(res => res));
  }
  updateStandardTemplate(data) {
    return this.http.put(environment.url + "/api" + environment.version + "/offboarding/template/update/" + data._id, data)
      .pipe(map(res => res));
  }
  createcustomTemplate(data) {
    return this.http.post(environment.url + "/api" + environment.version + "/offboarding/template/create", data)
      .pipe(map(res => res));
  }
  updateCustomTemplate(data) {
    console.log(data);

    return this.http.put(environment.url + "/api" + environment.version + "/offboarding/template/update/" + data._id, data)
      .pipe(map(res => res));
  }
  getcustomOffboardingTemplate(cID) {
    return this.http.get(environment.url + "/api" + environment.version + "/offboarding/template/company/get/" + cID)
      .pipe(map(res => res));
  }
  deleteOffboardingTemplate(cID) {
    return this.http.delete(environment.url + "/api" + environment.version + "/offboarding/template/delete/" + cID)
      .pipe(map(res => res));
  }
  getcustomOffboardingTemplatedata(tID) {
    return this.http.get(environment.url + "/api" + environment.version + "/offboarding/template/get/" + tID)
      .pipe(map(res => res));
  }
  getDirectReports(cId, uId, pageNo, perPage) {
    return this.http.get(environment.url + "/api" + environment.version + "/offboarding/getDirectReports/" + cId + '/' + uId + '/' + pageNo + '/' + perPage)
    .pipe(map(res => res));
  }
  getManagersEmployeesOnly(cId,mId){
    return this.http.get(environment.url + "/api" + environment.version + "/roles/users/" + cId + '/' + mId)
    .pipe(map(res => res));
  }
  terminateEmployee(data){
    return this.http.post(environment.url + "/api" + environment.version + "/offboarding/terminateEmployee" ,data)
    .pipe(map(res => res));
  }
  getDirectTasks(uId){
    return this.http.get(environment.url + "/api" + environment.version + "/offboarding/assignedTasks/"  + uId)
    .pipe(map(res => res));
  }
}
