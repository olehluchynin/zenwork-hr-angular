import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class InboxService {

  constructor( private http:HttpClient) { }

markAsComplete(data){
  return this.http.post(environment.url + "/api" + environment.version + "/notifications/markAsComplete", data)
}  


getAllComponents(data){
  return this.http.post(environment.url + "/api" + environment.version + "/notifications/list",data)
}


deleteEmail(id){
  return this.http.post(environment.url + "/api" + environment.version + "/notifications/delete",id)
}

emailsArchive(id){
  return this.http.post(environment.url + "/api" + environment.version + "/notifications/archive", id)
}


restoreMails(id){
  return this.http.post(environment.url + "/api" + environment.version + "/notifications/restore", id)
}

markasReadData(id){
  return this.http.post(environment.url + "/api" + environment.version + "/notifications/markAsRead", id)
}

}
