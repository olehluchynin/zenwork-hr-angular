import { TestBed } from '@angular/core/testing';

import { PayRollService } from './pay-roll.service';

describe('PayRollService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PayRollService = TestBed.get(PayRollService);
    expect(service).toBeTruthy();
  });
});
