import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from "@angular/common/http";

import { Observable } from "rxjs";
import { AccessLocalStorageService } from "./accessLocalStorage.service";
import { map } from 'rxjs/operators';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private accessLocalStorageService: AccessLocalStorageService
    ) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        let token = this.accessLocalStorageService.get('x-access-token');
        if (token != null) {

            request = request.clone({
                setHeaders: {
                    "x-access-token": token,
                    // "Content-Type": "application/json"
                }
            });
        } else {
            request = request.clone({
                setHeaders: {
                    // "Content-Type": "application/json"
                }
            });
        }

        return next.handle(request);
    }
}
