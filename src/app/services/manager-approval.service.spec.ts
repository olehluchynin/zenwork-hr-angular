import { TestBed } from '@angular/core/testing';

import { ManagerApprovalService } from './manager-approval.service';

describe('ManagerApprovalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManagerApprovalService = TestBed.get(ManagerApprovalService);
    expect(service).toBeTruthy();
  });
});
