import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRouting } from './app.routes';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MainLandingModule } from './main-landing/main-landing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ClientDemoRegistrationService } from './services/client-demo.service';
import { ZenworkersService } from './services/zenworkers.service';
import { AccessLocalStorageService } from './services/accessLocalStorage.service';
import { TokenInterceptor } from './services/tokenInterceptor.service';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CompanyService } from './services/company.service';
import { SwalAlertService } from './services/swalAlert.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoaderService } from './services/loader.service';


import { CompanySettingsService } from './services/companySettings.service';
import { EmployeeService } from './services/employee.service';
import { PackageAndAddOnService } from './services/packageAndAddon.service';
import { PurchasePackageService } from './services/purchasePackage.service';
import { InvoiceService } from './services/invoice.service';
import { MessageService } from './services/message-service.service';
import { ResellerDashboardComponent } from './reseller-dashboard/reseller-dashboard.component';
import { AuthenticationService } from './services/authentication.service';
import { MatStepperModule } from '@angular/material/stepper';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { MaterialModuleModule } from './material-module/material-module.module';
import { AuthService } from './auth.service';
import { AuthGuard as AuthGuard } from './auth.guard';




// import { ResellerDashboardComponent } from './reseller-dashboard/reseller-dashboard.component';









@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent,
    LoginComponent,
    EmailVerifyComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MainLandingModule,
    HttpClientModule,
    AppRouting,
    FormsModule,
    NgxMatSelectSearchModule,
    NgxSpinnerModule,
    MatStepperModule,
    MaterialModuleModule

  ],
  providers: [
    MessageService,
    ClientDemoRegistrationService,
    ZenworkersService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AccessLocalStorageService,
    CompanyService,
    SwalAlertService,
    LoaderService,
    EmployeeService,
    CompanySettingsService,
    PackageAndAddOnService,
    PurchasePackageService,
    InvoiceService,
    AuthenticationService,
    // AuthGuardService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
