import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  successTxt: any;
  buttonText1:string = 'Cancel';
  buttonText2:string = 'Proceed';
  successTxt1:any;


  constructor(public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

  }
  ngOnInit() {
    this.successTxt= this.data;

    if(this.data.text == 'delete'){
      this.successTxt1 = 'Are you sure you want to delete?'
      this.buttonText1 = "No";
      this.buttonText2 = "Yes"
    }

    if(this.data.text == 'cancel'){
      this.successTxt1 = 'Are you sure you want to cancel?'
      this.buttonText1 = "No";
      this.buttonText2 = "Yes"
    }

    console.log(this.successTxt)
  }
  confirm() {
    this.dialogRef.close(true);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
