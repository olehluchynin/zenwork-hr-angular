import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../../services/message-service.service';
import { Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError} from '@angular/router';
import { ResellerService } from '../../services/reseller.service';
import { ActivatedRoute } from '@angular/router';
import { CompanySettingsService } from '../../services/companySettings.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnDestroy {
  subscription: any;
  message: any;
  url: any;
  id: any;
  showTabs = {
    Assets: { hide: true },
    Benefits: { hide: true },
    Compensation: { hide: true },
    Documents: { hide: true },
    Emergency_Contact: { hide: true },
    Job: { hide: false },
    Notes: { hide: true },
    Offboarding: { hide: true },
    Onboarding: { hide: true },
    Performance: { hide: true },
    Personal: { hide: false },
    Time_Off: { hide: false },
    Training: { hide: true }

  };
  @Input()
  set getUserData(data: any) {
    console.log("Data", data)
    this.userData = data;
  }
  x: any;
  type: any;
  acceslevel: any;
  restrictLevel1: boolean = false;
  empType:any;
  empHR:boolean = false;
  empManager: boolean = false;
  empEmployee: boolean = false;
  constructor(private route: ActivatedRoute,
    private messageService: MessageService,
    public router: Router,
    private loaderService:LoaderService,
    private resellerService: ResellerService,
    private accessLocalStorageService: AccessLocalStorageService,
    private companySettingsService: CompanySettingsService) {
      router.events.subscribe((event: RouterEvent) => {
        this.navigationInterceptor(event)
      })
      this.empType = this.accessLocalStorageService.get('employeeType')
    this.type = this.accessLocalStorageService.get("type");
    console.log('type',this.empType);
    if(this.empType == 'HR'){
      this.empHR = true;
    }
    if(this.empType == 'Manager'){
      this.empManager = true;
    }
    if(this.empType == 'Employee'){
      this.empEmployee = true;
    }
    console.log(this.type, "2w2w");
    if (this.type == 'administrator') {
      this.message = 'reseller';
    }
    if (this.type == 'zenworker') {
      this.acceslevel = this.accessLocalStorageService.get('zenworkerId');
      console.log(this.acceslevel);
      if (this.acceslevel.roleId.name == 'Level 1' || 'Level 2' || 'Billing') {
        this.restrictLevel1 = true;
      }

    }
    router.events.subscribe((url: any) => console.log(url));
    console.log(router.url);
    this.url = router.url;
    if (this.url == "/super-admin/super-admin-dashboard/reseller/" + this.id) {
      this.message = 'reseller';
    }
    if (this.url == "/super-admin/super-admin-dashboard/reseller/" + this.id + "/administrator") {
      this.message = 'reseller';
    }

    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.message = message.text;
      console.log("message", this.message);

    });
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log("iddddd", this.id, params);
    })

    console.log("23fsdfsd", this.message);

    this.controlMyinfoSections()
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.loader(true);
    }
    if (event instanceof NavigationEnd) {
      this.loaderService.loader(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loaderService.loader(false);
    }
    if (event instanceof NavigationError) {
      this.loaderService.loader(false);
    }
  }

  public userData;
  controlMyinfoSections() {
    this.companySettingsService.controllSections()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.showTabs = res.data.myInfo_sections;
          if (this.showTabs.Personal.hide == false) {
            this.x = 'job'
          }
        },
        (err: any) => {

        })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  resellerClients() {
    console.log("resellerloist")
    var id: any = localStorage.getItem('resellercmpnyID');
   
    console.log("cid", id);
    id = id.replace(/^"|"$/g, "");
    console.log("cin2", id);
    this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id])
  }
  administrator() {
    var id: any = localStorage.getItem('resellerId');
    console.log(id);
    console.log("cid", id);
    id = id.replace(/^"|"$/g, "");
    console.log("cin2", id);

    this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id + '/administrator'])
  }




}
