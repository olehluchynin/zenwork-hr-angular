import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SideNavComponent } from './side-nav-component/side-nav.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModuleModule } from '../material-module/material-module.module';
import { MessageService } from '../services/message-service.service';
import { MatIconModule } from '@angular/material';
import { ConfirmationComponent } from './confirmation/confirmation.component';



@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule
  ],
  declarations: [
    NavbarComponent,
    SideNavComponent,
    ConfirmationComponent
  ],
  entryComponents: [ConfirmationComponent],
  exports: [
    NavbarComponent,
    SideNavComponent,
    ConfirmationComponent
  ],
  providers: [MessageService]
})
export class SharedModule { }
