import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../../services/message-service.service';
import { ActivatedRoute } from '@angular/router';
import { ResellerService } from '../../services/reseller.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnDestroy {
  subscription: any;
  message: any;
  id : any;
  resellerName : any;
  constructor(
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private resellerService:ResellerService
  ) {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.message = message.text;
      console.log("message", this.message);
      this.resellerName = localStorage.getItem('resellerName')
    });
  }
  showFiller = false;
  ngOnDestroy() {
   
    this.subscription.unsubscribe();
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['/zenwork-login']);
  }

}
