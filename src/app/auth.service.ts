import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class AuthService {
  private messageSource = new BehaviorSubject < string > ("start");
  currentMessage = this.messageSource.asObservable();
 


  constructor() {}

  public getToken(): string {
    return localStorage.getItem("x-access-token");
  }

  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting
    // whether or not the token is expired
    return token ? true : false;
  }

  changemessage(message: string): void {  
    this.messageSource.next(message);
}
}