import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterAuthenticationComponent } from './footer-authentication/footer-authentication.component';
import { AuthenticationComponent } from './authentication.component';
import { MatSelectModule } from '@angular/material';
import { RegisterComponent } from './register/register.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TermsComponent } from './terms/terms.component';
import { AuthenticationService } from '../services/authentication.service';
import { SelfSignonDetailsComponent } from './self-signon-details/self-signon-details.component';
import { MaterialModuleModule } from '../material-module/material-module.module';
import { SelfSignOnDataComponent } from './self-sign-on-data/self-sign-on-data.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';


const router: Routes = [
  {
    path: '', component: AuthenticationComponent, children: [
      { path: 'zenwork-login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'sign-up', component: SignUpComponent },
      { path: 'terms', component: TermsComponent },
      { path: 'self-signon-detail', component: SelfSignonDetailsComponent },
      { path: 'social', component: SelfSignonDetailsComponent },
      {path:'reset-password/:id', component:ForgotPasswordComponent},
      {path:'reset', component:ResetPasswordComponent}
    ]
  }


]


@NgModule({
  imports: [
    MaterialModuleModule, CommonModule, RouterModule.forChild(router), FormsModule, ReactiveFormsModule, MatSelectModule, MatCheckboxModule,
    MatFormFieldModule,MatDialogModule
  ],
  declarations: [LoginComponent, FooterAuthenticationComponent, AuthenticationComponent, RegisterComponent,
    SignUpComponent, TermsComponent, SelfSignonDetailsComponent, SelfSignOnDataComponent,
    ForgotPasswordComponent, ResetPasswordComponent],
  providers: [
    AuthenticationService
  ],
  entryComponents: [
    SelfSignonDetailsComponent,
    SelfSignOnDataComponent
  ]

})
export class AuthenticationModule { }
