import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SwalAlertService } from '../../services/swalAlert.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private registerService:AuthenticationService
  ) { }

  public countrys = [
    "USA", "UK", "Canada", "India", "China"
  ]

  public years = [
    "January", "February", "March", "April", "May", "June" , "July", "August"
  ]

  public dates = [
    "1", "2","3","4","5","6","7","8","9","10"
  ]

  public times = [
    "1:00AM", "1:30AM", "2:00AM", "2:30AM", "3:00AM", "3:30AM", "4:00AM", "4:30AM", "5:00PM"
  ]

public registerForm : FormGroup

  ngOnInit() {

    this.registerForm = new FormGroup ({
      name: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email]),
      phone: new FormControl("", Validators.required),
    })


  }



registerSubmit(){
  console.log("Register", this.registerForm.value)
  if(this.registerForm.valid){

  }else{
    this.registerForm.get('name').markAsTouched();
    this.registerForm.get('email').markAsTouched();
    this.registerForm.get('phone').markAsTouched();
  }
}



}
