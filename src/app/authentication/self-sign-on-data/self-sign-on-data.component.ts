import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { SwalAlertService } from '../../services/swalAlert.service';


@Component({
  selector: 'app-self-sign-on-data',
  templateUrl: './self-sign-on-data.component.html',
  styleUrls: ['./self-sign-on-data.component.css']
})
export class SelfSignOnDataComponent implements OnInit {
  public signupForm: FormGroup

  public passwordType = "password"
  public tempData: any;

  constructor(
    public dialogRef: MatDialogRef<SelfSignOnDataComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private router: Router,
    public dialog: MatDialog,
    public signupService: AuthenticationService,
    public swalAlertService: SwalAlertService
  ) { }

  ngOnInit() {
    console.log(this.data);


    this.signupForm = new FormGroup({
      name: new FormControl("", Validators.required),
      employeCount: new FormControl("", Validators.required),
      primaryContact: new FormGroup({
        name: new FormControl("", Validators.required),
        phone: new FormControl("", Validators.required),
      }),

      // email: new FormControl("", [Validators.required, Validators.email]),
      // password: new FormControl("", Validators.required),
    })
    // this.getdata = JSON.parse(localStorage.getItem('addedCompany'))
    // console.log(this.getdata);

    // if (this.getdata) {
    //   console.log('dataim');

    //   this.signupForm.patchValue(this.getdata)
    //   this.signupDisable = true;
    // }
  }

  signupSubmit() {

    console.log("Signup", this.signupForm.value)
    var finalObj = this.signupForm.value
    // this.signupForm.patchValue({ signUpType: 'google' })
    // this.signupForm.patchValue({ googleId: this.data.googleId })
    // this.signupForm.patchValue({ email: this.data.email })
    finalObj['signUpType'] = 'google';
    finalObj['googleId'] = this.data.googleId;
    finalObj['email'] = this.data.email;
    console.log("Signup", finalObj)

    if (this.signupForm.valid) {
      // localStorage.setItem('addedCompany', JSON.stringify(this.signupForm.value));
      // this.router.navigate(['/self-on-boarding/client-details'])
      this.tempData = this.signupForm.valid

      this.signupService.signup(finalObj)
        .subscribe((response: any) => {
          console.log('Resposneee', response);
          if (response.status == true && response.data) {
            localStorage.setItem('addedCompany', JSON.stringify(response.data));
            this.router.navigate(['/self-on-boarding/client-details'])
          }
          if (response.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Details added successfully", response.status, " ");
            this.dialogRef.close();
            // Swal({
            //   title: 'Details added successfully',
            //   text: '',
            //   showConfirmButton: false,
            //   timer: 2500

            // });

          } else {

          }
        },
          (err) => {
            console.log(err);
            if (err.status == 409) {
              this.swalAlertService.SweetAlertWithoutConfirmation("Email", err.error.message, "error")
            }
          });
    } else {
      this.signupForm.get('name').markAsTouched();
      this.signupForm.get('employeCount').markAsTouched();
      this.signupForm.get('primaryContact').get('name').markAsTouched();
      this.signupForm.get('email').markAsTouched();
      this.signupForm.get('primaryContact').get('phone').markAsTouched();
      // this.signupForm.get('password').markAsTouched();
    }

  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
