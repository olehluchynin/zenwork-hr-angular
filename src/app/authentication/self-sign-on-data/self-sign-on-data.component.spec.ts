import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfSignOnDataComponent } from './self-sign-on-data.component';

describe('SelfSignOnDataComponent', () => {
  let component: SelfSignOnDataComponent;
  let fixture: ComponentFixture<SelfSignOnDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfSignOnDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfSignOnDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
