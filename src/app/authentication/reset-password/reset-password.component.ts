import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { SwalAlertService } from '../../services/swalAlert.service';




@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ResetPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private router: Router,
    private resetService: AuthenticationService,
    private SwalService: SwalAlertService
  ) { }

  isValid: any;
  email: any;

  ngOnInit() {


  }

// Author:Suresh M, Date:27-08-19
// resetSubmit
  resetSubmit() {

    var resetMail = {
      email: this.email
    }

    if (!this.email) {
      this.isValid = true
    } else {

      this.resetService.resetPassword(resetMail)
        .subscribe((res: any) => {
          console.log("reset linkk", res);
          if (res.status = true) {
            this.SwalService.SweetAlertWithoutConfirmation("Reset Password", "Reset password link is sent to your email", "success")
          }
          this.dialogRef.close();
        }, (err => {
          this.SwalService.SweetAlertWithoutConfirmation(err.error.message, "", "error")
        })
        );

    }

  }

  // Author:Suresh M, Date:27-08-19
// onNoClick Close
  onNoClick(): void {
    this.dialogRef.close();
  }

// Author:Suresh M, Date:27-08-19
// termsClick
  termsClick() {
    this.router.navigate(['/terms']);
    this.dialogRef.close();
  }


}
