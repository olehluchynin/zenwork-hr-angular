import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfSignonDetailsComponent } from './self-signon-details.component';

describe('SelfSignonDetailsComponent', () => {
  let component: SelfSignonDetailsComponent;
  let fixture: ComponentFixture<SelfSignonDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfSignonDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfSignonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
