import { Component, OnInit, Inject,NgZone } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { SelfSignOnDataComponent } from '../self-sign-on-data/self-sign-on-data.component';

import 'rxjs/add/operator/filter';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { MessageService } from '../../services/message-service.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-self-signon-details',
  templateUrl: './self-signon-details.component.html',
  styleUrls: ['./self-signon-details.component.css']
})
export class SelfSignonDetailsComponent implements OnInit {
  url: any;
  // param1: any;
  // param2: any;
  order: string;
  email: any;
  googleId: any;
  type:any;
  zenworkerDetails:any;
  companyId: any;
  videoURL: any;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private accessLocalStorageService:AccessLocalStorageService,
    private messageService:MessageService,
    private ngZone: NgZone

  ) {
    router.events.subscribe((url: any) => console.log(url));
    console.log(router.url);
    this.url = router.url;
    var routeurl = this.router.url.split('?')
    console.log(routeurl[0]);

    console.log(routeurl[0] == 'self-signon-detail');
    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (routeurl[0] == "/self-signon-detail") {

        this.email = params['email'];
        this.googleId = params['googleId'];
      }
    });

    if (routeurl[0] == "/social") {
      this.route.queryParams.subscribe(params => {
        console.log(params);
        this.type = params.type;
        console.log(this.type);
        this.companyId = params.companyId
         this.companyId = JSON.parse(this.companyId)
         this.accessLocalStorageService.set('x-access-token', this.companyId.token)

       
        
      });
    }

    // console.log(this.googleId, this.email);
    if (routeurl[0] == "/self-signon-detail") {
      this.openModal()
    }
    if (routeurl[0] == "/social") {
      if(this.type == 'company'){
        // JSON.parse(this.)
        console.log(this.companyId);
        this.ngZone.run(()=>{
          this.router.navigate(['/admin/admin-dashboard/company-settings/']);
        })
        this.accessLocalStorageService.set('type', this.type)
        this.router.navigate(['/admin/admin-dashboard/company-settings/']);
        this.accessLocalStorageService.set('companyId', this.companyId._id);
        this.accessLocalStorageService.set('addedBy', this.companyId.userId);
      }
      // if(this.type == 'sr-support' || 'zenworker' ){     
        
      //   this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
      //   this.accessLocalStorageService.set('companyId', this.companyId._id);
      //   this.accessLocalStorageService.set('addedBy', this.companyId.userId);
      // }
      // if(this.type == 'administrator' ){
      // this.accessLocalStorageService.set('type', this.type)
      // this.accessLocalStorageService.set('resellercmpnyID',this.companyId.userId)
      // this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.companyId.userId])
      // this.messageService.sendMessage('reseller');
      // }
    }

  }

  openModal(): void {
    const dialogRef = this.dialog.open(SelfSignOnDataComponent, {
      width: '700px',
      backdropClass: 'structure-model',
      data: {
        email: this.email,
        googleId: this.googleId
      },

    });

    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed', result);
      // this.myModal.nativeElement.click();
      // jQuery("#myModal").modal('show');


    });
  }


  ngOnInit() {

    environment.url
    this.videoURL = environment.url+'/videos/zenwork_demo.mp4'


  }

  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

  registerTop() {
    window.scroll(0, 0);
  }

}