import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-authentication',
  templateUrl: './footer-authentication.component.html',
  styleUrls: ['./footer-authentication.component.css']
})
export class FooterAuthenticationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  termsClick(){
    window.scroll(0,0);
  }

}
