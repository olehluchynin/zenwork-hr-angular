import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { SwalAlertService } from '../../services/swalAlert.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private forgotService: AuthenticationService, private swalService: SwalAlertService,
    private active: ActivatedRoute, private router: Router) { }

  passwordType = "password"
  passwordTypeNew = "password"

  newPassword: any;
  confirmPassword: any;
  isValid: any;

  sub: any;
  token: any;


  ngOnInit() {
    this.sub = this.active.params.subscribe(params => {
      this.token = params['id']
      console.log("tokenn iddddd", this.token);
    });

  }


  // Author:Suresh M, Date:27-08-19
  // newPasswordView
  newPasswordView() {
    if (this.passwordType === 'password') {
      this.passwordType = 'text'
    } else {
      this.passwordType = 'password'
    }
  }

  // Author:Suresh M, Date:27-08-19
  // ConfirmPasswordView
  ConfirmPasswordView() {
    if (this.passwordTypeNew === 'password') {
      this.passwordTypeNew = 'text'
    } else {
      this.passwordTypeNew = 'password'
    }
  }

  // Author:Suresh M, Date:27-08-19
  // forgetSubmit
  forgetSubmit() {

    var changePassword: any = {
      token: this.token,
      newPassword: this.newPassword,
      confirmPassword: this.confirmPassword
    }

    if (!this.newPassword) {
      this.isValid = true
    } else if (!this.confirmPassword) {
      this.isValid = true
    } else {

      this.forgotService.forgotPassword(changePassword)
        .subscribe((res: any) => {
          console.log("Change password success", res);
          if (res.status = true) {
            this.swalService.SweetAlertWithoutConfirmation("Change password", "Password changed successfully", "success")
          }
          this.router.navigate(['/zenwork-login']);
        }, (err => {
          this.swalService.SweetAlertWithoutConfirmation(err.error.message, "", "error")
        })
        );

    }

  }

}
