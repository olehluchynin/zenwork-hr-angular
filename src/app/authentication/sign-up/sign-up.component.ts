import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { SwalAlertService } from '../../services/swalAlert.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';



@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(
    private signupService: AuthenticationService,
    private swalAlertService: SwalAlertService,
    private router: Router
  ) { }

  public signupForm: FormGroup

  public passwordType = "password"
  public tempData: any;
  signupDisable: boolean = false;
  getdata: any;
  enviUrl:any;

  ngOnInit() {
    this.enviUrl = environment.url + "/api/v1.0/users/login/google" 

    this.signupForm = new FormGroup({
      name: new FormControl("", Validators.required),
      employeCount: new FormControl("", Validators.required),
      primaryContact: new FormGroup({
        name: new FormControl("", Validators.required),
        phone: new FormControl("", Validators.required),
      }),

      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", Validators.required),
    })
    this.getdata = JSON.parse(localStorage.getItem('addedCompany'))
    console.log(this.getdata);

    if (this.getdata) {
      console.log('dataim');

      this.signupForm.patchValue(this.getdata)
      this.signupDisable = true;
    }


  }

  getview() {
    if (this.passwordType === "password") {
      this.passwordType = "text";
    }
    else {
      this.passwordType = "password"
    }
  }

  signupSubmit() {

    console.log("Signup", this.signupForm.value)
    if (this.signupForm.valid) {
      localStorage.setItem('addedCompany', JSON.stringify(this.signupForm.value));
      // this.router.navigate(['/self-on-boarding/client-details'])
      this.tempData = this.signupForm.valid

      this.signupService.signup(this.signupForm.value)
        .subscribe((response: any) => {
          console.log('Resposneee', response);
          if (response.status == true && response.data) {
            localStorage.setItem('addedCompany', JSON.stringify(response.data));
            this.router.navigate(['/self-on-boarding/client-details'])
          }
          if (response.status == true) {
            // this.swalAlertService.SweetAlertWithoutConfirmation("Details added successfully", response.status, " ");

            Swal({
              title: 'Details added successfully',
              text: '',
              showConfirmButton: false,
              timer: 2500

            });

          } else {

          }
        },
          (err) => {
            console.log(err);
            if (err.status == 409) {
              this.swalAlertService.SweetAlertWithoutConfirmation("Email", err.error.message, "error")
            }
          });
    } else {
      this.signupForm.get('name').markAsTouched();
      this.signupForm.get('employeCount').markAsTouched();
      this.signupForm.get('primaryContact').get('name').markAsTouched();
      this.signupForm.get('email').markAsTouched();
      this.signupForm.get('primaryContact').get('phone').markAsTouched();
      this.signupForm.get('password').markAsTouched();
    }

  }


  signupUpdate() {
    console.log("update", this.getdata._id);
    // console.log(this.getdata.companyId);

    // this.signupForm.patchValue(this.getdata.companyId)
    // this.signupForm.value.patchValue(this.getdata._id)
    console.log(this.signupForm.value);
    var postdata = {
      'companyDetails': this.signupForm.value,

    }
    postdata['companyId'] = this.getdata._id;


    this.signupService.updateCompanyDetail(postdata)
      .subscribe((response: any) => {
        console.log('Resposneee', response);
        if (response.status == true && response.data) {
          localStorage.removeItem('addedCompany');
          localStorage.setItem('addedCompany', JSON.stringify(response.data));
          this.router.navigate(['/self-on-boarding/client-details'])
        }
        if (response.status == true) {
          // this.swalAlertService.SweetAlertWithoutConfirmation("Details added successfully", response.status, " ");

          Swal({
            title: 'Details updated successfully',
            text: '',
            showConfirmButton: false,
            timer: 2500

          });

        }



        else {
          this.signupForm.get('name').markAsTouched();
          this.signupForm.get('employeCount').markAsTouched();
          this.signupForm.get('primaryContact').get('name').markAsTouched();
          this.signupForm.get('email').markAsTouched();
          this.signupForm.get('primaryContact').get('phone').markAsTouched();
          this.signupForm.get('password').markAsTouched();
        }

      },
        (err) => {
          console.log(err);
          if (err.status == 409) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Email", err.error.message, "error")
          }
        })



  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  terms() {
    window.scroll(0, 0)
  }
  // googleSignup() {
  //   this.signupService.gooleSignIn()
  //     .subscribe((response: any) => {
  //       console.log('Resposneee', response);
  //     },
  //       (err) => {
  //         console.log(err);

  //       })
  // }


}
