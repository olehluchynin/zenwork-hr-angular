import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ResellerService } from '../services/reseller.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ZenworkersService } from '../services/zenworkers.service';
import { CompanyService } from '../services/company.service';
import { AccessLocalStorageService } from '../services/accessLocalStorage.service';
import { Router } from '@angular/router';
// import { MessageService } from '../services/message-service.service';
@Component({
  selector: 'app-reseller-dashboard',
  templateUrl: './reseller-dashboard.component.html',
  styleUrls: ['./reseller-dashboard.component.css']
})
export class ResellerDashboardComponent implements OnInit {
  id: any;

  public clientsSearch: FormGroup;
  public companyDetails;
  public pageNo = 1;
  public perPage = 10;
  // public selectedClient = '';
  zenworkClients: any;
  resellerName: any;
  constructor(private _location: Location,
    private route: ActivatedRoute,
    private resellerService: ResellerService,
    private companyService: CompanyService,
    private accessLocalStorageService: AccessLocalStorageService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.clientsSearch = new FormGroup({
      search: new FormControl('')
    })
    this.resellerName = localStorage.getItem('resellerName')
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log("iddddd", this.id, params);
    })

    // this.resellerService.getreseller(this.id)
    //   .subscribe(
    //     (res: any) => {

    //     })
    // this.clientsSearch = new FormGroup({
    //   search: new FormControl('')
    // })

    // this.messageService.sendMessage('super-admin');
    // this.onChanges(this.id);
    this.getClientsData();

  }
  backClicked() {
    console.log('backclickk');

    this._location.back();

  }
  // filterForClient(){
  //   console.log(this.selectedClient);
  //   this.getClientsData()
  // }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getClientsData();
  }
  // onChanges(id) {
  //   this.clientsSearch.controls['search'].valueChanges
  //     .subscribe(val => {
  //       // console.log(this.zenworkersSearch.get('search').value);
  //       this.getClientsData();
  //     });
  // }


  getClientsData() {
    this.resellerService.getAllCompanies({ searchQuery: this.clientsSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, resellerId: this.id })
      .subscribe(
        (response: any) => {
          if (response.status) {
            this.zenworkClients = response.data.length;
            this.companyDetails = response.data;
          }
          console.log("Response", response);
        }
      )
  }

  editCompany(company) {
    this.accessLocalStorageService.set('editCompany', company);
    this.router.navigate(['/super-admin/super-admin-dashboard/edit-company']);
  }
  // sendMessage(){
  //   this.messageService.sendMessage('add-client')
  // }
  resellerDashboard(id, comapanyName) {
    localStorage.setItem('resellerName', comapanyName);
    this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id])
    // this.messageService.sendMessage('reseller')
  }
  addclient() {
    this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.id + '/add-client'])
  }
  editResellerClient(company, id) {
    console.log(company, id);

    this.accessLocalStorageService.set('editCompany', company)
    this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.id + '/edit-client/' + id])
  }


}
