import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ResellerDashboardComponent } from './reseller-dashboard.component';
import { MaterialModuleModule } from '../material-module/material-module.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdministratorComponent } from './administrator/administrator.component';
import { CreateAdministratorTemplateComponent } from './create-administrator-template/create-administrator-template.component';
import { ResellerAddClientComponent } from './reseller-add-client/reseller-add-client.component';
import {MatDialogModule} from '@angular/material/dialog';
import { ResellerEditClientComponent } from './reseller-edit-client/reseller-edit-client.component'
const router: Routes = [
  { path: '', redirectTo: 'ResellerDashboardComponent', pathMatch: 'full' },
  {
    path: '', component: ResellerDashboardComponent,
  },
  {
    path: 'reseller-dashboard', component: ResellerDashboardComponent,
  },
  { path: 'add-client', component: ResellerAddClientComponent },
  { path: 'edit-client/:id', component: ResellerEditClientComponent },
  { path: 'administrator', component: AdministratorComponent }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(router),
    MaterialModuleModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule
  ],
  entryComponents: [
    CreateAdministratorTemplateComponent
  ],
  exports: [
    RouterModule
  ],
  declarations: [CreateAdministratorTemplateComponent,
    ResellerDashboardComponent,
    AdministratorComponent,
    ResellerAddClientComponent,
    ResellerEditClientComponent]
})
export class ResellerDashboardModule { }
