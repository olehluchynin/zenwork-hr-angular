import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ZenworkersService } from '../../services/zenworkers.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { LoaderService } from '../../services/loader.service';
import { ActivatedRoute } from '@angular/router';
import { CompanySettingsService } from '../../services/companySettings.service';

@Component({
  selector: 'app-create-administrator-template',
  templateUrl: './create-administrator-template.component.html',
  styleUrls: ['./create-administrator-template.component.css']
})
export class CreateAdministratorTemplateComponent implements OnInit {
  id: any;
  isValid: boolean = false;
  public createZenworker = {
    name: '',
    email: '',
    roleId: '',
    address: { state: '' },
    status: ''
  }
  userProfile: any;
  allStates = [];
  constructor(
    public dialogRef: MatDialogRef<CreateAdministratorTemplateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private zenworkersService: ZenworkersService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private companySettingsService: CompanySettingsService
  ) { }


  ngOnInit() {
    this.id = localStorage.getItem('resellerId')
    console.log(this.id);

    if (this.data) {
      this.createZenworker = this.data;
    }
    this.getAllRoles()
    this.getAllUSstates();
  }

  getAllUSstates() {
    this.companySettingsService.getStuctureFields('State',0)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allStates.push(element)
              console.log("22222111111", this.allStates);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allStates.push(element)
            });
            console.log("22222", this.allStates);
          }
        },
        (err) => {
          console.log(err);

        })
  }
  getAllRoles() {

    this.zenworkersService.getAllroles()
      .subscribe(
        (res: any) => {
          console.log("111111111", res);
          if (res.status == true) {
            // this.loaderService.loader(false)
            this.userProfile = res.data;
            this.userProfile.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }
  addZenworker() {
    // this.loaderService.loader(true);
    /* if (!this.createZenworker.name) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Name", 'error')
    } else if (!this.createZenworker.email) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Valid Email", 'error')
    } else if (!this.createZenworker.type) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Select Valid Zenworker Role", 'error')
    } else if (!this.createZenworker.address.state) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Add Address", 'error')
    } else if (!this.createZenworker.status) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Select Status", 'error')
    } else { */
    this.isValid = true;
    if (this.createZenworker.name && this.createZenworker.email && this.createZenworker.address.state &&
      this.createZenworker.roleId && this.createZenworker.status) {
      if (this.createZenworker.hasOwnProperty('_id')) {
        this.zenworkersService.editZenworker(this.createZenworker)
          .subscribe(
            (res) => {
              console.log("res", res);
              this.dialogRef.close();
            },
            (err) => {
              console.log("err", err);
            }
          )
      } else {
        var postData = this.createZenworker;
        postData['companyId'] = this.id
        console.log("admindtrator create", postData);

        this.zenworkersService.addZenworker(postData)
          .subscribe(
            (res: any) => {
              console.log("res", res);
              this.dialogRef.close();
              this.loaderService.loader(false);
              this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')

            },
            (err: HttpErrorResponse) => {
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          )
      }

    }
  }

}
