import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAdministratorTemplateComponent } from './create-administrator-template.component';

describe('CreateAdministratorTemplateComponent', () => {
  let component: CreateAdministratorTemplateComponent;
  let fixture: ComponentFixture<CreateAdministratorTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAdministratorTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdministratorTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
