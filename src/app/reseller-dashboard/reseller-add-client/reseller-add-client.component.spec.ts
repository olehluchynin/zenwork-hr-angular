import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResellerAddClientComponent } from './reseller-add-client.component';

describe('ResellerAddClientComponent', () => {
  let component: ResellerAddClientComponent;
  let fixture: ComponentFixture<ResellerAddClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResellerAddClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResellerAddClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
