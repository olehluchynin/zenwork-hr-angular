import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { Router } from '@angular/router';
import { PackageAndAddOnService } from '../../services/packageAndAddon.service';
import { PurchasePackageService } from '../../services/purchasePackage.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { CreditCardChargingService } from '../../services/chardgeCreditCard.service';
import { MatStepper } from '@angular/material/stepper'
import { MatSlideToggleChange, MatCheckbox } from '@angular/material';
import { LoaderService } from '../../services/loader.service';
// import * as jspdf from 'jspdf';
import { ActivatedRoute } from '@angular/router';
import { CompanySettingsService } from '../../services/companySettings.service';


@Component({
  selector: 'app-reseller-add-client',
  templateUrl: './reseller-add-client.component.html',
  styleUrls: ['./reseller-add-client.component.css']
})
export class ResellerAddClientComponent implements OnInit {
  dummy: any;
  public companyDetails;
  validity: boolean = false;
  isLinear: boolean = true;
  basePrice: any;
  selectedAddons: any;
  invoiceData1 = {
    startDate:'',
    endDate:''
  };
  paymentstatus: any;
  public selectedPackage = {
    clientId: '',
    name: '',
    wholeSaleDiscount: false,
    discount: 0,
    costPerEmployee: 0,
    price: 0,
    startDate: new Date(),
    endDate: new Date(new Date().setMonth(new Date().getMonth() + 1)),
    addOns: [],
    totalPrice: 0,
    packages: [],
    billingFrequency: 'monthly'
  };

  public companyContact = {
    name: '',
    email: '',
    phone: '',
    extention: '',
    isPrimaryContactIsBillingContact: true,
  }
  public billingContact = {
    name: '',
    email: '',
    phone: '',
    extention: ''

  }
  address = {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zipcode: '',
    country: '',
    isPrimaryAddressIsBillingAddress: true
  }
  billingAddress = {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zipcode: '',
    country: ''
  }
  dicountArray = [
    { value: 10, viewValue: '10%' },
    { value: 20, viewValue: '20%' },
    { value: 30, viewValue: '30%' },
    { value: 40, viewValue: '40%' },
    { value: 50, viewValue: '50%' },
    { value: 60, viewValue: '60%' },
    { value: 70, viewValue: '70%' },
    { value: 80, viewValue: '80%' },
    { value: 90, viewValue: '90%' },

  ];


  public packages;
  public selectedPackageData: any = {};
  public allAddOns;
  public orderId;
  public addedCompanyData: any = {};
  discountValue: any;
  discountEnable: boolean = true;
  basePriceDiscount: any;
  public invoiceData = {
    email: '',
    name: '',
    type: '',
    primaryContact: { phone: '', extention: '' },
    createdDate:''

  };
  invoiceBillingCompanyAddress = {
    address1: '',
    city: '',
    state: '',
    zipcode: '',
    country: '',
  };
  resellersList = [];
  invoiceadons: any; 
  id :any;
  allStates =[];
  constructor(
    private companyService: CompanyService,
    private swalAlertService: SwalAlertService,
    private router: Router,
    private packageAndAddOnService: PackageAndAddOnService,
    private purchasePackageService: PurchasePackageService,
    private accessLocalStorageService: AccessLocalStorageService,
    private creditCardChargingService: CreditCardChargingService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private companySettingsService:CompanySettingsService
  ) { }

ngOnInit() {
  this.route.params.subscribe(params => {
    this.id = params['id'];
    console.log("iddddd", this.id, params);
  })
    this.isLinear = true;
    // this.discountEnable = true;
    this.discountValue = 0;
    this.companyDetails = {
      name: '',
      tradeName: '',
      employeCount: '',
      referralcompany: '',
      email: '',
      type: '',
      belongsToReseller: '',
      resellerId: ''
    }
    this.companyContact = {
      name: '',
      email: '',
      phone: '',
      extention: '',
      isPrimaryContactIsBillingContact: true,
    }
    this.billingContact = {
      name: '',
      email: '',
      phone: '',
      extention: ''

    }
    this.address = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: '',
      isPrimaryAddressIsBillingAddress: true
    }
    this.billingAddress = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },

      this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    if (this.addedCompanyData == null) {
      this.addedCompanyData = {}
    }
    this.getAllPackages();
    this.getAllAddons();
    this.getAllresellers();
    this.getAllUSstates()
  }

  getAllUSstates() {
    this.companySettingsService.getStuctureFields('State',0)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allStates.push(element)
              console.log("22222111111", this.allStates);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allStates.push(element)
            });
            console.log("22222", this.allStates);
          }
        },
        (err) => {
          console.log(err);

        })
  }
  public cardDetails = {
    cardNumber: '',
    expMonth: '',
    expYear: '',
    cvv: '',
    cardName: ''
  }

 
  createCompany(stepper) {
    console.log(this.companyDetails);
    if (this.companyDetails.belongsToReseller == 'true') {
      this.companyDetails.type = 'reseller-client';
    }
    console.log(this.companyDetails)
    this.companyService.createCompany(this.companyDetails)
      .subscribe(
        (res: any) => {
          console.log("response-client", res);
          if (res.status == true) {
            this.addedCompanyData = res.data;
            if (this.addedCompanyData == null) {
              this.addedCompanyData = {}
            }
            this.accessLocalStorageService.set('addedCompany', this.addedCompanyData);
            this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')
           
            this.selectedPackage.clientId = this.addedCompanyData._id;
            this.goForward(stepper);
          }
          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error')
            

          }
        },

        (err: HttpErrorResponse) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }
  goForward(stepper: MatStepper) {
    stepper.next();
  }
  goBack(stepper: MatStepper) {
    stepper.previous();
  }
  getAllPackages() {
    this.packageAndAddOnService.getAllPackages()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.packages = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
  discountBasePrice() {
    console.log(this.discountValue);
    this.basePriceDiscount = (this.selectedPackage.price * this.discountValue) / 100;
    this.basePrice = this.selectedPackage.price - this.basePriceDiscount;
    this.getSpecificPackageDetails();
  }

  getAllAddons() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            console.log(this.allAddOns);

            this.allAddOns.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

  getAllAddOnDetails() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            this.allAddOns.forEach(addon => {
              addon.isSelected = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

  addAnotherAddOn() {
    this.selectedPackage.addOns.push({
      id: '',
      selectedAddOn: '',
      price: 0,
      index: this.selectedPackage.addOns.length + 1
    })
  }
  addOnChange(addOn) {
    console.log(addOn);

    console.log("before", this.selectedPackage.addOns);

    // for(let ref of this.selectedPackage.addOns){
    //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
    // }
    // console.log("after",this.selectedPackage.addOns);

    this.selectedPackage.totalPrice = 0
    for (let i = 0; i < this.allAddOns.length; i++) {
      this.selectedPackage.addOns.forEach(addon => {
        console.log(addon, this.allAddOns[i])
        if (addon.selectedAddOn == this.allAddOns[i].name) {
          addon.price = this.allAddOns[i].price;
          addon.id = this.allAddOns[i]._id;
          console.log(addon.id);
          this.selectedAddons = this.selectedPackage.addOns;
          this.selectedAddons['id'] = addon.id;
          console.log(this.selectedAddons);

        }
      });
    }
    var price = 0;
    this.selectedPackage.addOns.forEach(data => {
      price += data.price
    })
    console.log(price);

    this.selectedPackage.totalPrice = this.dummy + price
  }
  addonRemove(addon) {

    // if (addon == '') {
    //   this.selectedPackage.addOns = []
    // }
    console.log("addon", addon);
    // for (let i = 0; i < this.allAddOns.length; i++) {
    //   this.selectedPackage.addOns.forEach(addon => {
    //     console.log(addon, this.allAddOns[i])
    //     if (addon == this.allAddOns[i].name) {

    //    }
    //   });
    // }
    this.selectedPackage.addOns.forEach(data => {
      if (data.selectedAddOn == addon) {
        this.selectedPackage.totalPrice -= data.price;
      }
    });
    this.selectedPackage.addOns = this.selectedPackage.addOns.filter(data => addon != data.selectedAddOn)


    console.log(this.selectedPackage.addOns);

  }
  getAllresellers() {
    this.companyService.getresellers()
      .subscribe(
        (res: any) => {
          console.log("reselerrsList", res);
          this.resellersList = res.data;
        },
        (err: any) => {

        })
  }
  selectedReseller(id) {
    console.log(id);

  }

  getSpecificPackageDetails() {
    this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    console.log(this.selectedPackage.name, this.packages);
    this.packages.forEach(packageData => {
      if (packageData.name == this.selectedPackage.name) {
        this.selectedPackage.packages = this.packages.filter(data => data.name == this.selectedPackage.name)
        console.log(this.selectedPackage.packages);

        if (this.validity == false) {
          this.selectedPackage.price = packageData.price;
        }
        else {
          this.selectedPackage.price = (packageData.price * 12);
        }
        // this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
      }
    });
    // this.selectedPackage.totalPrice = this.selectedPackage.price + this.selectedPackage.costPerEmployee * this.addedCompanyData.employeCount;
    // if (this.basePriceDiscount > 0) {
      // this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
    // }
    this.discountEnable = false;
    /* this.selectedPackageData = packageData; */
    /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.selectedPackageData = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      ) */
    // this.dummy = this.selectedPackage.totalPrice;
  }

  toggleDateChange(event: MatSlideToggleChange) {
    console.log(event.checked);
    if (event.checked == true) {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
      this.selectedPackage.billingFrequency = 'annually';
      this.validity = true;
    }
    else {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
      this.validity = false;
      this.selectedPackage.billingFrequency = 'monthly';
    }
    this.getSpecificPackageDetails()
  }
  wholesaleDiscountChange(event) {

  }

  saveAddressDetails(stepper) {

    var postData = {
      _id: this.addedCompanyData._id,
      primaryContact: this.companyContact,
      isPrimaryContactIsBillingContact: this.companyContact.isPrimaryContactIsBillingContact,
      billingContact: this.billingContact,
      primaryAddress: this.address,
      isPrimaryAddressIsBillingAddress: this.address.isPrimaryAddressIsBillingAddress,
      billingAddress: this.billingAddress,

    }
    this.companyService.addContactDetails(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')

            this.companyContact = {
              name: '',
              email: '',
              phone: '',
              extention: '',
              isPrimaryContactIsBillingContact: true,
            }
            this.billingContact = {
              name: '',
              email: '',
              phone: '',
              extention: ''

            }
            this.address = {
              address1: '',
              address2: '',
              city: '',
              state: '',
              zipcode: '',
              country: '',
              isPrimaryAddressIsBillingAddress: true
            }
            this.billingAddress = {
              address1: '',
              address2: '',
              city: '',
              state: '',
              zipcode: '',
              country: ''
            }
            this.companyDetails = {
              name: '',
              tradeName: '',
              employeCount: '',
              referralcompany: '',
              email: '',
              type: '',
              belongsToReseller: '',
              resellerId: ''
            };
          }
          this.router.navigate(['super-admin/super-admin-dashboard/reseller/'+this.id])
        },
        (err) => {
          console.log(err);
        })
  }


  savePackageDetails(stepper) {
    console.log("this.selectedPackage", this.selectedPackage);
    this.invoiceadons = this.selectedPackage.addOns;

    var postData = this.selectedPackage
    delete postData.addOns
    postData['adons'] = this.invoiceadons;

    console.log(postData);


    this.purchasePackageService.purchasePackage(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.orderId = res.data.id
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success')
            // this.activeTab = 'payment';
            this.goForward(stepper);
          }

          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error')

        }
      )
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

 
  


}
