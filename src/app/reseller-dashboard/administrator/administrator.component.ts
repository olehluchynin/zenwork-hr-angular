import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ZenworkersService } from '../../services/zenworkers.service';
// import { CreateZenworkerTemplateComponent } from '../create-zenworker-template-component/create-zenworker-template-component.component';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { LoaderService } from '../../services/loader.service';
import { CreateAdministratorTemplateComponent } from '../create-administrator-template/create-administrator-template.component';
// import { CreateZenworkerTemplateComponent } from '../../super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {
  public clients;
  public zenworkerDetails;
  public selectedZenworkerDetails;
  public selectedZenworkRole;
  public zenworkersSearch: FormGroup;
  public perPage;
  public pages;
  public pageNo;
  public selectedRole: any;
  public allZenworkers: boolean = false;
  id :any;
  constructor(
    public dialog: MatDialog,
    private zenworkersService: ZenworkersService,
    private swalAlertService: SwalAlertService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log("iddddd", this.id, params);
    })
    this.loaderService.loader(true);
    this.zenworkersSearch = new FormGroup({
      search: new FormControl('')
    });
    this.selectedZenworkRole = "all";
    this.perPage = 10;
    this.pages = [10, 25, 50];
    this.pageNo = 1;
  
    // this.selectedZenworkerDetails = { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false }
    this.onChanges();
    this.getZenworkersData();
  }

  onChanges() {
    this.zenworkersSearch.controls['search'].valueChanges
      .subscribe(val => {
        // console.log(this.zenworkersSearch.get('search').value);
        this.getZenworkersData();
      });
  }


  getZenworkersData() {
    console.log("change")
    this.loaderService.loader(true);
    let zenworker;
    if (this.selectedZenworkRole == "all") {
      zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage }
    } else {
      this.selectedRole = this.selectedZenworkRole;
      zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, role: this.selectedRole }
    }
    var postData = zenworker;
      postData['companyId'] = this.id
      console.log("admindtrator create",postData);
    this.zenworkersService.getZenworkersDetails(zenworker)
      .subscribe(
        (response: any) => {
          console.log(response);
          
          this.loaderService.loader(false);
          if (response.status) {
            // this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')
            this.zenworkerDetails = response.data;
            this.zenworkerDetails.forEach(zenworker => {
              zenworker.isChecked = false;
            });
          }else{
            this.swalAlertService.SweetAlertWithoutConfirmation("Zenworkers", "Data Not Found", 'info')
          }
        },
        (err: HttpErrorResponse) => {
          this.loaderService.loader(false);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

  selectAllZenworkers() {
    // console.log("Selecting all", this.allZenworkers);
    this.zenworkerDetails.forEach(zenworker => {
      zenworker.isChecked = this.allZenworkers;
    });
  }

  changePage(p) {
    this.pageNo = p;
    this.getZenworkersData();
  }

  addZenworker(data) {
    localStorage.setItem('resellerId',this.id)
    const dialogRef = this.dialog.open(CreateAdministratorTemplateComponent, {
      height: '700px',
      width: '890px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("Result", result)
      this.getZenworkersData();
    });
  }

}
