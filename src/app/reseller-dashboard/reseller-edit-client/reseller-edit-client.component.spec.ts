import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResellerEditClientComponent } from './reseller-edit-client.component';

describe('ResellerEditClientComponent', () => {
  let component: ResellerEditClientComponent;
  let fixture: ComponentFixture<ResellerEditClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResellerEditClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResellerEditClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
