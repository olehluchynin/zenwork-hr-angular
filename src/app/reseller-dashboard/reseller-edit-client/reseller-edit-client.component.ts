import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanyService } from '../../services/company.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../services/swalAlert.service';
import { Router } from '@angular/router';
import { PackageAndAddOnService } from '../../services/packageAndAddon.service';
import { PurchasePackageService } from '../../services/purchasePackage.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { CreditCardChargingService } from '../../services/chardgeCreditCard.service';
import { ActivatedRoute } from '@angular/router';
import { MatSlideToggleChange, MatCheckbox } from '@angular/material';
import { CompanySettingsService } from '../../services/companySettings.service';

@Component({
  selector: 'app-reseller-edit-client',
  templateUrl: './reseller-edit-client.component.html',
  styleUrls: ['./reseller-edit-client.component.css']
})
export class ResellerEditClientComponent implements OnInit {

  public activeTab = 'create';
  isValid: boolean = false;
  public selectedPackage = {
    clientId: '',
    name: '',
    wholeSaleDiscount: false,
    discount: 0,
    costPerEmployee: 0,
    price: 0,
    startDate: new Date(),
    endDate: new Date(),
    addOns: [],
    packages: [],
    totalPrice: 0,
    billingFrequency: 'monthly'
  };
  public companyDetails;
  public packages;
  public selectedPackageData: any = {};
  public allAddOns;
  public orderId;

  public addedCompanyData: any = {};
  url: any;
  id: any;
  dummy: any;
  companyNameValue: any;
  comapnyInactiveDate: any;
  clientStatus: any;
  billingHistory = {};
  companyName: any;
  currentPlan = {

  };
  discountValue: any;
  discountEnable: boolean = true;
  basePriceDiscount: any;
  basePrice: any;
  validity: boolean = false;
  selectedAddons: any;
  billingHistoryAddons = [];
  billingHistoryPackages = [];
  currentPlanAddons = [];
  currentPlanPackage = [];
  resellersList = [];
  dicountArray = [
    { value: 10, viewValue: '10%' },
    { value: 20, viewValue: '20%' },
    { value: 30, viewValue: '30%' },
    { value: 40, viewValue: '40%' },
    { value: 50, viewValue: '50%' },
    { value: 60, viewValue: '60%' },
    { value: 70, viewValue: '70%' },
    { value: 80, viewValue: '80%' },
    { value: 90, viewValue: '90%' },

  ];
  invoiceadons: any;
  tempforDiscountChange: any;
  allStates = [];
  constructor(
    private companyService: CompanyService,
    private swalAlertService: SwalAlertService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private packageAndAddOnService: PackageAndAddOnService,
    private purchasePackageService: PurchasePackageService,
    private accessLocalStorageService: AccessLocalStorageService,
    private creditCardChargingService: CreditCardChargingService,
    private companySettingsService:CompanySettingsService
  ) { }


  ngOnInit() {
    this.discountValue = 0;
    this.companyDetails = {
      name: '',
      tradeName: '',
      employeCount: '',
      email: '',
      type: '',
      belongsToReseller: '',
      reseller: '',
      primaryContact: { name: '', phone: '', extention: '' },
      billingContact: { name: '', email: '', phone: '', extention: '' },
      address: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },
      billingAddress: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },
      isPrimaryContactIsBillingContact: true,
      isPrimaryAddressIsBillingAddress: true
    }

    this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    if (this.addedCompanyData == null) {
      this.addedCompanyData = {}
    }
    this.activatedRoute.params.subscribe(params => {

      this.id = params['id'];
      console.log("iddddd", this.id, params);
    })
    this.getSingleUser();
    this.getAllPackages();
    this.getAllAddons();
    this.getSingleCompanyBillingHistory();
    this.getSingleComapnyCurrentPlan();
    this.getAllresellers();
    this.getAllUSstates();
  }
  getAllUSstates() {
    this.companySettingsService.getStuctureFields('State',0)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.custom.length > 0) {
            res.custom.forEach(element => {
              this.allStates.push(element)
              console.log("22222111111", this.allStates);
            });
          }
          if (res.default.length > 0) {
            res.default.forEach(element => {
              this.allStates.push(element)
            });
            console.log("22222", this.allStates);
          }
        },
        (err) => {
          console.log(err);

        })
  }
  getSingleUser() {
    var comapnyData = this.accessLocalStorageService.get('editCompany');
    this.id = comapnyData._id;
    this.companyService.editClient(this.id)
      .subscribe(
        (res: any) => {
          console.log("editres", res);
          this.companyDetails = res.data;
          if (this.companyDetails.type == 'reseller-client') {
            var x = this.companyDetails.type;
            this.companyDetails.type = 'company';
            this.companyDetails.belongsToReseller = true;
          }
          if (this.companyDetails.type == "company" && x != 'reseller-client') {
            this.companyDetails.type = 'company';
            this.companyDetails.belongsToReseller = false;
          }

          this.companyDetails.primaryContact = res.data.primaryContact;

          this.companyDetails.billingContact = res.data.billingAddress;
          this.companyDetails.address = res.data.primaryAddress;
          this.companyDetails.billingAddress = res.data.billingAddress;
          if (res.data.orders) {
            this.comapnyInactiveDate = res.order.endDate;
            var date = new Date(this.comapnyInactiveDate);
            console.log(date);
            this.comapnyInactiveDate = date.toLocaleDateString();
            console.log(this.comapnyInactiveDate);
            var today = new Date().toLocaleDateString();
            if (today < this.comapnyInactiveDate) {
              this.clientStatus = "Active";
            }
            else {
              this.clientStatus = "Inactive"
            }
          }





        },
        (err: any) => {
          console.log(err);

        })
  }

  getSingleCompanyBillingHistory() {
    this.addedCompanyData = this.accessLocalStorageService.get('editCompany');
    console.log(this.addedCompanyData);
    this.companyNameValue = this.addedCompanyData.companyId.name
    console.log(this.companyNameValue);

    this.id = this.addedCompanyData.companyId._id;
    this.companyService.getBillingHistory(this.id)
      .subscribe(
        (res: any) => {
          console.log("billinghistory", res);
          if (res.data) {
            this.billingHistory = res.data;
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].adons.length >= 1) {
                if (res.data[i].adons.length == 1) {
                  this.billingHistoryAddons.push(res.data[i].adons);
                }
                if (res.data[i].adons.length > 1) {
                  console.log(res.data[i].adons);
                  for (var j = 0; j < res.data[i].adons.length; j++) {
                    this.billingHistoryAddons.push([res.data[i].adons[j]])
                  }

                }

              }
              if (res.data[i].packages.length >= 1) {
                this.billingHistoryPackages.push(res.data[i].packages[0]);
              }
              console.log("adins,packages", this.billingHistoryAddons, this.billingHistoryPackages);

            }
          }
          else {
            this.billingHistoryPackages = [];
            this.billingHistoryAddons = [];
          }
        },
        (err: any) => {

        })
  }
  saveEditDetails() {
    var comapnyData = this.accessLocalStorageService.get('editCompany');
    this.isValid = true;
    this.id = comapnyData.companyId._id;
    console.log(this.id);

    console.log("edit and update data", this.companyDetails);
    if (this.companyDetails.name && this.companyDetails.tradeName && this.companyDetails.employeCount &&
      this.companyDetails.type && this.companyDetails.primaryContact.name && this.companyDetails.primaryContact.phone &&
      this.companyDetails.email && this.companyDetails.primaryContact.extention && this.companyDetails.address.address1 &&
      this.companyDetails.address.address2 && this.companyDetails.address.city && this.companyDetails.address.state &&
      this.companyDetails.address.zipcode && this.companyDetails.address.country) {
      this.companyService.editDataSave(this.companyDetails, this.id)
        .subscribe(
          (res: any) => {
            console.log(res);
            if (res.status == true) {
              this.activeTab = 'package';
              this.swalAlertService.SweetAlertWithoutConfirmation("Client Details", res.message, 'success')
            }
          },
          (err: any) => {

          })
    }

  }
  getSingleComapnyCurrentPlan() {
    var comapnyData = this.accessLocalStorageService.get('editCompany');
    console.log(comapnyData);
    this.id = comapnyData.companyId._id;
    console.log(this.id);
    this.companyService.currentPlan(this.id)
      .subscribe(
        (res: any) => {
          console.log("currentPlan", res);
          this.currentPlan = res.data;
          if (this.currentPlan) {
            this.currentPlanAddons = res.data.adons;
            this.currentPlanPackage = res.data.packages;
          }
        },
        (err: any) => {

        })
  }


  discountBasePrice() {
    console.log(this.discountValue);
    if (this.discountValue <= 100) {
      var x = (this.tempforDiscountChange * this.discountValue / 100)
      this.selectedPackage.totalPrice = this.tempforDiscountChange - x;
      // this.getSpecificPackageDetails();
    }
    else {
      this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error')
      this.discountValue = ''
      this.getSpecificPackageDetails();
      this.addOnChange();
    }
  }

  addOnChange() {
    // console.log(addOn);

    console.log("before", this.selectedPackage.addOns);

    // for(let ref of this.selectedPackage.addOns){
    //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
    // }
    // console.log("after",this.selectedPackage.addOns);

    this.selectedPackage.totalPrice = 0
    for (let i = 0; i < this.allAddOns.length; i++) {
      this.selectedPackage.addOns.forEach(addon => {
        console.log(addon, this.allAddOns[i])
        if (addon.selectedAddOn == this.allAddOns[i].name) {
          addon.price = this.allAddOns[i].price;
          addon.id = this.allAddOns[i]._id;
          console.log(addon.id);
          this.selectedAddons = this.selectedPackage.addOns;
          this.selectedAddons['id'] = addon.id;
          console.log(this.selectedAddons);

        }
      });
    }
    var price = 0;
    this.selectedPackage.addOns.forEach(data => {
      price += data.price
    })
    console.log(price);

    this.selectedPackage.totalPrice = this.dummy + price;
    this.tempforDiscountChange = this.selectedPackage.totalPrice;
    this.discountBasePrice();
  }
  toggleDateChange(event: MatSlideToggleChange) {
    console.log(event.checked);
    if (event.checked == true) {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
      this.selectedPackage.billingFrequency = 'annually';
      this.validity = true;
    }
    else {
      this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
      this.validity = false;
      this.selectedPackage.billingFrequency = 'monthly';
    }
    this.getSpecificPackageDetails()
  }



  public cardDetails = {
    cardNumber: '',
    expMonth: '',
    expYear: '',
    cvv: '',
    cardName: ''
  }

  chargeCreditCard() {
    (<any>window).Stripe.card.createToken({
      number: this.cardDetails.cardNumber,
      exp_month: this.cardDetails.expMonth,
      exp_year: this.cardDetails.expYear,
      cvc: this.cardDetails.cvv
    }, (status: number, response: any) => {
      if (status === 200) {
        let token = response.id;
        console.log(token)
        // this.chargeCard(token);
        var id = JSON.parse(localStorage.getItem("addedCompany"))
        console.log(id)
        var amount = this.selectedPackage.totalPrice + this.selectedPackage.totalPrice * 0.08
        this.creditCardChargingService.createCharge(token, id._id, amount, this.orderId)
          .subscribe(
            (response: any) => {
              console.log(response);
              this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')

            },
            err => {
              console.log(err);
            }
          )
      } else {
        console.log(response.error.message);

      }
      this.cardDetails.cardNumber = '';
      this.cardDetails.expMonth = '';
      this.cardDetails.expYear = '';
      this.cardDetails.cvv = '';
    });

  }




  editCompany() {
    this.companyService.createCompany(this.companyDetails)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.addedCompanyData = res.data;
            if (this.addedCompanyData == null) {
              this.addedCompanyData = {}
            }
            this.accessLocalStorageService.set('addedCompany', this.addedCompanyData);
            this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')
            this.companyDetails = {
              name: '',
              tradeName: '',
              employeCount: '',
              email: '',
              type: '',
              belongsToReseller: '',
              reseller: '',
              primaryContact: { name: '', phone: '', extention: '' },
              billingContact: { name: '', email: '', phone: '', extention: '' },
              address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
              },
              billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
              },
              isPrimaryContactIsBillingContact: true,
              isPrimaryAddressIsBillingAddress: true
            };
            this.selectedPackage.clientId = this.addedCompanyData._id;
            this.activeTab = 'package';
            // this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }


  cancel() {
    this.companyDetails = {
      name: '',
      tradeName: '',
      employeCount: '',
      email: '',
      type: '',
      belongsToReseller: '',
      reseller: '',
      primaryContact: { name: '', phone: '', extention: '' },
      billingContact: { name: '', email: '', phone: '', extention: '' },
      address: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },
      billingAddress: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        country: ''
      },
      isPrimaryContactIsBillingContact: true,
      isPrimaryAddressIsBillingAddress: true
    }
  }

  getAllPackages() {
    this.packageAndAddOnService.getAllPackages()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.packages = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }



  getSpecificPackageDetails() {
    // this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    console.log(this.selectedPackage.name, this.packages);
    this.packages.forEach(packageData => {
      if (packageData.name == this.selectedPackage.name) {
        this.selectedPackage.packages = this.packages.filter(data => data.name == this.selectedPackage.name)
        console.log(this.selectedPackage.packages);

        if (this.validity == false) {
          this.selectedPackage.price = packageData.price;
        }
        else {
          this.selectedPackage.price = (packageData.price * 12);
        }
        this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
      }
    });
    this.selectedPackage.totalPrice = this.selectedPackage.price + this.selectedPackage.costPerEmployee * this.companyDetails.employeCount;
    // if (this.basePriceDiscount > 0) {
    //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
    // }
    this.discountEnable = false;
    /* this.selectedPackageData = packageData; */
    /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.selectedPackageData = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      ) */
    this.dummy = this.selectedPackage.totalPrice;
    this.tempforDiscountChange = this.selectedPackage.totalPrice;
  }


  getAllAddons() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            console.log(this.allAddOns);

            this.allAddOns.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }
  selectedReseller(id) {
    console.log(id);

  }
  getAllresellers() {
    this.companyService.getresellers()
      .subscribe(
        (res: any) => {
          console.log("reselerrsList", res);
          this.resellersList = res.data;
        },
        (err: any) => {

        })
  }

  getAllAddOnDetails() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            this.allAddOns.forEach(addon => {
              addon.isSelected = false;
            });
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      )
  }

  addAnotherAddOn() {
    this.selectedPackage.addOns.push({
      id: '',
      selectedAddOn: '',
      price: 0,
      index: this.selectedPackage.addOns.length + 1
    })
  }

  addonRemove(addon) {

    // if (addon == '') {
    //   this.selectedPackage.addOns = []
    // }
    console.log("addon", addon);
    // for (let i = 0; i < this.allAddOns.length; i++) {
    //   this.selectedPackage.addOns.forEach(addon => {
    //     console.log(addon, this.allAddOns[i])
    //     if (addon == this.allAddOns[i].name) {

    //    }
    //   });
    // }
    this.selectedPackage.addOns.forEach(data => {
      if (data.selectedAddOn == addon) {
        this.selectedPackage.totalPrice -= data.price;
      }
    });
    this.selectedPackage.addOns = this.selectedPackage.addOns.filter(data => addon != data.selectedAddOn)


    console.log(this.selectedPackage.addOns);

  }

  savePackageDetails() {
    console.log(postData);
    console.log("this.selectedPackage", this.selectedPackage);
    var id = localStorage.getItem('resellercmpnyID')
    var cmpnyId: any = JSON.parse(localStorage.getItem('editCompany'));
    console.log(cmpnyId);

    this.selectedPackage.clientId = cmpnyId.companyId._id;

    console.log("this.selectedPackage", this.selectedPackage);
    this.invoiceadons = this.selectedPackage.addOns;

    var postData = this.selectedPackage
    delete postData.addOns
    postData['adons'] = this.invoiceadons;


    this.purchasePackageService.purchasePackage(postData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.orderId = res.data.id
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success')
            this.router.navigate(['super-admin/super-admin-dashboard/reseller/' + id])
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error')

        }
      )
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
