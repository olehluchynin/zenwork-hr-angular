import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfBoardingComponent } from './self-boarding.component';

describe('SelfBoardingComponent', () => {
  let component: SelfBoardingComponent;
  let fixture: ComponentFixture<SelfBoardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfBoardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfBoardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
