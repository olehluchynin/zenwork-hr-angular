import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SelfBoardingComponent } from './self-boarding.component';
// import { ClientDetailsModule } from '../super-admin-dashboard/client-details/client-details.module';


const routes: Routes = [
  {
    path: '', component: SelfBoardingComponent, children: [
      { path: 'client-details', loadChildren: '../super-admin-dashboard/client-details/client-details.module#ClientDetailsModule' }
    ]
  }
  // { path: '', redirectTo: 'admin-dashboard', pathMatch: "full" },
  // {
  //   path: 'admin-dashboard', component: AdminDashboardComponent, children: [
  //     { path: 'company-settings', loadChildren: "./company-settings/company-settings.module#CompanySettingsModule" },
  //     { path: 'employee-management', loadChildren: "./employee-management/employee-management.module#EmployeeManagementModule" },
  //     { path: 'leave-management', loadChildren: './leave-management/leave-management.module#LeaveManagementModule' }
  //   ]
  // },

];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SelfBoardingComponent]
})
export class SelfBoardingModule { }
