import { NgModule } from '@angular/core';
import { RouterModule, Routes, } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SharedModule } from './shared-module/shared.module';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './app.interceptor';



const routes: Routes = [
    // { path: '', redirectTo: 'login', pathMatch: "full" },
    { path: 'login', component: LoginComponent },
    { path: 'email-verify/:id', component: EmailVerifyComponent },
    { path: 'admin', loadChildren: "./admin-dashboard/admin-dashboard.module#AdminDashboardModule",canActivate:[AuthGuard] },
    { path: 'main-landing', loadChildren: "./main-landing/main-landing.module#MainLandingModule" },
    { path: 'super-admin', loadChildren: "./super-admin-dashboard/super-admin-dashboard.module#SuperAdminDashboardModule",canActivate:[AuthGuard]},
    { path: '', loadChildren: "./landing-page/landing-page.module#LandingPageModule" },
    { path: '', loadChildren: "./authentication/authentication.module#AuthenticationModule" },
    { path: 'self-on-boarding', loadChildren: "./self-boarding/self-boarding.module#SelfBoardingModule"}
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    providers:[
        AuthService,
        AuthGuard,
      
    ]
})

export class AppRouting { }