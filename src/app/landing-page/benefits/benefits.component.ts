import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.css']
})
export class BenefitsComponent implements OnInit {

  constructor(
    private auth:AuthService

  ) { }

  ngOnInit() {
this.auth.changemessage('false');

  }

  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

}
