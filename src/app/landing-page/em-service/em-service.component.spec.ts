import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmServiceComponent } from './em-service.component';

describe('EmServiceComponent', () => {
  let component: EmServiceComponent;
  let fixture: ComponentFixture<EmServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
