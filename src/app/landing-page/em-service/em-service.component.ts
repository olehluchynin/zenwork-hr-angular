import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-em-service',
  templateUrl: './em-service.component.html',
  styleUrls: ['./em-service.component.css']
})
export class EmServiceComponent implements OnInit {

  constructor(
    private auth:AuthService

  ) { }

  ngOnInit() {
this.auth.changemessage('false');

  }
  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }
}
