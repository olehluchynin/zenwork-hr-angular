import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-integration-api',
  templateUrl: './integration-api.component.html',
  styleUrls: ['./integration-api.component.css']
})
export class IntegrationApiComponent implements OnInit {

  constructor(private auth:AuthService) { }

  ngOnInit() {
this.auth.changemessage('false');

  }

  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

}
