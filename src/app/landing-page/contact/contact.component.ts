import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { SwalAlertService } from '../../services/swalAlert.service';
import { AuthenticationService } from '../../services/authentication.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contact: any = {
    contactName: '',
    companyName: '',
    companySize: '',
    email: '',
    phNo: '',
    message: ''
  }

  isValid: boolean = false;
  constructor(private auth: AuthService,
    public swalAlertService: SwalAlertService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.auth.changemessage("contact");
  }
  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  contactForm() {
    this.isValid = true;
    console.log(this.contact);
    this.authenticationService.contactUsformSend(this.contact)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("COntact Info", res.message, "success")
            // this.isValid = false;
          }
        },
        (err) => {

        })


    this.contact.contactName = '',
      this.contact.companyName = '',
      this.contact.companySize = '',
      this.contact.email = '',
      this.contact.phNo = '',
      this.contact.message = ''
  }

}
