import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

  constructor(private auth:AuthService) { }

  ngOnInit() {
this.auth.changemessage('false');

  }
  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

}
