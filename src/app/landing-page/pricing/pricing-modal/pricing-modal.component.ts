import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pricing-modal',
  templateUrl: './pricing-modal.component.html',
  styleUrls: ['./pricing-modal.component.css']
})
export class PricingModalComponent implements OnInit {
  package = [];
  addons = [];
  employeeCount: any;
  totalEmpCost: any;
  estimatedPrice: number = 0;
  totalAddonsCost: number = 0;
  timeToggle: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<PricingModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private router: Router
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.totalAddonsCost = 0;
    this.estimatedPrice = 0;
    console.log(this.data);


    // this.totalAddonsCost
    // this.addons.forEach(element => {
    //   console.log(element);

    //   this.totalAddonsCost += element.price;
    // });

    console.log(this.totalAddonsCost, 'addonstotalcost');
    this.calculateEmpCost();
  }
  calculateEmpCost() {
    this.package = this.data.package;
    this.addons = this.data.addons;
    this.employeeCount = this.data.employeeCount;
    for (var i = 0; i < this.addons.length; i++) {
      console.log(this.addons[i].price);

      this.totalAddonsCost = this.totalAddonsCost + this.addons[i].price;
      console.log(this.totalAddonsCost);

    }
    this.totalEmpCost = this.package[0].costPerEmployee * this.employeeCount;
    this.estimatedPrice = parseInt(this.package[0].price) + parseInt(this.totalEmpCost) + this.totalAddonsCost;
    console.log(this.estimatedPrice);
  }
  timePeriod(event) {
    console.log(event);
    this.timeToggle = event.checked;


  }
  getStart() {
    this.dialogRef.close();
    
    this.router.navigate(['/sign-up'])
  }

}
