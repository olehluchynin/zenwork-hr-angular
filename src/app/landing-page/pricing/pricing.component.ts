import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PricingModalComponent } from './pricing-modal/pricing-modal.component';
import { PackageAndAddOnService } from '../../services/packageAndAddon.service';
import { AuthService } from '../../auth.service';

declare var jQuery: any;
@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
  @ViewChild('myModal') myModal: ElementRef;
  addOn: any;
  addOns = [];
  packages = [];
  allAddOns = [];
  employeeCount: any;
  key: any;
  isValid: boolean = false;
  public selectedPackage: any = {
    name: '',
    packages: [],
    adons: [],
  }
  validity: boolean = false;
  adonsPrice: number = 0;
  discountEnable: boolean = true;
  dummy: any;
  tempforDiscountChange: any;
  toggleValue: boolean = false;
  constructor(
    public dialog: MatDialog,
    private packageAndAddOnService: PackageAndAddOnService,
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.auth.changemessage("false")
    this.getAllPackages();
    this.getAllAddons()
  }

  getAllPackages() {
    this.packageAndAddOnService.getAllPackages()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.packages = res.data;
          } else {
            // this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        // (err: HttpErrorResponse) => {
        //   console.log('Err', err)
        //   this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        // }
      )
  }


  getAllAddons() {
    this.packageAndAddOnService.getAllAddOns()
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.allAddOns = res.data;
            console.log(this.allAddOns);

            this.allAddOns.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          } else {
            // this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
          }
        },
        // (err: HttpErrorResponse) => {
        //   console.log('Err', err);
        //   this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        // }
      )
    console.log(this.allAddOns);

  }


  getSpecificPackageDetails() {
    // this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
    console.log(this.selectedPackage.name, this.packages, this.allAddOns, this.selectedPackage.adons);
    this.packages.forEach(packageData => {
      if (packageData.name == this.selectedPackage.name) {
        this.selectedPackage.packages = this.packages.filter(data => data.name == this.selectedPackage.name)
        console.log(this.selectedPackage.packages);

        if (this.validity == false && this.selectedPackage.packages[0].subscriptionType == 'Monthly') {

          this.selectedPackage.price = packageData.price;
        }
        if (this.validity == false && (this.selectedPackage.packages[0].subscriptionType == "Annual" || this.selectedPackage.packages[0].subscriptionType == "Yearly")) {
          this.selectedPackage.price = packageData.price / 12;
        }
        if (this.validity == true && this.selectedPackage.packages[0].subscriptionType == "Monthly") {
          this.selectedPackage.price = (packageData.price * 12);
        }
        if (this.validity == true && (this.selectedPackage.packages[0].subscriptionType == "Annual" || this.selectedPackage.packages[0].subscriptionType == "Yearly")) {
          this.selectedPackage.price = packageData.price;
        }
        if (this.validity == false)
          this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
      }
    });


    // for (var i = 0; i < this.allAddOns.length; i++) {
    //   console.log(this.allAddOns);

    //   for (var j = 0; j < this.selectedPackage.adons.length; j++) {
    //     console.log(this.selectedPackage.adons[j]);

    //     if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn) {
    //       // if (this.validity == false) {
    //       if (this.validity == false && this.selectedPackage.adons[j].subscriptionType == 'monthly') {
    //         this.getAllAddons()
    //         this.adonsPrice = 0;
    //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

    //           this.selectedPackage.adons[j].price = this.allAddOns[i].price;
    //       }
    //       if (this.validity == false && this.selectedPackage.adons[j].subscriptionType == "yearly") {
    //         console.log("data come in");

    //         this.getAllAddons()
    //         this.adonsPrice = 0;
    //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

    //           this.selectedPackage.adons[j].price = this.allAddOns[i].price / 12;
    //       }
    //       if (this.validity == true && this.selectedPackage.adons[j].subscriptionType == "monthly") {
    //         this.getAllAddons()
    //         this.adonsPrice = 0;
    //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

    //           this.selectedPackage.adons[j].price = this.allAddOns[i].price * 12;
    //       }
    //       if (this.validity == true && this.selectedPackage.adons[j].subscriptionType == "yearly") {
    //         this.getAllAddons()
    //         this.adonsPrice = 0;
    //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)

    //           this.selectedPackage.adons[j].price = this.allAddOns[i].price;
    //       }
    //       // else {

    //       //   this.selectedPackage.adons[j].price = this.selectedPackage.adons[j].price * 12;
    //       // }
    //       // this.selectedPackage.costPerEmployee = this.allAddOns[i].costPerEmployee;
    //     }
    //   }
    // }

    //   this.allAddOns.forEach(element => {
    //   this.selectedPackage.adons.forEach(selectAdonsData => {
    //     if (element.name == selectAdonsData.selectedAddOn) {
    //       if (this.validity == false) {
    //         this.selectedPackage.adons = selectAdonsData.price;
    //       }
    //       else {
    //         this.selectedPackage.adons = (selectAdonsData.price * 12);
    //         // selectAdonsData.price

    //       }
    //     }
    //   });

    // });


    // this.adonsPrice = 0;
    // this.selectedPackage.adons.forEach(element => {
    //   console.log(element);


    //   this.adonsPrice = this.adonsPrice + element.price
    // });
    // console.log(this.adonsPrice);


    // this.selectedPackage.totalPrice = this.selectedPackage.price + this.adonsPrice + this.selectedPackage.costPerEmployee * this.employeeCount;
    // if (this.basePriceDiscount > 0) {
    //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
    // }
    // this.discountEnable = false;
    /* this.selectedPackageData = packageData; */
    /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
      .subscribe(
        (res: any) => {
          console.log("Response", res);
          if (res.status) {
            this.selectedPackageData = res.data;
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err)
          this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        }
      ) */
    // this.dummy = this.selectedPackage.totalPrice;
    // this.tempforDiscountChange = this.selectedPackage.totalPrice;
  }



  arrowDown(el: HTMLElement) {
    el.scrollIntoView();

  }
  selectAddOn(event) {

    console.log(event);
    this.addOns = event;
    console.log(this.addOns);
    this.allAddOns.forEach(element => {
      if (element == event) {
        // this.allAddOns.splice(
        element.isChecked = true;
      }
    })
    console.log( this.allAddOns);
    
  }
  removeAddOns(addon) {
    console.log(addon, this.allAddOns);

    for (var i = 0; i < this.addOns.length; i++) {
      if (this.addOns[i].name === addon) {
        this.addOns.splice(i, 1);
      }
    }
    for (var i = 0; i < this.allAddOns.length; i++) {
      if (this.allAddOns[i].name == addon) {
        console.log(this.allAddOns[i].name, addon);

        this.allAddOns[i].isChecked = false;
      }
    }


  }
  openPricingModal(): void {
    this.isValid = true;
    if (this.selectedPackage.name && this.employeeCount && this.addOn) {
      jQuery("#myModal").modal('hide');
      const dialogRef = this.dialog.open(PricingModalComponent, {
        width: '700px',
        backdropClass: 'structure-model',
        data: {
          package: this.selectedPackage.packages,
          addons: this.addOns,
          employeeCount: this.employeeCount
        },

      });

      dialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed', result);
        // this.myModal.nativeElement.click();
        jQuery("#myModal").modal('show');


      });
    }

    else {
      console.log("data come");
      // event.preventDefault();
      // jQuery("#myModal").modal({"backdrop": "static"});
      // 
    }

  }


  packCalculation(event) {
    console.log(event);
    this.toggleValue = event.checked;

  }


}
