import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-leave-management',
  templateUrl: './leave-management.component.html',
  styleUrls: ['./leave-management.component.css']
})
export class LeaveManagementComponent implements OnInit {

  constructor(private auth:AuthService) { }

  ngOnInit() {
this.auth.changemessage('false');

  }
  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

}
