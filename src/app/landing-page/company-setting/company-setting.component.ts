import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-company-setting',
  templateUrl: './company-setting.component.html',
  styleUrls: ['./company-setting.component.css']
})
export class CompanySettingComponent implements OnInit {

  constructor(
    private auth:AuthService

  ) { }

  ngOnInit() {
this.auth.changemessage('false');

  }

  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

}
