import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;   // not required



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {

    $(".navbar-toggle").on("click", function () {
      $(this).toggleClass("active");
    });

  }
  signUp() {
    localStorage.clear();
    this.router.navigate(["/sign-up"])
  }

}
