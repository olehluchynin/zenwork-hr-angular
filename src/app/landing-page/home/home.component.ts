import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  videoURL:any;
  constructor(
    private auth:AuthService
    // public Environment:environment
  ) { }

  ngOnInit() {
this.auth.changemessage('false');

    // environment.url
   this.videoURL = environment.url+'/api/client/videos/zenwork_demo.mp4'
  //  =http://environment:3003/videos/zenwork_demo.mp4"
  }

  registerTop() {
    window.scroll(0, 0);
  }

  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }
}
