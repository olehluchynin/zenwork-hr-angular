import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../auth.service';


@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent implements OnInit {

  constructor(private partnerService: AuthenticationService,private auth:AuthService) { }

  public partnerForm: FormGroup;
  toggleValue: boolean = false;

  ngOnInit() {
this.auth.changemessage('false');
    this.partnerForm = new FormGroup({
      name: new FormControl("", Validators.required),
      employeCount: new FormControl("", Validators.required),
      primaryContact: new FormGroup({
        name: new FormControl("", Validators.required),
        phone: new FormControl("", Validators.required),
      }),

      email: new FormControl("", [Validators.required, Validators.email]),

    })

  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }



  partnerSubmit() {
    console.log("Become a Partner", this.partnerForm.value)
  }


  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

  packCalculation(event) {
    console.log(event);
    this.toggleValue = event.checked;

  }

}
