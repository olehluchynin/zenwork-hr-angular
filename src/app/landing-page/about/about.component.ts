import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(
    private auth:AuthService

  ) { }

  ngOnInit() {
this.auth.changemessage('false');

  }

  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }
}
