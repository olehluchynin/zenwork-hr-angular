import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit ,OnDestroy{
  url:any;
  footerRestrict:any;
  
  constructor(private router:Router,private auth:AuthService) {
     console.log(this.router.events.pipe());
     this.auth.currentMessage.subscribe(data=>{
      this.footerRestrict=data;
      console.log(this.footerRestrict);
      
    })
   }

  ngOnInit() {
    var routeurl = this.router.url.split('/')
    console.log(routeurl[0] == 'contact');
    this.footerRestrict = !(routeurl[0] == 'contact')

    // this.router.events.subscribe((url: any) => console.log(url));
    // console.log(this.router.url);
    // this.url = this.router.url;
    // if (this.url == '/contact') {
    //   this.footerRestrict = true;
    // }
    // else{
    //   this.footerRestrict = false;
    // }
    console.log(this.footerRestrict);
   
  }


  about(){
    window.scroll(0,0);
  }

  ngOnDestroy(){
    this.footerRestrict="sum";
    console.log(this.footerRestrict);
    
  }
}
