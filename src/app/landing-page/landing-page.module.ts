import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page.component';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { PartnerComponent } from './partner/partner.component';
import { BenefitsComponent } from './benefits/benefits.component';
import { LeaveManagementComponent } from './leave-management/leave-management.component';
import { RecruitmentsComponent } from './recruitments/recruitments.component';
import { CompanySettingComponent } from './company-setting/company-setting.component';
import { EmServiceComponent } from './em-service/em-service.component';
import { SecurityComponent } from './security/security.component';
import { IntegrationApiComponent } from './integration-api/integration-api.component';
import {MatSelectModule} from '@angular/material/select';
import { PricingModalComponent } from './pricing/pricing-modal/pricing-modal.component';
import { MaterialModuleModule } from '../material-module/material-module.module';


const router: Routes = [
  
  {path: '', component: LandingPageComponent, children: [
     { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'pricing', component: PricingComponent },
      { path: 'about', component: AboutComponent},
      { path: 'contact', component: ContactComponent },
      {path:'partner', component:PartnerComponent},
      {path:'benefits', component:BenefitsComponent},
      {path:'leave-management', component:LeaveManagementComponent},
      {path:'recruitment', component:RecruitmentsComponent},
      {path:'company-setting', component:CompanySettingComponent},
      {path:'em-service', component:EmServiceComponent},
      {path:'security', component:SecurityComponent},
      {path:'api-integration', component:IntegrationApiComponent},
      
      
    ]
  }
]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MaterialModuleModule,
    RouterModule.forChild(router),MatSelectModule
  ],
  declarations: [
    LandingPageComponent,
    NavbarComponent,
    HomeComponent,
    PricingComponent,
    FooterComponent,
    AboutComponent,
    ContactComponent,
    PartnerComponent,
    BenefitsComponent,
    LeaveManagementComponent,
    RecruitmentsComponent,
    CompanySettingComponent,
    EmServiceComponent,
    SecurityComponent,
    IntegrationApiComponent,
    PricingModalComponent
    ],
    entryComponents: [
      PricingModalComponent
    ]

})
export class LandingPageModule { }
