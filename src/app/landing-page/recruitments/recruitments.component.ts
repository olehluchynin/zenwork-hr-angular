import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-recruitments',
  templateUrl: './recruitments.component.html',
  styleUrls: ['./recruitments.component.css']
})
export class RecruitmentsComponent implements OnInit {

  constructor(private auth:AuthService) { }

  ngOnInit() {
this.auth.changemessage('false');

  }
  arrowDown(el: HTMLElement) {
    el.scrollIntoView();
  }

}
