import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";

import { AuthService } from "./auth.service";
import { Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/do';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService, private router: Router,) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    /* if (request.headers.get('reqType') == 'multipart/form-data') {
      console.log("multi")
      request = request.clone({
        setHeaders: {
          token: `${this.auth.getToken()}`,
          reqType:undefined
        }
      });
      console.log("fbdhfbhdfbdh",request);
      
    } else {
      console.log("single")
      request = request.clone({
        setHeaders: {
          token: `${this.auth.getToken()}` ,
          "Content-Type": "application/json" 
          
        }
      });
    } */
    request = request.clone({
      setHeaders: {
        token: `${this.auth.getToken()}` 
        
      }
    });
    console.log("sdcsdc", request);
    // return next.handle(request);
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401 || err.status === 403) {
          localStorage.clear()
            this.router.navigate(['login']);
        }
      }
    });
  }
}
