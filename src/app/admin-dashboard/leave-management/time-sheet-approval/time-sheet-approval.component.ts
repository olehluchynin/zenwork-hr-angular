import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { AccessLocalStorageService } from '../../../services/accessLocalStorage.service';
import { CompanySettingsService } from '../../../services/companySettings.service';
import { MyInfoService } from '../../../services/my-info.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { AmazingTimePickerService } from 'amazing-time-picker';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ReviewHistoricalTimesheetComponent } from './review-historical-timesheet/review-historical-timesheet.component';
declare var $: any;



@Component({
  selector: 'app-time-sheet-approval',
  templateUrl: './time-sheet-approval.component.html',
  styleUrls: ['./time-sheet-approval.component.css']
})
export class TimeSheetApprovalComponent implements OnInit {
  public userId:any;
  public getTimeSheetList = [];
  public getTimeSheetSummaryList = [];
  category:any;
  defaultValues = [];
  customValues = [];
  allValues = [];
  typeofselectCategory: any;
  public pageNo: any;
  public perPage: any;
  subCategoryName:any;
  searchForCategory:any;
  getAllStructureFields:any;
  customPayFrequency = [];
  defauluPayFrequency = [];
  allPayFrequency = [];
  companyId:any;
  selectPayFrequecy:any;
  payCycleForTimeSheet:any;
  payCycleDate:any;
  count:any;
  total:any;
  selectedTimeSheets = [];
  getTimeSheetDetails = [];
  punchArray = [];
  addedBy:any;
  selectedTimeSheetsDetails = [];
  punchDateTime:any;
  punchInTime:any;
  punchOutTime:any;
  dayOfWeek:any;
  missingPunch:any = {
    punchDate:'',
    punchIn:'',
    punchOut:''
  }
  searchEmployeeList = [];
  searchString:any;
  totalLength:any;
  searchEmployeeApprovalList = [];
  selectedCategoryName:any;
  checkedDisable:boolean = true;
  missingPunches = [];
  selectedTimeSheetMaintain = [];
  selectedEmployeeManagerSheetApproval = [];
  selectedMissingPunchesReview = [];
  selectedTimesheetSummaryReview =[];
  payCycleStartDate:any;
  payCycleEndDate:any;
  employeeName:any;
  startDate:any;
  endDate:any;
  selectSearchCategory:any;
  approvalId:any;
  approvalPayCycle:any;
  summaryId:any;
  summaryPayCycle:any;
  requestId:any;
  requestPayCycle:any;
  missingPunchId:any;
  timeSheetMaintainId:any;
  getHistoricalTimeSheetData:any;

  public selectDates = ["Current Cycle", "Previous Cycle", "Next Cycle"]
  @ViewChild('openEdit') openEdit: ElementRef;
  @ViewChild('openEdit1') openEdit1: ElementRef;
  @ViewChild('openEdit2') openEdit2: ElementRef;
  @ViewChild('openEdit3') openEdit3: ElementRef;
  @ViewChild('openEdit4') openEdit4: ElementRef;

  constructor(
    private router:Router,
    private leaveManagementService: LeaveManagementService,
    private accessLocalStorageService: AccessLocalStorageService,
    private companySettingsService: CompanySettingsService,
    private myInfoService: MyInfoService,
    private swalAlertService: SwalAlertService,
    private atp: AmazingTimePickerService,
    public dialog: MatDialog
  ) { }

 

  ngOnInit() {
    console.log(this.missingPunch.punchIn);
    this.pageNo = 1;
    this.perPage = 10;
    this.companyId = this.accessLocalStorageService.get('companyId');
    this.userId = this.accessLocalStorageService.get('employeeId');
    this.addedBy = this.accessLocalStorageService.get('addedBy');
    console.log(this.userId);
    this.typeofselectCategory = "AllEmployees";
    this.searchForCategory = "AllEmployees";
    this.selectedCategoryName = "AllEmployees"
    this.getStandardAndCustomStructureFields();
    this.getTimeSheetApprovals();
    // this.getMissingPunches();
    this.getTimeSheetSummary();
    this.searchEmployeInTimesheetMaintenance();
    this.searchEmployeeAndManagerApproval();
  }
 
  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getTimeSheetSummary();
    this.getTimeSheetApprovals();
    this.searchEmployeInTimesheetMaintenance();

  }

  searchByCategory(event){
    console.log(event);
    this.searchForCategory = event;
    this.getTimeSheetApprovals();
    
  }

  selectPayGroup(event){
    console.log(event);
    this.selectPayFrequecy = event;
    this.getTimeSheetApprovals();
    
  }

  payCycleNumber(number){
    console.log(number);
    this.payCycleForTimeSheet = number;
    this.getTimeSheetApprovals();
    
  }

    // Author:Saiprakash, Date:3/07/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields(){
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res:any)=>{
        console.log(res); 
        this.getAllStructureFields = res.data;
        // this.customPayFrequency = this.getAllStructureFields['Pay Frequency'].custom;
        // this.defauluPayFrequency = this.getAllStructureFields['Pay Frequency'].default;
        // this.allPayFrequency = this.customPayFrequency.concat(this.defauluPayFrequency);
 
      }
    )
  }

  // Author: Saiprakash G, Date: 01/07/19,
  // Description: Get time sheet approval requests

  getTimeSheetApprovals(){
    var data:any = {
      employeeId:this.userId,
	    selectedCategory: this.searchForCategory,
   	  selectedPayGroup: this.selectPayFrequecy,
	    selectedPayCycle: this.payCycleForTimeSheet,
 	    perPageCount:this.perPage,
    	currentPageNum:this.pageNo - 1
    }
    this.leaveManagementService.getTimeSheetApprovals(data).subscribe((res:any)=>{
      console.log(res);
      this.getTimeSheetList = res.data.timesheetReqList;
      this.count = res.data.totalCount;
      // this.payCycleDate = res.data[0]
      
    },(err)=>{
      console.log(err);
      
    })

  }
   // Author: Saiprakash G, Date: 02/07/19,
  // Description: Get time sheet details(Review Timesheet)

  reviewTimeSheet(){

    this.selectedTimeSheets = this.getTimeSheetList.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedTimeSheets);
   
    if (this.selectedTimeSheets.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed","", 'error')
    } else if (this.selectedTimeSheets.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
      this.openEdit.nativeElement.click();
      this.leaveManagementService.getTimeSheetDetails(this.selectedTimeSheets[0].pay_cycle, this.selectedTimeSheets[0].userId).subscribe((res:any)=>{
        console.log(res);
        this.getTimeSheetDetails = res.data;
        // this.payCycleStartDate = res.data[0].pay_cycle_start;
        // this.payCycleEndDate = res.data[0].pay_cycle_end;
        // this.employeeName = res.data[0].employee_fullname;
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    }

  }

   // Author: Saiprakash G, Date: 04/07/19,
  // Description: Add missing punches

  addMissingPunches(){
    this.punchArray.push({
      name: this.employeeName,
      day: this.dayOfWeek,
      punchTime: '',
      punchType:'IN'
    },
   
    )

  }
   // Author: Saiprakash G, Date: 05/07/19
    // Punch Time and Date values

  onChangeDateValue(date){
    console.log(date);
    var t = new Date(date)
    console.log(t);
    var day = t.getDay();
    console.log(day);
    const allDays =  ["Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    if(day == 0){
      this.dayOfWeek = allDays[0];
    } else if(day == 1){
      this.dayOfWeek = allDays[1];
    } else if(day == 2){
      this.dayOfWeek = allDays[2];
    } else if(day == 3){
      this.dayOfWeek = allDays[3];
    } else if(day == 4){
      this.dayOfWeek = allDays[4];
    } else if(day == 5){
      this.dayOfWeek = allDays[5];
    } else if(day == 6){
      this.dayOfWeek = allDays[6];
    } 
    console.log(this.dayOfWeek);
    
    this.punchDateTime = t.toISOString();
    console.log(this.punchDateTime);
    console.log(this.missingPunch.punchIn);
    console.log(this.missingPunch.punchOut);
    
    var timeInSplit = this.missingPunch.punchIn.split(":");
    var timeOutSplit = this.missingPunch.punchOut.split(":");
    console.log(timeInSplit[0],timeInSplit[1]);
    let timeInHours = timeInSplit[0];
    let timeInMinutes = timeInSplit[1];
    let timeOutHours = timeOutSplit[0];
    let timeOutMinutes = timeOutSplit[1];
    
    this.punchInTime = new Date(t.setHours(timeInHours,timeInMinutes));
    this.punchOutTime = new Date(t.setHours(timeOutHours,timeOutMinutes));

    console.log(this.punchInTime);
    console.log(this.punchOutTime);
 
    
  }
     // Author: Saiprakash G, Date: 17/06/19
    // Add Time picker

    open(event) {
      console.log(event);
      
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        console.log(time);
        // this.missingPunch.punchIn = time;
        

      });
    }
  // Author: Saiprakash G, Date: 04/07/19,
  // Description: Save missing punch
  saveMissingPunch(){

    if(this.requestId){
      var data1: any = {
        addedBy: this.userId,
        userId: this.requestId,
        companyId: this.companyId,
        punchArray: [ {
         day: this.dayOfWeek,
         punchTime: this.punchInTime,
         punchType: "IN"
     },
     {
         day: this.dayOfWeek,
         punchTime: this.punchOutTime,
         punchType: "OUT"
     },
   ]
 
      }
      this.leaveManagementService.addMissingPunches(data1).subscribe((res:any)=>{
        console.log(res);
        this.reviewTimeSheet();
        this.punchArray = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
 
    }

  else if(this.missingPunchId){
      var data2: any = {
        addedBy: this.userId,
        userId: this.missingPunchId,
        companyId: this.companyId,
        punchArray: [ {
         day: this.dayOfWeek,
         punchTime: this.punchInTime,
         punchType: "IN"
     },
     {
         day: this.dayOfWeek,
         punchTime: this.punchOutTime,
         punchType: "OUT"
     },
   ]
 
      }
      this.leaveManagementService.addMissingPunches(data2).subscribe((res:any)=>{
        console.log(res);
        this.reviewTimeSheetForMissingPunches();
        this.punchArray = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success"); 
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
 
    }
  else if(this.summaryId){
      var data3: any = {
        addedBy: this.userId,
        userId: this.summaryId,
        companyId: this.companyId,
        punchArray: [ {
         day: this.dayOfWeek,
         punchTime: this.punchInTime,
         punchType: "IN"
     },
     {
         day: this.dayOfWeek,
         punchTime: this.punchOutTime,
         punchType: "OUT"
     },
   ]
 
      }
      this.leaveManagementService.addMissingPunches(data3).subscribe((res:any)=>{
        console.log(res);
        this.reviewTimeSheetForTimesheetSummary();
        this.punchArray = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success"); 
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
 
    }
   else if(this.timeSheetMaintainId){
      var data4: any = {
        addedBy: this.userId,
        userId: this.timeSheetMaintainId,
        companyId: this.companyId,
        punchArray: [ {
         day: this.dayOfWeek,
         punchTime: this.punchInTime,
         punchType: "IN"
     },
     {
         day: this.dayOfWeek,
         punchTime: this.punchOutTime,
         punchType: "OUT"
     },
   ]
 
      }
      this.leaveManagementService.addMissingPunches(data4).subscribe((res:any)=>{
        console.log(res);
        this.reviewTimeSheetForTimesheetMaintain();
        this.punchArray = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success"); 
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
 
    }
    else if(this.approvalId){
      var data5: any = {
        addedBy: this.userId,
        userId: this.approvalId,
        companyId: this.companyId,
        punchArray: [ {
         day: this.dayOfWeek,
         punchTime: this.punchInTime,
         punchType: "IN"
     },
     {
         day: this.dayOfWeek,
         punchTime: this.punchOutTime,
         punchType: "OUT"
     },
   ]
 
      }
      this.leaveManagementService.addMissingPunches(data5).subscribe((res:any)=>{
        console.log(res);
        this.reviewTimeSheetForEmployeeManagerApproval();
        this.punchArray = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success"); 
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
 
    }
    
  }
  checkRequestTimeSheet(id,payCycle,name,startDate,endDate){
    console.log(id,payCycle);
    
     this.requestId = id;
     this.requestPayCycle = payCycle;
     this.employeeName = name;
     this.payCycleStartDate = startDate;
     this.payCycleEndDate = endDate
  }

  checkSummaryTimeSheet(id1, payCycle1,name,startDate,endDate){
    console.log(id1,payCycle1);
    
     this.summaryId = id1;
     this.summaryPayCycle = payCycle1;
     this.employeeName = name;
     this.payCycleStartDate = startDate;
     this.payCycleEndDate = endDate
  }
  checkMissingPunches(id){
    this.missingPunchId = id;
  }
  checkTimeSheetMaintain(id){
     this.timeSheetMaintainId = id;
  }

  adminReject(){
    if(this.requestId){
      var data1:any = {
        employeeId: this.requestId,
        payCycle: this.requestPayCycle,
        approveType: "admin",
        isApproved: false,
        comments: "test comment",
        approvedBy: this.userId
      }
      this.leaveManagementService.approveOrRejectTimeSheet(data1).subscribe((res:any)=>{
        console.log(res);
        this.getTimeSheetApprovals();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        $("#myModal").modal("hide");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
     } 
    else if(this.summaryId){
      var data3:any = {
        employeeId: this.summaryId,
        payCycle: this.summaryPayCycle,
        approveType: "admin",
        isApproved: false,
        comments: this.selectedTimesheetSummaryReview[0].timesheetDetails.comments,
        approvedBy: this.userId
      }
      
     this.leaveManagementService.approveOrRejectTimeSheet(data3).subscribe((res:any)=>{
       console.log(res);
       this.getTimeSheetSummary();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");

       $("#myModal").modal("hide");
     }, (err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    } 
    else if(this.missingPunchId){
      var data2:any = {
        employeeId: this.missingPunchId,
        payCycle: "13",
        approveType: "admin",
        isApproved: false,
        comments: "test comment",
        approvedBy: this.userId
      }
      this.leaveManagementService.approveOrRejectTimeSheet(data2).subscribe((res:any)=>{
        console.log(res);
        this.getMissingPunches();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        $("#myModal").modal("hide");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
     } 
 
    else if(this.timeSheetMaintainId){
      var data4:any = {
        employeeId: this.timeSheetMaintainId,
        payCycle: "13",
        approveType: "admin",
        isApproved: false,
        comments: "test comment",
        approvedBy: this.userId
      }
      
     this.leaveManagementService.approveOrRejectTimeSheet(data4).subscribe((res:any)=>{
       console.log(res);
       this.searchEmployeInTimesheetMaintenance();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
       $("#myModal").modal("hide");
     }, (err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    } 
   else if(this.approvalId){
      var data5:any = {
        employeeId: this.approvalId,
        payCycle: this.approvalPayCycle,
        approveType: "admin",
        isApproved: false,
        comments: "test comment",
        approvedBy: this.userId
      }
    
     this.leaveManagementService.approveOrRejectTimeSheet(data5).subscribe((res:any)=>{
       console.log(res);
       this.searchEmployeeAndManagerApproval();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
       $("#myModal").modal("hide");
     }, (err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    }
  }
    // Author: Saiprakash G, Date: 04/07/19,
  // Description: Admin approve or reject timesheet

  adminApproveOrRemove(){
  
    if(this.requestId){
      var data1:any = {
        employeeId: this.requestId,
        payCycle: this.requestPayCycle,
        approveType: "admin",
        isApproved: false,
        comments: "test comment",
        approvedBy: this.userId
      }
      this.leaveManagementService.approveOrRejectTimeSheet(data1).subscribe((res:any)=>{
        console.log(res);
        this.getTimeSheetApprovals();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        $("#myModal").modal("hide");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
     } 
     else if(this.summaryId){
      var data3:any = {
        employeeId: this.summaryId,
        payCycle: this.summaryPayCycle,
        approveType: "admin",
        isApproved: true,
        comments: this.selectedTimesheetSummaryReview[0].timesheetDetails.comments,
        approvedBy: this.userId
      }
      
     this.leaveManagementService.approveOrRejectTimeSheet(data3).subscribe((res:any)=>{
       console.log(res);
       this.getTimeSheetSummary();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");

       $("#myModal").modal("hide");
     }, (err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    } 
     else if(this.missingPunchId){
      var data2:any = {
        employeeId: this.missingPunchId,
        payCycle: "13",
        approveType: "admin",
        isApproved: true,
        comments: "test comment",
        approvedBy: this.userId
      }
      this.leaveManagementService.approveOrRejectTimeSheet(data2).subscribe((res:any)=>{
        console.log(res);
        this.getMissingPunches();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        $("#myModal").modal("hide");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
     } 
 
    else if(this.timeSheetMaintainId){
      var data4:any = {
        employeeId: this.timeSheetMaintainId,
        payCycle: "13",
        approveType: "admin",
        isApproved: true,
        comments: "test comment",
        approvedBy: this.userId
      }
      
     this.leaveManagementService.approveOrRejectTimeSheet(data4).subscribe((res:any)=>{
       console.log(res);
       this.searchEmployeInTimesheetMaintenance();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
       $("#myModal").modal("hide");
     }, (err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    } 
   else if(this.approvalId){
      var data5:any = {
        employeeId: this.approvalId,
        payCycle: this.approvalPayCycle,
        approveType: "admin",
        isApproved: true,
        comments: "test comment",
        approvedBy: this.userId
      }
    
     this.leaveManagementService.approveOrRejectTimeSheet(data5).subscribe((res:any)=>{
       console.log(res);
       this.searchEmployeeAndManagerApproval();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
       $("#myModal").modal("hide");
     }, (err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    }
    
    

  }
    // Author: Saiprakash G, Date: 04/07/19,
  // Description: Delete punch date

  deletePunch(){
    this.selectedTimeSheetsDetails = this.getTimeSheetDetails.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedTimeSheetsDetails);
   
    if (this.selectedTimeSheetsDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
    
      this.leaveManagementService.deletePunch(this.userId,this.selectedTimeSheetsDetails[0].userId,this.selectedTimeSheetsDetails[0].punchDate,this.selectedTimeSheetsDetails[0].pay_cycle).subscribe((res:any)=>{
        console.log(res);
        this.reviewTimeSheet();
        this.reviewTimeSheetForTimesheetSummary();
        this.reviewTimeSheetForMissingPunches();
        this.reviewTimeSheetForTimesheetMaintain();
        this.reviewTimeSheetForEmployeeManagerApproval();

      }, (err)=>{
        console.log(err);
        
      })
    }
  }

  onChangeStartDate(date){
    console.log(date);
    var s = new Date(date);
    this.startDate = s.toISOString().split("T")[0];
    console.log(this.startDate);
    
  }
  onChangeEndDate(date){
    console.log(date);
    var e = new Date(date);
    this.endDate = e.toISOString().split("T")[0];
    console.log(this.endDate);
    this. getMissingPunches();
  }
   // Author: Saiprakash G, Date: 02/07/19,
  // Description: Get missing punches

  getMissingPunches(){
    var data: any = {
      startdate: this.startDate,
      enddate: this.endDate,
      "employeetype": {
          "type": {
              "businesunit": [
                 
              ],
              "location": [
                this.subCategoryName
            ],
            "department": [
              
            ],
            "paygroup": [
             
            ],
            "All": false
        
          }
      }
    }
    this.leaveManagementService.getMissingPunches(data).subscribe((res:any)=>{
      console.log(res);
      this.missingPunches = res.missingpunches;
      
    },(err)=>{
      console.log(err);
      
    })

  }

  reviewTimeSheetForMissingPunches(){
    this.selectedMissingPunchesReview = this.missingPunches.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedMissingPunchesReview);
   
    if (this.selectedMissingPunchesReview.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed","", 'error')
    } else if (this.selectedMissingPunchesReview.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
      this.openEdit3.nativeElement.click();
      this.leaveManagementService.getTimeSheetDetails("25", this.selectedMissingPunchesReview[0].EmployeeDetails._id).subscribe((res:any)=>{
        console.log(res);
        
        this.getTimeSheetDetails = res.data;
        // this.payCycleStartDate = res.data[0].pay_cycle_start;
        // this.payCycleEndDate = res.data[0].pay_cycle_end;
        // this.employeeName = res.data[0].employee_fullname;
      }, (err)=>{
        console.log(err);
        
      })
    }
  }
  // Author: Saiprakash G, Date: 03/07/19,
  // Description: Select Category

  selectCategory(event){
  console.log(event);
  var str = event;
  this.selectSearchCategory = event
  // if(event == "Pay Frequency"){
  //   var payGroupCategory =  str.replace("Pay Frequency", "PayGroup");
  //   console.log(payGroupCategory);
    
  // }
  this.typeofselectCategory = str.replace(/ +/g, "");
  // this.typeofselectCategory = payGroupCategory
  console.log(this.typeofselectCategory);
  
  // this.typeofselectCategory = event;

  this.companySettingsService.getStuctureFields(event,0).subscribe((res:any)=>{
  console.log(res);
  this.defaultValues = res.default;
  this.customValues = res.custom;
  this.allValues = this.defaultValues.concat(this.customValues)
  console.log(this.allValues);
 
 }, err =>{
   console.log(err);
   
 })

  }

  selectSubCategory(name){
     this.subCategoryName = name;
     this.getTimeSheetSummary();
     this.searchEmployeInTimesheetMaintenance();
  }


   // Author: Saiprakash G, Date: 02/07/19,
  // Description: Get time sheet summary

  getTimeSheetSummary(){
    var data: any = {
      selectedCategoryType:this.typeofselectCategory,
      selectedCategory:this.subCategoryName,
      payCycle:"13",
      userId:this.userId,
      perPageCount: this.perPage,
      currentPageNum: this.pageNo - 1
    }

    this.leaveManagementService.getTimeSheetSummary(data).subscribe((res:any)=>{
      console.log(res);
      this.getTimeSheetSummaryList = res.data.attendenceList;
      this.total = res.data.totalCount;
      
    },(err)=>{
      console.log(err);
      
    })

  }
  // Author: Saiprakash G, Date: 09/07/19,
// Description: Get time sheet details(Review Timesheet)

  reviewTimeSheetForTimesheetSummary(){
    this.selectedTimesheetSummaryReview = this.getTimeSheetSummaryList.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedTimesheetSummaryReview);
   
    if (this.selectedTimesheetSummaryReview.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed","", 'error')
    } else if (this.selectedTimesheetSummaryReview.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
      this.openEdit4.nativeElement.click();
      this.leaveManagementService.getTimeSheetDetails(this.selectedTimesheetSummaryReview[0].timesheetDetails.pay_cycle, this.selectedTimesheetSummaryReview[0].timesheetDetails.userId).subscribe((res:any)=>{
        console.log(res);
        
        this.getTimeSheetDetails = res.data;
        // this.payCycleStartDate = res.data[0].pay_cycle_start;
        // this.payCycleEndDate = res.data[0].pay_cycle_end;
        // this.employeeName = res.data[0].employee_fullname;
      }, (err)=>{
        console.log(err);
        
      })
    }
  }
  // Author: Saiprakash G, Date: 05/07/19,
// Description: Search for employee string

  searchEmployee(string){
     console.log(string);
     this.searchString = string;
     this.searchEmployeInTimesheetMaintenance();
     this.searchEmployeeAndManagerApproval();
  }
  // Author: Saiprakash G, Date: 05/07/19,
// Description: Employee timesheet maintenance

  searchEmployeInTimesheetMaintenance(){
    
    var data: any = {
      selectedCategoryType:this.typeofselectCategory,
      selectedCategory:this.subCategoryName,
      searchString: this.searchString,
      userId: this.userId,
      perPageCount: this.perPage,
      currentPageNum: this.pageNo - 1
    }
   this.leaveManagementService.searchEmployeeInTimeSheetMaintenance(data).subscribe((res:any)=>{
     console.log(res);
     this.searchEmployeeList = res.data.searchResult;
     this.totalLength = res.data.totalCount;
     
   },(err)=>{
     console.log(err);
     
   })

  }
// Author: Saiprakash G, Date: 09/07/19,
// Description: Get time sheet details(Review Timesheet)

  reviewTimeSheetForTimesheetMaintain(){
    this.selectedTimeSheetMaintain = this.searchEmployeeList.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedTimeSheetMaintain);
   
    if (this.selectedTimeSheetMaintain.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed","", 'error')
    } else if (this.selectedTimeSheetMaintain.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
      this.openEdit1.nativeElement.click();
      this.leaveManagementService.getTimeSheetDetails("25", this.selectedTimeSheetMaintain[0]._id).subscribe((res:any)=>{
        console.log(res);
        
        this.getTimeSheetDetails = res.data;
        // this.payCycleStartDate = res.data[0].pay_cycle_start;
        // this.payCycleEndDate = res.data[0].pay_cycle_end;
        // this.employeeName = res.data[0].employee_fullname;
      }, (err)=>{
        console.log(err);
        
      })
    }
  }

  reviewHistoricalDialog(): void {
    if(this.getHistoricalTimeSheetData){
      const dialogRef = this.dialog.open(ReviewHistoricalTimesheetComponent, {
        width: '1300px',
        height: '800px',
        data: this.getHistoricalTimeSheetData
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed',result);
        
      });
    }
   
  }

  reviewHistoricalTimesheet(){
    this.selectedTimeSheetMaintain = this.searchEmployeeList.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedTimeSheetMaintain);
    if (this.selectedTimeSheetMaintain.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed","", 'error')
    } else if (this.selectedTimeSheetMaintain.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
     var data:any ={
      userId:"5cd91cc3f308c21c43e5054d",
      timesheetStartDate: "2019-07-21 00:00:00.000Z",
      timesheetEndDate:"2019-07-26 00:00:00.000Z"
     }
      this.leaveManagementService.getHistoricalTimeshhet(data).subscribe((res:any)=>{
        console.log(res);
        
        this.getHistoricalTimeSheetData = res.data;
        this.reviewHistoricalDialog();
      }, (err)=>{
        console.log(err);
        
      })
    }
  }

  selectEmployeeCategory(event){
    console.log(event);
    this.selectedCategoryName = event;
    this.searchEmployeeAndManagerApproval()

  }

// Author: Saiprakash G, Date: 08/07/19,
// Description: Employee and manager approvals

  searchEmployeeAndManagerApproval(){
    var data: any = {
      selectedCategory:this.selectedCategoryName,
      searchString: this.searchString,
      userId: this.userId,
      payCycle: 13,
      perPageCount: this.perPage,
      currentPageNum: this.pageNo - 1
    }
   this.leaveManagementService.searchEmployeeForApproval(data).subscribe((res:any)=>{
     console.log(res);
     this.searchEmployeeApprovalList = res.data.timesheetList;
     this.totalLength = res.data.totalCount;
     
   },(err)=>{
     console.log(err);
     
   })
  }
  // Author: Saiprakash G, Date: 09/07/19,
// Description: Get time sheet details(Review Timesheet)

  reviewTimeSheetForEmployeeManagerApproval(){
    this.selectedEmployeeManagerSheetApproval = this.searchEmployeeApprovalList.filter(timeSheetCheck => {
      return timeSheetCheck.isChecked
    })
   console.log(this.selectedEmployeeManagerSheetApproval);
   
    if (this.selectedEmployeeManagerSheetApproval.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed","", 'error')
    } else if (this.selectedEmployeeManagerSheetApproval.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed","", 'error')
    } else {
      this.openEdit2.nativeElement.click();
      this.leaveManagementService.getTimeSheetDetails(this.selectedEmployeeManagerSheetApproval[0].pay_cycle, this.selectedEmployeeManagerSheetApproval[0].userId).subscribe((res:any)=>{
        console.log(res);
        
        this.getTimeSheetDetails = res.data;
        // this.payCycleStartDate = res.data[0].pay_cycle_start;
        // this.payCycleEndDate = res.data[0].pay_cycle_end;
        // this.employeeName = res.data[0].employee_fullname;
      }, (err)=>{
        console.log(err);
        
      })
    }
  }
  checkApproval(id,payCycle,name,startDate, endDate){
      this.approvalId = id,
      this.approvalPayCycle = payCycle,
      this.employeeName = name;
      this.payCycleStartDate = startDate;
      this.payCycleEndDate = endDate
  }

  employeeMissingApproval(name){
      var data:any = {
        userId: this.approvalId,
        payCycle: this.approvalPayCycle,
        missingApprovalType: name
      }

      this.leaveManagementService.sendMissingApproval(data).subscribe((res:any)=>{
        console.log(res);
        
      },(err)=>{
        console.log(err);
        
      })
  }
  managerMissingApproval(name){
    var data:any = {
      userId: this.approvalId,
      payCycle: this.approvalPayCycle,
      missingApprovalType: name
    }

    this.leaveManagementService.sendMissingApproval(data).subscribe((res:any)=>{
      console.log(res);
      
    },(err)=>{
      console.log(err);
      
    })
  }
  anyOneMissingApproval(){
    var data:any = {
      userId: this.approvalId,
      payCycle: this.approvalPayCycle,
      missingApprovalType: ""
    }

    this.leaveManagementService.sendMissingApproval(data).subscribe((res:any)=>{
      console.log(res);
      
    },(err)=>{
      console.log(err);
      
    })
  }

  previousPage(){
    this.router.navigate(['/admin/admin-dashboard/leave-management']);
  }

}
