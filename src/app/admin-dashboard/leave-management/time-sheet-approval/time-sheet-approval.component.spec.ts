import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeSheetApprovalComponent } from './time-sheet-approval.component';

describe('TimeSheetApprovalComponent', () => {
  let component: TimeSheetApprovalComponent;
  let fixture: ComponentFixture<TimeSheetApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeSheetApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSheetApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
