import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-review-historical-timesheet',
  templateUrl: './review-historical-timesheet.component.html',
  styleUrls: ['./review-historical-timesheet.component.css']
})
export class ReviewHistoricalTimesheetComponent implements OnInit {
  arrivalDetails:any;
  dateWiseAttendanceList:any;
  timeInsideData: any = [
    {
      lateByTime : '',
      hoursworked:'',
      grosshours:'',
      attendanceDetails : [],
      schedules: [
        {
          punchIn:'',
          marginLeft:''
        }
      ]
    }
  ]


  constructor(
    public dialogRef: MatDialogRef<ReviewHistoricalTimesheetComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log(this.data);
    if(this.data.ArrivalDetails && this.data.dateWiseAttendanceList){
      this.arrivalDetails = this.data.ArrivalDetails;
      this.dateWiseAttendanceList = this.data.dateWiseAttendanceList;
      this.timeInsideData = Object.keys(this.data.dateWiseAttendanceList).map(key =>({
        key, value : this.data.dateWiseAttendanceList [key]
      }));

      this.timeInsideData.forEach((element:any) => {
        var totalMinutes=1440;
        element.schedules=[]
        if(element.value.attendanceDetails && element.value.attendanceDetails.length ){
          element.value.attendanceDetails.forEach(times => {
            if(times.punchType==="IN"){
              console.log(element)
              var nt=new Date(times.punchTime);
              var timenow=(nt.getHours()*60) +nt.getMinutes()
  
              element.schedules.push({punchIn:timenow})
            }else if(times.punchType==="OUT"){
              console.log(element)
              var nt=new Date(times.punchTime);
              var timenow=(nt.getHours()*60) +nt.getMinutes();
  
              element.schedules[element.schedules.length-1].punchOut=timenow;
              element.schedules[element.schedules.length-1].marginLeft=(element.schedules[element.schedules.length-1].punchIn/totalMinutes)*100;
              element.schedules[element.schedules.length-1].width=((element.schedules[element.schedules.length-1].punchOut-element.schedules[element.schedules.length-1].punchIn)/totalMinutes)*100;
            }
          });
        }
  
  
      });

      }
 
      
      
    }
   // Author: Suresh M, Date: 24/07/19
  // getToolTimeData
getToolTimeData(data){
  // console.log(data);
  var punchinTime=this.getTimeFromMinutes(data.punchIn);
  var punchoutTime=this.getTimeFromMinutes(data.punchOut);

  // console.log(punchinTime, punchinTime)
return 'Punch in '+ punchinTime +', Punch out '+punchoutTime;
}

// Author: Suresh M, Date: 24/07/19
  // getTimeFromMinutes
getTimeFromMinutes(mins){
  let h = Math.floor(mins / 60);
  let m:any = mins % 60;
  m = m < 10 ? '0' + m : m;

 
  let a = 'am';
  if (h >= 12) a = 'pm';
  if (h > 12) h = h - 12;
  return `${h}:${m} ${a}`;

}


}
