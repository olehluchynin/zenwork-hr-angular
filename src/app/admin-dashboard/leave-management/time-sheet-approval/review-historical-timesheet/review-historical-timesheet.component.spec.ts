import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewHistoricalTimesheetComponent } from './review-historical-timesheet.component';

describe('ReviewHistoricalTimesheetComponent', () => {
  let component: ReviewHistoricalTimesheetComponent;
  let fixture: ComponentFixture<ReviewHistoricalTimesheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewHistoricalTimesheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewHistoricalTimesheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
