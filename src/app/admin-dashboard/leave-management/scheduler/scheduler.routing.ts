import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchedulerComponent } from './scheduler.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { WorkSchedulesModule } from '../work-schedules/work-schedules.module';
WorkSchedulesModule

const routes: Routes = [
    
    {
        path: '', component: SchedulerComponent, children: [
            { path: 'holidays', component: HolidaysComponent },
            { path: 'blackoutdates', loadChildren: "../black-out-dates/black-out-dates.module#BlackOutDatesModule" },
            { path: 'workschedules', loadChildren: "../work-schedules/work-schedules.module#WorkSchedulesModule" }
        ]
    },
]




@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SchedulerRouting { }