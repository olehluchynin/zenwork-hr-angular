import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulerComponent } from './scheduler.component';
import { SchedulerRouting } from './scheduler.routing';
import { HolidaysComponent } from './holidays/holidays.component';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateHolidayComponent } from './holidays/create-holiday/create-holiday.component';


@NgModule({
  imports: [
    CommonModule,
    SchedulerRouting,
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [SchedulerComponent, HolidaysComponent, CreateHolidayComponent],
  entryComponents: [CreateHolidayComponent]
})
export class SchedulerModule { }
