import { Component, OnInit } from '@angular/core';
import { LeaveManagementService } from '../../../../services/leaveManagement.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { CreateHolidayComponent } from './create-holiday/create-holiday.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SwalAlertService } from '../../../../services/swalAlert.service';


@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.css']
})
export class HolidaysComponent implements OnInit {
  public companyId: any;
  public holidayList = [];
  public holidayDetailsList = [];
  public customLocation = [];
  public defaultLocation = [];
  public allLocation = [];
  public getAllStructureFields:any;
  public disableField:boolean = true;
  public selectedHolidays = [];
  public getSingleHoliday:any;
  public locationId:any;
  public selectedDeleteHolidays = [];
  public loadHolidaysList = [];
  year:any;
  name:any;
  date:any;
  holidayId:any;
  loadHolidayIndex:any;
  checkHolidaysIds = [];

  constructor(
    private leaveManagementService: LeaveManagementService,
    private myInfoService : MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    public dialog: MatDialog,
    private swalAlertService: SwalAlertService,
  ) { }

  ngOnInit() {

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.getStandardAndCustomStructureFields();
    var d = new Date();
    this.year = d.getFullYear();
    this.getHolidaysList();
  }

  // Author: Saiprakash G, Date: 13/06/19
  // Get standard and custom structure fields

  getStandardAndCustomStructureFields(){
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res:any)=>{
        console.log(res); 
        this.getAllStructureFields = res.data;
        this.customLocation = this.getAllStructureFields['Location'].custom;
        this.defaultLocation = this.getAllStructureFields['Location'].default;
        this.allLocation = this.customLocation.concat(this.allLocation);
 
      }
    )
  }
  yearFilter(year){
    this.year = year;
    this.getHolidaysList()
     
  }
  // Author: Saiprakash G, Date: 13/06/19
  // Open holiday model

  openHolidayModel(): void{
    if(this.getSingleHoliday && this.holidayId){
      const dialogRef = this.dialog.open(CreateHolidayComponent, {
        width: '1300px',
        backdropClass: 'structure-model',
        data: this.getSingleHoliday, 
             
      });
  
      dialogRef.afterClosed().subscribe((result:any) => {
        console.log('The dialog was closed', result);
        this.getHolidaysList();
        this.getSingleHoliday = "";
        this.checkHolidaysIds = [];
       
      });
    }
    else {
      var loadHolidaysData : any = {
        year : this.year,
        name: this.name,
        date: this.date

      }
      const dialogRef = this.dialog.open(CreateHolidayComponent, {
        width: '1300px',
        backdropClass: 'structure-model',
        data: loadHolidaysData, 
             
      });
  
      dialogRef.afterClosed().subscribe((result:any) => {
        console.log('The dialog was closed', result);
        this.getHolidaysList();

        // this.loadHolidays();
       
      });
    }
  
   
  }
// Author: Saiprakash G, Date: 02/07/19
// Load holidays

loadHolidays(){
  this.leaveManagementService.loadHolidays(this.year).subscribe((res:any)=>{
    console.log(res);
    this.loadHolidaysList = res.result;
    
  },(err)=>{
    console.log(err);
    
  })

}

loadHolidayData(name, date, index){
  console.log(name, date, index);
  this.loadHolidayIndex = index
    this.name = name;
    this.date = date;
}

  // Author: Saiprakash G, Date: 13/06/19
  // Get holidays list

  getHolidaysList(){
   
    this.leaveManagementService.getHolidayList(this.year).subscribe((res:any)=>{
      console.log(res); 
      this.holidayList = res.data;
      
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 13/06/19
  // Checked particular holidays
  checkedHolidays(event,id){
    console.log(event,id);
    this.holidayId = id;
    if(event.checked == true){
      this.checkHolidaysIds.push(id)
    }
    console.log(this.checkHolidaysIds);
    if(event.checked == false){
      var index = this.checkHolidaysIds.indexOf(id);
        if (index > -1) {
          this.checkHolidaysIds.splice(index, 1);
        }
        console.log(this.checkHolidaysIds);
    }

    // for (var i = 0; i < this.holidayDetailsList.length; i++) {
    //   if (this.holidayDetailsList[i]._id == id) {
    //     this.holidayDetailsList[i].show = event.checked;
    //     if (event.checked == true) {
    //       this.selectedHolidays.push(this.holidayDetailsList[i]._id)
    //       console.log('222222', this.selectedHolidays);
    //     }
    //     if (event.checked == false) {
    //       for (var j = 0; j < this.selectedHolidays.length; j++) {
    //         if (this.selectedHolidays[j] === id) {
    //           this.selectedHolidays.splice(j, 1);
    //           console.log(this.selectedHolidays, "155555555");
    //         }
    //       }
    //     }

    //   }
    // }

  }
  // Author: Saiprakash G, Date: 14/06/19
  // Edit holidays

  editHoliday(){
    this.selectedHolidays = this.holidayList.filter(holidayCheck => {
      return holidayCheck.isChecked
    })

    if (this.selectedHolidays.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one holiday to proceed","", 'error')
    } else if (this.selectedHolidays.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one holiday to proceed","", 'error')
    } else {
      
      this.leaveManagementService.getHoliday(this.selectedHolidays[0]._id).subscribe((res:any)=>{
        console.log(res);  
        this.getSingleHoliday = res.data;
        this.openHolidayModel();
      }, (err)=>{
        console.log(err); 
        
      })
    }
  }

  // Author: Saiprakash G, Date: 14/06/19
  // Delete holidays

  deleteHolidays(){
    this.selectedHolidays = this.holidayList.filter(holidayCheck => {
      return holidayCheck.isChecked
    })

    if (this.selectedHolidays.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select holidays to proceed","", 'error')
    } else {

      var data: any = {
        _ids : this.selectedHolidays
      
      }
     
      this.leaveManagementService.deleteHoliday(data).subscribe((res:any)=>{
        console.log(res);
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        this.getHolidaysList();
        this.checkHolidaysIds = [];
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
      

    }
  }

}
