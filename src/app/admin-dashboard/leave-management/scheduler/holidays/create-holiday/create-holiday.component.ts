import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MyInfoService } from '../../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
import { LeaveManagementService } from '../../../../../services/leaveManagement.service';

@Component({
  selector: 'app-create-holiday',
  templateUrl: './create-holiday.component.html',
  styleUrls: ['./create-holiday.component.css']
})
export class CreateHolidayComponent implements OnInit {

  public holiday : any = {
    location_id:'',
    name:'',
    date:''
  }
  public companyId:any;
  public customLocation = [];
  public defaultLocation = [];
  public allLocation = [];
  public getAllStructureFields:any;
  onlyDate:any;
  onlyYear:any;
  selectedHolidays = [];
  holidayCheck:any;
  selectedLocationIds:any;
  

  constructor(
    private myInfoService : MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private leaveManagementService: LeaveManagementService,
    private swalAlertService : SwalAlertService,
    public dialogRef: MatDialogRef<CreateHolidayComponent>,
    @Inject(MAT_DIALOG_DATA) public data 
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
    
  }

  ngOnInit() {
    console.log(this.data);
    if(this.data){
      this.holiday.name = this.data.name;
      this.holiday.date = this.data.date;
    }
    if(this.data && this.data.location_id){
      this.holiday.location_id = this.data.location_id._id;
      this.holiday.name = this.data.holiday_name;
      this.holiday.date = this.data.holiday_date;
    }
    
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.getStandardAndCustomStructureFields();
  }

  // Author: Saiprakash G, Date: 12/06/19
  // Get standard and custom structure fields

  getStandardAndCustomStructureFields(){
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res:any)=>{
        console.log(res); 
        this.getAllStructureFields = res.data;
        this.customLocation = this.getAllStructureFields['Location'].custom;
        this.defaultLocation = this.getAllStructureFields['Location'].default;
        this.allLocation = this.customLocation.concat(this.allLocation);
 
      }
    )
  }

  onChangeDatevalue(date){
    console.log(date);
    var s = new Date(date);
    this.onlyDate = s.toISOString().split("T")[0];
    console.log(this.onlyDate);
    var yearOfDate = this.onlyDate.split("-");
    console.log(yearOfDate[0]);
    this.onlyYear = yearOfDate[0];
    
  }

  // Author: Saiprakash G, Date: 12/06/19
  // Create holidays
  
  createHolidays(){
    var data1:any = {
      companyId : this.companyId,
      location_ids : this.selectedLocationIds,
      year: this.data.year,
      holiday_name : this.holiday.name,
      holiday_date : this.holiday.date
    }
    if(this.data._id){
      data1._id = this.data._id;
      data1.location_id = this.holiday.location_id
      this.leaveManagementService.updateHoliday(data1).subscribe((res:any)=>{
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        this.data._id= "";
        this.data.location_id._id = "";
        this.data.holiday_name = "";
        this.data.holiday_date = "";
        this.holiday = "";
        // this.holiday.location_id = null;
        // this.holiday.name = null;
        // this.holiday.date = null;
        this.dialogRef.close(res);
        console.log(this.holiday);
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        
      })
    }
    else {
      this.leaveManagementService.createHoliday(data1).subscribe((res:any)=>{
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        this.holiday.location_id = null;
        this.holiday.name = null;
        this.holiday.date = null;
        this.dialogRef.close(res);
        console.log(this.holiday);
        
        
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        
      })
    }
  
  }

  selectAll(e?){
    console.log("all",e);
    this.selectedLocationIds = e;
    
    // this.selectedHolidays = [];
    // this.allLocation.forEach(element => {
    //   element.isChecked = this.holiday.location_id;
    // });
    // await this.allLocation.forEach(element => {

    //   if (element.isChecked) {
    //     this.selectedHolidays.push(element._id);
       
    //   }
    // });
    // console.log(this.selectedHolidays);
  }

  async selectHolidays(data){
   console.log(data);
   
    this.selectedHolidays = [];
    if (data) {
      this.holidayCheck = 0;
      this.allLocation.forEach(element => {

        if (!element.isChecked) {
          this.holidayCheck = 1;
        }

      });

      if (!this.holidayCheck) {
        this.holiday.location_id = true;
      }
    }
    else {
      this.holiday.location_id = false;
    }

    await this.allLocation.forEach(element => {

      if (element.isChecked) {
        this.selectedHolidays.push(element._id);
      }
    });
    console.log(this.selectedHolidays);

  }

  cancel(){
    this.dialogRef.close();
    // this.data._id= "";
    // this.data.location_id._id = "";
    // this.data.holiday_name = "";
    // this.data.holiday_date = "";
    // this.holiday.location_id = null;
    // this.holiday.name = null;
    // this.holiday.date = null;
  }
}
