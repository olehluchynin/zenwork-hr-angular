import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackOutDatesComponent } from './black-out-dates.component';

describe('BlackOutDatesComponent', () => {
  let component: BlackOutDatesComponent;
  let fixture: ComponentFixture<BlackOutDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackOutDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackOutDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
