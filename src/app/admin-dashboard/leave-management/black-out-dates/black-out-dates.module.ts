import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlackOutDatesRouting } from './black-out-dates.routing';
import { BlackOutDatesComponent } from './black-out-dates.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { CompanySettingsService } from '../../../services/companySettings.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularDualListBoxModule } from 'angular-dual-listbox';


@NgModule({
  imports: [
    CommonModule,
    BlackOutDatesRouting,
    BsDatepickerModule.forRoot(),
    MatSelectModule,
    MatSlideToggleModule,
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    AngularDualListBoxModule
  ],
  declarations: [BlackOutDatesComponent],
  providers: [CompanySettingsService]
})
export class BlackOutDatesModule { }
