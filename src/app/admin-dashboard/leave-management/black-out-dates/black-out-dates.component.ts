import { Component, OnInit ,TemplateRef} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CompanySettingsService } from '../../../services/companySettings.service';
import { AccessLocalStorageService } from '../../../services/accessLocalStorage.service';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { SiteAccessRolesService } from '../../../services/site-access-roles-service.service';
import { MyInfoService } from '../../../services/my-info.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { DualListComponent } from 'angular-dual-listbox';

@Component({
  selector: 'app-black-out-dates',
  templateUrl: './black-out-dates.component.html',
  styleUrls: ['./black-out-dates.component.css']
})
export class BlackOutDatesComponent implements OnInit {
  modalRef: BsModalRef;
  selectedNav : any;
  chooseFields = false;
  chooseSteps = true;
  jobSalary = false;
  fieldsName:any;
  checked:any;
  message:string;

  StructureValues: Array<any> = [];
  customValues: Array<any> = []
  eligibilityArray = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  selectedUsers = [];
  groupNames = [];
  blackOuts: any = [
    {
      location_id:'',
      blackoutName:'',
      blackoutEmployeeGroup:'',
      startDate:'',
      endDate:'',
      blockType:'',
      isChecked: false,
      edit: false
    }
  ];
  blackoutDetails: any;
  companyId:any;
  allGroups = [
    {name: 'Business Unit1'},
    {name: 'Business Unit2'},
  ]
  public customLocation = [];
  public defaultLocation = [];
  public allLocation = [];
  public getAllStructureFields:any;
  selectedBlackouts = [];
  blackoutId: any;
  updateDetails: any;
  showField1: boolean = false;
  public pageNo: any;
  public perPage: any;
  public count: any;
  previewList = [];
  checkBlackoutIds = [];
  disableCancel:boolean = true;

  structureFields: Array<any> = [
    {
      name: 'Business Unit',
      isChecked: true
    }
  ];
  blackoutGroup:any = {
    name:'',
    effectiveDate:'',
    hireDate:'',
    customDate:''
  }

  format = {
    add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
    direction: DualListComponent.LTR, draggable: true, locale: 'da',
  };

  constructor(private modalService: BsModalService,
    private companySettingsService: CompanySettingsService,
    private accessLocalStorageService: AccessLocalStorageService,
    private leaveManagementService: LeaveManagementService,
    private siteAccessRolesService: SiteAccessRolesService,
    private myInfoService: MyInfoService,
    private swalAlertService: SwalAlertService
    ) { }

  openModalWithClass(template: TemplateRef<any>) {
    this.previewEmployeeList();
    this.selectedNav = 'chooseSteps';
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  confirm(): void {
    this.message = 'Confirmed!';
    this.modalRef.hide();
  }
 
  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }

  ngOnInit() {
    this.pageNo = 1;
    this.perPage = 10;

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.getFieldsForRoles();
    this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
    this.getStandardAndCustomStructureFields();
    this.getEmployeeGroupList();
    this.getBlackoutList();

    // this.dropdownSettings = {
    //   singleSelection: false,
    //   idField: 'item_id',
    //   textField: 'item_text',
    //   selectAllText: 'Select All',
    //   unSelectAllText: 'UnSelect All',
    //   itemsShowLimit: 0,
    //   maxHeight: 150,
    //   defaultOpen: true,
    //   allowSearchFilter: true
    // };
  }

  // Author: Saiprakash G, Date: 14/06/19
  // Get standard and custom structure fields

  getStandardAndCustomStructureFields(){
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res:any)=>{
        console.log(res); 
        this.getAllStructureFields = res.data;
        this.customLocation = this.getAllStructureFields['Location'].custom;
        this.defaultLocation = this.getAllStructureFields['Location'].default;
        this.allLocation = this.customLocation.concat(this.allLocation);
 
      }
    )
  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.previewEmployeeList();
  }

  previewEmployeeList(){

    var finalPreviewUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalPreviewUsers.push(element._id)
    });

    var data:any = {
      per_page: this.perPage,
      page_no: this.pageNo,
      _ids: finalPreviewUsers
    }
    this.companySettingsService.prviewEmployeeList(data).subscribe((res:any)=>{
      console.log(res);
      this.previewList = res.data
      this.count = res.total_count;
      
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 15/06/19,
  // Slect and deselect employees for include and exclude

  // onItemSelect(item: any) {
  //   this.selectedUsers = []
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   for (var i = 0; i < this.selectedItems.length; i++) {
  //     this.selectedUsers.push(this.selectedItems[i].item_id);
  //   }
  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();

  // }
  // OnItemDeSelect(item: any) {
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();
  // }
  // onSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];
  //   for (var i = 0; i < items.length; i++) {
  //     this.selectedUsers.push(items[i].item_id);
  //   }
  //   console.log("users", this.selectedUsers);
  //   this.previewEmployeeList();

  // }
  // onDeSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];

  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();
  // }
 

  getFieldsForRoles() {
    this.companySettingsService.getFieldsForRoles()
      .subscribe((res: any) => {
        console.log("12122123", res);
        this.structureFields = res.data

      }, err => {
        console.log(err);

      })
  }

  getStructureSubFields(fields, isChecked?) {
    console.log(fields, isChecked);
    this.fieldsName = fields;
    this.checked = isChecked
    if (isChecked == true) {
        this.eligibilityArray = [];
      this.structureFields.forEach(element => {
        if (element.name !== fields) {
          element.isChecked = false
        }
      })
      this.companySettingsService.getStuctureFields(fields,0)
        .subscribe((res: any) => {
          console.log(res, name);
          this.StructureValues = res.default;
          this.customValues = res.custom
          console.log(this.StructureValues);


        }, err => {
          console.log(err);

        })
    }
  }
  // Author: Saiprakash G, Date: 15/06/19
  // Sub structure fields

  subFieldsStructure($event, value) {
    console.log($event, value);

    // this.eligibilityArray = [];
    if ($event.checked == true) {
      this.eligibilityArray.push(value)
    }
    console.log(this.eligibilityArray, "123321");
    if ($event.checked == false) {
      var index = this.eligibilityArray.indexOf(value);
      if (index > -1) {
        this.eligibilityArray.splice(index, 1);
      }
      console.log(this.eligibilityArray);

    }
    var postdata = {
      field: this.fieldsName,
      chooseEmployeesForRoles: this.eligibilityArray
    }
    console.log(postdata);
    this.siteAccessRolesService.chooseEmployeesData(postdata)
      .subscribe((res: any) => {
        console.log("emp data", res);
        this.dropdownList = []
        for (var i = 0; i < res.data.length; i++) {
          if ($event.checked == true)
            this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName })
        }
        console.log(this.dropdownList, "employee dropdown");

      },
        (err) => {
          console.log(err);

        })
  }
// Author: Saiprakash G, Date: 17/06/19
// Get employee group list
  getEmployeeGroupList(){
    this.leaveManagementService.getEmployeeGroupList(this.companyId).subscribe((res:any)=>{
      console.log(res);
      this.groupNames = res.result;
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 15/06/19
  // Create blackout employee group

  createBlackoutEmployeeGroup(){
    var finalSelectedUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalSelectedUsers.push(element._id)
    });
    var data:any = {

      companyId:this.companyId,
      name: this.blackoutGroup.name,
      effectiveDate: this.blackoutGroup.effectiveDate,
     initial_group_effective_date_details: {
      date_selection: this.blackoutGroup.hireDate
     },
     employee_ids : finalSelectedUsers
    }

    this.leaveManagementService.createEmployeeGroup(data).subscribe((res:any)=>{
      console.log(res);
      this.getEmployeeGroupList();
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      this.message = 'Confirmed!';
      this.modalRef.hide();
      this.blackoutGroup = "";
      this.dropdownList = [];
      this.selectedUsers = [];

      
    },(err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    })
  }
 // Author: Saiprakash G, Date: 15/06/19
  // Push empty blackouts

  pushBlockouts(){

    this.blackoutDetails = {
      location_id:'',
      blackoutName:'',
      blackoutEmployeeGroup:'',
      startDate:'',
      endDate:'',
      blockType:''
    }
    if(!this.blackOuts){
      this.blackOuts = [];
    }

    this.blackOuts.push(this.blackoutDetails);
    console.log(this.blackOuts);
    
    this.disableCancel = false;
  }
   // Author: Saiprakash G, Date: 15/06/19
  // Select blackout 

  selectBlackout(event, id, blackout){
    console.log(event, id, blackout);
    this.updateDetails = blackout;
    this.blackoutId = id;
    if(event.checked == true){
      this.checkBlackoutIds.push(id)
    }
    console.log(this.checkBlackoutIds);
    if(event.checked == false){
      var index = this.checkBlackoutIds.indexOf(id);
        if (index > -1) {
          this.checkBlackoutIds.splice(index, 1);
        }
        console.log(this.checkBlackoutIds);
    }

  }
   // Author: Saiprakash G, Date: 15/06/19
  // Create blackout date

  createBlackout(){

    var blackoutAdd: any = [];
    for(var i=0; i<this.blackOuts.length; i++){
      if(!this.blackOuts[i]._id){
        blackoutAdd.push(this.blackOuts[i])
      }
    }
    // var data: any = {
    //   country: "India",
    //   companyId: this.companyId,
    //   location_id: this.blackoutDetails.location_id,
    //   blackoutName: this.blackoutDetails.blackoutName,
    //   employeeGrpName: this.blackoutDetails.employeeGrpName,
    //   startDate:this.blackoutDetails.startDate,
    //   endDate: this.blackoutDetails.endDate,
    //   blockType: this.blackoutDetails.blockType
    // }

    if(blackoutAdd.length == 0){
        this.swalAlertService.SweetAlertWithoutConfirmation("Please add blackout date", "", "error")
           
    }
    else {
      this.leaveManagementService.createBlackout(blackoutAdd).subscribe((res:any)=>{
        console.log(res);
        this.getBlackoutList();
        this.blackoutDetails = "";
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    }
   

   
  }

   // Author: Saiprakash G, Date: 15/06/19
  // Update blackout day

  updateBlackout(){
    var data1: any = {
      _id: this.blackoutId,
      companyId: this.companyId,
      location_id: this.updateDetails.location_id,
      blackoutName: this.updateDetails.blackoutName,
      blackoutEmployeeGroup: this.updateDetails.blackoutEmployeeGroup,
      startDate:this.updateDetails.startDate,
      endDate: this.updateDetails.endDate,
      blockType: this.updateDetails.blockType
    }
    if(this.blackoutId){
       
      this.leaveManagementService.updateBlackout(data1).subscribe((res:any)=>{
        console.log(res);
        this.getBlackoutList();
        this.checkBlackoutIds = [];
        this.blackoutId = "";
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    }
  }
 // Author: Saiprakash G, Date: 15/06/19
  // Get blackout list
  getBlackoutList(){
    var data : any = {
      country: "India"
    }
    this.leaveManagementService.getBlackoutList(data).subscribe((res:any)=>{
      console.log(res);
      this.blackOuts = res.result;
      this.blackOuts.filter(item=>{
        item.edit=true;
      });
    },(err)=>{
      console.log(err);
      
    })
  }

   // Author: Saiprakash G, Date: 15/06/19
  // Edit blackout
  editBlackout(){
    this.selectedBlackouts = this.blackOuts.filter( checkVisa =>{
      return checkVisa.isChecked
    })
  
    if (this.selectedBlackouts.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one blackout to proceed","", 'error')
    } else if (this.selectedBlackouts.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one blackout to proceed","", 'error')
    } else {
      if(this.selectedBlackouts[0]._id){
        this.selectedBlackouts[0].edit = false;
    }
  }
  }

   // Author: Saiprakash G, Date: 15/06/19
  //  Delete blackout
  deleteBlackout(){
    this.selectedBlackouts = this.blackOuts.filter( checkVisa =>{
      return checkVisa.isChecked
    })
    if (this.selectedBlackouts.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one blackout to proceed","", 'error')
    } else {
     var data:any = {
       _id: this.selectedBlackouts
     }
     this.leaveManagementService.deleteBlackout(data).subscribe((res:any)=>{
       console.log(res);
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
       this.getBlackoutList();
       this.checkBlackoutIds = [];
       this.blackoutId = "";
     },(err)=>{
       console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
     })
    }
  }

  selectEffectiveDate(event) {
    if (event === "date_of_hire") {
      this.showField1 = false;
    }
    else if (event === "custom_date") {
      this.showField1= true;
    }
  }

  cancelBlackout(){
    this.getBlackoutList();
    this.disableCancel = true;
  }

  onSelectUsers(selectSpecificPerson){
    console.log("users",selectSpecificPerson);
   
    console.log('selcetd users', this.selectedUsers);
     this.previewEmployeeList();
    
  }

}
