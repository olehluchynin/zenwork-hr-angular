import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlackOutDatesComponent } from './black-out-dates.component';

const routes: Routes = [
   
    {path:'',component:BlackOutDatesComponent }
]
    



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BlackOutDatesRouting { }