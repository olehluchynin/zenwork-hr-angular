import { BlackOutDatesModule } from './black-out-dates.module';

describe('BlackOutDatesModule', () => {
  let blackOutDatesModule: BlackOutDatesModule;

  beforeEach(() => {
    blackOutDatesModule = new BlackOutDatesModule();
  });

  it('should create an instance', () => {
    expect(blackOutDatesModule).toBeTruthy();
  });
});
