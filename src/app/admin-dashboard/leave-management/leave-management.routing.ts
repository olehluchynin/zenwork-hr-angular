import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeaveManagementComponent } from './leave-management.component';
import { SharedModule } from '../../shared-module/shared.module';
import { TimePoliciesComponent } from './time-policies/time-policies.component';
import { TimeSheetApprovalComponent } from './time-sheet-approval/time-sheet-approval.component';


const routes: Routes = [
    // { path: '', redirectTo: "leave-management", pathMatch: "full" },
     { path:'', component:LeaveManagementComponent},
     {path:'time-policies', component:TimePoliciesComponent},
     {path:'scheduler' ,loadChildren:"./scheduler/scheduler.module#SchedulerModule"},
     {path:'time-sheet', component:TimeSheetApprovalComponent},
     {path:'leave-main', loadChildren:'./leave-management-eligibility/leave-management-eligibility.module#LeaveManagementEligibilityModule'}
];

@NgModule({
    imports: [RouterModule.forChild(routes), SharedModule],
    exports: [RouterModule],
   

})

export class LeaveManagementRouting { }