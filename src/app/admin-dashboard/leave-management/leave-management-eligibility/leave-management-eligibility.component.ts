import { Component, OnInit } from '@angular/core';
import { AddLeaveEligibilityComponent } from './add-leave-eligibility/add-leave-eligibility.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-leave-management-eligibility',
  templateUrl: './leave-management-eligibility.component.html',
  styleUrls: ['./leave-management-eligibility.component.css']
})
export class LeaveManagementEligibilityComponent implements OnInit {

  public leaveEligibilityGroups = [];
  public selectedEligibilityGroups = [];
  public getSingleEligibilityGroup:any;
  checkEligibilityIds = [];

  constructor(
    public dialog: MatDialog,
    private leaveManagementService: LeaveManagementService,
    private swalAlertService: SwalAlertService,
    private router:Router 
  ) { }



  ngOnInit() {
    this.getAllLeaveEligibilityGroups();
  }

    // Author: Saiprakash G, Date: 27/05/19,
  // Open dialog for add and edit leave eligibility group

  openDialog(): void {

    if(this.getSingleEligibilityGroup){
      const dialogRef = this.dialog.open(AddLeaveEligibilityComponent, {
        width: '1200px',
        height:'700px',
        backdropClass: 'structure-model',
        data: this.getSingleEligibilityGroup, 
             
      });
  
      dialogRef.afterClosed().subscribe((result:any) => {
        console.log('The dialog was closed', result);
        this.getAllLeaveEligibilityGroups();
        this.getSingleEligibilityGroup = "";
        this.checkEligibilityIds = [];
    
      });
    }
else {
    const dialogRef = this.dialog.open(AddLeaveEligibilityComponent, {
      width: '1300px',
      height:'700px',
      backdropClass: 'structure-model',
      data: {}, 
           
    });

    dialogRef.afterClosed().subscribe((result:any) => {
      console.log('The dialog was closed', result);
      this.getAllLeaveEligibilityGroups();
  
    });
  }
  }

    // Author: Saiprakash G, Date: 29/05/19,
  // Get all leave eligibility groups
  getAllLeaveEligibilityGroups(){
    this.leaveManagementService.getAllLeaveEligibilityGroup().subscribe((res:any)=>{
      console.log(res);
      this.leaveEligibilityGroups = res.data;
      
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 30/05/19,
  // Edit leave eligibility group

  editEligibilityGroup(){
    this.selectedEligibilityGroups = this.leaveEligibilityGroups.filter(checkGroups =>{
      return checkGroups.isChecked
    })

    if (this.selectedEligibilityGroups.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one leave eligibility group on to proceed","", 'error')
    } else if (this.selectedEligibilityGroups.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one leave eligibility group on to proceed","", 'error')
    } else {

      this.leaveManagementService.getLeaveEligibilityGroup(this.selectedEligibilityGroups[0]._id).subscribe((res:any)=>{
        console.log(res);
        
        this.getSingleEligibilityGroup = res.data;
        this.openDialog();
        
      }, (err)=>{
        console.log(err);
        
      })
    }
  }

  // Author: Saiprakash G, Date: 30/05/19,
  // Delete leave eligibility group
  deleteEligibilityGroup(){
    this.selectedEligibilityGroups = this.leaveEligibilityGroups.filter(checkGroups =>{
      return checkGroups.isChecked
    })

    if (this.selectedEligibilityGroups.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select leave eligibility group on to proceed","", 'error')
    } else {

      var deleteGroup: any = {
        _ids: this.selectedEligibilityGroups

      }
     
      this.leaveManagementService.deleteLeaveEligibilityGroup(deleteGroup).subscribe((res:any)=>{
        console.log(res);
        this.checkEligibilityIds = [];
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        this.getAllLeaveEligibilityGroups();
        
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
      

    }
  }

  selectLeaveEligibility(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.checkEligibilityIds.push(id)
    }
    console.log(this.checkEligibilityIds);
    if(event.checked == false){
      var index = this.checkEligibilityIds.indexOf(id);
        if (index > -1) {
          this.checkEligibilityIds.splice(index, 1);
        }
        console.log(this.checkEligibilityIds);
    }
  }
  previousPage(){
    this.router.navigate(['/admin/admin-dashboard/leave-management']);
    
  }

}
