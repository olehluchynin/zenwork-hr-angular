import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveManagementEligibilityComponent } from './leave-management-eligibility.component';

describe('LeaveManagementEligibilityComponent', () => {
  let component: LeaveManagementEligibilityComponent;
  let fixture: ComponentFixture<LeaveManagementEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveManagementEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveManagementEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
