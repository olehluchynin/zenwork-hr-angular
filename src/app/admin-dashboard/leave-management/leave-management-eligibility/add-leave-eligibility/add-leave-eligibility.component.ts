import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { LeaveManagementService } from '../../../../services/leaveManagement.service';
import { environment } from '../../../../../environments/environment';
import * as FileSaver from 'file-saver';
import { DualListComponent } from 'angular-dual-listbox';
import { EmployeeService } from '../../../../services/employee.service';

@Component({
  selector: 'app-add-leave-eligibility',
  templateUrl: './add-leave-eligibility.component.html',
  styleUrls: ['./add-leave-eligibility.component.css']
})
export class AddLeaveEligibilityComponent implements OnInit {
  StructureValues: Array<any> = [

  ];
  customValues: Array<any> = [

  ]
  eligibilityArray = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  fieldsName: any;
  checked: any;
  selectedUsers = [];
  companyId: any;
  showField1: boolean = false;
  showField2: boolean = false;
  editField: boolean = true;
  subFieldCheck: any;
  downloadUrl: string;
  eventName: any;
  structureFields: Array<any> = [
    {
      name: 'Business Unit',
      isChecked: true
    }
  ];
  eligibilityGroup: any = {
    name: '',
    effectiveDate: '',
    // useBenefitGroupRules: '',
    // benifitGroup: '',
    date_selection: '',
    custom_date_field: ''

  }

  public pageNo: any;
  public perPage: any;
  public count: any;
  previewList = [];
  selectedEmployees = [];

  format = {
    add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
    direction: DualListComponent.LTR, draggable: true, locale: 'da',
  };

  constructor(
    public dialogRef: MatDialogRef<AddLeaveEligibilityComponent>,
    private companySettingsService: CompanySettingsService,
    private siteAccessRolesService: SiteAccessRolesService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private leaveManagementService: LeaveManagementService,
    private employeeService: EmployeeService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {


  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    this.pageNo = 1;
    this.perPage = 10;

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    console.log(this.data);
    this.getAllEmp();
    this.getFieldsForRoles();
    
    if (this.data) {
      this.eligibilityGroup = this.data;
      // if (this.data.useBenefitGroupRules === true) {
      //   this.showField1 = false;
      // }
      
    if(this.data.userIds){
      var data:any = {
        per_page: this.perPage,
        page_no: this.pageNo,
        _ids: this.data.userIds
      }
      this.companySettingsService.prviewEmployeeList(data).subscribe((res:any)=>{
        console.log(res);

        this.previewList = res.data
        this.count = res.total_count;
        for (var i = 0; i < res.data.length; i++) {
        
          this.selectedUsers.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName })
         
        }
        
      },(err)=>{
        console.log(err);
        
      })
    }
      if (this.data.initial_group_effective_date_details) {
        this.eligibilityGroup.date_selection = this.data.initial_group_effective_date_details.date_selection;
        if (this.data.initial_group_effective_date_details.date_selection === "date_of_hire") {
          this.showField2 = false;
        }
        this.eligibilityGroup.custom_date_field = this.data.initial_group_effective_date_details.custom_date_field;
        console.log(this.eligibilityGroup.date_selection);


      }

    }

    // if (this.data.eligibility_criteria && this.data.eligibility_criteria.sub_fields.length) {
    //   this.eligibilityArray = this.data.eligibility_criteria.sub_fields;
    //   this.getStructureSubFields(this.data.eligibility_criteria.selected_field, true, this.data.eligibility_criteria.sub_fields);

    // }
    

    // this.dropdownSettings = {
    //   singleSelection: false,
    //   idField: 'item_id',
    //   textField: 'item_text',
    //   selectAllText: 'Select All',
    //   unSelectAllText: 'UnSelect All',
    //   itemsShowLimit: 0,
    //   maxHeight: 150,
    //   defaultOpen: true,
    //   allowSearchFilter: true
    // };

  }

  // Author: Saiprakash G, Date: 05/06/19,
  // Restrict fields
  selectBenefitEligibilityGroup(event) {
    console.log(event);
    if (event === true) {
      this.showField1 = true;
    }
    else {
      this.showField1 = false;
    }
  }

  selectEffectiveDate(event) {
    if (event === "date_of_hire") {
      this.showField2 = false;
    }
    else if (event === "custom_date") {
      this.showField2 = true;
    }
  }

  // Author: Saiprakash G, Date: 05/06/19,
  // Upload custom date file
  fileChangeEvent(e: any) {

    var files = e.target.files[0];
    console.log(files.name)
    var formData: FormData = new FormData();
    formData.append('file', files);

    this.leaveManagementService.uploadCustomFile(formData).subscribe((res: any) => {
      console.log(res);

    }, err => {
      console.log(err);

    })

  }

  // Author: Saiprakash G, Date: 05/06/19,
  // Download sample custom date xlxs file

  downloadExcelFile() {
    var downloadData: any = [
      {
        "empId": "2432534",
        "empName": "Alex",
        "groupName": "Team Titans",
        "effectiveDate": "2019-03-29(yyyy-mm-dd)"
      }
    ]
    this.leaveManagementService.downloadCustomFile(downloadData).subscribe((res: any) => {
      console.log(res);
      var file = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      var fileURL = URL.createObjectURL(file);
      FileSaver.saveAs(file, 'file.xlsx');
      // window.open(fileURL)
    }, err => {
      console.log(err)
    })

  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.previewEmployeeList();
  }

  previewEmployeeList(){

    var finalPreviewUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalPreviewUsers.push(element._id)
    });

    var data:any = {
      per_page: this.perPage,
      page_no: this.pageNo,
      _ids: finalPreviewUsers
    }
    this.companySettingsService.prviewEmployeeList(data).subscribe((res:any)=>{
      console.log(res);
      this.previewList = res.data;
      this.count = res.total_count;
      
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 04/06/19,
  // Slect and deselect employees for include and exclude

  // onItemSelect(item: any) {
  //   this.selectedUsers = []
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   for (var i = 0; i < this.selectedItems.length; i++) {
  //     this.selectedUsers.push(this.selectedItems[i].item_id);
  //   }
  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();

  // }
  // OnItemDeSelect(item: any) {
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();
  // }
  // onSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];
  //   for (var i = 0; i < items.length; i++) {
  //     this.selectedUsers.push(items[i].item_id);
  //   }
  //   console.log("users", this.selectedUsers);
  //   this.previewEmployeeList();

  // }
  // onDeSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];

  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();
  // }

  // Author: Saiprakash , Date: 27/05/19
  // Get fields for roles
  getFieldsForRoles() {
    this.companySettingsService.getFieldsForRoles()
      .subscribe((res: any) => {
        console.log("12122123", res);
        this.structureFields = res.data;
        this.structureFields[0].isChecked = false;
        // if (this.data.eligibility_criteria && this.data.eligibility_criteria.selected_field) {
        //   let index = this.structureFields.findIndex(e => e.name === this.data.eligibility_criteria.selected_field)
        //   console.log(index, 'index')
        //   if (index >= 0) {
        //     this.structureFields[index].isChecked = true;
        //   }
        // }

      }, err => {
        console.log(err);

      })
  }

  // Author: Saiprakash , Date: 27/05/19
  // Get Structure Fields
  getStructureSubFields(fields, isChecked, sub_fields) {
    console.log(fields, isChecked);
    this.fieldsName = fields;
    // this.eligibilityArray = [];
    this.checked = isChecked
    if (isChecked == true) {

      this.structureFields.forEach(element => {
        if (element.name !== fields) {
          element.isChecked = false
        }
      })
      this.companySettingsService.getStuctureFields(fields, 0)
        .subscribe((res: any) => {

          this.EmployeeListSummary();
          console.log(res, name);

          this.StructureValues = res.default;
          this.customValues = res.custom;

          console.log(this.StructureValues);
          if (sub_fields && sub_fields.length) {
            sub_fields.forEach(element => {
              let structureIndex = this.StructureValues.findIndex(e => e.name === element);
              let customValuesIndex = this.customValues.findIndex(e => e.name === element);

              if (structureIndex >= 0) {
                this.StructureValues[structureIndex].isChecked = true;
              }
              if (customValuesIndex >= 0) {
                this.customValues[customValuesIndex].isChecked = true;
              }
            });
          }

        }, err => {
          console.log(err);

        })
    }
  }
  // Author: Saiprakash , Date: 27/05/19
  // Get sub fields structure 

  subFieldsStructure($event, value) {
    console.log($event, value);
    this.eventName = $event;
    // this.eligibilityArray = [];
    if ($event.checked == true) {
      this.eligibilityArray.push(value)
    }
    console.log(this.eligibilityArray, "123321");
    if ($event.checked == false) {
      var index = this.eligibilityArray.indexOf(value);
      if (index > -1) {
        this.eligibilityArray.splice(index, 1);
      }
      console.log(this.eligibilityArray);

    }
    this.EmployeeListSummary();
  }

  EmployeeListSummary() {
    var postdata = {
      field: this.fieldsName,
      chooseEmployeesForRoles: this.eligibilityArray
    }
    console.log(postdata);
    this.siteAccessRolesService.chooseEmployeesData(postdata)
      .subscribe((res: any) => {
        console.log("emp data", res);
        this.dropdownList = []
        for (var i = 0; i < res.data.length; i++) {
        
          this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName })
         
        }
        console.log(this.dropdownList, "employee dropdown");
        var result = this.selectedUsers.filter(item => (this.dropdownList.findIndex(e => e.item_id === item)) >= 0);
        this.selectedUsers = result
        console.log(result, this.selectedUsers);

      },
        (err) => {
          console.log(err);

        })
  }

  // Author: Saiprakash G, Date: 28/05/19,
  // Create and update leave eligibility group

  createAndUpdateLeaveEligibilityGroup() {

    var finalSelectedUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalSelectedUsers.push(element._id)
    });

    var data1: any = {
     
      name: this.eligibilityGroup.name,
      effectiveDate: this.eligibilityGroup.effectiveDate,
      // useBenefitGroupRules: this.eligibilityGroup.useBenefitGroupRules,
      // benifitGroup: this.eligibilityGroup.benifitGroup,
      initial_group_effective_date_details: {
        date_selection: this.eligibilityGroup.date_selection,
        custom_date_field: this.eligibilityGroup.custom_date_field
      },
      empIds: finalSelectedUsers
    }
    if (this.data._id) {
      data1._id = this.data._id;
      
      this.leaveManagementService.updateLeaveEligibilityGroup(data1).subscribe((res: any) => {
        console.log(res);
        this.dialogRef.close(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }
    else {
      this.leaveManagementService.createLeaveEligibilityGroup(data1).subscribe((res: any) => {
        console.log(res);
        this.dialogRef.close(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }
  }


  // Author: Saiprakash G, Date:29/08/19,
  // Description: Preview employee list

  onSelectUsers(selectSpecificPerson){
    console.log("users",selectSpecificPerson);
   
    console.log('selcetd users', this.selectedUsers);
     this.previewEmployeeList();
    
  }

   // Author: Saiprakash G, Date:29/08/19,
  // Description: Get all employees in include box for edit

  getAllEmp() {
    var listallUsers;
    var postData = {
      name: ''
    }
    console.log("data comes");

    this.siteAccessRolesService.getAllEmployees(postData)
      .subscribe((res: any) => {
        console.log("resonse for all employees", res);
        listallUsers = res.data;
        for (var i = 0; i < listallUsers.length; i++) {
          this.dropdownList.push({ _id: listallUsers[i]._id, _name: listallUsers[i].personal.name.preferredName })
          
        }
        console.log( "DropDownList" ,this.dropdownList);
      },
        (err) => {
          console.log(err);

        })
  }

}
