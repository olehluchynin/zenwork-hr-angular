import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLeaveEligibilityComponent } from './add-leave-eligibility.component';

describe('AddLeaveEligibilityComponent', () => {
  let component: AddLeaveEligibilityComponent;
  let fixture: ComponentFixture<AddLeaveEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLeaveEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLeaveEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
