import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveManagementEligibilityComponent } from './leave-management-eligibility.component';
import { RouterModule, Routes} from '@angular/router';
import { MatDialogModule} from '@angular/material';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { AddLeaveEligibilityComponent } from './add-leave-eligibility/add-leave-eligibility.component';
import { CompanySettingsService } from '../../../services/companySettings.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularDualListBoxModule } from 'angular-dual-listbox';


const router:Routes = [
  {path:'', redirectTo:'eligibility', pathMatch:'full'},
  {path:'eligibility', component:LeaveManagementEligibilityComponent }
]

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(router),MatDialogModule,
    MaterialModuleModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    AngularDualListBoxModule
  ],
  declarations: [LeaveManagementEligibilityComponent, AddLeaveEligibilityComponent],
  entryComponents: [AddLeaveEligibilityComponent],
  providers: [CompanySettingsService]
})
export class LeaveManagementEligibilityModule { }
