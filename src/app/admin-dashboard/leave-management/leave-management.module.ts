import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveManagementComponent } from './leave-management.component';
import { LeaveManagementRouting } from './leave-management.routing';
import { TimePoliciesComponent } from './time-policies/time-policies.component';


import { MatSelectModule, MatSlideToggleModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { SchedulerModule } from './scheduler/scheduler.module';
import { TimeSheetApprovalComponent } from './time-sheet-approval/time-sheet-approval.component';
import { MaterialModuleModule } from '../../material-module/material-module.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { LeaveManagementService } from '../../services/leaveManagement.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TierLevelModelComponent } from './time-policies/tier-level-model/tier-level-model.component';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { ReviewHistoricalTimesheetComponent } from './time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component'; 



@NgModule({
  imports: [
    CommonModule,LeaveManagementRouting,SchedulerModule,MatSelectModule,MatSlideToggleModule,MatDatepickerModule, MatNativeDateModule, MaterialModuleModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    AmazingTimePickerModule
  ],
  declarations: [LeaveManagementComponent, TimePoliciesComponent, TimeSheetApprovalComponent, TierLevelModelComponent, ReviewHistoricalTimesheetComponent,],
  providers: [LeaveManagementService],
  entryComponents: [TierLevelModelComponent,ReviewHistoricalTimesheetComponent]
})
export class LeaveManagementModule { }
