import { Component, OnInit ,TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { AccessLocalStorageService } from '../../../services/accessLocalStorage.service';
import { SiteAccessRolesService } from '../../../services/site-access-roles-service.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { MyInfoService } from '../../../services/my-info.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CompanySettingsService } from '../../../services/companySettings.service';
import { DynamicResourceModelComponent } from './dynamic-resource-model/dynamic-resource-model.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DualListComponent } from 'angular-dual-listbox';



@Component({
  selector: 'app-work-schedules',
  templateUrl: './work-schedules.component.html',
  styleUrls: ['./work-schedules.component.css']
})
export class WorkSchedulesComponent implements OnInit {

  modalRef: BsModalRef;
  //first popup variables start
  selectedNav : any;
  workSchedule = true;
  assignWork = false;
  recap = false;
  //secoond-popup variables start
  defineTeam = true;
  applyData = false;
  selectEmployees = false;
  scheduleBuilder = false;
  recap2 = false;
  message:string;
  dropdownList = [];
  dropdownSettings = {};
  selectedItems = [];
  weekDays: any = [

  ];
  companyId:any;
  allPositions = [];
  employeeData = [];
  employeeIds = [];
  selectedPostions = [];
  selectedDays = [];
  departmentSelectDays = [];
  allSelectedDepartmentDays = []
  editTemplate:any;
  templateRef:any;
  getPositionScheduler: any;
  dayCheckbox: any;
  selectDayOfWeek:any;
  disableFiled: boolean = true;
  getAllStructureFields:any;
  customJobTitles = [];
  defaultJobTitles = [];
  allJobTitles = [];
  allSelectedDays = [];
  timeIntervals = [];
  assignedEmployees = [];
  totalDayEvent:any;
  startTimeValue:any;
  isValid: boolean = false;
  checkPostionIds = [];
  dayCheck:any;
  lunchTime:boolean = false;
  jobTitleShow:boolean = true;
  
  public positionSchedule: any = {
    scheduleTitle:'',
    scheduleStartDate:'',
    type:'Standard',
    jobTitle:'',
    totalHrsPerDay:'',
    totalHrsPerWeek:'',
    lunchBreakTime:'',
    // selectFewDays:''

  };
  
  StructureValues: Array<any> = [];
  customValues: Array<any> = []
  eligibilityArray = [];
  selectedUsers = [];
  fieldsName:any;
  checked:any;
  structureFields: Array<any> = [];
  allDepartments = [];
  selectedDepartments = [];
  schedulesStatus =[];
  index:any;
  employeeList : Array<any> = [];
  scheduleStatus= [];
  getDepartmentSchedule:any = {
    Open_Time:''
  };
  startTime:any;
  lunchStartTime:any;
  lunchEndTime:any;
  endTime:any;
  minutes:any;
  hours:any;
  intervalStatus = [];
  newTime:any;
  status = [];
  schedule_Builder = [];
  uniqueDays = [];
  employeeStartTime:any;
  employeeLunchTime:any;
  employeeEndTmie:any;
  scheduleBuilderData:any;
  employeeStatusList = [];
  getemployeeList = [];
  public pageNo: any;
  public perPage: any;
  public count: any;
  previewList = [];
  dynamicResourceHide:boolean = false;
  standardResourceHide:boolean = false;
  departmentDay:any;
  departmentEmployeesScheduleInfo = [];
  checkDeptScheduleIds = [];
  departmentCreationDate:any;
  dayWiseList = [];
  firstDay:any;
  usersList = [];

  public departmentSchedule:any = {
    departmentScheduleName:'',
    openTime:'',
    closeTime:'',
    intervalSpan:'',
    resourceType:'',
    standardNoOfResourcesPerInterval:'',
    startDate:'',
    endDate:'',
    // ScheduleDate:'',
    // Scedule_Resource_Type:'',
    // Schedule_StartDate:'',
    // Schedule_EndDate:''

  }
  public days = [
    {name:'Monday', isChecked: false},
    {name:'Tuesday', isChecked: false},
    {name:'Wednesday', isChecked: false},
    {name:'Thursday', isChecked: false},
    {name:'Friday', isChecked: false},
    {name:'Saturday', isChecked: false},
    {name:'Sunday', isChecked: false},
  ];
  public days2 = [
    {name:'Monday', isChecked: false},
    {name:'Tuesday', isChecked: false},
    {name:'Wednesday', isChecked: false},
    {name:'Thursday', isChecked: false},
    {name:'Friday', isChecked: false},
    {name:'Saturday', isChecked: false},
    {name:'Sunday', isChecked: false},
  ];

  @ViewChild('define_dept') define_dept: ElementRef;
  @ViewChild('select_employees') select_employees: ElementRef;
  @ViewChild('schedule_builder') schedule_builder: ElementRef;
  @ViewChild('recap_all') recap_all: ElementRef;

  format = {
    add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
    direction: DualListComponent.LTR, draggable: true, locale: 'da',
  };
 
  constructor(private modalService: BsModalService,
    private leaveManagementService: LeaveManagementService,
    private atp: AmazingTimePickerService,
    private accessLocalStorageService: AccessLocalStorageService,
    private siteAccessRolesService: SiteAccessRolesService,
    private swalAlertService: SwalAlertService,
    private myInfoService: MyInfoService,
    private companySettingsService: CompanySettingsService,
    public dialog: MatDialog,
    private fb: FormBuilder
    ) { }

   

    // Author: Saiprakash G, Date: 17/06/19
    // Add Time picker

    open(event) {
      console.log(event);
      
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        console.log(time);

      });
    }


  openModalWithClass1(template1: TemplateRef<any>) {//create a work schedle popup
    
    this.selectedNav = 'workSchedule';
   this.modalRef = this.modalService.show(
     template1,
     Object.assign({}, { class: 'gray modal-lg' })
   );
 }
 openModalWithClass(template1: TemplateRef<any>) { //create a Department work schedule
  this.selectedNav = 'defineTeam';
 this.modalRef = this.modalService.show(
   template1,
   Object.assign({}, { class: 'gray modal-lg' })
 );
}

confirm(): void {
  this.message = 'Confirmed!';
  this.modalRef.hide();
}

decline(): void {
  this.message = 'Declined!';
  this.modalRef.hide();
  this.departmentSchedule = '';
  this.getAllDepartmentSchedules();
  this.checkDeptScheduleIds = [];
  
  this.selectedUsers = [];
  this.employeeList = [];
  

  let index = this.departmentSelectDays.map(e =>{
    let i=this.days2.findIndex(ele=>ele.name===e)
    if(i>=0){
      this.days2[i].isChecked=false;
      
    }

  } );

  this.getDepartmentSchedule = "";
  this.departmentSelectDays = [];
  console.log(this.departmentSelectDays);
  

}

  ngOnInit() {

    this.departmentCreationDate = new Date().toLocaleDateString();
    
   
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.getStandardAndCustomStructureFields();
    this.getAllWorkPostions();
    if(this.getDepartmentSchedule.eligibility_criteria && this.getDepartmentSchedule.eligibility_criteria.selected_field){
      this.getStructureSubFields(this.getDepartmentSchedule.eligibility_criteria.selected_field, true, this.getDepartmentSchedule.eligibility_criteria.sub_fields);

    }
    this.getAllEmp()
    this.getFieldsForRoles();
    this.getAllDepartmentSchedules();
    // this.dropdownSettings = {
    //   singleSelection: false,
    //   idField: 'item_id',
    //   textField: 'item_text',
    //   selectAllText: 'Select All',
    //   unSelectAllText: 'UnSelect All',
    //   itemsShowLimit: 0,
    //   maxHeight: 150,
    //   defaultOpen: true,
    //   allowSearchFilter: true
    // };

  }

  positionCancel(){
    this.message = 'Declined!';
    this.modalRef.hide();
    this.positionSchedule = {
      scheduleTitle: '',
      scheduleStartDate: '',
      jobTitle: '',
      totalHrsPerDay: '',
      totalHrsPerWeek: '',
      lunchBreakTime: ''

    };
    // this.isValid = false;
    this.weekDays = [];
    
   
    let index = this.getPositionScheduler.daysOfWeek.map(e =>{
      let i=this.days.findIndex(ele=>ele.name===e)
      console.log(i);
      
      if(i>=0){
        console.log('days',this.days,this.days[i]);
        
        this.days[i].isChecked=false;

      }

    } )
    this.checkPostionIds = [];
    this.selectedDays = [];
    this.getPositionScheduler = "";
    this.getAllWorkPostions();
    console.log(this.selectedDays);
    
  }

  setLunchTime(event){
    console.log(event);
    if(event == true){
      this.lunchTime = true;
    } else {
      this.lunchTime = false;
    }

    
  }

 
  // Author:Saiprakash, Date:20/06/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields(){
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res:any)=>{
        console.log(res); 
        this.getAllStructureFields = res.data;
        this.customJobTitles = this.getAllStructureFields['Job Title'].custom;
        this.defaultJobTitles = this.getAllStructureFields['Job Title'].default;
        this.allJobTitles = this.customJobTitles.concat(this.defaultJobTitles);
       
      }
    )
  }

  // async selectAll(e?) {
  //   console.log(e);
    
  //   this.selectedDays = [];
  //   this.days.forEach(element => {
  //     element.isChecked = this.positionSchedule.selectFewDays;
  //   });
  //   await this.days.forEach(element => {

  //     if (element.isChecked == true) {
  //       this.selectedDays.push(element.name );
       
  //     }
  //   });
  //   console.log(this.selectedDays);

  // }

  // async selectDay(event, day, data) {
  //   this.selectedDays = [];
  //   if (data) {
  //     this.dayCheck = 0;
  //     this.days.forEach(element => {

  //       if (!element.isChecked) {
  //         this.dayCheck = 1;
  //       }

  //     });

  //     if (!this.dayCheck) {
  //       this.positionSchedule.selectFewDays = true;
  //     }
  //   }
  //   else {
  //     this.positionSchedule.selectFewDays = false;
  //   }

  //   await this.days.forEach(element => {

  //     if (element.isChecked) {
  //       this.selectedDays.push(element.name);
  //     }
  //   });
  //   console.log(this.selectedDays);

    

  // }


   // Author: Saiprakash G, Date: 17/06/19
  // Select week days

  selectDay(event, day, isChecked){
    console.log(event, day);
    this.selectDayOfWeek = day;
    if(event == true){
      if(this.selectedDays){
        this.selectedDays.push(day);
      
      }
      
      this.weekDays.push({
        day: day,
        shiftStartTime: '',
        lunchStartTime: '' ,
        lunchEndTime: '',
        shiftEndTime: '',
        // edit: false,
        // isChecked: false
      })
    }
    else if(event == false){
      for(var i=0; i < this.selectedDays.length; i++){
        if(this.selectedDays[i] === day){
          this.selectedDays.splice(i,1);
        }
        
      }
  
      for(var i=0; i < this.weekDays.length; i++){
        if(this.weekDays[i].day === day){
          this.weekDays.splice(i,1);
        }
        
      }
     
    }
  
    console.log(this.weekDays);
    console.log(this.selectedDays);
    if(this.getPositionScheduler && this.getPositionScheduler.daysOfWeek){
      // this.allSelectedDays = this.selectedDays.concat(this.getPositionScheduler.daysOfWeek)
      this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * this.getPositionScheduler.totalHrsPerDay;
     
    }

    if(this.totalDayEvent){
      this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * this.totalDayEvent;
    }
    
    // console.log(this.allSelectedDays); 
  }

   // Author: Saiprakash G, Date: 31/07/19
  // totalHoursDay
  totalHoursDay(event) {
    console.log("dayyyyyyyyy", event);
    this.totalDayEvent = event;

    if (this.selectedDays.length) {
      var num = event;
      var min, hrs;
      this.positionSchedule.totalHrsPerWeek = [];
      this.weekDays.shiftStartTime = [];
      this.weekDays.shiftEndTime = [];

      if (num) {
        min = num.split(':');
      }

      // console.log(min[0], "minutes afcter");
      hrs = min[0] * 60;
      if (min[1] && min[1] !== '') {
        console.log(min, "minutes afcter");

        hrs = parseInt(hrs) + parseInt(min[1]);
        console.log("hrssssss", hrs)

      }
      this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * hrs / 60;
      this.weekDays.shiftStartTime = [];
    } else {
      this.swalAlertService.SweetAlertWithoutConfirmation("", "Select days of the week applicable", "Error")
    }


  }

 // Author: Saiprakash G, Date: 31/07/19
  // timeStartDay
  timeStartDay(event, i){
    console.log("time starttt", event);
    this.startTimeValue = event;
  }

 // Author: Saiprakash G, Date: 31/07/19
  // endTimeDays
  endTimeDays(event, i) {
    var clearTime = i;
    console.log("endddddd", event,i);

    console.log("weekday iiiiii", this.startTimeValue);

    var t0 = this.startTimeValue;

    console.log("check t00000", t0);

    var t1 = event;

    console.log("t00000", t0, t1);

    var diff = this.getTimeDifference(t0, t1);
    console.log("timeeee difffff", diff)
    var d = parseInt(diff.split(':')[0]);
    var p = parseInt(this.positionSchedule.totalHrsPerDay)
    console.log(d, typeof d, typeof d)
    console.log(d > p || d < p)
  
    if (d !== p) {
    
      console.log(this.weekDays)
      clearTime.shiftEndTime = null;

      this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Please check the correct time duration", "error")
    } else {
       this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Verify time duration successfully", "success")
    }

  }

  timeStringToMins(s) {
    s = s.split(':');
    s[0] = /m$/i.test(s[1]) && s[0] == 12 ? 0 : s[0];
    return s[0] * 60 + parseInt(s[1]) + (/pm$/i.test(s[1]) ? 720 : 0);
  }

  // Return difference between two times in hh:mm[am/pm] format as hh:mm
  getTimeDifference(t0, t1) {

    // Small helper function to padd single digits
    function z(n) { return (n < 10 ? '0' : '') + n; }

    // Get difference in minutes
    var diff = this.timeStringToMins(t1) - this.timeStringToMins(t0);

    // Format difference as hh:mm and return
    return z(diff / 60 | 0) + ':' + z(diff % 60);
  }

  DepartmentSelectDay(event, day, isChecked){

    if(isChecked == true){
      if(this.departmentSelectDays){
        this.departmentSelectDays.push(day);
        
      }
    }
      else if(isChecked == false){
        for(var i=0; i < this.departmentSelectDays.length; i++){
          if(this.departmentSelectDays[i] === day){
            this.departmentSelectDays.splice(i,1);
          }
          
        }
      
  }

  console.log(this.departmentSelectDays);
  // if(this.getDepartmentSchedule.Days_Week_Applicable){
  //   this.allSelectedDepartmentDays = this.departmentSelectDays.concat(this.getDepartmentSchedule.Days_Week_Applicable)

  // }
}

   // Author: Saiprakash G, Date: 18/06/19
  // Select schedule type for get employess list

  // selectScheduleType(event){

  //   console.log(event);
  //   if(event == 'Standard'){
  //     var data = {
  //       name: ''
  //     }
  //     this.siteAccessRolesService.getAllEmployees(data)
  //       .subscribe((res: any) => {
  //         console.log("employees", res);
  //         this.employeeData = res.data;
  //         this.employeeData.forEach(element => {
  //           this.employeeIds.push(element._id)
  //         });
  //       console.log(this.employeeIds);
        
  //       },
  //         (err) => {
  //           console.log(err);
  
  //         })
  //   }

    
  // }

   // Author: Saiprakash G, Date: 17/06/19
    // Create work postion scheduler

  createWorkPosition(createEmployeeForm){

    var data: any = {
      scheduleTitle: this.positionSchedule.scheduleTitle,
      scheduleStartDate: this.positionSchedule.scheduleStartDate,
      type: this.positionSchedule.type,
      jobTitle: this.positionSchedule.jobTitle,
      daysOfWeek: this.selectedDays,
      weekApplicable: this.weekDays,
      totalHrsPerDay: this.positionSchedule.totalHrsPerDay,
      totalHrsPerWeek: this.positionSchedule.totalHrsPerWeek,
      lunchBreakTime: this.positionSchedule.lunchBreakTime
    }

    if (!this.positionSchedule.scheduleTitle) {
      this.isValid = true;
    } else if (!this.positionSchedule.scheduleStartDate) {
      this.isValid = true;
    } else if (!this.positionSchedule.type) {
      this.isValid = true;
    } else if (!this.positionSchedule.totalHrsPerDay) {
      this.isValid = true;
    } else if (!this.positionSchedule.totalHrsPerWeek) {
      this.isValid = true;
    } 
    // else if (!this.positionSchedule.lunchBreakTime) {
    //   this.isValid = true;
    // }
   
  else if(this.getPositionScheduler && this.getPositionScheduler._id){
     data._id = this.getPositionScheduler._id;
    //  data.daysOfWeek = this.allSelectedDays;
  
    this.leaveManagementService.updatePosition(data).subscribe((res:any)=>{
      console.log(res);
     
      this.message = 'Confirmed!';
      this.modalRef.hide();
      createEmployeeForm.resetForm();
      this.positionSchedule = {
        scheduleTitle: '',
        scheduleStartDate: '',
        jobTitle: '',
        // type: '',
        totalHrsPerDay: '',
        totalHrsPerWeek: '',
        lunchBreakTime: ''

      };
      this.isValid = false;
      
      this.weekDays = [];
      this.selectedDays = [];
      this.checkPostionIds = [];
     
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      this.getAllWorkPostions();
    },(err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    })
   } else {
    this.leaveManagementService.createPosition(data).subscribe((res:any)=>{
      console.log(res);
      this.message = 'Confirmed!';
      this.modalRef.hide();
      createEmployeeForm.resetForm();
      this.positionSchedule = {
        scheduleTitle: '',
        scheduleStartDate: '',
        jobTitle: '',
        // type: '',
        totalHrsPerDay: '',
        totalHrsPerWeek: '',
        lunchBreakTime: ''

      };
      this.isValid = false;
      this.weekDays = [];
      this.selectedDays = [];
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      this.getAllWorkPostions();
    },(err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    })
   }
  
   
  }

   // Author: Saiprakash G, Date: 18/06/19
  // Get all position work scheduler

  getAllWorkPostions(){
    this.leaveManagementService.getAllPosition().subscribe((res:any)=>{
      console.log(res);
      this.allPositions = res.data;
      this.positionSchedule.type = "Standard"
      
    },(err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      
    })
  }

  selectWorkPosition(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.checkPostionIds.push(id)
    }
    console.log(this.checkPostionIds);
    if(event.checked == false){
      var index = this.checkPostionIds.indexOf(id);
        if (index > -1) {
          this.checkPostionIds.splice(index, 1);
        }
        console.log(this.checkPostionIds);
    }
  }
// Author: Saiprakash G, Date: 18/06/19
// Get position work scheduler

  editWorkPosition(template1: TemplateRef<any>){
    this.selectedPostions = this.allPositions.filter(positionCheck => {
      return positionCheck.isChecked
    })

    if (this.selectedPostions.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one position work schedule to proceed","", 'error')
    } else if (this.selectedPostions.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one position work schedule to proceed","", 'error')
    } else {
      this.selectedNav = 'workSchedule';
      this.modalRef = this.modalService.show(
        template1,
        Object.assign({}, { class: 'gray modal-lg' })
      );
      this.leaveManagementService.getPosition(this.selectedPostions[0]._id).subscribe((res:any)=>{
        console.log(res);
        
         this.getPositionScheduler = res.data;
         console.log(this.getPositionScheduler);
         this.positionSchedule = this.getPositionScheduler;
         this.weekDays = this.getPositionScheduler.weekApplicable;
         this.positionSchedule.jobTitle = this.getPositionScheduler.jobTitle;
         console.log( this.positionSchedule.jobTitle);
         if(this.positionSchedule.type == 'Employee Specific'){
           this.jobTitleShow = false;
         } else {
          this.jobTitleShow = true;
         }
         
         if(this.positionSchedule.lunchBreakTime == true){
           this.lunchTime = true;
         } else {
          this.lunchTime = false;
         }
        //  this.weekDays.filter(item=>{
        //   item.edit= true;
        // });
               
         if(this.getPositionScheduler.daysOfWeek && this.getPositionScheduler.daysOfWeek.length){
           console.log(this.getPositionScheduler.daysOfWeek);
           this.selectedDays = this.getPositionScheduler.daysOfWeek;

           let index = this.getPositionScheduler.daysOfWeek.map(e =>{
            let i=this.days.findIndex(ele=>ele.name===e)
            console.log(i);
            
            if(i>=0){
              console.log('days',this.days,this.days[i]);
              
              this.days[i].isChecked=true;

            }

          } )
          

          // this.allSelectedDays = this.selectedDays.concat(this.getPositionScheduler.daysOfWeek);


        }
        // console.log(this.allSelectedDays);
        
        
      }, (err)=>{
        console.log(err);
        
      })
    }
  }
  // Author: Saiprakash G, Date: 19/06/19
 // Delete position work scheduler

  deleteWorkPosition(){
    this.selectedPostions = this.allPositions.filter(postionCheck => {
      return postionCheck.isChecked
    })
    if (this.selectedPostions.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one position work schedule to proceed","", 'error')
    } else {
     var data:any = {
       _id: this.selectedPostions
     }
      this.leaveManagementService.deletePosition(data).subscribe((res:any)=>{
        console.log(res);
        this.checkPostionIds = [];
       this.getAllWorkPostions();
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    }
  }
  // Author: Saiprakash G, Date: 24/06/19,
  // Description: Open dynamic resource model

  openDynamicResourceModel(){
    var dynamicData: any = {
      Open_Time: this.departmentSchedule.openTime,
      Close_Time: this.departmentSchedule.closeTime,
      Interval_Definition: this.departmentSchedule.intervalSpan,
      resourceInfo: this.getDepartmentSchedule
    }
    const dialogRef = this.dialog.open(DynamicResourceModelComponent, {
      width: '1200px',
      height:'700px',
      data: dynamicData
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
      if(result){
        this.timeIntervals = result.Intervals;
       
        console.log(this.timeIntervals);
        
      }   
      
    });
  }

  standardResourceInterval(value){
       console.log(value);
       
    let openTimeMS = this.timeToMs(this.timeConvertor(this.departmentSchedule.openTime));
    console.log(openTimeMS);
 
    let closeTimeMS = this.timeToMs(this.timeConvertor(this.departmentSchedule.closeTime));
    console.log(closeTimeMS);
    
    const intervalDiff = (this.departmentSchedule.intervalSpan*60)*1000;
    console.log(intervalDiff); 

    for(var i=openTimeMS; i<closeTimeMS; i=i+intervalDiff){
       
      if(i < closeTimeMS){
    
        this.timeIntervals.push({
          scheduledTime: this.msToTime(i),
          resourcesNeeded: value,
          
        })
      }
     
      for(var j=0; j<this.timeIntervals.length; j++){
        this.timeIntervals[j].interval = j+1;
      }

      console.log(this.timeIntervals);
    }
    var resourceNeeded = [];

     if(this.getDepartmentSchedule && this.getDepartmentSchedule.intervalResourcesInformation){
       console.log(this.getDepartmentSchedule);

       for(var j=0; j<this.timeIntervals.length; j++){
        this.getDepartmentSchedule.intervalResourcesInformation.forEach(element => {
          resourceNeeded.push(element.resourcesNeeded);

          this.timeIntervals[j].resourcesNeeded = resourceNeeded[j];
          this.timeIntervals[j].interval = j+1;
          
        });
        console.log(this.timeIntervals);
       }
    
     }

  }

  msToTime(i){
   
    this.minutes = Math.floor((i / (1000 * 60)) % 60);
    this.hours = Math.floor((i / (1000 * 60 * 60)) % 24);
    this.hours = (this.hours < 10) ? "0" + this.hours : this.hours;
    this.minutes = (this.minutes < 10) ? "0" + this.minutes : this.minutes;

    let time:any = ['', ':', '', ' ', '']
    if (this.hours < 12) {
        time[4] = 'AM'
    }
    else {
        time[4] = 'PM'
    }
​
    time[2] = String(this.minutes)
​
    if (time[2].length == 1) {
        time[2] = '0' + time[2]
    }
​
    time[0] = this.hours % 12 || 12
    console.log(time.join(""))
    return time.join("")
    
      // return this.hours+":"+this.minutes;

  }

  typeOfResource(event){
    console.log(event);
    if(event == "Dynamic"){
      this.dynamicResourceHide = true;
      this.standardResourceHide = false;
    } else {
      this.dynamicResourceHide = false;
      this.standardResourceHide = true;
    }
    
  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.previewEmployeeList();
  }

  getAllEmp() {
    var listallUsers;
    var postData = {
      name: ''
    }
    console.log("data comes");

    this.siteAccessRolesService.getAllEmployees(postData)
      .subscribe((res: any) => {
        console.log("resonse for all employees", res);
        listallUsers = res.data;
        for (var i = 0; i < listallUsers.length; i++) {
          this.dropdownList.push({ _id: listallUsers[i]._id, _name: listallUsers[i].personal.name.preferredName })
          
        }
        console.log( "DropDownList" ,this.dropdownList);
      },
        (err) => {
          console.log(err);

        })
  }

  previewEmployeeList(){

    var finalPreviewUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalPreviewUsers.push(element._id)
    });

    var data:any = {
      per_page: this.perPage,
      page_no: this.pageNo,
      _ids: finalPreviewUsers
    }
    this.companySettingsService.prviewEmployeeList(data).subscribe((res:any)=>{
      console.log(res);
      this.previewList = res.data
      this.count = res.total_count;
      
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 25/06/19,
  // Slect and deselect employees for include and exclude

  // onItemSelect(item: any) {
  //   this.selectedUsers = []
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   for (var i = 0; i < this.selectedItems.length; i++) {
  //     this.selectedUsers.push(this.selectedItems[i].item_id);
  //   }
  //   console.log('selcetd users', this.selectedUsers);


  // }
  // OnItemDeSelect(item: any) {
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
  //   console.log('selcetd users', this.selectedUsers);

  // }
  // onSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];
  //   for (var i = 0; i < items.length; i++) {
  //     this.selectedUsers.push(items[i].item_id);
  //   }
  //   console.log("users", this.selectedUsers);


  // }
  // onDeSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];

  //   console.log('selcetd users', this.selectedUsers);

  // }
    // Author: Saiprakash G, Date: 24/06/19,
  // Description: Get stricture fields for roles

  getFieldsForRoles() {
    this.companySettingsService.getFieldsForRoles()
      .subscribe((res: any) => {
        console.log("12122123", res);
        this.structureFields = res.data;
        this.structureFields[0].isChecked = false;
        if (this.getDepartmentSchedule.eligibility_criteria && this.getDepartmentSchedule.eligibility_criteria.selected_field) {
          let index = this.structureFields.findIndex(e => e.name === this.getDepartmentSchedule.eligibility_criteria.selected_field)
          console.log(index, 'index')
          if (index >= 0) {
            this.structureFields[index].isChecked = true;
          }
        }

      }, err => {
        console.log(err);

      })
  }


  resourceAvailableCount(){
  
    for (var k=0; k<this.timeIntervals.length; k++ ){
      console.log(k);
      var count=0;
      this.employeeList.forEach(data=>{
        if(data.status[k]=='Working' || data.status[k]=='Working/Lunch'){
  count++;
        } 
      })
      this.timeIntervals[k].resourceAvailable=count;
    
    }

  }
    // Author: Saiprakash G, Date: 24/06/19,
  // Description: Get structure sub fields

  getStructureSubFields(fields, isChecked, sub_fields) {
    console.log(fields, isChecked);
    this.fieldsName = fields;
    // this.eligibilityArray = [];
    this.checked = isChecked
    if (isChecked == true) {

      this.structureFields.forEach(element => {
        if (element.name !== fields) {
          element.isChecked = false
        }
      })
      this.companySettingsService.getStuctureFields(fields, 0)
        .subscribe((res: any) => {

          this.EmployeeListSummary();
          console.log(res, name);

          this.StructureValues = res.default;
          this.customValues = res.custom;

          console.log(this.StructureValues);
          if (sub_fields && sub_fields.length) {
            sub_fields.forEach(element => {
              let structureIndex = this.StructureValues.findIndex(e => e.name === element);
              let customValuesIndex = this.customValues.findIndex(e => e.name === element);

              if (structureIndex >= 0) {
                this.StructureValues[structureIndex].isChecked = true;
              }
              if (customValuesIndex >= 0) {
                this.customValues[customValuesIndex].isChecked = true;
              }
            });
          }

        }, err => {
          console.log(err);

        })
    }
  }
  // Author: Saiprakash G, Date: 15/06/19
  // Sub structure fields

  subFieldsStructure($event, value) {
    console.log($event, value);
    // this.eventName = $event;
    // this.eligibilityArray = [];
    if ($event.checked == true) {
      this.eligibilityArray.push(value)
    }
    console.log(this.eligibilityArray, "123321");
    if ($event.checked == false) {
      var index = this.eligibilityArray.indexOf(value);
      if (index > -1) {
        this.eligibilityArray.splice(index, 1);
      }
      console.log(this.eligibilityArray);

    }
    this.EmployeeListSummary();
  }

  // Author: Saiprakash G, Date: 25/06/19,
  // Description: Employee list summary

  EmployeeListSummary() {
    var postdata = {
      field: this.fieldsName,
      chooseEmployeesForRoles: this.eligibilityArray
    }
    console.log(postdata);
    this.siteAccessRolesService.chooseEmployeesData(postdata)
      .subscribe((res: any) => {
        console.log("emp data", res);
        this.dropdownList = []
        for (var i = 0; i < res.data.length; i++) {
          // if (this.eventName.checked == true){
          this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName })
          // }
        }
        console.log(this.dropdownList, "employee dropdown");
        var result = this.selectedUsers.filter(item => (this.dropdownList.findIndex(e => e.item_id === item)) >= 0);
        this.selectedUsers = result
        console.log(result, this.selectedUsers);

      },
        (err) => {
          console.log(err);

        })
  }
  // Author: Saiprakash G, Date: 21/06/19,
  // Description: Selected active tabs

  chooseFieldss(event: any) {
    this.selectedNav = event;
    if (event == "defineTeam") {
      this.defineTeam = true;
      this.selectEmployees = false;
      this.scheduleBuilder = false;
      this.recap2 = false;
    
    }
    else if (event == "selectEmployees") {
      this.defineTeam = false;
      this.selectEmployees = true;
      this.scheduleBuilder = false;
      this.recap2 = false;
     
    }
    else if (event == "scheduleBuilder") {
      this.defineTeam = false;
      this.selectEmployees = false;
      this.scheduleBuilder = true;
      this.recap2 = false;
      this.dayWiseEmployeeStatus(this.departmentSelectDays[0]);
    }
    else if(event == "recap2"){
      this.defineTeam = false;
      this.selectEmployees = false;
      this.scheduleBuilder = false;
      this.recap2 = true;
    }
  }

  clickSelectEmployees(){
    this.selectedNav = "selectEmployees"
  }

  // Author: Saiprakash G, Date: 24/07/19,
  // Description: Get position scheduler for scheduler builder tab
 
  clickSchedulerBuilder(){
    
    this.selectedNav = "scheduleBuilder";
    this.dayWiseEmployeeStatus(this.departmentSelectDays[0]);


  }


  // Author: Saiprakash G, Date: 24/07/19,
  // Description: Assigned status of employee for different time intervals

  statusOfEmployee(day){
 
     this.employeeList.forEach(element => {


    // let ds_Obj:any={
    //   userId:element._id,
    //   day: this.departmentDay,
    //   intervalsInfo:[]
    // }

      element.timeIntervals = this.timeIntervals;
      element.status = [];


      
      element.position_work_schedule_id.weekApplicable.forEach(days => {
        element.position_work_schedule_id.time = days;

          console.log(days);
          if(days.day == day){
            console.log(days.shiftStartTime,days.shiftEndTime,days.lunchStartTime);
            this.employeeStartTime = days.shiftStartTime;
            this.employeeLunchTime = days.lunchStartTime;
            this.employeeEndTmie = days.shiftEndTime;
            if(days.shiftStartTime){
              this.startTime = this.timeToMs(this.timeConvertor(days.shiftStartTime));
            }
            if(days.lunchStartTime){
              this.lunchStartTime = this.timeToMs(this.timeConvertor(days.lunchStartTime));
            }
            if(days.lunchEndTime){
              this.lunchEndTime = this.timeToMs(this.timeConvertor(days.lunchEndTime));
            }
            if(days.shiftEndTime){

              this.endTime = this.timeToMs(this.timeConvertor(days.shiftEndTime));
            }
           
            console.log(this.startTime,this.endTime);
           
          }
          
          
        })
       
        element.timeIntervals.forEach(intervals => {
          intervals.resourceAvailable = this.employeeList.length;
          this.newTime = this.timeToMs(this.timeConvertor(intervals.scheduledTime));
          console.log(this.newTime);
          if(this.startTime > this.newTime){
            element.status.push("Not Working");
          }
          
         else if(this.newTime >= this.startTime && this.newTime < this.lunchStartTime){
            element.status.push("Working")
            
          } else if(this.newTime >= this.lunchStartTime && this.newTime < this.lunchEndTime){
            if((this.lunchEndTime - this.lunchStartTime)>1800000){
              element.status.push("Lunch");
            } else if((this.lunchEndTime - this.lunchStartTime) <= 1800000 ){
              element.status.push("Working/Lunch");
            }
            
           
          } else if(this.newTime >= this.lunchEndTime && this.newTime <= this.endTime){
            element.status.push("Working");
            
          } else if(this.endTime < this.newTime){
            element.status.push("Not Working");
          }
          
         console.log(element.status);

        });
      


        // for(var n=0; n<this.timeIntervals.length; n++){
        //   let interval_status_info:any = {
        //     status :element.status[n],
        //    interval: this.timeIntervals[n].interval,
        //   scheduledTime : element.timeIntervals[n].scheduledTime
    
        //   }
        //   ds_Obj.intervalsInfo.push(interval_status_info)
        // }
        // this.departmentEmployeesScheduleInfo.push(ds_Obj)

      });
   
     console.log(this.employeeList);
    //  console.log(this.departmentEmployeesScheduleInfo);
     
     this.resourceAvailableCount();
  }


  // Author: Saiprakash G, Date: 25/07/19,
  // Description: Day wise employee status showed

  dayWiseEmployeeStatus(day){
  console.log(this.weekDayNames);
  

if(this.weekDayNames.length){
  let dayindex=this.weekDayNames.findIndex(e=>e.day==day);
  console.log('dayindex',dayindex,this.weekDayNames)
  if(dayindex<0){
    this.getNewEmployList(day);
  }else{
    let oldEmploy=this.weekDayNames[dayindex].employList
    this.employeeList=oldEmploy;
  }
}else{
  this.getNewEmployList(day);

}

  }

  getNewEmployList(day){

    console.log(day);
    this.departmentDay = day;
    this.employeeList = [];
    if(this.getDepartmentSchedule && this.getDepartmentSchedule._id){
      this.getUsersList();
      
    }

else {
       
      for(let employee of this.previewList){
        if(employee.position_work_schedule_id){
         if (employee.position_work_schedule_id.daysOfWeek.includes(day)) {
           this.employeeList.push(employee)
       }
        }
      
      }
      console.log(this.employeeList);
      this.statusOfEmployee(day)
  }
     
  }

  // Author: Saiprakash G, Date: 23/07/19,
  // Description: Converted time to milliseconds

  timeToMs(time){

    let milliSec=  Number(time.split(':')[0]) * 60 * 60 * 1000 + Number(time.split(':')[1]) * 60 * 1000;
    return milliSec;
}

timeConvertor(time) {
  time = time.toUpperCase().replace(' ', '')
  var PM = time.match('PM') ? true : false
 
  time = time.split(':')
  var min = parseInt(time[1],10)
  
  if (PM) {
      var hour = parseInt(time[0],10)
      if (hour < 12) {
          hour += 12 
      }
      // var sec = time[2].replace('PM', '')
  } else {
      var hour = parseInt(time[0], 10)
      if(hour == 12) {
          hour = 0
      }
      // var sec = time[2].replace('AM', '')       
  }
  
  // console.log(hour + ':' + min + ':' + sec)
  console.log(hour + ':' + min);

  return hour + ':' + min
  
}


  // Author: Saiprakash G, Date: 23/07/19,
  // Description: Drag and drop of employee status for particular time intervals

  drop(event: CdkDragDrop<string[]>, j) {
   
     moveItemInArray(this.employeeList[j].status, event.previousIndex, event.currentIndex);
     console.log("drag event", event, this.employeeList[j].status);

     this.resourceAvailableCount();
     
  }

  goToDept(){
    this.selectedNav = "defineTeam"
  }

  recapTab(){
    this.selectedNav = "recap2"
  }
  weekDayNames:Array<any>=[]
  dayWiseSchedules(employList){
    const emplist=employList;
    let daynames:any={
      day:this.departmentDay,
      employList:emplist
    }
    let dayIndex=this.weekDayNames.findIndex(e=>e.day==this.departmentDay);
    if(dayIndex<0){
      this.weekDayNames.push(daynames)
    }else{
      this.weekDayNames[dayIndex]=daynames
    }
    console.log('weekDayNames',this.weekDayNames)
  
  

    if(this.getDepartmentSchedule && this.getDepartmentSchedule._id){
      this.employeeList.forEach(element => {

        element.scheduleStatus = this.scheduleStatus;
       
          for(var i=0; i<element.scheduleStatus.length; i++){
            if(element.scheduleStatus[i].userId._id.indexOf(element._id)>=0){
              let ds_Obj:any={
                userId:element._id,
                _id: element.scheduleStatus[i]._id,
                day: this.departmentDay,
                intervalsInfo:[]
              }
           
    
        for(var n=0; n<this.timeIntervals.length; n++){
        
          let interval_status_info:any = {
            status :element.status[n],
           interval: this.timeIntervals[n].interval,
          scheduledTime : this.timeIntervals[n].scheduledTime
    
          }
          ds_Obj.intervalsInfo.push(interval_status_info)
        }
       
       let userFilter = this.departmentEmployeesScheduleInfo.filter(x=>x.userId == element._id);
       let dayFilter = userFilter.filter(x=>x.day == this.departmentDay)
       if (!dayFilter[0]) {
        this.departmentEmployeesScheduleInfo.push(ds_Obj)
       } else {
        let index = this.departmentEmployeesScheduleInfo.indexOf(dayFilter[0])
        this.departmentEmployeesScheduleInfo[index] = ds_Obj
        console.log(index);
    }
      
  }
  }
      });
    }

    else {
      this.employeeList.forEach(element => {

              let ds_Obj:any={
                userId:element._id,
                day: this.departmentDay,
                intervalsInfo:[]
              }
           
    
        for(var n=0; n<this.timeIntervals.length; n++){
        
          let interval_status_info:any = {
            status :element.status[n],
           interval: this.timeIntervals[n].interval,
          scheduledTime : this.timeIntervals[n].scheduledTime
    
          }
          ds_Obj.intervalsInfo.push(interval_status_info)
        }
       
       let userFilter = this.departmentEmployeesScheduleInfo.filter(x=>x.userId == element._id);
       let dayFilter = userFilter.filter(x=>x.day == this.departmentDay)
       if (!dayFilter[0]) {
        this.departmentEmployeesScheduleInfo.push(ds_Obj)
       } else {
        let index = this.departmentEmployeesScheduleInfo.indexOf(dayFilter[0])
        this.departmentEmployeesScheduleInfo[index] = ds_Obj
        console.log(index);
    }

      });
    }
   
    this.swalAlertService.SweetAlertWithoutConfirmation("Successfully saved day wise status", "","success");

    
    console.log(this.departmentEmployeesScheduleInfo);

   
  }

  // Author: Saiprakash G, Date: 26/07/19,
  // Description: Create and update department work schedule

  createDepartmentSchedule(){

    var intervalResourcesInformation = [];
    // var departmentEmployeesScheduleInfo = [];
    for(var n=0; n<this.timeIntervals.length; n++){
      intervalResourcesInformation.push({
        interval: this.timeIntervals[n].interval,
        scheduledTime: this.timeIntervals[n].scheduledTime,
        resourcesNeeded: this.timeIntervals[n].resourcesNeeded,
        resourcesAvailable: this.timeIntervals[n].resourceAvailable
      })
     
    }
  //  this.employeeList.forEach(element => {

  //   let ds_Obj:any={
  //     userId:element._id,
  //     day: this.departmentDay,
  //     intervalsInfo:[]
  //   }

  //   for(var n=0; n<this.timeIntervals.length; n++){
    
  //     let interval_status_info:any = {
  //       status :element.status[n],
  //      interval: this.timeIntervals[n].interval,
  //     scheduledTime : element.timeIntervals[n].scheduledTime

  //     }
  //     ds_Obj.intervalsInfo.push(interval_status_info)
  //   }
    
  //  this.departmentEmployeesScheduleInfo.push(ds_Obj)
  // });
     

   console.log(intervalResourcesInformation);
   console.log(this.departmentEmployeesScheduleInfo);
 

    var data:any = {
      departmentScheduleName: this.departmentSchedule.departmentScheduleName,
      openTime: this.departmentSchedule.openTime,
      closeTime: this.departmentSchedule.closeTime,
      intervalSpan: this.departmentSchedule.intervalSpan,
      resourceType: this.departmentSchedule.resourceType,
      standardNoOfResourcesPerInterval: this.departmentSchedule.standardNoOfResourcesPerInterval,
      daysOfWeekApplicable: this.departmentSelectDays,
      startDate: this.departmentSchedule.startDate,
      endDate: this.departmentSchedule.endDate,
      intervalResourcesInformation: intervalResourcesInformation,
      departmentEmployeesScheduleInfo: this.departmentEmployeesScheduleInfo
    }
    if(this.getDepartmentSchedule && this.getDepartmentSchedule._id){
      
      data._id = this.getDepartmentSchedule._id;
      // data.specific_employee.include = this.selectedItems
      this.leaveManagementService.updateDepartment(data).subscribe((res:any)=>{
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        this.getAllDepartmentSchedules();
        this.modalRef.hide();
        this.getDepartmentSchedule._id = "";
        this.checkDeptScheduleIds = [];
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    } else {
      this.leaveManagementService.createDepartment(data).subscribe((res:any)=>{
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        this.getAllDepartmentSchedules();
        this.modalRef.hide();
      },(err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    }
   
    
  }
    // Author: Saiprakash G, Date: 27/06/19,
  // Description: Get all department work schedules

   getAllDepartmentSchedules(){
     this.leaveManagementService.getAllDepartment().subscribe((res:any)=>{
       console.log(res);
       this.allDepartments = res.result;
       
     },(err)=>{
       console.log(err);
       
     })
   }

    // Author: Saiprakash G, Date: 27/06/19,
  // Description: Edit single department work schedule

   editDepartmentSchedule(template1: TemplateRef<any> ){
    this.selectedDepartments = this.allDepartments.filter(positionCheck => {
      return positionCheck.isChecked
    })

    if (this.selectedDepartments.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one department work schedule to proceed","", 'error')
    } else if (this.selectedDepartments.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one department work schedule to proceed","", 'error')
    } else {
      this.selectedNav = 'defineTeam';
      this.modalRef = this.modalService.show(
        template1,
        Object.assign({}, { class: 'gray modal-lg' })
      );
      this.leaveManagementService.getDepartmnet(this.selectedDepartments[0]._id).subscribe((res:any)=>{
        console.log(res);
        
         this.getDepartmentSchedule = res.data.departmentSchedule;
        console.log(this.getDepartmentSchedule);
        this.departmentSchedule = this.getDepartmentSchedule;
        if(this.getDepartmentSchedule.resourceType == 'Dynamic'){
          this.dynamicResourceHide = true;
          this.standardResourceHide = false;
        } else {
          this.dynamicResourceHide = false;
          this.standardResourceHide = true;
        }
         
        this.timeIntervals = this.getDepartmentSchedule.intervalResourcesInformation;
        console.log(this.timeIntervals);
       
        this.scheduleStatus = res.data.departmentEmployeesScheduleInfo;
        this.assignedEmployees = res.data.assignedEmployees;
        this.departmentSelectDays = this.getDepartmentSchedule.daysOfWeekApplicable;
        // this.departmentDay =  this.departmentSelectDays[0];

        var data:any = {
          per_page: this.perPage,
          page_no: this.pageNo,
          _ids: res.data.assignedEmployees
        }
        this.companySettingsService.prviewEmployeeList(data).subscribe((res:any)=>{
          console.log(res);
          this.previewList = res.data
          this.count = res.total_count;

          for (var i = 0; i < this.previewList.length; i++) {
            this.selectedUsers.push({ _id: this.previewList[i]._id, _name: this.previewList[i].personal.name.preferredName })
            console.log(this.selectedUsers, "selectedUsers");
          
          
          }
          
          
        },(err)=>{
          console.log(err);
          
        })
       
       
        
    
         if(this.getDepartmentSchedule.daysOfWeekApplicable && this.getDepartmentSchedule.daysOfWeekApplicable.length){
          this.departmentSelectDays = this.getDepartmentSchedule.daysOfWeekApplicable;
           console.log(this.getDepartmentSchedule.daysOfWeekApplicable);
          
          let index = this.getDepartmentSchedule.daysOfWeekApplicable.map(e =>{
            let i=this.days2.findIndex(ele=>ele.name===e)
            if(i>=0){
              this.days2[i].isChecked=true;
              
            }

          } );

     

        // this.allSelectedDepartmentDays = this.departmentSelectDays.concat(this.getDepartmentSchedule.daysOfWeekApplicable)

        }
        if(this.getDepartmentSchedule.eligibility_criteria && this.getDepartmentSchedule.eligibility_criteria.sub_fields.length){
          this.eligibilityArray = this.getDepartmentSchedule.eligibility_criteria.sub_fields;
          this.getStructureSubFields(this.getDepartmentSchedule.eligibility_criteria.selected_field, true, this.getDepartmentSchedule.eligibility_criteria.sub_fields);
    
        }
        this.getFieldsForRoles();
        if (this.getDepartmentSchedule.specific_employee) {
          this.selectedItems = this.getDepartmentSchedule.specific_employee.include;
  
        }
        

        
      }, (err)=>{
        console.log(err);
        
      })
    }
   }


   getUsersList(){

    for(let employee of this.previewList){
      if(employee.position_work_schedule_id){
       if (employee.position_work_schedule_id.daysOfWeek.includes(this.departmentDay)) {
         this.employeeList.push(employee)
     }
      }
    
    }
    for(var i=0; i<this.employeeList.length; i++){
      this.employeeList[i].status = [];
      for(var j=0; j< this.scheduleStatus.length; j++){
        if(this.scheduleStatus[j].userId._id.indexOf(this.employeeList[i]._id)>=0){
          if(this.scheduleStatus[j].day == this.departmentDay){
            this.scheduleStatus[j].intervalsInfo.forEach(element => {
              this.employeeList[i].status.push(element.status)
            });
          } else {
             this.statusOfEmployee(this.departmentDay);
          }
         
        }
       
      } 
    }

       this.employeeList.forEach(element => {
            element.position_work_schedule_id.weekApplicable.forEach(days => {
          element.position_work_schedule_id.time = days;
          });

        })
        
          console.log(this.employeeList);
    
          this.resourceAvailableCount();

  

   }

   changeInterval(value){
     console.log("interval", value);
     
     if(this.getDepartmentSchedule && this.getDepartmentSchedule._id){
       if(value == 30 || value == 60 || value == 90){
        this.getDepartmentSchedule = "";
        this.timeIntervals = []
        this.employeeList = [];
       }
      
     }
console.log("123",this.getDepartmentSchedule, "1234", this.employeeList);

   }

   editTime(time){
     console.log(time);
     if(this.getDepartmentSchedule && this.getDepartmentSchedule._id){
      if(time == time){
       this.getDepartmentSchedule = "";
       this.timeIntervals = []
       this.employeeList = [];
      }
     
    }
     

   }
  // Author: Saiprakash G, Date: 27/06/19,
  // Description: Delete department work schedule
  
   deleteDepartmentSchedule(){
    this.selectedDepartments = this.allDepartments.filter(postionCheck => {
      return postionCheck.isChecked
    })
    if (this.selectedDepartments.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one department work schedule to proceed","", 'error')
    } else {
     var data:any = {
       _id: this.checkDeptScheduleIds
     }
      this.leaveManagementService.deleteDepartment(data).subscribe((res:any)=>{
        console.log(res);
       this.getAllDepartmentSchedules();
       this.checkDeptScheduleIds = [];
       this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    }
   }

  // Author: Saiprakash G, Date:30/08/19,
  // Description: Preview employee list
    
   onSelectUsers(selectSpecificPerson){
    console.log("users",selectSpecificPerson);
   
    console.log('selcetd users', this.selectedUsers);
     this.previewEmployeeList();
    
  }

  numbers(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  checkedDeptSchedules(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.checkDeptScheduleIds.push(id)
    }
    console.log(this.checkDeptScheduleIds);
    if(event.checked == false){
      var index = this.checkDeptScheduleIds.indexOf(id);
        if (index > -1) {
          this.checkDeptScheduleIds.splice(index, 1);
        }
        console.log(this.checkDeptScheduleIds);
    }

  }


}
