import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkSchedulesRouting } from './work-schedules.routing';
import { WorkSchedulesComponent } from './work-schedules.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AmazingTimePickerModule } from 'amazing-time-picker'; 
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DynamicResourceModelComponent } from './dynamic-resource-model/dynamic-resource-model.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AngularDualListBoxModule } from 'angular-dual-listbox';


@NgModule({
  imports: [
    CommonModule,
    WorkSchedulesRouting,
    BsDatepickerModule.forRoot(),
    MatSelectModule,
    MaterialModuleModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    AmazingTimePickerModule,
    DragDropModule,MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,MatCheckboxModule,
    NgxMaterialTimepickerModule,
    AngularDualListBoxModule

  ],
  
  declarations: [WorkSchedulesComponent, DynamicResourceModelComponent],
  entryComponents: [DynamicResourceModelComponent]
})
export class WorkSchedulesModule { }
