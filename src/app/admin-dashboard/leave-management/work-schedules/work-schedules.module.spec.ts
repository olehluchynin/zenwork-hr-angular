import { WorkSchedulesModule } from './work-schedules.module';

describe('WorkSchedulesModule', () => {
  let workSchedulesModule: WorkSchedulesModule;

  beforeEach(() => {
    workSchedulesModule = new WorkSchedulesModule();
  });

  it('should create an instance', () => {
    expect(workSchedulesModule).toBeTruthy();
  });
});
