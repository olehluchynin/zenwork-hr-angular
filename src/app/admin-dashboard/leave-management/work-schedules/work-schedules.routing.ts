import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkSchedulesComponent } from './work-schedules.component';
// import { BlackOutDatesComponent } from './black-out-dates.component';


const routes: Routes = [
 
    {path:'',component:WorkSchedulesComponent }
]
    



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WorkSchedulesRouting { }