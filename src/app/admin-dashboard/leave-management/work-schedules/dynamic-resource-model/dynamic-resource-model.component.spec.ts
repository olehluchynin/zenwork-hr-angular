import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicResourceModelComponent } from './dynamic-resource-model.component';

describe('DynamicResourceModelComponent', () => {
  let component: DynamicResourceModelComponent;
  let fixture: ComponentFixture<DynamicResourceModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicResourceModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicResourceModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
