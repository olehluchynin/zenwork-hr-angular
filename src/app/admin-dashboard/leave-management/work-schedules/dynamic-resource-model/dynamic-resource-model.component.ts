import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as moment from 'moment';
import { SwalAlertService } from '../../../../services/swalAlert.service';



@Component({
  selector: 'app-dynamic-resource-model',
  templateUrl: './dynamic-resource-model.component.html',
  styleUrls: ['./dynamic-resource-model.component.css']
})
export class DynamicResourceModelComponent implements OnInit {
  public openTime:any;
  public closeTime:any;
  public intervalDefinition:any;
  numberOfResources:any;

  public resources = [1,2,3,4,5,6,7,8,9,10,11];
  public timeDiff:any;
  public startTime:any;
  public endTime:any;
  public intervals = [];
  public setTimeIntervals = [];
  hours:any;
  minutes:any;
  interval:any;
  number:any;
  openTimeMS:any;
  closeTimeMS:any;


  constructor(
    public dialogRef: MatDialogRef<DynamicResourceModelComponent>,
    private swalAlertService: SwalAlertService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit() {
    
    console.log(this.data);
    if(this.data){
      if(this.data.Open_Time){
        this.openTime = this.data.Open_Time;
        var OT = this.openTime;
        this.timeConvertor(OT);
        console.log(this.timeConvertor(OT));
        
        this.openTimeMS = this.timeToMs(this.timeConvertor(OT));
        console.log(this.openTimeMS);
      }
     if(this.data.Close_Time){
      this.closeTime = this.data.Close_Time;
      var CT = this.closeTime;
      this.timeConvertor(CT);
      this.closeTimeMS = this.timeToMs(this.timeConvertor(CT));
      console.log(this.closeTimeMS);
     }
      
      
      
      this.intervalDefinition = this.data.Interval_Definition;
      const intervalDiff = (this.intervalDefinition*60)*1000;
      console.log(intervalDiff); 

      for(var i=this.openTimeMS; i<this.closeTimeMS; i=i+intervalDiff){
         
        if(i < this.closeTimeMS){
      
          this.intervals.push({
            scheduledTime: this.msToTime(i)
          })
        }
        console.log(this.intervals);
      }
      var resourceNeeded = [];

       if(this.data.resourceInfo && this.data.resourceInfo.intervalResourcesInformation){
         console.log(this.data.resourceInfo);

         for(var j=0; j<this.intervals.length; j++){
          this.data.resourceInfo.intervalResourcesInformation.forEach(element => {
            resourceNeeded.push(element.resourcesNeeded);

            this.intervals[j].resourcesNeeded = resourceNeeded[j];
            this.intervals[j].interval = j+1;
            
          });
          console.log(this.intervals);
         }
      
       }
     

     
    }

 
  }


  timeConvertor(time) {
    time = time.toUpperCase().replace(' ', '')
    var PM = time.match('PM') ? true : false
    
    time = time.split(':')
    var min = parseInt(time[1],10)
    
    if (PM) {
        var hour = parseInt(time[0],10)
        if (hour < 12) {
            hour += 12 
        }
        // var sec = time[2].replace('PM', '')
    } else {
        var hour = parseInt(time[0], 10)
        if(hour == 12) {
            hour = 0
        }
        // var sec = time[2].replace('AM', '')       
    }
    
    // console.log(hour + ':' + min + ':' + sec)
    console.log(hour + ':' + min);

    return hour + ':' + min
    
  }

  timeToMs(time){

    let milliSec=  Number(time.split(':')[0]) * 60 * 60 * 1000 + Number(time.split(':')[1]) * 60 * 1000;
    return milliSec;
}

  // Author: Saiprakash G, Date: 25/06/19,
  // Description: Converted milliseconds to time
  
  msToTime(i){
   
    this.minutes = Math.floor((i / (1000 * 60)) % 60);
    this.hours = Math.floor((i / (1000 * 60 * 60)) % 24);
    this.hours = (this.hours < 10) ? "0" + this.hours : this.hours;
    this.minutes = (this.minutes < 10) ? "0" + this.minutes : this.minutes;

    let time:any = ['', ':', '', ' ', '']
    if (this.hours < 12) {
        time[4] = 'AM'
    }
    else {
        time[4] = 'PM'
    }
​
    time[2] = String(this.minutes)
​
    if (time[2].length == 1) {
        time[2] = '0' + time[2]
    }
​
    time[0] = this.hours % 12 || 12
    console.log(time.join(""))
    return time.join("")
    
      // return this.hours+":"+this.minutes;

  }

 
  // Author: Saiprakash G, Date: 25/06/19,
  // Description: Selected number of resources

  selectNumberOfResources(number,interval,i){
    console.log(number,interval);
    this.number = number;

    this.interval = interval;
    var intervalNumber = [];
    for(var j=0; j<this.intervals.length; j++){
      intervalNumber.push(j+1)
        this.intervals[j].interval = intervalNumber[j];
        
      console.log(this.intervals);
     }


  }

  setIntervalAndResources(){
    // if(this.intervals){
    //   for(var i=0; i<this.intervals.length; i++){
    //     if(this.intervals[i].resourceNeeded == this.intervals.length){
    //       this.dialogRef.close({
    //         Intervals: this.intervals
    //       })
    //     } else {
    //       this.swalAlertService.SweetAlertWithoutConfirmation( "Select all number of resources","", "error");
    //     }
    //   }
    // }

    this.dialogRef.close({
      Intervals: this.intervals
    })
    
  }


}
