import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimePoliciesComponent } from './time-policies.component';

describe('TimePoliciesComponent', () => {
  let component: TimePoliciesComponent;
  let fixture: ComponentFixture<TimePoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimePoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimePoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
