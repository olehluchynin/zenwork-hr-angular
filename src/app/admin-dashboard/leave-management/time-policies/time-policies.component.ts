import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MyInfoService } from '../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../services/accessLocalStorage.service';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TierLevelModelComponent } from './tier-level-model/tier-level-model.component';
import { SwalAlertService } from '../../../services/swalAlert.service';


declare var jQuery: any;



@Component({
  selector: 'app-time-policies',
  templateUrl: './time-policies.component.html',
  styleUrls: ['./time-policies.component.css']
})
export class TimePoliciesComponent implements OnInit {


  public companyId: any;
  public getAllStructureFields: any;
  public customPolicyType = [];
  public defaultPolicyType = [];
  public allPolicies = [];
  public customTimeEarned = [];
  public defaultTimeEarned = [];
  public allTimeEarned = [];
  public customPayFrequency = [];
  public defaultPayFrequency = [];
  public allPayFrequency = [];
  public eligibleGroups: any;
  public eligibleGroupsArray = [];
  public employeeList = [];
  public getAllPolicies = [];
  public totalLength: any;
  public tierLevelId: any;
  public allTierLevels = [];
  public selectedTierLevels = [];
  public tierLevelData: any;
  public selectedPolicyType = [];
  public getPolicyData: any;
  public getTierLevelData: any;
  public allGroups: any;
  public selectedGroups = [];
  public selectedGroupNames = [];
  public leaveEligibilityGroups = [
    // {
    //   isChecked: false
    // }
  ];
  public groupCheck: any;
  public showField: boolean = true;
  public showField1: boolean = true;
  public showField2: boolean = true;
  public showField3: boolean = true;
  public showField4: boolean = true;
  public tab1: boolean = false;
  public tab2: boolean = false;
  public tab3: boolean = false;
  public selectedNav: any;
  public pageNo: any;
  public perPage: any;
  public count: any;
  public files: any;
  public disableDateOfHire:boolean = false;
  public disablefirstMonth:boolean = false;
  public disableFollowingDOH:boolean = false;
  public disableAfter:boolean = false;
  public disableAfterDays:boolean = false;
  public disableTimerBased:boolean = false;
  public disableSelectDays:boolean = false;
  public disableDays:boolean = false;
  public disableTimerDays:boolean = false;
  public disableOther:boolean = false;
  public groups = [
    { name: 'Full Time', isChecked: false },
    { name: 'Part Time', isChecked: false },
    { name: 'Retirees', isChecked: false },

  ]

  public timePolicy: any = {
    policytype: '',
    policy_effective_date: '',
    selectall_eligibilitygroup: false,
    dateOfHire: false,
    firstMonth: false,
    followingDOH: false,
    timerBased: '',
    selectDays: false,
    days: '',
    after: false,
    afterDays: '',
    timerDays: '',
    others: false,
    policyterm: '',
    policytermdays: '',
    timeearned: '',
    pay_period_freq: '',
    dayofweek: '',
    dateofMonth: '',
    dayofmonth: '',
    yearsofservices: '',
    negativebalance: '',
    neg_max_hrs: '',
    expiretime: '',
    expireoptions: '',
    expiredate: ''
  }

  public lifeEvent: any = {
    dateOfHire: false,
    firstMonth: false,
    followingDOH: false,
    timerBased: '',
    selectDays: false,
    days: '',
    after: false,
    afterDays: '',
    timerDays: '',
    others: false,
  }
  @ViewChild('openEdit') openEdit: ElementRef;
  @ViewChild('policy_type') policy_type: ElementRef;
  @ViewChild('time_earned') time_earned: ElementRef;
  @ViewChild('recap') recap: ElementRef;

  constructor(private router: Router,
    private myInfoService: MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private leaveManagementService: LeaveManagementService,
    public dialog: MatDialog,
    private swalAlertService: SwalAlertService,
  ) { 
    this.selectedNav = "tab1"
    console.log(this.selectedNav);
    
  }



  ngOnInit() {
    this.pageNo = 1;
    this.perPage = 10;
    
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.getStandardAndCustomStructureFields();
    this.getAllLeaveEligibilityGroups();
    this.getAllTypePolicies();
    this.clickTimeEarnerd();
  }
  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.clickTimeEarnerd();
  }

  dateOfHire(value,name){
   console.log(value,name);
      if(value == true && name == 'DateOfHire' ){
        this.disableDateOfHire = false;
        this.disablefirstMonth = true;
        this.disableFollowingDOH = true;
        this.disableAfter = true;
        this.disableAfterDays = true;
        this.disableTimerBased = true;
        this.disableSelectDays = true;
        this.disableDays = true;
        this.disableTimerDays = true;
        this.disableOther = true;
      }
     else if(value == true && name == 'FirstMonth'){
        this.disableDateOfHire = true;
        this.disablefirstMonth = false;
        this.disableFollowingDOH = false;
        this.disableAfter = false;
        this.disableAfterDays = false;
        this.disableTimerBased = false;
        this.disableSelectDays = true;
        this.disableDays = true;
        this.disableTimerDays = true;
        this.disableOther = true;
      }
     else if(value == true && name == 'SelectDays'){
        this.disableDateOfHire = true;
        this.disablefirstMonth = true;
        this.disableFollowingDOH = true;
        this.disableAfter = true;
        this.disableAfterDays = true;
        this.disableTimerBased = true;
        this.disableSelectDays = false;
        this.disableDays = false;
        this.disableTimerDays = false;
        this.disableOther = true;
      }
    else if(value == true && name == 'Other'){
        this.disableDateOfHire = true;
        this.disablefirstMonth = true;
        this.disableFollowingDOH = true;
        this.disableAfter = true;
        this.disableAfterDays = true;
        this.disableTimerBased = true;
        this.disableSelectDays = true;
        this.disableDays = true;
        this.disableTimerDays = true;
        this.disableOther = false;
      }
      else if(value == false){
        this.disableDateOfHire = false;
        this.disablefirstMonth = false;
        this.disableFollowingDOH = false;
        this.disableAfter = false;
        this.disableAfterDays = false;
        this.disableTimerBased = false;
        this.disableSelectDays = false;
        this.disableDays = false;
        this.disableTimerDays = false;
        this.disableOther = false;
      }
  }

  // Author: Saiprakash G, Date: 07/06/19,
  // Active tabs in time off policy

  onPolicy(event) {
    console.log(event);
    this.selectedNav = event;
    if (event == "tab1") {
      this.tab1 = true;
      this.tab2 = false;
      this.tab3 = false;

    }
    else if (event == 'tab2') {
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = false;

    }
    else if (event == 'tab3') {
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = true;

    }
  }
 // Author: Saiprakash G, Date: 07/06/19,
  // Active tab for policy type

  clickPolicyType() {
    console.log("adsfsdfs");

    this.selectedNav = "tab2";
  }

   // Author: Saiprakash G, Date: 05/06/19,
  // Active tab for tiime earned and get employee list for specific eligibility groups

  clickTimeEarnerd() {

    this.selectedNav = "tab3";
    var data: any = {
      benefitType: this.selectedGroups,
      page_no: this.pageNo,
      per_page: this.perPage
    }
    if (this.getPolicyData && this.getPolicyData._id) {
      data.benefitType = this.getPolicyData.leave_eligibility_group;
      this.leaveManagementService.getEmployeeList(data).subscribe((res: any) => {
        console.log(res);
        this.employeeList = res.EmployeeList;
        this.totalLength = res.EmployeeList.length;
        this.count = res.count;
        console.log(this.count);


      }, (err) => {
        console.log(err);

      })
    }
    else {
      this.leaveManagementService.getEmployeeList(data).subscribe((res: any) => {
        console.log(res);
        this.employeeList = res.EmployeeList;
        // this.totalLength = res.EmployeeList.length 
        this.count = res.count
      }, (err) => {
        console.log(err);

      })
    }

  }

  goToPolicy() {
    this.policy_type.nativeElement.click();
  }
  goToTimeEarned() {
    this.time_earned.nativeElement.click();
  }
 // Author: Saiprakash G, Date: 30/05/19,
  // Get all time off policies

  getAllTypePolicies() {
    this.leaveManagementService.getAllPolicies(this.companyId).subscribe((res: any) => {
      console.log(res);
      this.getAllPolicies = res.data;
     
    }, (err) => {
      console.log(err);

    })

  }
  // Author:Saiprakash, Date:28/05/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields() {
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res: any) => {
        console.log(res);
        this.getAllStructureFields = res.data;
        this.customPolicyType = this.getAllStructureFields['Policy Type'].custom;
        this.defaultPolicyType = this.getAllStructureFields['Policy Type'].default;
        this.allPolicies = this.customPolicyType.concat(this.defaultPolicyType)
        // this.customPayFrequency = this.getAllStructureFields['Pay Frequency'].custom;
        // this.defaultPayFrequency = this.getAllStructureFields['Pay Frequency'].default;
        // this.allPayFrequency = this.customPayFrequency.concat(this.defaultPayFrequency);
        this.customTimeEarned = this.getAllStructureFields['Time Earned'].custom;
        this.defaultTimeEarned = this.getAllStructureFields['Time Earned'].default;
        this.allTimeEarned = this.customTimeEarned.concat(this.defaultTimeEarned)


      }
    )
  }

  // Author: Saiprakash G, Date: 02/07/19,
  // Get all leave eligibility groups
  getAllLeaveEligibilityGroups(){
    this.leaveManagementService.getAllLeaveEligibilityGroup().subscribe((res:any)=>{
      console.log(res);
      this.leaveEligibilityGroups = res.Result;
      
    },(err)=>{
      console.log(err);
      
    })
  }

   // Author: Saiprakash G, Date: 07/06/19,
  // Restrict fields

  selectEmployeeUsage(event) {
    if (event === "Immediately") {
      this.showField = false;
    }
    else {
      this.showField = true;
    }
  }

  selectTierLevel(event) {
    if (event === true) {
      this.showField1 = true;
    }
    else {
      this.showField1 = false;
    }
  }

  selectNegativeBalance(event) {
    if (event === true) {
      this.showField2 = true;
    }
    else {
      this.showField2 = false;
    }
  }
  selectExpire(event) {
    if (event === true) {
      this.showField3 = true;
    }
    else {
      this.showField3 = false;
    }
  }
  selectExpireDate(event) {
    if (event === "Expiration Date") {
      this.showField4 = true;
    }
    else {
      this.showField4 = false;
    }
  }

  // Author: Saiprakash G, Date: 07/06/19,
  // Select all and deselect all leave eligibility groups

  async selectAll(e?) {
    this.selectedGroups = [];
    this.leaveEligibilityGroups.forEach(element => {
      element.isChecked = this.timePolicy.selectall_eligibilitygroup;
    });
    await this.leaveEligibilityGroups.forEach(element => {

      if (element.isChecked) {
        this.selectedGroups.push(element._id);
       
      }
    });
    console.log(this.selectedGroups);

  }

   // Author: Saiprakash G, Date: 07/06/19,
  // individual select leave eligibility groups

  async selecEligibilityGroup(data) {
    this.selectedGroups = [];
    if (data) {
      this.groupCheck = 0;
      this.leaveEligibilityGroups.forEach(element => {

        if (!element.isChecked) {
          this.groupCheck = 1;
        }

      });

      if (!this.groupCheck) {
        this.timePolicy.selectall_eligibilitygroup = true;
      }
    }
    else {
      this.timePolicy.selectall_eligibilitygroup = false;
    }

    await this.leaveEligibilityGroups.forEach(element => {

      if (element.isChecked) {
        this.selectedGroups.push(element._id);
      }
    });
    console.log(this.selectedGroups);

    

  }

   // Author: Saiprakash G, Date: 31/05/19,
  // Open tier level model

  tierLevelModel(): void {

    const dialogRef = this.dialog.open(TierLevelModelComponent, {
      width: '1200px',
      height: '800px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.tierLevelData = result.data;
      this.tierLevelId = result.data._id;
      this.getTierLevels();
    });

  }

  // Author: Saiprakash G, Date: 31/05/19,
  // Get tier levels

  getTierLevels() {
    this.leaveManagementService.getTierLevel(this.companyId, this.tierLevelId).subscribe((res: any) => {
      console.log(res);
      this.tierLevelData = res.data[0];
      console.log(this.tierLevelData);

      this.allTierLevels = res.data[0].tierLevel;
      // this.selectedTierLevels = this.allTierLevels.filter(checkTiers =>{
      //   return checkTiers.isChecked
      // })
      // console.log(this.selectedTierLevels);


    }, (err) => {
      console.log(err);

    })

  }

    // Author: Saiprakash G, Date: 31/05/19,
  // Checked tier levels
  checkedTierLevels(event, id) {
    console.log(event, id);
    for (var i = 0; i < this.allTierLevels.length; i++) {
      if (this.allTierLevels[i]._id == id) {
        this.allTierLevels[i].show = event.checked;
        if (event.checked == true) {
          this.selectedTierLevels.push(this.allTierLevels[i]._id)
          console.log('222222', this.selectedTierLevels);
        }
        if (event.checked == false) {
          for (var j = 0; j < this.selectedTierLevels.length; j++) {
            if (this.selectedTierLevels[j] === id) {
              this.selectedTierLevels.splice(j, 1);
              console.log(this.selectedTierLevels, "155555555");
            }
          }
        }

      }
    }
  }
  // Author: Saiprakash G, Date: 31/05/19,
  // Edit tier level model

  EditTierLevelModel() {

    const dialogRef = this.dialog.open(TierLevelModelComponent, {
      width: '1200px',
      height: '800px',
      data: this.getTierLevelData
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.getTierLevels();
    });
  }

    // Author: Saiprakash G, Date: 03/06/19,
  // Delete tier levels

  deleteTierLevels() {
    this.selectedTierLevels = this.getTierLevelData.tierLevel.filter(checkTiers => {
      return checkTiers.isChecked
    })
    if (this.selectedTierLevels.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select a tier level on to Proceed", "", 'error')
    }

    else {

      var data: any = {
        _id: this.getTierLevelData._id,
        tier_id: this.selectedTierLevels
      }
      this.leaveManagementService.deleteTierLevel(data).subscribe((res: any) => {
        console.log(res);
        var tierLevelsList = [];
        this.getTierLevelData.tierLevel.filter(element => {
          if (this.selectedTierLevels[0]._id != element._id) {
            tierLevelsList.push(element);
          }
        })
        this.allTierLevels = tierLevelsList;
        console.log(this.allTierLevels);

        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

      })
    }

  }


  // Author: Saiprakash G, Date: 29/05/19,
  // Create time off policy and update policy


  createTimeOffPolicy() {

  
    var data: any = {
      companyId: this.companyId,
      selectall_eligibilitygroup: this.timePolicy.selectall_eligibilitygroup,
      leave_eligibility_group: this.selectedGroups,
      policytype: this.timePolicy.policytype,
      employee_count: this.count,
      policy_effective_date: this.timePolicy.policy_effective_date,
      employee_policy_period: {
        new_hire: {
          hire_date: {
            selected: this.timePolicy.dateOfHire,
            date: ""
          },
          first_month: {
            selected: this.timePolicy.firstMonth,
            following_DOH: this.timePolicy.followingDOH,
            after_days: {
              selected: this.timePolicy.after,
              days: this.timePolicy.afterDays,
              timerbased: this.timePolicy.timerBased
            }
          },
          following_DOT: {
            selected: this.timePolicy.selectDays,
            days: this.timePolicy.days,
            timerbased: this.timePolicy.timerDays
          },
          others: this.timePolicy.others
        },
        life_event: {
          hire_date: {
            selected: this.lifeEvent.dateOfHire,
            date: ""
          },
          first_month: {
            selected: this.lifeEvent.firstMonth,
            following_DOH: this.lifeEvent.followingDOH,
            after_days: {
              selected: this.lifeEvent.after,
              days: this.lifeEvent.afterDays,
              timerbased: this.lifeEvent.timerBased
            }
          },
          following_DOT: {
            selected: this.lifeEvent.selectDays,
            days: this.lifeEvent.days,
            timerbased: this.lifeEvent.timerDays
          },
          others: this.lifeEvent.others
        },
        employee_usage: {
          policyterm: this.timePolicy.policyterm,
          policytermdays: this.timePolicy.policytermdays
        }
      },
      time_earned: {
        timeearned: this.timePolicy.timeearned,
        pay_period_freq: this.timePolicy.pay_period_freq,
        dayofweek: this.timePolicy.dayofweek,
        dateofMonth: this.timePolicy.dateofMonth,
        dayofmonth: this.timePolicy.dayofmonth,
        tireslevel: {
          yearsofservices: this.timePolicy.yearsofservices,
          tier_level_id: this.tierLevelId
        },
        negativebalance: this.timePolicy.negativebalance,
        neg_max_hrs: this.timePolicy.neg_max_hrs
      },
      expirationoptions: {
        expiretime: this.timePolicy.expiretime,
        expireoptions: this.timePolicy.expireoptions,
        expiredate: this.timePolicy.expiredate
      }
    }

    console.log("time-off-policy", data);

    if (this.getPolicyData && this.getPolicyData._id) {
      var updateData: any = {
        _id: this.getPolicyData._id,
        params: data,

      }


      updateData.params.leave_eligibility_group = this.getPolicyData.leave_eligibility_group;
      console.log(updateData.params.leave_eligibility_group);

      this.leaveManagementService.updateTimePolicy(updateData).subscribe((res: any) => {
        console.log(res);
        jQuery("#myModal1").modal("hide");
        this.getAllTypePolicies();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");


      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

      })
    } else {
      this.leaveManagementService.createTimeOffPolicies(data).subscribe((res: any) => {
        console.log(res);
        jQuery("#myModal1").modal("hide");
        this.getAllTypePolicies();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.Message, "", "success");


      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.Message, "", "error");

      })
    }
  }


  addTimePolicy(){
    this.selectedNav = "tab1";
    this.openEdit.nativeElement.click();
  }

    // Author: Saiprakash G, Date: 04/06/19,
  // edit time policy

  editTimePolicy() {
    this.selectedNav = "tab1"
    this.selectedPolicyType = this.getAllPolicies.filter(policyCheck => {
      return policyCheck.isChecked
    })

    if (this.selectedPolicyType.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one time off policy to Proceed", "", 'error')
    } else if (this.selectedPolicyType.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select a time off policy to Proceed", "", 'error')
    }
    else {
      this.openEdit.nativeElement.click();
      this.leaveManagementService.getTimeOffPolicy(this.selectedPolicyType[0]._id).subscribe((res: any) => {
        console.log(res);
        // this.getTierLevels();
        this.getPolicyData = res.policyData;
        if (res.tierLevelData) {
          this.getTierLevelData = res.tierLevelData[0];
          console.log(this.getTierLevelData);
        }


        this.timePolicy = this.getPolicyData;
        this.timePolicy.selectall_eligibilitygroup = this.getPolicyData.selectall_eligibilitygroup;

        if (this.getPolicyData.selectall_eligibilitygroup === true) {
          this.groups.forEach(element => {
            element.isChecked = this.timePolicy.selectall_eligibilitygroup;
          });
        }

           this.leaveEligibilityGroups.forEach(element => {
             console.log("iddddddddd",element._id);
             
               if(this.getPolicyData.leave_eligibility_group.indexOf(element._id)>=0){

                 element.isChecked = true
               }
           });
         
          console.log(this.getPolicyData.leave_eligibility_group);

      
        this.timePolicy.dateOfHire = this.getPolicyData.employee_policy_period.new_hire.hire_date.selected;
        this.timePolicy.firstMonth = this.getPolicyData.employee_policy_period.new_hire.first_month.selected;
        this.timePolicy.after = this.getPolicyData.employee_policy_period.new_hire.first_month.after_days.selected;
        this.timePolicy.afterDays = this.getPolicyData.employee_policy_period.new_hire.first_month.after_days.days;
        this.timePolicy.timerBased = this.getPolicyData.employee_policy_period.new_hire.first_month.after_days.timerbased;
        this.timePolicy.selectDays = this.getPolicyData.employee_policy_period.new_hire.following_DOT.selected;
        this.timePolicy.followingDOH = this.getPolicyData.employee_policy_period.new_hire.first_month.following_DOH;
        this.timePolicy.days = this.getPolicyData.employee_policy_period.new_hire.following_DOT.days;
        this.timePolicy.timerDays = this.getPolicyData.employee_policy_period.new_hire.following_DOT.timerbased;
        this.timePolicy.others = this.getPolicyData.employee_policy_period.new_hire.others;

        this.lifeEvent.dateOfHire = this.getPolicyData.employee_policy_period.life_event.hire_date.selected;
        this.lifeEvent.firstMonth = this.getPolicyData.employee_policy_period.life_event.first_month.selected;
        this.lifeEvent.after = this.getPolicyData.employee_policy_period.life_event.first_month.after_days.selected;
        this.lifeEvent.afterDays = this.getPolicyData.employee_policy_period.life_event.first_month.after_days.days;
        this.lifeEvent.timerBased = this.getPolicyData.employee_policy_period.life_event.first_month.after_days.timerbased;
        this.lifeEvent.selectDays = this.getPolicyData.employee_policy_period.life_event.following_DOT.selected;
        this.lifeEvent.followingDOH = this.getPolicyData.employee_policy_period.life_event.first_month.following_DOH;
        this.lifeEvent.days = this.getPolicyData.employee_policy_period.life_event.following_DOT.days;
        this.lifeEvent.timerDays = this.getPolicyData.employee_policy_period.life_event.following_DOT.timerbased;
        this.lifeEvent.others = this.getPolicyData.employee_policy_period.life_event.others;

        this.timePolicy.policyterm = this.getPolicyData.employee_policy_period.employee_usage.policyterm;
        this.timePolicy.policytermdays = this.getPolicyData.employee_policy_period.employee_usage.policytermdays;
        if (this.timePolicy.policytermdays === "") {
          this.showField = false;
        }

        this.timePolicy.timeearned = this.getPolicyData.time_earned.timeearned;
        this.timePolicy.pay_period_freq = this.getPolicyData.time_earned.pay_period_freq;
        this.timePolicy.dayofweek = this.getPolicyData.time_earned.dayofweek;
        this.timePolicy.dateofMonth = this.getPolicyData.time_earned.dateofMonth;
        this.timePolicy.dayofmonth = this.getPolicyData.time_earned.dayofmonth;
        this.timePolicy.yearsofservices = this.getPolicyData.time_earned.tireslevel.yearsofservices;
        console.log(this.timePolicy.yearsofservices);

        if (this.timePolicy.yearsofservices == true) {
          this.showField1 = true;
        } else if (this.timePolicy.yearsofservices == false) {
          this.showField1 = false;
        }

        if (res.tierLevelData) {
          this.allTierLevels = res.tierLevelData[0].tierLevel;
        }

        this.timePolicy.negativebalance = this.getPolicyData.time_earned.negativebalance;

        if (this.getPolicyData.time_earned.negativebalance == true) {
          this.showField2 = true;
        }
        else {
          this.showField2 = false;
        }

        this.timePolicy.neg_max_hrs = this.getPolicyData.time_earned.neg_max_hrs;

        this.timePolicy.expiretime = this.getPolicyData.expirationoptions.expiretime;

        if (this.getPolicyData.expirationoptions.expiretime) {
          this.showField3 = true;
        } else {
          this.showField3 = false;
        }

        this.timePolicy.expireoptions = this.getPolicyData.expirationoptions.expireoptions;

        if (this.getPolicyData.expirationoptions.expireoptions == "Expiration Date") {
          this.showField4 = true;
        } else {
          this.showField4 = false;
        }

        this.timePolicy.expiredate = this.getPolicyData.expirationoptions.expiredate;
        this.eligibleGroupsArray = this.getPolicyData.leave_eligibility_group;
        this.tierLevelId = this.getPolicyData.time_earned.tireslevel.tier_level_id[0];
        console.log(this.tierLevelId);
        

      }, err => {
        console.log(err);

      })
    }
  }

    // Author: Saiprakash G, Date: 04/06/19,
  // Delete time policy

  deleteTimeOffPolicy() {
    this.selectedPolicyType = this.getAllPolicies.filter(policyCheck => {
      return policyCheck.isChecked
    })

    if (this.selectedPolicyType.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select a time off policy on to Proceed", "", 'error')
    }
    else {
      var data: any = {
        _id: this.selectedPolicyType
      }
      this.leaveManagementService.deleteTimeOffPolicies(data).subscribe((res: any) => {
        console.log(res);
        this.getAllTypePolicies();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.Message, "", "error");

      })
    }
  }

    // Author: Saiprakash G, Date: 12/06/19,
  // Upload import balances file
  fileChangeEvent(e: any) {

    this.files = e.target.files[0];
    console.log(this.files.name)

  }

  uploadImportBalance(){
    var formData: FormData = new FormData();
    formData.append('file', this.files);
    formData.append('companyId', this.companyId)
   
    this.leaveManagementService.uploadImportBalance(formData).subscribe((res:any)=>{
      console.log(res);
      this.files = "";
      jQuery("#myModal").modal("hide");
    
    }, err =>{
      console.log(err);
      
    })
  }

  previousPage() {
    this.router.navigate(['/admin/admin-dashboard/leave-management']);
  }

}
