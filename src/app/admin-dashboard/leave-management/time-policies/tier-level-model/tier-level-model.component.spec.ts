import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TierLevelModelComponent } from './tier-level-model.component';

describe('TierLevelModelComponent', () => {
  let component: TierLevelModelComponent;
  let fixture: ComponentFixture<TierLevelModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TierLevelModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TierLevelModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
