import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LeaveManagementService } from '../../../../services/leaveManagement.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';



@Component({
  selector: 'app-tier-level-model',
  templateUrl: './tier-level-model.component.html',
  styleUrls: ['./tier-level-model.component.css']
})
export class TierLevelModelComponent implements OnInit {

  public datetype_Of_Servicecalc: any;
  public tierLevel_Entry: any;
  public TierLevel_Ln: any;
  public tierLevel1: any = {
    serviceLength: {
      date: '',
      duration: ''
    },
    hoursEarnedMax: '',
    hoursEarnedPerCycle: '',
    carryOverAllowed: '',
    carryOverMax: ''
  }
  public tierLevels = [

  ];
  public companyId: any;
  flag = false;
  public selectedTierLevels = [];
  editField: boolean = true;
  hideField: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<TierLevelModelComponent>,
    private leaveManagementService: LeaveManagementService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    console.log(this.data);
    this.data.tierLevel.filter(item => {
      item.edit = true;
    })
    if (this.data) {
      this.TierLevel_Ln = this.data.TierLevel_Ln;
      this.datetype_Of_Servicecalc = this.data.datetype_Of_Servicecalc;
      this.tierLevel_Entry = this.data.tierLevel_Entry;
      this.tierLevels = this.data.tierLevel
    }


  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // Author: Saiprakash G, Date: 03/06/19,
  // push tier levels


  pushTierLevels() {

    console.log(this.TierLevel_Ln);
    this.editField = false;
    let newLength = this.TierLevel_Ln - this.tierLevels.length;
    let decreaseLength = this.tierLevels.length - this.TierLevel_Ln;
    console.log(decreaseLength);
    if (decreaseLength > 0) {
      this.tierLevels.splice(this.tierLevels.length - decreaseLength)
    }


    if (newLength > 0) {
      for (let i = 0; i < newLength; i++) {
        this.tierLevels.push({
          serviceLength: {
            date: '',
            duration: ''
          },
          hoursEarnedMax: '',
          hoursEarnedPerCycle: '',
          carryOverAllowed: '',
          carryOverMax: '',
          edit: false
        });
        this.flag = true;

      }
    }

    console.log(this.tierLevels);

  }
  levelNumber(levelNum) {
    console.log(levelNum);

  }

  // Author: Saiprakash G, Date: 29/06/19,
  // Create and update tier levels
  addTierLevel() {
    var data: any = {
      companyId: this.companyId,
      datetype_Of_Servicecalc: this.datetype_Of_Servicecalc,
      tierLevel_Entry: this.tierLevel_Entry,
      TierLevel_Ln: this.TierLevel_Ln,
      tierLevel: this.tierLevels
    }

    if (this.data._id) {
      var updateData: any = {
        tier_id: this.data._id,
        params: data
      }
      console.log(updateData, "tier levels data");


      this.leaveManagementService.updateTierLevel(updateData).subscribe((res: any) => {
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
        this.datetype_Of_Servicecalc = null;
        this.tierLevel_Entry = null;
        this.TierLevel_Ln = null;
        this.tierLevels = [];
        this.dialogRef.close(res);

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

      })
    } else {
      this.leaveManagementService.createTierLevels(data).subscribe((res: any) => {
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.Message, "", "success");
        this.datetype_Of_Servicecalc = null;
        this.tierLevel_Entry = null;
        this.TierLevel_Ln = null;
        this.tierLevels = [];
        this.dialogRef.close(res);

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.Message, "", "error");

      })
    }

  }

  // Author: Saiprakash G, Date: 31/05/19,
  // Edit tier level
  editTierLevels() {
    this.selectedTierLevels = this.data.tierLevel.filter(checkTiers => {
      return checkTiers.isChecked
    })
    console.log(this.selectedTierLevels);
    if (this.selectedTierLevels.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one tier level on to proceed", "", 'error')
    } else if (this.selectedTierLevels.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select a tier level on to Proceed", "", 'error')
    }
    else {
      if (this.selectedTierLevels[0]._id) {
        this.selectedTierLevels[0].edit = false;
      }

      console.log(this.selectedTierLevels[0]._id);

    }

  }

  // Author: Saiprakash G, Date: 31/05/19,
  // Delete tier level

  deleteTierLevels() {
    this.selectedTierLevels = this.data.tierLevel.filter(checkTiers => {
      return checkTiers.isChecked
    })
    if (this.selectedTierLevels.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select a tier level on to Proceed", "", 'error')
    }

    else {

      var data: any = {
        _id: this.data._id,
        tier_id: this.selectedTierLevels
      }
      this.leaveManagementService.deleteTierLevel(data).subscribe((res: any) => {
        console.log(res);
        var tierLevelsList = [];
        this.data.tierLevel.filter(element => {
          if (this.selectedTierLevels[0]._id != element._id) {
            tierLevelsList.push(element);
          }
        })
        this.tierLevels = tierLevelsList;
        console.log(this.data.tierLevel);

        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

      })
    }

  }

  allowedCarryOver(event){
     console.log(event);
     if(event == 'Yes'){
       this.hideField = true;
     } else {
       this.hideField = false
     }
     
  }
}
