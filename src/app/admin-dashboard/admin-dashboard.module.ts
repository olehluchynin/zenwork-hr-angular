import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


/* External Routing */
import { AdminRouting } from './admin.routes';

/* Importing Components */
import { AdminDashboardComponent } from './admin-dashboard.component';
import { SharedModule } from '../shared-module/shared.module';








@NgModule({
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        AdminDashboardComponent,
    ],
    providers: [
    ],
    imports: [
        CommonModule,
        AdminRouting,
        SharedModule,
        BsDatepickerModule.forRoot()
    ]
})

export class AdminDashboardModule { }