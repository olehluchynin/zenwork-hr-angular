import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuallyApplicantComponent } from './manually-applicant.component';

describe('ManuallyApplicantComponent', () => {
  let component: ManuallyApplicantComponent;
  let fixture: ComponentFixture<ManuallyApplicantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuallyApplicantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuallyApplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
