import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-manually-applicant',
  templateUrl: './manually-applicant.component.html',
  styleUrls: ['./manually-applicant.component.css']
})
export class ManuallyApplicantComponent implements OnInit {

  constructor( public dialogRef : MatDialogRef <ManuallyApplicantComponent>, @Inject(MAT_DIALOG_DATA) public data  ) { }

  ngOnInit() {
  }


  onClick(){
    this.dialogRef.close();
  }

}
