import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecruitmentComponent } from './recruitment.component';
import { RouterModule,Routes } from '@angular/router';
import { ApplicantCandidatesComponent } from './applicant-candidates/applicant-candidates.component';
import { InterviewsComponent } from './interviews/interviews.component';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ManuallyApplicantComponent } from './manually-applicant/manually-applicant.component';
import { InterviewScheduleComponent } from './interview-schedule/interview-schedule.component';





const routes : Routes = [

{path:'',component:RecruitmentComponent},
{path:'post-jobs',loadChildren:'./post-jobs/post-jobs.module#PostJobsModule'},
{path:'job-description',loadChildren:'./job-description/job-description.module#JobDescriptionModule'},
{path:'candidates',component:ApplicantCandidatesComponent},
{path:'interviews', component:InterviewsComponent}

]

@NgModule({
  declarations: [RecruitmentComponent, ApplicantCandidatesComponent, InterviewsComponent, ManuallyApplicantComponent, InterviewScheduleComponent],
  imports: [
    CommonModule,RouterModule.forChild(routes),MatSelectModule,MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,
    MatCheckboxModule,MatPaginatorModule
  ],
  entryComponents:[ManuallyApplicantComponent,InterviewScheduleComponent]
})
export class RecruitmentModule { }
