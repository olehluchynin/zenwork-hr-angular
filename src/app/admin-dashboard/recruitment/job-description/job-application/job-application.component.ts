import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { JobApplicationAddComponent } from '../job-application-add/job-application-add.component';


@Component({
  selector: 'app-job-application',
  templateUrl: './job-application.component.html',
  styleUrls: ['./job-application.component.css']
})
export class JobApplicationComponent implements OnInit {

  constructor( private dialog :MatDialog ) { }

  ngOnInit() {
  }


addJobApplication(){
  const dialogRef = this.dialog.open(JobApplicationAddComponent, {
    width:'1200px',
    height:'800px'
  });
  dialogRef.afterClosed().subscribe(data =>{

  })
}


}
