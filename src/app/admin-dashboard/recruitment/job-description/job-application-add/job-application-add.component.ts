import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-job-application-add',
  templateUrl: './job-application-add.component.html',
  styleUrls: ['./job-application-add.component.css']
})
export class JobApplicationAddComponent implements OnInit {

  constructor( public dialogRef : MatDialogRef<JobApplicationAddComponent>, @Inject (MAT_DIALOG_DATA) public data  ) { }

  ngOnInit() {

  }


  onClick(){
    this.dialogRef.close();
  }

}
