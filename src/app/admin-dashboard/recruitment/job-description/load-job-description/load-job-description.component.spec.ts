import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadJobDescriptionComponent } from './load-job-description.component';

describe('LoadJobDescriptionComponent', () => {
  let component: LoadJobDescriptionComponent;
  let fixture: ComponentFixture<LoadJobDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadJobDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadJobDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
