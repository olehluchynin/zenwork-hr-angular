import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { JobDescriptionComponent } from './job-description.component';
import { JobBankComponent } from './job-bank/job-bank.component';
import { LoadJobDescriptionComponent } from './load-job-description/load-job-description.component';
import { JobApplicationComponent } from './job-application/job-application.component';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { JobApplicationAddComponent } from './job-application-add/job-application-add.component';


const routes : Routes = [
    {path:'',component:JobDescriptionComponent,children:[
        {path:'',redirectTo:'job-bank',pathMatch:'full'},
        {path:'job-bank',component:JobBankComponent},
        {path:'load-job',component:LoadJobDescriptionComponent},
        {path:'job-application',component:JobApplicationComponent}
    ]}
]

@NgModule({

declarations : [JobDescriptionComponent, JobBankComponent, LoadJobDescriptionComponent, JobApplicationComponent, JobApplicationAddComponent],
imports:[CommonModule,RouterModule.forChild(routes),MatSelectModule,MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule
,MatCheckboxModule,MatPaginatorModule],
entryComponents : [JobApplicationAddComponent] 

})

export class JobDescriptionModule{}