import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-interview-schedule',
  templateUrl: './interview-schedule.component.html',
  styleUrls: ['./interview-schedule.component.css']
})
export class InterviewScheduleComponent implements OnInit {

  constructor( private dialog : MatDialogRef<InterviewScheduleComponent>, @Inject(MAT_DIALOG_DATA) private data  ) { }

  ngOnInit() {
    
  }


onClick(){
  this.dialog.close();
}

}
