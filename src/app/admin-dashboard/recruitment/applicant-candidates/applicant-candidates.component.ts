import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ManuallyApplicantComponent } from '../manually-applicant/manually-applicant.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-applicant-candidates',
  templateUrl: './applicant-candidates.component.html',
  styleUrls: ['./applicant-candidates.component.css']
})
export class ApplicantCandidatesComponent implements OnInit {

  constructor( private dialog : MatDialog, private router:Router) { }

  ngOnInit() {

  }



manuallyClick(){

 const dialogRef = this.dialog.open(ManuallyApplicantComponent, {
  width:'900px',
  height:'800px'
 });
 
 dialogRef.afterClosed().subscribe(data =>{

 });

}


previousPage(){
this.router.navigate(['/admin/admin-dashboard/recruitment']);
}



}
