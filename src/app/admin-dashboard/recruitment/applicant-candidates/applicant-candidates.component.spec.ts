import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantCandidatesComponent } from './applicant-candidates.component';

describe('ApplicantCandidatesComponent', () => {
  let component: ApplicantCandidatesComponent;
  let fixture: ComponentFixture<ApplicantCandidatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantCandidatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantCandidatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
