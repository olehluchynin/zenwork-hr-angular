import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCalculatorResultsComponent } from './job-calculator-results.component';

describe('JobCalculatorResultsComponent', () => {
  let component: JobCalculatorResultsComponent;
  let fixture: ComponentFixture<JobCalculatorResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobCalculatorResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCalculatorResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
