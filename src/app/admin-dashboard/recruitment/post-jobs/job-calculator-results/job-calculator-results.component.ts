import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';



@Component({
  selector: 'app-job-calculator-results',
  templateUrl: './job-calculator-results.component.html',
  styleUrls: ['./job-calculator-results.component.css']
})
export class JobCalculatorResultsComponent implements OnInit {

  constructor( public dialogRef : MatDialogRef<JobCalculatorResultsComponent>, @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
  }


  onClick(){
    this.dialogRef.close();
  }  

}
