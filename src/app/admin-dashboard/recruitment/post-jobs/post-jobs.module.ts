import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { PostJobsComponent } from './post-jobs.component';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import { JobCalculatorSetttingComponent } from './job-calculator-settting/job-calculator-settting.component';
import { JobCalculatorResultsComponent } from './job-calculator-results/job-calculator-results.component';
import { PostJobsAddComponent } from './post-jobs-add/post-jobs-add.component';
import {MatRadioModule} from '@angular/material/radio';

const router:Routes = [
 {path:'', component:PostJobsComponent}
]

@NgModule({
  declarations: [PostJobsComponent,PostJobsAddComponent, JobCalculatorSetttingComponent, JobCalculatorResultsComponent],
  imports: [
    CommonModule,RouterModule.forChild(router),MatSelectModule,MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule
    ,MatCheckboxModule,MatPaginatorModule,MatDialogModule,MatRadioModule
  ],
  entryComponents:[PostJobsAddComponent,JobCalculatorSetttingComponent,JobCalculatorResultsComponent]
})
export class PostJobsModule { }
