import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCalculatorSetttingComponent } from './job-calculator-settting.component';

describe('JobCalculatorSetttingComponent', () => {
  let component: JobCalculatorSetttingComponent;
  let fixture: ComponentFixture<JobCalculatorSetttingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobCalculatorSetttingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCalculatorSetttingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
