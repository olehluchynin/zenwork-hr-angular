import { Component, OnInit } from '@angular/core';
import {MatDialog,MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { PostJobsAddComponent } from './post-jobs-add/post-jobs-add.component';
import { JobCalculatorSetttingComponent } from './job-calculator-settting/job-calculator-settting.component';
import { JobCalculatorResultsComponent } from './job-calculator-results/job-calculator-results.component';



@Component({
  selector: 'app-post-jobs',
  templateUrl: './post-jobs.component.html',
  styleUrls: ['./post-jobs.component.css']
})
export class PostJobsComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }


pageEvents(event){
  
}




postJobsAdd(){
  const dialogRef = this.dialog.open(PostJobsAddComponent, {
    width: '1000px',
    height :'1000px'
  });

  dialogRef.afterClosed().subscribe(result => {

  });
}


jobSettings(){
  const dialogRef = this.dialog.open(JobCalculatorSetttingComponent,{
    width:'1000px',
    height:'600px'
  });

  dialogRef.afterClosed().subscribe( result => {

  });
}


jobResults(){
  const dialogRef = this.dialog.open(JobCalculatorResultsComponent, {
    width :'1300px',
    height :'1000px'
  });

  dialogRef.afterClosed().subscribe(data =>{

  });

}


}
