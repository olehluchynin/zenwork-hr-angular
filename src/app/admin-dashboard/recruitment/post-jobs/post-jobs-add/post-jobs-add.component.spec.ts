import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostJobsAddComponent } from './post-jobs-add.component';

describe('PostJobsAddComponent', () => {
  let component: PostJobsAddComponent;
  let fixture: ComponentFixture<PostJobsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostJobsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostJobsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
