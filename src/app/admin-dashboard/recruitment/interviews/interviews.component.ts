import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { InterviewScheduleComponent } from '../interview-schedule/interview-schedule.component';



@Component({
  selector: 'app-interviews',
  templateUrl: './interviews.component.html',
  styleUrls: ['./interviews.component.css']
})
export class InterviewsComponent implements OnInit {

  constructor( private router:Router, private dialog : MatDialog) { }

  ngOnInit() {
  }


  previousPage(){
    this.router.navigate(['/admin/admin-dashboard/recruitment/']);
  }


scheduleClick (){
  let scheduleOpen = this.dialog.open(InterviewScheduleComponent, {
    width:'1000px',
    height:'800px'
  });

  scheduleOpen.afterClosed().subscribe(data =>{

  });

}


}


// @Component({
//   selector:'app-interview-schedule',
//   templateUrl:'./interview-schedule.component.html',
// })

// export class InterviewScheduleComponent {
//   constructor( private dialog :MatDialogRef <InterviewScheduleComponent>, @Inject(MAT_DIALOG_DATA) public data) {

//   }
// }