import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewHireHrTimeComponent } from './new-hire-hr-time.component';

const routes: Routes = [
    { path:'',redirectTo:"/new-hire-hr-time",pathMatch:"full" },
    {path:'new-hire-hr-time',component:NewHireHrTimeComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewhirehrtimeRouting { }
