import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewhirehrtimeRouting } from './new-hire-hr-time.routing';
import { NewHireHrTimeComponent } from './new-hire-hr-time.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    NewhirehrtimeRouting,
    BsDatepickerModule.forRoot(),
  ],
  declarations: [NewHireHrTimeComponent]
})
export class NewHireHrTimeModule { }
