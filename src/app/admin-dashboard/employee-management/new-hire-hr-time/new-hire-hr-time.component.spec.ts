import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHireHrTimeComponent } from './new-hire-hr-time.component';

describe('NewHireHrTimeComponent', () => {
  let component: NewHireHrTimeComponent;
  let fixture: ComponentFixture<NewHireHrTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHireHrTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHireHrTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
