import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm, FormGroupDirective } from '@angular/forms';
import { MyInfoService } from '../../../../services/my-info.service';
import { OnboardingService } from '../../../../services/onboarding.service';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { Router } from '@angular/router';
import { post } from 'selenium-webdriver/http';

declare var jQuery: any;

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})

export class JobComponent implements OnInit {
  public individualEmployee: boolean = false;
  public individualEmployeeDropdown: boolean = true;
  public individualEmployeefly: boolean = true;
  public individualEmployeeflyDropdown: boolean = true;
  isFirstOpen = true;
  companyRoles: any;
  public tabName: string = "Job";
  public business: any;
  public department: any;
  public eeoJob: any;
  public empType: any;
  public flsaCode: any;
  public jobTitle: any;
  public location: any;
  public frequency: any;
  public payType: any;
  public union: any;
  public assignEmployee: any;
  employeeId: any;
  dt: Date;
  HRcontactDetails = [];
  ManagerContactDetails = [];
  public fieldsShow: any = {
    Employee_Id: {
      hide: '',
      required: '',
      format: ''
    },
    Employee_Type: {
      hide: '',
      required: '',
      format: ''
    },
    Employee_Self_Service_Id: {
      hide: '',
      required: '',
      format: ''
    },
    // HR_Contact:{
    //   hide:'',
    //   required:'',
    //   format:''
    // },
    Work_Location: {
      hide: '',
      required: '',
      format: ''
    },
    Reporting_Location: {
      hide: '',
      required: '',
      format: ''
    },
    Hire_Date: {
      hide: '',
      required: '',
      format: ''
    },
    Rehire_Date: {
      hide: '',
      required: '',
      format: ''
    },
    // Manager_Contact:{
    //   hide:'',
    //   required:'',
    //   format:''
    // },
    Termination_Date: {
      hide: '',
      required: '',
      format: ''
    },
    Termination_Reason: {
      hide: '',
      required: '',
      format: ''
    },
    Rehire_Status: {
      hide: '',
      required: '',
      format: ''
    },
    Archive_Status: {
      hide: '',
      required: '',
      format: ''
    },
    FLSA_Code: {
      hide: '',
      required: '',
      format: ''
    },
    EEO_Job_Category: {
      hide: '',
      required: '',
      format: ''
    },
    Vendor_Id: {
      hide: '',
      required: '',
      format: ''
    },
    Specific_Workflow_Approval: {
      hide: '',
      required: '',
      format: ''
    },
    Site_AccessRole: {
      hide: '',
      required: '',
      format: ''
    },
    Type_of_User_Role_type: {
      hide: '',
      required: '',
      format: ''
    },
    Custom_1: {
      hide: '',
      required: '',
      format: ''
    },
    Custom_2: {
      hide: '',
      required: '',
      format: ''
    }
  }
  type: any;
  updateJobData = {
    jobTitle: '',
    unionFields: '',
    businessUnit: '',
    department: '',
    location: '',
    workRemote: '',
    changeReason: '',
    effective_date: '',
    ReportsTo: '',
    notes: '',
    companyId: '',
    userId: '',

  }
  jobTitleUpdate: any;
  unionFieldsUpdate: any;
  businessUnitUpdate: any;
  departmentUpdate: any;
  locationUpdate: any;
  workRemoteUpdate: any;
  changeReasonUpdate: any;
  effectiveDateJobUpdate: any;
  reportsToUpdate: any;
  notesUpdate: any;

  jobHireData:any;
  jobReHireData:any;
  jobTerminationData:any;

  constructor(
    private fb: FormBuilder,
    private myInfoService: MyInfoService,
    private onboardingService: OnboardingService,
    private siteAccessRolesService: SiteAccessRolesService,
    private swalAlertService: SwalAlertService,
    private router: Router
  ) { }
  empdata: any;
  jobData: any = {
    current_employment_status : {
      status: ""
    }
  };
  rehireStatus: any;

  jobDetailsForm: FormGroup;
  unamePattern = "^[a-z0-9_-]{8,15}$";
  jobInfoHistory: any;
  employeeStatusHistory: any;
  rehireArray = [];
  allStructureFieldsData: any;
  allEmployementStatusArray = [];
  alljobTitleArray = [];
  allbusinessUnitData = [];
  allDepartmentData = [];
  allUnionData = [];
  allLocationData = [];

  newStatus={
    empStatus: ''
  }
  
  newEffectivedate: any = {
    effectDate:''
  };
  employeeData: any;
  roleType: any;
  pagesAccess=[]
  roletype:any;
  jobTabView:boolean = false;

  ngOnInit() {
    var cId = JSON.parse(localStorage.getItem('companyId'));
    this.roletype = JSON.parse(localStorage.getItem('type'))
    if (this.roletype != 'company') {
      this.pageAccesslevels();
    }

    this.getStandardAndCustomFieldsforStructure(cId);
    this.initJobDetailsForm();
    this.getSingleTabSettingForJob()
    this.getAllJobDetails();
   
    this.getStandardStructureField()
    

    this.empdata = JSON.parse(localStorage.getItem("employee"))
    console.log(this.empdata, "121212empdata");
    
    if(this.empdata){
    this.employeeId = this.empdata._id;
    }

    this.roleType = JSON.parse(localStorage.getItem('type'))

    if(this.roleType != 'company'){
      this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
    }
    console.log(this.employeeId,"vpin");
    
    this.getThisUser(cId);
    this.employeeSearch();
    // this.jobData = this.empdata.job;
    // this.jobInfoHistory = this.jobData.job_information_history;
    // this.employeeStatusHistory = this.jobData.employment_status_history;
    // console.log("jobDaata", this.jobData, this.jobInfoHistory);
    // if (this.jobData) {

    //   console.log(new Date(this.jobData.hireDate));
    //   this.dt = new Date(this.jobData.hireDate);
    //   this.jobDetailsForm.patchValue(this.jobData);
    //   this.jobDetailsForm.patchValue({ hireDate: this.dt });
    //   console.log("jobDaata1", this.jobDetailsForm.value, this.jobData);

    // }
    this.getAllroles();


  }


  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Job" && element.access == 'view') {
              console.log('data comes in2');

              this.jobTabView = true;
              console.log('loggss', this.jobTabView);

            }

          });

        },
        (err: any) => {

        })


  }

  getStandardAndCustomFieldsforStructure(cID) {
    this.onboardingService.getStructureFields(cID)
      .subscribe((res: any) => {
        console.log("structure fields", res);
        this.allStructureFieldsData = res.data;
        // Employment Status 
        if (this.allStructureFieldsData['Employment Status'].custom.length > 0) {
          this.allStructureFieldsData['Employment Status'].custom.forEach(element => {
            this.allEmployementStatusArray.push(element)
            console.log("22222111111", this.allEmployementStatusArray);
          });
        }
        if (this.allStructureFieldsData['Employment Status'].default.length > 0) {
          this.allStructureFieldsData['Employment Status'].default.forEach(element => {
            this.allEmployementStatusArray.push(element)
          });
          console.log("22222", this.allEmployementStatusArray);
        }

        if (this.allStructureFieldsData['Job Title'].custom.length > 0) {
          this.allStructureFieldsData['Job Title'].custom.forEach(element => {
            this.alljobTitleArray.push(element)
            console.log("22222111111", this.alljobTitleArray);
          });
        }
        if (this.allStructureFieldsData['Job Title'].default.length > 0) {
          this.allStructureFieldsData['Job Title'].default.forEach(element => {
            this.alljobTitleArray.push(element)
          });
          console.log("22222", this.alljobTitleArray);
        }
        if (this.allStructureFieldsData['Business Unit'].custom.length > 0) {
          this.allStructureFieldsData['Business Unit'].custom.forEach(element => {
            this.allbusinessUnitData.push(element)
            console.log("22222111111", this.allbusinessUnitData);
          });
        }
        if (this.allStructureFieldsData['Business Unit'].default.length > 0) {
          this.allStructureFieldsData['Business Unit'].default.forEach(element => {
            this.allbusinessUnitData.push(element)
          });
          console.log("22222", this.allbusinessUnitData);
        }
        if (this.allStructureFieldsData['Department'].custom.length > 0) {
          this.allStructureFieldsData['Department'].custom.forEach(element => {
            this.allDepartmentData.push(element)
            console.log("22222111111", this.allDepartmentData);
          });
        }
        if (this.allStructureFieldsData['Department'].default.length > 0) {
          this.allStructureFieldsData['Department'].default.forEach(element => {
            this.allDepartmentData.push(element)
          });
          console.log("22222", this.allDepartmentData);
        }
        if (this.allStructureFieldsData['Union'].custom.length > 0) {
          this.allStructureFieldsData['Union'].custom.forEach(element => {
            this.allUnionData.push(element)
            console.log("22222111111", this.allUnionData);
          });
        }
        if (this.allStructureFieldsData['Union'].default.length > 0) {
          this.allStructureFieldsData['Union'].default.forEach(element => {
            this.allUnionData.push(element)
          });
          console.log("22222", this.allUnionData);
        }
        if (this.allStructureFieldsData['Location'].custom.length > 0) {
          this.allStructureFieldsData['Location'].custom.forEach(element => {
            this.allLocationData.push(element)
            console.log("22222111111", this.allLocationData);
          });
        }
        if (this.allStructureFieldsData['Location'].default.length > 0) {
          this.allStructureFieldsData['Location'].default.forEach(element => {
            this.allLocationData.push(element)
          });
          console.log("22222", this.allLocationData);
        }
      })
  }



  /* Description: getting employees list
   author : vipin reddy */
  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;
        this.employeeData.forEach(element => {
             if(element._id == this.jobData.HR_Contact){
               this.HRcontactDetails.push(element);
          this.jobDetailsForm.get('HRContact').patchValue(this.HRcontactDetails[0].email);

             }
        });
        console.log(this.HRcontactDetails);
        this.employeeData.forEach(element => {
          if(element._id == this.jobData.ReportsTo){
            this.ManagerContactDetails.push(element);
          this.jobDetailsForm.get('managerContact').patchValue(this.ManagerContactDetails[0].email);

          }
        });
        console.log(this.ManagerContactDetails);
        

      },
        (err) => {
          console.log(err);

        })
  }


  getStandardStructureField() {
    var cID = JSON.parse(localStorage.getItem('companyId'))

    this.onboardingService.getStructureFields(cID)
      .subscribe((res: any) => {
        console.log("get rehirestatus", res);
        this.rehireStatus = res.data['Rehire Status'];
        if (this.rehireStatus.custom.length > 0) {
          this.rehireStatus.custom.forEach(element => {
            this.rehireArray.push(element)
          });
        }
        if (this.rehireStatus.default.length > 0) {
          this.rehireStatus.default.forEach(element => {
            this.rehireArray.push(element)
          });
        }
        console.log("22222union", this.rehireArray);
      },
        (err) => {
          console.log(err);

        })
  }
  empStatusChange(event) {
    console.log(event, this.newStatus.empStatus);

  }

  updateEmployeeStatusInfo() {
    var cId = JSON.parse(localStorage.getItem('companyId'))
    var postdata = {
      companyId: cId,
      userId: this.employeeId,
      status: this.newStatus.empStatus,
      effective_date: this.newEffectivedate.effectDate
        }
    console.log(postdata);
    this.myInfoService.updateEmployeInfoHistory(postdata)
      .subscribe((res: any) => {
        console.log("get update emp status", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Employee status history", res.message, "success")
          jQuery("#myModal").modal("hide");
          this.getThisUser(cId)
        }
      },
        (err) => {

        })

  }
  updateJobInfo() {
    var cId = JSON.parse(localStorage.getItem('companyId'))


    // var postdata = {
    //   companyId: cId,
    //   userId: this.employeeId,
    //   jobTitle: '',
    //   department: '',
    //   businessUnit: '',
    //   unionFields: '',
    //   location: '',
    //   // reportingLocation: ,
    //   ReportsTo: '',
    //   changeReason: '',
    //   remoteWork: '',
    //   effective_date: this.effectiveDateJobUpdate.toISOString(),
    //   notes: ''
    // }
    console.log(this.updateJobData);
    this.updateJobData.companyId = cId,
      this.updateJobData.userId = this.employeeId,


      console.log(this.updateJobData.jobTitle);
    this.myInfoService.updateJobInfoHistory(this.updateJobData)
      .subscribe((res: any) => {
        console.log("get update job info history", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Employee status history", res.message, "success")
          jQuery("#myModal2").modal("hide");
          this.getThisUser(cId)
        }
      },
        (err) => {

        })

  }
  getThisUser(cId) {
    this.myInfoService.getEmpJobInfo(cId, this.employeeId)
      .subscribe((res: any) => {
        console.log("get job total tab", res);
        this.jobData = res.data.job;
        this.jobInfoHistory = this.jobData.job_information_history;
        this.employeeStatusHistory = this.jobData.employment_status_history;
        console.log("jobDaata", this.jobData, this.jobInfoHistory);
        console.log(this.jobData.HR_Contact);
        console.log(this.jobData.ReportsTo);
        if (this.jobData) {

          console.log(new Date(this.jobData.hireDate));
          this.dt = new Date(this.jobData.hireDate);

          this.jobDetailsForm.patchValue(this.jobData);
          this.jobDetailsForm.patchValue({ hireDate: this.dt });
          this.jobDetailsForm.get('reHireDate').patchValue(new Date(this.jobData.reHireDate));
          this.jobDetailsForm.get('terminationDate').patchValue(new Date(this.jobData.terminationDate));
          console.log("jobDaata1", this.jobDetailsForm.value, this.jobData);
          this.employeeSearch();
        }
        
      },
        (err) => {
          console.log(err);

        })
  }


  // Author:vipin reddy
  //Description: getting all custom roles
  getAllroles() {
    var cID = localStorage.getItem('companyId')
    console.log("cid", cID);
    cID = cID.replace(/^"|"$/g, "");
    console.log("cin2", cID);


    this.siteAccessRolesService.companyId(cID)
      .subscribe((res: any) => {
        console.log("get ROles", res);
        this.companyRoles = res.data;
        var result = this.companyRoles.map(function (el) {
          var o = Object.assign({}, el);
          o.isActive = false;
          return o;
        })
        this.companyRoles = result;
        console.log(this.companyRoles, 'custom create');

      },
        (err) => {
          console.log(err);


        })
  }


  // Author:vipin reddy
  //Description: getting dropdowns fields data from structure

  getAllJobDetails() {
    this.onboardingService.getAllJobDetails()
      .subscribe((res: any) => {
        console.log("Get the job Compensations", res)
        this.business = res.data['Business Unit'];
        this.department = res.data['Department'];
        this.eeoJob = res.data['EEO Job Category'];
        this.empType = res.data['Employee Type'];
        this.flsaCode = res.data['FLSA Code'];
        this.jobTitle = res.data['Job Title'];
        this.location = res.data['Location'];
        this.frequency = res.data['Pay Frequency'];
        this.payType = res.data['Pay Type'];
        this.union = res.data['Union'];
        this.assignEmployee = res['settings']['idNumber'];
      },
        (err) => {

        })
  }

  // Author:Saiprakash, Date:09/04/2019
  //initializing job details form

  initJobDetailsForm() {

    this.jobDetailsForm = new FormGroup({
      employeeId: new FormControl(""),
      employeeType: new FormControl(""),
      // employeeSelfServiceId: new FormControl(""),
      HRContact: new FormControl("", Validators.required),
      location: new FormControl(""),
      reportingLocation: new FormControl(""),
      hireDate: new FormControl(),
      reHireDate: new FormControl(""),
      managerContact: new FormControl("", Validators.required),
      terminationDate: new FormControl(""),
      terminationReason: new FormControl(""),
      rehireStatus: new FormControl(""),
      archiveStatus: new FormControl(""),
      FLSA_Code: new FormControl(""),
      EEO_Job_Category: new FormControl(""),
      VendorId: new FormControl(""),
      Specific_Workflow_Approval: new FormControl(""),
      Site_AccessRole: new FormControl(""),


    })
  }



  // Author:Saiprakash, Date:10/04/2019
  // Get single tab settings for job

  getSingleTabSettingForJob() {

    this.myInfoService.getSingleTabSettingForMyinfo(this.tabName).subscribe((res: any) => {
      console.log(res);
      this.fieldsShow = res.data.fields;

      if (this.fieldsShow.Employee_Id.format == 'Alpha') {
        this.type = 'text';
      }
      else if (this.fieldsShow.Employee_Id.format == "AlphaNumeric") {
        this.type = 'text';
      }
      else if (this.fieldsShow.Employee_Id.format == "Numeric") {
        this.type = 'number';
      }
      console.log(this.fieldsShow);


    }, err => {
      console.log(err);

    })
  }

  customPattern() {
    if (this.fieldsShow.Employee_Id.format == 'Alpha') {
      return "[1-9][0-9]";
    }
  }


  saveJobDetails() {
    var cId = JSON.parse(localStorage.getItem('companyId'))
    console.log(this.jobDetailsForm.value);
    let finalObj = this.jobDetailsForm.value;

    finalObj.unionFields = this.jobData.unionFields;
    finalObj.jobTitle = this.jobData.jobTitle;
    finalObj.department = this.jobData.department;
    finalObj.businessUnit = this.jobData.businessUnit;
    // var businessUnit = this.jobData.businessUnit
    // this.jobDetailsForm.setValue(businessUnit)
    // finalObj._id = this.employeeId
    console.log(finalObj, "vipin");
    var postdata = {
      _id: this.employeeId,
      job: {

      }
    }
    postdata.job = Object.assign(this.jobData, finalObj)

    console.log(postdata, "121212121212121");


    if(this.jobDetailsForm.valid){
      this.myInfoService.updateUSer(postdata)
      .subscribe(
        (res: any) => {
          this.getThisUser(cId);
          this.swalAlertService.SweetAlertWithoutConfirmation("Employee job history", res.message, "success")

        },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation(err.error.mesage, '', 'error')

        })
    } else {
      this.jobDetailsForm.get('employeeId').markAsTouched();
      this.jobDetailsForm.get('employeeType').markAsTouched();
      // this.jobDetailsForm.get('employeeSelfServiceId').markAsTouched();
      this.jobDetailsForm.get('HRContact').markAsTouched();
      this.jobDetailsForm.get('location').markAsTouched();
      this.jobDetailsForm.get('reportingLocation').markAsTouched();
      this.jobDetailsForm.get('hireDate').markAsTouched();
      this.jobDetailsForm.get('reHireDate').markAsTouched();
      this.jobDetailsForm.get('managerContact').markAsTouched();
      this.jobDetailsForm.get('terminationDate').markAsTouched();
      this.jobDetailsForm.get('terminationReason').markAsTouched();
      this.jobDetailsForm.get('rehireStatus').markAsTouched();
      this.jobDetailsForm.get('archiveStatus').markAsTouched();
      this.jobDetailsForm.get('FLSA_Code').markAsTouched();
      this.jobDetailsForm.get('EEO_Job_Category').markAsTouched();
      this.jobDetailsForm.get('VendorId').markAsTouched();
      this.jobDetailsForm.get('Specific_Workflow_Approval').markAsTouched();
      this.jobDetailsForm.get('Site_AccessRole').markAsTouched();

    }
  
  }

  cancelJob(){
    this.router.navigate(['/admin/admin-dashboard/employee-management/profile/profile-view/personal'])
  }


  detailEmployee() {
    this.individualEmployee = !this.individualEmployee;
    this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
  }
  hideEmployeeData() {
    this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
    this.individualEmployee = !this.individualEmployee;
  }
  detailEmployeefly() {
    this.individualEmployeefly = !this.individualEmployeefly;
    this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
  }
  hideEmployeeflyData() {
    this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
    this.individualEmployeefly = !this.individualEmployeefly;
  }
  /* Description:accept only alphabets
  author : vipin reddy */
  alphabets(event) {
    return (event.charCode > 64 &&
      event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)
  }
  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/emergency"])

  }

  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omit_special_char(event){
   
   var k;  
   k = event.charCode;  //         k = event.keyCode;  (Both can be used)
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

hireDateChange(){
 console.log(this.jobDetailsForm.get('hireDate').value);
  this.jobHireData = this.jobDetailsForm.get('hireDate').value
}

reHireDateChange(){
  console.log(this.jobDetailsForm.get('reHireDate').value);
  this.jobReHireData = this.jobDetailsForm.get('reHireDate').value
}

terminationDateChange(){
  console.log(this.jobDetailsForm.get('terminationDate').value);
  this.jobTerminationData = this.jobDetailsForm.get('terminationDate').value
}

}
