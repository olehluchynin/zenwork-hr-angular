import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JObRouting } from './job.routing';
import { JobComponent } from './job/job.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyInfoService } from '../../../services/my-info.service';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';



@NgModule({
  imports: [
    CommonModule,
    JObRouting,  
    BsDatepickerModule.forRoot(),
    MatSlideToggleModule,
    AccordionModule,
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule,MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,MatCheckboxModule
  ],
  declarations: [JobComponent],
  providers :[MyInfoService]
 
})
export class JobModule { }
