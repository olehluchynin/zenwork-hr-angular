import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnBoardigTabComponent } from './on-boardig-tab/on-boardig-tab.component';
import { OnboardingTabRouting } from './onboardingtab.router';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModuleModule } from '../../../material-module/material-module.module';


@NgModule({
  imports: [
    CommonModule,
    OnboardingTabRouting,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    MaterialModuleModule

  ],
  declarations: [OnBoardigTabComponent]
})
export class OnboardingtabModule { }
