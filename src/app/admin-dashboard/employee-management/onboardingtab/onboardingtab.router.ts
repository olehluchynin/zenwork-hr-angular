import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OnBoardigTabComponent } from './on-boardig-tab/on-boardig-tab.component';

const routes: Routes = [
    // { path:'',redirectTo:"/onboardingtab",pathMatch:"full" },
    {path:'',component:OnBoardigTabComponent},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class OnboardingTabRouting { }