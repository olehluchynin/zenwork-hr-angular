import { Component, OnInit } from '@angular/core';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { Router } from '@angular/router';
import { SwalAlertService } from '../../../../services/swalAlert.service';



@Component({
  selector: 'app-on-boardig-tab',
  templateUrl: './on-boardig-tab.component.html',
  styleUrls: ['./on-boardig-tab.component.css']
})
export class OnBoardigTabComponent implements OnInit {

  public companyId: any;
  public employeeDetails: any;
  public templateId: any;
  public assignedDate: any;
  public disableField: boolean = true;
  overRide: boolean = false;
  btnOverRide: boolean = true;
  completeDate: any;
  type: any;
  pagesAccess = [];
  onboardingTabView: boolean = false;

  public template: any = {
    "templateName": "Onboarding Template",
    "templateType": "base",
    "Onboarding Tasks": {

    }
  }

  constructor(
    private accessLocalStorageService: AccessLocalStorageService,
    private myInfoService: MyInfoService,
    private router: Router,
    private swalAlertService: SwalAlertService
  ) {
    this.assignedDate = new Date();
  }

  ngOnInit() {
    this.type = JSON.parse(localStorage.getItem('type'))
    if (this.type != 'company') {
      this.pageAccesslevels();
    }
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    this.templateId = this.employeeDetails.module_settings.onboarding_template;
    console.log(this.templateId);

    this.getOnboardingDetails();
  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - On-Boarding" && element.access == 'view') {
              console.log('data comes in2');

              this.onboardingTabView = true;
              console.log('loggss', this.onboardingTabView);

            }

          });

        },
        (err: any) => {

        })


  }

  getKeys(data) {
    // console.log(data);
    return Object.keys(data);
  }
  //  Author: Saiprakash, Date: 18/05/19
  //  Get onboarding details

  getOnboardingDetails() {
    this.myInfoService.getOnboardingDetails(this.companyId, this.templateId).subscribe((res: any) => {
      console.log(res);
      this.template = res.data;
      this.assignedDate = new Date(this.template.assigned_date);
      if (this.template.completion_date) {
        this.completeDate = new Date(this.template.completion_date);
        // console.log(this.assignedDate);
      }
    }, (err) => {
      console.log(err);

    })
  }

  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/offboardingtab"])

  }

  adminOverride(event) {
    console.log(event);
    this.btnOverRide = !event.checked;

  }
  adminOverRideOfboarding() {
    console.log("vpiin");

    var uId = JSON.parse(localStorage.getItem('employee'))
    this.myInfoService.adminOonBoardingOverRide(uId._id)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Adminover Ride", res.message, "success")
          this.getOnboardingDetails();
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Adminover Ride", err.error.message, "error")
        })
  }
}
