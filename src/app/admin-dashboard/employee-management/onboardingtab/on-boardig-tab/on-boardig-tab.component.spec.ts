import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnBoardigTabComponent } from './on-boardig-tab.component';

describe('OnBoardigTabComponent', () => {
  let component: OnBoardigTabComponent;
  let fixture: ComponentFixture<OnBoardigTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnBoardigTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnBoardigTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
