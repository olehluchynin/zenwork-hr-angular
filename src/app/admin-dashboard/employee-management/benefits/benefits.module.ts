import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BenefitsRouting } from './benefits.routing';
import { BenefitsComponent } from './benefits/benefits.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    BenefitsRouting,
    BsDatepickerModule.forRoot(),
    MatSlideToggleModule,
    MatSelectModule
  ],
  declarations: [BenefitsComponent]
})
export class BenefitsModule { }
