import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffboardingtabComponent } from './offboardingtab.component';

describe('OffboardingtabComponent', () => {
  let component: OffboardingtabComponent;
  let fixture: ComponentFixture<OffboardingtabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffboardingtabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffboardingtabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
