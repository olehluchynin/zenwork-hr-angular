import { Component, OnInit } from '@angular/core';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { Router } from '@angular/router';
import { SwalAlertService } from '../../../../services/swalAlert.service';


@Component({
  selector: 'app-offboardingtab',
  templateUrl: './offboardingtab.component.html',
  styleUrls: ['./offboardingtab.component.css']
})
export class OffboardingtabComponent implements OnInit {
  public companyId: any;
  public employeeDetails: any;
  public templateId: any;
  public assignedDate: any;
  public disableField: boolean = true;
  overRide: boolean = false;
  btnOverRide: boolean = true;
  completeDate: any;
  public template: any = {
    "templateName": "Onboarding Template",
    "templateType": "base",
    "offBoardingTasks": {

    }
  }
  roletype: any;
  pagesAccess = [];
  offboardingTabView: boolean = false;

  constructor(
    private accessLocalStorageService: AccessLocalStorageService,
    private myInfoService: MyInfoService,
    private router: Router,
    private swalAlertService: SwalAlertService
  ) {
    this.assignedDate = new Date();
  }

  ngOnInit() {

    this.roletype = JSON.parse(localStorage.getItem('type'))
    if (this.roletype != 'company') {
      this.pageAccesslevels();
    }

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    this.templateId = this.employeeDetails.job.off_boarding_template_assigned;
    console.log(this.templateId);

    this.getOffboardingDetails();
  }
  getKeys(data) {
    // console.log(data);
    return Object.keys(data);
  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Off-Boarding" && element.access == 'view') {
              console.log('data comes in2');

              this.offboardingTabView = true;
              console.log('loggss', this.offboardingTabView);

            }

          });

        },
        (err: any) => {

        })


  }
  //  Author: Saiprakash, Date: 18/05/19
  //  Get onboarding details

  getOffboardingDetails() {
    this.myInfoService.getOffboardingDetails(this.companyId, this.templateId).subscribe((res: any) => {
      console.log(res);
      this.template = res.data;
      this.assignedDate = new Date(this.template.assigned_date);
      // console.log(this.assignedDate);
      if (this.template.completion_date) {
        this.completeDate = new Date(this.template.completion_date);
        // console.log(this.assignedDate);
      }

    }, (err) => {
      console.log(err);

    })
  }

  adminOverride(event) {
    console.log(event);
    this.btnOverRide = !event.checked;

  }
  adminOverRideOfboarding() {
    console.log("vpiin");

    var uId = JSON.parse(localStorage.getItem('employee'))
    this.myInfoService.adminOffBoardingOverRide(uId._id)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Admin Override", res.message, "success");
          this.getOffboardingDetails();
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Admin Override", err.error.message, "error")
        })
  }

}
