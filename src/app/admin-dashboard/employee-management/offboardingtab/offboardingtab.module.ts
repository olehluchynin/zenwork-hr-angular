import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffboardingtabComponent } from './offboardingtab/offboardingtab.component';
import { OffboardingTabRouting } from './offboardingtab.routing';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    OffboardingTabRouting,
    BsDatepickerModule.forRoot(),
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [OffboardingtabComponent]
})
export class OffboardingtabModule { }
