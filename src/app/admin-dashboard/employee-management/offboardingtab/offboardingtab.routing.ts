import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OffboardingtabComponent } from './offboardingtab/offboardingtab.component';

const routes: Routes = [
    // { path:'',redirectTo:"/offboardingtab",pathMatch:"full" },
    {path:'',component:OffboardingtabComponent},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class OffboardingTabRouting { }