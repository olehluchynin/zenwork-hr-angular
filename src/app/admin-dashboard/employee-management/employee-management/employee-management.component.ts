import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { LoaderService } from '../../../services/loader.service';
import {
  Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { LoaderService } from '../../../services/loader.service';
import { MyInfoService } from '../../../services/my-info.service';


@Component({
  selector: 'app-employee-management',
  templateUrl: './employee-management.component.html',
  styleUrls: ['./employee-management.component.css']
})
export class EmployeeManagementComponent implements OnInit {

  constructor(private router: Router,
    private loaderService: LoaderService,
    private myInfoService: MyInfoService) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }



  ngOnInit() {
   
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.loader(true);
    }
    if (event instanceof NavigationEnd) {
      this.loaderService.loader(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loaderService.loader(false);
    }
    if (event instanceof NavigationError) {
      this.loaderService.loader(false);
    }
  }

  empManagement() {

    this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding'])
  }


}
