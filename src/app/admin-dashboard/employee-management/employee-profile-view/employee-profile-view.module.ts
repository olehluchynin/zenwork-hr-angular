import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { employeeProfileViewRouting} from './employee-profile-view.routing';
import { ProfileViewNavbarComponent } from './profile-view-navbar/profile-view-navbar.component';
import { JobModule } from '../job/job.module';
import { PersonalComponent } from './personal/personal.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MaterialModuleModule } from '../../../material-module/material-module.module';


import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { NgxMaskModule } from 'ngx-mask';




@NgModule({
  imports: [
    CommonModule, 
    employeeProfileViewRouting,
    JobModule,
    MatSelectModule,
    BsDatepickerModule.forRoot(),
    FormsModule,MatDatepickerModule,MatMenuModule,MatButtonModule,
    MatIconModule,MaterialModuleModule,ReactiveFormsModule,
    NgxMaskModule.forRoot({
      showMaskTyped: true
    })
  ],
  declarations: [ProfileViewNavbarComponent, PersonalComponent]
})
export class EmployeeProfileViewModule { }
