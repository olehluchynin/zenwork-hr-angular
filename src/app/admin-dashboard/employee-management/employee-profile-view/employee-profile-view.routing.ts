import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileViewNavbarComponent } from './profile-view-navbar/profile-view-navbar.component';
import { PersonalComponent } from './personal/personal.component';



const routes: Routes = [
    { path:'',redirectTo:"/profile-view",pathMatch:"full" },
    {path:'profile-view',component:ProfileViewNavbarComponent, children: [

             {path:'personal',component:PersonalComponent},
             {path:'job',loadChildren: "../job/job.module#JobModule"},
             {path:'time-schedule',loadChildren:"../time-schedule/time-schedule.module#TimeScheduleModule"},
             {path:'emergency',loadChildren: "../emergency/emergency.module#EmergencyModule"},
             {path:'performance',loadChildren: "../performance/performance.module#PerformanceModule"},
             {path:'compensation',loadChildren: "../compensation/compensation.module#CompensationModule"},
             {path:'notes',loadChildren: "../notes/notes.module#NotesModule"},
             {path:'benefits',loadChildren: "../benefits/benefits.module#BenefitsModule"},
             {path:'training',loadChildren: "../training/training.module#TrainingModule"},
             {path:'documents', loadChildren: "../documents/documents.module#DocumentsModule"},
             {path:'assets',loadChildren: "../assets/assets.module#AssetsModule"},
             {path:'onboardingtab', loadChildren: "../onboardingtab/onboardingtab.module#OnboardingtabModule"},
             {path:'offboardingtab',loadChildren:"../offboardingtab/offboardingtab.module#OffboardingtabModule"},
             {path:'custom' ,loadChildren:"../custom/custom.module#CustomModule"},
             {path:'audit-trail', loadChildren:"../audit-trail/audit-trail.module#AuditTrailModule"}
    ]},

]





@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class employeeProfileViewRouting { }