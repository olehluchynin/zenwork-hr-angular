import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileViewNavbarComponent } from './profile-view-navbar.component';

describe('ProfileViewNavbarComponent', () => {
  let component: ProfileViewNavbarComponent;
  let fixture: ComponentFixture<ProfileViewNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileViewNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileViewNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
