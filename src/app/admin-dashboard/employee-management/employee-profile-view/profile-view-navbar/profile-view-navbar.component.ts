import { Component, OnInit } from '@angular/core';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { MyInfoService } from '../../../../services/my-info.service';


@Component({
  selector: 'app-profile-view-navbar',
  templateUrl: './profile-view-navbar.component.html',
  styleUrls: ['./profile-view-navbar.component.css']
})
export class ProfileViewNavbarComponent implements OnInit {
  showTabs = {
    Assets: { hide: true },
    Benefits: { hide: true },
    Compensation: { hide: true },
    Documents: { hide: true },
    Emergency_Contact: { hide: true },
    Job: { hide: true },
    Notes: { hide: true },
    Offboarding: { hide: true },
    Onboarding: { hide: true },
    Performance: { hide: true },
    Personal: { hide: true },
    Time_Off: { hide: true },
    Training: { hide: true }

  };

  pagesAccess: any;
  jobTab: any;
  emergencyContact: any;
  timeSchedule: any;
  compensation: any;
  notes: boolean = false;
  benifits: boolean = false;
  training: boolean = false;
  documents: boolean = false;
  assets: boolean = false;
  onBoarding: boolean = false;
  offBoarding: boolean = false;
  custom: boolean = false;
  auditTrial: boolean = false
  type: any;
  constructor(private companySettingsService: CompanySettingsService,
    private myInfoService: MyInfoService) { }

  ngOnInit() {
    this.controlMyinfoSections()
    this.type = JSON.parse(localStorage.getItem('type'))
    if (this.type != 'company') {
      this.pageAccesslevels()
    }
    if (this.type == 'company') {
      this.allShowPages()
    }
  }
  allShowPages() {
    this.jobTab = true;
    this.emergencyContact = true;
    this.timeSchedule = true;
    this.compensation = true;
    this.notes = true;
    this.benifits = true;
    this.training = true;
    this.documents = true;
    this.assets = true;
    this.onBoarding = true;
    this.offBoarding = true;
    this.custom = true;
    this.auditTrial = true;
  }


  controlMyinfoSections() {
    this.companySettingsService.controllSections()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.showTabs = res.data.myInfo_sections;
        },
        (err: any) => {

        })
  }
  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            if (element.page == "My Info (EE's) - Job" && element.access != 'no') {
              this.jobTab = true;
            }
            if (element.page == "My Info (EE's) - Emergency Contact" && element.access != 'no') {
              this.emergencyContact = true;
            }
            if (element.page == "My Info (EE's) - Time Schedule" && element.access != 'no') {
              this.timeSchedule = true;
            }
            if (element.page == "My Info (EE's) - Compensation" && element.access != 'no') {
              this.compensation = true;
            }
            if (element.page == "My Info (EE's) - Notes" && element.access != 'no') {
              this.notes = true;
            }
            if (element.page == "My Info (EE's) - Benifits" && element.access != 'no') {
              this.benifits = true;
            }
            if (element.page == "My Info (EE's) - Training" && element.access != 'no') {
              this.training = true;
            }
            if (element.page == "My Info (EE's) - Documents" && element.access != 'no') {
              this.documents = true;
            }
            if (element.page == "My Info (EE's) - Assets" && element.access != 'no') {
              this.assets = true;
            }
            if (element.page == "My Info (EE's) - On-Boarding" && element.access != 'no') {
              this.onBoarding = true;
            }
            if (element.page == "My Info (EE's) - Off-Boarding" && element.access != 'no') {
              this.offBoarding = true;
            }
            if (element.page == "My Info (EE's) - Custom" && element.access != 'no') {
              this.custom = true;
            }
            if (element.page == "My Info (EE's) - Audit Trial" && element.access != 'no') {
              this.auditTrial = true;
            }
          });
        },
        (err: any) => {

        })


  }

}
