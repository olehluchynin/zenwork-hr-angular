import { Component, OnInit } from '@angular/core';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { EmployeeService } from '../../../../services/employee.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Pipe, PipeTransform } from '@angular/core';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  // transform(value: string, showMask :boolean): string {
  //   if (!showMask || value.length < 10) {
  //     return value;
  //   }
  //   return 'XXX-XX-' + value.substr(0, value.length - 6);
  // }



  constructor(
    private accessLocalStorageService: AccessLocalStorageService,
    private employeeService: EmployeeService,
    private swalAlertService: SwalAlertService,
    private personalService: MyInfoService,
    private router: Router,
  ) {

  }

  public personal = 'Personal'
  public basicDetailsForm: FormGroup
  public personalDetails: any;
  id: any;
  public companyId: any;
  public getAllStructureFields: any;
  public customMaritalStatus = [];
  public defaultMaritalStatus = [];
  public allMaritalStatus = [];
  public customVeteranStatus = [];
  public defaultVeteranStatus = [];
  public allVeteranStatus = [];
  public defaultRace = [];
  public customRace = [];
  public allRace = [];
  public defaultEthnicity = [];
  public customEthnicity = [];
  public allEthnicity = [];
  public defaultState = [];
  public customState = [];
  public allState = [];
  public defaultCountry = [];
  public customCountry = [];
  public allCountry = [];
  public defaultLanguages = [];
  public customLanguages = [];
  public allLanguages = [];
  public customLanguageFluency = [];
  public defaultLanguageFluency = [];
  public allLanguageFluency = [];
  public customVisaStatus = [];
  public defaultVisaStatus = [];
  public allVisaStatus = [];
  public customVisaType = [];
  public defaultVisaType = [];
  public allVisaType = [];
  public customSalutation = [];
  public defaultSalutaion = [];
  public allSalutation = [];
  public profilePhoto: any;
  public originalFileName: any;
  public s3Path: any;
  public salutationName: any;
  public educationList = [
    {
      university: '',
      degree: '',
      specialization: '',
      gpa: '',
      start_date: '',
      end_date: '',
      isChecked: false,
      edit: true
    }
  ];
  public languageList = [
    {
      language: '',
      level_of_fluency: '',
      isChecked: false,
      edit: true
    }
  ];
  public visaList = [
    {
      visa_type: '',
      issuing_country: '',
      issued_date: '',
      expiration_date: '',
      status: '',
      notes: '',
      isChecked: false,
      edit: true
    }
  ];
  public educationAdd = [];
  public languagesAdd = [];
  public visaAdd = [];
  public selectedVisaList = [];
  public selectedEducationList = [];
  public selectedLanguage = [];

  public employeeEducation: any;
  public employeeLanguages: any;
  public employeeVisaInformation: any;
  editField: boolean = true;
  public visaId: any;
  public eduId: any;
  public languageId: any;
  public personalId: any;
  checkEduIds = [];
  checkLangIds = [];
  checkVisaIds = [];
  dobDate: any;

  public ssnMask: boolean = false;

  public formatter: any;

  public basicDetailsData = {
    First_Name: {
      hide: '',
      required: false,
      format: ''
    },
    Middle_Name: {
      hide: '',
      required: false,
      format: ''
    },
    Last_Name: {
      hide: '',
      required: '',
      format: ''
    },
    Preferred_Name: {
      hide: '',
      required: '',
      format: ''
    },
    Salutation: {
      hide: '',
      required: '',
      format: ''
    },
    Date_of_Birth: {
      hide: '',
      required: '',
      format: ''
    },
    Gender: {
      hide: '',
      required: '',
      format: ''
    },
    Maritial_Status: {
      hide: '',
      required: '',
      format: ''
    },
    Maritial_Status_Effective_Date: {
      hide: '',
      required: '',
      format: ''
    },
    Veteran_Status: {
      hide: '',
      required: '',
      format: ''
    },
    SSN: {
      hide: '',
      required: '',
      format: ''
    },
    Race: {
      hide: '',
      required: '',
      format: ''
    },
    Ethnicity: {
      hide: '',
      required: '',
      format: ''
    }
  };
  roleType: any;
  type: any;
  pagesAccess: any;
  personalTabView: boolean = false;

  ngOnInit() {
    this.type = JSON.parse(localStorage.getItem('type'))
    if (this.type != 'company') {
      this.pageAccesslevels();
    }

    this.dobDate = new Date();

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);

    this.personalId = JSON.parse(localStorage.getItem('employee'));
    if (this.personalId) {
      this.id = this.personalId._id;
    }
    this.roleType = this.accessLocalStorageService.get('type')
    if (this.roleType != 'company') {
      this.id = JSON.parse(localStorage.getItem('employeeId'));
    }
    console.log(this.id);
    this.getUserPersonal();
    // this.educationList = this.personalDetails.personal.education;
    // this.languageList = this.personalDetails.personal.languages;
    // this.visaList = this.personalDetails.personal.visa_information;

    // console.log(this.personalDetails.personal);


    this.getAllBasicDetails();
    this.getStandardAndCustomStructureFields();

    this.basicDetailsForm = new FormGroup({

      firstName: new FormControl(""),
      middleName: new FormControl(""),
      lastName: new FormControl(""),
      preferredName: new FormControl(""),
      salutation: new FormControl(""),
      dob: new FormControl(""),
      gender: new FormControl(""),
      maritalStatus: new FormControl(""),
      effectiveDate: new FormControl(""),
      veteranStatus: new FormControl(""),
      ssn: new FormControl(""),
      race: new FormControl(""),
      ethnicity: new FormControl(""),
      street1: new FormControl("", Validators.required),
      street2: new FormControl(""),
      state: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      zipcode: new FormControl("", Validators.required),
      country: new FormControl("", Validators.required),
      extention: new FormControl("", Validators.required),
      mobilePhone: new FormControl("", Validators.required),
      homePhone: new FormControl("", Validators.required),
      workMail: new FormControl("", Validators.required),
      workPhone: new FormControl("", Validators.required),
      personalMail: new FormControl("", Validators.required),
      linkedin: new FormControl(""),
      twitter: new FormControl(""),
      facebook: new FormControl(""),
      // university: new FormControl(""),
      // degree: new FormControl(""),
      // specialization: new FormControl(""),
      // gpa: new FormControl(""),
      // start_date: new FormControl(""),
      // end_date: new FormControl(""),
      // language: new FormControl(""),
      // level_of_fluency: new FormControl(""),
      // visa_type: new FormControl(""),
      // issuing_country: new FormControl(""),
      // issued_date: new FormControl(""),
      // expiration_date: new FormControl(""),
      // status: new FormControl(""),
      // notes: new FormControl(""),

    });

    this.getSsnFields();


    // this.basicDetailsForm.get('degree').patchValue(this.personalDetails.personal.education[0].degree);
    // this.basicDetailsForm.get('end_date').patchValue(this.personalDetails.personal.education[0].end_date);
    // this.basicDetailsForm.get('gpa').patchValue(this.personalDetails.personal.education[0].gpa);
    // this.basicDetailsForm.get('specialization').patchValue(this.personalDetails.personal.education[0].specialization);
    // this.basicDetailsForm.get('start_date').patchValue(this.personalDetails.personal.education[0].start_date);
    // this.basicDetailsForm.get('university').patchValue(this.personalDetails.personal.education[0].university);

    // this.basicDetailsForm.get('language').patchValue(this.personalDetails.personal.languages[0].language);
    // this.basicDetailsForm.get('level_of_fluency').patchValue(this.personalDetails.personal.languages[0].level_of_fluency);

    // this.basicDetailsForm.get('facebook').patchValue(this.personalDetails.personal.social_links.facebook);
    // this.basicDetailsForm.get('linkedin').patchValue(this.personalDetails.personal.social_links.linkedin);
    // this.basicDetailsForm.get('twitter').patchValue(this.personalDetails.personal.social_links.twitter);

    // this.basicDetailsForm.get('expiration_date').patchValue(this.personalDetails.personal.visa_information[0].expiration_date);
    // this.basicDetailsForm.get('issued_date').patchValue(this.personalDetails.personal.visa_information[0].issued_date);
    // this.basicDetailsForm.get('issuing_country').patchValue(this.personalDetails.personal.visa_information[0].issuing_country);
    // this.basicDetailsForm.get('notes').patchValue(this.personalDetails.personal.visa_information[0].notes);
    // this.basicDetailsForm.get('status').patchValue(this.personalDetails.personal.visa_information[0].status);
    // this.basicDetailsForm.get('visa_type').patchValue(this.personalDetails.personal.visa_information[0].visa_type);


    //  console.log(' this.basicDetailsForm.get(firstName)', this.basicDetailsForm.get('firstName').value)
  }


  pageAccesslevels() {
    console.log("shfjksdh");

    this.personalService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Personal" && element.access == 'view') {
              console.log('data comes in2');

              this.personalTabView = true;
              console.log('loggss', this.personalTabView);

            }

          });

        },
        (err: any) => {

        })


  }
  // Author:Saiprakash G, Date:18-06-19
  // Upload employee photo

  fileChangeEvent(e: any) {
    console.log(e);


    var files = e.target.files[0];
    console.log(files.name)

    var data: any = {
      companyId: this.companyId,
      userId: this.id,

    }

    var formData: FormData = new FormData();
    formData.append('file', files);
    formData.append('data', JSON.stringify(data));

    this.personalService.uploadPhoto(formData).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      this.profilePhoto = res.data.url;


      console.log(this.profilePhoto);

    }, err => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
    })

  }


  // Author:Suresh M, Date:10-04-19
  // Basic Details get the Api
  getAllBasicDetails() {
    this.personalService.basicDetailsGetAll(this.personal)
      .subscribe((res: any) => {
        console.log("Personalssss", res);
        this.basicDetailsData = res.data.fields;

      });

  }

  // Author:Saiprakash, Date:15/05/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields() {
    this.personalService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res: any) => {
        console.log(res);
        this.getAllStructureFields = res.data;
        this.customMaritalStatus = this.getAllStructureFields['Marital Status'].custom;
        this.defaultMaritalStatus = this.getAllStructureFields['Marital Status'].default;
        this.allMaritalStatus = this.customMaritalStatus.concat(this.defaultMaritalStatus);
        console.log(this.allMaritalStatus);
        this.customVeteranStatus = this.getAllStructureFields['Veteran Status'].custom;
        this.defaultVeteranStatus = this.getAllStructureFields['Veteran Status'].default;
        this.allVeteranStatus = this.customVeteranStatus.concat(this.defaultVeteranStatus);
        this.defaultRace = this.getAllStructureFields['Race'].default;
        this.customRace = this.getAllStructureFields['Race'].custom;
        this.allRace = this.customRace.concat(this.defaultRace)
        this.defaultEthnicity = this.getAllStructureFields['Ethnicity'].default;
        this.customEthnicity = this.getAllStructureFields['Ethnicity'].custom;
        this.allEthnicity = this.customEthnicity.concat(this.defaultEthnicity);
        this.customCountry = this.getAllStructureFields['Country'].custom;
        this.defaultCountry = this.getAllStructureFields['Country'].default;
        this.allCountry = this.customCountry.concat(this.defaultCountry);
        this.customState = this.getAllStructureFields['State'].custom;
        this.defaultState = this.getAllStructureFields['State'].default;
        this.allState = this.customState.concat(this.defaultState);
        this.customLanguages = this.getAllStructureFields['Language'].custom;
        this.defaultLanguages = this.getAllStructureFields['Language'].default;
        this.allLanguages = this.customLanguages.concat(this.defaultLanguages);
        this.customLanguageFluency = this.getAllStructureFields['Language Fluency'].custom;
        this.defaultLanguageFluency = this.getAllStructureFields['Language Fluency'].default;
        this.allLanguageFluency = this.customLanguageFluency.concat(this.defaultLanguageFluency);
        this.customVisaType = this.getAllStructureFields['Visa Type'].custom;
        this.defaultVisaType = this.getAllStructureFields['Visa Type'].default;
        this.allVisaType = this.customVisaType.concat(this.defaultVisaType);
        this.customVisaStatus = this.getAllStructureFields['Visa Status'].custom;
        this.defaultVisaStatus = this.getAllStructureFields['Visa Status'].default;
        this.allVisaStatus = this.customVisaStatus.concat(this.defaultVisaStatus);
        this.customSalutation = this.getAllStructureFields['Salutation'].custom;
        this.defaultSalutaion = this.getAllStructureFields['Salutation'].default;
        this.allSalutation = this.customSalutation.concat(this.defaultSalutaion)


      }
    )
  }

  // Author:Saiprakash G, Date:22-05-19
  // education, languages and visa information IDs
  educationIds(event, id) {
    console.log(event, id);
    this.eduId = id;
    if (event == true) {
      this.checkEduIds.push(id)
    }
    console.log(this.checkEduIds);
    if (event == false) {
      var index = this.checkEduIds.indexOf(id);
      if (index > -1) {
        this.checkEduIds.splice(index, 1);
      }
      console.log(this.checkEduIds);
    }
  }
  languagesIds(event, id) {
    this.languageId = id;
    if (event == true) {
      this.checkLangIds.push(id)
    }
    console.log(this.checkLangIds);
    if (event == false) {
      var index = this.checkLangIds.indexOf(id);
      if (index > -1) {
        this.checkLangIds.splice(index, 1);
      }
      console.log(this.checkLangIds);
    }
  }
  visaIds(event, id) {
    this.visaId = id;
    if (event == true) {
      this.checkVisaIds.push(id)
    }
    console.log(this.checkVisaIds);
    if (event == false) {
      var index = this.checkVisaIds.indexOf(id);
      if (index > -1) {
        this.checkVisaIds.splice(index, 1);
      }
      console.log(this.checkVisaIds);
    }

  }
  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  // Author:Saiprakash G, Date:22-05-19
  // Add education
  addEducation() {
    this.employeeEducation = {

      university: '',
      degree: '',
      specialization: '',
      gpa: '',
      start_date: '',
      end_date: '',
      eduId: this.eduId

    }
    this.educationList.push(this.employeeEducation);
    this.editField = false;
  }
  // Author:Saiprakash G, Date:22-05-19
  // Add languages
  addLanguages() {
    this.employeeLanguages = {
      language: '',
      level_of_fluency: '',
      languageId: this.languageId
    }

    this.languageList.push(this.employeeLanguages);
    this.editField = false;
  }
  // Author:Saiprakash G, Date:22-05-19
  // Add visa information
  addVisaInformation() {
    this.employeeVisaInformation = {
      visa_type: '',
      issuing_country: '',
      issued_date: '',
      expiration_date: '',
      status: '',
      notes: '',
      visaId: this.visaId
    }

    console.log(this.employeeVisaInformation);
    this.visaList.push(this.employeeVisaInformation);
    this.editField = false;
    console.log(this.visaList);

  }
  // Author:Saiprakash, Date:22/05/2019
  // Get user personal

  getUserPersonal() {
    this.personalService.getOtherUser(this.companyId, this.id).subscribe((res: any) => {
      console.log(res);
      this.personalDetails = res.data;
      if (this.personalDetails.personal.profile_pic_details) {
        this.profilePhoto = this.personalDetails.personal.profile_pic_details.url;
        this.originalFileName = this.personalDetails.personal.profile_pic_details.originalFilename;
        this.s3Path = this.personalDetails.personal.profile_pic_details.s3Path;

      }

      this.basicDetailsForm.patchValue(this.personalDetails.personal);
      this.basicDetailsForm.get('firstName').patchValue(this.personalDetails.personal.name.firstName);
      this.basicDetailsForm.get('middleName').patchValue(this.personalDetails.personal.name.middleName);
      this.basicDetailsForm.get('lastName').patchValue(this.personalDetails.personal.name.lastName);
      this.basicDetailsForm.get('preferredName').patchValue(this.personalDetails.personal.name.preferredName);
      this.basicDetailsForm.get('salutation').patchValue(this.personalDetails.personal.name.salutation);


      this.basicDetailsForm.get('street1').patchValue(this.personalDetails.personal.address.street1);
      this.basicDetailsForm.get('street2').patchValue(this.personalDetails.personal.address.street2);
      this.basicDetailsForm.get('state').patchValue(this.personalDetails.personal.address.state);
      this.basicDetailsForm.get('city').patchValue(this.personalDetails.personal.address.city);
      this.basicDetailsForm.get('zipcode').patchValue(this.personalDetails.personal.address.zipcode);
      this.basicDetailsForm.get('country').patchValue(this.personalDetails.personal.address.country);

      this.basicDetailsForm.get('extention').patchValue(this.personalDetails.personal.contact.extention);
      this.basicDetailsForm.get('homePhone').patchValue(this.personalDetails.personal.contact.homePhone);
      this.basicDetailsForm.get('mobilePhone').patchValue(this.personalDetails.personal.contact.mobilePhone);
      this.basicDetailsForm.get('personalMail').patchValue(this.personalDetails.personal.contact.personalMail);
      this.basicDetailsForm.get('workMail').patchValue(this.personalDetails.personal.contact.workMail);
      this.basicDetailsForm.get('workPhone').patchValue(this.personalDetails.personal.contact.workPhone);

      if (this.personalDetails.personal.social_links) {
        this.basicDetailsForm.get('facebook').patchValue(this.personalDetails.personal.social_links.facebook);
        this.basicDetailsForm.get('linkedin').patchValue(this.personalDetails.personal.social_links.linkedin);
        this.basicDetailsForm.get('twitter').patchValue(this.personalDetails.personal.social_links.twitter);
      }


      this.educationList = res.data.personal.education;
      this.languageList = res.data.personal.languages;
      this.visaList = res.data.personal.visa_information;
      console.log(this.visaList);
      this.visaList.filter(item => {
        item.edit = true;
      });
      this.educationList.filter(item => {
        item.edit = true;
      });
      this.languageList.filter(item => {
        item.edit = true;
      });
    }, (err) => {
      console.log(err);

    })

  }
  // Author:Saiprakash G, Date:09-05-19
  // Save and update employee personal details

  personalSubmit() {

    var x = this.basicDetailsForm.get('ssn').value;
    x.length;

    console.log("XXX Length", x)

    for (var i = 0; i < 6; i++) {

      console.log((i));

      var str = '';
      str.replace['i'] = 'x';

      // x.replace[i] = 'x';

    }
    console.log("11111111111111", x)

    var tempVL = [];
    this.visaList.forEach(element => {
      if (element && element.visa_type && element.issuing_country && element.issued_date && element.expiration_date && element.status && element.notes && element.visa_type != "" && element.issuing_country != "" && element.issued_date != "" && element.expiration_date != "" && element.status != "" && element.notes != "") {
        tempVL.push(element);
      }
    });
    this.visaList = tempVL;

    var tempEL = [];
    this.educationList.forEach(element => {
      if (element && element.degree && element.specialization && element.university && element.start_date && element.end_date && element.gpa && element.degree != "" && element.specialization != "" && element.university != "" && element.start_date != "" && element.end_date != "" && element.gpa != "") {
        tempEL.push(element)
      }

    });
    this.educationList = tempEL;

    var tempLl = [];
    this.languageList.forEach(element => {
      if (element && element.language && element.level_of_fluency && element.language != "" && element.level_of_fluency != "") {
        tempLl.push(element)
      }
    });
    this.languageList = tempLl;

    var data: any = {

      _id: this.id,
      personal: {
        name: {
          firstName: this.basicDetailsForm.get('firstName').value,
          lastName: this.basicDetailsForm.get('lastName').value,
          middleName: this.basicDetailsForm.get('middleName').value,
          preferredName: this.basicDetailsForm.get('preferredName').value,
          salutation: this.basicDetailsForm.get('salutation').value
        },
        dob: this.basicDetailsForm.get('dob').value,
        gender: this.basicDetailsForm.get('gender').value,
        maritalStatus: this.basicDetailsForm.get('maritalStatus').value,
        effectiveDate: this.basicDetailsForm.get('effectiveDate').value,
        veteranStatus: this.basicDetailsForm.get('veteranStatus').value,
        race: this.basicDetailsForm.get('race').value,
        ssn: this.basicDetailsForm.get('ssn').value,
        ethnicity: this.basicDetailsForm.get('ethnicity').value,
        profile_pic_details: {
          originalFilename: this.originalFileName,
          s3Path: this.s3Path,
          url: this.profilePhoto
        },
        address: {
          street1: this.basicDetailsForm.get('street1').value,
          street2: this.basicDetailsForm.get('street2').value,
          city: this.basicDetailsForm.get('city').value,
          state: this.basicDetailsForm.get('state').value,
          zipcode: this.basicDetailsForm.get('zipcode').value,
          country: this.basicDetailsForm.get('country').value
        },
        contact: {
          workPhone: this.basicDetailsForm.get('workPhone').value,
          extention: this.basicDetailsForm.get('extention').value,
          mobilePhone: this.basicDetailsForm.get('mobilePhone').value,
          homePhone: this.basicDetailsForm.get('homePhone').value,
          workMail: this.basicDetailsForm.get('workMail').value,
          personalMail: this.basicDetailsForm.get('personalMail').value
        },
        social_links: {
          linkedin: this.basicDetailsForm.get('linkedin').value,
          twitter: this.basicDetailsForm.get('twitter').value,
          facebook: this.basicDetailsForm.get('facebook').value
        },
        education: this.educationList,
        languages: this.languageList,
        visa_information: this.visaList
      }


    }


    if (this.basicDetailsForm.valid) {
      this.personalService.updateUSer(data).subscribe((res: any) => {
        console.log(res);
        this.getUserPersonal();
        this.checkEduIds = [];
        this.checkLangIds = [];
        this.checkVisaIds = [];
        this.swalAlertService.SweetAlertWithoutConfirmation("Successfully updated user", "", "success");
        if (res.status == true) {
          this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/job"])

        }

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      }
      )
    }
    else {
      this.basicDetailsForm.get('firstName').markAsTouched();
      this.basicDetailsForm.get('middleName').markAsTouched();
      this.basicDetailsForm.get('lastName').markAsTouched();
      this.basicDetailsForm.get('preferredName').markAsTouched();
      this.basicDetailsForm.get('salutation').markAsTouched();
      this.basicDetailsForm.get('dob').markAsTouched();
      this.basicDetailsForm.get('gender').markAsTouched();
      this.basicDetailsForm.get('maritalStatus').markAsTouched();
      this.basicDetailsForm.get('effectiveDate').markAsTouched();
      this.basicDetailsForm.get('veteranStatus').markAsTouched();
      this.basicDetailsForm.get('race').markAsTouched();
      this.basicDetailsForm.get('ssn').markAsTouched();
      this.basicDetailsForm.get('ethnicity').markAsTouched();
      this.basicDetailsForm.get('street1').markAsTouched();
      this.basicDetailsForm.get('city').markAsTouched();
      this.basicDetailsForm.get('state').markAsTouched();
      this.basicDetailsForm.get('zipcode').markAsTouched();
      this.basicDetailsForm.get('country').markAsTouched();
      this.basicDetailsForm.get('workPhone').markAsTouched();
      this.basicDetailsForm.get('extention').markAsTouched();
      this.basicDetailsForm.get('mobilePhone').markAsTouched();
      this.basicDetailsForm.get('workMail').markAsTouched();
      this.basicDetailsForm.get('personalMail').markAsTouched();
      this.basicDetailsForm.get('linkedin').markAsTouched();
      this.basicDetailsForm.get('twitter').markAsTouched();
      this.basicDetailsForm.get('facebook').markAsTouched();

    }


  }
  // Author:Saiprakash G, Date:22-05-19
  // Edit visa information
  editVisa() {
    this.selectedVisaList = this.visaList.filter(checkVisa => {
      return checkVisa.isChecked
    })

    if (this.selectedVisaList.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one visa information to proceed", "", 'error')
    } else if (this.selectedVisaList.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one visa information to proceed", "", 'error')
    } else {
      if (this.selectedVisaList[0]._id) {
        this.selectedVisaList[0].edit = false;
      }
    }
  }
  // Author:Saiprakash G, Date:22-05-19
  // Edit education
  editEducation() {
    this.selectedEducationList = this.educationList.filter(checkEducation => {
      return checkEducation.isChecked
    })

    if (this.selectedEducationList.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one education to proceed", "", 'error')
    } else if (this.selectedEducationList.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one education to proceed", "", 'error')
    } else {
      if (this.selectedEducationList[0]._id) {
        this.selectedEducationList[0].edit = false;
      }
    }

  }
  // Author:Saiprakash G, Date:22-05-19
  // Edit language
  editLanguage() {
    this.selectedLanguage = this.languageList.filter(checkLanguage => {
      return checkLanguage.isChecked
    })

    if (this.selectedLanguage.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one language to proceed", "", 'error')
    } else if (this.selectedLanguage.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one language to proceed", "", 'error')
    } else {
      if (this.selectedLanguage[0]._id) {
        this.selectedLanguage[0].edit = false;
      }
    }
  }
  /* Description:accept only alphabets
   author : vipin reddy */
  alphabets(event) {
    return (event.charCode > 64 &&
      event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)
  }
  saveCompanyContactInfo() {

  }
  // Author:Saiprakash G, Date:22-05-19
  // Delete education
  deleteEducation() {
    this.selectedEducationList = this.educationList.filter(checkEducation => {
      return checkEducation.isChecked
    })
    if (this.selectedEducationList.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one education to proceed", "", 'error')
    } else {
      var data: any = {
        companyId: this.companyId,
        userId: this.id,
        _ids: this.selectedEducationList
      }
      this.personalService.deleteEducation(data).subscribe((res: any) => {
        console.log(res);
        this.checkEduIds = [];
        this.getUserPersonal();

        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }
  }
  // Author:Saiprakash G, Date:22-05-19
  // Delete language
  deleteLanguage() {
    this.selectedLanguage = this.languageList.filter(checkLanguage => {
      return checkLanguage.isChecked
    })

    if (this.selectedLanguage.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one language to proceed", "", 'error')
    } else {
      var data: any = {
        companyId: this.companyId,
        userId: this.id,
        _ids: this.selectedLanguage
      }
      this.personalService.deleteLanguages(data).subscribe((res: any) => {
        console.log(res);
        this.checkLangIds = [];
        this.getUserPersonal();
        this.swalAlertService.SweetAlertWithoutConfirmation("Deleted language successfully", "", "success");
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }
  }
  // Author:Saiprakash G, Date:22-05-19
  // Delete visa information
  deleteVisa() {
    this.selectedVisaList = this.visaList.filter(checkVisa => {
      return checkVisa.isChecked
    })

    if (this.selectedVisaList.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one visa information to proceed", "", 'error')
    } else {
      var data: any = {
        companyId: this.companyId,
        userId: this.id,
        _ids: this.selectedVisaList
      }
      this.personalService.deleteVisaInformation(data).subscribe((res: any) => {
        console.log(res);
        this.checkVisaIds = [];
        this.getUserPersonal();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }
  }



  getSsnFields() {
    var ID = JSON.parse(localStorage.getItem('companyId'));
    console.log("SSN IDDDDDDDd", ID)
    this.personalService.getPersonalSSNData(ID)
      .subscribe((res: any) => {
        console.log("SSN Responsee", res)
        this.ssnMask = res.data;
      });
  }

  // Author:Saiprakash G, Date:07-08-19
  // Salutation validation

  selectSalutation(event) {
    console.log(event);
    if (event) {
      this.salutationName = event;
    }

  }

  omit_special_char(event) {

    const pattern = /^[0-9#$%^&*()@!.-]*$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  // Author:Saiprakash G, Date:08/08/19
  // Cancel personal details it's redirect to directory page

  cancelPersonalDetails() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/directory/directory"])
  }


}
