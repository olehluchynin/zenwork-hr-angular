import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MyInfoService } from '../../../../services/my-info.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { Router } from '@angular/router';
import { PayRollService } from '../../../../services/pay-roll.service';




declare var $: any;

@Component({
  selector: 'app-compensation',
  templateUrl: './compensation.component.html',
  styleUrls: ['./compensation.component.css']
})
export class CompensationComponent implements OnInit {


  public tabName: string = "Compensation";
  public compensationData: any;
  public fieldsShow: any = {
    Compensation_ratio: {
      hide: '',
      required: '',
      format: ''
    },
    Highly_compensated_Employee_type: {
      hide: '',
      required: '',
      format: ''
    },
    Nonpaid_Record: {
      hide: '',
      required: '',
      format: ''
    },
    Pay_frequency: {
      hide: '',
      required: '',
      format: ''
    },
    Pay_group: {
      hide: '',
      required: '',
      format: ''
    },
    Salary_Grade: {
      hide: '',
      required: '',
      format: ''
    },
  }

  public compensationDetails: any;
  public id: any;
  public showFilelds: boolean = true;
  public data2:any = {
   
  }
  public tepmCF: any = 
    {
      payRate: Number,
      effective_date: '',
      payType: '',
      Pay_frequency: '',
      notes: '',
      changeReason: '',
    }
  
  
  public currentCompensation = [
   
  ]
  public getCompensation:any;
  public companyId:any;
  public selectedCurrentCompensation = [];
  public getAllStructureFields:any;
  public customPayFrequency = [];
  public defaultPayFrewuency = [];
  public allPayFrequency = [];
  public customPayType = [];
  public defaultPayType = [];
  public allPayType = [];
  public customChangeReason = [];
  public defaultChangeReason = [];
  public allChangeReason = [];
  public currentCompensationId:any;
  payGroupList = [];
  salaryGradeList = []
  Pay_group: any;
  payTypeFields:boolean = false;
  payTypeBonus:any;
  salaryGradeValue:any;
  compensationRatio: any;
  compensationError: boolean = false;
  calculateCompensation: any;
  salaryGradeId:any;
  workHours:any;
  checkCompensationIds = [];
  nonPaidRecord:any;
  roleType:any;
  compensationTabView:any;

  @ViewChild('openEdit') openEdit: ElementRef

  constructor(
    private myInfoService: MyInfoService,
    private swalAlertService: SwalAlertService,
    private accessLocalStorageService: AccessLocalStorageService,
    private router: Router,
    private payRollService: PayRollService
  ) { }

  compensationDetailsForm: FormGroup
  compensationForm: FormGroup
  roletype:any;
  pagesAccess=[];

  ngOnInit() {
    this.roletype = JSON.parse(localStorage.getItem('type'))
    if (this.roletype != 'company') {
      this.pageAccesslevels();
    }

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);

    this.compensationDetails = JSON.parse(localStorage.getItem('employee'));
    if(this.compensationDetails){
    this.id = this.compensationDetails._id
    }

    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.id = JSON.parse(localStorage.getItem('employeeId'));
    }
    
    // console.log(this.compensationDetails.compensation);
    // this.currentCompensation = this.compensationDetails.compensation.current_compensation;
    
    // console.log(this.currentCompensation);
    this.listSalaryGrades();
    this.payRollGroupList();
    this.getUserCompensation();
    this.getSingleTabSettingForCompensation();
    this.getStandardAndCustomStructureFields();
    this.initCompensationDetailsForm();


    this.compensationForm = new FormGroup({

      payRate: new FormControl("", [Validators.required]),
      payType: new FormControl("", [Validators.required]),
      Pay_frequency: new FormControl("", [Validators.required]),
      compensationRatio: new FormControl("", [Validators.required]),
      changeReason: new FormControl("", [Validators.required]),
      effective_date: new FormControl("", [Validators.required]),
      notes: new FormControl("", [Validators.required]),
      Pay_group: new FormControl("", [Validators.required]),
      Salary_Grade: new FormControl("", [Validators.required]),
      grade_band1: new FormControl("", [Validators.required]),



    })
  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Compensation" && element.access == 'view') {
              console.log('data comes in2');

              this.compensationTabView = true;
              console.log('loggss', this.compensationTabView);

            }

          });

        },
        (err: any) => {

        })
  }

  initCompensationDetailsForm() {

    this.compensationDetailsForm = new FormGroup({
      NonpaidPosition: new FormControl(""),
      HighlyCompensatedEmployee: new FormControl(""),
      // Pay_group: new FormControl(""),
      // Pay_frequency: new FormControl(""),
      Salary_Grade: new FormControl(""),
      grade_band: new FormControl(""),
      // compensationRatio: new FormControl(""),

    })
  }
  nonPaidRecordsFields(event){
    console.log(event);
    this.nonPaidRecord = event;
    
    if(event == true){
      this.showFilelds = false
    } else {
      this.showFilelds = true
    }
  }

  // Author:Saiprakash, Date:15/05/2019
  // Get Standard And Custom Structure Fields

getStandardAndCustomStructureFields(){
  this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
    (res:any)=>{
      console.log(res); 
      this.getAllStructureFields = res.data;
   
      // this.customPayFrequency = this.getAllStructureFields['Pay Frequency'].custom;
      // this.defaultPayFrewuency = this.getAllStructureFields['Pay Frequency'].default;
      // this.allPayFrequency = this.customPayFrequency.concat(this.defaultPayFrewuency);
      // this.customPayType = this.getAllStructureFields['Pay Type'].custom;
      // this.defaultPayType = this.getAllStructureFields['Pay Type'].default;
      // this.allPayType = this.customPayType.concat(this.defaultPayType); 
      this.customChangeReason = this.getAllStructureFields['Job Change Reason'].custom;
      this.defaultChangeReason = this.getAllStructureFields['Job Change Reason'].default;
      this.allChangeReason = this.customChangeReason.concat(this.defaultChangeReason)
console.log(this.allChangeReason);

    }
  )
}


  // Author:Saiprakash, Date:12/04/2019
  // Get single tab settings for compensation details

  getSingleTabSettingForCompensation() {

    this.myInfoService.getSingleTabSettingForMyinfo(this.tabName).subscribe((res: any) => {
      console.log(res);
      this.fieldsShow = res.data.fields;
      console.log(this.fieldsShow);

    }, err => {
      console.log(err);

    })
  }

  // Author:Saiprakash G, Date:09-05-19
  // save employee compensation details

  saveCompensationDetails() {
    var data1: any = {
      _id: this.id,
      compensation: {
        NonpaidPosition: this.compensationDetailsForm.get('NonpaidPosition').value,
        HighlyCompensatedEmployee: this.compensationDetailsForm.get('HighlyCompensatedEmployee').value,
        Salary_Grade: this.compensationDetailsForm.get('Salary_Grade').value,
        grade_band: this.salaryGradeId,
        current_compensation: this.currentCompensation
      }
    }
    if(this.compensationDetailsForm.valid){
      this.myInfoService.updateUSer(data1).subscribe((res: any) => {
        console.log(res);
        this.getUserCompensation();
        this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/notes"])
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
    } else {
      this.compensationDetailsForm.get('NonpaidPosition').markAsTouched();
      this.compensationDetailsForm.get('HighlyCompensatedEmployee').markAsTouched();
      // this.compensationDetailsForm.get('Pay_group').markAsTouched();
      // this.compensationDetailsForm.get('Pay_frequency').markAsTouched();
      // this.compensationDetailsForm.get('Salary_Grade').markAsTouched();
      // this.compensationDetailsForm.get('compensationRatio').markAsTouched();
    }
  

  }
   // Author:Saiprakash, Date:14/05/2019
  // Get user compensation

  getUserCompensation(){
    this.myInfoService.getOtherUser(this.companyId, this.id).subscribe((res:any)=>{
      console.log(res);
      this.compensationData = res.data;
      // console.log(this.compensationData.compensation.Pay_group);


      // if(this.compensationData.compensation.Pay_group){
      //   this.payRollService.getSinglePayrollGroup(this.companyId ,this.compensationData.compensation.Pay_group).subscribe((res:any)=>{
      //     console.log(res);
      //     this.compensationForm.get('Pay_frequency').patchValue(res.data.pay_frequency)
      //   },(err)=>{
      //     console.log(err);
          
      //   })
      // }
      this.compensationDetailsForm.get('Salary_Grade').patchValue(this.compensationData.compensation.Salary_Grade);
      this.compensationForm.get('Salary_Grade').patchValue(this.compensationData.compensation.Salary_Grade);
      if(this.compensationData.compensation.grade_band){
        this.compensationDetailsForm.get('grade_band').patchValue(this.compensationData.compensation.grade_band.grade);
        this.salaryGradeValue = this.compensationData.compensation.grade_band.grade;
        this.salaryGradeId = this.compensationData.compensation.grade_band._id;
      }
      if(this.compensationData.job.workHours){
        this.workHours = this.compensationData.job.workHours;
      }
    
      
      this.compensationDetailsForm.patchValue(this.compensationData.compensation);
      if(this.compensationData.compensation.NonpaidPosition == false){
        this.showFilelds = true;
      } else if(this.compensationData.compensation.NonpaidPosition == true){
        this.showFilelds = false;
      }
  if(res.data.compensation.current_compensation && res.data.compensation.current_compensation.length){
    this.currentCompensation = res.data.compensation.current_compensation;

  }
    }, (err)=>{
      console.log(err);
      
    })

  }

   // Author:Saiprakash, Date:14/05/2019
  // Add and edit user compensation

  addCompensation() {

  
  if(this.payTypeBonus == "Bonus"){
      var bonusCompensation: any = {
        payRate: this.compensationForm.get('payRate').value,
        effective_date: this.compensationForm.get('effective_date').value,
        payType: this.compensationForm.get('payType').value,
        Pay_frequency: this.compensationForm.get('Pay_frequency').value,
        notes: this.compensationForm.get('notes').value,
        changeReason: this.compensationForm.get('changeReason').value,
      }

      this.currentCompensation.push(bonusCompensation);
      console.log(this.currentCompensation);
      
      var bonusData:any = {
        _id: this.id,
        compensation: {
        NonpaidPosition: this.compensationDetailsForm.get('NonpaidPosition').value,
        HighlyCompensatedEmployee: this.compensationDetailsForm.get('HighlyCompensatedEmployee').value,
        Salary_Grade: this.compensationDetailsForm.get('Salary_Grade').value,
        grade_band: this.compensationData.compensation.grade_band,
        current_compensation: this.currentCompensation
  
      }
      }
      if(this.compensationForm.get('payRate').value && this.compensationForm.get('Pay_frequency').value && this.compensationForm.get('payType').value){
        this.myInfoService.updateUSer(bonusData).subscribe((res: any) => {
          console.log(res);
          this.compensationForm.reset();
          $("#myModal").modal("hide");
          this.getUserCompensation();
          this.swalAlertService.SweetAlertWithoutConfirmation( res.message, "","success");
      
      
        }, (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      
        })
      } else {
        this.compensationForm.get('Pay_frequency').markAsTouched();
        this.compensationForm.get('payRate').markAsTouched();
        this.compensationForm.get('payType').markAsTouched();

      }

   

  }

else {

  this.tepmCF = 
  {
    payRate: this.compensationForm.get('payRate').value,
    effective_date: this.compensationForm.get('effective_date').value,
    payType: this.compensationForm.get('payType').value,
    Pay_group: this.compensationForm.get('Pay_group').value,
    // Pay_frequency: this.compensationForm.get('Pay_frequency').value,
    notes: this.compensationForm.get('notes').value,
    changeReason: this.compensationForm.get('changeReason').value,
    compensationRatio: this.compensationForm.get('compensationRatio').value

  }
  

console.log(this.tepmCF,this.currentCompensation);

  this.currentCompensation.push(this.tepmCF);



console.log(this.currentCompensation);


this.data2 = {
  _id: this.id,
  compensation: {
  NonpaidPosition: this.compensationDetailsForm.get('NonpaidPosition').value,
  HighlyCompensatedEmployee: this.compensationDetailsForm.get('HighlyCompensatedEmployee').value,
  Salary_Grade: this.compensationDetailsForm.get('Salary_Grade').value,
  grade_band: this.compensationData.compensation.grade_band,
  current_compensation: this.currentCompensation

}
}
if(this.compensationForm.get('payRate').value && this.compensationForm.get('Pay_group').value && this.compensationForm.get('payType').value){
  this.myInfoService.updateUSer(this.data2).subscribe((res: any) => {
    console.log(res);
    this.compensationForm.reset();
    $("#myModal").modal("hide");
    this.getUserCompensation();
    this.swalAlertService.SweetAlertWithoutConfirmation( res.message, "","success");


  }, (err) => {
    console.log(err);
    this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");

  })
} else {
  this.compensationForm.get('Pay_group').markAsTouched();
  this.compensationForm.get('payRate').markAsTouched();
  this.compensationForm.get('payType').markAsTouched();
}
 
}
 


   }

 

  editCompensation(){
    var data3:any = {
      companyId: this.companyId,
      userId: this.id,
      cur_comp_id: this.currentCompensationId,
      payRate: this.compensationForm.get('payRate').value,
      effective_date: this.compensationForm.get('effective_date').value,
      payType: this.compensationForm.get('payType').value,
      notes: this.compensationForm.get('notes').value,
      changeReason: this.compensationForm.get('changeReason').value,
      compensationRatio: this.compensationForm.get('compensationRatio').value
  }
  if(this.payTypeBonus == "Bonus"){
    data3.Pay_frequency = this.compensationForm.get('Pay_frequency').value;
    if(data3.payRate){
      this.myInfoService.updateCurrentCompensation(data3).subscribe((res: any) => {
        console.log(res);
        this.compensationForm.reset();
       
        $("#myModal").modal("hide");
        this.currentCompensationId = "";
        this.checkCompensationIds = [];
        this.getUserCompensation();
      this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
  
    
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    
      })
    } else {
      this.compensationForm.get('payRate').markAsTouched();
    }

  }

  else {
    data3.Pay_group = this.compensationForm.get('Pay_group').value;
    if(data3.payRate){
      this.myInfoService.updateCurrentCompensation(data3).subscribe((res: any) => {
        console.log(res);
        this.compensationForm.reset();
       
        $("#myModal").modal("hide");
        this.currentCompensationId = "";
        this.checkCompensationIds = [];
        this.getUserCompensation();
      this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
  
    
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    
      })
    } else {
      this.compensationForm.get('payRate').markAsTouched();
    }
   
  }
  
  }

  checkCurrentCompensation(event,id){



       this.currentCompensationId = id;
       console.log(event, id);
       if(event.checked == true){
         this.checkCompensationIds.push(id);
         
       }
       console.log(this.checkCompensationIds);
       if(event.checked == false){
         var index = this.checkCompensationIds.indexOf(id);
           if (index > -1) {
             this.checkCompensationIds.splice(index, 1);
           }
           console.log(this.checkCompensationIds);
       }
  }

   // Author:Saiprakash, Date:14/05/2019
  // get single comensation details

  edit(){
    this.selectedCurrentCompensation = this.currentCompensation.filter(compensationCheck => {
      return compensationCheck.isChecked
    })
  
    if (this.selectedCurrentCompensation.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one compensation to proceed","", 'error')
    } else if (this.selectedCurrentCompensation.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one compensation to proceed","", 'error')
    } else {
      this.openEdit.nativeElement.click();
     
      this.compensationForm.patchValue(this.selectedCurrentCompensation[0]);
      console.log(this.selectedCurrentCompensation[0]);
      this.Pay_group = this.selectedCurrentCompensation[0].Pay_group._id;
      this.compensationForm.get('Pay_frequency').patchValue(this.selectedCurrentCompensation[0].Pay_group.pay_frequency)
      this.compensationForm.get('payRate').patchValue(this.selectedCurrentCompensation[0].payRate);
      this.compensationForm.get('effective_date').patchValue(new Date (this.selectedCurrentCompensation[0].effective_date));
      this.compensationForm.get('compensationRatio').patchValue(this.selectedCurrentCompensation[0].compensationRatio);
      this.payRateChangeforCompensation();
    }
  
  }

  listSalaryGrades(){
    var data:any = {
      companyId: this.companyId,
      // archive: false
    }
    this.payRollService.getSalrayGradeList(data).subscribe((res:any)=>{
      console.log(res);
      this.salaryGradeList = res.data
      
    },(err)=>{
      console.log(err);
      
    })

  }

  payRollGroupList(){
    var data:any = {
      companyId: this.companyId,
      // status: "Active"
    }
    this.payRollService.getPayrollGroup(data).subscribe((res:any)=>{
      console.log(res);
      this.payGroupList = res.data;
    },(err)=>{
      console.log(err);
      
    })
  }

  payGroupChange() {

    console.log(this.Pay_group);
    if (this.Pay_group) {
      this.payRollService.getSinglePayrollGroup(this.companyId, this.Pay_group)
        .subscribe((res: any) => {
          console.log(res);
          this.compensationForm.get('Pay_frequency').patchValue(res.data.pay_frequency);
        },
          (err) => {

            console.log(err);

          })
    }
  }

  payTypeSelect(type){
    console.log(type);
    this.payTypeBonus = type;
    if(type == "Salary" || type == "Hourly"){
      this.payTypeFields = true;
    } else if(type == "Bonus"){
      this.payTypeFields = false;
    }
    
  }


  payRateChangeforCompensation() {
    this.compensationRatio = ''
    this.compensationForm.get('compensationRatio').patchValue('')
    var postData = {
      payRate: this.compensationForm.get('payRate').value,
      payType: this.compensationForm.get('payType').value,
      pay_frequency: this.compensationForm.get('Pay_frequency').value,
      grade_band: "",
      workHours: this.workHours
    }
    if (this.salaryGradeId) {
      postData.grade_band = this.salaryGradeId
    }
    if (postData.payRate && postData.payType && postData.pay_frequency) {

      this.payRollService.compensationRationCalculate(postData)
        .subscribe((res: any) => {
          console.log(res);
          if (res.status == true) {
            this.compensationError = false;
            this.calculateCompensation = res.data
            this.compensationForm.get('compensationRatio').patchValue(this.calculateCompensation.compensationRatio)
            this.compensationRatio = this.calculateCompensation.compensationRatio

          }
        },
          (err) => {
            console.log(err);
            this.compensationError = true;
            // this.swalAlertService.SweetAlertWithoutConfirmation('Compensation Ratio',err.error.message,'error');
          })
    }
  }

  nextTab(){
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/notes"])

  }
  keyPress(event: any) {
    const pattern = /^[0-9#$%^&*()@!]*$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  cancelForm(){
    this.compensationForm.reset();
    this.getUserCompensation();
    this.checkCompensationIds = [];
  }

  openDialog(){
    
    this.compensationForm.reset();
    this.getUserCompensation();
  }
  // cancelDetails(){
  //   this.compensationDetailsForm.reset();
  //   this.getUserCompensation();
  // }
}
