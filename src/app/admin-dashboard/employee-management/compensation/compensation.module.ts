import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompensationComponent } from './compensation/compensation.component';
import { CompensationRouting } from './compensation.routing';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MyInfoService } from '../../../services/my-info.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PayRollService } from '../../../services/pay-roll.service';



@NgModule({
  imports: [
    CommonModule,
    CompensationRouting,
    MaterialModuleModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [CompensationComponent],
  providers: [MyInfoService,PayRollService]
})
export class CompensationModule { }
