import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompensationComponent } from './compensation/compensation.component';

const routes: Routes = [
    // { path:'',redirectTo:"/compensation",pathMatch:"full" },
    {path:'',component:CompensationComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class CompensationRouting { }