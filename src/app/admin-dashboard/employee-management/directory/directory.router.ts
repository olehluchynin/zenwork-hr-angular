import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirectoryComponent } from './directory.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    { path:'',redirectTo:"/directory",pathMatch:"full" },
    {path:'directory',component:DirectoryComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes),ReactiveFormsModule],
    exports: [RouterModule,ReactiveFormsModule]
})

export class DirectoryRouting { }