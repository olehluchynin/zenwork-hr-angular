import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectoryRouting } from './directory.router';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { DirectoryComponent } from './directory.component';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { DirectorySettingsComponent } from './directory-settings/directory-settings.component';

import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';


@NgModule({
  imports: [
    CommonModule,
    DirectoryRouting,
    MaterialModuleModule,
    AccordionModule.forRoot(),
    FormsModule,MatSelectModule,MatExpansionModule,MatCheckboxModule,MatFormFieldModule,MatInputModule
  ],

  declarations: [DirectoryComponent, DirectorySettingsComponent],
  entryComponents: [
    DirectorySettingsComponent
  ]
})
export class DirectoryModule { }
