import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DirectoryService } from '../../../../services/directory.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';

@Component({
  selector: 'app-directory-settings',
  templateUrl: './directory-settings.component.html',
  styleUrls: ['./directory-settings.component.css']
})
export class DirectorySettingsComponent implements OnInit {
  
  companyId: any;
  directorySettings = {

    "companyId": "",
    "settings": {
      "photo": true,
      "name": true,
      "job_title": true,
      "location": true,
      "email": true,
      "work_phone": true,
      "cell_phone": true,
      "department": true,
      "business_unit": true

    }

  }


  constructor(public dialogRef: MatDialogRef<DirectorySettingsComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private directoryService: DirectoryService,
    private swalAlertService: SwalAlertService,
    private siteAccessRolesService:SiteAccessRolesService) { }

  ngOnInit() {
    var cID = localStorage.getItem('companyId')
    console.log("cid", cID);
    cID = cID.replace(/^"|"$/g, "");
    console.log("cin2", cID);
    this.companyId = cID;
    this.directorySettings.companyId = cID;
    this.getDirectorySettings(this.companyId);
    

  }


 
  /* Description: getv directory settings fields
 author : vipin reddy */


  getDirectorySettings(companyId) {
    this.directoryService.getDirectorySettings(companyId)
      .subscribe(
        (res: any) => {
          console.log("get directory settings", res);
          this.directorySettings.settings = res.data.settings;
        },
        (err) => {
          console.log(err);

        })
  }

  /* Description: update directory settings fields
author : vipin reddy */

  sendSettings() {
    this.directoryService.updateDirectorySettings(this.directorySettings)
      .subscribe(
        (res: any) => {
          console.log("directory settings update", res);
          this.swalAlertService.SweetAlertWithoutConfirmation("Update directory settings", res.message, "success")
          if (res.status == true) {
            this.directorySettings.settings = res.data;
            this.cancel();
          }

        },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation("error", err.error.message, "error")
        })
  }

  cancel() {

    this.dialogRef.close()
  }

}
