import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { EmployeeService } from '../../../services/employee.service';
import { AccessLocalStorageService } from '../../../services/accessLocalStorage.service';
// import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DirectorySettingsComponent } from './directory-settings/directory-settings.component';
import { DirectoryService } from '../../../services/directory.service';
import { CompanySettingsService } from '../../../services/companySettings.service';

import { Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError} from '@angular/router';
import { LoaderService } from '../../../services/loader.service'; 

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {
  public individualEmployee: boolean = true;
  public individualEmployeeDropdown: boolean = true;
  public individualEmployeefly: boolean = true;
  public individualEmployeeflyDropdown: boolean = false;

  selectedNav: any;
  updateBenefits: boolean = true;
  chooseFields: boolean = false;
  addEditDelete: boolean = false;
  summaryReview: boolean = false;


  modalRef: BsModalRef;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private modalService: BsModalService,
    private employeeOnboardingService: EmployeeService,
    private swalAlertService: SwalAlertService,
    private accessLocalStorageService: AccessLocalStorageService,
    private directoryService: DirectoryService,
    private companySettingsService: CompanySettingsService,
    private loaderService: LoaderService
  ) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
   }
   navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.loader(true);
    }
    if (event instanceof NavigationEnd) {
      this.loaderService.loader(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loaderService.loader(false);
    }
    if (event instanceof NavigationError) {
      this.loaderService.loader(false);
    }
  }

  employeeSearch: FormGroup;
  perPage;
  // pages;
  pageNo;
  companyId: any;
  allEmpLength: any;
  directorySettings = {

    "companyId": "",
    "settings": {
      "photo": true,
      "name": true,
      "job_title": true,
      "location": true,
      "email": true,
      "work_phone": true,
      "cell_phone": true,
      "department": true,
      "business_unit": true

    }

  }

  allEmployeeDetails = [
    {
      personal: {
        name: {
          firstName: '',
          middleName: '',
          lastName: '',
        },
        address: {
          city: '',
          state: '',
          country: ''
        },
        contact: {
          workMail: '',
          workPhone: '',
          extention: '',
          homePhone: ''
        }
      },
      job: {
        jobTitle: {
          name: '',
        }
      },
    }
  ]
  filterData = {
    location: [],
    employment_status: [],
    department: [],
    employment_type: []
  }
  StructureValues: any = [];
  StructureValuesEmpStatus = [];
  customValuesEmpStatus = [];
  StructureValuesLocation = [];
  customValuesLocation = [];
  StructureValuesEmpType = [];
  customValuesEmpType = [];
  StructureValuesDepartment = [];
  customValuesDepartment = [];
  customValues = []

  @ViewChild('ref') ref;

  ngOnInit() {
    this.employeeSearch = new FormGroup({
      search: new FormControl('')
    });
    this.perPage = 10;

    this.pageNo = 1;
    this.onChanges();

    this.companyId = JSON.parse(localStorage.getItem('companyId'))
    this.getDirectorySettings(this.companyId);
    this.getEmployeeDetails();
    // this.getStructureFields()
    this.getStructureFields('Employment Status')
    this.getStructureFields('Location')
    this.getStructureFields('Employee Type')
    this.getStructureFields('Department')
  }

  getStructureFields(fields) {
    // this.customValues = []
    // this.StructureValues = []
    this.companySettingsService.getStuctureFields(fields, 0).subscribe((res: any) => {
      console.log(res, name);
      if (fields == 'Employment Status') {
        this.StructureValuesEmpStatus = res.default;
        this.customValuesEmpStatus = res.custom


        this.customValuesEmpStatus.forEach(element => {
          this.StructureValuesEmpStatus.push(element)
        });
        this.StructureValuesEmpStatus = this.StructureValuesEmpStatus.map(function (el) {
          var o = Object.assign({}, el);
          o.isChecked = false;
          return o;
        })
      }
      else if (fields == 'Location') {
        this.StructureValuesLocation = res.default;
        this.customValuesLocation = res.custom


        this.customValuesLocation.forEach(element => {
          this.StructureValuesLocation.push(element)
        });
        this.StructureValuesLocation = this.StructureValuesLocation.map(function (el) {
          var o = Object.assign({}, el);
          o.isChecked = false;
          return o;
        })
      }


      else if (fields == 'Employee Type') {

        this.StructureValuesEmpType = res.default;
        this.customValuesEmpType = res.custom


        this.customValuesEmpType.forEach(element => {
          this.StructureValuesEmpType.push(element)
        });
        this.StructureValuesEmpType = this.StructureValuesEmpType.map(function (el) {
          var o = Object.assign({}, el);
          o.isChecked = false;
          return o;
        })

      }
      else if (fields = 'Department') {
        this.StructureValuesDepartment = res.default;
        this.customValuesDepartment = res.custom


        this.customValuesDepartment.forEach(element => {
          this.StructureValuesDepartment.push(element)
        });
        this.StructureValuesDepartment = this.StructureValuesDepartment.map(function (el) {
          var o = Object.assign({}, el);
          o.isChecked = false;
          return o;
        })
      }
      console.log(this.StructureValuesDepartment);
    }, err => {
      console.log(err);

    })
    // console.log(this.StructureValues);
  }
  getDirectorySettings(id) {
    this.directoryService.getDirectorySettings(id)
      .subscribe(
        (res: any) => {
          console.log("get directory settings", res);
          this.directorySettings.settings = res.data.settings;
        },
        (err) => {
          console.log(err);

        })
  }


  onChanges() {
    this.employeeSearch.controls['search'].valueChanges
      .subscribe(val => {
        // console.log(this.zenworkersSearch.get('search').value);
        this.getEmployeeDetails();
      });
  }



  openModalWithClass(template: TemplateRef<any>) {
    this.selectedNav = 'updateBenefits';
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }
  detailEmployee() {
    this.individualEmployee = !this.individualEmployee;
    this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
  }
  // hideEmployeeData() {
  //   this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
  //   this.individualEmployee = !this.individualEmployee;
  // }
  // detailEmployeefly() {
  //   this.individualEmployeefly = !this.individualEmployeefly;
  //   this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
  // }
  // hideEmployeeflyData() {
  //   this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
  //   this.individualEmployeefly = !this.individualEmployeefly;
  // }
  chooseFieldss(event: any) {
    this.selectedNav = event;
    if (event == "updateBenefits") {
      this.updateBenefits = true;
      this.addEditDelete = false;
      this.chooseFields = false;
      this.summaryReview = false;
    }
    else if (event == "chooseFields") {
      this.chooseFields = true;
      this.updateBenefits = false;
      this.addEditDelete = false;
      this.summaryReview = false;
    }
    else if (event == "addEditDelete") {
      this.addEditDelete = true;
      this.updateBenefits = false;
      this.chooseFields = false;
      this.summaryReview = false;
    }
    else if (event == 'summaryReview') {
      this.summaryReview = true;
      this.updateBenefits = false;
      this.addEditDelete = false;
      this.chooseFields = false;

    }

  }

  pageEvent($event) {
    console.log("pages", $event);
    this.pageNo = $event.pageIndex + 1;
    this.perPage = $event.pageSize;
    this.getEmployeeDetails();
  }

  /* Getting Employee Directory(All Employees) Details from Backend */

  getEmployeeDetails() {
    var postData = {
      search_query: this.employeeSearch.get('search').value,
      page_no: this.pageNo,
      per_page: this.perPage,
      sort: {
        // "personal.name.firstName": 1,
        // "personal.name.lastName": 1,
        // "job.businessUnit": 1,
        // "job.department": 1,
        // "job.location": 1,
        updatedAt: -1
      },
      employment_status: [],
      employment_type: [],
      location: [],
      department: []

    }
    if (this.filterData.department.length > 0) {
      postData.department = this.filterData.department
    }
    else if (this.filterData.employment_status.length > 0) {
      postData.employment_status = this.filterData.employment_status
    }
    else if (this.filterData.employment_type.length > 0) {
      postData.employment_type = this.filterData.employment_type
    }
    else if (this.filterData.location.length > 0) {
      postData.location = this.filterData.location
    }

    this.employeeOnboardingService.getOnboardedEmployeeDetails(postData)
      .subscribe(
        (res: any) => {
          console.log("directory res", res);
          if (res.status) {
            // this.swalAlertService.SweetAlertWithoutConfirmation("Employees", res.message, 'success')
            this.allEmployeeDetails = res.data
            this.allEmpLength = res.total_count;
            console.log(this.allEmployeeDetails.length, "len");

          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Employees", "Employee Data Not Found", 'info')
          }
        },
        (err: HttpErrorResponse) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Employees", err.error.message, 'error')
        }
      )
  }

  /* Getting Employee Directory Details from Backend Ending*/



  /* Edit Details of Employee */

  editDetailsOfEmployee(employee) {
    this.accessLocalStorageService.set('employee', employee);
    this.router.navigate(['/admin/admin-dashboard/employee-management/profile/profile-view/personal'])
  }




  openDialog(): void {


    const dialogRef = this.dialog.open(DirectorySettingsComponent, {
      width: '1200px',
      height: '700px',
      backdropClass: 'directory-settings-model',
      data: ""

    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.getDirectorySettings(this.companyId);
      this.getEmployeeDetails()

    });


  }

  linkedInLink(link) {
    console.log(link);
    window.open(link, "_blank");

  }

  openGroup(data) {
    console.log(data);
    this.getStructureFields(data)
  }

  filterEmployee(event, data, value) {
    console.log(event, data, value);
    // event.preventDefault();
    // console.log('onClick this.ref._checked '+ this.ref._checked);
    // this.ref._checked = !this.ref._checked;

    if (data == 'Employee Type' && event.checked == true) {

      this.filterData.employment_type.push(value.name)
      this.StructureValuesEmpStatus.forEach(element => {
        if (element.name == value.name) {
          element.isChecked = true;
        }
      });
    }
    if (data == 'Location' && event.checked == true) {
      this.filterData.location.push(value.name)
      this.StructureValuesLocation.forEach(element => {
        if (element.name == value.name) {
          element.isChecked = true;
        }
      });
    }
    if (data == 'Employment Status' && event.checked == true) {
      this.filterData.employment_status.push(value.name)
      this.StructureValuesEmpStatus.forEach(element => {
        if (element.name == value.name) {
          element.isChecked = true;
        }
      });
    }
    if (data == 'Department' && event.checked == true) {

      this.filterData.department.push(value.name)
      this.StructureValuesLocation.forEach(element => {
        if (element.name == value.name) {
          element.isChecked = true;
        }
      });
    }

    if (data == 'Employee Type' && event.checked == false) {
      var index = this.filterData.employment_type.indexOf(value.name)
      this.filterData.employment_type.splice(index, 1)
    }
    if (data == 'Location' && event.checked == false) {
      var index = this.filterData.employment_type.indexOf(value.name)
      this.filterData.location.splice(index, 1)
    }
    if (data == 'Employment Status' && event.checked == false) {
      var index = this.filterData.employment_type.indexOf(value.name)
      this.filterData.employment_status.splice(index, 1)
    }
    if (data == 'Department' && event.checked == false) {
      var index = this.filterData.employment_type.indexOf(value.name)
      this.filterData.department.splice(index, 1)
    }

    console.log(this.filterData);

    this.getEmployeeDetails();


  }

  somethingClick(checkbox, item: { id: string }) {
    console.log(checkbox, item);

  }
  filterOptionsClear() {
    this.filterData.department = [];
    
    this.filterData.employment_status = [];
    this.filterData.employment_type = [];
    this.filterData.location = [];

    this.StructureValuesEmpStatus.forEach(element => {
      element.isChecked = false;
    })

    this.StructureValuesLocation.forEach(element => {
      element.isChecked = false;
    })

    this.StructureValuesEmpType.forEach(element => {
      element.isChecked = false;
    })

    this.StructureValuesDepartment.forEach(element => {
      element.isChecked = false;
    })
    this.getEmployeeDetails()
  }
}
