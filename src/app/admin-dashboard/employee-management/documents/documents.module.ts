import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentsComponent } from './documents/documents.component';
import { DocumentsRouting } from './documents.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { DocumentDialogComponent } from './documents/document-dialog/document-dialog.component';


@NgModule({
  imports: [
    CommonModule,
    DocumentsRouting,
    FormsModule,
    ReactiveFormsModule,
    MaterialModuleModule
  ],
  declarations: [DocumentsComponent, DocumentDialogComponent],
  entryComponents: [DocumentDialogComponent]
})
export class DocumentsModule { }
