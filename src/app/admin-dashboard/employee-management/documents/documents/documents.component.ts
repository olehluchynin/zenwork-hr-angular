import { Component, OnInit } from '@angular/core';
import { MyInfoService } from '../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DocumentDialogComponent } from './document-dialog/document-dialog.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  public pageNo: any;
  public perPage: any;
  public count: any;
  public companyId: any;
  public userId: any;
  public employeeDetails: any;
  public documentName: any;
  public documentType: any;
  public files: any;
  public allDocuments = [];
  public selectedDocuments = [];
  public fileName: any;
  public getSingleDocument: any;
  public filterDocumentType: any;
  public sortDocument: any;
  public getAllStructureFields: any;
  public customDocumentType = [];
  public defaultDocumentType = [];
  public allDocumentType = [];
  checkDocumentIds = [];
  // public documentTypes = [
  //   "New Hire Documents","Workflow Attachments", "Task-list Attachments", "Signed Documents", "Resume and Applications"
  // ]
  roleType: any;
  pagesAccess = [];
  documentsTabView: boolean = false;

  constructor(
    public dialog: MatDialog,
    private myInfoService: MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private router: Router

  ) { }

  ngOnInit() {
    this.pageNo = 1;
    this.perPage = 10;

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    if (this.employeeDetails) {
      this.userId = this.employeeDetails._id;
    }
    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.userId = JSON.parse(localStorage.getItem('employeeId'));
      this.pageAccesslevels();
    }
    console.log(this.userId);
    this.getStandardAndCustomStructureFields();
    this.getAllDocuments();

  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Documents" && element.access == 'view') {
              console.log('data comes in2');

              this.documentsTabView = true;
              console.log('loggss', this.documentsTabView);

            }

          });

        },
        (err: any) => {

        })


  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllDocuments();
  }

  // Author:Saiprakash G, Date:21-05-19
  // Filter Documents
  filter(type) {
    this.filterDocumentType = type
    this.getAllDocuments();
  }

  // Author:Saiprakash G, Date:21-05-19
  // Sorting Document
  documentSort(sort) {
    this.sortDocument = sort;
    this.getAllDocuments();
  }

  // Author:Saiprakash, Date:24/05/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields() {
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res: any) => {
        console.log(res);
        this.getAllStructureFields = res.data;
        this.customDocumentType = this.getAllStructureFields['Document Type'].custom;
        this.defaultDocumentType = this.getAllStructureFields['Document Type'].default;
        this.allDocumentType = this.customDocumentType.concat(this.defaultDocumentType);

      }
    )
  }

  // Author:Saiprakash G, Date:21-05-19
  // Open document dialog
  openDocumentDialog(): void {

    if (this.getSingleDocument && this.getSingleDocument._id) {
      const dialogRef = this.dialog.open(DocumentDialogComponent, {
        width: '900px',
        height: '700px',
        backdropClass: 'structure-model',
        data: this.getSingleDocument,


      });

      dialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed', result);
        this.getAllDocuments();
        this.checkDocumentIds = [];
        this.getSingleDocument = '';

      });
    }
    else {
      const dialogRef = this.dialog.open(DocumentDialogComponent, {
        width: '900px',
        backdropClass: 'structure-model',
        data: {},

      });

      dialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed', result);
        this.getAllDocuments();
        this.checkDocumentIds = [];

      });
    }
  }
  // Author:Saiprakash G, Date:21-05-19
  // Get all documents
  getAllDocuments() {
    var postData: any = {
      companyId: this.companyId,
      userId: this.userId,
      per_page: this.perPage,
      page_no: this.pageNo,
      filter: {
        documentType: this.filterDocumentType
      },
      sort: {
        documentName: this.sortDocument
      }
    }
    this.myInfoService.listDocuments(postData).subscribe((res: any) => {
      console.log(res);
      this.allDocuments = res.data;
      // if(this.allDocuments.length){
      //   this.documentName = this.allDocuments[0].documentName;
      //   this.documentType = this.allDocuments[0].documentType
      // }

      this.count = res.count

    }, (err) => {
      console.log(err);

    })
  }
  // Author:Saiprakash G, Date:21-05-19
  // Single get document
  editDocument() {
    this.selectedDocuments = this.allDocuments.filter(documentCheck => {
      return documentCheck.isChecked
    })

    if (this.selectedDocuments.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one document to proceed", "", 'error')
    } else if (this.selectedDocuments.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one document to proceed", "", 'error')
    } else {

      this.myInfoService.getDocumentDetails(this.companyId, this.userId, this.selectedDocuments[0]._id).subscribe((res: any) => {
        console.log(res);

        this.getSingleDocument = res.data;
        this.openDocumentDialog();

      }, (err) => {
        console.log(err);

      })


    }
  }
  // Author:Saiprakash G, Date:21-05-19
  // Delete Documents
  deleteDocument() {
    this.selectedDocuments = this.allDocuments.filter(documentCheck => {
      return documentCheck.isChecked
    })
    if (this.selectedDocuments.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one document to proceed", "", 'error')
    } else {
      var data: any = {
        companyId: this.companyId,
        userId: this.userId,
        _ids: this.selectedDocuments
      }

      this.myInfoService.deleteDocuments(data).subscribe((res: any) => {
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation("Deleted Successfully", "", "success");
        this.checkDocumentIds = [];
        this.getAllDocuments();

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

      })


    }
  }

  checkDocument(event, id) {
    console.log(event, id);
    if (event.checked == true) {
      this.checkDocumentIds.push(id)
    }
    console.log(this.checkDocumentIds);
    if (event.checked == false) {
      var index = this.checkDocumentIds.indexOf(id);
      if (index > -1) {
        this.checkDocumentIds.splice(index, 1);
      }
      console.log(this.checkDocumentIds);
    }

  }
  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/assets"])

  }


}
