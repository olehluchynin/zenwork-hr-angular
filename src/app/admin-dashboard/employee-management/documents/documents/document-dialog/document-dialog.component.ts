import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MyInfoService } from '../../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';

@Component({
  selector: 'app-document-dialog',
  templateUrl: './document-dialog.component.html',
  styleUrls: ['./document-dialog.component.css']
})
export class DocumentDialogComponent implements OnInit {

  public companyId:any;
  public userId:any;
  public employeeDetails:any;
  public documentName:any;
  public documentType:any;
  public files:any;
  public fileName:any;
  public getAllStructureFields:any;
  public customDocumentType = [];
  public defaultDocumentType = [];
  public allDocumentType = [];
  public isValid:boolean = false;
  

  constructor(
    private myInfoService : MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService : SwalAlertService,
    public dialogRef: MatDialogRef<DocumentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data 
  ) { }

  ngOnInit() {
    console.log(this.data);
    if(this.data){
      this.documentName = this.data.documentName;
      this.documentType = this.data.documentType;
      this.fileName = this.data.originalFilename;

    }
   
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    this.userId = this.employeeDetails._id;
    console.log(this.userId);
    this.getStandardAndCustomStructureFields();
    
    
  }

  onNoClick(): void {
    this.dialogRef.close();

  }
// Author:Saiprakash G, Date:21-05-19
// File upload
  fileChangeEvent(e: any) {

    this.files = e.target.files[0];
    console.log(this.files)
    this.fileName = this.files.name

  }

    // Author:Saiprakash, Date:24/05/2019
  // Get Standard And Custom Structure Fields

getStandardAndCustomStructureFields(){
  this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
    (res:any)=>{
      console.log(res); 
      this.getAllStructureFields = res.data;
      this.customDocumentType = this.getAllStructureFields['Document Type'].custom;
      this.defaultDocumentType = this.getAllStructureFields['Document Type'].default;
      this.allDocumentType = this.customDocumentType.concat(this.defaultDocumentType);
   
     
    }
  )
}

// Author:Saiprakash G, Date:21-05-19
// Upload Document
  uploadDocument(){
    var data:any = {
      companyId: this.companyId,
      userId: this.userId,
      documentName: this.documentName.trim(),
      documentType: this.documentType
    }
    console.log(data);
    
    var formData: FormData = new FormData();
    formData.append('file', this.files);
    formData.append('data', JSON.stringify(data));

  //   if(this.data._id){
   
  //     formData.append('data', JSON.stringify(this.data._id));
  //     formData.append('data', JSON.stringify(this.data.originalFilename));
  //     formData.append('data', JSON.stringify(this.data.s3Path));
  //  this.myInfoService.editDocuments(formData).subscribe((res:any)=>{
  //   console.log(res);
  //   this.documentName =null;
  //   this.documentType = null;
  //   this.files = null
  //   this.dialogRef.close(res);
   
  //  this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
  
  // },(err)=>{
  //   console.log(err);
  //  this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    
  // })

  //   }
  this.isValid = true;
  if(this.fileName && this.documentName && this.documentType){
    this.myInfoService.uploadDocument(formData).subscribe((res:any)=>{
      console.log(res);
      this.dialogRef.close(res);
     this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
     this.documentName ='';
     this.documentType = '';
     this.files = ''
      
    },(err)=>{
      console.log(err);
     this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      
    })
  }
 

  }
// Author:Saiprakash G, Date:21-05-19
// Update Document
  updateDocument(){
    console.log(this.data);
    
      var data:any = {
        documentId: this.data._id,
        companyId: this.companyId,
        userId: this.userId,
        originalFilename:this.data.originalFilename,
        documentName: this.documentName.trim(),
        documentType: this.documentType,
        s3Path: this.data.s3Path
      }
   
    console.log(data, this.fileName, this.documentName);
    
    var formData: FormData = new FormData();
    formData.append('file', this.files);
    formData.append('data', JSON.stringify(data));
    this.isValid = true;
    if(this.fileName && this.documentName && this.documentType){
      this.myInfoService.editDocuments(formData).subscribe((res:any)=>{
        console.log(res);
       
        this.documentName =null;
        this.documentType = null;
        this.files = null;
        this.data._id = "";
        
        console.log(this.data.checkDocumentIds);
        
        this.dialogRef.close(res);
       
       this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
      
      },(err)=>{
        console.log(err);
       this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        
      })
    }



  }
  cancel(){    
    this.onNoClick();
   
  }

}
