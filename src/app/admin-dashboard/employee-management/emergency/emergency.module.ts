import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmergencyComponent } from './emergency/emergency.component';
import { EmergencyRouting } from './emergency.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { MyInfoService } from '../../../services/my-info.service';


@NgModule({
  imports: [
    CommonModule,
    EmergencyRouting,
    FormsModule,
    MatSelectModule,MatMenuModule,MatButtonModule,MatIconModule,MaterialModuleModule,ReactiveFormsModule
  ],
  declarations: [EmergencyComponent],
  providers: [MyInfoService]
})
export class EmergencyModule { }
