import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmployeeService } from '../../../../services/employee.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyInfoService } from '../../../../services/my-info.service';
import { Router } from '@angular/router';

declare var $: any;


@Component({
  selector: 'app-emergency',
  templateUrl: './emergency.component.html',
  styleUrls: ['./emergency.component.css']
})
export class EmergencyComponent implements OnInit {


  public employeeForm: FormGroup

  public customEmergencyContact = [];
  public getAllStructureFields: any;
  public defaultState = [];
  public customState = [];
  public allState = [];
  public defaultCountry = [];
  public customCountry = [];
  public allCountry = [];
  public customRelation = [];
  public defaultRelation = [];
  public allRelation = [];
  public tabName: string = "Emergency_Contact";
  public contactIds = [];
  public fieldsShow: any = {
    City: {
      hide: '',
      required: '',
      format: ''
    },
    Ext: {
      hide: '',
      required: '',
      format: ''
    },
    Home_Email: {
      hide: '',
      required: '',
      format: ''
    },
    Home_Phone: {
      hide: '',
      required: '',
      format: ''
    },
    Mobile_Phone: {
      hide: '',
      required: '',
      format: ''
    },
    Name: {
      hide: '',
      required: '',
      format: ''
    },
    State: {
      hide: '',
      required: '',
      format: ''
    },
    Relationship: {
      hide: '',
      required: '',
      format: ''
    },
    Work_Email: {
      hide: '',
      required: '',
      format: ''
    },
    Street_1: {
      hide: '',
      required: '',
      format: ''
    },
    Street_2: {
      hide: '',
      required: '',
      format: ''
    },
    Work_Phone: {
      hide: '',
      required: '',
      format: ''
    },
    Zip: {
      hide: '',
      required: '',
      format: ''
    },
  }

  public contact = {
    Name: "",
    Relationship: '',
    contact: {
      Work_Phone: '',
      Ext: '',
      Mobile_Phone: '',
      Home_Phone: ''
    },
    email: {
      Work_Email: '',
      Home_Email: ''
    },
    address: {
      Street_1: '',
      Street_2: '',
      City: '',
      State: '',
      Zip: '',
    },
    isPrimaryContact: false
  };
  public emergencyContacts = [];
  public employeeData: any;
  public pageNo: any;
  public perPage: any;
  public count: any;
  public companyId: any;
  public userId: any;
  public employeeDetails: any;
  public selectedEmergencyContact = [];
  public getSingleEmergencyContact: any;
  public emergencyContactId: any;
  public deleteSelectedContacts = [];
  roleType: any;
  @ViewChild('openEdit') openEdit: ElementRef
  
  roletype: any;
  pagesAccess = []
  emergencyTabView: boolean = false;

  constructor(
    private employeeService: EmployeeService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private myInfoService: MyInfoService,
    private router: Router
  ) { }

  ngOnInit() {

    this.roletype = JSON.parse(localStorage.getItem('type'))
    if (this.roletype != 'company') {
      this.pageAccesslevels();
    }

    this.getSingleTabSettingForEmergencyContact();

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    if (this.employeeDetails) {
      this.userId = this.employeeDetails._id;
    }
    console.log(this.userId);

    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.userId = JSON.parse(localStorage.getItem('employeeId'));
    }
    
    this.pageNo = 1;
    this.perPage = 10;

    this.getAllEmergencyContacts();
    this.getStandardAndCustomStructureFields();

    this.employeeForm = new FormGroup({
      fullName: new FormControl(""),
      relationship: new FormControl(""),
      primaryContact: new FormControl(false),

      workPhone: new FormControl(""),
      extention: new FormControl(""),
      mobilePhone: new FormControl(""),
      homePhone: new FormControl(""),
      workMail: new FormControl("", [Validators.email]),
      personalMail: new FormControl("", [Validators.email]),

      street1: new FormControl(""),
      street2: new FormControl(""),
      city: new FormControl(""),
      state: new FormControl(""),
      zipcode: new FormControl(""),
      country: new FormControl("", Validators.required)

    });





    // this.emergencyContact = this.employeeData.emergencyContact;


  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Emergency Contact" && element.access == 'view') {
              console.log('data comes in2');

              this.emergencyTabView = true;
              console.log('loggss', this.emergencyTabView);

            }

          });

        },
        (err: any) => {

        })


  }


  // Author:Saiprakash, Date:15/05/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields() {
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res: any) => {
        console.log(res);
        this.getAllStructureFields = res.data;

        this.customCountry = this.getAllStructureFields['Country'].custom;
        this.defaultCountry = this.getAllStructureFields['Country'].default;
        this.allCountry = this.customCountry.concat(this.defaultCountry);
        this.customState = this.getAllStructureFields['State'].custom;
        this.defaultState = this.getAllStructureFields['State'].default;
        this.allState = this.customState.concat(this.defaultState);
        this.customRelation = this.getAllStructureFields['Emergency Contact Relationship'].custom;
        this.defaultRelation = this.getAllStructureFields['Emergency Contact Relationship'].default;
        this.allRelation = this.customRelation.concat(this.defaultRelation)
      },
      (err) => {
        console.log(err);

      }
    )
  }

  // Author:Saiprakash, Date:12/04/2019
  // Get single tab settings for emergency contact

  getSingleTabSettingForEmergencyContact() {

    this.myInfoService.getSingleTabSettingForMyinfo(this.tabName).subscribe((res: any) => {
      console.log(res);
      this.fieldsShow = res.data.fields;
      console.log(this.fieldsShow);


    }, err => {
      console.log(err);

    })
  }

  checkPrimaryContact(event) {
    console.log(event);

  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllEmergencyContacts();
  }

  // Author:Saiprakash G, Date:10-05-19
  // Get all employee emergency contact details

  getAllEmergencyContacts() {
    var postData: any = {
      companyId: this.companyId,
      userId: this.userId,
      per_page: this.perPage,
      page_no: this.pageNo
    }
    console.log(this.companyId);

    this.myInfoService.getAllEmergencyContacts(postData).subscribe((res: any) => {
      console.log(res);
      this.emergencyContacts = res.data;
      this.count = res.total_count
    }, (err) => {
      console.log(err);

    })
  }

  // Author:Saiprakash G, Date:10-05-19
  // Add and edit employee emergency contact details


  employeeEmergencyContactSubmit() {
    console.log("Employee Details", this.employeeForm.value)

    var data: any = {
      companyId: this.companyId,
      userId: this.userId,
      fullName: this.employeeForm.get('fullName').value,
      relationship: this.employeeForm.get('relationship').value,
      primaryContact: this.employeeForm.get('primaryContact').value,
      contact: {
        workPhone: this.employeeForm.get('workPhone').value,
        extention: this.employeeForm.get('extention').value,
        mobilePhone: this.employeeForm.get('mobilePhone').value,
        homePhone: this.employeeForm.get('homePhone').value,
        workMail: this.employeeForm.get('workMail').value,
        personalMail: this.employeeForm.get('personalMail').value,
      },
      address: {
        street1: this.employeeForm.get('street1').value,
        street2: this.employeeForm.get('street2').value,
        city: this.employeeForm.get('city').value,
        state: this.employeeForm.get('state').value,
        zipcode: this.employeeForm.get('zipcode').value,
        country: this.employeeForm.get('country').value,
      }
    }
    if (this.employeeForm.valid) {
      if (this.getSingleEmergencyContact && this.emergencyContactId) {
        this.myInfoService.editEmergencyContact(this.emergencyContactId, data).subscribe((res: any) => {
          console.log(res);
          this.employeeForm.reset();
          this.emergencyContactId = "";
          this.contactIds = [];
          $("#myModal").modal("hide");
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success")
          this.getAllEmergencyContacts();
        }, (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        })

      }
      else {

        this.myInfoService.addEmergenctContact(data).subscribe((res: any) => {
          console.log(res);
          this.employeeForm.reset();
          $("#myModal").modal("hide");
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success")
          this.contactIds = [];
          this.getAllEmergencyContacts();
        }, (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        })

      }
    }
    else {
      this.employeeForm.get('fullName').markAsTouched();
      this.employeeForm.get('relationship').markAsTouched();
      this.employeeForm.get('workPhone').markAsTouched();
      this.employeeForm.get('extention').markAsTouched();
      this.employeeForm.get('mobilePhone').markAsTouched();
      this.employeeForm.get('homePhone').markAsTouched();
      this.employeeForm.get('workMail').markAsTouched();
      this.employeeForm.get('personalMail').markAsTouched();
      this.employeeForm.get('street1').markAsTouched();
      this.employeeForm.get('city').markAsTouched();
      this.employeeForm.get('state').markAsTouched();
      this.employeeForm.get('zipcode').markAsTouched();
      this.employeeForm.get('country').markAsTouched();

    }


  }
  // Author:Saiprakash G, Date:10-05-19
  // Get single employee emergency contact details


  edit() {
    this.selectedEmergencyContact = this.emergencyContacts.filter(contactCheck => {
      return contactCheck.isChecked
    })

    if (this.selectedEmergencyContact.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one contact to proceed", "", 'error')
    } else if (this.selectedEmergencyContact.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one contact to proceed", "", 'error')
    } else {
      this.openEdit.nativeElement.click();

      this.myInfoService.getEmergencyContact(this.companyId, this.userId, this.selectedEmergencyContact[0]._id)
        .subscribe((res: any) => {
          console.log(res)
          this.getSingleEmergencyContact = res.data;
          this.emergencyContactId = res.data._id;
          this.employeeForm.patchValue(this.getSingleEmergencyContact);
          this.employeeForm.get('city').patchValue(this.getSingleEmergencyContact.address.city)
          this.employeeForm.get('country').patchValue(this.getSingleEmergencyContact.address.country)
          this.employeeForm.get('state').patchValue(this.getSingleEmergencyContact.address.state)
          this.employeeForm.get('street1').patchValue(this.getSingleEmergencyContact.address.street1)
          this.employeeForm.get('street2').patchValue(this.getSingleEmergencyContact.address.street2)
          this.employeeForm.get('zipcode').patchValue(this.getSingleEmergencyContact.address.zipcode)
          this.employeeForm.get('extention').patchValue(this.getSingleEmergencyContact.contact.extention)
          this.employeeForm.get('homePhone').patchValue(this.getSingleEmergencyContact.contact.homePhone)
          this.employeeForm.get('mobilePhone').patchValue(this.getSingleEmergencyContact.contact.mobilePhone)
          this.employeeForm.get('personalMail').patchValue(this.getSingleEmergencyContact.contact.personalMail)
          this.employeeForm.get('workMail').patchValue(this.getSingleEmergencyContact.contact.workMail)
          this.employeeForm.get('workPhone').patchValue(this.getSingleEmergencyContact.contact.workPhone)

        });
    }

  }

  // Author:Saiprakash G, Date:10-05-19
  // Delete employee emergency contacts


  delete() {

    this.selectedEmergencyContact = this.emergencyContacts.filter(contactCheck => {
      return contactCheck.isChecked


    })

    if (this.selectedEmergencyContact.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one contact to proceed", "", 'error')
    }

    else {
      // this.deleteSelectedContacts.push(this.selectedEmergencyContact.Id)
      var deleteData: any = {
        companyId: this.companyId,
        userId: this.userId,
        _ids: this.selectedEmergencyContact

      }
      console.log(deleteData);

      this.myInfoService.deleteEmergencyContact(deleteData).subscribe((res: any) => {
        console.log(res);
        this.contactIds = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success")
        this.getAllEmergencyContacts();

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })

    }

  }

  cancelEmergencyContact() {
    this.employeeForm.reset();
    this.contactIds = [];
    $("#myModal").modal("hide");
    this.getAllEmergencyContacts();
  }

  /* Description:accept only alphabets
  author : vipin reddy */
  alphabets(event) {
    return (event.charCode > 64 &&
      event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)
  }
  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/compensation"])

  }

  checkContact(event, id) {
    console.log(event, id);
    if (event == true) {
      this.contactIds.push(id)
    }
    console.log(this.contactIds);
    if (event == false) {
      var index = this.contactIds.indexOf(id);
      if (index > -1) {
        this.contactIds.splice(index, 1);
      }
      console.log(this.contactIds);
    }

  }


}
