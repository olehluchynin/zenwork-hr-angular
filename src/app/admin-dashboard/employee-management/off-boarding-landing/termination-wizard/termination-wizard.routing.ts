import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TerminationWizardComponent } from './termination-wizard/termination-wizard.component';

const routes: Routes = [
    { path:'',redirectTo:"/termination-wizard",pathMatch:"full" },
    {path:'termination-wizard',component:TerminationWizardComponent} 
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class TerminationWizardRouting { }