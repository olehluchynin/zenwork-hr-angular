import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-termination-wizard',
  templateUrl: './termination-wizard.component.html',
  styleUrls: ['./termination-wizard.component.css']
})
export class TerminationWizardComponent implements OnInit {

  minDate = new Date(100, 5, 10);
  maxDate = new Date(4000, 9, 15);
  
  constructor() { }

  
  myForm = new FormGroup({
    myDateYMD: new FormControl(new Date()),
    myDateFull: new FormControl(new Date()),
    myDateMDY: new FormControl(new Date())
  });

  ngOnInit() {
  }

}
