import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminationWizardComponent } from './termination-wizard.component';

describe('TerminationWizardComponent', () => {
  let component: TerminationWizardComponent;
  let fixture: ComponentFixture<TerminationWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminationWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminationWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
