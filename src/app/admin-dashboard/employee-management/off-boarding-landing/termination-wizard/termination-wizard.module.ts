import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TerminationWizardComponent } from './termination-wizard/termination-wizard.component';
import { TerminationWizardRouting } from './termination-wizard.routing';

@NgModule({
  imports: [
    CommonModule,
    BsDatepickerModule.forRoot(),
    TerminationWizardRouting
  ],
  declarations: [TerminationWizardComponent]
})

export class TerminationWizardModule { }
