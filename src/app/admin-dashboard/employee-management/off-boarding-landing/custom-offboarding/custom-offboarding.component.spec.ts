import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomOffboardingComponent } from './custom-offboarding.component';

describe('CustomOffboardingComponent', () => {
  let component: CustomOffboardingComponent;
  let fixture: ComponentFixture<CustomOffboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomOffboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomOffboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
