import { Component, OnInit } from '@angular/core';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { OffBoardingTemplateService } from '../../../../services/off-boarding-template.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { ActivatedRoute } from '@angular/router';


declare var jQuery: any;

@Component({
  selector: 'app-custom-offboarding',
  templateUrl: './custom-offboarding.component.html',
  styleUrls: ['./custom-offboarding.component.css']
})
export class CustomOffboardingComponent implements OnInit {


  customOffboardingtemplateName: any;
  taskCOmpletionTime = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  customTask: any = {
    taskName: '',
    taskAssignee: '',
    assigneeEmail: '',
    taskDesription: '',
    timeToComplete: {
      time: '',
      unit: ''
    },
    category: '',
    task_assigned_date: new Date().toISOString(),
    Date: new Date().toISOString()
  }
  pushData = {
    taskName: '',
    taskAssignee: '',
    assigneeEmail: '',
    taskDesription: '',
    timeToComplete: {
      time: '',
      unit: ''
    },
    category: '',
    task_assigned_date: new Date().toISOString(),
    Date: new Date().toISOString()
  }

  pushData1 = {
    taskName: '',
    taskAssignee: '',
    assigneeEmail: '',
    taskDesription: '',
    timeToComplete: {
      time: '',
      unit: ''
    },
    category: '',
    task_assigned_date: new Date().toISOString(),
    Date: new Date().toISOString()
  }

  customTaskEdit: any = {
    taskName: '',
    taskAssignee: '',
    assigneeEmail: '',
    taskDesription: '',
    timeToComplete: {
      time: '',
      unit: ''
    },
    category: '',
    task_assigned_date: new Date().toISOString(),
    Date: new Date().toISOString()

  }
  employeeData: any;

  customTemplateSend = {
    companyId: '',
    templateType: '',
    templateName: '',
    offBoardingTasks: {

    },
    createdBy: '',
    updatedBy: ''
  }

  id: any;

  customTemplateSend1 = {
    companyId: '',
    templateType: '',
    templateName: '',
    offBoardingTasks: {

    },
    createdBy: '',
    updatedBy: ''
  }

  objName = [];
  customtemp: boolean = false;
  categoryInput: boolean = false;
  categoryAddInput: boolean = false;
  isValid: boolean = false;
  isValidMainSubmit: boolean = false;
  tempCategory: any;
  tempTaskName: any;
  addbtn:any;
  isDisabled: boolean = false;
  constructor(private siteAccessRolesService: SiteAccessRolesService,
    private offBoardingTemplateService: OffBoardingTemplateService,
    private swalAlertService: SwalAlertService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.employeeSearch()
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);

    })
    if (this.id) {
      this.customtemp = true;
      this.getCustomOffboardingtemplate(this.id);
    }

  }
  categoryChange() {
    console.log(this.customTaskEdit.category, this.tempTaskName, '122');
    if (this.tempCategory != this.customTaskEdit.category) {
      this.customTemplateSend['offBoardingTasks'][this.tempCategory].forEach(element => {
        console.log(element, 'taskssss');
        if (element.taskName == this.tempTaskName) {

          var index = this.customTemplateSend['offBoardingTasks'][this.tempCategory].indexOf(this.customTaskEdit)
          if (index > -1) {
            this.customTemplateSend['offBoardingTasks'][this.tempCategory].splice(index, 1)
            delete this.customTaskEdit['isChecked']
            // this.customTemplateSend['offBoardingTasks'][this.customTaskEdit.category].push(this.customTaskEdit)

          }


        }

      });
      console.log(this.customTemplateSend['offBoardingTasks'], "vipin");

    }
  }

  getCustomOffboardingtemplate(id) {
    this.offBoardingTemplateService.getcustomOffboardingTemplatedata(id)
      .subscribe((res: any) => {
        console.log("get custom template data", res);

        this.customTemplateSend = res.data[0];
        this.customOffboardingtemplateName = res.data[0].templateName;
        console.log(this.customTemplateSend);

      },
        (err) => {
          console.log(err);

        })
  }


  singleTaskChoose(taskArray, taskName, isChecked) {
    console.log(taskArray, taskName, isChecked);

    this.tempCategory = taskArray;
    this.tempTaskName = taskName;
    if (isChecked == false) {
      this.customTaskEdit = {}
      this.customTaskEdit.timeToComplete = {}
      this.isDisabled = false;


    }
    if (isChecked == true) {
      for (var task in this.customTemplateSend["offBoardingTasks"]) {
        console.log(task);
        this.customTemplateSend["offBoardingTasks"][task].forEach(element => {
          console.log(element);

          if (element.taskName === taskName) {
            element.isChecked = true;
            this.customTaskEdit = element;
            // this.addbtn = element;
            console.log("popdaata", this.customTaskEdit);
            this.addbtn = element;
            this.isDisabled = true;
          } else {
            element.isChecked = false;
          }
        });
      }
    }


  }


  deleteTask() {

    this.customTemplateSend["offBoardingTasks"][this.tempCategory].forEach(element => {
      console.log(element);
      if (element.taskName === this.tempTaskName) {
        element.isChecked = true;
        console.log(element, '1234567');
        // delete this.customTaskEdi
        delete this.customTemplateSend.offBoardingTasks[this.tempCategory].element
        var index = this.customTemplateSend.offBoardingTasks[this.tempCategory].indexOf(element)
        console.log("index", index);
        if (index > -1) {
          this.customTemplateSend['offBoardingTasks'][this.tempCategory].splice(index, 1)

        }

        console.log("popdaata", this.customTemplateSend.offBoardingTasks);

      } else {
        element.isChecked = false;
      }
      this.customTemplateUpdate()

    })

  }

  getKeys(data) {
    // console.log(data);
    return Object.keys(data);

  }

  /* Description: getting employees list
  author : vipin reddy */
  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;

      },
        (err) => {
          console.log(err);

        })
  }
  empEmail(event) {
    console.log(event);
    this.employeeData.forEach(element => {
      if (element._id == event) {
        this.customTask.assigneeEmail = element.email;
      }
      console.log(this.customTask.assigneeEmail);

    });

  }
  empEmailForEdit(event) {
    console.log(event);
    this.employeeData.forEach(element => {
      if (element._id == event) {
        this.customTaskEdit.assigneeEmail = element.email;
      }
      console.log(this.customTaskEdit.assigneeEmail);

    });

  }
  customTaskUpdate() {
    delete this.customTaskEdit['isChecked']
    this.pushData1 = this.customTaskEdit;
    this.customTaskEdit = {}
    this.customTaskEdit.timeToComplete = {}
    console.log(this.tempCategory, this.pushData1.category, this.customTemplateSend['offBoardingTasks'])
    if (this.pushData1.category != this.tempCategory) {
      console.log("not equal");

      this.customTemplateSend['offBoardingTasks'][this.pushData1.category].push(this.pushData1);
    }
    if (this.pushData1.category == this.tempCategory) {
      console.log("equal");

      this.customTemplateSend['offBoardingTasks'][this.pushData1.category] = [this.pushData1];
    }
    console.log(this.customTemplateSend, this.pushData1, "12121");
    jQuery("#myModal1").modal("hide");
    console.log(this.customTemplateSend);
    this.offBoardingTemplateService.updateCustomTemplate(this.customTemplateSend)
      .subscribe((res: any) => {
        console.log("custom template create", res);
        if (res.status == true) {
          this.isDisabled = false;
          this.swalAlertService.SweetAlertWithoutConfirmation('Custom template', res.message, 'success')
        }
      },
        (err) => {
          console.log(err);


        })

  }

  offboardingTask() {
    this.isValid = true;
    jQuery("#myModal").modal("hide");
    console.log(this.customTask)
    this.pushData = this.customTask
    this.customTask = {}
    this.customTask.timeToComplete = {}

    if (this.customTemplateSend['offBoardingTasks'][this.pushData.category]) {
      this.customTemplateSend['offBoardingTasks'][this.pushData.category].push(this.pushData)
    }
    else {
      this.customTemplateSend['offBoardingTasks'][this.pushData.category] = [this.pushData];
    }
    console.log(this.customTemplateSend, this.pushData, "12121");
    this.isValid = false;
    this.categoryAddInput = false;

  }
  // tempform() {
  //   this.customTask.category = '';
  //   this.customTask.taskName = '';
  //   this.customTask.assigneeEmail = '';
  //   this.customTask.taskAssignee = '';
  //   this.customTask.timeToComplete.time = '';
  //   this.customTask.timeToComplete.unit = '';
  //   this.customTask.taskDesription = '';
  //   this.customTask.Date = '';
  //   this.customTask.task_assigned_date = '';
  // }

  customTemplateCreate() {
    this.isValidMainSubmit = true;
    if (Object.keys(this.customTemplateSend['offBoardingTasks']).length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Offboarding template", "Add atleast one category", "error")
    }

    if (this.customOffboardingtemplateName && Object.keys(this.customTemplateSend['offBoardingTasks']).length > 0) {
      var cmpnyId = JSON.parse(localStorage.getItem('companyId'))
      this.customTemplateSend.companyId = cmpnyId;
      this.customTemplateSend.createdBy = cmpnyId;
      this.customTemplateSend.updatedBy = cmpnyId;
      this.customTemplateSend.templateType = 'custom'
      this.customTemplateSend.templateName = this.customOffboardingtemplateName;
      console.log(this.customTemplateSend, 'vipin121212');

      this.offBoardingTemplateService.createcustomTemplate(this.customTemplateSend)
        .subscribe((res: any) => {
          console.log("custom template create", res);
          if (res.status == true) {

            this.swalAlertService.SweetAlertWithoutConfirmation('customtemplate', res.message, "success")
          }
        },
          (err) => {
            console.log(err);

          })
    }
  }

  customTemplateUpdate() {
    console.log(this.customTemplateSend);
    this.offBoardingTemplateService.updateCustomTemplate(this.customTemplateSend)
      .subscribe((res: any) => {
        console.log("custom template create", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('Custom template', res.message, 'success')
        }
      },
        (err) => {
          console.log(err);
        })
  }



  categoryAdd() {
    this.categoryInput = !this.categoryInput;
  }
  categoryInputAdd() {
    this.categoryAddInput = !this.categoryAddInput;
  }
}
