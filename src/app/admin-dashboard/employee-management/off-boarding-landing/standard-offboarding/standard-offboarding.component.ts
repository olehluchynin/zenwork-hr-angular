import { Component, OnInit } from '@angular/core';
import { OffBoardingTemplateService } from '../../../../services/off-boarding-template.service';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';

declare var jQuery: any;

@Component({
    selector: 'app-standard-offboarding',
    templateUrl: './standard-offboarding.component.html',
    styleUrls: ['./standard-offboarding.component.css']
})
export class StandardOffboardingComponent implements OnInit {

    constructor(private offBoardingTemplateService: OffBoardingTemplateService,
        private siteAccessRolesService: SiteAccessRolesService,
        private swalAlertService: SwalAlertService) { }

    standardOffboardTemplate = {
        templateName: '',
        offBoardingTasks: {
            HrTasks: [],
            ITSetup: [],
            ManagerTasks: []

        }
    }
    popupData = {
        taskName: '',
        taskAssignee: '',
        assigneeEmail: '',
        taskDesription: '',
        timeToComplete: {
            time: '',
            unit: ''
        },
        category: '',
    };
    addShowHide: boolean = false;
    taskCOmpletionTime = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    employeeData: any;


    ngOnInit() {
        this.getStandardTemplate()
        this.employeeSearch()
    }


    /* Description: getting employees list
      author : vipin reddy */
    employeeSearch() {
        var data = {
            name: ''
        }
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe((res: any) => {
                console.log("employees", res);
                this.employeeData = res.data;

            },
                (err) => {
                    console.log(err);

                })
    }


    getStandardTemplate() {
        this.offBoardingTemplateService.getStandardOffboardingTemplate()
            .subscribe((res: any) => {
                console.log("offboarding standard template", res);
                this.standardOffboardTemplate = res.data;
                this.standardOffboardTemplate.offBoardingTasks.HrTasks = this.standardOffboardTemplate.offBoardingTasks.HrTasks.map(function(el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                })
                console.log(this.standardOffboardTemplate.offBoardingTasks.HrTasks);
            },
                (err) => {

                })
    }
    empEmail(event) {
        console.log(event);
        this.employeeData.forEach(element => {
          if (element._id == event) {
            this.popupData.assigneeEmail = element.email;
          }
        });
    
      }

    getKeys(data) {
        // console.log(data);
        return Object.keys(data);

    }

    singleTaskChoose(taskArray, taskName, isChecked?) {
        console.log(taskArray, taskName, isChecked);
        if (isChecked == false) {
            this.popupData = {
                taskName: '',
                taskAssignee: '',
                assigneeEmail: '',
                taskDesription: '',
                timeToComplete: {
                    time: '',
                    unit: ''
                },
                category: '',
            }
            this.addShowHide = false;

        }
        if (isChecked == true) {
            for (var task in this.standardOffboardTemplate["offBoardingTasks"]) {
                console.log(task);
                this.standardOffboardTemplate["offBoardingTasks"][task].forEach(element => {
                    if (element.taskName === taskName) {
                        element.isChecked = true;
                        this.popupData = element;
                        this.addShowHide = true;
                        console.log("popdaata", this.popupData);

                    } else {
                        element.isChecked = false;
                    }
                });
            }
        }


    }

    editStandardOffboarding() {
        console.log(this.popupData)
        delete this.popupData['isChecked']
        jQuery("#myModal").modal("hide");
        if (this.standardOffboardTemplate['offBoardingTasks'][this.popupData.category]) {
            this.standardOffboardTemplate['offBoardingTasks'][this.popupData.category] = [this.popupData];
        }
        //  else {
        //   this.standardOffboardTemplate['offBoardingTasks'][this.popupData.category] = [this.popupData];
        // }
        console.log(this.standardOffboardTemplate, "12121");


    }


    updateStandardTemplate() {
        console.log(this.standardOffboardTemplate);
        this.offBoardingTemplateService.updateStandardTemplate(this.standardOffboardTemplate)
            .subscribe((res: any) => {
                console.log("offboarding standard template update", res);
                if (res.status == true) {
                    this.swalAlertService.SweetAlertWithoutConfirmation("standard", res.message, "success")
                }
            },
                (err) => {
                    console.log(err);
                    this.swalAlertService.SweetAlertWithoutConfirmation("standard", err.error.message, "error")
                })


    }




}
