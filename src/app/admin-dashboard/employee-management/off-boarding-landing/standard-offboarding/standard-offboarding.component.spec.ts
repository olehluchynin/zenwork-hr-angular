import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardOffboardingComponent } from './standard-offboarding.component';

describe('StandardOffboardingComponent', () => {
  let component: StandardOffboardingComponent;
  let fixture: ComponentFixture<StandardOffboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardOffboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardOffboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
