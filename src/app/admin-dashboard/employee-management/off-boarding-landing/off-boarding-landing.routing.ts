import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OffBoardingLandingComponent } from './off-boarding-landing/off-boarding-landing.component';
import { TerminationWizardModule } from './termination-wizard/termination-wizard.module';
import { StandardOffboardingComponent } from './standard-offboarding/standard-offboarding.component';
import { CustomOffboardingComponent } from './custom-offboarding/custom-offboarding.component';


const routes: Routes = [
    { path: '', redirectTo: "/off-boarding-landing", pathMatch: "full" },
    { path: 'off-boarding-landing', component: OffBoardingLandingComponent },
    { path: 'termination-wizard', loadChildren: "./termination-wizard/termination-wizard.module#TerminationWizardModule" },
    { path: 'standard-offboard', component: StandardOffboardingComponent },
    { path: 'custom-offboard', component: CustomOffboardingComponent },
    { path: 'custom-offboard/:id', component: CustomOffboardingComponent }

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class OffBoardingLandingRouting { }