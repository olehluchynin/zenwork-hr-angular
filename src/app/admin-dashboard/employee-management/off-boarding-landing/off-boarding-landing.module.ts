import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffBoardingLandingComponent } from './off-boarding-landing/off-boarding-landing.component';
import { OffBoardingLandingRouting } from './off-boarding-landing.routing';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule,MatTooltipModule,MatToolbarModule} from '@angular/material';

import {SelectModule} from 'ng2-select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {StandardOffboardingComponent} from './standard-offboarding/standard-offboarding.component'
import { OffBoardingService } from '../../../services/off-boarding.service';
import{CustomOffboardingComponent} from './custom-offboarding/custom-offboarding.component'




@NgModule({
  imports: [
    CommonModule,
    OffBoardingLandingRouting,
    MatSelectModule,
    MatSlideToggleModule,
    BsDatepickerModule.forRoot(),
    MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,FormsModule,ReactiveFormsModule
    ,MatFormFieldModule,MatTooltipModule,MatToolbarModule,SelectModule,MatCheckboxModule
  ],
  declarations: [OffBoardingLandingComponent,StandardOffboardingComponent,CustomOffboardingComponent],
  providers:[OffBoardingService]
})
export class OffBoardingLandingModule { }
