import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffBoardingLandingComponent } from './off-boarding-landing.component';

describe('OffBoardingLandingComponent', () => {
  let component: OffBoardingLandingComponent;
  let fixture: ComponentFixture<OffBoardingLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffBoardingLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffBoardingLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
