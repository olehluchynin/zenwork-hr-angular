import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OffBoardingService } from '../../../../services/off-boarding.service';
import { OffBoardingTemplateService } from '../../../../services/off-boarding-template.service';
import { Router } from '@angular/router';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { OnboardingService } from '../../../../services/onboarding.service';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { WorkflowService } from '../../../../services/workflow.service';

declare var jQuery: any;


@Component({
  selector: 'app-off-boarding-landing',
  templateUrl: './off-boarding-landing.component.html',
  styleUrls: ['./off-boarding-landing.component.css']
})
export class OffBoardingLandingComponent implements OnInit {

  @ViewChild('client_Info') client_Info: ElementRef;
  @ViewChild('offboardTask') offboardTask: ElementRef;
  @ViewChild('directReports') directReports: ElementRef;
  @ViewChild('companyProperty') companyProperty: ElementRef;

  constructor(
    private offBoarding: OffBoardingService,
    private router: Router,
    private offBoardingTemplateService: OffBoardingTemplateService,
    private swalAlertService: SwalAlertService,
    private onboardingService: OnboardingService,
    private siteAccessRolesService: SiteAccessRolesService,
    private myInfoService: MyInfoService,
    private workflowService: WorkflowService,

  ) { }
  public items = []

  // public items: Array<string> = ['Amsterdam', 'Antwerp', 'Athens', 'Barcelona',
  //   'Berlin', 'Birmingham', 'Bradford', 'Bremen', 'Brussels', 'Bucharest',
  //   'Budapest', 'Cologne', 'Copenhagen', 'Dortmund', 'Dresden', 'Dublin',
  //   'Düsseldorf', 'Essen', 'Frankfurt', 'Genoa', 'Glasgow', 'Gothenburg',
  //   'Hamburg', 'Hannover', 'Helsinki', 'Kraków', 'Leeds', 'Leipzig', 'Lisbon',
  //   'London', 'Madrid', 'Manchester', 'Marseille', 'Milan', 'Munich', 'Málaga',
  //   'Naples', 'Palermo', 'Paris', 'Poznań', 'Prague', 'Riga', 'Rome',
  //   'Rotterdam', 'Seville', 'Sheffield', 'Sofia', 'Stockholm', 'Stuttgart',
  //   'The Hague', 'Turin', 'Valencia', 'Vienna', 'Vilnius', 'Warsaw', 'Wrocław',
  //   'Zagreb', 'Zaragoza', 'Łódź'];

  employeeData: any;

  public value: any = {};
  public _disabledV: string = '0';
  public disable: boolean = false;

  allStructureFieldsData: any;
  allEmployeeStatusArray = [];
  allTerminationTypeArray = [];
  allTerminationReasonArray = [];
  fileName: any;
  filesArray = []
  tempStandardTemplate = {};
  tempCustomTemplate: any;
  tempTemplateTotal = []
  perPage: any;
  pageNo: any;
  perPageAssets: any;
  pageNoAssets: any;
  directReportsArray = [];
  manageremployees = [];
  OffboardingTemplateName: any;
  directReportmanagerId: any;
  directReportsSend = [];
  directTaskSend: any = []
  file = [];
  files = [];
  terminateEmpData: any;
  directTasksArray = [];

  // private get disabledV(): string {
  //   return this._disabledV;
  // }

  // private set disabledV(value: string) {
  //   this._disabledV = value;
  //   this.disable = this._disabledV === '1';
  // }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }
  terminationDate: any;
  lastWorkingDate
  selectedNav: any;
  ClientInfo: boolean = false
  tab2: boolean = false;
  tab3: boolean = false;
  tab4: boolean = false;
  tab7: boolean = false;
  tasks: boolean = false;

  standardOffboardTemplate = {
    templateName: '',
    offBoardingTasks: {
      HrTasks: [],
      ITSetup: [],
      ManagerTasks: []

    }
  }

  customTemplateSend = {
    companyId: '',
    templateType: '',
    templateName: '',
    offBoardingTasks: {

    },
    createdBy: '',
    updatedBy: ''
  }
  customOffboardTemplate: any;
  customTempId: any;
  employees = []
  tempTotalTemplates = [];
  assetsData = []
  terminationForm: FormGroup;
  empWorkFlow: boolean = false;

  ngOnInit() {
    console.log("firsttt");

    this.perPage = 10;
    this.pageNo = 1;
    this.perPageAssets = 10;
    this.pageNoAssets = 1;
    this.employeeSearch()

    this.selectedNav = "ClientInfo";
    this.terminationForm = new FormGroup({
      Employee: new FormControl("", Validators.required),
      Status: new FormControl("", Validators.required),
      Type: new FormControl("", Validators.required),
      Reason: new FormControl("", Validators.required),
      Eligible: new FormControl("", Validators.required),
      Date1: new FormControl("", Validators.required),
      Date2: new FormControl("", Validators.required),
      Procees: new FormControl(""),
      Comments: new FormControl("", Validators.required),
      Template: new FormControl("", Validators.required)
    });

    this.getStandardTemplate()
    this.getCustomTemplate()
    var cId = JSON.parse(localStorage.getItem('companyId'))
    this.getStandardAndCustomFieldsforStructure(cId);
    this.getMangersUsersOnly(cId)

  }
  getMangersUsersOnly(cId) {
    var managerId = '5cbe98a3561562212689f747'
    this.offBoardingTemplateService.getManagersEmployeesOnly(cId, managerId)
      .subscribe((res: any) => {
        console.log("manager employees", res);
        this.manageremployees = res.data;

      },
        (err) => {
          console.log(err);

        })

  }


  /* Description: getting employees list
  author : vipin reddy */
  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;


      },
        (err) => {
          console.log(err);

        })
  }




  getStandardAndCustomFieldsforStructure(cID) {
    this.onboardingService.getStructureFields(cID)
      .subscribe((res: any) => {
        console.log("structure fields", res);
        this.allStructureFieldsData = res.data;
        //employement-status
        if (this.allStructureFieldsData['Employment Status'].custom.length > 0) {
          this.allStructureFieldsData['Employment Status'].custom.forEach(element => {
            this.allEmployeeStatusArray.push(element)
            console.log("22222111111", this.allEmployeeStatusArray);
          });
        }
        if (this.allStructureFieldsData['Employment Status'].default.length > 0) {
          this.allStructureFieldsData['Employment Status'].default.forEach(element => {
            this.allEmployeeStatusArray.push(element)
          });
          console.log("22222", this.allEmployeeStatusArray);
        }

        if (this.allStructureFieldsData['Termination Type'].custom.length > 0) {
          this.allStructureFieldsData['Termination Type'].custom.forEach(element => {
            this.allTerminationTypeArray.push(element)
            console.log("22222111111", this.allTerminationTypeArray);
          });
        }
        if (this.allStructureFieldsData['Termination Type'].default.length > 0) {
          this.allStructureFieldsData['Termination Type'].default.forEach(element => {
            this.allTerminationTypeArray.push(element)
          });
          console.log("22222", this.allTerminationTypeArray);
        }
        // Termination Reason
        if (this.allStructureFieldsData['Termination Reason'].custom.length > 0) {
          this.allStructureFieldsData['Termination Reason'].custom.forEach(element => {
            this.allTerminationReasonArray.push(element)
            console.log("22222111111", this.allTerminationReasonArray);
          });
        }
        if (this.allStructureFieldsData['Termination Reason'].default.length > 0) {
          this.allStructureFieldsData['Termination Reason'].default.forEach(element => {
            this.allTerminationReasonArray.push(element)
          });
          console.log("22222", this.allTerminationReasonArray);
        }


      },
        (err) => {

        })
  }

  getKeys(data) {
    // console.log(data);
    return Object.keys(data);

  }




  getStandardTemplate() {
    this.offBoardingTemplateService.getStandardOffboardingTemplate()
      .subscribe((res: any) => {
        console.log("offboarding standard template", res);
        this.standardOffboardTemplate = res.data;
        this.tempStandardTemplate = this.standardOffboardTemplate;
        // this.standardOffboardTemplate.offBoardingTasks.HrTasks = this.standardOffboardTemplate.offBoardingTasks.HrTasks.map(function (el) {
        //   var o = Object.assign({}, el);
        //   o.isChecked = false;
        //   return o;
        // })
        console.log(this.standardOffboardTemplate.offBoardingTasks.HrTasks);
      },

        (err) => {

        })
  }

  getCustomTemplate() {
    var cID = JSON.parse(localStorage.getItem('companyId'))
    this.offBoardingTemplateService.getcustomOffboardingTemplate(cID)
      .subscribe((res: any) => {
        console.log("offboarding custom template", res);
        this.customOffboardTemplate = res.data;
        this.tempCustomTemplate = this.customOffboardTemplate;
        console.log(this.tempCustomTemplate, this.tempStandardTemplate);
        this.tempTemplateTotal.push(this.standardOffboardTemplate);
        if (this.tempCustomTemplate) {
          this.tempCustomTemplate.forEach(element => {
            this.tempTemplateTotal.push(element)

          });
        }
        console.log(this.tempTemplateTotal);

        this.customOffboardTemplate = this.customOffboardTemplate.map(function (el) {
          var o = Object.assign({}, el);
          o.isChecked = false;
          return o;
        })
        console.log(this.customOffboardTemplate, "1111");


      },
        (err) => {

        })

  }


  getCustomOffboardingtemplate() {
    var id = this.OffboardingTemplateName
    this.offBoardingTemplateService.getcustomOffboardingTemplatedata(id)
      .subscribe((res: any) => {
        console.log("get custom template data", res);

        this.standardOffboardTemplate = res.data[0];
        console.log(this.standardOffboardTemplate);

      },
        (err) => {
          console.log(err);

        })
  }


  templateChange() {
    console.log(this.terminationForm.get('Template').value)
    this.OffboardingTemplateName = this.terminationForm.get('Template').value;
    console.log(this.OffboardingTemplateName);
    this.getCustomOffboardingtemplate();
  }


  showOptions($event, id) {
    console.log("1222222", $event.checked, id);
    if ($event.checked == false) {
      this.customTempId = ''
    }

    if ($event.checked == true) {
      this.customOffboardTemplate.forEach(element => {
        if (element._id == id) {
          element.isChecked = true
          this.customTempId = id;
        }
        else {
          element.isChecked = false;
        }
      });

    }

    console.log(this.customTempId);



  }
  customTemplateRedirect(id) {
    this.router.navigate(['/admin/admin-dashboard/employee-management/off-boarding-landing/custom-offboard/' + id])
  }


  terminationDateChange() {
    console.log(this.terminationForm.get('Date1').value);
    this.terminationDate = this.terminationForm.get('Date1').value
  }

  lastWorkingDateChange() {
    console.log(this.terminationForm.get('Date2').value);
    this.lastWorkingDate = this.terminationForm.get('Date2').value
  }


  terminationDetails(event) {
    console.log("sdfsdfsdfsdfsdf", event)
    this.selectedNav = event;
    if (event == "ClientInfo") {
      this.ClientInfo = true;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab7 = false;
      this.tasks = false;

    } else if (event == "tab2") {
      this.ClientInfo = false;
      this.tab2 = true;
      this.tab3 = false;
      this.tab4 = false;
      this.tab7 = false;
      this.tasks = false;

    }
    else if (event == "tab3") {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = true;
      this.tab4 = false;
      this.tab7 = false;
      this.tasks = false;

    }
    else if (event == "tab4") {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = true;
      this.tab7 = false;
      this.tasks = false;

    }
    else if (event == "tab7") {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tasks = false;
      this.tab7 = true;
    }
    else if (event == 'tasks') {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab7 = false;
      this.tasks = true
    }


  }

  goToTermination() {
    this.client_Info.nativeElement.click();
  }

  goToOffboard() {
    this.offboardTask.nativeElement.click();
  }

  goToDirect() {
    this.directReports.nativeElement.click();
  }

  goToCompany() {
    this.companyProperty.nativeElement.click();
  }

  terminationProceed() {

    console.log("TerminationDetails", this.terminationForm.value)

    if (this.terminationForm.valid && !this.empWorkFlow) {
      var d1 = this.terminationForm.get('Date1').value
      var d2 = this.terminationForm.get('Date2').value
      this.terminationForm.patchValue({ Date1: d1 });
      this.terminationForm.patchValue({ Date2: d2 });
      this.selectedNav = "tab2";
    }
    // Status: new FormControl("", Validators.required),
    // Type: new FormControl("", Validators.required),
    // Reason: new FormControl("", Validators.required),
    // Eligible: new FormControl("", Validators.required),
    // Date1: new FormControl("", Validators.required),
    // Date2: new FormControl("", Validators.required),
    // Procees: new FormControl("", Validators.required),
    // Comments: new FormControl("", Validators.required),
    // Comments: new FormControl("", Validators.required)
    else {
      this.terminationForm.get('Employee').markAsTouched();
      this.terminationForm.get('Status').markAsTouched();
      this.terminationForm.get('Type').markAsTouched();
      this.terminationForm.get('Reason').markAsTouched();
      this.terminationForm.get('Eligible').markAsTouched();
      this.terminationForm.get('Date1').markAsTouched();
      this.terminationForm.get('Date2').markAsTouched();
      this.terminationForm.get('Procees').markAsTouched();
      this.terminationForm.get('Comments').markAsTouched();
      this.terminationForm.get('Template').markAsTouched();
    }
  }

  employeeForDirectReports() {
    console.log("dasdas");
    var uID = this.terminationForm.get('Employee').value;
    var cId = JSON.parse(localStorage.getItem('companyId'))
    console.log(this.terminationForm.get('Employee').value);
    // this.offBoardingTemplateService.
    this.myInfoService.getOtherUser(cId, uID)
      .subscribe((res: any) => {
        console.log("get termiinsate emp data", res);
        this.terminateEmpData = res.data;
        this.terminationForm.get('Status').patchValue(this.terminateEmpData.job.current_employment_status.status)
      },
        (err) => {

        })


    this.offBoardingTemplateService.getDirectReports(cId, uID, this.pageNo, this.perPage)
      .subscribe((res: any) => {
        console.log("get directr reports", res);
        this.directReportsArray = res.data;
      },
        (err) => {

        })
    var postAssetsData = {
      "companyId": cId,
      "userId": uID,
      "per_page": this.perPageAssets,
      "page_no": this.pageNoAssets
    }
    this.myInfoService.getAllAssets(postAssetsData)
      .subscribe((res: any) => {
        console.log("assets Data", res);
        this.assetsData = res.data;
      },
        (err) => {
          console.log(err);

        })


    this.offBoardingTemplateService.getDirectTasks(uID)
      .subscribe((res: any) => {
        console.log("get directr tasks", res);
        this.directTasksArray = res.data;
        if (this.directTasksArray.length <= 1) {
          var result = this.directTasksArray.map(function (el) {
            var o = Object.assign({}, el);
            o.validate = false;
            return o;
          })
          this.directTasksArray = result;
        }
      },
        (err) => {

        })

    this.workflowService.checkEmpInWorkflow(uID)
      .subscribe((res: any) => {
        console.log("Emp workflow check", res);
        this.empWorkFlow = res.data.isAssignedInWorkflow;

      },
        (err) => {
          console.log(err);

        })

  }
  managerChange(empId, event) {
    console.log(empId, event.value);
    if (this.directReportsSend.length == 0) {
      this.directReportsSend.push({ '_id': empId, 'ReportsTo': event.value })
    }
    else {
      for (var i = 0; i < this.directReportsSend.length; i++) {
        if (empId == this.directReportsSend[i]._id) {
          var index = i;
          if (index > -1) {
            this.directReportsSend.splice(index, 1)
          }
        }
      }
      this.directReportsSend.push({ '_id': empId, 'ReportsTo': event.value })
    }
    console.log(this.directReportsSend, "finalSend Directreports");



  }



  employeeChange(data, event, j) {
    console.log(data, event);
    this.directTasksArray[j].validate = false;
    if (this.directTaskSend.length == 0) {
      this.directTaskSend.push({
        'templateId': data.templateId,
        'taskType': data.taskType,
        'taskCategory': data.taskCategory,
        'taskId': data.taskId,
        'taskName': data.taskName,
        'taskAssignee': event.value._id,
        'assigneeEmail': event.value.email
      })
    }
    else {
      for (var i = 0; i < this.directTaskSend.length; i++) {
        if (data.taskId == this.directTaskSend[i].taskId) {
          var index = i;
          if (index > -1) {
            this.directTaskSend.splice(index, 1)
          }
        }
      }
      this.directTaskSend.push({
        'templateId': data.templateId,
        'taskType': data.taskType,
        'taskCategory': data.taskCategory,
        'taskId': data.taskId,
        'taskName': data.taskName,
        'taskAssignee': event.value._id,
        'assigneeEmail': event.value.email
      })
    }
    console.log(this.directTaskSend, "finalSend DirectTaskss");
  }

  taskProceed() {
    this.selectedNav = "tab3";
  }

  reportsProceed() {
    this.selectedNav = "tasks";
  }

  directTasksEmp() {
    console.log("data", this.directTasksArray, this.directTaskSend, this.directTaskSend.length, this.directTasksArray.length);

    if (this.directTasksArray.length > 0 && (this.directTaskSend.length != this.directTasksArray.length)) {

      for (var i = 0; i < this.directTasksArray.length; i++) {
        if (!this.directTasksArray[i].taskAssignee) {
          if (this.directTaskSend.length != this.directTasksArray.length)

            this.directTasksArray[i].validate = true;
        }
      }
    }
    else {
      this.selectedNav = "tab4";

    }
  }

  companyProceed() {
    this.selectedNav = "tab7";
  }


  recapSubmit() {
    var uID = this.terminationForm.get('Employee').value;
    var cId = JSON.parse(localStorage.getItem('companyId'))


    // var file = ,
    var data: any = {
      direct_tasks: this.directTaskSend,
      "direct_reports": this.directReportsSend,
      "companyId": cId,
      "userId": uID,
      "off_boarding_template_assigned": this.terminationForm.get('Template').value,
      "employmentStatus": this.terminationForm.get('Status').value,
      "terminationDate": this.terminationForm.get('Date1').value,
      "terminationType": this.terminationForm.get('Type').value,
      "terminationReason": this.terminationForm.get('Reason').value,
      "eligibleForRehire": this.terminationForm.get('Eligible').value,
      "last_day_of_work": this.terminationForm.get('Date2').value,
      "termination_comments": this.terminationForm.get('Comments').value

    }

    var formdata: FormData = new FormData();
    for (var i = 0; i < this.files.length; i++) {
      formdata.append('file', this.files[i]);
    }
    formdata.append('data', JSON.stringify(data))
    console.log(formdata, 'latest final');
    if (!this.empWorkFlow) {
      this.offBoardingTemplateService.terminateEmployee(formdata)
        .subscribe((res: any) => {
          console.log("terminate employee", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Terminate Employee", res.message, "success")
            jQuery("#myModal").modal("hide");
            this.terminationForm.reset(this.terminationForm.value);
            this.selectedNav = "ClientInfo";
          }

        },
          (err) => {
            console.log(err);
          })
    }
  }


  offboardDelete() {

  }



  templateRedirect() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/off-boarding-landing/standard-offboard"])
  }

  customTemplateAdd() {
    console.log("sadsadasdasdasd");

    this.router.navigate(["/admin/admin-dashboard/employee-management/off-boarding-landing/custom-offboard"])
  }

  customTemplateDelete() {
    this.offBoardingTemplateService.deleteOffboardingTemplate(this.customTempId)
      .subscribe((res: any) => {
        console.log("delete custom template data", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('Custom Template', res.message, "success")
          this.getCustomTemplate();
        }
      },
        (err) => {

        })
  }


  fileChange(event) {
    console.log(event);
    var x = event.target.files;
    this.file = []
    for (var i = 0; i < x.length; i++) {
      this.file.push(x[i])
    }
    console.log(this.file);

    if (this.filesArray.length < 5) {
      console.log(this.file)
      for (var i = 0; i < this.file.length; i++) {
        this.filesArray.push(this.file[i].name)
        this.files.push(this.file[i])
      }

    }
    else {
      this.swalAlertService.SweetAlertWithoutConfirmation("Termination", "You can select up to 5 files.", "error")

    }






    // let fileList: FileList = event.target.files;
    // console.log(fileList);

    // if (fileList.length > 0) {
    //   this.file = fileList[0];
    //   console.log(this.file);

    //   this.fileName = this.file.name;

    //   this.filesArray.push(this.fileName)


    // }
  }

  removeFile(i, data) {
    console.log(i, data);
    this.filesArray.splice(i, 1)
    this.files.splice(i, 1)
    console.log(this.filesArray, this.files);

  }









}
