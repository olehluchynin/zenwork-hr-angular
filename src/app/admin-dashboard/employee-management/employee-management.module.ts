import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeManagementComponent } from './employee-management/employee-management.component';
import { employeeManagementRouting } from './employee-management.routing';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
// import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MyInfoService } from '../../services/my-info.service';
import { NgxMaskModule } from 'ngx-mask';



// import { DirectoryComponent } from './directory/directory.component';
// import { OnBoardingComponent } from './on-boarding/on-boarding.component';
// import { ManagerSelfServiceComponent } from './manager-self-service/manager-self-service.component';
// import { EmployeeSelfServiceComponent } from './employee-self-service/employee-self-service.component';
// import { NewHireHrComponent } from './new-hire-hr/new-hire-hr.component';
// import { NewHireHrTimeComponent } from './new-hire-hr-time/new-hire-hr-time.component';
// import { NewHireWizardComponent } from './new-hire-wizard/new-hire-wizard.component';



@NgModule({
  imports: [
    CommonModule,
    employeeManagementRouting,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSelectModule,
    NgxMaskModule.forRoot({
      showMaskTyped: true
    })
    // FormsModule

  ],
  declarations: [EmployeeManagementComponent,
    //  DirectoryComponent, 
    //  OnBoardingComponent, 
    //  ManagerSelfServiceComponent, 
    //  EmployeeSelfServiceComponent, 
    //  NewHireHrComponent, 
    //  NewHireHrTimeComponent, 
    //  NewHireWizardComponent, 
     ],
  providers: [
    MyInfoService
  ]
})
export class EmployeeManagementModule { }
