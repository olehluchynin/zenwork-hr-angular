import { Component, OnInit } from '@angular/core';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';



@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})
export class CustomComponent implements OnInit {
  show: boolean = false;
  isValid: boolean = false;
  customFieldsData = [

    {
      text: '',
      fieldName: '',
      fieldType: '',
      required: false,
    },


  ];
  fieldData = [];
  fieldName = [];
  sendingObj = {}
  responseKeys = [];
  text: any;
  companyId: any;
  userId: any;
  customFieldValidate: boolean = false;
  constructor(private companySettingsService: CompanySettingsService,
    private myInfoService: MyInfoService,
    private swalAlertService: SwalAlertService) { }

  ngOnInit() {
    this.companyId = JSON.parse(localStorage.getItem('companyId'));
    this.userId = JSON.parse(localStorage.getItem('employeeId'));
    this.getAllCustomFields()
    this.getcustomfieldsData();
  }
  /* Description:get all custom fields
  author : vipin reddy */

  getAllCustomFields() {
    this.companySettingsService.getAllCustomFields()
      .subscribe(
        (res: any) => {
          console.log("res of custom", res);
          this.customFieldsData = res.data.customFields;

          this.customFieldsData = this.customFieldsData.map(function (el) {
            var o = Object.assign({}, el);
            o.text = '';

            return o;
          })
        },
        (err: any) => {
          console.log("err res", err);

        })
  }

  getcustomfieldsData() {
    this.myInfoService.getcustomfieldsData(this.companyId, this.userId)
      .subscribe((res: any) => {
        console.log(res);
        if (res.data) {
          Object.assign(this.sendingObj, res.data)
          console.log(this.sendingObj);
          this.responseKeys = Object.keys(this.sendingObj)

          for (var i = 0; i < this.customFieldsData.length; i++) {
            this.responseKeys.forEach(element1 => {
              if (this.customFieldsData[i].fieldName == element1) {
                this.customFieldsData[i].text = this.sendingObj[this.customFieldsData[i].fieldName]

              }
              console.log(this.customFieldsData);

            })
          }
        }
      },
        (err) => {
          console.log(err);

        })

  }
  /* Description:accept only alphabets
author : vipin reddy */
  alphabets(event) {
    return (event.charCode > 64 &&
      event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)
  }

  saveCompanyContactInfo() {
    this.isValid = true;
    this.customFieldValidate = false;
    console.log(this.customFieldsData);

    for (var i = 0; i < this.customFieldsData.length; i++) {
      this.sendingObj[this.customFieldsData[i].fieldName] = this.customFieldsData[i].text
      console.log(this.sendingObj);
      if (!this.customFieldsData[i].text && this.customFieldsData[i].required) {
        this.customFieldValidate = true;
      }
    }

    var postData = {
      companyId: this.companyId,
      userId: this.userId
    }

    Object.assign(this.sendingObj, postData)
    console.log(this.sendingObj, 'sendingObj');
    if (!this.customFieldValidate) {
      this.myInfoService.sendingCustomService(this.sendingObj)
        .subscribe(
          (res: any) => {
            console.log(res);
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation('custom fields', res.message, 'success');
            }

          },
          (err) => {
            console.log(err);

          })
    }

  }

}
