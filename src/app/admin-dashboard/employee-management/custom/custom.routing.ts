import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomComponent } from './custom/custom.component';

const routes: Routes = [
    // { path:'',redirectTo:"/custom",pathMatch:"full" },
    { path:'',component:CustomComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class CustomRouting { }