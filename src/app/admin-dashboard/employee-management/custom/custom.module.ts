import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomComponent } from './custom/custom.component';
import { CustomRouting } from './custom.routing';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CustomRouting,
    FormsModule,
    BsDatepickerModule.forRoot(),
    MaterialModuleModule
  ],
  declarations: [CustomComponent]
})
export class CustomModule { }
