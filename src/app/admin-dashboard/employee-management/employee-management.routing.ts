import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeManagementComponent } from './employee-management/employee-management.component';
// import { DirectoryComponent } from './directory/directory.component';
// import { OnBoardingComponent } from './on-boarding/on-boarding.component';
// import { ManagerSelfServiceComponent } from './manager-self-service/manager-self-service.component';
// import { EmployeeSelfServiceComponent } from './employee-self-service/employee-self-service.component';
// import { NewHireHrComponent } from './new-hire-hr/new-hire-hr.component';
// import { NewHireHrTimeComponent } from './new-hire-hr-time/new-hire-hr-time.component';
// import { NewHireWizardComponent } from './new-hire-wizard/new-hire-wizard.component';



const routes: Routes = [
    
    { path: '', component: EmployeeManagementComponent },
    { path: 'directory', loadChildren:"./directory/directory.module#DirectoryModule" },
    { path: 'profile', loadChildren: "./employee-profile-view/employee-profile-view.module#EmployeeProfileViewModule" },
    { path: 'off-boarding-landing', loadChildren: "./off-boarding-landing/off-boarding-landing.module#OffBoardingLandingModule"},
    { path: 'on-boarding', loadChildren: "./on-boarding/on-boarding.module#OnBoardingModule"},
    { path: 'new-hire-hr',  loadChildren: "./new-hire-hr/new-hire-hr.module#NewHireHrModule"},
    { path: 'new-hire-hr-time', loadChildren: "./new-hire-hr-time/new-hire-hr-time.module#NewHireHrTimeModule" },
    { path: 'new-hire-wizard', loadChildren: "./new-hire-wizard/new-hire-wizard.module#NewHireWizardModule" },

    { path: 'manager-self-service', loadChildren: "./manager-self-service/manager-self-service.module#ManagerSelfServiceModule"},
    { path: 'employee-self-service', loadChildren: "./employee-self-service/employee-self-service.module#EmployeeSelfServiceModule" },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class employeeManagementRouting { }
