import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PerformanceComponent } from './performance/performance.component';

const routes: Routes = [
    // { path:'',redirectTo:"/performance",pathMatch:"full" },
    {path:'',component:PerformanceComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PerformanceRouting { }
