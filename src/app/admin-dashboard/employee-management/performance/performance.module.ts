import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerformanceComponent } from './performance/performance.component';
import { PerformanceRouting } from './performance.routing';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    PerformanceRouting,
    BsDatepickerModule.forRoot()
  ],
  declarations: [PerformanceComponent]
})
export class PerformanceModule { }
