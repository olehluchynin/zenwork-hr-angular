import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHireWizardComponent } from './new-hire-wizard.component';

describe('NewHireWizardComponent', () => {
  let component: NewHireWizardComponent;
  let fixture: ComponentFixture<NewHireWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHireWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHireWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
