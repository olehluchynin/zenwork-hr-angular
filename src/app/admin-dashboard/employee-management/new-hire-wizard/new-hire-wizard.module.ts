import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewhirewizardRouting } from './new-hire-wizard.routing';
import { NewHireWizardComponent } from './new-hire-wizard.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSelectModule } from '@angular/material/select';
@NgModule({
  imports: [
    CommonModule,
    NewhirewizardRouting,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSelectModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [NewHireWizardComponent]
})
export class NewHireWizardModule { }
