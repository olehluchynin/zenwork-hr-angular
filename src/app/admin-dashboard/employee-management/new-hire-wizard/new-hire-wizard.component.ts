import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-new-hire-wizard',
  templateUrl: './new-hire-wizard.component.html',
  styleUrls: ['./new-hire-wizard.component.css']
})


export class NewHireWizardComponent implements OnInit {
  public selectedField = false;
  selectedNav : any;
  chooseFields = false;
  chooseSteps = true;
  jobSalary = false;
  benefits = false;
  leave = false;
  performance = false;
  summaryReview = false;

  // benefits-tab-starts
  benefitsTab = true;

  modalRef: BsModalRef;
  minDate = new Date(100, 5, 10);
  maxDate = new Date(4000, 9, 15);
  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  openModalWithClass(template: TemplateRef<any>) {
    this.selectedNav = 'chooseSteps';
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }
  openModalWithClass1(template1: TemplateRef<any>) {
    this.modalRef.hide();//first model close
    this.modalRef = this.modalService.show(
      template1,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  myForm = new FormGroup({
    myDateYMD: new FormControl(new Date()),
    myDateFull: new FormControl(new Date()),
    myDateMDY: new FormControl(new Date())
  });
  toggleValue() {
    this.selectedField = !this.selectedField;

  }
  // chooseFields(field, chooseSteps, jobSalary, benefits, leave, performance, summaryReview) {
  //   console.log(field, chooseSteps, jobSalary, benefits, leave, performance, summaryReview)

  //   this.field = !field;

  // }
  chooseFieldss(event: any) {
    this.selectedNav = event;
    if (event == "chooseFields") {
      this.chooseFields = true;
      this.chooseSteps = false;
      this.jobSalary = false;
      this.benefits = false;
      this.leave = false;
      this.performance = false;
      this.summaryReview = false;
    }
    else if (event == "chooseSteps") {
      this.chooseSteps = true;
      this.chooseFields = false;
      this.jobSalary = false;
      this.benefits = false;
      this.leave = false;
      this.performance = false;
      this.summaryReview = false;
    }
    else if (event == "jobSalary") {
      this.jobSalary = true;
      this.chooseSteps = false;
      this.chooseFields = false;
      this.benefits = false;
      this.leave = false;
      this.performance = false;
      this.summaryReview = false;
    }
    else if (event == 'benefits') {
      this.benefits = true;
      this.chooseSteps = false;
      this.chooseFields = false;
      this.jobSalary = false;
      this.leave = false;
      this.performance = false;
      this.summaryReview = false;
    }
    else if (event == 'leave') {
      this.leave = true;
      this.chooseSteps = false;
      this.chooseFields = false;
      this.jobSalary = false;
      this.benefits = false;
      this.performance = false;
      this.summaryReview = false;
    }
    else if (event == "performance") {
      this.performance = true;
      this.chooseSteps = false;
      this.chooseFields = false;
      this.jobSalary = false;
      this.benefits = false;
      this.leave = false;
      this.summaryReview = false;
    }
    else if (event == 'summaryReview') {
      this.summaryReview = true;
      this.chooseSteps = false;
      this.chooseFields = false;
      this.jobSalary = false;
      this.benefits = false;
      this.leave = false;
      this.performance = false;
    }
  }

}
