import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewHireWizardComponent } from './new-hire-wizard.component';

const routes: Routes = [
    { path:'',redirectTo:"/new-hire-wizard",pathMatch:"full" },
    {path:'new-hire-wizard',component:NewHireWizardComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewhirewizardRouting { }
