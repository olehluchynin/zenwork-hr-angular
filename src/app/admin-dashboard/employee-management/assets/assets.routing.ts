import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AssetsComponent } from './assets/assets.component';


const routes: Routes = [
    // { path:'',redirectTo:"/assets",pathMatch:"full" },
    {path:'',component:AssetsComponent},
    
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AssetsRouting { }