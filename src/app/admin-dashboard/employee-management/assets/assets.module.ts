import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetsComponent } from './assets/assets.component';
import { AssetsRouting } from './assets.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { AddAssetDialogComponent } from './assets/add-asset-dialog/add-asset-dialog.component';
import { MyInfoService } from '../../../services/my-info.service';



@NgModule({
  imports: [
    CommonModule,
    AssetsRouting,
    FormsModule,
    ReactiveFormsModule,
    MaterialModuleModule
  ],
  declarations: [AssetsComponent, AddAssetDialogComponent],
  entryComponents: [AddAssetDialogComponent],
  providers: [MyInfoService]
})
export class AssetsModule { }
