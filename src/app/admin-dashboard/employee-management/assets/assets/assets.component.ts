import { Component, OnInit } from '@angular/core';
import { AddAssetDialogComponent } from '../assets/add-asset-dialog/add-asset-dialog.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { MyInfoService } from '../../../../services/my-info.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit {

  public pageNo: any;
  public perPage: any;
  public count: any;
  public companyId: any;
  public userId: any;
  public employeeDetails: any;
  public allAssets = [];
  public selectedAssets = [];
  public getSingleAsset: any;
  public checkAssetIds = [];
  roleType: any;
  pagesAccess = [];
  assetsTabView: boolean = false;

  constructor(
    public dialog: MatDialog,
    private accessLocalStorageService: AccessLocalStorageService,
    private myInfoService: MyInfoService,
    private swalAlertService: SwalAlertService,
    private router: Router


  ) { }

  ngOnInit() {

    this.pageNo = 1;
    this.perPage = 10;

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));

    if (this.employeeDetails) {
      this.userId = this.employeeDetails._id;
    }
    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.userId = JSON.parse(localStorage.getItem('employeeId'));
      this.pageAccesslevels();
    }
    console.log(this.userId);

    this.getAllAssets();
  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllAssets();
  }



  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Assets" && element.access == 'view') {
              console.log('data comes in2');

              this.assetsTabView = true;
              console.log('loggss', this.assetsTabView);

            }

          });

        },
        (err: any) => {

        })


  }


  openDialog(): void {

    if (this.getSingleAsset) {

      const dialogRef = this.dialog.open(AddAssetDialogComponent, {
        width: '1300px',
        backdropClass: 'structure-model',
        data: this.getSingleAsset,

      });

      dialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed', result);
        this.getAllAssets();
        this.checkAssetIds = [];

      });
    }
    else {
      const dialogRef = this.dialog.open(AddAssetDialogComponent, {
        width: '1300px',
        backdropClass: 'structure-model',
        data: {},

      });

      dialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed', result);
        this.getAllAssets();
        this.checkAssetIds = [];
      });
    }
  }

  // Author:Saiprakash, Date:13/05/2019
  // Get all assets

  getAllAssets() {
    var getAssets: any = {
      companyId: this.companyId,
      userId: this.userId,
      per_page: this.perPage,
      page_no: this.pageNo
    }
    this.myInfoService.getAllAssets(getAssets).subscribe((res: any) => {
      console.log(res);
      this.allAssets = res.data;
      this.count = res.total_count;

    }, (err) => {
      console.log(err);

    })

  }

  // Author:Saiprakash, Date:13/05/2019
  // Get single assets

  editAsset() {
    this.selectedAssets = this.allAssets.filter(assetCheck => {
      return assetCheck.isChecked
    })

    if (this.selectedAssets.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one asset to proceed", "", 'error')
    } else if (this.selectedAssets.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one asset to proceed", "", 'error')
    } else {

      this.myInfoService.getAsset(this.companyId, this.userId, this.selectedAssets[0]._id).subscribe((res: any) => {
        console.log(res);

        this.getSingleAsset = res.data;
        this.openDialog();

      }, (err) => {
        console.log(err);

      })
    }

  }

  // Author:Saiprakash, Date:13/05/2019
  // Delete multiple assets

  deleteAssets() {
    this.selectedAssets = this.allAssets.filter(assetCheck => {
      return assetCheck.isChecked
    })

    if (this.selectedAssets.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select assets to proceed", "", 'error')
    } else {

      var deleteAsset: any = {
        companyId: this.companyId,
        userId: this.userId,
        _ids: this.selectedAssets

      }

      this.myInfoService.deleteAsset(deleteAsset).subscribe((res: any) => {
        console.log(res);

        this.swalAlertService.SweetAlertWithoutConfirmation("Deleted asset Successfully", "", "success");
        this.getAllAssets();
        this.checkAssetIds = [];

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })


    }

  }

  checkedAssets(event, id) {
    console.log(event, id);
    if (event.checked == true) {
      this.checkAssetIds.push(id)
    }
    console.log(this.checkAssetIds);
    if (event.checked == false) {
      var index = this.checkAssetIds.indexOf(id);
      if (index > -1) {
        this.checkAssetIds.splice(index, 1);
      }
      console.log(this.checkAssetIds);
    }


  }
  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/onboardingtab"])

  }


}
