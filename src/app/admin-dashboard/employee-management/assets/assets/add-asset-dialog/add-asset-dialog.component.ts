import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MyInfoService } from '../../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';


@Component({
  selector: 'app-add-asset-dialog',
  templateUrl: './add-asset-dialog.component.html',
  styleUrls: ['./add-asset-dialog.component.css']
})
export class AddAssetDialogComponent implements OnInit {

  public employeeAssets : any = {
    category:'',
    description:'',
    serial_number:'',
    date_loaned:'',
    date_returned:''
  }
  public companyId:any;
  public userId:any;
  public employeeDetails:any;
  public getAllStructureFields:any;
  public customAssets = [];
  public defaultAssets = [];
  public allAssets = [];
  disableDateLoaned:boolean = true;
  roleType:any;
  constructor(
    private myInfoService : MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService : SwalAlertService,
    public dialogRef: MatDialogRef<AddAssetDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data 
  ) { }


  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    console.log(this.data);
    

    if(this.data){
      this.employeeAssets = this.data;

    }
   
    
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.getStandardAndCustomStructureFields();
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    if(this.employeeDetails){
      this.userId = this.employeeDetails._id;
      }
      this.roleType = JSON.parse(localStorage.getItem('type'))
  
      if (this.roleType != 'company') {
        this.userId = JSON.parse(localStorage.getItem('employeeId'));
      }
    console.log(this.userId);
    
  }

    // Author:Saiprakash, Date:24/05/2019
  // Get Standard And Custom Structure Fields

  getStandardAndCustomStructureFields(){
    this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(
      (res:any)=>{
        console.log(res); 
        this.getAllStructureFields = res.data;
        this.customAssets = this.getAllStructureFields['Assets'].custom;
        this.defaultAssets = this.getAllStructureFields['Assets'].default;
        this.allAssets = this.customAssets.concat(this.defaultAssets);
 
      }
    )
  }

  // Author:Saiprakash, Date:13/05/2019
  // Add and edit employee assets
  addAsset(){
    var addData:any = {
      companyId: this.companyId,
      userId: this.userId,
      category: this.employeeAssets.category,
      description: this.employeeAssets.description,
      serial_number: this.employeeAssets.serial_number,
      date_loaned: this.employeeAssets.date_loaned,
      date_returned: this.employeeAssets.date_returned
    }

    if(this.data && this.data._id){
        this.myInfoService.editAsset(this.data._id, addData).subscribe((res:any)=>{
          console.log(res);
          this.employeeAssets.category = '';
          this.employeeAssets.description = '';
          this.employeeAssets.serial_number = '';
          this.employeeAssets.date_loaned = '';
          this.employeeAssets.date_returned = '';
          this.data._id = '';
         this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        
         this.dialogRef.close(res.data);
         
        }, (err)=>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        })
    }

    else {

    this.myInfoService.addAssets(addData).subscribe((res:any)=>{
      console.log(res);
    this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
    this.employeeAssets = "";
    this.dialogRef.close(res.data);
    
      
    }, (err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    })
  }
  }

  cancel(){
    this.employeeAssets.category = '';
    this.employeeAssets.description = '';
    this.employeeAssets.serial_number = '';
    this.employeeAssets.date_loaned = '';
    this.employeeAssets.date_returned ='';
    this.data._id = '';
    this.dialogRef.close();
   
  }

}
