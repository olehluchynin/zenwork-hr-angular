import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { EmployeeService } from '../../../services/employee.service';


@Component({
  selector: 'app-new-hire-hr',
  templateUrl: './new-hire-hr.component.html',
  styleUrls: ['./new-hire-hr.component.css']
})
export class NewHireHrComponent implements OnInit {
  /* minDate = new Date(100, 5, 10);
  maxDate = new Date(4000, 9, 15); */

  public newEmployeeDetails = {
    name: { firstName: '', lastName: '', middleName: '', prefferedName: '' },
    birth: '',
    gender: '',
    maritalStatus: '',
    effectiveDate: '',
    veteranStatus: '',
    ssn: '',
    sin: '',
    nin: '',
    ethnicity: '',
    type: '',
    contact: {
      workPhone: '',
      extention: '',
      mobilePhone: '',
      homePhone: '',
      workMail: '',
      personalMail: ''
    },
    address: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: '',
      country: ''
    },
  }


  constructor(
    private employeeOnboardingService: EmployeeService,
    private swalAlertService: SwalAlertService
  ) { }

  ngOnInit() {
  }
  /* myForm = new FormGroup({
    myDateYMD: new FormControl(new Date()),
    myDateFull: new FormControl(new Date()),
    myDateMDY: new FormControl(new Date())
  }); */

  hireNewEmployee() {
    this.employeeOnboardingService.addNewEmployee(this.newEmployeeDetails)
      .subscribe(
        (res: any) => {
          if (res.status) {
            this.newEmployeeDetails = {
              name: { firstName: '', lastName: '', middleName: '', prefferedName: '' },
              birth: '',
              gender: '',
              maritalStatus: '',
              effectiveDate: '',
              veteranStatus: '',
              ssn: '',
              sin: '',
              nin: '',
              ethnicity: '',
              type: '',
              contact: {
                workPhone: '',
                extention: '',
                mobilePhone: '',
                homePhone: '',
                workMail: '',
                personalMail: ''
              },
              address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
              },
            }
            this.swalAlertService.SweetAlertWithoutConfirmation("Employees Onboarding", "Employee onboarded successfully", 'success')
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Employees Onboarding", "Data Not Found", 'info')
          }
        },
        (err: HttpErrorResponse) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Employees Onboarding", err.error.message, 'error')
        }
      )
  }


}
