import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewhirehrRouting } from './new-hire-hr.routing';
import { NewHireHrComponent } from './new-hire-hr.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NewhirehrRouting,
    BsDatepickerModule.forRoot(),
    MatSelectModule
  ],
  declarations: [NewHireHrComponent]
})
export class NewHireHrModule { }
