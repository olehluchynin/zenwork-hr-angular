import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewHireHrComponent } from './new-hire-hr.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    { path:'',redirectTo:"/new-hire-hr",pathMatch:"full" },
    {path:'new-hire-hr',component:NewHireHrComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule,CommonModule],
    exports: [RouterModule]
})
export class NewhirehrRouting { }
