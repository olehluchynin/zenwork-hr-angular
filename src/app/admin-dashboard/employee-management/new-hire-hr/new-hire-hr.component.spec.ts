import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHireHrComponent } from './new-hire-hr.component';

describe('NewHireHrComponent', () => {
  let component: NewHireHrComponent;
  let fixture: ComponentFixture<NewHireHrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHireHrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHireHrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
