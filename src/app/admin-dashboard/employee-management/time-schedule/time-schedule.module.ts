import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeScheduleComponent } from './time-schedule/time-schedule.component';
import{ timeScheduleRouting } from'./time-schedule.routing';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { NgbModalModule, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { WorkScheduleComponent } from './work-schedule/work-schedule.component';
import { ScheduleTimeComponent } from './schedule-time/schedule-time.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { SummeryComponent } from './summery/summery.component';
import { ListComponent } from './list/list.component';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import {MatTooltipModule} from '@angular/material/tooltip';




@NgModule({
  imports: [
    CommonModule,
    timeScheduleRouting,
    MatSelectModule,
    MatSlideToggleModule,
    BsDatepickerModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgbModalModule,MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,MatCheckboxModule,
    FormsModule,ReactiveFormsModule,NgxMaterialTimepickerModule,MatTooltipModule
  ],
  declarations: [TimeScheduleComponent, WorkScheduleComponent, ScheduleTimeComponent, SummeryComponent, ListComponent],
  providers: [LeaveManagementService]
})
export class TimeScheduleModule { }
