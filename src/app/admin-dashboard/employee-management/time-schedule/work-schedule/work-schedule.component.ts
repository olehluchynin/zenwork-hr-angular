import { Component, OnInit } from '@angular/core';
import { LeaveManagementService } from '../../../../services/leaveManagement.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { TimeScheduleService } from '../../../../services/time-schedule.service';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';



declare var jQuery: any;




@Component({
  selector: 'app-work-schedule',
  templateUrl: './work-schedule.component.html',
  styleUrls: ['./work-schedule.component.css']
})
export class WorkScheduleComponent implements OnInit {
  public userId: any;
  constructor(
    private leaveManagementService: LeaveManagementService,
    private swalAlertService: SwalAlertService,
    private accessLocalStorageService: AccessLocalStorageService,
    private timeScheduleService: TimeScheduleService
  ) { }

  companyId: any;
  workScheduleNames = [];
  // workSchedules: boolean = false;
  scheduletype: any;

  editWorkSchedule: any = {
    scheduletitle: '',
    scheduletype:'',
    schedulestartdate: '',
    day: '',
    total_hrs_day: '',
    total_hrs_week: '',
    lunch_break_time: '',
    starttime: '',
    lunch_starttime: '',
    lunch_endtime: '',
    end_time: '',
    type: '',
    employeelist : [],
    days_of_week: [

    ],

    week_applicable: [

    ]


  }

  selectDayOfWeek: any;
  selectedDays: any = [];
  weekDays: any = [

  ];

  public days = [
    { name: 'Monday', isChecked: false },
    { name: 'Tuesday', isChecked: false },
    { name: 'Wednesday', isChecked: false },
    { name: 'Thursday', isChecked: false },
    { name: 'Friday', isChecked: false },
    { name: 'Saturday', isChecked: false },
    { name: 'Sunday', isChecked: false },
  ];

  workScheduleFetchData: any;

  isValid: boolean = false;
  workScheduleNameId: any;
  allSelectedDays: any = [];
  totalDayEvent: any;
  startTimeValue:any;
  personalId:any;
  directoryID:any;

  workHistory : {

    position_work_schedule_history : [
      {
        position_schedule_history : []
      }
    ]

  };
  timeSheetData:any;
  timeData:any = {
    keys : []
  }

  timeInsideData: any = [
    {
      lateByTime : '',
      hoursworked:'',
      grosshours:'',
      attendanceDetails : [],
      schedules: [
        {
          punchIn:'',
          marginLeft:''
        }
      ]
    }
  ]

  attendanceData :any;
  marginLeftValue:any;
  timeValue:any;
  punchInValue:any;

  ngOnInit() {
    this.userId = this.accessLocalStorageService.get('employeeId');
    console.log(this.userId);

    this.companyId = localStorage.getItem('companyId');
    console.log("company iddd", this.companyId);
    this.companyId = this.companyId.replace(/^"|"$/g, "");
    console.log("company iddd11233333", this.companyId);

    this.personalId = JSON.parse(localStorage.getItem('employee'));
    console.log("personal iddddddd",this.personalId);
    
    if(this.personalId){
      this.directoryID = this.personalId._id;
    }

    this.getAllWorksData();
    this.getAllAttendenceData();

  }

 // Author: Suresh M, Date: 17/07/19
  // approveTimesheet
  approveTimesheet() {
    var data: any = {
      userId: "5cda377f51f0be25b3adceb4"
    }
    this.leaveManagementService.timeSheetApproval(data).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");

    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
    })
  }


 // Author: Suresh M, Date: 17/07/19
  // getAllWorksData
  getAllWorksData() {
    this.timeScheduleService.getAllWorkScheduleData(this.companyId)
      .subscribe((res: any) => {
        console.log("work shcedule namesss", res);
        this.workScheduleNames = res.result;
        console.log("get dataaa", this.workScheduleNames);
      });
  }


// Author: Suresh M, Date: 17/07/19
  // scheduleTypeChange
  scheduleTypeChange(event) {
    console.log("type name", event);

    var typeData = {
      schedulename: this.editWorkSchedule.scheduletitle,
      scheduletype: this.scheduletype
    }
    console.log("data valueee", typeData,this.editWorkSchedule.scheduletitle);

    this.timeScheduleService.workScheduleTypeData(typeData)
      .subscribe((res: any) => {
        console.log("typeeee", res);
        this.workScheduleFetchData = res.result[0];
        this.workScheduleNameId = res.result[0]._id;
        console.log("names and typee idddddd", this.workScheduleNameId);
        console.log("fetch dataaaa", this.workScheduleFetchData)
        this.editWorkSchedule = this.workScheduleFetchData;

        this.weekDays = this.workScheduleFetchData.week_applicable;
        if (this.workScheduleFetchData.days_of_week && this.workScheduleFetchData.days_of_week.length) {
          console.log(this.workScheduleFetchData.days_of_week);

          let index = this.workScheduleFetchData.days_of_week.map(e => {
            let i = this.days.findIndex(ele => ele.name === e)
            if (i >= 0) {
              this.days[i].isChecked = true;

            }

          });
          this.allSelectedDays = this.selectedDays.concat(this.workScheduleFetchData.days_of_week)

        }
        console.log("dataaaaaaa", this.allSelectedDays);

        // this.workSchedules = true;
      });

  }

  // Author: Suresh M, Date: 17/07/19
  // selectDay
  selectDay(event, day, isChecked) {

    console.log(event, day);

    console.log("dayssss offfff", event);

    this.selectDayOfWeek = day;
    console.log("select day", this.selectDayOfWeek);
    if (event == true) {
      if (this.selectedDays) {
        this.selectedDays.push(day)
      }

      this.weekDays.push({
        day: day,
        starttime: '',
        lunch_starttime: '',
        lunch_endtime: '',
        end_time: '',
        edit: false,
        isChecked: false
      })
    }
    else if (event == false) {
      for (var i = 0; i < this.selectedDays.length; i++) {
        if (this.selectedDays[i] === day) {
          this.selectedDays.splice(i, 1);
        }

      }

      for (var i = 0; i < this.weekDays.length; i++) {
        if (this.weekDays[i].day === day) {
          this.weekDays.splice(i, 1);
        }

      }

    }

    console.log(this.weekDays);
    console.log("dayssssssssssssssss", this.selectedDays);

    //  if(this.allSelectedDays.length){
    //    console.log("lenghtttt", this.allSelectedDays.length)
    //    this.editWorkSchedule.total_hrs_week = this.allSelectedDays.length * event;
    //  }

    if (this.workScheduleFetchData.days_of_week) {

      this.allSelectedDays = this.selectedDays.concat(this.workScheduleFetchData.days_of_week);
      
      this.editWorkSchedule.total_hrs_week = this.allSelectedDays.length * this.workScheduleFetchData.total_hrs_day;

      if(this.totalDayEvent){
        this.editWorkSchedule.total_hrs_week = this.allSelectedDays.length * this.editWorkSchedule.total_hrs_day;
      }

    }
    console.log("allselectdays", this.allSelectedDays.length);

  }


  // Author: Suresh M, Date: 17/07/19
  // updateWorkSchedule
  updateWorkSchedule() {

    var updateData: any = {
      _id: this.workScheduleNameId,
      companyId: this.companyId,
      scheduletitle: this.editWorkSchedule.scheduletitle,
      schedulestartdate: this.editWorkSchedule.schedulestartdate,
      type: this.editWorkSchedule.type,
      total_hrs_day: this.editWorkSchedule.total_hrs_day,
      total_hrs_week: this.editWorkSchedule.total_hrs_week,
      lunch_break_time: this.editWorkSchedule.lunch_break_time,

      week_applicable: this.weekDays,
      days_of_week: this.allSelectedDays,
      employeelist :this.editWorkSchedule.employeelist,

    }
   

   
      // if(this.workScheduleFetchData.days_of_week ){
      this.timeScheduleService.updateWorkScheduler(updateData)
        .subscribe((res: any) => {
          console.log("updateeeee", res);
          this.getAllWorksData();
          jQuery("#myModal1").modal("hide");

          this.editWorkSchedule = {};
          this.selectedDays = [];
          this.weekDays = [];
          this.isValid = false;
          this.workScheduleFetchData = '';
          this.scheduletype = '';
          this.editWorkSchedule.scheduletitle='';
          // this.workSchedules = false;
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Updates", "Successfully Update the data", "success")
          } else {

          }
        });

  }

  

  // Author: Suresh M, Date: 17/07/19
  // totalHoursDay
  totalHoursDay(event) {
    console.log("dayyyyyyyyy", event);
    this.totalDayEvent = event;

    if (this.allSelectedDays.length) {
      var num = event;
      var min, hrs;
      this.editWorkSchedule.total_hrs_week = [];
      this.weekDays.starttime = [];
      this.weekDays.end_time = [];

      if (num) {
        min = num.split(':');
      }

      // console.log(min[0], "minutes afcter");
      hrs = min[0] * 60;
      if (min[1] && min[1] !== '') {
        console.log(min, "minutes afcter");

        hrs = parseInt(hrs) + parseInt(min[1]);
        console.log("hrssssss", hrs)

      }
      this.editWorkSchedule.total_hrs_week = this.allSelectedDays.length * hrs / 60;
      this.weekDays.starttime = [];
    } else {
      // this.swalAlertService.SweetAlertWithoutConfirmation("Please Select the Days", "", "Error")
    }


  }

 // Author: Suresh M, Date: 17/07/19
  // timeStartDay
  timeStartDay(event){
    console.log("time starttt", event);
    this.startTimeValue = event;
  }

 // Author: Suresh M, Date: 17/07/19
  // endTimeDays
  endTimeDays(event, i) {
    console.log("endddddd", event,i);

    console.log("weekday iiiiii", this.startTimeValue);

    var t0 = this.startTimeValue;

    console.log("check t00000", t0);

    var t1 = event;

    console.log("t00000", t0, t1);

    var diff = this.getTimeDifference(t0, t1);
    console.log("timeeee difffff", diff)
    var d = parseInt(diff.split(':')[0]);
    var p = parseInt(this.editWorkSchedule.total_hrs_day)
    console.log(d, typeof d, typeof d)
    console.log(d > p || d < p)
    // this.weekDays[i].end_time=event;
    if (d !== p) {
      console.log(this.weekDays)
      // this.weekDays[i].end_time='';
      console.log(this.weekDays)

      this.swalAlertService.SweetAlertWithoutConfirmation("Time period", "Please Check the correct time duration ", "error")
    } else {
      this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Successfully the time duratio", "success")
    }

  }

  timeStringToMins(s) {
    s = s.split(':');
    s[0] = /m$/i.test(s[1]) && s[0] == 12 ? 0 : s[0];
    return s[0] * 60 + parseInt(s[1]) + (/pm$/i.test(s[1]) ? 720 : 0);
  }

  // Return difference between two times in hh:mm[am/pm] format as hh:mm
  getTimeDifference(t0, t1) {

    // Small helper function to padd single digits
    function z(n) { return (n < 10 ? '0' : '') + n; }

    // Get difference in minutes
    var diff = this.timeStringToMins(t1) - this.timeStringToMins(t0);

    // Format difference as hh:mm and return
    return z(diff / 60 | 0) + ':' + z(diff % 60);
  }

   // Author: Suresh M, Date: 17/07/19
  // getAllHistoryData
getAllHistoryData(){

  console.log("history idddddd", this.workScheduleNameId)
  this.timeScheduleService.getHistoryData(this.directoryID,this.workScheduleNameId)
  .subscribe((res:any)=>{
    console.log("historyyyy",res);
    this.workHistory = res.result[0].position_work_schedule_history;
    console.log("work hissssss", this.workHistory);

  });

}

 // Author: Suresh M, Date: 24/07/19
  // getAllAttendenceData
getAllAttendenceData(){

  var data = {
    userId :'5cd91cc3f308c21c43e5054d',
    timesheetStartDate: "2019-07-21 00:00:00.000Z",
    timesheetEndDate:"2019-07-26 00:00:00.000Z"
  }

  this.timeScheduleService.getWorkScheduleTimeSheet(data)
  .subscribe((res:any)=>{
    console.log("attendencee", res);
    this.timeSheetData = res.data.dateWiseAttendanceList;
    console.log("timeeee", this.timeSheetData);
    this.timeData = Object.keys(res.data.dateWiseAttendanceList);
    console.log("time dataaaaaa", this.timeData);
    this.timeInsideData = Object.keys(res.data.dateWiseAttendanceList).map(key =>({
      key, value : res.data.dateWiseAttendanceList [key]
    }));
  
    console.log("time insideeeee", this.timeInsideData);

  
    this.timeInsideData.forEach((element:any) => {
      var totalMinutes=1440;
      element.schedules=[]
      if(element.value.attendanceDetails && element.value.attendanceDetails.length ){
        element.value.attendanceDetails.forEach(times => {
          if(times.punchType==="IN"){
            console.log(element)
            var nt=new Date(times.punchTime);
            var timenow=(nt.getHours()*60) +nt.getMinutes()

            element.schedules.push({punchIn:timenow})
          }else if(times.punchType==="OUT"){
            console.log(element)
            var nt=new Date(times.punchTime);
            var timenow=(nt.getHours()*60) +nt.getMinutes();

            element.schedules[element.schedules.length-1].punchOut=timenow;
            element.schedules[element.schedules.length-1].marginLeft=(element.schedules[element.schedules.length-1].punchIn/totalMinutes)*100;
            element.schedules[element.schedules.length-1].width=((element.schedules[element.schedules.length-1].punchOut-element.schedules[element.schedules.length-1].punchIn)/totalMinutes)*100;
          }
        });
      }


    });

    
  });

}

 // Author: Suresh M, Date: 24/07/19
  // getToolTimeData
getToolTimeData(data){
  // console.log(data);
  var punchinTime=this.getTimeFromMinutes(data.punchIn);
  var punchoutTime=this.getTimeFromMinutes(data.punchOut);

  // console.log(punchinTime, punchinTime)
return 'Punch in '+ punchinTime +', Punch out '+punchoutTime;
}

// Author: Suresh M, Date: 24/07/19
  // getTimeFromMinutes
getTimeFromMinutes(mins){
  let h = Math.floor(mins / 60);
  let m:any = mins % 60;
  m = m < 10 ? '0' + m : m;

 
  let a = 'am';
  if (h >= 12) a = 'pm';
  if (h > 12) h = h - 12;
  return `${h}:${m} ${a}`;

}

positionsChange(event){
  console.log("positionss", event);



}





}
