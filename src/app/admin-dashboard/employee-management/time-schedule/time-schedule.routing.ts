import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimeScheduleComponent } from './time-schedule/time-schedule.component';
import { WorkScheduleComponent } from './work-schedule/work-schedule.component';
import { ScheduleTimeComponent } from './schedule-time/schedule-time.component';


const routes: Routes = [

    {
        path: '', component: TimeScheduleComponent, children: [
            { path: '', redirectTo: "work-schedule", pathMatch: "full" },
            { path: 'work-schedule', component: WorkScheduleComponent },
            { path: 'schedule-time', component:ScheduleTimeComponent }
        ]
    },

]




@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class timeScheduleRouting { }