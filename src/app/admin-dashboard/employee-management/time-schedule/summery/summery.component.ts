import { Component, OnInit } from '@angular/core';
import { TimeScheduleService } from '../../../../services/time-schedule.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';


@Component({
  selector: 'app-summery',
  templateUrl: './summery.component.html',
  styleUrls: ['./summery.component.css']
})
export class SummeryComponent implements OnInit {
  requestType:any;
  requestTimeOffData = [];

  constructor(
    private timeScheduleService: TimeScheduleService,
    private swalAlertService: SwalAlertService

  ) { }

  ngOnInit() {
  }

  // Author: Saiprakash G, Date: 22/07/19,
  // Get time off request type

  timeOffRequestType(type){
    this.requestType = type;
    var data:any = {
      "employeeId": "5cd91cc3f308c21c43e5054d",
      "requestType": this.requestType,
      "perPageCount": 10,
      "currentPageNumber": 0
    }

    this.timeScheduleService.getTimeoffRequests(data).subscribe((res:any)=>{
      console.log(res);
      this.requestTimeOffData = res.data
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
    },(err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    })

  }


}
