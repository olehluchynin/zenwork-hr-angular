import { Component, OnInit } from '@angular/core';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { TimeScheduleService } from '../../../../services/time-schedule.service';
import { LeaveManagementService } from '../../../../services/leaveManagement.service';


declare var jQuery:any;



@Component({
  selector: 'app-schedule-time',
  templateUrl: './schedule-time.component.html',
  styleUrls: ['./schedule-time.component.css']
})
export class ScheduleTimeComponent implements OnInit {
  companyId:any;
  id:any;
  personalId:any;
  groupName:any;
  effectiveDate:any;
  currentLeaveEligibilityId:any;
  leaveEligibilityGroupHistory = [];

  constructor(
    private accessLocalStorageService: AccessLocalStorageService,
    private timeScheduleService : TimeScheduleService,
    private leaveManagementService: LeaveManagementService


  ) { }

  ngOnInit() {
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);

    this.personalId = JSON.parse(localStorage.getItem('employee'));
    console.log(this.personalId);
    
    if(this.personalId){
      this.id = this.personalId._id;
    }

    this.getCurrentLeaveEligibilityGroup();
    this.getTimeOffPolicies();
  }

  // Author: Saiprakash G, Date: 19/07/19,
  // Description: Get current leave eligibility group

  getCurrentLeaveEligibilityGroup(){
    this.timeScheduleService.getCurrentLeaveEligGroup("5cd91cc3f308c21c43e5054d").subscribe((res:any)=>{
      console.log(res);
      this.groupName = res.data.leave_eligibility_id[0].name;
      this.effectiveDate = res.data.leave_EligGrp_EffectDate;
      this.currentLeaveEligibilityId = res.data.leave_eligibility_id[0]._id;
      // this.getTimeOffPolicies();
    },(err)=>{
      console.log(err);
      
    })
  }

  // Author: Saiprakash G, Date: 19/07/19,
  // Description: Get leave eligibility group history

  getLeaveEligibilityGroupHistory(){
   this.timeScheduleService.getLeaveEligGroupHistory("5cda377f51f0be25b3adceb4").subscribe((res:any)=>{
     console.log(res);
     this.leaveEligibilityGroupHistory = res.data.leave_eligibility_group_history
   },(err)=>{
     console.log(err);
     
   })
  }

  // Author: Saiprakash G, Date: 19/07/19,
  // Description: Change leave eligibilty group

  changeLeaveEligibilityGroup(){
    var data:any = {
      "newEligibilityGroup": "5d0cdcbdc213d57fd5a1e33c",
      "userId": "5cd91cc3f308c21c43e5054d",
      "updatedBy": "5c9e11894b1d726375b23057"
    }
    this.timeScheduleService.changeLeaveEligibilityGroup(data).subscribe((res:any)=>{
      console.log(res);
      
    },(err)=>{
      console.log(err);
      
    })
  }

  getTimeOffPolicies(){
    this.timeScheduleService.getTimeOffPolicies("5cda377f51f0be25b3adceb4").subscribe((res:any)=>{
      console.log(res);
      
    },(err)=>{
      console.log(err);
      
    })
  }

  applyScheduleTimeOff(){
    var data:any = {
      "timeoffEmployee": "5cd91cc3f308c21c43e5054d",
      "selectedPolicy": "5d23227784d011475d5e8fb9",
      "timeoffPolicyType": "Sick",
      "timeoffStartDate": "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
      "timeoffEndDate": "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
      "timeOffStartTime": "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
      "timeoffEndTime": "Sat Jul 13 2019 23:59:00 GMT+0530 (India Standard Time)",
      "createdBy": "5cd91cc3f308c21c43e5054d"
    }

    this.timeScheduleService.applyForScheduledTimeOff(data).subscribe((res:any)=>{
      console.log(res);
      
    },(err)=>{
      console.log(err);
      
    })
  }

  updateScheduleTimeOff(status){
    var data:any = {
      "timeoffRequestId": "5d25794512ba8410189f126d",
      "timeoffEmployee": "5cd91cc3f308c21c43e5054d",
      "timeoffStartDate": "Thu Jul 11 2019 00:00:00 GMT+0530 (India Standard Time)",
      "timeoffEndDate": "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
      "timeOffStartTime": "Thu Jul 11 2019 00:00:00 GMT+0530 (India Standard Time)",
      "timeoffEndTime": "Sat Jul 13 2019 23:59:00 GMT+0530 (India Standard Time)",
      "timeoffStatus": status,
      "updatedBy": "5cda377f51f0be25b3adceb4"
    }

    this.timeScheduleService.updateScheduledTimeOff(data).subscribe((res:any)=>{
      console.log(res);
      
    },(err)=>{
      console.log(err);
      
    })
  }


  timeOffAdd(){
    jQuery("#myModal1").modal("hide");
    jQuery("#myModal2").modal("show");
  }


  





}
