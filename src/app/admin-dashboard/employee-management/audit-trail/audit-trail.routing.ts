import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';

const routes: Routes = [
    // { path:'',redirectTo:"/audit-trail",pathMatch:"full" },
    { path:'',component:AuditTrailComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AuditTrailRouting { }