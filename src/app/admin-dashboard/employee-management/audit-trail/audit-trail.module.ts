import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';
import { AuditTrailRouting } from './audit-trail.routing';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    AuditTrailRouting,
    BsDatepickerModule.forRoot(),
  ],
  declarations: [AuditTrailComponent]
})
export class AuditTrailModule { }
