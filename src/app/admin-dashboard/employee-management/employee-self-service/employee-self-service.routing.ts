import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeSelfServiceComponent } from './employee-self-service.component';

const routes: Routes = [
    { path:'',redirectTo:"/employee-self-service",pathMatch:"full" },
    {path:'employee-self-service',component:EmployeeSelfServiceComponent},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployeeselfserviceRouting { }
