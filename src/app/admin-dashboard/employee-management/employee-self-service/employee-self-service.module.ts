import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeselfserviceRouting } from './employee-self-service.routing';
import { EmployeeSelfServiceComponent } from './employee-self-service.component';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    EmployeeselfserviceRouting,
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [EmployeeSelfServiceComponent]
})
export class EmployeeSelfServiceModule { }
