import { Component, OnInit } from '@angular/core';
import { EmployeeServiceService } from '../../../services/employee-service.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employee-self-service',
  templateUrl: './employee-self-service.component.html',
  styleUrls: ['./employee-self-service.component.css']
})
export class EmployeeSelfServiceComponent implements OnInit {

  constructor(private employeeSelfService: EmployeeServiceService, private swalService: SwalAlertService,
    private router: Router) { }

  title: any;
  scheduled_date: any;
  expiration_date: any;
  description: any;

  selectData: any = [

  ]

  checkDeleteIds: any = [];

  list: any;
  companyID: any;
  userID: any;
  lists: any;

  companyAddList: Array<any> = [
    {
      edit: true,
      buttonDisabled: false,
      resetDisabled: false
    }
  ]

  selectCheckId: any;

  managersData = [
    {
      linkEdit: true
    }
  ]

  removeCancel: any;

  additionalContents: any = {

    show_time: '',
    show_benifits: '',
    show_training: '',
    show_company_policies: '',
    show_birthdays: '',
    birthday_settings: '',
    show_work_anniversary: '',
    work_anniversary_settings: '',
    show_company_links: '',

  }

  site_name: any;
  site_link: any;

  managerLink: any = {

  }

  companyCheckID: any;

  isValid: boolean = false;

  managerEditLink: any;

  public pastDate: any;
  selectIdList: any;

  ngOnInit() {
    this.pastDate = new Date();

    this.companyID = localStorage.getItem("companyId");
    console.log("company ID", this.companyID);
    this.companyID = this.companyID.replace(/^"|"$/g, "");
    console.log("company IDDDD", this.companyID);

    this.userID = localStorage.getItem("employeeId");
    console.log("user id", this.userID);
    this.userID = this.userID.replace(/^"|"$/g, "");

    this.getCompanyAnnouncements();
    this.getManagerData();

  }

  // Author:Suresh M, Date:04-07-19
  // Company Adding
  companyAdd() {
    this.companyAddList.push({ title: '', scheduled_date: '', expiration_date: '', description: '', buttonDisabled: false,});
    console.log("addlist", this.companyAddList);

  }


  // Author:Suresh M, Date:04-07-19
  // CompanyAnnouncements
  getCompanyAnnouncements() {

    this.employeeSelfService.getCompanyList(this.companyID)
      .subscribe((res: any) => {
        console.log("company listt", res);
        this.companyAddList = res.data;
        console.log("companyy listtt", this.companyAddList);
        this.companyAddList.filter(check => {
          check.edit = true;
          check.buttonDisabled = true;
        })
      });
  }

  // Author:Suresh M, Date:04-07-19
  // Company Submit
  companySubmit(list) {

    var employeeData: any = {
      companyId: this.companyID,
      title: list.title,
      description: list.description,
      scheduled_date: list.scheduled_date,
      expiration_date: list.expiration_date
    }

    if (list._id) {
      employeeData._id = list._id

      this.employeeSelfService.companyEditData(employeeData)
        .subscribe((res: any) => {
          console.log("compnay edit", res);
          this.getCompanyAnnouncements();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Update Data", "Successfully Updated data", "success");
          }
        });
    }

    else {

      this.employeeSelfService.AddCompanyData(employeeData)
        .subscribe((res: any) => {
          console.log("adding data", res);
          this.getCompanyAnnouncements();          
          
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Add", "Suceessfully Added", "success");
          }
        }, (err => {
          this.swalService.SweetAlertWithoutConfirmation(err.error.message, '', 'error')
        })

        );

    }

  }

  // Author:Suresh M, Date:04-07-19
  // Company Edit
  companyEdit() {

    this.selectData = this.companyAddList.filter(companyCheck => {
      return companyCheck.isChecked;
    });

    if (this.selectData.length > 1) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one data", "error");
    }
    else if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one", "error");
    }
    else {
      if (this.selectData[0]._id) {
        this.selectData[0].edit = false;
      }

      this.selectData[0].buttonDisabled = false;

    }
  }

  // Author:Suresh M, Date:04-07-19
  // Company Delete
  companyDelete() {

    this.selectData = this.companyAddList.filter(companyCheck => {
      return companyCheck.isChecked;
    });

    console.log("selectttt", this.selectData);


    if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Delete", "Select any data", "error");
    }
    else {
      var deleteData = {
        companyId: this.companyID,
        _ids: this.checkDeleteIds
      }

      this.employeeSelfService.deleteCompanayData(deleteData)
        .subscribe((res: any) => {
          console.log("delete data", res);
          this.getCompanyAnnouncements();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully delete data", "success")
          }
        });
    }

  }

  // Author:Suresh M, Date:04-07-19
  // cancelClick
  cancelClick(i) {

    console.log("removeeeee", i);
    this.companyAddList.splice(i, 1);

  }


  resetClick() {
    console.log("resett datataa");
    this.getCompanyAnnouncements();
  }



  // Author:Suresh M, Date:04-07-19
  // Company check ID
  employeCheck(event, id) {
    console.log("check idd", event, id);
    // this.selectCheckId = id;

    if (event.checked == true) {
      this.checkDeleteIds.push(id);


    }
    console.log("push iddddddd", this.checkDeleteIds);

    if (event.checked == false) {
      var index = this.checkDeleteIds.indexOf(id);
      if (index > -1) {
        this.checkDeleteIds.splice(index, 1);
        this.companyAddList[0].buttonDisabled = true;
        this.companyAddList[0].edit = true;
      }
      console.log("uncheck idd", this.checkDeleteIds);
    }


  }

  // Author:Suresh M, Date:04-07-19
  // Manager data
  getManagerData() {
    this.employeeSelfService.getManagerSettings(this.companyID)
      .subscribe((res: any) => {
        console.log("manager data", res);
        this.managersData = res.data;
        this.additionalContents = this.managersData;
        this.additionalContents.company_links.filter(item => {
          item.linkEdit = true;
        });
      });
  }

  // Author:Suresh M, Date:04-07-19
  // Manager submit
  managerSave() {
    var managerAddData = {
      companyId: this.companyID,

      show_time: this.additionalContents.show_time,
      show_benifits: this.additionalContents.show_benifits,
      show_training: this.additionalContents.show_training,
      show_company_policies: this.additionalContents.show_company_policies,
      show_birthdays: this.additionalContents.show_birthdays,
      birthday_settings: this.additionalContents.birthday_settings,
      show_work_anniversary: this.additionalContents.show_work_anniversary,
      work_anniversary_settings: this.additionalContents.work_anniversary_settings,
      show_company_links: this.additionalContents.show_company_links

    }

    this.employeeSelfService.managerSettingsAdd(managerAddData)
      .subscribe((res: any) => {
        console.log("add settings", res);
        if (res.status == true) {
          this.swalService.SweetAlertWithoutConfirmation("Settings", "Successfully Added Settings", "success");
        }
      });

  }


  // Author:Suresh M, Date:04-07-19
  // company links
  companyLinkAdd() {
    this.managerLink = {
      site_name: '',
      site_link: ''
    }
    if (!this.additionalContents.company_links) {
      this.additionalContents.company_links = []
    }
    this.additionalContents.company_links.push(this.managerLink);

  }



  // Author:Suresh M, Date:04-07-19
  // company link submit
  companyLinksAdd() {
    this.isValid = false;
    if (this.selectData && this.selectData.length && this.selectData[0]._id) {

      var companyEditData: any = {
        companyId: this.companyID,
        site_name: this.managerEditLink.site_name,
        site_link: this.managerEditLink.site_link,
        linkId: this.companyCheckID
      }

      this.employeeSelfService.companyLinkEditData(companyEditData)
        .subscribe((res: any) => {
          console.log("update company links", res);
          this.getManagerData();
          if (res.status == true) {
            this.selectData = [];
            this.swalService.SweetAlertWithoutConfirmation("Updated", "Successfully update the Data", "success");
          }
        });

    }
    else {

      var postData: any = []
      for (var i = 0; i < this.additionalContents.company_links.length; i++) {
        if (!this.additionalContents.company_links[i]._id) {
          postData.push(this.additionalContents.company_links[i]);
        }
      }

      console.log("postt", postData);

      if (postData.length == 0) {
        this.swalService.SweetAlertWithoutConfirmation("Add", "Please add the data", "error");
      } else {
        if (!this.managerLink.site_name) {
          this.isValid = true;
        } else if (!this.managerLink.site_link) {
          this.isValid = true;
        } else {

          this.employeeSelfService.companyLinkAdding(postData)
            .subscribe((res: any) => {
              console.log("company link addd data", res);
              this.additionalContents.company_links = [];
              postData = [];
              this.getManagerData();
              this.isValid = true;
              if (res.status == true) {
                this.swalService.SweetAlertWithoutConfirmation("Company Links", "Successfully Added links", "success");
              }
            });

        }

      }

    }


  }

  // Author:Suresh M, Date:04-07-19
  // company check id
  companyCheck(id, list) {
    console.log("idddd", id);
    this.companyCheckID = id;
    this.managerEditLink = list;
    console.log("managerrrr", this.managerEditLink);
  }



  // Author:Suresh M, Date:04-07-19
  // company link edit
  companyLinkEdit() {

    this.selectData = this.additionalContents.company_links.filter(companyCheck => {
      return companyCheck.isChecked;
    });

    if (this.selectData.length > 1) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select only one", "error");
    }
    else if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one data", "error");
    } else {
      if (this.selectData[0]._id) {
        this.selectData[0].linkEdit = false;
      }
    }
  }



  // Author:Suresh M, Date:04-07-19
  // company link delete
  companyLinksDelete() {

    this.selectData = this.additionalContents.company_links.filter(companyCheck => {
      return companyCheck.isChecked;
    });

    var deleteData = {
      companyId: this.companyID,
      _ids: this.selectData
    }

    if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Delete", "Select any Data", "error");
    }
    else {
      this.employeeSelfService.companyLinksDeleteData(deleteData)
        .subscribe((res: any) => {
          console.log("company links delete", res);
          this.getManagerData();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully delete the data", "success");
          }
        });
    }

  }


  essBackClick() {
    this.router.navigate(['/admin/admin-dashboard/employee-management']);
    window.scroll(0, 0);
  }



}
