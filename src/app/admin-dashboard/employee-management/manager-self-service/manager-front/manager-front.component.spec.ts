import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerFrontComponent } from './manager-front.component';

describe('ManagerFrontComponent', () => {
  let component: ManagerFrontComponent;
  let fixture: ComponentFixture<ManagerFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
