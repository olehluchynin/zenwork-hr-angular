import { Component, OnInit } from '@angular/core';
import { ManagerFrontpageService } from '../../../../services/manager-frontpage.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-manager-front',
  templateUrl: './manager-front.component.html',
  styleUrls: ['./manager-front.component.css']
})
export class ManagerFrontComponent implements OnInit {

  constructor(private managerFrontPage: ManagerFrontpageService, private swalService: SwalAlertService,
    private router:Router) { }

  addList: any = [
    {
      editValue: false,
      disabledButtons: false
    }
  ];

  managerAnnounceID: any = [];
  deleteSelectId:any = [];

  announce: any;
  selectData: any = [];
  description: any;
  scheduled_date: any;
  expiration_date: any;
  checkID: any;
  announceEdit: any;
  additionalData: any = [
    {
      edit: false
    },

  ]

  title: any;
  managerLinks: any;
  isValid: boolean = false;

  additionContents: any = {
    show_team_summary: '',
    show_to_do_list: '',
    show_todays_reports: '',
    show_employment_type: '',
    show_birthdays: '',
    birthday_settings: '',
    show_work_anniversary: '',
    work_anniversary_settings: '',
    show_manager_links: ''
  }

  managerLinkID: any = [];
  managerEditLinks: any;

  site_name: any;
  site_link: any;


  cID: any;
  userID: any;

 pastDate : any;


  ngOnInit() {
    this.pastDate = new Date();

    // this.managerLinks = [
    //   {
    //     site_name: '',
    //     site_link: ''
    //   }
    // ]

    this.cID = localStorage.getItem('companyId')
    console.log("IDDDDDDD", this.cID);
    this.cID = this.cID.replace(/^"|"$/g, "");
    console.log("cin222222222", this.cID);

    this.userID = localStorage.getItem('employeeId')
    console.log("userrr iddd", this.userID);
    this.userID = this.userID.replace(/^"|"$/g, "");
    console.log("cin222222222", this.userID);
    this.getAllAnnouncement();
    this.getAllAdditionalContent();
  }

  // Author:Suresh M, Date:25-06-19
  // All Announcements
  getAllAnnouncement() {
    console.log("Userrrrrrr", this.userID)

    this.managerFrontPage.getAllData(this.cID, this.userID)
      .subscribe((res: any) => {
        console.log("get dataa", res);
        this.addList = res.data;
        console.log("responsivee get data", this.addList);
        this.addList.filter(item => {
          item.editValue = true;
          item.disabledButtons = true;
        })
        console.log("responsivee get data", this.addList);

      });
  }

  // Author:Suresh M, Date:25-06-19
  // add data
  addFields() {
    this.addList.push({ scheduled_date: '', expiration_date: '', description: '', disabledButtons: false });

  }

  // Author:Suresh M, Date:25-06-19
  // Announcements submit
  announceSubmit(announce) {
    console.log("anounce", announce)
    var postData: any = {
      companyId: this.cID,

      description: announce.description,
      title: announce.title,
      expiration_date: announce.expiration_date,
      scheduled_date: announce.scheduled_date
    }

    if (announce._id) {
      postData._id = announce._id

      this.managerFrontPage.editAnnounceData(postData)
        .subscribe((res: any) => {
          console.log("Editt data", res);
          this.getAllAnnouncement();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Successfull Edit Data", "success")
          }
        });
    }
    else {
      this.managerFrontPage.submitData(postData)
        .subscribe((res: any) => {
          console.log("submit dataaa", res);
          this.getAllAnnouncement();

          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Added", "Successfully Added", "success")
          }
        }, (err =>{
            this.swalService.SweetAlertWithoutConfirmation(err.error.message,"","error")
        })
        );
    }

  }

  // Author:Suresh M, Date:25-06-19
  // Edit data
  editFields() {
    this.selectData = this.addList.filter(announceCheck => {
      return announceCheck.isChecked;

    });

    if (this.selectData.length > 1) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select only one data", "error")
    } else if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one", "error")
    } else {
      if (this.selectData[0]._id) {
        this.selectData[0].editValue = false;
        this.selectData[0].disabledButtons = false;

      }

    }


  }

  // Author:Suresh M, Date:25-06-19
  // Delete
  delete() {

    this.selectData = this.addList.filter(announceCheck => {
      return announceCheck.isChecked;
    })

    console.log("selecttt", this.selectData)

    if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Delete", "Select any data", "error")
    } else {

      var deleteData: any = {
        companyId: this.cID,
        userId: this.userID,
        _ids: this.deleteSelectId
      }

      this.managerFrontPage.deleteData(deleteData)
        .subscribe((res: any) => {
          console.log("deletee", res);
          this.getAllAnnouncement();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully Deleted", "success")
          }
          (err) => {

          }
        });
    }

  }

  // Author:Suresh M, Date:25-06-19
  // select id
  changeSelect(announce_id) {
    console.log("Changeeeeeeee", announce_id)
    this.checkID = announce_id;
    console.log("Chnage IDDDDDD", this.checkID)
  }


  // Author:Suresh M, Date:25-06-19
  // cancel data 
  cancelData(i) {
    this.addList.splice(i,1)
    console.log("Cancelll", i)
  }


  resetClick(){
    this.getAllAnnouncement();
  }


  managerSelectId(event,id){
    console.log("managerrr", event,id);
   
    if(event.checked == true){
      this.deleteSelectId.push(id);
    }
    console.log("pushh idd", this.deleteSelectId);

    if(event.checked == false){
      var index = this.deleteSelectId.indexOf(id);
      if(index > -1){
        this.deleteSelectId.splice(index, 1);
        this.addList[0].disabledButtons = true;
        this.addList[0].editValue = true;
        
      }
      console.log("uncheckkkk value", this.deleteSelectId);
    }


  }

  // Author:Suresh M, Date:25-06-19
  // All Additional contents
  getAllAdditionalContent() {
    this.managerFrontPage.getAllAdditionalData(this.cID)
      .subscribe((res: any) => {
        console.log("get Additionals dataa", res);
        this.additionalData = res.data;
        this.additionContents = this.additionalData;
        if (this.additionalData.manager_links) {
          this.additionalData.manager_links.filter(item => {
            item.edit = true;
          });
        }
      });
  }


  // Author:Suresh M, Date:25-06-19
  // additional submit
  additionalSave() {

    var postAdditional: any = {
      companyId: this.cID,

      show_team_summary: this.additionContents.show_team_summary,
      show_to_do_list: this.additionContents.show_to_do_list,
      show_todays_reports: this.additionContents.show_todays_reports,
      show_employment_type: this.additionContents.show_employment_type,
      show_birthdays: this.additionContents.show_birthdays,
      birthday_settings: this.additionContents.birthday_settings,
      show_work_anniversary: this.additionContents.show_work_anniversary,
      work_anniversary_settings: this.additionContents.work_anniversary_settings,
      show_manager_links: this.additionContents.show_manager_links
    }

    this.managerFrontPage.additionalDataSave(postAdditional)
      .subscribe((res: any) => {
        console.log("additional submit", res);
        if (res.status == true) {
          this.swalService.SweetAlertWithoutConfirmation("Successfully Updates Manager Settings", "", "")
        }
      });
  }


  // Author:Suresh M, Date:25-06-19
  // manager links Add
  managerAdd() {
    this.isValid = false;
    this.managerLinks = {
      site_link: '',
      site_name: '',
    }
    if (!this.additionalData.manager_links) {
      this.additionalData.manager_links = []
    }
    this.additionalData.manager_links.push(this.managerLinks);
  }

  // Author:Suresh M, Date:25-06-19
  // manager links submit
  managerSave() {
    this.isValid = true;
    if (this.selectData && this.selectData.length && this.selectData[0]._id) {
      var managerEditData: any = {
        companyId: this.cID,
        site_name: this.managerEditLinks.site_name,
        site_link: this.managerEditLinks.site_link,
        linkId: this.managerLinkID
      }

      this.managerFrontPage.managerLinkEdit(managerEditData)
        .subscribe((res: any) => {
          console.log("manager links edit", res);
          this.getAllAdditionalContent();
          if (res.status == true) {
            this.selectData = []
            this.swalService.SweetAlertWithoutConfirmation("Edited", "Successfully Edit", "success")
          }
        });

    }
    else {

      var postData: any = []
      for (var i = 0; i < this.additionalData.manager_links.length; i++) {
        if (!this.additionalData.manager_links[i]._id) {
          postData.push(this.additionalData.manager_links[i])
        }
      }
      console.log(postData, "sendingdata");

      if (postData.length == 0) {
        this.swalService.SweetAlertWithoutConfirmation("Add", "Please Add data", "error")
      } else {

        if (!this.managerLinks.site_name) {
          this.isValid = true;
        } else if (!this.managerLinks.site_link) {
          this.isValid = true;
        } else {
          this.managerFrontPage.managerLinksAdd(postData)
            .subscribe((res: any) => {
              console.log("managee links add", res);
              this.additionalData.manager_links = [];
              postData = []
              this.getAllAdditionalContent();
              this.isValid = true;
              if (res.status == true) {
                this.swalService.SweetAlertWithoutConfirmation("Added", "Successfully Added", "success")
              }

            });

        }

      }

    }

  }


  // Author:Suresh M, Date:25-06-19
  // manager links edit
  managerEdit() {

    this.selectData = this.additionalData.manager_links.filter(managerCheck => {
      return managerCheck.isChecked;
    });

    if (this.selectData.length > 1) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select only one", "error")
    } else if (this.selectData == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one data", "error")
    }
    else {
      if (this.selectData[0]._id) {
        this.selectData[0].edit = false;
      }
      
    }

  }

  // Author:Suresh M, Date:25-06-19
  // manager id
  managerID(id, links) {
    console.log("Manager idd", id);
    this.managerLinkID = id;
    this.managerEditLinks = links;
    console.log("new manager id", this.managerLinkID);
  }


  // Author:Suresh M, Date:25-06-19
  // manager links delete
  managerDelete() {

    this.selectData = this.additionalData.manager_links.filter(managerCheck => {
      return managerCheck.isChecked;
    });


    if (this.selectData.length == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Delete", "Please select data", "error")
    } else {

      var daleteData = {
        companyId: this.cID,
        userId: this.userID,
        _ids: this.selectData
      }

      this.managerFrontPage.managerLinksDelete(daleteData)
        .subscribe((res: any) => {
          console.log("manager dalete", res);
          this.getAllAdditionalContent();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully Deleted", "success")
          }
        });
    }



  }


  mssBackClick(){
  
    this.router.navigate(['/admin/admin-dashboard/employee-management']);
    window.scroll(0,0);
  } 


}
