import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerSelfServiceComponent } from './manager-self-service.component';

describe('ManagerSelfServiceComponent', () => {
  let component: ManagerSelfServiceComponent;
  let fixture: ComponentFixture<ManagerSelfServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerSelfServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerSelfServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
