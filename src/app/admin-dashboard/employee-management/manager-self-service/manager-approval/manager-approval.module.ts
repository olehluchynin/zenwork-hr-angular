import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerApprovalComponent } from './manager-approval.component';
import { RouterModule,Routes } from '@angular/router';
import { CompensationComponent } from './compensation/compensation.component';
import { JobComponent } from './job/job.component';
import { TimeSheetComponent } from './time-sheet/time-sheet.component';
import { VacationComponent } from './vacation/vacation.component';

import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';


const router: Routes = [
 {path:'', component:ManagerApprovalComponent, children:[
   {path:'',redirectTo:'compensation', pathMatch:'full'},
  {path:'compensation',component:CompensationComponent},
  {path:'job', component:JobComponent},
  {path:'time-sheet', component:TimeSheetComponent},
  {path:'vacation', component:VacationComponent}
 ]}
]


@NgModule({
  declarations: [ManagerApprovalComponent, CompensationComponent, JobComponent, TimeSheetComponent, VacationComponent],
  imports: [
    CommonModule,RouterModule.forChild(router),MatSelectModule,MatSlideToggleModule,MatMenuModule,MatButtonModule,
    MatIconModule,MatDatepickerModule,DragDropModule,MatCheckboxModule,MatAutocompleteModule,ReactiveFormsModule,FormsModule,
    MatPaginatorModule,MatFormFieldModule,MatInputModule
  ]
})
export class ManagerApprovalModule { }
