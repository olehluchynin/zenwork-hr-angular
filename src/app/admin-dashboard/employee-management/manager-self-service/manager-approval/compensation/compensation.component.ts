import { Component, OnInit } from '@angular/core';
import { ManagerApprovalService } from '../../../../../services/manager-approval.service';
import { FormGroup, FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
declare var jQuery: any;



@Component({
  selector: 'app-compensation',
  templateUrl: './compensation.component.html',
  styleUrls: ['./compensation.component.css']
})
export class CompensationComponent implements OnInit {

  constructor(private compensationService: ManagerApprovalService,
    private swalAlertService: SwalAlertService) { }

  timePeriods = [

  ];
  compensationData: any = [

  ];
  compensationDataHistory = []

  approvalStatus: boolean = false;
  Pendingperpage: any;
  PendingPageno: any;
  HistoryPageno: any;
  Historyperpage: any;
  searchFilter: any;
  count: any;
  countHistory: any;
  requestStatus: any;
  searchFilterHistory: any;
  beginDate: any;
  endDate: any;
  popupName: any;
  tempId: any;
  selectedId: any;
  statusReject: any;
  statusApprove: any;
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  supAdminReadonly: boolean = false;

  public serchingForm: FormGroup
  public pendingData: any;
  approveRejectHide: boolean = false;

  // MatPaginator Output
  pageEvent: PageEvent;

  // pageNo: any;
  // perPage: any;
  singlePerson: boolean = false;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [2, 5, 10, 25, 100];

  compensationRes = {
    changeReason: '',
    notes: '',
    compensationRes: '',
    approvalPriority: [

    ],
    requestedFor: {
      personal: {
        name: {
          preferredName: ''
        }
      }
    },
    oldStatus: {
      Pay_frequency: {
        effective_date: "",
        old_value: {
          amount: ''
        }
      },
      payRate: {
        old_value: {
          amount: ''
        },
        effective_date: ""
      },
      payType: {
        old_value: '',
        effective_date: ""
      },
      percentage_change: {
        old_value: '',
        effective_date: ""
      },

    },
    newStatus: {
      Pay_frequency: {
        effective_date: "",
        new_value: {
          amount: ''
        }
      },
      payRate: {
        new_value: {
          amount: ''
        },
        effective_date: ""
      },
      payType: {
        new_value: '',
        effective_date: ""
      },
      percentage_change: {
        new_value: '',
        effective_date: ""
      },

    },

  }
  submissionCompleteDate:any;
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.timePeriods, event.previousIndex, event.currentIndex);
  }

  ngOnInit() {
    this.Pendingperpage = 5;
    this.PendingPageno = 1;
    this.HistoryPageno = 1;
    this.Historyperpage = 5;
    this.requestStatus = ''
    this.beginDate = '';
    this.endDate = '',
      this.searchFilterHistory = '';
    this.searchFilter = '';



    this.getCompensationPendingData();
    this.getCompensationHistory();


  }



  /* Description:  mat-paginatoin in manager pending history list
  author : vipin reddy */
  pageEvents(event: any) {
    console.log(event);
    this.PendingPageno = event.pageIndex + 1;
    this.Pendingperpage = event.pageSize;
    this.getCompensationPendingData();
  }

  /* Description: mat-paginatoin in manager approal and pending history list
  author : vipin reddy */
  pageEventsHistory(event: any) {
    console.log(event);
    this.HistoryPageno = event.pageIndex + 1;
    this.Historyperpage = event.pageSize;
    this.getCompensationHistory();
  }
  /* Description: get manager requests list date filter
author : vipin reddy */

  beginDateChange() {
    console.log(this.beginDate);
    this.beginDate = this.beginDate.toISOString();
    this.getCompensationHistory();

  }

  /* Description: get manager requests list date filter
  author : vipin reddy */

  endDateChange() {
    console.log(this.endDate);
    this.endDate = this.endDate.toISOString();
    this.getCompensationHistory();

  }


  /* Description: manager requests Showall,approal and pending history list active tabs
  author : vipin reddy */

  requestType(data) {
    this.requestStatus = data;
    this.getCompensationHistory()
  }
  /* Description: manager requests list search filter 
author : vipin reddy */
  searchFilterHistoryChange() {
    console.log(this.searchFilterHistory);

    this.getCompensationHistory()
  }
  /* Description: manager requests list single request showing in popup
   author : vipin reddy */
  selectRequest(isChecked, id, mId) {
    console.log(isChecked, id, mId);
    this.compensationData.forEach(element => {
      if (id == element._id) {
        element.isChecked = true;
        this.selectedId = mId;
      }

      else {
        element.isChecked = false;
      }
      console.log(this.selectedId);

    });
  }

  /* Description: get manager requests list s
author : vipin reddy */

  getCompensationHistory() {
    var cId = JSON.parse(localStorage.getItem('companyId'));
    var eId = JSON.parse(localStorage.getItem('employeeId'))
    var type = JSON.parse(localStorage.getItem('type'));
    var siteAccessRoleId = JSON.parse(localStorage.getItem('siteAccessRoleId'))

    var postData = {
      companyId: cId,
      userId: eId,
      request_status: this.requestStatus,
      changeRequestType: 'Salary',
      per_page: this.Historyperpage,
      page_no: this.HistoryPageno,
      from_date: this.beginDate,
      to_date: this.endDate,
      roleId: siteAccessRoleId,
      search_query: this.searchFilterHistory,

    }

    if (postData.userId == null) {
      this.supAdminReadonly = true
    }
    if (type == 'company') {
      delete postData['userId']
      delete postData['roleId']
      this.supAdminReadonly = true

    }
    console.log(postData, 'compensation history');

    this.compensationService.getAllPendingData(postData)
      .subscribe((res: any) => {
        if (res.status == true) {
          console.log("Compensation history", res);
          this.compensationDataHistory = res.data;
          this.countHistory = res.total_count;

          this.compensationDataHistory = this.compensationDataHistory.map(function (el) {
            var o = Object.assign({}, el);
            o.isChecked = false;
            return o;
          })


        }
        console.log(this.compensationDataHistory);

        (err) => {
          console.log(err);

        }
      })
  }

  /* Description: manager requests pending list search filter 
  author : vipin reddy */
  searchfilterChange() {
    console.log(this.searchFilter);
    this.getCompensationPendingData();
  }

  /* Description: get manager requests pending list 
  author : vipin reddy */
  getCompensationPendingData() {
    var cId = JSON.parse(localStorage.getItem('companyId'));
    var eId = JSON.parse(localStorage.getItem('employeeId'));
    var type = JSON.parse(localStorage.getItem('type'));
    var siteAccessRoleId = JSON.parse(localStorage.getItem('siteAccessRoleId'))


    var postData = {
      companyId: cId,
      userId: eId,
      request_status: 'Pending',
      changeRequestType: 'Salary',
      per_page: this.Pendingperpage,
      page_no: this.PendingPageno,
      roleId: siteAccessRoleId,
      search_query: this.searchFilter,

    }
    if (postData.userId == null) {
      this.supAdminReadonly = true
    }
    if (type == 'company') {
      delete postData['userId']
      delete postData['roleId']
      this.supAdminReadonly = true

    }

    this.compensationService.getAllPendingData(postData)
      .subscribe((res: any) => {
        if (res.status == true) {
          console.log("Compensation pending", res);
          this.compensationData = res.data;
          this.count = res.total_count;

          this.compensationData = this.compensationData.map(function (el) {
            var o = Object.assign({}, el);
            o.isChecked = false;
            o.approvalStatus = false;
            return o;
          })


        }
        console.log(this.compensationData);

        (err) => {
          console.log(err);

        }
      })
  };

  /* Description: manager requests approve or reject request
  author : vipin reddy */

  statusChange(data, status) {
    console.log(data);
    if (status == 'approve') {
      this.statusReject = false;
      this.statusApprove = true;
    }
    if (status == 'reject') {
      this.statusApprove = false;
      this.statusReject = true;
    }
    console.log(this.statusReject, this.statusApprove);

  }
  /* Description: manager single request open in popup and approve or reject
  author : vipin reddy */
  compensationEdit() {
    this.approveRejectHide = false;
    this.editCompensation()

  }
  /* Description: manager single request open in popup for view only
  author : vipin reddy */

  showCompensationHistory(mId) {
    this.selectedId = mId;
    jQuery("#myModal2").modal('show');

    this.approveRejectHide = true;
    this.editCompensation()
  }

  /* Description: manager get single request
  author : vipin reddy */

  editCompensation() {
    this.statusApprove = false;
    this.statusReject = false;
    var cId = JSON.parse(localStorage.getItem('companyId'));
    var mId = this.selectedId;
    this.submissionCompleteDate = ""

    this.compensationService.editCompensationPending(cId, mId)
      .subscribe((res: any) => {
        console.log("Edit Compensation pending ", res)
        this.compensationRes = res.data;
        if (this.compensationRes.approvalPriority[3].request_status == 'Approved') {
          this.submissionCompleteDate = this.compensationRes.approvalPriority[3].approval_date;
        }
        console.log(this.submissionCompleteDate);

        this.compensationRes.approvalPriority = res.data.approvalPriority;
        // this.priorityUsers[(res.data[0].reportsTo.priority) - 1]
        // let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
        // console.log(indexManager, this.managerData);

        // let userManager = this.managerData[indexManager]
        // postdata.job = Object.assign(this.jobData, finalObj)

        for (var i = 0; i < this.compensationRes.approvalPriority.length; i++) {

          if (this.compensationRes.approvalPriority[i].personId) {
            this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)] = { name: (this.compensationRes.approvalPriority[i].personId.personal.name.preferredName) }
            if (this.compensationRes.approvalPriority[i].approval_date) {
              this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: (this.compensationRes.approvalPriority[i].approval_date) })
            }
          }
          else {
            if (this.compensationRes.approvalPriority[i].roleId) {
            this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)] = { name: (this.compensationRes.approvalPriority[i].roleId.name) }
            if (this.compensationRes.approvalPriority[i].approval_date) {
              this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: (this.compensationRes.approvalPriority[i].approval_date) })
            }
          }
          else{
            this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)] = { name: ('deleted role') }
            if (this.compensationRes.approvalPriority[i].approval_date) {
              this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: '--' })
            }
          }
          }
          

        }

      })
    // this.approveRejectHide = false;

    console.log(this.timePeriods);

  }

  /* Description: wrok flow approval priority setting
author : vipin reddy */

  approvalPriority(id, approvalStatus, name, mId) {
    console.log(id, approvalStatus, name, mId);
    this.compensationData.forEach(element => {
      if (id == element._id) {
        if (approvalStatus == true)
          this.popupName = name;
        this.tempId = id;
        this.selectedId = mId;
        // element.approvalStatus = true;
        jQuery("#myModal3").modal('show');

      }
      else {
        // element.approvalStatus = false;
      }


    });

  }
  /* Description: manager request approve or reject request sending
author : vipin reddy */
  approvalsending() {
    var cId = JSON.parse(localStorage.getItem('companyId'));
    var eId = JSON.parse(localStorage.getItem('employeeId'))
    var postData = {
      companyId: cId,
      manager_request_id: this.selectedId,
      userId: eId,
      request_status: ''

    }
    if (this.statusReject == true) {
      postData.request_status = 'Rejected'
    }
    if (this.statusApprove == true) {
      postData.request_status = 'Approved'
    }
    this.compensationService.approveOrRejectRequest(postData)
      .subscribe((res: any) => {
        console.log("Edit Compensation request ", res)
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Request Updated", res.message, 'success')
          this.getCompensationHistory();
          this.getCompensationPendingData();
          jQuery("#myModal2").modal('hide');
        }

      },
        (err) => {
          console.log(err);

        })


  }

  /* Description: manager request approving using toogle
   author : vipin reddy */
  taskApproved() {
    this.compensationData.forEach(element => {
      console.log(this.tempId, element._id);

      if (this.tempId == element._id) {
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'))
        var postData = {
          companyId: cId,
          manager_request_id: this.selectedId,
          userId: eId,
          request_status: 'Approved'

        }
        console.log(postData);

        this.compensationService.approveOrRejectRequest(postData)
          .subscribe((res: any) => {
            console.log("Edit Compensation request ", res)
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation("Request Updated", res.message, 'success')
              this.getCompensationHistory();
              this.getCompensationPendingData();
              jQuery("#myModal2").modal('hide');
            }

          },
            (err) => {
              console.log(err);

            })

        element.approvalStatus = true;
      }
    })
    jQuery("#myModal3").modal('hide');


  }
  /* Description: mosal close for toggle selection
 author : vipin reddy */
  modalClose() {
    this.compensationData.forEach(element => {
      if (this.tempId == element._id) {
        element.approvalStatus = false;
      }
    })

    jQuery("#myModal3").modal('hide');
  }

}
