import { Component, OnInit } from '@angular/core';
import { ManagerReportsService } from '../../../../services/manager-reports.service';

@Component({
  selector: 'app-manager-reports',
  templateUrl: './manager-reports.component.html',
  styleUrls: ['./manager-reports.component.css']
})
export class ManagerReportsComponent implements OnInit {

  constructor( private managerRepostService:ManagerReportsService) { }

  ngOnInit() {
  }

}
