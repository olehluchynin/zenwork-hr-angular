import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerselfserviceRouting } from './manager-self-service.routing';
import { ManagerSelfServiceComponent } from './manager-self-service.component';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ManagerSelfService } from '../../../services/manager-self-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManagerReportsComponent } from './manager-reports/manager-reports.component';
import { ManagerRequestComponent } from './manager-request/manager-request.component';
import { ManagerFrontComponent } from './manager-front/manager-front.component';
import { ManagerFrontpageService } from '../../../services/manager-frontpage.service';
import { EmployeeServiceService } from '../../../services/employee-service.service';





@NgModule({
  imports: [
    CommonModule,
    ManagerselfserviceRouting,
    MatSelectModule,
    MatSlideToggleModule,
    TabsModule.forRoot(),MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,DragDropModule,MatCheckboxModule,
    ReactiveFormsModule,FormsModule,MatAutocompleteModule
  ],
  declarations: [ManagerSelfServiceComponent, ManagerReportsComponent, ManagerRequestComponent, ManagerFrontComponent],
  providers:[ManagerSelfService,ManagerFrontpageService,EmployeeServiceService]
})
export class ManagerSelfServiceModule { }
