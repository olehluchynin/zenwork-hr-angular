import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagerSelfServiceComponent } from './manager-self-service.component';
import { ManagerReportsComponent } from './manager-reports/manager-reports.component';
import { ManagerRequestComponent } from './manager-request/manager-request.component';
import { ManagerFrontComponent } from './manager-front/manager-front.component';



const routes: Routes = [
    {path:'',component:ManagerSelfServiceComponent, children:[
        {path:'',redirectTo:'manager-reports',pathMatch:'full'},
        {path:'manager-reports', component:ManagerReportsComponent},
        {path:'manager-request', component:ManagerRequestComponent},
        {path:'manager-front', component:ManagerFrontComponent},
        {path:'manager-approval', loadChildren:'./manager-approval/manager-approval.module#ManagerApprovalModule'}
    ]},
    
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManagerselfserviceRouting { }
