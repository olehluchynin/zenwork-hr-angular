import { Component, OnInit } from '@angular/core';
import { ManagerSelfService } from '../../../../services/manager-self-service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { WorkflowService } from '../../../../services/workflow.service';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { MyInfoService } from '../../../../services/my-info.service';


declare var jQuery: any;

@Component({
  selector: 'app-manager-request',
  templateUrl: './manager-request.component.html',
  styleUrls: ['./manager-request.component.css']
})
export class ManagerRequestComponent implements OnInit {

  timePeriods = [
    'Bronze age',
    'Iron age',
    'Middle ages',
    'Early modern period',
    'Long nineteenth century'
  ];

  public employees: any;
  public empId: any;
  public empStatus: any = {
    job: '',
    jobTitle: '',
    personal: '',
    effectiveDate: '',
    compensation: {
      current_compensation: [
        {
          payType: '',
          effective_date: '',
          payRate: {
            amount: ''
          },
          Pay_frequency: ''
        }
      ]
    }
  };

  public payTypeID: any;

  public payTypeData: any;

  public getStandardData: any;
  public allJobTitleFields = [];
  public allDepartments = [];
  public allLocations = [];
  public jobChangeDetails: any;
  public allJobChanges = [];
  public allPayType = [];
  allPayTypenew= [];
  public allPayFrequency = [];
  public companyId: any;
  public managerId: any;
  public managerData1: any;
  public hrData: any;
  public managerData: any;
  public sysAdminId: any;
  public splRoleData: any;
  public resRolesObj: any;
  public employeeBaseId: any;
  updateJobWorkflow: boolean = false;
  jobWorkflowId: any;
  priorityUsersJob = [];
  employeeData = [];
  userId: any;
  hrPriority: any;
  reportsToPriority: any;
  salaryHrPriority: any;
  salaryReportsPriority: any;
  specialPersonPriority: any;
  specialPersonId: any;
  specialRolePriority: any;
  specialRoleId: any;
  hrLevelId: any;
  reportsToLevelId: any;
  jobHrPersonId: any;
  jobReportsToPersonId: any;
  salaryHrLevelId: any;
  salaryReportsToLevelId: any;
  salarySpecialPersonPriority: any;
  salarySpecialPersonId: any;
  salarySpecialRolePriority: any;
  salarySpecialRoleId: any;
  salaryHrPersonId: any;
  salaryReportsPersonId: any;
  jobSelectedWorkflowId: any;
  salarySelectedWorkflowId: any;

  updateCompensationWorkflow: boolean = false;
  compensationWorkflowId: any;
  priorityUsersCompensation = [];
  disableField: boolean = true;



  newEmpStatus = {
    payType: '',
    effective_date: '',
    payRate: {
      amount: ''
    },
    Pay_frequency: ''


  };
  usersOrder = [
    
  ]
  usersCompensationOrder= [];

  public jobChangeForm: FormGroup;
  public salaryChangeForm: FormGroup;

  pastDate:any;

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.timePeriods, event.previousIndex, event.currentIndex);
  }



  constructor(private managerRequestService: ManagerSelfService,
    private workflowService: WorkflowService,
    private siteAccessRolesService: SiteAccessRolesService,
    private accessLocalStorageService: AccessLocalStorageService,
    private myInfoService: MyInfoService,
  ) { }


  ngOnInit() {

    // Author:Suresh M, Date:28-05-19
    // Job Change request Form  
    this.jobChangeForm = new FormGroup({
      new_value: new FormControl("",[Validators.required]),
      effective_date: new FormControl("",[Validators.required]),
      new_value1: new FormControl("",[Validators.required]),
      effective_date1: new FormControl("",[Validators.required]),
      new_value2: new FormControl("",[Validators.required]),
      effective_date2: new FormControl("",[Validators.required]),
      impactsSalary: new FormControl(""),
      changeReason: new FormControl(""),
      notes: new FormControl(""),
      jobManagerName: new FormControl(""),
      jobHrName: new FormControl(""),
      jobSplRole: new FormControl(""),
      jobEmpName: new FormControl(""),
    });


    // Author:Suresh M, Date:28-05-19
    // Salary Change request Form  
    this.salaryChangeForm = new FormGroup({
      new_value: new FormControl("",),
      effective_date: new FormControl(""),
      new_value1: new FormControl("",[Validators.required]),
      effective_date1: new FormControl("",[Validators.required]),
      new_value2: new FormControl("",[Validators.required]),
      effective_date2: new FormControl("",[Validators.required]),
      new_value3: new FormControl("",[Validators.required]),
      effective_date3: new FormControl("",[Validators.required]),

      old_value: new FormControl(""),
      effective_date4: new FormControl(""),
      old_value1: new FormControl(""),
      effective_date5: new FormControl(""),
      old_value2: new FormControl(""),
      effective_date6: new FormControl(""),
      old_value3: new FormControl(""),
      effective_date7: new FormControl(""),

      changeReason: new FormControl(""),
      notes: new FormControl(""),
      compensationManagerName: new FormControl(""),
      compensationHrName: new FormControl(""),
      compensationSplRole: new FormControl(""),
      compensationEmpName: new FormControl(""),
    });

    // this.employeeList();

    var cID = localStorage.getItem('companyId')
    console.log("cidddddddddd", cID);
    cID = cID.replace(/^"|"$/g, "");
    this.companyId = cID;
    this.userId = this.accessLocalStorageService.get('employeeId');
    console.log("cin222222222", cID);

    this.getStandardFields();
    this.jobChangeReasonFields();
    this.employeeSearch();
    this.getAllBaseRoles();
    this.getManagerRoles();
    this.gettingLevelsHr();
    this.gettingLevelsManager();
    this.sysAdminRoles();
    this.getJobWorkFlow();
    this.getCompensationWorkFlow();


  }
  jobchangePopup() {
    // this.employeeList();

    this.getStandardFields();
    this.jobChangeReasonFields();
    this.employeeSearch();
    this.getAllBaseRoles();
    this.getManagerRoles();
    this.gettingLevelsHr();
    this.gettingLevelsManager();
    this.sysAdminRoles();
    this.getJobWorkFlow();
    this.getCompensationWorkFlow();

  }


  employeeSearch() {
   

    this.siteAccessRolesService.getAllEmployeesUnderManager(this.companyId,this.userId)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;
      },
        (err) => {
          console.log(err);

        })
  }

  getAllBaseRoles() {
    this.siteAccessRolesService.getBaseRoles()
      .subscribe((res: any) => {
        console.log("roles", res);

        this.resRolesObj = res.data;
        for (var i = 0; i < this.resRolesObj.length; i++) {
          console.log(this.resRolesObj, "rolesssss");

          if (this.resRolesObj[i].name == 'Manager') {
            this.managerId = this.resRolesObj[i]._id;
            console.log(this.managerId);
            this.getManagerRoles();
          }
          if (this.resRolesObj[i].name == "System Admin") {
            this.sysAdminId = this.resRolesObj[i]._id;
            this.sysAdminRoles();
          }
          if (this.resRolesObj[i].name == "Employee") {
            this.employeeBaseId = this.resRolesObj[i]._id;

          }


        }

      },
        (err) => {
          console.log(err);

        })
  }
  getManagerRoles() {
    console.log("managerId", this.managerId);
    // this.managerId = "5cbe98a3561562212689f747"
    this.workflowService.getManagerEmployees(this.companyId, this.managerId)
      .subscribe((res: any) => {
        console.log("manager roles", res);
        this.managerData1 = res.data;

      },
        (err) => {
          console.log(err);

        })
  }

  gettingLevelsManager() {
    var cmpnyId = JSON.parse(localStorage.getItem('companyId'))
    this.workflowService.gettingofManagerlevels(cmpnyId)
      .subscribe((res: any) => {
        console.log("roles", res);
        this.managerData = res.data;
      },
        (err) => {
          console.log(err);

        })
  }

  gettingLevelsHr() {
    var cmpnyId = JSON.parse(localStorage.getItem('companyId'))
    this.workflowService.gettingofHrlevels(cmpnyId)
      .subscribe((res: any) => {
        console.log("roles", res);
        this.hrData = res.data;
      },
        (err) => {
          console.log(err);

        })
  }

  sysAdminRoles() {
    console.log("vippip");
    // this.sysAdminId = "5cbe9922561562212689f74a",
      this.workflowService.getManagerEmployees(this.companyId, this.sysAdminId)
        .subscribe((res: any) => {
          console.log("sys admin roles", res);
          this.splRoleData = res.data;
          this.managerData1.forEach(element => {
            this.splRoleData.push(element)
          });

          console.log(this.splRoleData, 'splroledata');

        },
          (err) => {
            console.log(err);

          })

  }

  getJobWorkFlow() {
    this.workflowService.getWorkflowservice(this.companyId, "job")
      .subscribe((res: any) => {
        console.log("get job workflow", res);
        this.jobSelectedWorkflowId = res.data[0]._id;
        this.hrPriority = res.data[0].hr.priority;
        this.hrLevelId = res.data[0].hr.levelId;
        this.reportsToLevelId = res.data[0].reportsTo.levelId;
        this.specialPersonPriority = res.data[0].specialPerson.priority;
        this.specialPersonId = res.data[0].specialPerson.personId;
        this.specialRolePriority = res.data[0].specialRole.priority;
        this.specialRoleId = res.data[0].specialRole.roleId;
        this.reportsToPriority = res.data[0].reportsTo.priority;


        console.log(this.hrPriority, this.reportsToPriority, this.specialRolePriority, this.specialPersonPriority, this.userId);
        if (this.hrPriority) {
          this.workflowService.findCorrespondingPersonId(this.companyId, 'job', this.userId, this.hrPriority).subscribe((res: any) => {
            console.log(res);
            this.jobHrPersonId = res.data;
            console.log(this.jobHrPersonId);

            if (res.resultType == 'user') {
              this.myInfoService.getOtherUser(this.companyId, this.jobHrPersonId).subscribe((res: any) => {
                console.log(res);

                this.usersOrder[(this.hrPriority - 1)] = res.data.personal.name.preferredName;
                this.jobChangeForm.get('jobSplRole').patchValue( res.data.personal.name.preferredName);
                console.log( this.usersOrder);

              }, (err) => {
                console.log(err);

              })
            }

          }, (err) => {
            console.log(err);

          })

        }

        if (this.reportsToPriority) {
          this.workflowService.findCorrespondingPersonId(this.companyId, 'job', this.userId, this.reportsToPriority).subscribe((res: any) => {
            console.log(res);
            this.jobReportsToPersonId = res.data;
            console.log(this.jobReportsToPersonId);

            if (res.resultType == 'user') {
              this.myInfoService.getOtherUser(this.companyId, this.jobReportsToPersonId).subscribe((res: any) => {
                console.log(res);
                // this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)]
                this.usersOrder[(this.reportsToPriority - 1)] = res.data.personal.name.preferredName
                this.jobChangeForm.get('jobSplRole').patchValue(res.data.personal.name.preferredName)
                console.log( this.usersOrder);

              }, (err) => {
                console.log(err);

              })
            }

          }, (err) => {
            console.log(err);

          })

        }
        if (res.data.length >= 1) {
          this.jobWorkflowId = res.data[0]._id;
          this.updateJobWorkflow = true;

          // this.jobChangeForm.get('jobHrName').patchValue(res.data[0].hr.levelId);
          // console.log(this.jobChangeForm.get('jobHrName').value);

          let index = this.hrData.findIndex(userData => userData._id === res.data[0].hr.levelId)
          let user: any = this.hrData[index]
          // user['userType'] = 'hr';
          var name = user.levelName;
          var _id = user._id;
          this.priorityUsersJob[(res.data[0].hr.priority) - 1] = { name, _id, userType: 'hr' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
          console.log("res get of workflow", user);

          // this.jobChangeForm.get('jobManagerName').patchValue(res.data[0].reportsTo.levelId);

          // console.log(this.jobChangeForm.get('jobManagerName').value);

          let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
          let userManager = this.managerData[indexManager]
          // userManager['userType'] = 'manager';
          var name = userManager.levelName;
          var _id = userManager._id;

          this.priorityUsersJob[(res.data[0].reportsTo.priority) - 1] = { name, _id, userType: 'manager' }// temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
          console.log("res get of workflow", userManager);
          // this.jobChangeForm.get('jobEmpName').patchValue(res.data[0].specialPerson.personId);

          // console.log(this.jobChangeForm.get('jobEmpName').value);

          let indexSpecialPerson = this.employeeData.findIndex(userData => userData._id === res.data[0].specialPerson.personId)
          let userSpecialManager = this.employeeData[indexSpecialPerson]
          console.log(userSpecialManager, 'newqqqqqqqqqqqqq1');

          var name = userSpecialManager.personal.name.preferredName;
          this.usersOrder[(this.specialPersonPriority - 1)] = userSpecialManager.personal.name.preferredName
          this.jobChangeForm.get('jobSplRole').patchValue(userSpecialManager.personal.name.preferredName)
          console.log(name);

          var _id = userSpecialManager._id;

          this.priorityUsersJob[(res.data[0].specialPerson.priority) - 1] = { name, _id, userType: 'specialPerson' }
          console.log("res get of workflow", { name, _id, userType: 'specialPerson' });
          // this.jobChangeForm.get('jobSplRole').patchValue(res.data[0].specialRole.roleId);

          // console.log(this.jobChangeForm.get('jobSplRole').value);
          console.log(this.splRoleData);

          let indexSpecialRole = this.splRoleData.findIndex(userData => userData._id === res.data[0].specialRole.roleId)
          console.log("indexx value", indexSpecialRole);

          let userSpecialManagerole: any = this.splRoleData[indexSpecialRole]
          console.log(userSpecialManagerole, "11111");

          var name = userSpecialManagerole.name;
          this.usersOrder[(this.specialRolePriority - 1)] = userSpecialManagerole.name
          this.jobChangeForm.get('jobSplRole').patchValue(userSpecialManagerole.name)
          var _id = userSpecialManagerole._id;

          this.priorityUsersJob[(res.data[0].specialRole.priority) - 1] = { name, _id, userType: 'specialRole' }
          console.log("res get of workflow", { name, _id });
          console.log(this.priorityUsersJob, this.usersOrder);

        }

      },
        (err) => {

          console.log("error", err);

        })

    console.log(this.usersOrder, 'userdorders');
  }



  getCompensationWorkFlow() {
    this.workflowService.getWorkflowservice(this.companyId, "compensation")
      .subscribe((res: any) => {
        console.log("get workflow", res);
        this.salarySelectedWorkflowId = res.data[0]._id;
        this.salaryHrPriority = res.data[0].hr.priority;
        this.salaryHrLevelId = res.data[0].hr.levelId;
        this.salaryReportsToLevelId = res.data[0].reportsTo.levelId;
        this.salarySpecialPersonPriority = res.data[0].specialPerson.priority;
        this.salarySpecialPersonId = res.data[0].specialPerson.personId;
        this.salarySpecialRolePriority = res.data[0].specialRole.priority;
        this.salarySpecialRoleId = res.data[0].specialRole.roleId;
        this.salaryReportsPriority = res.data[0].reportsTo.priority;

        console.log(this.salaryHrPriority, 'vipin')

        if (this.salaryHrPriority) {
          this.workflowService.findCorrespondingPersonId(this.companyId, 'compensation', this.userId, this.salaryHrPriority).subscribe((res: any) => {
            console.log(res);
            this.salaryHrPersonId = res.data;
            if (res.resultType == 'user') {
              this.myInfoService.getOtherUser(this.companyId, this.salaryHrPersonId).subscribe((res: any) => {
                console.log(res);
                this.usersCompensationOrder[(this.salaryHrPriority - 1)] =  res.data.personal.name.preferredName
                this.salaryChangeForm.get('compensationHrName').patchValue(res.data.personal.name.preferredName)
                console.log(this.salaryChangeForm.get('compensationHrName').value);

              }, (err) => {
                console.log(err);

              })
            }

          }, (err) => {
            console.log(err);

          })
        }
        if (this.salaryReportsPriority) {
          this.workflowService.findCorrespondingPersonId(this.companyId, 'compensation', this.userId, this.salaryReportsPriority).subscribe((res: any) => {
            console.log(res);
            this.salaryReportsPersonId = res.data;
            if (res.resultType == 'user') {
              this.myInfoService.getOtherUser(this.companyId, this.salaryReportsPersonId).subscribe((res: any) => {
                console.log(res);
                this.usersCompensationOrder[(this.salaryReportsPriority - 1)]  = res.data.personal.name.preferredName
                this.salaryChangeForm.get('compensationHrName').patchValue(res.data.personal.name.preferredName)
                console.log(this.salaryChangeForm.get('compensationManagerName').value);

              }, (err) => {
                console.log(err);

              })
            }

          }, (err) => {
            console.log(err);

          })
        }

        if (res.data.length >= 1) {
          this.compensationWorkflowId = res.data[0]._id;
          this.updateCompensationWorkflow = true;

          // this.salaryChangeForm.get('compensationHrName').patchValue(res.data[0].hr.levelId);

          let index = this.hrData.findIndex(userData => userData._id === res.data[0].hr.levelId)
          let user = this.hrData[index]
          // user['userType'] = 'hr';
          var name = user.levelName;
          var _id = user._id;
          this.priorityUsersCompensation[(res.data[0].hr.priority) - 1] = { name, _id, userType: 'hr' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
          console.log("res get of workflow", user);
          // this.salaryChangeForm.get('compensationManagerName').patchValue(res.data[0].reportsTo.levelId);

          let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
          let userManager = this.managerData[indexManager]
          // userManager['userType'] = 'manager';
          var name = userManager.levelName;
          var _id = userManager._id;

          this.priorityUsersCompensation[(res.data[0].reportsTo.priority) - 1] = { name, _id, userType: 'manager' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
          console.log("res get of workflow", userManager);
          // this.salaryChangeForm.get('compensationEmpName').patchValue(res.data[0].specialPerson.personId);

          let indexSpecialPerson = this.employeeData.findIndex(userData => userData._id === res.data[0].specialPerson.personId)

          let userSpecialManager = this.employeeData[indexSpecialPerson]
          console.log(userSpecialManager);

          var name = userSpecialManager.personal.name.preferredName;
          this.usersCompensationOrder[(this.salarySpecialPersonPriority - 1)]  = userSpecialManager.personal.name.preferredName
          this.salaryChangeForm.get('compensationHrName').patchValue(userSpecialManager.personal.name.preferredName)
          console.log("name", name);

          var _id = userSpecialManager._id;

          this.priorityUsersCompensation[(res.data[0].specialPerson.priority) - 1] = { name, _id, userType: 'specialPerson' }
          console.log("res get of workflow", { name, _id, userType: 'specialPerson' });
          // this.salaryChangeForm.get('compensationSplRole').patchValue(res.data[0].specialRole.roleId);
          // console.log(this.salaryChangeForm.get('compensationSplRole').value);

          console.log(this.splRoleData);

          let indexSpecialRole = this.splRoleData.findIndex(userData => userData._id === res.data[0].specialRole.roleId)
          console.log("indexx value", indexSpecialRole);

          let userSpecialManagerole: any = this.splRoleData[indexSpecialRole]
          console.log(userSpecialManagerole, "11111");

          var name = userSpecialManagerole.name;
          this.usersCompensationOrder[(this.salarySpecialRolePriority - 1)]  = userSpecialManagerole.name;
          this.salaryChangeForm.get('compensationHrName').patchValue(userSpecialManagerole.name)
          var _id = userSpecialManagerole._id;

          this.priorityUsersCompensation[(res.data[0].specialRole.priority) - 1] = { name, _id, userType: 'specialRole' }
          console.log("res get of workflow", { name, _id });
          console.log(this.priorityUsersJob);
        }
        

      },
        (err) => {
          console.log("error", err);

        })
  }


  // Author:Suresh M, Date:28-05-19
  // Employee List  
  // employeeList() {
  //   var data: any = {
  //     name: ""
  //   };
  //   this.managerRequestService.jobEmployeeList(data)
  //     .subscribe((res: any) => {
  //       console.log("Employeees", res)
  //       this.employees = res.data
  //     });
  // }



  // Author:Suresh M, Date:28-05-19
  // Select Employee ID
  selectEmployee($event) {
    this.newEmpStatus = {
      payType: '',
      effective_date: '',
      payRate: {
        amount: ''
      },
      Pay_frequency: ''


    };
    this.allPayType = [];
    console.log("SelectEmppppp", $event)
    this.empId = $event.value;
    console.log("EmployeeIDDDD", this.empId)
    var cID = JSON.parse(localStorage.getItem('companyId'))
    console.log("Company IDDDDD", cID);
    this.managerRequestService.selectUserId(cID, this.empId)
      .subscribe((res: any) => {
        console.log("Comapny & User", res)
        // if(res.data.)
        this.empStatus = res.data;
        this.pastDate = this.empStatus.personal.effectiveDate;
        console.log("past dateeee", this.pastDate);
        if (this.empStatus.compensation.current_compensation) {
          this.empStatus.compensation.current_compensation.forEach(element => {
            if (element.payType) {
              this.allPayType.push(element)
            }
          });
        }
        console.log(this.allPayType);
      });
    console.log(this.empStatus);
     


  }

  // Author:Suresh M, Date:30-05-19
  // selectPayType Name
  selectPayType($event) {

    console.log("Select Pay typess", $event, this.empStatus);
    this.payTypeID = $event.value;
    this.empStatus.compensation.current_compensation.forEach(element => {
      console.log(element.payType, this.payTypeID);

      if (this.payTypeID == element.payType) {
        this.newEmpStatus = element;

      }
      console.log(this.newEmpStatus, "AAAAAAAAA");
      this.pastDate = this.newEmpStatus.effective_date;
      console.log("salary dateeeee", this.pastDate);

    });


  }

  // Author:Suresh M, Date:28-05-19
  // New Status Fieldss
  getStandardFields() {
    var cID = JSON.parse(localStorage.getItem('companyId'))
    this.managerRequestService.getNewStatusFields(cID)
      .subscribe((res: any) => {
        console.log("New Statusss", res);
        this.getStandardData = res.data;

        if (this.getStandardData['Job Title'].custom.length > 0) {
          this.getStandardData['Job Title'].custom.forEach(element => {
            this.allJobTitleFields.push(element);
          });
        }

        if (this.getStandardData['Job Title'].default.length > 0) {
          this.getStandardData['Job Title'].default.forEach(element => {
            this.allJobTitleFields.push(element);
          });
        }
        console.log("All Jobbbb", this.allJobTitleFields)

        if (this.getStandardData['Department'].custom.length > 0) {
          this.getStandardData['Department'].custom.forEach(element => {
            this.allDepartments.push(element);
          });
        }

        if (this.getStandardData['Department'].default.length > 0) {
          this.getStandardData['Department'].default.forEach(element => {
            this.allDepartments.push(element);
          });
        }

        console.log("All Departt", this.allDepartments)


        if (this.getStandardData['Location'].custom.length > 0) {
          this.getStandardData['Location'].custom.forEach(element => {
            this.allLocations.push(element);
          });
        }

        if (this.getStandardData['Location'].default.length > 0) {
          this.getStandardData['Location'].default.forEach(element => {
            this.allLocations.push(element);
          });
        }

        console.log("All locations", this.allLocations)

        if (this.getStandardData['Pay Type'].default.length > 0) {
          this.getStandardData['Pay Type'].default.forEach(element => {
            this.allPayTypenew.push(element);
          });
        }

        if (this.getStandardData['Pay Type'].custom.length > 0) {
          this.getStandardData['Pay Type'].custom.forEach(element => {
            this.allPayTypenew.push(element);
          });
        }

        console.log("All Pay Typess", this.allPayTypenew)

        if (this.getStandardData['Pay Frequency'].custom.length > 0) {
          this.getStandardData['Pay Frequency'].custom.forEach(element => {
            this.allPayFrequency.push(element);
          });
        }

        if (this.getStandardData['Pay Frequency'].default.length > 0) {
          this.getStandardData['Pay Frequency'].default.forEach(element => {
            this.allPayFrequency.push(element);
          });
        }
        console.log("All Pay Frequency", this.allPayFrequency)



      });
  }

  // Author:Suresh M, Date:28-05-19
  // Job Change Request Submit
  jobChangeSubmit() {
    var cID = JSON.parse(localStorage.getItem('companyId'));

    var newData = {
      companyId: cID,
      changeRequestType: "Job",
      requestedBy: this.userId,
      requestedFor: this.empId,

      newStatus: {

        jobTitle: {
          new_value: this.jobChangeForm.get('new_value').value,
          effective_date: this.jobChangeForm.get('effective_date').value
        },

        department: {
          new_value: this.jobChangeForm.get('new_value1').value,
          effective_date: this.jobChangeForm.get('effective_date1').value,
        },

        location: {
          new_value: this.jobChangeForm.get('new_value2').value,
          effective_date: this.jobChangeForm.get('effective_date2').value,
        },

      },
      oldStatus: {

        jobTitle: {
          old_value: this.empStatus.job.jobTitle,
          effective_date: this.empStatus.personal.effectiveDate
        },

        department: {
          old_value: this.empStatus.job.department,
          effective_date: this.empStatus.personal.effectiveDate,
        },

        location: {
          old_value: this.empStatus.job.location,
          effective_date: this.empStatus.personal.effectiveDate,
        },

      },
      changeReason: this.jobChangeForm.get('changeReason').value,
      notes: this.jobChangeForm.get('notes').value,
      impactsSalary: this.jobChangeForm.get('impactsSalary').value,
      selected_workflow: this.jobSelectedWorkflowId,
      approvalPriority: [
        {
          priority: this.reportsToPriority,
          key: "reportsTo",
          levelId: this.reportsToLevelId,
          personId: this.jobReportsToPersonId
        },
        {
          priority: this.hrPriority,
          key: "hr",
          levelId: this.hrLevelId,
          personId: this.jobHrPersonId
        },
        {
          priority: this.specialPersonPriority,
          key: "specialPerson",
          personId: this.specialPersonId
        },
        {
          priority: this.specialRolePriority,
          key: "specialRole",
          roleId: this.specialRoleId
        }
      ]

    }

    if(this.jobChangeForm.valid){

      this.managerRequestService.jobChangeRequest(newData)
      .subscribe((res: any) => {
        console.log("Job Chnage Request", res)
        this.jobChangeForm.reset();
        this.empId = '';
        jQuery("#myModal").modal("hide");
      })

    } else {
      this.jobChangeForm.get('new_value').markAsTouched();
      this.jobChangeForm.get('new_value1').markAsTouched();
      this.jobChangeForm.get('new_value2').markAsTouched();
      this.jobChangeForm.get('effectiveDate').markAsTouched();
      this.jobChangeForm.get('effectiveDate1').markAsTouched();
      this.jobChangeForm.get('effectiveDate2').markAsTouched();
    }

    

  }


  // Author:Suresh M, Date:28-05-19
  // Job chnage Reasons
  jobChangeReasonFields() {
    var cID = JSON.parse(localStorage.getItem('companyId'));
    this.managerRequestService.getJobChangeReason(cID)
      .subscribe((res: any) => {
        console.log("Change Reasonssss", res)
        this.jobChangeDetails = res.data;

        if (this.jobChangeDetails['Job Change Reason'].custom.length > 0) {
          this.jobChangeDetails['Job Change Reason'].custoom.forEach(element => {
            this.allJobChanges.push(element);
          });
        }

        if (this.jobChangeDetails['Job Change Reason'].default.length > 0) {
          this.jobChangeDetails['Job Change Reason'].default.forEach(element => {
            this.allJobChanges.push(element);
          });
        }

        console.log("All Job Change Reasonss", this.allJobChanges)

      });
  }

  // Author:Suresh M, Date:29-05-19
  // Salary Change request
  salaryChangeSubmit() {
    var cID = JSON.parse(localStorage.getItem('companyId'));
    console.log("Salary Company IDDDD", cID);

    var salaryData = {
      companyId: cID,
      changeRequestType: "Salary",
      requestedBy: cID,
      requestedFor: this.empId,

      newStatus: {

        payType: {
          new_value: this.salaryChangeForm.get('new_value').value,
          effective_date: this.salaryChangeForm.get('effective_date').value
        },

        payRate: {
          new_value: {
            amount: this.salaryChangeForm.get('new_value1').value,
            unit: "USD"
          },
          effective_date: this.salaryChangeForm.get('effective_date1').value
        },


        Pay_frequency: {
          new_value: this.salaryChangeForm.get('new_value2').value,
          effective_date: this.salaryChangeForm.get('effective_date2').value
        },

        percentage_change: {
          new_value: this.salaryChangeForm.get('new_value3').value,
          effective_date: this.salaryChangeForm.get('effective_date3').value
        }

      },

      oldStatus: {

        payType: {
          old_value: this.payTypeID,
          effective_date: this.newEmpStatus.effective_date
        },

        payRate: {
          old_value: {
            amount: this.newEmpStatus.payRate.amount,
            unit: "USD"
          },
          effective_date: this.newEmpStatus.effective_date
        },

        Pay_frequency: {
          old_value: this.newEmpStatus.Pay_frequency,
          effective_date: this.newEmpStatus.effective_date
        },

        percentage_change: {
          old_value: this.salaryChangeForm.get('old_value3').value,
          effective_date: this.newEmpStatus.effective_date
        }

      },

      changeReason: this.salaryChangeForm.get('changeReason').value,
      notes: this.salaryChangeForm.get('notes').value,
      selected_workflow: this.salarySelectedWorkflowId,
      approvalPriority: [
        {
          priority: this.salaryReportsPriority,
          key: "reportsTo",
          levelId: this.salaryReportsToLevelId,
          personId: this.salaryReportsPersonId
        },
        {
          priority: this.salaryHrPriority,
          key: "hr",
          levelId: this.salaryHrLevelId,
          personId: this.salaryHrPersonId
        },
        {
          priority: this.salarySpecialPersonPriority,
          key: "specialPerson",
          personId: this.salarySpecialPersonId
        },
        {
          priority: this.salarySpecialRolePriority,
          key: "specialRole",
          roleId: this.salarySpecialRoleId
        }
      ]
    }
    console.log('newstatus oldstatus', salaryData);

    if(this.salaryChangeForm.valid){
      this.managerRequestService.salaryChangeRequestData(salaryData)
      .subscribe((res: any) => {
        console.log("Salary Changeee", res)
        this.salaryChangeForm.reset();
        jQuery("#myModal1").modal("hide");
      });
    } else {
      this.salaryChangeForm.get('new_value1').markAsTouched();
      this.salaryChangeForm.get('new_value2').markAsTouched();
      this.salaryChangeForm.get('new_value3').markAsTouched();
      this.salaryChangeForm.get('effective_date1').markAsTouched();
      this.salaryChangeForm.get('effective_date2').markAsTouched();
      this.salaryChangeForm.get('effective_date3').markAsTouched();
    }

    
  }


}
