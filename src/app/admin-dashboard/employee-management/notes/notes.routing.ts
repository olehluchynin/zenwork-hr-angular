import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotesComponent } from './notes/notes.component';

const routes: Routes = [
    // { path:'',redirectTo:"/notes",pathMatch:"full" },
    {path:'',component:NotesComponent} 
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})


export class NotesRouting { }