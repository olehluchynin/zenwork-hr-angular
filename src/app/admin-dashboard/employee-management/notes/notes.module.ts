import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesComponent } from './notes/notes.component';
import { NotesRouting } from './notes.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { MyInfoService } from '../../../services/my-info.service';


@NgModule({
  imports: [
    CommonModule,
    NotesRouting,
    FormsModule,
    ReactiveFormsModule,
    MaterialModuleModule
  ],
  declarations: [NotesComponent],
  providers: [MyInfoService]
})
export class NotesModule { }
