import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../../../services/employee.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MyInfoService } from '../../../../services/my-info.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  public employeeNotes = { _id: '', description: "" };
  public employeeData: any;
  public allNotes = [];
  public archivedNotes = [];
  public companyId: any;
  public userId: any;
  public employeeDetails: any;
  public addedBy: any;
  public noteId: any;
  // public archive : boolean = false;
  public archiveDisabled: boolean = true;
  roleType: any;
  type: any;
  pagesAccess = []
  notesTabView: boolean = false;
  constructor(
    private employeeService: EmployeeService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private myInfoService: MyInfoService,
    private router: Router
  ) { }

  ngOnInit() {

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));

    if (this.employeeDetails) {
      this.userId = this.employeeDetails._id;
    }
    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.userId = JSON.parse(localStorage.getItem('employeeId'));
      this.pageAccesslevels();

    }



    console.log(this.userId);

    this.addedBy = this.accessLocalStorageService.get('addedBy');
    console.log(this.addedBy);

    this.getAllNotes();
  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Notes" && element.access == 'view') {
              console.log('data comes in2');

              this.notesTabView = true;
              console.log('loggss', this.notesTabView);

            }

          });

        },
        (err: any) => {

        })


  }

  // Author:Saiprakash G, Date:11-05-19
  // Get all notes for user


  getAllNotes() {

    var data: any = {
      companyId: this.companyId,
      userId: this.userId,
      archive: false

    }

    this.myInfoService.getAllNotes(data)
      .subscribe(
        (notes: any) => {
          console.log(notes);
          this.allNotes = notes.data;
          if (this.allNotes.length > 0) {
            this.archiveDisabled = false;
          }
          // this.allNotes.forEach(element => {
          //   if(element.archive == true){
          //     this.archiveDisabled = false;
          //   }

          // });

        },
        (err: HttpErrorResponse) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Notes", err.error.message, "error");
        }
      )
  }
  // Author:Saiprakash G, Date:11-05-19
  // Save and update notes

  saveNotes() {
    var data: any = {
      companyId: this.companyId,
      userId: this.userId,
      addedBy: this.addedBy,
      note: this.employeeNotes.description
    }


    if (this.noteId) {
      this.myInfoService.editNote(this.noteId, data).subscribe((res: any) => {
        console.log(res);
        this.employeeNotes.description = "";
        this.getAllNotes();
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/training"])
      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }
    else {
      this.myInfoService.addNotes(data)
        .subscribe(
          (notes: any) => {
            console.log(notes);
            this.employeeNotes.description = "";
            this.getAllNotes();
            if (notes.status) {
              this.swalAlertService.SweetAlertWithoutConfirmation(notes.message, "", "success");
              this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/training"])
            } else {
              this.swalAlertService.SweetAlertWithoutConfirmation(notes.message, "", "error");
            }
          },
          (err: HttpErrorResponse) => {
            this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
          }
        )
    }


  }

  // Author:Saiprakash G, Date:11-05-19
  // Get single notes edit

  editNotes(id) {

    this.noteId = id;
    this.myInfoService.getNote(this.companyId, this.userId, id).subscribe((res: any) => {
      console.log(res);
      this.employeeNotes.description = res.data.note;

    }, (err) => {
      console.log(err);

    })
  }

  // Author:Saiprakash G, Date:11-05-19
  // Delete notes 

  deleteNotes(id) {
    this.myInfoService.deleteNote(this.companyId, this.userId, id).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      this.getAllNotes();
    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
    })
  }

  // Author:Saiprakash G, Date:11-05-19
  // Archive notes 
  archiveNotes(id) {
    // this.archiveDisabled = false;
    var data1: any = {
      companyId: this.companyId,
      userId: this.userId,
      archive: true
    }

    this.myInfoService.changeStatus(id, data1).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      this.getAllNotes();
    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

    })
  }

  unArchiveNotes(id) {

    var data2: any = {
      companyId: this.companyId,
      userId: this.userId,
      archive: false
    }

    this.myInfoService.changeStatus(id, data2).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      this.getAllNotes();
    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

    })
  }


  // Author:Saiprakash G, Date:11-05-19
  // View Archived notes 
  viewArchivedNotes() {

    var data: any = {
      companyId: this.companyId,
      userId: this.userId,
      // archive: true,

    }

    this.myInfoService.getAllNotes(data)
      .subscribe(
        (notes: any) => {
          console.log(notes);
          this.allNotes = notes.data;

        },
        (err: HttpErrorResponse) => {
          this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        }
      )
  }

  cancelNotes() {
    this.employeeNotes.description = "";
  }

  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/training"])

  }

}
