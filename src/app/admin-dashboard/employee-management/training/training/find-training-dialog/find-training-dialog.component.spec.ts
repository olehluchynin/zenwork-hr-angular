import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindTrainingDialogComponent } from './find-training-dialog.component';

describe('FindTrainingDialogComponent', () => {
  let component: FindTrainingDialogComponent;
  let fixture: ComponentFixture<FindTrainingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindTrainingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindTrainingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
