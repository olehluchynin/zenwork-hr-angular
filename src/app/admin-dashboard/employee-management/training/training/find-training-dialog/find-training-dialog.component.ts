import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CompanySettingsService } from '../../../../../services/companySettings.service';
import { AccessLocalStorageService } from '../../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
import { element } from '@angular/core/src/render3';


@Component({
  selector: 'app-find-training-dialog',
  templateUrl: './find-training-dialog.component.html',
  styleUrls: ['./find-training-dialog.component.css']
})
export class FindTrainingDialogComponent implements OnInit {

  public trainingSearch: FormGroup;
  public subjectFields: any;
  public trainingSubjectData: any;
  public trainingTypeData: any;
  public trainingAverageTime: any;
  public pageNo: any;
  public perPage: any;
  public count: any;
  public filterName: any;
  public filterTypeName: any;
  public filterTimeName: any;

  trainingData: Array<any> = [
    {
      isChecked: true
    }
  ]
  public selectId: any;
  public companyId: any;
  public userId: any;
  public employeeDetails: any;
  activeTrainings = [];
  roleType: any;
  
  constructor(
    public dialogRef: MatDialogRef<FindTrainingDialogComponent>,
    private companySettingsService: CompanySettingsService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    if (this.employeeDetails) {
      this.userId = this.employeeDetails._id;
    }
    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.userId = JSON.parse(localStorage.getItem('employeeId'));
    }
    console.log(this.userId);
    this.trainingSearch = new FormGroup({
      search: new FormControl("")
    })

    this.pageNo = 1;
    this.perPage = 10;
    this.filterName = '';
    this.filterTypeName = '';
    this.filterTimeName = '';

    this.onChanges();
    this.getAllData();
    this.getSubjectData();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onChanges() {
    this.trainingSearch.controls['search'].valueChanges
      .subscribe(val => {
        this.getAllData();
      });

  }


  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllData();
  }

  //Filter Training Subject
  filter(event) {
    this.filterName = event;
    console.log("Eventttt", event)
    this.getAllData();
  }

  //Filter Training Type
  filterType(event) {
    this.filterTypeName = event;
    this.getAllData();
  }

  //Filter Training Time to Complete
  filterTime(event) {
    this.filterTimeName = event;
    this.getAllData();
  }
  //  Author: Saiprakash, Date: 17/05/19
  //  Get all trainings
  getAllData() {
    console.log("Pagesss", this.pageNo, this.perPage)

    this.companySettingsService.viewTrainingData({
      searchQuery: this.trainingSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage,
      trainingSubject: this.filterName, trainingType: this.filterTypeName, timeToComplete: this.filterTimeName
    })
      .subscribe((res: any) => {
        console.log("Get the Training Data", res)
        this.count = res.count;
        this.trainingData = res.data;

        // this.trainingData.forEach(element => {
        // if(element.status == 1){
        //   this.activeTrainings.push(element)
        // }
        //   element.isChecked = false;
        // });
        // console.log(this.activeTrainings);

      });
  }

  getSubjectData() {
    this.companySettingsService.getAllTraniningData()
      .subscribe((res: any) => {
        console.log("Get All Subjects Dataa", res)
        this.subjectFields = res.temp;
        this.trainingSubjectData = this.subjectFields['Training Subject'],
          this.trainingTypeData = this.subjectFields['Training Type'],
          this.trainingAverageTime = this.subjectFields['Training - Average Time to Complete'],

          console.log("Get All Subjects 21345768", this.subjectFields)
      });
  }
  selctTrainingId(isChecked, id) {
    console.log(isChecked, id);

    this.trainingData.forEach(element => {
      if (element._id !== id) {
        element.isChecked = false;
      }
    });
    this.selectId = id
  }

  //  Author: Saiprakash, Date: 17/05/19
  //  Self Assign training to employees

  assignTraining() {
    var data: any = {
      companyId: this.companyId,
      trainingId: this.selectId,
      _ids: [this.userId],
      origin: "FindTraining"
    }

    this.companySettingsService.assignTrainingToEmployees(data).subscribe((res: any) => {
      console.log(res);
      this.dialogRef.close(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");


    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");


    })

  }
}
