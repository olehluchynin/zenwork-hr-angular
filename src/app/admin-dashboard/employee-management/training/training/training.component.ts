import { Component, OnInit } from '@angular/core';
import { FindTrainingDialogComponent } from './find-training-dialog/find-training-dialog.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MyInfoService } from '../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { TrainingHistoryDialogComponent } from './training-history-dialog/training-history-dialog.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {

  public pageNo: any;
  public perPage: any;
  public count: any;
  public companyId: any;
  public userId: any;
  public employeeDetails: any;
  public allAssignedTrainings = [

  ];
  public assignedTrainings = [];
  public allAssignedHistoryTrainings = [];
  public selectedMarkAsCompleted = [];
  public selectedTrainingHistories = [];
  public completion_date: any;
  public from_date: any;
  public to_date: any;
  public fromDate: any;
  public toDate: any;
  public getSingleTraining: any;
  public searchString: any;
  assignedDate: any;
  checkAssignTrainingIds = [];
  checkHistoryTrainingIds = [];
  // public trainingSearch: FormGroup
  roleType: any;
  pagesAccess = []
  trainingTabView: boolean = false;

  constructor(
    public dialog: MatDialog,
    private myInfoService: MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private router: Router
  ) { }

  ngOnInit() {
    this.pageNo = 1;
    this.perPage = 10;

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));

    if (this.employeeDetails) {
      this.userId = this.employeeDetails._id;
    }
    this.roleType = JSON.parse(localStorage.getItem('type'))

    if (this.roleType != 'company') {
      this.userId = JSON.parse(localStorage.getItem('employeeId'));
      this.pageAccesslevels();

    }
    console.log(this.userId);
    this.listAssignedTrainings();
    this.listAssignedHistoryTrainings();

  }

  pageAccesslevels() {
    console.log("shfjksdh");

    this.myInfoService.getPageACLS()
      .subscribe(
        (res: any) => {
          console.log(res);
          this.pagesAccess = res.data;
          this.pagesAccess.forEach(element => {
            console.log('data comes in1');

            if (element.page == "My Info (EE's) - Training" && element.access == 'view') {
              console.log('data comes in2');

              this.trainingTabView = true;
              console.log('loggss', this.trainingTabView);
            }
          });

        },
        (err: any) => {

        })


  }

  searchFilter() {
    console.log(this.searchString);

    this.listAssignedHistoryTrainings()
  }
  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.listAssignedHistoryTrainings();
  }

  // selectTraining(assignDate){
  //   console.log(assignDate);

  //    this.assignedDate = assignDate;
  // }

  //  Author: Saiprakash, Date: 17/05/19
  //  Find training open dialog

  findTrainingDialog(): void {
    const dialogRef = this.dialog.open(FindTrainingDialogComponent, {
      width: '1300px',
      height: '800px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.listAssignedTrainings();
    });
  }
  //  Author: Saiprakash, Date: 17/05/19
  //  Training history open dialog

  trainingHistoryOpen(): void {

    if (this.getSingleTraining) {
      const dialogRef = this.dialog.open(TrainingHistoryDialogComponent, {
        width: '1000px',
        height: '700px',
        data: this.getSingleTraining
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);

        this.listAssignedHistoryTrainings();
        this.checkHistoryTrainingIds = [];
      });
    }
  }

  //  Author: Saiprakash, Date: 17/05/19
  //  Get all list assigned trainings

  listAssignedTrainings() {
    var data1: any = {
      companyId: this.companyId,
      userId: this.userId,
      onlyActive: 1
    }

    this.myInfoService.listAssignedTrainings(data1).subscribe((res: any) => {
      console.log(res);
      this.assignedTrainings = res.data;
      this.assignedTrainings.forEach(element => {
        if (element.training !== null) {
          console.log(element.training);
          this.allAssignedTrainings = element;
        }
      });


    }, (err) => {
      console.log(err);

    })
  }

  //  Author: Saiprakash, Date: 17/05/19
  //  Mark as completed trainings

  markAsCompleteTraining() {

    this.selectedMarkAsCompleted = this.assignedTrainings.filter(trainingCheck => {
      return trainingCheck.isChecked
    })

    if (this.selectedMarkAsCompleted.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one training to proceed", "", 'error')
    } else if (this.selectedMarkAsCompleted.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select one training to proceed", "", 'error')
    } else {
      var data3: any = {
        companyId: this.companyId,
        userId: this.userId,
        _id: this.selectedMarkAsCompleted[0]._id,
        completion_date: this.completion_date
      }

      this.myInfoService.markAsComplete(data3).subscribe((res: any) => {
        console.log(res);
        this.completion_date = '';
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
        this.checkAssignTrainingIds = [];
        this.listAssignedTrainings();
        this.listAssignedHistoryTrainings();

      }, (err) => {
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })


    }

  }

  onChangeFromValue(value) {
    // console.log('fromDate', value);
    var f = new Date(value)
    this.fromDate = f.toISOString();
    console.log(this.fromDate);
    this.listAssignedHistoryTrainings();


  }
  onChangeToValue(value) {
    console.log('toDate', value);
    var t = new Date(value)
    this.toDate = t.toISOString();
    console.log(this.toDate);
    this.listAssignedHistoryTrainings();

  }
  //  Author: Saiprakash, Date: 17/05/19
  //  Get all list assigned history trainings

  listAssignedHistoryTrainings() {
    var data2: any = {
      companyId: this.companyId,
      userId: this.userId,
      onlyActive: 0,
      per_page: this.perPage,
      page_no: this.pageNo,
      from_date: this.fromDate,
      to_date: this.toDate,
      search_query: this.searchString
    }

    this.myInfoService.listAssignedTrainings(data2).subscribe((res: any) => {
      console.log(res);
      this.allAssignedHistoryTrainings = res.data;
      this.count = res.total_count;

    }, (err) => {
      console.log(err);

    })
  }

  // applyDateRange(){
  //   this.listAssignedHistoryTrainings();
  // }

  //  Author: Saiprakash, Date: 17/05/19
  //  Get single assigned training

  editTraining() {
    this.selectedTrainingHistories = this.allAssignedHistoryTrainings.filter(trainingCheck => {
      return trainingCheck.isChecked
    })

    if (this.selectedTrainingHistories.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select only one training to proceed", "", 'error')
    } else if (this.selectedTrainingHistories.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Please select one training to proceed", "", 'error')
    } else {

      this.myInfoService.getAssignedTraining(this.companyId, this.userId, this.selectedTrainingHistories[0]._id).subscribe((res: any) => {
        console.log(res);
        this.getSingleTraining = res.data;
        this.trainingHistoryOpen();

      }, (err) => {
        console.log(err);

      })


    }

  }
  //  Author: Saiprakash, Date: 17/05/19
  //  Delete assigned trainings

  deleteAssignedTrainings() {
    this.selectedMarkAsCompleted = this.assignedTrainings.filter(trainingCheck => {
      return trainingCheck.isChecked
    })


    var deleteTrainings: any = {
      companyId: this.companyId,
      userId: this.userId,
      _ids: this.selectedMarkAsCompleted

    }

    this.myInfoService.deleteAssignedTrainings(deleteTrainings).subscribe((res: any) => {
      console.log(res);
      this.checkAssignTrainingIds = [];
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      this.listAssignedTrainings();

    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
    })


  }

  //  Author: Saiprakash, Date: 17/05/19
  //  Delete assigned history trainings

  deleteHistoryTraining() {
    this.selectedTrainingHistories = this.allAssignedHistoryTrainings.filter(trainingCheck => {
      return trainingCheck.isChecked
    })

    if (this.selectedTrainingHistories.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Select assigned training to proceed", "", 'error')
    } else {

      var deleteTrainings: any = {
        companyId: this.companyId,
        userId: this.userId,
        _ids: this.selectedTrainingHistories

      }

      this.myInfoService.deleteAssignedTrainings(deleteTrainings).subscribe((res: any) => {
        console.log(res);
        this.checkHistoryTrainingIds = [];
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
        this.listAssignedHistoryTrainings();

      }, (err) => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })


    }
  }

  checkAssignTrainings(event, id) {
    console.log(event, id);

    if (event.checked == true) {
      this.checkAssignTrainingIds.push(id)
    }
    console.log(this.checkAssignTrainingIds);
    if (event.checked == false) {
      var index = this.checkAssignTrainingIds.indexOf(id);
      if (index > -1) {
        this.checkAssignTrainingIds.splice(index, 1);
      }
      console.log(this.checkAssignTrainingIds);
    }
  }
  checkTrainingsHistory(event, id) {
    console.log(event, id);
    if (event.checked == true) {
      this.checkHistoryTrainingIds.push(id)
    }
    console.log(this.checkHistoryTrainingIds);
    if (event.checked == false) {
      var index = this.checkHistoryTrainingIds.indexOf(id);
      if (index > -1) {
        this.checkHistoryTrainingIds.splice(index, 1);
      }
      console.log(this.checkHistoryTrainingIds);
    }
  }

  nextTab() {
    this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/documents"])

  }

}
