import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CompanySettingsService } from '../../../../../services/companySettings.service';
import { MyInfoService } from '../../../../../services/my-info.service';
import { AccessLocalStorageService } from '../../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
import { SiteAccessRolesService } from '../../../../../services/site-access-roles-service.service';


@Component({
  selector: 'app-training-history-dialog',
  templateUrl: './training-history-dialog.component.html',
  styleUrls: ['./training-history-dialog.component.css']
})
export class TrainingHistoryDialogComponent implements OnInit {

  public trainingForm: FormGroup;
  public subjectFields: any;
  public trainingSubjectData: any;
  public trainingTypeData: any;
  public trainingAverageTime: any;
  public id:any;
  public companyId:any;
  public userId:any;
  public employeeDetails:any;
  public disableField: boolean = true;
  public employeeData = [];
  roleType:any;
  
  constructor(
    public dialogRef: MatDialogRef<TrainingHistoryDialogComponent>,
    private trainingService: CompanySettingsService,
    private myInfoService: MyInfoService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private siteAccessRolesService: SiteAccessRolesService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {

    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
    if(this.employeeDetails){
      this.userId = this.employeeDetails._id;
      }
      this.roleType = JSON.parse(localStorage.getItem('type'))
  
      if (this.roleType != 'company') {
        this.userId = JSON.parse(localStorage.getItem('employeeId'));
      }
    console.log(this.userId);
    this.trainingForm = new FormGroup({
      trainingName: new FormControl("", Validators.required),
      trainingId: new FormControl("", Validators.required),
      instructorName: new FormControl("", Validators.required),
      trainingSubject: new FormControl("", Validators.required),
      trainingType: new FormControl("", Validators.required),
      timeToComplete: new FormControl("", Validators.required),
      trainingUrl: new FormControl("", Validators.required),
      score: new FormControl("", Validators.required),
      result: new FormControl("", Validators.required),


    });
    this.getSubjectData();
    this.employeeSearch();
    console.log(this.data);
    this.id = this.data._id;
    this.trainingForm.patchValue(this.data.training);
    this.trainingForm.get('score').patchValue(this.data.score);
    this.trainingForm.get('result').patchValue(this.data.result)

  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  //  Author: Saiprakash, Date: 17/05/19
//  Edit assigned trainings

  editTraining(){
    
     var data:any ={
       companyId:this.companyId,
       userId: this.userId,
       _id:this.id,
       score:this.trainingForm.get('score').value,
       result:this.trainingForm.get('result').value,
     }
     if(this.trainingForm.valid){
      this.myInfoService.editAssignedTraining(data).subscribe((res:any)=>{
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");

        this.dialogRef.close(res)
      }, (err)=>{
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
      })
     }
    
  }

  //  Author: Saiprakash, Date: 17/05/19
//  Get all employees

  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;
        console.log(this.employeeData);
        

      },
        (err) => {
          console.log(err);

        })
  }

  getSubjectData() {
    this.trainingService.getAllTraniningData()
      .subscribe((res: any) => {
        console.log("Get All Subjects Dataa", res)
        this.subjectFields = res.temp;
        this.trainingSubjectData = this.subjectFields['Training Subject'],
          this.trainingTypeData = this.subjectFields['Training Type'],
          this.trainingAverageTime = this.subjectFields['Training - Average Time to Complete'],

          console.log("Get All Subjects 21345768", this.subjectFields)
      });
  }

}
