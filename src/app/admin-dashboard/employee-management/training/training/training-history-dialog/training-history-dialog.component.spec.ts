import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingHistoryDialogComponent } from './training-history-dialog.component';

describe('TrainingHistoryDialogComponent', () => {
  let component: TrainingHistoryDialogComponent;
  let fixture: ComponentFixture<TrainingHistoryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingHistoryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
