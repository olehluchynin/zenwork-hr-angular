import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingComponent } from './training/training.component';
import { TrainingRouting } from './training.routing';
import { MyInfoService } from '../../../services/my-info.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { FindTrainingDialogComponent } from './training/find-training-dialog/find-training-dialog.component';
import { TrainingHistoryDialogComponent } from './training/training-history-dialog/training-history-dialog.component';




@NgModule({
  imports: [
    CommonModule,
    TrainingRouting,
    FormsModule,
    ReactiveFormsModule,
    MaterialModuleModule

  ],
  declarations: [TrainingComponent, FindTrainingDialogComponent, TrainingHistoryDialogComponent],
  providers: [MyInfoService],
  entryComponents: [FindTrainingDialogComponent, TrainingHistoryDialogComponent]
})
export class TrainingModule { }
