import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStandardOnboardingTemplateComponent } from './edit-standard-onboarding-template.component';

describe('EditStandardOnboardingTemplateComponent', () => {
  let component: EditStandardOnboardingTemplateComponent;
  let fixture: ComponentFixture<EditStandardOnboardingTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStandardOnboardingTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStandardOnboardingTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
