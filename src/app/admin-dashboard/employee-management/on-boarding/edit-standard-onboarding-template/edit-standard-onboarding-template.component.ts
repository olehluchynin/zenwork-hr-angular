import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SwalAlertService } from '../../..../../../../services/swalAlert.service';
import { OnboardingService } from '../../../../services/onboarding.service';
import { FormsModule } from '@angular/forms';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';



@Component({
  selector: 'app-edit-standard-onboarding-template',
  templateUrl: './edit-standard-onboarding-template.component.html',
  styleUrls: ['./edit-standard-onboarding-template.component.css']
})
export class EditStandardOnboardingTemplateComponent implements OnInit {
  categoryInput: boolean = false;
  customTemplateAdd: boolean = false;
  editStandardData: boolean = false;
  standardData: boolean = false;
  isValid: boolean = false;
  newCategory = [];
  taskCOmpletionTime = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  employeeData: any;
  empId: any;
  timeTypes = ["Day", "Week", "Month"];
  customOnboard = {
    taskName: '',
    taskAssignee: '',
    assigneeEmail: '',
    taskDesription: '',
    timeToComplete: {
      time: '',
      unit: ''
    },
    category: '',
    // taskCategory:''
  }
  template = {
    "templateName": "Onboarding Template",
    "Onboarding Tasks": {

    }
  }
  templateId: any;
  tempcategory: any;
  tempTaskname: any;
  constructor(public dialogRef: MatDialogRef<EditStandardOnboardingTemplateComponent>,
    private swalAlertService: SwalAlertService,
    private onboardingService: OnboardingService,
    private siteAccessRolesService: SiteAccessRolesService,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
    console.log("viiviiiii hgxhgsvah", this.data)
    if (this.data.data == 'customOnboardTemplate') {
      console.log(this.data.category);
      if (this.data.category != undefined) {
        this.template['Onboarding Tasks'] = this.data.category;
      }
      this.customTemplateAdd = (this.data == 'customOnboardTemplate');

      // this.standardData = !this.standardData;
      if (this.data.id) {
        this.getStandardOnboardingtemplate(this.data.id)
        console.log(this.customTemplateAdd);
      }
    }
    if (this.data.data.templatename == 'standard') {
      this.standardData = !this.standardData;
    }
    if (this.data._id) {
      console.log(this.data._id);

      this.editStandardData = !this.editStandardData;
      this.getStandardOnboardingtemplate(this.data._id)
      // this.template = this.data.template;
      this.tempcategory = this.data.data.category;
      this.tempTaskname = this.data.data.taskName;
    }

    if (this.data._id == undefined && this.data.data != 'customOnboardTemplate') {
      this.editStandardData = !this.editStandardData;
      this.tempcategory = this.data.data.category;
      this.tempTaskname = this.data.data.taskName;
      this.data.data = this.data.data;
      this.template['Onboarding Tasks'] = this.data.template['Onboarding Tasks']
    }
    this.employeeSearch()


  }
  /* Description: getting employees list
     author : vipin reddy */
  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;

      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: data auto complete in email field
   author : vipin reddy */
  empEmail($event, e) {
    console.log($event, "sd");

    if (e == 'data') {
      this.data.data.assigneeEmail = $event;

      // this.data.data.taskAssignee = $event.personal.name.preferredName;

    }
    else (e == 'custom')
    {
      this.customOnboard.assigneeEmail = $event;

    }


  }

  categoryChange(event) {
    console.log("sadas", event, this.data.data, this.tempTaskname, this.template['Onboarding Tasks'][this.tempcategory]);
    console.log(this.tempcategory, this.data.data.category, "2222222222222");

    if (this.tempcategory != event) {
      this.template['Onboarding Tasks'][this.tempcategory].forEach(element => {
        console.log(element, 'taskssss');
        if (element.taskName == this.tempTaskname) {
          console.log(element.taskName, this.tempTaskname, "1dhjhjffdjs`");
          var index = this.template['Onboarding Tasks'][this.tempcategory].indexOf(element)
          console.log("index", index);
          this.template['Onboarding Tasks'][event].push(element)

          if (index > -1) {
            this.template['Onboarding Tasks'][this.tempcategory].splice(index, 1)
            delete this.data.data['isChecked']
          }
          // this.template['Onboarding Tasks'][this.data.data.category].push(element)

        }
      })
    }
    console.log(this.template['Onboarding Tasks'], "vipin");

  }

  /* Description: After editing a popup data(Task) Submition 
    author : vipin reddy */
  editData() {
    console.log(this.data.data.category);

    // this.categoryChange(this.data.data.category)

    this.data.data.isChecked = false;
    if (this.data.data.taskName && this.data.data.taskAssignee && this.data.data.assigneeEmail &&
      this.data.data.taskDesription && this.data.data.timeToComplete.time && this.data.data.timeToComplete.unit &&
      this.data.data.category) {
      // this.template['Onboarding Tasks'][this.data.data.category]['isChecked'] = false;
      // this.template['Onboarding Tasks'][this.data.data] =  Object.assign(this.template['Onboarding Tasks'][this.data.data.category], { isChecked: false })
      console.log(this.template['Onboarding Tasks'][this.data.data.category], this.data.data);


      if (this.template['Onboarding Tasks'][this.data.data.category]) {
        console.log("data come in 1");
        var count1 = 0
        var count = 0
        this.template['Onboarding Tasks'][this.data.data.category].forEach(element => {
          if (count1 < 1 && count < 1) {
            console.log('count inner', this.data.data._id, element._id);

            if (element._id == this.data.data._id) {
              console.log("====1");
              var index = this.template['Onboarding Tasks'][this.data.data.category].indexOf(element)
              console.log("index", index);
              if (index > -1) {
                this.template['Onboarding Tasks'][this.data.data.category].splice(index, 1)
                delete this.data.data['isChecked']
              }

              this.template['Onboarding Tasks'][this.data.data.category].push(this.data.data);
              count1 = count1 + 1
            }
          }
        });

        this.template['Onboarding Tasks'][this.data.data.category].forEach(element => {

          if (count < 1 && count1 < 0) {
            if (element._id != this.data.data._id) {

              console.log("push in else");

              element = [this.data.data];
              count = count + 1
              console.log(count, 'count');

            }
          }
        });

      } else {
        console.log("data come in 2");

        this.template['Onboarding Tasks'][this.data.data.category] = [this.data.data];
      }
      console.log('senfingdata', this.template);
      if (this.data._id != undefined) {
        this.onboardingService.addCustomOnboardTemplate(this.template)
          .subscribe((res: any) => {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", res.message, "success")
            console.log(res, "edit res");

            this.dialogRef.close()
          },
            (err) => {
              console.log(err);
              this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", err.error.message, "error")
            })

      }
      if (this.data._id == undefined) {
        this.dialogRef.close()


      }
    }

  }
  /* Description: To get a data and show in popup
      author : vipin reddy */
  getStandardOnboardingtemplate(id) {
    this.onboardingService.getStandardOnboardingtemplate(id)
      .subscribe((res: any) => {
        console.log(res);
        this.template = res.data;

        this.templateId = res.data._id;
      },
        (err: any) => {
          console.log(err);

        })
  }
  /* Description: to get a task names in ngFor loop
   author : vipin reddy */
  getKeys(data) {
    // console.log(data);
    return Object.keys(data);
  }
  /* Description: adding custom onboarding template
     author : vipin reddy */
  addCustomOnboard() {
    console.log(this.customOnboard, "vipin 4355");

    this.isValid = true;
    if (this.customOnboard.taskName && this.customOnboard.taskAssignee && this.customOnboard.assigneeEmail &&
      this.customOnboard.taskDesription && this.customOnboard.timeToComplete.time &&
      this.customOnboard.timeToComplete.unit && this.customOnboard.category) {
      this.dialogRef.close(this.customOnboard)

    }
    console.log(this.newCategory);

  }


  /* Description: new add category in popup
     author : vipin reddy */

  addCategory() {
    this.categoryInput = !this.categoryInput;
  }

  /* Description: close the popup
     author : vipin reddy */
  cancel() {



    this.dialogRef.close()
  }

}
