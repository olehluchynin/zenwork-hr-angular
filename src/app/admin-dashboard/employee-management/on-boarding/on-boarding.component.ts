import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { Router } from '@angular/router';
import { SwalAlertService } from '../..../../../../services/swalAlert.service';
import { OnboardingService } from '../../../services/onboarding.service';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { WorkflowService } from '../../../services/workflow.service';
import { SiteAccessRolesService } from '../../../services/site-access-roles-service.service';
import { AmazingTimePickerService } from 'amazing-time-picker';

import * as moment from 'moment';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { PayRollService } from '../../../services/pay-roll.service';
import { LeaveManagementService } from '../../../services/leaveManagement.service';

import {
  Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { LoaderService } from '../../../services/loader.service';

declare var jQuery: any;



@Component({
  selector: 'app-on-boarding',
  templateUrl: './on-boarding.component.html',
  styleUrls: ['./on-boarding.component.css']
})
export class OnBoardingComponent implements OnInit {

  @ViewChild('client_info') client_info: ElementRef;
  @ViewChild('scrollTop') scrollTop: ElementRef;

  @ViewChild('job_compensation') job_compensation: ElementRef;
  @ViewChild('module_Settings') module_Settings: ElementRef;
  @ViewChild('view_Onboard') view_Onboard: ElementRef;
  @ViewChild('leave_management') leave_management: ElementRef;

  @ViewChild('client_info1') client_info1: ElementRef;

  @ViewChild('openEdit') openEdit: ElementRef;

  onboardNewHire: boolean = true;
  customNewHire: boolean = true;

  deleteId = [];
  deleteID: any;
  template = {
    "templateName": "Onboarding Template",
    "Onboarding Tasks": {

    }
  }
  templates: any;
  standradTemplateId: any;
  standardTemplate = {
    templateName: ''
  };
  scheduleData = [];
  payrollGroups = []
  constructor(private router: Router,
    private onboardingService: OnboardingService,
    private swalAlertService: SwalAlertService,
    private route: ActivatedRoute,
    private workflowService: WorkflowService,
    private siteAccessRolesService: SiteAccessRolesService,
    private atp: AmazingTimePickerService,
    private payRollService: PayRollService,
    private leaveManagementService: LeaveManagementService,

    private loaderService: LoaderService) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.loader(true);
    }
    if (event instanceof NavigationEnd) {
      this.loaderService.loader(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loaderService.loader(false);
    }
    if (event instanceof NavigationError) {
      this.loaderService.loader(false);
    }
  }

  allTasks = [];
  popupData = {};
  templateId: any;
  id: any;
  custonWizardID: any;

  startTimeDifference: any;

  public business: any;
  public department: any;
  public eeoJob: any;
  public empType: any;
  public flsaCode: any;
  public jobTitle: any;
  public location: any;
  public frequency: any;
  public payType: any;
  public union: any;
  public getModulesTemplates: any;
  public selectedNav: any;
  public tempId: any;
  selectedNewHire: any;
  assignEmployee: any = {
    numberType: ''
  };
  public getAllCustomHR: any;
  public customNewhrEdit: any;
  public roles: any;


  public moduleForm: FormGroup
  public moduleForm1: FormGroup
  public ClientInfo: boolean = false;
  public tab2: boolean = false;
  public tab3: boolean = false;
  public tab4: boolean = false;
  tab6: boolean = false;
  tab8: boolean = false;
  ClientInfo1: boolean = false;
  tab15: boolean = false;
  tab12: boolean = false;
  tab13: boolean = false;
  tab14: boolean = false;

  public eligible_for_benifits: any;
  skip_template_assignment: any;
  workHours: any;
  assign_leave_management_rules: any;
  assign_performance_management: any;
  // assaign_employee_number: any;
  onboarding_template: any;

  firstName: any;
  middleName: any;
  lastName: any;
  preferredName: any;
  dob: any;
  gender: any;
  maritalStatus: any;
  effectiveDate: any;
  veteranStatus: any;
  ssn: any;
  ethnicity: any;
  street1: any;
  street2: any;
  city: any;
  state: any;
  zipcode: any;
  country: any;
  workPhone: any;
  extention: any;
  mobilePhone: any;
  homePhone: any;
  workMail: any;
  personalMail: any;

  employeeId: any;
  employeeType: any;
  hireDate: any;
  businessUnit: any;
  unionFields: any;
  FLSA_Code: any;
  EEO_Job_Category: any;
  Site_AccessRole: any;
  VendorId: any;
  Specific_Workflow_Approval: any;
  HR_Contact: any;
  ReportsTo: any;
  skipReportsto: boolean = false;
  NonpaidPosition: any;
  Pay_group: any;

  Pay_frequency: any;
  First_Pay_Date: any;
  compensationRatio: any;
  Salary_Grade: any;
  grade_band: any;
  payRate: any;
  amount: any;

  wizardName: any;
  jobTitle1: any;
  department1: any;
  location1: any;
  payType1: any;

  HrId: any;
  hrData: any;
  employeeData: any;

  allStructureFieldsData: any;
  allMaritalStatusArray = [];
  allVeteranStatusArray = [];
  ethnicityArray = [];
  employeeTypeArray = [];
  jobTitleArray = [];
  departmentArray = [];
  locationArray = [];
  businessUnitArray = [];
  unionArray = [];
  flsaCodeArray = [];
  eeoJobCategoryArray = [];
  payTypeArray = [];
  payFrequencyArray = [];

  countryArray = [];
  stateArray = [];
  today: any;
  companyID: any;
  workSchedule = [];
  applyWorks: any;

  leaveConditon: boolean = false;

  scheduleTypeEvents: any

  scheduletitle: any;
  standardSchedule: any;
  createEmployee: boolean = false;
  positionScheduleShow: boolean = false;

  selectDayOfWeek: any;
  selectedDays: any = [];
  weekDays: any = [

  ];
  getPositionScheduler: any;
  allSelectedDays = [];
  employeeIds = [];

  isValid: boolean = false;
  positionWorkScheduleId: any;
  standardID: any;
  isNotValid: boolean = false;

  allDays: any = [];
  startTime: any;
  endTimeValue: any;
  salGradeBands: any;
  calculateCompensation: any;
  compensationError: boolean = false;
  jobObj: any;
  compensationObj: any;
  resRolesObj = [];

  public days = [
    { name: 'Monday', isChecked: false },
    { name: 'Tuesday', isChecked: false },
    { name: 'Wednesday', isChecked: false },
    { name: 'Thursday', isChecked: false },
    { name: 'Friday', isChecked: false },
    { name: 'Saturday', isChecked: false },
    { name: 'Sunday', isChecked: false },
  ];

  public positionSchedule: any = {
    scheduleTitle: '',
    scheduleStartDate: '',
    type: 'Employee Specific',
    totalHrsPerDay: '',
    totalHrsPerWeek: '',

  };

  WorkScheduleData: any;
  workScheduleType: any;
  salaryGradeList: any;

  public dobDate: any;
  skipOnboardTemplate: boolean = false;
  ngOnInit() {
    this.dobDate = new Date();
    this.companyID = localStorage.getItem('companyId');
    console.log("iddd111111", this.companyID);
    this.companyID = this.companyID.replace(/^"|"$/g, "");
    console.log("iddd222222222", this.companyID);

    var cId = JSON.parse(localStorage.getItem('companyId'))
    this.getStandardAndCustomFieldsforStructure(cId);
    this.getHrRoles()
    this.getBaseRoles()
    this.selectedNav = "ClientInfo";
    this.selectedNewHire = "ClientInfo1";
    // this.selectedNav="tab2";
    // this.ClientInfo=true;
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);

    });
    // this.id="5cb870f2b494651a2bd98750"
    // this.getStandardOnboardingtemplate(this.id);
    this.getStandardOnBoardTemplate();

    this.getCustomOnboardTemplates();


    this.personalForm = new FormGroup({
      personal: new FormGroup({
        name: new FormGroup({
          firstName: new FormControl("", Validators.required),
          middleName: new FormControl("", Validators.required),
          lastName: new FormControl("", Validators.required),
          preferredName: new FormControl("", Validators.required),
        }),

        dob: new FormControl("", Validators.required),
        gender: new FormControl("", Validators.required),
        maritalStatus: new FormControl("", Validators.required),
        effectiveDate: new FormControl("", Validators.required),
        veteranStatus: new FormControl("", Validators.required),
        ssn: new FormControl("", Validators.required),
        ethnicity: new FormControl("", Validators.required),

        address: new FormGroup({
          street1: new FormControl("", Validators.required),
          street2: new FormControl(""),
          city: new FormControl("", Validators.required),
          state: new FormControl("", Validators.required),
          zipcode: new FormControl("", Validators.required),
          country: new FormControl("", Validators.required),
        }),

        contact: new FormGroup({
          workPhone: new FormControl("", Validators.required),
          extention: new FormControl("", Validators.required),
          mobilePhone: new FormControl("", Validators.required),
          homePhone: new FormControl("", Validators.required),
          workMail: new FormControl("", [Validators.required, Validators.email]),
          personalMail: new FormControl("", [Validators.required, Validators.email]),
        }),
      }),
    });
    this.personalValueChanges()


    // Author:Suresh M, Date:23-04-19
    // Job & Compensation Validation
    this.jobForm = new FormGroup({
      job: new FormGroup({
        employeeId: new FormControl(""),
        employeeType: new FormControl("", Validators.required),
        hireDate: new FormControl("", Validators.required),
        jobTitle: new FormControl("", Validators.required),
        department: new FormControl("", Validators.required),
        location: new FormControl("", Validators.required),
        businessUnit: new FormControl("", Validators.required),
        unionFields: new FormControl("", Validators.required),
        FLSA_Code: new FormControl("", Validators.required),
        EEO_Job_Category: new FormControl(""),
        Site_AccessRole: new FormControl("", Validators.required),
        // UserRole: new FormControl("", Validators.required),
        VendorId: new FormControl("", Validators.required),
        Specific_Workflow_Approval: new FormControl("", Validators.required),
        HR_Contact: new FormControl("", Validators.required),
        ReportsTo: new FormControl("", Validators.required),
        // skipReportsto: new FormControl('',Validators.required)
        workHours: new FormControl('', Validators.required),
      }),
      compensation: new FormGroup({
        NonpaidPosition: new FormControl("", Validators.required),
        Pay_group: new FormControl("", Validators.required),
        Pay_frequency: new FormControl(""),
        First_Pay_Date: new FormControl("", Validators.required),
        compensationRatio: new FormControl(""),
        Salary_Grade: new FormControl(""),
        grade_band: new FormControl(""),

        payRate: new FormGroup({
          amount: new FormControl("", Validators.required),
          payType: new FormControl("", Validators.required),
        })
      }),

    });
    this.compensationValueChanges()

    this.getAllJobData();

    this.moduleForm = new FormGroup({
      module_settings: new FormGroup({
        eligible_for_benifits: new FormControl("", Validators.required),
        assign_leave_management_rules: new FormControl("", Validators.required),
        assign_performance_management: new FormControl("", Validators.required),
        // assaign_employee_number: new FormControl("", Validators.required),
        onboarding_template: new FormControl("", Validators.required),
        skip_template_assignment: new FormControl(''),

      })

    });
    this.moduleValueChanges()


    this.moduleForm1 = new FormGroup({
      module_settings: new FormGroup({
        wizardName: new FormControl("", Validators.required),
        eligible_for_benifits: new FormControl("", Validators.required),
        assign_leave_management_rules: new FormControl("", Validators.required),
        assign_performance_management: new FormControl("", Validators.required),
        // assaign_employee_number: new FormControl("", Validators.required),
        onboarding_template: new FormControl("", Validators.required),

      })

    });

    this.getTemplates();
    this.GetAllCustomData();

    var cID = localStorage.getItem('companyId')
    console.log("cidddddddddd", cID);
    cID = cID.replace(/^"|"$/g, "");
    console.log("cin222222222", cID);
    this.onboardingService.getSiteAccesRole(cID)
      .subscribe((res: any) => {
        console.log("Site Access Roless", res)
        this.roles = res.data;
        for (var i = 0; i < this.resRolesObj.length; i++) {
          console.log("sldkjfkldsj");
          this.roles.push(this.resRolesObj[i])
        }
      });



    this.employeeSearch()

    this.today = new Date().toISOString();

    // this.getAllWorkSchdules();
    this.getPayrollGroup();
    this.getsalrayGrades();
  }




  personalValueChanges() {
    // var mrStatus = this.personalForm.get('personal').get('maritalStatus')
    var eDate = this.personalForm.get('personal').get('effectiveDate')
    this.personalForm.get('personal').get('maritalStatus').valueChanges.subscribe(
      (val: string) => {
        console.log(val);
        if (val === 'Single') {
          eDate.clearValidators();

        }
        else if (val === 'Married' || val === 'Divorced' || val === "Registered Parternship") {
          eDate.setValidators([Validators.required]);
        }
        eDate.updateValueAndValidity();
      })
  }

  moduleValueChanges() {
    var skiptemp = this.moduleForm.get('module_settings').get('onboarding_template')
    this.moduleForm.get('module_settings').get('skip_template_assignment').valueChanges.subscribe(
      (val: boolean) => {
        console.log(val);
        if (val == true) {
          skiptemp.clearValidators();
        }
        else if (val == false) {
          skiptemp.setValidators([Validators.required]);
        }
        skiptemp.updateValueAndValidity();
      })


  }

  compensationValueChanges() {
    var FpayDate = this.jobForm.get('compensation').get('First_Pay_Date')
    var pGroup = this.jobForm.get('compensation').get('Pay_group');
    var pType = this.jobForm.get('compensation').get('payRate').get('payType');
    var pRate = this.jobForm.get('compensation').get('payRate').get('amount');

    this.jobForm.get('compensation').get('NonpaidPosition').valueChanges.subscribe(
      (val) => {
        console.log(val, typeof val);
        if (val == false) {
          FpayDate.setValidators([Validators.required]);
          pGroup.setValidators([Validators.required]);
          pType.setValidators([Validators.required]);
          pRate.setValidators([Validators.required]);
        }
        else {
          FpayDate.clearValidators();
          pGroup.clearValidators();
          pType.clearValidators();
          pRate.clearValidators();
        }
        FpayDate.updateValueAndValidity();
        pGroup.updateValueAndValidity();
        pType.updateValueAndValidity();
        pRate.updateValueAndValidity();
      })
  }



  getStandardAndCustomFieldsforStructure(cID) {
    this.onboardingService.getStructureFields(cID)
      .subscribe((res: any) => {
        console.log("structure fields", res);
        this.allStructureFieldsData = res.data;
        //matital-status
        if (this.allStructureFieldsData['Marital Status'].custom.length > 0) {
          this.allStructureFieldsData['Marital Status'].custom.forEach(element => {
            this.allMaritalStatusArray.push(element)
            console.log("22222111111", this.allMaritalStatusArray);
          });
        }
        if (this.allStructureFieldsData['Marital Status'].default.length > 0) {
          this.allStructureFieldsData['Marital Status'].default.forEach(element => {
            this.allMaritalStatusArray.push(element)
          });
          console.log("22222", this.allMaritalStatusArray);
        }

        if (this.allStructureFieldsData['Veteran Status'].custom.length > 0) {
          this.allStructureFieldsData['Veteran Status'].custom.forEach(element => {
            this.allVeteranStatusArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Veteran Status'].default.length > 0) {
          this.allStructureFieldsData['Veteran Status'].default.forEach(element => {
            this.allVeteranStatusArray.push(element)
          });
          console.log("22222", this.allVeteranStatusArray);
        }
        if (this.allStructureFieldsData['Ethnicity'].custom.length > 0) {
          this.allStructureFieldsData['Ethnicity'].custom.forEach(element => {
            this.ethnicityArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Ethnicity'].default.length > 0) {
          this.allStructureFieldsData['Ethnicity'].default.forEach(element => {
            this.ethnicityArray.push(element)
          });
          console.log("22222ethnicity", this.ethnicityArray);
        }
        if (this.allStructureFieldsData['Employee Type'].custom.length > 0) {
          this.allStructureFieldsData['Employee Type'].custom.forEach(element => {
            this.employeeTypeArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Employee Type'].default.length > 0) {
          this.allStructureFieldsData['Employee Type'].default.forEach(element => {
            this.employeeTypeArray.push(element)
          });
          console.log("22222EMpType", this.employeeTypeArray);
        }
        if (this.allStructureFieldsData['Job Title'].custom.length > 0) {
          this.allStructureFieldsData['Job Title'].custom.forEach(element => {
            this.jobTitleArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Job Title'].default.length > 0) {
          this.allStructureFieldsData['Job Title'].default.forEach(element => {
            this.jobTitleArray.push(element)
          });

        }
        if (this.allStructureFieldsData['Department'].custom.length > 0) {
          this.allStructureFieldsData['Department'].custom.forEach(element => {
            this.departmentArray.push(element)
          });
          console.log("22222", this.jobTitleArray);
        }
        if (this.allStructureFieldsData['Department'].default.length > 0) {
          this.allStructureFieldsData['Department'].default.forEach(element => {
            this.departmentArray.push(element)
          });
          console.log("22222", this.departmentArray);
        }
        if (this.allStructureFieldsData['Location'].custom.length > 0) {
          this.allStructureFieldsData['Location'].custom.forEach(element => {
            this.locationArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Location'].default.length > 0) {
          this.allStructureFieldsData['Location'].default.forEach(element => {
            this.locationArray.push(element)
          });
          console.log("22222", this.locationArray);
        }
        if (this.allStructureFieldsData['Business Unit'].custom.length > 0) {
          this.allStructureFieldsData['Business Unit'].custom.forEach(element => {
            this.businessUnitArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Business Unit'].default.length > 0) {
          this.allStructureFieldsData['Business Unit'].default.forEach(element => {
            this.businessUnitArray.push(element)
          });
          console.log("22222", this.businessUnitArray);
        }
        if (this.allStructureFieldsData['Union'].custom.length > 0) {
          this.allStructureFieldsData['Union'].custom.forEach(element => {
            this.unionArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Union'].default.length > 0) {
          this.allStructureFieldsData['Union'].default.forEach(element => {
            this.unionArray.push(element)
          });
          console.log("22222union", this.unionArray);
        }
        if (this.allStructureFieldsData['FLSA Code'].custom.length > 0) {
          this.allStructureFieldsData['FLSA Code'].custom.forEach(element => {
            this.flsaCodeArray.push(element)
          });
        }
        if (this.allStructureFieldsData['FLSA Code'].default.length > 0) {
          this.allStructureFieldsData['FLSA Code'].default.forEach(element => {
            this.flsaCodeArray.push(element)
          });
          console.log("22222", this.flsaCodeArray);
        }
        if (this.allStructureFieldsData['EEO Job Category'].custom.length > 0) {
          this.allStructureFieldsData['EEO Job Category'].custom.forEach(element => {
            this.eeoJobCategoryArray.push(element)
          });
        }
        if (this.allStructureFieldsData['EEO Job Category'].default.length > 0) {
          this.allStructureFieldsData['EEO Job Category'].default.forEach(element => {
            this.eeoJobCategoryArray.push(element)
          });
          console.log("22222", this.eeoJobCategoryArray);
        }



        if (this.allStructureFieldsData['Country'].custom.length > 0) {
          this.allStructureFieldsData['Country'].custom.forEach(element => {
            this.countryArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Country'].default.length > 0) {
          this.allStructureFieldsData['Country'].default.forEach(element => {
            this.countryArray.push(element)
          });
          console.log("22222country", this.countryArray);
        }

        if (this.allStructureFieldsData['State'].custom.length > 0) {
          this.allStructureFieldsData['State'].custom.forEach(element => {
            this.stateArray.push(element)
          });
        }
        if (this.allStructureFieldsData['State'].default.length > 0) {
          this.allStructureFieldsData['State'].default.forEach(element => {
            this.stateArray.push(element)
          });
          console.log("22222", this.stateArray);
        }


      },
        (err) => {

        })
  }


  /* Description: getting employees list
   author : vipin reddy */
  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;

      },
        (err) => {
          console.log(err);

        })
  }


  /* Description: get only Hr employees
  author : vipin reddy */

  getHrRoles() {
    this.HrId = "5cbe98f8561562212689f748"
    var Cid = JSON.parse(localStorage.getItem('companyId'))
    this.workflowService.getHrRoleEmployeesOnly(Cid, this.HrId)
      .subscribe((res: any) => {
        console.log("HR employees", res);
        this.hrData = res.data;

      },
        (err) => {
          console.log(err);

        })

  }
  /* Description: custom onboard template create
  author : vipin reddy Date:17-04-2019*/

  customTemplate() {
    this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding/custom-onboard-template'])
  }

  /* Description: delete custom template
  author : vipin reddy Date:17-04-2019*/

  deleteTemplate() {
    console.log(this.deleteId);

    this.onboardingService.deleteTemplate(this.deleteId)
      .subscribe((res: any) => {
        console.log("delete", res);
        this.swalAlertService.SweetAlertWithoutConfirmation("Delete", res.message, "success")
        this.getCustomOnboardTemplates();
        this.deleteId = []
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Delete", err.error.message, 'error')
        })

  }
  /* Description: get custom onboard template data
  author : vipin reddy Date:17-04-2019*/
  getCustomOnboardTemplates() {

    this.onboardingService.getAllCustomTemplates()
      .subscribe((res: any) => {
        console.log(res);
        this.templates = res.data;

      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: get standard onboard template data
   author : vipin reddy Date:17-04-2019*/

  getStandardOnBoardTemplate() {
    this.onboardingService.getStandardTemplates()
      .subscribe((res: any) => {
        console.log(res);
        this.standardTemplate = res.data;
        this.standradTemplateId = res.data._id;
      },
        (err) => {
          console.log(err);

        })
  }

  // Author:Suresh M, Date:23-04-19
  // showOptions
  showOptions($event, id) {
    console.log("1222222", $event.checked, id);
    this.custonWizardID = id;

    if ($event.checked == true) {
      this.deleteId.push(id)
    }
    console.log(this.deleteId, "123321");
    if ($event.checked == false) {
      var index = this.deleteId.indexOf(id);
      if (index > -1) {
        this.deleteId.splice(index, 1);
      }
      console.log(this.deleteId);

    }

  }
  /* Description: standard onboarding template open
  author : vipin reddy Date:17-04-2019*/
  templateRedirect() {
    this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding/onboardingTemplate/' + this.standradTemplateId])
  }

  /* Description: custom onboarding template open
  author : vipin reddy Date:17-04-2019*/
  customTemplateRedirect(id) {
    this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding/custom-onboard-template//' + id])
  }


  public personalForm: FormGroup;
  public jobForm: FormGroup


  goToPersonel() {
    console.log('clickkkkk')
    this.client_info.nativeElement.click();
    // this.scrollTop.nativeElement.scrollIntoView();
  }
  goToJob() {
    this.job_compensation.nativeElement.click();
  }

  goToModule() {
    this.module_Settings.nativeElement.click();
  }

  goToView() {
    this.view_Onboard.nativeElement.click();
  }

  goToOnboard() {
    this.leave_management.nativeElement.click();
  }


  goToChoose() {
    console.log('clickkkkk')
    this.client_info1.nativeElement.click();
    // this.scrollTop.nativeElement.scrollIntoView();
  }

  // Author:Suresh M, Date:23-04-19
  // Personal Fields Add Api
  personalFieldSubmit() {
    // this.selectedNav='tab2';
    console.log("personal Fieldsss", this.assignEmployee.numberType, this.personalForm.value)

    if (this.personalForm.valid) {
      if (this.assignEmployee.numberType == 'system Generated') {
        var cId = JSON.parse(localStorage.getItem('companyId'))

        this.onboardingService.preEmployeeIdGeneration(cId)
          .subscribe((res: any) => {
            console.log("Get the job Compensations", res)
            this.jobForm.get('job').get('employeeId').patchValue(res.data.employeeId)
            this.onboardNewHire = false;
          },
            (err) => {
              console.log(err);

            })

      }
      this.selectedNav = "tab2";
    } else {
      this.personalForm.get('personal').get('name').get('firstName').markAsTouched();
      this.personalForm.get('personal').get('name').get('middleName').markAsTouched();
      this.personalForm.get('personal').get('name').get('lastName').markAsTouched();
      this.personalForm.get('personal').get('name').get('preferredName').markAsTouched();
      this.personalForm.get('personal').get('dob').markAsTouched();
      this.personalForm.get('personal').get('gender').markAsTouched();
      this.personalForm.get('personal').get('maritalStatus').markAsTouched();
      // if (this.maritalStatus != 'Single') {
      //   this.personalForm.get('personal').get('effectiveDate').markAsTouched();
      // }
      this.personalForm.get('personal').get('effectiveDate').markAsTouched();
      this.personalForm.get('personal').get('veteranStatus').markAsTouched();
      this.personalForm.get('personal').get('ssn').markAsTouched();
      this.personalForm.get('personal').get('ethnicity').markAsTouched();
      this.personalForm.get('personal').get('address').get('street1').markAsTouched();
      // this.personalForm.get('personal').get('address').get('street2').markAsTouched();
      this.personalForm.get('personal').get('address').get('city').markAsTouched();
      this.personalForm.get('personal').get('address').get('state').markAsTouched();
      this.personalForm.get('personal').get('address').get('zipcode').markAsTouched();
      this.personalForm.get('personal').get('address').get('country').markAsTouched();
      this.personalForm.get('personal').get('contact').get('workPhone').markAsTouched();
      this.personalForm.get('personal').get('contact').get('extention').markAsTouched();
      this.personalForm.get('personal').get('contact').get('mobilePhone').markAsTouched();
      this.personalForm.get('personal').get('contact').get('homePhone').markAsTouched();
      this.personalForm.get('personal').get('contact').get('workMail').markAsTouched();
      this.personalForm.get('personal').get('contact').get('personalMail').markAsTouched();
    }

  }

  // Author:Suresh M, Date:23-04-19
  // Job & Compensation post Api
  jobSubmit() {
    console.log("jobbbbbbbb", this.jobForm.value)

    if (this.jobForm.valid) {
      console.log("111111111111", this.jobForm.valid)
      if(this.skipReportsto == false){
      this.jobObj = this.jobForm.get('job').value;
      }
      this.selectedNav = "tab3";
      console.log(this.jobObj,"jobOBj");
      

    } else {
      console.log("NOT VALID", this.jobForm.get('job').get('employeeId'))

      this.jobForm.get('job').get('employeeId').markAsTouched();
      this.jobForm.get('job').get('employeeType').markAsTouched();
      this.jobForm.get('job').get('hireDate').markAsTouched();
      this.jobForm.get('job').get('jobTitle').markAsTouched();
      this.jobForm.get('job').get('department').markAsTouched();
      this.jobForm.get('job').get('location').markAsTouched();
      this.jobForm.get('job').get('businessUnit').markAsTouched();
      this.jobForm.get('job').get('unionFields').markAsTouched();
      this.jobForm.get('job').get('FLSA_Code').markAsTouched();
      // this.jobForm.get('job').get('EEO_Job_Category').markAsTouched();
      this.jobForm.get('job').get('Site_AccessRole').markAsTouched();
      // this.jobForm.get('job').get('UserRole').markAsTouched();
      this.jobForm.get('job').get('VendorId').markAsTouched();
      this.jobForm.get('job').get('Specific_Workflow_Approval').markAsTouched();
      this.jobForm.get('job').get('HR_Contact').markAsTouched();
      this.jobForm.get('job').get('ReportsTo').markAsTouched();

      this.jobForm.get('compensation').get('NonpaidPosition').markAsTouched();

      this.jobForm.get('compensation').get('Pay_group').markAsTouched();
      // this.jobForm.get('compensation').get('Pay_frequency').markAsTouched();
      this.jobForm.get('compensation').get('First_Pay_Date').markAsTouched();
      // this.jobForm.get('compensation').get('compensationRatio').markAsTouched();
      this.jobForm.get('compensation').get('Salary_Grade').markAsTouched();
      // this.jobForm.get('compensation').get('grade_band').markAsTouched();
      this.jobForm.get('compensation').get('payRate').get('amount').markAsTouched();
      // this.jobForm.get('compensation').get('payRate').get('payType').markAsTouched();
      // this.jobForm.get('compensation').get('workHours').markAsTouched();
    }

  }



  // Author:Suresh M, Date:24-04-19
  // Get the Api in Job & Compensation
  getAllJobData() {
    this.onboardingService.getAllJobDetails()
      .subscribe((res: any) => {
        console.log("Get the job Compensations", res)
        this.business = res.data['Business Unit'];
        this.department = res.data['Department'];
        this.eeoJob = res.data['EEO Job Category'];
        this.empType = res.data['Employee Type'];
        this.flsaCode = res.data['FLSA Code'];
        this.jobTitle = res.data['Job Title'];
        this.location = res.data['Location'];
        this.frequency = res.data['Pay Frequency'];
        this.payType = res.data['Pay Type'];
        this.union = res.data['Union'];
        this.assignEmployee = res['settings']['idNumber'];


      });
    console.log(this.jobTitle);

  }

  // Author:Suresh M, Date:26-04-19
  // Validate in Module Settings
  moduleSubmit() {
    console.log("Modulessssss", this.moduleForm.value)
    if (this.skipOnboardTemplate && this.moduleForm.valid) {
      this.selectedNav = "tab6";
    }
    if (this.moduleForm.valid && !this.skipOnboardTemplate) {
      this.selectedNav = "tab4";
      this.onboardNewHire = false;
    } else {
      this.moduleForm.get('module_settings').get('eligible_for_benifits').markAsTouched();
      this.moduleForm.get('module_settings').get('assign_leave_management_rules').markAsTouched();
      this.moduleForm.get('module_settings').get('assign_performance_management').markAsTouched();
      // this.moduleForm.get('module_settings').get('assaign_employee_number').markAsTouched();
      this.moduleForm.get('module_settings').get('onboarding_template').markAsTouched();


    }

  }


  // Author:Suresh M, Date:30-04-19
  // new hire Hr validation
  chooseStepSubmit() {
    console.log("ChooseSubmitModulessss", this.moduleForm1.value)
    if (this.moduleForm1.valid) {
      this.selectedNewHire = "tab15";
    } else {
      this.moduleForm1.get('module_settings').get('wizardName').markAsTouched();
      this.moduleForm1.get('module_settings').get('eligible_for_benifits').markAsTouched();
      this.moduleForm1.get('module_settings').get('assign_leave_management_rules').markAsTouched();
      this.moduleForm1.get('module_settings').get('assign_performance_management').markAsTouched();
      // this.moduleForm1.get('module_settings').get('assaign_employee_number').markAsTouched();
      this.moduleForm1.get('module_settings').get('onboarding_template').markAsTouched();

    }

  }

  // Author:Suresh M, Date:30-04-19
  // Standard Onboarding Summary All Data  
  standardSummarySubmit() {
    console.log(this.jobObj);

    var postData = {
      personal: this.personalForm.value.personal,
      job: this.jobObj,
      compensation: this.jobForm.value.compensation,
      module_settings: this.moduleForm.value.module_settings,
      position_work_schedule_id: this.positionWorkScheduleId

    }
    console.log("personalllll", postData)

    this.onboardingService.standardNewHireAllData(postData)
      .subscribe((res: any) => {
        console.log("Sandard All Tabssss Dataaa", res)
        if (res.status == true) {
          this.skipReportsto = false;
          this.swalAlertService.SweetAlertWithoutConfirmation("Onboard", "Standard Onboarding Successfully Added", "success")
          jQuery("#myModal").modal("hide");
        } else {
          this.swalAlertService.SweetAlertWithoutConfirmation("Standard Added Successfully", "", "error")
        }
        jQuery("#myModal").modal("hide");
        this.personalForm.reset();
        this.jobForm.reset();
        this.moduleForm.reset();
      },
        (err) => {

          this.swalAlertService.SweetAlertWithoutConfirmation("Standard Added Successfully", err.error.message, "error")
        });

  }



  // Author:Suresh M, Date:30-04-19
  // new hire Hr Proceed Submit
  customSummarySubmit() {
    console.log("New Hire Dataa", this.moduleForm1.value)
    if (this.custonWizardID) {
      this.moduleForm1.value['_id'] = this.custonWizardID;
      console.log("Update iDDDDDDD", this.moduleForm1.value);

      this.onboardingService.newHireHrSummaryReviewData(this.moduleForm1.value)
        .subscribe((res: any) => {
          console.log("New Hire All Tabs Dataaaaaa", res)
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Custom edited successfully", "success")
            jQuery("#myModal1").modal("hide");
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Something wrong!", "Error")
          }
          jQuery("#myModal1").modal("hide");
          this.GetAllCustomData();
          this.moduleForm1.reset();
        });
    } else {
      this.onboardingService.newHireHrSummaryReviewData(this.moduleForm1.value)
        .subscribe((res: any) => {
          console.log("New Hire All Tabs Dataaaaaa", res)
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add", "Custom added successfully", "success")
          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Add", "Something wrong!", "Error")
          }
          jQuery("#myModal1").modal("hide");
          this.GetAllCustomData();
          this.moduleForm1.reset();
        });
    }

  }


  // Author:Suresh M, Date:01-05-19
  // Goto FiledMaintaince Routerlink
  goToFieldMaintains() {
    this.router.navigate(['/admin/admin-dashboard/company-settings/company-setup/field-maintenance']);
    jQuery("#myModal1").modal("hide");
  }


  // Author:Suresh M, Date:01-05-19
  // Get All New Hire hr data
  GetAllCustomData() {
    this.onboardingService.getAllNewHireHRData()
      .subscribe((res: any) => {
        console.log("Get All Custom New hire hrrrrrr", res)
        this.getAllCustomHR = res.data;
      });
  }

  // Author:Suresh M, Date:01-05-19
  // Custom new hire hr Deletee
  customHrDelete() {
    if (this.deleteId.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Delete", "Select any data", "error")
    } else {
      this.onboardingService.customNewHrDelete(this.deleteId)
        .subscribe((res: any) => {
          console.log("Custom Deletee", res)
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete", "Custom deleted successfully", "success")
          } else {

          }
          this.GetAllCustomData();
        });
    }

  }

  // Author:Suresh M, Date:01-05-19
  // Custom new hire hr Edittt
  customEdit() {

    if (this.deleteId.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Select only one", "error")
    }
    else if (this.deleteId.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Select any one", "error")
    }
    else {
      this.openEdit.nativeElement.click();
      this.getCustomeWizardData(this.deleteId)
    }
  }

  openstandpopupedit(id) {
    // this.openEdit.nativeElement.click();
    this.getCustomeWizardData(id)
  }

  getCustomeWizardData(id) {
    this.onboardingService.getCustomNewHireWizard(id)
      .subscribe((res: any) => {
        console.log("Get the Dataaaaaa", res);
        this.customNewhrEdit = res.data.module_settings
        this.moduleForm1.get('module_settings').patchValue(this.customNewhrEdit);
        console.log("patch valueeee", this.moduleForm1.get('module_settings').value, this.customNewhrEdit)
        // this.moduleForm1.get('module_settings').reset();
      });
  }


  // Author:Suresh M, Date:24-04-19
  // Phone number function
  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  // Author:Suresh M, Date:24-04-19
  // Phone number function
  keyPress1(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  keyPress2(event: any) {
    const pattern = /[0-9\+\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }



  // Author:Suresh M, Date:29-04-19
  // Api hit in ID Module Settings
  onboardTemplate($event) {
    console.log("onboardingggg", $event)
    this.tempId = $event.value;
    console.log("temp IDDDD", this.tempId)
    this.onboardingService.selectOnboardTemplate(this.tempId)
      .subscribe((res: any) => {
        console.log("Module Setting IDDDDD", res)
        this.template = res.data;
      });

  }

  // Author:Suresh M, Date:29-04-19
  // view on board procceed
  viewOnboard() {
    this.selectedNav = "tab6";
    this.onboardNewHire = false;
  }

  // Author:Suresh M, Date:02-05-19
  // view on board procceed
  getTemplates() {
    this.onboardingService.getModulesData(this.moduleForm.value)
      .subscribe((res: any) => {
        console.log("Get Templatesss", res)
        this.getModulesTemplates = res.data;
      });
  }

  // Author:Suresh M, Date:02-05-19
  // Site Access Roles in company ID
  accessRoleId($event) {
    console.log("Roless IDDDD", $event)
  }


  // Author:Suresh M, Date:26-04-19
  // Tab active in Standard Wizard

  personalFieldss(event) {
    this.selectedNav = event;
    if (event == "ClientInfo") {
      this.ClientInfo = true;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab8 = false;
    }
    else if (event == 'tab2') {
      this.ClientInfo = false;
      this.tab2 = true;
      this.tab3 = false;
      this.tab4 = false;
      this.tab8 = false;
    }
    else if (event == 'tab3') {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = true;
      this.tab4 = false;
      this.tab8 = false;
    }
    else if (event == 'tab4') {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = true;
      this.tab8 = false;
    }
    else if (event == 'tab6') {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab6 = true;
      this.tab8 = false;
    }
    else if (event == 'tab8') {
      this.ClientInfo = false;
      this.tab2 = false;
      this.tab3 = false;
      this.tab4 = false;
      this.tab8 = true;

      console.log(this.personalForm);

    }

  }

  // Author:Suresh M, Date:26-04-19
  // Choose steps 
  chooseSteps(event) {
    this.selectedNewHire = event;
    if (event == "ClientInfo1") {
      this.ClientInfo1 = true;
      this.tab15 = false;
    }
    else if (event == 'tab15') {
      this.ClientInfo1 = false;
      this.tab15 = true;
    }

  }

  /* Description: standard onboardinng template open
     author : vipin reddy */
  getStandardOnboardingtemplate(id) {
    this.onboardingService.getStandardOnboardingtemplate(id)
      .subscribe((res: any) => {
        console.log(res);
        this.template = res.data;

        this.templateId = res.data._id;
      },
        (err: any) => {
          console.log(err);

        });
  }
  getKeys(data) {
    // console.log(data);
    return Object.keys(data);
  }


  // Author:Suresh M, Date:26-04-19
  // getAllWorkSchdules
  getAllWorkSchdules() {

    this.onboardingService.getAllWorkSchedule({ jobTitle: this.jobTitle1 })
      .subscribe((res: any) => {
        console.log("work schudles", res);
        this.workSchedule = res.data;
        console.log("work schdule 1111111", this.workSchedule);
      });

  }

  // Author:Suresh M, Date:26-04-19
  // applyWorkSchedule
  applyWorkSchedule(event) {
    console.log("applayyy", event);
    this.applyWorks = event.value;
    console.log("eventtsssss", this.applyWorks);
    if (this.applyWorks == true) {
      this.leaveConditon = true;
    } else {
      this.leaveConditon = false;
    }
  }


  // Author:Suresh M, Date:26-04-19
  // scheduleType chnage events
  scheduleType(event) {
    console.log("typeeeeeee", event);
    this.scheduleTypeEvents = event.value;
    if (this.scheduleTypeEvents == "Standard") {
      this.createEmployee = false;
      this.positionScheduleShow = true;
    } else {
      this.createEmployee = true;
      this.positionScheduleShow = false;
    }
    console.log("schedule Typeeeee", this.scheduleTypeEvents);

    // var standardData = {
    //   schedulename: this.scheduletitle,
    //   scheduletype: this.scheduleTypeEvents
    // }

    // if (this.scheduleTypeEvents == 'Standard') {
    //   this.onboardingService.standardScheduleData(standardData)
    //     .subscribe((res: any) => {
    //       console.log("standard dataaa", res);
    //       this.createEmployee = false;
    //       this.standardSchedule = res.result[0].week_applicable
    //       console.log("standardsssss", this.standardSchedule);
    //       this.standardID = res.result[0]._id;
    //       console.log("standardiddd", this.standardID);
    //     });
    // } else {
    //   this.createEmployee = true;
    // }



  }

  getPositionSchedule(id) {
    this.positionWorkScheduleId = id;
    this.leaveManagementService.getPosition(id).subscribe((res: any) => {
      console.log(res);
      this.scheduleData = res.data.weekApplicable;


    }, (err) => {
      console.log(err);

    })


  }


  // Author: Suresh M, Date: 17/06/19
  // Select week days

  selectDay(event, day, isChecked) {

    console.log(event, day);
    this.allDays = event;
    console.log("dayssss offfff", this.allDays);

    this.selectDayOfWeek = day;
    if (event == true) {
      if (this.selectedDays) {
        this.selectedDays.push(day)
      }

      this.weekDays.push({
        day: day,
        shiftStartTime: '',
        shiftEndTime: '',
        // edit: false,
        // isChecked: false
      })
    }
    else if (event == false) {
      for (var i = 0; i < this.selectedDays.length; i++) {
        if (this.selectedDays[i] === day) {
          this.selectedDays.splice(i, 1);
        }

      }

      for (var i = 0; i < this.weekDays.length; i++) {
        if (this.weekDays[i].day === day) {
          this.weekDays.splice(i, 1);
        }

      }

    }

    console.log(this.weekDays);
    console.log("dayssssssssssssssss", this.selectedDays);

  }

  // Author: Suresh M, Date: 17/06/19
  // createWorkPosition submit
  createWorkPosition(createEmployeeForm) {

    var data: any = {
      scheduleTitle: this.positionSchedule.scheduleTitle,
      scheduleStartDate: this.positionSchedule.scheduleStartDate,
      type: this.positionSchedule.type,
      totalHrsPerDay: this.positionSchedule.totalHrsPerDay,
      totalHrsPerWeek: this.positionSchedule.totalHrsPerWeek,
      weekApplicable: this.weekDays,
      daysOfWeek: this.selectedDays,

    }

    if (!this.positionSchedule.scheduleTitle) {
      this.isValid = true;

    } else if (!this.positionSchedule.scheduleStartDate) {
      this.isValid = true;
    } else if (!this.positionSchedule.type) {
      this.isValid = true;
    } else if (!this.positionSchedule.totalHrsPerDay) {
      this.isValid = true;
    } else if (!this.positionSchedule.totalHrsPerWeek) {
      this.isValid = true;
    }
    else {

      this.leaveManagementService.createPosition(data)
        .subscribe((res: any) => {
          console.log("create submit dataaaa", res);
          jQuery("#myModal5").modal("hide");
          createEmployeeForm.resetForm();
          this.positionSchedule = {
            scheduleTitle: '',
            scheduleStartDate: '',
            type: '',
            totalHrsPerDay: '',
            totalHrsPerWeek: '',

          };
          this.selectedDays = [];
          this.isValid = false;
          this.weekDays = [];
          this.positionWorkScheduleId = res.data._id;
          console.log("create idddddd", this.positionWorkScheduleId);
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");

        }, (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });

    }


  }


  // Author: Suresh M, Date: 17/06/19
  // standardProceed
  standardProceed() {
    console.log("RRRRRRRRRRR", this.WorkScheduleData, this.scheduletitle, this.workScheduleType)
    if (this.applyWorks == true) {
      if (!this.WorkScheduleData) {
        this.isNotValid = true;
      }
      // else if (!this.scheduletitle) {
      //   this.isNotValid = true;
      // }
      else if (!this.workScheduleType) {
        this.isNotValid = true;
      } else {
        console.log("wwwwwwwwww", this.selectedNav)
        this.selectedNav = "tab8";
        this.onboardNewHire = false;
      }
    } else {
      console.log("wwwwwwwwww", this.selectedNav)
      this.selectedNav = "tab8";
      this.onboardNewHire = false;
    }


  }

  // Author: Suresh M, Date: 17/06/19
  // totalHoursDay
  totalHoursDay(event) {
    console.log("hoursss", event);


    if (this.selectedDays.length) {
      var num = event;
      var min, hrs;
      this.positionSchedule.totalHrsPerWeek = [];
      this.weekDays.shiftStartTime = [];
      this.weekDays.shiftEndTime = [];

      if (num) {
        min = num.split(':');
      }

      // console.log(min[0], "minutes afcter");
      hrs = min[0] * 60;
      if (min[1] && min[1] !== '') {
        console.log(min, "minutes afcter");

        hrs = parseInt(hrs) + parseInt(min[1]);


      }
      this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * hrs / 60
      this.weekDays.shiftStartTime = [];
    }
    // else if (event.length){
    //   console.log("eventssss", event.length);
    //   this.positionSchedule.total_hrs_week = this.selectedDays.length * hrs / 60;
    // }

    else {
      this.swalAlertService.SweetAlertWithoutConfirmation("", "Select days of the week applicable", "Error")
    }

    console.log(hrs);



  }

  // Author: Suresh M, Date: 17/06/19
  // startTimeDay
  startTimeDay(event, i) {
    console.log("timeeeeee", i);
    this.startTime = event;
    console.log("starttt", this.startTime);
    //  this.weekDays[i].end_time ="";

    console.log("11111111111", this.weekDays);


  }

  // Author: Suresh M, Date: 17/06/19
  // endTimeDays
  endTimeDays(event, i) {
    console.log("endddddd", event);
    this.endTimeValue = event;

    var t0 = this.weekDays[i].shiftStartTime;
    var t1 = event;

    console.log("t00000", t0, t1);

    var diff = this.getTimeDifference(t0, t1);
    console.log("timeeee difffff", diff)
    var d = parseInt(diff.split(':')[0]);
    this.startTimeDifference = d;
    var p = parseInt(this.positionSchedule.totalHrsPerDay)
    console.log(d, typeof d, typeof d)
    console.log(d > p || d < p)
    // this.weekDays[i].end_time=event;
    if (d !== p) {
      console.log(this.weekDays)
      // this.weekDays[i].end_time='';
      console.log(this.weekDays)

      this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Please check the correct time duration ", "error")
    } else {
      this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Verify time duration sucessfully", "success")

    }

  }

  timeStringToMins(s) {
    s = s.split(':');
    s[0] = /m$/i.test(s[1]) && s[0] == 12 ? 0 : s[0];
    return s[0] * 60 + parseInt(s[1]) + (/pm$/i.test(s[1]) ? 720 : 0);
  }

  // Return difference between two times in hh:mm[am/pm] format as hh:mm
  getTimeDifference(t0, t1) {

    // Small helper function to padd single digits
    function z(n) { return (n < 10 ? '0' : '') + n; }

    // Get difference in minutes
    var diff = this.timeStringToMins(t1) - this.timeStringToMins(t0);

    // Format difference as hh:mm and return
    return z(diff / 60 | 0) + ':' + z(diff % 60);
  }


  omit_special_char(event) {
    var k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }



  jobTitleForPayrollData() {

    console.log(this.jobTitle1, this.jobForm.get('job').get('jobTitle').value);
    this.amount = '';
    // this.jobForm.get('compensation').get('amount').patchValue('')

    this.calculateCompensation = ''
    // this.jobForm.get('compensation').get('compensationRatio').patchValue('')
    this.getJobSalarayLinks();
    this.getAllWorkSchdules();

  }

  getJobSalarayLinks() {
    if (this.jobTitle1) {
      this.payRollService.getJobLinks(this.jobTitle1)
        .subscribe((res: any) => {
          console.log(res);
          if (res.data.EeoJobCategoryDetails) {
            this.EEO_Job_Category = res.data.EeoJobCategoryDetails.name
            this.jobForm.get('job').get('EEO_Job_Category').patchValue(res.data.EeoJobCategoryDetails.name)
          }
          if (res.data.salary_grade) {
            this.jobForm.get('compensation').get('Salary_Grade').patchValue(res.data.salary_grade._id)
            this.Salary_Grade = res.data.salary_grade._id;
            this.salarygradelistChange(res.data.salary_grade._id)
          }
          if (!res.data.salary_grade) {
            this.Salary_Grade = ''
            this.jobForm.get('compensation').get('Salary_Grade').patchValue('')
            this.jobForm.get('compensation').get('grade_band').patchValue('')
            this.grade_band = '';
          }
          if (!res.data.EeoJobCategoryDetails) {
            this.EEO_Job_Category = ''
            this.jobForm.get('job').get('EEO_Job_Category').patchValue('')
          }
          // this.jobForm.get('compensation').get('grade_band').patchValue(res.data.grade_band._id)
          console.log(this.EEO_Job_Category, this.jobForm.get('job').get('EEO_Job_Category').value);
        },
          (err) => {
            console.log(err);

          })
    }
  }
  getJobSalarayLinks1() {
    this.payRollService.getJobLinks(this.jobTitle1)
      .subscribe((res: any) => {
        console.log(res, res.data.EeoJobCategoryCode);

        this.jobForm.get('compensation').get('grade_band').patchValue(res.data.grade_band._id)
        this.grade_band = res.data.grade_band._id;

      },
        (err) => {
          console.log(err);

        })
    console.log(this.jobForm.get('compensation').get('grade_band').value, "12121212vipn");

  }

  salarygradelistChange(id) {



    this.payRollService.getSingleSalaryGrade(this.companyID, id)
      .subscribe((res: any) => {
        console.log(res);
        this.salGradeBands = res.data.grades;
      },
        (err) => {
          console.log(err);

        })
    this.getJobSalarayLinks1()
  }

  getPayrollGroup() {
    var postData = {
      companyId: this.companyID,

    }
    this.payRollService.getPayrollGroup(postData)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.payrollGroups = res.data;
          // this.payrollGroupCount = res.total_count;
        }
      },
        (err) => {
          console.log(err);

        })

  }

  payGroupChange() {

    console.log(this.Pay_group);
    if (this.Pay_group) {
      this.payRollService.getSinglePayrollGroup(this.companyID, this.Pay_group)
        .subscribe((res: any) => {
          console.log(res);
          this.Pay_frequency = res.data.pay_frequency
          this.jobForm.get('compensation').get('Pay_frequency').patchValue(res.data.pay_frequency);
        },
          (err) => {

            console.log(err);

          })
    }
  }

  getsalrayGrades() {
    var postData = {
      "companyId": this.companyID,

    }

    this.payRollService.getSalrayGradeList(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.salaryGradeList = res.data

        // this.salaryGradeCount = res.total_count
      },
        (err) => {
          console.log(err);

        })
  }

  payRateChangeforCompensation() {
    this.compensationRatio = ''
    this.jobForm.get('compensation').get('compensationRatio').patchValue('')
    var postData = {
      "payRate": this.amount,
      "payType": this.payType1,
      "pay_frequency": this.Pay_frequency,
      "grade_band": "",
      "workHours": this.workHours
    }
    if (this.grade_band) {
      postData.grade_band = this.grade_band
    }
    if (postData.payRate && postData.payType && postData.pay_frequency) {

      this.payRollService.compensationRationCalculate(postData)
        .subscribe((res: any) => {
          console.log(res);
          if (res.status == true) {
            this.compensationError = false;
            this.calculateCompensation = res.data
            this.jobForm.get('compensation').get('compensationRatio').patchValue(this.calculateCompensation.compensationRatio)
            this.compensationRatio = this.calculateCompensation.compensationRatio

          }
        },
          (err) => {
            console.log(err);
            this.compensationError = true;
            // this.swalAlertService.SweetAlertWithoutConfirmation('Compensation Ratio',err.error.message,'error');
          })
    }
  }
  nonPaidPositionChange() {
    console.log(this.NonpaidPosition);
    this.compensationObj = this.jobForm.get('compensation').value
    if (this.NonpaidPosition == false) {
      this.compensationError = false;
    }
    else {

      // delete this.compensationObj.Salary_Grade 
      // delete this.compensationObj.Pay_group 
      // delete this.compensationObj.Pay_frequency 
      // delete this.compensationObj.compensationRatio 
      // delete this.compensationObj.grade_band 
      // delete this.compensationObj.payRate.amount
      // delete this.compensationObj.payRate.payType 
      // delete this.compensationObj.workHours
      // this.jobForm.get('compensation').get('Pay_group').markAsTouched();
      // this.jobForm.get('compensation').get('Pay_frequency').markAsTouched();
      // this.jobForm.get('compensation').get('First_Pay_Date').markAsTouched();
      // this.jobForm.get('compensation').get('compensationRatio').markAsTouched();
      // this.jobForm.get('compensation').get('Salary_Grade').markAsTouched();
      // this.jobForm.get('compensation').get('grade_band').markAsTouched();
      // this.jobForm.get('compensation').get('payRate').get('amount').markAsTouched();
      // this.jobForm.get('compensation').get('payRate').get('payType').markAsTouched();
      // this.jobForm.get('compensation').get('workHours').markAsTouched();
    }



  }
  getBaseRoles() {
    this.siteAccessRolesService.getBaseRoles()
      .subscribe((res: any) => {
        console.log("roles", res);
        this.resRolesObj = res.data;

      },
        (err) => {
          console.log(err);
        })


  }

  skipTemplate(event) {
    console.log(event);
    this.skipOnboardTemplate = event.checked;
    this.onboarding_template = ''
    if (event.checked == true) {
      this.moduleForm.get('module_settings').get('onboarding_template').patchValue('')
    }

  }

  skipReportsChange() {
    console.log(this.skipReportsto);
    var HContact = this.jobForm.get('job').get('HR_Contact')
    var Reporter = this.jobForm.get('job').get('ReportsTo')
    this.skipReportsto = !this.skipReportsto;
    // this.jobForm.get('job').get('HR_Contact').
    if (this.skipReportsto == true) {
      HContact.clearValidators();
      Reporter.clearValidators();
      HContact.updateValueAndValidity();
      Reporter.updateValueAndValidity();
      this.ReportsTo = '';
      this.HR_Contact = '';
      this.jobObj = this.jobForm.get('job').value
      console.log(this.jobObj);
      delete this.jobObj.HR_Contact;
      delete this.jobObj.ReportsTo;


      console.log(this.jobObj, 'when checked no value');
    }
    if (this.skipReportsto == false) {
      HContact.setValidators([Validators.required]);
      Reporter.setValidators([Validators.required]);
      HContact.updateValueAndValidity();
      Reporter.updateValueAndValidity();
      this.jobObj = this.jobForm.get('job').value
      console.log(this.jobObj);
      this.jobObj.HR_Contact = this.jobForm.get('job').get('HR_Contact').value;
      this.jobObj.ReportsTo = this.jobForm.get('job').get('ReportsTo').value;
      console.log(this.jobObj, 'when unchecked value in');
    }


    // console.log(this.jobObj)

  }


}
