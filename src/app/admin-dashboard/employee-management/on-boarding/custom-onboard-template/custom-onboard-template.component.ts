import { Component, OnInit } from '@angular/core';
import { OnboardingService } from '../../../../services/onboarding.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EditStandardOnboardingTemplateComponent } from '../edit-standard-onboarding-template/edit-standard-onboarding-template.component';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-custom-onboard-template',
  templateUrl: './custom-onboard-template.component.html',
  styleUrls: ['./custom-onboard-template.component.css']
})
export class CustomOnboardTemplateComponent implements OnInit {
  checked: false;
  fieldsName: any;
  allTasks = [];
  catArray: any;
  templateId: any;
  id: any;
  isValid: boolean = false;
  isDisabled: boolean = false;
  addbtn: any;
  popupData = "customOnboardTemplate";

  template = {

    templateName: "",
    templateType: "base",
    "Onboarding Tasks": {
      // "HR Tasks": [
      //   // {
      //     // category: "HR Tasks",
      //     // taskName: "tttt",
      //     // timeToComplete: { time: 1, unit: "Day" },
      //     // _id: "5cb9775328e5a5231db1f8c7"
      //   // }
      // ],
      // "New Hire Forms": [

      // ],
      // "IT Setup": [

      // ],
      // "Manager Tasks": [

      // ]
    }
  };

  constructor(public dialog: MatDialog,
    private swalAlertService: SwalAlertService,
    private onboardingService: OnboardingService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    // this.popupData = 'customOnboardTemplate';
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);

    })
    if (this.id)
      this.getStandardOnboardingtemplate(this.id);

  }


  /* Description: to get a custom onboarding template data
   author : vipin reddy */
  getStandardOnboardingtemplate(id) {
    this.onboardingService.getStandardOnboardingtemplate(id)
      .subscribe((res: any) => {
        console.log(res);
        this.template = res.data;

        this.templateId = res.data._id;
      },
        (err: any) => {
          console.log(err);

        })
  }

  /* Description: to show task names in ngfor 
   author : vipin reddy */
  getKeys(data) {
    // console.log(data);
    return Object.keys(data);
  }

  /* Description: select only one task
   author : vipin reddy */
  singleTaskChoose(taskArray, taskName, isChecked?) {
    console.log(taskArray, taskName, isChecked);
    if (isChecked == false) {
      this.popupData = ''
      this.isDisabled = false;
    }
    if (isChecked == true) {
      for (var task in this.template["Onboarding Tasks"]) {
        console.log(task);
        this.template["Onboarding Tasks"][task].forEach(element => {
          console.log(element);

          if (element.taskName === taskName) {
            element.isChecked = true;
            this.popupData = element;
            this.addbtn = element;
            this.isDisabled = true;
            console.log("popdaata", this.popupData);

          } else {
            element.isChecked = false;
          }
        });
      }
    }
    this.template["Onboarding Tasks"][taskArray]


  }



  /* Description: open a popup for custom template add/delete
   author : vipin reddy */
  openDialog(field): void {
    // var data = this.popupData
    console.log(field);

    if (field == 'add') {
      this.popupData = 'customOnboardTemplate';

      var data = {
        id: this.templateId,
        data: this.popupData,
        category: this.catArray
      }
      if (data.data != '') {
        const dialogRef = this.dialog.open(EditStandardOnboardingTemplateComponent, {
          width: '1300px',
          backdropClass: 'structure-model',
          data: data,

        });

        dialogRef.afterClosed().subscribe((result: any) => {
          console.log("resultttt", result);
          if (this.template['Onboarding Tasks'][result.category]) {
            this.template['Onboarding Tasks'][result.category].push(result);
          } else {
            this.template['Onboarding Tasks'][result.category] = [result];
          }
          console.log(this.template, "12121");
          this.catArray = this.template['Onboarding Tasks']

          // for(var i=0)
          // if(result.category == )

          console.log('The dialog was closed', this.catArray);

        });
      }
      else {
        this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", "Atleast select one task", "error");
      }
    }

    if (field == 'edit') {

      var data1 = {
        _id: this.templateId,
        data: this.popupData,
        template: this.template
      }

      if (data1._id != '' || data1._id == undefined) {
        console.log("data coming");

        const dialogRef = this.dialog.open(EditStandardOnboardingTemplateComponent, {
          width: '1300px',
          backdropClass: 'structure-model',
          data: data1,

        });

        dialogRef.afterClosed().subscribe((result: any) => {
          console.log("resultttt", result);
          this.isDisabled = false;
          // if (this.template['Onboarding Tasks'][result.category]) {
          //   this.template['Onboarding Tasks'][result.category].push(result);
          // } else {
          //   this.template['Onboarding Tasks'][result.category] = [result];
          // }
          if (this.id != undefined) {
            this.getStandardOnboardingtemplate(this.id)
          }
          console.log(this.template, "12121");

          // for(var i=0)
          // if(result.category == )

          console.log('The dialog was closed');

        });
      }
      else {
        this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", "Atleast select one task", "error");
      }
    }

  }


  /* Description: data submit for custom onboarding template
   author : vipin reddy */
  customOnboardTemplateAdd() {
    this.isValid = true;
    console.log(this.template['Onboarding Tasks']);
    if (Object.keys(this.template['Onboarding Tasks']).length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Onboarding template", "Add atleast one category", "error")
    }

    if (this.template.templateName && Object.keys(this.template['Onboarding Tasks']).length > 0)
      this.onboardingService.addCustomOnboardTemplate(this.template)
        .subscribe((res: any) => {
          console.log(res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Custom Onboarding Template", res.message, "success")
             this.getStandardOnboardingtemplate(res.data._id);

          }
          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Custom Onboarding Template", res.message, "error")
          }
        },
          (err) => {
            console.log(err);
            this.swalAlertService.SweetAlertWithoutConfirmation("Custom Onboarding Template", err.error.message, "error")
          })
  }

  /* Description: delete a category
   author : vipin reddy */
  deleteCategory() {
    var data = {
      _id: this.templateId,
      data: this.popupData
    }
    this.onboardingService.deleteCategory(data)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Delete Category", res.message, "success");
          this.getStandardOnboardingtemplate(this.id);
        }
      },
        (err) => {
          console.log(err);

        })
  }

}
