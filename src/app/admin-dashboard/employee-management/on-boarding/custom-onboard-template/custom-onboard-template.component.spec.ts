import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomOnboardTemplateComponent } from './custom-onboard-template.component';

describe('CustomOnboardTemplateComponent', () => {
  let component: CustomOnboardTemplateComponent;
  let fixture: ComponentFixture<CustomOnboardTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomOnboardTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomOnboardTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
