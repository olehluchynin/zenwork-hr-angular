import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardOnboardTemplateComponent } from './standard-onboard-template.component';

describe('StandardOnboardTemplateComponent', () => {
  let component: StandardOnboardTemplateComponent;
  let fixture: ComponentFixture<StandardOnboardTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardOnboardTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardOnboardTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
