import { Component, OnInit } from '@angular/core';
import { OnboardingService } from '../../../../services/onboarding.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EditStandardOnboardingTemplateComponent } from '../edit-standard-onboarding-template/edit-standard-onboarding-template.component';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { ActivatedRoute } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
    selector: 'app-standard-onboard-template',
    templateUrl: './standard-onboard-template.component.html',
    styleUrls: ['./standard-onboard-template.component.css']
})
export class StandardOnboardTemplateComponent implements OnInit {
    checked: false;
    fieldsName: any;
    allTasks = [];
    popupData = {};
    templateId: any;
    addShowHide: boolean = false;
    id: any;
    constructor(public dialog: MatDialog,
        private swalAlertService: SwalAlertService,
        private onboardingService: OnboardingService,
        private route: ActivatedRoute) { }

    /* Description: dummy Object 
    author : vipin reddy Date:17-04-2019*/
    template = {
        "templateName": "Onboarding Template",
        "templateType":"base",
        "Onboarding Tasks": {

        }
    }

    /* Description: ngFor loop object
   author : vipin reddy Date:17-04-2019*/
    getKeys(data) {
        // console.log(data);
        return Object.keys(data);
    }

    ngOnInit() {

        // this.allTasks.push(this.Manager_Tasks, this.IT_Setup, this.HR_Tasks, this.New_Hire_Forms)
        console.log(this.allTasks, "allTasks");
        this.route.params.subscribe(params => {
            this.id = params['id'];
            console.log(this.id);

        })
        this.getStandardOnboardingtemplate(this.id);
    }

    /* Description: get onboarding tasks 
    author : vipin reddy Date:17-04-2019*/
    getStandardOnboardingtemplate(id) {
        this.onboardingService.getStandardOnboardingtemplate(id)
            .subscribe((res: any) => {
                console.log(res);
                this.template = res.data;

                this.templateId = res.data._id;
            },
                (err: any) => {
                    console.log(err);

                })
    }
    /* Description: single checkbox choose
    author : vipin reddy Date:17-04-2019*/
    singleTaskChoose(taskArray, taskName, isChecked?) {
        console.log(taskArray, taskName, isChecked);
        if (isChecked == false) {
            this.popupData = ''
            this.addShowHide = false;

        }
        if (isChecked == true) {
            for (var task in this.template["Onboarding Tasks"]) {
                console.log(task);
                this.template["Onboarding Tasks"][task].forEach(element => {
                    if (element.taskName === taskName) {
                        element.isChecked = true;
                        this.popupData = element;
                        this.addShowHide = true;
                        console.log("popdaata", this.popupData);

                    } else {
                        element.isChecked = false;
                    }
                });
            }
        }
        this.template["Onboarding Tasks"][taskArray]
        console.log("");

    }

    /* Description: edit standard onboarding template open EDIT popup 
    author : vipin reddy */

    openDialog(): void {

        this.popupData['templatename'] = 'standard';
        var data = {
            _id: this.templateId,
            data: this.popupData
        }



        if (data._id != '') {
            const dialogRef = this.dialog.open(EditStandardOnboardingTemplateComponent, {
                width: '1300px',
                backdropClass: 'structure-model',
                data: data,

            });

            dialogRef.afterClosed().subscribe((result: any) => {
                this.getStandardOnboardingtemplate(this.id);
                console.log('The dialog was closed');

            });
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", "Atleast select one task", "error");
        }
    }

}
