import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnboardingRouting } from './on-boarding.routing';
import { OnBoardingComponent } from './on-boarding.component';
import { StandardOnboardTemplateComponent } from './standard-onboard-template/standard-onboard-template.component';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';

import { MatSelectModule } from '@angular/material';

import { EditStandardOnboardingTemplateComponent } from './edit-standard-onboarding-template/edit-standard-onboarding-template.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomOnboardTemplateComponent } from './custom-onboard-template/custom-onboard-template.component';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import {NgxMaskModule} from 'ngx-mask'


@NgModule({
  imports: [
    CommonModule,
    OnboardingRouting,MatMenuModule,MatButtonModule,MatIconModule,MaterialModuleModule,MatDatepickerModule,FormsModule,ReactiveFormsModule,MatSelectModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,AmazingTimePickerModule,NgxMaterialTimepickerModule,NgxMaskModule.forRoot({
      showMaskTyped : true,
    })
  ],
  declarations: [OnBoardingComponent, StandardOnboardTemplateComponent, 
    EditStandardOnboardingTemplateComponent, CustomOnboardTemplateComponent],
    entryComponents: [
      EditStandardOnboardingTemplateComponent
    ],
    providers :[LeaveManagementService]
})
export class OnBoardingModule { }
