import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OnBoardingComponent } from './on-boarding.component';
import { StandardOnboardTemplateComponent } from './standard-onboard-template/standard-onboard-template.component';
import { CustomOnboardTemplateComponent } from './custom-onboard-template/custom-onboard-template.component';

const routes: Routes = [
    // { path:'',redirectTo:"/on-boarding",pathMatch:"full" },
    { path: '', component: OnBoardingComponent },
    { path: 'onboardingTemplate/:id', component: StandardOnboardTemplateComponent },
    { path: 'custom-onboard-template/:id', component: CustomOnboardTemplateComponent },
    { path: 'custom-onboard-template', component: CustomOnboardTemplateComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OnboardingRouting { }
