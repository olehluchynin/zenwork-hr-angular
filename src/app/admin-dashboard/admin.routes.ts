import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { CommonModule } from '@angular/common';
/* import { SharedModule } from '../shared-module/shared.module'; */
import { MatSelectModule } from '@angular/material/select';





const routes: Routes = [
    { path: '', redirectTo: 'admin-dashboard', pathMatch: "full" },
    {
        path: 'admin-dashboard', component: AdminDashboardComponent, children: [
            { path: 'company-settings', loadChildren: "./company-settings/company-settings.module#CompanySettingsModule" },
            { path: 'employee-management', loadChildren: "./employee-management/employee-management.module#EmployeeManagementModule"},
            {path:'leave-management', loadChildren:'./leave-management/leave-management.module#LeaveManagementModule'},
            {path:'inbox', loadChildren:'./inbox/inbox.module#InboxModule'},
            {path:'dashboard', loadChildren:'./dashboard/dashboard.module#DashboardModule'},
            {path:'recruitment', loadChildren:'./recruitment/recruitment.module#RecruitmentModule'}
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes), CommonModule,MatSelectModule],
    exports: [RouterModule],
    declarations:[
        
    ]
})

export class AdminRouting { }