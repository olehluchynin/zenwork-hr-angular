import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  empType: any;
  type: any;

  constructor() { }

  ngOnInit() {
    this.empType = JSON.parse(localStorage.getItem('employeeType'));
    this.type = JSON.parse(localStorage.getItem('type'));

  }

}
