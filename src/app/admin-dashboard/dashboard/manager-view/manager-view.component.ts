import { Component, OnInit } from '@angular/core';
import { ManagerFrontpageService } from '../../../services/manager-frontpage.service';


@Component({
  selector: 'app-manager-view',
  templateUrl: './manager-view.component.html',
  styleUrls: ['./manager-view.component.css']
})
export class ManagerViewComponent implements OnInit {

  companyId: any;
  FrontPageControls = {
    show_work_anniversary:false,
    show_birthdays:false,
    show_to_do_list:false,
    manager_links:[]
  };


  constructor(
    private managerFrontpageService: ManagerFrontpageService
  ) { }

  ngOnInit() {
    this.companyId = JSON.parse(localStorage.getItem('companyId'))
    this.managerFrontPageControls()

  }

  managerFrontPageControls() {
    this.managerFrontpageService.getAllAdditionalData(this.companyId)
      .subscribe((res: any) => {
        console.log("get manager controls", res);
        this.FrontPageControls = res.data;

      },
        (err) => {
          console.log(err);

        })
  }
  goToLink(url: string){
    window.open(url, "_blank");
}

}
