import { Component, OnInit } from '@angular/core';
import { LeaveManagementService } from '../../../services/leaveManagement.service';
import { AccessLocalStorageService } from '../../../services/accessLocalStorage.service';
import { EmployeeServiceService } from '../../../services/employee-service.service';
import { SwalAlertService } from '../../../services/swalAlert.service';



@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent implements OnInit {
  public userId: any;
  managersData = {
    company_links: [],
    show_company_policies: false,
    show_company_links: false,
    show_training: false
  };
  companyID: any;
  companyAddList = [];


  constructor(
    private leaveManagementService: LeaveManagementService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private employeeServiceService: EmployeeServiceService
  ) { }

  ngOnInit() {
    this.companyID = JSON.parse(localStorage.getItem('companyId'));
    this.userId = this.accessLocalStorageService.get('employeeId');
    console.log(this.userId);
    this.getManagerData();
    this.getCompanyAnnouncements();

  }

  // Author: Saiprakash G, Date: 01/07/19,
  // Description: Employee punch in

  punchIn() {
    var data: any = {
      userId: this.userId
    }
    this.leaveManagementService.punchIn(data).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
      
    },(err)=>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");

    }, )

  }
  getCompanyAnnouncements() {

    this.employeeServiceService.getCompanyList(this.companyID)
      .subscribe((res: any) => {
        console.log("company annoucments", res);
        this.companyAddList = res.data;
      },
        (err) => {
          console.log(err);

        })
  }
  getManagerData() {
    this.employeeServiceService.getManagerSettings(this.companyID)
      .subscribe((res: any) => {
        console.log("manager data", res);
        this.managersData = res.data;
      },
        (err) => {
          console.log(err);

        })
  }
  // Author: Saiprakash G, Date: 01/07/19,
  // Description: Employee punch out

  punchOut() {
    var data: any = {
      userId: this.userId
    }
    this.leaveManagementService.punchOut(data).subscribe((res: any) => {
      console.log(res);

    }, (err) => {
      console.log(err);

    })

  }

  // Author: Saiprakash G, Date: 01/07/19,
  // Description: Approve time sheet

  approveTimeSheet() {
    var data: any = {
      userId: this.userId
    }
    this.leaveManagementService.timeSheetApproval(data).subscribe((res: any) => {
      console.log(res);

    }, (err) => {
      console.log(err);

    })
  }

}
