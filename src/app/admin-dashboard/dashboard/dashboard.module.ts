import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModuleModule } from '../../material-module/material-module.module';
import { ManagerViewComponent } from './manager-view/manager-view.component';
import { HrViewComponent } from './hr-view/hr-view.component';
import { SystemAdminViewComponent } from './system-admin-view/system-admin-view.component';
import { LeaveManagementService } from '../../services/leaveManagement.service';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';





const routes: Routes = [
  {
    path: '', component: DashboardComponent, 
    children: [
    {path:'',redirectTo:'employee-view',pathMatch:'full'},
    { path: 'employee-view', component: EmployeeViewComponent},
    { path: 'manager-view', component: ManagerViewComponent},
    { path: 'hr-view', component: HrViewComponent},
    { path: 'system-admin-view', component: SystemAdminViewComponent},
    ]
  },

]

@NgModule({
  declarations: [
    DashboardComponent, 
    EmployeeViewComponent,
    ManagerViewComponent,
    HrViewComponent,
    SystemAdminViewComponent

  ],
  imports: [
    CommonModule, RouterModule.forChild(routes),MatMenuModule,MatButtonModule,MatIconModule,MatDatepickerModule,
    FormsModule,ReactiveFormsModule,MaterialModuleModule
  ],
  providers: [LeaveManagementService,AccessLocalStorageService]
})
export class DashboardModule { }
