import { Component, OnInit } from '@angular/core';
import { InboxService } from '../../services/inbox.service';
import { PageEvent } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { AccessLocalStorageService } from '../../services/accessLocalStorage.service';
import { ResourceLoader } from '@angular/compiler';
import { SwalAlertService } from '../../services/swalAlert.service';



@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  selectID: any = {
    _id: ''
  }

  selectEmailId: any = [

  ]

  taskCatsArchived = [];

  taskOffBoardingsArchived = []

  tasknamesArchived = [];

  taskStatusCompleted: any;

  markasReadId: any;
  archiveDataList = [];

  archiveStatus: any;

  public cID: any;
  userID: any;
  public categoryName: any;
  public lists: any = [
    { name: 'Off-boarding Tasks' },
    { name: 'On-boarding Tasks' },
    { name: 'Employee Requests' },
    { name: 'Manager Requests' },
    { name: 'Workflow Approvals' },
    { name: 'Benefit Requests' },
    { name: 'Leave Management Requests' },
    { name: 'Performance Requests' },

  ]
  tasknames = [];
  taskCats: any = [];
  taskOffBoardings: any = [];
  taskManagerRequest: any;

  dataList = [];

  onboardTask: any;

  taskName: any;
  tasks: any;

  onboarding_template_id: {
    "Onboarding Tasks": {
    }
  }

  isChecked: boolean = false;

  selectCurrentEmailsId = [];

  // MatPaginator Output
  pageEvent: PageEvent;

  pageNo: any;
  perPage: any;
  singlePerson: boolean = false;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [2, 5, 10, 25, 100];

  count: any;
  archiveCount: any;

  public currentEmails: FormGroup
  tName: any = [];
  constructor(private inboxService: InboxService, private accessLocalStorageService: AccessLocalStorageService, private swalService: SwalAlertService) { }

  tNameArchive: any = [];

  ngOnInit() {

    this.currentEmails = new FormGroup({
      textarea: new FormControl("")
    });
    // this.taskStatusCompleted = 'Completed'

    this.cID = localStorage.getItem('companyId')
    console.log("inbox IDD", this.cID);
    this.cID = this.cID.replace(/^"|"$/g, "");
    console.log("cin222222222", this.cID);

    this.userID = this.accessLocalStorageService.get('employeeId');
    console.log("inbox userr", this.userID);

    this.getAllEmails();
    this.getAllAchived();

  }

  // Author:Suresh M, Date:06-06-19
  // Page Event
  pageEvents(event: any) {
    console.log("222222222222", event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllEmails();
  }

  // Author:Suresh M, Date:03-07-19
  // archivePageEvents
  archivePageEvents(event: any) {
    console.log("1111111111111", event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllAchived();
  }


  // Author:Saiprakash G, Date:08-06-19
  // Add notes and mark as complete

  currentEmailSubmit(id) {
    var data: any = {
      notif_id: id,
      notes: this.currentEmails.get('textarea').value

    }
    this.inboxService.markAsComplete(data).subscribe((res: any) => {
      console.log(res);
      this.getAllEmails();
      this.currentEmails.reset();

    }, (err) => {
      console.log(err);

    })

  }
  // Author:Saiprakash G, Date:08-06-19
  // Category filter 

  categoryFilter(event) {
    console.log("category", event);
    this.categoryName = event;
    this.getAllEmails();

  }

  // Author:Suresh M, Date:03-07-19
  // archiveCategoryFilter
  archiveCategoryFilter(event) {
    console.log("category", event);
    this.categoryName = event;
    this.getAllAchived();
  }


  // Author:Suresh M, Date:06-06-19
  // get All Components names
  getAllEmails() {

    var AllIds: any = {
      companyId: this.cID,
      userId: this.userID,
      category: this.categoryName,
      archive: false,
      per_page: this.perPage,
      page_no: this.pageNo
    }

    this.inboxService.getAllComponents(AllIds)
      .subscribe((res: any) => {
        console.log("All Components names", res);

        this.dataList = res.data;
        this.count = res.total_count;

        var categorynames: any = []

        for (var i = 0; i < this.dataList.length; i++) {
          this.tName.push(this.dataList[i].task_category)
          if (this.dataList[i].onboarding_template_id) {
            this.taskCats = this.dataList[i].onboarding_template_id['Onboarding Tasks'][this.tName[i]];
            for (var j = 0; j < this.taskCats.length; j++) {
              if (this.dataList[i].task_id == this.taskCats[j]._id) {
                console.log(this.dataList[i].task_id);
                console.log(this.taskCats[j]._id);

                this.tasknames.push({
                  taskName: this.taskCats[j].taskName,
                  taskStatus: this.taskCats[j].status
                })
              }

            }
          }
          if (this.dataList[i].offboarding_template_id) {
            this.taskOffBoardings = this.dataList[i].offboarding_template_id['offBoardingTasks'][this.tName[i]];
            for (var k = 0; k < this.taskOffBoardings.length; k++) {
              if (this.dataList[i].task_id == this.taskOffBoardings[k]._id) {
                console.log(this.dataList[i].task_id);
                console.log(this.taskOffBoardings[k]._id);

                this.tasknames.push({
                  taskName: this.taskOffBoardings[k].taskName,
                  taskStatus: this.taskOffBoardings[k].status
                })
              }

            }
          }

          if (this.dataList[i].manager_request_id) {
            this.tasknames.push({
              taskStatus: this.dataList[i].request_status

            })

          }
        }
        console.log("catnames", this.tasknames);

        for (var i = 0; i < this.dataList.length; i++) {
          Object.assign(this.dataList[i], this.tasknames[i])

          console.log("task namesssssssss", this.tasknames[i], this.dataList[i])
        }

        console.log("1111111", this.dataList)

      });

  }



  // Author:Suresh M, Date:06-06-19
  // get All Components names
  getAllAchived() {

    var AllIds: any = {
      companyId: this.cID,
      userId: this.userID,
      category: this.categoryName,
      archive: true,
      per_page: this.perPage,
      page_no: this.pageNo
    }

    this.inboxService.getAllComponents(AllIds)
      .subscribe((res: any) => {
        console.log("All Archived Emails", res);
        this.archiveDataList = res.data;
        console.log("Archiveeeeee", this.archiveDataList);
        this.archiveCount = res.total_count;

        for (var i = 0; i < this.archiveDataList.length; i++) {
          this.tNameArchive.push(this.archiveDataList[i].task_category)
          if (this.archiveDataList[i].onboarding_template_id) {
            this.taskCatsArchived = this.archiveDataList[i].onboarding_template_id['Onboarding Tasks'][this.tNameArchive[i]];
            console.log('true valuesssss', this.taskCatsArchived);
            if (this.taskCatsArchived && this.taskCatsArchived.length) {

              for (var j = 0; j < this.taskCatsArchived.length; j++) {
                if (this.archiveDataList[i].task_id == this.taskCatsArchived[j]._id) {
                  console.log(this.archiveDataList[i].task_id);
                  console.log(this.taskCatsArchived[j]._id);

                  this.tasknamesArchived.push({
                    taskName: this.taskCatsArchived[j].taskName,
                    taskStatus: this.taskCatsArchived[j].status
                  })
                }

              }

            }

          }
          if (this.archiveDataList[i].offboarding_template_id) {
            this.taskOffBoardingsArchived = this.archiveDataList[i].offboarding_template_id['offBoardingTasks'][this.tNameArchive[i]];
            console.log("archive filessssssssssss", this.taskOffBoardingsArchived);
            for (var k = 0; k < this.taskOffBoardingsArchived.length; k++) {
              if (this.archiveDataList[i].task_id == this.taskOffBoardingsArchived[k]._id) {
                console.log(this.archiveDataList[i].task_id);
                console.log(this.taskOffBoardingsArchived[k]._id);

                this.tasknamesArchived.push({
                  taskName: this.taskOffBoardingsArchived[k].taskName,
                  taskStatus: this.taskOffBoardingsArchived[k].status
                })
              }

            }
          }


        }

        for (var i = 0; i < this.archiveDataList.length; i++) {
          Object.assign(this.archiveDataList[i], this.tasknames[i])
        }

      });

  }

  getKeys(data) {
    return Object.keys(data)
  }


  // Author:Suresh M, Date:03-07-19
  // delete mails
  deleteEmails() {

    this.selectEmailId = this.dataList.filter(emailCheck => {
      return emailCheck.isChecked;
    });

    var deletaData: any = {
      companyId: this.cID,
      _ids: this.selectID
    }

    if (this.selectEmailId == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Select any Data", "", "")
    }
    else {
      this.inboxService.deleteEmail(deletaData)
        .subscribe((res: any) => {
          console.log("Delete mailss", res);
          this.getAllEmails();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Notifications successfully Delete", "success")
          }
        });
    }

  }

  // Author:Suresh M, Date:03-07-19
  // archiveMails

  archiveMails() {

    this.selectEmailId = this.dataList.filter(archiveCheck => {
      return archiveCheck.isChecked;
    });

    var archiveData = {
      companyId: this.cID,
      _ids: this.selectID
    }

    if (this.selectEmailId > 1) {
      this.swalService.SweetAlertWithoutConfirmation("Archived", "Select only one", "Request")
    } else if (this.selectEmailId == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Archived", "Select one data", "Request")
    }
    else {
      this.inboxService.emailsArchive(archiveData)
        .subscribe((res: any) => {
          console.log("Archive mailsss", res);
          this.getAllEmails();
          this.getAllAchived();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Archive", "Successfully Archive data", "suceess")
          }
        });
    }

  }


  // Author:Suresh M, Date:03-07-19
  // restoreEmails
  restoreEmails() {

    this.selectEmailId = this.archiveDataList.filter(archiveCheck => {
      return archiveCheck.isChecked;
    });

    var restoreData = {
      companyId: this.cID,
      _ids: this.selectID,
    }

    if (this.selectEmailId == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Select any Data", "", "")
    }
    else {
      this.inboxService.restoreMails(restoreData)
        .subscribe((res: any) => {
          console.log("restore mails", res);
          this.getAllAchived();
          this.getAllEmails();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Restore", "Successfully Restore mails", "success")
          }
        });
    }

  }


  // Author:Suresh M, Date:03-07-19
  // archive Delete
  archiveDelete() {

    this.selectEmailId = this.archiveDataList.filter(archiveCheck => {
      return archiveCheck.isChecked;
    });

    var archiveDelete = {
      companyId: this.cID,
      _ids: this.selectID
    }

    if (this.selectEmailId == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Select any data", "", "")
    }
    else {
      this.inboxService.deleteEmail(archiveDelete)
        .subscribe((res: any) => {
          console.log("archive deleteee", res);
          this.getAllAchived();
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully Delete archive", "success")
          }
        });
    }


  }

  // Author:Suresh M, Date:03-07-19
  // selectData Id
  selectData(id, status) {
    console.log("idddd", id, status);
    this.selectID = id
    console.log("id111111", this.selectID);
    this.taskStatusCompleted = status;
    console.log("statusssss", this.taskStatusCompleted)
  }

  // Author:Suresh M, Date:03-07-19
  // Mark As Read
  markAsRead(id) {
    console.log("mark assss", id)
    this.markasReadId = id
    console.log("mark readdd", this.markasReadId)

    var markasreadData = {
      companyId: this.cID,
      _id: this.markasReadId
    }

    this.inboxService.markasReadData(markasreadData)
      .subscribe((res: any) => {
        console.log("mark as success response", res);

      });

  }


}
