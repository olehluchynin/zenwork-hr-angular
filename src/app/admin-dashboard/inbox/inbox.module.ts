import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InboxComponent } from './inbox.component';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', component: InboxComponent, pathMatch: 'full' }
]


@NgModule({
  declarations: [InboxComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, RouterModule.forChild(routes), MatSelectModule, MatMenuModule, MatButtonModule, MatIconModule, MatDatepickerModule,
    MatCheckboxModule, MatPaginatorModule
  ]
})
export class InboxModule { }
