import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CompanySettingsService } from '../../../../services/companySettings.service';
declare var jQuery: any; 
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { Router } from '@angular/router';
import { ConfirmationComponent } from '../../../../shared-module/confirmation/confirmation.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-email-templates',
  templateUrl: './email-templates.component.html',
  styleUrls: ['./email-templates.component.css']
})
export class EmailTemplatesComponent implements OnInit {
  checkedStructure:any = {
    checked: ''
  }
  module:any;
  givenFileName:any
  files:any;
  fileName:any;
  emailData:any;
  recruitmentData:any;
  administatetData:any;
  performanceEmailData:any;
  leaveEmailData:any;

  selectedEmailTemplate = [];
  selectedRecruitmentTemplate = [];
  selectedAdministateTemplate = [];
  selectedPerformanceTemplate = [];
  selectedLeaveTemplate = [];


employeeManagement = [];
recruitments = [];
benefitsAdministration = [];
performanceManagement = [];
leaveManagement = [];
public isValid:boolean = false;
emailTemplate1 = [];
emailTemplate2 = [];
emailTemplate3 = [];
emailTemplate4 = [];
emailTemplate5 = [];


@ViewChild('openEdit1') openEdit1: ElementRef;
@ViewChild('openEdit2') openEdit2: ElementRef;
@ViewChild('openEdit3') openEdit3: ElementRef;
@ViewChild('openEdit4') openEdit4: ElementRef;
@ViewChild('openEdit5') openEdit5: ElementRef
  constructor(private companySettingsService:CompanySettingsService,
    private swalAlertService:SwalAlertService,
    private router:Router,
    private dialog: MatDialog
    ) { }

  ngOnInit() {
    this.getAllEmialTempates()
  }
    // Author: Saiprakash , Date: 15/04/19
  // Upload email templates

  fileChangeEvent(e: any) {

    this.files = e.target.files[0];
    console.log(this.files.name);
    this.fileName = this.files.name
    

  }
  getAllEmialTempates(){
    this.companySettingsService.getAllEmialTempates()
    .subscribe((res:any)=>{
      console.log(res)
      this.employeeManagement = res.grouped['Employee Management'];
      this.recruitments = res.grouped['Recruitment'];
      this.benefitsAdministration = res.grouped['Benefits Administration'];
      this.performanceManagement = res.grouped['Performance Management'];
      this.leaveManagement = res.grouped['Leave Management'];
    },
    (err)=>{
      console.log(err);
      
    })
  }



  
  moduleChange(){
    console.log(this.module);
    
    
  }
  
    // Author: Saiprakash , Date: 15/04/19
  // Upload for email templates

  uploadEmailTemplate(emailForm){

    var formData: FormData = new FormData();
    formData.append('file', this.files);
    formData.append('module',this.module);
    formData.append('givenFileName',this.givenFileName.trim());
    this.isValid = true;

    if(this.fileName && this.module && this.givenFileName){

      if(this.emailData && this.emailData._id ){
        formData.append('_id', this.emailData._id);
        formData.append('s3Path', this.emailData.s3Path);
        formData.append('originalFilename', this.fileName);

        this.companySettingsService.editEmailTemplate(formData).subscribe((res:any)=>{
          console.log(res);
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
          this.isValid = false;
          emailForm.resetForm();
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.module = '';
          this.emailTemplate1 = [];
          this.emailData._id = '';
          this.getAllEmialTempates()
        }, err =>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        })
      }
      else if (this.recruitmentData && this.recruitmentData._id){
        formData.append('_id', this.recruitmentData._id);
        formData.append('s3Path', this.recruitmentData.s3Path);
        formData.append('originalFilename', this.fileName);

        this.companySettingsService.editEmailTemplate(formData).subscribe((res:any)=>{
          console.log(res);
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
          this.isValid = false;
          emailForm.resetForm();
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.module = '';
          this.emailTemplate2 = [];
          this.recruitmentData._id = '';
          this.getAllEmialTempates()
        }, err =>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        })
      }

      else if (this.administatetData && this.administatetData._id){
        formData.append('_id', this.administatetData._id);
        formData.append('s3Path', this.administatetData.s3Path);
        formData.append('originalFilename', this.fileName);
       

        this.companySettingsService.editEmailTemplate(formData).subscribe((res:any)=>{
          console.log(res);
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
          this.isValid = false;
          emailForm.resetForm();
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.module = '';
          this.emailTemplate3 = [];
          this.administatetData._id = '';
          this.getAllEmialTempates()
        }, err =>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        })
      }
      else if (this.performanceEmailData && this.performanceEmailData._id){
        formData.append('_id', this.performanceEmailData._id);
        formData.append('s3Path', this.performanceEmailData.s3Path);
        formData.append('originalFilename', this.fileName);

        this.companySettingsService.editEmailTemplate(formData).subscribe((res:any)=>{
          console.log(res);
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
          this.isValid = false;
          emailForm.resetForm();
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.module = '';
          this.emailTemplate4 = [];
          this.performanceEmailData._id = '';
          this.getAllEmialTempates()
        }, err =>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error"); 
        })
      }
      else if (this.leaveEmailData && this.leaveEmailData._id){
        formData.append('_id', this.leaveEmailData._id);
        formData.append('s3Path', this.leaveEmailData.s3Path);
        formData.append('originalFilename', this.fileName);

        this.companySettingsService.editEmailTemplate(formData).subscribe((res:any)=>{
          console.log(res);
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
          this.isValid = false;
          emailForm.resetForm();
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.module = '';
          this.emailTemplate5 = [];
          this.leaveEmailData._id = '';
          this.getAllEmialTempates()
        }, err =>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        })
      }
      
      else {
        this.companySettingsService.addEmailTemplates(formData).subscribe((res:any)=>{
          console.log(res);
                    
          jQuery("#myModal").modal("hide");
          this.fileName='';
          this.givenFileName = '';
          this.module = '';
          this.isValid = false;
          emailForm.resetForm();
          this.emailTemplate1 = [];
          this.emailTemplate2 = [];
          this.emailTemplate3 = [];
          this.emailTemplate4 = [];
          this.emailTemplate5 = [];
          this.swalAlertService.SweetAlertWithoutConfirmation("Email template uploaded successfully","","success")

          this.getAllEmialTempates()
        }, err =>{
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
          
        })
      }
    
    }
    
  

  }
   // Author: Vipin Reddy , Date: 16/04/19
  // next page moving
  saveandNext(){
    this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/company-policy"])
  }
  // Author: saiprakash , Date: 16/04/19
  // Edit single Emailtamplate
  editEmailTemplate1(){

    this.selectedEmailTemplate = this.employeeManagement.filter(templateData => {
      return templateData.isChecked
      
     })
 
     if (this.selectedEmailTemplate.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error')
    } else if (this.selectedEmailTemplate.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error')
    } 
    else {
      this.openEdit1.nativeElement.click();
      this.companySettingsService.singleEmailTemplate(this.selectedEmailTemplate[0]._id).subscribe((res:any)=>{
        console.log(res);
        this.emailData = res.data;

        this.fileName = res.data.originalFilename;
        this.givenFileName = res.data.givenFileName
        this.module = res.data.module
        
      }, err=>{
        console.log(err);
        
      })
    }
  }

  editEmailTemplate2(){
    this.selectedRecruitmentTemplate = this.recruitments.filter(recruitData => {
      return recruitData.isChecked    
     })
     if (this.selectedRecruitmentTemplate.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error')
    } else if (this.selectedRecruitmentTemplate.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error')
    } 
     else {
      this.openEdit2.nativeElement.click();
      this.companySettingsService.singleEmailTemplate(this.selectedRecruitmentTemplate[0]._id).subscribe((res:any)=>{
        console.log(res);
        this.recruitmentData = res.data;

        this.fileName = res.data.originalFilename;
        this.givenFileName = res.data.givenFileName
        this.module = res.data.module
        
      }, err=>{
        console.log(err);
        
      })
    }

  }
  editEmailTemplate3(){
    this.selectedAdministateTemplate = this.benefitsAdministration.filter(benefitData => {
      return benefitData.isChecked    
     })
     if (this.selectedAdministateTemplate.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error')
    } else if (this.selectedAdministateTemplate.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error')
    } 
     else {
      this.openEdit3.nativeElement.click();
      this.companySettingsService.singleEmailTemplate(this.selectedAdministateTemplate[0]._id).subscribe((res:any)=>{
        console.log(res);
        this.administatetData = res.data;

        this.fileName = res.data.originalFilename;
        this.givenFileName = res.data.givenFileName
        this.module = res.data.module
        
      }, err=>{
        console.log(err);
        
      })
    }
  }

  editEmailTemplate4(){
    this.selectedPerformanceTemplate = this.performanceManagement.filter(performanceData => {
      return performanceData.isChecked    
     })
     if (this.selectedPerformanceTemplate.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error')
    } else if (this.selectedPerformanceTemplate.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error')
    } 
     else {
      this.openEdit4.nativeElement.click();
      this.companySettingsService.singleEmailTemplate(this.selectedPerformanceTemplate[0]._id).subscribe((res:any)=>{
        console.log(res);
        this.performanceEmailData = res.data;

        this.fileName = res.data.originalFilename;
        this.givenFileName = res.data.givenFileName
        this.module = res.data.module
        
      }, err=>{
        console.log(err);
        
      })
    }

  }  
  editEmailTemplate5(){
    this.selectedLeaveTemplate = this.leaveManagement.filter(leaveData => {
      return leaveData.isChecked    
     })

     if (this.selectedLeaveTemplate.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error')
    } else if (this.selectedLeaveTemplate.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error')
    } 
     else {
       console.log(this.selectedLeaveTemplate);
       
      this.openEdit5.nativeElement.click();
      this.companySettingsService.singleEmailTemplate(this.selectedLeaveTemplate[0]._id).subscribe((res:any)=>{
        console.log(res);
        this.leaveEmailData = res.data;

        this.fileName = res.data.originalFilename;
        this.givenFileName = res.data.givenFileName
        this.module = res.data.module
        
      }, err=>{
        console.log(err);
        
      })
    }
  }
  


  // Author: saiprakash , Date: 16/04/19
  // Delete email tamplate

  deleteEmailTemplate1(){
    this.selectedEmailTemplate = this.employeeManagement.filter(policyData => {
     
      return policyData.isChecked
          
     })
      if (this.selectedEmailTemplate.length == 0) {
       this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error')
     } 
     else {
      
       this.companySettingsService.deleteEmailTemplates(this.selectedEmailTemplate).subscribe((res:any)=>{
         console.log(res);
         this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success')
         this.emailTemplate1 = [];
         this.getAllEmialTempates()
       }, err=>{
         console.log(err);
         this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
       })
     }

  }
  deleteEmailTemplate2(){
    this.selectedRecruitmentTemplate = this.recruitments.filter(policyData => {
     
      return policyData.isChecked
          
     })
      if (this.selectedRecruitmentTemplate.length == 0) {
       this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error')
     } 
     else {
      
       this.companySettingsService.deleteEmailTemplates(this.selectedRecruitmentTemplate).subscribe((res:any)=>{
         console.log(res);
         this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success')
         this.emailTemplate2 = [];
         this.getAllEmialTempates()
       }, err=>{
         console.log(err);
         this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
       })
     }

  }
  deleteEmailTemplate3(){
    this.selectedAdministateTemplate = this.benefitsAdministration.filter(policyData => {
     
      return policyData.isChecked
          
     })
      if (this.selectedAdministateTemplate.length == 0) {
       this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error')
     } 
     else {
      
       this.companySettingsService.deleteEmailTemplates(this.selectedAdministateTemplate).subscribe((res:any)=>{
         console.log(res);
         this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success')
         this.emailTemplate3 = [];
         this.getAllEmialTempates()
       }, err=>{
         console.log(err);
         this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
       })
     }

  }
  deleteEmailTemplate4(){
    this.selectedPerformanceTemplate = this.performanceManagement.filter(policyData => {
     
      return policyData.isChecked
          
     })
      if (this.selectedPerformanceTemplate.length == 0) {
       this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error')
     } 
     else {
      
       this.companySettingsService.deleteEmailTemplates(this.selectedPerformanceTemplate).subscribe((res:any)=>{
         console.log(res);
         this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success')
         this.emailTemplate4 = [];
         this.getAllEmialTempates()
       }, err=>{
         console.log(err);
         this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
       })
     }

  }
  deleteEmailTemplate5(){
    this.selectedLeaveTemplate = this.leaveManagement.filter(element => {
     
      return element.isChecked
          
     })
      if (this.selectedLeaveTemplate.length == 0) {
       this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error')
     } 
     else {
      
       this.companySettingsService.deleteEmailTemplates(this.selectedLeaveTemplate).subscribe((res:any)=>{
         console.log(res);
         this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success')
         this.emailTemplate5 = [];
         this.getAllEmialTempates()
       }, err=>{
         console.log(err);
         this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
       })
     }

  }

  checkEmailTemplate1(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.emailTemplate1.push(id)
    }
    console.log(this.emailTemplate1);
    if(event.checked == false){
      var index = this.emailTemplate1.indexOf(id);
        if (index > -1) {
          this.emailTemplate1.splice(index, 1);
        }
        console.log(this.emailTemplate1);
    }

  }
  checkEmailTemplate2(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.emailTemplate2.push(id)
    }
    console.log(this.emailTemplate2);
    if(event.checked == false){
      var index = this.emailTemplate2.indexOf(id);
        if (index > -1) {
          this.emailTemplate2.splice(index, 1);
        }
        console.log(this.emailTemplate2);
    }

  }
  checkEmailTemplate3(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.emailTemplate3.push(id)
    }
    console.log(this.emailTemplate3);
    if(event.checked == false){
      var index = this.emailTemplate3.indexOf(id);
        if (index > -1) {
          this.emailTemplate3.splice(index, 1);
        }
        console.log(this.emailTemplate3);
    }

  }
  checkEmailTemplate4(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.emailTemplate4.push(id)
    }
    console.log(this.emailTemplate4);
    if(event.checked == false){
      var index = this.emailTemplate4.indexOf(id);
        if (index > -1) {
          this.emailTemplate4.splice(index, 1);
        }
        console.log(this.emailTemplate4);
    }

  }
  checkEmailTemplate5(event, id){
    console.log(event, id);
    if(event.checked == true){
      this.emailTemplate5.push(id)
    }
    console.log(this.emailTemplate5);
    if(event.checked == false){
      var index = this.emailTemplate5.indexOf(id);
        if (index > -1) {
          this.emailTemplate5.splice(index, 1);
        }
        console.log(this.emailTemplate5);
    }

  }

  deleteEmailTemplate(){
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '350px',
      panelClass: 'confirm-dialog',
      data: {
        text: 'delete'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        if(this.emailTemplate1 && this.emailTemplate1.length){
          this.deleteEmailTemplate1();
        }
        else if(this.emailTemplate2 && this.emailTemplate2.length){
          this.deleteEmailTemplate2();
        }
        else if(this.emailTemplate3 && this.emailTemplate3.length){
          this.deleteEmailTemplate3();
        }
        else if(this.emailTemplate4 && this.emailTemplate4.length){
          this.deleteEmailTemplate4();
        }
       else if(this.emailTemplate5 && this.emailTemplate5.length){
        this.deleteEmailTemplate5();
       }
        
       
      }
    });
  }
  alphabets(event){
    return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)
  }

  cancelEmailForm(){
    this.fileName='';
    this.givenFileName = '';
    this.module = '';
    this.isValid = false;
  }
}
