import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureFieldDialogComponent } from './structure-field-dialog.component';

describe('StructureFieldDialogComponent', () => {
  let component: StructureFieldDialogComponent;
  let fixture: ComponentFixture<StructureFieldDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StructureFieldDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StructureFieldDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
