import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CompanySettingsService } from '../../../../../services/companySettings.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
import { element } from '@angular/core/src/render3/instructions';
import { PayRollService } from '../../../../../services/pay-roll.service';
import { looseIdentical } from '@angular/core/src/util';



@Component({
  selector: 'app-structure-field-dialog',
  templateUrl: './structure-field-dialog.component.html',
  styleUrls: ['./structure-field-dialog.component.css']
})
export class StructureFieldDialogComponent implements OnInit {

  structureFieldName: string
  editField: boolean = true;
  editField1: boolean = true;
  structureValues: Array<any> = [];
  customValues: any = []
  selectedStructureDetails: any;
  structureStatus: string = "Active"
  jobtitle: boolean = false;
  EEOFields = [];
  companyId: any;
  salarygradelist: any;

  selectedEeojob: any;
  selectedSalarayGrade: any;
  salGradeBands: any;
  selectedSalarayGradeBand: any;
  checkFiledIds = [];
  status: any;

  JobStructue: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<StructureFieldDialogComponent>,
    private swalAlertService: SwalAlertService,
    public companySettingsService: CompanySettingsService,
    public payRollService: PayRollService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.companyId = JSON.parse(localStorage.getItem('companyId'))
    console.log(this.data);
    if (this.data.fieldsName == 'Job Title') {
      this.jobtitle = true;
      this.getEEOJobcategoryFields()
      this.getSalaryGradelist()
    }
    this.structureValues = this.data.StructureValues
    this.customValues = this.data.customValues

    this.customValues.filter(item => {
      console.log(item);

      item.editField = true;
      // item.isValidBand = false;
    })
    this.structureFieldName = this.data.fieldsName

    // this.addStructureField()
  }

  getEEOJobcategoryFields() {
    this.companySettingsService.getStuctureFields('EEO Job Category', 0)
      .subscribe((res: any) => {
        console.log(res);

        this.EEOFields = res.default;
        res.custom.forEach(element => {
          this.EEOFields.push(element);
        });
        console.log(this.EEOFields, "eeofields");

      },
        (err) => {
          console.log(err);

        })

  }

  getSalaryGradelist() {
    var postData = {
      "companyId": this.companyId,
    }

    this.payRollService.getSalrayGradeList(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.salarygradelist = res.data;
        for (var i = 0; i < this.customValues.length; i++) {
          if (this.customValues[i].salary_grade)
            this.salarygradelistChange(this.customValues[i].salary_grade, i)

        }
      },
        (err) => {
          console.log(err);

        })
  }

  salarygradelistChange(id, i) {

    this.selectedSalarayGrade = id
    console.log(this.selectedSalarayGrade);

    this.payRollService.getSingleSalaryGrade(this.companyId, this.selectedSalarayGrade)
      .subscribe((res: any) => {
        console.log(res);
        this.customValues[i].salGradeBands = res.data.grades;
        // this.customValues[i].isValidBand = false;
      },
        (err) => {
          console.log(err);

        })
    // this.customValues.forEach(element => {
    //   if (element.grade_band == this.salGradeBands._id) {
    //     var index = this.salGradeBands.indexOf(element.grade_band);
    //     console.log(index);

    //     if (index > -1) {
    //       this.salGradeBands.splice(index, 1);
    //     }
    //   }
    // })


    console.log(this.customValues[i].salGradeBands);

  }
  salarygradeBandlistChange() {

    console.log(this.selectedSalarayGradeBand);
  }
  addStructureField() {
    this.customValues.push({ name: '', status: '', parentRef: this.structureFieldName })
    console.log(this.customValues);


    this.editField = false;
  }

  fieldStatus(status) {
    console.log(status);
    this.status = status;

  }
  editStructureField() {
    this.selectedStructureDetails = this.customValues.filter(structureData => {
      return structureData.isChecked

    })
    if (this.selectedStructureDetails.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit structure", "Please select only one record to proceed", 'error')
    } else if (this.selectedStructureDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit structure", "Please select one record to proceed", 'error')
    }
    else {
      if (this.selectedStructureDetails[0]._id) {
        this.selectedStructureDetails[0].editField = false;
      }

      console.log(this.selectedStructureDetails[0]._id);

    }

  }

  // Author: Saiprakash, Date: 04/04/19 
  // Add and edit structure fields

  submitStructure() {
    // var tempSF = [];
    // this.customValues.forEach(sf => {
    //   if (sf && sf.name && sf.status) {
    //     tempSF.push(sf);
    //   }
    // });
    // this.customValues = tempSF;
    console.log(this.customValues);

    this.companySettingsService.addUpdateStructureData(this.customValues).subscribe((res: any) => {
      console.log(res);
      this.checkFiledIds = [];
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      var resdata;
      resdata = res;
      resdata.field = this.structureFieldName;
      console.log(resdata, this.structureFieldName)
      console.log(res, "111111111111111");
      this.dialogRef.close(resdata);

    }, err => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");

    })

  }

  submitStructureJob() {

    console.log(this.customValues);
    for (var i = 0; i < this.customValues.length; i++) {
      if (this.customValues[i].salary_grade && !this.customValues[i].grade_band)
        this.customValues[i].isValidBand = true;
    }
    for (var i = 0; i < this.customValues.length; i++) {
      if (this.customValues[i].isValidBand == true) {
        console.log(i);

        this.JobStructue = true;
      }
    }
    console.log(this.customValues, this.JobStructue);

    if (!this.JobStructue) {
      this.companySettingsService.addUpdateStructureData(this.customValues).subscribe((res: any) => {
        console.log(res);
        this.checkFiledIds = [];
        if (res.status == true) {
          var resdata;
          resdata = res;
          resdata.field = this.structureFieldName;
          console.log(resdata, this.structureFieldName)
          console.log(res, "111111111111111");
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success")
          this.dialogRef.close(resdata);
        }


      }, err => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation('Job Title', err.error.message, 'error')

      })
    }
  }

  eeoJobCategorySend(event, id: any) {
    console.log(event, id);
    this.customValues.forEach(element => {
      if (element._id == id) {
        element['EeoJobCategoryCode'] = event.value
      }
    });
    this.structureValues.forEach(element => {
      if (element._id == id) {
        element['EeoJobCategoryCode'] = event.value
      }
    })
    console.log(this.customValues);

  }

  selectedSalGrade(event, id: any, i) {
    console.log(event, id, i);
    this.salarygradelistChange(event, i)
    this.customValues.forEach(element => {
      if (element._id == id) {
        element['salary_grade'] = event
      }
    });
    this.structureValues.forEach(element => {
      if (element._id == id) {
        element['salary_grade'] = event
      }
    })
    console.log(this.customValues);
  }

  selectedSalGradeBand(event, id: any, i) {
    console.log(event, id);

    this.customValues.forEach(element => {
      if (element._id == id) {
        element['grade_band'] = event.value
      }
    });
    this.structureValues.forEach(element => {
      if (element._id == id) {
        element['grade_band'] = event.value
      }
    })
    this.customValues[i].isValidBand = false;
    this.JobStructue = false;
    console.log(this.customValues);
  }

  // Author: Saiprakash, Date: 04/04/19 
  // Delete structure fields

  deleteStructureField() {

    this.selectedStructureDetails = this.customValues.filter(structureData => {
      return structureData.isChecked

    })

    if (this.selectedStructureDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Delete structure", "Please select one record to proceed", 'error')
    }

    else {
      var fieldsList = [];
      var data: any = {
        _ids: this.selectedStructureDetails
      }
      this.companySettingsService.deleteStructure(data).subscribe((res: any) => {
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");

        this.customValues.forEach(element => {
          console.log(this.checkFiledIds);

          if (this.checkFiledIds.indexOf(element._id) < 0) {
            fieldsList.push(element);
          }
          console.log(fieldsList);

        })
        this.customValues = fieldsList;
        this.checkFiledIds = [];
      }, err => {
        console.log(err);
        this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
      })
    }

  }

  checkFields(event, id) {
    console.log(event, id);

    if (event.checked == true) {
      this.checkFiledIds.push(id);
      console.log(this.checkFiledIds);

    }
    if (event.checked == false) {
      var index = this.checkFiledIds.indexOf(id);
      if (index > -1) {
        this.checkFiledIds.splice(index, 1);
      }
      console.log(this.checkFiledIds);
    }
  }


  cancel() {

    console.log(this.structureFieldName)

    this.dialogRef.close(this.structureFieldName)
  }
}
