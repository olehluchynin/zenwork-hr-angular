import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { StructureFieldDialogComponent } from './structure-field-dialog/structure-field-dialog.component';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.css']
})
export class StructureComponent implements OnInit {
 
  structureName:any;
  checked:any;
  StructureValues:Array<any> = [];
  customValues:Array<any> = []
  fieldsName: string
  structureFields:Array<any> = [
    {
      name:'Assets',
      isChecked: true
    } 
 ]

   
  
  constructor(
    public dialog: MatDialog,
    private companySettingsService: CompanySettingsService,
    private router:Router

  ) { }

  ngOnInit() {

    this.getFields();

    this.getStructureSubFields(this.structureFields[0].name,this.structureFields[0].isChecked);
   
  }

   // Author: Saiprakash, Date: 13/04/19 
  // Get structure fields
    getFields(){
      this.companySettingsService.getFields().subscribe((res:any)=>{
       console.log(res);
       this.structureFields = res.data
       
      }, err=>{
        console.log(err);
        
      })
    }

    

   // Author: Saiprakash, Date: 04/04/19 
  // Get structure sub docs fields

  getStructureSubFields(fields, isChecked?){
    console.log(fields, isChecked);
    this.fieldsName = fields;
    this.checked = isChecked
    if(isChecked == true){
      this.structureFields.forEach(element =>{
        if(element.name !== fields){
          element.isChecked = false
        }
      })
      this.companySettingsService.getStuctureFields(fields,0).subscribe((res:any)=>{
        console.log(res,name);
        this.StructureValues = res.default;
        this.customValues = res.custom
        console.log(this.StructureValues);
        
        
       }, err =>{
         console.log(err);
         
       })
    }
    
 
  
  }

  openDialog(): void {

    var data = {
      StructureValues: this.StructureValues,
      fieldsName: this.fieldsName,
      customValues: this.customValues

    }
    
    const dialogRef = this.dialog.open(StructureFieldDialogComponent, {
      width: '1300px',
      height: '700px',
      backdropClass: 'structure-model',
      data: data, 
           
    });

    dialogRef.afterClosed().subscribe((result:any) => {
      console.log('The dialog was closed');
     if(result.field){

      this.getStructureSubFields(result.field, this.checked)
     }else {
       console.log(result);
       
       this.getStructureSubFields(result, this.checked)
     }
     
    });
  }
 // Author: Saiprakash, Date: 03/04/19 
  // Upload to structure xlsx sile

  fileChangeEvent(e: any) {

    var files = e.target.files[0];
    console.log(files.name)
    var formData: FormData = new FormData();
    formData.append('file', files);
   
    this.companySettingsService.structureUpload(formData).subscribe((res:any)=>{
      console.log(res);
     this.getStructureSubFields(this.fieldsName, this.checked)
    }, err =>{
      console.log(err);
      
    })

  }
  saveandNext(){
    this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/training"])
  } 
}
