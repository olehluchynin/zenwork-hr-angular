import { Component, OnInit } from '@angular/core';
import { PayRollService } from '../../../../services/pay-roll.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
declare var jQuery: any;
import { ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OnboardingService } from '../../../../services/onboarding.service';
import { ConfirmationComponent } from '../../../../shared-module/confirmation/confirmation.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';




@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.css']
})
export class PayrollComponent implements OnInit {
  @ViewChild('f') form: NgForm;


  addSaralySubmit: boolean = true;

  payrollGroup = {
    name: '',
    frequency: '',
    code: ''
  }
  isValid: boolean = false;
  deleteIds = []
  payFrequency = []
  payrollGroups = []
  payrollGroupId: any;
  companyId: any;
  salaryGradedeleteIds = []

  payrollGroupCount: any;
  pageEvents1 = {
    perpage: 5,
    pageno: 1,
    salaryGradePerpage: 5,
    salaryGradePageNo: 1,
    calanderPerPage: 5,
    calanderPageNo: 1,
    perpageSalaryGradeBand: 10,
    pagenoSalaryGradeBand: 1

  }
  SinglePayrollGroup = {
    pay_frequency: '',
    pay_group_code: "",
    payroll_group_name: "",
    status: ""
  }
  salryGrades = []
  chooseSalGrades = []
  salGrade = {
    name: ''
  }
  isValidSal: boolean = false;
  salaryGradePerpage: any;
  salaryGradePageNo: any;

  salaryGradeList = [];
  deleteSalGradeBand = [];
  salaryGradeCount: any;
  SingleSalaryGrade: any = [
    {
      grades: []
    }
  ];
  updateSalGrade: boolean = false;
  salGradeGradeBands = [];
  salrayGradeMinValue: any;
  salrayGradeMaxValue: any;
  salrayGradeMidValue: any;

  startDate: any;


  payrollAssign: any;

  payrollCalendar: any = {
    number_of_cycles: "",
    calendar_name: "",
    number_of_days_in_a_cycle: "",
    first_cycle_start_date: "",
    first_cycle_end_date: "",
    assigned_payroll_group: '',
    calendar_year: ''
  }

  payrollCalanderCycle = [];
  payrollCalanderList: any;
  payrollCalanderCount: any;
  isValidCalander: boolean = false;
  showWeeksError: boolean = false;

  calanderGroupId: any;
  calanderdeleteIds = [];
  payrollCalenderUpdate: boolean = false;
  salaryGradeBands: any;
  payrollGroupsDropdown: any;
  // jobTitleArray = []
  nextCycle: any;
  currentPayCycleDates = {
    pay_cycle_number: '',
    pay_cycle_start_date: '',
    pay_cycle_end_date: ''
  };
  currentCylcle: boolean = false;
  nextCylcle: boolean = false;
  nextPayCycleDates = {
    pay_cycle_number: '',
    pay_cycle_start_date: '',
    pay_cycle_end_date: ''
  };
  today: any;

  nextCycle1: any;
  currentCylcle1: boolean = false;
  currentPayCycleDates1 = {
    pay_cycle_number: '',
    pay_cycle_start_date: '',
    pay_cycle_end_date: ''
  };
  nextPayCycleDates1 = {
    pay_cycle_number: '',
    pay_cycle_start_date: '',
    pay_cycle_end_date: ''
  };
  nextCylcle1: boolean = false;
  midSalValueError: boolean = false;
  gradeBandSubmit: boolean = false;

  constructor(private payRollService: PayRollService,
    private swalAlertService: SwalAlertService,
    private onboardingService: OnboardingService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.companyId = JSON.parse(localStorage.getItem('companyId'));
    this.getPayrollGroup()
    // this.getpayFrequency()
    this.getsalrayGrades()
    this.getPayrollCalandereList()
    this.getStructureJobTitleFields()
  }



  /*Description: start Date change 
author : vipin reddy */

  startDatechange() {
    console.log(this.startDate.toISOString(), "startDate");

  }

  getStructureJobTitleFields() {

    // this.onboardingService.getSingleStructureFields()
    // .subscribe((res: any) => {
    //   console.log(res);
    // },
    // (err)=>{

    // })
  }

  /*Description: get payrollgroup list
author : vipin reddy */

  getPayrollGroup() {
    var postData = {
      companyId: this.companyId,
      per_page: this.pageEvents1.perpage,
      page_no: this.pageEvents1.pageno
    }
    this.payRollService.getPayrollGroup(postData)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.payrollGroups = res.data;
          this.payrollGroupCount = res.total_count;
        }
      },
        (err) => {
          console.log(err);

        })

  }




  updatePayrollGroup() {
    this.SinglePayrollGroup['companyId'] = this.companyId;
    this.SinglePayrollGroup['_id'] = this.deleteIds[0]
    console.log(this.SinglePayrollGroup, 'update data');

    if (this.SinglePayrollGroup.pay_frequency && this.SinglePayrollGroup.pay_group_code && this.SinglePayrollGroup.payroll_group_name &&
      this.SinglePayrollGroup.status) {
      this.SinglePayrollGroup.pay_group_code = this.SinglePayrollGroup.pay_group_code.trim()
      this.SinglePayrollGroup.payroll_group_name = this.SinglePayrollGroup.payroll_group_name.trim()

      this.payRollService.updatePayrollGroup(this.SinglePayrollGroup)
        .subscribe((res: any) => {
          console.log(res);
          if (res.status = true) {
            this.swalAlertService.SweetAlertWithoutConfirmation('PayrollGroup', res.message, 'success')
            jQuery("#myModal1").modal("hide");
            this.deleteIds = []
            this.getPayrollGroup()
          }

        },
          (err) => {
            console.log(err);
            this.swalAlertService.SweetAlertWithoutConfirmation('PayrollGroup', err.error.message, 'error')

          })
    }
  }


  showOptions($event, id, data) {
    if (data == 'paygroup') {
      console.log("1222222", $event.checked, id);
      this.payrollGroupId = id;

      if ($event.checked == true) {
        this.deleteIds.push(id)
      }
      console.log(this.deleteIds, "123321");
      if ($event.checked == false) {
        var index = this.deleteIds.indexOf(id);
        if (index > -1) {
          this.deleteIds.splice(index, 1);
        }
        console.log(this.deleteIds);

      }
    }
    if (data == 'salaryGrade') {

      console.log("1222222", $event.checked, id);
      this.payrollGroupId = id;

      if ($event.checked == true) {
        if (this.salaryGradedeleteIds) {

          this.salaryGradedeleteIds = [id];

          this.salaryGradeList.forEach(element => {
            if (id == element._id) {
              element.isChecked = true
            }
            else {
              element.isChecked = false;
            }
          });
        }
        else {
          this.salaryGradedeleteIds.push(id)
        }
        // this.salaryGradedeleteIds.push(id)
      }
      console.log(this.salaryGradedeleteIds, "123321");
      if ($event.checked == false) {
        var index = this.salaryGradedeleteIds.indexOf(id);
        if (index > -1) {
          this.salaryGradedeleteIds.splice(index, 1);
        }
        console.log(this.salaryGradedeleteIds);

      }
      if (this.salaryGradedeleteIds.length == 0) {
        this.updateSalGrade = false;
      }

    }
    if (data == 'calander') {
      console.log("1222222", $event.checked, id);
      this.calanderGroupId = id;

      if ($event.checked == true) {
        if (this.calanderdeleteIds) {

          this.calanderdeleteIds = [id];

          this.payrollCalanderList.forEach(element => {
            if (id == element._id) {
              element.isChecked = true
            }
            else {
              element.isChecked = false;
            }
          });
        }
        else {
          this.calanderdeleteIds.push(id)
        }
      }
      console.log(this.calanderdeleteIds, "123321");
      if ($event.checked == false) {
        var index = this.calanderdeleteIds.indexOf(id);
        if (index > -1) {
          this.calanderdeleteIds.splice(index, 1);
        }
        console.log(this.calanderdeleteIds);

      }
    }

  }



  pageEvents(event: any) {
    console.log(event);
    this.pageEvents1.pageno = event.pageIndex + 1;
    this.pageEvents1.perpage = event.pageSize;
    this.getPayrollGroup();
  }

  payrollGroupAdd() {
    this.isValid = true;
    if (this.payrollGroup.name && this.payrollGroup.frequency && this.payrollGroup.code) {
      var postData = {
        companyId: this.companyId,
        payroll_group_name: this.payrollGroup.name.trim(),
        pay_frequency: this.payrollGroup.frequency,
        pay_group_code: this.payrollGroup.code.trim()
      }

      this.payRollService.addPayrollGroup(postData)
        .subscribe((res: any) => {
          console.log(res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Group", res.message, "success")
            jQuery("#myModal").modal("hide");
            this.getPayrollGroup()
            this.payrollGroup.code = '';
            this.payrollGroup.frequency = '';
            this.payrollGroup.name = '';
            this.isValid = false;
          }
        },
          (err) => {
            console.log(err);
            this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Group", err.error.message, "error")

          })
    }

  }

  payrollGroupName() {
    console.log(this.payrollGroup.name);

  }

  payGroupCode() {
    console.log(this.payrollGroup.code);

  }

  editPayrollGroupName() {
    if (this.deleteIds.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Payrollgroup', "please select only one payroll Group", "error")
    }
    if (this.deleteIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Payrollgroup', "please select one payroll Group", "error")

    }
    if (this.deleteIds.length == 1) {
      jQuery("#myModal1").modal("show");
      this.getSinglePayrollGroup(this.deleteIds[0])
    }

  }


  getSinglePayrollGroup(id) {
    this.payRollService.getSinglePayrollGroup(this.companyId, id)
      .subscribe((res: any) => {
        console.log(res);
        this.SinglePayrollGroup = res.data
        var result_frequency = this.SinglePayrollGroup.pay_frequency
        if (result_frequency == 'Weekly') {
          this.payrollCalendar.number_of_days_in_a_cycle = 7

        }
        if (result_frequency == 'Bi-Weekly') {
          this.payrollCalendar.number_of_days_in_a_cycle = 14

        }
        if (result_frequency == 'Semi-Monthly') {
          this.payrollCalendar.number_of_days_in_a_cycle = 15

        }
        if (result_frequency == 'Monthly') {
          this.payrollCalendar.number_of_days_in_a_cycle = 31

        }
        this.stattsDateChange()

      },
        (err) => {
          console.log(err);

        })
  }

  deletePayrollGroupName() {
    var postData = {
      companyId: this.companyId,
      _ids: this.deleteIds
    }
    this.payRollService.deletePaygroups(postData)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Group", res.message, "success")
          this.deleteIds = []
        }
        this.getPayrollGroup()
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Group', err.error.message, 'error')
        })

  }


  // ========================================================================salaryGrade=================================================

  addSalaryGrade() {
    var x = {
      isChecked: false,
      grade: '',
      min: '',
      mid: '',
      max: '',
      average_salary: '',
      average_market_value: '',
      midSalValueError: false,
      minSalValueError: false,
      maxSalValueError: false,

    }
    this.isValidSal = false;
    this.addSaralySubmit = false
    this.salryGrades.push(x)
    console.log(this.salryGrades, "this.salryGrades");


  }

  selectedSalGrade(i, event, data) {
    console.log(i, event, data);
    // var deleteSalGradeBand = []
    this.deleteSalGradeBand = []
    if (event == true) {
      this.chooseSalGrades.push(i)
      if (data._id) {
        this.salGradeGradeBands.push(data)
      }
    }
    if (event == false) {
      this.chooseSalGrades.splice(this.chooseSalGrades[i], 1);
      if (data._id) {
        this.salGradeGradeBands.splice(data, 1)
      }
    }
    console.log(this.chooseSalGrades, this.salGradeGradeBands, "this.chooseSalGrades");
    this.salGradeGradeBands.forEach(element => {
      if (element._id) {
        this.deleteSalGradeBand.push(element._id)
      }
    });


  }
  deleteSalGradeBands() {
    var postData = {
      companyId: this.companyId,
      salary_grade_id: this.salaryGradedeleteIds[0],
      _ids: this.deleteSalGradeBand
    }
    this.payRollService.deleteSalaryGradeBands(postData)
      .subscribe((res: any) => {
        console.log(res);

        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, 'success')
          this.getSingleSalaryGrade(this.salaryGradedeleteIds[0]);
          // this.salaryGradedeleteIds = [];
          this.salGradeGradeBands = [];
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', err.error.message, 'error')
        })
  }

  deleteSalaryGrades() {
    console.log(this.chooseSalGrades, this.salryGrades);

    for (var i = 0; i < this.chooseSalGrades.length; i++) {
      this.salryGrades.splice(this.chooseSalGrades[i], 1)
    }
    console.log(this.chooseSalGrades, this.salryGrades, "after");

  }

  salaryGradeSubmit() {
    this.isValidSal = true;
    this.salryGrades.forEach(element => {
      if (element.min == '' || element.mid == '' || element.max == '' || element.grade == '') {
        this.gradeBandSubmit = true;
      }
    });
    console.log(this.gradeBandSubmit);


    if (this.salGrade.name && this.payrollGroup.frequency) {

      var postData = {
        companyId: this.companyId,
        salary_grade_name: this.salGrade.name,
        pay_frequency: this.payrollGroup.frequency,
        grades: []

      }
      if (this.salryGrades.length >= 1) {
        postData['grades'] = this.salryGrades
      }
      postData.grades = postData.grades.map(x => {
        delete x.isChecked
        delete x.minSalValueError
        delete x.midSalValueError
        delete x.maxSalValueError
        return x
      })

      console.log(postData);

      console.log(postData, "sending data");
      if (!this.gradeBandSubmit) {
        this.payRollService.addSalaryGrade(postData)
          .subscribe((res: any) => {
            console.log(res);
            if (res.status = true) {
              this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, "success")
              this.salryGrades = [];
              this.salGrade.name = ''
              jQuery("#myModal6").modal("hide");
              this.getsalrayGrades();
              this.isValidSal = false;
              this.salaryGradedeleteIds = [];
            }
          },
            (err) => {
              console.log(err);
              this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', err.error.message, "error")
            })
      }
    }
  }

  salaryGradeUpdate() {

    this.isValidSal = true;
    this.salryGrades.forEach(element => {
      if (element.min == '' || element.mid == '' || element.max == '' || element.grade == '') {
        this.gradeBandSubmit = true;
      }
    });
    if (this.salGrade.name && this.payrollGroup.frequency) {
      // if(this.salryGrades[i].maxSalValueError)
      var postData = {
        _id: this.salaryGradedeleteIds[0],
        companyId: this.companyId,
        salary_grade_name: this.salGrade.name,
        pay_frequency: this.payrollGroup.frequency,
        grades: []

      }
      if (this.salryGrades.length >= 1) {
        postData['grades'] = this.salryGrades
      }
      postData.grades = postData.grades.map(x => {
        delete x.isChecked
        delete x.minSalValueError
        delete x.midSalValueError
        delete x.maxSalValueError
        return x
      })
      // this.salryGrades.forEach(element => {
      //   if(element.min != '' && element.mid && element.max && element.grade){

      //   }
      // });

      console.log(postData, "sending data");

      if (this.salryGrades.length && !this.gradeBandSubmit) {
        this.payRollService.updateSalaryGrade(postData)
          .subscribe((res: any) => {
            console.log(res);
            if (res.status = true) {
              this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, "success")
              this.salryGrades = [];
              this.salaryGradedeleteIds = [];
              this.salGradeGradeBands = [];
              this.salGrade.name = ''
              jQuery("#myModal6").modal("hide");
              this.isValidSal = false;
              this.updateSalGrade = false;
              this.getsalrayGrades();

            }
          },
            (err) => {
              console.log(err);
              this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade band', err.error.message, "error")
            });

      }
      if (this.salryGrades.length == 0) {
        this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade band', 'Please add the Grade', 'error')
      }


    }

  }

  getsalrayGrades() {
    var postData = {
      "companyId": this.companyId,
      "per_page": this.pageEvents1.salaryGradePerpage,
      "page_no": this.pageEvents1.salaryGradePageNo,
      "archive": false
    }

    this.payRollService.getSalrayGradeList(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.salaryGradeList = res.data

        this.salaryGradeCount = res.total_count
      },
        (err) => {
          console.log(err);

        })
  }

  pageEventsSalaryGrade(event) {
    console.log(event);
    this.pageEvents1.salaryGradePageNo = event.pageIndex + 1;
    this.pageEvents1.salaryGradePerpage = event.pageSize;
    this.getsalrayGrades();
  }

  editSalryGradeName() {
    this.updateSalGrade = true;
    this.salGradeGradeBands = []
    if (this.salaryGradedeleteIds.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', "please select only one Salary Grade", "error")
    }
    if (this.salaryGradedeleteIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', "please select one Salary Grade", "error")

    }
    if (this.salaryGradedeleteIds.length == 1) {
      jQuery("#myModal6").modal("show");
      this.getSingleSalaryGrade(this.salaryGradedeleteIds[0]);

    }


  }

  getSingleSalaryGrade(id) {
    this.salryGrades = []
    this.payRollService.getSingleSalaryGrade(this.companyId, id)
      .subscribe((res: any) => {
        console.log(res);
        this.SingleSalaryGrade = res.data
        this.salaryGradeBands = res.grades_total_count
        this.salGrade.name = res.data.salary_grade_name
        this.payrollGroup.frequency = res.data.pay_frequency
        this.SingleSalaryGrade.grades.forEach(element => {
          element['isChecked'] = false;
          this.salryGrades.push(element)

        });

      },
        (err) => {
          console.log(err);

        })
  }



  deleteSAlaryGrades() {
    var postData = {
      companyId: this.companyId,
      _ids: this.salaryGradedeleteIds
    }
    this.payRollService.deleteSalaryGrades(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, 'success');
        this.getsalrayGrades();
        this.salGradeGradeBands = [];
        this.salaryGradedeleteIds = [];
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', err.error.message, 'error');

        })


  }

  minValue(i, event) {
    console.log(i, event);
    this.salrayGradeMinValue = event;
    if (this.salrayGradeMinValue > this.salrayGradeMidValue && this.salrayGradeMinValue > this.salrayGradeMaxValue) {
      console.log("min value");

      this.salryGrades[i].minSalValueError = true;
    }
    if (this.salrayGradeMinValue < this.salrayGradeMidValue && this.salrayGradeMinValue < this.salrayGradeMaxValue) {

      console.log("min value");
      this.salryGrades[i].minSalValueError = false;
    }
    this.gradeBandSubmit = false;
  }
  maxValue(i, event) {
    console.log(i, event);
    this.salrayGradeMaxValue = event;
    if (this.salrayGradeMaxValue < this.salrayGradeMidValue && this.salrayGradeMaxValue < this.salrayGradeMinValue) {
      console.log("min value");

      this.salryGrades[i].maxSalValueError = true;
    }
    if (this.salrayGradeMaxValue > this.salrayGradeMidValue && this.salrayGradeMaxValue > this.salrayGradeMinValue) {
      console.log("min value");
      this.salryGrades[i].maxSalValueError = false;
    }
    console.log(this.salrayGradeMaxValue, this.salrayGradeMinValue);
    this.gradeBandSubmit = false;
  }

  midValue(i, event) {
    console.log(i, event);
    this.salrayGradeMidValue = event;
    if (this.salrayGradeMinValue > this.salrayGradeMidValue) {
      console.log("min value");

      this.salryGrades[i].midSalValueError = true;
    }
    if (this.salrayGradeMinValue < this.salrayGradeMidValue) {
      console.log("min value");
      this.salryGrades[i].midSalValueError = false;
    }
    this.gradeBandSubmit = false;
  }


  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }



  keyPress1(event: any) {
    const pattern = /^[A-Za-z0-9]+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  salaryGradepageEvents(event) {
    this.pageEvents1.pagenoSalaryGradeBand = event.pageIndex + 1;
    this.pageEvents1.perpageSalaryGradeBand = event.pageSize;
    this.getSingleSalaryGradeBands(this.salaryGradedeleteIds[0])
  }


  getSingleSalaryGradeBands(id) {
    var postData = {
      "companyId": this.companyId,
      "salary_grade_id": id,
      "per_page": this.pageEvents1.perpageSalaryGradeBand,
      "page_no": this.pageEvents1.pagenoSalaryGradeBand
    }

    this.payRollService.getSingleSalaraygradeBandsPagination(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.salryGrades = []
        this.SingleSalaryGrade = res.data
        this.salaryGradeBands = res.total_count
        this.salGrade.name = res.data.salary_grade_name
        this.SingleSalaryGrade.forEach(element => {
          element['isChecked'] = false;
          this.salryGrades.push(element)
        });
      },
        (err) => {

        })
  }




  // ====================================================================================payroll calandre===========================================================================================

  previewCalander() {
    this.isValidCalander = true;
    if (this.payrollCalendar.calendar_name && this.payrollCalendar.first_cycle_start_date && this.payrollCalendar.number_of_cycles && this.payrollCalendar.number_of_days_in_a_cycle) {
      console.log(this.payrollCalendar);
      // if (parseInt(this.payrollCalendar.number_of_cycles) <= 53) {
      this.payRollService.previewCalander(this.payrollCalendar)
        .subscribe((res: any) => {
          console.log(res);
          this.payrollCalanderCycle = res.data
        },
          (err) => {

          })
    }
    // }
  }

  numberOfCycle() {
    console.log(this.payrollCalendar.number_of_cycles);
    if (parseInt(this.payrollCalendar.number_of_cycles) >= 53) {
      this.showWeeksError = true;
    }
    if (parseInt(this.payrollCalendar.number_of_cycles) <= 53) {
      this.showWeeksError = false;
    }

  }

  singlePayrollCalander() {
    var postData = {

    }
    postData = Object.assign(postData, this.payrollCalendar)
    postData['companyId'] = this.companyId;
    postData['paycycle_dates'] = this.payrollCalanderCycle;
    console.log(postData, "sending postdaata");
    if (!postData['assigned_payroll_group']) {
      delete postData['assigned_payroll_group']
    }
    this.isValidCalander = true;
    if (this.payrollCalendar.calendar_name && this.payrollCalendar.first_cycle_start_date && this.payrollCalendar.number_of_cycles && this.payrollCalendar.number_of_days_in_a_cycle) {
      console.log(this.payrollCalendar);
      if (parseInt(this.payrollCalendar.number_of_cycles) <= 53) {

        this.payRollService.singlePayrollCalanderAdd(postData)

          .subscribe((res: any) => {
            console.log(res);
            if (res.status == true) {
              this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", res.message, 'success')
              jQuery("#myModal3").modal("hide");
              this.getPayrollCalandereList();
              this.payrollCalendar.calendar_name = '';
              this.payrollCalendar.calendar_year = '';
              this.payrollCalendar.first_cycle_end_date = '';
              this.payrollCalendar.first_cycle_start_date = '';
              this.payrollCalendar.number_of_cycles = '';
              this.payrollCalendar.number_of_days_in_a_cycle = '';
              this.payrollCalendar.assigned_payroll_group = '';
              // this.payrollCalendar={};
              // myForm.reset();
              this.isValidCalander = false;
              this.payrollCalanderCycle = []
            }
          },
            (err) => {
              console.log(err);
              this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", err.error.message, 'error')

            })
      }
    }
  }

  singlePayrollCalanderUpdate() {
    var postData = {

    }
    postData = Object.assign(postData, this.payrollCalendar)
    postData['companyId'] = this.companyId;
    postData['paycycle_dates'] = this.payrollCalanderCycle;
    postData['_id'] = this.calanderdeleteIds[0]
    console.log(postData, "sending postdaata");
    this.payRollService.singlePayrollCalanderUpdate(postData)

      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", res.message, 'success')
          jQuery("#myModal3").modal("hide");
          this.getPayrollCalandereList();
          this.payrollCalendar.calendar_name = '';
          this.payrollCalendar.calendar_year = '';
          this.payrollCalendar.first_cycle_end_date = '';
          this.payrollCalendar.first_cycle_start_date = '';
          this.payrollCalendar.number_of_cycles = '';
          this.payrollCalendar.number_of_days_in_a_cycle = '';
          this.payrollCalendar.assigned_payroll_group = '';
          // this.payrollCalendar={};
          // myForm.reset();
          this.isValidCalander = false;
          this.payrollCalanderCycle = []
          this.calanderdeleteIds = [];
        }
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", err.error.message, 'error')
        })
  }

  stattsDateChange() {

    var result = new Date(this.payrollCalendar.first_cycle_start_date);
    console.log(result, this.payrollCalendar.number_of_days_in_a_cycle);


    result.setDate(result.getDate() + parseInt(this.payrollCalendar.number_of_days_in_a_cycle));
    // return result;
    console.log(result);
    this.payrollCalendar.first_cycle_end_date = result.toISOString()
    console.log(new Date(this.payrollCalendar.first_cycle_end_date).getFullYear());
    this.payrollCalendar.calendar_year = new Date(this.payrollCalendar.first_cycle_end_date).getFullYear()
  }

  getPayrollCalandereList() {
    var postData = {
      companyId: this.companyId,
      per_page: this.pageEvents1.calanderPerPage,
      page_no: this.pageEvents1.calanderPageNo,
      archive: false
    }
    this.payRollService.pageEvents1(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.payrollCalanderList = res.data;
        this.payrollCalanderCount = res.total_count;

      },
        (err) => {
          console.log(err);

        })

  }

  pageEventsCalander(event) {
    console.log(event);
    this.pageEvents1.calanderPageNo = event.pageIndex + 1;
    this.pageEvents1.calanderPerPage = event.pageSize;
    this.getPayrollCalandereList();
  }

  deletePayrollCalander() {

    var postData = {
      companyId: this.companyId,
      _ids: this.calanderdeleteIds
    }
    this.payRollService.deleteCalander(postData)
      .subscribe((res: any) => {
        console.log(res);
        this.swalAlertService.SweetAlertWithoutConfirmation('Payroll calander', res.message, 'success');
        this.getPayrollCalandereList();
        this.calanderdeleteIds = [];

      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation('Payroll calander', err.error.message, 'error');

        })

  }

  payGroupAssign() {
    console.log("vpin", this.payrollCalendar.assigned_payroll_group);
    this.getSinglePayrollGroup(this.payrollCalendar.assigned_payroll_group)

    // var result_frequency = this.SinglePayrollGroup.pay_frequency
    // if (result_frequency == 'Weekly') {
    //   this.payrollCalendar.number_of_days_in_a_cycle = 7

    // }
    // if (result_frequency == 'Bi-Weekly') {
    //   this.payrollCalendar.number_of_days_in_a_cycle = 14

    // }
    // if (result_frequency == 'Semi-Monthly') {
    //   this.payrollCalendar.number_of_days_in_a_cycle = 15

    // }
    // if (result_frequency == 'Monthly') {
    //   this.payrollCalendar.number_of_days_in_a_cycle = 31

    // }
    // this.stattsDateChange()
  }

  openFormCalender() {
    this.addSaralySubmit = true;
    // this.salGradeGradeBands = [];
    this.salGrade.name = '';
    this.payrollGroup.frequency = '';
    this.salryGrades = []
    this.payrollCalenderUpdate = false;
    this.salGradeGradeBands = [];
    console.log(this.salaryGradeBands);

  }

  payrollCalenderOpen() {
    this.payrollCalendar.calendar_name = '';
    this.payrollCalendar.assigned_payroll_group = '';
    this.payrollCalendar.number_of_days_in_a_cycle = '';
    this.payrollCalendar.number_of_cycles = '';
    this.payrollCalendar.first_cycle_start_date = '';
    this.payrollCalendar.first_cycle_end_date = '';
    this.payrollCalendar.calendar_year = '';

    this.payrollCalanderCycle = []
    this.payrollCalenderUpdate = false;
    this.today = new Date()
    this.paygroup()
  }

  payrollCalenderEdit() {

    // jQuery("#myModal3").modal("show");

    if (this.calanderdeleteIds.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select only one payroll Calender", "error")
    }
    if (this.calanderdeleteIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select one payroll Calender", "error")

    }
    if (this.calanderdeleteIds.length == 1) {

      this.getSinglePayrollCalender(this.calanderdeleteIds[0])
    }

  }

  startNextCycle() {
    this.currentCylcle = false;
    this.nextCylcle = false;

    jQuery("#myModal4").modal("show");
    this.payRollService.StartNextCycle(this.companyId, this.calanderdeleteIds[0])
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.nextCycle = res.data;
          if (!this.nextCycle.current_paycycle) {
            console.log('1');

            this.currentCylcle = true;
          }
          if (this.nextCycle.current_paycycle) {
            console.log('2');

            this.currentPayCycleDates = this.nextCycle.current_paycycle;
          }
          if (!this.nextCycle.next_pay_cycle) {
            console.log('3');
            this.nextCylcle = true;
          }
          if (this.nextCycle.next_pay_cycle) {
            console.log('4');

            this.nextPayCycleDates = this.nextCycle.next_pay_cycle;

          }
        }
      },
        (err) => {

        })

  }


  reopenPreviousCycle() {
    if (this.calanderdeleteIds.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select only one payroll Calender", "error")
    }
    if (this.calanderdeleteIds.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select one payroll Calender", "error")

    }
    if (this.calanderdeleteIds.length == 1) {
      this.reopenPreviousCyclePopup()
    }

  }
  reopenPreviousCyclePopup() {

    this.payRollService.reopenPreviousCycle(this.companyId, this.calanderdeleteIds[0])
      .subscribe((res: any) => {
        console.log(res);

        if (res.status == true) {
          jQuery("#myModal5").modal("show");

          this.nextCycle1 = res.data;

          if (!this.nextCycle1.previous_cycle) {
            console.log('1');

            this.currentCylcle1 = true;
          }
          if (this.nextCycle1.previous_cycle) {
            console.log('2');

            this.currentPayCycleDates1 = this.nextCycle1.previous_cycle;
          }
          if (!this.nextCycle1.active_cycle) {
            console.log('3');
            this.nextCylcle1 = true;
          }
          if (this.nextCycle1.active_cycle) {
            console.log('4');

            this.nextPayCycleDates1 = this.nextCycle1.active_cycle;

          }
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Previuos Cycle", err.error.message, 'error')
        })
  }

  reopenCycleSubmit() {
    var postData = {
      companyId: this.companyId,
      _id: this.calanderdeleteIds[0],

    }
    this.payRollService.reopenPreviousCycleSubmit(postData)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          jQuery("#myModal5").modal("hide");
        }
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation('Previuos Cycle', err.error.message, 'error')

        })

  }

  payrollCalenderCopy() {
    var postData = {
      companyId: this.companyId,
      _id: this.calanderdeleteIds[0]
    }
    this.payRollService.copyCalender(postData)
      .subscribe((res: any) => {
        console.log(res)
        if (res.status == true) {
          // this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', res.message, 'success')
          this.payrollCalenderUpdate = false;
          this.paygroup()
          jQuery("#myModal3").modal("show");
          this.payrollCalendar = res.data;
          this.calanderdeleteIds = [];
          this.getPayrollCalandereList()
        }

      },
        (err) => {
          console.log(err);

        })
  }

  cycleDataSend() {
    var postData = {
      "companyId": this.companyId,
      "_id": this.calanderdeleteIds[0],
    }
    // if (this.calanderGroupId) {
    //   postData['active_cycle_number'] = 2;

    // }
    this.payRollService.cycleDataSend(postData)
      .subscribe((res: any) => {
        console.log(res)
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Cycle Date", res.message, 'success')
          jQuery("#myModal4").modal("hide");
        }
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Cycle Date", err.error.message, 'success')

        })
  }

  paygroup() {
    var postData = {
      companyId: this.companyId,

    }
    this.payRollService.getPayrollGroup(postData)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.payrollGroupsDropdown = res.data;
          // this.payrollGroupCount = res.total_count;
        }
      },
        (err) => {
          console.log(err);

        })

  }

  getSinglePayrollCalender(id) {


    this.paygroup()

    this.payRollService.getPayrollCalender(this.companyId, id)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.payrollCalendar = res.data
          this.payrollCalenderUpdate = true;
          this.payrollCalendar.assigned_payroll_group = res.data.assigned_payroll_group._id;
          if (res.data.paycycle_dates) {
            this.payrollCalanderCycle = res.data.paycycle_dates
          }
          jQuery("#myModal3").modal("show");

        }
      },
        (err) => {
          console.log(err);

        })
  }






  salaryGradeCancel() {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '350px',
      panelClass: 'confirm-dialog',
      data: {
        text: 'cancel'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      if (result) {
        jQuery("#myModal6").modal("hide");

      }
    });



  }









}
