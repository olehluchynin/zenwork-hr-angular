import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CompanySettingsService } from '../../../../../services/companySettings.service';
import { SiteAccessRolesService } from '../../../../../services/site-access-roles-service.service';
import { AccessLocalStorageService } from '../../../../../services/accessLocalStorage.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
import { DualListComponent } from 'angular-dual-listbox';


@Component({
  selector: 'app-assign-employees',
  templateUrl: './assign-employees.component.html',
  styleUrls: ['./assign-employees.component.css']
})
export class AssignEmployeesComponent implements OnInit {
  structureName: any;
  checked: any;
  StructureValues: Array<any> = [];
  customValues: Array<any> = []
  fieldsName: string
  structureFields: Array<any> = [
    {
      name: 'Business Unit',
      isChecked: true
    }
  ]
  eligibilityArray = [];
  dropdownSettings = {};
  selectedUsers = [];
  selectedItems = [];
  dropdownList = [];
  companyId: any;
  public pageNo: any;
  public perPage: any;
  public count: any;
  previewList = [];

  format = {
    add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
    direction: DualListComponent.LTR, draggable: true, locale: 'da',
  };

  constructor(
    public dialogRef: MatDialogRef<AssignEmployeesComponent>,
    private companySettingsService: CompanySettingsService,
    private siteAccessRolesService: SiteAccessRolesService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.companyId = this.accessLocalStorageService.get('companyId');
    console.log(this.companyId);
    console.log(this.data);

    this.getFieldsForRoles();
    this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
    this.pageNo = 1;
    this.perPage = 10;

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 0,
      maxHeight: 150,
      defaultOpen: true,
      allowSearchFilter: true
    };
  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.previewEmployeeList();
  }

  previewEmployeeList(){

    var finalPreviewUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalPreviewUsers.push(element._id)
    });
    var data:any = {
      per_page: this.perPage,
      page_no: this.pageNo,
      _ids: finalPreviewUsers
    }
    this.companySettingsService.prviewEmployeeList(data).subscribe((res:any)=>{
      console.log(res);
      this.previewList = res.data
      this.count = res.total_count;
      
    },(err)=>{
      console.log(err);
      
    })
  }

  // onItemSelect(item: any) {
   
  //   this.selectedUsers = []
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   for (var i = 0; i < this.selectedItems.length; i++) {
  //     this.selectedUsers.push(this.selectedItems[i].item_id);
  //   }
  //   console.log('selcetd users', this.selectedUsers);
  //    this.previewEmployeeList();
  // }
  // OnItemDeSelect(item: any) {
  //   console.log(item);
  //   console.log(this.selectedItems);
  //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();
  // }
  // onSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];
  //   for (var i = 0; i < items.length; i++) {
  //     this.selectedUsers.push(items[i].item_id);
  //   }
  //   console.log("users", this.selectedUsers);
  //   this.previewEmployeeList();

  // }
  // onDeSelectAll(items: any) {
  //   console.log(items);
  //   this.selectedUsers = [];

  //   console.log('selcetd users', this.selectedUsers);
  //   this.previewEmployeeList();
  // }

  onNoClick(): void {
    this.dialogRef.close();
  }
  // Author: Saiprakash , Date: 15/05/19
  // Get fields for roles
  getFieldsForRoles() {
    this.companySettingsService.getFieldsForRoles()
      .subscribe((res: any) => {
        console.log("12122123", res);
        this.structureFields = res.data;

      }, err => {
        console.log(err);

      })
  }
  // Author: Saiprakash , Date: 15/05/19
  // Get Structure Fields

  getStructureSubFields(fields, isChecked?) {
    console.log(fields, isChecked);
    this.fieldsName = fields;
    this.checked = isChecked
    if (isChecked == true) {
      this.eligibilityArray = [];
      this.structureFields.forEach(element => {
        if (element.name !== fields) {
          element.isChecked = false
        }
      })
      this.companySettingsService.getStuctureFields(fields, 0)
        .subscribe((res: any) => {
          console.log(res, name);
          this.StructureValues = res.default;
          this.customValues = res.custom;
          console.log(this.StructureValues);


        }, err => {
          console.log(err);

        })
    }



  }

  subFieldsStructure($event, value) {
    console.log($event, value);

    // this.eligibilityArray = [];
    if ($event.checked == true) {
      this.eligibilityArray.push(value)
    }
    console.log(this.eligibilityArray, "123321");
    if ($event.checked == false) {
      var index = this.eligibilityArray.indexOf(value);
      if (index > -1) {
        this.eligibilityArray.splice(index, 1);
      }
      console.log(this.eligibilityArray);

    }
    var postdata = {
      field: this.fieldsName,
      chooseEmployeesForRoles: this.eligibilityArray
    }
    console.log(postdata);
    this.siteAccessRolesService.chooseEmployeesData(postdata)
      .subscribe((res: any) => {
        console.log("emp data", res);
        this.dropdownList = []
        for (var i = 0; i < res.data.length; i++) {
          if ($event.checked == true)
            this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName })
        }
        console.log(this.dropdownList, "employee dropdown");

      },
        (err) => {
          console.log(err);

        })
  }

  // Author: Saiprakash , Date: 15/05/19
  // Assign Training To Employees

  assignTraining() {

    var finalSelectedUsers = [];
    
    this.selectedUsers.forEach(element => {
      finalSelectedUsers.push(element._id)
    });

    var assignData: any = {
      companyId: this.companyId,
      trainingId: this.data,
      _ids: finalSelectedUsers
    }
    this.companySettingsService.assignTrainingToEmployees(assignData).subscribe((res: any) => {
      console.log(res);
      this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
      this.onNoClick();
    }, (err) => {
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
    })
  }

  onSelectUsers(selectSpecificPerson){
    console.log("users",selectSpecificPerson);
   
    console.log('selcetd users', this.selectedUsers);
     this.previewEmployeeList();
    
  }

}
