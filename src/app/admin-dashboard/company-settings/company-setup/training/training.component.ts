import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PageEvent } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { ActivatedRoute } from '@angular/router';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AssignEmployeesComponent } from './assign-employees/assign-employees.component';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { computeStyle } from '@angular/animations/browser/src/util';



declare var jQuery: any;


@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {

  constructor(
    private trainingService: CompanySettingsService,
    private activate: ActivatedRoute,
    private swalService: SwalAlertService,
    private router: Router,
    public dialog: MatDialog,
    private siteAccessRolesService: SiteAccessRolesService
  ) { }

  public trainingForm: FormGroup
  public trainingData = [];
  public id: any;
  public trainingDataEdit: any;
  public trainingSearch: FormGroup;
  public pageNo: any;
  public perPage: any;
  public count: any;
  public selectTrainingData: any = [];
  public datatrain: boolean = false;
  public filterName: any;
  public filterTypeName: any;
  public filterTimeName: any;
  public subjectFields: any;
  public trainingSubjectData: any;
  public trainingTypeData: any;
  public trainingAverageTime: any;
  public trainingId: any;
  public employeeData = [];
  public activeTrainings = [];
  companyID: any;
  userID: any;
  trainingCheckDeleteID = [];
  trainingCheckIdPush = [];
  activeStatus: any;
  archiveLength = [];


  @ViewChild('openEdit') openEdit: ElementRef

  // MatPaginator Output
  pageEvent: PageEvent;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 15, 25, 100];

  trainingDisable: boolean = false;

  ngOnInit() {
    this.activeStatus = ""

    this.trainingSearch = new FormGroup({
      search: new FormControl("")
    })

    this.trainingForm = new FormGroup({
      trainingName: new FormControl("", Validators.required),
      trainingId: new FormControl("", Validators.required),
      instructorName: new FormControl("", Validators.required),
      trainingSubject: new FormControl("", Validators.required),
      trainingType: new FormControl("", Validators.required),
      timeToComplete: new FormControl("", Validators.required),
      trainingUrl: new FormControl("", Validators.required),
      status: new FormControl("", Validators.required)
    });
    this.pageNo = 1;
    this.perPage = 10;
    this.filterName = '';
    this.filterTypeName = '';
    this.filterTimeName = '';
    this.getAllData();
    this.getActiveTrainings();
    this.onChanges();
    this.employeeSearch();
    this.getSubjectData();


    this.companyID = localStorage.getItem("companyId");
    console.log("company ID", this.companyID);
    this.companyID = this.companyID.replace(/^"|"$/g, "");
    console.log("company IDDDD", this.companyID);

    this.userID = localStorage.getItem("employeeId");
    console.log("user iddddddddd", this.userID);
    this.userID = this.userID.replace(/^"|"$/g, "");

  }

  selectTraining(id) {
    this.trainingId = id;
  }

  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;
        console.log(this.employeeData);


      },
        (err) => {
          console.log(err);

        })
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(AssignEmployeesComponent, {
      width: '1200px',
      height: '800px',
      data: this.trainingId
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);

    });
  }

  //Page Event
  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.getAllData();
  }

  //Search on filter
  onChanges() {
    this.trainingSearch.controls['search'].valueChanges
      .subscribe(val => {
        this.getAllData();
      });

  }


  // Training classes Submit
  addTraining() {
    console.log("Trainings", this.trainingForm.value)

    if (this.trainingForm.valid) {
      if (this.trainingDataEdit && this.trainingDataEdit._id) {
        var updateDate = this.trainingForm.value;
        updateDate._id = this.trainingDataEdit._id;

        this.trainingService.updateTrainingData(updateDate)
          .subscribe((res: any) => {
            console.log("Updatinggg", res)
            jQuery("#myModal").modal("hide");
            if (res.status == true) {
              this.swalService.SweetAlertWithoutConfirmation("Updated", "Training updated successfully", "success")
            }
            this.getAllData();
            this.getActiveTrainings();
            this.trainingForm.reset();
            this.trainingCheckIdPush = [];
          });
      } else {
        this.trainingService.addTraining(this.trainingForm.value)
          .subscribe((response: any) => {
            console.log("Add Trainings", response)
            jQuery("#myModal").modal("hide");
            if (response.status == true) {
              this.swalService.SweetAlertWithoutConfirmation("Training", "Training created successfully", "success")
              this.getAllData();
              this.getActiveTrainings();
            }
            this.trainingForm.reset();

          }, err => {
            this.swalService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
          });
      }

    } else {
      this.trainingForm.get('trainingName').markAsTouched();
      this.trainingForm.get('trainingId').markAsTouched();
      this.trainingForm.get('trainingSubject').markAsTouched();
      this.trainingForm.get('trainingType').markAsTouched();
      this.trainingForm.get('timeToComplete').markAsTouched();
      this.trainingForm.get('trainingUrl').markAsTouched();
      this.trainingForm.get('status').markAsTouched();

    }

  }

  //Filter Training Subject
  filter(event) {
    this.filterName = event;
    console.log("Eventttt", event)
    this.getAllData();
  }

  //Filter Training Type
  filterType(event) {
    this.filterTypeName = event;
    this.getAllData();
  }

  //Filter Training Time to Complete
  filterTime(event) {
    this.filterTimeName = event;
    this.getAllData();
  }

  // Get the Training Data
  getAllData() {
    console.log("Pagesss", this.pageNo, this.perPage)
    
    this.trainingService.viewTrainingData({
      searchQuery: this.trainingSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage,
      trainingSubject: this.filterName, trainingType: this.filterTypeName, timeToComplete: this.filterTimeName, status: this.activeStatus, archive: false
    })
      .subscribe((res: any) => {
        console.log("Get the Training Data", res)
        this.count = res.count;
        this.trainingData = res.data;
        // this.trainingData.forEach(element => {
        //   if(element.archive == true){
        //     this.archiveLength.push(element)
        //   }
        // });
       
      });
  }

  getActiveTrainings(){
    this.trainingService.viewTrainingData({
     status: 1
    }).subscribe((res: any) => {
      console.log(res);
      
      this.activeTrainings = res.data;
    }, (err)=>{
      console.log(err);
      
    });
  }

  // Delete the Training Data
  deleteData() {

    console.log("Edit IDDDDD")
    this.selectTrainingData = this.trainingData.filter(trainingCheck => {
      return trainingCheck.isChecked;
    });
    if (this.selectTrainingData.length == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Select any Data", "", "")
    } else {
      var deleteData = {
        companyId: this.companyID,
        _ids: this.trainingCheckIdPush
      }
      this.trainingService.TrainingDelete(deleteData)
        .subscribe((res: any) => {
          console.log("Deletee", res)
          this.getAllData();
          this.getActiveTrainings();
          this.trainingCheckIdPush = [];
          if (res.status == true) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Deleted successfully", "success")
          }
        });
    }

  }



  //Edit the Training Data
  edit() {
    console.log("Edit IDDDDD")
    this.selectTrainingData = this.trainingData.filter(trainingCheck => {
      return trainingCheck.isChecked;
    });
    console.log("AAAAAAAAA", this.selectTrainingData)
    if (this.selectTrainingData.length > 1) {
      this.swalService.SweetAlertWithoutConfirmation("Select only one data", "", "")

    } else if (this.selectTrainingData.length == 0) {
      this.swalService.SweetAlertWithoutConfirmation("Select any one", "", "");

    }
    else {
      this.openEdit.nativeElement.click();
      this.trainingService.EditTrainingData(this.selectTrainingData[0]._id)
        .subscribe((res: any) => {
          console.log("Edittt IDD", res)
          this.trainingDataEdit = res.data;
          console.log("54545454545454545454", this.trainingDataEdit)
          this.trainingForm.patchValue(this.trainingDataEdit);
          console.log("12345678", this.trainingForm)

        });
    }

  }
  saveandNext() {
    this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/payroll"]);
  }

  // Author:Suresh M, Date:15-04-19
  // Get all data in Training dropdownn
  getSubjectData() {
    this.trainingService.getAllTraniningData()
      .subscribe((res: any) => {
        console.log("Get All Subjects Dataa", res)
        this.subjectFields = res.temp;
        this.trainingSubjectData = this.subjectFields['Training Subject'],
          this.trainingTypeData = this.subjectFields['Training Type'],
          this.trainingAverageTime = this.subjectFields['Training - Average Time to Complete'],

          console.log("Get All Subjects 21345768", this.subjectFields)
      });
  }


  // Author:Suresh M, Date:10-08-19
  // cancelTraining
  cancelTraining() {
    this.trainingForm.reset();
  }

  // Author:Suresh M, Date:15-04-19
  // trainingCheckID
  trainingCheckID(event, id) {

    if (event.checked == true) {
      this.trainingCheckIdPush.push(id);

    }
    console.log("push iddddddd", this.trainingCheckIdPush);

    if (event.checked == false) {
      var index = this.trainingCheckIdPush.indexOf(id);
      if (index > -1) {
        this.trainingCheckIdPush.splice(index, 1)
      }
      console.log("uncheck idd", this.trainingCheckIdPush);
    }


  }

  // Author:Suresh M, Date:15-04-19
  // Alphabets
  alphabets(event) {
    return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32)
  }

  // Author:Suresh M, Date:15-04-19
  // keyPress
  keyPress(event: any) {
    const pattern = /^[A-Za-z0-9]+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  statusClick(event) {
    console.log("activee", event);
    this.activeStatus = event;
    console.log("AAAAAAAAAA", this.activeStatus)
    this.getAllData();
  }

  // Author:Suresh M, Date:10-08-19
  // clickArchive
  clickArchive() {

    var archiveData:any = {
      companyId: this.companyID,
      _ids: this.trainingCheckIdPush,
      archive: true
    }
    console.log(archiveData);
    

    this.trainingService.archiveTraining(archiveData)
      .subscribe((res: any) => {
        console.log("archive data", res);
       
        this.getAllData();
        this.trainingCheckIdPush = [];
      },(err)=>{
        console.log(err);
        
      });

  }

  // Author:Suresh M, Date:10-08-19
  // clickUnArchive
  clickUnArchive() {
    this.trainingService.unArchiveTraining()
      .subscribe((res: any) => {
        console.log("unarchive", res);
        this.getAllData();
      },(err)=>{
        console.log(err);
        
      });

  }



}



