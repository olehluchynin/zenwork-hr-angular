import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { MessageService } from '../../../../services/message-service.service';
import { ConfirmationComponent } from '../../../../shared-module/confirmation/confirmation.component';

import { MatDialog } from '@angular/material';
import { OnboardingService } from '../../../../services/onboarding.service';

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.css']
})
export class GeneralSettingsComponent implements OnInit {
  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private router: Router,
    private companySetupService: CompanySettingsService,
    private swalAlertService: SwalAlertService,
    private messageService: MessageService,
    public dialog: MatDialog,
    private onboardingService:OnboardingService
  ) { }
  public selectedField = false;
  public companySetup;
  public isValid: boolean = false;
  tempFirstMail: any;
  tempSecondMail: any;
  emialSecondDisable: boolean = true;
  emialFirstDisable: boolean = true;
  text: any;
  note: any;
  public companyContactInfo = {

    companyDetails: {
      primaryContact: {
        email: '',
        extention: "",
        isEmailVerified: '',
        // name: "test",
        phone: "",
        mobilePhone: '',
      },
      secondaryContact: {
        extention: "",
        isEmailVerified: false,
        // name: "test",
        email: "",
        phone: "",
        mobilePhone: ''
      },
      primaryAddress: {
        address1: '',
        address2: '',
        city: '',
        country: '',
        state: '',
        zipcode: '',
        phone: ''
      },
      name: '',
      fein: ''
    },
    settings: {
      general: {
        bootAllUsersNeeded: false,
        lockAccessNeeded: false,
        messageOnFrontPage: '',
        messageOnFrontPageNeeded: false
      }
    }
  }

  mobilePhoneNumeric: boolean = false;
  phoneNumeric: boolean = false;
  extentionNumeric: boolean = false;
  mobilephoneSecondary: boolean = false;
  extentionSecondary: boolean = false;
  phoneSecondary: boolean = false;
  phoneCompanyAddress: boolean = false;
  zipcodeCompanyAddress: boolean = false;

  // public digits = ["1digit", "2digit", "3digit", "4digit", "5digit", "6digit"]
  // public numbers = ["01", "001", "0001", "00001", "000001", "0000001"]

  public generalForm: FormGroup;
  allStructureFieldsData:any;
  countryArray = [];
  stateArray = [];
  
  ngOnInit() {

    this.getGeneralSettings();
    var cId = JSON.parse(localStorage.getItem('companyId'))
    this.getStandardAndCustomFieldsforStructure(cId);
  }

  /*Description: go to previous page 
author : vipin reddy */
  previousPage() {
    this.router.navigate(['/admin/admin-dashboard/company-settings']);
  }
  keyPress1(event: any) {
    const pattern = /^[a-zA-Z]*$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }






  /* Description: Get General Settings 
  author : vipin reddy */

  getGeneralSettings() {
    this.companySetupService.getGeneralSettings()
      .subscribe(
        (data: any) => {

          if (data.status == true) {
            console.log("res", data)
            this.companyContactInfo.settings.general = data.settings.general;
            this.companyContactInfo.companyDetails.name = data.companyDetails.name;
            this.companyContactInfo.companyDetails.fein = data.companyDetails.fein;
            this.companyContactInfo.companyDetails.primaryAddress = data.companyDetails.primaryAddress;
            this.companyContactInfo.companyDetails.primaryContact = data.companyDetails.primaryContact;
            this.companyContactInfo.companyDetails.secondaryContact = data.companyDetails.secondaryContact;
            this.tempFirstMail = data.companyDetails.secondaryContact
            this.tempSecondMail = data.companyDetails.secondaryContact;
            // this.swalAlertService.SweetAlertWithoutConfirmation("General Settings", data.message, "success");

          }
          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("General Settings", data.message, "error");
          }
        },
        (err: HttpErrorResponse) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("General Settings", err.error.message, "error");
        }
      )
  }

  /* Get General Settings Ends*/

  /* 
  author : vipin reddy
  Description:country change function starts*/
  comapanyConactCountrty() {

  }

  /*country chnage ends*/

  /* author : vipin reddy
  Description: save and update general settings data*/
  saveCompanySetup() {
    console.log(this.companyContactInfo);
    this.isValid = true;

    // this.companyContactInfo.companyDetails.primaryContact.name = "test";
    // this.companyContactInfo.companyDetails.secondaryContact.name = "test"
    if (this.companyContactInfo.companyDetails.primaryAddress.address1 &&
      this.companyContactInfo.companyDetails.primaryAddress.city &&
      this.companyContactInfo.companyDetails.primaryAddress.country && this.companyContactInfo.companyDetails.primaryAddress.phone &&
      this.companyContactInfo.companyDetails.primaryAddress.state && this.companyContactInfo.companyDetails.primaryAddress.zipcode &&
      this.companyContactInfo.companyDetails.fein && this.companyContactInfo.companyDetails.name &&
      this.companyContactInfo.companyDetails.primaryContact.email && this.companyContactInfo.companyDetails.primaryContact.extention &&
      this.companyContactInfo.companyDetails.primaryContact.mobilePhone &&
      this.companyContactInfo.companyDetails.primaryContact.phone && this.companyContactInfo.companyDetails.secondaryContact.phone &&
      this.companyContactInfo.companyDetails.secondaryContact.mobilePhone &&
      this.companyContactInfo.companyDetails.secondaryContact.extention && !this.phoneNumeric && !this.mobilePhoneNumeric && !this.extentionNumeric &&
      !this.mobilephoneSecondary && !this.extentionSecondary && !this.phoneSecondary && !this.zipcodeCompanyAddress && !this.phoneCompanyAddress) {

      this.companySetupService.updateGeneralSettings(this.companyContactInfo)
        .subscribe(
          (res: any) => {
            console.log(res);
            if (res.status == true) {
              localStorage.setItem('nextTab', 'in')
              this.emialSecondDisable = false;
              this.emialFirstDisable = false;
              // this.messageService.sendMessage('general')
              this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/field-maintenance"])
            }
          },
          (err: any) => {
            console.log(err);

          })
    }
  }
  /*save and update generalsetings ends*/

  /* author : vipin reddy
  Description: Email verification on general-settings (primary and secondary)*/
  sendVerificationEmail(email) {
    var data = {
      email: email,
      name: this.companyContactInfo.companyDetails.name
    }
    console.log(data);

    this.companySetupService.sendVerificationEmail(data)
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Send Verification Link", res.message, "success")
          }
        },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Send Verification Link", err.message, "error")
        })
  }
  /*Email verification on general-settings (primary and secondary) Ends*/
  emailStatus2() {
    console.log("111111111123", this.companyContactInfo.companyDetails.secondaryContact.email);
    if (this.tempSecondMail != this.companyContactInfo.companyDetails.secondaryContact.email) {
      this.companyContactInfo.companyDetails.secondaryContact.isEmailVerified = false;
      // this.emialSecondDisable = false;
    }
    if (this.tempSecondMail == this.companyContactInfo.companyDetails.secondaryContact.email) {
      this.companyContactInfo.companyDetails.secondaryContact.isEmailVerified = true;
    }
  }

  emailStatus(event) {

  }

  cancelReset() {
    this.text = "The data entered will be lost,";
    this.note = "Would you like to proceed";
    this.openDialog();
  }


  openDialog(): void {

    let dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '550px',
      data: { txt: this.text, note: this.note },
      panelClass: 'confirm-class'

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {

        this.getGeneralSettings();
      }
      // if (data == 'delete') {
      //   this.deleteSingleCategory(id)
      // }



    });
  }

  phoneNumberChange(contact, data) {
    console.log(contact, data);

    // console.log(this.companyContactInfo.companyDetails.primaryContact.mobilePhone);
    // console.log(this.companyContactInfo.companyDetails.primaryContact.extention);
    // console.log(this.companyContactInfo.companyDetails.primaryContact.phone);
    // if(contact == 'secondaryContact'){
    //   var str = this.companyContactInfo.companyDetails[contact][data]
    // }
    var str = this.companyContactInfo.companyDetails[contact][data]
    console.log(str);

    if (str.match(/[a-z]/i)) {
      console.log('coming 11');

      // alphabet letters found
      if (data == 'phone' && contact == 'primaryContact') {
        this.phoneNumeric = true;
      }
      if (data == 'extention' && contact == 'primaryContact') {
        this.extentionNumeric = true;
      }
      if (data == 'mobilePhone' && contact == 'primaryContact') {
        this.mobilePhoneNumeric = true;
      }
      if (data == 'mobilePhone' && contact == 'secondaryContact') {
        this.mobilephoneSecondary = true;
      }
      if (data == 'extention' && contact == 'secondaryContact') {
        this.extentionSecondary = true;
      }
      if (data == 'phone' && contact == 'secondaryContact') {
        this.phoneSecondary = true;
      }
      if (data == 'phone' && contact == 'primaryAddress') {
        this.phoneCompanyAddress = true;
      }
      if (data == 'zipcode' && contact == 'primaryAddress') {
        this.zipcodeCompanyAddress = true;
      }

    }
    else {
      console.log('coming 222');


      if (data == 'phone' && contact == 'primaryContact') {
        this.phoneNumeric = false;
      }
      if (data == 'extention' && contact == 'primaryContact') {
        this.extentionNumeric = false;
      }
      if (data == 'mobilePhone' && contact == 'primaryContact') {
        this.mobilePhoneNumeric = false;
      }
      if (data == 'mobilePhone' && contact == 'secondaryContact') {
        this.mobilephoneSecondary = false;
      }
      if (data == 'extention' && contact == 'secondaryContact') {
        this.extentionSecondary = false;
      }
      if (data == 'phone' && contact == 'secondaryContact') {
        this.phoneSecondary = false;
      }
      if (data == 'phone' && contact == 'primaryAddress') {
        this.phoneCompanyAddress = false;
      }
      if (data == 'zipcode' && contact == 'primaryAddress') {
        this.zipcodeCompanyAddress = false;
      }
    }

  }
  getStandardAndCustomFieldsforStructure(cID) {
    this.onboardingService.getStructureFields(cID)
      .subscribe((res: any) => {
        console.log("structure fields", res);
        this.allStructureFieldsData = res.data;

        if (this.allStructureFieldsData['Country'].custom.length > 0) {
          this.allStructureFieldsData['Country'].custom.forEach(element => {
            this.countryArray.push(element)
          });
        }
        if (this.allStructureFieldsData['Country'].default.length > 0) {
          this.allStructureFieldsData['Country'].default.forEach(element => {
            this.countryArray.push(element)
          });
          console.log("22222country", this.countryArray);
        }
        if (this.allStructureFieldsData['State'].custom.length > 0) {
          this.allStructureFieldsData['State'].custom.forEach(element => {
            this.stateArray.push(element)
          });
        }
        if (this.allStructureFieldsData['State'].default.length > 0) {
          this.allStructureFieldsData['State'].default.forEach(element => {
            this.stateArray.push(element)
          });
          console.log("22222", this.stateArray);
        }
      },
        (err) => {
          console.log(err);

        })
  }



}
