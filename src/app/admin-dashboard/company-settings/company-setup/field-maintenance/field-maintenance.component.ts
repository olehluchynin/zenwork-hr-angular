import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormControl, FormGroup } from '@angular/forms';
import { isThisISOWeek } from 'date-fns';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';
import { Router } from '@angular/router';
import { EmployeeService } from '../../../../services/employee.service';
@Component({
  selector: 'app-field-maintenance',
  templateUrl: './field-maintenance.component.html',
  styleUrls: ['./field-maintenance.component.css']
})
export class FieldMaintenanceComponent implements OnInit {
  modalRef: BsModalRef;
  responseObj = {
    Personal: {
      hide: false,
      fields: {
        First_Name: { format: "Alpha", hide: false, required: false, show: false },
        Middle_Name: { format: "Alpha", hide: false, required: false, show: false },
        Last_Name: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Preferred_Name: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Salutation: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Date_of_Birth: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Gender: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Maritial_Status: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Maritial_Status_Effective_Date: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Veteran_Status: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        SSN: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Race: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        },
        Ethnicity: {
          format: "Alpha",
          hide: false,
          required: false,
          show: false
        }
      }
    },
    Job: {
      hide: false,
      fields: {
        Archive_Status: { show: false, format: "Alpha", hide: true, required: true },
        Custom_1: { show: false, format: "Alpha", hide: true, required: true },
        Custom_2: { show: false, format: "Alpha", hide: true, required: true },
        EEO_Job_Category: { show: false, format: "Alpha", hide: true, required: true },
        Employee_Id: { show: false, format: "Alpha", hide: true, required: true },
        Employee_Self_Service_Id: { show: false, format: "Alpha", hide: true, required: true },
        Employee_Type: { show: false, format: "Alpha", hide: true, required: true },
        FLSA_Code: { show: false, format: "Alpha", hide: true, required: true },
        Hire_Date: { show: false, format: "Alpha", hide: true, required: true },
        Rehire_Date: { show: false, format: "Alpha", hide: true, required: true },
        Rehire_Status: { show: false, format: "Alpha", hide: true, required: true },
        Reporting_Location: { show: false, format: "Alpha", hide: true, required: true },
        Site_AccessRole: { show: false, format: "Alpha", hide: true, required: true },
        Specific_Workflow_Approval: { show: false, format: "Alpha", hide: true, required: true },
        Termination_Date: { show: false, format: "Alpha", hide: true, required: true },
        Termination_Reason: { show: false, format: "Alpha", hide: true, required: true },
        Type_of_User_Role_type: { show: false, format: "Alpha", hide: true, required: true },
        Vendor_Id: { show: false, format: "Alpha", hide: true, required: true },
        Work_Location: { show: false, format: "Alpha", hide: true, required: true },
      }
    },
    Compensation: {
      hide: false,
      fields: {
        Compensation_ratio: { show: false, format: "Alpha", hide: true, required: true },
        Highly_compensated_Employee_type: { show: false, format: "Alpha", hide: true, required: true },
        Nonpaid_Record: { show: false, format: "Alpha", hide: false, required: true },
        Pay_frequency: { show: false, format: "Alpha", hide: true, required: true },
        Pay_group: { show: false, format: "Alpha", hide: true, required: true },
        Salary_Grade: { show: false, format: "Alpha", hide: true, required: true },
      }
    },
    Emergency_Contact: {
      hide: false,
      fields: {
        City: { show: false, format: "Alpha", hide: false, required: false },
        Ext: { show: false, format: "Alpha", hide: false, required: false },
        Home_Email: { show: false, format: "Alpha", hide: false, required: false },
        Home_Phone: { show: false, format: "Alpha", hide: false, required: false },
        Mobile_Phone: { show: false, format: "Alpha", hide: false, required: false },
        Name: { show: false, format: "Alpha", hide: false, required: false },
        Relationship: { show: false, format: "Alpha", hide: false, required: false },
        State: { show: false, format: "Alpha", hide: false, required: false },
        Street_1: { show: false, format: "Alpha", hide: false, required: false },
        Street_2: { show: false, format: "Alpha", hide: false, required: false },
        Work_Email: { show: false, format: "Alpha", hide: false, required: false },
        Work_Phone: { show: false, format: "Alpha", hide: false, required: false },
        Zip: { show: false, format: "Alpha", hide: false, required: false },
      }
    },

    Offboarding: {
      hide: false
    },
    Onboarding: {
      hide: false
    },
    Performance: {
      hide: false
    },
    Time_Off: {
      hide: false
    },
    Training: {
      hide: false
    },
    Notes: {
      hide: false
    },
    Documents: {
      hide: false
    },
    Benefits: {
      hide: false
    },
    Assets: {
      hide: false
    }
  }
  selectedField: any;

  selectedNav: any;
  chooseFields: boolean = true;
  chooseSteps: boolean = false;
  public isValid: boolean = false;
  allFields: boolean = false;
  toggleSelectAll: boolean = false;
  maxLength: any;
  matChecked: any;
  getresponse: any;
  employeeIdentity: any = {
    maskSSN: false,
    idNumber: {
      numberType: '',
      digits: '',
      firstNumber: ''
    }
  }
  customFields = {
    fieldName: '',
    fieldType: '',
    fieldCode: '',
    hide: false,
    required: true
  }

  selectEnable: boolean = true;
  public pageNo: any;
  public perPage: any;
  public count: any;
  empIdentityres = {
    maskSSN: '',
    idNumber: {
      numberType: '',
      digits: '',
      firstNumber: ''
    }
  };
  constructor(
    private modalService: BsModalService,
    private companySettingsService: CompanySettingsService,
    private swalAlertService: SwalAlertService,
    private router: Router,
    private employeeService: EmployeeService,

  ) { }

  public digits = [
    //   { value: 1, viewVlaue: '1 Digit' },
    // { value: 2, viewVlaue: '2 Digits' },
    { value: 3, viewVlaue: '3 Digits' },
    { value: 4, viewVlaue: '4 Digits' },
    { value: 5, viewVlaue: '5 Digits' },
    { value: 6, viewVlaue: '6 Digits' }]
  selectedFieldName = [];
  inputFields = [];
  customArr = [];
  customFieldsData: any;
  singleCustomFieldId: any;
  selectedPackageDetails = [];
  allEmployees = [];
  public searchString: any;
  ngOnInit() {
    this.pageNo = 1;
    this.perPage = 10;
    this.listEmployees();
    this.selectedNav = 'chooseSteps';
    this.getEmpSettings()

  }
  /* Description: Get General Settings 
  author : vipin reddy */

  getEmpSettings() {
    var cId = JSON.parse(localStorage.getItem('companyId'))
    this.companySettingsService.getEmpIdentitySettings(cId)
      .subscribe(
        (res: any) => {
          console.log(res);
          this.empIdentityres = res.data;
          this.employeeIdentity.maskSSN = this.empIdentityres.maskSSN;
          console.log(this.empIdentityres, this.empIdentityres.idNumber);
          if (this.empIdentityres.idNumber.numberType == 'Manual') {
            this.employeeIdentity.idNumber.numberType = this.empIdentityres.idNumber.numberType;


          }
          if (this.empIdentityres.idNumber.numberType == 'system Generated') {
            this.employeeIdentity.idNumber = this.empIdentityres.idNumber;


          }


        },
        (err) => {

        })
  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.listEmployees();
  }

  // Author: Saiprakash G, Date: 23/05/2019
  // Search employees

  searchFilter() {
    console.log(this.searchString);

    this.listEmployees()
  }

  /* Description: maskSsnChange field change
  author : vipin reddy */

  maskSsnChange() {

  }
  /* Description: employee IdNumber Settings Change
  author : vipin reddy */

  empIdNumberSettingsChange() {


  }

  // Author: Saiprakash G, Date: 23/05/2019
  // Get All Employees

  listEmployees() {
    var data: any = {
      per_page: this.perPage,
      page_no: this.pageNo,
      search_query: this.searchString
    }

    this.employeeService.getOnboardedEmployeeDetails(data).subscribe((res: any) => {
      console.log(res);
      this.allEmployees = res.data;
      this.count = res.total_count;

    }, (err) => {
      console.log(err);

    })
  }

  /* Description:  employeeId number length Change
  author : vipin reddy */

  employeeIdformatChange(event) {
    console.log(event);
    this.isValid = false;
    this.employeeIdentity.idNumber.firstNumber = ''

    this.maxLength = event;
  }

  empIdSettingsCancel() {
    this.isValid = true;
    this.employeeIdentity.maskSSN = '';
    this.employeeIdentity.idNumber.numberType = '';
    this.employeeIdentity.idNumber.digits = '';
    this.employeeIdentity.idNumber.digits = '';
  }


  keyPress3(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  /* Description:  modal popup open for View/Edit Stanadard fields
  author : vipin reddy */

  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
    this.getShowFields();
  }
  /* Description: In side modal popup View/Edit Stanadard get showing checked/unchecked fields
   author : vipin reddy */
  getShowFields() {

    this.companySettingsService.getShowFeilds()
      .subscribe(
        (res: any) => {
          console.log("res of get", res);
          this.getresponse = res;
          this.responseObj = res.data.myInfo_sections

        },

        (err: any) => {
          console.log(err);

        })

  }


  /* Description: In side modal popup View/Edit Stanadard send checked/unchecked fields
 author : vipin reddy */
  postShowFields() {
    console.log("11111111111", this.responseObj);
    var data = {
      myInfo_sections: this.responseObj
    }
    this.companySettingsService.postShowFeilds(data)
      .subscribe(
        (res: any) => {
          console.log("res of get", res);
          if (res.status == true) {
            this.selectedNav == 'chooseFields';
            this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success")
            // this.selectedNav == 'chooseFields'

          }

        },
        (err: any) => {
          console.log(err);

        })

  }


  /* Description:  modal popup open for Add /View/Edit Custom fields
  author : vipin reddy */
  openModalWithClass1(template1: TemplateRef<any>) {
    this.selectedPackageDetails = []
    this.modalRef = this.modalService.show(
      template1,
      Object.assign({}, { class: 'gray modal-lg' })
    );
    this.getAllCustomFields();
  }
  /* Description:  modal popup open for Add single customfield
author : vipin reddy */

  openModalWithClass2(template2: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template2,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  /* Description: view/edit standard fields select the checkbox 
  checking/unchecking function on Edit functionality 
  1)personal section
  author : vipin reddy */

  fieldName(event, fieldname) {
    console.log(event, fieldname);
    if (fieldname == 'All') {
      this.allFields = event.checked;
      this.responseObj.Personal.fields.First_Name.show = event.checked;
      this.responseObj.Personal.fields.Middle_Name.show = event.checked;
      this.responseObj.Personal.fields.Last_Name.show = event.checked;
      this.responseObj.Personal.fields.Preferred_Name.show = event.checked;
      this.responseObj.Personal.fields.Salutation.show = event.checked;
      this.responseObj.Personal.fields.Date_of_Birth.show = event.checked;
      this.responseObj.Personal.fields.Gender.show = event.checked;
      this.responseObj.Personal.fields.Maritial_Status.show = event.checked;
      this.responseObj.Personal.fields.Maritial_Status_Effective_Date.show = event.checked;
      this.responseObj.Personal.fields.Veteran_Status.show = event.checked;
      this.responseObj.Personal.fields.SSN.show = event.checked;
      this.responseObj.Personal.fields.Race.show = event.checked;
      this.responseObj.Personal.fields.Ethnicity.show = event.checked;
    }
    else {
      let toChange = fieldname;
      this.responseObj.Personal.fields[toChange].show = event.checked;
    }
  }

  /* 2)job section */
  fieldNameForJob(event, fieldname) {
    console.log(event, fieldname);
    if (fieldname == 'All') {
      this.allFields = event.checked;
      this.responseObj.Job.fields.Employee_Id.show = event.checked;
      this.responseObj.Job.fields.Employee_Type.show = event.checked;
      this.responseObj.Job.fields.Employee_Self_Service_Id.show = event.checked;
      this.responseObj.Job.fields.Work_Location.show = event.checked;
      this.responseObj.Job.fields.Reporting_Location.show = event.checked;
      this.responseObj.Job.fields.Hire_Date.show = event.checked;
      this.responseObj.Job.fields.Rehire_Date.show = event.checked;
      this.responseObj.Job.fields.Termination_Date.show = event.checked;
      this.responseObj.Job.fields.Termination_Reason.show = event.checked;
      this.responseObj.Job.fields.Rehire_Status.show = event.checked;
      this.responseObj.Job.fields.Archive_Status.show = event.checked;
      this.responseObj.Job.fields.FLSA_Code.show = event.checked;
      this.responseObj.Job.fields.EEO_Job_Category.show = event.checked;
      this.responseObj.Job.fields.Vendor_Id.show = event.checked;
      this.responseObj.Job.fields.Specific_Workflow_Approval.show = event.checked;
      this.responseObj.Job.fields.Site_AccessRole.show = event.checked;
      this.responseObj.Job.fields.Type_of_User_Role_type.show = event.checked;
      this.responseObj.Job.fields.Custom_1.show = event.checked;
      this.responseObj.Job.fields.Custom_2.show = event.checked;

    }
    else {
      let toChange = fieldname;
      this.responseObj.Job.fields[toChange].show = event.checked;
    }
  }
  /*3)compensation */
  fieldNameForCompensation(event, fieldname) {
    console.log(event, fieldname);
    if (fieldname == 'All') {
      this.allFields = event.checked;
      this.responseObj.Compensation.fields.Nonpaid_Record.show = event.checked;
      this.responseObj.Compensation.fields.Highly_compensated_Employee_type.show = event.checked;
      this.responseObj.Compensation.fields.Pay_group.show = event.checked;
      this.responseObj.Compensation.fields.Pay_frequency.show = event.checked;
      this.responseObj.Compensation.fields.Salary_Grade.show = event.checked;
      this.responseObj.Compensation.fields.Compensation_ratio.show = event.checked;
    }
    else {
      let toChange = fieldname;
      this.responseObj.Compensation.fields[toChange].show = event.checked;
    }

  }
  /* 3)fieldsNameforEmergency */
  fieldNameForEmergencyContact(event, fieldname) {
    if (fieldname == 'All') {
      this.allFields = event.checked;
      this.responseObj.Emergency_Contact.fields.City.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Ext.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Home_Email.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Home_Phone.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Mobile_Phone.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Name.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Relationship.show = event.checked;
      this.responseObj.Emergency_Contact.fields.State.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Street_1.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Street_2.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Work_Email.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Work_Phone.show = event.checked;
      this.responseObj.Emergency_Contact.fields.Zip.show = event.checked;
    }
    else {
      let toChange = fieldname;
      this.responseObj.Emergency_Contact.fields[toChange].show = event.checked;
    }
  }

  /* Description: view/edit standard fields select the checkbox 
  checking/unchecking for disable property field
  1)personal section
  author : vipin reddy */
  fieldDisableStatus(event, fieldname) {
    console.log(fieldname, event);

    let toChange = fieldname;
    this.responseObj.Personal.fields[toChange].hide = event.checked;
    this.responseObj.Personal.fields[toChange].required = !event.checked;
    console.log(this.responseObj.Personal.fields[toChange].hide);
  }
  /*2)job section*/
  fieldDisableStatusforJob(event, fieldname) {
    console.log(fieldname, event);

    let toChange = fieldname;
    this.responseObj.Job.fields[toChange].hide = event.checked;
    this.responseObj.Job.fields[toChange].required = !event.checked;

    console.log(this.responseObj.Job.fields[toChange].hide);
  }
  /* 3)compensation section */
  fieldDisableStatusforCompensation(event, fieldname) {
    let toChange = fieldname;
    this.responseObj.Compensation.fields[toChange].hide = event.checked;
    this.responseObj.Compensation.fields[toChange].required = !event.checked;
  }
  /*4)emergency contact*/
  fieldDisableStatusforEmergencyContact(event, fieldname) {
    let toChange = fieldname;
    this.responseObj.Emergency_Contact.fields[toChange].hide = event.checked;
    this.responseObj.Emergency_Contact.fields[toChange].required = !event.checked;
  }
  /* Description: view/edit standard fields select the checkbox 
checking/unchecking for required field
1)personal section
author : vipin reddy */
  fieldRequiredStatus(event, fieldName) {
    let toChange = fieldName;
    this.responseObj.Personal.fields[toChange].required = event.checked;
  }
  /* 2) Job section */
  fieldRequiredStatusforJob(event, fieldname) {
    let toChange = fieldname;
    this.responseObj.Job.fields[toChange].required = event.checked;
  }
  /* 3) compensation section */
  fieldRequiredStatusforCompensation(event, fieldname) {
    let toChange = fieldname;
    this.responseObj.Compensation.fields[toChange].required = event.checked;
  }
  /* 4)emergenccy contact */
  fieldRequiredStatusforEmergencyContact(event, fieldname) {
    let toChange = fieldname;
    this.responseObj.Emergency_Contact.fields[toChange].required = event.checked;
  }
  /* Description: view/edit standard fields select the
  format slecting dropdown field
  1)personal fields
  author : vipin reddy */
  formatChange($event, fieldName) {
    console.log($event);
    let toChange = fieldName;
    this.responseObj.Personal.fields[toChange].format = $event;
  }
  /* 2)Jobfields*/
  formatChangeforJob($event, fieldname) {
    console.log($event);
    let toChange = fieldname;
    this.responseObj.Job.fields[toChange].format = $event;
  }
  /*3)Compensation*/
  formatChangeforCompensation($event, fieldName) {
    let toChange = fieldName;
    this.responseObj.Compensation.fields[toChange].format = $event;
  }
  /*4)emergency contacct*/
  formatChangeforEmergencyContact($event, fieldName) {
    let toChange = fieldName;
    this.responseObj.Emergency_Contact.fields[toChange].format = $event;
  }
  /* Description: view/edit standard fields each section show/hide functionality
  author : vipin reddy */
  toggleValue(event, name) {
    console.log(event, name);

    this.selectedField = name;
    if (this.selectedField == 'offBoarding') {
      this.responseObj.Offboarding.hide = !event.value;
    }
    else if (this.selectedField == 'personal') {
      this.responseObj.Personal.hide = !event.value;
    }
    else if (this.selectedField == 'job') {
      this.responseObj.Job.hide = !event.value;
    }
    else if (this.selectedField == 'timeOff') {
      this.responseObj.Time_Off.hide = !event.value;
    }
    else if (this.selectedField == 'emergencyContact') {
      this.responseObj.Emergency_Contact.hide = !event.value;
    }
    else if (this.selectedField == 'performance') {
      this.responseObj.Performance.hide = !event.value;
    }
    else if (this.selectedField == 'compensation') {
      this.responseObj.Compensation.hide = !event.value;
    }
    else if (this.selectedField == 'notes') {
      this.responseObj.Notes.hide = !event.value;
    }
    else if (this.selectedField == 'benefits') {
      this.responseObj.Benefits.hide = !event.value;
    }
    else if (this.selectedField == 'training') {
      this.responseObj.Training.hide = !event.value;
    }
    else if (this.selectedField == 'documnets') {
      this.responseObj.Documents.hide = !event.value;
    }
    else if (this.selectedField == 'assets') {
      this.responseObj.Assets.hide = !event.value;

    }
    else if (this.selectedField == 'onBoarding') {
      this.responseObj.Onboarding.hide = !event.value;
    }



  }
  /* Description: view/edit standard fields show/hide functionality for all tabs
  author : vipin reddy */

  toggleValueAll(event) {

    console.log(event);
    // if(this.personalField == false || this.jobField == false){
    //   this.personalField = !this.personalField;
    //   this.jobField = !this.jobField;
    // }

    if (event.value == false || event.value == true) {
      console.log(this.toggleSelectAll);

      this.toggleSelectAll = !event.value;
      this.responseObj.Personal.hide = !event.value;
      this.responseObj.Job.hide = !event.value;
      this.responseObj.Offboarding.hide = !event.value;
      this.responseObj.Time_Off.hide = !event.value;
      this.responseObj.Emergency_Contact.hide = !event.value;
      this.responseObj.Performance.hide = !event.value;
      this.responseObj.Compensation.hide = !event.value;
      this.responseObj.Notes.hide = !event.value;
      this.responseObj.Benefits.hide = !event.value;
      this.responseObj.Training.hide = !event.value;
      this.responseObj.Documents.hide = !event.value;
      this.responseObj.Assets.hide = !event.value;
      this.responseObj.Onboarding.hide = !event.value;
    }
  }
  /* Description: side nav activation in  view/edit standard fields popup
  author : vipin reddy */
  chooseFieldss(event: any) {
    this.selectedNav = event;
    if (event == "chooseFields") {
      this.chooseFields = true;
      this.chooseSteps = false;
    }
    else {
      this.chooseFields = false;
      this.chooseSteps = true;
    }
  }
  /* Description:Post the employee identification settings
  author : vipin reddy */

  empIdentitySettings() {
    this.isValid = true;
    if (this.employeeIdentity.idNumber.numberType) {
      if (this.employeeIdentity.idNumber.numberType == 'system Generated') {
        if (this.employeeIdentity.idNumber.digits && this.employeeIdentity.idNumber.firstNumber &&
          this.employeeIdentity.idNumber.digits == this.employeeIdentity.idNumber.firstNumber.length) {

          console.log(this.employeeIdentity, this.employeeIdentity.idNumber.firstNumber.length);

          this.companySettingsService.empSettings(this.employeeIdentity)
            .subscribe(
              (res: any) => {
                console.log("EI Settingss", res);
                if (res.status == true) {
                  this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "success");
                  this.getEmpSettings()
                  // this.employeeIdentity = {
                  //   maskSSN: false,
                  //   idNumber: {
                  //     numberType: 'Manual',
                  //     digits: '',
                  //     firstNumber: ''
                  //   }
                  // }
                }

                else {
                  this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "error");
                }
              },
              (err: any) => {
                this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", err.message, "error");

              })
        }
        else {
          this.swalAlertService.SweetAlertWithoutConfirmation('Employee ID Number', 'please enter selected format length of digits', 'error')
        }
      }
      if (this.employeeIdentity.idNumber.numberType == 'Manual') {
        console.log(this.employeeIdentity);

        this.companySettingsService.empSettings(this.employeeIdentity)
          .subscribe(
            (res: any) => {
              console.log("EI Settingss", res);
              if (res.status == true) {
                this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "success");
                this.getEmpSettings()
                // this.employeeIdentity = {
                //   maskSSN: false,
                //   idNumber: {
                //     numberType: 'Manual',
                //     digits: '',
                //     firstNumber: ''
                //   }
                // }
              }

              else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "error");
              }
            },
            (err: any) => {
              this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", err.message, "error");

            })
      }

    }

  }
  /* Description:Post the In side modal popup View/Edit Stanadard format&requirments each field data post
  author : vipin reddy */
  saveFormats() {
    console.log("11111 post daata", this.responseObj);
    var postData = {
      myInfo_sections: this.responseObj
    }
    this.companySettingsService.postShowFeilds(postData)
      .subscribe(
        (res: any) => {
          console.log("res of get", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success")


          }

        },
        (err: any) => {
          console.log(err);

        })


  }
  /* Description:adding custom fields if dropdown adding dynamically 
  each dropdown value adding to array
  author : vipin reddy */

  addInput() {
    this.inputFields.push({ value: '' })

  }
  /* Description:removing custom fields if dropdown adding dynamically 
  each dropdown value adding to array
  author : vipin reddy */
  removeInput() {
    this.inputFields.pop()
  }
  /* Description:Post the cudtom-fields your adding
  author : vipin reddy */
  fieldAdd() {
    this.isValid = true;
    if (this.customFields.fieldName && this.customFields.fieldType && this.customFields.fieldCode) {

      if (this.customFields.fieldType == 'Dropdown') {
        var x = this.customFields
        x['dropDownOptions'] = this.inputFields
      }

      if (this.singleCustomFieldId) {
        var y = this.customFields
        y['_id'] = this.singleCustomFieldId;
      }

      console.log(this.customFields, this.inputFields.length);
      if (this.customFields.fieldType == 'Dropdown' && this.inputFields.length > 0) {
        this.companySettingsService.addCustomFields(this.customFields)
          .subscribe(
            (res: any) => {
              console.log("res of custom", res);
              if (res.status == true) {
                this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success")

                this.customFields.fieldCode = '',
                  this.customFields.fieldName = '',
                  this.customFields.fieldType = '',
                  this.inputFields = []
                this.modalRef.hide();
                this.getAllCustomFields()
              }
            },
            (err: any) => {
              console.log(err);

            })
      }
      if (this.customFields.fieldType == 'Dropdown' && this.inputFields.length == 0) {
        this.swalAlertService.SweetAlertWithoutConfirmation('CustomFields', 'add dropdown options', 'error')
      }
      if (this.customFields.fieldType != 'Dropdown') {
        this.companySettingsService.addCustomFields(this.customFields)
          .subscribe(
            (res: any) => {
              console.log("res of custom", res);
              if (res.status == true) {
                this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success")

                this.customFields.fieldCode = '',
                  this.customFields.fieldName = '',
                  this.customFields.fieldType = '',
                  this.inputFields = []
                this.modalRef.hide();
                this.getAllCustomFields()
              }
            },
            (err: any) => {
              console.log(err);

            })
      }
    }
  }
  /* Description:Get all cudtom-fields
author : vipin reddy */

  getAllCustomFields() {
    this.companySettingsService.getAllCustomFields()
      .subscribe(
        (res: any) => {
          console.log("res of custom", res);
          this.customFieldsData = res.data.customFields;
        },
        (err: any) => {
          console.log("err res", err);

        })
  }
  /* Description:single cudtom-field disable status
author : vipin reddy */

  fieldDisableStatusforCustom(event, data) {
    console.log(event, data);
    this.customFields = data;
    for (var i = 0; i < this.customFieldsData.length; i++) {
      if (this.customFieldsData[i]._id == data._id) {
        this.customFieldsData[i].hide = event.checked;
        this.customFieldsData[i].required = !event.checked;
      }
    }
    var y = this.customFields
    y['_id'] = data._id;
    console.log("2525236", this.customFields);

    this.companySettingsService.addCustomFields(this.customFields)
      .subscribe(
        (res: any) => {
          console.log(res);

        },
        (err: any) => {
          console.log(err);

        })

  }
  /* Description:single cudtom-field required status
  author : vipin reddy */
  fieldRequiredStatusforCustom(event, data) {
    console.log(event, data);
    this.customFields = data;
    for (var i = 0; i < this.customFieldsData.length; i++) {
      if (this.customFieldsData[i]._id == data._id) {
        this.customFieldsData[i].required = event.checked;
      }
    }
    var y = this.customFields
    y['_id'] = data._id;
    console.log("2525236", this.customFields);

    this.companySettingsService.addCustomFields(this.customFields)
      .subscribe(
        (res: any) => {
          console.log(res);

        },
        (err: any) => {
          console.log(err);

        })


  }
  /* Description:single cudtom-field EDIT
  author : vipin reddy */
  fieldNameForCustom(event, id) {
    console.log(event, id);
    // this.selectedPackageDetails = [];
    for (var i = 0; i < this.customFieldsData.length; i++) {
      if (this.customFieldsData[i]._id == id) {
        this.customFieldsData[i].show = event.checked;
        if (event.checked == true) {
          this.selectedPackageDetails.push(this.customFieldsData[i]._id)
          console.log('222222', this.selectedPackageDetails);
        }
        if (event.checked == false) {
          for (var j = 0; j < this.selectedPackageDetails.length; j++) {
            if (this.selectedPackageDetails[j] === id) {
              this.selectedPackageDetails.splice(j, 1);
              console.log(this.selectedPackageDetails, "155555555");
            }
          }
        }

      }
    }
    // this.singleCustomFieldId = id;   

  }
  /* Description:single single cudtom-field type change to dropdown
     dropdownfield array empty
   author : vipin reddy */
  fieldTypeChange() {
    console.log('111111', this.inputFields);

    if (this.customFields.fieldType != 'Dropdown')
      this.inputFields = []
    console.log('111111', this.inputFields);
  }
  /* Description:single cudtom-field EDIT
   author : vipin reddy */

  editSingleCustomField(template2) {
    if (this.selectedPackageDetails.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Field", "Select only one Field to Proceed", 'error')
    } else if (this.selectedPackageDetails.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Field", "Select a Field to Proceed", 'error')
    }
    else {

      this.companySettingsService.getSingleCustomFieldData(this.selectedPackageDetails[0])
        .subscribe(
          (res: any) => {
            console.log('res of single field', res);
            this.customFields = res.data;
            console.log("singlefield data", this.customFields);

            if (res.data.fieldType == 'Dropdown') {
              this.inputFields = res.data.dropDownOptions;
            }
            this.openModalWithClass2(template2)
          },
          (err: any) => {
            console.log(err);

          })
    }

  }
  /* Description:single cudtom-field Delete multiple
 author : vipin reddy */
  deleteFields() {
    console.log('123123123', this.selectedPackageDetails);

    this.companySettingsService.deleteCustomFieldData(this.selectedPackageDetails)
      .subscribe(
        (res: any) => {
          console.log("delete res", res);
          if (res.status == true) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Delete Fields', res.message, 'success')
            this.selectedPackageDetails = []
            this.getAllCustomFields()
          }
        },
        (err: any) => {

        })
  }
  /* Description:move next page
author : vipin reddy */
  saveandNext() {
    this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/structure"])
  }

}
