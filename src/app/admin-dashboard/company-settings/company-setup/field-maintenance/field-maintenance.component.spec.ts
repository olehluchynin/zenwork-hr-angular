import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldMaintenanceComponent } from './field-maintenance.component';

describe('FieldMaintenanceComponent', () => {
  let component: FieldMaintenanceComponent;
  let fixture: ComponentFixture<FieldMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
