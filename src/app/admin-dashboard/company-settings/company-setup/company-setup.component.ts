import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
// import { Router } from '@angular/router';
import { CompanySettingsService } from '../../../services/companySettings.service';
import { FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { MessageService } from '../../../services/message-service.service';

import { Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError} from '@angular/router';
import { LoaderService } from '../../../services/loader.service';


@Component({
  selector: 'app-company-setup',
  templateUrl: './company-setup.component.html',
  styleUrls: ['./company-setup.component.css']
})
export class CompanySetupComponent implements OnDestroy {
  subscription:any;
  message:any;
  activeTab:any;
  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private router: Router,
    private loaderService:LoaderService,
    private companySetupService: CompanySettingsService,
    private swalAlertService: SwalAlertService,
    private messageService:MessageService
  ) { 
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.message = message.text;
      console.log("message", this.message);
      this.activeTab = this.message;
    });
  }


  ngOnInit() {
    
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.loader(true);
    }
    if (event instanceof NavigationEnd) {
      this.loaderService.loader(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loaderService.loader(false);
    }
    if (event instanceof NavigationError) {
      this.loaderService.loader(false);
    }
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  previousPage(){
    this.router.navigate(['/admin/admin-dashboard/company-settings'])
  }
 

  /* Get Company Contact Information Ends*/

}
