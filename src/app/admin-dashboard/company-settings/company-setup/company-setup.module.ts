import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanySetupComponent } from './company-setup.component';
import { MatSelectModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import { FieldMaintenanceComponent } from './field-maintenance/field-maintenance.component';
import { StructureComponent } from './structure/structure.component';
import { TrainingComponent } from './training/training.component';
import { PayrollComponent } from './payroll/payroll.component';
import { EmailTemplatesComponent } from './email-templates/email-templates.component';
import { CompanyPolicyComponent } from './company-policy/company-policy.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import {MatPaginatorModule} from '@angular/material/paginator';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { StructureFieldDialogComponent } from './structure/structure-field-dialog/structure-field-dialog.component';
import { AssignEmployeesComponent } from './training/assign-employees/assign-employees.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../../../shared-module/shared.module';
import { AngularDualListBoxModule } from 'angular-dual-listbox';




const routes: Routes = [
  {
    path: '', component:CompanySetupComponent , children: [
      { path: '', redirectTo: 'general-settings', pathMatch: 'full' },
      { path: 'general-settings', component: GeneralSettingsComponent },
      { path: 'field-maintenance', component: FieldMaintenanceComponent },
      { path: 'structure', component: StructureComponent },
      { path: 'training', component: TrainingComponent },
      { path: 'payroll', component: PayrollComponent },
      { path : 'email-templates', component: EmailTemplatesComponent},
      { path: 'company-policy', component: CompanyPolicyComponent }

    ]
  },
]



@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    RouterModule.forChild(routes), FormsModule, ReactiveFormsModule,MatCheckboxModule,MatMenuModule,MatButtonModule,MatIconModule,MatPaginatorModule,MaterialModuleModule,
    SharedModule,
    AngularDualListBoxModule,
    NgMultiSelectDropDownModule.forRoot(),NgxMaskModule.forRoot({
      showMaskTyped : true,
    })
  ],
  declarations: [CompanySetupComponent,
    GeneralSettingsComponent,
    FieldMaintenanceComponent,
    StructureComponent,
    TrainingComponent,
    PayrollComponent,
    EmailTemplatesComponent,
    CompanyPolicyComponent,
    StructureFieldDialogComponent,
    AssignEmployeesComponent],
    entryComponents: [
      StructureFieldDialogComponent,
      AssignEmployeesComponent
    ] 
})
export class CompanySetupModule { }
