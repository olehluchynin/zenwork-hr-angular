import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CompanySettingsService } from '../../../../services/companySettings.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';

declare var jQuery: any;


@Component({
  selector: 'app-company-policy',
  templateUrl: './company-policy.component.html',
  styleUrls: ['./company-policy.component.css']
})
export class CompanyPolicyComponent implements OnInit {

  givenFileName:any
  files:any;
  fileName:any;
  selectedPolicy:Array<any> = [];
  policyData:any;
  public isValid:boolean = false;
  getFileName:Array<any> = [
    {
      isChecked: '',
      givenFileName:'',
      url:''
    }  
]

checkIds = []

@ViewChild('openEdit') openEdit: ElementRef
  constructor(
    private companySettingsService : CompanySettingsService,
    private swalAlertService: SwalAlertService,
  ) { }

  ngOnInit() {
    this.getUploadPolicies()
  }


   // Author: Saiprakash , Date: 15/04/19
  // Get files for company policies


  getUploadPolicies(){
    this.companySettingsService.getUploadPolicies().subscribe((res:any)=>{
      console.log(res);
     this.getFileName = res.data
     console.log(this.getFileName);
   
     
    }, err =>{
      console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
    })
  }


  

  fileChangeEvent(e: any) {

    this.files = e.target.files[0];
    console.log(this.files.name)
    this.fileName = this.files.name

  }
  // Author: Saiprakash , Date: 15/04/19
  // Upload file and edit for company policies

  uploadPolicy(policyForm){


    var formData: FormData = new FormData();
    formData.append('file', this.files);
    formData.append('givenFileName', this.givenFileName.trim())
    this.isValid = true;
    if(this.fileName && this.givenFileName){
      if(this.policyData && this.policyData._id ){
        formData.append('_id', this.policyData._id);
        formData.append('s3Path', this.policyData.s3Path);
        formData.append('originalFilename', this.fileName)

        this.companySettingsService.editUploadPolicy(formData).subscribe((res:any)=>{
          console.log(res);
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.checkIds = [];
          this.policyData._id = "";
          policyForm.resetForm();
          this.isValid = false;
          this.getUploadPolicies();
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");
        }, err =>{
          console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
          
        })
      }
      else {
        this.companySettingsService.uploadPolicy(formData).subscribe((res:any)=>{
          console.log(res);
          jQuery("#myModal").modal("hide");
          this.fileName = "";
          this.givenFileName = '';
          this.checkIds = [];
          policyForm.resetForm();
          this.getUploadPolicies();
          this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "","success");

        }, err =>{
          console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
          
        })
      }
     
    }

  }

  checkedIds(event, id){
    if(event.checked == true){
      this.checkIds.push(id)
    }
    console.log(this.checkIds);
    if(event.checked == false){
      var index = this.checkIds.indexOf(id);
        if (index > -1) {
          this.checkIds.splice(index, 1);
        }
        console.log(this.checkIds);
    }

  }

  editCompanyPolicy(){
    this.selectedPolicy = this.getFileName.filter(policyData => {
     return policyData.isChecked
     
    })
    if (this.selectedPolicy.length > 1) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Policy", "Select only one policy on to Proceed", 'error')
    } else if (this.selectedPolicy.length == 0) {
      this.swalAlertService.SweetAlertWithoutConfirmation("Edit Policy", "Select a policy on to Proceed", 'error')
    } 
    else {
      this.openEdit.nativeElement.click();
      this.companySettingsService.getsinglePolicy(this.selectedPolicy[0]._id).subscribe((res:any)=>{
        console.log(res);
        this.policyData = res.data;
        this.fileName = res.data.originalFilename;
        this.givenFileName = res.data.givenFileName
        
      }, err=>{
        console.log(err);
        
      })
    }
   
  }
 // Author: Saiprakash , Date: 15/04/19
  // Company policies delete

  deleteCompanyPolicy(){
    
    this.selectedPolicy = this.getFileName.filter(policyData => {
     
      return policyData.isChecked
          
     })
      if (this.selectedPolicy.length == 0) {
       this.swalAlertService.SweetAlertWithoutConfirmation("Delete Policy", "Select a policy to Proceed", 'error')
     } 
     else {
      
       this.companySettingsService.deletePolicies(this.selectedPolicy).subscribe((res:any)=>{
         console.log(res);
         this.swalAlertService.SweetAlertWithoutConfirmation('Record deleted successfully', "", 'success');
         this.checkIds = [];
         this.getUploadPolicies();
       
       }, err=>{
         console.log(err);
      this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
         
       })
     }

  }


cancelPolicies(){
 this.givenFileName="";
 this.fileName = "";
 this.isValid = false;

}

alphabets(event){
  return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)
}


}
