import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';

import { CompanySettingsRouting } from './company-settings.routing';
import { CompanySettingsComponent } from './company-settings.component';
import { ThemeComponent } from './theme/theme.component';
import { MatSelectModule } from '@angular/material/select';





@NgModule({
    declarations: [
        CompanySettingsComponent,
        ThemeComponent,

    ],
    providers: [
    ],
    imports: [
        CommonModule,
        CompanySettingsRouting,
        AngularFontAwesomeModule,
        FormsModule,
        TabsModule.forRoot(),
        ModalModule.forRoot(),
        MatButtonToggleModule,
        MatListModule,
        MatDividerModule,
        MatExpansionModule,
        MatSelectModule,
    ]
})
export class CompanySettingsModule { }