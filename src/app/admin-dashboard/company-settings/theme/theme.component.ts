import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css']
})
export class ThemeComponent implements OnInit {

  public showEditTheme:boolean = false;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  previousPage() {
    this.router.navigate(['/admin/admin-dashboard/company-settings']);
  }
  editTheme(){
    this.showEditTheme = true;
  }
}
