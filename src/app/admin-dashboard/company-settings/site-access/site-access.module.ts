import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule, MatFormFieldModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { SiteAccessComponent } from '../site-access/site-access.component';
import { MaterialModuleModule } from '../../../material-module/material-module.module';
import { CreateCustomUserRoleModelComponent } from './site-user/create-custom-user-role-model/create-custom-user-role-model.component';

import { FormsModule } from '@angular/forms';
import { EditCustomUserRoleModelComponent } from './site-user/edit-custom-user-role-model/edit-custom-user-role-model.component';
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import {  ChangeUserRoleComponent } from './change-user-role/change-user-role.component';

const router: Routes = [
  // {path:'', redirectTo:'site', pathMatch:'full'},
  { path: 'site', component: SiteAccessComponent },
  // {path:'', redirectTo:'site-user', pathMatch:'full'},
  { path: 'site-user', loadChildren: './site-user/site-user.module#SiteUserModule' },

]

@NgModule({
  imports: [
    CommonModule,
    MaterialModuleModule,
    FormsModule,
    // AngularMultiSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatSelectModule, RouterModule.forChild(router), MatFormFieldModule,
    AngularDualListBoxModule
  ],
  declarations: [SiteAccessComponent,CreateCustomUserRoleModelComponent,EditCustomUserRoleModelComponent, ChangeUserRoleComponent],
  entryComponents: [
    CreateCustomUserRoleModelComponent,EditCustomUserRoleModelComponent,ChangeUserRoleComponent
  ] 
})
export class SiteAccessModule { }
