import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SiteAccessRolesService } from '../../../../services/site-access-roles-service.service';
import { SwalAlertService } from '../../../../services/swalAlert.service';

@Component({
  selector: 'app-change-user-role',
  templateUrl: './change-user-role.component.html',
  styleUrls: ['./change-user-role.component.css']
})
export class ChangeUserRoleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ChangeUserRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private siteAccessRolesService: SiteAccessRolesService,
    private swalAlertService: SwalAlertService) { }

  companyRoles = [];
  resRolesObj = [];
  newRole: any;

  ngOnInit() {
    console.log(this.data);
    this.getAllroles()
    this.getBaseRoles();
  }

  getAllroles() {
    var cID = localStorage.getItem('companyId')
    console.log("cid", cID);
    cID = cID.replace(/^"|"$/g, "");
    console.log("cin2", cID);


    this.siteAccessRolesService.companyId(cID)
      .subscribe((res: any) => {
        console.log("get ROles", res);
        this.companyRoles = res.data;
        var result = this.companyRoles.map(function (el) {
          var o = Object.assign({}, el);
          o.isActive = false;
          return o;
        })
        this.companyRoles = result;
        console.log(this.companyRoles, 'custom create');

      },
        (err) => {
          console.log(err);


        })
  }

  getBaseRoles() {
    this.siteAccessRolesService.getBaseRoles()
      .subscribe((res: any) => {
        console.log("roles", res);

        this.resRolesObj = res.data;
        this.companyRoles.forEach(element => {
          this.resRolesObj.push(element)
        });
        console.log(this.resRolesObj, "all roles");

      },
        (err) => {

        })
  }


  cancel() {

    this.dialogRef.close()
  }

  submitChangeRole() {
    var postData = {
      "userId": this.data._id,
      "newRoleId": this.newRole,
    }
    console.log("sendingdata", postData);

    this.siteAccessRolesService.changeSiteAccessRoleforEmp(postData)
      .subscribe((res: any) => {
        console.log("roles", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('User Role', res.message, 'success')
          this.cancel()
        }
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation('User Role', err.error.message, 'error')

        })
  }

}
