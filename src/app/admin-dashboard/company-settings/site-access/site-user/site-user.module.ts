import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteUserComponent } from './site-user.component';
import { RouterModule, Routes } from '@angular/router';
import { SystemViewComponent } from './system-view/system-view.component';
import { ManagerServiceComponent } from './manager-service/manager-service.component'
import { MatSlideToggleModule } from '@angular/material';
// import { CreateCustomUserRoleModelComponent } from './create-custom-user-role-model/create-custom-user-role-model.component';
// import { EditCustomUserRoleModelComponent } from './edit-custom-user-role-model/edit-custom-user-role-model.component';
import { FormsModule } from '@angular/forms';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
const router: Routes = [

  { path: '', redirectTo: 'view', pathMatch: 'full' },
  {
    path: 'view', component: SiteUserComponent, children: [
      { path: 'system-view', component: SystemViewComponent },
    ]
  },
  { path: 'manager-service', component: ManagerServiceComponent }

]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(router),
    MatSlideToggleModule,
    FormsModule,
    // NgMultiSelectDropDownModule.forRoot()
    // AngularMultiSelectModule
  ],
  declarations: [SiteUserComponent, SystemViewComponent, ManagerServiceComponent]
})
export class SiteUserModule { }
