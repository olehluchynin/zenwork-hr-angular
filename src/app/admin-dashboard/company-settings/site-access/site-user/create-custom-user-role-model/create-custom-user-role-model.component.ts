import { Component, OnInit, Inject } from '@angular/core';
import { CompanySettingsService } from '../../../../../services/companySettings.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SiteAccessRolesService } from '../../../../../services/site-access-roles-service.service';
import { MyInfoService } from '../../../../../services/my-info.service';
import { SwalAlertService } from '../../../../../services/swalAlert.service';
import { DualListComponent } from 'angular-dual-listbox';
import { EmployeeService } from '../../../../../services/employee.service';

@Component({
  selector: 'app-create-custom-user-role-model',
  templateUrl: './create-custom-user-role-model.component.html',
  styleUrls: ['./create-custom-user-role-model.component.css']
})
export class CreateCustomUserRoleModelComponent implements OnInit {
  selectedNav: any;
  chooseFields: boolean = false;
  chooseSteps: boolean = false;
  chooseEmployees: boolean = false;
  summaryRecap: boolean = false;
  isValid: boolean = false;
  roleId: any;
  page: any;
  editCustomRole: boolean = false;

  structureName: any;
  checked: any;
  StructureValues: Array<any> = [];
  customValues: Array<any> = []
  fieldsName: string
  structureFields: Array<any> = [
    {
      name: 'Business Unit',
      isChecked: true
    }
  ]


  resRolesObj = {
    "data": [

    ]
  }

  pagesAllObj = {
    "data": [

    ]
  }
  public userRoleData = {
    baseRoleId: '',
    name: "",
    description: "",
    access: "",
    roletype: 1,
    companyId: '',
    createdBy: '',
    updatedBy: '',

  }
  roleName: any;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  fields = [];
  result = [];
  personalFields = [

  ];
  JobFields = [];
  emergency = [];
  compensationFields = [];

  eligibilityArray = [];
  selectedUsers = [];
  selectedEmployees: any;

  format = {
    add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
    direction: DualListComponent.LTR, draggable: true, locale: 'da',
  };

  tempPagesControlls: any;

  constructor(public dialogRef: MatDialogRef<CreateCustomUserRoleModelComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private employeeService: EmployeeService,
    private siteAccessRolesService: SiteAccessRolesService, private companySettingsService: CompanySettingsService,
    private myInfoService: MyInfoService, private swalAlertService: SwalAlertService) { }

  ngOnInit() {
    this.selectedNav = 'chooseSteps';
    console.log("data from other component", this.data);
    if (this.data.data != 'CreateCustomUserRole') {
      this.editCustomRole = (this.data.data != 'CreateCustomUserRole');
      console.log(this.editCustomRole, "edit");
      this.roleName = this.data.data.baseRoleId.name
      this.allPagesData();
      this.getAllEmp();
      this.getAllsingleRole(this.data.data);
      this.getAllpagesForUpdate(this.data.data);
      this.getAllMyInfoTabFieldsUpdate(this.data.data);

      this.getEmployeesUnderRole(this.data.data);
    }
    if (this.editCustomRole == false) {
      this.allPagesData();
      this.getMyInfoTabsData('Personal');
      this.getMyInfoTabsData('Job');
      this.getMyInfoTabsData('Emergency_Contact');
      this.getMyInfoTabsData('Compensation');
    }
    this.getFieldsForRoles();
    this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
    this.getBaseRoles();



    // this.getMyInfoTabsData('Job')
    // this.getMyInfoTabsData('Emergency_Contact')
    // this.getMyInfoTabsData('Compensation')
    //npm multi select dropdown sample start
    this.dropdownList = [
      // { item_id: 1, item_text: 'Mumbai' },
      // { item_id: 2, item_text: 'Bangaluru' },
      // { item_id: 3, item_text: 'Pune' },
      // { item_id: 4, item_text: 'Navsari' },
      // { item_id: 5, item_text: 'New Delhi' },
      // { item_id: 6, item_text: 'New Delhi' },
      // { item_id: 7, item_text: 'New Delhi' },
      // { item_id: 8, item_text: 'New Delhi' },
      // { item_id: 9, item_text: 'New Delhi' },
    ];



    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 0,
      maxHeight: 150,
      defaultOpen: true,
      allowSearchFilter: true,

    };

  }
  onItemSelect(item: any) {
    this.selectedUsers = []
    console.log(item);
    console.log(this.selectedItems);
    for (var i = 0; i < this.selectedItems.length; i++) {
      this.selectedUsers.push(this.selectedItems[i].item_id);
    }
    console.log('selcetd users', this.selectedUsers);


  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);

    // for (var i = 0; i < this.selectedUsers.length; i++) {
    //   if(item.item_id == this.selectedUsers[i])
    //   delete this.selectedUsers[i].item_id;
    // }

    this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
    console.log('selcetd users', this.selectedUsers);

  }
  onSelectAll(items: any) {
    console.log(items);
    this.selectedUsers = [];
    for (var i = 0; i < items.length; i++) {
      this.selectedUsers.push(items[i].item_id);
    }
    console.log("users", this.selectedUsers);


  }
  onDeSelectAll(items: any) {
    console.log(items);
    this.selectedUsers = [];

    console.log('selcetd users', this.selectedUsers);

  }
  /* Description: getting all pages data
  author : vipin reddy */
  getAllsingleRole(data) {
    console.log(data);
    this.siteAccessRolesService.getSingleCustomRole(data._id)
      .subscribe((res: any) => {
        console.log("single role", res);
        this.userRoleData = res.data;

      },
        (err) => {
          console.log(err);

        })
  }

  getAllEmp() {
    var listallUsers;
    var postData = {
      name: ''
    }
    console.log("data comes");

    this.siteAccessRolesService.getAllEmployees(postData)
      .subscribe((res: any) => {
        console.log("resonse for all employees", res);
        listallUsers = res.data;
        for (var i = 0; i < listallUsers.length; i++) {
          this.dropdownList.push({ _id: listallUsers[i]._id, _name: listallUsers[i].personal.name.preferredName })
          console.log(this.selectedUsers, this.dropdownList, "selectedUsers");

        }
      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: update all pages data
  author : vipin reddy */
  getEmployeesUnderRole(data) {
    console.log(data, "emplyes");
    var listUsers = [];
    this.siteAccessRolesService.getAllEmployeesforRoles(data.companyId, data._id)
      .subscribe((res: any) => {
        console.log("resonse for after edit pages", res);
        listUsers = res.data;
        this.getEmpNames(listUsers)
      },
        (err) => {
          console.log(err);

        })

  }

  getEmpNames(empIds) {
    var postData = {

      "_ids": empIds
    }

    this.employeeService.getOnboardedEmployeeDetails(postData)
      .subscribe((res: any) => {
        console.log("get ROle of employees", res);
        this.selectedEmployees = res.data;
        for (var i = 0; i < this.selectedEmployees.length; i++) {
          this.selectedUsers.push({ _id: this.selectedEmployees[i]._id, _name: this.selectedEmployees[i].personal.name.preferredName })
          console.log(this.selectedUsers, this.dropdownList, "selectedUsers");

        }
        // this.count = res.total_count;
      },
        (err) => {
          console.log(err);

        })
  }


  getAllpagesForUpdate(data) {
    console.log(data.companyId, data._id, 'comanyidroleid,');
    this.siteAccessRolesService.getAllPagesForCompany(data.companyId, data._id)
      .subscribe((res: any) => {
        console.log("resonse for after edit pages", res);
        this.pagesAllObj = res;
        // this.tempPagesControlls
        for (var i = 0; i < this.pagesAllObj.data.length; i++) {
          this.pagesAllObj.data[i]['baseEmployee'] = this.tempPagesControlls.data[i].employee
          this.pagesAllObj.data[i]['baseHr'] = this.tempPagesControlls.data[i].hr
          this.pagesAllObj.data[i]['baseManager'] = this.tempPagesControlls.data[i].manager
          this.pagesAllObj.data[i]['baseSysAdmin'] = this.tempPagesControlls.data[i].sysadmin
        }

        console.log(this.pagesAllObj, "new data pages ACLs");

      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: update all pages data
author : vipin reddy */
  getAllMyInfoTabFieldsUpdate(data) {

    this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Personal')
      .subscribe((res: any) => {
        console.log("myinfo field data for personal", res);
        this.personalFields = res.data;
        console.log("viiiiiiiiiiii perosnal", this.personalFields);
        if (res.data.length == 0) {
          this.getMyInfoTabsData('Personal');
        }

      },
        (err) => {
          console.log(err);

        })

    this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Job')
      .subscribe((res: any) => {
        console.log("myinfo field data for Job", res);
        this.JobFields = res.data;
        if (res.data.length == 0) {
          this.getMyInfoTabsData('Job');
        }
        console.log("viiiiiiiiiiii Job", this.JobFields);

      },
        (err) => {
          console.log(err);

        })
    this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Emergency_Contact')
      .subscribe((res: any) => {
        console.log("myinfo field data for Emergency_Contact", res);
        this.emergency = res.data;
        console.log("viiiiiiiiiiii Emergency_Contact", this.emergency);
        if (res.data.length == 0) {
          this.getMyInfoTabsData('Emergency_Contact');
        }

      },
        (err) => {
          console.log(err);

        })
    this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Compensation')
      .subscribe((res: any) => {
        console.log("myinfo field data for Compensation", res);
        this.compensationFields = res.data;
        console.log("viiiiiiiiiiii Compensation", this.compensationFields);
        if (res.data.length == 0) {
          this.getMyInfoTabsData('Compensation');
        }
      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: getting my-info tabs key names from companies serivce
 author : vipin reddy */
  getMyInfoTabsData(data) {
    this.fields = []
    this.result = []
    console.log(this.fields, "fields data");

    this.myInfoService.basicDetailsGetAll(data)
      .subscribe((res: any) => {

        console.log("myinfo field data", res);
        console.log(res.data.fields);
        this.fields = []
        for (var k in res.data.fields)

          this.fields.push({ "field_name": k })

        console.log(this.fields);

        this.result = this.fields.map(function (el) {
          var o = Object.assign({}, el);
          o.full_access = false;
          o.no_access = false;
          o.not_req_approval = false;
          o.req_approval = false;

          return o;

        })

        console.log("viiiiii", this.result);
        if (data == 'Personal') {
          this.personalFields = this.result;
          console.log('111111', this.personalFields);
        }
        if (data == 'Job') {
          this.JobFields = this.result;
          console.log('2222222', this.JobFields);
        }
        if (data == 'Emergency_Contact') {
          this.emergency = this.result;
          console.log('2222222', this.emergency);
        }
        if (data == 'Compensation') {
          this.compensationFields = this.result;
          console.log('2222222', this.compensationFields);
        }

        // this.personalfields = res.data.fields.keys
      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: add popup/side nav tabs active tabI
  author : vipin reddy */

  chooseFieldss(event) {
    this.selectedNav = event;
    if (event == "chooseFields") {
      this.chooseFields = true;
      this.chooseSteps = false;
      this.chooseEmployees = false;
      this.summaryRecap = false;
    }
    else if (event == 'chooseSteps') {
      this.chooseFields = false;
      this.chooseSteps = true;
      this.chooseEmployees = false;
      this.summaryRecap = false;
    }
    else if (event == 'chooseEmployees') {
      this.chooseFields = false;
      this.chooseSteps = false;
      this.chooseEmployees = true;
      this.summaryRecap = false;
    }
    else if (event == 'summaryRecap') {
      this.chooseFields = false;
      this.chooseSteps = false;
      this.chooseEmployees = false;
      this.summaryRecap = true;
    }
  }

  /* Description: getBaseRoles
  author : vipin reddy */
  getBaseRoles() {
    this.siteAccessRolesService.getBaseRoles()
      .subscribe((res: any) => {
        console.log("roles", res);

        this.resRolesObj = res;
      },
        (err) => {

        })
  }

  /* Description: roleChange for API
 author : vipin reddy */
  roleChange(access) {
    console.log(access, this.resRolesObj, "121212");
    for (var i = 0; i < this.resRolesObj.data.length; i++) {
      if (access == this.resRolesObj.data[i]._id) {
        this.userRoleData.access = this.resRolesObj.data[i].access;
        this.roleName = this.resRolesObj.data[i].name;
      }
    }
    console.log("access", this.userRoleData.access, this.roleName);

  }
  /* Description: create user role for (Add popup first popup)
   author : vipin reddy */
  createUserRole() {
    this.isValid = true;
    if (this.userRoleData.name && this.userRoleData.roletype && this.userRoleData.description) {
      var cID = localStorage.getItem('companyId')
      console.log("cid", cID);
      cID = cID.replace(/^"|"$/g, "");
      console.log("cin2", cID);

      this.userRoleData.companyId = cID;
      this.userRoleData.createdBy = cID;
      this.userRoleData.updatedBy = cID;
      var postData = this.userRoleData;
      console.log(postData);
      // delete this.roleName;

      this.siteAccessRolesService.createRoleType(postData)
        .subscribe((res: any) => {
          console.log("userrole", res);

          if (res.status == true) {
            // this.roleId = res.data._id
            this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", res.message, "success")
            this.selectedNav = 'chooseFields'
            this.roleId = res.role._id;

            console.log(this.roleId, "11111111111111111111111111111111111111");

            // this.getAllMyInfoTabFields()


          }
          else {
            this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", res.message, "error")
          }

        },
          (err) => {
            this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", err.error.message, "error")
          })
    }
  }
  /* Description: update single user role
 author : vipin reddy */

  UpdateUserRole() {
    this.siteAccessRolesService.UpdateRoleType(this.userRoleData)
      .subscribe((res: any) => {
        console.log("update custom role res", res)

        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", res.message, "success")
          this.selectedNav = 'chooseFields'
        }
      },
        (err) => {
          console.log(err);

        })

  }

  getAllMyInfoTabFields() {
    var cID = localStorage.getItem('companyId')
    console.log("cid", cID);
    cID = cID.replace(/^"|"$/g, "");
    console.log("cin2", cID);

    this.siteAccessRolesService.getAllMyinfoFields(this.roleId, cID, 'personal')
      .subscribe((res: any) => {
        console.log("personal res", res)
        if (res.data.length == 0) {
          // this.getMyInfoTabsData('Personal')
        }
        else {
          this.personalFields = res.data;
        }

      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: getting all pages data
 author : vipin reddy */

  allPagesData() {
    var companyId = JSON.parse(localStorage.getItem('companyId'))
    this.siteAccessRolesService.allPagesData(companyId)
      .subscribe((res: any) => {
        console.log("respages", res);
        this.pagesAllObj = res;
        this.tempPagesControlls = this.pagesAllObj;
        console.log(this.tempPagesControlls, this.pagesAllObj);

      },
        (err) => {
          console.log("err");

        })
  }

  /* Description: first time creating pages (ACLS) and perosnal,job,emergency,compensation fields
   author : vipin reddy */
  postShowFields() {
    console.log("create", this.pagesAllObj);
    // for (var i = 0; i < this.resRolesObj.data.length; i++) {
    //   if (this.userRoleData.name == this.resRolesObj.data[i].name) {
    //     console.log("121212", this.userRoleData.name, this.resRolesObj.data[i].name);

    //     var catname = this.resRolesObj.data[i].name;
    //     console.log("catname", catname);
    //   }
    // }
    if (this.userRoleData.name == 'HR' || this.roleName == 'HR') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.employee; delete item.manager; delete item.sysadmin; delete item.status; delete item._id; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; delete item.updatedBy; return item;
      });
    }
    if (this.userRoleData.name == 'Manager' || this.roleName == 'Manager') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.employee; delete item.hr; delete item.sysadmin; delete item.status; delete item._id; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; delete item.updatedBy; return item;
      });
    }
    if (this.userRoleData.name == 'Employee' || this.roleName == 'Employee') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.hr; delete item.manager; delete item.sysadmin; delete item.status; delete item._id; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; delete item.updatedBy; return item;
      });
    }
    if (this.userRoleData.name == 'System Admin' || this.roleName == 'System Admin') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.employee; delete item.manager; delete item.hr; delete item.status; delete item._id; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; delete item.updatedBy; return item;
      });
    }


    console.log("postdata of modules", this.pagesAllObj.data);

    var postData = {
      company_id: this.userRoleData.companyId,
      role_id: this.roleId,
      pages: this.pagesAllObj.data
    }
    postData['page_type'] = 'derived';
    console.log(postData, "vipin");


    this.pagesCreation(postData);

    var postData1 = {
      company_id: this.userRoleData.companyId,
      role_id: this.roleId,
      category: 'Personal',
      fields: this.personalFields
    }

    this.postPersonalFields(postData1);
    var postdata2 = {
      company_id: this.userRoleData.companyId,
      role_id: this.roleId,
      category: 'Job',
      fields: this.JobFields
    }
    this.postJobFields(postdata2);
    var postdata3 = {
      company_id: this.userRoleData.companyId,
      role_id: this.roleId,
      category: 'Emergency_Contact',
      fields: this.emergency
    }
    this.postEmergencyFields(postdata3);
    var postdata4 = {
      company_id: this.userRoleData.companyId,
      role_id: this.roleId,
      category: 'Compensation',
      fields: this.compensationFields
    }
    this.postCompensationFields(postdata4);

  }

  /* Description: sending updated pages(ACL) when custom role created 
   author : vipin reddy */
  postUpdateShowFields() {
    console.log("123456789", this.pagesAllObj);

    if (this.userRoleData.name == 'HR') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.employee; delete item.manager; delete item.roleId; delete item.sysadmin; delete item.status; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; return item;
      });
    }
    if (this.userRoleData.name == 'Manager') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.employee; delete item.hr; delete item.roleId; delete item.sysadmin; delete item.status; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; return item;
      });
    }
    if (this.userRoleData.name == 'Employee') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.hr; delete item.manager; delete item.roleId; delete item.sysadmin; delete item.status; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; return item;
      });
    }
    if (this.userRoleData.name == 'System Admin') {
      this.pagesAllObj.data.map(function (item) {

        delete item.pageType; delete item.employee; delete item.roleId; delete item.manager; delete item.hr; delete item.status; delete item.companyId; delete item.createdAt; delete item.updatedAt; delete item.pageid; return item;
      });
    }

    console.log(this.pagesAllObj.data, "post pages data");

    this.pagesUpdation(this.pagesAllObj.data)

    this.updatePersonalFields(this.personalFields)
    this.updateJobFields(this.JobFields)
    this.updateEmergencyFields(this.emergency)
    this.updateCompensationFields(this.compensationFields)


  }
  /* Description: update pages fields post
 author : vipin reddy */
  pagesUpdation(data) {
    this.siteAccessRolesService.pagesUpdate(data)
      .subscribe((res: any) => {
        console.log("reges aftyer update", res);
        this.selectedNav = 'chooseEmployees'
      },
        (err) => {
          console.log(err);

        })


  }

  /* Description: first time update personal fields post
   author : vipin reddy */
  updatePersonalFields(personalFields) {
    console.log(personalFields, "personalFields");

    this.siteAccessRolesService.personalFieldsUpdate(personalFields)
      .subscribe((res: any) => {
        console.log("update res of personal", res);
      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: first time update job fields post
 author : vipin reddy */

  updateJobFields(jobfields) {
    console.log("jobfields", jobfields);

    this.siteAccessRolesService.jobFieldsUpdate(jobfields)
      .subscribe((res: any) => {
        console.log("update res of job", res);
      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: first time update Emergency fields post
 author : vipin reddy */

  updateEmergencyFields(emergencyfields) {
    console.log("jobfields", emergencyfields);
    this.siteAccessRolesService.emergencyFieldsUpdate(emergencyfields)
      .subscribe((res: any) => {
        console.log("update res of emergency", res);
      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: update compensation fields
   author : vipin reddy */
  updateCompensationFields(compensationfields) {

    this.siteAccessRolesService.compensationFieldsUpdate(compensationfields)
      .subscribe((res: any) => {
        console.log("update res of compensation", res);
      },
        (err) => {
          console.log(err);

        })
  }




  /* Description: first time create Pages first time cretaion fields post
   author : vipin reddy */


  pagesCreation(postData) {
    this.siteAccessRolesService.pagesCreation(postData)
      .subscribe((res: any) => {
        console.log(res, 'pages creation');
        if (res.status == true) {
          this.selectedNav = 'chooseEmployees'
        }
      },
        (err) => {
          console.log(err);

        })

    console.log(postData);
  }
  /* Description: first time create personal fields post
 author : vipin reddy */
  postPersonalFields(personalFields) {
    console.log('personal fields post', personalFields);

    this.siteAccessRolesService.postpersonalfields(personalFields)
      .subscribe((res: any) => {
        console.log(res, 'personal fields create')
        if (res.status == true) {
          this.selectedNav = 'chooseEmployees'
        }
      },
        (err) => {
          console.log(err);

        })
  }
  /* Description: first time create job fields post
 author : vipin reddy */
  postJobFields(jobfields) {
    console.log('personal fields post', jobfields);

    this.siteAccessRolesService.postpersonalfields(jobfields)
      .subscribe((res: any) => {
        console.log(res, name)
      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: first time create Emergency fields post
   author : vipin reddy */
  postEmergencyFields(emergency) {
    console.log('personal fields post', emergency);

    this.siteAccessRolesService.postpersonalfields(emergency)
      .subscribe((res: any) => {
        console.log(res, name)
      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: first time create compensation fields post
   author : vipin reddy */
  postCompensationFields(compensation) {
    console.log('personal fields post', compensation);

    this.siteAccessRolesService.postpersonalfields(compensation)
      .subscribe((res: any) => {
        console.log(res, name)


      },
        (err) => {
          console.log(err);

        })

  }


  /* Description: post employee selection for specific task
   author : vipin reddy */
  postEmpData() {
    var postData = {
      roleId: this.roleId,
      users: this.selectedUsers

    }
    console.log(this.selectedUsers);

    this.siteAccessRolesService.postEmpDatatoRole(postData)
      .subscribe((res: any) => {
        console.log('rwspone emp', res);
        if (res.status == true) {
          this.selectedNav = 'summaryRecap'
          this.swalAlertService.SweetAlertWithoutConfirmation("Employee", res.message, "success")
        }
        else {
          this.swalAlertService.SweetAlertWithoutConfirmation("Employee", res.message, "error")
        }

      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Employee", err.error.message, "error")

        })


  }
  /* Description: in model moving back tab
    author : vipin reddy */

  backModulesandFields(tabName) {
    this.selectedNav = tabName;
  }

  /* Description: get structure fields data for choose employee tab
  author : vipin reddy */

  getStructureSubFields(fields, isChecked?) {
    console.log(fields, isChecked);
    this.fieldsName = fields;
    this.checked = isChecked
    if (isChecked == true) {
      this.eligibilityArray = [];
      this.structureFields.forEach(element => {
        if (element.name !== fields) {
          element.isChecked = false
        }
      })
      this.companySettingsService.getStuctureFields(fields, 0)
        .subscribe((res: any) => {
          console.log(res, name);
          this.StructureValues = res.default;
          this.customValues = res.custom;
          console.log(this.StructureValues);


        }, err => {
          console.log(err);

        })
    }



  }
  /* Description: select multiple subfields to get employee data
author : vipin reddy */
  subFieldsStructure($event, value) {
    console.log($event, value);

    // this.eligibilityArray = [];
    if ($event.checked == true) {
      this.eligibilityArray.push(value)
    }
    console.log(this.eligibilityArray, "123321");
    if ($event.checked == false) {
      var index = this.eligibilityArray.indexOf(value);
      if (index > -1) {
        this.eligibilityArray.splice(index, 1);
      }
      console.log(this.eligibilityArray);

    }
    var postdata = {
      field: this.fieldsName,
      chooseEmployeesForRoles: this.eligibilityArray
    }
    console.log(postdata);
    this.siteAccessRolesService.chooseEmployeesData(postdata)
      .subscribe((res: any) => {
        console.log("emp data", res);
        this.dropdownList = []
        for (var i = 0; i < res.data.length; i++) {
          if ($event.checked == true)
            this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName })
        }
        console.log(this.dropdownList, "employee dropdown");

      },
        (err) => {
          console.log(err);

        })


  }
  /* Description: select single structure values
  author : vipin reddy */
  getFieldsForRoles() {
    this.companySettingsService.getFieldsForRoles()
      .subscribe((res: any) => {
        console.log("12122123", res);
        this.structureFields = res.data


        // this.dropdownList =  { item_id: 1, item_text: 'Mumbai' }

      }, err => {
        console.log(err);

      })
  }

  /* Description: summary recap last submit button model close
  author : vipin reddy */

  closeRecapPopup() {
    this.dialogRef.close()

  }

  /* Description: model close
  author : vipin reddy */

  cancel() {

    this.dialogRef.close()
  }

  accessLevelChange(roleName, event, i) {
    console.log(roleName, event, i);

  }


}
