import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCustomUserRoleModelComponent } from './create-custom-user-role-model.component';

describe('CreateCustomUserRoleModelComponent', () => {
  let component: CreateCustomUserRoleModelComponent;
  let fixture: ComponentFixture<CreateCustomUserRoleModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCustomUserRoleModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCustomUserRoleModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
