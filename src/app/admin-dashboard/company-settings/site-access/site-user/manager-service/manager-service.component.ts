import { Component, OnInit } from '@angular/core';
declare var $: any;


@Component({
  selector: 'app-manager-service',
  templateUrl: './manager-service.component.html',
  styleUrls: ['./manager-service.component.css']
})
export class ManagerServiceComponent implements OnInit {

  constructor(

  ) { }

  ngOnInit() {

    $(document).ready(function() {
      $('#myCarousel').carousel({
        interval: 2000
      });
      $('#myCarousel1').carousel({
        interval: 3000
      });
      $('#myCarousel2').carousel({
        interval: 4000
      });
    });

  }

}
