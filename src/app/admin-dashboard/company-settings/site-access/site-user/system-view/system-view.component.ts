import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-system-view',
  templateUrl: './system-view.component.html',
  styleUrls: ['./system-view.component.css']
})
export class SystemViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(document).ready(function() {
      $('#myCarousel').carousel({
        interval: 3000
      });
      $('#myCarousel1').carousel({
        interval: 4000
      });
    });

  }

}
