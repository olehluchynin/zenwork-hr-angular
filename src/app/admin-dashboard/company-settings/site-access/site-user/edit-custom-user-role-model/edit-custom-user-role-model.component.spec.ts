import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomUserRoleModelComponent } from './edit-custom-user-role-model.component';

describe('EditCustomUserRoleModelComponent', () => {
  let component: EditCustomUserRoleModelComponent;
  let fixture: ComponentFixture<EditCustomUserRoleModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCustomUserRoleModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCustomUserRoleModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
