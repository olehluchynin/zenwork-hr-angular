import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SiteAccessRolesService } from '../../../../../services/site-access-roles-service.service';

@Component({
  selector: 'app-edit-custom-user-role-model',
  templateUrl: './edit-custom-user-role-model.component.html',
  styleUrls: ['./edit-custom-user-role-model.component.css']
})
export class EditCustomUserRoleModelComponent implements OnInit {

  updateDeleteArray = [];
  postData = {}
  companyId: any;
  constructor(public dialogRef: MatDialogRef<EditCustomUserRoleModelComponent>,
    private siteAccessRolesService: SiteAccessRolesService) { }

  sampleArray =
    [

    ]


  ngOnInit() {
    this.companyId = JSON.parse(localStorage.getItem('companyId'))
    this.allPagesData()
  }

  /* Description: getting all pages data
   author : vipin reddy */

  allPagesData() {
    this.siteAccessRolesService.allPagesData(this.companyId)
      .subscribe((res: any) => {
        console.log("respages", res);
        this.sampleArray = res.data;
      },
        (err) => {
          console.log("err");

        })
  }


  /* Description: update pagewise access with baseroles
   author : vipin reddy */
  updatePageRoles() {
    console.log('deleteArray', this.updateDeleteArray);

    if (this.updateDeleteArray.length == 1) {
      console.log(this.postData);
      delete this.postData['createdAt'];
      delete this.postData['name'];
      delete this.postData['pageid'];
      delete this.postData['status'];
      delete this.postData['updatedAt'];
      console.log(this.postData);

      this.siteAccessRolesService.updateRoles(this.postData)
        .subscribe((res: any) => {
          console.log("respages", res);
          if (res.status == true) {
            this.cancel()
          }
          // this.sampleArray = res.data;

          this.allPagesData();
        },
          (err) => {
            console.log("err");

          })
    }
    if (this.updateDeleteArray.length > 1) {
      for (var i = 0; i < this.updateDeleteArray.length; i++) {

        delete this.updateDeleteArray[i]['createdAt'];
        delete this.updateDeleteArray[i]['name'];
        delete this.updateDeleteArray[i]['pageid'];
        delete this.updateDeleteArray[i]['status'];
        delete this.updateDeleteArray[i]['updatedAt'];
        delete this.updateDeleteArray[i]['pageType'];

      }

      // var result = this.updateDeleteArray.map(function(el) {
      //   var o = Object.assign({}, el);
      //   o.pageType = 'derived';
      //   return o;
      // })
      // console.log("asjkdhjkasdh123",result,this.updateDeleteArray);

      // this.updateDeleteArray = result;
      console.log(this.updateDeleteArray, 'final array');

      console.log('1q2w3e', this.updateDeleteArray);

      this.siteAccessRolesService.updateMultipleRoles(this.updateDeleteArray)
        .subscribe((res: any) => {
          console.log("respages multi", res);
          this.allPagesData()
          if (res.status == true) {
            this.cancel()
          }
          // this.sampleArray = res.data;
        },
          (err) => {
            console.log(err);

          })
    }
  }


  /* Description: change access level of page wise with baseroles
   author : vipin reddy */


  changeaccessLevel(data) {
    console.log(data);
    // if(data._id ==
    var index = -1;
    for (var i = 0; i < this.updateDeleteArray.length; i++) {
      if (data._id === this.updateDeleteArray[i]._id)
        index = i;
    }
    if (index >= 0) {
      this.updateDeleteArray[index] = data;
      this.postData = data;


      // this.updatePagesRoles(data)
    }
    else {
      this.updateDeleteArray.push(data);
      // this.updataMultipleRoles(data)
    }
    console.log("111111111", this.updateDeleteArray);

  }

  /* Description: close the popup
   author : vipin reddy */
  cancel() {

    this.dialogRef.close()
  }

}
