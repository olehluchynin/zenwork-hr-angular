import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CreateCustomUserRoleModelComponent } from './site-user/create-custom-user-role-model/create-custom-user-role-model.component';
import { EditCustomUserRoleModelComponent } from './site-user/edit-custom-user-role-model/edit-custom-user-role-model.component';
import { SiteAccessRolesService } from '../../../services/site-access-roles-service.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { EmployeeService } from '../../../services/employee.service';
import { ChangeUserRoleComponent } from './change-user-role/change-user-role.component';


@Component({
  selector: 'app-site-access',
  templateUrl: './site-access.component.html',
  styleUrls: ['./site-access.component.css']
})
export class SiteAccessComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  companyRoles: any;
  popupEditData: any;
  slectedUserType: any;
  selectedEmployees = [];
  showEmployees: boolean = false;
  addShowHide: boolean = false;
  resRolesObj: any;
  tempEmpIds: any;
  pageNo: any;
  perPage: any;
  roleID: any;
  count: any;

  selectedId: any;
  changeUserRoleData: any;

  popupRoleData: any;
  selectedRoleId: any;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private siteAccessRolesService: SiteAccessRolesService,
    private swalAlertService: SwalAlertService,
    private employeeService: EmployeeService
  ) { }

  public standards = ["standards1", "standards2", "standards3"]

  ngOnInit() {
    //npm multi select dropdown sample start
    this.perPage = 5;
    this.pageNo = 1;
    this.getAllroles()
    this.getBaseRoles();

  }
  /* Description: get custom roles
   author : vipin reddy */
  getAllroles() {
    var cID = localStorage.getItem('companyId')
    console.log("cid", cID);
    cID = cID.replace(/^"|"$/g, "");
    console.log("cin2", cID);


    this.siteAccessRolesService.companyId(cID)
      .subscribe((res: any) => {
        console.log("get ROles", res);
        this.companyRoles = res.data;
        var result = this.companyRoles.map(function (el) {
          var o = Object.assign({}, el);
          o.isActive = false;
          return o;
        })
        this.companyRoles = result;
        console.log(this.companyRoles, 'custom create');

      },
        (err) => {
          console.log(err);


        })
  }

  /* Description: get baseroles
   author : vipin reddy */
  getBaseRoles() {
    this.siteAccessRolesService.getBaseRoles()
      .subscribe((res: any) => {
        console.log("roles", res);

        this.resRolesObj = res.data;
      },
        (err) => {

        })
  }

  /* Description: single edit delete for checkbox
     author : vipin reddy */
  singleEditDelete(data) {
    console.log(data);
    if (data.isActive == false) {
      this.popupEditData = ''
      this.addShowHide = false;
    }
    if (data.isActive == true) {
      this.companyRoles.forEach(element => {


        if (element._id === data._id) {
          element.isActive = true;
          this.addShowHide = true;
          this.popupEditData = element;
          this.selectedId = data._id;
        }
        else {
          element.isActive = false;
        }
        // console.log(element._id, data._id, element.isActive);
      });
    }
  }
  /* Description: getting users for custom role wise
   author : vipin reddy */


  postIdforEmployees(event) {
    console.log(event);
    var cID = JSON.parse(localStorage.getItem('companyId'))
    this.roleID = event
    this.siteAccessRolesService.getAllEmployeesforCustomRoles( this.roleID)
      .subscribe((res: any) => {
        console.log("get ROles", res);
        this.tempEmpIds = res.data;
        this.getEmpNames(this.tempEmpIds)


      },
        (err) => {
          console.log(err);

        })


  }

  pageEvents(event: any) {
    console.log(event);
    this.pageNo = event.pageIndex + 1;
    this.perPage = event.pageSize;
    this.postIdforEmployees(this.roleID);
  }


  getEmpNames(empIds) {
    var postData = {
      "per_page": this.perPage,
      "page_no": this.pageNo,
      "_ids": empIds
    }

    this.employeeService.getOnboardedEmployeeDetails(postData)
      .subscribe((res: any) => {
        console.log("get ROle of employees", res);
        this.selectedEmployees = res.data;
        var result = this.selectedEmployees.map(function (el) {
          var o = Object.assign({}, el);
          o.isActive = false;
          return o;
        })
        this.count = res.total_count;
      },
        (err) => {
          console.log(err);

        })
  }


  /* Description: before page redirect
     author : vipin reddy */

  previousPage() {
    this.router.navigate(['/admin/admin-dashboard/company-settings']);
  }
  /* Description: create/add the popup
     author : vipin reddy */
  openDialog(event): void {
    if (event == 'add') {
      var data = {
        data: 'CreateCustomUserRole'
      }
      const dialogRef = this.dialog.open(CreateCustomUserRoleModelComponent, {
        width: '2000px',
        height: '700px',
        backdropClass: 'custom-user-role-model',
        data: data,

      });
      dialogRef.afterClosed().subscribe((result: any) => {
        this.getAllroles()
      });
    }
    if (event == 'edit') {

      var data1 = {
        data: this.popupEditData

      }

      const dialogRef = this.dialog.open(CreateCustomUserRoleModelComponent, {
        width: '2000px',
        height: '700px',
        backdropClass: 'custom-user-role-model',
        data: data1,

      });

      dialogRef.afterClosed().subscribe((result: any) => {
        this.getAllroles();
        this.addShowHide = false;

      });
    }
  }
  /* Description: configure base roles for pages ACLS
     author : vipin reddy */
  openDialog1(): void {


    var data = {
      data: 'CreateCustomUserRole'
    }

    const dialogRef = this.dialog.open(EditCustomUserRoleModelComponent, {
      width: '1600px',
      height: '700px',
      backdropClass: 'custom-user-role-model',
      data: data,

    });

    dialogRef.afterClosed().subscribe((result: any) => {
      console.log("resultttt", result);

    });
  }

  openDialogforChangeRole() {
    const dialogRef = this.dialog.open(ChangeUserRoleComponent, {
      width: '1300px',
      height: '600px',
      backdropClass: 'custom-user-role-model',
      data: this.popupRoleData,

    });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log("resultttt", result);
      this.slectedUserType = '';
      this.selectedEmployees = [];
    });
  }

  /* Description: delete custom user role
   author : vipin reddy */

  deleteCustomUserRole() {
    this.siteAccessRolesService.deleteRole(this.popupEditData._id)
      .subscribe((res: any) => {
        console.log("get ROles", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Delete Role", res.message, "success")
          this.addShowHide = false;
          this.getAllroles();
        }
      },
        (err) => {
          console.log(err);
          this.swalAlertService.SweetAlertWithoutConfirmation("Delete Role", err.error.message, "error")
        })

  }


  copyData() {
    var cID = JSON.parse(localStorage.getItem('companyId'))
    var copyData: any = {
      companyId: cID,
      _id: this.selectedId
    }

    this.employeeService.copyService(copyData)
      .subscribe((res: any) => {
        console.log("copyy data", res);
      },
        (err) => {
          console.log(err);

        });

  }

  singleEmpEdit(data) {
    console.log(data);
    // this.changeUserRoleData = data;
    if (data.isActive == false) {
      this.popupRoleData = ''
      // this.addShowHide = false;
    }
    if (data.isActive == true) {
      this.selectedEmployees.forEach(element => {


        if (element._id === data._id) {
          element.isActive = true;
          // this.addShowHide = true;
          this.popupRoleData = element;
          this.selectedRoleId = data._id;
        }
        else {
          element.isActive = false;
        }
        // console.log(element._id, data._id, element.isActive);
      });
    }

  }



}
