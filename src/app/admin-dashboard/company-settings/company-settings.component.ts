import { Component, OnInit } from '@angular/core';
import { Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError} from '@angular/router';
import { LoaderService } from '../../services/loader.service'; 

@Component({
  selector: 'app-company-settings',
  templateUrl: './company-settings.component.html',
  styleUrls: ['./company-settings.component.css']
})
export class CompanySettingsComponent implements OnInit {

  constructor(
    private loaderService:LoaderService,
    private router:Router
  ) { 
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }

  ngOnInit() {
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.loader(true);
    }
    if (event instanceof NavigationEnd) {
      this.loaderService.loader(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loaderService.loader(false);
    }
    if (event instanceof NavigationError) {
      this.loaderService.loader(false);
    }
  }

}
