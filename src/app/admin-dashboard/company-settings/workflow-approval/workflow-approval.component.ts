import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { SiteAccessRolesService } from '../../../services/site-access-roles-service.service';
import { WorkflowService } from '../../../services/workflow.service';
import { SwalAlertService } from '../../../services/swalAlert.service';
import { NgModuleRef } from '@angular/core/src/render3';


@Component({
  selector: 'app-workflow-approval',
  templateUrl: './workflow-approval.component.html',
  styleUrls: ['./workflow-approval.component.css']
})
export class WorkflowApprovalComponent implements OnInit {

  modalRef: BsModalRef;
  selectedField: any;
  subHeader: any;
  mangerLevels: any;
  hrLevels: any;
  isValidHrlevel: boolean = false;
  isValidManagerlevel: boolean = false;


  constructor(
    private modalService: BsModalService,
    private router: Router,
    private siteAccessRolesService: SiteAccessRolesService,
    private workflowService: WorkflowService,
    private swalAlertService: SwalAlertService
  ) { }


  hrData: any;
  managerData: any;
  managerData1: any;
  splRoleData: any;

  // hrData = [{ name: "HR", _id: "5c9e11894b1d726375b2sdfsd" }, { name: "Hr2", _id: "5cd28a60bd134f6d5b83972b" }, { name: "Hr3", _id: "5c9e1394b1d726asdasd33" }]
  // managerData = [{ name: "manager", _id: "5c9e11894b1d726375b2shdh7" }, { name: "manager 1", _id: "5cd00f0c0606e63cd0b385a7" }, { name: "manager 2", _id: "5c9e1189dhd675b2shdh7" }]

  public priorities;
  employeeData = [];


  priorityUsers = [];
  priorityUsersJob = [];
  priorityUsersCompensation = [];

  empName: any;
  managerName: any;
  hrName: any;
  splRole: any;

  jobEmpName: any;
  jobHrName: any;
  jobSplRole: any;
  jobManagerName: any;

  compensationManagerName: any;
  compensationHrName: any;
  compensationSplRole: any;
  compensationEmpName: any;


  workflowId: any;
  jobWorkflowId: any;
  compensationWorkflowId: any;

  updateWorkflow: boolean = false;
  updateJobWorkflow: boolean = false;
  updateCompensationWorkflow: boolean = false;



  persons = ["person1", "person2", "person3"]
  resRolesObj: any;
  managerId: any;
  HrId: any;
  employeeBaseId: any;
  Cid: any;
  sysAdminId: any;

  // HrEmployees: any;
  // managerEmployees: any;

  ngOnInit() {


    this.Cid = JSON.parse(localStorage.getItem('companyId'))
    this.employeeSearch();
    this.getAllBaseRoles();
    // this.getHrRoles()
    this.getManagerRoles()
    this.gettingLevelsHr()
    this.gettingLevelsManager()
    this.sysAdminRoles()
    this.priorities = ["First", "Second", "Third", "Fourth"];
    // this.employeeSearch()
    this.getDemographicWorkFlow(this.Cid)
    this.getJobWorkFlow(this.Cid)
    this.getCompensationWorkFlow(this.Cid)


  }


  gettingLevelsHr() {
    var cmpnyId = JSON.parse(localStorage.getItem('companyId'))
    this.workflowService.gettingofHrlevels(cmpnyId)
      .subscribe((res: any) => {
        console.log("roles", res);
        this.hrData = res.data;
        this.hrLevels = this.hrData.length;
      },
        (err) => {
          console.log(err);

        })
  }

  gettingLevelsManager() {
    var cmpnyId = JSON.parse(localStorage.getItem('companyId'))
    this.workflowService.gettingofManagerlevels(cmpnyId)
      .subscribe((res: any) => {
        console.log("roles", res);
        this.managerData = res.data;
        this.mangerLevels = this.managerData.length;
      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: get baseroles
  author : vipin reddy */
  getAllBaseRoles() {
    this.siteAccessRolesService.getBaseRoles()
      .subscribe((res: any) => {
        console.log("roles", res);

        this.resRolesObj = res.data;
        for (var i = 0; i < this.resRolesObj.length; i++) {
          console.log(this.resRolesObj, "rolesssss");

          if (this.resRolesObj[i].name == 'Manager') {
            this.managerId = this.resRolesObj[i]._id;
            console.log(this.managerId);
            // this.getManagerRoles();
          }
          if (this.resRolesObj[i].name == "System Admin") {
            this.sysAdminId = this.resRolesObj[i]._id;
            // this.getHrRoles();
          }
          if (this.resRolesObj[i].name == "Employee") {
            this.employeeBaseId = this.resRolesObj[i]._id;

          }


        }

      },
        (err) => {
          console.log(err);

        })
  }

  /* Description: get only Hr employees
 author : vipin reddy */

  // getHrRoles() {
  //   this.HrId = "5cbe98f8561562212689f748"
  //   this.workflowService.getHrRoleEmployees(this.Cid, this.HrId)
  //     .subscribe((res: any) => {
  //       console.log("HR roles", res);
  //       this.hrData = res.data;

  //     },
  //       (err) => {
  //         console.log(err);

  //       })

  // }
  /* Description: get only managers employees
author : vipin reddy */

  getManagerRoles() {
    console.log("vippip");
    this.managerId = "5cbe98a3561562212689f747"
    this.workflowService.getManagerEmployees(this.Cid, this.managerId)
      .subscribe((res: any) => {
        console.log("manager roles", res);
        this.managerData1 = res.data;

      },
        (err) => {
          console.log(err);

        })
  }

  sysAdminRoles() {
    console.log("vippip", this.sysAdminId);
    this.sysAdminId = "5cbe9922561562212689f74a",
      this.workflowService.getManagerEmployees(this.Cid, this.sysAdminId)
        .subscribe((res: any) => {
          console.log("sys admin roles", res);
          this.splRoleData = res.data;
          this.managerData1.forEach(element => {
            this.splRoleData.push(element)
          });

          console.log(this.splRoleData, 'splroledata');

        },
          (err) => {
            console.log(err);

          })

  }



  /* Description: getting employees list
    author : vipin reddy */
  employeeSearch() {
    var data = {
      name: ''
    }
    this.siteAccessRolesService.getAllEmployees(data)
      .subscribe((res: any) => {
        console.log("employees", res);
        this.employeeData = res.data;

      },
        (err) => {
          console.log(err);

        })
  }


  /* Description: get workflow details
 author : vipin reddy */
  getDemographicWorkFlow(Cid) {
    this.employeeSearch()
    this.workflowService.getWorkflowservice(Cid, "demographic")



      .subscribe((res: any) => {
        console.log("get workflow", res);
        console.log(this.priorityUsers, 'AAAARRRAAYY');




        if (res.data.length >= 1) {
          this.workflowId = res.data[0]._id;
          this.updateWorkflow = true;
          if (res.data[0].hr) {
            this.hrName = res.data[0].hr.levelId;
            console.log(this.hrData);

            let index = this.hrData.findIndex(userData => userData._id === res.data[0].hr.levelId)
            console.log(index);

            let user: any = this.hrData[index]
            console.log(user);

            var name = user.levelName;
            var _id = user._id;

            // user['userType'] = 'hr';

            this.priorityUsers[(res.data[0].hr.priority) - 1] = { name, _id, userType: 'hr' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
            console.log("res get of workflow", user);
          }
          if (res.data[0].reportsTo) {
            this.managerName = res.data[0].reportsTo.levelId;
            let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
            console.log(indexManager, this.managerData);

            let userManager = this.managerData[indexManager]
            // userManager['userType'] = 'manager';
            var name = userManager.levelName;
            var _id = userManager._id;
            this.priorityUsers[(res.data[0].reportsTo.priority) - 1] = { name, _id, userType: 'manager' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
            console.log(this.priorityUsers);

            console.log("res get of workflow", userManager, res.data[0].specialPerson);
          }
          if (res.data[0].specialPerson) {
            this.empName = res.data[0].specialPerson.personId;
            console.log(this.employeeData, res.data[0].specialPerson.personId, "77777777777");
            console.log(this.priorityUsers);

            let indexSpecialPerson1 = this.employeeData.findIndex(userData => userData._id === res.data[0].specialPerson.personId)
            console.log("indexx value", indexSpecialPerson1);

            let userSpecialManager: any = this.employeeData[indexSpecialPerson1]
            console.log(userSpecialManager, "11111");
            console.log(userSpecialManager, "vivpin spl manaager");

            var name = userSpecialManager.personal.name.preferredName;
            var _id = userSpecialManager._id;
            console.log("index3", res.data[0].specialPerson.priority);

            this.priorityUsers[(res.data[0].specialPerson.priority) - 1] = { name, _id, userType: 'specialPerson' }
            console.log(this.priorityUsers[3], "users");

            console.log("res get of workflow", { name, _id });
            console.log(this.priorityUsers);
          }
          if (res.data[0].specialRole) {
            this.splRole = res.data[0].specialRole.roleId;
            console.log(this.splRoleData);

            let indexSpecialRole = this.splRoleData.findIndex(userData => userData._id === res.data[0].specialRole.roleId)
            console.log("indexx value", indexSpecialRole);

            let userSpecialManagerole: any = this.splRoleData[indexSpecialRole]
            console.log(userSpecialManagerole, "11111");

            var name = userSpecialManagerole.name;
            var _id = userSpecialManagerole._id;

            this.priorityUsers[(res.data[0].specialRole.priority) - 1] = { name, _id, userType: 'specialRole' }
            console.log("res get of workflow", { name, _id });
            console.log(this.priorityUsers);
          }
        }

      },
        (err) => {
          console.log("error", err);

        })
  }


  /* Description: selcted person comes to drag and drop section for specialPerson employee only when create
author : vipin reddy */
  empSpecialPerson($event) {
    let index = this.employeeData.findIndex(userData => userData._id === $event)
    let user = this.employeeData[index]

    console.log("event and data", this.empName, $event);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "specialPerson");
    var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsers[spIndex] = splPerson
    } else {
      this.priorityUsers.push(splPerson)

    }
    console.log("spname", this.priorityUsers);

  }

  empSplRole($event) {

    let index = this.splRoleData.findIndex(userData => userData._id === $event)
    let user = this.splRoleData[index]

    console.log("event and data", this.splRole, $event);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "specialRole");
    var splPerson = { userType: "specialRole", name: user.name, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsers[spIndex] = splPerson
    } else {
      this.priorityUsers.push(splPerson)

    }
    console.log("spname", this.priorityUsers);


  }

  empSplRoleUpdate($event) {


    let index = this.splRoleData.findIndex(userData => userData._id === $event)
    let user = this.splRoleData[index]

    console.log("event and data", this.splRole, $event);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "specialRole");
    var splPerson1 = { userType: "specialRole", name: user.name, _id: user._id }

    if (spIndex == -1) {
      this.priorityUsers[this.priorityUsers.length] = splPerson1
    }
    else {
      this.priorityUsers[spIndex] = splPerson1
    }


    console.log("spname role update", this.priorityUsers);

  }

  /* Description: selcted person comes to drag and drop section for specialPeeson employee only when Update
author : vipin reddy */

  empSpecialPersonUpdate($event) {

    let index = this.employeeData.findIndex(userData => userData._id === $event)
    let user = this.employeeData[index]

    console.log("event and data", this.empName, $event);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "specialPerson");
    var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id }

    if (spIndex == -1) {
      this.priorityUsers[this.priorityUsers.length] = splPerson
    }
    else {
      this.priorityUsers[spIndex] = splPerson
    }

    console.log("spname", this.priorityUsers);


  }

  /* Description: selcted person comes to drag and drop section for manager employee only when create
 author : vipin reddy */
  empManagerPerson($event) {
    console.log($event);
    let index = this.managerData.findIndex(userData => userData._id === $event)
    let user: any = this.managerData[index]

    console.log("event and data", this.managerName, $event);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "manager");
    var manager = { userType: "manager", name: user.levelName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsers[spIndex] = manager
    } else {
      this.priorityUsers.push(manager)

    }
    console.log("spname", this.priorityUsers);
  }

  /* Description: selcted person comes to drag and drop section for Manager employee only when update
author : vipin reddy */
  empManagerPersonUpdate($event) {
    console.log(this.priorityUsers);

    let index = this.managerData.findIndex(userData => userData._id === $event)
    console.log(index, 'manager');

    let user: any = this.managerData[index]

    console.log("event and data", this.managerName, $event, user);
    var spIndex1 = this.priorityUsers.findIndex(e => e.userType === "manager");
    var manager = { userType: "manager", name: user.levelName, _id: user._id }

    console.log(spIndex1, manager);
    if (spIndex1 == -1) {
      this.priorityUsers[this.priorityUsers.length] = manager
    }
    else {
      this.priorityUsers[spIndex1] = manager
    }


  }

  /* Description: selcted person comes to drag and drop section for HR employee only when create
author : vipin reddy */
  empHrPerson($event) {
    console.log($event);
    let index = this.hrData.findIndex(userData => userData._id === $event)
    let user: any = this.hrData[index]
    console.log("event and data", this.hrName, $event, user);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "hr");
    var hr = { name: user.levelName, _id: user._id, userType: "hr" }
    if (spIndex >= 0) {
      this.priorityUsers[spIndex] = hr
    } else {
      this.priorityUsers.push(hr)

    }
    console.log("spname", this.priorityUsers);

  }

  /* Description: selcted person comes to drag and drop section for HR employee only when update
 author : vipin reddy */

  empHrPersonUpdate($event) {
    console.log($event);
    let index = this.hrData.findIndex(userData => userData._id === $event)
    let user: any = this.hrData[index];
    console.log("event and data", this.hrName, $event);
    var spIndex = this.priorityUsers.findIndex(e => e.userType === "hr");
    var hr = { name: user.levelName, _id: user._id, userType: "hr", }
    if (spIndex == -1) {
      this.priorityUsers[this.priorityUsers.length] = hr
    }
    else {
      this.priorityUsers[spIndex] = hr
    }

    console.log("spname", this.priorityUsers);
  }

  /* Description: send (POST) demographic workflow
 author : vipin reddy */
  workflowSubmit() {
    console.log("asdasdasdas");

    for (var i = 0; i < this.priorityUsers.length; i++) {
      if (this.priorityUsers[i].userType == "manager") {
        var prioritymanager = i + 1;
        var pID = this.priorityUsers[i]._id;
      }
      if (this.priorityUsers[i].userType == "hr") {
        var priorityHr = i + 1;
        var pIdHR = this.priorityUsers[i]._id;
      }
      if (this.priorityUsers[i].userType == "specialPerson") {
        var prioritySP = i + 1;
        var pIdSP = this.priorityUsers[i]._id;
      }
      if (this.priorityUsers[i].userType == 'specialRole') {
        var prioritySR = i + 1;
        var pIdSR = this.priorityUsers[i]._id;
      }
    }
    var Cid = JSON.parse(localStorage.getItem('companyId'))



    var postdata = {
      workflowType: 'demographic',
      reportsTo: {
        priority: prioritymanager,
        levelId: pID
      },
      hr: {
        priority: priorityHr,
        levelId: pIdHR
      },
      specialPerson: {
        priority: prioritySP,
        personId: pIdSP
      },
      specialRole: {
        priority: prioritySR,
        roleId: pIdSR
      },

      // reportedBy: this.employeeBaseId,
      companyId: Cid,
      // updatedBy: Cid,
      // createdBy: Cid,


    }

    console.log("postDATa", postdata);

    this.workflowService.createWorkFlow(postdata)
      .subscribe((res: any) => {
        console.log("post weokflow", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('workflow approval', res.message, "success")
          this.modalRef.hide();
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Workflow Approval", err.error.message, "error")

        })

  }

  /* Description: update (PUT) demographic workflow
 author : vipin reddy */

  updateDemographicWorkflow() {
    console.log(this.priorityUsers, 'users');

    for (var i = 0; i < this.priorityUsers.length; i++) {
      if (this.priorityUsers[i].userType == "manager") {
        console.log(i);

        var pID = this.priorityUsers[i]._id;
        var prioritymanager = i + 1;

      }
      if (this.priorityUsers[i].userType == "hr") {
        console.log(i);
        var pIdHR = this.priorityUsers[i]._id;
        var priorityHr = i + 1;

      }
      if (this.priorityUsers[i].userType == "specialPerson") {
        var pIdSP = this.priorityUsers[i]._id;
        var prioritySP = i + 1;
      }
      if (this.priorityUsers[i].userType == 'specialRole') {
        var pIdSR = this.priorityUsers[i]._id;
        var prioritySR = i + 1;
      }
    }
    console.log(pIdHR, 'hrpersonid', pIdSR, "specialperson id")
    var postdata = {
      workflowType: 'demographic',

      reportsTo: {
        priority: prioritymanager,
        levelId: pID
      },
      hr: {
        priority: priorityHr,
        levelId: pIdHR
      },
      specialPerson: {
        priority: prioritySP,
        personId: pIdSP
      },
      specialRole: {
        priority: prioritySR,
        roleId: pIdSR
      },
    }

    console.log("update work flow data", postdata);

    this.workflowService.updateDemographicWorkflow(this.workflowId, postdata)
      .subscribe((res: any) => {
        console.log("update demographic workflow", res);
        if (res.status == true)
          this.modalRef.hide();

      },
        (err) => {



        })


  }

  /* Description: drag and drop for priority changes in demographic workflow
author : vipin reddy */
  drop(event: CdkDragDrop<string[]>) {

    moveItemInArray(this.priorityUsers, event.previousIndex, event.currentIndex);
    console.log("drag event", event, this.priorityUsers);

  }

  // ===================================================================JOB CHANGE WORK FLOW  ===============================================================================
  /* Description: get job workflow details
 author : vipin reddy */
  getJobWorkFlow(cid) {
    this.workflowService.getWorkflowservice(cid, "job")
      .subscribe((res: any) => {
        console.log("get job workflow", res);
        if (res.data.length >= 1) {
          this.jobWorkflowId = res.data[0]._id;
          this.updateJobWorkflow = true;
          if (res.data[0].hr) {
            this.jobHrName = res.data[0].hr.levelId;
            let index = this.hrData.findIndex(userData => userData._id === res.data[0].hr.levelId)
            let user: any = this.hrData[index]
            // user['userType'] = 'hr';
            var name = user.levelName;
            var _id = user._id;
            this.priorityUsersJob[(res.data[0].hr.priority) - 1] = { name, _id, userType: 'hr' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
            console.log("res get of workflow", user);
          }
          if (res.data[0].reportsTo) {
            this.jobManagerName = res.data[0].reportsTo.levelId;
            let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
            let userManager = this.managerData[indexManager]
            // userManager['userType'] = 'manager';
            var name = userManager.levelName;
            var _id = userManager._id;

            this.priorityUsersJob[(res.data[0].reportsTo.priority) - 1] = { name, _id, userType: 'manager' }// temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
            console.log("res get of workflow", userManager);
          }
          if (res.data[0].specialPerson) {
            this.jobEmpName = res.data[0].specialPerson.personId;
            let indexSpecialPerson = this.employeeData.findIndex(userData => userData._id === res.data[0].specialPerson.personId)
            let userSpecialManager = this.employeeData[indexSpecialPerson]
            console.log(userSpecialManager, 'newqqqqqqqqqqqqq1');

            var name = userSpecialManager.personal.name.preferredName;
            var _id = userSpecialManager._id;

            this.priorityUsersJob[(res.data[0].specialPerson.priority) - 1] = { name, _id, userType: 'specialPerson' }
            console.log("res get of workflow", { name, _id, userType: 'specialPerson' });
          }
          if (res.data[0].specialRole) {
            this.jobSplRole = res.data[0].specialRole.roleId;
            console.log(this.splRoleData);

            let indexSpecialRole = this.splRoleData.findIndex(userData => userData._id === res.data[0].specialRole.roleId)
            console.log("indexx value", indexSpecialRole);

            let userSpecialManagerole: any = this.splRoleData[indexSpecialRole]
            console.log(userSpecialManagerole, "11111");

            var name = userSpecialManagerole.name;
            var _id = userSpecialManagerole._id;

            this.priorityUsersJob[(res.data[0].specialRole.priority) - 1] = { name, _id, userType: 'specialRole' }
            console.log("res get of workflow", { name, _id });
            console.log(this.priorityUsersJob);
          }
        }

      },
        (err) => {

          console.log("error", err);

        })
  }


  /* Description: selcted person comes to drag and drop section for manager  only when create(Job change workflow)
author : vipin reddy */
  empJobManagerPerson($event) {

    console.log($event);
    let index = this.managerData.findIndex(userData => userData._id === $event)
    let user: any = this.managerData[index]

    console.log("event and data", this.jobManagerName, $event);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "manager");
    var manager = { userType: "manager", name: user.levelName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersJob[spIndex] = manager
    } else {
      this.priorityUsersJob.push(manager)

    }
    console.log("spname", this.priorityUsersJob);

  }

  /* Description: selcted person comes to drag and drop section for manager  only when Update(Job change workflow)
author : vipin reddy */
  empJobManagerPersonUpdate($event) {
    console.log(this.priorityUsers);

    let index = this.managerData.findIndex(userData => userData._id === $event)
    let user: any = this.managerData[index]

    console.log("event and data", this.jobManagerName, $event, user);
    var spIndex1 = this.priorityUsersJob.findIndex(e => e.userType === "manager");
    var manager = { userType: "manager", name: user.levelName, _id: user._id }

    console.log(spIndex1, manager);

    if (spIndex1 == -1) {
      this.priorityUsersJob[this.priorityUsersJob.length] = manager
    }
    else {
      this.priorityUsersJob[spIndex1] = manager
    }

  }

  /* Description: selcted person comes to drag and drop section for Hr  only when create(Job change workflow)
author : vipin reddy */
  empJobHrPerson($event) {
    console.log($event);
    let index = this.hrData.findIndex(userData => userData._id === $event)
    let user: any = this.hrData[index]
    console.log("event and data", this.jobHrName, $event);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "hr");
    var hr = { userType: "hr", name: user.levelName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersJob[spIndex] = hr
    } else {
      this.priorityUsersJob.push(hr)

    }
    console.log("spname", this.priorityUsersJob);


  }

  /* Description: selcted person comes to drag and drop section for HR only when Update(Job change workflow)
author : vipin reddy */
  empJobHrPersonUpdate($event) {
    console.log($event);
    let index = this.hrData.findIndex(userData => userData._id === $event)
    let user: any = this.hrData[index];
    console.log("event and data", this.jobHrName, $event);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "hr");
    var hr = { userType: "hr", name: user.levelName, _id: user._id }

    if (spIndex == -1) {
      this.priorityUsersJob[this.priorityUsersJob.length] = hr
    }
    else {
      this.priorityUsersJob[spIndex] = hr
    }

    console.log("spname", this.priorityUsersJob);
  }

  jobSpecialRole($event) {
    let index = this.splRoleData.findIndex(userData => userData._id === $event)
    let user = this.splRoleData[index]

    console.log("event and data", this.jobSplRole, $event, user);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "specialRole");
    var splPerson = { userType: "specialRole", name: user.name, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersJob[spIndex] = splPerson
    } else {
      this.priorityUsersJob.push(splPerson)

    }
    console.log("spname", this.priorityUsers);

  }

  jobSpeciaRoleUpdate($event) {
    let index = this.splRoleData.findIndex(userData => userData._id === $event)
    let user = this.splRoleData[index]

    console.log("event and data", this.jobSplRole, $event);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "specialRole");
    var splPerson1 = { userType: "specialRole", name: user.name, _id: user._id }

    if (spIndex == -1) {
      this.priorityUsersJob[this.priorityUsersJob.length] = splPerson1
    }
    else {
      this.priorityUsersJob[spIndex] = splPerson1
    }

    console.log("spname role update", this.priorityUsersJob);
  }

  /* Description: selcted person comes to drag and drop section for specialPerson  only when create(Job change workflow)
author : vipin reddy */
  empJobSpecialPerson($event) {
    let index = this.employeeData.findIndex(userData => userData._id === $event)
    let user = this.employeeData[index]

    console.log("event and data", this.jobEmpName, $event);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "specialPerson");
    var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersJob[spIndex] = splPerson
    } else {
      this.priorityUsersJob.push(splPerson)

    }
    console.log("spname", this.priorityUsersJob);

  }

  /* Description: selcted person comes to drag and drop section for specialPerson  only when Update(Job change workflow)
author : vipin reddy */
  empJobSpecialPersonUpdate($event) {
    let index = this.employeeData.findIndex(userData => userData._id === $event)
    let user = this.employeeData[index]

    console.log("event and data", this.jobEmpName, $event);
    var spIndex = this.priorityUsersJob.findIndex(e => e.userType === "specialPerson");
    var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id }

    if (spIndex == -1) {
      this.priorityUsersJob[this.priorityUsersJob.length] = splPerson
    }
    else {
      this.priorityUsersJob[spIndex] = splPerson
    }

    console.log("spname", this.priorityUsersJob);


  }

  /* Description: drag and drop for priority changes in job change workflow
 author : vipin reddy */
  dropForJob(event: CdkDragDrop<string[]>) {

    moveItemInArray(this.priorityUsersJob, event.previousIndex, event.currentIndex);
    console.log("drag event", event, this.priorityUsersJob);
  }

  /* Description: send (POST) job change workflow
 author : vipin reddy */
  jobWorkflowSubmit() {
    console.log("asdasdasdas");

    for (var i = 0; i < this.priorityUsersJob.length; i++) {
      if (this.priorityUsersJob[i].userType == "manager") {
        var prioritymanager = i + 1;
        var pID = this.priorityUsersJob[i]._id;
      }
      if (this.priorityUsersJob[i].userType == "hr") {
        var priorityHr = i + 1;
        var pIdHR = this.priorityUsersJob[i]._id;
      }
      if (this.priorityUsersJob[i].userType == "specialRole") {
        var prioritySR = i + 1;
        var pIdSR = this.priorityUsersJob[i]._id;
      }
      if (this.priorityUsersJob[i].userType == "specialPerson") {
        var prioritySP = i + 1;
        var pIdSP = this.priorityUsersJob[i]._id;
      }
    }
    var Cid = JSON.parse(localStorage.getItem('companyId'))



    var postdata = {
      workflowType: 'job',
      reportsTo: {
        priority: prioritymanager,
        levelId: pID
      },
      hr: {
        priority: priorityHr,
        levelId: pIdHR
      },
      specialPerson: {
        priority: prioritySP,
        personId: pIdSP
      },
      specialRole: {
        priority: prioritySR,
        roleId: pIdSR
      },

      // reportedBy: this.managerId,
      companyId: Cid,
      // updatedBy: Cid,
      // createdBy: Cid,


    }

    console.log("postDATa", postdata);

    this.workflowService.createWorkFlow(postdata)
      .subscribe((res: any) => {
        console.log("post weokflow", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('workflow approval', res.message, "success")
          this.modalRef.hide();
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Workflow Approval", err.error.message, "error")

        })
  }

  /* Description: update (PUT) job change workflow
 author : vipin reddy */
  jobUpdateWorkflow() {

    for (var i = 0; i < this.priorityUsersJob.length; i++) {
      console.log(this.priorityUsersJob[i]);

      if (this.priorityUsersJob[i].userType == "manager") {
        var prioritymanager = i + 1;
        var pID = this.priorityUsersJob[i]._id;
      }
      if (this.priorityUsersJob[i].userType == "hr") {
        var priorityHr = i + 1;
        var pIdHR = this.priorityUsersJob[i]._id;
      }
      if (this.priorityUsersJob[i].userType == "specialPerson") {
        var prioritySP = i + 1;
        var pIdSP = this.priorityUsersJob[i]._id;
      }
      if (this.priorityUsersJob[i].userType == "specialRole") {
        var prioritySR = i + 1;
        var pIdSR = this.priorityUsersJob[i]._id;
      }
    }

    var postdata = {
      workflowType: 'job',
      reportsTo: {
        priority: prioritymanager,
        levelId: pID
      },
      hr: {
        priority: priorityHr,
        levelId: pIdHR
      },
      specialPerson: {
        priority: prioritySP,
        personId: pIdSP
      },
      specialRole: {
        priority: prioritySR,
        roleId: pIdSR
      }
    }

    console.log("update job workflow", postdata);


    this.workflowService.updateDemographicWorkflow(this.jobWorkflowId, postdata)
      .subscribe((res: any) => {
        console.log("update job workflow", res);
        if (res.status == true)
          this.modalRef.hide();

      },
        (err) => {



        })
  }


  // ===================================================compensation change workflow===================================================


  empCompensationManagerPerson($event) {
    console.log($event);
    let index = this.managerData.findIndex(userData => userData._id === $event)
    let user = this.managerData[index]

    console.log("event and data", this.compensationManagerName, $event);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "manager");
    var manager = { userType: "manager", name: user.levelName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersCompensation[spIndex] = manager
    } else {
      this.priorityUsersCompensation.push(manager)

    }
    console.log("spname", this.priorityUsersCompensation);


  }

  empCompensationManagerPersonUpdate($event) {
    console.log(this.priorityUsersCompensation);

    let index = this.managerData.findIndex(userData => userData._id === $event)
    let user = this.managerData[index]

    console.log("event and data", this.compensationManagerName, $event, user);
    var spIndex1 = this.priorityUsersCompensation.findIndex(e => e.userType === "manager");
    var manager = { userType: "manager", name: user.levelName, _id: user._id }

    console.log(spIndex1, manager);

    if (spIndex1 == -1) {
      this.priorityUsersCompensation[this.priorityUsersCompensation.length] = manager
    }
    else {
      this.priorityUsersCompensation[spIndex1] = manager
    }
  }


  empCompensationHrPerson($event) {

    console.log($event);
    let index = this.hrData.findIndex(userData => userData._id === $event)
    let user = this.hrData[index]
    console.log("event and data", this.compensationHrName, $event);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "hr");
    var hr = { userType: "hr", name: user.levelName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersCompensation[spIndex] = hr
    } else {
      this.priorityUsersCompensation.push(hr)

    }
    console.log("spname", this.priorityUsersCompensation);

  }

  empCompensationHrPersonUpdate($event) {
    console.log($event);
    let index = this.hrData.findIndex(userData => userData._id === $event)
    let user = this.hrData[index];
    console.log("event and data", this.compensationHrName, $event);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "hr");
    var hr = { userType: "hr", name: user.levelName, _id: user._id }
    console.log(spIndex, hr);

    if (spIndex == -1) {
      this.priorityUsersCompensation[this.priorityUsersCompensation.length] = hr
    }
    else {
      this.priorityUsersCompensation[spIndex] = hr
    }

    console.log("spname", this.priorityUsersCompensation);

  }
  compensationSpecialRole($event) {
    let index = this.splRoleData.findIndex(userData => userData._id === $event)
    let user = this.splRoleData[index]

    console.log("event and data", this.jobSplRole, $event, user);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "specialRole");
    var splPerson = { userType: "specialRole", name: user.name, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersCompensation[spIndex] = splPerson
    } else {
      this.priorityUsersCompensation.push(splPerson)

    }
    console.log("spname", this.priorityUsers);
  }

  compensationSpeciaRoleUpdate($event) {
    let index = this.splRoleData.findIndex(userData => userData._id === $event)
    let user = this.splRoleData[index]

    console.log("event and data", this.jobSplRole, $event);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "specialRole");
    var splPerson1 = { userType: "specialRole", name: user.name, _id: user._id }

    if (spIndex == -1) {
      this.priorityUsersCompensation[this.priorityUsersCompensation.length] = splPerson1
    }
    else {
      this.priorityUsersCompensation[spIndex] = splPerson1
    }

    console.log("spname role update", this.priorityUsersCompensation);
  }

  empCompensationSpecialPerson($event) {

    let index = this.employeeData.findIndex(userData => userData._id === $event)
    let user = this.employeeData[index]

    console.log("event and data", this.compensationEmpName, $event);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "specialPerson");
    var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id }
    if (spIndex >= 0) {
      this.priorityUsersCompensation[spIndex] = splPerson
    } else {
      this.priorityUsersCompensation.push(splPerson)

    }
    console.log("spname", this.priorityUsersCompensation);


  }

  empCompensationSpecialPersonUpdate($event) {
    let index = this.employeeData.findIndex(userData => userData._id === $event)
    let user = this.employeeData[index]

    console.log("event and data", this.compensationEmpName, $event);
    var spIndex = this.priorityUsersCompensation.findIndex(e => e.userType === "specialPerson");
    var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id }


    if (spIndex == -1) {
      this.priorityUsersCompensation[this.priorityUsersCompensation.length] = splPerson
    }
    else {
      this.priorityUsersCompensation[spIndex] = splPerson
    }

    console.log("spname", this.priorityUsersCompensation);

  }

  getCompensationWorkFlow(cid) {
    this.workflowService.getWorkflowservice(cid, "compensation")
      .subscribe((res: any) => {
        console.log("get workflow", res);
        if (res.data.length >= 1) {
          this.compensationWorkflowId = res.data[0]._id;
          this.updateCompensationWorkflow = true;
          if (res.data[0].hr) {
            this.compensationHrName = res.data[0].hr.levelId;
            let index = this.hrData.findIndex(userData => userData._id === res.data[0].hr.levelId)
            let user = this.hrData[index]
            // user['userType'] = 'hr';
            var name = user.levelName;
            var _id = user._id;
            this.priorityUsersCompensation[(res.data[0].hr.priority) - 1] = { name, _id, userType: 'hr' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
            console.log("res get of workflow", user);
          }
          if (res.data[0].reportsTo) {
            this.compensationManagerName = res.data[0].reportsTo.levelId;
            let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
            let userManager = this.managerData[indexManager]
            // userManager['userType'] = 'manager';
            var name = userManager.levelName;
            var _id = userManager._id;

            this.priorityUsersCompensation[(res.data[0].reportsTo.priority) - 1] = { name, _id, userType: 'manager' } // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
            console.log("res get of workflow", userManager);
          }
          if (res.data[0].specialPerson) {
            this.compensationEmpName = res.data[0].specialPerson.personId;
            let indexSpecialPerson = this.employeeData.findIndex(userData => userData._id === res.data[0].specialPerson.personId)

            let userSpecialManager = this.employeeData[indexSpecialPerson]
            var name = userSpecialManager.personal.name.preferredName;
            var _id = userSpecialManager._id;

            this.priorityUsersCompensation[(res.data[0].specialPerson.priority) - 1] = { name, _id, userType: 'specialPerson' }
            console.log("res get of workflow", { name, _id, userType: 'specialPerson' });
          }
          if (res.data[0].specialPerson) {
            this.compensationSplRole = res.data[0].specialRole.roleId;
            console.log(this.splRoleData);

            let indexSpecialRole = this.splRoleData.findIndex(userData => userData._id === res.data[0].specialRole.roleId)
            console.log("indexx value", indexSpecialRole);

            let userSpecialManagerole: any = this.splRoleData[indexSpecialRole]
            console.log(userSpecialManagerole, "11111");

            var name = userSpecialManagerole.name;
            var _id = userSpecialManagerole._id;

            this.priorityUsersCompensation[(res.data[0].specialRole.priority) - 1] = { name, _id, userType: 'specialRole' }
            console.log("res get of workflow", { name, _id });
            console.log(this.priorityUsersJob);
          }
        }
      },
        (err) => {
          console.log("error", err);

        })
  }



  dropCompensation(event: CdkDragDrop<string[]>) {

    moveItemInArray(this.priorityUsersCompensation, event.previousIndex, event.currentIndex);
    console.log("drag event", event, this.priorityUsersCompensation);

  }


  /* Description: send (POST) job change workflow
author : vipin reddy */
  compensationWorkflowSubmit() {
    console.log("asdasdasdas");

    for (var i = 0; i < this.priorityUsersCompensation.length; i++) {
      if (this.priorityUsersCompensation[i].userType == "manager") {
        var prioritymanager = i + 1;
        var pID = this.priorityUsersCompensation[i]._id;
      }
      if (this.priorityUsersCompensation[i].userType == "hr") {
        var priorityHr = i + 1;
        var pIdHR = this.priorityUsersCompensation[i]._id;
      }
      if (this.priorityUsersCompensation[i].userType == "specialPerson") {
        var prioritySP = i + 1;
        var pIdSP = this.priorityUsersCompensation[i]._id;
      }
      if (this.priorityUsersCompensation[i].userType == "specialRole") {
        var prioritySR = i + 1;
        var pIdSR = this.priorityUsersCompensation[i]._id;
      }
    }
    var Cid = JSON.parse(localStorage.getItem('companyId'))



    var postdata = {
      workflowType: 'compensation',
      reportsTo: {
        priority: prioritymanager,
        levelId: pID
      },
      hr: {
        priority: priorityHr,
        levelId: pIdHR
      },
      specialPerson: {
        priority: prioritySP,
        personId: pIdSP
      },
      specialRole: {
        priority: prioritySR,
        roleId: pIdSR
      },

      // reportedBy: this.managerId,
      companyId: Cid,
      // updatedBy: Cid,
      // createdBy: Cid,


    }

    console.log("postDATa", postdata);

    this.workflowService.createWorkFlow(postdata)
      .subscribe((res: any) => {
        console.log("post weokflow", res);
        if (res.status == true) {
          this.swalAlertService.SweetAlertWithoutConfirmation('workflow approval', res.message, "success")
          this.modalRef.hide();
        }
      },
        (err) => {
          this.swalAlertService.SweetAlertWithoutConfirmation("Workflow Approval", err.error.message, "error")

        })
  }

  /* Description: update (PUT) job change workflow
  author : vipin reddy */
  compensationUpdateWorkflow() {

    for (var i = 0; i < this.priorityUsersCompensation.length; i++) {
      console.log(this.priorityUsersCompensation[i]);

      if (this.priorityUsersCompensation[i].userType == "manager") {
        var prioritymanager = i + 1;
        var pID = this.priorityUsersCompensation[i]._id;
      }
      if (this.priorityUsersCompensation[i].userType == "hr") {
        var priorityHr = i + 1;
        var pIdHR = this.priorityUsersCompensation[i]._id;
      }
      if (this.priorityUsersCompensation[i].userType == "specialPerson") {
        var prioritySP = i + 1;
        var pIdSP = this.priorityUsersCompensation[i]._id;
      }
      if (this.priorityUsersCompensation[i].userType == "specialRole") {
        var prioritySR = i + 1;
        var pIdSR = this.priorityUsersCompensation[i]._id;
      }
    }

    var postdata = {
      workflowType: 'compensation',
      reportsTo: {
        priority: prioritymanager,
        levelId: pID
      },
      hr: {
        priority: priorityHr,
        levelId: pIdHR
      },
      specialPerson: {
        priority: prioritySP,
        personId: pIdSP
      },
      specialRole: {
        priority: prioritySR,
        roleId: pIdSR
      }
    }

    console.log("update job workflow", postdata);


    this.workflowService.updateDemographicWorkflow(this.compensationWorkflowId, postdata)
      .subscribe((res: any) => {
        console.log("update compensation workflow", res);
        if (res.status == true)
          this.modalRef.hide();

      },
        (err) => {



        })

  }






  // ================================================== whole component main functions==================================================


  /* Description: before page routing
   author : vipin reddy */

  previousPage() {
    this.router.navigate(['/admin/admin-dashboard/company-settings']);
  }

  /* Description: open modal for all 3 modals
  author : vipin reddy */
  workflowApprovalModal(template: TemplateRef<any>) {
    this.selectedField = 'additionalQuestions';
    this.subHeader = "Who can request the change?";
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg demographic-popup' })
    );
  }

  /* Description: selected tabs for choosing one
  author : vipin reddy */
  selectedTab(activeTab) {
    console.log(activeTab);
    this.selectedField = activeTab;
    if (this.selectedField == "requestChange") {
      this.subHeader = "Who can request the change?";
    } else if (this.selectedField == "approveChange") {
      this.subHeader = "Who can Approve?";
    } else if (this.selectedField == "additionalQuestions") {
      this.subHeader = "What is changed? / Additional Questions";
    } else if (this.selectedField == "summary") {
      this.subHeader = "Summary / Review";
    }
  }


  levelnumberSubmitHr() {
    var postdata = {
      levelIndex: this.hrLevels,
      levelType: 'HR',
      companyId: JSON.parse(localStorage.getItem('companyId'))
    }
    console.log(postdata);

    this.workflowService.LevelCreation(postdata)
      .subscribe((res: any) => {
        console.log("hr levels submit", res);
        if (res.status == true) {
          this.gettingLevelsHr()
          this.gettingLevelsManager()
          this.swalAlertService.SweetAlertWithoutConfirmation("Levels", res.message, 'success')
          // this.hrLevels = ''
        }
      },
        (err) => {
          console.log(err);

        })

  }
  levelnumberSubmitManager() {
    this.isValidManagerlevel = true;
    var postdata = {
      levelIndex: this.mangerLevels,
      levelType: 'Manager',
      companyId: JSON.parse(localStorage.getItem('companyId'))
    }

    console.log(postdata);
    if (this.isValidManagerlevel) {
      this.workflowService.LevelCreation(postdata)
        .subscribe((res: any) => {
          console.log("hr levels submit", res);
          if (res.status == true) {
            // this.mangerLevels = ''
            // this.mangerLevels = 
            this.gettingLevelsHr();
            this.gettingLevelsManager();
            this.swalAlertService.SweetAlertWithoutConfirmation("Levels", res.message, 'success');
          }
        },
          (err) => {
            console.log(err);

          })
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
