import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowApprovalComponent } from './workflow-approval.component';
import { MatSelectModule, MatFormFieldModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { RouterModule, Routes } from '@angular/router';
import { MatNativeDateModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormsModule } from '@angular/forms';



const router: Routes = [
  { path: '', redirectTo: 'workflow', pathMatch: 'full' },
  { path: 'workflow', component: WorkflowApprovalComponent }
]


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    RouterModule.forChild(router),
    MatNativeDateModule,
    MatCheckboxModule,
    DragDropModule

  ],
  declarations: [WorkflowApprovalComponent]
})
export class WorkflowApprovalModule { }
