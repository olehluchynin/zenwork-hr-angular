import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanySettingsComponent } from './company-settings.component';
import { ThemeComponent } from './theme/theme.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from '../../shared-module/shared.module';




const routes: Routes = [
    { path: '', component: CompanySettingsComponent },
    { path: 'company-setup', loadChildren: "./company-setup/company-setup.module#CompanySetupModule" },
    { path: 'theme-management', component: ThemeComponent },
    { path: 'site-access-management', loadChildren: "./site-access/site-access.module#SiteAccessModule" },
    { path: 'workflow-approval', loadChildren: "./workflow-approval/workflow-approval.module#WorkflowApprovalModule" }
];

@NgModule({
    imports: [RouterModule.forChild(routes), MatDatepickerModule, SharedModule],
    exports: [RouterModule],
    declarations:
        [

        ]

})

export class CompanySettingsRouting { }