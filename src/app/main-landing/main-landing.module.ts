import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

/* Components */

import { RequestDemoComponent } from './request-demo/request-demo.component';
import { MainLandingComponent } from './main-landing.component';
import { MainLandingRouting } from './main-landing.routing';








@NgModule({
    declarations: [
        MainLandingComponent,
        RequestDemoComponent
    ],
    providers: [
    ],
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        MainLandingRouting,
        MatSelectModule,
        MatDialogModule
    ]
})
export class MainLandingModule { }