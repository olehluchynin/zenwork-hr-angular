import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLandingComponent } from './main-landing.component';
import { RequestDemoComponent } from './request-demo/request-demo.component';


const routes: Routes = [
// { path: 'home', redirectTo: '', pathMatch: "full" },
    { path: 'main', component: MainLandingComponent },
    { path: 'request-demo', component: RequestDemoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MainLandingRouting { }