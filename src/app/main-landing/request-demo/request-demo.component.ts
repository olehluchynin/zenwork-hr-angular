import { Component, OnInit, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClientDemoRegistrationService } from '../../services/client-demo.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request-demo',
  templateUrl: './request-demo.component.html',
  styleUrls: ['./request-demo.component.css']
})
export class RequestDemoComponent implements OnInit {



  constructor(
    public dialog: MatDialog,
    private clientDemoRegistrationService: ClientDemoRegistrationService,
    private route:Router
  ) { }

  public companySizes = ["1-10","10-30","30-50","50-100","100-150","150-200","200-250","250+"];

  public clientRequestDetails: any = {
    firstName: '',
    lastName: '',
    companyName: '',
    companySize: '',
    email: '',
    phone: ''
  }

  public error: any = {
    firstName: '',
    lastName: '',
    companyName: '',
    companySize: '',
    email: '',
    phone: ''
  }
  registrationSuccess;
  ngOnInit() {
  }

  clientDemoDetails() {
    if (!this.clientRequestDetails.firstName) {
      this.error.firstName = "Enter First Name";
    } else if (!this.clientRequestDetails.lastName) {
      this.error.firstName = "";
      this.error.lastName = "Enter Last Name";
    } else if (!this.clientRequestDetails.companyName) {
      this.error.firstName = "";
      this.error.lastName = "";
      this.error.companyName = "Enter Company Name";
    } else if (!this.clientRequestDetails.companySize) {
      this.error.firstName = "";
      this.error.lastName = "";
      this.error.companyName = "";
      this.error.companySize = "Select Number of Employees";
    } else if (!this.clientRequestDetails.email || !(/^\S+@\S+\.\S+/.test(this.clientRequestDetails.email))) {
      this.error.firstName = "";
      this.error.lastName = "";
      this.error.companyName = "";
      this.error.companySize = "";
      this.error.email = "Enter a Valid email address";
    } else if (!this.clientRequestDetails.phone || !(/[0-9]{10}/.test(this.clientRequestDetails.phone))) {
      this.error.firstName = "";
      this.error.lastName = "";
      this.error.companyName = "";
      this.error.companySize = "";
      this.error.email = "";
      this.error.phone = "Enter valid phone number";
    } else {
      this.error = {
        firstName: '',
        lastName: '',
        companyName: '',
        companySize: '',
        email: '',
        phone: ''
      };
      this.clientDemoRegistrationService.registerClient(this.clientRequestDetails)
        .subscribe(
          (res: Response) => {
            console.log(res);
            if (res.status) {
              console.log("Success");
              this.clientRequestDetails = {
                firstName: '',
                lastName: '',
                companyName: '',
                companySize: '',
                email: '',
                phone: ''
              };
              this.clientDetailsRegistrationModal();
               this.route.navigate(['/home']);

            } else {
              this.error = res;
              this.error = this.error.err;
            }
          },
          (err: Error) => {
            console.log(err);
            this.error = err;
          }
        )
    }
  }

  clientDetailsRegistrationModal() {
    Swal({
      title: 'Registered Successfully',
      text: 'We will get back to you soon',
      type: 'success',
      timer: 2500,
      showConfirmButton: false
    })
  }
  


}
