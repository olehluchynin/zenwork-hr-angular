import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ZenworkersService } from '../services/zenworkers.service';
import { AccessLocalStorageService } from '../services/accessLocalStorage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalAlertService } from '../services/swalAlert.service';
import { MessageService } from '../services/message-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public userData: any;
  public zenworkerDetails: any;
  userProfile: any;

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required)
    })
    // this.getAllRoles()
  }
  constructor(
    private router: Router,
    private zenworkerService: ZenworkersService,
    private accessLocalStorageService: AccessLocalStorageService,
    private swalAlertService: SwalAlertService,
    private messageService: MessageService,
  ) { }

  loginForm: FormGroup;

  dynamicWindowHeight: any;
  /* Getting Dynamic Height */
  getWindowHeight() {
    this.dynamicWindowHeight = (window.innerHeight) + "px";
    return this.dynamicWindowHeight;
  }

  getAllRoles() {

    this.zenworkerService.getAllroles()
      .subscribe(
        (res: any) => {
          console.log(res);
          if (res.status == true) {
            // this.loaderService.loader(false)
            this.userProfile = res.data;
            this.userProfile.forEach(addonsData => {
              addonsData.isChecked = false;
            });
          }
        },
        (err: HttpErrorResponse) => {
          console.log('Err', err);
          // this.loaderService.loader(false);
          // this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        })
  }
  /* Login */

  login() {
    this.zenworkerService.login(this.loginForm.value)
      .subscribe(
        (res: any) => {
          if (res.status) {
            console.log(res)
            this.zenworkerDetails = res.data;
            this.accessLocalStorageService.set('x-access-token', this.zenworkerDetails.token)

            console.log(this.zenworkerDetails.type)
            if (this.zenworkerDetails.type == "employee") {
              console.log("sadasd");
              
              this.accessLocalStorageService.set('type', this.zenworkerDetails.type)
              if ((this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'Manager') || 
              (this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && this.zenworkerDetails.job.Site_AccessRole.name == 'Manager')) {
                this.accessLocalStorageService.set('siteAccessRoleId', this.zenworkerDetails.job.Site_AccessRole);
                this.accessLocalStorageService.set('employeeId', this.zenworkerDetails._id);
                this.accessLocalStorageService.set('companyId', this.zenworkerDetails.companyId._id);
                this.accessLocalStorageService.set('addedBy', this.zenworkerDetails.companyId.userId);
                if(this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                  this.accessLocalStorageService.set('employeeType', this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name)
                  this.accessLocalStorageService.set('roleType', 'customManager');
                }
                if(this.zenworkerDetails.job.Site_AccessRole.roletype == 0){
                   this.accessLocalStorageService.set('employeeType', this.zenworkerDetails.job.Site_AccessRole.name) 
                   this.accessLocalStorageService.set('roleType', 'baseManager');                  
                  }

                this.router.navigate(['/admin/admin-dashboard/employee-management']);
              }
              if ((this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'HR') ||
              (this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && this.zenworkerDetails.job.Site_AccessRole.name == 'HR')) {
                this.accessLocalStorageService.set('siteAccessRoleId', this.zenworkerDetails.job.Site_AccessRole);
                this.accessLocalStorageService.set('employeeId', this.zenworkerDetails._id);
                this.accessLocalStorageService.set('companyId', this.zenworkerDetails.companyId._id);
                this.accessLocalStorageService.set('addedBy', this.zenworkerDetails.companyId.userId);
                if(this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                  this.accessLocalStorageService.set('employeeType', this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name)
                  this.accessLocalStorageService.set('roleType', 'customHR');
                }
                if(this.zenworkerDetails.job.Site_AccessRole.roletype == 0){
                   this.accessLocalStorageService.set('employeeType', this.zenworkerDetails.job.Site_AccessRole.name)
                   this.accessLocalStorageService.set('roleType', 'baseHR'); 
                  }
                this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                console.log("data comes in HR");
              }
              if ((this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'Employee') || 
              (this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && this.zenworkerDetails.job.Site_AccessRole.name == 'Employee')) {
                this.accessLocalStorageService.set('siteAccessRoleId', this.zenworkerDetails.job.Site_AccessRole);
                this.accessLocalStorageService.set('employeeId', this.zenworkerDetails._id);
                this.accessLocalStorageService.set('companyId', this.zenworkerDetails.companyId._id);
                this.accessLocalStorageService.set('addedBy', this.zenworkerDetails.companyId.userId);
                if(this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                  this.accessLocalStorageService.set('employeeType', this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name)
                  this.accessLocalStorageService.set('roleType', 'customEmployee');

                }
                if(this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                  this.accessLocalStorageService.set('employeeType', this.zenworkerDetails.job.Site_AccessRole.name)
                  this.accessLocalStorageService.set('roleType', 'baseEmployee');
                }
                this.router.navigate(['/admin/admin-dashboard/dashboard/employee-view']);
                console.log("data comes in emplpoyee");
              }

            }
            if (this.zenworkerDetails.type === 'company' || this.zenworkerDetails.type === 'reseller') {
              console.log("company");
              this.accessLocalStorageService.set('employeeId', this.zenworkerDetails._id);
              this.accessLocalStorageService.set('type', this.zenworkerDetails.type)
              console.log(this.zenworkerDetails._id);

              this.router.navigate(['/admin/admin-dashboard/company-settings/']);
              this.accessLocalStorageService.set('companyId', this.zenworkerDetails.companyId._id);
              this.accessLocalStorageService.set('addedBy', this.zenworkerDetails.companyId.userId);
              // localStorage.setItem('addedBy', this.zenworkerDetails.companyId.userId)

            }
            // for (var i = 0; i < this.userProfile.length; i++) {
            else if (this.zenworkerDetails.type === 'sr-support' || this.zenworkerDetails.type === 'zenworker') {
              console.log("dsfsfsd");

              this.accessLocalStorageService.set('type', this.zenworkerDetails.type)
              this.accessLocalStorageService.set('zenworkerId', this.zenworkerDetails.zenworkerId);
              this.accessLocalStorageService.set('resellerId', this.zenworkerDetails.zenworkerId.companyId)
              this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
            }
            if (this.zenworkerDetails.type === 'administrator') {
              this.accessLocalStorageService.set('type', this.zenworkerDetails.type)
              this.accessLocalStorageService.set('resellercmpnyID', this.zenworkerDetails.companyId.userId)
              this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.zenworkerDetails.companyId.userId])
              this.messageService.sendMessage('reseller');

            }

            // }

          } else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Login", res.message, 'info');
          }
        },
        (err: HttpErrorResponse) => {
          console.log("Error", err.error.message);
          this.swalAlertService.SweetAlertWithoutConfirmation("Login", err.error.message, 'error');
        }
      )
  }

}