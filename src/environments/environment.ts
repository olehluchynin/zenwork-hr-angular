// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // url: "http://13.233.37.94:3003",
  // url: "http://10.1.3.64:3003",
  url : "http://52.207.167.138:3003",
//  url : "http://localhost:3003",

  // url :"http://192.168.1.202:3003",
  // url: "http://192.168.1.59:3003",
  // url : "http://192.168.1.171:3003",
  // url : "http://10.1.6.203:3003",
  // url : "http://10.1.5/.24:3003",
  version: "/v1.0"

};
