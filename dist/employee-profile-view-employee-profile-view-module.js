(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["employee-profile-view-employee-profile-view-module"],{

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/employee-profile-view.module.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/employee-profile-view.module.ts ***!
  \***********************************************************************************************************/
/*! exports provided: EmployeeProfileViewModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeProfileViewModule", function() { return EmployeeProfileViewModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _employee_profile_view_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-profile-view.routing */ "./src/app/admin-dashboard/employee-management/employee-profile-view/employee-profile-view.routing.ts");
/* harmony import */ var _profile_view_navbar_profile_view_navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile-view-navbar/profile-view-navbar.component */ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.ts");
/* harmony import */ var _job_job_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../job/job.module */ "./src/app/admin-dashboard/employee-management/job/job.module.ts");
/* harmony import */ var _personal_personal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./personal/personal.component */ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.ts");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var EmployeeProfileViewModule = /** @class */ (function () {
    function EmployeeProfileViewModule() {
    }
    EmployeeProfileViewModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _employee_profile_view_routing__WEBPACK_IMPORTED_MODULE_2__["employeeProfileViewRouting"],
                _job_job_module__WEBPACK_IMPORTED_MODULE_4__["JobModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatSelectModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_11__["BsDatepickerModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_10__["MaterialModuleModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_14__["NgxMaskModule"].forRoot({
                    showMaskTyped: true
                })
            ],
            declarations: [_profile_view_navbar_profile_view_navbar_component__WEBPACK_IMPORTED_MODULE_3__["ProfileViewNavbarComponent"], _personal_personal_component__WEBPACK_IMPORTED_MODULE_5__["PersonalComponent"]]
        })
    ], EmployeeProfileViewModule);
    return EmployeeProfileViewModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/employee-profile-view.routing.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/employee-profile-view.routing.ts ***!
  \************************************************************************************************************/
/*! exports provided: employeeProfileViewRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employeeProfileViewRouting", function() { return employeeProfileViewRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profile_view_navbar_profile_view_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile-view-navbar/profile-view-navbar.component */ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.ts");
/* harmony import */ var _personal_personal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./personal/personal.component */ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', redirectTo: "/profile-view", pathMatch: "full" },
    { path: 'profile-view', component: _profile_view_navbar_profile_view_navbar_component__WEBPACK_IMPORTED_MODULE_2__["ProfileViewNavbarComponent"], children: [
            { path: 'personal', component: _personal_personal_component__WEBPACK_IMPORTED_MODULE_3__["PersonalComponent"] },
            { path: 'job', loadChildren: "../job/job.module#JobModule" },
            { path: 'time-schedule', loadChildren: "../time-schedule/time-schedule.module#TimeScheduleModule" },
            { path: 'emergency', loadChildren: "../emergency/emergency.module#EmergencyModule" },
            { path: 'performance', loadChildren: "../performance/performance.module#PerformanceModule" },
            { path: 'compensation', loadChildren: "../compensation/compensation.module#CompensationModule" },
            { path: 'notes', loadChildren: "../notes/notes.module#NotesModule" },
            { path: 'benefits', loadChildren: "../benefits/benefits.module#BenefitsModule" },
            { path: 'training', loadChildren: "../training/training.module#TrainingModule" },
            { path: 'documents', loadChildren: "../documents/documents.module#DocumentsModule" },
            { path: 'assets', loadChildren: "../assets/assets.module#AssetsModule" },
            { path: 'onboardingtab', loadChildren: "../onboardingtab/onboardingtab.module#OnboardingtabModule" },
            { path: 'offboardingtab', loadChildren: "../offboardingtab/offboardingtab.module#OffboardingtabModule" },
            { path: 'custom', loadChildren: "../custom/custom.module#CustomModule" },
            { path: 'audit-trail', loadChildren: "../audit-trail/audit-trail.module#AuditTrailModule" }
        ] },
];
var employeeProfileViewRouting = /** @class */ (function () {
    function employeeProfileViewRouting() {
    }
    employeeProfileViewRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], employeeProfileViewRouting);
    return employeeProfileViewRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.css":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.css ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal { margin: 30px auto 0; float: none;}\n.personal .panel-default>.panel-heading { padding: 0 0 25px; background-color:transparent !important; border: none;}\n.personal .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n.personal .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.personal .panel-title>.small, .personal .panel-title>.small>a, .personal .panel-title>a, .personal .panel-title>small, .personal .panel-title>small>a { text-decoration:none;}\n.personal .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.personal .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.personal .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.personal .panel-group .panel-heading+.panel-collapse>.list-group, .personal .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.personal .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.personal .panel-body { padding: 0 0 5px;}\n.personal .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.personal .panel-default { border-color:transparent;}\n.personal .panel-group .panel+.panel { margin-top: 20px;}\n.personal-cont { display:block; padding: 0;}\n.personal-cont ul { display:block;}\n.personal-cont ul li { margin: 0 15px 25px 0; padding: 0;}\n.personal-cont ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 10px; display: block;}\n.personal-cont ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%;}\n.personal-cont ul li .form-control:focus{ box-shadow: none;}\n.personal-cont ul li .date { height: auto !important; line-height:inherit !important; width:99% !important;}\n.personal-cont ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.personal-cont ul li .date span { border: none !important; padding: 0 !important;}\n.personal-cont ul li .date .form-control { width:80%; height: auto; padding: 12px 0 12px 10px;}\n.personal-cont ul li .date .form-control:focus { box-shadow: none; border: none;}\n.personal-cont ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.personal-cont ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#484747;\n  }\n.personal-cont ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484747;\n  }\n.personal-cont ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.personal-cont ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.personal-cont ul li.veteran { float: none; clear: both;}\n.administration-address { padding: 0 0 20px;}\n.administration-address ul { display: inline-block; width: 100%;}\n.administration-address ul li { padding: 0; margin: 0 20px 20px 0; float: none;}\n.administration-address ul li.details-width{ width: 25%;}\n.administration-address ul li .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none; height: auto; width: 100%; padding: 12px;}\n.administration-address ul li select { background:#fff; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n.administration-address ul li.city { width: 16%;float: left;}\n.administration-address ul li.state { width: 16%; float: left;}\n.administration-address ul li.zip { width: 10%; float: left; padding: 0;}\n.administration-address ul li.united-state { margin: 0; padding: 0; clear: left;}\n.administration-contact { display: block;}\n.administration-contact ul { margin: 0 0 30px;display: inline-block; width: 100%;}\n.administration-contact ul li {margin: 0 20px 15px 0; padding: 0;}\n.administration-contact ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n.administration-contact ul li .phone { display: block; background:#fff; padding: 0 10px; height: 38px; line-height: 38px; clear: left;}\n.administration-contact ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n.administration-contact ul li .phone span .fa { color: #636060;\n    font-size: 23px;\n    line-height: 20px;\n    width: 18px;\n    text-align: center;}\n.administration-contact ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 80%; border: none; background:transparent; box-shadow: none; padding: 0 0 0 10px;}\n.administration-contact ul li .form-control { display: inline-block;vertical-align: middle; width: 40%; border: none; background:#fff; box-shadow: none; \nheight: 40px; line-height: 40px; padding: 0 10px; font-size: 15px;}\n.administration-contact ul li.phone-width {  clear:left;width:20%}\n.administration-contact ul li.email-width { float: none;}\n.social.administration-contact ul li.phone-width {  clear:left;width:25%}\n.personal .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 15px; padding: 0 24px;margin:30px 0 0;background: #008f3d; color:#fff;}\n.personal .btn.cancel { color:#e44a49; background:transparent; padding: 30px 0 0;}\n.add-fields { position: absolute; top:-35px; right:20px;}\n.add-fields a { display: inline-block; vertical-align: middle; cursor: pointer; color:#676767; font-size: 18px; line-height: 18px;}\n.add-fields a small { display: inline-block; padding: 0 15px 0 0;}\n.add-fields a small .fa { color:#008f3d; font-size: 15px; line-height: 15px;}\n.education { display: block; position: relative;}\n.education ul { margin: 0 0 20px;display: inline-block; width: 100%;}\n.education ul li {margin: 0 20px 0 0; padding: 0;}\n.education h5 { color:#444343; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n.education ul li .phone { display: block; background:#fff; padding: 0  0 0 10px; height:43px; line-height:43px; clear: left; margin-top: 30px;}\n.education ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n.education ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 80%; \nborder: none; background:transparent; box-shadow: none; padding: 0 10px; height:auto;}\n.education ul li.phone-width { float: none;}\n.education ul li.phone-width .phone .form-control  { width: 90%; display: inline-block;}\n.education ul li.bachelor { width:40%; float: none; display: inline-block;vertical-align: text-bottom;}\n.education ul li.bachelor .phone .form-control { padding:10px; width:85%;}\n.education ul li.bachelor.business .phone .form-control { width: 100%;}\n.education ul li.bachelor.business { width: 25%;float: none; display: inline-block;}\n.education ul li.bachelor.gpa { width: 15%;float: none; display: inline-block;}\n.education ul li.bachelor.gpa label { display: block; font-weight: normal;color:#444343;}\n.education ul li.bachelor.gpa .form-control { display: inline-block;vertical-align: middle; width: 80%; border: none; background:#fff; \nbox-shadow: none; padding: 0 10px; height: 43px; line-height: 43px;}\n.education ul li .date { height: auto; line-height:inherit; width:100%;}\n.education ul li .date.start { height: auto; line-height:inherit; width:100%;}\n.education ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.education ul li .date span { border: none; padding: 0;}\n.education ul li .date .form-control { width:80%; height: auto; padding: 12px 0 12px 10px;}\n.education ul li .date .form-control:focus { box-shadow: none; border: none;}\n.education ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.language { position: relative;}\n.custom { display: block;}\n.personal-cont ul li.custom-width { float: none;}\n.custom.personal-cont h3{color: #3c3c3c; display: inline-block; font-size: 20px;}\n.custom.personal-cont ul li .date .form-control { width: 80%;}\n.custom.personal-cont ul li .date { width: 90%;}\n/*-- Author:Suresh M, Date:08-04-19  --*/\n/*-- Informaton relative css code  --*/\n.information { display: block;}\n.information .table{ background:#fff;}\n.information .table>tbody>tr>th, .information .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal;\nfont-size:16px;vertical-align: middle; position: relative; background: #edf7ff;}\n.information .table>tbody>tr>td, .information .table>tfoot>tr>td, .information .table>tfoot>tr>th, \n.information .table>thead>tr>td {color: #484747;padding:15px;font-size:15px;}\n.information button.mat-menu-item a { color:#ccc;}\n.information button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.information .table>tbody>tr>th a, .information .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.information .table>tbody>tr>th a .fa, .information .table>thead>tr>th a .fa { color: #6f6d6d;}\n.information button { border: none; outline: none; background:transparent;}\n.information .date { height: auto !important; line-height:inherit !important; width:100%; background: #f8f8f8;}\n.information .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.information .date span { border: none !important; padding: 0 !important;}\n.information .date .form-control { width:70%; height: auto; padding: 10px 0 10px 10px;}\n.information .date .form-control:focus { box-shadow: none; border: none;}\n.information .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.information .form-control.notes{ box-shadow: none; background: #f3f3f3; padding:10px; height: auto; font-size: 15px;}\n.status { width: 100px;}\n/*-- Author:Suresh M, Date:15-04-19  --*/\n/*-- Upload img block  --*/\n.upload { margin: 50px 0 0; text-align: center;}\n.upload img { margin: 0 auto 30px;}\n.upload .form-control { width:90%; height: auto; padding:8px 15px; border:#e0e0e0 1px solid; border-radius:20px; text-align: center; box-shadow: none; margin: 0 auto;\nfont-size: 16px;}\n/*-------------------------------------       ----------------------------------- */\n.zenwork-personal-tab { float: none; margin:0 auto; padding: 0;}\n#fileUpload {\n    height: 0;\n    display: none;\n    width: 0;\n  }\n.import-btn {\n    border: 1px solid #797d7b;\n    background-color: #fff;\n    color: #000;\n    border-radius: 50px;\n    outline: none;\n    box-shadow: none;\n  \n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\ninput{\n        height:34px;\n        border:0;\n        width: 178px;\n        padding: 0px 10px;\n    }\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"personal col-md-11\">\n\n  <div class=\"panel-group\" id=\"accordion3\">\n\n    <form [formGroup]=\"basicDetailsForm\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseOne\">\n              Basic Details\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"personal-cont col-md-9\">\n\n              <ul >\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.First_Name.hide\">\n                  <label>First Name <span *ngIf=\"basicDetailsData.First_Name.required\">*</span></label>\n                  <input type=\"text\" class=\"form-control\" onkeypress=\"return (event.charCode > 64 && \n                  event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)\"\n                    placeholder=\"First Name\" formControlName=\"firstName\"\n                    [required]=\"basicDetailsData.First_Name.required\" maxlength=\"50\" [readonly]=\"personalTabView\">\n                  <!-- <input *ngIf=\"basicDetailsData.First_Name.format == 'number'\" type=\"text\" class=\"form-control\"\n                    placeholder=\"First Name\" formControlName=\"firstName\"\n                    [required]=\"basicDetailsData.First_Name.required\">\n                  <input\n                    *ngIf=\"basicDetailsData.First_Name.format == 'Alpha' || basicDetailsData.First_Name.format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control\" placeholder=\"First_Name\"\n                    formControlName=\"firstName\" [required]=\"basicDetailsData.First_Name.required\">\n                  <input *ngIf=\"basicDetailsData.First_Name.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\"\n                    placeholder=\"First Name\" formControlName=\"firstName\"\n                    [required]=\"basicDetailsData.First_Name.required\"> -->\n\n                  <span *ngIf=\"basicDetailsForm.get('firstName').invalid && basicDetailsForm.get('firstName').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must enter a first name</span>\n                </li>\n\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Middle_Name.hide\">\n                  <label>Middle Name <span *ngIf=\"basicDetailsData.Middle_Name.required\">*</span></label>\n                  <input type=\"text\" class=\"form-control\" onkeypress=\"return (event.charCode > 64 && \n                  event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)\"\n                    placeholder=\"Middle Name\" formControlName=\"middleName\"\n                    [required]=\"basicDetailsData.Middle_Name.required\" maxlength=\"50\"  [readonly]=\"personalTabView\">\n                  <!-- <input\n                    *ngIf=\"basicDetailsData.Middle_Name.format == 'Alpha' || basicDetailsData.Middle_Name.format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control\" placeholder=\"Middle Name\"\n                    formControlName=\"middleName\" [required]=\"basicDetailsData.Middle_Name.required\">\n                  <input *ngIf=\"basicDetailsData.Middle_Name.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\"\n                    placeholder=\"Middle Name\" formControlName=\"middleName\"\n                    [required]=\"basicDetailsData.Middle_Name.required\"> -->\n\n                  <span *ngIf=\"basicDetailsForm.get('middleName').invalid && basicDetailsForm.get('middleName').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must enter a middle name</span>\n                </li>\n\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Last_Name.hide\">\n                  <label>Last Name <span *ngIf=\"basicDetailsData.Last_Name.required\">*</span></label>\n                  <input  type=\"text\" onkeypress=\"return (event.charCode > 64 && \n                  event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)\" class=\"form-control\"\n                    placeholder=\"Last Name\" formControlName=\"lastName\" [required]=\"basicDetailsData.Last_Name.required\" maxlength=\"50\"  [readonly]=\"personalTabView\">\n                  <!-- <input\n                    *ngIf=\"basicDetailsData.Last_Name.format == 'Alpha' || basicDetailsData.Last_Name.format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control\" placeholder=\"Last Name\"\n                    formControlName=\"lastName\" [required]=\"basicDetailsData.Last_Name.required\">\n                  <input *ngIf=\"basicDetailsData.Last_Name.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\"\n                    placeholder=\"Last Name\" formControlName=\"lastName\" [required]=\"basicDetailsData.Last_Name.required\"> -->\n\n                  <span *ngIf=\"basicDetailsForm.get('lastName').invalid && basicDetailsForm.get('lastName').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must enter a last name</span>\n                </li>\n        \n\n              </ul>\n              <div class=\"clearfix\"></div>\n\n\n              <ul>\n                  <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Preferred_Name.hide\">\n                      <label>Prefered Name <span *ngIf=\"basicDetailsData.Preferred_Name.required\">*</span></label>\n                      <input type=\"text\" onkeypress=\"return (event.charCode > 64 && \n                      event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)\" class=\"form-control\"\n                        placeholder=\"Preferred Name\" formControlName=\"preferredName\"\n                        [required]=\"basicDetailsData.Preferred_Name.required\" maxlength=\"50\" [readonly]=\"personalTabView\">\n                      <!-- <input\n                        *ngIf=\"basicDetailsData.Preferred_Name.format == 'Alpha' || basicDetailsData.Preferred_Name.format == 'text'\"\n                        (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control\" placeholder=\"Preferred Name\"\n                        formControlName=\"preferredName\" [required]=\"basicDetailsData.Preferred_Name.required\">\n                      <input *ngIf=\"basicDetailsData.Preferred_Name.format == 'AlphaNumeric'\" type=\"text\"\n                        class=\"form-control\" placeholder=\"Preferred Name\" formControlName=\"preferredName\"\n                        [required]=\"basicDetailsData.Preferred_Name.required\"> -->\n    \n                      <span\n                        *ngIf=\"basicDetailsForm.get('preferredName').invalid && basicDetailsForm.get('preferredName').touched\"\n                        style=\"color:#e44a49; padding-top: 5px;\">You must enter a prefered name</span>\n                    </li>\n\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Salutation.hide\">\n                  <label>Salutation <span *ngIf=\"basicDetailsData.Salutation.required\">*</span></label>\n                  <mat-select class=\"form-control\" formControlName=\"salutation\"\n                    [required]=\"basicDetailsData.Salutation.required\" (ngModelChange)=\"selectSalutation($event)\"  [disabled]=\"personalTabView\">\n                    <mat-option value=\"\">Select</mat-option>\n                    <mat-option *ngFor=\"let salutationName of allSalutation\" [value]=\"salutationName.name\" >{{salutationName.name}}</mat-option>\n                   \n                  </mat-select>\n                \n                  <span *ngIf=\"basicDetailsForm.get('salutation').invalid && basicDetailsForm.get('salutation').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select a salutation</span>\n                </li>\n        \n\n              </ul>\n              <div class=\"clearfix\"></div>\n\n              <ul>\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Date_of_Birth.hide\">\n                  <label>Date of Birth <span *ngIf=\"basicDetailsData.Date_of_Birth.required\">*</span></label>\n\n                  <div class=\"date\">\n                    <input matInput [matDatepicker]=\"picker\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                      formControlName=\"dob\" [required]=\"basicDetailsData.Date_of_Birth.required\" [max]=\"dobDate\" [disabled]=\"personalTabView\">\n                    <mat-datepicker #picker></mat-datepicker>\n                    <button mat-raised-button (click)=\"picker.open()\">\n                      <span>\n                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                      </span>\n                    </button>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('dob').invalid && basicDetailsForm.get('dob').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select date of birth</span>\n                </li>\n\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Gender.hide\">\n                  <label>Gender <span *ngIf=\"basicDetailsData.Gender.required\">*</span></label>\n                  <mat-select class=\"form-control\" formControlName=\"gender\"\n                    [required]=\"basicDetailsData.Gender.required\" [disabled]=\"personalTabView\">\n                    <mat-option value=\"\">Select</mat-option>\n                    <mat-option *ngIf=\"(salutationName == 'Mr.') || (salutationName == 'Dr.') || (salutationName == 'Prof.') || (!salutationName) \" value=\"male\">Male</mat-option>\n                    <mat-option *ngIf=\"(salutationName == 'Miss') || (salutationName == 'Mrs.') || (salutationName == 'Dr.') || (salutationName == 'Prof.') || (salutationName == 'Ms.') || (!salutationName)\" value=\"female\">Female</mat-option>\n                  </mat-select>\n                  <span *ngIf=\"basicDetailsForm.get('gender').invalid && basicDetailsForm.get('gender').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select a gender</span>\n                </li>\n\n              </ul>\n              <div class=\"clearfix\"></div>\n\n\n              <ul>\n\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Maritial_Status.hide\">\n                  <label>Marital Status <span *ngIf=\"basicDetailsData.Maritial_Status.required\">*</span></label>\n                  <mat-select class=\"form-control\" formControlName=\"maritalStatus\"\n                    [required]=\"basicDetailsData.Maritial_Status.required\" placeholder=\"Marital status\" [disabled]=\"personalTabView\">\n                    <mat-option value=\"\">Select</mat-option>\n                    <mat-option *ngFor=\"let maritalstatus of allMaritalStatus\" [value]=\"maritalstatus.name\">{{maritalstatus.name}}</mat-option>\n                   \n                  </mat-select>\n                  <span\n                    *ngIf=\"basicDetailsForm.get('maritalStatus').invalid && basicDetailsForm.get('maritalStatus').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select a marital status</span>\n                </li>\n\n                <li class=\"col-md-3 right-space\" *ngIf=\"!basicDetailsData.Maritial_Status_Effective_Date.hide\">\n                  <label>Marital Status Effective Date <span\n                      *ngIf=\"basicDetailsData.Maritial_Status_Effective_Date.required\">*</span></label>\n\n                  <div class=\"date\">\n                    <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                      formControlName=\"effectiveDate\"\n                      [required]=\"basicDetailsData.Maritial_Status_Effective_Date.required\" [disabled]=\"personalTabView\">\n                    <mat-datepicker #picker1></mat-datepicker>\n                    <button mat-raised-button (click)=\"picker1.open()\">\n                      <span>\n                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                      </span>\n                    </button>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                  <span\n                    *ngIf=\"basicDetailsForm.get('effectiveDate').invalid && basicDetailsForm.get('effectiveDate').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select marital status effective date</span>\n                </li>\n\n\n                <li class=\"col-md-3 veteran\" *ngIf=\"!basicDetailsData.Veteran_Status.hide\">\n                  <label>Veteran Status <span *ngIf=\"basicDetailsData.Veteran_Status.required\">*</span></label>\n                  <mat-select class=\"form-control\" formControlName=\"veteranStatus\"\n                    [required]=\"basicDetailsData.Veteran_Status.required\" placeholder=\"Veteran status\" [disabled]=\"personalTabView\">\n                    <mat-option value=\"\">Select</mat-option>\n                    <mat-option *ngFor=\"let veteranstatus of allVeteranStatus\" [value]=\"veteranstatus.name\">{{veteranstatus.name}}</mat-option>\n                    \n                  </mat-select>\n                  <span\n                    *ngIf=\"basicDetailsForm.get('veteranStatus').invalid && basicDetailsForm.get('veteranStatus').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You select a veteran status</span>\n                </li>\n\n                <li class=\"col-md-3 veteran\" *ngIf=\"!basicDetailsData.SSN.hide\">\n                  <label>SSN <span *ngIf=\"basicDetailsData.SSN.required\">*</span></label>\n                  <input  class=\"form-control\" mask=\"000-00-0000\" [dropSpecialCharacters]='false'\n                    placeholder=\"SSN\" formControlName=\"ssn\" [required]=\"basicDetailsData.SSN.required\" [readonly]=\"personalTabView\">\n                    \n                  <!-- <input\n                    *ngIf=\"basicDetailsData.SSN.format == 'Alpha' || basicDetailsData.Preferred_Name.format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control\" placeholder=\"SSN\"\n                    formControlName=\"ssn\" [required]=\"basicDetailsData.SSN.required\">\n                  <input *ngIf=\"basicDetailsData.SSN.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\"\n                    placeholder=\"SSN\" formControlName=\"ssn\" [required]=\"basicDetailsData.SSN.required\"> -->\n\n                  <span *ngIf=\"basicDetailsForm.get('ssn').invalid && basicDetailsForm.get('ssn').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must enter a SSN</span>\n                </li>\n\n<!-- \n                <li>\n\n                  <input type=\"text\" (blur)=\"showMask = true\" (click)=\"showMask = !showMask\" name=\"value\" (ngModelChange)=\"value = $event\" [ngModel]=\"value | mask:showMask\">\n                  <p>real value: {{ value }}</p>\n                </li> -->\n\n\n              </ul>\n              <div class=\"clearfix\"></div>\n\n\n              <ul>\n\n                <li class=\"col-md-3\" *ngIf=\"!basicDetailsData.Race.hide\">\n                  <label>Race <span *ngIf=\"basicDetailsData.Race.required\">*</span></label>\n                  <mat-select class=\"form-control\" formControlName=\"race\" [required]=\"basicDetailsData.Race.required\" [disabled]=\"personalTabView\">\n                      <mat-option value=\"\">Select</mat-option>\n                    <mat-option *ngFor=\"let race of allRace\" [value]=\"race.name\">{{race.name}}</mat-option>\n                 \n                  </mat-select>\n                  <span *ngIf=\"basicDetailsForm.get('race').invalid && basicDetailsForm.get('race').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select a race</span>\n                </li>\n\n                <li class=\"col-md-5\" *ngIf=\"!basicDetailsData.Ethnicity.hide\">\n                  <label>Ethnicity <span *ngIf=\"basicDetailsData.Ethnicity.required\">*</span></label>\n                  <mat-select class=\"form-control\" formControlName=\"ethnicity\"\n                    [required]=\"basicDetailsData.Ethnicity.required\" [disabled]=\"personalTabView\">\n                    <mat-option value=\"\">Select</mat-option>\n                    <mat-option *ngFor=\"let ethnicity of allEthnicity\" [value]=\"ethnicity.name\">{{ethnicity.name}}</mat-option>\n                   \n                  </mat-select>\n                  <span *ngIf=\"basicDetailsForm.get('ethnicity').invalid && basicDetailsForm.get('ethnicity').touched\"\n                    style=\"color:#e44a49; padding-top: 5px;\">You must select a ethnicity</span>\n                </li>\n\n              </ul>\n              <div class=\"clearfix\"></div>\n\n\n            </div>\n            <!-- Author:Suresh M, Date:15-04-19  -->\n            <!-- Upload img block -->\n            <div class=\"upload col-md-3\">\n              <img *ngIf=\"!profilePhoto\" src=\"../../../../../assets/images/directory_tabs/Personal/emp.png\" width=\"60\" height=\"73\"\n                alt=\"img\">\n                <img *ngIf=\"profilePhoto\" src=\"{{profilePhoto}}\"  width=\"80\" height=\"80\"\n                alt=\"img\" style=\"border-radius: 50%;\">\n                <br>\n\n              <input type=\"file\" id=\"fileUpload\" accept=\".png, .jpg, .jpeg\" (change)=\"fileChangeEvent($event)\">\n              <label for=\"fileUpload\">\n                <a mat-raised-button class=\"import-btn\">Upload Employee Photo</a>\n\n              </label>\n\n            </div>\n\n            <div class=\"clearfix\"></div>\n\n\n          </div>\n        </div>\n\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseTwo\">\n              Address\n            </a>\n          </h4>\n\n        </div>\n\n        <div id=\"collapseTwo\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"administration-address\">\n\n              <ul>\n                <li class=\"col-md-4\">\n                  <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" maxlength=\"60\" formControlName=\"street1\" [readonly]=\"personalTabView\">\n                  <span *ngIf=\"basicDetailsForm.get('street1').invalid && basicDetailsForm.get('street1').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a street</span>\n                </li>\n                <li class=\"col-md-4\">\n                  <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" maxlength=\"60\" formControlName=\"street2\" [readonly]=\"personalTabView\">\n                </li>\n              </ul>\n\n              <ul>\n                <li class=\"state\">\n                  <input type=\"text\" placeholder=\"City\" class=\"form-control\" maxlength=\"24\" formControlName=\"city\"\n                  onkeypress=\"return (event.charCode > 64 &&  event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)\" [readonly]=\"personalTabView\">\n                  <span *ngIf=\"basicDetailsForm.get('city').invalid && basicDetailsForm.get('city').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a city</span>\n                </li>\n                <li class=\"city\">\n                  <mat-select class=\"form-control\" placeholder=\"State\" formControlName=\"state\" [disabled]=\"personalTabView\">\n                    <mat-option *ngFor=\"let state of allState\" [value]=\"state.name\">{{state.name}}</mat-option>\n                    \n                  </mat-select>\n                  <span *ngIf=\"basicDetailsForm.get('state').invalid && basicDetailsForm.get('state').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must select a state</span>\n                </li>\n                <li class=\"zip\">\n                  <input placeholder=\"ZIP\" class=\"form-control\" minlength=\"5\" maxlength=\"5\" formControlName=\"zipcode\"\n                  (keypress)=\"keyPress($event)\" [readonly]=\"personalTabView\">\n                  <span *ngIf=\"basicDetailsForm.get('zipcode').invalid && basicDetailsForm.get('zipcode').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a zipcode</span>\n                </li>\n\n                <li class=\"col-md-3 united-state\">\n                  <mat-select class=\"form-control\" placeholder=\"Country\" formControlName=\"country\" [disabled]=\"personalTabView\">\n                    <mat-option *ngFor=\"let country of allCountry\" [value]=\"country.name\">{{country.name}}</mat-option>\n                  </mat-select>\n                  <span *ngIf=\"basicDetailsForm.get('country').invalid && basicDetailsForm.get('country').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must select a country</span>\n                </li>\n              </ul>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseThree\">\n              Contact\n            </a>\n          </h4>\n        </div>\n\n        <div id=\"collapseThree\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"administration-contact\">\n\n              <ul>\n                <li>\n                  <h4>Phone</h4>\n                </li>\n                <li class=\"col-md-3\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input  class=\"form-control\" placeholder=\"Work Phone\" formControlName=\"workPhone\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10 [readonly]=\"personalTabView\">\n \n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('workPhone').invalid && basicDetailsForm.get('workPhone').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must eneter a work phone</span>\n                </li>\n                <li class=\"col-md-2\">\n\n                  <input class=\"form-control\" placeholder=\"Extn\" formControlName=\"extention\" (keypress)=\"keyPress($event)\" minlength=4\n                  maxlength=4 [readonly]=\"personalTabView\">\n                  <span *ngIf=\"basicDetailsForm.get('extention').invalid && basicDetailsForm.get('extention').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a extention</span>\n                </li>\n                <li class=\"col-md-3 phone-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                    </span>\n                    <input  class=\"form-control\" placeholder=\"Mobile Phone\" formControlName=\"mobilePhone\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10 [readonly]=\"personalTabView\">\n \n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('mobilePhone').invalid && basicDetailsForm.get('mobilePhone').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a mobile phone</span>\n                </li>\n                <li class=\"col-md-3 phone-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                    </span>\n                    <input class=\"form-control\" placeholder=\"Home Phone\" formControlName=\"homePhone\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10 [readonly]=\"personalTabView\">\n                   \n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('homePhone').invalid && basicDetailsForm.get('homePhone').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a home phone</span>\n                </li>\n              </ul>\n\n              <ul>\n\n                <li>\n                  <h4>Email</h4>\n                </li>\n\n                <li class=\"col-md-3 email-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" formControlName=\"workMail\"\n                    pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\" [readonly]=\"personalTabView\">\n                  </div>\n                  \n                  <span *ngIf=\"basicDetailsForm.get('workMail').invalid && basicDetailsForm.get('workMail').touched\"\n                  style=\"color:#e44a49; padding: 5px 0 0;\">Enter a valid email</span>\n                </li>\n\n                <li class=\"col-md-3 email-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                    </span>\n                    <input type=\"email\" class=\"form-control\" placeholder=\"Home Email\" formControlName=\"personalMail\"\n                    pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\" [readonly]=\"personalTabView\">\n                  </div>\n                  \n                  <span *ngIf=\"basicDetailsForm.get('personalMail').invalid && basicDetailsForm.get('personalMail').touched\"\n                  style=\"color:#e44a49; padding: 5px 0 0;\">Enter a valid email</span>\n                </li>\n              </ul>\n\n            </div>\n\n          </div>\n        </div>\n\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseFour\">\n              Social links\n            </a>\n          </h4>\n        </div>\n\n        <div id=\"collapseFour\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"administration-contact social\">\n\n              <ul>\n\n                <li class=\"col-md-4 phone-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../../assets/images/directory_tabs/Personal/layer1_1_.png\" width=\"19\"\n                        height=\"19\" alt=\"img\">\n                    </span>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"LinkedIn\" formControlName=\"linkedin\" [readonly]=\"personalTabView\">\n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('linkedin').invalid && basicDetailsForm.get('linkedin').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a linkedin link</span>\n                </li>\n\n                <li class=\"col-md-4 phone-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../../assets/images/directory_tabs/Personal/Path 39.png\" width=\"19\" height=\"19\"\n                        alt=\"img\">\n                    </span>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"Twitter Username\"\n                      formControlName=\"twitter\" [readonly]=\"personalTabView\">\n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('twitter').invalid && basicDetailsForm.get('twitter').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a twitter link</span>\n                </li>\n                <li class=\"col-md-4 phone-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../../assets/images/directory_tabs/Personal/layer-1.png\" width=\"19\" height=\"19\"\n                        alt=\"img\">\n                    </span>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"Facebook\" formControlName=\"facebook\" [readonly]=\"personalTabView\">\n                  </div>\n                  <span *ngIf=\"basicDetailsForm.get('facebook').invalid && basicDetailsForm.get('facebook').touched\"\n                  style=\"color:#e44a49; padding-top: 5px;\">You must enter a facebook link</span>\n                </li>\n              </ul>\n\n            </div>\n\n          </div>\n        </div>\n\n      </div>\n    \n    </form>\n    \n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\" style=\"margin-top:15px;\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseFive\">\n              Education\n            </a>\n          </h4>\n\n        </div>\n\n        <div id=\"collapseFive\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"education\">\n\n              <h5>No Education Info has been added.</h5>\n\n              <div *ngIf = \"!personalTabView\" class=\"add-fields\">\n                  <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\" >more_vert</mat-icon>\n\n                  <mat-menu #menu1=\"matMenu\">\n                    <button *ngIf=\"!checkEduIds.length >=1\" mat-menu-item (click)=\"addEducation()\">Add</button>\n                    <button *ngIf=\"(!checkEduIds.length <=0) && (checkEduIds.length ==1)\" mat-menu-item (click)=\"editEducation()\">Edit</button>\n                    <button *ngIf=\"(!checkEduIds.length <=0) && (checkEduIds.length >=1)\" mat-menu-item (click)=\"deleteEducation()\">Delete</button>\n                  </mat-menu>\n                <!-- <a (click)=\"addEducation()\"><small><i aria-hidden=\"true\" class=\"fa fa-plus\"></i></small>Add Education</a> -->\n              </div>\n              <ul class=\"row\" *ngFor=\"let education of educationList\">\n               \n\n                <div class=\"col-md-1\" style=\"margin-top:30px;\">\n                    <li>\n                        <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"education.isChecked\" (ngModelChange)=\"educationIds($event, education._id)\"></mat-checkbox>\n    \n                    </li>\n                </div>\n                <div class=\"col-md-8\">\n                <li class=\"col-md-6 phone-width\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../../assets/images/directory_tabs/Personal/inst.png\" width=\"19\" height=\"19\"\n                        alt=\"img\">\n                    </span>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"University\" [(ngModel)]=\"education.university\" [readOnly]=\"education.edit\"\n                    onkeypress=\"return (event.charCode > 64 &&  event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode ==32)\" [readonly]=\"personalTabView\">\n                  </div>\n                </li>\n\n                <li class=\"col-md-8 bachelor\">\n                  <div class=\"phone\">\n                    <span class=\"pull-left\">\n                      <img src=\"../../../../../assets/images/directory_tabs/Personal/cap.png\" width=\"19\" height=\"19\"\n                        alt=\"img\">\n                    </span>\n                    <mat-select class=\"form-control pull-left\" placeholder=\"Bachelor's\" [(ngModel)]=\"education.degree\" [disabled]=\"education.edit\" [disabled]=\"personalTabView\">\n                      <mat-option value='Bachelors'>Bachelors</mat-option>\n                      <mat-option value='Masters'>Masters</mat-option>\n                    </mat-select>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                </li>\n\n                <li class=\"col-md-6 bachelor business\">\n                  <div class=\"phone\">\n                    <mat-select class=\"form-control pull-left\" placeholder=\"Business\" [(ngModel)]=\"education.specialization\" [disabled]=\"education.edit\">\n                      <mat-option value='IT'>IT</mat-option>\n                      <mat-option value='Electronics'>Electronics</mat-option>\n                    </mat-select>\n                  </div>\n                </li>\n\n                <li class=\"col-md-4 bachelor gpa\">\n                  <label>GPA </label>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"GPA\" [(ngModel)]=\"education.gpa\" [readOnly]=\"education.edit\"\n                  (keypress)=\"omit_special_char($event)\" [readonly]=\"personalTabView\">\n                </li>\n                <ul style=\"margin-top:20px;\">\n                    <li class=\"col-md-4\">\n                      <div class=\"date start\">\n                        <input matInput [matDatepicker]=\"picker2\" placeholder=\"Start Date\" class=\"form-control\"\n                        [(ngModel)]=\"education.start_date\" [max]=\"education.end_date\" [disabled]=\"personalTabView\">\n                        <mat-datepicker #picker2></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker2.open()\" [disabled]=\"education.edit\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n    \n                    </li>\n    \n    \n                    <li class=\"col-md-4\">\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker3\" placeholder=\"End Date\" class=\"form-control\"\n                        [(ngModel)]=\"education.end_date\" [min]=\"education.start_date\" [disabled]=\"personalTabView\">\n                        <mat-datepicker #picker3></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker3.open()\" [disabled]=\"education.edit\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n    \n                    </li>\n                 \n    \n                  </ul>\n                </div>\n                \n              </ul>\n\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseSix\">\n              Language\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseSix\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"personal-cont language\">\n              <div *ngIf = \"!personalTabView\" class=\"add-fields\">\n                  <mat-icon class=\"cursor\"  [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n\n                  <mat-menu #menu2=\"matMenu\">\n                    <button *ngIf=\"!checkLangIds.length >=1\" mat-menu-item (click)=\"addLanguages()\">Add</button>\n                    <button *ngIf=\"(!checkLangIds.length <=0) && (checkLangIds.length ==1)\" mat-menu-item (click)=\"editLanguage()\">Edit</button>\n                    <button *ngIf=\"(!checkLangIds.length <=0) && (checkLangIds.length >=1)\" mat-menu-item (click)=\"deleteLanguage()\">Delete</button>\n                  </mat-menu>\n                <!-- <a (click)=\"addLanguages()\"><small><i aria-hidden=\"true\" class=\"fa fa-plus\"></i></small>\n                  Add Language</a> -->\n                </div>\n\n              <ul class=\"row\" style=\"margin:0px;\" *ngFor=\"let language of languageList\">\n                <li class=\"col-sm-1\">\n                    <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"language.isChecked\" (ngModelChange)=\"languagesIds($event,language._id)\"></mat-checkbox>\n                </li>\n                <li class=\"col-md-3\">\n                  <label>Select Language *</label>\n                  <mat-select class=\"form-control\" [(ngModel)]=\"language.language\" [disabled]=\"language.edit\" [disabled]=\"personalTabView\">\n                    <mat-option *ngFor=\"let language of allLanguages\" [value]=\"language.name\">{{language.name}}</mat-option>\n                  \n                  </mat-select>\n                </li>\n\n                <li class=\"col-md-3\">\n                  <label>Level of Fluency</label>\n                  <mat-select class=\"form-control\" [(ngModel)]=\"language.level_of_fluency\" [disabled]=\"language.edit\" [disabled]=\"personalTabView\">\n                    <mat-option *ngFor=\"let fluency of allLanguageFluency\" [value]=\"fluency.name\">{{fluency.name}}</mat-option>\n                  </mat-select>\n                </li>\n\n              </ul><br>\n              <div class=\"clearfix\"></div>\n\n            </div>\n            <div class=\"clearfix\"></div>\n\n          </div>\n        </div>\n\n      </div>\n\n      <!-- Author:Suresh M, Date:08-04-19  -->\n      <!-- Visa Informaton Accordion -->\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapse-visa\">\n              Visa Information\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapse-visa\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"personal-cont information\">\n\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>\n                      \n                    </th>\n                    <th>Visa Type</th>\n                    <th>Issuing Country</th>\n                    <th>Issue Date</th>\n                    <th>Expiration Date</th>\n                    <th>Status</th>\n                    <th>Notes</th>\n                    <th *ngIf = \"!personalTabView\">\n                    <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\" >more_vert</mat-icon>\n\n                      <mat-menu #menu=\"matMenu\">\n                        <button *ngIf=\"!checkVisaIds.length >=1\" mat-menu-item (click)=\"addVisaInformation()\">Add</button>\n                        <button *ngIf=\"(!checkVisaIds.length <=0) && (checkVisaIds.length ==1)\" mat-menu-item (click)=\"editVisa()\">Edit</button>\n                        <button *ngIf=\"(!checkVisaIds.length <=0) && (checkVisaIds.length >=1)\" mat-menu-item (click)=\"deleteVisa()\">Delete</button>\n                      </mat-menu>\n                    </th>\n                  </tr>\n                </thead>\n                <tbody>\n\n                  <tr  *ngFor=\"let visa of visaList\">\n\n                    <td [ngClass]=\"{'zenwork-checked-border': visa.isChecked}\">\n                      <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"visa.isChecked\" (ngModelChange)=\"visaIds($event, visa._id)\"></mat-checkbox>\n                    </td>\n                    <td>\n                      <div class=\"status\">\n                        <mat-select class=\"form-control\" placeholder=\"Type\" [(ngModel)]=\"visa.visa_type\" [disabled]=\"visa.edit\">\n                          <mat-option *ngFor=\"let type of allVisaType\" [value]=\"type.name\">{{type.name}}</mat-option>\n                        </mat-select>\n                      </div>\n                    </td>\n                    <td>\n                      <mat-select class=\"form-control\" placeholder=\"Country\" [(ngModel)]=\"visa.issuing_country\" [disabled]=\"visa.edit\" [disabled]=\"personalTabView\">\n\n                        <mat-option value=\"USA\">USA</mat-option>\n                        <mat-option value=\"India\">India</mat-option>\n                      </mat-select>\n                    </td>\n                    <td>\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker10\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)]=\"visa.issued_date\"\n                        [max]=\"visa.expiration_date\" [disabled]=\"personalTabView\">\n                        <mat-datepicker #picker10></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker10.open()\" [disabled]=\"visa.edit\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </td>\n                    <td>\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker11\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)]=\"visa.expiration_date\"\n                        [min]=\"visa.issued_date\" [disabled]=\"personalTabView\">\n                        <mat-datepicker #picker11></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker11.open()\" [disabled]=\"visa.edit\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </td>\n                    <td>\n                      <div class=\"status\">\n                        <mat-select class=\"form-control\" placeholder=\"Status\" [(ngModel)]=\"visa.status\" [disabled]=\"visa.edit\" [disabled]=\"personalTabView\">\n                          <mat-option *ngFor=\"let status of allVisaStatus\" [value]=\"status.name\">{{status.name}}</mat-option>\n                          \n                        </mat-select>\n                      </div>\n                    </td>\n                    <td>\n                      <input type=\"text\" placeholder=\"notes\" class=\"form-control notes\" [(ngModel)]=\"visa.notes\" [readOnly]=\"visa.edit\" [readonly]=\"personalTabView\">\n                    </td>\n                  </tr>\n\n                </tbody>\n              </table>\n\n            </div>\n\n\n          </div>\n        </div>\n\n      </div>\n\n      <button *ngIf=\"!personalTabView\" class=\"btn pull-left cancel\" (click)=\"cancelPersonalDetails()\">Cancel</button>\n      <button *ngIf=\"!personalTabView\" class=\"btn pull-right save\" type=\"submit\" (click)=\"personalSubmit()\">Save Changes</button>\n\n      <div class=\"clearfix\"></div>\n \n\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: PersonalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersonalComponent", function() { return PersonalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PersonalComponent = /** @class */ (function () {
    // transform(value: string, showMask :boolean): string {
    //   if (!showMask || value.length < 10) {
    //     return value;
    //   }
    //   return 'XXX-XX-' + value.substr(0, value.length - 6);
    // }
    function PersonalComponent(accessLocalStorageService, employeeService, swalAlertService, personalService, router) {
        this.accessLocalStorageService = accessLocalStorageService;
        this.employeeService = employeeService;
        this.swalAlertService = swalAlertService;
        this.personalService = personalService;
        this.router = router;
        this.personal = 'Personal';
        this.customMaritalStatus = [];
        this.defaultMaritalStatus = [];
        this.allMaritalStatus = [];
        this.customVeteranStatus = [];
        this.defaultVeteranStatus = [];
        this.allVeteranStatus = [];
        this.defaultRace = [];
        this.customRace = [];
        this.allRace = [];
        this.defaultEthnicity = [];
        this.customEthnicity = [];
        this.allEthnicity = [];
        this.defaultState = [];
        this.customState = [];
        this.allState = [];
        this.defaultCountry = [];
        this.customCountry = [];
        this.allCountry = [];
        this.defaultLanguages = [];
        this.customLanguages = [];
        this.allLanguages = [];
        this.customLanguageFluency = [];
        this.defaultLanguageFluency = [];
        this.allLanguageFluency = [];
        this.customVisaStatus = [];
        this.defaultVisaStatus = [];
        this.allVisaStatus = [];
        this.customVisaType = [];
        this.defaultVisaType = [];
        this.allVisaType = [];
        this.customSalutation = [];
        this.defaultSalutaion = [];
        this.allSalutation = [];
        this.educationList = [
            {
                university: '',
                degree: '',
                specialization: '',
                gpa: '',
                start_date: '',
                end_date: '',
                isChecked: false,
                edit: true
            }
        ];
        this.languageList = [
            {
                language: '',
                level_of_fluency: '',
                isChecked: false,
                edit: true
            }
        ];
        this.visaList = [
            {
                visa_type: '',
                issuing_country: '',
                issued_date: '',
                expiration_date: '',
                status: '',
                notes: '',
                isChecked: false,
                edit: true
            }
        ];
        this.educationAdd = [];
        this.languagesAdd = [];
        this.visaAdd = [];
        this.selectedVisaList = [];
        this.selectedEducationList = [];
        this.selectedLanguage = [];
        this.editField = true;
        this.checkEduIds = [];
        this.checkLangIds = [];
        this.checkVisaIds = [];
        this.ssnMask = false;
        this.basicDetailsData = {
            First_Name: {
                hide: '',
                required: false,
                format: ''
            },
            Middle_Name: {
                hide: '',
                required: false,
                format: ''
            },
            Last_Name: {
                hide: '',
                required: '',
                format: ''
            },
            Preferred_Name: {
                hide: '',
                required: '',
                format: ''
            },
            Salutation: {
                hide: '',
                required: '',
                format: ''
            },
            Date_of_Birth: {
                hide: '',
                required: '',
                format: ''
            },
            Gender: {
                hide: '',
                required: '',
                format: ''
            },
            Maritial_Status: {
                hide: '',
                required: '',
                format: ''
            },
            Maritial_Status_Effective_Date: {
                hide: '',
                required: '',
                format: ''
            },
            Veteran_Status: {
                hide: '',
                required: '',
                format: ''
            },
            SSN: {
                hide: '',
                required: '',
                format: ''
            },
            Race: {
                hide: '',
                required: '',
                format: ''
            },
            Ethnicity: {
                hide: '',
                required: '',
                format: ''
            }
        };
        this.personalTabView = false;
    }
    PersonalComponent.prototype.ngOnInit = function () {
        this.type = JSON.parse(localStorage.getItem('type'));
        if (this.type != 'company') {
            this.pageAccesslevels();
        }
        this.dobDate = new Date();
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.personalId = JSON.parse(localStorage.getItem('employee'));
        if (this.personalId) {
            this.id = this.personalId._id;
        }
        this.roleType = this.accessLocalStorageService.get('type');
        if (this.roleType != 'company') {
            this.id = JSON.parse(localStorage.getItem('employeeId'));
        }
        console.log(this.id);
        this.getUserPersonal();
        // this.educationList = this.personalDetails.personal.education;
        // this.languageList = this.personalDetails.personal.languages;
        // this.visaList = this.personalDetails.personal.visa_information;
        // console.log(this.personalDetails.personal);
        this.getAllBasicDetails();
        this.getStandardAndCustomStructureFields();
        this.basicDetailsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            middleName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            preferredName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            salutation: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            dob: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            maritalStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            effectiveDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            veteranStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            ssn: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            race: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            ethnicity: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            street1: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            street2: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            state: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            extention: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            mobilePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            homePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            workMail: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            workPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            personalMail: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            linkedin: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            twitter: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
            facebook: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](""),
        });
        this.getSsnFields();
        // this.basicDetailsForm.get('degree').patchValue(this.personalDetails.personal.education[0].degree);
        // this.basicDetailsForm.get('end_date').patchValue(this.personalDetails.personal.education[0].end_date);
        // this.basicDetailsForm.get('gpa').patchValue(this.personalDetails.personal.education[0].gpa);
        // this.basicDetailsForm.get('specialization').patchValue(this.personalDetails.personal.education[0].specialization);
        // this.basicDetailsForm.get('start_date').patchValue(this.personalDetails.personal.education[0].start_date);
        // this.basicDetailsForm.get('university').patchValue(this.personalDetails.personal.education[0].university);
        // this.basicDetailsForm.get('language').patchValue(this.personalDetails.personal.languages[0].language);
        // this.basicDetailsForm.get('level_of_fluency').patchValue(this.personalDetails.personal.languages[0].level_of_fluency);
        // this.basicDetailsForm.get('facebook').patchValue(this.personalDetails.personal.social_links.facebook);
        // this.basicDetailsForm.get('linkedin').patchValue(this.personalDetails.personal.social_links.linkedin);
        // this.basicDetailsForm.get('twitter').patchValue(this.personalDetails.personal.social_links.twitter);
        // this.basicDetailsForm.get('expiration_date').patchValue(this.personalDetails.personal.visa_information[0].expiration_date);
        // this.basicDetailsForm.get('issued_date').patchValue(this.personalDetails.personal.visa_information[0].issued_date);
        // this.basicDetailsForm.get('issuing_country').patchValue(this.personalDetails.personal.visa_information[0].issuing_country);
        // this.basicDetailsForm.get('notes').patchValue(this.personalDetails.personal.visa_information[0].notes);
        // this.basicDetailsForm.get('status').patchValue(this.personalDetails.personal.visa_information[0].status);
        // this.basicDetailsForm.get('visa_type').patchValue(this.personalDetails.personal.visa_information[0].visa_type);
        //  console.log(' this.basicDetailsForm.get(firstName)', this.basicDetailsForm.get('firstName').value)
    };
    PersonalComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.personalService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Personal" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.personalTabView = true;
                    console.log('loggss', _this.personalTabView);
                }
            });
        }, function (err) {
        });
    };
    // Author:Saiprakash G, Date:18-06-19
    // Upload employee photo
    PersonalComponent.prototype.fileChangeEvent = function (e) {
        var _this = this;
        console.log(e);
        var files = e.target.files[0];
        console.log(files.name);
        var data = {
            companyId: this.companyId,
            userId: this.id,
        };
        var formData = new FormData();
        formData.append('file', files);
        formData.append('data', JSON.stringify(data));
        this.personalService.uploadPhoto(formData).subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.profilePhoto = res.data.url;
            console.log(_this.profilePhoto);
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    // Author:Suresh M, Date:10-04-19
    // Basic Details get the Api
    PersonalComponent.prototype.getAllBasicDetails = function () {
        var _this = this;
        this.personalService.basicDetailsGetAll(this.personal)
            .subscribe(function (res) {
            console.log("Personalssss", res);
            _this.basicDetailsData = res.data.fields;
        });
    };
    // Author:Saiprakash, Date:15/05/2019
    // Get Standard And Custom Structure Fields
    PersonalComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.personalService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customMaritalStatus = _this.getAllStructureFields['Marital Status'].custom;
            _this.defaultMaritalStatus = _this.getAllStructureFields['Marital Status'].default;
            _this.allMaritalStatus = _this.customMaritalStatus.concat(_this.defaultMaritalStatus);
            console.log(_this.allMaritalStatus);
            _this.customVeteranStatus = _this.getAllStructureFields['Veteran Status'].custom;
            _this.defaultVeteranStatus = _this.getAllStructureFields['Veteran Status'].default;
            _this.allVeteranStatus = _this.customVeteranStatus.concat(_this.defaultVeteranStatus);
            _this.defaultRace = _this.getAllStructureFields['Race'].default;
            _this.customRace = _this.getAllStructureFields['Race'].custom;
            _this.allRace = _this.customRace.concat(_this.defaultRace);
            _this.defaultEthnicity = _this.getAllStructureFields['Ethnicity'].default;
            _this.customEthnicity = _this.getAllStructureFields['Ethnicity'].custom;
            _this.allEthnicity = _this.customEthnicity.concat(_this.defaultEthnicity);
            _this.customCountry = _this.getAllStructureFields['Country'].custom;
            _this.defaultCountry = _this.getAllStructureFields['Country'].default;
            _this.allCountry = _this.customCountry.concat(_this.defaultCountry);
            _this.customState = _this.getAllStructureFields['State'].custom;
            _this.defaultState = _this.getAllStructureFields['State'].default;
            _this.allState = _this.customState.concat(_this.defaultState);
            _this.customLanguages = _this.getAllStructureFields['Language'].custom;
            _this.defaultLanguages = _this.getAllStructureFields['Language'].default;
            _this.allLanguages = _this.customLanguages.concat(_this.defaultLanguages);
            _this.customLanguageFluency = _this.getAllStructureFields['Language Fluency'].custom;
            _this.defaultLanguageFluency = _this.getAllStructureFields['Language Fluency'].default;
            _this.allLanguageFluency = _this.customLanguageFluency.concat(_this.defaultLanguageFluency);
            _this.customVisaType = _this.getAllStructureFields['Visa Type'].custom;
            _this.defaultVisaType = _this.getAllStructureFields['Visa Type'].default;
            _this.allVisaType = _this.customVisaType.concat(_this.defaultVisaType);
            _this.customVisaStatus = _this.getAllStructureFields['Visa Status'].custom;
            _this.defaultVisaStatus = _this.getAllStructureFields['Visa Status'].default;
            _this.allVisaStatus = _this.customVisaStatus.concat(_this.defaultVisaStatus);
            _this.customSalutation = _this.getAllStructureFields['Salutation'].custom;
            _this.defaultSalutaion = _this.getAllStructureFields['Salutation'].default;
            _this.allSalutation = _this.customSalutation.concat(_this.defaultSalutaion);
        });
    };
    // Author:Saiprakash G, Date:22-05-19
    // education, languages and visa information IDs
    PersonalComponent.prototype.educationIds = function (event, id) {
        console.log(event, id);
        this.eduId = id;
        if (event == true) {
            this.checkEduIds.push(id);
        }
        console.log(this.checkEduIds);
        if (event == false) {
            var index = this.checkEduIds.indexOf(id);
            if (index > -1) {
                this.checkEduIds.splice(index, 1);
            }
            console.log(this.checkEduIds);
        }
    };
    PersonalComponent.prototype.languagesIds = function (event, id) {
        this.languageId = id;
        if (event == true) {
            this.checkLangIds.push(id);
        }
        console.log(this.checkLangIds);
        if (event == false) {
            var index = this.checkLangIds.indexOf(id);
            if (index > -1) {
                this.checkLangIds.splice(index, 1);
            }
            console.log(this.checkLangIds);
        }
    };
    PersonalComponent.prototype.visaIds = function (event, id) {
        this.visaId = id;
        if (event == true) {
            this.checkVisaIds.push(id);
        }
        console.log(this.checkVisaIds);
        if (event == false) {
            var index = this.checkVisaIds.indexOf(id);
            if (index > -1) {
                this.checkVisaIds.splice(index, 1);
            }
            console.log(this.checkVisaIds);
        }
    };
    PersonalComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    // Author:Saiprakash G, Date:22-05-19
    // Add education
    PersonalComponent.prototype.addEducation = function () {
        this.employeeEducation = {
            university: '',
            degree: '',
            specialization: '',
            gpa: '',
            start_date: '',
            end_date: '',
            eduId: this.eduId
        };
        this.educationList.push(this.employeeEducation);
        this.editField = false;
    };
    // Author:Saiprakash G, Date:22-05-19
    // Add languages
    PersonalComponent.prototype.addLanguages = function () {
        this.employeeLanguages = {
            language: '',
            level_of_fluency: '',
            languageId: this.languageId
        };
        this.languageList.push(this.employeeLanguages);
        this.editField = false;
    };
    // Author:Saiprakash G, Date:22-05-19
    // Add visa information
    PersonalComponent.prototype.addVisaInformation = function () {
        this.employeeVisaInformation = {
            visa_type: '',
            issuing_country: '',
            issued_date: '',
            expiration_date: '',
            status: '',
            notes: '',
            visaId: this.visaId
        };
        console.log(this.employeeVisaInformation);
        this.visaList.push(this.employeeVisaInformation);
        this.editField = false;
        console.log(this.visaList);
    };
    // Author:Saiprakash, Date:22/05/2019
    // Get user personal
    PersonalComponent.prototype.getUserPersonal = function () {
        var _this = this;
        this.personalService.getOtherUser(this.companyId, this.id).subscribe(function (res) {
            console.log(res);
            _this.personalDetails = res.data;
            if (_this.personalDetails.personal.profile_pic_details) {
                _this.profilePhoto = _this.personalDetails.personal.profile_pic_details.url;
                _this.originalFileName = _this.personalDetails.personal.profile_pic_details.originalFilename;
                _this.s3Path = _this.personalDetails.personal.profile_pic_details.s3Path;
            }
            _this.basicDetailsForm.patchValue(_this.personalDetails.personal);
            _this.basicDetailsForm.get('firstName').patchValue(_this.personalDetails.personal.name.firstName);
            _this.basicDetailsForm.get('middleName').patchValue(_this.personalDetails.personal.name.middleName);
            _this.basicDetailsForm.get('lastName').patchValue(_this.personalDetails.personal.name.lastName);
            _this.basicDetailsForm.get('preferredName').patchValue(_this.personalDetails.personal.name.preferredName);
            _this.basicDetailsForm.get('salutation').patchValue(_this.personalDetails.personal.name.salutation);
            _this.basicDetailsForm.get('street1').patchValue(_this.personalDetails.personal.address.street1);
            _this.basicDetailsForm.get('street2').patchValue(_this.personalDetails.personal.address.street2);
            _this.basicDetailsForm.get('state').patchValue(_this.personalDetails.personal.address.state);
            _this.basicDetailsForm.get('city').patchValue(_this.personalDetails.personal.address.city);
            _this.basicDetailsForm.get('zipcode').patchValue(_this.personalDetails.personal.address.zipcode);
            _this.basicDetailsForm.get('country').patchValue(_this.personalDetails.personal.address.country);
            _this.basicDetailsForm.get('extention').patchValue(_this.personalDetails.personal.contact.extention);
            _this.basicDetailsForm.get('homePhone').patchValue(_this.personalDetails.personal.contact.homePhone);
            _this.basicDetailsForm.get('mobilePhone').patchValue(_this.personalDetails.personal.contact.mobilePhone);
            _this.basicDetailsForm.get('personalMail').patchValue(_this.personalDetails.personal.contact.personalMail);
            _this.basicDetailsForm.get('workMail').patchValue(_this.personalDetails.personal.contact.workMail);
            _this.basicDetailsForm.get('workPhone').patchValue(_this.personalDetails.personal.contact.workPhone);
            if (_this.personalDetails.personal.social_links) {
                _this.basicDetailsForm.get('facebook').patchValue(_this.personalDetails.personal.social_links.facebook);
                _this.basicDetailsForm.get('linkedin').patchValue(_this.personalDetails.personal.social_links.linkedin);
                _this.basicDetailsForm.get('twitter').patchValue(_this.personalDetails.personal.social_links.twitter);
            }
            _this.educationList = res.data.personal.education;
            _this.languageList = res.data.personal.languages;
            _this.visaList = res.data.personal.visa_information;
            console.log(_this.visaList);
            _this.visaList.filter(function (item) {
                item.edit = true;
            });
            _this.educationList.filter(function (item) {
                item.edit = true;
            });
            _this.languageList.filter(function (item) {
                item.edit = true;
            });
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash G, Date:09-05-19
    // Save and update employee personal details
    PersonalComponent.prototype.personalSubmit = function () {
        var _this = this;
        var x = this.basicDetailsForm.get('ssn').value;
        x.length;
        console.log("XXX Length", x);
        for (var i = 0; i < 6; i++) {
            console.log((i));
            var str = '';
            str.replace['i'] = 'x';
            // x.replace[i] = 'x';
        }
        console.log("11111111111111", x);
        var tempVL = [];
        this.visaList.forEach(function (element) {
            if (element && element.visa_type && element.issuing_country && element.issued_date && element.expiration_date && element.status && element.notes && element.visa_type != "" && element.issuing_country != "" && element.issued_date != "" && element.expiration_date != "" && element.status != "" && element.notes != "") {
                tempVL.push(element);
            }
        });
        this.visaList = tempVL;
        var tempEL = [];
        this.educationList.forEach(function (element) {
            if (element && element.degree && element.specialization && element.university && element.start_date && element.end_date && element.gpa && element.degree != "" && element.specialization != "" && element.university != "" && element.start_date != "" && element.end_date != "" && element.gpa != "") {
                tempEL.push(element);
            }
        });
        this.educationList = tempEL;
        var tempLl = [];
        this.languageList.forEach(function (element) {
            if (element && element.language && element.level_of_fluency && element.language != "" && element.level_of_fluency != "") {
                tempLl.push(element);
            }
        });
        this.languageList = tempLl;
        var data = {
            _id: this.id,
            personal: {
                name: {
                    firstName: this.basicDetailsForm.get('firstName').value,
                    lastName: this.basicDetailsForm.get('lastName').value,
                    middleName: this.basicDetailsForm.get('middleName').value,
                    preferredName: this.basicDetailsForm.get('preferredName').value,
                    salutation: this.basicDetailsForm.get('salutation').value
                },
                dob: this.basicDetailsForm.get('dob').value,
                gender: this.basicDetailsForm.get('gender').value,
                maritalStatus: this.basicDetailsForm.get('maritalStatus').value,
                effectiveDate: this.basicDetailsForm.get('effectiveDate').value,
                veteranStatus: this.basicDetailsForm.get('veteranStatus').value,
                race: this.basicDetailsForm.get('race').value,
                ssn: this.basicDetailsForm.get('ssn').value,
                ethnicity: this.basicDetailsForm.get('ethnicity').value,
                profile_pic_details: {
                    originalFilename: this.originalFileName,
                    s3Path: this.s3Path,
                    url: this.profilePhoto
                },
                address: {
                    street1: this.basicDetailsForm.get('street1').value,
                    street2: this.basicDetailsForm.get('street2').value,
                    city: this.basicDetailsForm.get('city').value,
                    state: this.basicDetailsForm.get('state').value,
                    zipcode: this.basicDetailsForm.get('zipcode').value,
                    country: this.basicDetailsForm.get('country').value
                },
                contact: {
                    workPhone: this.basicDetailsForm.get('workPhone').value,
                    extention: this.basicDetailsForm.get('extention').value,
                    mobilePhone: this.basicDetailsForm.get('mobilePhone').value,
                    homePhone: this.basicDetailsForm.get('homePhone').value,
                    workMail: this.basicDetailsForm.get('workMail').value,
                    personalMail: this.basicDetailsForm.get('personalMail').value
                },
                social_links: {
                    linkedin: this.basicDetailsForm.get('linkedin').value,
                    twitter: this.basicDetailsForm.get('twitter').value,
                    facebook: this.basicDetailsForm.get('facebook').value
                },
                education: this.educationList,
                languages: this.languageList,
                visa_information: this.visaList
            }
        };
        if (this.basicDetailsForm.valid) {
            this.personalService.updateUSer(data).subscribe(function (res) {
                console.log(res);
                _this.getUserPersonal();
                _this.checkEduIds = [];
                _this.checkLangIds = [];
                _this.checkVisaIds = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation("Successfully updated user", "", "success");
                if (res.status == true) {
                    _this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/job"]);
                }
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.basicDetailsForm.get('firstName').markAsTouched();
            this.basicDetailsForm.get('middleName').markAsTouched();
            this.basicDetailsForm.get('lastName').markAsTouched();
            this.basicDetailsForm.get('preferredName').markAsTouched();
            this.basicDetailsForm.get('salutation').markAsTouched();
            this.basicDetailsForm.get('dob').markAsTouched();
            this.basicDetailsForm.get('gender').markAsTouched();
            this.basicDetailsForm.get('maritalStatus').markAsTouched();
            this.basicDetailsForm.get('effectiveDate').markAsTouched();
            this.basicDetailsForm.get('veteranStatus').markAsTouched();
            this.basicDetailsForm.get('race').markAsTouched();
            this.basicDetailsForm.get('ssn').markAsTouched();
            this.basicDetailsForm.get('ethnicity').markAsTouched();
            this.basicDetailsForm.get('street1').markAsTouched();
            this.basicDetailsForm.get('city').markAsTouched();
            this.basicDetailsForm.get('state').markAsTouched();
            this.basicDetailsForm.get('zipcode').markAsTouched();
            this.basicDetailsForm.get('country').markAsTouched();
            this.basicDetailsForm.get('workPhone').markAsTouched();
            this.basicDetailsForm.get('extention').markAsTouched();
            this.basicDetailsForm.get('mobilePhone').markAsTouched();
            this.basicDetailsForm.get('workMail').markAsTouched();
            this.basicDetailsForm.get('personalMail').markAsTouched();
            this.basicDetailsForm.get('linkedin').markAsTouched();
            this.basicDetailsForm.get('twitter').markAsTouched();
            this.basicDetailsForm.get('facebook').markAsTouched();
        }
    };
    // Author:Saiprakash G, Date:22-05-19
    // Edit visa information
    PersonalComponent.prototype.editVisa = function () {
        this.selectedVisaList = this.visaList.filter(function (checkVisa) {
            return checkVisa.isChecked;
        });
        if (this.selectedVisaList.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one visa information to proceed", "", 'error');
        }
        else if (this.selectedVisaList.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one visa information to proceed", "", 'error');
        }
        else {
            if (this.selectedVisaList[0]._id) {
                this.selectedVisaList[0].edit = false;
            }
        }
    };
    // Author:Saiprakash G, Date:22-05-19
    // Edit education
    PersonalComponent.prototype.editEducation = function () {
        this.selectedEducationList = this.educationList.filter(function (checkEducation) {
            return checkEducation.isChecked;
        });
        if (this.selectedEducationList.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one education to proceed", "", 'error');
        }
        else if (this.selectedEducationList.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one education to proceed", "", 'error');
        }
        else {
            if (this.selectedEducationList[0]._id) {
                this.selectedEducationList[0].edit = false;
            }
        }
    };
    // Author:Saiprakash G, Date:22-05-19
    // Edit language
    PersonalComponent.prototype.editLanguage = function () {
        this.selectedLanguage = this.languageList.filter(function (checkLanguage) {
            return checkLanguage.isChecked;
        });
        if (this.selectedLanguage.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one language to proceed", "", 'error');
        }
        else if (this.selectedLanguage.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one language to proceed", "", 'error');
        }
        else {
            if (this.selectedLanguage[0]._id) {
                this.selectedLanguage[0].edit = false;
            }
        }
    };
    /* Description:accept only alphabets
     author : vipin reddy */
    PersonalComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 &&
            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123);
    };
    PersonalComponent.prototype.saveCompanyContactInfo = function () {
    };
    // Author:Saiprakash G, Date:22-05-19
    // Delete education
    PersonalComponent.prototype.deleteEducation = function () {
        var _this = this;
        this.selectedEducationList = this.educationList.filter(function (checkEducation) {
            return checkEducation.isChecked;
        });
        if (this.selectedEducationList.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one education to proceed", "", 'error');
        }
        else {
            var data = {
                companyId: this.companyId,
                userId: this.id,
                _ids: this.selectedEducationList
            };
            this.personalService.deleteEducation(data).subscribe(function (res) {
                console.log(res);
                _this.checkEduIds = [];
                _this.getUserPersonal();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author:Saiprakash G, Date:22-05-19
    // Delete language
    PersonalComponent.prototype.deleteLanguage = function () {
        var _this = this;
        this.selectedLanguage = this.languageList.filter(function (checkLanguage) {
            return checkLanguage.isChecked;
        });
        if (this.selectedLanguage.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one language to proceed", "", 'error');
        }
        else {
            var data = {
                companyId: this.companyId,
                userId: this.id,
                _ids: this.selectedLanguage
            };
            this.personalService.deleteLanguages(data).subscribe(function (res) {
                console.log(res);
                _this.checkLangIds = [];
                _this.getUserPersonal();
                _this.swalAlertService.SweetAlertWithoutConfirmation("Deleted language successfully", "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author:Saiprakash G, Date:22-05-19
    // Delete visa information
    PersonalComponent.prototype.deleteVisa = function () {
        var _this = this;
        this.selectedVisaList = this.visaList.filter(function (checkVisa) {
            return checkVisa.isChecked;
        });
        if (this.selectedVisaList.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one visa information to proceed", "", 'error');
        }
        else {
            var data = {
                companyId: this.companyId,
                userId: this.id,
                _ids: this.selectedVisaList
            };
            this.personalService.deleteVisaInformation(data).subscribe(function (res) {
                console.log(res);
                _this.checkVisaIds = [];
                _this.getUserPersonal();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    PersonalComponent.prototype.getSsnFields = function () {
        var _this = this;
        var ID = JSON.parse(localStorage.getItem('companyId'));
        console.log("SSN IDDDDDDDd", ID);
        this.personalService.getPersonalSSNData(ID)
            .subscribe(function (res) {
            console.log("SSN Responsee", res);
            _this.ssnMask = res.data;
        });
    };
    // Author:Saiprakash G, Date:07-08-19
    // Salutation validation
    PersonalComponent.prototype.selectSalutation = function (event) {
        console.log(event);
        if (event) {
            this.salutationName = event;
        }
    };
    PersonalComponent.prototype.omit_special_char = function (event) {
        var pattern = /^[0-9#$%^&*()@!.-]*$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    // Author:Saiprakash G, Date:08/08/19
    // Cancel personal details it's redirect to directory page
    PersonalComponent.prototype.cancelPersonalDetails = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/directory/directory"]);
    };
    PersonalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-personal',
            template: __webpack_require__(/*! ./personal.component.html */ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.html"),
            styles: [__webpack_require__(/*! ./personal.component.css */ "./src/app/admin-dashboard/employee-management/employee-profile-view/personal/personal.component.css")]
        }),
        __metadata("design:paramtypes", [_services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__["AccessLocalStorageService"],
            _services_employee_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__["MyInfoService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], PersonalComponent);
    return PersonalComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.css":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.css ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.nav-tabs > li {\n    padding: 1px 8px;\n}\n.main-navbar{\n    margin: 20px auto; float: none; padding: 0;\n}\n.onboarding-tabs li a{\n    padding: 0px 10px 10px;font-size: 18px;\n}\n.nav-tabs>li>a{\n    border: none;\n}\n.nav>li>a:hover{\n    background: transparent;\n}\n.onboarding-tabs li a:active,.onboarding-tabs li a:visited,.onboarding-tabs li a:focus{\n    /* border: 1px solid transparent!important; */\n    /* border-bottom: 4px solid #439348!important; */\n    border-radius: 0px!important;\n    color: #353131!important;\n    /* font-size: 16px; */\n}\n.onboarding-tabs>li.active>a, .onboarding-tabs>li.active>a:focus, .onboarding-tabs>li.active>a:hover{\n    /* border: 1px solid transparent!important; */\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    background: #f8f8f8;\n    \n}\n.settings-dropdown-list > li >a:hover{\n    background-color: #008f3d !important;\n    color: #fff !important;\n    border-bottom: none;\n}\n.settings-dropdown-list > li >a{\n    font-size: 14px;\n    padding:6px 10px; cursor: pointer;\n}\n.settings-dropdown,.settings-dropdown:hover,.settings-dropdown:active{\n    border: none;\n    background: #f8f8f8 !important;\n    padding: 0px;\n}\n.open>.dropdown-menu{\n    left: -66px !important;\n    min-width: 145px;\n    top: 24px;\n    border-radius: 5px;\n    border: none;\n    box-shadow: 0 0 10px #ccc;\n}\n.more{\n    font-size: 18px !important;\n    color:#008f3d !important;\n}\n.btn-group.open .dropdown-toggle {\n    box-shadow: none;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.html ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-navbar col-md-11\">\n  <ul class=\"nav nav-tabs onboarding-tabs\">\n\n      <li *ngIf=\"showTabs.Personal.hide\" routerLink=\"./personal\" routerLinkActive=\"active\"><a routerLink=\"./personal\"\n          routerLinkActive=\"active\">Personal</a></li>\n        \n      <li *ngIf=\"showTabs.Job.hide && jobTab\" routerLink=\"./job\"\n        routerLinkActive=\"active\"><a routerLink=\"./job\">Job</a></li>\n      <li *ngIf=\"showTabs.Time_Off.hide && timeSchedule\" routerLink=\"./time-schedule\" routerLinkActive=\"active\"><a\n          routerLink=\"./time-schedule\">Time Schedule</a></li>\n      <li *ngIf=\"showTabs.Emergency_Contact.hide && emergencyContact\" routerLink=\"./emergency\" routerLinkActive=\"active\"><a\n          routerLink=\"./emergency\">Emergency Contact</a></li>\n      <li *ngIf=\"showTabs.Performance.hide\" routerLink=\"./performance\" routerLinkActive=\"active\"><a\n          routerLink=\"./performance\">Performance</a></li>\n      <li *ngIf=\"showTabs.Compensation.hide && compensation\" routerLink=\"./compensation\" routerLinkActive=\"active\"><a\n          routerLink=\"./compensation\">Compensation</a></li>\n      <li *ngIf=\"showTabs.Notes.hide && notes\" routerLink=\"./notes\" routerLinkActive=\"active\"><a routerLink=\"./notes\">Notes</a>\n      </li>\n      <li *ngIf=\"showTabs.Benefits.hide && benifits\" routerLink=\"./benefits\" routerLinkActive=\"active\"><a\n          routerLink=\"./benefits\">Benefits</a></li>\n      <li *ngIf=\"showTabs.Training.hide && training\" routerLink=\"./training\" routerLinkActive=\"active\"><a\n          routerLink=\"./training\">Training</a></li>\n      <li>\n        <div class=\"btn-group\">\n          <button type=\"button\" class=\"btn btn-default dropdown-toggle settings-dropdown\" data-toggle=\"dropdown\"\n            aria-haspopup=\"true\" aria-expanded=\"false\">\n            <a class=\"more\"> More</a>\n            <span class=\"caret\"></span>\n          </button>\n          <ul class=\"dropdown-menu settings-dropdown-list\">\n            <li><a *ngIf=\"showTabs.Documents.hide && documents\" routerLink=\"./documents\">Documents</a></li>\n            <li><a *ngIf=\"showTabs.Assets.hide && assets\" routerLink=\"./assets\">Assets</a></li>\n            <li><a *ngIf=\"showTabs.Onboarding.hide && onBoarding\" routerLink=\"./onboardingtab\">Onboarding</a></li>\n            <li><a *ngIf=\"showTabs.Offboarding.hide && offBoarding\" routerLink=\"./offboardingtab\">Offboarding</a></li>\n            <li><a *ngIf=\"showTabs.Personal.hide && custom\" routerLink=\"./custom\">Custom</a></li>\n            <li><a *ngIf=\"showTabs.Personal.hide && auditTrial\" routerLink=\"./audit-trail\">Audit Trail</a></li>\n            <!-- <li><a *ngIf = \"showTabs.Personal.hide\" >Rearrange Fields</a></li> -->\n          </ul>\n        </div>\n      </li>\n\n  </ul>\n</div>\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.ts":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.ts ***!
  \********************************************************************************************************************************/
/*! exports provided: ProfileViewNavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileViewNavbarComponent", function() { return ProfileViewNavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileViewNavbarComponent = /** @class */ (function () {
    function ProfileViewNavbarComponent(companySettingsService, myInfoService) {
        this.companySettingsService = companySettingsService;
        this.myInfoService = myInfoService;
        this.showTabs = {
            Assets: { hide: true },
            Benefits: { hide: true },
            Compensation: { hide: true },
            Documents: { hide: true },
            Emergency_Contact: { hide: true },
            Job: { hide: true },
            Notes: { hide: true },
            Offboarding: { hide: true },
            Onboarding: { hide: true },
            Performance: { hide: true },
            Personal: { hide: true },
            Time_Off: { hide: true },
            Training: { hide: true }
        };
        this.notes = false;
        this.benifits = false;
        this.training = false;
        this.documents = false;
        this.assets = false;
        this.onBoarding = false;
        this.offBoarding = false;
        this.custom = false;
        this.auditTrial = false;
    }
    ProfileViewNavbarComponent.prototype.ngOnInit = function () {
        this.controlMyinfoSections();
        this.type = JSON.parse(localStorage.getItem('type'));
        if (this.type != 'company') {
            this.pageAccesslevels();
        }
        if (this.type == 'company') {
            this.allShowPages();
        }
    };
    ProfileViewNavbarComponent.prototype.allShowPages = function () {
        this.jobTab = true;
        this.emergencyContact = true;
        this.timeSchedule = true;
        this.compensation = true;
        this.notes = true;
        this.benifits = true;
        this.training = true;
        this.documents = true;
        this.assets = true;
        this.onBoarding = true;
        this.offBoarding = true;
        this.custom = true;
        this.auditTrial = true;
    };
    ProfileViewNavbarComponent.prototype.controlMyinfoSections = function () {
        var _this = this;
        this.companySettingsService.controllSections()
            .subscribe(function (res) {
            console.log(res);
            _this.showTabs = res.data.myInfo_sections;
        }, function (err) {
        });
    };
    ProfileViewNavbarComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                if (element.page == "My Info (EE's) - Job" && element.access != 'no') {
                    _this.jobTab = true;
                }
                if (element.page == "My Info (EE's) - Emergency Contact" && element.access != 'no') {
                    _this.emergencyContact = true;
                }
                if (element.page == "My Info (EE's) - Time Schedule" && element.access != 'no') {
                    _this.timeSchedule = true;
                }
                if (element.page == "My Info (EE's) - Compensation" && element.access != 'no') {
                    _this.compensation = true;
                }
                if (element.page == "My Info (EE's) - Notes" && element.access != 'no') {
                    _this.notes = true;
                }
                if (element.page == "My Info (EE's) - Benifits" && element.access != 'no') {
                    _this.benifits = true;
                }
                if (element.page == "My Info (EE's) - Training" && element.access != 'no') {
                    _this.training = true;
                }
                if (element.page == "My Info (EE's) - Documents" && element.access != 'no') {
                    _this.documents = true;
                }
                if (element.page == "My Info (EE's) - Assets" && element.access != 'no') {
                    _this.assets = true;
                }
                if (element.page == "My Info (EE's) - On-Boarding" && element.access != 'no') {
                    _this.onBoarding = true;
                }
                if (element.page == "My Info (EE's) - Off-Boarding" && element.access != 'no') {
                    _this.offBoarding = true;
                }
                if (element.page == "My Info (EE's) - Custom" && element.access != 'no') {
                    _this.custom = true;
                }
                if (element.page == "My Info (EE's) - Audit Trial" && element.access != 'no') {
                    _this.auditTrial = true;
                }
            });
        }, function (err) {
        });
    };
    ProfileViewNavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile-view-navbar',
            template: __webpack_require__(/*! ./profile-view-navbar.component.html */ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.html"),
            styles: [__webpack_require__(/*! ./profile-view-navbar.component.css */ "./src/app/admin-dashboard/employee-management/employee-profile-view/profile-view-navbar/profile-view-navbar.component.css")]
        }),
        __metadata("design:paramtypes", [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__["CompanySettingsService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"]])
    ], ProfileViewNavbarComponent);
    return ProfileViewNavbarComponent;
}());



/***/ })

}]);
//# sourceMappingURL=employee-profile-view-employee-profile-view-module.js.map