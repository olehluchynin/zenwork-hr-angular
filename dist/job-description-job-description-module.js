(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["job-description-job-description-module"],{

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.css":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.css ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".job-application { display: block;}\n.job-application h2 { margin: 0; font-size: 20px; border-bottom:#ccc 1px solid; padding: 0 0 20px;}\n.job-app-in { margin: 30px auto 0; float: none; padding: 0;}\n.app-details {border-bottom:#ccc 1px solid; padding: 0 0 20px; margin: 0 0 30px;}\n.app-details h3 { margin: 0; font-size:18px;padding: 0 0 20px;}\n.app-details ul { display: block; padding: 0;}\n.app-details ul li {margin: 0 0 25px;}\n.app-details ul li label{color: #484747;font-size: 17px; padding: 0 0 10px;}\n.app-details ul li .form-control{ border: none; box-shadow: none; padding: 13px 12px; height: auto;\nbackground:#f8f8f8;}\n.app-details ul li .form-control:focus{ box-shadow: none;}\n.date { height: auto !important; line-height:inherit !important; width:100%;background:#f8f8f8;}\n.edit-work .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none;\nwidth: 40px;height: 30px; min-width: auto; border-left: #ccc 1px solid; border-radius: 0;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:65%; height: auto; padding: 13px 0 13px 10px; border: none; box-shadow: none; display: inline-block;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.application-fields {border-bottom:#ccc 1px solid; padding: 0 0 20px; margin: 0 0 30px;}\n.application-fields h3 { margin: 0; font-size:18px;padding: 0 0 20px;}\n.application-questions {padding: 0 0 20px;}\n.application-questions h3 { margin: 0; font-size:18px;padding: 0 0 20px;}\n.application-questions .table{ background:#fff;}\n.application-questions .table>tbody>tr>th, .application-questions .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.application-questions .table>tbody>tr>td, .application-questions .table>tfoot>tr>td, .application-questions .table>tfoot>tr>th, \n.application-questions .table>thead>tr>td {color: #484747;padding:15px;font-size:15px; vertical-align: middle; background:#f8f8f8;}\n.application-questions .table>tbody>tr>th a, .application-questions .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.application-questions .table>tbody>tr>th a .fa, .application-questions .table>thead>tr>th a .fa { color: #6f6d6d;}\n.job-app-in .btn.save {border-radius: 20px;font-size: 16px; padding:8px 30px;\n    background: #008f3d; color:#fff;}\n.job-app-in .btn.cancel {color:#e5423d; background:transparent;font-size: 16px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.html":
/*!********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"job-application\">\n\n  <h2>Add / Edit - Job Application</h2>\n\n   <div class=\"job-app-in col-md-10\">\n       \n    <div class=\"app-details\">\n\n    <h3>Application Details</h3>\n    <ul>\n      <li class=\"col-md-3\">\n        <label>Application Name</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"Application name\">\n      </li>\n  \n      <li class=\"col-md-3\">\n        <label>Effective Date</label>\n        <div class=\"date\">\n          <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n          <mat-datepicker #picker1></mat-datepicker>\n          <button mat-raised-button (click)=\"picker1.open()\">\n            <span>\n              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n            </span>\n          </button>\n          <div class=\"clearfix\"></div>\n        </div>\n      </li>\n  \n      <li class=\"col-md-3\">\n        <label>Location</label>\n        <mat-select class=\"form-control\" placeholder=\"Location\">\n          <mat-option value=\"option1\">HYD</mat-option>\n        </mat-select>\n      </li>\n\n      <li class=\"col-md-3\">\n        <label>Status</label>\n        <mat-select class=\"form-control\" placeholder=\"Status\">\n          <mat-option value=\"option1\">Active</mat-option>\n        </mat-select>\n      </li>\n\n    </ul>  \n    <div class=\"clearfix\"></div>\n\n  </div>\n\n\n  <div class=\"application-fields\">\n     <h3>Application Fields</h3>\n\n  </div>\n\n\n  <div class=\"application-questions\">\n\n    <h3>Application Questions</h3>\n\n\n    <table class=\"table\">\n      <thead>\n        <tr>\n          \n          <th>Questions</th>\n          <th>Data Type</th>\n          \n          <th>\n            <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n              <mat-icon>more_vert</mat-icon>\n            </button>\n            <mat-menu #menu=\"matMenu\">\n              <button mat-menu-item>\n                <a>Add</a>\n              </button>\n             \n              <button mat-menu-item>\n                  <a>Edit</a>\n              </button>\n            \n              <button mat-menu-item>\n                <a>Delete</a>\n              </button>\n\n            </mat-menu>\n          </th>\n        </tr>\n      </thead>\n\n      <tbody> \n        <tr>\n          <td>What is your salary Requirement?</td>\n          <td>\n            <mat-select class=\"form-control\" placeholder=\"Data Types\">\n              <mat-option value=\"option1\">Active</mat-option>\n            </mat-select>\n          </td>\n         <td></td>\n        </tr>\n\n        <tr>\n          <td>Have you been convicted of a felon?</td>\n          <td>\n            <mat-select class=\"form-control\" placeholder=\"Data Types\">\n              <mat-option value=\"option1\">Active</mat-option>\n            </mat-select>\n          </td>\n         <td></td>\n        </tr>\n        \n      </tbody>\n  </table>\n    \n  </div>\n\n        <button class=\"btn pull-left cancel\" (click)=\"onClick()\">Cancel</button>\n        <button class=\"btn pull-right save\">Submit</button>\n        <div class=\"clearfix\"></div>\n\n   </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: JobApplicationAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobApplicationAddComponent", function() { return JobApplicationAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var JobApplicationAddComponent = /** @class */ (function () {
    function JobApplicationAddComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    JobApplicationAddComponent.prototype.ngOnInit = function () {
    };
    JobApplicationAddComponent.prototype.onClick = function () {
        this.dialogRef.close();
    };
    JobApplicationAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-application-add',
            template: __webpack_require__(/*! ./job-application-add.component.html */ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.html"),
            styles: [__webpack_require__(/*! ./job-application-add.component.css */ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], JobApplicationAddComponent);
    return JobApplicationAddComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.css":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.css ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"job-bank col-md-11\">\n\n  <h2>Job Application</h2>\n\n  <div class=\"post-jobs-cont\">  \n  \n    <table class=\"table\">\n        <thead>\n          <tr>\n            <th><mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox></th>\n            <th>Application Name</th>\n            <th>Location</th>\n            <th>Status</th>\n            <th>\n              <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n                <mat-icon>more_vert</mat-icon>\n              </button>\n              <mat-menu #menu=\"matMenu\">\n                <button mat-menu-item>\n                  <a (click)=\"addJobApplication()\">Add</a>\n                </button>\n               \n                <button mat-menu-item>\n                    <a>Edit</a>\n                </button>\n                <button mat-menu-item>\n                  <a>Copy</a>\n                </button>\n                \n                <button mat-menu-item>\n                  <a>Preview</a>\n                </button>\n\n                <button mat-menu-item>\n                  <a>Archive</a>\n                </button>\n\n                <button mat-menu-item>\n                  <a>Delete</a>\n                </button>\n\n              </mat-menu>\n            </th>\n          </tr>\n        </thead>\n\n        <tbody> \n          <tr>\n            <td><mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox></td>\n            <td>Administrator</td>\n            <td>USA</td>\n            <td>Actibe</td>\n           <td></td>\n           \n          </tr>\n          \n        </tbody>\n    </table>\n\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: JobApplicationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobApplicationComponent", function() { return JobApplicationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _job_application_add_job_application_add_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../job-application-add/job-application-add.component */ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var JobApplicationComponent = /** @class */ (function () {
    function JobApplicationComponent(dialog) {
        this.dialog = dialog;
    }
    JobApplicationComponent.prototype.ngOnInit = function () {
    };
    JobApplicationComponent.prototype.addJobApplication = function () {
        var dialogRef = this.dialog.open(_job_application_add_job_application_add_component__WEBPACK_IMPORTED_MODULE_2__["JobApplicationAddComponent"], {
            width: '1200px',
            height: '800px'
        });
        dialogRef.afterClosed().subscribe(function (data) {
        });
    };
    JobApplicationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-application',
            template: __webpack_require__(/*! ./job-application.component.html */ "./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.html"),
            styles: [__webpack_require__(/*! ./job-application.component.css */ "./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], JobApplicationComponent);
    return JobApplicationComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.jobs-bank-popup { display: block;}\n.jobs-bank-popup .modal-dialog { width:65%;}\n.jobs-bank-popup .modal-content {border-radius: 5px; border: none; padding: 0 0 40px;}\n.jobs-bank-popup .modal-header {padding:30px;}\n.jobs-bank-popup .modal-body { padding: 0; background: #fff;border-radius: 5px;}\n.jobs-bank-main {float: none; margin: 0 auto; padding: 30px 0 0;}\n.jobs-bank-add {float: none; margin: 0 auto; padding: 0;}\n.jobs-bank-add ul { display:inline-block; padding: 0; width: 100%;}\n.jobs-bank-add ul li {margin: 0 0 25px;}\n.jobs-bank-add ul li label{color: #484747;font-size: 17px; padding: 0 0 10px;}\n.jobs-bank-add ul li .form-control{ border: none; box-shadow: none; padding: 13px 12px; height: auto;\nbackground:#f8f8f8; font-size: 15px;}\n.jobs-bank-add ul li .form-control:focus{ box-shadow: none;}\n.jobs-bank-add ul li .form-control.text-area{ resize: none; height: 160px;}\n.jobs-bank-add ul li.space-top { margin: 0 0 40px;}\n.jobs-bank-add ul li label.about-comp { display: inline-block; float:left}\n.standard-cont { float: right;}\n.standard-cont > ul { display: block;}\n.standard-cont > ul > li { display: inline-block; margin: 0 10px;}\n.standard-cont > ul > li > a { color: #848282; border:#ccc 1px solid; border-radius:30px; padding: 5px 20px; cursor: pointer;}\n.jobs-bank-main .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff;}\n.jobs-bank-main .btn.cancel {color:#6b6969; background:transparent;font-size: 16px;}\n.border-top {border-top:#ccc 1px solid; padding: 40px 0 0; float: none; margin: 0 auto;}\n.job-bank-width {float: none; margin: 0 auto; padding: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"job-bank col-md-11\">\n\n  <h2>Job Description</h2>\n\n  <div class=\"post-jobs-cont\">  \n  \n    <table class=\"table\">\n        <thead>\n          <tr>\n            <th><mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox></th>\n            <th>Job Title</th>\n            <th>Department</th>\n            <th>Business Unit</th>\n            <th>Location</th>\n           <th></th>\n            <th>\n              <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n                <mat-icon>more_vert</mat-icon>\n              </button>\n              <mat-menu #menu=\"matMenu\">\n                <button mat-menu-item>\n                  <a data-toggle=\"modal\" data-target=\"#myModal\">Add</a>\n                </button>\n               \n                <button mat-menu-item>\n                    <a>Edit</a>\n                </button>\n                <button mat-menu-item>\n                  <a>Copy</a>\n                </button>\n                \n                <button mat-menu-item>\n                  <a>Preview</a>\n                </button>\n\n                <button mat-menu-item>\n                  <a>Archive</a>\n                </button>\n\n                <button mat-menu-item>\n                  <a>Delete</a>\n                </button>\n\n              </mat-menu>\n            </th>\n          </tr>\n        </thead>\n\n        <tbody> \n          <tr>\n            <td><mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox></td>\n            <td>Administrator</td>\n            <td>Sales</td>\n            <td>East</td>\n            <td>India</td>\n            <td></td>\n           <td></td>\n           \n          </tr>\n          \n        </tbody>\n    </table>\n\n  </div>\n\n</div>\n\n\n\n\n\n<!-- Add / Edit - Job Description -->\n<div class=\"jobs-bank-popup\">\n\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Add / Edit - Job Description</h4>\n        </div>\n        <div class=\"modal-body\">\n          \n          <div class=\"jobs-bank-main\">\n\n            <div class=\"jobs-bank-add col-md-10\">\n\n              <ul>\n                <li class=\"col-md-4\">\n                  <label>Posting Title *</label>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Job Title\">\n                </li>                       \n              </ul>\n    \n              <ul>\n                <li class=\"col-md-4\">\n                  <label>Department *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Department\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n            \n                <li class=\"col-md-4\">\n                  <label>Location *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Hiring Manager\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n            \n                <li class=\"col-md-4\">\n                  <label>Business Unit *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Hiring Manager\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n              </ul>\n\n              <ul>\n                <li class=\"col-md-4\">\n                  <label>Industry *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Industry\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n            \n                <li class=\"col-md-4\">\n                  <label>Function *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Function\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n                        \n              </ul>\n\n              <ul>\n                <li class=\"col-md-3\">\n                  <label>Grade *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Grade\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n            \n                <li class=\"col-md-3\">\n                  <label>Job Type *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Job Type\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n\n                <li class=\"col-md-6\">\n                  <label>Will the position have direct reports? *</label>\n                  <mat-select class=\"form-control\" placeholder=\"position\">\n                    <mat-option value='Standard'>Manager</mat-option>\n                    <mat-option value='Employee'>Manager12</mat-option>\n                  </mat-select>\n                </li>\n\n                <li class=\"col-md-12 space-top\">\n                    <label>Description</label>\n                  <textarea class=\"form-control text-area\" rows=\"5\" id=\"comment\" placeholder=\"Description\"></textarea>\n                </li>\n\n                <li class=\"col-md-12 space-top\">\n                    <label>Requirements</label>\n                  <textarea class=\"form-control text-area\" rows=\"5\" id=\"comment\" placeholder=\"Requirements\"></textarea>\n                </li>\n\n                <li class=\"col-md-12 space-top\">\n                  <label class=\"about-comp\">About the Compnay</label>\n                  <div class=\"standard-cont\">\n                    <ul>\n                      <li><a (click)=\"editStandard()\">Edit Standard Content</a></li>\n                      <li><a>Load Standard Content</a></li>\n                    </ul>\n                  </div>\n                  <div class=\"clearfix\"></div>\n                  <textarea class=\"form-control text-area\" rows=\"5\" id=\"comment\" placeholder=\"Requirements\"></textarea>\n              </li>\n                        \n              </ul>\n    \n              <div class=\"clearfix\"></div>\n             </div>            \n             <div class=\"border-top\">\n               <div class=\"job-bank-width col-md-10\">\n                  <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                  <button class=\"btn pull-right save\">Save / Create the Posting</button>\n                  <div class=\"clearfix\"></div>\n               </div>\n              \n             </div>\n              \n          </div>\n\n        \n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n\n\n<!-- Job Description -- About Company -->\n<div class=\"jobs-bank-popup\">\n\n    <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n  \n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <h4 class=\"modal-title\">Add / Edit - Job Description <br>\n              (About the Compnay - Standard Content)\n            </h4>\n          </div>\n          <div class=\"modal-body\">\n            \n            <div class=\"jobs-bank-main\">\n  \n              <div class=\"jobs-bank-add col-md-11\">\n  \n                <ul>\n                  \n                  <li class=\"col-md-12 space-top\">\n                      <label>About the Compnay</label>\n                    <textarea class=\"form-control text-area\" rows=\"5\" id=\"comment\" placeholder=\"Description\"></textarea>\n                  </li>\n                \n                </ul>\n      \n                <div class=\"clearfix\"></div>\n               </div>            \n               <div class=\"border-top\">\n                 <div class=\"job-bank-width col-md-10\">\n                    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                    <button class=\"btn pull-right save\">Submit</button>\n                    <div class=\"clearfix\"></div>\n                 </div>\n                \n               </div>\n                \n            </div>\n  \n           \n  \n          </div>\n  \n        </div>\n  \n      </div>\n    </div>\n  \n  </div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.ts ***!
  \********************************************************************************************/
/*! exports provided: JobBankComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobBankComponent", function() { return JobBankComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JobBankComponent = /** @class */ (function () {
    function JobBankComponent() {
    }
    JobBankComponent.prototype.ngOnInit = function () {
    };
    JobBankComponent.prototype.editStandard = function () {
        $("#myModal1").modal('show');
        $("#myModal").modal('hide');
    };
    JobBankComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-bank',
            template: __webpack_require__(/*! ./job-bank.component.html */ "./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.html"),
            styles: [__webpack_require__(/*! ./job-bank.component.css */ "./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], JobBankComponent);
    return JobBankComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-description.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-description.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.job-main { margin: 0 auto; float: none; padding: 0;}\n\n.job-tabs {margin: 30px 0 0;}\n\n.job-tabs .tab-content { margin:40px 0 0;}\n\n.job-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus\n{ border-bottom:#439348 4px solid;background: transparent;color: #484747 !important;font-size: 18px;}\n\n.job-tabs .nav-tabs {border-bottom:none; padding: 15px 0 0; background: #fff;}\n\n.job-tabs .nav-tabs > li { margin: 0 50px;}\n\n.job-tabs .nav-tabs > li > a {font-size: 18px;line-height: 17px;font-weight:normal;color:#484747 !important;border: none;padding: 10px 0 18px !important;\nmargin: 0; cursor: pointer;}\n\n.job-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;color: #484747 !important;font-size: 18px;border-bottom:transparent;}\n\n.job-tabs .nav-tabs > li.active a{border-bottom:#439348 4px solid;color: #484747 !important;font-size: 18px;}\n"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-description.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-description.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"job-main col-md-11\">\n\n<div class=\"arrow-block\">\n\n    <a routerLink=\"/admin/admin-dashboard/recruitment\">\n        <small><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></small>\n        <em>Back</em>\n    </a>\n\n    <h3>\n        <img src=\"../../../assets/images/company-settings/company-setup/company-setup.png\" alt=\"Company-settings icon\">\n        <span>Jobs Description & Applications</span> \n    </h3>\n\n  </div>\n\n  <div class=\"job-tabs\">\n\n    <ul class=\"nav nav-tabs\">\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/recruitment/job-description/job-bank\">Job Bank</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/recruitment/job-description/load-job\">Load Job Description</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/recruitment/job-description/job-application\">Job Applications</a>\n      </li>\n     \n    </ul>\n    <div class=\"clearfix\"></div>\n\n  </div>\n\n</div>\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-description.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-description.component.ts ***!
  \******************************************************************************************/
/*! exports provided: JobDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobDescriptionComponent", function() { return JobDescriptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JobDescriptionComponent = /** @class */ (function () {
    function JobDescriptionComponent() {
    }
    JobDescriptionComponent.prototype.ngOnInit = function () {
    };
    JobDescriptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job-description',
            template: __webpack_require__(/*! ./job-description.component.html */ "./src/app/admin-dashboard/recruitment/job-description/job-description.component.html"),
            styles: [__webpack_require__(/*! ./job-description.component.css */ "./src/app/admin-dashboard/recruitment/job-description/job-description.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], JobDescriptionComponent);
    return JobDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/job-description.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/job-description.module.ts ***!
  \***************************************************************************************/
/*! exports provided: JobDescriptionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobDescriptionModule", function() { return JobDescriptionModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _job_description_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./job-description.component */ "./src/app/admin-dashboard/recruitment/job-description/job-description.component.ts");
/* harmony import */ var _job_bank_job_bank_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./job-bank/job-bank.component */ "./src/app/admin-dashboard/recruitment/job-description/job-bank/job-bank.component.ts");
/* harmony import */ var _load_job_description_load_job_description_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./load-job-description/load-job-description.component */ "./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.ts");
/* harmony import */ var _job_application_job_application_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./job-application/job-application.component */ "./src/app/admin-dashboard/recruitment/job-description/job-application/job-application.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _job_application_add_job_application_add_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./job-application-add/job-application-add.component */ "./src/app/admin-dashboard/recruitment/job-description/job-application-add/job-application-add.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    { path: '', component: _job_description_component__WEBPACK_IMPORTED_MODULE_3__["JobDescriptionComponent"], children: [
            { path: '', redirectTo: 'job-bank', pathMatch: 'full' },
            { path: 'job-bank', component: _job_bank_job_bank_component__WEBPACK_IMPORTED_MODULE_4__["JobBankComponent"] },
            { path: 'load-job', component: _load_job_description_load_job_description_component__WEBPACK_IMPORTED_MODULE_5__["LoadJobDescriptionComponent"] },
            { path: 'job-application', component: _job_application_job_application_component__WEBPACK_IMPORTED_MODULE_6__["JobApplicationComponent"] }
        ] }
];
var JobDescriptionModule = /** @class */ (function () {
    function JobDescriptionModule() {
    }
    JobDescriptionModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_job_description_component__WEBPACK_IMPORTED_MODULE_3__["JobDescriptionComponent"], _job_bank_job_bank_component__WEBPACK_IMPORTED_MODULE_4__["JobBankComponent"], _load_job_description_load_job_description_component__WEBPACK_IMPORTED_MODULE_5__["LoadJobDescriptionComponent"], _job_application_job_application_component__WEBPACK_IMPORTED_MODULE_6__["JobApplicationComponent"], _job_application_add_job_application_add_component__WEBPACK_IMPORTED_MODULE_14__["JobApplicationAddComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes), _angular_material_select__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_9__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_11__["MatDatepickerModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__["MatCheckboxModule"], _angular_material_paginator__WEBPACK_IMPORTED_MODULE_13__["MatPaginatorModule"]],
            entryComponents: [_job_application_add_job_application_add_component__WEBPACK_IMPORTED_MODULE_14__["JobApplicationAddComponent"]]
        })
    ], JobDescriptionModule);
    return JobDescriptionModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.css":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.css ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".load-job { margin: 0 auto; float: none; padding: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.html":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.html ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"load-job col-md-11\">\n  load-job-description works!\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.ts":
/*!********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.ts ***!
  \********************************************************************************************************************/
/*! exports provided: LoadJobDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadJobDescriptionComponent", function() { return LoadJobDescriptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadJobDescriptionComponent = /** @class */ (function () {
    function LoadJobDescriptionComponent() {
    }
    LoadJobDescriptionComponent.prototype.ngOnInit = function () {
    };
    LoadJobDescriptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-load-job-description',
            template: __webpack_require__(/*! ./load-job-description.component.html */ "./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.html"),
            styles: [__webpack_require__(/*! ./load-job-description.component.css */ "./src/app/admin-dashboard/recruitment/job-description/load-job-description/load-job-description.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LoadJobDescriptionComponent);
    return LoadJobDescriptionComponent;
}());



/***/ })

}]);
//# sourceMappingURL=job-description-job-description-module.js.map