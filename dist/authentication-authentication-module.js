(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["authentication-authentication-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/filter.js ***!
  \***************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/filter */ "./node_modules/rxjs-compat/_esm5/operator/filter.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.filter = _operator_filter__WEBPACK_IMPORTED_MODULE_1__["filter"];
//# sourceMappingURL=filter.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/filter.js":
/*!***********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/filter.js ***!
  \***********************************************************/
/*! exports provided: filter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filter", function() { return filter; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

function filter(predicate, thisArg) {
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["filter"])(predicate, thisArg)(this);
}
//# sourceMappingURL=filter.js.map

/***/ }),

/***/ "./src/app/authentication/authentication.component.css":
/*!*************************************************************!*\
  !*** ./src/app/authentication/authentication.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/authentication/authentication.component.html":
/*!**************************************************************!*\
  !*** ./src/app/authentication/authentication.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n\n<app-footer-authentication></app-footer-authentication>"

/***/ }),

/***/ "./src/app/authentication/authentication.component.ts":
/*!************************************************************!*\
  !*** ./src/app/authentication/authentication.component.ts ***!
  \************************************************************/
/*! exports provided: AuthenticationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationComponent", function() { return AuthenticationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthenticationComponent = /** @class */ (function () {
    function AuthenticationComponent() {
    }
    AuthenticationComponent.prototype.ngOnInit = function () {
    };
    AuthenticationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-authentication',
            template: __webpack_require__(/*! ./authentication.component.html */ "./src/app/authentication/authentication.component.html"),
            styles: [__webpack_require__(/*! ./authentication.component.css */ "./src/app/authentication/authentication.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AuthenticationComponent);
    return AuthenticationComponent;
}());



/***/ }),

/***/ "./src/app/authentication/authentication.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/authentication/authentication.module.ts ***!
  \*********************************************************/
/*! exports provided: AuthenticationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationModule", function() { return AuthenticationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/authentication/login/login.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _footer_authentication_footer_authentication_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./footer-authentication/footer-authentication.component */ "./src/app/authentication/footer-authentication/footer-authentication.component.ts");
/* harmony import */ var _authentication_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./authentication.component */ "./src/app/authentication/authentication.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./register/register.component */ "./src/app/authentication/register/register.component.ts");
/* harmony import */ var _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./sign-up/sign-up.component */ "./src/app/authentication/sign-up/sign-up.component.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _terms_terms_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./terms/terms.component */ "./src/app/authentication/terms/terms.component.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _self_signon_details_self_signon_details_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./self-signon-details/self-signon-details.component */ "./src/app/authentication/self-signon-details/self-signon-details.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _self_sign_on_data_self_sign_on_data_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./self-sign-on-data/self-sign-on-data.component */ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/authentication/forgot-password/forgot-password.component.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./reset-password/reset-password.component */ "./src/app/authentication/reset-password/reset-password.component.ts");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var router = [
    {
        path: '', component: _authentication_component__WEBPACK_IMPORTED_MODULE_6__["AuthenticationComponent"], children: [
            { path: 'zenwork-login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
            { path: 'register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"] },
            { path: 'sign-up', component: _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_9__["SignUpComponent"] },
            { path: 'terms', component: _terms_terms_component__WEBPACK_IMPORTED_MODULE_11__["TermsComponent"] },
            { path: 'self-signon-detail', component: _self_signon_details_self_signon_details_component__WEBPACK_IMPORTED_MODULE_13__["SelfSignonDetailsComponent"] },
            { path: 'social', component: _self_signon_details_self_signon_details_component__WEBPACK_IMPORTED_MODULE_13__["SelfSignonDetailsComponent"] },
            { path: 'reset-password/:id', component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_16__["ForgotPasswordComponent"] },
            { path: 'reset', component: _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_17__["ResetPasswordComponent"] }
        ]
    }
];
var AuthenticationModule = /** @class */ (function () {
    function AuthenticationModule() {
    }
    AuthenticationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_14__["MaterialModuleModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(router), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_18__["MatFormFieldModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"]
            ],
            declarations: [_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"], _footer_authentication_footer_authentication_component__WEBPACK_IMPORTED_MODULE_5__["FooterAuthenticationComponent"], _authentication_component__WEBPACK_IMPORTED_MODULE_6__["AuthenticationComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"],
                _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_9__["SignUpComponent"], _terms_terms_component__WEBPACK_IMPORTED_MODULE_11__["TermsComponent"], _self_signon_details_self_signon_details_component__WEBPACK_IMPORTED_MODULE_13__["SelfSignonDetailsComponent"], _self_sign_on_data_self_sign_on_data_component__WEBPACK_IMPORTED_MODULE_15__["SelfSignOnDataComponent"],
                _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_16__["ForgotPasswordComponent"], _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_17__["ResetPasswordComponent"]],
            providers: [
                _services_authentication_service__WEBPACK_IMPORTED_MODULE_12__["AuthenticationService"]
            ],
            entryComponents: [
                _self_signon_details_self_signon_details_component__WEBPACK_IMPORTED_MODULE_13__["SelfSignonDetailsComponent"],
                _self_sign_on_data_self_sign_on_data_component__WEBPACK_IMPORTED_MODULE_15__["SelfSignOnDataComponent"]
            ]
        })
    ], AuthenticationModule);
    return AuthenticationModule;
}());



/***/ }),

/***/ "./src/app/authentication/footer-authentication/footer-authentication.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/authentication/footer-authentication/footer-authentication.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.login-footer { background: #3e3e3e; padding: 30px 0 25px; margin:50px 0 0; position: relative;}\n\n.login-footer figure { position: absolute; top:35px; left: 10%;}\n\n.copy{ text-align: center;}\n\n.copy p { color: #807e7e; font-size: 15px; line-height: 15px; padding: 10px 0 0;}\n\n.copy p a {color: #807e7e;}"

/***/ }),

/***/ "./src/app/authentication/footer-authentication/footer-authentication.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/authentication/footer-authentication/footer-authentication.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-footer\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <figure><a href=\"#\"><img src=\"../../../assets/images/main-nav/whitelogo2.png\" width=\"200\" alt=\"img\"></a></figure>\n\n      <div class=\"copy\">\n        <p>2019 © Copyright | Zenwork, Inc. | All Rights Reserved. | <a href=\"#\">Privacy Policy</a> | \n          <a routerLink=\"/terms\" (click)=\"termsClick()\">Terms\n            of Use</a> </p>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/authentication/footer-authentication/footer-authentication.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/authentication/footer-authentication/footer-authentication.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: FooterAuthenticationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterAuthenticationComponent", function() { return FooterAuthenticationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterAuthenticationComponent = /** @class */ (function () {
    function FooterAuthenticationComponent() {
    }
    FooterAuthenticationComponent.prototype.ngOnInit = function () {
    };
    FooterAuthenticationComponent.prototype.termsClick = function () {
        window.scroll(0, 0);
    };
    FooterAuthenticationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer-authentication',
            template: __webpack_require__(/*! ./footer-authentication.component.html */ "./src/app/authentication/footer-authentication/footer-authentication.component.html"),
            styles: [__webpack_require__(/*! ./footer-authentication.component.css */ "./src/app/authentication/footer-authentication/footer-authentication.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterAuthenticationComponent);
    return FooterAuthenticationComponent;
}());



/***/ }),

/***/ "./src/app/authentication/forgot-password/forgot-password.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/authentication/forgot-password/forgot-password.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".forgot { display: block; float: none; margin: 0 auto; padding: 0; height: 500px;}\n.tab{ display: table; width: 100%; height: 100%;}\n.tab-cel{ display: table-cell; width: 100%; height: 100%; vertical-align: middle;}\n.forgot a img { margin: 0 auto 40px; display: block;}\n.forgot a img:focus, .reset-pass a:focus { outline: none; border: none;}\n.forgot ul { display: block;}\n.forgot ul li { margin: 0 0 30px; position: relative;}\n.forgot ul li label { display: block; font-size: 17px; color:#525252; padding:0 0 10px;}\n.forgot ul li .form-control { height: auto; padding: 10px 12px; box-shadow: none; height: auto; font-size: 15px; position: relative;\nwidth: 90%;float: left; border:none;}\n.forgot ul li .btn { border: none; background:#439348; color:#fff; border-radius:30px; padding:10px 30px; font-size: 15px;\ndisplay: block; margin:15px auto 0; cursor: pointer;}\n.forgot ul li:nth-last-child(1){ margin-bottom: 0;}\n.forgot ul li b {cursor: pointer; float: right; margin: 10px 10px 0 0;}\n.forgot ul li strong {cursor: pointer; float: right; margin: 10px 10px 0 0;}\n.input-block { display: block; border: 1px solid #ccc; border-radius:4px;}\n.error { color:#e44a49; padding: 5px 0 0; font-size: 14px;}"

/***/ }),

/***/ "./src/app/authentication/forgot-password/forgot-password.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/authentication/forgot-password/forgot-password.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "|<div class=\"forgot col-md-3\">\n    <div class=\"tab\">\n      <div class=\"tab-cel\">\n          <a href=\"#\"><img src=\"../../../assets/images/main-nav/side-logo.png\"></a>\n        <ul>\n          <li>\n            <label>New Password</label>\n            <div class=\"input-block\">\n                <input type=\"text\" placeholder=\"New password\" class=\"form-control\" [type]=\"passwordType\" \n                [(ngModel)]=\"newPassword\" name=\"password\" #password=\"ngModel\" required>\n                <b (click)=\"newPasswordView()\" *ngIf=\"passwordType === 'password'\"> <img src=\"../../../assets/images/authentication/not-view.png\" width=\"17\" height=\"9\" alt=\"img\"></b>\n                <strong (click)=\"newPasswordView()\" *ngIf=\"passwordType === 'text'\"> <img src=\"../../../assets/images/authentication/view.png\" width=\"17\" height=\"14\" alt=\"img\"></strong>\n                <div class=\"clearfix\"></div>\n            </div>\n            <p *ngIf=\"!newPassword && password.touched || (!newPassword && isValid)\" class=\"error\">Enter New password</p>\n          </li>\n          <li>\n              <label>Confirm Password</label>\n              <div class=\"input-block\">\n                  <input type=\"text\" placeholder=\"Cofirm password\" class=\"form-control\" [type]=\"passwordTypeNew\" \n                  [(ngModel)]=\"confirmPassword\" name=\"confirm\" #confirm=\"ngModel\" required>\n                  <b (click)=\"ConfirmPasswordView()\" *ngIf=\"passwordTypeNew === 'password'\"> <img src=\"../../../assets/images/authentication/not-view.png\" width=\"17\" height=\"9\" alt=\"img\"></b>\n                  <strong (click)=\"ConfirmPasswordView()\" *ngIf=\"passwordTypeNew === 'text'\"> <img src=\"../../../assets/images/authentication/view.png\" width=\"17\" height=\"14\" alt=\"img\"></strong>\n                  <div class=\"clearfix\"></div>\n              </div>\n             <p *ngIf=\"!confirmPassword && confirm.touched || (!confirmPassword && isValid)\" class=\"error\">Enter Confirm password</p>\n          </li>\n          <li>\n              <button class=\"btn\" (click)=\"forgetSubmit()\">Submit</button>\n          </li>\n        </ul>\n\n      </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/authentication/forgot-password/forgot-password.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/authentication/forgot-password/forgot-password.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(forgotService, swalService, active, router) {
        this.forgotService = forgotService;
        this.swalService = swalService;
        this.active = active;
        this.router = router;
        this.passwordType = "password";
        this.passwordTypeNew = "password";
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.active.params.subscribe(function (params) {
            _this.token = params['id'];
            console.log("tokenn iddddd", _this.token);
        });
    };
    // Author:Suresh M, Date:27-08-19
    // newPasswordView
    ForgotPasswordComponent.prototype.newPasswordView = function () {
        if (this.passwordType === 'password') {
            this.passwordType = 'text';
        }
        else {
            this.passwordType = 'password';
        }
    };
    // Author:Suresh M, Date:27-08-19
    // ConfirmPasswordView
    ForgotPasswordComponent.prototype.ConfirmPasswordView = function () {
        if (this.passwordTypeNew === 'password') {
            this.passwordTypeNew = 'text';
        }
        else {
            this.passwordTypeNew = 'password';
        }
    };
    // Author:Suresh M, Date:27-08-19
    // forgetSubmit
    ForgotPasswordComponent.prototype.forgetSubmit = function () {
        var _this = this;
        var changePassword = {
            token: this.token,
            newPassword: this.newPassword,
            confirmPassword: this.confirmPassword
        };
        if (!this.newPassword) {
            this.isValid = true;
        }
        else if (!this.confirmPassword) {
            this.isValid = true;
        }
        else {
            this.forgotService.forgotPassword(changePassword)
                .subscribe(function (res) {
                console.log("Change password success", res);
                if (res.status = true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Change password", "Password changed successfully", "success");
                }
                _this.router.navigate(['/zenwork-login']);
            }, (function (err) {
                _this.swalService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            }));
        }
    };
    ForgotPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__(/*! ./forgot-password.component.html */ "./src/app/authentication/forgot-password/forgot-password.component.html"),
            styles: [__webpack_require__(/*! ./forgot-password.component.css */ "./src/app/authentication/forgot-password/forgot-password.component.css")]
        }),
        __metadata("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"], _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/authentication/login/login.component.css":
/*!**********************************************************!*\
  !*** ./src/app/authentication/login/login.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login { padding: 100px 0;}\n\n.login a { display: inline-block;}\n\n.login-in { padding: 50px 0 0;}\n\n.login-lft { padding: 0;}\n\n.login-lft h2 { color:#c1c2c1; font-size: 27px; line-height: 25px; font-weight: bold; padding: 0 0 10px;}\n\n.login-lft small { color:#cccccc; font-size: 13px; line-height: 14px; padding: 0 0 20px; display: block;}\n\n.login-lft h3 { color:#000; font-size: 16px; padding: 0 0 10px;}\n\n.login-lft a.forget { color:#e44a49;font-size: 14px; line-height: 14px; margin: 5px 0 0; cursor: pointer;}\n\n.login-lft ul { margin: 0 0 40px;}\n\n.login-lft ul li { width: 56%; margin: 0 0 20px; position: relative;}\n\n.login-lft ul li .form-group { background-color:#f8f8f8; border-radius: 4px; margin-bottom: 0;}\n\n.login-lft ul li em { float: left; width:38px; padding: 4px 0 10px 10px; border-right: #c7c7c7 1px solid; margin: 6px 0; height: 30px;}\n\n.login-lft ul li .form-control { border: none; box-shadow:none; height: auto; padding: 11px 12px; float:left; width: 87%; background-color:transparent;}\n\n.login-lft ul li .form-control:focus { border:none; box-shadow: none;}\n\n.login-lft ul li:nth-last-child(1) { background-color: transparent;}\n\n.login-lft h4 { border-top:#e0e0e0 1px solid; position: relative;width: 50%; padding: 8px 0 0;}\n\n.login-lft h4 span { \n    color: #9c9c9c;\n    font-size: 13px;\n    position: absolute;\n    top: -9px;\n    left: 0;\n    right: 0;\n    text-align: center;\n    background: #fff;\n    display: inline-block;\n    width: 40%;\n    margin: 0 auto;\n}\n\n.login-lft .btn { color:#fff; padding: 10px 35px; border-radius: 30px;font-size: 15px; float: left; background: #099247; margin: 0 0 10px;}\n\n.login-lft p { color:#2b7aca; font-size: 13px; float: left; padding: 12px 0 0 15px; margin: 0;}\n\n.login-lft p a { color:#e44a49;}\n\n.login-rgt { padding: 0;}\n\n.login-rgt figure { display: block;}\n\n.login-lft a p{ display: inline-block; float:none; padding: 0 10px 0 0; vertical-align: text-top;}\n\n.login-lft a.google { color:#076c93;font-size: 14px; line-height:14px;border-radius:30px;text-align: center;\n    background: #f1f9f4; padding:15px 0; width: 56%; font-weight: bold; margin: 5px 0 0;}\n\n.company-logos { margin: 10px 0 0;}\n\n.company-logos > ul { display: block;}\n\n.company-logos > ul > li{ display:inline-block; margin: 0 10px 0 0; background-color: transparent; width: 27%;}\n\n.company-logos > ul > li a { color:#076c93;font-size: 14px; line-height:14px;border-radius:30px;text-align: center;\nbackground: #f1f9f4; padding:15px 0; font-weight: bold; width: 100%; cursor: pointer;}\n\n.login-lft ul li b { position: absolute; top:10px; right:10px; cursor: pointer;}\n\n.login-lft ul li strong { position: absolute; top:10px; right:10px; cursor: pointer;}\n\n.errors {color:#e44a49; padding: 5px 0 0;}"

/***/ }),

/***/ "./src/app/authentication/login/login.component.html":
/*!***********************************************************!*\
  !*** ./src/app/authentication/login/login.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <a routerLink=\"/\"><img src=\"../../../assets/images/main-nav/Component 1@2x.png\" alt=\"img\"></a>\n\n      <div class=\"login-in\">\n\n        <div class=\"login-lft col-sm-6\">\n          <h2>HRIS Management</h2>\n          <small>Let Zenwork crunch the numbers while you focus on the big picture.</small>\n\n          <h3>Login as</h3>\n          \n          <form [formGroup]=\"loginForm\" (ngSubmit)=\"loginSubmit()\">\n\n          <ul>\n\n            <li>\n                <mat-select class=\"form-control\" formControlName=\"type\">\n                    <mat-option value=\"\">Select</mat-option>\n                    <mat-option value=\"employee\">Employee</mat-option>\n                    <mat-option value=\"reseller\">Reseller</mat-option>\n                    <mat-option value=\"company\">Company</mat-option>\n                  </mat-select>\n\n            </li>\n            \n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/mail.png\" width=\"13\" height=\"9\" alt=\"img\"></em>\n                <input  class=\"form-control\" placeholder=\"User mail\" formControlName=\"email\" pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n                <div class=\"clearfix\"></div>\n              </div>  \n              <div *ngIf=\"loginForm.get('email').invalid && loginForm.get('email').touched\" class=\"errors\">\n                  Please enter Email\n                </div>\n            </li>\n            \n\n            <li>\n              <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/lock.png\" width=\"10\" height=\"13\" alt=\"img\"></em>\n                <input  class=\"form-control\" placeholder=\"Password\" formControlName=\"password\" [type]=\"passwordType\">\n                <b (click)=\"getview()\" *ngIf=\"passwordType==='password'\"> <img src=\"../../../assets/images/authentication/not-view.png\" width=\"17\" height=\"9\" alt=\"img\"></b>\n                <strong (click)=\"getview()\" *ngIf=\"passwordType==='text'\"> <img src=\"../../../assets/images/authentication/view.png\" width=\"17\" height=\"14\" alt=\"img\"></strong>\n                <div class=\"clearfix\"></div>\n              </div>  \n              <div *ngIf=\"loginForm.get('password').invalid && loginForm.get('password').touched\" class=\"errors\">\n                  Please enter password\n                </div>\n            </li>\n\n            <li>\n              <a class=\"forget\" (click)=\"openDialog()\">Forgot Password?</a>\n            </li>\n          </ul>\n\n          <button type=\"submit\" class=\"btn\">Login</button>\n\n        </form>\n        \n          \n          \n          <p>Don't Have an Account? <a routerLink=\"/sign-up\">Get Started </a></p>\n          <div class=\"clearfix\"></div>\n\n          <h4><span>or Login with</span></h4>\n\n          <a  [href] = \"enviUrl\" class=\"google\"><p><img src=\"../../../assets/images/authentication/google.png\" width=\"18\" height=\"18\" alt=\"img\"> </p>Google</a>\n\n          <div class=\"company-logos\">\n            <ul>\n              <li><a href=\"#\"><img src=\"../../../assets/images/authentication/intuit.png\" width=\"52\" height=\"15\" alt=\"img\"></a></li>\n              <li><a href=\"#\"><img src=\"../../../assets/images/authentication/xero.png\" width=\"78\" height=\"24\" alt=\"img\"></a></li>\n            </ul>\n            <div class=\"clearfx\"></div>\n          </div>\n        \n        </div>\n\n\n        <div class=\"login-rgt col-sm-6\">\n          <figure><img src=\"../../../assets/images/authentication/login-img.png\" width=\"679\" height=\"594\" alt=\"img\"></figure>\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n\n    </div>\n  </div>\n</div>\n\n\n\n"

/***/ }),

/***/ "./src/app/authentication/login/login.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/authentication/login/login.component.ts ***!
  \*********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../reset-password/reset-password.component */ "./src/app/authentication/reset-password/reset-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var LoginComponent = /** @class */ (function () {
    function LoginComponent(loginService, router, zenworkerService, accessLocalStorageService, swalAlertService, messageService, route, dialog) {
        this.loginService = loginService;
        this.router = router;
        this.zenworkerService = zenworkerService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.messageService = messageService;
        this.route = route;
        this.dialog = dialog;
        this.passwordType = "password";
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.enviUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].url + "/api/v1.0/users/login/google";
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    LoginComponent.prototype.getview = function () {
        if (this.passwordType === "password") {
            this.passwordType = "text";
        }
        else {
            this.passwordType = "password";
        }
    };
    LoginComponent.prototype.loginSubmit = function () {
        var _this = this;
        this.zenworkerService.login(this.loginForm.value)
            .subscribe(function (res) {
            if (res.status) {
                console.log(res);
                _this.zenworkerDetails = res.data;
                _this.accessLocalStorageService.set('x-access-token', _this.zenworkerDetails.token);
                console.log(_this.zenworkerDetails.type);
                if (_this.zenworkerDetails.type == "employee") {
                    console.log("sadasd");
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    if ((_this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'Manager') ||
                        (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && _this.zenworkerDetails.job.Site_AccessRole.name == 'Manager')) {
                        _this.accessLocalStorageService.set('siteAccessRoleId', _this.zenworkerDetails.job.Site_AccessRole);
                        _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                        _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                        _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name);
                            _this.accessLocalStorageService.set('roleType', 'customManager');
                        }
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.name);
                            _this.accessLocalStorageService.set('roleType', 'baseManager');
                        }
                        _this.router.navigate(['/admin/admin-dashboard/employee-management']);
                    }
                    if ((_this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'HR') ||
                        (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && _this.zenworkerDetails.job.Site_AccessRole.name == 'HR')) {
                        _this.accessLocalStorageService.set('siteAccessRoleId', _this.zenworkerDetails.job.Site_AccessRole);
                        _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                        _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                        _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name);
                            _this.accessLocalStorageService.set('roleType', 'customHR');
                        }
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.name);
                            _this.accessLocalStorageService.set('roleType', 'baseHR');
                        }
                        _this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                        console.log("data comes in HR");
                    }
                    if ((_this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'Employee') ||
                        (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && _this.zenworkerDetails.job.Site_AccessRole.name == 'Employee')) {
                        _this.accessLocalStorageService.set('siteAccessRoleId', _this.zenworkerDetails.job.Site_AccessRole);
                        _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                        _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                        _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name);
                            _this.accessLocalStorageService.set('roleType', 'customEmployee');
                        }
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.name);
                            _this.accessLocalStorageService.set('roleType', 'baseEmployee');
                        }
                        _this.router.navigate(['/admin/admin-dashboard/dashboard/employee-view']);
                        console.log("data comes in emplpoyee");
                    }
                }
                if (_this.zenworkerDetails.type === 'company' || _this.zenworkerDetails.type === 'reseller') {
                    console.log("company");
                    _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    console.log(_this.zenworkerDetails._id);
                    _this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                    _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                    _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                    // localStorage.setItem('addedBy', this.zenworkerDetails.companyId.userId)
                }
                // for (var i = 0; i < this.userProfile.length; i++) {
                else if (_this.zenworkerDetails.type === 'sr-support' || _this.zenworkerDetails.type === 'zenworker') {
                    console.log("dsfsfsd");
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    _this.accessLocalStorageService.set('zenworkerId', _this.zenworkerDetails.zenworkerId);
                    _this.accessLocalStorageService.set('resellerId', _this.zenworkerDetails.zenworkerId.companyId);
                    _this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
                }
                if (_this.zenworkerDetails.type === 'administrator') {
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    _this.accessLocalStorageService.set('resellercmpnyID', _this.zenworkerDetails.companyId.userId);
                    _this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + _this.zenworkerDetails.companyId.userId]);
                    _this.messageService.sendMessage('reseller');
                }
                // }
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Login", res.message, 'info');
            }
        }, function (err) {
            console.log("Error", err.error.message);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Login", err.error.message, 'error');
        });
    };
    // Author:Suresh M, Date:27-08-19
    // openDialog Popup
    LoginComponent.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_10__["ResetPasswordComponent"], {
            width: '700px',
            height: '600px',
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/authentication/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/authentication/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_5__["ZenworkersService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_7__["MessageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatDialog"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/authentication/register/register.component.css":
/*!****************************************************************!*\
  !*** ./src/app/authentication/register/register.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.register-popup { text-align: center;}\n.register-popup .modal-dialog { width:35%;}\n.register-popup .modal-content {border-radius: 3px; padding: 40px 0;}\n.register-popup .modal-header {padding: 25px 30px;}\n.register-popup .modal-body { padding: 0; background: #fff;border-radius: 3px;}\n.register-popup a img { margin:0 auto 15px;}\n.register-popup h5{ color:#696767; font-size:18px;}\n.time-zone { padding: 20px 0;}\n.time-zone small { display: inline-block; vertical-align: middle; font-size: 17px; float:none; color:#757575;}\n.time-zone span { display: inline-block; vertical-align: middle; text-align: left; float: none;}\n.time-zone span .form-control { height: auto; padding:10px 12px;background: #f1f1f1;border: none;}\n.time-zone span .form-control:focus{ border-color:transparent; box-shadow: none;}\n.register-popup em { font-style: normal;color:#696767; font-size: 15px; display: block; padding: 0 0 15px;}\n.register-popup p {color:#099247; font-size:20px;display: block; padding: 0 0 50px; font-weight: 500;}\n.register-popup .btn { background: #099247; border-radius: 20px; color: #fff; text-align: center; \npadding:8px 50px; font-size: 17px;}\n.register-popup .btm-popup{ border-top: #d0cccc 1px solid; padding: 20px 0 0; margin:50px 50px 0;}\n.register-popup .btm-popup p {color:#696767; font-size: 15px; padding: 0; margin: 0 0 5px;}\n.register-popup .btm-popup ul { display: block;}\n.register-popup .btm-popup ul li { display: inline-block;}\n.register-popup .btm-popup ul li a { color: #276bb1; font-size: 18px;}"

/***/ }),

/***/ "./src/app/authentication/register/register.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/authentication/register/register.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"top-header\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <a routerLink=\"/\" class=\"pull-left\"><img src=\"../../../assets/images/main-nav/Component 1@2x.png\"\n          alt=\"img\"></a>\n      <div class=\"top-header-rgt pull-right\">\n        <ul>\n          <li><a routerLink=\"/zenwork-login\" class=\"reg-log\">Login</a></li>\n          <li><a routerLink=\"/sign-up\" class=\"sign\">Sign Up</a></li>\n        </ul>\n      </div>\n      <div class=\"clearfix\"></div>\n\n    </div>\n  </div>\n</div>\n\n\n<div class=\"register\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"register-lft col-md-6\">\n\n        <h2>Register for a Demo</h2>\n        <small>Our team is happy to answer your questions and <br> take you on a product demo.</small>\n\n        <form [formGroup]=\"registerForm\" (ngSubmit)=\"registerSubmit()\">\n\n          <ul>\n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/user.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n                <input class=\"form-control\" placeholder=\"Name\" formControlName=\"name\">\n                <div class=\"clearfix\"></div>\n              </div>\n              <div *ngIf=\"registerForm.get('name').invalid && registerForm.get('name').touched\"\n                style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Name</div>\n            </li>\n\n\n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/mail.png\" width=\"13\" height=\"9\" alt=\"img\"></em>\n                <input class=\"form-control\" placeholder=\"Email Address\" formControlName=\"email\">\n                <div class=\"clearfix\"></div>\n              </div>\n              <div *ngIf=\"registerForm.get('email').invalid && registerForm.get('email').touched\"\n                style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Email</div>\n            </li>\n\n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/phone.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n                <input class=\"form-control\" placeholder=\"Phone\" formControlName=\"phone\">\n                <div class=\"clearfix\"></div>\n              </div>\n              <div *ngIf=\"registerForm.get('phone').invalid && registerForm.get('phone').touched\"\n                style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Phone</div>\n            </li>\n\n          </ul>\n\n          <button type=\"submit\" class=\"btn\" data-toggle=\"modal\" data-target=\"#myModal\">Get Started</button>\n\n        </form>\n\n        <h4><span>or Sign in with</span></h4>\n\n        <a href=\"#\" class=\"google\">\n          <p><img src=\"../../../assets/images/authentication/google.png\" width=\"18\" height=\"18\" alt=\"img\"> </p>Sign up\n          with Google\n        </a>\n\n      </div>\n\n      <div class=\"register-lft col-md-6\">\n        <figure><img src=\"../../../assets/images/authentication/register.png\" width=\"767\" height=\"753\" alt=\"img\">\n        </figure>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n\n<div class=\"register-popup\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n\n        <div class=\"modal-body\">\n         \n           <a href=\"#\"> <img src=\"../../../assets/images/main-nav/Component 1@2x.png\"  alt=\"img\"></a>\n            <h5>What time works best for you?</h5>\n            \n            <div class=\"time-zone\">\n                <small class=\"col-md-3\">Time Zone: </small>\n                <span class=\"col-md-5\">\n                  <mat-select class=\"form-control\" placeholder=\"Time zone\">\n                    <mat-option [value]=\"country\" *ngFor=\"let country of countrys\">{{country}}</mat-option>\n                  </mat-select>\n                </span>\n                <div class=\"clearfix\"></div>\n            </div>\n\n            <h5>Select Day That works for you</h5>\n            <div class=\"time-zone\">\n                \n                <span class=\"col-md-4\">\n                  <mat-select class=\"form-control\" placeholder=\"Years\">\n                    <mat-option [value]=\"year\" *ngFor=\"let year of years\">{{year}}</mat-option>\n                  </mat-select>\n                </span>\n\n                <span class=\"col-md-3\">\n                    <mat-select class=\"form-control\" placeholder=\"Date\">\n                      <mat-option [value]=\"date\" *ngFor=\"let date of dates\">{{date}}</mat-option>\n                    </mat-select>\n                  </span>\n\n                <div class=\"clearfix\"></div>\n            </div>\n\n            <h5>Select Time that works for you</h5>\n\n            <div class=\"time-zone\">\n                \n                <span class=\"col-md-3\">\n                  <mat-select class=\"form-control\" placeholder=\"Time\">\n                    <mat-option [value]=\"time\" *ngFor=\"let time of times\">{{time}}</mat-option>\n                  </mat-select>\n                </span>\n\n                <small class=\"col-md-4\" style=\"font-size:14px;\">(Demo Length-30 Minutes)</small>\n\n                <div class=\"clearfix\"></div>\n            </div>\n\n            <em>Your Scheduled Slot</em>\n            <p>20 January,2019 - Thursday -12:30PM</p>\n\n            <button class=\"btn\">Confirm your Slot</button>\n\n            <div class=\"btm-popup\">\n                <p>2019 &copy; Copyright | Powered by Zenwork, Inc.</p>\n                <ul>\n                  <li><a href=\"#\">Privacy Policy</a></li>\n                  <li> &nbsp; | &nbsp;</li>\n                  <li><a href=\"#\">Terms of Use</a></li>\n                </ul>\n            </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/authentication/register/register.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/authentication/register/register.component.ts ***!
  \***************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(registerService) {
        this.registerService = registerService;
        this.countrys = [
            "USA", "UK", "Canada", "India", "China"
        ];
        this.years = [
            "January", "February", "March", "April", "May", "June", "July", "August"
        ];
        this.dates = [
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
        ];
        this.times = [
            "1:00AM", "1:30AM", "2:00AM", "2:30AM", "3:00AM", "3:30AM", "4:00AM", "4:30AM", "5:00PM"
        ];
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.registerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    RegisterComponent.prototype.registerSubmit = function () {
        console.log("Register", this.registerForm.value);
        if (this.registerForm.valid) {
        }
        else {
            this.registerForm.get('name').markAsTouched();
            this.registerForm.get('email').markAsTouched();
            this.registerForm.get('phone').markAsTouched();
        }
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/authentication/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/authentication/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/authentication/reset-password/reset-password.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/authentication/reset-password/reset-password.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".reset-pass { text-align: center; height: 100%;}\n\n.reset-pass a img { margin: 0 auto 40px; display: block;}\n\n.reset-pass a img:focus, .reset-pass a:focus { outline: none; border: none;}\n\n.reset-pass h2 { font-size: 20px; font-weight: normal;margin: 0 0 20px; color: #7d7d7d;}\n\n.reset-pass small { font-size: 14px; padding: 0 0 20px;color: #7d7d7d; display: block;}\n\n.reset-pass .btn { border: none; background:#439348; color:#fff; border-radius:30px; padding: 9px 25px; font-size: 15px;\nwidth:50%; margin: 0 auto 15px; display: block;}\n\n.reset-pass .form-group { background-color:#f8f8f8; border-radius: 4px; margin: 0 auto;}\n\n.reset-pass em { float: left; width:38px; padding: 4px 0 10px 10px; border-right: #c7c7c7 1px solid; margin: 6px 0; height: 30px;}\n\n.reset-pass .form-control { border: none; box-shadow:none; height: auto; padding: 13px 12px; float:left; width:90%; background-color:transparent;\nfont-size: 15px;text-align: left;}\n\n.reset-pass .form-control:focus { border:none; box-shadow: none;}\n\n.top-block { display: block;width: 60%; margin: 0 auto 30px;}\n\n.reset-pass span { font-size: 14px; padding: 0 0 30px;color: #7d7d7d; display: block;}\n\n.reset-pass span a { color:#2b7aca; cursor: pointer;}\n\n.reset-copy {display: block;border-top: #c3c0c0 1px solid;\n    padding: 20px 0 0;\n    width:60%;\n    margin: 0 auto;\n}\n\n.reset-copy > p {font-size: 14px;color: #7d7d7d; margin: 0 0 5px;}\n\n.reset-copy > small { display: block; padding: 0;}\n\n.reset-copy > small a { display: inline-block;color:#2b7aca; cursor: pointer;}\n\n.tab { display: table; width: 100%; height: 100%;}\n\n.tab-cel { display: table-cell; width: 100%; height: 100%; vertical-align: middle;}\n\n.errors { color:#e44a49; padding: 5px 0 0; font-size: 14px; text-align: left;}"

/***/ }),

/***/ "./src/app/authentication/reset-password/reset-password.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/authentication/reset-password/reset-password.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"reset-pass\">\n   <div class=\"tab\">\n     <div class=\"tab-cel\">\n\n\n        <a href=\"#\"><img src=\"../../../assets/images/main-nav/side-logo.png\"></a>\n\n\n        <h2>Reset your Password</h2>\n        <small>Enter the Register email address to reset your password</small>\n      \n        <div class=\"top-block\">        \n        <div class=\"form-group\">\n          <em><img src=\"../../../assets/images/authentication/mail.png\" width=\"13\" height=\"9\" alt=\"img\"></em>\n          <input class=\"form-control\" placeholder=\"Email address\" pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\"\n           [(ngModel)]=\"email\" name=\"Email\" #Email=\"ngModel\" required>\n          <div class=\"clearfix\"></div>\n        </div>\n        <p *ngIf=\"!email && Email.touched || (!email && isValid)\" class=\"errors\">Enter valid email address</p>\n      </div>\n      \n        <button class=\"btn\" (click)=\"resetSubmit()\">Send Reset Link</button>\n        <span>Wait, I remember Take me back to <a (click)=\"onNoClick()\">Login!</a></span>\n      \n        <div class=\"reset-copy\">\n          <p>2019 © Copyright | Zenwork, Inc.</p>\n          <small><a href=\"#\"> Privacy Policy </a> | <a (click)=\"termsClick()\">Terms of Use</a></small>\n        </div>\n\n        \n\n     </div>\n   </div>\n  \n\n</div>"

/***/ }),

/***/ "./src/app/authentication/reset-password/reset-password.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/authentication/reset-password/reset-password.component.ts ***!
  \***************************************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(dialogRef, data, router, resetService, SwalService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.router = router;
        this.resetService = resetService;
        this.SwalService = SwalService;
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
    };
    // Author:Suresh M, Date:27-08-19
    // resetSubmit
    ResetPasswordComponent.prototype.resetSubmit = function () {
        var _this = this;
        var resetMail = {
            email: this.email
        };
        if (!this.email) {
            this.isValid = true;
        }
        else {
            this.resetService.resetPassword(resetMail)
                .subscribe(function (res) {
                console.log("reset linkk", res);
                if (res.status = true) {
                    _this.SwalService.SweetAlertWithoutConfirmation("Reset Password", "Reset password link is sent to your email", "success");
                }
                _this.dialogRef.close();
            }, (function (err) {
                _this.SwalService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            }));
        }
    };
    // Author:Suresh M, Date:27-08-19
    // onNoClick Close
    ResetPasswordComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    // Author:Suresh M, Date:27-08-19
    // termsClick
    ResetPasswordComponent.prototype.termsClick = function () {
        this.router.navigate(['/terms']);
        this.dialogRef.close();
    };
    ResetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reset-password',
            template: __webpack_require__(/*! ./reset-password.component.html */ "./src/app/authentication/reset-password/reset-password.component.html"),
            styles: [__webpack_require__(/*! ./reset-password.component.css */ "./src/app/authentication/reset-password/reset-password.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/authentication/self-sign-on-data/self-sign-on-data.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".signup .top-header-rgt > ul > li > a.regi:hover { background:#fec550; color:#000; border:transparent 1px solid;}\n.signup .top-header-rgt > ul > li > a{ color:#099247;}\n.signup .top-header-rgt > ul > li > a.log:hover{ background:#099247; color: #fff;}\n.signup .register-lft ul li { width:70%; position: relative;}\n.signup .register-lft .btn { color:#fff; padding: 10px 35px; border-radius: 30px;font-size: 15px; float: left; background: #099247; margin: 0 0 10px;}\n.signup .register-lft > p { color:#2b7aca; font-size: 13px; float: left; padding: 12px 0 0 15px; margin: 0;}\n.signup .register-lft > p > a { color:#e44a49;}\n.signup .register-lft small { color:#464545; font-size: 14px; line-height: 14px;border-top:#e0e0e0 1px solid; padding: 10px 0 8px; margin: 10px 0 0; display: inline-block;}\n.signup .register-lft small a { color:#2b7aca; font-size: 14px; line-height: 15px;}\n.signup .register-lft var {margin: 0 0 25px; display:block;}\n.signup .register-lft h4 { width: 80%; margin:20px 0 0;}\n.signup .register-lft a.google  { width: 70%; margin: 10px 0 0;}\n.signup .register-lft ul li b { position: absolute; top:10px; right:10px; cursor: pointer;}\n.signup .register-lft ul li strong { position: absolute; top:10px; right:10px; cursor: pointer;}\n.register-lft ul li{\n    margin: 0 auto 20px auto !important;\n    width: 50% !important\n}\n.logo{\n    margin: 0 auto;\n    width: 50% !important;\n    padding: 0 0 20px 0;\n}\n.register-lft .btn{\n    background: #3d9348;\n    color: #fff;\n}"

/***/ }),

/***/ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/authentication/self-sign-on-data/self-sign-on-data.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='logo text-center'>\n  <a class=\"\"><img src=\"../../../assets/images/main-nav/side-logo.png\" alt=\"img\"></a>\n</div>\n<div class=\"register-lft col-md-12\">\n  <div class=\"self-details\">\n\n    <form [formGroup]=\"signupForm\" (ngSubmit)=\"signupSubmit()\" class=\"text-center\">\n\n      <!-- <form  *ngIf = \"signupDisable\"  [formGroup]=\"signupForm\" (ngSubmit)=\"signupUpdate()\"> -->\n\n      <ul>\n        <li>\n          <div formGroupName=\"primaryContact\">\n            <div class=\"form-group\">\n              <em><img src=\"../../../assets/images/authentication/user.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n              <input class=\"form-control\" placeholder=\"Full Name\" maxlength=\"40\" formControlName=\"name\">\n              <div class=\"clearfix\"></div>\n            </div>\n            <div\n              *ngIf=\"signupForm.get('primaryContact').get('name').invalid && signupForm.get('primaryContact').get('name').touched\"\n              style=\"color:#e44a49; padding: 5px 0 0;\">Please enter full name</div>\n          </div>\n        </li>\n        <li>\n          <div class=\"form-group\">\n            <em><img src=\"../../../assets/images/authentication/domain.png\" width=\"16\" height=\"14\" alt=\"img\"></em>\n            <input type=\"text\" class=\"form-control\" maxlength=\"32\" placeholder=\"Company Name\" formControlName=\"name\">\n            <div class=\"clearfix\"></div>\n          </div>\n          <div *ngIf=\"signupForm.get('name').invalid && signupForm.get('name').touched\"\n            style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Company Name</div>\n        </li>\n\n        <li>\n          <div class=\"form-group\">\n            <em><img src=\"../../../assets/images/authentication/employee.png\" width=\"21\" height=\"13\" alt=\"img\"></em>\n            <input class=\"form-control\" placeholder=\"Employees\" maxlength=\"5\" (keypress)=\"keyPress($event)\"\n              formControlName=\"employeCount\" type=\"text\">\n            <div class=\"clearfix\"></div>\n          </div>\n          <div *ngIf=\"signupForm.get('employeCount').invalid && signupForm.get('employeCount').touched\"\n            style=\"color:#e44a49; padding: 5px 0 0;\">Please enter employee count</div>\n        </li>\n\n\n\n\n        <li>\n          <div formGroupName=\"primaryContact\">\n            <div class=\"form-group\">\n              <em><img src=\"../../../assets/images/authentication/phone.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n              <input class=\"form-control\" placeholder=\"Phone\" formControlName=\"phone\" (keypress)=\"keyPress($event)\"\n                minlength=10 maxlength=10>\n              <div class=\"clearfix\"></div>\n            </div>\n            <div\n              *ngIf=\"signupForm.get('primaryContact').get('phone').invalid && signupForm.get('primaryContact').get('phone').touched\"\n              style=\"color:#e44a49; padding: 5px 0 0;\">Please enter phone number</div>\n          </div>\n        </li>\n\n\n\n      </ul>\n\n      <button type=\"submit\" class=\"btn\">Get Started</button>\n\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/authentication/self-sign-on-data/self-sign-on-data.component.ts ***!
  \*********************************************************************************/
/*! exports provided: SelfSignOnDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfSignOnDataComponent", function() { return SelfSignOnDataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var SelfSignOnDataComponent = /** @class */ (function () {
    function SelfSignOnDataComponent(dialogRef, data, router, dialog, signupService, swalAlertService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.router = router;
        this.dialog = dialog;
        this.signupService = signupService;
        this.swalAlertService = swalAlertService;
        this.passwordType = "password";
    }
    SelfSignOnDataComponent.prototype.ngOnInit = function () {
        console.log(this.data);
        this.signupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            employeCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            primaryContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            }),
        });
        // this.getdata = JSON.parse(localStorage.getItem('addedCompany'))
        // console.log(this.getdata);
        // if (this.getdata) {
        //   console.log('dataim');
        //   this.signupForm.patchValue(this.getdata)
        //   this.signupDisable = true;
        // }
    };
    SelfSignOnDataComponent.prototype.signupSubmit = function () {
        var _this = this;
        console.log("Signup", this.signupForm.value);
        var finalObj = this.signupForm.value;
        // this.signupForm.patchValue({ signUpType: 'google' })
        // this.signupForm.patchValue({ googleId: this.data.googleId })
        // this.signupForm.patchValue({ email: this.data.email })
        finalObj['signUpType'] = 'google';
        finalObj['googleId'] = this.data.googleId;
        finalObj['email'] = this.data.email;
        console.log("Signup", finalObj);
        if (this.signupForm.valid) {
            // localStorage.setItem('addedCompany', JSON.stringify(this.signupForm.value));
            // this.router.navigate(['/self-on-boarding/client-details'])
            this.tempData = this.signupForm.valid;
            this.signupService.signup(finalObj)
                .subscribe(function (response) {
                console.log('Resposneee', response);
                if (response.status == true && response.data) {
                    localStorage.setItem('addedCompany', JSON.stringify(response.data));
                    _this.router.navigate(['/self-on-boarding/client-details']);
                }
                if (response.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Details added successfully", response.status, " ");
                    _this.dialogRef.close();
                    // Swal({
                    //   title: 'Details added successfully',
                    //   text: '',
                    //   showConfirmButton: false,
                    //   timer: 2500
                    // });
                }
                else {
                }
            }, function (err) {
                console.log(err);
                if (err.status == 409) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Email", err.error.message, "error");
                }
            });
        }
        else {
            this.signupForm.get('name').markAsTouched();
            this.signupForm.get('employeCount').markAsTouched();
            this.signupForm.get('primaryContact').get('name').markAsTouched();
            this.signupForm.get('email').markAsTouched();
            this.signupForm.get('primaryContact').get('phone').markAsTouched();
            // this.signupForm.get('password').markAsTouched();
        }
    };
    SelfSignOnDataComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    SelfSignOnDataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-self-sign-on-data',
            template: __webpack_require__(/*! ./self-sign-on-data.component.html */ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.html"),
            styles: [__webpack_require__(/*! ./self-sign-on-data.component.css */ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"]])
    ], SelfSignOnDataComponent);
    return SelfSignOnDataComponent;
}());



/***/ }),

/***/ "./src/app/authentication/self-signon-details/self-signon-details.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/authentication/self-signon-details/self-signon-details.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.banner { height:650px;}\n\n.company-modules { text-align: center; margin: 100px 0 0;}\n\n.company-modules h3 { color: #ffc550; font-size: 30px; line-height: 30px; margin: 0 0 10px; font-weight: 500;}\n\n.company-modules h4 { color: #658fa8; font-size: 25px; line-height: 25px; margin: 0 0 70px;}\n\n.company-modules ul{ display: block;}\n\n.company-modules ul li{ margin:0 0 40px; float: none; display: inline-block; vertical-align: top;}\n\n.company-modules ul li img { margin: 0 auto 20px;}\n\n.company-modules ul li h2{color: #5f5f5f; font-size: 22px; line-height:28px; margin: 0 0 20px; font-weight: 500; height: 56px;}\n\n.company-modules ul li p {color: #5f5f5f; font-size: 16px; line-height: 26px; margin: 0 0 30px; height: 125px;}\n\n.company-modules ul li a.btn {color: #5f5f5f; font-size: 15px; line-height: 14px; border:#ffc550 1px solid;\nborder-radius: 20px; padding:8px 15px;}\n\n.company-modules ul li a.btn:hover{ color:#ffc550; background:#5f5f5f; border:transparent 1px solid;}\n\n.team { position: relative; height: 1050px; text-align: center;}\n\n.team-lft { padding: 350px 0 0;}\n\n.team-lft h2 { color: #658fa8; font-size: 27px; line-height: 27px; margin: 0 0 30px;}\n\n.team-lft p {color: #5f5f5f; font-size: 17px; line-height:33px; margin: 0 0 50px;}\n\n.team-lft a.btn{ background: #ffc550;color: #313131;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 26px;\n    line-height: 16px;}\n\n.team-rgt     { padding: 0;position: absolute; top: 0; right: 0; width: 35%;}\n\n.team-rgt figure  { position: relative; }\n\n.team-rgt figure img { width: 100%; height: auto;}\n\n.team-rgt span { position: absolute; top:25%; right:30%; width:65%;}\n\n.why-zenwork { background:url('why-zenwork.jpg') no-repeat top center; width: 100%; height:750px; text-align: center;background-size: cover;}\n\n.why-zenwork .tbl-cell { padding: 50px 0 0;}\n\n.why-zenwork h3 { color: #658fa8; font-size: 27px; line-height: 27px; margin: 0 0 40px;}\n\n.why-zenwork ul{ display: block;}\n\n.why-zenwork ul li{ margin:0 0 40px;}\n\n.why-zenwork ul li img { margin: 0 auto 20px;}\n\n.why-zenwork ul li h2{color: #5f5f5f; font-size: 22px; line-height:28px; margin: 0 0 20px; font-weight: 500;}\n\n.why-zenwork ul li p {color: #5f5f5f; font-size: 17px; line-height:30px;}\n\n.why-zenwork a.btn {background: #ffc550;color: #313131;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 50px;\n    line-height: 16px;}\n\n.integrates {text-align: center; padding:100px 0;}\n\n.integrates h2 { color: #658fa8; font-size: 30px; line-height: 30px; margin: 0 0 80px;}\n\n.integrates ul { display:inline-block; width: 100%;}\n\n.integrates ul li { margin: 0 0 80px;}\n\n.integrates ul li a { margin: 0;}\n\n.integrates ul li a img { margin: 0 auto; position: relative; z-index: 99;}\n\n.zenworks-top { padding: 0 0 0 130px; position: relative;}\n\n.zenworks-top::after{ content: ''; position: absolute; top: -25%; left:210px; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXEAAAFxCAYAAACFh5ikAAAABHNCSVQICAgIfAhkiAAABUdJREFUeJzt1LENwjAAAEGYiTYFG7AqC9mWB4nEDKGJXrqb4Kt/PgDI2Xu/zvN8390BwEVrrc+c83t3BwAXGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQOEjTGOuxsA+M8PCuzwxLWQjuMAAAAASUVORK5CYII=) no-repeat top center; height: 369px; width: 369px;}\n\n.zenworks-btm { margin: 0 0 0 -150px; position: relative;}\n\n.zenworks-btm::after{ content: ''; position: absolute; top: -160px; right:220px; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXEAAAFxCAYAAACFh5ikAAAABHNCSVQICAgIfAhkiAAABUdJREFUeJzt1LENwjAAAEGYiTYFG7AqC9mWB4nEDKGJXrqb4Kt/PgDI2Xu/zvN8390BwEVrrc+c83t3BwAXGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQOEjTGOuxsA+M8PCuzwxLWQjuMAAAAASUVORK5CYII=) no-repeat top center; height: 369px; width: 369px;-webkit-transform: rotateY(30deg);transform: rotateY(30deg);}\n\n.agile-hr-rgt video{ width: 75%; height: 300px; border: none; margin: 0 0 25px;}\n\n/* .agile-section{\n    margin: 80px 0 0 0;\n} */"

/***/ }),

/***/ "./src/app/authentication/self-signon-details/self-signon-details.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/authentication/self-signon-details/self-signon-details.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"banner\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"banner-lft col-sm-7\">\n          <h2>What do we do?</h2>\n          <h1>Zenwork HR simplifies HR <br>\n            for the sophisticated business.</h1>\n          <small>Let Zenwork crunch the numbers while you focus on the big picture.</small>\n          <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n        </div>\n        <div class=\"banner-rgt pull-right\">\n          <figure><img src=\"../../../assets/images/home/banner-bg.png\" width=\"1057\" height=\"828\" alt=\"img\">\n            <span><img src=\"../../../assets/images/home/banner-img.png\" width=\"661\" height=\"782\" alt=\"img\"></span>\n          </figure>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n    </div>\n  </div>\n  \n  <!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\" (click)=\"arrowDown(agile)\"></i>\n  </div> -->\n  <section class=\"agile-section\">\n  <div class=\"arrow\"> <a (click)=\"arrowDown(agile)\"> <img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\" height=\"41\" alt=\"img\"\n        ></a></div>\n  \n  <div #agile class=\"agile-hr\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <h2>Agile HR Experience</h2>\n        <p>Zenwork HR is a cloud-based so ware that quickly resolves HR issues for your company. No more clunky HR <br>\n          systems on your computer that take forever to run. We integrate with other products throughout the Quickbooks\n          <br>\n          EcoSystem so that you can choose the solu ons that work best for your company.</p>\n  \n        <div class=\"agile-hr-in\">\n          <div class=\"agile-hr-lft pull-left\">\n            <figure><img src=\"../../../assets/images/home/agile-hr-bg.png\" width=\"800\" height=\"1099\" alt=\"img\">\n  \n              <span><img src=\"../../../assets/images/home/agile-hr-img.png\" width=\"459\" height=\"354\" alt=\"img\"></span>\n  \n            </figure>\n  \n          </div>\n          <div class=\"agile-hr-rgt col-sm-6 pull-right\">\n            <h3>If it works for you, it works for us!</h3>\n            <small>Customizable Workflows allow you to build any type of approval path that <br>\n              you need for HR functions. Assign any specific employee with approval <br>\n              authority so that you can easily include Finance, Administra on or <br>\n              Opera ons support without any do ed lines.</small>\n            <video poster=\"placeholder.png\" width=\"100%\" height=\"150\"\n              poster=\"../../../assets/images/home/video_thumbnail.png\" controls controlsList=\"nodownload\">\n              <source [src]='videoURL' type=\"video/mp4\">\n            </video>\n  \n            <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" (click)=\"registerTop()\" target=\"_blank\">Register\n              for a Demo</a>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  </section>\n  \n  <div class=\"company-modules\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <h3>Be the Star of the Show!</h3>\n        <h4>We’ll do the behind-the-scene work for you!</h4>\n  \n        <ul>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon11.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Benefits</h2>\n            <p>Build plans and use our Open Enrollment tools\n              to make benefits a breeze! Capture Employee\n              NPS feedback to learn what benefits maer\n              most to your team.</p>\n            <a class=\"btn\" routerLink=\"/benefits\" (click)=\"registerTop()\">Know More</a>\n          </li>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon21.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Leave Management</h2>\n            <p>Create Time-Off Policies to handle employee\n              accruals. Use the Work Scheduler and\n              TimeSheet tools to make sure your staff is in\n              place when you need them</p>\n            <a class=\"btn\" routerLink=\"/leave-management\" (click)=\"registerTop()\">Know More</a>\n          </li>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon31.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Recruitment</h2>\n            <p>Post Jobs to websites and store candidate data within\n              Zenwork HR. Use the Job Calculator to select the best\n              method in filling a posion.\n              Schedule interviews with\n              candidates and track the scores for future openings.</p>\n            <a class=\"btn\" routerLink=\"/recruitment\" (click)=\"registerTop()\">Know More</a>\n          </li>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon41.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Company Settings</h2>\n            <p>Make our system your system! Adjustable Theme sengs to\n              make the site look like your company. Site Access Rights &\n              Management allow you to determine what your employees\n              can do within the site. Mobile Access so that key data is\n              always at your fingerps.</p>\n            <a class=\"btn\" routerLink=\"/company-setting\" (click)=\"registerTop()\">Know More</a>\n          </li>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon51.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Employee Self Service & Manager Self Service</h2>\n            <p>Dashboards that have been modified for employees\n              and managers to easily guide them through roune\n              HR updates that save you me.</p>\n            <a class=\"btn\" routerLink=\"/em-service\" (click)=\"registerTop()\">Know More</a>\n          </li>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon61.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Powerful Security</h2>\n            <p>ZenworkHR protects your sensive\n              informaon under 256-bit encrypted security. This is the same security\n              level used by banks, the US gov, and even the military.</p>\n            <a class=\"btn\" routerLink=\"/security\" (click)=\"registerTop()\">Know More</a>\n          </li>\n          <li class=\"col-sm-4 col-xs-6\">\n            <img src=\"../../../assets/images/home/icon71.png\" width=\"74\" height=\"70\" alt=\"img\">\n            <h2>Integrations</h2>\n            <p>We play nice with others! Zenwork HR integrate with the tools in the Quick Books EcoSystem so that you can\n              use what works best for your company.</p>\n            <a class=\"btn\" routerLink=\"/api-integration\" (click)=\"registerTop()\">Know More</a>\n          </li>\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n    </div>\n  </div>\n  \n  \n  <div class=\"team\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"team-lft col-sm-7\">\n          <h2>You’ve got a friend with Zenwork!</h2>\n          <p>Our team is ready to support you! We offer phone, email and online chat support to help\n            you with any ques ons. We partner with you so that our system accommodates your\n            company needs and procedures, not the other way around. The workforce and HR are\n            constantly changing. Our expert staff will address ques ons with your perspec ve in mind.\n            We want to take care of your HR issues so that you spend less me trying to build reports\n            and more me focusing on your company’s goals for success.</p>\n          <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" (click)=\"registerTop()\" target=\"_blank\">Register for\n            a Demo</a>\n        </div>\n        <div class=\"team-rgt pull-right\">\n          <figure><img src=\"../../../assets/images/home/team-bg.png\" width=\"713\" height=\"1135\" alt=\"img\">\n  \n            <span><img src=\"../../../assets/images/home/team-img.png\" width=\"449\" height=\"529\" alt=\"img\"></span>\n          </figure>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n    </div>\n  </div>\n  \n  \n  \n  <div class=\"why-zenwork\">\n  \n    <div class=\"tbl\">\n      <div class=\"tbl-cell\">\n  \n        <div class=\"container\">\n          <div class=\"row\">\n  \n            <h3>Why Zenwork</h3>\n  \n            <ul>\n              <li class=\"col-md-3 col-sm-4 col-xs-12\">\n                <img src=\"../../../assets/images/home/zenwork1.png\" width=\"74\" height=\"70\" alt=\"img\">\n                <h2>Flexible</h2>\n                <p>Zenwork HR is cloud-based, which\n                  provides us with an agile producon\n                  environment to keep up with new HR\n                  requests so that our system\n                  keeps up with YOU!</p>\n  \n              </li>\n              <li class=\"col-md-3 col-sm-4 col-xs-12\">\n                <img src=\"../../../assets/images/home/zenwork21.png\" width=\"69\" height=\"65\" alt=\"img\">\n                <h2>User-Friendly</h2>\n                <p>We have built simplified workflows so\n                  that employees and managers will easily\n                  be able to make HR updates on their\n                  own that will save you me\n                  and resources.</p>\n  \n              </li>\n              <li class=\"col-md-3 col-sm-4 col-xs-12\">\n                <img src=\"../../../assets/images/home/zenwork31.png\" width=\"69\" height=\"65\" alt=\"img\">\n                <h2>Insightful</h2>\n                <p>We have built automated reports and use\n                  modern tools such as eNPS and the Job\n                  Calculator to provide you with the key data\n                  you need. We’ll do the legwork on the data\n                  so that you can focus on the decision that is\n                  best for the company.</p>\n  \n              </li>\n              <li class=\"col-md-3 col-sm-4 col-xs-12\">\n                <img src=\"../../../assets/images/home/zenwork41.png\" width=\"69\" height=\"65\" alt=\"img\">\n                <h2>Client Support</h2>\n                <p>We are available via phone,\n                  email and chat and want to\n                  help! We partner with you to\n                  make HR easier.</p>\n  \n              </li>\n  \n            </ul>\n            <div class=\"clearfix\"></div>\n  \n            <a href=\"#\" class=\"btn\">Get Started</a>\n  \n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  \n  \n  <div class=\"integrates\">\n    <div class=\"container\">\n      <div class=\"row\">\n  \n        <h2>Zenwork Integrates with</h2>\n  \n        <ul class=\"zenworks-top\">\n          <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n              <img src=\"../../../assets/images/home/logo11.png\"\n                onmouseover=\"this.src='../../../assets/images/home/logo1-hover1.png'\"\n                onmouseout=\"this.src='../../../assets/images/home/logo11.png'\" />\n            </a>\n          </li>\n  \n          <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n              <img src=\"../../../assets/images/home/logo21.png\"\n                onmouseover=\"this.src='../../../assets/images/home/logo2-hover1.png'\"\n                onmouseout=\"this.src='../../../assets/images/home/logo21.png'\" />\n            </a>\n          </li>\n  \n  \n          <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n              <img src=\"../../../assets/images/home/logo31.png\"\n                onmouseover=\"this.src='../../../assets/images/home/logo3-hover1.png'\"\n                onmouseout=\"this.src='../../../assets/images/home/logo31.png'\" />\n            </a>\n          </li>\n  \n        </ul>\n  \n  \n        <ul class=\"zenworks-btm\">\n  \n          <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n              <img src=\"../../../assets/images/home/logo61.png\"\n                onmouseover=\"this.src='../../../assets/images/home/logo6-hover1.png'\"\n                onmouseout=\"this.src='../../../assets/images/home/logo61.png'\" />\n            </a>\n          </li>\n  \n  \n          <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n              <img src=\"../../../assets/images/home/logo41.png\"\n                onmouseover=\"this.src='../../../assets/images/home/logo4-hover1.png'\"\n                onmouseout=\"this.src='../../../assets/images/home/logo41.png'\" />\n            </a>\n          </li>\n  \n  \n          <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n              <img src=\"../../../assets/images/home/logo51.png\"\n                onmouseover=\"this.src='../../../assets/images/home/logo5-hover11.png'\"\n                onmouseout=\"this.src='../../../assets/images/home/logo51.png'\" />\n            </a>\n          </li>\n  \n        </ul>\n  \n      </div>\n    </div>\n  </div>\n  \n  \n  \n  <div class=\"employees\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"employees-lft pull-left\">\n          <figure>\n            <div class=\"employee-bg\">\n              <img src=\"../../../assets/images/home/employee-bg.png\" width=\"806\" height=\"1160\" alt=\"img\">\n              <span><img src=\"../../../assets/images/home/employee-lady.png\" width=\"288\" height=\"270\" alt=\"img\"></span>\n              <div class=\"employee-cont\">\n                <p>Moving to Zenwork HR has made us wonder how <br>\n                  we survived before it happened, the product is <br>\n                  comprehensive, built with great a en on to <br>\n                  detail and is highly differen ated from most other <br>\n                  HRIS Products in the market.</p>\n                <div class=\"employee-right\">\n                  <h3>Business Analyst</h3>\n                  <em>Sarah-Crisp</em>\n                </div>\n              </div>\n            </div>\n          </figure>\n        </div>\n  \n        <div class=\"employees-rgt col-sm-7 pull-right\">\n          <h2>Employees love the Zenwork HR experience</h2>\n          <small>Enterprises across sectors use Zenwork HR to manage their Human Capital.</small>\n          <figure><img src=\"../../../assets/images/home/employee.jpg\" width=\"727\" height=\"621\" alt=\"img\"></figure>\n        </div>\n  \n        <div class=\"clearfix\"></div>\n  \n  \n  \n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/authentication/self-signon-details/self-signon-details.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/authentication/self-signon-details/self-signon-details.component.ts ***!
  \*************************************************************************************/
/*! exports provided: SelfSignonDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfSignonDetailsComponent", function() { return SelfSignonDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _self_sign_on_data_self_sign_on_data_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../self-sign-on-data/self-sign-on-data.component */ "./src/app/authentication/self-sign-on-data/self-sign-on-data.component.ts");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SelfSignonDetailsComponent = /** @class */ (function () {
    function SelfSignonDetailsComponent(router, dialog, route, accessLocalStorageService, messageService, ngZone) {
        var _this = this;
        this.router = router;
        this.dialog = dialog;
        this.route = route;
        this.accessLocalStorageService = accessLocalStorageService;
        this.messageService = messageService;
        this.ngZone = ngZone;
        router.events.subscribe(function (url) { return console.log(url); });
        console.log(router.url);
        this.url = router.url;
        var routeurl = this.router.url.split('?');
        console.log(routeurl[0]);
        console.log(routeurl[0] == 'self-signon-detail');
        this.route.queryParams.subscribe(function (params) {
            console.log(params);
            if (routeurl[0] == "/self-signon-detail") {
                _this.email = params['email'];
                _this.googleId = params['googleId'];
            }
        });
        if (routeurl[0] == "/social") {
            this.route.queryParams.subscribe(function (params) {
                console.log(params);
                _this.type = params.type;
                console.log(_this.type);
                _this.companyId = params.companyId;
                _this.companyId = JSON.parse(_this.companyId);
                _this.accessLocalStorageService.set('x-access-token', _this.companyId.token);
            });
        }
        // console.log(this.googleId, this.email);
        if (routeurl[0] == "/self-signon-detail") {
            this.openModal();
        }
        if (routeurl[0] == "/social") {
            if (this.type == 'company') {
                // JSON.parse(this.)
                console.log(this.companyId);
                this.ngZone.run(function () {
                    _this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                });
                this.accessLocalStorageService.set('type', this.type);
                this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                this.accessLocalStorageService.set('companyId', this.companyId._id);
                this.accessLocalStorageService.set('addedBy', this.companyId.userId);
            }
            // if(this.type == 'sr-support' || 'zenworker' ){     
            //   this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
            //   this.accessLocalStorageService.set('companyId', this.companyId._id);
            //   this.accessLocalStorageService.set('addedBy', this.companyId.userId);
            // }
            // if(this.type == 'administrator' ){
            // this.accessLocalStorageService.set('type', this.type)
            // this.accessLocalStorageService.set('resellercmpnyID',this.companyId.userId)
            // this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.companyId.userId])
            // this.messageService.sendMessage('reseller');
            // }
        }
    }
    SelfSignonDetailsComponent.prototype.openModal = function () {
        var dialogRef = this.dialog.open(_self_sign_on_data_self_sign_on_data_component__WEBPACK_IMPORTED_MODULE_3__["SelfSignOnDataComponent"], {
            width: '700px',
            backdropClass: 'structure-model',
            data: {
                email: this.email,
                googleId: this.googleId
            },
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
            // this.myModal.nativeElement.click();
            // jQuery("#myModal").modal('show');
        });
    };
    SelfSignonDetailsComponent.prototype.ngOnInit = function () {
        _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url;
        this.videoURL = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + '/videos/zenwork_demo.mp4';
    };
    SelfSignonDetailsComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    SelfSignonDetailsComponent.prototype.registerTop = function () {
        window.scroll(0, 0);
    };
    SelfSignonDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-self-signon-details',
            template: __webpack_require__(/*! ./self-signon-details.component.html */ "./src/app/authentication/self-signon-details/self-signon-details.component.html"),
            styles: [__webpack_require__(/*! ./self-signon-details.component.css */ "./src/app/authentication/self-signon-details/self-signon-details.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__["AccessLocalStorageService"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], SelfSignonDetailsComponent);
    return SelfSignonDetailsComponent;
}());



/***/ }),

/***/ "./src/app/authentication/sign-up/sign-up.component.css":
/*!**************************************************************!*\
  !*** ./src/app/authentication/sign-up/sign-up.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".signup .top-header-rgt > ul > li > a.regi { background:#fec550; color:#000; border:transparent 1px solid;}\n.signup .top-header-rgt > ul > li > a{ color:#099247;}\n.signup .top-header-rgt > ul > li > a.log:hover{ background:#099247; color: #fff;}\n.signup .register-lft ul li { width:80%; position: relative;}\n.signup .register-lft .btn { color:#fff; padding: 10px 35px; border-radius: 30px;font-size: 15px; float: left; background: #099247; margin: 0 0 10px;}\n.signup .register-lft > p { color:#2b7aca; font-size: 13px; float: left; padding: 12px 0 0 15px; margin: 0;}\n.signup .register-lft > p > a { color:#e44a49;}\n.signup .register-lft small { color:#464545; font-size: 14px; line-height: 14px;border-top:#e0e0e0 1px solid; padding: 10px 0 8px; margin: 10px 0 0; display: inline-block;}\n.signup .register-lft small a { color:#2b7aca; font-size: 14px; line-height: 15px;}\n.signup .register-lft var {margin: 0 0 25px; display:block;}\n.signup .register-lft var br { display: none;}\n.signup .register-lft h4 { width: 80%; margin:20px 0 0;}\n.signup .register-lft a.google  { width: 80%; margin: 10px 0 0;}\n.signup .register-lft ul li b { position: absolute; top:10px; right:10px; cursor: pointer;}\n.signup .register-lft ul li strong { position: absolute; top:10px; right:10px; cursor: pointer;}\n.get-btn { display: block;}\n.get-btn p{ padding: 10px 0 0 30px; display: inline-block;}"

/***/ }),

/***/ "./src/app/authentication/sign-up/sign-up.component.html":
/*!***************************************************************!*\
  !*** ./src/app/authentication/sign-up/sign-up.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"signup\">\n\n  <div class=\"top-header\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n      \n        <a routerLink=\"/\" class=\"col-xs-5 pull-right\"><img src=\"../../../assets/images/main-nav/Component 1@2x.png\"\n            alt=\"img\"></a>\n\n          <div class=\"top-header-rgt col-xs-7\">\n            <ul>\n              <li><a routerLink=\"/zenwork-login\" class=\"log\">Login</a></li>\n              <li><a routerLink=\"/register\" class=\"regi\">Register for a Demo</a></li>\n            </ul>\n          </div>\n\n        <div class=\"clearfix\"></div>\n\n      </div>\n    </div>\n  </div>\n\n\n\n  <div class=\"register\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <div class=\"register-lft col-sm-7\">\n          <figure><img src=\"../../../assets/images/authentication/signup.png\" width=\"630\" height=\"629\" alt=\"img\">\n          </figure>\n        </div>\n\n        <div class=\"register-lft col-sm-5\">\n\n          <form [formGroup]=\"signupForm\">\n\n            <!-- <form  *ngIf = \"signupDisable\"  [formGroup]=\"signupForm\" (ngSubmit)=\"signupUpdate()\"> -->\n\n            <ul>\n\n              <li>\n                <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/domain.png\" width=\"16\" height=\"14\" alt=\"img\"></em>\n                  <input type=\"text\" class=\"form-control\" maxlength=\"32\" placeholder=\"Company Name\"\n                    formControlName=\"name\">\n                  <div class=\"clearfix\"></div>\n                </div>\n                <div *ngIf=\"signupForm.get('name').invalid && signupForm.get('name').touched\"\n                  style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Name</div>\n              </li>\n\n              <li>\n                <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/employee.png\" width=\"21\" height=\"13\"\n                      alt=\"img\"></em>\n                  <input class=\"form-control\" placeholder=\"Employees\" maxlength=\"5\" (keypress)=\"keyPress($event)\"\n                    formControlName=\"employeCount\" type=\"text\">\n                  <div class=\"clearfix\"></div>\n                </div>\n                <div *ngIf=\"signupForm.get('employeCount').invalid && signupForm.get('employeCount').touched\"\n                  style=\"color:#e44a49; padding: 5px 0 0;\">Please enter employee count</div>\n              </li>\n\n              <li>\n                <div formGroupName=\"primaryContact\">\n                  <div class=\"form-group\">\n                    <em><img src=\"../../../assets/images/authentication/user.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n                    <input class=\"form-control\" placeholder=\"Full Name\" maxlength=\"24\" formControlName=\"name\">\n                    <div class=\"clearfix\"></div>\n                  </div>\n                  <div\n                    *ngIf=\"signupForm.get('primaryContact').get('name').invalid && signupForm.get('primaryContact').get('name').touched\"\n                    style=\"color:#e44a49; padding: 5px 0 0;\">Please enter full name</div>\n                </div>\n              </li>\n\n              <li>\n                <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/mail.png\" width=\"13\" height=\"9\" alt=\"img\"></em>\n                  <input class=\"form-control\" placeholder=\"Email\" formControlName=\"email\"\n                    pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\" [readonly]=\"signupDisable\">\n                  <div class=\"clearfix\"></div>\n                </div>\n                <div *ngIf=\"signupForm.get('email').invalid && signupForm.get('email').touched\"\n                  style=\"color:#e44a49; padding: 5px 0 0;\">Please enter email</div>\n                <div *ngIf=\"signupDisable\" style=\"color:#e44a49; padding: 5px 0 0;\">Email entered can't be replaced\n                </div>\n              </li>\n\n              <li>\n                <div formGroupName=\"primaryContact\">\n                  <div class=\"form-group\">\n                    <em><img src=\"../../../assets/images/authentication/phone.png\" width=\"13\" height=\"13\"\n                        alt=\"img\"></em>\n                    <input class=\"form-control\" placeholder=\"Phone\" formControlName=\"phone\"\n                      (keypress)=\"keyPress($event)\" minlength=10 maxlength=10>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                  <div\n                    *ngIf=\"signupForm.get('primaryContact').get('phone').invalid && signupForm.get('primaryContact').get('phone').touched\"\n                    style=\"color:#e44a49; padding: 5px 0 0;\">Please enter phone number</div>\n                </div>\n              </li>\n\n              <li>\n                <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/lock.png\" width=\"10\" height=\"13\" alt=\"img\"></em>\n                  <input class=\"form-control\" placeholder=\"Password\" formControlName=\"password\" [type]=\"passwordType\">\n                  <b (click)=\"getview()\" *ngIf=\"passwordType==='password'\"> <img\n                      src=\"../../../assets/images/authentication/not-view.png\" width=\"17\" height=\"9\" alt=\"img\"></b>\n                  <strong (click)=\"getview()\" *ngIf=\"passwordType==='text'\"> <img\n                      src=\"../../../assets/images/authentication/view.png\" width=\"17\" height=\"14\" alt=\"img\"></strong>\n                  <div class=\"clearfix\"></div>\n                </div>\n                <div *ngIf=\"signupForm.get('password').invalid && signupForm.get('password').touched\"\n                  style=\"color:#e44a49; padding: 5px 0 0;\">Please enter password</div>\n              </li>\n\n            </ul>\n\n            \n          </form>\n\n          <div class=\"get-btn\">\n              <button *ngIf=\"!signupDisable\" type=\"submit\" class=\"btn\" (click)=\"signupSubmit()\">Get Started</button>\n            <button *ngIf=\"signupDisable\" type=\"submit\" (click)=\"signupUpdate()\" class=\"btn\">Get Started</button>\n            <p>Already Have an Account? <a routerLink=\"/zenwork-login\">Login </a></p>\n\n          </div>\n          \n          <div class=\"clearfix\"></div>\n\n          <small>By clicking this button, you agree to zenwork.com's <a routerLink=\"/terms\" (click)=\"terms()\">Terms of\n              Use</a></small>\n          <var>\n            <mat-checkbox>Receive compliance updates and <br> marketing communications?</mat-checkbox>\n          </var>\n\n          <h4><span>or Sign up with</span></h4>\n\n          <a [href] = \"enviUrl\"  class=\"google\">\n            <p><img src=\"../../../assets/images/authentication/google.png\" width=\"18\" height=\"18\" alt=\"img\"> </p>Sign up\n            with Google\n          </a>\n\n        </div>\n\n\n        <div class=\"clearfix\"></div>\n      </div>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/authentication/sign-up/sign-up.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/authentication/sign-up/sign-up.component.ts ***!
  \*************************************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(signupService, swalAlertService, router) {
        this.signupService = signupService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.passwordType = "password";
        this.signupDisable = false;
    }
    SignUpComponent.prototype.ngOnInit = function () {
        this.enviUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + "/api/v1.0/users/login/google";
        this.signupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            employeCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            primaryContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
                phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            }),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
        this.getdata = JSON.parse(localStorage.getItem('addedCompany'));
        console.log(this.getdata);
        if (this.getdata) {
            console.log('dataim');
            this.signupForm.patchValue(this.getdata);
            this.signupDisable = true;
        }
    };
    SignUpComponent.prototype.getview = function () {
        if (this.passwordType === "password") {
            this.passwordType = "text";
        }
        else {
            this.passwordType = "password";
        }
    };
    SignUpComponent.prototype.signupSubmit = function () {
        var _this = this;
        console.log("Signup", this.signupForm.value);
        if (this.signupForm.valid) {
            localStorage.setItem('addedCompany', JSON.stringify(this.signupForm.value));
            // this.router.navigate(['/self-on-boarding/client-details'])
            this.tempData = this.signupForm.valid;
            this.signupService.signup(this.signupForm.value)
                .subscribe(function (response) {
                console.log('Resposneee', response);
                if (response.status == true && response.data) {
                    localStorage.setItem('addedCompany', JSON.stringify(response.data));
                    _this.router.navigate(['/self-on-boarding/client-details']);
                }
                if (response.status == true) {
                    // this.swalAlertService.SweetAlertWithoutConfirmation("Details added successfully", response.status, " ");
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                        title: 'Details added successfully',
                        text: '',
                        showConfirmButton: false,
                        timer: 2500
                    });
                }
                else {
                }
            }, function (err) {
                console.log(err);
                if (err.status == 409) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Email", err.error.message, "error");
                }
            });
        }
        else {
            this.signupForm.get('name').markAsTouched();
            this.signupForm.get('employeCount').markAsTouched();
            this.signupForm.get('primaryContact').get('name').markAsTouched();
            this.signupForm.get('email').markAsTouched();
            this.signupForm.get('primaryContact').get('phone').markAsTouched();
            this.signupForm.get('password').markAsTouched();
        }
    };
    SignUpComponent.prototype.signupUpdate = function () {
        var _this = this;
        console.log("update", this.getdata._id);
        // console.log(this.getdata.companyId);
        // this.signupForm.patchValue(this.getdata.companyId)
        // this.signupForm.value.patchValue(this.getdata._id)
        console.log(this.signupForm.value);
        var postdata = {
            'companyDetails': this.signupForm.value,
        };
        postdata['companyId'] = this.getdata._id;
        this.signupService.updateCompanyDetail(postdata)
            .subscribe(function (response) {
            console.log('Resposneee', response);
            if (response.status == true && response.data) {
                localStorage.removeItem('addedCompany');
                localStorage.setItem('addedCompany', JSON.stringify(response.data));
                _this.router.navigate(['/self-on-boarding/client-details']);
            }
            if (response.status == true) {
                // this.swalAlertService.SweetAlertWithoutConfirmation("Details added successfully", response.status, " ");
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    title: 'Details updated successfully',
                    text: '',
                    showConfirmButton: false,
                    timer: 2500
                });
            }
            else {
                _this.signupForm.get('name').markAsTouched();
                _this.signupForm.get('employeCount').markAsTouched();
                _this.signupForm.get('primaryContact').get('name').markAsTouched();
                _this.signupForm.get('email').markAsTouched();
                _this.signupForm.get('primaryContact').get('phone').markAsTouched();
                _this.signupForm.get('password').markAsTouched();
            }
        }, function (err) {
            console.log(err);
            if (err.status == 409) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Email", err.error.message, "error");
            }
        });
    };
    SignUpComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    SignUpComponent.prototype.terms = function () {
        window.scroll(0, 0);
    };
    SignUpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/authentication/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.css */ "./src/app/authentication/sign-up/sign-up.component.css")]
        }),
        __metadata("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SignUpComponent);
    return SignUpComponent;
}());



/***/ }),

/***/ "./src/app/authentication/terms/terms.component.css":
/*!**********************************************************!*\
  !*** ./src/app/authentication/terms/terms.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".top-header {\n    padding: 60px 0 !important;\n}\n\n.signup .top-header-rgt > ul > li > a.sign:hover { background:#099247; color:#fff;}\n\n.signup .top-header-rgt > ul > li > a.sign { color: #fff;}\n\n.signup .top-header-rgt > ul > li > a{ color:#099247;}\n\n.signup .top-header-rgt > ul > li > a.register:hover { background:#fec550; color:#000; border:transparent 1px solid;}\n\n.signup .top-header-rgt > ul > li > a.log:hover{ background:#099247; color: #fff;}\n\n.terms-border { border-bottom: #c5c5c5 1px solid; padding: 0 0 50px;}\n\n.terms-use { background: #f1f9f4; padding:30px 40px; border-radius: 5px;}\n\n.terms-use h2{color:#099247; font-size: 25px; line-height: 25px; padding: 0 0 10px; font-weight: bold;}\n\n.terms-use p{color:#5f5f5f; font-size: 17px; line-height: 25px;}\n\n.terms-cont { padding: 40px 0 0;}\n\n.terms-cont small{color:#5f5f5f; font-size: 16px; line-height: 25px; padding: 0 0 30px; display: block;}\n\n.terms-cont h3{color:#5f5f5f; font-size: 18px; line-height: 20px; padding: 0 0 10px;}\n\n.terms-cont em{color:#5f5f5f; font-size: 15px; line-height: 25px; font-style: normal; padding: 0 0 35px; display: block;}\n\n.terms-cont h4{color:#7a9caf; font-size: 18px; line-height: 20px;}\n\n.terms-cont h5{color:#7a9caf; font-size: 18px; line-height: 20px; padding: 0 0 20px;}\n\n.terms-cont ul { display: inline-block; width: 100%; padding: 0 0 50px;}\n\n.terms-cont ul li { margin: 0 0 20px;}\n\n.terms-cont ul li span {color:#5f5f5f; font-size: 15px; line-height:20px; position: relative; padding: 0 0 0 25px;}\n\n.terms-cont ul li span:after{ content: \"\"; position: absolute; top:5px; left: 0; background: #f0be29;\nwidth: 8px; height: 8px;border-radius:50%;}\n\n.terms-cont ul li span a{ display: inline-block; color: #2b7aca;}\n\n.terms-infor { padding:0;}\n\n.terms-infor ul {border-bottom: #e0e0e0 1px solid;}\n\n.terms-infor ul li { margin: 0 0 50px;}\n\n.terms-infor ul li h5 {color:#7a9caf; font-size: 18px; line-height: 20px; margin: 0 0 25px;}\n\n.terms-infor ul li p {color:#5f5f5f; font-size: 15px; line-height:27px; margin: 0;}\n\n.terms-infor ul li span {color:#5f5f5f; font-size: 15px; line-height:27px;text-transform:uppercase; padding:20px 0; display: block;}\n\n.terms-infor ul li p a { display: inline-block; color: #2b7aca;}\n\n.terms-infor .btn {background:#099247; color:#fff; border-radius: 20px; padding: 9px 30px; font-size: 16px; margin: 50px 0 0;}\n"

/***/ }),

/***/ "./src/app/authentication/terms/terms.component.html":
/*!***********************************************************!*\
  !*** ./src/app/authentication/terms/terms.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"signup\">\n\n  <div class=\"top-header\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"terms-border\">\n\n          <a routerLink=\"/\" class=\"col-md-4\"><img src=\"../../../assets/images/main-nav/Component 1@2x.png\"\n              alt=\"img\"></a>\n\n          <div class=\"top-header-rgt col-md-6 pull-right\">\n            <ul>\n              <li><a routerLink=\"/zenwork-login\" class=\"log\">Login</a></li>\n              <li><a routerLink=\"/register\" class=\"register\">Register for a Demo</a></li>\n              <li><a routerLink=\"/sign-up\" class=\"sign\">Sign Up</a></li>\n            </ul>\n          </div>\n\n\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"terms-use\">\n        <h2>Terms of Use</h2>\n        <p>By visiting and/or using Zenwork's HRIS platform, you fully and unconditionally agree to these Terms of Use.\n          <br> if you do not agree to these Terms of Use, please do not visit or use the website.</p>\n      </div>\n    </div>\n  </div>\n\n\n\n  <div class=\"terms-cont\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <small>Any offer provided by us is conditioned upon your acceptance of this Terms of Use Agreement. By using\n          this site, you represent that you lawfully consent to U.S. jurisdiction for all purposes on our site as if a\n          resident of the U.S, and have the right, authority and capacity to entire into these Terms of Use\n          Agreement</small>\n\n        <h3>Registration Online or Over the Phone</h3>\n        <em>When and if you set up an account and/or use the Services on the Website, you agree to: (a) provide\n          accurate, current, and complete information; (b) maintain and keep your information accurate, current, and\n          complete; (c) not impersonate any person or entity, and (d) you will not select a user name that in our sole\n          discretion deems offensive. We reserve the right to terminate your access to and use of the Website and\n          Services if any information provided by you is untrue, inaccurate, not current, or incomplete. Our use and\n          disclosure of any information you provide us is governed by our privacy policy. All activity conducted in\n          connection with your account will be your responsibility.</em>\n\n\n        <h4>Your Conduct</h4>\n        <h5>By Visiting and using this Zenwork HRIS site you agree not to:</h5>\n\n        <ul>\n          <li>\n            <span>Defame, abuse, harass, stalk, threaten, or otherwise violate the rights of others, including without\n              limitation others’ privacy rights or rights of publicity.</span>\n          </li>\n          <li>\n            <span>Falsely state or otherwise misrepresent your affiliation with any person or entity, or use any\n              fraudulent, misleading, or inaccurate email address or other contact information.</span>\n          </li>\n          <li>\n            <span>Restrict or inhibit other users from using the Services.</span>\n          </li>\n          <li>\n            <span>Violate any applicable laws, rules, or regulations.</span>\n          </li>\n          <li>\n            <span>Express or imply that any statements you make are endorsed by us.</span>\n          </li>\n          <li>\n            <span>Engage in spamming or flooding.</span>\n          </li>\n          <li>\n            <span>Access or use (or attempt to access or use) another user’s content without permission.</span>\n          </li>\n          <li>\n            <span>Modify, adapt, sublicense, translate, sell, reverse engineer, decompile, or disassemble any portion of\n              the Services or the Website.</span>\n          </li>\n          <li>\n            <span>Remove any copyright, trademark, or other proprietary rights notices contained in or displayed on any\n              portion of the Website.</span>\n          </li>\n          <li>\n            <span>\"Frame\" or \"mirror\" any portion of the Website, or link to any page from the Website without our prior\n              written authorization.</span>\n          </li>\n          <li>\n            <span>Order or purchase Services through the Website if you are not 18 years of age or older, or have the\n              specific permission of a parent or legal guardian.</span>\n          </li>\n\n        </ul>\n\n      </div>\n    </div>\n  </div>\n\n\n  <div class=\"terms-infor\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <ul class=\"no-bor\">\n\n          <li>\n            <h5>Sharing Information</h5>\n            <p>By using the Zenwork HRIS website, you agree not to upload, post, email or otherwise send or transmit any\n              material that: (1) contains viruses, worms, Trojan horses, defects, date bombs, time bombs or any other\n              computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer\n              software or hardware or telecommunications equipment associated with the Website; (2) is abusive, illegal,\n              libelous, defamatory, obscene, threatening, invasive of privacy or publicity rights or otherwise\n              objectionable or which may give rise to liability or violate any law; (3) is in violation of a copyright,\n              trademark or other intellectual property or other right of any person; (4) requests personally\n              identifiable information; or (5) contains any advertisement, solicitations, chain letters, pyramid\n              schemes, investment opportunities or other unsolicited commercial communication.</p>\n          </li>\n\n          <li>\n            <h5>Your Submissions</h5>\n            <p>If you choose to upload or otherwise submit any material through the Zenwork HRIS website, including\n              without limitation, video photographs, images, text, graphics, sounds, data, or files to the Website, by\n              uploading such video or photographs you represent and warrant that: (1) you own or otherwise possess all\n              necessary rights with respect to your Submissions; (2) your Submissions do not and will not infringe,\n              misappropriate, use or disclose without authorization, or otherwise violate any intellectual property or\n              proprietary rights of any third party; and (3) you hereby consent to the use of your likeness, and you\n              acknowledge you have obtained the written consent, release, and/or permission of every identifiable\n              individual who appears in a Submission to use such individual’s likeness, or if any such identifiable\n              individual is under the age of eighteen (18), you have obtained such written consent, release and/or\n              permission from such individual’s parent or guardian (and you agree to provide to us with a copy of any\n              such consents, releases and/or permission upon our request). We may request that you submit evidence of\n              your ownership of or right to use your Submissions. If, upon such request, we do not receive information\n              we deem sufficient to evidence such rights, we reserve the right to: (i) suspend the shipping of an order\n              or service relating to such content, and/or (ii) share the content and information regarding the member\n              with governmental organizations, law enforcement authorities or other third parties.</p>\n          </li>\n\n          <li>\n            <h5>Age Limitations</h5>\n            <p>You must be 18 to order or purchase Services from this site unless you have the specific permission from\n              a parent or legal guardian. If you use any chat room or other community page on the Website, you must be\n              at least thirteen (13) years old. We recommend that minors over age 13 (that is, between ages 13 and 18)\n              obtain their parent’s or guardian’s permission before sending information about themselves to anyone\n              online. Click here for tips on staying safe online. If you are a convicted sexual predator, you may not\n              use any such Website feature (chat rooms or community pages).</p>\n          </li>\n\n          <li>\n            <h5>Obligations & Responsibilities</h5>\n            <p>You acknowledge that, by providing you with the ability to view and distribute user-generated content on\n              the Website, we are acting only as a passive conduit for such distribution, and we are not undertaking any\n              obligation or liability relating to any such content. We do not and cannot review all communications and\n              materials posted to the Website, and we are not responsible for the content of such communications and\n              materials. All such content is offered AS IS, and you view and use it at your own risk. You acknowledge\n              and agree that we may (but are not obligated to) do any of the following, at our discretion: (1) monitor\n              and/or filter any Submissions; (2) remove or refuse to send, transmit, or otherwise use any Submission\n              (including without limitation, by suspending the processing and shipping of any order related to any\n              Submission); and/or (3) disclose any Submission, and the circumstances surrounding the transmission or use\n              thereof to any third party. If you become aware of misuse of the Website by any person, please contact us\n              at info@zenwork.com. Because we do not control the security of the Internet or other networks you use to\n              access the Website or communicate with us, we can’t be, and are not responsible for, the security of\n              information that you choose to communicate with us and the Website while it is being transmitted. In\n              addition, we are not responsible for any data lost during transmission.</p>\n          </li>\n\n          <li>\n            <h5>Our use of Information Submitted</h5>\n            <p>You agree that we are free to use any comments, information, reviews, feedback, postings, materials,\n              photographs, artwork, ideas, or any other content contained in any communication you may send to us,\n              without notice, compensation or acknowledgement to you, for any purpose whatsoever, including but not\n              limited to developing, manufacturing and marketing products and services and creating, modifying or\n              improving the Website or other products or services.\n              You agree that we are free to use the company name and logo associated with your email address domain for\n              any purpose whatsoever, including on our Zenwork HRIS and Zenwork websites and social platforms, without\n              notice or compensation, unless you inform us in writing at social@zenwork.com that you deny permission for\n              us to do so.</p>\n          </li>\n\n          <li>\n            <h5>Compliance</h5>\n            <p>You agree to comply with all applicable laws, statutes and regulations regarding your use of this Site\n              and your purchase of products or services (if applicable) through our Zenwork HRIS website. We may, in our\n              sole discretion report actual or perceived violations of law to law enforcement or appropriate\n              authorities. If we become aware of any potential violation of the Terms of Use or our Privacy Policy, we\n              may (but are not obligated to) conduct an investigation to determine the appropriate enforcement action,\n              during which we may suspend services or terminate the account of any customer being investigated.</p>\n          </li>\n\n\n          <li>\n            <h5>Termination</h5>\n            <p>We reserve the right in our sole discretion to terminate or restrict your use of the Website or the\n              Services, without notice, for any or no reason, and without liability to you or any third party. You agree\n              that we shall not be liable to you or any third party for any termination of your access to the Website or\n              the Services. At our sole discretion, we may modify or remove any User Content that violates or is\n              inconsistent with these Terms of Use or their intent, that your conduct is disruptive, or you have\n              violated the law, these Terms of Use, or the rights of us or another user. We will have no liability to\n              you for any deletion of your User Content. We may also change, suspend, or discontinue any aspect of the\n              Website or the Services at any time, including the availability of any feature or content without prior\n              notice. Any updates to the Site will be subject to these Terms of Use.</p>\n          </li>\n\n          <li>\n            <h5>Trademarks</h5>\n            <p>All trademarks, service marks and trade names of ours used on the Website are trademarks or registered\n              trademarks of us in the U.S. and/or other countries. They may not be used without our prior express\n              written permission. All other trademarks or logo that appear on the Website are the property of their\n              respective owners, who may or may not be affiliated with, connected to or endorsed by us.</p>\n          </li>\n\n          <li>\n            <h5>Copyright</h5>\n            <p>The entire content of the Website, including but not limited to text, graphics and code, is our property.\n              We grant you permission to electronically copy and print hard copy portions of the Website solely for your\n              own personal, non-commercial use, provided that you do not change or delete any proprietary notices from\n              downloaded or printed materials. Any other use, including but not limited to the reproduction,\n              distribution, display or transmission of the Website content is strictly prohibited, unless authorized by\n              us in writing.</p>\n          </li>\n\n        </ul>\n\n\n\n      </div>\n    </div>\n  </div>\n\n\n  <div class=\"terms-cont\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <h4>Copyright Violations</h4>\n        <em>We respect the intellectual property rights of others and will take appropriate steps to protect the\n          intellectual property rights of third parties if it receives notice in accordance with the Digital Millennium\n          Copyright Act. If you believe your copyright or the copyright of another has been infringed on, please provide\n          our Copyright Agent with a written notice containing the following information:</em>\n\n        <ul>\n          <li>\n            <span>a physical or electronic signature of the person authorized to act on behalf of the owner of the\n              copyright interest that you believe has been infringed;</span>\n          </li>\n          <li>\n            <span>an identification of the copyrighted work that you claim has been infringed;</span>\n          </li>\n          <li>\n            <span>a description of where the material that you claim is infringed is located on the site;</span>\n          </li>\n          <li>\n            <span>information on how to contact you including your address, telephone number, and email address, if\n              available;</span>\n          </li>\n          <li>\n            <span>a statement by you that you have a good faith belief that the disputed use is not authorized by the\n              copyright owner, its agent, or the law; and</span>\n          </li>\n          <li>\n            <span>a statement by you, made under penalty of perjury, that the above information in your notice is\n              accurate and that you are the copyright owner or authorized to act on the copyright owner’s behalf.</span>\n          </li>\n          <li>\n            <span>Claims of infringement should be sent to: <a href=\"mailto:info@zenwork.com\">\n                info@zenwork.com.</a></span>\n          </li>\n\n        </ul>\n\n      </div>\n    </div>\n  </div>\n\n\n  <div class=\"terms-infor\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <ul>\n\n          <li>\n            <h5>Warranty Disclaimer & Liability Limit</h5>\n            <p>To the fullest extent permissible under applicable law, we present the Website and the Services “AS IS”\n              and without any warranty of any kind, whether express, implied, or statutory. We specifically disclaim any\n              implied warranties of title, merchantability, fitness for a particular purpose, and non- infringement. We\n              do not warrant that the functions contained in the Website will be uninterrupted or error-free, that\n              defects will be corrected or that this Website or the server that makes this website available are free of\n              viruses or other harmful components. In the event of any breach of any warranty, your exclusive remedy\n              shall be that we shall, at our option, repair, replace, or refund the price you paid for any defective\n              goods. We assume no liability or responsibility for any errors or omissions on the Website; any failures,\n              delays or interruptions in the Website's accessibility; any losses or damages arising from the use of the\n              Website; or any conduct by other users of the Website. We reserve the right to deliver the Website in our\n              sole and absolute discretion. Some jurisdictions do not allow the disclaimer of implied warranties, so the\n              foregoing may not apply to you.</p>\n\n            <span>IN NO EVENT SHALL WE, OUR AFFILIATES, SUBSIDIARIES, SHAREHOLDERS, DIRECTORS, OFFICERS, EMPLOYEES OR\n              AGENTS BE LIABLE (JOINTLY OR SEVERALLY) TO YOU FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL\n              DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE WEBSITE OR THESE TERMS, ON ANY THEORY OF LIABILITY, AND\n              WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE ARISING OUT OF OR RELATING IN ANY WAY TO THE SERVICES\n              (INCLUDING, BUT NOT LIMITED TO, ALL MATERIALS INCORPORATED THEREIN AND ALL FEATURES AND FUNCTIONALITY OF\n              THE SERVICE) AND TOTAL LIABILITY FOR DAMAGES UNDER THIS AGREEMENT SHALL BE LIMITED TO THE TOTAL AMOUNT\n              PAID FOR THE PRODUCT.</span>\n            <p>Some states do not allow the exclusion of certain damages, so the above may not apply to you. If any\n              authority holds any portion of this section to be unenforceable, then liability will be limited to the\n              fullest possible extent permitted by law.</p>\n          </li>\n\n          <li>\n            <h5>Indemnification</h5>\n            <p>You agree to indemnify, defend and hold us, our shareholders, officers, directors, employees and\n              agents harmless from and against any third-party claim or cause of action, including reasonable attorneys’\n              fees and court costs, arising, directly or indirectly, in whole or in part, out of your use of the Website\n              or your violation of these Terms of Use, any law or the rights of any third party.</p>\n          </li>\n\n          <li>\n            <h5>Electronic Notices</h5>\n            <p>By using the Website, you agree to receive electronic communications from us. You agree that any\n              notice, agreement, disclosure or other communication that we send you electronically will satisfy any\n              legal communication requirements, including that such communications be in writing.</p>\n          </li>\n\n          <li>\n            <h5>Third-Party Links</h5>\n            <p>The Website may link to sites operated by third parties. However, we have no control over these linked\n              sites, all of which have their own terms of use and data collection practices. These linked sites are only\n              for your convenience, and you access them at your own risk.</p>\n          </li>\n\n          <li>\n            <h5>Disputes</h5>\n            <p>Your use of the Website shall be governed by the laws of Arkansas, without regard to choice of law\n              provisions. Except where prohibited, you agree that any and all disputes, claims and causes of action\n              directly or indirectly arising out of or relating to the Website shall be resolved individually, without\n              resort to any form of class action, and exclusively in the state or federal courts located in Washington\n              County, Arkansas. Any cause of action or claim you may have with respect to the Website must be commenced\n              within one (1) year after the claim or cause of action arises, or it shall be forever barred.</p>\n          </li>\n\n          <li>\n            <h5>General</h5>\n            <p>The Services are controlled and operated within the United States, and are not intended to be subject to\n              the laws or jurisdiction of any country or territory other than that of the United States. We do not\n              represent or warrant that the Services or any part thereof are appropriate or available for use in any\n              particular jurisdiction. Those who choose to access the Services do so on their own initiative and at\n              their own risk, and are responsible for complying with all local laws, rules, and regulations. We may\n              limit the Services availability, in whole or in part, to any person, geographic area or jurisdiction we\n              choose, at any time and in our sole discretion. If any provision of these Terms of Use is held to be\n              invalid or unenforceable, such provision shall be struck, and the remaining provisions shall be enforced.\n              Headings are for reference purposes only and in no way define, limit, construe or describe the extent or\n              scope of such provision. Our failure to enforce any provision of these Terms of Use shall not constitute a\n              waiver of that or any other provision. These Terms of Use set forth the entire understanding and agreement\n              between you and us with respect to the subject matter hereof.</p>\n          </li>\n\n          <li>\n            <h5>Contact Us</h5>\n            <p>If you have any questions about these Terms of Use, please contact us – <a href=\"mailto:info@zenwork.com\">info@zenwork.com</a>.</p>\n          </li>\n\n\n        </ul>\n\n        <button class=\"btn\" routerLink=\"/about\" (click)=\"about()\">Get Started</button>\n\n      </div>\n    </div>\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/authentication/terms/terms.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/authentication/terms/terms.component.ts ***!
  \*********************************************************/
/*! exports provided: TermsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsComponent", function() { return TermsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TermsComponent = /** @class */ (function () {
    function TermsComponent() {
    }
    TermsComponent.prototype.ngOnInit = function () {
    };
    TermsComponent.prototype.about = function () {
        window.scroll(0, 0);
    };
    TermsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-terms',
            template: __webpack_require__(/*! ./terms.component.html */ "./src/app/authentication/terms/terms.component.html"),
            styles: [__webpack_require__(/*! ./terms.component.css */ "./src/app/authentication/terms/terms.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TermsComponent);
    return TermsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=authentication-authentication-module.js.map