(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notes-notes-module"],{

/***/ "./src/app/admin-dashboard/employee-management/notes/notes.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/notes/notes.module.ts ***!
  \***************************************************************************/
/*! exports provided: NotesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesModule", function() { return NotesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _notes_notes_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notes/notes.component */ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.ts");
/* harmony import */ var _notes_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notes.routing */ "./src/app/admin-dashboard/employee-management/notes/notes.routing.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var NotesModule = /** @class */ (function () {
    function NotesModule() {
    }
    NotesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _notes_routing__WEBPACK_IMPORTED_MODULE_3__["NotesRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"]
            ],
            declarations: [_notes_notes_component__WEBPACK_IMPORTED_MODULE_2__["NotesComponent"]],
            providers: [_services_my_info_service__WEBPACK_IMPORTED_MODULE_6__["MyInfoService"]]
        })
    ], NotesModule);
    return NotesModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/notes/notes.routing.ts":
/*!****************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/notes/notes.routing.ts ***!
  \****************************************************************************/
/*! exports provided: NotesRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesRouting", function() { return NotesRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _notes_notes_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notes/notes.component */ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/notes",pathMatch:"full" },
    { path: '', component: _notes_notes_component__WEBPACK_IMPORTED_MODULE_2__["NotesComponent"] }
];
var NotesRouting = /** @class */ (function () {
    function NotesRouting() {
    }
    NotesRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NotesRouting);
    return NotesRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/notes/notes/notes.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-compensation-tab{\n    padding: 0 35px 0 0;\n}\n.personal-head{\n    margin: 10px;\n}\n.main-head .list-items {\n    display: inline-block;\n    padding: 0px 5px;\n    font-size: 16px;\n}\n.zenwork-padding-20-zero{\n    padding:10px 0px!important;\n}\n.img-personal-icon{\n    width: 20px;\n}\n.add-notes{\n    padding: 30px 0 0 15px;\n}\n.add-notes .cancel { background: transparent; border: none; color:#6b6f71;}\n.padding-left-15{\n    padding: 0 0 0 15px;\n}\n.notes .list-items span{\n    font-size: 14px;\n    padding: 0 0 0 5px;\n}\n.img-plus-icon{\n    width: 20px;\n}\n.notes .list-items{\n    display: inline-block;\n    padding: 0px 10px 0 0;\n}\n.archive-btn{\n    padding: 7px 13px;\n    background: #fff;\n    border: none;\n    font-size: 16px;\n    border: 1px solid #c6d4de;\n    color: #544f4f;\n\n}\n.submit-buttons .btn-success {\n    background-color: #439348;\n}\n.submit-buttons  .list-item{\n    display: inline-block;\n    padding: 0px 25px 0 0;\n\n}\n.btn-style{\n    padding: 6px 16px!important;\n    border-radius: 30px!important;\n    font-size: 14px;\n}\n.text-area{\n    padding: 0 10px;\n}\n.employee-notes{\n    margin: 10px;\n}\ntextarea{\n    resize: none;\n    border: 0;\n    box-shadow: none;\n    font-size: 14px;\n }\n.emp-notes-head{\n     background: #edf7fe;\n     padding: 10px;\n }\n.notes-heading{\n     font-size: 14px;\n }\n.archive-text{\n     padding: 0 15px 0 0;\n }\n.archive-text small{\n     padding: 0 0 0 5px;\n     font-size: 14px;\n }\n.note-body{\n     background: #fff;\n     padding: 15px;\n }\n.note-body small{\n     color:#edf7fe;\n }\n.employee-img{\n    width: 33px;\n    margin: 0 10px 0 0;\n }\n.emp-details{\n    width: 44%;\n    display: inline-block;\n    vertical-align:top; padding: 10px 0 0 10px;\n }\n.emp-details-timings::before{\n    content: \"\";\n    position: absolute;\n    border-left: 1px solid #eee;\n    top: 8px;\n    left: -4px;\n    height: 60%\n }\n.emp-details-timings{\n    display: inline-block;\n    position: relative;\n    padding: 0 0 0 10px;\n\n }\n.emp-details span{\n    font-size: 14px;\n    display: block;\n }\n.emp-details-timings span{\n     font-size: 14px;\n }\n.emp-details small,.emp-details-timings small{\n    color: #3e3e3ea3;\n    font-size: 12px;\n }\n.emp-main-details{\n    padding: 10px;\n    background: #fff;\n }\n.icons-list .list-items{\n     padding: 0px 10px;\n     display: inline-block;\n }\n.emp-description p {\n     font-size: 14px;\n    margin: 0 0 0 45px;\n }\n.zenwork-compensation-tab { float: none; margin: 0 auto; padding: 0;}\n.rounded-0:focus{\n   box-shadow: none;\n }\n.emplployee-icon{\n    font-size: 30px;\n    color: #ccc;\n }\n.next-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 20px;\n    color: #fff;\n    padding: 0 30px;    \n}\n.zenwork-margin-ten-zero {\n    margin: 20px 0px!important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/notes/notes/notes.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-compensation-tab col-md-11\">\n  <div class=\"personal-head\">\n    <ul class=\"list-unstyled main-head\">\n      <li class=\"list-items\">\n        <img src=\"../../../assets/images/directory_tabs/Notes/notes.png\" class=\"img-personal-icon\" alt=\"money-icon\">\n      </li>\n      <li class=\"list-items\">\n        <span>Notes</span>\n      </li>\n    </ul>\n  </div>\n  <hr class=\"zenwork-margin-ten-zero \">\n  <div class=\"add-notes\">\n    <ul  *ngIf=\"!notesTabView\" class=\"list-unstyled notes pull-left\" style=\"margin-bottom:20px;\">\n      <li class=\"list-items\">\n        <img src=\"../../../assets/images/directory_tabs/Notes/note_plus.png\" class=\"img-plus-icon\">\n        <span>Add Notes</span>\n      </li>\n      <li class=\"list-items\">\n        <button class=\"btn archive-btn\" (click)=\"viewArchivedNotes()\" [disabled]=\"archiveDisabled\">view archived notes</button>\n      </li>\n    </ul>\n    <!-- <ul class=\"list-unstyled submit-buttons pull-right\">\n      <li class=\"list-item\">\n        <button class=\"btn btn-success btn-style\" (click)=\"saveNotes()\"> Save Changes</button>\n      </li>\n      <li class=\"list-item\">\n        <button class=\"btn btn-style\"> cancel </button>\n      </li>\n    </ul> -->\n    <div class=\"clearfix\"></div>\n  </div>\n  <div  *ngIf=\"!notesTabView\" class=\"text-area form-group\">\n    <textarea class=\"form-control rounded-0\" rows=\"5\" placeholder=\"Add a note...\" [(ngModel)]=\"employeeNotes.description\"></textarea>\n  </div>\n\n  <div *ngIf=\"!notesTabView\" class=\"add-notes\">\n    <ul class=\"list-unstyled submit-buttons pull-right\">\n      <li class=\"list-item\">\n        <button class=\"btn btn-success btn-style\" (click)=\"saveNotes()\"> {{noteId ? 'Update' : 'Save Changes'}}</button>\n      </li>\n      <li class=\"list-item\">\n        <button class=\"btn cancel\" (click)=\"cancelNotes()\"> Cancel </button>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n  </div>\n  <hr class=\"zenwork-margin-ten-zero\">\n  <!-- <div class=\"employee-notes\" *ngFor=\"let note of allNotes\">\n    <div class=\"emp-notes-head\">\n      <span class=\"notes-heading pull-left\">Employee Notes</span>\n      <span class=\"archive-text pull-right\">\n        <img src=\"../../../assets/images/directory_tabs/Notes/archive.png\" class=\"archive-icon\" alt=\"arcchiveicon\">\n        <small>Add to archive</small>\n      </span>\n      <div class=\"clearfix\"></div>\n    </div>\n    <div class=\"emp-main-details\" >\n      <div class=\"emp-individual-details pull-left col-md-4\">\n        <img src=\"../../../assets/images/Directory/empoyee_-1.png\" class=\"employee-img\">\n        <div class=\"emp-details\">\n          <span class=\"emp-name\">{{note.createdBy.name}}</span>\n          <small>{{note.createdBy.type}}</small>\n        </div>\n        <div class=\"emp-details-timings\">\n          <span>{{note.createdDate.split('T')[0]}}</span><br>\n          <small>{{note.createdDate.split('T')[1].split('Z')[0].split('.')[0]}}</small>\n        </div>\n      </div>\n      <div class=\"pull-right\">\n        <ul class=\"list-unstyled icons-list\">\n          <li class=\"list-items\">\n            <img src=\"../../../assets/images/directory_tabs/Assets/edit.png\" class=\"edit-icon\">\n          </li>\n          <li class=\"list-items\">\n            <img src=\"../../../assets/images/directory_tabs/Assets/delete.png\" class=\"edit-icon\">\n          </li>\n        </ul>\n      </div>\n      <div class=\"clearfix\"></div>\n      <hr class=\"zenwork-margin-ten-zero\">\n      <div class=\"row\">\n        <div class=\"col-xs-9\">\n          <div class=\"emp-description\">\n            <p>\n              {{note.description}}\n            </p>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div> -->\n  <div class=\"employee-notes\" *ngFor=\"let notes of allNotes\">\n      <div class=\"emp-notes-head\">\n        <span class=\"notes-heading pull-left\">Employee Notes</span>\n        <span  *ngIf=\"!notesTabView\" class=\"archive-text pull-right\">\n          <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n          <mat-menu #menu=\"matMenu\">\n            <button mat-menu-item (click)=\"editNotes(notes._id)\">Edit</button>\n            <button mat-menu-item (click)=\"deleteNotes(notes._id)\">Delete</button>\n            <button mat-menu-item (click)=\"archiveNotes(notes._id)\" *ngIf=\"notes.archive == false\">\n              Archive</button>\n              <button mat-menu-item (click)=\"unArchiveNotes(notes._id)\" *ngIf=\"notes.archive == true\">\n                  Unarchive</button>\n\n          </mat-menu>\n        </span>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"emp-main-details\">\n\n        <div class=\"emp-individual-details pull-left col-md-4\">\n\n          <span>\n            <i *ngIf=\"notes.addedBy.type == 'company'\" class=\"material-icons emplployee-icon employee-img\">\n              account_circle\n            </i>\n            <img *ngIf=\"notes.addedBy.type == 'employee'\" src=\"{{notes.addedBy.personal.profile_pic_details.url}}\">\n\n          </span>\n         \n          <div class=\"emp-details\">\n            <span class=\"emp-name\" *ngIf=\"notes.addedBy.type == 'company'\">System Admin</span>\n            <span class=\"emp-name\" *ngIf=\"notes.addedBy.type == 'employee'\">{{notes.addedBy.personal.name.preferredName}}</span>\n\n            <small></small>\n          </div>\n\n          <div class=\"emp-details-timings\">\n            <span>{{notes.createdAt | date: 'MM/dd/yyyy'}}</span><br>\n            <small>{{notes.createdAt | date: \"shortTime\"}}</small>\n          </div>\n        </div>\n    \n        <div class=\"clearfix\"></div>\n        <hr class=\"zenwork-margin-ten-zero\">\n        <div class=\"row\">\n          <div class=\"col-xs-9\">\n            <div class=\"emp-description\">\n              <p>{{notes.note}}</p>\n            </div>\n          </div>\n        </div>\n      </div>\n  \n    </div>\n    \n\n    <div style=\"margin:20px 0px;\">\n  \n        <!-- <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Next</button>\n    <div class=\"clear-fix\"></div> -->\n      </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/notes/notes/notes.component.ts ***!
  \************************************************************************************/
/*! exports provided: NotesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesComponent", function() { return NotesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NotesComponent = /** @class */ (function () {
    function NotesComponent(employeeService, accessLocalStorageService, swalAlertService, myInfoService, router) {
        this.employeeService = employeeService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.myInfoService = myInfoService;
        this.router = router;
        this.employeeNotes = { _id: '', description: "" };
        this.allNotes = [];
        this.archivedNotes = [];
        // public archive : boolean = false;
        this.archiveDisabled = true;
        this.pagesAccess = [];
        this.notesTabView = false;
    }
    NotesComponent.prototype.ngOnInit = function () {
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
            this.pageAccesslevels();
        }
        console.log(this.userId);
        this.addedBy = this.accessLocalStorageService.get('addedBy');
        console.log(this.addedBy);
        this.getAllNotes();
    };
    NotesComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Notes" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.notesTabView = true;
                    console.log('loggss', _this.notesTabView);
                }
            });
        }, function (err) {
        });
    };
    // Author:Saiprakash G, Date:11-05-19
    // Get all notes for user
    NotesComponent.prototype.getAllNotes = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            userId: this.userId,
            archive: false
        };
        this.myInfoService.getAllNotes(data)
            .subscribe(function (notes) {
            console.log(notes);
            _this.allNotes = notes.data;
            if (_this.allNotes.length > 0) {
                _this.archiveDisabled = false;
            }
            // this.allNotes.forEach(element => {
            //   if(element.archive == true){
            //     this.archiveDisabled = false;
            //   }
            // });
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Notes", err.error.message, "error");
        });
    };
    // Author:Saiprakash G, Date:11-05-19
    // Save and update notes
    NotesComponent.prototype.saveNotes = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            userId: this.userId,
            addedBy: this.addedBy,
            note: this.employeeNotes.description
        };
        if (this.noteId) {
            this.myInfoService.editNote(this.noteId, data).subscribe(function (res) {
                console.log(res);
                _this.employeeNotes.description = "";
                _this.getAllNotes();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/training"]);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.myInfoService.addNotes(data)
                .subscribe(function (notes) {
                console.log(notes);
                _this.employeeNotes.description = "";
                _this.getAllNotes();
                if (notes.status) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation(notes.message, "", "success");
                    _this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/training"]);
                }
                else {
                    _this.swalAlertService.SweetAlertWithoutConfirmation(notes.message, "", "error");
                }
            }, function (err) {
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author:Saiprakash G, Date:11-05-19
    // Get single notes edit
    NotesComponent.prototype.editNotes = function (id) {
        var _this = this;
        this.noteId = id;
        this.myInfoService.getNote(this.companyId, this.userId, id).subscribe(function (res) {
            console.log(res);
            _this.employeeNotes.description = res.data.note;
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash G, Date:11-05-19
    // Delete notes 
    NotesComponent.prototype.deleteNotes = function (id) {
        var _this = this;
        this.myInfoService.deleteNote(this.companyId, this.userId, id).subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.getAllNotes();
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    // Author:Saiprakash G, Date:11-05-19
    // Archive notes 
    NotesComponent.prototype.archiveNotes = function (id) {
        var _this = this;
        // this.archiveDisabled = false;
        var data1 = {
            companyId: this.companyId,
            userId: this.userId,
            archive: true
        };
        this.myInfoService.changeStatus(id, data1).subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.getAllNotes();
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    NotesComponent.prototype.unArchiveNotes = function (id) {
        var _this = this;
        var data2 = {
            companyId: this.companyId,
            userId: this.userId,
            archive: false
        };
        this.myInfoService.changeStatus(id, data2).subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.getAllNotes();
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    // Author:Saiprakash G, Date:11-05-19
    // View Archived notes 
    NotesComponent.prototype.viewArchivedNotes = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            userId: this.userId,
        };
        this.myInfoService.getAllNotes(data)
            .subscribe(function (notes) {
            console.log(notes);
            _this.allNotes = notes.data;
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    NotesComponent.prototype.cancelNotes = function () {
        this.employeeNotes.description = "";
    };
    NotesComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/training"]);
    };
    NotesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notes',
            template: __webpack_require__(/*! ./notes.component.html */ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.html"),
            styles: [__webpack_require__(/*! ./notes.component.css */ "./src/app/admin-dashboard/employee-management/notes/notes/notes.component.css")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__["MyInfoService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], NotesComponent);
    return NotesComponent;
}());



/***/ })

}]);
//# sourceMappingURL=notes-notes-module.js.map