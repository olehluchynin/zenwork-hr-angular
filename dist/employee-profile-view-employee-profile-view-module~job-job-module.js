(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["employee-profile-view-employee-profile-view-module~job-job-module"],{

/***/ "./src/app/admin-dashboard/employee-management/job/job.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/job/job.module.ts ***!
  \***********************************************************************/
/*! exports provided: JobModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobModule", function() { return JobModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _job_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./job.routing */ "./src/app/admin-dashboard/employee-management/job/job.routing.ts");
/* harmony import */ var _job_job_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./job/job.component */ "./src/app/admin-dashboard/employee-management/job/job/job.component.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/accordion */ "./node_modules/ngx-bootstrap/accordion/fesm5/ngx-bootstrap-accordion.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var JobModule = /** @class */ (function () {
    function JobModule() {
    }
    JobModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _job_routing__WEBPACK_IMPORTED_MODULE_2__["JObRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"],
                ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_6__["AccordionModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_7__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__["MatCheckboxModule"]
            ],
            declarations: [_job_job_component__WEBPACK_IMPORTED_MODULE_3__["JobComponent"]],
            providers: [_services_my_info_service__WEBPACK_IMPORTED_MODULE_9__["MyInfoService"]]
        })
    ], JobModule);
    return JobModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/job/job.routing.ts":
/*!************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/job/job.routing.ts ***!
  \************************************************************************/
/*! exports provided: JObRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JObRouting", function() { return JObRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _job_job_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./job/job.component */ "./src/app/admin-dashboard/employee-management/job/job/job.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"job",pathMatch:"full" },
    { path: '', component: _job_job_component__WEBPACK_IMPORTED_MODULE_2__["JobComponent"] },
];
var JObRouting = /** @class */ (function () {
    function JObRouting() {
    }
    JObRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], JObRouting);
    return JObRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/job/job/job.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/job/job/job.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal { margin: 30px auto 0; float: none;}\n.personal .panel-default>.panel-heading { padding: 0 0 25px; background-color:transparent !important; border: none;}\n.personal .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n.personal .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.personal .panel-title>.small, .personal .panel-title>.small>a, .personal .panel-title>a, .personal .panel-title>small, .personal .panel-title>small>a { text-decoration:none;}\n.personal .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.personal .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.personal .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.personal .panel-group .panel-heading+.panel-collapse>.list-group, .personal .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.personal .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.personal .panel-body { padding: 0 0 5px;}\n.personal .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.personal .panel-default { border-color:transparent;}\n.personal .panel-group .panel+.panel { margin-top: 20px;}\n.employee-details{\n    margin: 10px 0 0 0;\n}\n.padding-left-15{\n    padding: 0 0 0 15px;\n \n}\n.list-items-addresslocal{\n    display: inline-block;\n    padding: 0px 7px;margin: 0 0 20px;\n}\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n}\n.mar-btm-25 {\n    margin-bottom: 25px;\n}\ninput{\n    height:30px;\n    border:0;\n    /* width: 178px; */\n}\nselect{\n    font-size: 12px;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 15px;\n    padding: 0 0 6px;\n}\n.address-state-dropodown{\n    width: 100%;\n    border-radius: 0px!important;\n    height: 30px;\n    border: 0;\n    box-shadow: none;\n}\n.education-date {\n    margin: 0px 5px 10px 0;\n}\n.emp-imgs{\n    vertical-align: text-bottom;\n    margin: 0 0 6px 0;\n    /* margin-left: 20%; */\n    height: 64px;\n}\n.emp-communicate-icons {\n    padding: 10px 0 0 0; width: 80px;\n}\n.emp-communicate-icons i{\n    display: inline-block;\n    font-size: 20px;\n    padding: 0px 10px;\n}\n.education-date-list {\n    \n    position: relative;\n    display: inline-block;\n    padding: 0px 7px;\n    \n}\n.birthdate{\n    font-size: 14px;\n    padding-left: 15px;\n    box-shadow: none;\n}\n.edu-ends-calendar-icon{\n    position: absolute;\n    top: 42px;\n    width: 15px;\n    right: 15px;\n}\n.width-17 {\n    width: 17%;\n}\n.status-list-items{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.submit-buttons .list-item{\n    display: inline-block;\n    padding: 0px 10px 0 0;\n  }\n.submit-buttons .btn-success {\n      background-color: #fff;\n      color:#439348;\n      border: #439348 1px solid;\n  }\n.margin-top{\n      margin-top: 40px 0 20px 0;\n  }\n.save-changes{\n      margin: 20px 0px;\n  }\n.btn-style{\n      padding: 7px 28px;\n      border-radius: 30px;\n      font-size: 15px;\n  }\n.zenwork-padding-20-zero{\n    padding:10px 0px!important;\n}\n.add-education{\n    font-size: 14px;\n    margin: 0 30px 0 0;\n}\n.submit-buttons .cancel { background: transparent; border: transparent; color: #e44a49; font-size: 16px;}\n.name b{\n    color:#616161;\n    cursor: pointer;\n    font-size: 14px;\n    font-weight: normal;\n}\n.select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n    width:100%;\n}\n.emp-manager-imgs {\n    vertical-align: text-bottom;\n    margin: 0 0 6px 0;\n    height: 64px;\n}\n.tbl-head {\n    background: #ecf7ff\n}\n.rows-bg {\n    background: #fff;\n}\n.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {\n    border-bottom: 1px solid #f8f8f8;\n    border-top: 1px solid #f8f8f8; font-size: 15px; padding: 15px 10px;\n}\n.employement-status-modal .modal-dialog { width: 60%;}\n.employement-status-modal .modal-content { border-radius: 5px !important;}\n.employement-status-modal .modal-header {padding: 15px 15px 20px;}\n.employement-status-modal .modal-header h4 { padding: 0 0 0 30px;}\n.model-bg {\n    background: #fff;\n    padding: 30px;\n}\n.zenwork-margin-ten-zero {\n    margin: 10px 0px!important;\n    border-top: 2px solid #eee;\n}\n.mar-btm-30 {\n    margin-bottom: 25px;\n}\ninput{\n    height:30px;\n    border:0;\n}\n.list-items-employement{\n    display: inline-block;\n    width: 24%;\n    padding: 0px 7px;\n}\n.list-job-employement {\n    display: inline-block;\n    width: 50%;\n    padding: 0px 7px;\n}\n.list-job-employement .text-field {\n    resize: none;\n    border: none;\n    background: #f8f8f8;\n    width: 100%;\n}\n.list-items-employement .text-field{\n    font-size: 15px;\n    padding-left: 15px;\n    width: 100%;\n    background: #f8f8f8\n}\n.employement-details{\n    margin: 20px 0 0 0;\n}\n.padding-left-15{\n    padding: 0 0 0 15px;\n \n}\n.emp-select-btn {\n    border: none;\n    box-shadow: none;\n    height: auto;\n    width: 100%;\n    background: #f8f8f8;\n}\n.height-30 {\n    height: 30px;\n}\n.status-date {\n    margin: 0px 5px 10px 0;\n}\n.employment-date-list {   \n    position: relative;\n    display: inline-block;\n    padding: 0px 23px; \n    width: 27%;   \n}\n.job-date-list {   \n    position: relative;\n    display: inline-block; \n    width: 24%;   \n}\n.employmnent-dates {\n    margin-top: 20px;\n}\n.mar-btm-45 {\n    margin-bottom: 45px;\n}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.mar-top-40 {\n    margin-top: 40px;\n}\n.mar-btm-50 {\n    margin-bottom: 50px;\n}\n.error {\n    color:#e44a49;\n    padding: 5px 0 0;\n}\ntextarea{\n    resize: none;\n    width:90%;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n}\n.next-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 20px;\n    color: #fff;\n    padding: 0 30px;    \n}\n.date { height: auto; line-height:inherit; width:100%;background:#fff;}\n.date a { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date a:focus { border: none; outline: none;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:75%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.employmnent-dates .date, .job-date-list .date { background:#f8f8f8;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/job/job/job.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/job/job/job.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"personal col-md-11\">\n  <form [formGroup]=\"jobDetailsForm\" (ngSubmit)=\"saveJobDetails()\">\n\n    <div class=\"panel-group\" id=\"accordion3\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseOne\">\n              Details\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"employee-details padding-left-15\">\n              <div class=\"row\">\n                <div class=\"col-xs-2 list-items-addresslocal city\" *ngIf=\"!fieldsShow.Employee_Id.hide\">\n                  <label>Employee#\n                    <span *ngIf=\"fieldsShow.Employee_Id.required\"> * </span>\n                  </label>\n                  <input *ngIf=\"fieldsShow.Employee_Id.format == 'number'\" type=\"text\" (keypress)=\"keyPress($event)\" class=\"form-control text-field\"\n                    placeholder=\"\" formControlName=\"employeeId\" [required]=\"fieldsShow.Employee_Id.required\" placeholder=\"Employee\" [readonly]=\"jobTabView\">\n\n                  <input *ngIf=\"fieldsShow.Employee_Id.format == 'Alpha' || fieldsShow.Employee_Id.format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control text-field\" placeholder=\"Employee\"\n                    formControlName=\"employeeId\" [required]=\"fieldsShow.Employee_Id.required\" [readonly]=\"jobTabView\">\n\n                  <input *ngIf=\"fieldsShow.Employee_Id.format == 'AlphaNumeric'\" type=\"text\" (keypress)=\"omit_special_char($event)\"\n                    class=\"form-control text-field\" placeholder=\"Employee\" formControlName=\"employeeId\"\n                    [required]=\"fieldsShow.Employee_Id.required\" [readonly]=\"jobTabView\">\n\n                  <span *ngIf=\"jobDetailsForm.get('employeeId').invalid && jobDetailsForm.get('employeeId').touched\"\n                    class=\"error\">You must enter a employee Id</span>\n                </div>\n                <div class=\"col-xs-2 list-items-addresslocal\" *ngIf=\"!fieldsShow.Employee_Type.hide\">\n                  <label>Employee Type <span *ngIf=\"fieldsShow.Employee_Type.required\"> * </span></label>\n                  <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select employee type\"\n                    formControlName=\"employeeType\" [required]=\"fieldsShow.Employee_Type.required\" [disabled]=\"jobTabView\">\n                    <mat-option *ngFor=\"let emp of empType\" [value]=\"emp.name\">{{emp.name}}\n                    </mat-option>\n\n                  </mat-select>\n                  <span *ngIf=\"jobDetailsForm.get('employeeType').invalid && jobDetailsForm.get('employeeType').touched\"\n                    class=\"error\">You must select a employee type</span>\n                </div>\n                <!-- <div class=\"col-xs-2 list-items-addresslocal city\" *ngIf=\"!fieldsShow.Employee_Self_Service_Id.hide\">\n                  <label>Employee Self Service ID <span *ngIf=\"fieldsShow.Employee_Self_Service_Id.required\"> *\n                    </span></label><br>\n\n                  <input *ngIf=\"fieldsShow.Employee_Self_Service_Id.format == 'number'\" type=\"text\" (keypress)=\"keyPress($event)\"\n                    class=\"form-control text-field\" placeholder=\"\" formControlName=\"employeeSelfServiceId\"\n                    [required]=\"fieldsShow.Employee_Self_Service_Id.required\">\n                  <input\n                    *ngIf=\"fieldsShow.Employee_Self_Service_Id.format == 'Alpha' || fieldsShow.Employee_Self_Service_Id  .format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control text-field\" placeholder=\"\"\n                    formControlName=\"employeeSelfServiceId\" [required]=\"fieldsShow.Employee_Self_Service_Id.required\">\n                  <input *ngIf=\"fieldsShow.Employee_Self_Service_Id.format == 'AlphaNumeric'\" type=\"text\" (keypress)=\"omit_special_char($event)\" \n                    class=\"form-control text-field\" placeholder=\"\" formControlName=\"employeeSelfServiceId\"\n                    [required]=\"fieldsShow.Employee_Self_Service_Id.required\">\n\n\n                  <span\n                    *ngIf=\"jobDetailsForm.get('employeeSelfServiceId').invalid && jobDetailsForm.get('employeeSelfServiceId').touched\"\n                    class=\"error\">You must enter a employee self service Id</span>\n                </div> -->\n                <div class=\"col-xs-4 pull-right\">\n                  <div class=\"col-xs-3 education-date-list emp-imgs\">\n\n                    <img src=\"../../../assets/images/directory_tabs/jobs/emp.png\" class=\"img-emp-icon\" alt=\"emp-icon\">\n                    <div class=\"emp-communicate-icons\">\n                      <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                      <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\n                    </div>\n\n                  </div>\n                  <div class=\"col-xs-6 city hr-contact\">\n                    <label>HR Contact*</label>\n                    <input type=\"text\" class=\"form-control text-field\" placeholder=\"\" formControlName=\"HRContact\" [readonly]=\"jobTabView\">\n                    <span *ngIf=\"jobDetailsForm.get('HRContact').invalid && jobDetailsForm.get('HRContact').touched\"\n                      class=\"error\">You must enter a HR contact</span>\n                  </div>\n                </div>\n              </div>\n              \n              <div class=\"row education-dates\">\n                <div class=\"col-xs-2 list-items-addresslocal\" *ngIf=\"!fieldsShow.Work_Location.hide\">\n                  <label>Work Location <span *ngIf=\"fieldsShow.Work_Location.required\"> * </span></label>\n                  <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select location\"\n                    formControlName=\"location\" [required]=\"fieldsShow.Work_Location.required\" [disabled]=\"jobTabView\">\n                    <mat-option *ngFor=\"let locations of location\" [value]=\"locations.name\">\n                      {{locations.name}}</mat-option>\n\n                  </mat-select>\n                  <span *ngIf=\"jobDetailsForm.get('location').invalid && jobDetailsForm.get('location').touched\"\n                    class=\"error\">You must select a work location</span>\n                </div>\n                <div class=\"col-xs-2 list-items-addresslocal\" *ngIf=\"!fieldsShow.Reporting_Location.hide\">\n                  <label>Reporting Location <span *ngIf=\"fieldsShow.Reporting_Location.required\"> * </span></label>\n                  <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select location\"\n                    formControlName=\"reportingLocation\" [required]=\"fieldsShow.Reporting_Location.required\" [disabled]=\"jobTabView\">\n                    <mat-option *ngFor=\"let locations of location\" [value]='locations.name'>\n                      {{locations.name}}</mat-option>\n                  </mat-select>\n                  <span\n                    *ngIf=\"jobDetailsForm.get('reportingLocation').invalid && jobDetailsForm.get('reportingLocation').touched\"\n                    class=\"error\">You must select a reporting location</span>\n                </div>\n              </div>\n\n              <div class=\"row education-dates\">\n                <div class=\"education-date\">\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.Hire_Date.hide\">\n                    <label>Hire Date <span *ngIf=\"fieldsShow.Hire_Date.required\"> * </span></label>\n\n\n                    <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\" class=\"form-control\" formControlName=\"hireDate\" [max]=\"jobTerminationData\" (ngModelChange)=\"hireDateChange()\" [required]=\"fieldsShow.Hire_Date.required\" [disabled]=\"jobTabView\">\n                        <mat-datepicker #picker1></mat-datepicker>\n                        <a mat-raised-button (click)=\"picker1.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </a>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n                    <span *ngIf=\"jobDetailsForm.get('hireDate').invalid && jobDetailsForm.get('hireDate').touched\"\n                    class=\"error\">You must select a hire date</span>\n\n                    <!-- <input class=\"form-control text-field birthdate address-state-dropodown\" #dpMDYSdate=\"bsDatepicker\"\n                      bsDatepicker formControlName=\"hireDate\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                      placeholder=\"mm/dd/yyyy\" name=\"birth-date\" [required]=\"fieldsShow.Hire_Date.required\">\n\n                  \n\n                    <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\"\n                      alt=\"Company-settings icon\" (click)=\"dpMDYSdate.toggle()\"\n                      [attr.aria-expanded]=\"dpMDYSdate.isOpen\"> -->\n\n                  </div>\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.Rehire_Date.hide\" >\n                    <label>Rehire Date <span *ngIf=\"fieldsShow.Rehire_Date.required\"> * </span></label>\n\n                    <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm / dd / yy\" class=\"form-control\" formControlName=\"reHireDate\" [min]=\"jobHireData\" (ngModelChange)=\"reHireDateChange()\" [required]=\"fieldsShow.Rehire_Date.required\" [disabled]=\"jobTabView\">\n                        <mat-datepicker #picker2></mat-datepicker>\n                        <a mat-raised-button (click)=\"picker2.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </a>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n\n                      <span *ngIf=\"jobDetailsForm.get('reHireDate').invalid && jobDetailsForm.get('reHireDate').touched\"\n                      class=\"error\">You must select a rehire date</span>\n\n                    <!-- <input class=\"form-control text-field birthdate address-state-dropodown\" #dpMDYEdate=\"bsDatepicker\"\n                      bsDatepicker formControlName=\"reHireDate\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                      placeholder=\"mm/dd/yyyy\" name=\"birth-date\" [required]=\"fieldsShow.Rehire_Date.required\">\n                   \n                    <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\"\n                      alt=\"Company-settings icon\" (click)=\"dpMDYEdate.toggle()\"\n                      [attr.aria-expanded]=\"dpMDYEdate.isOpen\"> -->\n\n                  </div>\n                  <div class=\"col-xs-4 pull-right\">\n                    <div class=\"col-xs-3 education-date-list emp-manager-imgs\">\n                      <img src=\"../../../assets/images/directory_tabs/jobs/emp.png\" class=\"img-emp-icon\" alt=\"emp-icon\">\n                      <div class=\"emp-communicate-icons\">\n                        <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                        <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\n                      </div>\n                    </div>\n                    <div class=\"col-xs-6 city hr-contact\">\n                      <label>Manager Contact*</label><br>\n                      <input type=\"text\" class=\"form-control text-field\" placeholder=\"\" formControlName=\"managerContact\"\n                        required [disabled]=\"jobTabView\">\n                      <span\n                        *ngIf=\"jobDetailsForm.get('managerContact').invalid && jobDetailsForm.get('managerContact').touched\"\n                        class=\"error\">You must enter a manager contact</span>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"row education-dates \">\n                <div class=\" education-date\">\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.Termination_Date.hide\">\n                    <label>Termination Date <span *ngIf=\"fieldsShow.Termination_Date.required\"> * </span></label>\n\n\n                    <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker3\" placeholder=\"mm / dd / yy\" class=\"form-control\" formControlName=\"terminationDate\"   [min]=\"jobHireData\"  (ngModelChange)=\"terminationDateChange()\" [required]=\"fieldsShow.Termination_Date.required\" [disabled]=\"jobTabView\">\n                        <mat-datepicker #picker3></mat-datepicker>\n                        <a mat-raised-button (click)=\"picker3.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </a>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n                      <span\n                      *ngIf=\"jobDetailsForm.get('terminationDate').invalid && jobDetailsForm.get('terminationDate').touched\"\n                      class=\"error\">You must select a termination date</span>\n\n                    <!-- <input class=\"form-control text-field birthdate address-state-dropodown\" #dpMDYTDATE=\"bsDatepicker\"\n                      bsDatepicker formControlName=\"terminationDate\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                      placeholder=\"mm/dd/yyyy\" name=\"birth-date\" [required]=\"fieldsShow.Termination_Date.required\">\n                    \n                    <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\"\n                      alt=\"Company-settings icon\" (click)=\"dpMDYTDATE.toggle()\"\n                      [attr.aria-expanded]=\"dpMDYTDATE.isOpen\"> -->\n                  </div>\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.Termination_Reason.hide\">\n                    <label>Termination Reason <span *ngIf=\"fieldsShow.Termination_Reason.required\"> * </span></label>\n                    <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select termination\"\n                      formControlName=\"terminationReason\" [required]=\"fieldsShow.Termination_Reason.required\" [disabled]=\"jobTabView\">\n                      <mat-option value=\"Absence\">A-Absence</mat-option>\n                      <mat-option value=\"Performance\">B-Performance</mat-option>\n                      <mat-option value=\"Better Opportunity\">C-Better Opportunity</mat-option>\n                      <mat-option value=\"Misconduct\">D-Misconduct</mat-option>\n                    </mat-select>\n                    <span\n                      *ngIf=\"jobDetailsForm.get('terminationReason').invalid && jobDetailsForm.get('terminationReason').touched\"\n                      class=\"error\">You must select a termination reason</span>\n                  </div>\n\n                  <div class=\"clearfix\"></div>\n                </div>\n              </div>\n              <div class=\"row education-dates\">\n                <div class=\"col-xs-2 list-items-addresslocal\" *ngIf=\"!fieldsShow.Rehire_Status.hide\">\n                  <label>Rehire Status <span *ngIf=\"fieldsShow.Rehire_Status.required\"> * </span></label>\n                  <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select status\"\n                    formControlName=\"rehireStatus\" [required]=\"fieldsShow.Rehire_Status.required\" [disabled]=\"jobTabView\">\n                    <mat-option *ngFor=\"let data of rehireArray\" [value]=\"data.name\">{{data.name}}</mat-option>\n                    <!-- <mat-option value=\"inactive\">Inactive</mat-option> -->\n                  </mat-select>\n                  <span *ngIf=\"jobDetailsForm.get('rehireStatus').invalid && jobDetailsForm.get('rehireStatus').touched\"\n                    class=\"error\">You must select a status</span>\n                </div>\n                <div class=\"col-xs-2 list-items-addresslocal\" *ngIf=\"!fieldsShow.Archive_Status.hide\">\n                  <label>Archive Status <span *ngIf=\"fieldsShow.Archive_Status.required\"> * </span></label>\n                  <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select status\"\n                    formControlName=\"archiveStatus\" [required]=\"fieldsShow.Archive_Status.required\" [disabled]=\"jobTabView\">\n                    <mat-option value=\"active\">Active</mat-option>\n                    <mat-option value=\"inactive\">Inactive</mat-option>\n                  </mat-select>\n                  <span\n                    *ngIf=\"jobDetailsForm.get('archiveStatus').invalid && jobDetailsForm.get('archiveStatus').touched\"\n                    class=\"error\">You must select a status</span>\n                </div>\n              </div>\n\n              <div class=\"row education-dates \">\n                <div class=\"education-date\">\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.FLSA_Code.hide\">\n                    <label>FLSA Code <span *ngIf=\"fieldsShow.FLSA_Code.required\"> * </span></label><br>\n                    <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select code\"\n                      formControlName=\"FLSA_Code\" [required]=\"fieldsShow.FLSA_Code.required\" [disabled]=\"jobTabView\">\n                      <mat-option [value]='flsa.name' *ngFor=\"let flsa of flsaCode\">{{flsa.name}}\n                      </mat-option>\n                    </mat-select>\n                    <span *ngIf=\"jobDetailsForm.get('FLSA_Code').invalid && jobDetailsForm.get('FLSA_Code').touched\"\n                      class=\"error\">You must select a FLSA code</span>\n                  </div>\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.EEO_Job_Category.hide\">\n                    <label>EEO Job Category <span *ngIf=\"fieldsShow.EEO_Job_Category.required\"> * </span></label><br>\n                    <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select category\"\n                      formControlName=\"EEO_Job_Category\" [required]=\"fieldsShow.EEO_Job_Category.required\" [disabled]=\"jobTabView\">\n                      <mat-option [value]='eeo.name' *ngFor=\"let eeo of eeoJob\">{{eeo.name}}\n                      </mat-option>\n                    </mat-select>\n                    <span\n                      *ngIf=\"jobDetailsForm.get('EEO_Job_Category').invalid && jobDetailsForm.get('EEO_Job_Category').touched\"\n                      class=\"error\">You must select job category</span>\n                  </div>\n                </div>\n              </div>\n              <div class=\"row education-dates \">\n                <div class=\"col-xs-2 list-items-addresslocal city\" *ngIf=\"!fieldsShow.Vendor_Id.hide\">\n                  <label>Vendor ID <span *ngIf=\"fieldsShow.Vendor_Id.required\"> * </span></label>\n                  <input *ngIf=\"fieldsShow.Vendor_Id.format == 'number'\" type=\"number\" class=\"form-control text-field\"\n                    placeholder=\"Vendor id\" formControlName=\"VendorId\" [required]=\"fieldsShow.Vendor_Id.required\" [readonly]=\"jobTabView\">\n                  <input *ngIf=\"fieldsShow.Vendor_Id.format == 'Alpha' || fieldsShow.Vendor_Id.format == 'text'\"\n                    (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control text-field\" placeholder=\"Vendor id\"\n                    formControlName=\"VendorId\" [required]=\"fieldsShow.Vendor_Id.required\" [readonly]=\"jobTabView\">\n                  <input *ngIf=\"fieldsShow.Vendor_Id.format == 'AlphaNumeric'\" type=\"text\"\n                    class=\"form-control text-field\" placeholder=\"Vendor id\" formControlName=\"VendorId\"\n                    [required]=\"fieldsShow.Vendor_Id.required\" [readonly]=\"jobTabView\">\n\n\n                  <span *ngIf=\"jobDetailsForm.get('VendorId').invalid && jobDetailsForm.get('VendorId').touched\"\n                    class=\"error\">You must enter a vendor Id</span>\n                </div>\n\n                <div class=\"col-xs-3 list-items-addresslocal\"\n                  *ngIf=\"!fieldsShow.Specific_Workflow_Approval.hide\">\n                  <label>Specific Workflow Approval <span *ngIf=\"fieldsShow.Specific_Workflow_Approval.required\"> *\n                    </span></label><br>\n                  <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select approval\"\n                    formControlName=\"Specific_Workflow_Approval\"\n                    [required]=\"fieldsShow.Specific_Workflow_Approval.required\" [disabled]=\"jobTabView\">\n                    <mat-option value=\"Yes\">Yes</mat-option>\n                    <mat-option value=\"No\">No</mat-option>\n                  </mat-select>\n                  <span\n                    *ngIf=\"jobDetailsForm.get('Specific_Workflow_Approval').invalid && jobDetailsForm.get('Specific_Workflow_Approval').touched\"\n                    class=\"error\">You must select a workflow approval</span>\n                </div>\n              </div>\n              <div class=\"row education-dates\">\n                <div class=\"education-date\">\n                  <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.Site_AccessRole.hide\">\n                    <label>Site Access Role <span *ngIf=\"fieldsShow.Site_AccessRole.required\"> * </span></label><br>\n                    <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select role\"\n                      formControlName=\"Site_AccessRole\" [required]=\"fieldsShow.Site_AccessRole.required\" [disabled]=\"jobTabView\">\n                      <mat-option *ngFor=\"let data of companyRoles\" [value]=\"data._id\">{{data.name}}</mat-option>\n                      <!-- <mat-option value=\"Job2\">Job2</mat-option> -->\n                    </mat-select>\n\n                    <span\n                      *ngIf=\"jobDetailsForm.get('Site_AccessRole').invalid && jobDetailsForm.get('Site_AccessRole').touched\"\n                      class=\"error\">You must select a access role</span>\n                  </div>\n                  <!-- <div class=\"col-xs-2 education-date-list padding10-listitems list-items-addresslocal\"\n                    *ngIf=\"!fieldsShow.Type_of_User_Role_type.hide\">\n                    <label>Type of User Role <span *ngIf=\"fieldsShow.Type_of_User_Role_type.required\"> *\n                      </span></label><br>\n                    <mat-select class=\"form-control text-field select-btn\" placeholder=\"Select role\"\n                      formControlName=\"typeofUserRoletype\" [required]=\"fieldsShow.Type_of_User_Role_type.required\">\n                      <mat-option value=\"role1\">Role 1</mat-option>\n                      <mat-option value=\"role2\">Role 2</mat-option>\n                    </mat-select>\n                    <span\n                      *ngIf=\"jobDetailsForm.get('typeofUserRoletype').invalid && jobDetailsForm.get('typeofUserRoletype').touched\"\n                      class=\"error\">You must select a user role</span>\n                  </div> -->\n                </div>\n              </div>\n\n              <div *ngIf=\"!jobTabView\" class=\"save-changes\">\n                <div class=\"margin-top\">\n                  <ul class=\"submit-buttons list-unstyled\">\n                    <li class=\"list-item\">\n                      <button class=\"btn btn-success btn-style\" type=\"submit\"> Save </button>\n                    </li>\n                    <li class=\"list-item\">\n                      <button class=\"cancel\" (click)=\"cancelJob()\"> Cancel </button>\n                    </li>\n                  </ul>\n                </div>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n      <!-- <hr class=\"zenwork-margin-ten-zero\"> -->\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseTwo\">\n              Employee Status History\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseTwo\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n            <table class=\"table\">\n              <thead class=\"tbl-head\">\n                <tr>\n\n                  <th>Status</th>\n                  <th>Effective Date</th>\n                  <th>End Date</th>\n\n                  <th>\n                    <span *ngIf=\"!jobTabView\">\n                      <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n                      <mat-menu #menu=\"matMenu\">\n                        <button mat-menu-item data-toggle=\"modal\" data-target=\"#myModal\">Update Employment\n                          Status</button>\n\n                      </mat-menu>\n                    </span>\n                  </th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let data of employeeStatusHistory\" class=\"rows-bg\">\n                  <td>\n                    {{data.status}}\n                  </td>\n                  <td>\n                    {{data.effective_date | date}}\n                  </td>\n                  <td *ngIf=\"data.end_date\">\n                    {{data.end_date | date}}\n                  </td>\n                  <td *ngIf=\"!data.end_date\">\n                      --\n                    </td>\n\n\n                  <td>\n\n                  </td>\n                </tr>\n\n\n\n\n              </tbody>\n            </table>\n            <mat-paginator [length]=\"10\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\">\n            </mat-paginator>\n          </div>\n        </div>\n      </div>\n\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseThree\">\n              Job Information History\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseThree\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n            <table class=\"table\">\n              <thead class=\"tbl-head\">\n                <tr>\n\n                  <th> Job Title</th>\n                  <th> Business Unit</th>\n                  <th>Department</th>\n\n                  <th>Union</th>\n                  <th>Reports To</th>\n                  <th>Effective Date</th>\n                  <th>End Date</th>\n\n                  <th>\n                    <span *ngIf=\"!jobTabView\" class=\"option-position\">\n                      <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                      <mat-menu #menu1=\"matMenu\">\n                        <button mat-menu-item data-toggle=\"modal\" data-target=\"#myModal2\">Update Job Information\n                        </button>\n\n                      </mat-menu>\n                    </span>\n                  </th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let data of jobInfoHistory \" class=\"rows-bg\">\n                  <td>\n                    {{data.jobTitle.name}}\n                  </td>\n\n                  <td>\n                    {{data.businessUnit}}\n                  </td>\n                  <td>\n                    {{data.department}}\n                  </td>\n                  <td>\n                    {{data.unionFields}}\n                  </td>\n                  <td>\n                    <span *ngIf=\"data.ReportsTo\">{{data.ReportsTo.personal.name.preferredName || '--'}}</span>\n                    <span *ngIf=\"!data.ReportsTo\"> -- </span>\n\n                  </td>\n\n                  <td>\n\n                    {{data.effective_date | date}}\n                  </td>\n                  <td *ngIf=\"data.end_date\">\n                      {{data.end_date | date}}\n                  </td>\n                  <td *ngIf=\"!data.end_date\">\n                     --\n                  </td>\n                  <td>\n\n                  </td>\n\n\n                </tr>\n\n              </tbody>\n            </table>\n            <mat-paginator [length]=\"10\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\">\n            </mat-paginator>\n          </div>\n        </div>\n      </div>\n\n\n      <!-- <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseFive\">\n              Direct Reports\n\n            </a>\n\n          </h4>\n        </div>\n        <div id=\"collapseFive\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n            <div class=\"employees-details\">\n              <accordion class=\"margin-top-15\">\n                <accordion-group #group [isOpen]=\"isFirstOpen\">\n                  <span accordion-heading class=\"panel-border\">\n                    Employee's Details &nbsp;<i class=\"fa\"\n                      [ngClass]=\"group?.isOpen ? 'fa-caret-down': 'fa-caret-right'\"></i>\n                  </span>\n                  <div *ngIf=\"individualEmployee\">\n\n\n                    <div class=\"col-xs-12 name-employee\">\n                      <div class=\"row\">\n                        <a (click)=\"detailEmployee()\">\n                          Crane Andy\n\n                          <i class=\"glyphicon glyphicon-triangle-right angle\"></i>\n                        </a>\n                      </div>\n\n                    </div>\n\n                  </div>\n                  <div *ngIf=\"individualEmployeeDropdown\"\n                    class=\"col-xs-12 employee-individual-dropdown individual-employee\">\n                    <div class='row'>\n\n\n                      <div class=\"col-xs-5\">\n                        <div class=\"row\">\n                          <ul class=\"list-unstyled list-profile\">\n                            <li class=\"name\"> <a (click)=\"hideEmployeeData()\"><b>Crane Andy\n                                  <i class=\"glyphicon glyphicon-triangle-bottom angle\"></i></b></a>\n                            </li>\n                            <li class=\"list-details\">Sr. HR Administrator in Human Resources</li>\n                            <li class=\"list-details\"> Lindon,Utah</li>\n                            <li class=\"list-details\"> North America</li>\n                          \n                          </ul>\n                          <img src=\"../../../assets/images/Directory/layer1_1_.png\" class=\"list-profile\">\n                        </div>\n                      </div>\n\n                      <div class=\"col-xs-7\">\n                        <div class=\"row\">\n                          <ul class=\"list-personal list-unstyled\">\n                            <li class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/ic_email_24px.png\" class=\"img-emp-info\">\n                              <span class=\"emp-info\">craneandy@efficientoffice.com</span>\n                            </li>\n                            <li class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" class=\"img-emp-info\">\n                              <span class=\"emp-info\">801-724-6600 Ext.1272</span>\n                            </li>\n                            <li class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/Group 504.png\" class=\"img-emp-info-mobile\">\n                              <span class=\"emp-info\">&nbsp;415-555-8985</span>\n                            </li>\n                          </ul>\n                        </div>\n                      </div>\n\n                    </div>\n                  </div>\n                  <div *ngIf=\"individualEmployeefly\">\n\n\n                    <div class=\"col-xs-12 name-employee\">\n                      <div class=\"row\">\n                        <a (click)=\"detailEmployeefly()\">\n                          Crane Andy\n\n                          <i class=\"glyphicon glyphicon-triangle-right angle\"></i>\n                        </a>\n                      </div>\n\n                    </div>\n\n                  </div>\n                  <div *ngIf=\"individualEmployeeflyDropdown\"\n                    class=\"col-xs-12 employee-individual-dropdown individual-employee\">\n                    <div class='row'>\n\n\n                      <div class=\"col-xs-5\">\n                        <div class=\"row\">\n                          <ul class=\"list-unstyled list-profile\">\n                            <li class=\"name\"> <a (click)=\"hideEmployeeflyData()\"><b>Crane Andy\n                                  <i class=\"glyphicon glyphicon-triangle-bottom angle\"></i></b></a>\n                            </li>\n                            <li class=\"list-details\">Sr. HR Administrator in Human Resources</li>\n                            <li class=\"list-details\"> Lindon,Utah</li>\n                            <li class=\"list-details\"> North America</li>\n                        \n                          </ul>\n                          <img src=\"../../../assets/images/Directory/layer1_1_.png\" class=\"list-profile\">\n                        </div>\n                      </div>\n\n                      <div class=\"col-xs-7\">\n                        <div class=\"row\">\n                          <ul class=\"list-personal list-unstyled\">\n                            <li class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/ic_email_24px.png\" class=\"img-emp-info\">\n                              <span class=\"emp-info\">craneandy@efficientoffice.com</span>\n                            </li>\n                            <li class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/ic_domain_24px.png\" class=\"img-emp-info\">\n                              <span class=\"emp-info\">801-724-6600 Ext.1272</span>\n                            </li>\n                            <li class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/Group 504.png\" class=\"img-emp-info-mobile\">\n                              <span class=\"emp-info\">&nbsp;415-555-8985</span>\n                            </li>\n                          </ul>\n                        </div>\n                      </div>\n\n                    </div>\n                  </div>\n                </accordion-group>\n\n              </accordion>\n            </div>\n          </div>\n        </div>\n      </div> -->\n\n    </div>\n  </form>\n</div>\n\n<!-- Update employment status -->\n\n<div class=\"employement-status-modal\">\n  <div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"col-xs-12 model-bg\">\n          <h4 class=\"mar-btm-30\">Employment Status</h4>\n\n          <hr class=\"zenwork-margin-ten-zero\">\n\n          <div class=\"col-xs-12\">\n            <div>\n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Current Status</label><br>\n                    <input type=\"text\" [(ngModel)]=\"jobData.current_employment_status.status\"\n                      class=\"form-control text-field\" placeholder=\"Status\" readonly>\n                  </li>\n                </ul>\n              </div>\n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Select New Status</label><br>\n                    <mat-select class=\"form-control text-field\" [(ngModel)]=\"newStatus.empStatus\" (ngModelChange)=\"empStatusChange($event)\" placeholder=\"Select status\">\n                      <mat-option *ngFor=\"let data of allEmployementStatusArray\" [value]=\"data.name\">{{data.name}}\n                      </mat-option>\n\n                    </mat-select>\n                  </li>\n                </ul>\n              </div>\n            </div>\n            <div class=\"employmnent-dates\">\n              <ul class=\"list-unstyled status-date\">\n                <li class=\"employment-date-list padding10-listitems list-items-addresslocal\">\n                  <label>Select Effective Date</label>\n\n                  <div class=\"date\">\n                      <input matInput [matDatepicker]=\"picker4\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)]=\"newEffectivedate.effectDate\">\n                      <mat-datepicker #picker4></mat-datepicker>\n                      <a mat-raised-button (click)=\"picker4.open()\">\n                        <span>\n                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                        </span>\n                      </a>\n                      <div class=\"clearfix\"></div>\n                    </div>\n\n\n                  <!-- <input class=\"form-control text-field start-date address-state-dropodown data-picker-align\"\n                    [(ngModel)]=\"newEffectivedate\" #dpMDYSdate=\"bsDatepicker\" bsDatepicker autocomplete=\"off\"\n                    [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" placeholder=\"mm/dd/yyyy\" name=\"birth-date\">\n\n                  <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"emp-ends-calendar-icon\"\n                    alt=\"Company-settings icon\" (click)=\"dpMDYSdate.toggle()\" [attr.aria-expanded]=\"dpMDYSdate.isOpen\"> -->\n\n                </li>\n\n              </ul>\n            </div>\n            <div class=\"employement-details padding-left-15 mar-btm-45\">\n              <!-- <ul class=\"list-unstyled\">\n                <li class=\"list-items-employement\" style=\"width:24%\">\n                  <label>Available Termination Wizards</label><br>\n                  <mat-select class=\"form-control text-field\">\n                    <mat-option value=\"\">Standard Termination Wizard</mat-option>\n                  </mat-select>\n                </li>\n              </ul> -->\n            </div>\n            <hr class=\"zenwork-margin-ten-zero\">\n            <div class=\"row mar-top-40\">\n\n              <button mat-button data-dismiss=\"modal\">Cancel</button>\n\n              <button mat-button class=\"submit-btn pull-right\" (click)=\"updateEmployeeStatusInfo()\">Submit</button>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!-- Update job information -->\n\n<div class=\"employement-status-modal\">\n  <div id=\"myModal2\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"col-xs-12 model-bg\">\n          <h4 class=\"mar-btm-30\">Update Job Information</h4>\n\n          <hr class=\"zenwork-margin-ten-zero\">\n\n          <div class=\"col-xs-12\">\n            <div>\n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Job Title</label>\n                    <mat-select [(ngModel)]=\"updateJobData.jobTitle\" class=\"form-control text-field\" placeholder=\"Job title\">\n                      <mat-option *ngFor=\"let data of alljobTitleArray\" [value]=\"data._id\">{{data.name}}</mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"list-items-employement\">\n                    <label>Reports to</label>\n                    <mat-select [(ngModel)]=\"updateJobData.ReportsTo\" class=\"form-control text-field\" placeholder=\"Reports to\">\n                      <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>\n                        {{data.personal.name.preferredName}}</mat-option>\n                    </mat-select>\n                  </li>\n                </ul>\n              </div>\n            </div>\n            <div>\n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Business Unit</label>\n                    <mat-select [(ngModel)]=\"updateJobData.businessUnit\" class=\"form-control text-field\" placeholder=\"Business unit\">\n                      <mat-option *ngFor=\"let data of allbusinessUnitData\" [value]=\"data.name\">{{data.name}}\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"list-items-employement\">\n                    <label>Department</label>\n                    <mat-select [(ngModel)]=\"updateJobData.department\" class=\"form-control text-field\" placeholder=\"Department\">\n                      <mat-option *ngFor=\"let data of allDepartmentData\" [value]=\"data.name\">{{data.name}}\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"list-items-employement\">\n                    <label>Union</label>\n                    <mat-select [(ngModel)]=\"updateJobData.unionFields\" class=\"form-control text-field\" placeholder=\"Union\">\n                      <mat-option *ngFor=\"let data of allUnionData\" [value]=\"data.name\">{{data.name}}\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                </ul>\n              </div>\n            </div>\n            <div>\n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Location</label>\n                    <mat-select [(ngModel)]=\"updateJobData.location\" class=\"form-control text-field\" placeholder=\"Location\">\n                      <mat-option *ngFor=\"let data of allLocationData\" [value]=\"data.name\">{{data.name}}\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"list-items-employement\">\n                    <label>Work Remote</label>\n                    <mat-select [(ngModel)]=\"updateJobData.workRemote\" class=\"form-control text-field\" placeholder=\"Work remote\">\n                      <mat-option value=\"Yes\">Yes</mat-option>\n                      <mat-option value=\"No\">No</mat-option>\n                    </mat-select>\n                  </li>\n                </ul>\n              </div>\n            </div>\n            <div>\n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Change Reason</label>\n                    <mat-select [(ngModel)]=\"updateJobData.changeReason\" class=\"form-control text-field\" placeholder=\"Change reason\">\n                      <mat-option value=\"A-Promotion\">A-Promotion</mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"job-date-list padding10-listitems list-items-addresslocal\">\n                    <label>Effective Date</label>\n\n\n                    <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker5\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)]=\"updateJobData.effective_date\">\n                        <mat-datepicker #picker5></mat-datepicker>\n                        <a mat-raised-button (click)=\"picker5.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </a>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n                    <!-- <input class=\"form-control text-field start-date address-state-dropodown data-picker-align\"\n                      [(ngModel)]=\"updateJobData.effective_date\" #dpMDYSdate=\"bsDatepicker\" bsDatepicker\n                      [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" placeholder=\"mm/dd/yyyy\" name=\"birth-date\">\n                    <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"emp-ends-calendar-icon\"\n                      alt=\"Company-settings icon\" (click)=\"dpMDYSdate.toggle()\"\n                      [attr.aria-expanded]=\"dpMDYSdate.isOpen\"> -->\n\n                  </li>\n                </ul>\n              </div>\n            </div>\n            <div class=\"employement-details padding-left-15 mar-btm-50\">\n              <ul class=\"list-unstyled\">\n                <li class=\"list-job-employement text-area-width\">\n                  <label>Notes</label><br>\n                  <textarea class=\"form-control\" [(ngModel)]=\"updateJobData.notes\" rows=\"5\" placeholder=\"Note\"></textarea>\n                </li>\n              </ul>\n            </div>\n\n            <hr class=\"zenwork-margin-ten-zero\">\n            <div class=\"row mar-top-40\">\n\n              <button mat-button data-dismiss=\"modal\">Cancel</button>\n\n              <button mat-button class=\"submit-btn pull-right\" (click)=\"updateJobInfo()\">Submit</button>\n\n            </div>\n          </div>\n\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n  <div style=\"margin:20px 50px;\">\n\n    <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Save Changes</button>\n    <div class=\"clear-fix\"></div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/job/job/job.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/job/job/job.component.ts ***!
  \******************************************************************************/
/*! exports provided: JobComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobComponent", function() { return JobComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var JobComponent = /** @class */ (function () {
    function JobComponent(fb, myInfoService, onboardingService, siteAccessRolesService, swalAlertService, router) {
        this.fb = fb;
        this.myInfoService = myInfoService;
        this.onboardingService = onboardingService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.individualEmployee = false;
        this.individualEmployeeDropdown = true;
        this.individualEmployeefly = true;
        this.individualEmployeeflyDropdown = true;
        this.isFirstOpen = true;
        this.tabName = "Job";
        this.HRcontactDetails = [];
        this.ManagerContactDetails = [];
        this.fieldsShow = {
            Employee_Id: {
                hide: '',
                required: '',
                format: ''
            },
            Employee_Type: {
                hide: '',
                required: '',
                format: ''
            },
            Employee_Self_Service_Id: {
                hide: '',
                required: '',
                format: ''
            },
            // HR_Contact:{
            //   hide:'',
            //   required:'',
            //   format:''
            // },
            Work_Location: {
                hide: '',
                required: '',
                format: ''
            },
            Reporting_Location: {
                hide: '',
                required: '',
                format: ''
            },
            Hire_Date: {
                hide: '',
                required: '',
                format: ''
            },
            Rehire_Date: {
                hide: '',
                required: '',
                format: ''
            },
            // Manager_Contact:{
            //   hide:'',
            //   required:'',
            //   format:''
            // },
            Termination_Date: {
                hide: '',
                required: '',
                format: ''
            },
            Termination_Reason: {
                hide: '',
                required: '',
                format: ''
            },
            Rehire_Status: {
                hide: '',
                required: '',
                format: ''
            },
            Archive_Status: {
                hide: '',
                required: '',
                format: ''
            },
            FLSA_Code: {
                hide: '',
                required: '',
                format: ''
            },
            EEO_Job_Category: {
                hide: '',
                required: '',
                format: ''
            },
            Vendor_Id: {
                hide: '',
                required: '',
                format: ''
            },
            Specific_Workflow_Approval: {
                hide: '',
                required: '',
                format: ''
            },
            Site_AccessRole: {
                hide: '',
                required: '',
                format: ''
            },
            Type_of_User_Role_type: {
                hide: '',
                required: '',
                format: ''
            },
            Custom_1: {
                hide: '',
                required: '',
                format: ''
            },
            Custom_2: {
                hide: '',
                required: '',
                format: ''
            }
        };
        this.updateJobData = {
            jobTitle: '',
            unionFields: '',
            businessUnit: '',
            department: '',
            location: '',
            workRemote: '',
            changeReason: '',
            effective_date: '',
            ReportsTo: '',
            notes: '',
            companyId: '',
            userId: '',
        };
        this.jobData = {
            current_employment_status: {
                status: ""
            }
        };
        this.unamePattern = "^[a-z0-9_-]{8,15}$";
        this.rehireArray = [];
        this.allEmployementStatusArray = [];
        this.alljobTitleArray = [];
        this.allbusinessUnitData = [];
        this.allDepartmentData = [];
        this.allUnionData = [];
        this.allLocationData = [];
        this.newStatus = {
            empStatus: ''
        };
        this.newEffectivedate = {
            effectDate: ''
        };
        this.pagesAccess = [];
        this.jobTabView = false;
    }
    JobComponent.prototype.ngOnInit = function () {
        var cId = JSON.parse(localStorage.getItem('companyId'));
        this.roletype = JSON.parse(localStorage.getItem('type'));
        if (this.roletype != 'company') {
            this.pageAccesslevels();
        }
        this.getStandardAndCustomFieldsforStructure(cId);
        this.initJobDetailsForm();
        this.getSingleTabSettingForJob();
        this.getAllJobDetails();
        this.getStandardStructureField();
        this.empdata = JSON.parse(localStorage.getItem("employee"));
        console.log(this.empdata, "121212empdata");
        if (this.empdata) {
            this.employeeId = this.empdata._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
        }
        console.log(this.employeeId, "vpin");
        this.getThisUser(cId);
        this.employeeSearch();
        // this.jobData = this.empdata.job;
        // this.jobInfoHistory = this.jobData.job_information_history;
        // this.employeeStatusHistory = this.jobData.employment_status_history;
        // console.log("jobDaata", this.jobData, this.jobInfoHistory);
        // if (this.jobData) {
        //   console.log(new Date(this.jobData.hireDate));
        //   this.dt = new Date(this.jobData.hireDate);
        //   this.jobDetailsForm.patchValue(this.jobData);
        //   this.jobDetailsForm.patchValue({ hireDate: this.dt });
        //   console.log("jobDaata1", this.jobDetailsForm.value, this.jobData);
        // }
        this.getAllroles();
    };
    JobComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Job" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.jobTabView = true;
                    console.log('loggss', _this.jobTabView);
                }
            });
        }, function (err) {
        });
    };
    JobComponent.prototype.getStandardAndCustomFieldsforStructure = function (cID) {
        var _this = this;
        this.onboardingService.getStructureFields(cID)
            .subscribe(function (res) {
            console.log("structure fields", res);
            _this.allStructureFieldsData = res.data;
            // Employment Status 
            if (_this.allStructureFieldsData['Employment Status'].custom.length > 0) {
                _this.allStructureFieldsData['Employment Status'].custom.forEach(function (element) {
                    _this.allEmployementStatusArray.push(element);
                    console.log("22222111111", _this.allEmployementStatusArray);
                });
            }
            if (_this.allStructureFieldsData['Employment Status'].default.length > 0) {
                _this.allStructureFieldsData['Employment Status'].default.forEach(function (element) {
                    _this.allEmployementStatusArray.push(element);
                });
                console.log("22222", _this.allEmployementStatusArray);
            }
            if (_this.allStructureFieldsData['Job Title'].custom.length > 0) {
                _this.allStructureFieldsData['Job Title'].custom.forEach(function (element) {
                    _this.alljobTitleArray.push(element);
                    console.log("22222111111", _this.alljobTitleArray);
                });
            }
            if (_this.allStructureFieldsData['Job Title'].default.length > 0) {
                _this.allStructureFieldsData['Job Title'].default.forEach(function (element) {
                    _this.alljobTitleArray.push(element);
                });
                console.log("22222", _this.alljobTitleArray);
            }
            if (_this.allStructureFieldsData['Business Unit'].custom.length > 0) {
                _this.allStructureFieldsData['Business Unit'].custom.forEach(function (element) {
                    _this.allbusinessUnitData.push(element);
                    console.log("22222111111", _this.allbusinessUnitData);
                });
            }
            if (_this.allStructureFieldsData['Business Unit'].default.length > 0) {
                _this.allStructureFieldsData['Business Unit'].default.forEach(function (element) {
                    _this.allbusinessUnitData.push(element);
                });
                console.log("22222", _this.allbusinessUnitData);
            }
            if (_this.allStructureFieldsData['Department'].custom.length > 0) {
                _this.allStructureFieldsData['Department'].custom.forEach(function (element) {
                    _this.allDepartmentData.push(element);
                    console.log("22222111111", _this.allDepartmentData);
                });
            }
            if (_this.allStructureFieldsData['Department'].default.length > 0) {
                _this.allStructureFieldsData['Department'].default.forEach(function (element) {
                    _this.allDepartmentData.push(element);
                });
                console.log("22222", _this.allDepartmentData);
            }
            if (_this.allStructureFieldsData['Union'].custom.length > 0) {
                _this.allStructureFieldsData['Union'].custom.forEach(function (element) {
                    _this.allUnionData.push(element);
                    console.log("22222111111", _this.allUnionData);
                });
            }
            if (_this.allStructureFieldsData['Union'].default.length > 0) {
                _this.allStructureFieldsData['Union'].default.forEach(function (element) {
                    _this.allUnionData.push(element);
                });
                console.log("22222", _this.allUnionData);
            }
            if (_this.allStructureFieldsData['Location'].custom.length > 0) {
                _this.allStructureFieldsData['Location'].custom.forEach(function (element) {
                    _this.allLocationData.push(element);
                    console.log("22222111111", _this.allLocationData);
                });
            }
            if (_this.allStructureFieldsData['Location'].default.length > 0) {
                _this.allStructureFieldsData['Location'].default.forEach(function (element) {
                    _this.allLocationData.push(element);
                });
                console.log("22222", _this.allLocationData);
            }
        });
    };
    /* Description: getting employees list
     author : vipin reddy */
    JobComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
            _this.employeeData.forEach(function (element) {
                if (element._id == _this.jobData.HR_Contact) {
                    _this.HRcontactDetails.push(element);
                    _this.jobDetailsForm.get('HRContact').patchValue(_this.HRcontactDetails[0].email);
                }
            });
            console.log(_this.HRcontactDetails);
            _this.employeeData.forEach(function (element) {
                if (element._id == _this.jobData.ReportsTo) {
                    _this.ManagerContactDetails.push(element);
                    _this.jobDetailsForm.get('managerContact').patchValue(_this.ManagerContactDetails[0].email);
                }
            });
            console.log(_this.ManagerContactDetails);
        }, function (err) {
            console.log(err);
        });
    };
    JobComponent.prototype.getStandardStructureField = function () {
        var _this = this;
        var cID = JSON.parse(localStorage.getItem('companyId'));
        this.onboardingService.getStructureFields(cID)
            .subscribe(function (res) {
            console.log("get rehirestatus", res);
            _this.rehireStatus = res.data['Rehire Status'];
            if (_this.rehireStatus.custom.length > 0) {
                _this.rehireStatus.custom.forEach(function (element) {
                    _this.rehireArray.push(element);
                });
            }
            if (_this.rehireStatus.default.length > 0) {
                _this.rehireStatus.default.forEach(function (element) {
                    _this.rehireArray.push(element);
                });
            }
            console.log("22222union", _this.rehireArray);
        }, function (err) {
            console.log(err);
        });
    };
    JobComponent.prototype.empStatusChange = function (event) {
        console.log(event, this.newStatus.empStatus);
    };
    JobComponent.prototype.updateEmployeeStatusInfo = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var postdata = {
            companyId: cId,
            userId: this.employeeId,
            status: this.newStatus.empStatus,
            effective_date: this.newEffectivedate.effectDate
        };
        console.log(postdata);
        this.myInfoService.updateEmployeInfoHistory(postdata)
            .subscribe(function (res) {
            console.log("get update emp status", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employee status history", res.message, "success");
                jQuery("#myModal").modal("hide");
                _this.getThisUser(cId);
            }
        }, function (err) {
        });
    };
    JobComponent.prototype.updateJobInfo = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        // var postdata = {
        //   companyId: cId,
        //   userId: this.employeeId,
        //   jobTitle: '',
        //   department: '',
        //   businessUnit: '',
        //   unionFields: '',
        //   location: '',
        //   // reportingLocation: ,
        //   ReportsTo: '',
        //   changeReason: '',
        //   remoteWork: '',
        //   effective_date: this.effectiveDateJobUpdate.toISOString(),
        //   notes: ''
        // }
        console.log(this.updateJobData);
        this.updateJobData.companyId = cId,
            this.updateJobData.userId = this.employeeId,
            console.log(this.updateJobData.jobTitle);
        this.myInfoService.updateJobInfoHistory(this.updateJobData)
            .subscribe(function (res) {
            console.log("get update job info history", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employee status history", res.message, "success");
                jQuery("#myModal2").modal("hide");
                _this.getThisUser(cId);
            }
        }, function (err) {
        });
    };
    JobComponent.prototype.getThisUser = function (cId) {
        var _this = this;
        this.myInfoService.getEmpJobInfo(cId, this.employeeId)
            .subscribe(function (res) {
            console.log("get job total tab", res);
            _this.jobData = res.data.job;
            _this.jobInfoHistory = _this.jobData.job_information_history;
            _this.employeeStatusHistory = _this.jobData.employment_status_history;
            console.log("jobDaata", _this.jobData, _this.jobInfoHistory);
            console.log(_this.jobData.HR_Contact);
            console.log(_this.jobData.ReportsTo);
            if (_this.jobData) {
                console.log(new Date(_this.jobData.hireDate));
                _this.dt = new Date(_this.jobData.hireDate);
                _this.jobDetailsForm.patchValue(_this.jobData);
                _this.jobDetailsForm.patchValue({ hireDate: _this.dt });
                _this.jobDetailsForm.get('reHireDate').patchValue(new Date(_this.jobData.reHireDate));
                _this.jobDetailsForm.get('terminationDate').patchValue(new Date(_this.jobData.terminationDate));
                console.log("jobDaata1", _this.jobDetailsForm.value, _this.jobData);
                _this.employeeSearch();
            }
        }, function (err) {
            console.log(err);
        });
    };
    // Author:vipin reddy
    //Description: getting all custom roles
    JobComponent.prototype.getAllroles = function () {
        var _this = this;
        var cID = localStorage.getItem('companyId');
        console.log("cid", cID);
        cID = cID.replace(/^"|"$/g, "");
        console.log("cin2", cID);
        this.siteAccessRolesService.companyId(cID)
            .subscribe(function (res) {
            console.log("get ROles", res);
            _this.companyRoles = res.data;
            var result = _this.companyRoles.map(function (el) {
                var o = Object.assign({}, el);
                o.isActive = false;
                return o;
            });
            _this.companyRoles = result;
            console.log(_this.companyRoles, 'custom create');
        }, function (err) {
            console.log(err);
        });
    };
    // Author:vipin reddy
    //Description: getting dropdowns fields data from structure
    JobComponent.prototype.getAllJobDetails = function () {
        var _this = this;
        this.onboardingService.getAllJobDetails()
            .subscribe(function (res) {
            console.log("Get the job Compensations", res);
            _this.business = res.data['Business Unit'];
            _this.department = res.data['Department'];
            _this.eeoJob = res.data['EEO Job Category'];
            _this.empType = res.data['Employee Type'];
            _this.flsaCode = res.data['FLSA Code'];
            _this.jobTitle = res.data['Job Title'];
            _this.location = res.data['Location'];
            _this.frequency = res.data['Pay Frequency'];
            _this.payType = res.data['Pay Type'];
            _this.union = res.data['Union'];
            _this.assignEmployee = res['settings']['idNumber'];
        }, function (err) {
        });
    };
    // Author:Saiprakash, Date:09/04/2019
    //initializing job details form
    JobComponent.prototype.initJobDetailsForm = function () {
        this.jobDetailsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            employeeId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            employeeType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            // employeeSelfServiceId: new FormControl(""),
            HRContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            location: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            reportingLocation: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            hireDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            reHireDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            managerContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            terminationDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            terminationReason: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            rehireStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            archiveStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            FLSA_Code: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            EEO_Job_Category: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            VendorId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            Specific_Workflow_Approval: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            Site_AccessRole: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
        });
    };
    // Author:Saiprakash, Date:10/04/2019
    // Get single tab settings for job
    JobComponent.prototype.getSingleTabSettingForJob = function () {
        var _this = this;
        this.myInfoService.getSingleTabSettingForMyinfo(this.tabName).subscribe(function (res) {
            console.log(res);
            _this.fieldsShow = res.data.fields;
            if (_this.fieldsShow.Employee_Id.format == 'Alpha') {
                _this.type = 'text';
            }
            else if (_this.fieldsShow.Employee_Id.format == "AlphaNumeric") {
                _this.type = 'text';
            }
            else if (_this.fieldsShow.Employee_Id.format == "Numeric") {
                _this.type = 'number';
            }
            console.log(_this.fieldsShow);
        }, function (err) {
            console.log(err);
        });
    };
    JobComponent.prototype.customPattern = function () {
        if (this.fieldsShow.Employee_Id.format == 'Alpha') {
            return "[1-9][0-9]";
        }
    };
    JobComponent.prototype.saveJobDetails = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        console.log(this.jobDetailsForm.value);
        var finalObj = this.jobDetailsForm.value;
        finalObj.unionFields = this.jobData.unionFields;
        finalObj.jobTitle = this.jobData.jobTitle;
        finalObj.department = this.jobData.department;
        finalObj.businessUnit = this.jobData.businessUnit;
        // var businessUnit = this.jobData.businessUnit
        // this.jobDetailsForm.setValue(businessUnit)
        // finalObj._id = this.employeeId
        console.log(finalObj, "vipin");
        var postdata = {
            _id: this.employeeId,
            job: {}
        };
        postdata.job = Object.assign(this.jobData, finalObj);
        console.log(postdata, "121212121212121");
        if (this.jobDetailsForm.valid) {
            this.myInfoService.updateUSer(postdata)
                .subscribe(function (res) {
                _this.getThisUser(cId);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employee job history", res.message, "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.mesage, '', 'error');
            });
        }
        else {
            this.jobDetailsForm.get('employeeId').markAsTouched();
            this.jobDetailsForm.get('employeeType').markAsTouched();
            // this.jobDetailsForm.get('employeeSelfServiceId').markAsTouched();
            this.jobDetailsForm.get('HRContact').markAsTouched();
            this.jobDetailsForm.get('location').markAsTouched();
            this.jobDetailsForm.get('reportingLocation').markAsTouched();
            this.jobDetailsForm.get('hireDate').markAsTouched();
            this.jobDetailsForm.get('reHireDate').markAsTouched();
            this.jobDetailsForm.get('managerContact').markAsTouched();
            this.jobDetailsForm.get('terminationDate').markAsTouched();
            this.jobDetailsForm.get('terminationReason').markAsTouched();
            this.jobDetailsForm.get('rehireStatus').markAsTouched();
            this.jobDetailsForm.get('archiveStatus').markAsTouched();
            this.jobDetailsForm.get('FLSA_Code').markAsTouched();
            this.jobDetailsForm.get('EEO_Job_Category').markAsTouched();
            this.jobDetailsForm.get('VendorId').markAsTouched();
            this.jobDetailsForm.get('Specific_Workflow_Approval').markAsTouched();
            this.jobDetailsForm.get('Site_AccessRole').markAsTouched();
        }
    };
    JobComponent.prototype.cancelJob = function () {
        this.router.navigate(['/admin/admin-dashboard/employee-management/profile/profile-view/personal']);
    };
    JobComponent.prototype.detailEmployee = function () {
        this.individualEmployee = !this.individualEmployee;
        this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
    };
    JobComponent.prototype.hideEmployeeData = function () {
        this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
        this.individualEmployee = !this.individualEmployee;
    };
    JobComponent.prototype.detailEmployeefly = function () {
        this.individualEmployeefly = !this.individualEmployeefly;
        this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
    };
    JobComponent.prototype.hideEmployeeflyData = function () {
        this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
        this.individualEmployeefly = !this.individualEmployeefly;
    };
    /* Description:accept only alphabets
    author : vipin reddy */
    JobComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 &&
            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123);
    };
    JobComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/emergency"]);
    };
    JobComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    JobComponent.prototype.omit_special_char = function (event) {
        var k;
        k = event.charCode; //         k = event.keyCode;  (Both can be used)
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    };
    JobComponent.prototype.hireDateChange = function () {
        console.log(this.jobDetailsForm.get('hireDate').value);
        this.jobHireData = this.jobDetailsForm.get('hireDate').value;
    };
    JobComponent.prototype.reHireDateChange = function () {
        console.log(this.jobDetailsForm.get('reHireDate').value);
        this.jobReHireData = this.jobDetailsForm.get('reHireDate').value;
    };
    JobComponent.prototype.terminationDateChange = function () {
        console.log(this.jobDetailsForm.get('terminationDate').value);
        this.jobTerminationData = this.jobDetailsForm.get('terminationDate').value;
    };
    JobComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job',
            template: __webpack_require__(/*! ./job.component.html */ "./src/app/admin-dashboard/employee-management/job/job/job.component.html"),
            styles: [__webpack_require__(/*! ./job.component.css */ "./src/app/admin-dashboard/employee-management/job/job/job.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_3__["OnboardingService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__["SiteAccessRolesService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], JobComponent);
    return JobComponent;
}());



/***/ }),

/***/ "./src/app/services/onboarding.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/onboarding.service.ts ***!
  \************************************************/
/*! exports provided: OnboardingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingService", function() { return OnboardingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OnboardingService = /** @class */ (function () {
    function OnboardingService(http) {
        this.http = http;
    }
    OnboardingService.prototype.getStandardOnboardingtemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.editDataSubit = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/editTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.addCustomOnboardTemplate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/addEditTemplate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllCustomTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getCustomTemplates")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getStandardTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getStandardTemplate")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.deleteCategory = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllJobDetails = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getFieldsForAddEmployee");
    };
    OnboardingService.prototype.selectOnboardTemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id);
    };
    OnboardingService.prototype.getModulesData = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplates", data);
    };
    OnboardingService.prototype.deleteTemplate = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTemplate", id);
    };
    OnboardingService.prototype.standardNewHireAllData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addEmployee", data);
    };
    OnboardingService.prototype.newHireHrSummaryReviewData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addUpdateCustomWizard", data);
    };
    OnboardingService.prototype.getAllNewHireHRData = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getAllCustomWizards");
    };
    OnboardingService.prototype.customNewHrDelete = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/deleteCustomWizards", id);
    };
    OnboardingService.prototype.getCustomNewHireWizard = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getCustomWizard/" + id);
    };
    OnboardingService.prototype.getSiteAccesRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id);
    };
    OnboardingService.prototype.getStructureFields = function (cID) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + cID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.preEmployeeIdGeneration = function (cId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getEmployeeId/" + cId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllWorkSchedule = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/listPositionSchedules", { params: data });
    };
    OnboardingService.prototype.standardScheduleData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getempschedule", data);
    };
    OnboardingService.prototype.createEmployeeData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/employeespecific", data);
    };
    OnboardingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OnboardingService);
    return OnboardingService;
}());



/***/ }),

/***/ "./src/app/services/site-access-roles-service.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/site-access-roles-service.service.ts ***!
  \***************************************************************/
/*! exports provided: SiteAccessRolesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteAccessRolesService", function() { return SiteAccessRolesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SiteAccessRolesService = /** @class */ (function () {
    function SiteAccessRolesService(http) {
        this.http = http;
    }
    SiteAccessRolesService.prototype.getStandardOnboardingtemplate = function (id) {
        // return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
        // .pipe(map(res => res));
    };
    SiteAccessRolesService.prototype.getAllEmployees = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/searchEmployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getuserRoleEmployees = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/users/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.chooseEmployeesData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/chooseEmployeesForRoles", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.postEmpDatatoRole = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addGroupsToUsers", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAlltabsData = function (cID, rID, category) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/get/" + cID + "/" + rID + "/" + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.UpdateRoleType = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getBaseRoles = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/baseroles")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.createRoleType = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/addUpdate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.allPagesData = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/all/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllPagesForCompany = function (cid, rid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/" + cid + "/" + rid)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getSingleCustomRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/role/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.pagesCreation = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/create/all", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.pagesUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/all/pages", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.personalFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.jobFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.emergencyFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.compensationFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.postpersonalfields = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/create", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.updateRoles = function (data) {
        console.log("seerveice array", data);
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.updateMultipleRoles = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/all/pages", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.companyId = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.deleteRole = function (id) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/delete/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllMyinfoFields = function (roleid, cId, category) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/get/" + cId + "/" + roleid + "/" + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesforRoles = function (cId, Rid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/users-under-group/" + cId + "/" + Rid)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesUnderManager = function (cId, uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/getAllDirectReports/" + cId + "/" + uId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesforCustomRoles = function (rId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/userIds/" + rId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.changeSiteAccessRoleforEmp = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/change-user-role", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SiteAccessRolesService);
    return SiteAccessRolesService;
}());



/***/ })

}]);
//# sourceMappingURL=employee-profile-view-employee-profile-view-module~job-job-module.js.map