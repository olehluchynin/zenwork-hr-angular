(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["landing-page-landing-page-module"],{

/***/ "./src/app/landing-page/about/about.component.css":
/*!********************************************************!*\
  !*** ./src/app/landing-page/about/about.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.about.banner { height:700px;}\n.about .banner-rgt { position: absolute; top: 0; right:0; z-index: -9; width: 50%;}\n.about .banner-rgt figure { position: relative; }\n.about .banner-rgt figure img { width: 100%; height: auto;}\n.about .banner-rgt figure span { position: absolute; top:0; right:15%; width: 60%;}\n.leadership { text-align: center;position: relative;}\n.leadership h2 { color: #658fa8; font-size: 30px; line-height: 30px; margin: 0 0 30px;}\n.leadership p { color: #757575; font-size: 18px; line-height: 27px;}\n.leadership ul { margin: 50px 0 0; width: 100%; display: inline-block;}\n.leadership ul li { margin: 0 0 60px;}\n.leadership ul li img { margin: 0 auto 20px;}\n.leadership ul li h3 { color: #658fa8; font-size: 18px; line-height: 18px; margin: 0 0 10px;}\n.leadership ul li small { color: #757575; font-size: 14px; line-height:22px; margin: 0 0 10px; display: block; text-transform: uppercase;}\n.leadership ul li a .fa{ font-size: 25px; line-height: 25px;}\n.leadership figure{ position: absolute; top: -15%; left: 0; z-index: -9;}"

/***/ }),

/***/ "./src/app/landing-page/about/about.component.html":
/*!*********************************************************!*\
  !*** ./src/app/landing-page/about/about.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"banner about\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"banner-lft col-sm-8\">\n        <h2>About Us</h2>\n        <h1>Creating a world <br>\n          where work empowers a better life</h1>\n        <small>We believe great businesses treat their employees like people, not ID numbers —and we’re <br>\n          building a platform that empowers organizations to do just that.</small>\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n      </div>\n      <div class=\"banner-rgt pull-right\">\n        <figure><img src=\"../../../assets/images/home/about-bg.png\" width=\"1052\" height=\"1038\" alt=\"img\">\n          <span><img src=\"../../../assets/images/home/about-img.png\" width=\"722\" height=\"912\" alt=\"img\"></span>\n        </figure>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div> -->\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n<div #content class=\"leadership\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/leader-bg.png\" width=\"492\" height=\"1233\" alt=\"img\"></figure>\n      <h2>Our Leadership</h2>\n      <p>Our executive team brings deep knowledge and experience from some of the biggest <br>\n        companies in technology and consulting.</p>\n\n      <ul>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/leadership2.png\" width=\"236\" height=\"260\" alt=\"img\">\n          <h3>SANJEEV SINGH</h3>\n          <small>FOUNDER & CEO (EX-ADP), <br>\n            HARVARD</small>\n          <a target=\"_blank\" href=\"https://www.linkedin.com/in/sanjeev-k-singh-43380325\"><i class=\"fa fa-linkedin-square\" aria-hidden=\"true\"></i></a>\n        </li>\n\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/Rekha.png\" width=\"245\" height=\"260\" alt=\"img\">\n          <h3>REKHA ROY SINGH</h3>\n          <small>CO-FOUNDER & CTO (EX-IBM), <br>\n            OCU</small>\n          <!-- <a href=\"#\"><i class=\"fa fa-linkedin-square\" aria-hidden=\"true\"></i></a> -->\n        </li>\n\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/Ed.png\" width=\"245\" height=\"260\" alt=\"img\">\n          <h3>EDWARD PRATT</h3>\n          <small>COO (EX-ADP), <br>\n            UNC G</small>\n          <a target=\"_blank\" href=\"https://www.linkedin.com/in/edwardprattjr\"><i class=\"fa fa-linkedin-square\" aria-hidden=\"true\"></i></a>\n        </li>\n\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/leadership3.png\" width=\"245\" height=\"260\" alt=\"img\">\n          <h3>NICHOLAS BONDURANT</h3>\n          <small>DIRECTOR - PRODUCTS, <br>\n            SPHR (EX-ADP), UMW</small>\n          <a target=\"_blank\" href=\"https://www.linkedin.com/in/nicholas-bondurant-0850763\"><i class=\"fa fa-linkedin-square\" aria-hidden=\"true\"></i></a>\n        </li>\n\n\n\n\n\n      </ul>\n      <div class=\"clearfix\"></div>\n\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/landing-page/about/about.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/landing-page/about/about.component.ts ***!
  \*******************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutComponent = /** @class */ (function () {
    function AboutComponent(auth) {
        this.auth = auth;
    }
    AboutComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    AboutComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    AboutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! ./about.component.html */ "./src/app/landing-page/about/about.component.html"),
            styles: [__webpack_require__(/*! ./about.component.css */ "./src/app/landing-page/about/about.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/benefits/benefits.component.css":
/*!**************************************************************!*\
  !*** ./src/app/landing-page/benefits/benefits.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/benefits/benefits.component.html":
/*!***************************************************************!*\
  !*** ./src/app/landing-page/benefits/benefits.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Benefits Administration</h2>\n        <h1>I'll sign up for that!</h1>\n        <small>Build plans and use our Open Enrollment tools to make benefits a breeze! <br>Capture Employee NPS\n          feedback to learn what benefits matter most to your team.</small>\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/benefits-img.png\" height=\"534\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- <div id=\"arrow-down\">\n      <i class=\"fa fa-chevron-down\"  (click)=\"arrowDown(content)\" ></i>\n    </div> -->\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n     \n      <p>Our Benefits Administration module makes plan management a piece of cake. Breakdown your open enrollment by\n        employees that are just getting started, employees that are in progress, and employees that have been completed.\n        Create <b>benefit eligibility groups </b>to determine employee eligibility, including eligibility for\n        grandfathered\n        plans. With our <b>open enrollment wizard,</b> you can test and preview any changes for the upcoming year before\n        publishing it. You can ensure enrollment works exactly as it should. Onboarding new employees into benefit\n        enrollments has never been easier!</p>\n      <p>Which plans do your employees appreciate? Which plans need further explanation? Use our built-in tools to\n        <b>capture NPS feedback</b> from your employees. Gather survey information from employees and take a data-driven\n        approach to benefits administration.</p>\n      \n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/benefits/benefits.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/landing-page/benefits/benefits.component.ts ***!
  \*************************************************************/
/*! exports provided: BenefitsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BenefitsComponent", function() { return BenefitsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BenefitsComponent = /** @class */ (function () {
    function BenefitsComponent(auth) {
        this.auth = auth;
    }
    BenefitsComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    BenefitsComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    BenefitsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-benefits',
            template: __webpack_require__(/*! ./benefits.component.html */ "./src/app/landing-page/benefits/benefits.component.html"),
            styles: [__webpack_require__(/*! ./benefits.component.css */ "./src/app/landing-page/benefits/benefits.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], BenefitsComponent);
    return BenefitsComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/company-setting/company-setting.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/landing-page/company-setting/company-setting.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/company-setting/company-setting.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/landing-page/company-setting/company-setting.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave company-mobile\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Company Settings</h2>\n        <h1>I’ll take a little from column A<br>\n          and a little from column B!\n        </h1>\n\n        <small>Make our system your system! Adjustable Theme settings to make the site look like your company. Site\n          Access Rights & Management allow you to determine what your employees can do within the site. Mobile Access so\n          that key data is always at your fingertips.</small>\n\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/settings-img.png\" width=\"618\" height=\"583\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div>\n   -->\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <p>Customize our system and turn it into your system! Adjustable theme settings personalize Zenwork HR to fit your\n        company’s unique style. Site access management allows you to determine what your employees have access to and\n        mobile access keeps key data at your fingertips.\n      </p>\n      <p>With easily <b>customizable workflows and adjustable theme settings,</b> personalize our system into your\n            system! Use <b>site access and rights management</b> to control the level of access that you as an admin\n            grant to employees and managers. We offer the ability to build custom workflows to grant specific users\n            approval authority, even to employees that are not directly linked.\n      </p>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/company-setting/company-setting.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/landing-page/company-setting/company-setting.component.ts ***!
  \***************************************************************************/
/*! exports provided: CompanySettingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanySettingComponent", function() { return CompanySettingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CompanySettingComponent = /** @class */ (function () {
    function CompanySettingComponent(auth) {
        this.auth = auth;
    }
    CompanySettingComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    CompanySettingComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    CompanySettingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-company-setting',
            template: __webpack_require__(/*! ./company-setting.component.html */ "./src/app/landing-page/company-setting/company-setting.component.html"),
            styles: [__webpack_require__(/*! ./company-setting.component.css */ "./src/app/landing-page/company-setting/company-setting.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], CompanySettingComponent);
    return CompanySettingComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/contact/contact.component.css":
/*!************************************************************!*\
  !*** ./src/app/landing-page/contact/contact.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.contact-banner.banner{ height: 600px;}\n\n.contact-banner .banner-lft { padding:0;}\n\n.contact-banner .banner-lft figure { position: absolute; top: 0; left: 0; z-index: -9;}\n\n.contact-banner .banner-lft figure img { width: 100%; height: auto;}\n\n.contact-banner .banner-rgt { position: absolute; top:23%; right:6%; z-index: -9; width:30%;}\n\n.contact-banner .banner-rgt figure { position: relative;}\n\n.contact-banner .banner-rgt figure img { width: 100%; height: auto;}\n\n.address-block { text-align: center; padding: 0 0 100px; position: relative;}\n\n.address-block::after { content: ''; position: absolute; top:-50px; left: 0; background:url('contact-lft-bg.png') no-repeat top center; width: 352px; height: 1217px;}\n\n.address-block-lft { padding: 0;}\n\n.address-block-lft img{ margin: 0 auto 25px;}\n\n.address-block-lft h2 { color: #ffc550; font-size: 28px; line-height:28px; margin: 0 0 30px;}\n\n.address-block-lft h3 { color: #009040; font-size: 28px; line-height:28px; margin: 0 0 30px;}\n\n.address-block-lft h4 { color: #009040; font-size: 15px; line-height:15px; margin: 0 0 10px;}\n\n.address-block-lft address { margin: 0;}\n\n.address-block-lft address p { color:#757575; font-size: 15px; line-height: 24px;}\n\n.address-block-lft a.mail{ color:#0064ce; font-size: 16px; line-height: 15px;}\n\n.address-block-lft a{ color:#313131; font-size: 15px; line-height: 15px; padding: 0 0 10px; display: inline-block;}\n\n.address-block-lft a.btn {border: #009040 1px solid;color: #009040;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 50px;\n    line-height: 16px; margin: 0 0 20px;}\n\n.address-block-lft ul { margin:10px 0 0;}\n\n.address-block-lft ul li{ display:inline-block; margin: 0 15px 0 0;}\n\n.address-block-lft ul li a{ display: inline-block;}\n\n.address-block-lft ul li a .fa{ font-size: 20px; line-height: 20px; color:#c2c2c2; }\n\n.address-block-lft ul li a .fa:hover{ color:#ffc550;}\n\n.address-block-img { position: relative;}\n\n.address-block-img::after { content:''; position: absolute; top:-50px; right:-90px; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXEAAAFxCAYAAACFh5ikAAAABHNCSVQICAgIfAhkiAAABUdJREFUeJzt1LENwjAAAEGYiTYFG7AqC9mWB4nEDKGJXrqb4Kt/PgDI2Xu/zvN8390BwEVrrc+c83t3BwAXGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQOEjTGOuxsA+M8PCuzwxLWQjuMAAAAASUVORK5CYII=) no-repeat top center; width: 369px; height: 369px;-webkit-transform: rotateY(40deg);transform: rotateY(40deg);}\n\n.contacts {\n    background: #fff;\n    box-shadow: 0 0px 20px 7px #dedede;\n    margin: 0 auto 70px 20px;\n    padding: 50px 0;\n    text-align: left;\n    position: relative;\n    z-index: 999;\n    }\n\n.contacts-lft { padding: 0 20px; }\n\n.contacts-lft h2 { color: #658fa8; font-size: 28px; line-height:28px; margin: 0 0 10px; font-weight: bold; text-align: left;}\n\n.contacts-lft small { color: #757575; font-size:15px; line-height:20px; margin: 0 0 30px; text-align: left; display: block;}\n\n.contacts-lft ul { text-align: left;}\n\n.contacts-lft ul li { margin: 0 0 20px;}\n\n.contacts-lft ul li .form-control { height: auto; padding: 9px 12px; resize: none;}\n\n.contacts-lft a.btn {background: #ffc550;color: #313131;font-size: 16px;font-weight: 400;border-radius: 20px;padding: 10px 35px;line-height: 16px;}\n\n.contacts-lft ul li select { color: #8e8c8c;}\n\n.contacts-rgt {background: #1daebb; border-radius:5px; text-align: left; position:absolute; top:30%; right:-100px;}\n\n.contacts-rgt h2 { color: #fff; font-size: 27px; line-height:37px; margin: 0 0 50px; font-weight: bold;}\n\n.contacts-rgt p { color: #fff; font-size: 16px; line-height:25px; margin: 0 0 50px;}\n\n.contacts-rgt-in { position: relative;padding:50px;}\n\n.contacts-rgt-in ul { padding: 0 0 60px; border-bottom:#fff 1px solid; margin: 0;}\n\n.contacts-rgt-in ul li { display: block; margin: 0 0 15px;}\n\n.contacts-rgt-in ul li span{ float: left; width:45px; margin: 3px 0 0;}\n\n.contacts-rgt-in > ul > li > p { color: #fff; font-size: 16px; line-height:25px; margin: 0; float: left;}\n\n.contacts-rgt-in > ul > li > p > a { color:#fff;}\n\n.contacts-rgt-in address p { margin: 0; float: left;}\n\n.contacts-rgt-in figure { position: absolute; top: 0; right:50px;}\n\n.mail-id{ float: left;}\n\n.mail-id p {color: #fff; font-size: 16px; line-height:25px; margin: 0;}\n\n.mail-id p a{ color:#fff;}\n\n.mail-id p a:hover{ text-decoration: underline;}\n\n.social-icons { padding:20px 0 0 50px;}\n\n.social-icons ul { margin:10px 0 0; padding: 0; border: none;}\n\n.social-icons ul li{ display:inline-block; margin: 0 20px 0 0;}\n\n.social-icons ul li a{ display: inline-block;}\n\n.social-icons ul li a .fa{ font-size: 20px; line-height: 20px; color:#0e828c; }\n\n.social-icons ul li a .fa:hover{ color:#ffc550;}\n\n#arrow-down { margin: 0 auto 100px !important;}\n\n.btm-block { display: none ;}"

/***/ }),

/***/ "./src/app/landing-page/contact/contact.component.html":
/*!*************************************************************!*\
  !*** ./src/app/landing-page/contact/contact.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"banner contact-banner\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"banner-lft\">\n        <figure><img src=\"../../../assets/images/home/contact-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n        <div class=\"col-sm-8\">\n          <h2>Contact Us</h2>\n          <h1>\n            Like what you hear? Want to hear more?<br>\n            Please contact us.\n          </h1>\n          <!-- <small>We believe great businesses treat their employees like people, not ID numbers —and we’re <br>\n                    building a platform that empowers organizations to do just that.</small> -->\n          <!-- <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a> -->\n        </div>\n\n      </div>\n      <div class=\"clearfix\"></div>\n\n      <div class=\"banner-rgt pull-right\">\n          <figure><img src=\"../../../assets/images/home/contact-img.png\" width=\"618\" height=\"520\" alt=\"img\"></figure>\n        </div>\n        \n      <div class=\"contacts col-lg-5 col-md-6 col-sm-8 col-xs-10 pull-left\">\n\n            <div class=\"contacts-lft  col-xs-12\">\n              <h2>SEND US A MESSAGE.</h2>\n              <small>Please reach out to us if you have any questions or <br>\n                need any additional information.</small>\n\n              <ul>\n                <li>\n                  <div class=\"form-group\">\n                    <input [(ngModel)]=\"contact.contactName\" class=\"form-control\" required name=\"contactname\" #contactname=\"ngModel\" placeholder=\"Contact Name\">\n                  </div>\n                  <!-- <span *ngIf=\"!contact.contactName && contactname.touched || (!contact.contactName && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Please\n                  enter name</span> -->\n                </li>\n                <li>\n                  <div class=\"form-group\">\n                    <input [(ngModel)]=\"contact.companyName\" class=\"form-control\" required name=\"companyName\" #companyName=\"ngModel\" placeholder=\"Company Name\">\n                  </div>\n                  <!-- <span *ngIf=\"!contact.companyName && companyName.touched || (!contact.companyName && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Please\n                  enter company name</span> -->\n                </li>\n\n                <li>\n                  <div class=\"form-group\">\n                    <input [(ngModel)]=\"contact.companySize\" (keypress)=\"keyPress($event)\" class=\"form-control\" name=\"companysize\" placeholder=\"Company Size\" required #companysize=\"ngModel\">\n                  </div>\n                  <!-- <span *ngIf=\"!contact.companySize && companysize.touched || (!contact.companySize && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Please\n                  enter company size</span> -->\n                </li>\n\n                <li>\n                  <div class=\"form-group\">\n                    <input [(ngModel)]=\"contact.email\" placeholder=\" Email\" class=\"form-control\" required name=\"billingEmail\" #billingEmail=\"ngModel\"\n                    pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n                    \n                    \n                  </div>\n                  <!-- <span *ngIf=\"!contact.email && billingEmail.touched || (!contact.email && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Please\n                  enter email</span> -->\n                </li>\n\n                <li>\n                  <div class=\"form-group\">\n                    <input [(ngModel)]=\"contact.phNo\" name=\"workPhone\" class=\"form-control\" required\n                    #workPhone=\"ngModel\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10 \n                    placeholder=\"Phone\">\n                  </div>\n                  <!-- <span *ngIf=\"!contact.phNo && workPhone.touched || (!contact.phNo && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Please\n                  enter phone</span> -->\n                </li>\n\n                <li>\n                  <div class=\"form-group\">\n\n                    <textarea [(ngModel)]=\"contact.message\" class=\"form-control\" rows=\"5\"\n                      placeholder=\"Tell us about you, your business and requirements of a new project you want to start with us!\"></textarea>\n                  </div>\n                </li>\n\n              </ul>\n              <a class=\"btn\" (click)=\"contactForm()\" >Submit</a>\n\n            </div>\n            \n            <!-- \n             <div class=\"contacts-rgt col-lg-5 col-sm-6 col-xs-11\">\n                 <div class=\"contacts-rgt-in\">\n                 \n                  <h2>DROP BY <br>\n                      OUR OFFICE.</h2>\n                  <p>Our office is located in a beau ful building inside <br>\n                      the busiest and fast-growing city.</p>\n      \n                      <figure><img src=\"../../../assets/images/home/star.png\" width=\"61\" height=\"116\" alt=\"img\"></figure>\n                  <ul>\n                    <li>\n                       <span><img src=\"../../../assets/images/home/address.png\" width=\"21\" height=\"20\" alt=\"img\"></span>\n                       <address><p>1 East Center Street, #250, <br>\n                          Faye eville, AR 72701</p></address>\n                          <div class=\"clearfix\"></div>\n                    </li>\n                    <li>\n                          <span><img src=\"../../../assets/images/home/mail.png\" width=\"20\" height=\"13\" alt=\"img\"></span>\n                          <div class=\"mail-id\">\n                              <p><a href=\"mail-to:info@zenworkhr.com\">info@zenworkhr.com</a></p> \n                              <p><a href=\"mail-to:sales@zenworkhr.com\">sales@zenworkhr.com</a></p>\n                          </div>\n                          \n                          <div class=\"clearfix\"></div>\n                     </li>\n                     <li>\n                       <span><img src=\"../../../assets/images/home/phone.png\" width=\"17\" height=\"17\" alt=\"img\"></span>\n                        <p><a href=\"tel:1-877-811-3829\">1-877-811-3829</a></p>\n                        <div class=\"clearfix\"></div>\n                     </li>\n                  </ul>\n      \n                  <div class=\"social-icons\">\n                      <ul>\n                          <li><a href=\"#\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>\n                          <li><a href=\"#\"><i class=\"fa fa-linkedin\" aria-hidden=\"true\"></i></a></li>\n                          <li><a href=\"#\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>\n                          <li><a href=\"#\"><i class=\"fa fa-youtube-play\" aria-hidden=\"true\"></i></a></li>\n                      </ul>\n                  </div>\n              </div>\n              </div> -->\n            <div class=\"clearfix\"></div>\n\n\n\n      </div>\n      <!-- <div class=\"clearfix\"></div> -->\n     \n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n<!-- <div id=\"arrow-down\">\n      <i class=\"fa fa-chevron-down\"></i>\n    </div> -->\n\n<!-- <div class=\"arrow\"> <a  (click)=\"arrowDown(content)\" ><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\" height=\"41\" alt=\"img\"></a></div> -->\n\n\n<!-- <div #content class=\"address-block\">\n  <div class=\"container\">\n     <div class=\"row\">\n\n        <div class=\"address-block-lft address-block-img  col-sm-6\">\n            <img src=\"../../../assets/images/home/graph.png\" width=\"295\" height=\"50\" alt=\"img\">\n            <h3>Arkansas</h3>\n            <address><p>1 East Center Street, #250, <br>\n                Faye eville, AR 72701</p></address>\n                <a href=\"mail-to:support@zenwork.com\" class=\"mail\">support@zenwork.com</a>\n        </div>\n\n        <div class=\"address-block-lft col-sm-6\">\n          \n            <h2>Already using a Zenwork platform?</h2>\n            <a href=\"#\" class=\"btn\">Login to contact us</a>\n            <h4>Contact Sales</h4>\n            <a href=\"tel:1-877-811-3829\">1-877-811-3829</a>\n\n            <address><p>Monday-Friday / 7am-5pm (PST)</p></address>\n                <a href=\"mail-to:sales@zenwork.com\" class=\"mail\">sales@zenwork.com</a>\n            <ul>\n                <li><a href=\"#\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>\n                <li><a href=\"#\"><i class=\"fa fa-linkedin\" aria-hidden=\"true\"></i></a></li>\n                <li><a href=\"#\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>\n                <li><a href=\"#\"><i class=\"fa fa-youtube-play\" aria-hidden=\"true\"></i></a></li>\n            </ul>\n\n        </div>\n\n     </div>\n  </div>\n</div>   -->"

/***/ }),

/***/ "./src/app/landing-page/contact/contact.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/landing-page/contact/contact.component.ts ***!
  \***********************************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactComponent = /** @class */ (function () {
    function ContactComponent(auth, swalAlertService, authenticationService) {
        this.auth = auth;
        this.swalAlertService = swalAlertService;
        this.authenticationService = authenticationService;
        this.contact = {
            contactName: '',
            companyName: '',
            companySize: '',
            email: '',
            phNo: '',
            message: ''
        };
        this.isValid = false;
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.auth.changemessage("contact");
    };
    ContactComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    ContactComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    ContactComponent.prototype.contactForm = function () {
        var _this = this;
        this.isValid = true;
        console.log(this.contact);
        this.authenticationService.contactUsformSend(this.contact)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("COntact Info", res.message, "success");
                // this.isValid = false;
            }
        }, function (err) {
        });
        this.contact.contactName = '',
            this.contact.companyName = '',
            this.contact.companySize = '',
            this.contact.email = '',
            this.contact.phNo = '',
            this.contact.message = '';
    };
    ContactComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__(/*! ./contact.component.html */ "./src/app/landing-page/contact/contact.component.html"),
            styles: [__webpack_require__(/*! ./contact.component.css */ "./src/app/landing-page/contact/contact.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/em-service/em-service.component.css":
/*!******************************************************************!*\
  !*** ./src/app/landing-page/em-service/em-service.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/em-service/em-service.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/landing-page/em-service/em-service.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave em-service-mobile\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Employee Self Service & Manager Self Service</h2>\n        <h1>“Many hands make light work”- John Heywood</h1>\n\n        <small>In order to fit all your company needs, we offer different dashboard displays modified for employees and\n          managers alike. These dashboards easily guide the user through routine HR updates and tasks that save\n          administration time.</small>\n\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/em-service.png\" width=\"618\" height=\"607\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div> -->\n\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n    \n      <p>Take an empowered approach to HR by encouraging <b>managers and employees to use self-service updates.</b>\n        Control content and information updates as you see fit through our easy approval dashboards while letting our\n        system guide your staff through the tasks. In these different platforms, staff can stay updated with messages\n        from leadership, required training, and key events happening in the company. Employees and managers also access\n        the benefit of being able to view key information about the company whenever they need it. Employees and\n        managers have the help and information they request at the ready, and administrators gain more time back in\n        their day... We think that is a win-win!</p>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/em-service/em-service.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/landing-page/em-service/em-service.component.ts ***!
  \*****************************************************************/
/*! exports provided: EmServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmServiceComponent", function() { return EmServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmServiceComponent = /** @class */ (function () {
    function EmServiceComponent(auth) {
        this.auth = auth;
    }
    EmServiceComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    EmServiceComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    EmServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-em-service',
            template: __webpack_require__(/*! ./em-service.component.html */ "./src/app/landing-page/em-service/em-service.component.html"),
            styles: [__webpack_require__(/*! ./em-service.component.css */ "./src/app/landing-page/em-service/em-service.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], EmServiceComponent);
    return EmServiceComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/footer/footer.component.css":
/*!**********************************************************!*\
  !*** ./src/app/landing-page/footer/footer.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.btm-block { text-align: center; padding:100px 0; position: relative; z-index:99;}\n.btm-block small { color: #658fa8; font-size: 28px; line-height:28px; margin: 0 0 30px; display: block;}\n.btm-block a.btn {background: #ffc550;color: #313131;font-size: 16px;font-weight: 400;border-radius: 20px;padding: 10px 35px;line-height: 16px;}\n.contact { background:url('footer-top.jpg') no-repeat top center; height: 400px; width: 100%; background-size: cover; text-align: center; margin: 0 0 40px;}\n.contact img { margin: 0 auto 30px;}\n.contact h2 {color: #658fa8; font-size: 30px; line-height:30px; margin: 0 0 20px; font-weight: bold;}\n.contact small {color: #658fa8; font-size: 15px; line-height: 23px; margin: 0 0 40px;}\n.search { margin: 20px auto 0; float: none;}\n.search-lft { display: inline-block; padding: 0 0 0 55px;}\n.search-lft .form-control { border-radius: 20px; height: auto; padding:12px 40px 12px 20px; border: none; color:#000;}\n.search-lft .form-group { margin: 0;}\n.search a.btn{background: #ffc550;color: #313131;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 12px 55px;\n    line-height: 16px;  margin: 0 0 0 -30px; z-index: 99; position: relative;}\nfooter { background:#3e3e3e; padding: 50px 0 30px;}\n.footer-logo{ text-align: center;}\n.footer-logo img { margin: 0 0 30px;}\n.footer-logo a.btn{border: #ffc550 1px solid;color: #ffc550;\n    font-size: 15px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 18px;\n    line-height: 16px; margin: 0 0 25px;}\n.footer-logo a.btn:hover{ border:transparent #ffc550 1px; background:#ffc550; color:#313131;}\n.footer-logo h4{ color: #fff; font-size: 15px; line-height: 15px; margin: 0 0 8px;}\n.footer-logo p { margin: 0 0 4px;}\n.footer-logo p a { color:#ffc550;font-size: 15px; line-height: 15px;}\n.footer-logo p a:hover { border-bottom:#ffc550 1px solid; }\n.footer-nav{margin:7px 0 0;}\n.footer-nav { padding: 0 0 0 40px;}\n.footer-nav ul {display: block;}\n.footer-nav ul li { float: left; width: 20%;}\n.footer-cont { display: block;}\n.footer-cont ul li { width: auto; float: none; margin: 0 0 10px;}\n.footer-cont ul li h3{ color:#fff; border-bottom:#3e3e3e 2px solid; font-size: 18px; line-height:22px; margin: 0 0 20px; display: inline-block;  }\n/* .footer-cont ul li h3:hover { cursor: pointer; text-decoration: underline;text-decoration-color: #429348;} */\n.footer-cont ul li h3:hover{ cursor: pointer; border-bottom:#099247 2px solid; line-height: 22px;}\n.footer-cont ul li a{ color:#c1c1c1; font-size: 15px; line-height: 18px;}\n.footer-cont ul li a:hover { color:#429348;text-decoration:underline !important;}\n.social-icons.footer-cont ul { width: 100%; margin:0 0 40px;}\n.social-icons.footer-cont ul li{ display:inline-block; margin: 0 15px 0 0;}\n.social-icons.footer-cont ul li a{ display: inline-block;}\n.social-icons.footer-cont ul li a .fa{ font-size: 20px; line-height: 20px; color:#2b2b2b; }\n.social-icons.footer-cont ul li a .fa:hover{ color:#ffc550;}\n.social-icons.footer-cont ul li:nth-child(1) { display: block; margin: 0 0 30px;}\n.talk-us.footer-cont ul li a{ color:#ffc550; font-size: 15px; line-height: 17px;}\n.talk-us.footer-cont ul li a:hover { border-bottom:#ffc550 1px solid;text-decoration: none !important;}\n.copy{ padding:30px 0 0;border-top:#797777 1px solid; text-align: center; margin: 30px 0 0;}\n.copy p { color: #5f5f5f; font-size: 15px; line-height: 15px;}\n.copy p a {color: #5f5f5f;}\n.hide{\n    display: none;\n}\n.btm-block1 { padding: 180px 0;}"

/***/ }),

/***/ "./src/app/landing-page/footer/footer.component.html":
/*!***********************************************************!*\
  !*** ./src/app/landing-page/footer/footer.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"btm-block\" *ngIf=\"footerRestrict!='contact'\">\n\n  <small>Get ready to unleash the power of your workforce</small>\n  <a class=\"btn\" href=\"https://calendly.com/zenworkhr/intro\" (click)=\"about()\" target=\"_blank\">Register for a Demo</a>\n\n</div>\n\n\n\n<div class=\"btm-block1\" *ngIf=\"footerRestrict=='contact'\">\n  \n</div>\n\n\n\n\n<div class=\"contact\">\n\n  <div class=\"tbl\">\n    <div class=\"tbl-cell\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <img src=\"../../../assets/images/home/arrow.png\" width=\"\" height=\"\" alt=\"img\">\n          <h2>Subscribe to our Newsletter</h2>\n\n          <small>Enter your email address to get updates on Zenwork HR <br>\n            special price offers, products and events.</small>\n\n          <div class=\"search col-md-7 col-sm-9 col-xs-11\">\n            <div class=\"search-lft col-xs-8\">\n              <div class=\"form-group\">\n                <input type=\"email\" class=\"form-control\" placeholder=\"Enter your email address\">\n              </div>\n            </div>\n            <a routerLink=\"/sign-up\" class=\"btn pull-left\" (click)=\"about()\">Get Started</a>\n            <div class=\"clearfix\"></div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n\n\n<footer>\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <div class=\"footer-logo col-md-2 col-xs-12\">\n        <a href=\"#\"> <img src=\"../../../assets/images/main-nav/whitelogo2.png\" width=\"155\" alt=\"img\"></a>\n        <a href=\"#\" class=\"btn\">Register Now</a>\n\n        <h4>Talk to Sales</h4>\n        <p><a href=\"tel:1-877-811-3829\">1-877-811-3829</a></p>\n        <p><a href=\"mail-to:sales@zenwork.com\">info@zenworkhr.com</a></p>\n\n      </div>\n\n      <div class=\"footer-nav col-md-10 col-xs-12\">\n\n        <ul>\n\n          <li>\n            <div class=\"footer-cont\">\n              <ul>\n                <li>\n                  <h3>Platforms</h3>\n                </li>\n                <li><a href=\"#\">Zenwork HR</a></li>\n                <li><a routerLink=\"/home\" (click)=\"about()\">Practice Management</a></li>\n                <li><a href=\"https://www.tax1099.com\" target=\"_blank\">Tax1099</a></li>\n                <li><a href=\"https://www.ez2290.com\" target=\"_blank\">EZ2290</a></li>\n                <li><a href=\"https://www.fbaronline.com\" target=\"_blank\">FBAR Online</a></li>\n                <li><a href=\"https://ezextension.com\" target=\"_blank\">eZExtension</a></li>\n              </ul>\n            </div>\n\n          </li>\n\n          <li>\n            <div class=\"footer-cont\">\n              <ul>\n                <li>\n                  <h3>Learn</h3>\n                </li>\n                <li><a href=\"#\">Resources</a></li>\n                <li><a href=\"#\">Features</a></li>\n                <li><a href=\"#\">Blog</a></li>\n                <li><a href=\"#\">Events</a></li>\n                <li><a href=\"#\">Webinars</a></li>\n              </ul>\n            </div>\n\n          </li>\n\n          <li>\n            <div class=\"footer-cont\">\n              <ul>\n                <li>\n                  <h3>About Zenwork HR</h3>\n                </li>\n                <li><a routerLink=\"/about\" (click)=\"about()\">About Us</a></li>\n                <li><a routerLink=\"/pricing\" (click)=\"about()\">Pricing</a></li>\n                <li><a href=\"#\">Careers</a></li>\n\n              </ul>\n            </div>\n\n          </li>\n\n          <li>\n            <div class=\"footer-cont talk-us\">\n              <ul>\n                <li>\n                  <h3>Talk to Us</h3>\n                </li>\n                <li><a href=\"tel:1-877-811-3829\">1-877-811-3829</a></li>\n                <li><a href=\"mail-to:support@zenwork.com\">info@zenworkhr.com</a></li>\n                <li><a routerLink=\"/zenwork-login\" (click)=\"about()\">Login</a></li>\n\n              </ul>\n            </div>\n\n          </li>\n\n          <li>\n            <div class=\"footer-cont social-icons\">\n\n              <ul>\n                <li>\n                  <h3>Follow us on</h3>\n                </li>\n                <li><a href=\"#\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>\n                <li><a href=\"#\"><i class=\"fa fa-linkedin\" aria-hidden=\"true\"></i></a></li>\n                <li><a href=\"#\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>\n                <li><a href=\"#\"><i class=\"fa fa-youtube-play\" aria-hidden=\"true\"></i></a></li>\n\n              </ul>\n              <div class=\"clearfix\"></div>\n              <!-- <a href=\"#\"><img src=\"../../../assets/images/home/truste.png\" width=\"115\" height=\"33\" alt=\"\"></a> -->\n            </div>\n\n          </li>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n\n      </div>\n      <div class=\"clearfix\"></div>\n\n      <div class=\"copy\">\n        <p>2019 © Copyright | Powered by Zenwork, Inc. | All Rights Reserved | <a href=\"#\">Privacy Policy</a> |\n          <a routerLink=\"/terms\" (click)=\"about()\">Terms of Use</a> </p>\n      </div>\n    </div>\n  </div>\n</footer>"

/***/ }),

/***/ "./src/app/landing-page/footer/footer.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/landing-page/footer/footer.component.ts ***!
  \*********************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FooterComponent = /** @class */ (function () {
    function FooterComponent(router, auth) {
        var _this = this;
        this.router = router;
        this.auth = auth;
        console.log(this.router.events.pipe());
        this.auth.currentMessage.subscribe(function (data) {
            _this.footerRestrict = data;
            console.log(_this.footerRestrict);
        });
    }
    FooterComponent.prototype.ngOnInit = function () {
        var routeurl = this.router.url.split('/');
        console.log(routeurl[0] == 'contact');
        this.footerRestrict = !(routeurl[0] == 'contact');
        // this.router.events.subscribe((url: any) => console.log(url));
        // console.log(this.router.url);
        // this.url = this.router.url;
        // if (this.url == '/contact') {
        //   this.footerRestrict = true;
        // }
        // else{
        //   this.footerRestrict = false;
        // }
        console.log(this.footerRestrict);
    };
    FooterComponent.prototype.about = function () {
        window.scroll(0, 0);
    };
    FooterComponent.prototype.ngOnDestroy = function () {
        this.footerRestrict = "sum";
        console.log(this.footerRestrict);
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/landing-page/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/landing-page/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/home/home.component.css":
/*!******************************************************!*\
  !*** ./src/app/landing-page/home/home.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .banner { margin: 170px 0 0; height: 900px;}\n.banner-lft { padding: 0;}\n.banner-lft h2 { color: #099247; font-size: 25px; line-height: 25px; margin: 0 0 30px; font-weight:bold;}\n.banner-lft h1 {color: #5f5f5f; font-size: 40px; line-height: 50px; margin: 0 0 10px; font-weight: bold;}\n.banner-lft small {color: #5f5f5f; font-size: 15px; line-height: 15px; display: block; margin: 0 0 80px;}\n.banner-lft a.btn{ color: #313131;\n    background: #ffc550;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 26px;\n    line-height: 16px;}\n\n\n.banner-rgt { padding: 0;}\n.banner-rgt figure { position: absolute; top: 0; right:0; z-index: -9;}\n.banner-rgt figure img { width: 100%; height: auto;} */\n\n\n.banner { height:650px;}\n\n\n.company-modules { text-align: center; margin: 100px 0 0;}\n\n\n.company-modules h3 { color: #ffc550; font-size: 30px; line-height: 30px; margin: 0 0 10px; font-weight: 500;}\n\n\n.company-modules h4 { color: #658fa8; font-size: 25px; line-height: 25px; margin: 0 0 70px;}\n\n\n.company-modules ul{ display: block;}\n\n\n.company-modules ul li{ margin:0 0 40px; float: none; display: inline-block; vertical-align: top;}\n\n\n.company-modules ul li img { margin: 0 auto 20px;}\n\n\n.company-modules ul li h2{color: #5f5f5f; font-size: 22px; line-height:28px; margin: 0 0 20px; font-weight: 500; height: 56px;}\n\n\n.company-modules ul li p {color: #5f5f5f; font-size: 16px; line-height: 26px; margin: 0 0 30px; height: 125px;}\n\n\n.company-modules ul li a.btn {color: #5f5f5f; font-size: 15px; line-height: 14px; border:#ffc550 1px solid;\nborder-radius: 20px; padding:8px 15px;}\n\n\n.company-modules ul li a.btn:hover{ color:#ffc550; background:#5f5f5f; border:transparent 1px solid;}\n\n\n.team { position: relative; height: 1050px; text-align: center;}\n\n\n.team-lft { padding: 350px 0 0;}\n\n\n.team-lft h2 { color: #658fa8; font-size: 27px; line-height: 27px; margin: 0 0 30px;}\n\n\n.team-lft p {color: #5f5f5f; font-size: 17px; line-height:33px; margin: 0 0 50px;}\n\n\n.team-lft a.btn{ background: #ffc550;color: #313131;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 26px;\n    line-height: 16px;}\n\n\n.team-rgt     { padding: 0;position: absolute; top: 0; right: 0; width: 35%;}\n\n\n.team-rgt figure  { position: relative; }\n\n\n.team-rgt figure img { width: 100%; height: auto;}\n\n\n.team-rgt span { position: absolute; top:25%; right:30%; width:65%;}\n\n\n.why-zenwork { background:url('why-zenwork.jpg') no-repeat top center; width: 100%; height:750px; text-align: center;background-size: cover;}\n\n\n.why-zenwork .tbl-cell { padding: 50px 0 0;}\n\n\n.why-zenwork h3 { color: #658fa8; font-size: 27px; line-height: 27px; margin: 0 0 40px;}\n\n\n.why-zenwork ul{ display: block;}\n\n\n.why-zenwork ul li{ margin:0 0 40px;}\n\n\n.why-zenwork ul li img { margin: 0 auto 20px;}\n\n\n.why-zenwork ul li h2{color: #5f5f5f; font-size: 22px; line-height:28px; margin: 0 0 20px; font-weight: 500;}\n\n\n.why-zenwork ul li p {color: #5f5f5f; font-size: 17px; line-height:30px;}\n\n\n.why-zenwork a.btn {background: #ffc550;color: #313131;\n    font-size: 16px;\n    font-weight: 400;\n    border-radius: 20px;\n    padding: 10px 50px;\n    line-height: 16px;}\n\n\n.integrates {text-align: center; padding:100px 0;}\n\n\n.integrates h2 { color: #658fa8; font-size: 30px; line-height: 30px; margin: 0 0 80px;}\n\n\n.integrates ul { display:inline-block; width: 100%;}\n\n\n.integrates ul li { margin: 0 0 80px;}\n\n\n.integrates ul li a { margin: 0;}\n\n\n.integrates ul li a img { margin: 0 auto; position: relative; z-index: 99;}\n\n\n.zenworks-top { padding: 0 0 0 130px; position: relative;}\n\n\n.zenworks-top::after{ content: ''; position: absolute; top: -25%; left:210px; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXEAAAFxCAYAAACFh5ikAAAABHNCSVQICAgIfAhkiAAABUdJREFUeJzt1LENwjAAAEGYiTYFG7AqC9mWB4nEDKGJXrqb4Kt/PgDI2Xu/zvN8390BwEVrrc+c83t3BwAXGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQOEjTGOuxsA+M8PCuzwxLWQjuMAAAAASUVORK5CYII=) no-repeat top center; height: 369px; width: 369px;}\n\n\n.zenworks-btm { margin: 0 0 0 -150px; position: relative;}\n\n\n.zenworks-btm::after{ content: ''; position: absolute; top: -160px; right:220px; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXEAAAFxCAYAAACFh5ikAAAABHNCSVQICAgIfAhkiAAABUdJREFUeJzt1LENwjAAAEGYiTYFG7AqC9mWB4nEDKGJXrqb4Kt/PgDI2Xu/zvN8390BwEVrrc+c83t3BwAXGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQNEGThAlIEDRBk4QJSBA0QZOECUgQOEjTGOuxsA+M8PCuzwxLWQjuMAAAAASUVORK5CYII=) no-repeat top center; height: 369px; width: 369px;-webkit-transform: rotateY(30deg);transform: rotateY(30deg);}\n\n\n.agile-hr-rgt video{ width: 71%; height: 300px; border: none; margin: 0 0 25px;}\n\n\n/* .agile-section{\n    margin: 80px 0 0 0;\n} */"

/***/ }),

/***/ "./src/app/landing-page/home/home.component.html":
/*!*******************************************************!*\
  !*** ./src/app/landing-page/home/home.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"banner\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"banner-lft col-sm-7\">\n        <!-- <h2>What do we do?</h2> -->\n        <h1>Zenwork HR, Simplified Human Resources Solutions for All Businesses\n        </h1>\n        <small>Transform your complicated HR tasks into game-changing productivity </small>\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n      </div>\n      <div class=\"banner-rgt pull-right\">\n        <figure><img src=\"../../../assets/images/home/banner-bg.png\" width=\"1057\" height=\"828\" alt=\"img\">\n          <span><img src=\"../../../assets/images/home/banner-img.png\" width=\"661\" height=\"782\" alt=\"img\"></span>\n        </figure>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- <div id=\"arrow-down\">\n  <i class=\"fa fa-chevron-down\" (click)=\"arrowDown(agile)\"></i>\n</div> -->\n<section class=\"agile-section\">\n  <div class=\"arrow\"> <a (click)=\"arrowDown(agile)\"> <img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n        height=\"41\" alt=\"img\"></a></div>\n\n  <div #agile class=\"agile-hr\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <h2>Agile HR Experience</h2>\n        <p>Zenwork HR is a cloud-based software that quickly resolves Human Resource issues for all companies. It has\n          been designed with HR professionals in mind to speed up all aspects of the HR functions. Whether it is\n          included in the system, or designed to integrate with Zenwork HR, the software will decrease stress and\n          increase productivity.</p>\n\n        <div class=\"agile-hr-in\">\n          <div class=\"agile-hr-lft pull-left\">\n            <figure><img src=\"../../../assets/images/home/agile-hr-bg.png\" width=\"800\" height=\"1099\" alt=\"img\">\n\n              <span><img src=\"../../../assets/images/home/agile-hr-img.png\" width=\"459\" height=\"354\" alt=\"img\"></span>\n\n            </figure>\n\n          </div>\n          <div class=\"agile-hr-rgt col-sm-6 pull-right\">\n            <h3>If it works for you, it works for us!</h3>\n            <small>Customizable workflows allow companies to build any type of approval path that is needed for the HR\n              function. Companies can assign any employee with approval authority to easily include departments such as:\n              Finance, Administration, and Operations Support even when they are not directly assigned</small>\n            <video poster=\"placeholder.png\" width=\"100%\" height=\"150\"\n              poster=\"../../../assets/images/home/video_thumbnail.png\" controls controlsList=\"nodownload\">\n              <source [src]='videoURL' type=\"video/mp4\">\n            </video>\n\n            <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" (click)=\"registerTop()\" target=\"_blank\">Register\n              for a Demo</a>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n\n<div class=\"company-modules\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <h3>Be the Star of the Show!</h3>\n      <h4>We’ll do the behind-the-scenes work for you!</h4>\n\n      <ul>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon11.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Benefits</h2>\n          <p>Build plans and use our Open Enrollment tools\n            to make benefits a breeze! Capture Employee\n            NPS feedback to learn what benefits matter\n            most to your team.</p>\n          <a class=\"btn\" routerLink=\"/benefits\" (click)=\"registerTop()\">Know More</a>\n        </li>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon21.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Leave Management</h2>\n          <p>Create Time-Off Policies to handle employee\n            accruals. Use the Work Scheduler and\n            Time-Sheet tools to make sure your staff is in\n            place when you need them</p>\n          <a class=\"btn\" routerLink=\"/leave-management\" (click)=\"registerTop()\">Know More</a>\n        </li>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon31.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Recruitment</h2>\n          <p>Post Jobs to websites and store candidate data within\n            Zenwork HR. Use the Job Calculator to select the best\n            method in filling a position.\n            Schedule interviews with\n            candidates and track the scores for future openings.</p>\n          <a class=\"btn\" routerLink=\"/recruitment\" (click)=\"registerTop()\">Know More</a>\n        </li>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon41.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Company Settings</h2>\n          <p>Make our system your system! Adjustable Theme settngs to\n            make the site look like your company. Site Access Rights &\n            Management allow you to determine what your employees\n            can do within the site. Mobile Access so that key data is\n            always at your fingertips.</p>\n          <a class=\"btn\" routerLink=\"/company-setting\" (click)=\"registerTop()\">Know More</a>\n        </li>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon51.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Employee Self-Service & Manager Self- Service</h2>\n          <p>Dashboards that have been modified for employees\n            and managers to easily guide them through routine\n            HR updates that save you time.</p>\n          <a class=\"btn\" routerLink=\"/em-service\" (click)=\"registerTop()\">Know More</a>\n        </li>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon61.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Powerful Security</h2>\n          <p>Zenwork HR protects your Senstive\n            information under 256-bit encrypted security. This is the same security\n            level used by banks, the U.S. government and U.S. military.</p>\n          <a class=\"btn\" routerLink=\"/security\" (click)=\"registerTop()\">Know More</a>\n        </li>\n        <li class=\"col-sm-4 col-xs-6\">\n          <img src=\"../../../assets/images/home/icon71.png\" width=\"74\" height=\"70\" alt=\"img\">\n          <h2>Integrations</h2>\n          <p>We play nice with others! Zenwork HR integrates with the Quickbooks Ecosystem so that companies can use\n            what works best for them.</p>\n          <a class=\"btn\" routerLink=\"/api-integration\" (click)=\"registerTop()\">Know More</a>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"team\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"team-lft col-sm-7\">\n        <h2>You’ve got a friend with Zenwork!</h2>\n        <p>Our Team is ready to support you! We offer phone, email, and online chat support to help you with any\n          questions you may have. We partner with you so that our system accommodates your company needs and procedures.\n          Our expert staff will address questions with your perspective in mind. We want to take care of your HR issues\n          so that you spend less time trying to build reports and more time focusing on your company’s goals for\n          success. </p>\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" (click)=\"registerTop()\" target=\"_blank\">Register for\n          a Demo</a>\n      </div>\n      <div class=\"team-rgt pull-right\">\n        <figure><img src=\"../../../assets/images/home/team-bg.png\" width=\"713\" height=\"1135\" alt=\"img\">\n\n          <span><img src=\"../../../assets/images/home/team-img.png\" width=\"449\" height=\"529\" alt=\"img\"></span>\n        </figure>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n\n<div class=\"why-zenwork\">\n\n  <div class=\"tbl\">\n    <div class=\"tbl-cell\">\n\n      <div class=\"container\">\n        <div class=\"row\">\n\n          <h3>Why Zenwork HR</h3>\n\n          <ul>\n            <li class=\"col-md-3 col-sm-4 col-xs-12\">\n              <img src=\"../../../assets/images/home/zenwork1.png\" width=\"74\" height=\"70\" alt=\"img\">\n              <h2>Flexible</h2>\n              <p>Zenwork HR is cloud-based, which\n                provides us with an agile Production\n                environment to keep up with new HR\n                requests so that our system\n                keeps up with YOU!</p>\n\n            </li>\n            <li class=\"col-md-3 col-sm-4 col-xs-12\">\n              <img src=\"../../../assets/images/home/zenwork21.png\" width=\"69\" height=\"65\" alt=\"img\">\n              <h2>User-Friendly</h2>\n              <p>We have built simplified workflows so\n                that employees and managers will easily\n                be able to make HR updates on their\n                own that will save you money\n                and resources.</p>\n\n            </li>\n            <li class=\"col-md-3 col-sm-4 col-xs-12\">\n              <img src=\"../../../assets/images/home/zenwork31.png\" width=\"69\" height=\"65\" alt=\"img\">\n              <h2>Insightful</h2>\n              <p>We have built automated reports and use\n                modern tools such as eNPS and the Job\n                Calculator to provide you with the key data\n                you need. We’ll do the legwork on the data\n                so that you can focus on the decision that is\n                best for the company.</p>\n\n            </li>\n            <li class=\"col-md-3 col-sm-4 col-xs-12\">\n              <img src=\"../../../assets/images/home/zenwork41.png\" width=\"69\" height=\"65\" alt=\"img\">\n              <h2>Client Support</h2>\n              <p>We are available via phone,\n                email and chat and want to\n                help! We partner with you to\n                make HR easier.</p>\n\n            </li>\n\n          </ul>\n          <div class=\"clearfix\"></div>\n\n          <a href=\"#\" class=\"btn\">Get Started</a>\n\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"integrates\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <h2>Zenwork HR&nbsp;Integrates with</h2>\n\n      <ul class=\"zenworks-top\">\n        <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n            <img src=\"../../../assets/images/home/logo11.png\"\n              onmouseover=\"this.src='../../../assets/images/home/logo1-hover1.png'\"\n              onmouseout=\"this.src='../../../assets/images/home/logo11.png'\" />\n          </a>\n        </li>\n\n        <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n            <img src=\"../../../assets/images/home/logo21.png\"\n              onmouseover=\"this.src='../../../assets/images/home/logo2-hover1.png'\"\n              onmouseout=\"this.src='../../../assets/images/home/logo21.png'\" />\n          </a>\n        </li>\n\n\n        <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n            <img src=\"../../../assets/images/home/logo31.png\"\n              onmouseover=\"this.src='../../../assets/images/home/logo3-hover1.png'\"\n              onmouseout=\"this.src='../../../assets/images/home/logo31.png'\" />\n          </a>\n        </li>\n\n      </ul>\n\n\n      <ul class=\"zenworks-btm\">\n\n        <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n            <img src=\"../../../assets/images/home/logo61.png\"\n              onmouseover=\"this.src='../../../assets/images/home/logo6-hover1.png'\"\n              onmouseout=\"this.src='../../../assets/images/home/logo61.png'\" />\n          </a>\n        </li>\n\n\n        <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n            <img src=\"../../../assets/images/home/logo41.png\"\n              onmouseover=\"this.src='../../../assets/images/home/logo4-hover1.png'\"\n              onmouseout=\"this.src='../../../assets/images/home/logo41.png'\" />\n          </a>\n        </li>\n\n\n        <li class=\"col-sm-4 col-xs-6\"><a href=\"#\">\n            <img src=\"../../../assets/images/home/logo51.png\"\n              onmouseover=\"this.src='../../../assets/images/home/logo5-hover11.png'\"\n              onmouseout=\"this.src='../../../assets/images/home/logo51.png'\" />\n          </a>\n        </li>\n\n      </ul>\n\n    </div>\n  </div>\n</div>\n\n\n\n<div class=\"employees\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"employees-lft pull-left\">\n        <figure>\n          <div class=\"employee-bg\">\n            <img src=\"../../../assets/images/home/employee-bg.png\" width=\"806\" height=\"1160\" alt=\"img\">\n            <span><img src=\"../../../assets/images/home/employee-lady.png\" width=\"288\" height=\"270\" alt=\"img\"></span>\n            <div class=\"employee-cont\">\n              <p>Contact us to learn more about how this comprehensive Human Capital Management platform can transform\n                your workplace.</p>\n              <!-- <div class=\"employee-right\">\n                <h3>Business Analyst</h3>\n                <em>Sarah-Crisp</em>\n              </div> -->\n            </div>\n          </div>\n        </figure>\n      </div>\n\n      <div class=\"employees-rgt col-sm-7 pull-right\">\n        <h2>Bring your organization the Zenwork HR experience</h2>\n        <small>Zenwork HR manages Human Capital for any sector.</small>\n        <figure><img src=\"../../../assets/images/home/employee.jpg\" width=\"727\" height=\"621\" alt=\"img\"></figure>\n      </div>\n\n      <div class=\"clearfix\"></div>\n\n\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/landing-page/home/home.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/landing-page/home/home.component.ts ***!
  \*****************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(auth
    // public Environment:environment
    ) {
        this.auth = auth;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
        // environment.url
        this.videoURL = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + '/api/client/videos/zenwork_demo.mp4';
        //  =http://environment:3003/videos/zenwork_demo.mp4"
    };
    HomeComponent.prototype.registerTop = function () {
        window.scroll(0, 0);
    };
    HomeComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/landing-page/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/landing-page/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
            // public Environment:environment
        ])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/integration-api/integration-api.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/landing-page/integration-api/integration-api.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/integration-api/integration-api.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/landing-page/integration-api/integration-api.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Integrations & API Partners</h2>\n        <h1>Creating A World <br>\n          Where Work Empowers A Better Life</h1>\n\n        <small>We work well with others so its easier for bussiness to get their work done!</small>\n\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/integration-img.png\" width=\"675\" height=\"530\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n<!-- <div id=\"arrow-down\">\n  <i class=\"fa fa-chevron-down\"></i>\n</div> -->\n\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\" ><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\" height=\"41\"\n        alt=\"img\"></a></div>\n  \n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <p>We integrate with third-party payroll systems to easily transfer information. We integrate with Quickbooks,\n        Intuit, Accountanto, TSheets, Bill.com, Shopify, and additional payroll systems in the future like Xero. We work\n        to offer as seemless experience as we can to our customers and if there are other software you would like us to\n        integrate with,<br> we will work to make those suggestions a reality!</p>\n\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/integration-api/integration-api.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/landing-page/integration-api/integration-api.component.ts ***!
  \***************************************************************************/
/*! exports provided: IntegrationApiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntegrationApiComponent", function() { return IntegrationApiComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IntegrationApiComponent = /** @class */ (function () {
    function IntegrationApiComponent(auth) {
        this.auth = auth;
    }
    IntegrationApiComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    IntegrationApiComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    IntegrationApiComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-integration-api',
            template: __webpack_require__(/*! ./integration-api.component.html */ "./src/app/landing-page/integration-api/integration-api.component.html"),
            styles: [__webpack_require__(/*! ./integration-api.component.css */ "./src/app/landing-page/integration-api/integration-api.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], IntegrationApiComponent);
    return IntegrationApiComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/landing-page.component.css":
/*!*********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<router-outlet></router-outlet>\n\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.ts ***!
  \********************************************************/
/*! exports provided: LandingPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPageComponent", function() { return LandingPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LandingPageComponent = /** @class */ (function () {
    function LandingPageComponent() {
    }
    LandingPageComponent.prototype.ngOnInit = function () {
    };
    LandingPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-landing-page',
            template: __webpack_require__(/*! ./landing-page.component.html */ "./src/app/landing-page/landing-page.component.html"),
            styles: [__webpack_require__(/*! ./landing-page.component.css */ "./src/app/landing-page/landing-page.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LandingPageComponent);
    return LandingPageComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/landing-page.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/landing-page/landing-page.module.ts ***!
  \*****************************************************/
/*! exports provided: LandingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPageModule", function() { return LandingPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/landing-page/navbar/navbar.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _landing_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./landing-page.component */ "./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/landing-page/home/home.component.ts");
/* harmony import */ var _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pricing/pricing.component */ "./src/app/landing-page/pricing/pricing.component.ts");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/landing-page/footer/footer.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./about/about.component */ "./src/app/landing-page/about/about.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/landing-page/contact/contact.component.ts");
/* harmony import */ var _partner_partner_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./partner/partner.component */ "./src/app/landing-page/partner/partner.component.ts");
/* harmony import */ var _benefits_benefits_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./benefits/benefits.component */ "./src/app/landing-page/benefits/benefits.component.ts");
/* harmony import */ var _leave_management_leave_management_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./leave-management/leave-management.component */ "./src/app/landing-page/leave-management/leave-management.component.ts");
/* harmony import */ var _recruitments_recruitments_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./recruitments/recruitments.component */ "./src/app/landing-page/recruitments/recruitments.component.ts");
/* harmony import */ var _company_setting_company_setting_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./company-setting/company-setting.component */ "./src/app/landing-page/company-setting/company-setting.component.ts");
/* harmony import */ var _em_service_em_service_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./em-service/em-service.component */ "./src/app/landing-page/em-service/em-service.component.ts");
/* harmony import */ var _security_security_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./security/security.component */ "./src/app/landing-page/security/security.component.ts");
/* harmony import */ var _integration_api_integration_api_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./integration-api/integration-api.component */ "./src/app/landing-page/integration-api/integration-api.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _pricing_pricing_modal_pricing_modal_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./pricing/pricing-modal/pricing-modal.component */ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var router = [
    { path: '', component: _landing_page_component__WEBPACK_IMPORTED_MODULE_5__["LandingPageComponent"], children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"] },
            { path: 'pricing', component: _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_7__["PricingComponent"] },
            { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_10__["AboutComponent"] },
            { path: 'contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_11__["ContactComponent"] },
            { path: 'partner', component: _partner_partner_component__WEBPACK_IMPORTED_MODULE_12__["PartnerComponent"] },
            { path: 'benefits', component: _benefits_benefits_component__WEBPACK_IMPORTED_MODULE_13__["BenefitsComponent"] },
            { path: 'leave-management', component: _leave_management_leave_management_component__WEBPACK_IMPORTED_MODULE_14__["LeaveManagementComponent"] },
            { path: 'recruitment', component: _recruitments_recruitments_component__WEBPACK_IMPORTED_MODULE_15__["RecruitmentsComponent"] },
            { path: 'company-setting', component: _company_setting_company_setting_component__WEBPACK_IMPORTED_MODULE_16__["CompanySettingComponent"] },
            { path: 'em-service', component: _em_service_em_service_component__WEBPACK_IMPORTED_MODULE_17__["EmServiceComponent"] },
            { path: 'security', component: _security_security_component__WEBPACK_IMPORTED_MODULE_18__["SecurityComponent"] },
            { path: 'api-integration', component: _integration_api_integration_api_component__WEBPACK_IMPORTED_MODULE_19__["IntegrationApiComponent"] },
        ]
    }
];
var LandingPageModule = /** @class */ (function () {
    function LandingPageModule() {
    }
    LandingPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_8__["MatSlideToggleModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_22__["MaterialModuleModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(router), _angular_material_select__WEBPACK_IMPORTED_MODULE_20__["MatSelectModule"]
            ],
            declarations: [
                _landing_page_component__WEBPACK_IMPORTED_MODULE_5__["LandingPageComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_7__["PricingComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"],
                _about_about_component__WEBPACK_IMPORTED_MODULE_10__["AboutComponent"],
                _contact_contact_component__WEBPACK_IMPORTED_MODULE_11__["ContactComponent"],
                _partner_partner_component__WEBPACK_IMPORTED_MODULE_12__["PartnerComponent"],
                _benefits_benefits_component__WEBPACK_IMPORTED_MODULE_13__["BenefitsComponent"],
                _leave_management_leave_management_component__WEBPACK_IMPORTED_MODULE_14__["LeaveManagementComponent"],
                _recruitments_recruitments_component__WEBPACK_IMPORTED_MODULE_15__["RecruitmentsComponent"],
                _company_setting_company_setting_component__WEBPACK_IMPORTED_MODULE_16__["CompanySettingComponent"],
                _em_service_em_service_component__WEBPACK_IMPORTED_MODULE_17__["EmServiceComponent"],
                _security_security_component__WEBPACK_IMPORTED_MODULE_18__["SecurityComponent"],
                _integration_api_integration_api_component__WEBPACK_IMPORTED_MODULE_19__["IntegrationApiComponent"],
                _pricing_pricing_modal_pricing_modal_component__WEBPACK_IMPORTED_MODULE_21__["PricingModalComponent"]
            ],
            entryComponents: [
                _pricing_pricing_modal_pricing_modal_component__WEBPACK_IMPORTED_MODULE_21__["PricingModalComponent"]
            ]
        })
    ], LandingPageModule);
    return LandingPageModule;
}());



/***/ }),

/***/ "./src/app/landing-page/leave-management/leave-management.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/landing-page/leave-management/leave-management.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/leave-management/leave-management.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/landing-page/leave-management/leave-management.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave leave-manage\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Leave Management</h2>\n        <h1>We're going to rock around the clock!</h1>\n\n        <small>Create Time-Off Policies to handle employee accruals. Use the Work Scheduler and <br> TimeSheet tools to\n          make sure your staff is in place when you need them.</small>\n\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/leave-img.png\" height=\"598\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div> -->\n\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <p>Create time-off policies to handle employee requests. Use the work scheduler and timesheet tools to outsmart\n        the busy season by intelligently managing your employees’ vacation requests.</p>\n      <p>ZenworkHR’s Leave Administration module gives you a way to manage timesheets and administer time-off policies.\n        Create standard work schedules for positions or build <b>custom schedules</b> needed for individual employees.\n        Receive automatic alerts for missed employee time punches or unapproved timesheets so that your data is accurate\n        when it’s time to process payroll.</p>\n      <p>Employees can request time-off and compare the time-off of other employees on their team with <b>at-a-glance\n          dashboards</b> so that they know when time is available</p>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/leave-management/leave-management.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/landing-page/leave-management/leave-management.component.ts ***!
  \*****************************************************************************/
/*! exports provided: LeaveManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementComponent", function() { return LeaveManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LeaveManagementComponent = /** @class */ (function () {
    function LeaveManagementComponent(auth) {
        this.auth = auth;
    }
    LeaveManagementComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    LeaveManagementComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    LeaveManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-leave-management',
            template: __webpack_require__(/*! ./leave-management.component.html */ "./src/app/landing-page/leave-management/leave-management.component.html"),
            styles: [__webpack_require__(/*! ./leave-management.component.css */ "./src/app/landing-page/leave-management/leave-management.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], LeaveManagementComponent);
    return LeaveManagementComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/navbar/navbar.component.css":
/*!**********************************************************!*\
  !*** ./src/app/landing-page/navbar/navbar.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header                  { margin: 35px 0 0; z-index: 1}\n.logo\t\t\t\t\t{ margin: 0;}\n.logo a\t\t\t\t\t{ display: inline-block;padding: 10px 0 0 0;}\n.header-rgt\t\t\t    {width:70%;}\n.main-nav               {display: block;}\n.main-nav .navbar { min-height: inherit; margin-bottom: 0; padding:0;}\n.main-nav .navbar-nav { float: none;}\n.main-nav .navbar-default {background: none;border: none;text-align:  left;margin:18px 0 0 90px;}\n.main-nav .navbar-nav>li {float: none; vertical-align: middle; display: inline-block; padding: 0 0 5px;}\n.main-nav .navbar-nav > li > a {margin: 0 15px;font-size: 16px;line-height:16px;font-weight:500;color:#5f5f5f;display: inline-block;border-radius: 3px;transition:all ease-in-out 0.4s;-moz-transition:all ease-in-out 0.4s;-ms-transition:all ease-in-out 0.4s;-o-transition:all ease-in-out 0.4s;-webkit-transition:all ease-in-out 0.4s;padding: 0;cursor: pointer;}\n.main-nav .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {color:#292929;}\n.main-nav .navbar-default .navbar-collapse, .navbar-default .navbar-form{ padding: 0;}\n.main-nav .container-fluid { padding: 0;}\n.main-nav .navbar-default .navbar-nav>.open>a, .main-nav .navbar-default .navbar-nav>.open>a:focus, .main-nav .navbar-default .navbar-nav>.open>a:hover\n { background: none; color:#5f5f5f;}\n.main-nav .navbar-nav>li>.dropdown-menu {border-radius:6px; box-shadow:0 0 15px #cecece; padding: 0; top:25px;}\n.main-nav .navbar-nav>li:hover .dropdown-menu{display:block;}\n.main-nav .dropdown-menu {left:0; min-width:auto; border: none; box-shadow: none; width: 300px;}\n.main-nav .dropdown-menu>li { margin: 0;}\n.main-nav .dropdown-menu>li>a {font-size: 14px;line-height: 15px;padding:13px 0 13px 15px;font-weight: 500;color:#8e8e8e;}\n.main-nav .dropdown-menu>li>a:hover {background:#83c8a2;color: #fff;}\n.login { margin: 0;}\n.akira {display: inline-block;}\n.akira > ul > li { display: inline-block; margin: 0 15px 0 0;}\n.akira > ul > li > a {cursor: pointer; color:#099247; font-size: 16px; line-height: 16px; padding:8px 35px 10px;font-weight:500; border-radius:30px; border:#099247 1px solid;}\n.akira > ul > li > a:hover {background:#099247;color:#fff;padding:8px 35px 10px;border-radius:30px; border:transparent 1px solid;}\n.akira > ul > li.last{ margin-right: 0;}\n.akira > ul > li > a.sign-up{ background:#099247; border:transparent 1px solid; color: #fff; padding: 8px 40px 10px;}\n.akira > ul > li > a.sign-up:hover{background:#09753a;}\n"

/***/ }),

/***/ "./src/app/landing-page/navbar/navbar.component.html":
/*!***********************************************************!*\
  !*** ./src/app/landing-page/navbar/navbar.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <div class=\"logo pull-left\">\n        <a routerLink=\"/home\"><img src=\"../../../assets/images/main-nav/Component 1@2x.png\" alt=\"img\"></a>\n      </div>\n\n      <div class=\"header-rgt pull-right\">\n\n        <div class=\"main-nav\">\n\n          <nav class=\"navbar navbar-default\">\n\n            <div class=\"container-fluid\">\n\n              <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"\n                  data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\n                  <span class=\"sr-only\">Toggle navigation</span>\n                  <span class=\"icon-bar\"></span>\n                  <span class=\"icon-bar\"></span>\n                  <span class=\"icon-bar\"></span>\n                </button>\n              </div>\n\n\n              <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n\n                <ul class=\"nav navbar-nav\" id=\"nav-scroll\">\n                  <li class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\">Features</a>\n                    <ul class=\"dropdown-menu\">\n                      <li ><a routerLink=\"/benefits\">Benefits Administration</a></li>\n                      <li ><a routerLink=\"/leave-management\">Leave Management</a></li>\n                      <li ><a routerLink=\"/recruitment\">Recruitment</a></li>\n                      <li ><a routerLink=\"/company-setting\">Company Settings</a></li>\n                      <li><a routerLink=\"/em-service\">Employee Self Service & Manager Self Service</a></li>\n                      <li><a routerLink=\"/security\">Security</a></li>\n                      <li><a routerLink=\"/api-integration\">Integration & API Partners</a></li>\n                    </ul>\n                  </li>\n                  <li class=\"dropdown\"><a routerLink=\"/pricing\">Pricing</a></li>\n                  <!-- <li ><a routerLink=\"\">Resources</a></li> -->\n                  <li ><a routerLink=\"/contact\">Contact Us</a></li>\n                  <li ><a routerLink=\"/partner\">Become a Partner</a></li>\n\n                  <li class=\"login pull-right\">\n                    <div class=\"akira\">\n                      <ul>\n                        <li><a routerLink=\"/zenwork-login\">Login</a></li>\n                        <li class=\"last\"><a (click) = \"signUp()\"  class=\"sign-up\">Sign Up</a></li>\n                      </ul>\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n                </ul>\n\n              </div>\n            </div>\n\n          </nav>\n\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</header>"

/***/ }),

/***/ "./src/app/landing-page/navbar/navbar.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/landing-page/navbar/navbar.component.ts ***!
  \*********************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(router) {
        this.router = router;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        $(".navbar-toggle").on("click", function () {
            $(this).toggleClass("active");
        });
    };
    NavbarComponent.prototype.signUp = function () {
        localStorage.clear();
        this.router.navigate(["/sign-up"]);
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/landing-page/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/landing-page/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/partner/partner.component.css":
/*!************************************************************!*\
  !*** ./src/app/landing-page/partner/partner.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.partner.banner { height:800px;}\n.partner .banner-rgt { position: absolute; top: 0; right:0; z-index: -9;}\n.partner .banner-rgt figure { position: relative; }\n.partner .banner-rgt figure img { width: 100%; height: auto;}\n.partner .banner-rgt figure span { position: absolute; top:30%; right:10%; width: 60%;}\n.banner-lft ul { margin: 0 0 50px;}\n.banner-lft ul li { width: 50%; margin: 0 0 20px;}\n.banner-lft ul li .form-group{ margin-bottom: 20px;background-color:#f8f8f8; border-radius: 4px; margin-bottom: 0;}\n.banner-lft ul li em { float: left; width:38px; padding: 4px 0 10px 10px; border-right: #c7c7c7 1px solid; margin: 6px 0; height: 30px;}\n.banner-lft ul li .form-control { background-color:transparent; border: none; box-shadow:none; height: auto; padding: 11px 12px; width: 87%; float: left;}\n.banner-lft ul li .form-control:focus { border:none; box-shadow: none;}\n.banner-lft .btn { color:#000; padding: 10px 35px; border-radius: 30px;font-size: 15px;background: #fec550; margin: 0 0 20px;}\n.partner .banner-lft small { margin: 0 0 60px;}\n.rewards { text-align: center; padding: 100px 50px 50px; position: relative;}\n.rewards h2 { color: #658fa8; font-size: 30px; line-height:30px; margin: 0 0 30px;}\n.rewards p { display: block; font-size: 18px; line-height: 28px;color: #5f5f5f; margin: 0; }\n.rewards figure { position: absolute; top: 0; left: 0; z-index: -9;}\n.rewards figure img { width: 100%; height: auto;}\n.rewards ul { margin:130px 0 0;}\n.rewards ul li { padding: 0;}\n.rewards ul li a { display: inline-block;}\n.rewards ul li a figure { display:block; position: relative; width: auto;}\n.rewards ul li a figure img { display:block;}\n.clients { position: absolute; top:0; left: 0; right: 0; margin: 0 auto; height:100%;}\n.clients h4{ color:#fff; display: block; font-size: 16px; line-height:20px; font-weight: 500; margin: 0;}\n.clients small{ color:#fff;font-size: 12px; line-height: 12px;}\n.clients .tbl-cell { width: 100%; height:100%;}\n.hr-roles { padding: 0 0 70px;}\n.hr-roles-in { background:#fff; box-shadow: 0 0 10px #efefef;}\n.hr-roles-in ul { display:block;}\n.hr-roles-in ul li { display:block;}\n.roles-cont > ul { display:block;}\n.roles-cont > ul > li {border-bottom:#f0f6f7 2px solid;}\n.roles-cont > ul > li.btm-border { border: none; margin-bottom: 0;}\n.roles-cont > ul > li h3 { color:#fff; background: #658da2; font-size: 18px; line-height: 18px; padding:35px 0 35px 40px;\nborder-radius: 5px 5px 0 0; font-weight: 600;margin: 0;}\n.roles-cont > ul > li p { color:#656464;font-size: 18px; line-height: 18px; float: left; width: 50%; margin: 0; padding:20px 0 20px 45px;}\n.roles-cont > ul > li p small{ display: inline-block; color: #166dc5;}\n.roles-cont > ul > li span { float: left; width: 12%; text-align: center; padding:12px 0 0;}\n.roles-cont > ul > li span.greens i.material-icons { color:#5ddb96;}\n.roles-cont > ul > li h2 { color:#fff; background: #658da2; font-size: 18px; line-height: 18px; padding:35px 0 35px 40px;font-weight: 600; margin: 0;}\n.roles-cont > ul > li span i.material-icons { color: #71b2d2;font-weight: bold; font-size: 30px;line-height: 35px;}\n.roles-cont > ul > li.discount { background: #507382;}\n.roles-cont > ul > li.discount p { color: #fff;}\n.roles-cont > ul > li.discount span { color: #fff; padding:20px 0 0;}\n.pad-tb-60 {\n    padding:60px 0; \n}\n.list-inline>li{ vertical-align: middle;}\n.partner-price h2 { margin: 0 0 60px;}\n.partner-price-type { margin: 60px 0 0;}\n.time-brdr-bottem{\n    border-bottom: 2px solid #ffc550 !important;\n    padding: 0px 0 3px 0;\n}\n.margin-lft-85{\n    margin: 70px 0 0 85px;\n}\n"

/***/ }),

/***/ "./src/app/landing-page/partner/partner.component.html":
/*!*************************************************************!*\
  !*** ./src/app/landing-page/partner/partner.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"banner partner\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"banner-lft col-sm-7 col-xs-12\">\n        <!-- <h2>Zenwork Partner</h2> -->\n        <h1>Create more value by<br>\n          Partnering with <img src=\"../../../assets/images/main-nav/Component 1@2x12.png\" alt=\"img\" width=\"245px;\"></h1>\n        <!-- <small>Let Zenwork crunch the numbers while you focus on the big picture.</small> -->\n        <small>&nbsp;&nbsp;</small>\n        <form [formGroup]=\"partnerForm\" (ngSubmit)=\"partnerSubmit()\">\n          <ul>\n\n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/domain.png\" width=\"16\" height=\"14\" alt=\"img\"></em>\n                <input class=\"form-control\" placeholder=\"Company Name\" formControlName=\"name\">\n                <div class=\"clearfix\"></div>\n              </div>\n              <!-- <div *ngIf=\"signupForm.get('name').invalid && signupForm.get('name').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Name</div> -->\n            </li>\n\n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/employee.png\" width=\"21\" height=\"13\" alt=\"img\"></em>\n                <input class=\"form-control\" placeholder=\"Number of Client Employees\" formControlName=\"employeCount\"\n                  type=\"number\">\n                <div class=\"clearfix\"></div>\n              </div>\n              <!-- <div *ngIf=\"signupForm.get('employeCount').invalid && signupForm.get('employeCount').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Employee</div> -->\n            </li>\n\n            <li>\n              <div formGroupName=\"primaryContact\">\n                <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/user.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n                  <input class=\"form-control\" placeholder=\"Full Name\" formControlName=\"name\">\n                  <div class=\"clearfix\"></div>\n                </div>\n                <!-- <div *ngIf=\"signupForm.get('primaryContact').get('name').invalid && signupForm.get('primaryContact').get('name').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Full name</div> -->\n              </div>\n            </li>\n\n            <li>\n              <div class=\"form-group\">\n                <em><img src=\"../../../assets/images/authentication/mail.png\" width=\"13\" height=\"9\" alt=\"img\"></em>\n                <input class=\"form-control\" placeholder=\"Email\" formControlName=\"email\"\n                  pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n                <div class=\"clearfix\"></div>\n              </div>\n              <!-- <div *ngIf=\"signupForm.get('email').invalid && signupForm.get('email').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Email</div> -->\n            </li>\n\n            <li>\n              <div formGroupName=\"primaryContact\">\n                <div class=\"form-group\">\n                  <em><img src=\"../../../assets/images/authentication/phone.png\" width=\"13\" height=\"13\" alt=\"img\"></em>\n                  <input class=\"form-control\" placeholder=\"Phone\" formControlName=\"phone\" (keypress)=\"keyPress($event)\"\n                    minlength=10 maxlength=10>\n                  <div class=\"clearfix\"></div>\n                </div>\n                <!-- <div *ngIf=\"signupForm.get('primaryContact').get('phone').invalid && signupForm.get('primaryContact').get('phone').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Please enter Phone</div> -->\n              </div>\n            </li>\n\n          </ul>\n\n          <button type=\"submit\" class=\"btn\">Become a Partner</button>\n        </form>\n\n\n      </div>\n      <div class=\"banner-rgt pull-right\">\n        <figure><img src=\"../../../assets/images/home/partner-bg.png\" width=\"1133\" height=\"936\" alt=\"img\">\n          <span><img src=\"../../../assets/images/home/partner-img.png\" width=\"668\" height=\"706\" alt=\"img\"></span>\n        </figure>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- \n<div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div> -->\n\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(agile)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n<div #agile class=\"agile-hr\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <h2>Agile HR Experience</h2>\n      <p>Your clients look to you for technical solutions to their everyday needs. In addition to our tax services such\n        as Tax1099 and eFile Assist, we now offer <br> Zenwork HR! Zenwork HR is a cloud-based HCM solution that has the\n        core\n        functionality any business can use at a price that won't break the bank. <br> Become more invested with your\n        clients\n        by meeting more than just their tax once a year... be a part of their everyday life... be a partner of Zenwork\n        HR!</p>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"rewards\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/rewards-bg.png\" width=\"802\" height=\"1105\" alt=\"img\"></figure>\n\n      <h2>Unlock rewards as you grow with ZenworkHR</h2>\n      <p>Pass on bulk discounts or earn revenue as you add clients, <br> plus access exclusive perks</p>\n\n      <ul class=\"col-md-8 col-xs-12 pull-right\">\n\n        <li class=\"col-sm-3 col-xs-6\"><a href=\"#\">\n            <figure>\n              <img src=\"../../../assets/images/home/client1.png\" width=\"103\" height=\"105\" alt=\"img\">\n              <div class=\"clients\">\n                <div class=\"tbl\">\n                  <div class=\"tbl-cell\">\n\n                    <h4>Starter</h4>\n                    <small>1-2 Clients</small>\n                  </div>\n                </div>\n              </div>\n            </figure>\n          </a>\n        </li>\n\n        <li class=\"col-sm-3 col-xs-6\"><a href=\"#\">\n            <figure>\n              <img src=\"../../../assets/images/home/client2.png\" width=\"103\" height=\"105\" alt=\"img\">\n              <div class=\"clients\">\n                <div class=\"tbl\">\n                  <div class=\"tbl-cell\">\n\n                    <h4>Bronze <br> Partner</h4>\n                    <small>3-14 Clients</small>\n                  </div>\n                </div>\n              </div>\n            </figure>\n          </a>\n        </li>\n\n        <li class=\"col-sm-3 col-xs-6\"><a href=\"#\">\n            <figure>\n              <img src=\"../../../assets/images/home/client3.png\" width=\"103\" height=\"105\" alt=\"img\">\n              <div class=\"clients\">\n                <div class=\"tbl\">\n                  <div class=\"tbl-cell\">\n\n                    <h4>Silver <br> Partner</h4>\n                    <small>15-49 Clients</small>\n                  </div>\n                </div>\n              </div>\n            </figure>\n          </a>\n        </li>\n\n\n        <li class=\"col-sm-3 col-xs-6\"><a href=\"#\">\n            <figure>\n              <img src=\"../../../assets/images/home/client4.png\" width=\"103\" height=\"105\" alt=\"img\">\n              <div class=\"clients\">\n                <div class=\"tbl\">\n                  <div class=\"tbl-cell\">\n\n                    <h4>Gold <br> Partner</h4>\n                    <small>50+ Clients</small>\n                  </div>\n                </div>\n              </div>\n            </figure>\n          </a>\n        </li>\n\n      </ul>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"hr-roles\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"hr-roles-in\">\n\n        <ul>\n          <li>\n            <div class=\"roles-cont\">\n              <ul>\n                <li class=\"btm-border\">\n                  <h3>Free Software</h3>\n                </li>\n                <!-- <li>\n                  <p>Free ZenworkHR Payroll for your firm</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li> -->\n                <!-- <li>\n                  <p>Free ZenworkHR Complete HR tools for your firm</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li> -->\n                <li class=\"btm-border\">\n                  <p>Lifetime Free for your Clients of 1-3 Employees</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n              </ul>\n            </div>\n          </li>\n\n\n          <li>\n            <div class=\"roles-cont\">\n              <ul>\n                <li class=\"btm-border\">\n                  <h2>Service</h2>\n                </li>\n                <!-- <li>collateral -->\n                <li>\n                  <p>Free migrations to move clients to Zenwork HR</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n                <li class=\"btm-border\">\n                  <p>VIP Care for your Firm</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n              </ul>\n            </div>\n          </li>\n\n\n          <li>\n            <div class=\"roles-cont\">\n              <ul>\n                <li class=\"btm-border\">\n                  <h2>Marketing tools</h2>\n                </li>\n                <li>\n                  <p>Co-branded dashboards and referral tools</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n                <li>\n                  <p>Flexible payment options for you and your clients</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n                <li>\n                  <p>Free Zenwork HR marketing collateral for your firm and clients</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n                <li>\n                  <p>Zenwork HR partner badges to add to your your website</p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n                <li>\n                  <p>Listing in Zenwork HR's <small> Partner Directory</small></p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n                <li class=\"btm-border\">\n                  <p>Featured profile in Zenwork HR's <small> Partner Directory</small></p>\n                  <span><i class=\"material-icons\">done</i></span><span><i class=\"material-icons\">done</i></span>\n                  <span><i class=\"material-icons\">done</i></span><span class=\"greens\"><i\n                      class=\"material-icons\">done</i></span>\n                  <div class=\"clearfix\"></div>\n                </li>\n              </ul>\n            </div>\n          </li>\n\n\n          <li>\n            <div class=\"roles-cont\">\n              <ul>\n                <li class=\"btm-border\">\n                  <h2>Financial Incentives</h2>\n                </li>\n                <li class=\"discount\">\n                  <p>Discounts to pass along or keep as revenue share</p>\n                  <span>0%</span><span>10%</span>\n                  <span>15%</span><span>20%</span>\n                  <div class=\"clearfix\"></div>\n                </li>\n\n              </ul>\n            </div>\n          </li>\n\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<section class=\"zenwork-pricing text-center pad-tb-20 partner-price\">\n  <h2 class=\"clr-blue mar-0\">Zenwork HR Pricing</h2>\n\n\n  <ul class=\" slider list-inline\">\n    <li>\n      <p [ngClass]=\"{'time-brdr-bottem': !toggleValue}\" class=\"clr-blue\">Monthly Pricing</p>\n    </li>\n    <li>\n      <mat-slide-toggle (change)=\"packCalculation($event)\"></mat-slide-toggle>\n    </li>\n    <li class=\"save-tag\">\n      <p [ngClass]=\"{'time-brdr-bottem': toggleValue}\" class=\"clr-blue\">Annual Pricing</p>\n      <!-- <img src=\"/assets/images/pricing/save.png\" width=\"168\" height=\"35\"> -->\n    </li>\n  </ul>\n</section>\n\n\n<section>\n  <div class=\"pricing-types pad-tb-60 text-center\">\n    <ul class=\"list-inline\">\n      <li class=\"type pos-relative\">\n        <img class=\"pos-relative\" src=\"/assets/images/pricing/pricing1.jpg\" width=\"305\" height=\"auto\">\n        <div class=\"pos-absolute\">\n          <h3 class=\"text-upper pad-btm-20\">Core</h3>\n          <div class=\"pad-btm-20\">\n            <img src=\"/assets/images/pricing/core.png\" width=\"64\" height=\"56\">\n          </div>\n\n          <p class=\"pad-btm-20\">All the Essentials for Onboarding Employees and Managing HR.</p>\n          <p class=\"\">$39 base fee per month</p>\n          <span>+</span>\n          <p *ngIf=\"!toggleValue\" class=\"dashed-border pad-btm-20\">$6 per employee per month.</p>\n          <p *ngIf=\"toggleValue\" class=\"dashed-border pad-btm-20\">$5 per employee per month.</p>\n\n\n          <ul class=\"pad-tb-20\">\n            <li>\n              <h5>HR System</h5>\n            </li>\n            <li>\n              <h5>Employee Self Service</h5>\n            </li>\n            <li>\n              <h5>Business Intelligence</h5>\n            </li>\n            <li>\n              <h5>Benefits</h5>\n            </li>\n            <li>\n              <h5>Time Off</h5>\n            </li>\n            <li>\n              <h5>Theme Settings</h5>\n            </li>\n            <li>\n              <h5>Support</h5>\n            </li>\n            <li>\n              <h5>Training</h5>\n            </li>\n          </ul>\n          <div class=\"pad-tb-20 pricing-dollar\">\n            <p *ngIf=\"!toggleValue\"> $6</p>\n            <p *ngIf=\"toggleValue\"> $5</p>\n          </div>\n          <div class=\"pad-tb-20\">\n            <a class=\"get-started-btn cursor clr-blue\" routerLink=\"/sign-up\">Get Started</a>\n          </div>\n          <div class=\"btm-tag pos-absolute\">\n            <p>LIFETIME FREE FOR 1-3 EMPLOYEES</p>\n          </div>\n        </div>\n\n      </li>\n      <li class=\"type mar-lr-10 pos-relative margin-lft-85\">\n        <img class=\"pos-relative\" src=\"/assets/images/pricing/pricing2.jpg\" width=\"305\" height=\"auto\">\n        <div class=\"pos-absolute \">\n          <div class=\"top-tag pos-absolute\">\n            <p class=\"green recommended\">RECOMMENDED</p>\n          </div>\n          <h3 class=\"text-upper pad-btm-20 middle\">PREMIUM</h3>\n          <div class=\"pad-btm-20\">\n            <img src=\"/assets/images/pricing/premium.png\" width=\"52\" height=\"73\">\n          </div>\n\n          <p class=\"pad-btm-20\">Expanded Compliance and HR for\n            Growing Companies.</p>\n          <p class=\"\">$39 base fee per month</p>\n          <span>+</span>\n          <p *ngIf=\"!toggleValue\" class=\"dashed-border pad-btm-20\">$12 per employee per month.</p>\n          <p *ngIf=\"toggleValue\" class=\"dashed-border pad-btm-20\">$10 per employee per month.</p>\n\n          <ul class=\"pad-tb-20\" style=\"padding: 0 0 0 0;\">\n            <li>\n              <h5>HR System</h5>\n            </li>\n            <li>\n              <h5>Employee Self Service</h5>\n            </li>\n            <li>\n              <h5>Business Intelligence</h5>\n            </li>\n            <li>\n              <h5>Benefits</h5>\n            </li>\n            <li>\n              <h5>Time Off</h5>\n            </li>\n            <li>\n              <h5>Theme Settings</h5>\n            </li>\n            <li>\n              <h5>Support</h5>\n            </li>\n            <li>\n              <h5>Training</h5>\n            </li>\n            <li>\n              <h5>Compliance Training and Assistance</h5>\n            </li>\n            <li>\n              <h5>ACA Compliance & Reporting</h5>\n            </li>\n            <li>\n              <h5>Recruitment</h5>\n            </li>\n            <li>\n              <h5>On Boarding and Off Boarding</h5>\n            </li>\n            <li>\n              <h5>Integrations and API</h5>\n            </li>\n            <li>\n              <h5>Surveys</h5>\n            </li>\n            <li>\n              <h5>Audit Trail</h5>\n            </li>\n          </ul>\n\n          <div class=\"pricing-dollar\">\n            <p *ngIf=\"!toggleValue\"> $12</p>\n            <p *ngIf=\"toggleValue\"> $10</p>\n          </div>\n          <div class=\"pad-tb-20\">\n            <a class=\"get-started-btn cursor clr-blue\" routerLink=\"/sign-up\">Get Started</a>\n          </div>\n\n        </div>\n      </li>\n      <li class=\"type  pos-relative\">\n        <img class=\"pos-relative\" src=\"/assets/images/pricing/pricing3.jpg\" width=\"305\" height=\"auto\">\n        <div class=\"pos-absolute\">\n          <h3 class=\"text-upper pad-btm-20\">ADD-ON</h3>\n          <div class=\"pad-btm-20\">\n            <img src=\"/assets/images/pricing/addon.png\" height=\"56\">\n          </div>\n\n          <p class=\"pad-btm-20\">Add-on available only\n            with Premium.</p>\n          <p class=\"dashed-border pad-btm-20\">Add-on charged per employee <br>\n            per month.</p>\n\n          <ul class=\"pad-tb-20\">\n            <li>\n              <h5>Time Tracking</h5>\n              <h4>$4</h4>\n            </li>\n            <li>\n              <h5>Time and Attendance Hardware\n                Integration</h5>\n              <h4>$4</h4>\n            </li>\n            <li>\n              <h5>Performance Management</h5>\n              <h4>$3</h4>\n            </li>\n          </ul>\n        </div>\n      </li>\n    </ul>\n  </div>\n</section>\n\n\n\n\n<div class=\"employees\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"employees-lft pull-left\">\n        <figure>\n          <div class=\"employee-bg\">\n            <img src=\"../../../assets/images/home/employee-bg.png\" width=\"806\" height=\"1160\" alt=\"img\">\n            <span><img src=\"../../../assets/images/home/employee-lady.png\" width=\"288\" height=\"270\" alt=\"img\"></span>\n            <div class=\"employee-cont\">\n              <p>Moving to Zenwork HR has made us wonder how <br>\n                we survived before it happened, the product is <br>\n                comprehensive, built with great a en on to <br>\n                detail and is highly differen ated from most other <br>\n                HRIS Products in the market.</p>\n              <div class=\"employee-right\">\n                <h3>Business Analyst</h3>\n                <em>Sarah-Crisp</em>\n              </div>\n            </div>\n          </div>\n        </figure>\n      </div>\n\n      <div class=\"employees-rgt col-sm-7 pull-right\">\n        <h2>Employees love the Zenwork HR experience</h2>\n        <small>Enterprises across sectors use Zenwork HR to manage their Human Capital.</small>\n        <figure><img src=\"../../../assets/images/home/employee.jpg\" width=\"727\" height=\"621\" alt=\"img\"></figure>\n      </div>\n\n      <div class=\"clearfix\"></div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/landing-page/partner/partner.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/landing-page/partner/partner.component.ts ***!
  \***********************************************************/
/*! exports provided: PartnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnerComponent", function() { return PartnerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PartnerComponent = /** @class */ (function () {
    function PartnerComponent(partnerService, auth) {
        this.partnerService = partnerService;
        this.auth = auth;
        this.toggleValue = false;
    }
    PartnerComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
        this.partnerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            employeCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            primaryContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
                phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            }),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
        });
    };
    PartnerComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    PartnerComponent.prototype.partnerSubmit = function () {
        console.log("Become a Partner", this.partnerForm.value);
    };
    PartnerComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    PartnerComponent.prototype.packCalculation = function (event) {
        console.log(event);
        this.toggleValue = event.checked;
    };
    PartnerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-partner',
            template: __webpack_require__(/*! ./partner.component.html */ "./src/app/landing-page/partner/partner.component.html"),
            styles: [__webpack_require__(/*! ./partner.component.css */ "./src/app/landing-page/partner/partner.component.css")]
        }),
        __metadata("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"], _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], PartnerComponent);
    return PartnerComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pricing-header {\n    padding: 20px 26px;\n    background: #fec550;\n    font-size: 17px;\n    margin: -24px -24px 20px -24px;\n }\n .price-margin{\n     border-bottom: 1px dotted #dac2c2;\n     margin: 10px 0px 10px 0px;\n }\n .total-text {\n     text-align: center;\n }\n .total-amount {\n    text-align: center;\n    margin-top: 33px;\n }\n .amount {\n    border: 1px solid #debfbf;\n    padding: 15px 60px;\n    font-size: 20px;\n    border-radius: 50px;\n }\n .price-btn {\n    background:#fec550; color:#444343; border-radius:30px; padding: 8px 50px; border: none;\n    margin: 50px 0 0; font-size: 17px;\n }\n .reset-text {\n    margin-top: 27px;\n    color: #8686c3;\n    text-decoration: underline;\n    cursor: pointer;\n }"

/***/ }),

/***/ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row pricing-header\">\n  <div class=\"col-xs-5\">\n    Billing\n  </div>\n  <div class=\"col-xs-2\">\n    Monthly\n  </div>\n  <div class=\"col-xs-1\">\n    <mat-slide-toggle (change)=\"timePeriod($event)\"></mat-slide-toggle>\n  </div>\n  <div class=\"col-xs-2\">\n    Annually\n  </div>\n</div>\n<div class=\"row price-margin\">\n  <div class=\"col-xs-6\">\n    <p class=\"green\" style=\"text-decoration: underline;\">Package</p>\n    <p>{{package[0].name}}</p>\n  </div>\n  <div *ngIf=\"timeToggle == true\" class=\"col-xs-6 green\" style=\"margin-top: 30px;\">\n    $ {{package[0].price * 12}}\n  </div>\n  <div *ngIf=\"timeToggle == false\" class=\"col-xs-6 green\" style=\"margin-top: 30px;\">\n    $ {{package[0].price}}\n  </div>\n\n</div>\n<div class=\"row price-margin\">\n  <div class=\"col-xs-6\">\n    <p>Number of Employees ({{employeeCount}})</p>\n    <p>Total Employee Cost</p>\n  </div>\n  <div *ngIf=\"timeToggle == false\" class=\"col-xs-6 green\" style=\"margin-top: 30px;\">\n    $ {{totalEmpCost}}\n  </div>\n  <div *ngIf=\"timeToggle == true\" class=\"col-xs-6 green\" style=\"margin-top: 30px;\">\n    $ {{totalEmpCost*12}}\n  </div>\n\n</div>\n\n<div class=\"row price-margin\">\n  <div class=\"col-xs-6\">\n    <p class=\"green\" style=\"text-decoration: underline;\">Add-On</p>\n    <p *ngFor=\"let data of addons\">{{data.name}}</p>\n    <!-- <p>Performance Management</p> -->\n    <br>\n    <p>Total Price</p>\n  </div>\n  <div *ngIf=\"timeToggle == false\" class=\"col-xs-6\" style=\"margin-top: 30px;\">\n\n    <p *ngFor=\"let data of addons\" class=\"green\">$ {{data.price}}</p>\n    <!-- <p class=\"green\">$ 53.00</p><br> -->\n    <p class=\"green\">$ {{estimatedPrice}}</p>\n  </div>\n  <div *ngIf=\"timeToggle == true\" class=\"col-xs-6\" style=\"margin-top: 30px;\">\n\n    <p *ngFor=\"let data of addons\" class=\"green\">$ {{data.price*12}}</p>\n    <!-- <p class=\"green\">$ 53.00</p><br> -->\n    <p *ngIf=\"timeToggle == false\" class=\"green\">$ {{estimatedPrice}}</p>\n    <p *ngIf=\"timeToggle == true\" class=\"green\">$ {{estimatedPrice*12}}</p>\n\n  </div>\n\n</div>\n\n<div class=\"text-center\">\n  <h3 class=\"total-text\">Your Estimated Total</h3>\n  <p *ngIf=\"timeToggle == true\" class=\"total-amount\"><span class=\"amount\">$ {{estimatedPrice *12}}</span></p>\n  <p *ngIf=\"timeToggle == false\" class=\"total-amount\"><span class=\"amount\">$ {{estimatedPrice}}</span></p>\n\n  <p class=\"text-center reset-text\" (click)=\"onNoClick()\">Reset</p>\n  <button class=\"price-btn\" (click) = \"getStart()\" >Get Started</button>\n</div>"

/***/ }),

/***/ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.ts ***!
  \*******************************************************************************/
/*! exports provided: PricingModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingModalComponent", function() { return PricingModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PricingModalComponent = /** @class */ (function () {
    function PricingModalComponent(dialogRef, data, router) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.router = router;
        this.package = [];
        this.addons = [];
        this.estimatedPrice = 0;
        this.totalAddonsCost = 0;
        this.timeToggle = false;
    }
    PricingModalComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    PricingModalComponent.prototype.ngOnInit = function () {
        this.totalAddonsCost = 0;
        this.estimatedPrice = 0;
        console.log(this.data);
        // this.totalAddonsCost
        // this.addons.forEach(element => {
        //   console.log(element);
        //   this.totalAddonsCost += element.price;
        // });
        console.log(this.totalAddonsCost, 'addonstotalcost');
        this.calculateEmpCost();
    };
    PricingModalComponent.prototype.calculateEmpCost = function () {
        this.package = this.data.package;
        this.addons = this.data.addons;
        this.employeeCount = this.data.employeeCount;
        for (var i = 0; i < this.addons.length; i++) {
            console.log(this.addons[i].price);
            this.totalAddonsCost = this.totalAddonsCost + this.addons[i].price;
            console.log(this.totalAddonsCost);
        }
        this.totalEmpCost = this.package[0].costPerEmployee * this.employeeCount;
        this.estimatedPrice = parseInt(this.package[0].price) + parseInt(this.totalEmpCost) + this.totalAddonsCost;
        console.log(this.estimatedPrice);
    };
    PricingModalComponent.prototype.timePeriod = function (event) {
        console.log(event);
        this.timeToggle = event.checked;
    };
    PricingModalComponent.prototype.getStart = function () {
        this.dialogRef.close();
        this.router.navigate(['/sign-up']);
    };
    PricingModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pricing-modal',
            template: __webpack_require__(/*! ./pricing-modal.component.html */ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.html"),
            styles: [__webpack_require__(/*! ./pricing-modal.component.css */ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PricingModalComponent);
    return PricingModalComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/pricing/pricing.component.css":
/*!************************************************************!*\
  !*** ./src/app/landing-page/pricing/pricing.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.pricing-banner .tbl-cell { padding: 150px 0 0;}\n.pricing-banner{\n    position: relative;  \n}\n.pricing-banner-bottom figure img{\n    z-index: -9;\n    position: relative; width: 100%; height: auto;\n}\n.pricing-banner-bottom figure { position: absolute; top:14%; right:90px; width: 35%;}\n.slider.list-inline>li{\n    vertical-align: top;\n    width: 140px;\n\n}\n.margin-lft-85{\n    margin: 70px 0 0 85px;\n}\n.zenwork-pricing { position: relative; padding: 90px 0 0 0;}\n.zenwork-pricing figure { position: absolute; top: 0; left: 0;z-index: -9;}\n.pricing-acc { display: block;}\n.pricing-acc .panel-default>.panel-heading { padding:20px 25px; background-color:#f1f6f7; border: none;}\n.pricing-acc .panel-title a { color: #5f5f5f; display:block; font-size: 17px;font-weight: 600;}\n.pricing-acc .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.pricing-acc .panel-title>.small, .pricing-acc .panel-title>.small>a, .pricing-acc .panel-title>a, .pricing-acc .panel-title>small, .compensation .panel-title>small>a { text-decoration:none;}\n.pricing-acc .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid;}\n.pricing-acc .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.pricing-acc .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.pricing-acc .panel-group .panel-heading+.panel-collapse>.list-group, .pricing-acc .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.pricing-acc .panel-heading .accordion-toggle:after { margin:7px 15px 0; font-size: 13px; line-height: 13px;}\n.pricing-acc .panel-body { padding: 0;}\n.pricing-acc .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.pricing-acc .panel-default { border-color:transparent;}\n.pricing-acc .panel-group .panel+.panel { margin-top: 2px;}\n.pricing-acc .panel-group {\n    margin-bottom: 1px;\n}\n.calculator-popup { text-align: center;}\n.calculator-popup .modal-dialog { width:35%;}\n.calculator-popup .modal-content {border-radius: 3px; padding: 50px 0; box-shadow: none;}\n.calculator-popup .modal-header {padding: 25px 30px;}\n.calculator-popup .modal-body { padding: 0;border-radius: 3px;}\n.pricing-popup { text-align: center;}\n.pricing-popup .modal-dialog { width:35%;}\n.pricing-popup .modal-content {border-radius: 3px; box-shadow: none;}\n.pricing-popup .modal-header {padding: 25px 30px;}\n.pricing-popup .modal-body { padding: 0;border-radius: 3px;}\n.pricing-cont { display: block;}\n.pricing-cont ul { display: block;}\n.pricing-cont ul li{ margin: 0 0 30px;}\n.pricing-cont ul li label{ display: inline-block; font-size: 17px; color: #87949e; width: 150px;\nvertical-align: middle;text-align: left;}\n.pricing-cont ul li span{ display: inline-block; width: 50%;vertical-align: middle; text-align: left;}\n.pricing-cont ul li span .form-control { height: auto; padding:10px 12px;background: #f8f8f8;border: none; font-size: 17px; box-shadow: none;}\n.pricing-cont ul li span .form-control:focus{ border-color:transparent; box-shadow: none;}\n.pricing-cont .btn {background:#fec550; color:#444343; border-radius:30px; padding: 8px 50px; border: none;\n margin: 50px 0 0; font-size: 17px;}\n.pricing-acc .panel-heading .accordion-toggle .collapsed::after{\n    content: \"\\e080\";\n }\n.pricing-acc .panel-heading .accordion-toggle::after{\n     content: \"\\e114\"\n }\n.addOn-box {\n    width: 35%;\n    border: 1px solid #cecece;\n    padding: 6px;\n    text-align: center;\n    border-radius: 30px;\n }\n.clear-icon {\n    font-size: 15px;\n    color: #cecece;\n    cursor: pointer;\n    vertical-align: middle;\n }\n.time-brdr-bottem{\n    border-bottom: 2px solid #ffc550 !important;\n    padding: 0px 0 3px 0;\n}\n\n"

/***/ }),

/***/ "./src/app/landing-page/pricing/pricing.component.html":
/*!*************************************************************!*\
  !*** ./src/app/landing-page/pricing/pricing.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Author:Kinnera Reddy, Date:20/08/2019-->\n\n<section class=\"pricing-banner tbl pad-tp-40\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <!-- <div class=\"pricing-banner-top\">\n   <img src=\"/assets/images/pricing/banner-top.jpg\">\n  </div> -->\n      <div class=\"tbl-cell\">\n\n        <h4 class=\"green\">Pricing</h4>\n        <h1>Choose the plan that’s right for\n          <br> your business</h1>\n        <!-- <p class=\"pad-tb-20\">We believe great businesses treat their employees like people, not ID numbers —and we’re\n          building a pla orm that empowers organiza ons to do just that.</p> -->\n      \n        <p> &nbsp; </p>\n        <a class=\"cursor\" href=\"https://calendly.com/zenworkhr/intro\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n\n      <div class=\"tbl-cell\">\n        <div class=\"pricing-banner-bottom pull-right\">\n          <figure><img src=\"/assets/images/pricing/banner-btm.jpg\" width=\"706\" height=\"589\" alt=\"img\"></figure>\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n</section>\n\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\" ></i>\n  </div> -->\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(zenPriceing)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n<span #zenPriceing>\n  <section class=\"zenwork-pricing text-center pad-tb-20\">\n\n    <figure><img src=\"../../../assets/images/home/pricing-lft-bg.png\" width=\"379\" height=\"1218\" alt=\"img\"></figure>\n    <h2 class=\"clr-blue mar-0\">Zenwork HR Pricing</h2>\n    <div class=\"pad-tb-60\">\n      <a class=\"cursor\" data-toggle=\"modal\" data-target=\"#myModal\">Calculator</a>\n    </div>\n    <ul class=\" slider list-inline\">\n      <li>\n        <p [ngClass]=\"{'time-brdr-bottem': !toggleValue}\" class=\"clr-blue\">Monthly Pricing</p>\n      </li>\n      <li>\n        <mat-slide-toggle (change)=\"packCalculation($event)\"></mat-slide-toggle>\n      </li>\n      <li class=\"save-tag\">\n        <p [ngClass]=\"{'time-brdr-bottem': toggleValue}\" class=\"clr-blue\">Annual Pricing</p>\n        <!-- <img src=\"/assets/images/pricing/save.png\" width=\"168\" height=\"35\"> -->\n      </li>\n    </ul>\n  </section>\n\n  <section>\n    <div class=\"pricing-types pad-tb-60 text-center\">\n      <ul class=\"list-inline\">\n        <li class=\"type pos-relative\">\n          <img class=\"pos-relative\" src=\"/assets/images/pricing/pricing1.jpg\" width=\"305\" height=\"auto\">\n          <div class=\"pos-absolute\">\n            <h3 class=\"text-upper pad-btm-20\">Core</h3>\n            <div class=\"pad-btm-20\">\n              <img src=\"/assets/images/pricing/core.png\" width=\"64\" height=\"56\">\n            </div>\n\n            <p class=\"pad-btm-20\">All the Essentials for Onboarding Employees and Managing HR.</p>\n            <p class=\"\">$39 base fee per month</p>\n            <span>+</span>\n            <p *ngIf=\"!toggleValue\" class=\"dashed-border pad-btm-20\">$6 per employee per month.</p>\n            <p *ngIf=\"toggleValue\" class=\"dashed-border pad-btm-20\">$5 per employee per month.</p>\n\n\n            <ul class=\"pad-tb-20\" >\n              <li>\n                <h5>HR System</h5>\n              </li>\n              <li>\n                <h5>Employee Self Service</h5>\n              </li>\n              <li>\n                <h5>Business Intelligence</h5>\n              </li>\n              <li>\n                <h5>Benefits</h5>\n              </li>\n              <li>\n                <h5>Time Off</h5>\n              </li>\n              <li>\n                <h5>Theme Settings</h5>\n              </li>\n              <li>\n                <h5>Support</h5>\n              </li>\n              <li>\n                <h5>Training</h5>\n              </li>\n            </ul>\n            <div class=\"pad-tb-20 pricing-dollar\">\n              <p *ngIf=\"!toggleValue\"> $6</p>\n              <p *ngIf=\"toggleValue\"> $5</p>\n            </div>\n            <div class=\"pad-tb-20\">\n              <a class=\"get-started-btn cursor clr-blue\" routerLink=\"/sign-up\">Get Started</a>\n            </div>\n            <div class=\"btm-tag pos-absolute\">\n              <p>LIFETIME FREE FOR 1-3 EMPLOYEES</p>\n            </div>\n          </div>\n\n        </li>\n        <li class=\"type mar-lr-10 pos-relative margin-lft-85\">\n          <img class=\"pos-relative\" src=\"/assets/images/pricing/pricing2.jpg\" width=\"305\" height=\"auto\">\n          <div class=\"pos-absolute \">\n            <div class=\"top-tag pos-absolute\">\n              <p class=\"green recommended\">RECOMMENDED</p>\n            </div>\n            <h3 class=\"text-upper pad-btm-20 middle\">PREMIUM</h3>\n            <div class=\"pad-btm-20\">\n              <img src=\"/assets/images/pricing/premium.png\" width=\"52\" height=\"73\">\n            </div>\n\n            <p class=\"pad-btm-20\">Expanded Compliance and HR for\n              Growing Companies.</p>\n            <p class=\"\">$39 base fee per month</p>\n            <span>+</span>\n            <p *ngIf=\"!toggleValue\" class=\"dashed-border pad-btm-20\">$12 per employee per month.</p>\n            <p *ngIf=\"toggleValue\" class=\"dashed-border pad-btm-20\">$10 per employee per month.</p>\n\n            <ul class=\"pad-tb-20\" style=\"padding: 0 0 0 0\">\n              <li>\n                <h5>HR System</h5>\n              </li>\n              <li>\n                <h5>Employee Self Service</h5>\n              </li>\n              <li>\n                <h5>Business Intelligence</h5>\n              </li>\n              <li>\n                <h5>Benefits</h5>\n              </li>\n              <li>\n                <h5>Time Off</h5>\n              </li>\n              <li>\n                <h5>Theme Settings</h5>\n              </li>\n              <li>\n                <h5>Support</h5>\n              </li>\n              <li>\n                <h5>Training</h5>\n              </li>\n              <li>\n                <h5>Compliance Training and Assistance</h5>\n              </li>\n              <li>\n                <h5>ACA Compliance & Reporting</h5>\n              </li>\n              <li>\n                <h5>Recruitment</h5>\n              </li>\n              <li>\n                <h5>On Boarding and Off Boarding</h5>\n              </li>\n              <li>\n                <h5>Integrations and API</h5>\n              </li>\n              <li>\n                <h5>Surveys</h5>\n              </li>\n              <li>\n                <h5>Audit Trail</h5>\n              </li>\n            </ul>\n\n            <div class=\"pricing-dollar\">\n              <p *ngIf=\"!toggleValue\"> $12</p>\n              <p *ngIf=\"toggleValue\"> $10</p>\n            </div>\n            <div class=\"pad-tb-20\">\n              <a class=\"get-started-btn cursor clr-blue\" routerLink=\"/sign-up\">Get Started</a>\n            </div>\n\n          </div>\n        </li>\n        <li class=\"type  pos-relative\">\n          <img class=\"pos-relative\" src=\"/assets/images/pricing/pricing3.jpg\" width=\"305\" height=\"auto\">\n          <div class=\"pos-absolute\">\n            <h3 class=\"text-upper pad-btm-20\">ADD-ON</h3>\n            <div class=\"pad-btm-20\">\n              <img src=\"/assets/images/pricing/addon.png\" height=\"56\">\n            </div>\n\n            <p class=\"pad-btm-20\">Add-on available only\n              with Premium.</p>\n            <p class=\"dashed-border pad-btm-20\">Add-on charged per employee <br>\n              per month.</p>\n\n            <ul class=\"pad-tb-20\">\n              <li>\n                <h5>Time Tracking</h5>\n                <h4>$4</h4>\n              </li>\n              <li>\n                <h5>Time and Attendance Hardware\n                  Integration</h5>\n                <h4>$4</h4>\n              </li>\n              <li>\n                <h5>Performance Management</h5>\n                <h4>$3</h4>\n              </li>\n            </ul>\n          </div>\n        </li>\n      </ul>\n    </div>\n  </section>\n</span>\n\n\n\n<div class=\"container\">\n  <section>\n    <div class=\"row\">\n\n      <div class=\"col-sm-8 col-xs-12 mobile-price\">\n\n        <div class=\"pricing-tables\">\n          <div class=\"col-sm-6 col-xs-5\">\n            <p>FEATURES</p>\n          </div>\n          <div class=\"col-xs-3\">\n            <p>CORE</p>\n          </div>\n          <div class=\"col-sm-3 col-xs-4\">\n            <p>PREMIUM</p>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n\n\n        <div class=\"pricing-acc\">\n\n          <div class=\"panel-group\" id=\"accordion1\">\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n                    HR System\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Employee Information</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Benefits Tracking</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Default Access & Rights</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Employee Document Management</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Basic Approvals</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Basic workflow</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>In App & Email Notifications</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Custom Fields</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Single Sign-On</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Company Calendar</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Organization Chart</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Chat Tool</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwo\">\n                    Employee Self Service\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseTwo\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Time Off Tracking and Request</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Mobile App</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Employee Directory</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Leave Request</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseThree\">\n                    Business Intelligence\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseThree\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Payroll Detail and Summary Reports</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Benefits Cost Analysis</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseFour\">\n                    Benefits\n                  </a>\n                </h4>\n              </div>\n              <div class=\"accordion-toggle\" id=\"collapseFour\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Benefits Tracking</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Benefits Tracking and Payroll Deduction Feed</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseFive\">\n                    Time Off\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseFive\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Leave Setup and Configuration</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Leave management</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseSix\">\n                    Theme Settings\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseSix\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Customize the Look and Feel</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseSeven\">\n                    Support\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseSeven\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Email</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Email, Phone and Chat Support</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseEight\">\n                    Training\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseEight\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Base Training</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Advanced Training</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-blue\">done</i>\n                      </p>\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseNine\">\n                    Compliance Training and Assistance\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseNine\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Self Help Videos and Tutorials</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTen\">\n                    ACA Compliance and Reporting\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseTen\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>ACA IRS eFile (IRS Approved eFile Provider)</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>State New Hire Reporting</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Contractor 1099 Reporting and TIN matching</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Contractor Compliance</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>W-9 Solicitation for Contractors</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseEleven\">\n                    Compliance Training and Assistance\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseEleven\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Custom Job posting URL</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Mass posting to external job boards and portals</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwelve\">\n                    On Boarding and Off Boarding\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseTwelve\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>I-9 Reporting</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Employee W-4 calculation wizard</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#Thirteen\">\n                    Integrations and API\n                  </a>\n                </h4>\n              </div>\n              <div id=\"Thirteen\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>QuickBooks Online Payroll</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>QuickBooks Desktop Payroll</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Xero Payroll</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Downloadable Payroll Flat Files</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#Fourteen\">\n                    Surveys\n                  </a>\n                </h4>\n              </div>\n              <div id=\"Fourteen\" class=\"panel-collapse collapse\">\n                <div class=\"panel-body\">\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Surveys</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Surveys</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n\n                  <div class=\"table-row\">\n                    <div class=\"col-xs-6\">\n                      <p>Polls</p>\n                    </div>\n                    <div class=\"col-xs-3\">\n                      &nbsp;\n\n                    </div>\n                    <div class=\"col-xs-3\">\n                      <p>\n                        <i class=\"material-icons clr-green\">done</i>\n                      </p>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                  <div class=\"table-head table-last-row pad-0\">\n                    <div class=\"col-xs-12\">\n                      &nbsp;\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                </div>\n\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n\n\n      </div>\n\n\n      <div class=\"col-sm-4 col-xs-12 mobile-price mobile-time\">\n        <div class=\"pricing-table-right text-center\">\n          <p> ADD-ON</p>\n        </div>\n        <div class=\"table-head col-xs-12\">\n          <h3> Time Tracking</h3>\n        </div>\n        <p class=\"table-row col-xs-12\">Employee Time Sheets</p>\n        <p class=\"table-row col-xs-12\">Time Tracking with Automated Tracker Clock</p>\n        <p class=\"table-row col-xs-12\">Task & Reminders</p>\n        <p class=\"table-row col-xs-12\">Time Tracking Approval Workflow</p>\n        <p class=\"table-row col-xs-12\">Overtime & Holiday Pay Calculation Plus Alerts</p>\n        <div class=\"table-head col-xs-12\">\n          <h3>Time & Attendance Hardware Integration</h3>\n        </div>\n        <p class=\"table-row col-xs-12\">Bio-metric Hardware Installation & Integration</p>\n        <p class=\"table-row col-xs-12\">Finger Print or PIN Access</p>\n        <p class=\"table-row col-xs-12\">Optional IRIS Scan</p>\n        <div class=\"table-head col-xs-12\">\n          <h3>Performance Management</h3>\n        </div>\n        <p class=\"table-row col-xs-12\">--</p>\n        <div class=\"table-head col-xs-12\">\n          <p>&nbsp; &nbsp; &nbsp; --</p>\n        </div>\n        <p class=\"table-row col-xs-12\"></p>\n        <p class=\"table-row col-xs-12\">--</p>\n        <div class=\"table-head col-xs-12 table-last-row pad-0\">\n          &nbsp;\n        </div>\n\n\n      </div>\n    </div>\n  </section>\n\n</div>\n\n<div class=\"calculator-popup\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n\n        <div class=\"modal-body\">\n\n          <div class=\"pricing-cont\">\n\n            <ul>\n              <li>\n                <label>Select Package</label>\n                <span>\n                  <mat-select class=\"form-control\" [(ngModel)]=\"selectedPackage.name\"\n                    (ngModelChange)=\"getSpecificPackageDetails()\" placeholder=\"Package\" name=\"Package\" required\n                    #Package=\"ngModel\">\n                    <mat-option *ngFor=\"let data of packages\" [value]=\"data.name\">{{data.name}}</mat-option>\n                    <!-- <mat-option value=\"Core\">Core</mat-option> -->\n                  </mat-select>\n                </span>\n                <span *ngIf=\"!selectedPackage.name && Package.touched || (!selectedPackage.name && isValid)\"\n                  style=\"color:#e44a49; padding:4px 0 0 77px;\">Select a package</span>\n              </li>\n\n              <li>\n                <label>Select Number of <br> Employees</label>\n                <span>\n                  <input type=\"text\" [(ngModel)]=\"employeeCount\" placeholder=\"Employees\" class=\"form-control\"\n                    name=\"empCount\" required #empCount=\"ngModel\">\n                </span>\n                <span *ngIf=\"!employeeCount && empCount.touched || (!employeeCount && isValid)\"\n                  style=\"color:#e44a49; padding:4px 0 0 77px;\">Enter a EmployeeCount</span>\n              </li>\n\n              <li>\n                <label>Add-On</label>\n                <span>\n                  <mat-select class=\"form-control\" placeholder=\"Select Add-On\" [(ngModel)]=\"addOn\" multiple\n                    (ngModelChange)=\"selectAddOn($event)\" name=\"adons\" required #adons=\"ngModel\">\n                    <mat-option *ngFor=\"let data of allAddOns\" [value]=\"data\">{{data.name}}\n                    </mat-option>\n                    <!-- <mat-option value=\"Performance Management\">Performance Management</mat-option> -->\n                  </mat-select>\n                </span>\n                <span *ngIf=\"!addOn && adons.touched || (!addOn && isValid)\"\n                  style=\"color:#e44a49; padding:4px 0 0 77px;\">Select Addons</span>\n              </li>\n              <li>\n                <span style=\"width: 35%; border: 1px solid #eadfdf;\n                        padding: 6px;\n                        text-align: center;\n                        border-radius: 30px; margin-right: 10px;margin-bottom: 20px;\"\n                  *ngFor=\"let addon of addOns\">{{addon.name}} &nbsp;\n                  <i class=\"material-icons clear-icon\" (click)=\"removeAddOns(addon.name)\">\n                    clear\n                  </i>\n                </span>\n              </li>\n\n            </ul>\n\n\n            <button class=\"btn\" (click)=\"openPricingModal()\">Get Estimation</button>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n<div class=\"pricing-popup\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n\n        <div class=\"modal-body\">\n          <div class=\"row pricing-header\">\n            <div class=\"col-xs-5\">\n              Billing\n            </div>\n            <div class=\"col-xs-2\">\n              Monthly\n            </div>\n            <div class=\"col-xs-1\">\n              <mat-slide-toggle></mat-slide-toggle>\n            </div>\n            <div class=\"col-xs-2\">\n              Annually\n            </div>\n          </div>\n          <div class=\"row\" class=\"\">\n            <div class=\"col-xs-6\">\n              <p>Package</p>\n              <p>Premium</p>\n            </div>\n            <div class=\"col-xs-6\">\n              $ 39.00\n            </div>\n\n          </div>\n\n          <!-- <div class=\"pricing-cont\">\n\n                <ul>\n                  <li>\n                    <label>Package <br> Premium</label>\n                    <span>\n                      $ 39.00\n                    </span>\n                  </li>\n                  <li>\n                      <label> Number of Employees (53) <br>\n                      Total Employee Cost\n                      </label>\n                      <span>\n                        $ 636.00\n                      </span>\n                    </li>\n\n                    <li>\n                        <label>Add-On\n                          <br>Time Tracking\n                          <br>Performance Management\n                          <br>Total Price\n                        </label>\n                        <span>\n                           $ 53.00\n                        </span>\n                      </li>\n                   \n\n                </ul>\n\n                <button class=\"btn\">Get Started</button>\n\n              </div> -->\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/pricing/pricing.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/landing-page/pricing/pricing.component.ts ***!
  \***********************************************************/
/*! exports provided: PricingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingComponent", function() { return PricingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _pricing_modal_pricing_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pricing-modal/pricing-modal.component */ "./src/app/landing-page/pricing/pricing-modal/pricing-modal.component.ts");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PricingComponent = /** @class */ (function () {
    function PricingComponent(dialog, packageAndAddOnService, auth) {
        this.dialog = dialog;
        this.packageAndAddOnService = packageAndAddOnService;
        this.auth = auth;
        this.addOns = [];
        this.packages = [];
        this.allAddOns = [];
        this.isValid = false;
        this.selectedPackage = {
            name: '',
            packages: [],
            adons: [],
        };
        this.validity = false;
        this.adonsPrice = 0;
        this.discountEnable = true;
        this.toggleValue = false;
    }
    PricingComponent.prototype.ngOnInit = function () {
        this.auth.changemessage("false");
        this.getAllPackages();
        this.getAllAddons();
    };
    PricingComponent.prototype.getAllPackages = function () {
        var _this = this;
        this.packageAndAddOnService.getAllPackages()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.packages = res.data;
            }
            else {
                // this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
            }
        });
    };
    PricingComponent.prototype.getAllAddons = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                console.log(_this.allAddOns);
                _this.allAddOns.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
            else {
                // this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error')
            }
        });
        console.log(this.allAddOns);
    };
    PricingComponent.prototype.getSpecificPackageDetails = function () {
        var _this = this;
        // this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        console.log(this.selectedPackage.name, this.packages, this.allAddOns, this.selectedPackage.adons);
        this.packages.forEach(function (packageData) {
            if (packageData.name == _this.selectedPackage.name) {
                _this.selectedPackage.packages = _this.packages.filter(function (data) { return data.name == _this.selectedPackage.name; });
                console.log(_this.selectedPackage.packages);
                if (_this.validity == false && _this.selectedPackage.packages[0].subscriptionType == 'Monthly') {
                    _this.selectedPackage.price = packageData.price;
                }
                if (_this.validity == false && (_this.selectedPackage.packages[0].subscriptionType == "Annual" || _this.selectedPackage.packages[0].subscriptionType == "Yearly")) {
                    _this.selectedPackage.price = packageData.price / 12;
                }
                if (_this.validity == true && _this.selectedPackage.packages[0].subscriptionType == "Monthly") {
                    _this.selectedPackage.price = (packageData.price * 12);
                }
                if (_this.validity == true && (_this.selectedPackage.packages[0].subscriptionType == "Annual" || _this.selectedPackage.packages[0].subscriptionType == "Yearly")) {
                    _this.selectedPackage.price = packageData.price;
                }
                if (_this.validity == false)
                    _this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
            }
        });
        // for (var i = 0; i < this.allAddOns.length; i++) {
        //   console.log(this.allAddOns);
        //   for (var j = 0; j < this.selectedPackage.adons.length; j++) {
        //     console.log(this.selectedPackage.adons[j]);
        //     if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn) {
        //       // if (this.validity == false) {
        //       if (this.validity == false && this.selectedPackage.adons[j].subscriptionType == 'monthly') {
        //         this.getAllAddons()
        //         this.adonsPrice = 0;
        //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)
        //           this.selectedPackage.adons[j].price = this.allAddOns[i].price;
        //       }
        //       if (this.validity == false && this.selectedPackage.adons[j].subscriptionType == "yearly") {
        //         console.log("data come in");
        //         this.getAllAddons()
        //         this.adonsPrice = 0;
        //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)
        //           this.selectedPackage.adons[j].price = this.allAddOns[i].price / 12;
        //       }
        //       if (this.validity == true && this.selectedPackage.adons[j].subscriptionType == "monthly") {
        //         this.getAllAddons()
        //         this.adonsPrice = 0;
        //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)
        //           this.selectedPackage.adons[j].price = this.allAddOns[i].price * 12;
        //       }
        //       if (this.validity == true && this.selectedPackage.adons[j].subscriptionType == "yearly") {
        //         this.getAllAddons()
        //         this.adonsPrice = 0;
        //         if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)
        //           this.selectedPackage.adons[j].price = this.allAddOns[i].price;
        //       }
        //       // else {
        //       //   this.selectedPackage.adons[j].price = this.selectedPackage.adons[j].price * 12;
        //       // }
        //       // this.selectedPackage.costPerEmployee = this.allAddOns[i].costPerEmployee;
        //     }
        //   }
        // }
        //   this.allAddOns.forEach(element => {
        //   this.selectedPackage.adons.forEach(selectAdonsData => {
        //     if (element.name == selectAdonsData.selectedAddOn) {
        //       if (this.validity == false) {
        //         this.selectedPackage.adons = selectAdonsData.price;
        //       }
        //       else {
        //         this.selectedPackage.adons = (selectAdonsData.price * 12);
        //         // selectAdonsData.price
        //       }
        //     }
        //   });
        // });
        // this.adonsPrice = 0;
        // this.selectedPackage.adons.forEach(element => {
        //   console.log(element);
        //   this.adonsPrice = this.adonsPrice + element.price
        // });
        // console.log(this.adonsPrice);
        // this.selectedPackage.totalPrice = this.selectedPackage.price + this.adonsPrice + this.selectedPackage.costPerEmployee * this.employeeCount;
        // if (this.basePriceDiscount > 0) {
        //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
        // }
        // this.discountEnable = false;
        /* this.selectedPackageData = packageData; */
        /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              if (res.status) {
                this.selectedPackageData = res.data;
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err)
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          ) */
        // this.dummy = this.selectedPackage.totalPrice;
        // this.tempforDiscountChange = this.selectedPackage.totalPrice;
    };
    PricingComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    PricingComponent.prototype.selectAddOn = function (event) {
        console.log(event);
        this.addOns = event;
        console.log(this.addOns);
        this.allAddOns.forEach(function (element) {
            if (element == event) {
                // this.allAddOns.splice(
                element.isChecked = true;
            }
        });
        console.log(this.allAddOns);
    };
    PricingComponent.prototype.removeAddOns = function (addon) {
        console.log(addon, this.allAddOns);
        for (var i = 0; i < this.addOns.length; i++) {
            if (this.addOns[i].name === addon) {
                this.addOns.splice(i, 1);
            }
        }
        for (var i = 0; i < this.allAddOns.length; i++) {
            if (this.allAddOns[i].name == addon) {
                console.log(this.allAddOns[i].name, addon);
                this.allAddOns[i].isChecked = false;
            }
        }
    };
    PricingComponent.prototype.openPricingModal = function () {
        this.isValid = true;
        if (this.selectedPackage.name && this.employeeCount && this.addOn) {
            jQuery("#myModal").modal('hide');
            var dialogRef = this.dialog.open(_pricing_modal_pricing_modal_component__WEBPACK_IMPORTED_MODULE_2__["PricingModalComponent"], {
                width: '700px',
                backdropClass: 'structure-model',
                data: {
                    package: this.selectedPackage.packages,
                    addons: this.addOns,
                    employeeCount: this.employeeCount
                },
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                // this.myModal.nativeElement.click();
                jQuery("#myModal").modal('show');
            });
        }
        else {
            console.log("data come");
            // event.preventDefault();
            // jQuery("#myModal").modal({"backdrop": "static"});
            // 
        }
    };
    PricingComponent.prototype.packCalculation = function (event) {
        console.log(event);
        this.toggleValue = event.checked;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('myModal'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], PricingComponent.prototype, "myModal", void 0);
    PricingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pricing',
            template: __webpack_require__(/*! ./pricing.component.html */ "./src/app/landing-page/pricing/pricing.component.html"),
            styles: [__webpack_require__(/*! ./pricing.component.css */ "./src/app/landing-page/pricing/pricing.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_3__["PackageAndAddOnService"],
            _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], PricingComponent);
    return PricingComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/recruitments/recruitments.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/landing-page/recruitments/recruitments.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/recruitments/recruitments.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/landing-page/recruitments/recruitments.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave recruitment-mobile\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Recruitment</h2>\n        <h1>You have chosen wisely!</h1>\n\n        <small>Post jobs to websites and store candidate information within Zenwork HR. Use the job calculator to select\n          the best method in filling your open position. Schedule interviews with applicants, track the candidates\n          as\n          they go through the recruitment process, and track the potential scores for possible future open positions.\n        </small>\n\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/recruitment-img.png\" width=\"618\" height=\"611\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div> -->\n\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <p>Examine what resources hiring takes, such as the associated time and costs of hiring for each position. The\n        recruitment module updates you on the status of your job postings as well as turn around time for it. Use our\n        <b>job calculator</b> to identify hidden recruitment costs and identify which hiring tools are the most\n        effective for\n        your varying open position. You can manage your job postings of various popular platforms such as LinkedIn,\n        Indeed, and more to generate traffic for your job openings.</p>\n      <p>Schedule interviews and track <b>candidate statuses and candidate potential scores,</b> all from inside Zenwork\n        HR.\n        Tired of starting of scratch from when hiring for a new job opening? We make creating and storing job\n        descriptions in your <b>job bank</b> easy and guarantee that using Zenwork HR tools it will help speed up the\n        process of\n        filling new job openings.</p>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/recruitments/recruitments.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/landing-page/recruitments/recruitments.component.ts ***!
  \*********************************************************************/
/*! exports provided: RecruitmentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecruitmentsComponent", function() { return RecruitmentsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecruitmentsComponent = /** @class */ (function () {
    function RecruitmentsComponent(auth) {
        this.auth = auth;
    }
    RecruitmentsComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    RecruitmentsComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    RecruitmentsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-recruitments',
            template: __webpack_require__(/*! ./recruitments.component.html */ "./src/app/landing-page/recruitments/recruitments.component.html"),
            styles: [__webpack_require__(/*! ./recruitments.component.css */ "./src/app/landing-page/recruitments/recruitments.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], RecruitmentsComponent);
    return RecruitmentsComponent;
}());



/***/ }),

/***/ "./src/app/landing-page/security/security.component.css":
/*!**************************************************************!*\
  !*** ./src/app/landing-page/security/security.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave .feature-benefits-lft h1 { margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/landing-page/security/security.component.html":
/*!***************************************************************!*\
  !*** ./src/app/landing-page/security/security.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feature-benefits leave security-mobile\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <figure><img src=\"../../../assets/images/home/feature-bg.png\" width=\"1409\" height=\"672\" alt=\"img\"></figure>\n      <div class=\"feature-benefits-lft col-sm-7\">\n\n        <h2>Security</h2>\n        <h1>It’s like having a superhero protect your data!</h1>\n\n        <small>We want to secure our platform and offer the leading practical security options to our users, such as\n          two-factor authentication.</small>\n\n        <a href=\"https://calendly.com/zenworkhr/intro\" class=\"btn\" target=\"_blank\">Register for a Demo</a>\n\n      </div>\n      <div class=\"feature-benefits-rgt pull-right col-sm-5\">\n        <span><img src=\"../../../assets/images/home/security-img.png\" width=\"618\" height=\"520\" alt=\"img\"></span>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n</div>\n\n\n\n<!-- <div id=\"arrow-down\">\n    <i class=\"fa fa-chevron-down\"></i>\n  </div> -->\n\n<div class=\"arrow\"> <a (click)=\"arrowDown(content)\"><img src=\"../../../assets/images/home/down-arrow.png\" width=\"94\"\n      height=\"41\" alt=\"img\"></a></div>\n\n\n\n<div #content class=\"benefits-cont\">\n  <div class=\"container\">\n    <div class=\"row\">\n\n      <p>Zenwork HR protects your sensitive information under 256-bit encrypted security. The same security level used\n        by banks, the U.S. government, and even the U.S. military. This encryption level would take 2 to the 256th power\n        (1.1579209*10^77) different combinations to break, something that even the most advanced computers in our day\n        would find problematic to figure out. Zenwork also renews and becomes certified in SSAE-16 Type II every year.\n        We take advice from security audit professionals and stay one step ahead in data security.</p>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/landing-page/security/security.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/landing-page/security/security.component.ts ***!
  \*************************************************************/
/*! exports provided: SecurityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityComponent", function() { return SecurityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SecurityComponent = /** @class */ (function () {
    function SecurityComponent(auth) {
        this.auth = auth;
    }
    SecurityComponent.prototype.ngOnInit = function () {
        this.auth.changemessage('false');
    };
    SecurityComponent.prototype.arrowDown = function (el) {
        el.scrollIntoView();
    };
    SecurityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-security',
            template: __webpack_require__(/*! ./security.component.html */ "./src/app/landing-page/security/security.component.html"),
            styles: [__webpack_require__(/*! ./security.component.css */ "./src/app/landing-page/security/security.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], SecurityComponent);
    return SecurityComponent;
}());



/***/ })

}]);
//# sourceMappingURL=landing-page-landing-page-module.js.map