(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../assets/assets.module": [
		"./src/app/admin-dashboard/employee-management/assets/assets.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"assets-assets-module"
	],
	"../audit-trail/audit-trail.module": [
		"./src/app/admin-dashboard/employee-management/audit-trail/audit-trail.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"audit-trail-audit-trail-module"
	],
	"../benefits/benefits.module": [
		"./src/app/admin-dashboard/employee-management/benefits/benefits.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"benefits-benefits-module"
	],
	"../black-out-dates/black-out-dates.module": [
		"./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"common",
		"black-out-dates-black-out-dates-module"
	],
	"../compensation/compensation.module": [
		"./src/app/admin-dashboard/employee-management/compensation/compensation.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"compensation-compensation-module"
	],
	"../custom/custom.module": [
		"./src/app/admin-dashboard/employee-management/custom/custom.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"custom-custom-module"
	],
	"../documents/documents.module": [
		"./src/app/admin-dashboard/employee-management/documents/documents.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"documents-documents-module"
	],
	"../emergency/emergency.module": [
		"./src/app/admin-dashboard/employee-management/emergency/emergency.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"emergency-emergency-module"
	],
	"../job/job.module": [
		"./src/app/admin-dashboard/employee-management/job/job.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"directory-directory-module~employee-management-employee-management-module~employee-profile-view-empl~4316b310",
		"employee-profile-view-employee-profile-view-module~job-job-module",
		"common"
	],
	"../notes/notes.module": [
		"./src/app/admin-dashboard/employee-management/notes/notes.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"notes-notes-module"
	],
	"../offboardingtab/offboardingtab.module": [
		"./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"offboardingtab-offboardingtab-module"
	],
	"../onboardingtab/onboardingtab.module": [
		"./src/app/admin-dashboard/employee-management/onboardingtab/onboardingtab.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"onboardingtab-onboardingtab-module"
	],
	"../performance/performance.module": [
		"./src/app/admin-dashboard/employee-management/performance/performance.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"performance-performance-module"
	],
	"../reseller-dashboard/reseller-dashboard.module": [
		"./src/app/reseller-dashboard/reseller-dashboard.module.ts",
		"common",
		"reseller-dashboard-reseller-dashboard-module"
	],
	"../super-admin-dashboard/client-details/client-details.module": [
		"./src/app/super-admin-dashboard/client-details/client-details.module.ts",
		"common",
		"client-details-client-details-module"
	],
	"../time-schedule/time-schedule.module": [
		"./src/app/admin-dashboard/employee-management/time-schedule/time-schedule.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~t~b03af65c",
		"common",
		"time-schedule-time-schedule-module"
	],
	"../training/training.module": [
		"./src/app/admin-dashboard/employee-management/training/training.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"common",
		"training-training-module"
	],
	"../work-schedules/work-schedules.module": [
		"./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"leave-management-leave-management-module~manager-approval-manager-approval-module~manager-self-servi~f85839ce",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~t~b03af65c",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~w~5c9d7c5d",
		"leave-management-leave-management-module~scheduler-scheduler-module~work-schedules-work-schedules-mo~f3cda310",
		"common"
	],
	"./admin-dashboard/admin-dashboard.module": [
		"./src/app/admin-dashboard/admin-dashboard.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"admin-dashboard-admin-dashboard-module"
	],
	"./authentication/authentication.module": [
		"./src/app/authentication/authentication.module.ts",
		"authentication-authentication-module"
	],
	"./client-details/client-details.module": [
		"./src/app/super-admin-dashboard/client-details/client-details.module.ts",
		"common",
		"client-details-client-details-module"
	],
	"./company-settings/company-settings.module": [
		"./src/app/admin-dashboard/company-settings/company-settings.module.ts",
		"company-settings-company-settings-module~employee-management-employee-management-module~manager-self~95d72917",
		"common",
		"company-settings-company-settings-module"
	],
	"./company-setup/company-setup.module": [
		"./src/app/admin-dashboard/company-settings/company-setup/company-setup.module.ts",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"company-setup-company-setup-module~employee-management-employee-management-module~employee-profile-v~387b4481",
		"common",
		"company-setup-company-setup-module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/admin-dashboard/dashboard/dashboard.module.ts",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"common",
		"dashboard-dashboard-module"
	],
	"./directory/directory.module": [
		"./src/app/admin-dashboard/employee-management/directory/directory.module.ts",
		"directory-directory-module~employee-management-employee-management-module~employee-profile-view-empl~4316b310",
		"common",
		"directory-directory-module"
	],
	"./employee-management/employee-management.module": [
		"./src/app/admin-dashboard/employee-management/employee-management.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"company-setup-company-setup-module~employee-management-employee-management-module~employee-profile-v~387b4481",
		"directory-directory-module~employee-management-employee-management-module~employee-profile-view-empl~4316b310",
		"company-settings-company-settings-module~employee-management-employee-management-module~manager-self~95d72917",
		"common",
		"employee-management-employee-management-module"
	],
	"./employee-profile-view/employee-profile-view.module": [
		"./src/app/admin-dashboard/employee-management/employee-profile-view/employee-profile-view.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"company-setup-company-setup-module~employee-management-employee-management-module~employee-profile-v~387b4481",
		"directory-directory-module~employee-management-employee-management-module~employee-profile-view-empl~4316b310",
		"employee-profile-view-employee-profile-view-module~job-job-module",
		"common",
		"employee-profile-view-employee-profile-view-module"
	],
	"./employee-self-service/employee-self-service.module": [
		"./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.module.ts",
		"common",
		"employee-self-service-employee-self-service-module"
	],
	"./inbox/inbox.module": [
		"./src/app/admin-dashboard/inbox/inbox.module.ts",
		"inbox-inbox-module"
	],
	"./job-description/job-description.module": [
		"./src/app/admin-dashboard/recruitment/job-description/job-description.module.ts",
		"job-description-job-description-module"
	],
	"./landing-page/landing-page.module": [
		"./src/app/landing-page/landing-page.module.ts",
		"landing-page-landing-page-module"
	],
	"./leave-management-eligibility/leave-management-eligibility.module": [
		"./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.module.ts",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"common",
		"leave-management-eligibility-leave-management-eligibility-module"
	],
	"./leave-management/leave-management.module": [
		"./src/app/admin-dashboard/leave-management/leave-management.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"leave-management-leave-management-module~manager-approval-manager-approval-module~manager-self-servi~f85839ce",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~t~b03af65c",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~w~5c9d7c5d",
		"leave-management-leave-management-module~scheduler-scheduler-module~work-schedules-work-schedules-mo~f3cda310",
		"leave-management-leave-management-module~scheduler-scheduler-module",
		"common",
		"leave-management-leave-management-module"
	],
	"./main-landing/main-landing.module": [
		"./src/app/main-landing/main-landing.module.ts"
	],
	"./manager-approval/manager-approval.module": [
		"./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.module.ts",
		"leave-management-leave-management-module~manager-approval-manager-approval-module~manager-self-servi~f85839ce",
		"common",
		"manager-approval-manager-approval-module"
	],
	"./manager-self-service/manager-self-service.module": [
		"./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"leave-management-leave-management-module~manager-approval-manager-approval-module~manager-self-servi~f85839ce",
		"manager-self-service-manager-self-service-module~off-boarding-landing-off-boarding-landing-module~on~75206ba3",
		"company-settings-company-settings-module~employee-management-employee-management-module~manager-self~95d72917",
		"common",
		"manager-self-service-manager-self-service-module"
	],
	"./new-hire-hr-time/new-hire-hr-time.module": [
		"./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"new-hire-hr-time-new-hire-hr-time-module"
	],
	"./new-hire-hr/new-hire-hr.module": [
		"./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"new-hire-hr-new-hire-hr-module"
	],
	"./new-hire-wizard/new-hire-wizard.module": [
		"./src/app/admin-dashboard/employee-management/new-hire-wizard/new-hire-wizard.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"new-hire-wizard-new-hire-wizard-module"
	],
	"./off-boarding-landing/off-boarding-landing.module": [
		"./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"manager-self-service-manager-self-service-module~off-boarding-landing-off-boarding-landing-module~on~75206ba3",
		"common",
		"off-boarding-landing-off-boarding-landing-module"
	],
	"./on-boarding/on-boarding.module": [
		"./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.module.ts",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~t~b03af65c",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~w~5c9d7c5d",
		"company-setup-company-setup-module~employee-management-employee-management-module~employee-profile-v~387b4481",
		"manager-self-service-manager-self-service-module~off-boarding-landing-off-boarding-landing-module~on~75206ba3",
		"common",
		"on-boarding-on-boarding-module"
	],
	"./package-creation/package-creation.module": [
		"./src/app/super-admin-dashboard/package-creation/package-creation.module.ts",
		"package-creation-package-creation-module"
	],
	"./post-jobs/post-jobs.module": [
		"./src/app/admin-dashboard/recruitment/post-jobs/post-jobs.module.ts",
		"post-jobs-post-jobs-module"
	],
	"./recruitment/recruitment.module": [
		"./src/app/admin-dashboard/recruitment/recruitment.module.ts",
		"recruitment-recruitment-module"
	],
	"./scheduler/scheduler.module": [
		"./src/app/admin-dashboard/leave-management/scheduler/scheduler.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"leave-management-leave-management-module~manager-approval-manager-approval-module~manager-self-servi~f85839ce",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~t~b03af65c",
		"leave-management-leave-management-module~on-boarding-on-boarding-module~scheduler-scheduler-module~w~5c9d7c5d",
		"leave-management-leave-management-module~scheduler-scheduler-module~work-schedules-work-schedules-mo~f3cda310",
		"leave-management-leave-management-module~scheduler-scheduler-module",
		"common"
	],
	"./self-boarding/self-boarding.module": [
		"./src/app/self-boarding/self-boarding.module.ts",
		"self-boarding-self-boarding-module"
	],
	"./site-access/site-access.module": [
		"./src/app/admin-dashboard/company-settings/site-access/site-access.module.ts",
		"assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375",
		"black-out-dates-black-out-dates-module~company-setup-company-setup-module~leave-management-eligibili~a04f54fb",
		"common",
		"site-access-site-access-module"
	],
	"./site-user/site-user.module": [
		"./src/app/admin-dashboard/company-settings/site-access/site-user/site-user.module.ts",
		"site-user-site-user-module"
	],
	"./super-admin-dashboard/super-admin-dashboard.module": [
		"./src/app/super-admin-dashboard/super-admin-dashboard.module.ts",
		"common",
		"super-admin-dashboard-super-admin-dashboard-module"
	],
	"./termination-wizard/termination-wizard.module": [
		"./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard.module.ts",
		"admin-dashboard-admin-dashboard-module~audit-trail-audit-trail-module~benefits-benefits-module~black~39327227",
		"common",
		"termination-wizard-termination-wizard-module"
	],
	"./workflow-approval/workflow-approval.module": [
		"./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.module.ts",
		"leave-management-leave-management-module~manager-approval-manager-approval-module~manager-self-servi~f85839ce",
		"manager-self-service-manager-self-service-module~off-boarding-landing-off-boarding-landing-module~on~75206ba3",
		"common",
		"workflow-approval-workflow-approval-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#circularG {\n    position: relative;\n    width: 113px;\n    height: 113px;\n    margin: auto;\n  }\n  \n  .circularG {\n    position: absolute;\n    background-color: #008f3d;\n    width: 26px;\n    height: 26px;\n    border-radius: 17px;\n    -o-border-radius: 17px;\n    -ms-border-radius: 17px;\n    -webkit-border-radius: 17px;\n    -moz-border-radius: 17px;\n    animation-name: bounce_circularG;\n    -o-animation-name: bounce_circularG;\n    -ms-animation-name: bounce_circularG;\n    -webkit-animation-name: bounce_circularG;\n    -moz-animation-name: bounce_circularG;\n    animation-duration: 1.1s;\n    -o-animation-duration: 1.1s;\n    -ms-animation-duration: 1.1s;\n    -webkit-animation-duration: 1.1s;\n    -moz-animation-duration: 1.1s;\n    animation-iteration-count: infinite;\n    -o-animation-iteration-count: infinite;\n    -ms-animation-iteration-count: infinite;\n    -webkit-animation-iteration-count: infinite;\n    -moz-animation-iteration-count: infinite;\n    animation-direction: normal;\n    -o-animation-direction: normal;\n    -ms-animation-direction: normal;\n    -webkit-animation-direction: normal;\n    -moz-animation-direction: normal;\n  }\n  \n  #circularG_1 {\n    left: 0;\n    top: 45px;\n    animation-delay: .41s;\n    -o-animation-delay: .41s;\n    -ms-animation-delay: .41s;\n    -webkit-animation-delay: .41s;\n    -moz-animation-delay: .41s;\n  }\n  \n  #circularG_2 {\n    left: 11px;\n    top: 11px;\n    animation-delay: .55s;\n    -o-animation-delay: .55s;\n    -ms-animation-delay: .55s;\n    -webkit-animation-delay: .55s;\n    -moz-animation-delay: .55s;\n  }\n  \n  #circularG_3 {\n    top: 0;\n    left: 45px;\n    animation-delay: .69s;\n    -o-animation-delay: .69s;\n    -ms-animation-delay: .69s;\n    -webkit-animation-delay: .69s;\n    -moz-animation-delay: .69s;\n  }\n  \n  #circularG_4 {\n    right: 11px;\n    top: 11px;\n    animation-delay: .83s;\n    -o-animation-delay: .83s;\n    -ms-animation-delay: .83s;\n    -webkit-animation-delay: .83s;\n    -moz-animation-delay: .83s;\n  }\n  \n  #circularG_5 {\n    right: 0;\n    top: 45px;\n    animation-delay: .97s;\n    -o-animation-delay: .97s;\n    -ms-animation-delay: .97s;\n    -webkit-animation-delay: .97s;\n    -moz-animation-delay: .97s;\n  }\n  \n  #circularG_6 {\n    right: 11px;\n    bottom: 11px;\n    animation-delay: 1.1s;\n    -o-animation-delay: 1.1s;\n    -ms-animation-delay: 1.1s;\n    -webkit-animation-delay: 1.1s;\n    -moz-animation-delay: 1.1s;\n  }\n  \n  #circularG_7 {\n    left: 45px;\n    bottom: 0;\n    animation-delay: 1.24s;\n    -o-animation-delay: 1.24s;\n    -ms-animation-delay: 1.24s;\n    -webkit-animation-delay: 1.24s;\n    -moz-animation-delay: 1.24s;\n  }\n  \n  #circularG_8 {\n    left: 11px;\n    bottom: 11px;\n    animation-delay: 1.38s;\n    -o-animation-delay: 1.38s;\n    -ms-animation-delay: 1.38s;\n    -webkit-animation-delay: 1.38s;\n    -moz-animation-delay: 1.38s;\n  }\n  \n  @keyframes bounce_circularG {\n    0% {\n      -webkit-transform: scale(1);\n              transform: scale(1);\n    }\n  \n    100% {\n      -webkit-transform: scale(.3);\n              transform: scale(.3);\n    }\n  }\n  \n  @-webkit-keyframes bounce_circularG {\n    0% {\n      -webkit-transform: scale(1);\n    }\n  \n    100% {\n      -webkit-transform: scale(.3);\n    }\n  }\n  \n  .loadbar {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    text-align: center;\n    background-color: rgba(243, 243, 243, 0.3);\n    z-index: 1000;\n  }\n  \n  .load {\n    margin: 0 auto;\n    position: relative;\n    top: 40%;\n    height: 50px;\n    width: 50px;\n  }\n  "

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ngx-spinner bdColor=\"rgba(51, 51, 51, 0.8)\" size=\"large\" color=\"#fff\" type=\"square-jelly-box\"></ngx-spinner> -->\n\n<div class=\"loadbar\" *ngIf=\"loaderService.showLoading\">\n  <div id=\"circularG\" class=\"load\">\n      <div id=\"circularG_1\" class=\"circularG\"></div>\n      <div id=\"circularG_2\" class=\"circularG\"></div>\n      <div id=\"circularG_3\" class=\"circularG\"></div>\n      <div id=\"circularG_4\" class=\"circularG\"></div>\n      <div id=\"circularG_5\" class=\"circularG\"></div>\n      <div id=\"circularG_6\" class=\"circularG\"></div>\n      <div id=\"circularG_7\" class=\"circularG\"></div>\n      <div id=\"circularG_8\" class=\"circularG\"></div>\n  </div>\n</div>\n<router-outlet></router-outlet>\n\n\n\n<!-- Code For Angular Calander -->\n\n\n<!-- <ng-template #modalContent let-close=\"close\">\n    <div class=\"modal-header\">\n      <h5 class=\"modal-title\">Event action occurred</h5>\n      <button type=\"button\" class=\"close\" (click)=\"close()\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <div>\n        Action:\n        <pre>{{ modalData?.action }}</pre>\n      </div>\n      <div>\n        Event:\n        <pre>{{ modalData?.event | json }}</pre>\n      </div>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-outline-secondary\" (click)=\"close()\">OK</button>\n    </div>\n  </ng-template>\n  \n  <div class=\"row text-center\">\n    <div class=\"col-md-4\">\n      <div class=\"btn-group\">\n        <div\n          class=\"btn btn-primary\"\n          mwlCalendarPreviousView\n          [view]=\"view\"\n          [(viewDate)]=\"viewDate\"\n          (viewDateChange)=\"activeDayIsOpen = false\">\n          Previous\n        </div>\n        <div\n          class=\"btn btn-outline-secondary\"\n          mwlCalendarToday\n          [(viewDate)]=\"viewDate\">\n          Today\n        </div>\n        <div\n          class=\"btn btn-primary\"\n          mwlCalendarNextView\n          [view]=\"view\"\n          [(viewDate)]=\"viewDate\"\n          (viewDateChange)=\"activeDayIsOpen = false\">\n          Next\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-4\">\n      <h3>{{ viewDate | calendarDate:(view + 'ViewTitle'):'en' }}</h3>\n    </div>\n    <div class=\"col-md-4\">\n      <div class=\"btn-group\">\n        <div\n          class=\"btn btn-primary\"\n          (click)=\"view = CalendarView.Month\"\n          [class.active]=\"view === CalendarView.Month\">\n          Month\n        </div>\n        <div\n          class=\"btn btn-primary\"\n          (click)=\"view = CalendarView.Week\"\n          [class.active]=\"view === CalendarView.Week\">\n          Week\n        </div>\n        <div\n          class=\"btn btn-primary\"\n          (click)=\"view = CalendarView.Day\"\n          [class.active]=\"view === CalendarView.Day\">\n          Day\n        </div>\n      </div>\n    </div>\n  </div>\n  <br>\n  <div [ngSwitch]=\"view\">\n    <mwl-calendar-month-view\n      *ngSwitchCase=\"CalendarView.Month\"\n      [viewDate]=\"viewDate\"\n      [events]=\"events\"\n      [refresh]=\"refresh\"\n      [activeDayIsOpen]=\"activeDayIsOpen\"\n      (dayClicked)=\"dayClicked($event.day)\"\n      (eventClicked)=\"handleEvent('Clicked', $event.event)\"\n      (eventTimesChanged)=\"eventTimesChanged($event)\">\n    </mwl-calendar-month-view>\n    <mwl-calendar-week-view\n      *ngSwitchCase=\"CalendarView.Week\"\n      [viewDate]=\"viewDate\"\n      [events]=\"events\"\n      [refresh]=\"refresh\"\n      (eventClicked)=\"handleEvent('Clicked', $event.event)\"\n      (eventTimesChanged)=\"eventTimesChanged($event)\">\n    </mwl-calendar-week-view>\n    <mwl-calendar-day-view\n      *ngSwitchCase=\"CalendarView.Day\"\n      [viewDate]=\"viewDate\"\n      [events]=\"events\"\n      [refresh]=\"refresh\"\n      (eventClicked)=\"handleEvent('Clicked', $event.event)\"\n      (eventTimesChanged)=\"eventTimesChanged($event)\">\n    </mwl-calendar-day-view>\n  </div>\n  \n  <br><br><br>\n  \n  <h3>\n    Edit events\n    <button\n      class=\"btn btn-primary pull-right\"\n      (click)=\"addEvent()\">\n      Add new\n    </button>\n    <div class=\"clearfix\"></div>\n  </h3>\n  \n  <table class=\"table table-bordered\">\n\n\n\n\n\n <table class=\"table table-bordered\">\n  \n    <thead>\n      <tr>\n        <th>Title</th>\n        <th>Primary color</th>\n        <th>Secondary color</th>\n        <th>Starts at</th>\n        <th>Ends at</th>\n        <th>Remove</th>\n      </tr>\n    </thead>\n  \n    <tbody>\n      <tr *ngFor=\"let event of events; let index = index\">\n        <td>\n          <input\n            type=\"text\"\n            class=\"form-control\"\n            [(ngModel)]=\"event.title\"\n            (keyup)=\"refresh.next()\">\n        </td>\n        <td>\n          <input\n            type=\"color\"\n            [(ngModel)]=\"event.color.primary\"\n            (change)=\"refresh.next()\">\n        </td>\n        <td>\n          <input\n            type=\"color\"\n            [(ngModel)]=\"event.color.secondary\"\n            (change)=\"refresh.next()\">\n        </td>\n        <td>\n          <input\n            class=\"form-control\"\n            type=\"text\"\n            mwlFlatpickr\n            [(ngModel)]=\"event.start\"\n            (ngModelChange)=\"refresh.next()\"\n            dateFormat=\"Y-m-dTH:i\"\n            altFormat=\"F j, Y H:i\"\n            placeholder=\"Not set\">\n        </td>\n        <td>\n          <input\n            class=\"form-control\"\n            type=\"text\"\n            mwlFlatpickr\n            [(ngModel)]=\"event.end\"\n            (ngModelChange)=\"refresh.next()\"\n            dateFormat=\"Y-m-dTH:i\"\n            altFormat=\"F j, Y H:i\"\n            placeholder=\"Not set\">\n        </td>\n        <td>\n          <button\n            class=\"btn btn-danger\"\n            (click)=\"events.splice(index, 1); refresh.next()\">\n            Delete\n          </button>\n        </td>\n      </tr>\n    </tbody>\n  \n  </table> -->\n\n\n<!-- Code For Angular Calander Ends -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var ngx_bootstrap_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ngx-bootstrap/utils */ "./node_modules/ngx-bootstrap/utils/fesm5/ngx-bootstrap-utils.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppComponent = /** @class */ (function () {
    function AppComponent(modal, router, messageService, loaderService) {
        this.modal = modal;
        this.router = router;
        this.messageService = messageService;
        this.loaderService = loaderService;
        Object(ngx_bootstrap_utils__WEBPACK_IMPORTED_MODULE_0__["setTheme"])('bs3');
        // router.events.subscribe((url: any) => console.log(url));
        // console.log("111111111111",router.url);
        // this.url = router.url;
    }
    /* public colors: any = {
      red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
      },
      blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
      },
      yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
      }
    };
    @ViewChild('modalContent')
    modalContent: TemplateRef<any>;
  
    view: CalendarView = CalendarView.Month;
  
    CalendarView = CalendarView;
  
    viewDate: Date = new Date();
  
    modalData: {
      action: string;
      event: CalendarEvent;
    }; */
    AppComponent.prototype.ngOnInit = function () {
    };
    /* actions: CalendarEventAction[] = [
      {
        label: '<i class="fa fa-fw fa-pencil"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.handleEvent('Edited', event);
        }
      },
      {
        label: '<i class="fa fa-fw fa-times"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.events = this.events.filter(iEvent => iEvent !== event);
          this.handleEvent('Deleted', event);
        }
      }
    ];
  
    refresh: Subject<any> = new Subject();
  
    events: CalendarEvent[] = [
      {
        start: subDays(startOfDay(new Date()), 1),
        end: addDays(new Date(), 1),
        title: 'A 3 day event',
        color: this.colors.red,
        actions: this.actions,
        allDay: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        },
        draggable: true
      },
      {
        start: startOfDay(new Date()),
        title: 'An event with no end date',
        color: this.colors.yellow,
        actions: this.actions
      },
      {
        start: subDays(endOfMonth(new Date()), 3),
        end: addDays(endOfMonth(new Date()), 3),
        title: 'A long event that spans 2 months',
        color: this.colors.blue,
        allDay: true
      },
      {
        start: addHours(startOfDay(new Date()), 2),
        end: new Date(),
        title: 'A draggable and resizable event',
        color: this.colors.yellow,
        actions: this.actions,
        resizable: {
          beforeStart: true,
          afterEnd: true
        },
        draggable: true
      }
    ];
  
    activeDayIsOpen: boolean = true;
  
  
  
    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
      if (isSameMonth(date, this.viewDate)) {
        this.viewDate = date;
        if (
          (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
          events.length === 0
        ) {
          this.activeDayIsOpen = false;
        } else {
          this.activeDayIsOpen = true;
        }
      }
    }
  
    eventTimesChanged({
      event,
      newStart,
      newEnd
    }: CalendarEventTimesChangedEvent): void {
      event.start = newStart;
      event.end = newEnd;
      this.handleEvent('Dropped or resized', event);
      this.refresh.next();
    }
  
    handleEvent(action: string, event: CalendarEvent): void {
      this.modalData = { event, action };
      this.modal.open(this.modalContent, { size: 'lg' });
    }
  
    addEvent(): void {
      this.events.push({
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: this.colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      });
      this.refresh.next();
    } */
    AppComponent.prototype.ngOnDestroy = function () { };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.routes */ "./src/app/app.routes.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _main_landing_main_landing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main-landing/main-landing.module */ "./src/app/main-landing/main-landing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_client_demo_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/client-demo.service */ "./src/app/services/client-demo.service.ts");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_tokenInterceptor_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/tokenInterceptor.service */ "./src/app/services/tokenInterceptor.service.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./services/purchasePackage.service */ "./src/app/services/purchasePackage.service.ts");
/* harmony import */ var _services_invoice_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./services/invoice.service */ "./src/app/services/invoice.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _email_verify_email_verify_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./email-verify/email-verify.component */ "./src/app/email-verify/email-verify.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























// import { ResellerDashboardComponent } from './reseller-dashboard/reseller-dashboard.component';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _email_verify_email_verify_component__WEBPACK_IMPORTED_MODULE_26__["EmailVerifyComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _main_landing_main_landing_module__WEBPACK_IMPORTED_MODULE_7__["MainLandingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"],
                _app_routes__WEBPACK_IMPORTED_MODULE_4__["AppRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_13__["NgxMatSelectSearchModule"],
                ngx_spinner__WEBPACK_IMPORTED_MODULE_16__["NgxSpinnerModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_25__["MatStepperModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_27__["MaterialModuleModule"]
            ],
            providers: [
                _services_message_service_service__WEBPACK_IMPORTED_MODULE_23__["MessageService"],
                _services_client_demo_service__WEBPACK_IMPORTED_MODULE_9__["ClientDemoRegistrationService"],
                _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_10__["ZenworkersService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HTTP_INTERCEPTORS"],
                    useClass: _services_tokenInterceptor_service__WEBPACK_IMPORTED_MODULE_12__["TokenInterceptor"],
                    multi: true
                },
                _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_11__["AccessLocalStorageService"],
                _services_company_service__WEBPACK_IMPORTED_MODULE_14__["CompanyService"],
                _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_15__["SwalAlertService"],
                _services_loader_service__WEBPACK_IMPORTED_MODULE_17__["LoaderService"],
                _services_employee_service__WEBPACK_IMPORTED_MODULE_19__["EmployeeService"],
                _services_companySettings_service__WEBPACK_IMPORTED_MODULE_18__["CompanySettingsService"],
                _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_20__["PackageAndAddOnService"],
                _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_21__["PurchasePackageService"],
                _services_invoice_service__WEBPACK_IMPORTED_MODULE_22__["InvoiceService"],
                _services_authentication_service__WEBPACK_IMPORTED_MODULE_24__["AuthenticationService"],
                // AuthGuardService,
                _auth_service__WEBPACK_IMPORTED_MODULE_28__["AuthService"],
                _auth_guard__WEBPACK_IMPORTED_MODULE_29__["AuthGuard"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routes.ts":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: AppRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRouting", function() { return AppRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared-module/shared.module */ "./src/app/shared-module/shared.module.ts");
/* harmony import */ var _email_verify_email_verify_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./email-verify/email-verify.component */ "./src/app/email-verify/email-verify.component.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    // { path: '', redirectTo: 'login', pathMatch: "full" },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'email-verify/:id', component: _email_verify_email_verify_component__WEBPACK_IMPORTED_MODULE_4__["EmailVerifyComponent"] },
    { path: 'admin', loadChildren: "./admin-dashboard/admin-dashboard.module#AdminDashboardModule", canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: 'main-landing', loadChildren: "./main-landing/main-landing.module#MainLandingModule" },
    { path: 'super-admin', loadChildren: "./super-admin-dashboard/super-admin-dashboard.module#SuperAdminDashboardModule", canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: '', loadChildren: "./landing-page/landing-page.module#LandingPageModule" },
    { path: '', loadChildren: "./authentication/authentication.module#AuthenticationModule" },
    { path: 'self-on-boarding', loadChildren: "./self-boarding/self-boarding.module#SelfBoardingModule" }
];
var AppRouting = /** @class */ (function () {
    function AppRouting() {
    }
    AppRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            providers: [
                _auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
                _auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"],
            ]
        })
    ], AppRouting);
    return AppRouting;
}());



/***/ }),

/***/ "./src/app/auth.guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        console.log("data come in");
        if (this.auth.getToken()) {
            console.log("data come in");
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = /** @class */ (function () {
    function AuthService() {
        this.messageSource = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]("start");
        this.currentMessage = this.messageSource.asObservable();
    }
    AuthService.prototype.getToken = function () {
        return localStorage.getItem("x-access-token");
    };
    AuthService.prototype.isAuthenticated = function () {
        // get the token
        var token = this.getToken();
        // return a boolean reflecting
        // whether or not the token is expired
        return token ? true : false;
    };
    AuthService.prototype.changemessage = function (message) {
        this.messageSource.next(message);
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/email-verify/email-verify.component.css":
/*!*********************************************************!*\
  !*** ./src/app/email-verify/email-verify.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center { \n    height: 200px;\n    position: relative;\n    /* border: 3px solid green;  */\n  }\n  \n  .center h2 {\n    margin: 0;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n  }\n  \n  .login-bg{\n    /* background-image: linear-gradient(to bottom, #11aac0, #43b5c7, #5fbfcf, #78cad7, #9dd9e3); */\n    background-size: cover;\n    /* position: relative; */\n    /* height: 100%; */\n    background: #ffffff;\n}\n  \n  .login-form{\n    height: 400px;\n    width: 475px;\n    text-align: center;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    background-color: #ffffff;\n    padding: 2% 5%;\n    box-shadow: 0px 0px 10px 4px rgba(0,0,0, 0.1);\n}\n  \n  .login-header{\n    text-align: center;\n    color: black;\n    font-weight: 500;\n    /* font-family: 'sf_ui_displaymedium'; */\n    margin-bottom: 25px;\n}\n  \n  .login-btn{\n      padding: 10px 30px;\n      background-color: #fcd65c;\n      border: 1px solid #fcd65c;\n      color: #5a461a;\n      font-size: 18px;\n      margin-top: 35px;\n      border-radius: 3px;\n\n  }"

/***/ }),

/***/ "./src/app/email-verify/email-verify.component.html":
/*!**********************************************************!*\
  !*** ./src/app/email-verify/email-verify.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid login-bg\">\n  <div class=\"login-form\">\n    <img src=\"./assets/images/company-settings/right-circle.png\">\n    <h2 class=\"login-header\">Thanks for verifying your email</h2>\n\n    <a routerLink='/login'>\n      <button type=\"submit\" class=\"login-btn\">Back To Login</button>\n    </a>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/email-verify/email-verify.component.ts":
/*!********************************************************!*\
  !*** ./src/app/email-verify/email-verify.component.ts ***!
  \********************************************************/
/*! exports provided: EmailVerifyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailVerifyComponent", function() { return EmailVerifyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmailVerifyComponent = /** @class */ (function () {
    function EmailVerifyComponent(router, route, companySettingsService) {
        this.router = router;
        this.route = route;
        this.companySettingsService = companySettingsService;
    }
    EmailVerifyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.token = params['id'];
            console.log(_this.token);
            var tokenDetails = {
                token: _this.token
            };
            _this.companySettingsService.updateEmailUser(tokenDetails)
                .subscribe(function (response) {
                console.log(response);
            });
        });
    };
    EmailVerifyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-verify',
            template: __webpack_require__(/*! ./email-verify.component.html */ "./src/app/email-verify/email-verify.component.html"),
            styles: [__webpack_require__(/*! ./email-verify.component.css */ "./src/app/email-verify/email-verify.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"]])
    ], EmailVerifyComponent);
    return EmailVerifyComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-text{\n    font-size: 30px;\n    font-weight: bold;\n    text-align: center;\n    margin-top:0px;\n    margin-bottom: 24px;\n    letter-spacing: 1.1px;\n    height: 28px;\n    color:#383434;\n}\n.login-form-alignment{\n    margin-top: 30px;\n}\n.login-form-alignment label { font-weight: 600;color:#797676; padding: 0 0 7px;}\n.login-form-alignment .form-control:focus, .login-form-alignment .form-control:active{ box-shadow:none;border: #66afe9 1px solid;}\n.login-form-alignment .form-control:select:focus, .login-form-alignment .form-control:select:active { box-shadow:none;border-color: #008f3d;}\n.zenwork-customized-bg{\n    background-color: #3e3e3e!important;\n}\n.btn-login-customized{\n    width: 100%;\n    height: 58px;\n    border-radius: 5px;\n    background-color: #13964f;\n    color: #fff;\n    box-shadow: 0px 1px 8.6px 0.5px rgba(0, 0, 0, 0.23); font-size: 18px; font-weight: 600; margin: 30px 0 0;\n}\n.login-wrapper{\n    position: relative;\n}\n.login-logo{\n    position: absolute;\n    top: 14%;\n    left: 50%;\n    -webkit-transform: translate(-50%,-50%);\n            transform: translate(-50%,-50%);\n}\n.login-form{\n    width: 35%;\n    padding: 30px;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%,-50%);\n            transform: translate(-50%,-50%);\n    background-color: #ffffff;\n    border-radius: 3px;\n}"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-wrapper zenwork-customized-bg\" [ngStyle]=\"{'height':getWindowHeight()}\">\n    <div class=\"login-logo\">\n        <img  src=\"../../assets/images/main-nav/logo.png\" alt=\"Zenwork Logo\">\n    </div>\n    <div class=\"login-form\">\n        <div class=\"col-sm-12\">\n            <div>\n                <h4 class=\"login-text\">Login</h4>\n            </div>\n            <div class=\"clearfix\"></div>\n        </div>\n        <div class=\"clearfix\"></div>\n        <div class=\"login-form-alignment\">\n            <form [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n                <div class=\"form-group\">\n                    <label>Email</label>\n                    <input type=\"email\" class=\"form-control customized-input\" formControlName=\"email\"  placeholder=\"Email\">\n                </div>\n                <div class=\"form-group\">\n                    <label>Password</label>\n                    <input type=\"password\" class=\"form-control customized-input\" formControlName=\"password\"  placeholder=\"Password\">\n                </div>\n                <div class=\"form-group\">\n                    <button class=\"btn btn-login-customized\" role=\"button\" type=\"submit\">Login</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/message-service.service */ "./src/app/services/message-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, zenworkerService, accessLocalStorageService, swalAlertService, messageService) {
        this.router = router;
        this.zenworkerService = zenworkerService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.messageService = messageService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'email': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
        // this.getAllRoles()
    };
    /* Getting Dynamic Height */
    LoginComponent.prototype.getWindowHeight = function () {
        this.dynamicWindowHeight = (window.innerHeight) + "px";
        return this.dynamicWindowHeight;
    };
    LoginComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.zenworkerService.getAllroles()
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                // this.loaderService.loader(false)
                _this.userProfile = res.data;
                _this.userProfile.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
        }, function (err) {
            console.log('Err', err);
            // this.loaderService.loader(false);
            // this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
        });
    };
    /* Login */
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.zenworkerService.login(this.loginForm.value)
            .subscribe(function (res) {
            if (res.status) {
                console.log(res);
                _this.zenworkerDetails = res.data;
                _this.accessLocalStorageService.set('x-access-token', _this.zenworkerDetails.token);
                console.log(_this.zenworkerDetails.type);
                if (_this.zenworkerDetails.type == "employee") {
                    console.log("sadasd");
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    if ((_this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'Manager') ||
                        (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && _this.zenworkerDetails.job.Site_AccessRole.name == 'Manager')) {
                        _this.accessLocalStorageService.set('siteAccessRoleId', _this.zenworkerDetails.job.Site_AccessRole);
                        _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                        _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                        _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name);
                            _this.accessLocalStorageService.set('roleType', 'customManager');
                        }
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.name);
                            _this.accessLocalStorageService.set('roleType', 'baseManager');
                        }
                        _this.router.navigate(['/admin/admin-dashboard/employee-management']);
                    }
                    if ((_this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'HR') ||
                        (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && _this.zenworkerDetails.job.Site_AccessRole.name == 'HR')) {
                        _this.accessLocalStorageService.set('siteAccessRoleId', _this.zenworkerDetails.job.Site_AccessRole);
                        _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                        _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                        _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name);
                            _this.accessLocalStorageService.set('roleType', 'customHR');
                        }
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.name);
                            _this.accessLocalStorageService.set('roleType', 'baseHR');
                        }
                        _this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                        console.log("data comes in HR");
                    }
                    if ((_this.zenworkerDetails.job.Site_AccessRole.roletype == 1 && _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name == 'Employee') ||
                        (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0 && _this.zenworkerDetails.job.Site_AccessRole.name == 'Employee')) {
                        _this.accessLocalStorageService.set('siteAccessRoleId', _this.zenworkerDetails.job.Site_AccessRole);
                        _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                        _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                        _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 1) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.baseRoleId.name);
                            _this.accessLocalStorageService.set('roleType', 'customEmployee');
                        }
                        if (_this.zenworkerDetails.job.Site_AccessRole.roletype == 0) {
                            _this.accessLocalStorageService.set('employeeType', _this.zenworkerDetails.job.Site_AccessRole.name);
                            _this.accessLocalStorageService.set('roleType', 'baseEmployee');
                        }
                        _this.router.navigate(['/admin/admin-dashboard/dashboard/employee-view']);
                        console.log("data comes in emplpoyee");
                    }
                }
                if (_this.zenworkerDetails.type === 'company' || _this.zenworkerDetails.type === 'reseller') {
                    console.log("company");
                    _this.accessLocalStorageService.set('employeeId', _this.zenworkerDetails._id);
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    console.log(_this.zenworkerDetails._id);
                    _this.router.navigate(['/admin/admin-dashboard/company-settings/']);
                    _this.accessLocalStorageService.set('companyId', _this.zenworkerDetails.companyId._id);
                    _this.accessLocalStorageService.set('addedBy', _this.zenworkerDetails.companyId.userId);
                    // localStorage.setItem('addedBy', this.zenworkerDetails.companyId.userId)
                }
                // for (var i = 0; i < this.userProfile.length; i++) {
                else if (_this.zenworkerDetails.type === 'sr-support' || _this.zenworkerDetails.type === 'zenworker') {
                    console.log("dsfsfsd");
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    _this.accessLocalStorageService.set('zenworkerId', _this.zenworkerDetails.zenworkerId);
                    _this.accessLocalStorageService.set('resellerId', _this.zenworkerDetails.zenworkerId.companyId);
                    _this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
                }
                if (_this.zenworkerDetails.type === 'administrator') {
                    _this.accessLocalStorageService.set('type', _this.zenworkerDetails.type);
                    _this.accessLocalStorageService.set('resellercmpnyID', _this.zenworkerDetails.companyId.userId);
                    _this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + _this.zenworkerDetails.companyId.userId]);
                    _this.messageService.sendMessage('reseller');
                }
                // }
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Login", res.message, 'info');
            }
        }, function (err) {
            console.log("Error", err.error.message);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Login", err.error.message, 'error');
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__["ZenworkersService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_6__["MessageService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/main-landing/main-landing.component.css":
/*!*********************************************************!*\
  !*** ./src/app/main-landing/main-landing.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-main-background{\n    background-color: #f8f8f8;\n    width: 100%;\n    height: 100vh;\n    padding: 15px;\n    display: table;\n}\n.zenwork-inner-div{\n\n    border-radius: 5px;\n    border-color: transparent;\n    background: url(\"/assets/images/newpages/Group 3278.png\") #fff no-repeat top right;\n    height: 100vh;\n    background-size: contain;\n    display: table-cell;\n    vertical-align: top;\n}\n.custom-img{\n    width: 70%;\n    margin-top: 10px\n}\n.custom-img img{\n    width: 100%;\n}\n.nav-items{\n    margin-top: 30px;\n    padding-right:25px;\n}\n.nav-items .list-items{\n    padding: 0px 10px;\n    font-size: 13px;\n}\n.nav-items .login-btn{\n    color: #439348;\n    text-decoration: none;\n    padding: 0 0 5px 0;\n    border-bottom: 2px solid #439348;\n}\n.navbar-brand{\n    padding: 15px 0px;\n}\n.btn-manual{\n    border-radius: 20px;\n    padding: 5px 30px;\n    background-color: #439348;\n    font-size: 13px;\n}\n.zenwork-margin-top20{\n    margin-top: 20px;\n}\n.zenwork-padding-left35{\n    padding-left: 35px;\n}\n.zenwork-margin-top60{\n    margin-top:60px;\n}\n.zenwork-body{\n    margin-top: 100px;\n}\n.body-content .content{\n    color: #626262;\n}\n.body-content small{\n    color: #c9c9c9;\n    font-size:12px;\n}\n.btn-register-demo{\n    border-radius: 20px;\n    padding: 5px 30px;\n    background-color: #439348;\n    color: #fff;\n    border-color: #439348;\n    font-size: 13px;\n}\n.chat-icon{\n    margin-top: 122px;\n    width: 9%;\n    float: right;\n}\n/* media query */\n/* above993 */\n@media (max-width:1900px){\n    .container {width: 100%;}\n\n}\n@media (max-width:1200px){\n    .container {width: 100%;}\n\n}\n/* above 768 */\n@media (max-width:992px){\n    .container {width: 100%}\n    /* .zenwork-inner-div {height:auto;} */\n    /* .zenwork-body h3{font-size: 20px;}\n    .body-content .content{font-size: 22px;}\n    .body-content small{font-size: 10px;} */\n    .chat-icon {margin-top: 0px;}\n\n}\n@media (max-width:768px){\n     .zenwork-body h3{font-size: 20px;}\n    .body-content .content{font-size: 22px;}\n    .body-content small{font-size: 10px;}\n}\n@media (max-width:480px){\n    .custom-img {width: 50%;margin: 0 auto;float: none!important;}\n    .custom-img img{margin: 0 auto; width: 50%;}\n    .zenwork-inner-div{background: none;}\n    .zenwork-main-background{position: relative;}\n    .navbar .zenwork-margin-top20{margin-top:0px!important;}\n    .nav-items{margin-top: 17px;padding-right: 112px;}\n    .nav-items .login-btn{font-size: 12px;}\n    .btn-manual{font-size: 12px;padding: 2px 18px}\n    .zenwork-body h3{font-size: 14px;}\n    .zenwork-body {position: relative;margin-top: 40px;}\n    .body-content .content {font-size: 15px;line-height: 20px;}\n    .body-content small{font-size: 11x;color: #8e8a88;}\n    .zenwork-margin-top60{margin-top: 30px;text-align:center;}\n    .btn-register-demo{padding:3px 15px;margin-right: 25px;}\n    .chat-icon{position: absolute;bottom: 0%;right: 10px;}\n}\n@media (max-width:320px){\n    .custom-img{margin: 0 auto;float: none!important;}\n    .custom-img img{width:60%;margin:15px auto;}\n    .nav-items{margin-top: 0px;padding-right: 54px;}\n    .zenwork-margin-top20 {padding-left: 35px;}\n    .zenwork-body {margin-top: 20px;}\n    .zenwork-body h3{margin-bottom: 5px!important;}\n    .zenwork-padding-left35{padding-left: 0px;}\n    .body-content .content{margin-top: 8px;}\n    .body-content small{color: #626262;}\n    .zenwork-margin-top60{margin: 10px 0px;}\n    .btn-register-demo{margin-right: 0;}\n    .chat-icon{bottom: 0%;width: 12%;}\n}"

/***/ }),

/***/ "./src/app/main-landing/main-landing.component.html":
/*!**********************************************************!*\
  !*** ./src/app/main-landing/main-landing.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-main-background\">\n  <div class=\"zenwork-inner-div\">\n    <div class=\"container\">\n      <nav class=\"navbar\">\n        <div class=\"zenwork-padding-left35 zenwork-margin-top20\">\n          <div class=\"navbar-header\">\n\n            <a class=\"navbar-brand custom-img\" [routerLink]=\"['/home']\">\n              <img alt=\"Brand\" src=\"./assets/images/newpages/logo.png\" class=\"img-responsive \">\n            </a>\n          </div>\n          <!-- <ul class=\"list-unstyled list-inline pull-right nav-items\">\n            <li class=\"list-items\">\n              <a class=\"login-btn\" [routerLink]=\"['/home']\">Login</a>\n            </li>\n            <li class=\"list-items \">\n              <a  class=\"btn btn-success btn-manual\" [routerLink]=\"['/home']\">Sign Up</a>\n            </li>\n           \n          </ul> -->\n          <div class=\"clearfix\"></div>\n\n        \n        </div>\n\n      </nav>\n      <div class=\"zenwork-body\">   \n            <div class=\"zenwork-padding-left35\">\n              <h3 class=\"green\"><b>What do we do?</b></h3>\n              <div class=\"body-content\">\n                <h2 class=\"content\">\n                  <b>Zenwork is a simplified HR system<br>\n                  for the sophisticated business.</b>\n                </h2>\n                <small>Let Zenwork crunch the numbers while you focuson the big picture.</small>\n              </div>\n              <div class=\"btn-register zenwork-margin-top60\">\n                <a [routerLink] = \"['/request-demo']\" class=\"btn btn-register-demo\">Register for Demo</a>\n              </div>\n            </div>\n\n        </div>\n\n    </div>\n    <div class=\"zenwork-chat\">\n      <div class=\"chat-icon \">\n        <img src=\"./assets/images/newpages/chat.png\" class=\"img-responsive\">\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/main-landing/main-landing.component.ts":
/*!********************************************************!*\
  !*** ./src/app/main-landing/main-landing.component.ts ***!
  \********************************************************/
/*! exports provided: MainLandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLandingComponent", function() { return MainLandingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainLandingComponent = /** @class */ (function () {
    function MainLandingComponent() {
    }
    MainLandingComponent.prototype.ngOnInit = function () {
    };
    MainLandingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-landing',
            template: __webpack_require__(/*! ./main-landing.component.html */ "./src/app/main-landing/main-landing.component.html"),
            styles: [__webpack_require__(/*! ./main-landing.component.css */ "./src/app/main-landing/main-landing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainLandingComponent);
    return MainLandingComponent;
}());



/***/ }),

/***/ "./src/app/main-landing/main-landing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/main-landing/main-landing.module.ts ***!
  \*****************************************************/
/*! exports provided: MainLandingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLandingModule", function() { return MainLandingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _request_demo_request_demo_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./request-demo/request-demo.component */ "./src/app/main-landing/request-demo/request-demo.component.ts");
/* harmony import */ var _main_landing_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main-landing.component */ "./src/app/main-landing/main-landing.component.ts");
/* harmony import */ var _main_landing_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main-landing.routing */ "./src/app/main-landing/main-landing.routing.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






/* Components */



var MainLandingModule = /** @class */ (function () {
    function MainLandingModule() {
    }
    MainLandingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _main_landing_component__WEBPACK_IMPORTED_MODULE_7__["MainLandingComponent"],
                _request_demo_request_demo_component__WEBPACK_IMPORTED_MODULE_6__["RequestDemoComponent"]
            ],
            providers: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["ModalModule"].forRoot(),
                _main_landing_routing__WEBPACK_IMPORTED_MODULE_8__["MainLandingRouting"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"]
            ]
        })
    ], MainLandingModule);
    return MainLandingModule;
}());



/***/ }),

/***/ "./src/app/main-landing/main-landing.routing.ts":
/*!******************************************************!*\
  !*** ./src/app/main-landing/main-landing.routing.ts ***!
  \******************************************************/
/*! exports provided: MainLandingRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLandingRouting", function() { return MainLandingRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _main_landing_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main-landing.component */ "./src/app/main-landing/main-landing.component.ts");
/* harmony import */ var _request_demo_request_demo_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./request-demo/request-demo.component */ "./src/app/main-landing/request-demo/request-demo.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    // { path: 'home', redirectTo: '', pathMatch: "full" },
    { path: 'main', component: _main_landing_component__WEBPACK_IMPORTED_MODULE_2__["MainLandingComponent"] },
    { path: 'request-demo', component: _request_demo_request_demo_component__WEBPACK_IMPORTED_MODULE_3__["RequestDemoComponent"] }
];
var MainLandingRouting = /** @class */ (function () {
    function MainLandingRouting() {
    }
    MainLandingRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], MainLandingRouting);
    return MainLandingRouting;
}());



/***/ }),

/***/ "./src/app/main-landing/request-demo/request-demo.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/main-landing/request-demo/request-demo.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-main-background{\n    background-color: #f8f8f8;\n    width: 100%;\n    height: 100%;\n    padding: 15px;\n}\n.zenwork-inner-div{\n    /* margin: 10px; */\n    /* background-color: #fff; */\n    border-radius: 5px;\n    border-color: transparent;\n    /* background: url(\"/assets/images/newpages/Group 3145.png\") #fff no-repeat top right; */\n    /* float:right; */\n    height: 100vh;\n    /* background-size: contain; */\n    background-color: #fff;\n    /* display: table-cell; */\n    vertical-align: top;\n}\n.custom-img{\n    width: 70%;\n    margin-top: 10px\n}\n.custom-img img{\n    width: 100%;\n}\n.nav-items{\n    margin-top: 30px;\n}\n.nav-items .list-items{\n    padding: 0px 10px;\n}\n.navbar-brand{\n    padding: 15px 0px;\n}\n.nav-items .login-btn{\n    color: #439348;\n    text-decoration: none;\n    padding: 0 0 5px 0;\n    border-bottom: 2px solid #439348;\n}\n.btn-manual{\n    border-radius: 20px;\n    padding: 5px 30px;\n    background-color: #439348;\n}\n.zenwork-padding-left35{\n    padding-left: 35px;\n}\n.zenwork-margin-top20{\n    margin-top: 20px;\n}\n.zenwork-request-demo-body-wrapper{\n    margin-top: 30px;\n}\n.zenwork-demo-input-image-group{\n    width: 10px;\n}\n.zenwork-input-group-addon{\n    background-color: #f8f8f8!important;\n    border-radius: 0px !important;\n    border:transparent !important;\n    border-right: 1px solid #cccccc75 !important;\n}\n.zenwork-input-error-alert{\n    position: absolute;\n    top: 0px;\n    left: 105%;\n    padding: 6px 14px;\n    background: #82c8a2;\n    color: #fff;\n    min-width: 215px;\n}\n.triangle-left {\n    position: absolute;\n    top: 11px;\n    left: -10px;\n\twidth: 0;\n    height: 0;\n    border-top: 5px solid transparent;\n    border-right: 10px solid #82c8a2;\n    border-bottom: 5px solid transparent;\n}\n.zenwork-btn-submit{\n    color: #fff;\n    border-radius: 25px;\n    padding: 5px 30px;\n    display: block;\n    background-color: #439348;\n}\n.zenwork-request-demo-form-wrapper{\n    padding-top: 20px;\n}\n.zenwork-font-size-twenty{\n    font-size: 20px !important;\n}\n.zenwork-small{\n    font-size: 82% !important;\n    color: #ccc !important;\n    line-height: 1.12857143 !important;\n}\n.zenwork-pt-twenty{\n    padding-top: 20px;\n}\n.chat-icon{\n    width: 20%;\n    position: absolute;\n    top: 80%;\n    left: 92%;\n}\n.zenwork-company-group-addon{\n    padding: 6px 9px !important;\n}\n.zenwork-company-group{\n    width: 15px !important;\n}\n/* media-query */\n@media (max-width:1200px){\n    .chat-icon{top: 83%;left: 85%;}\n}\n@media (max-width:992px){\n    .chat-icon{top: 89%;}\n}\n@media (max-width:768px){\n    .zenwork-pl-zero{width: 40%;margin-left: 5%}\n    .zenwork-responsive-img{margin-left: 5%;}\n    .zenwork-request-demo-wrapper{margin-top: 30px;}\n    .chat-icon{top: 100%;left: 82%;}\n}\n@media (max-width:425px){\n    .custom-img {width: 50%;}\n    .custom-img img{margin-left: 58px;}\n    .navbar .zenwork-margin-top20{margin-top:0px!important;}\n    .nav-items{margin-top: 17px;padding-right: 113px;}\n    .nav-items .login-btn{font-size: 12px;}\n    .btn-manual{font-size: 12px;padding: 2px 18px;}\n    .zenwork-pl-zero{width: 100%;margin-left: 3%;position: unset;}\n    .zenwork-request-demo-bg-image{display: none;}\n    .zenwork-responsive-img ,.input-group .form-control,.input-group{position: unset;}\n    .zenwork-main-background{position: relative!important;}\n    .chat-icon{left: 78%;top: 85%;}\n}\n@media (max-width:320px){\n    .custom-img{margin: 0 auto;float: none!important;}\n    .custom-img img{width:60%;margin:15px auto;}\n    .nav-items{margin-top: 0px;padding-right: 54px;}\n    .zenwork-padding-left35{padding-left: 0px;}\n    .zenwork-request-demo-body-wrapper{margin-top:0px;}\n    .navbar{margin-bottom:0px;}\n    .chat-icon{top:80%;}\n}"

/***/ }),

/***/ "./src/app/main-landing/request-demo/request-demo.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/main-landing/request-demo/request-demo.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-main-background\">\n  <div class=\"zenwork-inner-div\">\n    <div class=\"container\">\n      <nav class=\"navbar\">\n        <div class=\"zenwork-padding-left35 zenwork-margin-top20\">\n          <div class=\"navbar-header\">\n            <a class=\"navbar-brand custom-img\" routerLink=\"/home\">\n              <img alt=\"Brand\" src=\"./assets/images/newpages/logo.png\" class=\"img-responsive \">\n            </a>\n          </div>\n          <!-- <ul class=\"list-unstyled list-inline pull-right nav-items\">\n            <li class=\"list-items\">\n              <a class=\"login-btn\" routerLink=\"/home\">Login</a>\n            </li>\n            <li class=\"list-items \">\n              <a class=\"btn btn-success btn-manual\" routerLink=\"/home\">Sign Up</a>\n            </li>\n\n          </ul> -->\n          <div class=\"clearfix\"></div>\n\n\n        </div>\n\n      </nav>\n      <div class=\"zenwork-request-demo-body-wrapper\">\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col-sm-offset-1 col-sm-3 zenwork-pl-zero\">\n              <h3 class=\"green zenwork-font-size-twenty\">\n                <b>Register for Product Demo</b>\n              </h3>\n              <small class=\"zenwork-small\">\n                Our team is happy to answer your questions and take you on a product demo\n              </small>\n              <div class=\"zenwork-request-demo-form-wrapper\">\n                <form (ngSubmit)=\"clientDemoDetails()\">\n                  <div class=\"form-group\">\n                    <div class=\"input-group\">\n                      <span class=\"zenwork-input-group-addon input-group-addon\">\n                        <img src=\"../../../assets/images/newpages/Path 1273.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group\">\n                      </span>\n                      <input type=\"text\" class=\"form-control zenwork-input\" [(ngModel)]=\"clientRequestDetails.firstName\" placeholder=\"First Name\"\n                        name=\"firstName\" #firstName=\"ngModel\">\n                      <div *ngIf=\"error.firstName\" class=\"zenwork-input-error-alert\">\n                        <div class=\"triangle-left\"></div>\n                        {{error.firstName}}\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon zenwork-input-group-addon\">\n                        <img src=\"../../../assets/images/newpages/Path 1273.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group\">\n                      </span>\n                      <input type=\"text\" class=\"form-control zenwork-input\" [(ngModel)]=\"clientRequestDetails.lastName\" placeholder=\"Last Name\"\n                        name=\"lastName\" #lastName=\"ngModel\">\n                      <div *ngIf=\"error.lastName\" class=\"zenwork-input-error-alert\">\n                        <div class=\"triangle-left\"></div>\n                        {{error.lastName}}\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon zenwork-input-group-addon\">\n                        <img src=\"../../../assets/images/newpages/ic_domain_24px.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group\">\n                      </span>\n                      <input type=\"text\" class=\"form-control zenwork-input\" [(ngModel)]=\"clientRequestDetails.companyName\" placeholder=\"Company Name\"\n                        name=\"companyName\" #companyName=\"ngModel\">\n                      <div *ngIf=\"error.companyName\" class=\"zenwork-input-error-alert\">\n                        <div class=\"triangle-left\"></div>\n                        {{error.companyName}}\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon zenwork-company-group-addon zenwork-input-group-addon\">\n                        <img src=\"../../../assets/images/newpages/Group 2925.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group zenwork-company-group\">\n                      </span>\n                      <mat-select class=\"form-control zenwork-input\" placeholder=\"Company Size\" [(ngModel)]=\"clientRequestDetails.companySize\" name=\"size\" #size=\"ngModel\">\n                        <mat-option *ngFor=\"let size of companySizes\" [value]=\"size\">\n                          {{ size }}\n                        </mat-option>\n                      </mat-select>\n                     <!--  <select class=\"form-control zenwork-input\" [(ngModel)]=\"clientRequestDetails.companySize\" name=\"size\" #size=\"ngModel\">\n                        <option value=\"\">Company Size</option>\n                        <option>1-10</option>\n                        <option>1-30</option>\n                        <option>30-50</option>\n                        <option>50-100</option>\n                        <option>100-150</option>\n                        <option>150-200</option>\n                        <option>200-250</option>\n                        <option>250+</option>\n                      </select> -->\n                      <div *ngIf=\"error.companySize\" class=\"zenwork-input-error-alert\">\n                        <div class=\"triangle-left\"></div>\n                        {{error.companySize}}\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon zenwork-input-group-addon\">\n                        <img src=\"../../../assets/images/newpages/black-envelope.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group\">\n                      </span>\n                      <input type=\"text\" class=\"form-control zenwork-input\" [(ngModel)]=\"clientRequestDetails.email\" placeholder=\"Email\" #email=\"ngModel\"\n                        pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" name=\"email\">\n                      <div *ngIf=\"error.email\" class=\"zenwork-input-error-alert\">\n                        <div class=\"triangle-left\"></div>\n                        {{error.email}}\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon zenwork-input-group-addon\">\n                        <img src=\"../../../assets/images/newpages/phone-receiver.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group\">\n                      </span>\n                      <input type=\"text\" class=\"form-control zenwork-input\" [(ngModel)]=\"clientRequestDetails.phone\" placeholder=\"Phone\" #phone=\"ngModel\"\n                        name=\"phone\">\n                      <div *ngIf=\"error.phone\" class=\"zenwork-input-error-alert\">\n                        <div class=\"triangle-left\"></div>\n                        {{error.phone}}\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group zenwork-pt-twenty\">\n                    <button type=\"submit\" [disabled]=\"!clientRequestDetails.firstName || !clientRequestDetails.lastName || !clientRequestDetails.companyName || !clientRequestDetails.companySize || !clientRequestDetails.email || !clientRequestDetails.phone\"\n                      class=\"btn zenwork-btn-submit btn-block\">\n                      Register for Demo\n                    </button>\n                  </div>\n                </form>\n                <div class=\"clearfix\"></div>\n              </div>\n            </div>\n            <div class=\"col-sm-offset-2 col-sm-6 zenwork-responsive-img\">\n              <div class=\"zenwork-request-demo-wrapper\">\n                <img class=\"zenwork-request-demo-bg-image img-responsive\" src=\"../../../assets/images/newpages/Group 3145.png\">\n                <div class=\"zenwork-chat\">\n                  <div class=\"chat-icon \">\n                    <img src=\"./assets/images/newpages/chat.png\" class=\"img-responsive\">\n                  </div>\n                  <div class=\"clearfix\"></div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/main-landing/request-demo/request-demo.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/main-landing/request-demo/request-demo.component.ts ***!
  \*********************************************************************/
/*! exports provided: RequestDemoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestDemoComponent", function() { return RequestDemoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_client_demo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/client-demo.service */ "./src/app/services/client-demo.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RequestDemoComponent = /** @class */ (function () {
    function RequestDemoComponent(dialog, clientDemoRegistrationService, route) {
        this.dialog = dialog;
        this.clientDemoRegistrationService = clientDemoRegistrationService;
        this.route = route;
        this.companySizes = ["1-10", "10-30", "30-50", "50-100", "100-150", "150-200", "200-250", "250+"];
        this.clientRequestDetails = {
            firstName: '',
            lastName: '',
            companyName: '',
            companySize: '',
            email: '',
            phone: ''
        };
        this.error = {
            firstName: '',
            lastName: '',
            companyName: '',
            companySize: '',
            email: '',
            phone: ''
        };
    }
    RequestDemoComponent.prototype.ngOnInit = function () {
    };
    RequestDemoComponent.prototype.clientDemoDetails = function () {
        var _this = this;
        if (!this.clientRequestDetails.firstName) {
            this.error.firstName = "Enter First Name";
        }
        else if (!this.clientRequestDetails.lastName) {
            this.error.firstName = "";
            this.error.lastName = "Enter Last Name";
        }
        else if (!this.clientRequestDetails.companyName) {
            this.error.firstName = "";
            this.error.lastName = "";
            this.error.companyName = "Enter Company Name";
        }
        else if (!this.clientRequestDetails.companySize) {
            this.error.firstName = "";
            this.error.lastName = "";
            this.error.companyName = "";
            this.error.companySize = "Select Number of Employees";
        }
        else if (!this.clientRequestDetails.email || !(/^\S+@\S+\.\S+/.test(this.clientRequestDetails.email))) {
            this.error.firstName = "";
            this.error.lastName = "";
            this.error.companyName = "";
            this.error.companySize = "";
            this.error.email = "Enter a Valid email address";
        }
        else if (!this.clientRequestDetails.phone || !(/[0-9]{10}/.test(this.clientRequestDetails.phone))) {
            this.error.firstName = "";
            this.error.lastName = "";
            this.error.companyName = "";
            this.error.companySize = "";
            this.error.email = "";
            this.error.phone = "Enter valid phone number";
        }
        else {
            this.error = {
                firstName: '',
                lastName: '',
                companyName: '',
                companySize: '',
                email: '',
                phone: ''
            };
            this.clientDemoRegistrationService.registerClient(this.clientRequestDetails)
                .subscribe(function (res) {
                console.log(res);
                if (res.status) {
                    console.log("Success");
                    _this.clientRequestDetails = {
                        firstName: '',
                        lastName: '',
                        companyName: '',
                        companySize: '',
                        email: '',
                        phone: ''
                    };
                    _this.clientDetailsRegistrationModal();
                    _this.route.navigate(['/home']);
                }
                else {
                    _this.error = res;
                    _this.error = _this.error.err;
                }
            }, function (err) {
                console.log(err);
                _this.error = err;
            });
        }
    };
    RequestDemoComponent.prototype.clientDetailsRegistrationModal = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            title: 'Registered Successfully',
            text: 'We will get back to you soon',
            type: 'success',
            timer: 2500,
            showConfirmButton: false
        });
    };
    RequestDemoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-request-demo',
            template: __webpack_require__(/*! ./request-demo.component.html */ "./src/app/main-landing/request-demo/request-demo.component.html"),
            styles: [__webpack_require__(/*! ./request-demo.component.css */ "./src/app/main-landing/request-demo/request-demo.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_client_demo_service__WEBPACK_IMPORTED_MODULE_2__["ClientDemoRegistrationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], RequestDemoComponent);
    return RequestDemoComponent;
}());



/***/ }),

/***/ "./src/app/material-module/material-module.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/material-module/material-module.module.ts ***!
  \***********************************************************/
/*! exports provided: MaterialModuleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModuleModule", function() { return MaterialModuleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaterialModuleModule = /** @class */ (function () {
    function MaterialModuleModule() {
    }
    MaterialModuleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
            ],
            declarations: [],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
            ],
            providers: [],
        })
    ], MaterialModuleModule);
    return MaterialModuleModule;
}());



/***/ }),

/***/ "./src/app/services/accessLocalStorage.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/accessLocalStorage.service.ts ***!
  \********************************************************/
/*! exports provided: AccessLocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccessLocalStorageService", function() { return AccessLocalStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AccessLocalStorageService = /** @class */ (function () {
    function AccessLocalStorageService() {
    }
    AccessLocalStorageService.prototype.set = function (key, data) {
        try {
            localStorage.setItem(key, JSON.stringify(data));
        }
        catch (e) {
            console.error('Error saving to localStorage', e);
        }
    };
    AccessLocalStorageService.prototype.get = function (key) {
        try {
            return JSON.parse(localStorage.getItem(key));
        }
        catch (e) {
            console.error('Error getting data from localStorage', e);
            return null;
        }
    };
    AccessLocalStorageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AccessLocalStorageService);
    return AccessLocalStorageService;
}());



/***/ }),

/***/ "./src/app/services/authentication.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
    }
    AuthenticationService.prototype.loginDetails = function (details) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/login", details)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    AuthenticationService.prototype.register = function (details, data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "", details, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    AuthenticationService.prototype.signup = function (details) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/signup", details)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    AuthenticationService.prototype.updateCompanyDetail = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/updateCompanyDetails", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    AuthenticationService.prototype.gooleSignIn = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/login/google")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    AuthenticationService.prototype.contactUsformSend = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/contact-us/contactus", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    AuthenticationService.prototype.resetPassword = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/passwordresetlink", data);
    };
    AuthenticationService.prototype.forgotPassword = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/resetpasswordwithtoken", data);
    };
    AuthenticationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/services/client-demo.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/client-demo.service.ts ***!
  \*************************************************/
/*! exports provided: ClientDemoRegistrationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientDemoRegistrationService", function() { return ClientDemoRegistrationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ClientDemoRegistrationService = /** @class */ (function () {
    function ClientDemoRegistrationService(http) {
        this.http = http;
    }
    ClientDemoRegistrationService.prototype.registerClient = function (clientDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/client/registerForDemo", clientDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ClientDemoRegistrationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ClientDemoRegistrationService);
    return ClientDemoRegistrationService;
}());



/***/ }),

/***/ "./src/app/services/company.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/company.service.ts ***!
  \*********************************************/
/*! exports provided: CompanyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyService", function() { return CompanyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CompanyService = /** @class */ (function () {
    function CompanyService(http) {
        this.http = http;
    }
    CompanyService.prototype.createCompany = function (companyDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/addClientDetails", companyDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.editClient = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/getCompanyDetails/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.getBillingHistory = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/orders/BillingOfCompany/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.editDataSave = function (data, id) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/" + id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.currentPlan = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/orders/CurrentPlan/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.getAllCompanies = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/all", { params: data })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.getresellers = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/resellers")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.editCompany = function (companyDetails) {
        var id = companyDetails._id;
        var company = {
            name: companyDetails.name,
            tradeName: companyDetails.tradeName,
            employeCount: companyDetails.employeCount,
            email: companyDetails.email,
            type: companyDetails.type,
            belongsToReseller: companyDetails.belongsToReseller,
            reseller: companyDetails.reseller,
            primaryContact: { name: companyDetails.primaryContact.name, phone: companyDetails.primaryContact.phone, extention: companyDetails.primaryContact.extention },
            billingContact: { name: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.name, email: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.email, phone: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.phone, extention: companyDetails.isPrimaryContactIsBillingContact ? '' : companyDetails.billingContact.extention },
            address: {
                address1: companyDetails.address.address1,
                address2: companyDetails.address.address2,
                city: companyDetails.address.city,
                state: companyDetails.address.state,
                zipcode: companyDetails.address.zipcode,
                country: companyDetails.address.country
            },
            billingAddress: {
                address1: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.address1,
                address2: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.address2,
                city: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.city,
                state: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.state,
                zipcode: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.zipcode,
                country: companyDetails.isPrimaryAddressIsBillingAddress ? '' : companyDetails.billingAddress.country
            },
            isPrimaryContactIsBillingContact: companyDetails.isPrimaryContactIsBillingContact,
            isPrimaryAddressIsBillingAddress: companyDetails.isPrimaryAddressIsBillingAddress
        };
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/" + id, company)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService.prototype.addContactDetails = function (companyAddress) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/addContactDetails", companyAddress)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CompanyService);
    return CompanyService;
}());



/***/ }),

/***/ "./src/app/services/companySettings.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/companySettings.service.ts ***!
  \*****************************************************/
/*! exports provided: CompanySettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanySettingsService", function() { return CompanySettingsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CompanySettingsService = /** @class */ (function () {
    function CompanySettingsService(http) {
        this.http = http;
    }
    CompanySettingsService.prototype.saveGeneralSettings = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companySetup/generalSettings", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.sendVerificationEmail = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/sendVerificationEmail", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.updateEmailUser = function (token) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/verifyEmail", token)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getGeneralSettings = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/getGeneralSettings")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.updateGeneralSettings = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/updateGeneralSettings", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.empSettings = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/empIdentitySettings", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getShowFeilds = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/getStandardSettings")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.postShowFeilds = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/updateStandardSettings", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.addCustomFields = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/addUpdateCustomField", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getAllCustomFields = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/getAllCustomFields")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getSingleCustomFieldData = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/getCustomField/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.deleteCustomFieldData = function (deleteFields) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/deleteCustomField", deleteFields)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.saveCompanyContactInformation = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companySetup/companyContactInfo", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getCompanyContactInfo = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companySetup/getCompanyContactInfo")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.controllSections = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/settingsOfMyInfoSections")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    // Author: saiprakash, Date: 03/04/19
    // Company Structure APIs
    CompanySettingsService.prototype.getFields = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getFields")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getFieldsForRoles = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getFieldsForRoles")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getStuctureFields = function (data, id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getSubDocs/" + data + ("?withouttoken=" + id))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    // getStuctureFieldsWithoutToken(data,id) {
    //     return this.http.get(environment.url + "/api" + environment.version + "/structure/getSubDocs/" + data+`?withouttoken=${id}`)
    //         .pipe(map(res => res));
    // }
    CompanySettingsService.prototype.addUpdateStructureData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/addUpdate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.structureUpload = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/uploadxlsx", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.deleteStructure = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/multi-del", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    // end company structure apis
    CompanySettingsService.prototype.addTraining = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/add", data);
    };
    CompanySettingsService.prototype.viewTrainingData = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/getAll", { params: data });
    };
    CompanySettingsService.prototype.TrainingDelete = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/deleteMany", id);
    };
    CompanySettingsService.prototype.EditTrainingData = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/getSingleData/" + id);
    };
    CompanySettingsService.prototype.updateTrainingData = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/update/", data);
    };
    CompanySettingsService.prototype.assignTrainingToEmployees = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/assignTraining", data);
    };
    //employee template
    CompanySettingsService.prototype.addEmailTemplates = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/uploadEmailTemplate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getAllEmialTempates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/getEmailTemplates")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.deleteEmailTemplates = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/deleteEmailTemplates", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.editEmailTemplate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/editEmailTemplate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.singleEmailTemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/singleEmailTemplate/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    //   company policies
    CompanySettingsService.prototype.uploadPolicy = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/uploadPolicy", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getUploadPolicies = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/getUploadPolicies")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.deletePolicies = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/deletePolicies", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.editUploadPolicy = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/editUploadPolicy", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getsinglePolicy = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/singlePolicy/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getAllTraniningData = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getTrainingDetails")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.getEmpIdentitySettings = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.prviewEmployeeList = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/listEmployees", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.archiveTraining = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/changeStatus", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService.prototype.unArchiveTraining = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/training/unarchive")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    CompanySettingsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CompanySettingsService);
    return CompanySettingsService;
}());



/***/ }),

/***/ "./src/app/services/employee.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/employee.service.ts ***!
  \**********************************************/
/*! exports provided: EmployeeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeService", function() { return EmployeeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmployeeService = /** @class */ (function () {
    function EmployeeService(http) {
        this.http = http;
    }
    EmployeeService.prototype.addNewEmployee = function (employeeDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employees/add", employeeDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    EmployeeService.prototype.getOnboardedEmployeeDetails = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/listEmployees", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    EmployeeService.prototype.addNotes = function (notes) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employees/addNote", notes)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    EmployeeService.prototype.getAllNotes = function (_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employees/allNotes/" + _id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    EmployeeService.prototype.updateEmployeePersonalDetails = function (id, data, section) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employees/" + id + '/' + section, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    EmployeeService.prototype.updateEmergencyContact = function (id, data, section) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employees/" + id + '/' + section, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    EmployeeService.prototype.copyService = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/create-copy", data);
    };
    EmployeeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EmployeeService);
    return EmployeeService;
}());



/***/ }),

/***/ "./src/app/services/invoice.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/invoice.service.ts ***!
  \*********************************************/
/*! exports provided: InvoiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceService", function() { return InvoiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InvoiceService = /** @class */ (function () {
    function InvoiceService(http) {
        this.http = http;
    }
    InvoiceService.prototype.getInvoiceBillingDetails = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + '/orders/all', data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    InvoiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], InvoiceService);
    return InvoiceService;
}());



/***/ }),

/***/ "./src/app/services/loader.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoaderService = /** @class */ (function () {
    function LoaderService(spinner) {
        this.spinner = spinner;
        this.showLoading = false;
    }
    LoaderService.prototype.loader = function (show) {
        if (show) {
            this.showLoading = show;
            // this.spinner.show();
        }
        else {
            this.showLoading = show;
            // this.spinner.hide();
        }
    };
    LoaderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [ngx_spinner__WEBPACK_IMPORTED_MODULE_1__["NgxSpinnerService"]])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "./src/app/services/message-service.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/message-service.service.ts ***!
  \*****************************************************/
/*! exports provided: MessageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageService", function() { return MessageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessageService = /** @class */ (function () {
    function MessageService() {
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    MessageService.prototype.sendMessage = function (message) {
        console.log("messageservice", message);
        this.subject.next({ text: message });
    };
    MessageService.prototype.clearMessage = function () {
        this.subject.next();
    };
    MessageService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    MessageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], MessageService);
    return MessageService;
}());



/***/ }),

/***/ "./src/app/services/packageAndAddon.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/packageAndAddon.service.ts ***!
  \*****************************************************/
/*! exports provided: PackageAndAddOnService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackageAndAddOnService", function() { return PackageAndAddOnService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PackageAndAddOnService = /** @class */ (function () {
    function PackageAndAddOnService(http) {
        this.http = http;
    }
    PackageAndAddOnService.prototype.createPackage = function (packageDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/packages/add", packageDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.editPackage = function (packageDetails) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/packages/" + packageDetails._id, packageDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.createAddOn = function (addOnDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/ad-ons/add", addOnDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getTaxRate = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenwork/getTaxRate")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.editAddOn = function (addOnDetails) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/ad-ons/" + addOnDetails._id, addOnDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getAllPackagesforSettings = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/packages/list", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getAllPackages = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/packages/all")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getSpecificPackageDetails = function (packageData) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/packages/" + packageData._id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getAllAddOns = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/ad-ons/all")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getAllAddOnslist = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/ad-ons/all", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getSpecificAddOnDetails = function (AddOnData) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/ad-ons/" + AddOnData._id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.createCoupon = function (couponDetail) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + '/api' + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/add", couponDetail)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getAllCoupons = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/all")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getAllCouponsforAll = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/all", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.getSepcificCouponDetails = function (couponDetail) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + '/api' + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/" + couponDetail._id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.editCouponsData = function (couponDetail) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/" + couponDetail._id, couponDetail);
    };
    PackageAndAddOnService.prototype.updateTaxRate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenwork/updateTaxRate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.validateCoupon = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/validate-coupon", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.deleteAddons = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/ad-ons/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.deleteCoupons = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/coupons/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService.prototype.deletePackages = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/packages/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PackageAndAddOnService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PackageAndAddOnService);
    return PackageAndAddOnService;
}());



/***/ }),

/***/ "./src/app/services/purchasePackage.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/purchasePackage.service.ts ***!
  \*****************************************************/
/*! exports provided: PurchasePackageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasePackageService", function() { return PurchasePackageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PurchasePackageService = /** @class */ (function () {
    function PurchasePackageService(http) {
        this.http = http;
    }
    PurchasePackageService.prototype.purchasePackage = function (packageDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/orders/createOrder", packageDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PurchasePackageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PurchasePackageService);
    return PurchasePackageService;
}());



/***/ }),

/***/ "./src/app/services/reseller.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/reseller.service.ts ***!
  \**********************************************/
/*! exports provided: ResellerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResellerService", function() { return ResellerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResellerService = /** @class */ (function () {
    function ResellerService(http) {
        this.http = http;
    }
    // purchasePackage(packageDetails) {
    //   return this.http.post(environment.url + "/api" + environment.version + "/orders/createOrder", packageDetails)
    //     .pipe(map(res => res));
    // }
    ResellerService.prototype.getAllCompanies = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/resellerDetails", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ResellerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ResellerService);
    return ResellerService;
}());



/***/ }),

/***/ "./src/app/services/swalAlert.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/swalAlert.service.ts ***!
  \***********************************************/
/*! exports provided: SwalAlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertService", function() { return SwalAlertService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SwalAlertService = /** @class */ (function () {
    function SwalAlertService(http) {
        this.http = http;
    }
    SwalAlertService.prototype.SweetAlertWithoutConfirmation = function (title, text, type) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()({
            title: title,
            text: text,
            type: type,
            showCancelButton: false,
            showConfirmButton: false,
            timer: 2500
        });
    };
    SwalAlertService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SwalAlertService);
    return SwalAlertService;
}());



/***/ }),

/***/ "./src/app/services/tokenInterceptor.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/tokenInterceptor.service.ts ***!
  \******************************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(accessLocalStorageService) {
        this.accessLocalStorageService = accessLocalStorageService;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        var token = this.accessLocalStorageService.get('x-access-token');
        if (token != null) {
            request = request.clone({
                setHeaders: {
                    "x-access-token": token,
                }
            });
        }
        else {
            request = request.clone({
                setHeaders: {
                // "Content-Type": "application/json"
                }
            });
        }
        return next.handle(request);
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__["AccessLocalStorageService"]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/services/zenworkers.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/zenworkers.service.ts ***!
  \************************************************/
/*! exports provided: ZenworkersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZenworkersService", function() { return ZenworkersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ZenworkersService = /** @class */ (function () {
    function ZenworkersService(http) {
        this.http = http;
    }
    ZenworkersService.prototype.login = function (zenworkerDetails) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/login", zenworkerDetails)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.addProfile = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/addUpdate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.editProfile = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/role/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.getAllroles = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/acls")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.getZenworkersDetails = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenworkers/all", { params: data })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.addZenworker = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenworkers/add", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.editZenworker = function (data) {
        var id = data._id;
        var zenworker = {
            name: data.name,
            email: data.email,
            roleId: data.roleId,
            address: { state: data.address.state },
            status: data.status
        };
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenworkers/" + id, zenworker)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.getSingleZenworker = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenworkers/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService.prototype.deleteZenworkers = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/zenworkers/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ZenworkersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ZenworkersService);
    return ZenworkersService;
}());



/***/ }),

/***/ "./src/app/shared-module/confirmation/confirmation.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/shared-module/confirmation/confirmation.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".confirm-head {\n    font-size: 20px;\n    color: rgb(110, 110, 110);\n    text-align: center;\n    margin-bottom: 50px;\n    /* text-transform: capitalize; */\n  }\n  .note{\n    text-align: center;\n      /* padding: 0 0px 0 33px; */\n  }\n  .confirm-txt{\n    font-size: 20px;\n    color: rgb(110, 110, 110);\n    text-align: center;\n    margin-bottom: 50px;\n  }\n  .pad-40-5 {\n    padding: 40px 5px;\n  }\n  .cancell-btn {\n    color: #fff;\n    padding: 10px 30px;\n    background-color: #797d7b!important;\n    margin-right: 20px;\n  }\n  .confirm-btn {\n    color: #fff;\n    padding: 10px 30px;\n    background-color: #008f3d;\n  }\n  "

/***/ }),

/***/ "./src/app/shared-module/confirmation/confirmation.component.html":
/*!************************************************************************!*\
  !*** ./src/app/shared-module/confirmation/confirmation.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pad-40-5\">\n  <div class=\"confirm-head\">\n    <p>{{successTxt.txt}}</p>\n    <p class=\"note\" *ngIf = \"successTxt.note\">{{successTxt.note}}</p>\n    <p *ngIf = \"successTxt1\">{{successTxt1}}</p>\n  </div>\n  <div class=\"text-center bor-top pad-10\">\n    <button class=\"btn cancell-btn\" (click)=\"onNoClick()\">{{buttonText1}}</button>\n\n    <button class=\"btn confirm-btn\" (click)=\"confirm()\">{{buttonText2}}</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/shared-module/confirmation/confirmation.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared-module/confirmation/confirmation.component.ts ***!
  \**********************************************************************/
/*! exports provided: ConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationComponent", function() { return ConfirmationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ConfirmationComponent = /** @class */ (function () {
    function ConfirmationComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.buttonText1 = 'Cancel';
        this.buttonText2 = 'Proceed';
    }
    ConfirmationComponent.prototype.ngOnInit = function () {
        this.successTxt = this.data;
        if (this.data.text == 'delete') {
            this.successTxt1 = 'Are you sure you want to delete?';
            this.buttonText1 = "No";
            this.buttonText2 = "Yes";
        }
        if (this.data.text == 'cancel') {
            this.successTxt1 = 'Are you sure you want to cancel?';
            this.buttonText1 = "No";
            this.buttonText2 = "Yes";
        }
        console.log(this.successTxt);
    };
    ConfirmationComponent.prototype.confirm = function () {
        this.dialogRef.close(true);
    };
    ConfirmationComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    ConfirmationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-confirmation',
            template: __webpack_require__(/*! ./confirmation.component.html */ "./src/app/shared-module/confirmation/confirmation.component.html"),
            styles: [__webpack_require__(/*! ./confirmation.component.css */ "./src/app/shared-module/confirmation/confirmation.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ConfirmationComponent);
    return ConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/navbar/navbar.component.css":
/*!***********************************************************!*\
  !*** ./src/app/shared-module/navbar/navbar.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar-right{\n    padding-top:0 !important; margin: 0 20px 0 0;\n\n}\n.search-border{\n    border : 1px solid #9e9e9e;\n    border-radius: 25px;\n    position: relative;\n    bottom:5px; width: 300px; margin:10px 0 0;\n}\n.search-border .form-control { padding:10px 45px 10px 20px; height: auto; color:#fff;}\n.search-border .form-control:focus { outline: none; box-shadow: none;}\n.search-border .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#fff;\n  }\n.search-border .form-control::-moz-placeholder { /* Firefox 19+ */\n    color:#fff;\n  }\n.search-border .form-control:-ms-input-placeholder { /* IE 10+ */\n    color:#fff;\n  }\n.search-border .form-control:-moz-placeholder { /* Firefox 18- */\n    color:#fff;\n  }\n.search-border span { width: 30px;}\n.search-icon{\n    right: 13px;\n    position: absolute;\n    top: 11px;\n}\n.border-none{\n    border: none;\n    background: transparent;\n}\n.dropdown{\n    padding-top: 0 !important;\n}\n.dropdown a{\n    color: #fff !important; cursor: pointer;\n}\n.caret{\n    display: inline !important;\n    margin-left: 25px !important;\n    padding-top: 6px !important;\n}\n.main-nav-image{\n    margin: 16px 0 0;\n}\n.main-nav-image-notification{\n    \n    margin-right: 10px !important;\n}\n.zenwork-container {\n    width: 500px;\n    height: 300px;\n    border: 1px solid rgba(0, 0, 0, 0.5);\n  }\n.custom-navbar .navbar-nav>li { margin: 0 15px;}\n.custom-navbar .navbar-nav>li>a { padding: 0; cursor: pointer;}\n.custom-navbar .navbar-brand{\n    padding:0 !important;\n}\n.logo { width: auto; padding: 12px 0 0 30px;}\n.custom-navbar { padding: 10px 0;}\n.toggle-menu { cursor: pointer;padding:19px 0 0 80px;}\n.custom-navbar .navbar-nav>li.logout { margin: 0 0 0 30px; padding:2px 0 0;}\n.custom-navbar .nav .open>a, .custom-navbar .nav .open>a:focus, .custom-navbar .nav .open>a:hover { background:transparent;}\n.custom-navbar .dropdown-menu { min-width: 300px;left: auto; right: -20px; border: none; padding: 0; background:transparent; box-shadow:0 6px 12px #e0e0e0;}\n.top-block { background:#5f5f5f; padding:15px 25px; border-radius: 5px 5px 0 0;}\n.left-block { display: inline-block; vertical-align:middle; width: 195px;}\n.left-block h2 { font-size: 20px; line-height: 20px; color:#fff; font-weight: normal; padding: 0 0 7px; margin: 0;}\n.left-block small { font-size: 14px; line-height: 14px; color:#efeded; font-weight: normal; display: block;}\n.top-block span { display: inline-block; vertical-align: middle; cursor: pointer;}\n.settings { background:#fff; padding: 8px 0; border-radius: 0 0 5px 5px;}\n.settings ul { display: block;}\n.settings ul li { display: block;}\n.settings ul li a { display: block; font-size: 15px; line-height: 15px; color:#8c8c8c !important; padding:12px 0 12px 25px;}\n.settings ul li a:hover { background:#008f3d; color:#fff !important;}\n.reseller-logo{\n    margin: 10px 0 10px 50px !important;\n    color: #fff; \n}"

/***/ }),

/***/ "./src/app/shared-module/navbar/navbar.component.html":
/*!************************************************************!*\
  !*** ./src/app/shared-module/navbar/navbar.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-inverse custom-navbar\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\"\n        aria-expanded=\"false\" aria-controls=\"navbar\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n      <a *ngIf=\"message != 'reseller'\" class=\"navbar-brand\" href=\"#\">\n        <img src=\"../../assets/images/main-nav/whitelogo.png\" alt=\"Zenwork Hr\" class=\"logo\">\n      </a>\n      <a *ngIf=\"message == 'reseller'\" class=\"navbar-brand\" href=\"#\">\n        <h3 class=\"reseller-logo\"> {{resellerName}}</h3>\n      </a>\n      <a class=\"inline-block toggle-menu\">\n        <img src=\"../../assets/images/main-nav/expand.png\" alt=\"Menu Expand\" class=\"img-responsive\">\n      </a>\n    </div>\n    <div id=\"navbar\" class=\"collapse navbar-collapse\">\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li>\n          <a>\n            <div class=\"search-border\">\n              <input type=\"text\" class=\"border-none form-control\">\n              <span>\n                <img src=\"../../assets/images/main-nav/ic_search_24px.png\" alt=\"search-icon\"\n                  class=\"img-responsive search-icon\">\n              </span>\n            </div>\n\n          </a>\n        </li>\n        <li>\n          <a href=\"#\">\n            <img src=\"../../assets/images/main-nav/ic_home_24px.png\" alt=\"home-img\"\n              class=\"img-responsive main-nav-image\">\n          </a>\n        </li>\n        <li>\n          <a href=\"#\">\n            <img src=\"../../assets/images/main-nav/ic_notifications_24px.png\" alt=\"notification-img\"\n              class=\"img-responsive main-nav-image\">\n          </a>\n        </li>\n        <li>\n          <div class=\"dropdown\">\n            <a class=\"dropdown-toggle\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n              aria-expanded=\"true\">\n              <img src=\"../../assets/images/main-nav/ic_settings_24px.png\" alt=\"settings-icon\"\n                class=\"img-responsive main-nav-image\">\n              <span class=\"caret\"></span>\n            </a>\n            <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\">\n\n              <div class=\"top-block\">\n                <div class=\"left-block\">\n                  <h2>Nick Bondurant</h2>\n                  <small>Director</small>\n                </div>\n                <span><img src=\"../../../assets/images/main-nav/Employees.png\" width=\"53\" height=\"53\" alt=\"img\"></span>\n              </div>\n\n              <div class=\"settings\">\n\n                <ul>\n                  <li><a href=\"#\">Register</a></li>\n                  <li><a href=\"#\">Password Recovery</a></li>\n                  <li><a href=\"#\">My Account Settings</a></li>\n                  <li><a href=\"#\">My Info</a></li>\n                  <li><a href=\"#\">My API Keys</a></li>\n                  <li><a href=\"#\">Sign Out</a></li>\n                </ul>\n\n              </div>\n\n            </div>\n          </div>\n        </li>\n        <li class=\"logout\">\n          <a href=\"javascript:void(0)\" (click)=\"logOut()\">\n            <img src=\"../../assets/images/main-nav/ic_power_settings_new_24px.png \" alt=\"power-img\"\n              class=\"img-responsive main-nav-image\">\n          </a>\n        </li>\n\n      </ul>\n    </div>\n    <!--/.nav-collapse -->\n  </div>\n</nav>"

/***/ }),

/***/ "./src/app/shared-module/navbar/navbar.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared-module/navbar/navbar.component.ts ***!
  \**********************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _services_reseller_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/reseller.service */ "./src/app/services/reseller.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(messageService, router, route, resellerService) {
        var _this = this;
        this.messageService = messageService;
        this.router = router;
        this.route = route;
        this.resellerService = resellerService;
        this.showFiller = false;
        this.subscription = this.messageService.getMessage().subscribe(function (message) {
            _this.message = message.text;
            console.log("message", _this.message);
            _this.resellerName = localStorage.getItem('resellerName');
        });
    }
    NavbarComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    NavbarComponent.prototype.logOut = function () {
        localStorage.clear();
        this.router.navigate(['/zenwork-login']);
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/shared-module/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/shared-module/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [_services_message_service_service__WEBPACK_IMPORTED_MODULE_2__["MessageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_reseller_service__WEBPACK_IMPORTED_MODULE_3__["ResellerService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/shared.module.ts":
/*!************************************************!*\
  !*** ./src/app/shared-module/shared.module.ts ***!
  \************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/shared-module/navbar/navbar.component.ts");
/* harmony import */ var _side_nav_component_side_nav_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./side-nav-component/side-nav.component */ "./src/app/shared-module/side-nav-component/side-nav.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./confirmation/confirmation.component */ "./src/app/shared-module/confirmation/confirmation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"]
            ],
            declarations: [
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"],
                _side_nav_component_side_nav_component__WEBPACK_IMPORTED_MODULE_3__["SideNavComponent"],
                _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ConfirmationComponent"]
            ],
            entryComponents: [_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ConfirmationComponent"]],
            exports: [
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"],
                _side_nav_component_side_nav_component__WEBPACK_IMPORTED_MODULE_3__["SideNavComponent"],
                _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ConfirmationComponent"]
            ],
            providers: [_services_message_service_service__WEBPACK_IMPORTED_MODULE_7__["MessageService"]]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared-module/side-nav-component/side-nav.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/shared-module/side-nav-component/side-nav.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.zenwork-sidenav-content {\n  display: flex;\n  height: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.specifications{\n  display: none;\n}\n.zenwork-sidenav {\n  padding: 20px;\n}\n.zenwork-side-nav-icon{\n  margin: 0 10px;\n  display: inline-block;\n  vertical-align: middle;\n\n}\n.toggle-side-nav-inner{\n\n padding: 30px 0 0;\n}\n.toggle-side-nav-inner { text-align: center;}\n.toggle-side-nav-inner > a { display:inline-block;}\n.toggle-side-nav-inner > a > img{ margin: 0 auto 30px 0;}\n.toggle-side-nav-inner .nav>li>a{\n  padding: 15px 15px 15px 30px;\n  color: #8c8a8a;\n  display: block;\n  vertical-align: middle;\n  font-size: 16px; cursor: pointer;\n}\n.toggle-side-nav-inner .nav>li>a:focus, .toggle-side-nav-inner .nav>li>a:hover {\n  background-color: #f1f9f4;border: none; outline: none;\n}\n.dashboard-sub-menu-main-wrapper{\n  padding: 15px 0; text-align: left;\n  border-bottom:#e0e0e0 1px solid;\n}\n.zenwork-customized-sidebar-menu{\n    width:21.666667%!important;\n}\n.zenwork-main-submenu{\n  padding: 25px 0 !important; text-align: left;\n}\n.zenwork-customized-template{\n  width:76.666667%!important; \n}\nbody{\nbackground-color:#fff;\n}\n/*------ This is MySelf code ------*/\n.contact-info {margin: 0 auto; width: 85%;}\n.top-block { display: block;border-bottom:#008f3d 2px solid; padding: 0 0 20px; margin: 0 0 30px; }\n.top-block a { display: inline-block; vertical-align: middle; width:60px;}\n.right-block { display: inline-block; vertical-align: middle; width:260px;}\n.right-block h2 { color:#484747; font-weight: normal; font-size:20px; line-height:20px; margin:0 0 7px;}\n.right-block small { color:#8c8a8a; font-weight: normal; font-size: 17px; line-height: 17px;display: block;}\n.contact-in { display: block;}\n.contact-in .panel-default>.panel-heading { padding: 0 0 20px 15px; background-color:transparent !important; border: none;}\n.contact-in .panel-title { color: #008f3d; display: inline-block; font-size: 17px;}\n.contact-in .panel-title a:hover { text-decoration: none;}\n.contact-in .panel-title>.small, .contact-in .panel-title>.small>a, .contact-in .panel-title>a, .contact-in .panel-title>small, .contact-in .panel-title>small>a { text-decoration:none;}\n.contact-in .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#cccbcb 1px dashed !important;}\n.contact-in .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.contact-in .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.contact-in .panel-group .panel-heading+.panel-collapse>.list-group, .contact-in .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.contact-in .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.contact-in .panel-body { padding: 0 0 5px;}\n.contact-in .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.contact-in .panel-default { border-color:transparent;}\n.contact-in .panel-group .panel+.panel { margin-top: 20px;}\n.contacts { width: 90%; margin: 0 auto;}\n.contacts ul { display: block;}\n.contacts ul li { margin: 0 0 15px;}\n.contacts ul li span {display: inline-block; cursor: pointer; vertical-align: middle; width: 40px;}\n.contacts ul li a {  display: inline-block; cursor: pointer; vertical-align: middle; width: 200px; color:#8c8a8a; font-size: 17px; line-height: 17px;}\n.contacts h2 {color:#8c8a8a; font-size: 17px; line-height: 17px; font-weight: normal; margin:0 0 10px;}\n.contacts small {color:#8c8a8a; font-size: 15px; line-height: 15px; font-weight: normal;display: block; margin: 0 0 20px;}\n.director {width: 90%; margin: 0 auto ; padding: 0 0 20px;}\n.director a { display: inline-block; vertical-align: middle; width:60px;}\n.director-block { display: inline-block; vertical-align: middle; width:230px;}\n.director-block h2 { color:#8c8a8a; font-weight: normal; font-size:19px; line-height:19px; margin:0 0 7px;}\n.director-block small { color:#ababab; font-weight: normal; font-size: 15px; line-height: 15px;display: block;}\n.director.reports a { width:50px;}\n.green > a {\n  color : #008f3d !important;\n}\n.zenwork-side-nav-green { display: none;}\n/* .toggle-side-nav-inner .nav>li.green a.zenwork-side-nav-green{ visibility: visible;} */\n.zenwork-main-submenu li a span { width:60px; display: inline-block; vertical-align: middle;}\n.zenwork-main-submenu li a span img { margin: 0 auto;}\n.zenwork-main-submenu li a small { display: inline-block; color: #8c8a8a; font-size: 16px;vertical-align: middle;}\n.normal-icon { display: block;}\n.hover-icon { display:none;}\n.zenwork-main-submenu li.active a small{ color: #008f3d;}\n.dashboard-sub-menu-main-wrapper li a span { width:60px; display: inline-block; vertical-align: middle; text-align: center;}\n.dashboard-sub-menu-main-wrapper li a span img { margin: 0 auto; vertical-align:text-top;}\n.dashboard-sub-menu-main-wrapper li a small { display: inline-block; color: #8c8a8a; font-size: 16px;vertical-align: initial;}\n.dashboard-sub-menu-main-wrapper li a small var { display: inline-block; padding: 0 10px;}\n.dashboard-sub-menu-main-wrapper li a small var .fa { display: inline-block; font-size:18px;color: #008f3d;}\n.dashboard-sub-menu-main-wrapper li a small em { display: inline-block;}\n.dashboard-sub-menu-main-wrapper li a small em .fa { display: inline-block; font-size:18px;color: #f00;}\n.dashboard-sub-menu-main-wrapper li.active a small{ color: #008f3d;}\n.client-details{\n  font-size: 10px;\n  /* float: none; */\n  /* border-right: 1px solid gray; */\n}\n.client-details-wrapper{\n  background: #fff;\n  padding: 20px 20px;\n  position: relative;\n}\n.client-details-margin{\n  margin: 6px 0px;\n  display: inline-block;\n  /* float: none; */\n}\n.client-details .media-body ul{\n  margin: 0;                                      \n}\n.client-details .media-body ul li{\n  /* display: inline-block; */\n  padding-bottom: 10px;\n    display: block;\n    float: left;\n    padding-left: 25px;\n}\n.client-details .media-body ul li small{\n  font-size: 16px !important;\n}\n.client-details .media-body ul{\n  margin: 0;                                      \n}\n.client-details .media-body ul li .reseller-btn{\n  background: #f3616c;\n  width: 30px;\n  padding: 5px 0px;\n  display: inline-block;\n  font-size: 15px;\n  margin: 0px 10px 0 0;\n  text-align: center;\n}\n.client-details .media-body ul li .direct-client-btn{\n  background: #1aa4db;\n  width: 30px;\n  padding: 5px 0px;\n  display: inline-block;\n  font-size: 15px;\n  margin: 0px 10px 0 0;\n  text-align: center;\n}\n.client-details .media-body ul li .reseller-direct-client-btn{\n  background: #fbd437;\n  width: 30px;\n  padding: 5px 0px;\n  display: inline-block;\n  font-size: 15px;\n  margin: 0px 10px 0 0;\n  text-align: center;\n}\n/* .sidenav-position{\n  position: fixed;\n} */"

/***/ }),

/***/ "./src/app/shared-module/side-nav-component/side-nav.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/shared-module/side-nav-component/side-nav.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Admin Dashboard Menu Starts -->\n\n<div class=\"toggle-side-nav-inner\" *ngIf=\"userData.role == 'admin' && message != 'reseller'\">\n\n  <ul class=\"nav nav-sidebar dashboard-sub-menu-main-wrapper\">\n    <li routerLinkActive=\"active\">\n\n      <a [routerLink]=\"['/admin/admin-dashboard/dashboard']\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/dashboard_grey.png\" alt=\"Company-settings icon\">\n        </span>\n        <small>Dashboard</small>\n      </a>\n    </li>\n    <li routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management/profile/profile-view/personal']\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/my_info_grey1.png\" alt=\"Company-settings icon\">\n        </span>\n        <small>My Info</small>\n\n      </a>\n    </li>\n    <li routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/inbox']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/inbox_grey1.png\" alt=\"Company-settings icon\">\n        </span>\n        <small>Inbox <var><i class=\"fa fa-check-square\" aria-hidden=\"true\"></i></var> <em><i\n              class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i></em> </small>\n      </a>\n    </li>\n  </ul>\n\n  <ul class=\"nav nav-sidebar zenwork-main-submenu\">\n    <li *ngIf = \"!empHR && !empManager && !empEmployee\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/company-settings']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/company_settings_grey1.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/company_setting_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Company Settings</small>\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/company_settings_grey1.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Company Settings -->\n      </a>\n    </li>\n    <li *ngIf = \"!empEmployee\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/emp_management_grey.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/emp_management_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Employee Management</small>\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/emp_management_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Employee Management -->\n      </a>\n    </li>\n    <li *ngIf = \"!empManager && !empEmployee\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/recruitment']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/recruitment_grey.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/recruitment_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Recruitment</small>\n\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/recruitment_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Recruitment -->\n      </a>\n    </li>\n    <li *ngIf = \"!empManager && !empEmployee\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management/employee-management']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/benefits_grey.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/benefits_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Benefits Administration</small>\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/benefits_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Benefits Administration -->\n      </a>\n    </li>\n    <li *ngIf = \"!empManager && !empEmployee\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management/employee-management']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/performance_grey.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/performance_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Performance Management</small>\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/performance_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Performance Management -->\n      </a>\n    </li>\n    <li *ngIf = \"!empManager && !empEmployee\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/leave-management']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/leave_grey.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/leave_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Leave Management</small>\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/leave_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Leave Management -->\n      </a>\n    </li>\n    <li *ngIf = \"!empManager && !empEmployee\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management/employee-management']\">\n\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/reports_grey.png\" class=\"normal-icon\" alt=\"img\">\n          <img src=\"../../../assets/images/Side Menu/Green/reports_green.png\" class=\"hover-icon\" alt=\"img\">\n        </span>\n        <small>Reports</small>\n\n\n        <!-- <img src=\"../../../assets/images/Side Menu/Grey/reports_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\"> Reports -->\n      </a>\n    </li>\n  </ul>\n</div>\n\n<!-- Admin Dashboard Menu Ends -->\n\n<!-- Super Admin Dashboard Menu Starts -->\n\n<div class=\"toggle-side-nav-inner\" *ngIf=\"userData.role == 'super-admin'  && message != 'reseller'\">\n  <!-- \n\n  <a href=\"#\">\n    <img src=\"../../../assets/images/Side Menu/Green/side-logo.png\" width=\"213\" height=\"36\" alt=\"img\">\n  </a> -->\n\n\n  <ul class=\"nav nav-sidebar dashboard-sub-menu-main-wrapper\">\n    <!-- <li class=\"active\">\n\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management/employee-management']\" >\n        <img src=\"../../../assets/images/Side Menu/Grey/dashboard_grey.png\" class=\"zenwork-side-nav-icon\" alt=\"Company-settings icon\">   \n        Dashboard\n      </a>\n    </li> -->\n\n    <li routerLinkActive=\"active\">\n      <a [routerLink]=\"['/super-admin/super-admin-dashboard/clients']\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/my_info_grey.png\" class=\"zenwork-side-nav-icon\"\n            alt=\"Company-settings icon\">\n          <img src=\"../../../assets/images/Side Menu/Green/green-client.png\" class=\"zenwork-side-nav-green\"\n            alt=\"Company-settings icon\">\n        </span>\n        <small>Clients</small>\n      </a>\n    </li>\n    <li routerLinkActive=\"active\">\n      <a [routerLink]=\"['/super-admin/super-admin-dashboard/zenworkers']\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/inbox_grey.png\" class=\"zenwork-side-nav-icon\"\n            alt=\"Company-settings icon\">\n          <img src=\"../../../assets/images/Side Menu/Green/green-zenwork.png\" class=\"zenwork-side-nav-green\"\n            alt=\"Company-settings icon\">\n        </span>\n        <small> Zenworkers</small>\n      </a>\n    </li>\n    <li routerLinkActive=\"active\">\n      <a [routerLink]=\"['/super-admin/super-admin-dashboard/invoice']\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/company_settings_grey.png\" class=\"zenwork-side-nav-icon\"\n            alt=\"Company-settings icon\">\n          <img src=\"../../../assets/images/Side Menu/Green/green-billings.png\" class=\"zenwork-side-nav-green\"\n            alt=\"Company-settings icon\">\n        </span>\n        <small> Invoice</small>\n      </a>\n    </li>\n\n    <li *ngIf='!restrictLevel1' routerLinkActive=\"active\">\n      <a [routerLink]=\"['/super-admin/super-admin-dashboard/settings']\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/company_settings_grey1.png\" class=\"zenwork-side-nav-icon\"\n            alt=\"Company-settings icon\">\n          <img src=\"../../../assets/images/Side Menu/Green/company_setting_green.png\" class=\"zenwork-side-nav-green\"\n            alt=\"Company-settings icon\">\n        </span>\n        <small> Settings</small>\n      </a>\n    </li>\n\n  </ul>\n  <div class=\"client-details-wrapper form-group\">\n    <div class=\"client-details \">\n      <div class=\"media-body\">\n        <ul class=\"list-unstyled\">\n          <li class=\"list-items\">\n            <span class=\"reseller-btn\">R</span>\n            <small>Reseller</small>\n          </li>\n          <div class=\"clearfix\"></div>\n          <li class=\"list-items\">\n            <span class=\"direct-client-btn\">DC</span>\n            <small>Direct Client</small>\n          </li>\n          <div class=\"clearfix\"></div>\n          <li class=\"list-items\">\n            <span class=\"reseller-direct-client-btn\">RC</span>\n            <small> Reseller's Client</small>\n          </li>\n          <div class=\"clearfix\"></div>\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"toggle-side-nav-inner\" *ngIf=\"message == 'reseller'\">\n\n  <ul class=\"nav nav-sidebar dashboard-sub-menu-main-wrapper\">\n\n\n    <li routerLinkActive=\"active\">\n      <a (click)=\"resellerClients()\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/my_info_grey.png\" class=\"zenwork-side-nav-icon\"\n            alt=\"Company-settings icon\">\n          <img src=\"../../../assets/images/Side Menu/Green/green-client.png\" class=\"zenwork-side-nav-green\"\n            alt=\"Company-settings icon\">\n        </span>\n        <small>Clients</small>\n      </a>\n    </li>\n    <li routerLinkActive=\"active\">\n      <a (click)=\"administrator()\">\n        <span>\n          <img src=\"../../../assets/images/Side Menu/Grey/inbox_grey.png\" class=\"zenwork-side-nav-icon\"\n            alt=\"Company-settings icon\">\n          <img src=\"../../../assets/images/Side Menu/Green/green-zenwork.png\" class=\"zenwork-side-nav-green\"\n            alt=\"Company-settings icon\">\n        </span>\n        <small> Administrator</small>\n      </a>\n    </li>\n\n\n\n\n  </ul>\n  <div class=\"client-details-wrapper form-group\">\n    <div class=\"client-details \">\n      <div class=\"media-body\">\n        <ul class=\"list-unstyled\">\n          <li class=\"list-items\">\n            <span class=\"reseller-btn\">R</span>\n            <small class=\"specification\">Reseller </small>\n          </li>\n          <div class=\"clearfix\"></div>\n          <li class=\"list-items\">\n            <span class=\"direct-client-btn\">DC</span>\n            <small> Direct Client </small>\n          </li>\n          <div class=\"clearfix\"></div>\n          <li class=\"list-items\">\n            <span class=\"reseller-direct-client-btn\">RC</span>\n            <small> Reseller's Client </small>\n          </li>\n          <div class=\"clearfix\"></div>\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>\n<!-- <div *ngIf=\"addClient\" class=\"add-client\">\n\n  <mat-vertical-stepper [linear]=\"isLinear\" #stepper=\"matVerticalStepper\">\n    <ng-template matStepperIcon=\"edit\">\n      <mat-icon>check</mat-icon>\n    </ng-template>\n\n    <mat-step label=\"Client Details\"> </mat-step>\n    <mat-step label=\"Packages & Billing\"> </mat-step>\n\n    <mat-step label=\"Contact Details\"> </mat-step>\n    <mat-step label=\"Payment\"> </mat-step>\n    <mat-step label=\"Finished\"> </mat-step>\n  </mat-vertical-stepper>\n</div> -->\n\n<!-- Super Admin Dashboard Menu Ends -->\n\n\n<!-- <div class=\"contact-info\">\n\n    <div class=\"top-block\">\n       <a href=\"#\"><img src=\"../../../assets/images/company-settings/site-access/contact.png\" width=\"43\" height=\"43\" alt=\"img\"></a>\n       <div class=\"right-block\">\n           <h2>Out Until Jul 27</h2>\n           <small>Sick</small>\n       </div>\n    </div>\n\n\n    <div class=\"contact-in\">\n\n            <div class=\"panel-group\" id=\"accordion4\">\n    \n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseFifteen\">\n                      Contact Info\n                    </a>\n                  </h4>\n                </div>\n                <div id=\"collapseFifteen\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body\">\n\n                    <div class=\"contacts\">\n                         <ul>\n                           <li>\n                             <span><img src=\"../../../assets/images/company-settings/site-access/phone.png\" width=\"20\" height=\"18\" alt=\"img\"></span>\n                             <a href=\"#\">801-724-6600 Ext. 1278</a>\n                           </li>\n                           <li>\n                              <span><img src=\"../../../assets/images/company-settings/site-access/mobile.png\" width=\"14\" height=\"21\" alt=\"img\"></span>\n                              <a href=\"tel:9955576684\">995-557-6684</a>\n                            </li>\n\n                            <li>\n                                <span><img src=\"../../../assets/images/company-settings/site-access/email.png\" width=\"22\" height=\"18\" alt=\"img\"></span>\n                                <a href=\"mailto:nick@zenwork.com\">nick@zenwork.com</a>\n                              </li>\n                              <li>\n                                  <span><img src=\"../../../assets/images/company-settings/site-access/linked.png\" width=\"19\" height=\"19\" alt=\"img\"></span>\n                                  <a href=\"#\">linked in</a>\n                                </li>\n                         </ul>\n                    </div>\n                    \n                  </div>\n                </div>\n    \n              </div>\n    \n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseSixteen\">\n                      Hire Date\n                    </a>\n                  </h4>\n    \n                </div>\n    \n                <div id=\"collapseSixteen\" class=\"panel-collapse collapse\">\n                  <div class=\"panel-body\">\n\n                      <div class=\"contacts\">\n                          <h2>June 08, 2012</h2>\n                          <small>5yrs - 1m -16d</small>\n                      </div>\n    \n                  </div>\n                </div>\n              </div>\n    \n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseSventeen\">\n                      Job Type\n                    </a>\n                  </h4>\n                </div>\n    \n                <div id=\"collapseSventeen\" class=\"panel-collapse collapse\">\n                  <div class=\"panel-body\">\n    \n                      <div class=\"contacts\">\n                          <ul>\n                            <li>\n                              <span><img src=\"../../../assets/images/company-settings/site-access/user.png\" width=\"18\" height=\"18\" alt=\"img\"></span>\n                              <a href=\"#\"># 121</a>\n                            </li>\n                            <li>\n                               <span><img src=\"../../../assets/images/company-settings/site-access/full.png\" width=\"18\" height=\"18\" alt=\"img\"></span>\n                               <a href=\"#\">Full Time</a>\n                             </li>\n \n                             <li>\n                                 <span><img src=\"../../../assets/images/company-settings/site-access/customer.png\" width=\"26\" height=\"16\" alt=\"img\"></span>\n                                 <a href=\"#\">Customer Success</a>\n                               </li>\n                               <li>\n                                   <span><img src=\"../../../assets/images/company-settings/site-access/north.png\" width=\"18\" height=\"18\" alt=\"img\"></span>\n                                   <a href=\"#\">North America</a>\n                                 </li>\n\n                                 <li>\n                                    <span><img src=\"../../../assets/images/company-settings/site-access/usa.png\" width=\"13\" height=\"18\" alt=\"img\"></span>\n                                    <a href=\"#\">United States, Arkansas</a>\n                                  </li>\n                          </ul>\n                     </div>\n\n                  </div>\n                </div>\n\n              </div>\n\n\n              <div class=\"panel panel-default\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseEighteen\">\n                       Director\n                      </a>\n                    </h4>\n                  </div>\n      \n                  <div id=\"collapseEighteen\" class=\"panel-collapse collapse\">\n                    <div class=\"panel-body\">\n\n                        <div class=\"director\">\n                            <a href=\"#\"><img src=\"../../../assets/images/company-settings/site-access/director.png\" width=\"51\" height=\"51\" alt=\"img\"></a>\n                            <div class=\"director-block\">\n                                <h2>Sanjeev Kumar</h2>\n                                <small>Founder and CEO</small>\n                            </div>\n                         </div>\n                     \n                    </div>\n                  </div>\n  \n                </div>\n\n\n                <div class=\"panel panel-default\">\n                    <div class=\"panel-heading\">\n                      <h4 class=\"panel-title\">\n                        <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseNinteen\">\n                         Director Reports\n                        </a>\n                      </h4>\n                    </div>\n        \n                    <div id=\"collapseNinteen\" class=\"panel-collapse collapse\">\n                      <div class=\"panel-body\">\n\n                          <div class=\"director reports\">\n                              <a href=\"#\"><img src=\"../../../assets/images/company-settings/site-access/employee.png\" width=\"38\" height=\"38\" alt=\"img\"></a>\n                              <div class=\"director-block\">\n                                  <h2>Crane Andy</h2>\n                                  <small>Manager</small>\n                              </div>\n                           </div>\n                       \n                      </div>\n                    </div>\n    \n                  </div>\n  \n\n\n    \n            </div>\n    \n          </div>\n\n    \n\n</div>\n -->"

/***/ }),

/***/ "./src/app/shared-module/side-nav-component/side-nav.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/shared-module/side-nav-component/side-nav.component.ts ***!
  \************************************************************************/
/*! exports provided: SideNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideNavComponent", function() { return SideNavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_reseller_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/reseller.service */ "./src/app/services/reseller.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SideNavComponent = /** @class */ (function () {
    function SideNavComponent(route, messageService, router, loaderService, resellerService, accessLocalStorageService, companySettingsService) {
        var _this = this;
        this.route = route;
        this.messageService = messageService;
        this.router = router;
        this.loaderService = loaderService;
        this.resellerService = resellerService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.companySettingsService = companySettingsService;
        this.showTabs = {
            Assets: { hide: true },
            Benefits: { hide: true },
            Compensation: { hide: true },
            Documents: { hide: true },
            Emergency_Contact: { hide: true },
            Job: { hide: false },
            Notes: { hide: true },
            Offboarding: { hide: true },
            Onboarding: { hide: true },
            Performance: { hide: true },
            Personal: { hide: false },
            Time_Off: { hide: false },
            Training: { hide: true }
        };
        this.restrictLevel1 = false;
        this.empHR = false;
        this.empManager = false;
        this.empEmployee = false;
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
        this.empType = this.accessLocalStorageService.get('employeeType');
        this.type = this.accessLocalStorageService.get("type");
        console.log('type', this.empType);
        if (this.empType == 'HR') {
            this.empHR = true;
        }
        if (this.empType == 'Manager') {
            this.empManager = true;
        }
        if (this.empType == 'Employee') {
            this.empEmployee = true;
        }
        console.log(this.type, "2w2w");
        if (this.type == 'administrator') {
            this.message = 'reseller';
        }
        if (this.type == 'zenworker') {
            this.acceslevel = this.accessLocalStorageService.get('zenworkerId');
            console.log(this.acceslevel);
            if (this.acceslevel.roleId.name == 'Level 1' || 'Level 2' || 'Billing') {
                this.restrictLevel1 = true;
            }
        }
        router.events.subscribe(function (url) { return console.log(url); });
        console.log(router.url);
        this.url = router.url;
        if (this.url == "/super-admin/super-admin-dashboard/reseller/" + this.id) {
            this.message = 'reseller';
        }
        if (this.url == "/super-admin/super-admin-dashboard/reseller/" + this.id + "/administrator") {
            this.message = 'reseller';
        }
        this.subscription = this.messageService.getMessage().subscribe(function (message) {
            _this.message = message.text;
            console.log("message", _this.message);
        });
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log("iddddd", _this.id, params);
        });
        console.log("23fsdfsd", this.message);
        this.controlMyinfoSections();
    }
    Object.defineProperty(SideNavComponent.prototype, "getUserData", {
        set: function (data) {
            console.log("Data", data);
            this.userData = data;
        },
        enumerable: true,
        configurable: true
    });
    SideNavComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
            this.loaderService.loader(true);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
            this.loaderService.loader(false);
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationCancel"]) {
            this.loaderService.loader(false);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationError"]) {
            this.loaderService.loader(false);
        }
    };
    SideNavComponent.prototype.controlMyinfoSections = function () {
        var _this = this;
        this.companySettingsService.controllSections()
            .subscribe(function (res) {
            console.log(res);
            _this.showTabs = res.data.myInfo_sections;
            if (_this.showTabs.Personal.hide == false) {
                _this.x = 'job';
            }
        }, function (err) {
        });
    };
    SideNavComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    SideNavComponent.prototype.resellerClients = function () {
        console.log("resellerloist");
        var id = localStorage.getItem('resellercmpnyID');
        console.log("cid", id);
        id = id.replace(/^"|"$/g, "");
        console.log("cin2", id);
        this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id]);
    };
    SideNavComponent.prototype.administrator = function () {
        var id = localStorage.getItem('resellerId');
        console.log(id);
        console.log("cid", id);
        id = id.replace(/^"|"$/g, "");
        console.log("cin2", id);
        this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id + '/administrator']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideNavComponent.prototype, "getUserData", null);
    SideNavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-side-nav',
            template: __webpack_require__(/*! ./side-nav.component.html */ "./src/app/shared-module/side-nav-component/side-nav.component.html"),
            styles: [__webpack_require__(/*! ./side-nav.component.css */ "./src/app/shared-module/side-nav-component/side-nav.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_1__["MessageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"],
            _services_reseller_service__WEBPACK_IMPORTED_MODULE_3__["ResellerService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__["AccessLocalStorageService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_4__["CompanySettingsService"]])
    ], SideNavComponent);
    return SideNavComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    // url: "http://13.233.37.94:3003",
    // url: "http://10.1.3.64:3003",
    url: "http://52.207.167.138:3003",
    //  url : "http://localhost:3003",
    // url :"http://192.168.1.202:3003",
    // url: "http://192.168.1.59:3003",
    // url : "http://192.168.1.171:3003",
    // url : "http://10.1.6.203:3003",
    // url : "http://10.1.5/.24:3003",
    version: "/v1.0"
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/mtwlabs/zenwork-hr-angular/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map