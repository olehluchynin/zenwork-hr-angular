(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["emergency-emergency-module"],{

/***/ "./src/app/admin-dashboard/employee-management/emergency/emergency.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/emergency/emergency.module.ts ***!
  \***********************************************************************************/
/*! exports provided: EmergencyModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmergencyModule", function() { return EmergencyModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _emergency_emergency_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./emergency/emergency.component */ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.ts");
/* harmony import */ var _emergency_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./emergency.routing */ "./src/app/admin-dashboard/employee-management/emergency/emergency.routing.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var EmergencyModule = /** @class */ (function () {
    function EmergencyModule() {
    }
    EmergencyModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _emergency_routing__WEBPACK_IMPORTED_MODULE_3__["EmergencyRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_9__["MaterialModuleModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
            ],
            declarations: [_emergency_emergency_component__WEBPACK_IMPORTED_MODULE_2__["EmergencyComponent"]],
            providers: [_services_my_info_service__WEBPACK_IMPORTED_MODULE_10__["MyInfoService"]]
        })
    ], EmergencyModule);
    return EmergencyModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/emergency/emergency.routing.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/emergency/emergency.routing.ts ***!
  \************************************************************************************/
/*! exports provided: EmergencyRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmergencyRouting", function() { return EmergencyRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _emergency_emergency_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./emergency/emergency.component */ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/emergency",pathMatch:"full" },
    { path: '', component: _emergency_emergency_component__WEBPACK_IMPORTED_MODULE_2__["EmergencyComponent"] },
];
var EmergencyRouting = /** @class */ (function () {
    function EmergencyRouting() {
    }
    EmergencyRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EmergencyRouting);
    return EmergencyRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.css":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.css ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*-- Author:Suresh M, Date:08-04-19  --*/\n/*-- Informaton relative css code  --*/\n.emergency { margin: 30px auto 0; float: none;}\n.emergency .panel-default>.panel-heading {  border: none; position: relative;}\n.emergency .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px; float: left;}\n.emergency .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.emergency .panel-title>.small, .emergency .panel-title>.small>a, .emergency .panel-title>a, .emergency .panel-title>small, .emergency .panel-title>small>a { text-decoration:none;}\n.emergency .panel { box-shadow: none; border-radius: 0;}\n.emergency .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.emergency .panel-heading {background:#edf7ff; padding: 15px 0 35px 20px;}\n.emergency .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.emergency .panel-group .panel-heading+.panel-collapse>.list-group, .emergency .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.emergency .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.emergency .panel-body { padding: 0 0 5px;}\n.emergency .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.emergency .panel-default { border-color:transparent;}\n.emergency .panel-group .panel+.panel { margin-top: 20px;}\n.emergency button.mat-menu-item a { color:#ccc;}\n.emergency button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.emergency button { border: none; outline: none; background:transparent; position: absolute; top:3px; right: 0;}\n.emergency-cont {width: 100%; background:#fff; padding: 30px 20px;}\n.emergency-cont ul { display: block;}\n.emergency-cont ul li { border-bottom:#eaeaea 1px solid; padding: 0 0 15px; margin: 0 0 15px;}\n.emergency-cont ul li small { margin:15px 0 0;}\n.employee span { float: left;}\n.employee-rgt {margin:10px 0 0;}\n.employee-rgt em { display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n.employee-rgt em .material-icons {  font-size: 20px; line-height: 15px;}\n.primary-color {color:#fec550;}\n.employee-rgt .panel-default>.panel-heading {  border: none; background-color:transparent;}\n.employee-rgt .panel-title a { color: #888888; display: inline-block; font-size: 19px; position: static; float: none;}\n.employee-rgt .panel-title a:hover { text-decoration: none;color: #888888;}\n.employee-rgt .panel-title>.small, .employee-rgt .panel-title>.small>a, .employee-rgt .panel-title>a, .employee-rgt .panel-title>small, .employee-rgt .panel-title>small>a { text-decoration:none;}\n.employee-rgt .panel { box-shadow: none; border-radius: 0;}\n.employee-rgt .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.employee-rgt .panel-heading {padding:0;}\n.employee-rgt .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.employee-rgt .panel-group .panel-heading+.panel-collapse>.list-group, .employee-rgt .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.employee-rgt .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.employee-rgt .panel-body { padding: 0 0 5px;}\n.employee-rgt .panel-group { margin-bottom: 0;}\n.employee-rgt .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.employee-rgt .panel-default { border-color:transparent;}\n.employee-rgt .panel-group .panel+.panel { margin-top: 20px;}\n.emergency-cont-in { padding: 10px 0 0;}\n.employee-address {padding: 0;}\n.employee-address address { margin: 0;}\n.employee-address address p { color:#a7a5a5; font-size: 15px; line-height: 24px;}\n.employee-address address p:hover{color:#a7a5a5;}\n.employee-address ul { display: block;}\n.employee-address ul li { border-bottom: none; padding: 0;}\n.employee-address ul li p{ color:#a7a5a5;}\n.employee-address ul li p var { display: inline-block; vertical-align: middle; width: 30px;}\n.employee-address ul li p a{ display: inline-block; vertical-align: middle;color:#a7a5a5;font-size: 15px;}\n.employee-contacts .modal-dialog { width: 50%;}\n.employee-contacts .modal-content { border-radius: 5px !important;}\n.employee-contacts .modal-header {padding: 15px 15px 20px;}\n.employee-contacts .modal-header h4 { padding: 0 0 0 30px;}\n.employee-contacts { display: block;}\n.employee-details { padding: 30px 25px 35px 50px;}\n.employee-details ul { display: inline-block; width: 100%;}\n.employee-details ul li {margin: 0 20px 20px 0; padding: 0;}\n.employee-details ul li h4 { color:#636060; font-size: 17px; line-height: 16px; margin: 0 0 15px;}\n.employee-details ul li .phone { display: block; background:#f8f8f8; padding: 0 10px; height: 38px; line-height: 38px; clear: left;}\n.employee-details ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n.employee-details ul li .phone span .fa { color: #636060;\n    font-size: 23px;\n    line-height: 20px;\n    width: 18px;\n    text-align: center;}\n.employee-details ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 80%; border: none; background:#f8f8f8; box-shadow: none; padding: 0 0 0 10px;}\n.employee-details ul li .form-control { display: inline-block;vertical-align: middle; width: 40%; border: none; background:#f8f8f8; box-shadow: none; \nheight: 40px; line-height: 40px; padding: 0 10px; font-size: 15px;}\n.employee-details ul li.phone-width {  clear:left;}\n.employee-details ul li.email-width { float: none;}\n.social.administration-contact ul li.phone-width {  clear:left;width:25%}\n.employee-details ul li.details-width{ width: 25%;}\n.employee-details ul li .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none; height: auto; width: 100%; padding: 10px;}\n.employee-details ul li select { background:#f8f8f8; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n.employee-details ul li.city { width: 19.5%;float: left;}\n.employee-details ul li.state { width: 19.5%; float: left;}\n.employee-details ul li.zip { width: 12%; float: left; padding: 0;}\n.employee-details ul li.united-state { margin: 0; padding: 0; clear: left;}\n.employee-details ul li.address{ float: none;}\n.employee-contacts .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 15px; padding: 0 24px;margin:20px 20px 0 0;background: #008f3d; color:#fff;}\n.employee-contacts .btn.cancel { color:#e44a49; background:transparent; font-size: 17px; padding:20px 0 0 20px;}\n/*-- Author:Suresh M, Date:09-04-19  --*/\n/*-- Informaton relative css code  --*/\n.employee-details ul li.relation{ float:left; display: inline-block; vertical-align:middle; width:30%}\n.employee-details ul li label { font-weight: normal; font-size:17px;color:#636060; line-height: 17px; display: block; padding: 0 0 10px;}\n.employee-details ul li.relation:nth-child(3) { margin-top:27px;}\n.employee-details hr { margin:30px 0;}\n/* -----------------    ------------------------*/\n.next-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 20px;\n    color: #fff;\n    padding: 0 30px;    \n}\n.emergency-cont ul li var {padding:10px 0 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Author:Suresh M, Date:08-04-19  -->\n<!-- Emergency Contact Accordion -->\n\n<div class=\"emergency col-md-11\">\n\n  <div class=\"panel-group\" id=\"accordion3\">\n\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        <h4  class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseOne\">\n            Employee's Details\n          </a>\n          <button *ngIf=\"!emergencyTabView\" mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n\n          <mat-menu #menu=\"matMenu\">\n            <button mat-menu-item *ngIf=\"!contactIds.length >=1\">\n              <a data-toggle=\"modal\" data-target=\"#myModal\">Add</a>\n            </button>\n\n            <button mat-menu-item *ngIf=\"(!contactIds.length <=0) && (contactIds.length ==1)\">\n              <a (click)=\"edit()\" >Edit</a>\n              <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit></a>\n            </button>\n\n            <button mat-menu-item *ngIf=\"(!contactIds.length <=0) && (contactIds.length >=1)\">\n              <a (click)=\"delete()\">Delete</a>\n            </button>\n          </mat-menu>\n\n        </h4>\n      </div>\n      <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n\n          <div class=\"emergency-cont\">\n\n            <ul>\n\n              <li *ngFor=\"let contacts of emergencyContacts\">\n\n                <small class=\"col-md-1\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"contacts.isChecked\" (ngModelChange)=\"checkContact($event,contacts._id)\"></mat-checkbox>\n                </small>\n\n                <var class=\"col-md-1\">\n                    <em><i class=\"material-icons star-position\" [ngClass]=\"{'primary-color': contacts.primaryContact == true}\">star</i></em> \n                </var>\n\n\n                <div class=\"employee col-md-10\">\n\n                  <span class=\"col-md-1\">\n                      <i class=\"material-icons emplployee-icon\">\n                          account_circle\n                        </i>\n                    </span>\n\n                   \n                  <div class=\"employee-rgt col-md-11\">\n\n                    <div class=\"panel-group\" id=\"accordion4\">\n\n                      <div class=\"panel panel-default\">\n                        <div class=\"panel-heading\">\n                          <h4 class=\"panel-title\">\n                            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\"\n                              href=\"#collapseTwo\">\n                              {{contacts.fullName}}\n                            </a>\n                          </h4>\n                        </div>\n                        <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n                          <div class=\"panel-body\">\n\n                            <div class=\"emergency-cont-in\">\n                              <div class=\"employee-address col-md-8\">\n                                <address>\n                                  <p>Relationship: &nbsp;{{contacts.relationship}} <br> {{contacts.address.street1}} <br> {{contacts.address.street2}}, {{contacts.address.city}} - {{contacts.address.zipcode}}</p>\n                                </address>\n                              </div>\n                              <div class=\"employee-address col-md-4 pull-right\">\n\n                                <ul>\n                                  <li>\n                                    <p><var><img src=\"../../../../../assets/images/Directory/ic_email_24px.png\"\n                                          width=\"16\" height=\"13\" alt=\"img\"></var>\n                                      <a href=\"mail-to:\">{{contacts.contact.workMail}}</a> </p>\n                                  </li>\n                                  <li>\n                                    <p><var><img src=\"../../../../../assets/images/Directory/ic_domain_24px1.png\"\n                                          width=\"18\" height=\"16\" alt=\"img\"></var>\n                                      <a href=\"tel:\">{{contacts.contact.workPhone}}</a> Ext.{{contacts.contact.extention}} </p>\n                                  </li>\n                                  <li>\n                                    <p><var><img src=\"../../../../../assets/images/Directory/Group 504.png\" width=\"10\"\n                                          height=\"16\" alt=\"img\"></var>\n                                      <a href=\"tel:\">{{contacts.contact.homePhone}}</a></p>\n                                  </li>\n                                </ul>\n\n                              </div>\n                              <div class=\"clearfix\"></div>\n\n                            </div>\n                            <div class=\"clearfix\"></div>\n\n                          </div>\n                        </div>\n\n                      </div>\n                    </div>\n\n                  </div>\n                  <div class=\"clearfix\"></div>\n                </div>\n\n                <div class=\"employee-cont pull-right\">\n\n                </div>\n                <div class=\"clearfix\"></div>\n              </li>\n\n            </ul>\n\n          </div>\n\n\n          <div class=\"clearfix\"></div>\n\n\n        </div>\n      </div>\n\n\n    </div>\n    <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\"></mat-paginator>\n\n  </div>\n \n</div>\n<div style=\"margin:20px 65px;\">\n  \n    <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Save Changes</button>\n<div class=\"clear-fix\"></div>\n  </div>\n\n\n<div class=\"employee-contacts\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n\n          <h4>Add/Edit Emergency Contact</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"employee-details\">\n\n            <!-- Author:Suresh M, Date:09-04-19  -->\n            <!-- Emergency Contact Accordion -->\n            <form [formGroup]=\"employeeForm\">\n\n\n              <ul>\n                <li class=\"col-md-3 relation\" *ngIf=\"fieldsShow.Name.hide !== true\">\n                  <label>Name <span *ngIf=\"fieldsShow.Name.required\">*</span></label>\n                 \n                  <input *ngIf=\"fieldsShow.Name.format == 'Alpha' || fieldsShow.Name.format == 'text'\" (keypress)=\"alphabets($event)\" type=\"text\" class=\"form-control\" placeholder=\"Full Name\" formControlName=\"fullName\" [required]=\"fieldsShow.Name.required\"> \n                  <span *ngIf=\"employeeForm.get('fullName').invalid && employeeForm.get('fullName').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\"> Enter name</span>\n                </li>\n                <li class=\"col-md-3 relation\" *ngIf=\"fieldsShow.Relationship.hide !== true\">\n                  <label>Relationship <span *ngIf=\"fieldsShow.Relationship.required\">*</span></label>\n                  <mat-select class=\"form-control\" placeholder=\"Select relationship\" formControlName=\"relationship\" [required]=\"fieldsShow.Relationship.required\">\n                    <mat-option *ngFor=\"let relation of allRelation\" [value]='relation.name'>{{relation.name}}</mat-option>\n\n                  </mat-select>\n                  <span *ngIf=\"employeeForm.get('relationship').invalid && employeeForm.get('relationship').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Select relationship</span>\n                </li>\n                <li class=\"col-md-2 relation\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" formControlName=\"primaryContact\" (change)=\"checkPrimaryContact($event)\">Primary Contact</mat-checkbox>\n                </li>\n              </ul>\n\n              <ul>\n                <li>\n                  <h4>Phone</h4>\n                </li>\n                <li class=\"col-md-4\" *ngIf=\"fieldsShow.Work_Phone.hide !== true\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                    </span>\n                    <input *ngIf=\"fieldsShow.Work_Phone.format == 'number'\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10  class=\"form-control\" placeholder=\"Work Phone\" formControlName=\"workPhone\" [required]=\"fieldsShow.Work_Phone.required\">\n                    <!-- <input *ngIf=\"fieldsShow.Work_Phone.format == 'Alpha' || fieldsShow.Work_Phone.format == 'text'\" (keypress)=\"alphabets($event)\" type=\"text\"  class=\"form-control\" placeholder=\"Work Phone\" formControlName=\"workPhone\" [required]=\"fieldsShow.Work_Phone.required\">\n                    <input *ngIf=\"fieldsShow.Work_Phone.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\" placeholder=\"Work Phone\" formControlName=\"workPhone\" [required]=\"fieldsShow.Work_Phone.required\"> -->\n\n                    \n                  </div>\n                  <span *ngIf=\"employeeForm.get('workPhone').invalid && employeeForm.get('workPhone').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block; border:none;\">Enter valid mobile phone</span>\n                </li>\n\n                <li class=\"col-md-2\" *ngIf=\"fieldsShow.Ext.hide !== true\">\n\n                  <input  class=\"form-control\" placeholder=\"Ext\" (keypress)=\"keyPress($event)\" minlength=4\n                  maxlength=4 formControlName=\"extention\" [required]=\"fieldsShow.Ext.required\">\n                  <span *ngIf=\"employeeForm.get('extention').invalid && employeeForm.get('extention').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Provide valid Ext</span>\n\n                </li>\n                <div class=\"clearfix\"></div>\n             </ul>\n                \n\n             <ul>\n                <li class=\"col-md-4 phone-width\" *ngIf=\"fieldsShow.Mobile_Phone.hide !== true\">\n                  <div class=\"phone\">\n                    <span>\n                      <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                    </span>\n                    <input *ngIf=\"fieldsShow.Mobile_Phone.format == 'number'\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10 class=\"form-control\" placeholder=\"Mobile Phone\" formControlName=\"mobilePhone\" [required]=\"fieldsShow.Mobile_Phone.required\">\n                    <!-- <input *ngIf=\"fieldsShow.Mobile_Phone.format == 'Alpha' || fieldsShow.Mobile_Phone.format == 'text'\" (keypress)=\"alphabets($event)\" type=\"text\"  type=\"number\" class=\"form-control\" placeholder=\"Mobile Phone\" formControlName=\"mobilePhone\" [required]=\"fieldsShow.Mobile_Phone.required\">\n                    <input *ngIf=\"fieldsShow.Mobile_Phone.format == 'AlphaNumeric'\" type=\"number\" class=\"form-control\" placeholder=\"Mobile Phone\" formControlName=\"mobilePhone\" [required]=\"fieldsShow.Mobile_Phone.required\"> -->\n\n                  </div>\n                  <span *ngIf=\"employeeForm.get('mobilePhone').invalid && employeeForm.get('mobilePhone').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter valid mobile phone</span>\n                </li>\n                <li class=\"col-md-4 phone-width\" *ngIf=\"fieldsShow.Home_Phone.hide !== true\">\n                  <div class=\"phone\">\n                    <span>\n                      <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                    </span>\n                    <input *ngIf=\"fieldsShow.Home_Phone.format == 'number'\" (keypress)=\"keyPress($event)\" minlength=10 maxlength=10 class=\"form-control\" placeholder=\"Home Phone*\" formControlName=\"homePhone\" [required]=\"fieldsShow.Home_Phone.required\">\n                    <!-- <input *ngIf=\"fieldsShow.Home_Phone.format == 'Alpha' || fieldsShow.Home_Phone.format == 'text'\"  type=\"text\" (keypress)=\"alphabets($event)\" class=\"form-control\" placeholder=\"Home Phone\" formControlName=\"homePhone\" [required]=\"fieldsShow.Home_Phone.required\">\n                    <input *ngIf=\"fieldsShow.Home_Phone.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\" placeholder=\"Home Phone\" formControlName=\"homePhone\" [required]=\"fieldsShow.Home_Phone.required\">                                         -->\n                  </div>\n                  <span *ngIf=\"employeeForm.get('homePhone').invalid && employeeForm.get('homePhone').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter valid home phone</span>\n                </li>\n              </ul>\n\n              <ul>\n\n                <li>\n                  <h4 *ngIf=\"fieldsShow.Work_Email.hide !== true || fieldsShow.Home_Email.hide !== true\">Email</h4>\n                </li>\n\n                <li class=\"col-md-5 email-width\" *ngIf=\"fieldsShow.Work_Email.hide !== true\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" formControlName=\"workMail\" [required]=\"fieldsShow.Work_Email.required\"\n                    pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n                   \n                  </div>\n                  <span *ngIf=\"employeeForm.get('workMail').invalid && employeeForm.get('workMail').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Provide valid work E-mail</span>\n                </li>\n\n                <li class=\"col-md-5 email-width\" *ngIf=\"fieldsShow.Home_Email.hide !== true\">\n                  <div class=\"phone\">\n                    <span>\n                      <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                    </span>\n                    <input type=\"email\" class=\"form-control\" placeholder=\"Home Email*\" formControlName=\"personalMail\" [required]=\"fieldsShow.Home_Email.required\"\n                    pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n                    \n                  </div>\n                  <span *ngIf=\"employeeForm.get('personalMail').invalid && employeeForm.get('personalMail').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Provide valid home E-mail</span> \n                </li>\n              </ul>\n\n              <ul>\n                <li>\n                  <h4 *ngIf=\"fieldsShow.Street_1.hide !== true || fieldsShow.Street_2.hide !== true || fieldsShow.City.hide !== true || fieldsShow.State.hide !== true || fieldsShow.Zip.hide !== true\">Address</h4>\n                </li>\n\n                <li class=\"col-md-5 address\" *ngIf=\"fieldsShow.Street_1.hide !== true\">\n                  <input *ngIf=\"fieldsShow.Street_1.format == 'number'\" type=\"number\" placeholder=\"Street1*\" maxlength=\"28\" class=\"form-control\" formControlName=\"street1\" [required]=\"fieldsShow.Street_1.required\">\n                  <input *ngIf=\"fieldsShow.Street_1.format == 'Alpha' || fieldsShow.Street_1.format == 'text'\" maxlength=\"28\"  type=\"text\" placeholder=\"Street1*\" class=\"form-control\" formControlName=\"street1\" [required]=\"fieldsShow.Street_1.required\">\n                  <input *ngIf=\"fieldsShow.Street_1.format == 'AlphaNumeric'\" type=\"text\" placeholder=\"Street1*\" class=\"form-control\" maxlength=\"28\" formControlName=\"street1\" [required]=\"fieldsShow.Street_1.required\">\n\n                  <span *ngIf=\"employeeForm.get('street1').invalid && employeeForm.get('street1').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Provide valid address in street 1</span> \n                </li>\n                <li class=\"col-md-5 address\" *ngIf=\"fieldsShow.Street_2.hide !== true\">\n                  <input *ngIf=\"fieldsShow.Street_2.format == 'number'\" type=\"number\" placeholder=\"Street2\" maxlength=\"28\" class=\"form-control\" formControlName=\"street2\" >\n                  <input *ngIf=\"fieldsShow.Street_2.format == 'Alpha' || fieldsShow.Street_2.format == 'text'\" maxlength=\"28\" type=\"text\" placeholder=\"Street2\" class=\"form-control\" formControlName=\"street2\" >\n                  <input *ngIf=\"fieldsShow.Street_2.format == 'AlphaNumeric'\" type=\"text\" class=\"form-control\" maxlength=\"28\" formControlName=\"street2\" >\n\n                  <!-- <span *ngIf=\"employeeForm.get('street2').invalid && employeeForm.get('street2').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">You must enter a street2</span>  -->\n                </li>\n              </ul>\n\n              <ul>\n                <li class=\"state\" *ngIf=\"fieldsShow.City.hide !== true\">\n        \n                  <input *ngIf=\"fieldsShow.City.format == 'Alpha' || fieldsShow.City.format == 'text'\" maxlength=\"24\" (keypress)=\"alphabets($event)\" type=\"text\" placeholder=\"City*\" class=\"form-control\" formControlName=\"city\" [required]=\"fieldsShow.Zip.required\">\n\n\n                  <span *ngIf=\"employeeForm.get('city').invalid && employeeForm.get('city').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter city name</span>\n                </li>\n\n                <li class=\"city\" *ngIf=\"fieldsShow.State.hide !== true\">\n                  <mat-select class=\"form-control\" placeholder=\"State*\" formControlName=\"state\" [required]=\"fieldsShow.State.required\">\n                    <mat-option *ngFor=\"let state of allState\" [value]=\"state.name\">{{state.name}}</mat-option>\n                  </mat-select>\n                  <span *ngIf=\"employeeForm.get('state').invalid && employeeForm.get('state').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter state name</span>\n                </li>\n\n                <li class=\"zip\" *ngIf=\"fieldsShow.Zip.hide !== true\">\n                  <input *ngIf=\"fieldsShow.Zip.format == 'number'\" minlength=\"5\" maxlength=\"5\" (keypress)=\"keyPress($event)\" placeholder=\"ZIP*\" class=\"form-control\" formControlName=\"zipcode\" [required]=\"fieldsShow.Zip.required\">\n     \n\n                  <span *ngIf=\"employeeForm.get('zipcode').invalid && employeeForm.get('zipcode').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter valid zip</span>\n                </li>\n\n                <li class=\"col-md-3 united-state\">\n                  <mat-select class=\"form-control\" placeholder=\"Country\" formControlName=\"country\">\n                    <mat-option *ngFor=\"let country of allCountry\" [value]=\"country.name\">{{country.name}}</mat-option>\n\n                  </mat-select>\n                  <span *ngIf=\"employeeForm.get('country').invalid && employeeForm.get('country').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter country name</span>\n                </li>\n              </ul>\n\n\n            </form>\n              <div class=\"clearfix\"></div>\n\n              <hr>\n              <button class=\"btn pull-left cancel\"  data-dismiss=\"modal\" (click)=\"cancelEmergencyContact()\">Cancel</button>\n              <button class=\"btn pull-right save\" type=\"submit\" (click)=\"employeeEmergencyContactSubmit()\"> Submit</button>\n\n              <div class=\"clearfix\"></div>\n\n\n          </div>\n\n          \n\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.ts ***!
  \************************************************************************************************/
/*! exports provided: EmergencyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmergencyComponent", function() { return EmergencyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EmergencyComponent = /** @class */ (function () {
    function EmergencyComponent(employeeService, accessLocalStorageService, swalAlertService, myInfoService, router) {
        this.employeeService = employeeService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.myInfoService = myInfoService;
        this.router = router;
        this.customEmergencyContact = [];
        this.defaultState = [];
        this.customState = [];
        this.allState = [];
        this.defaultCountry = [];
        this.customCountry = [];
        this.allCountry = [];
        this.customRelation = [];
        this.defaultRelation = [];
        this.allRelation = [];
        this.tabName = "Emergency_Contact";
        this.contactIds = [];
        this.fieldsShow = {
            City: {
                hide: '',
                required: '',
                format: ''
            },
            Ext: {
                hide: '',
                required: '',
                format: ''
            },
            Home_Email: {
                hide: '',
                required: '',
                format: ''
            },
            Home_Phone: {
                hide: '',
                required: '',
                format: ''
            },
            Mobile_Phone: {
                hide: '',
                required: '',
                format: ''
            },
            Name: {
                hide: '',
                required: '',
                format: ''
            },
            State: {
                hide: '',
                required: '',
                format: ''
            },
            Relationship: {
                hide: '',
                required: '',
                format: ''
            },
            Work_Email: {
                hide: '',
                required: '',
                format: ''
            },
            Street_1: {
                hide: '',
                required: '',
                format: ''
            },
            Street_2: {
                hide: '',
                required: '',
                format: ''
            },
            Work_Phone: {
                hide: '',
                required: '',
                format: ''
            },
            Zip: {
                hide: '',
                required: '',
                format: ''
            },
        };
        this.contact = {
            Name: "",
            Relationship: '',
            contact: {
                Work_Phone: '',
                Ext: '',
                Mobile_Phone: '',
                Home_Phone: ''
            },
            email: {
                Work_Email: '',
                Home_Email: ''
            },
            address: {
                Street_1: '',
                Street_2: '',
                City: '',
                State: '',
                Zip: '',
            },
            isPrimaryContact: false
        };
        this.emergencyContacts = [];
        this.selectedEmergencyContact = [];
        this.deleteSelectedContacts = [];
        this.pagesAccess = [];
        this.emergencyTabView = false;
    }
    EmergencyComponent.prototype.ngOnInit = function () {
        this.roletype = JSON.parse(localStorage.getItem('type'));
        if (this.roletype != 'company') {
            this.pageAccesslevels();
        }
        this.getSingleTabSettingForEmergencyContact();
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        console.log(this.userId);
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
        }
        this.pageNo = 1;
        this.perPage = 10;
        this.getAllEmergencyContacts();
        this.getStandardAndCustomStructureFields();
        this.employeeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            fullName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            relationship: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            primaryContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](false),
            workPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            extention: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            mobilePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            homePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            workMail: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]),
            personalMail: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]),
            street1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            street2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            state: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](""),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
        });
        // this.emergencyContact = this.employeeData.emergencyContact;
    };
    EmergencyComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Emergency Contact" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.emergencyTabView = true;
                    console.log('loggss', _this.emergencyTabView);
                }
            });
        }, function (err) {
        });
    };
    // Author:Saiprakash, Date:15/05/2019
    // Get Standard And Custom Structure Fields
    EmergencyComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customCountry = _this.getAllStructureFields['Country'].custom;
            _this.defaultCountry = _this.getAllStructureFields['Country'].default;
            _this.allCountry = _this.customCountry.concat(_this.defaultCountry);
            _this.customState = _this.getAllStructureFields['State'].custom;
            _this.defaultState = _this.getAllStructureFields['State'].default;
            _this.allState = _this.customState.concat(_this.defaultState);
            _this.customRelation = _this.getAllStructureFields['Emergency Contact Relationship'].custom;
            _this.defaultRelation = _this.getAllStructureFields['Emergency Contact Relationship'].default;
            _this.allRelation = _this.customRelation.concat(_this.defaultRelation);
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash, Date:12/04/2019
    // Get single tab settings for emergency contact
    EmergencyComponent.prototype.getSingleTabSettingForEmergencyContact = function () {
        var _this = this;
        this.myInfoService.getSingleTabSettingForMyinfo(this.tabName).subscribe(function (res) {
            console.log(res);
            _this.fieldsShow = res.data.fields;
            console.log(_this.fieldsShow);
        }, function (err) {
            console.log(err);
        });
    };
    EmergencyComponent.prototype.checkPrimaryContact = function (event) {
        console.log(event);
    };
    EmergencyComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllEmergencyContacts();
    };
    // Author:Saiprakash G, Date:10-05-19
    // Get all employee emergency contact details
    EmergencyComponent.prototype.getAllEmergencyContacts = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            userId: this.userId,
            per_page: this.perPage,
            page_no: this.pageNo
        };
        console.log(this.companyId);
        this.myInfoService.getAllEmergencyContacts(postData).subscribe(function (res) {
            console.log(res);
            _this.emergencyContacts = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash G, Date:10-05-19
    // Add and edit employee emergency contact details
    EmergencyComponent.prototype.employeeEmergencyContactSubmit = function () {
        var _this = this;
        console.log("Employee Details", this.employeeForm.value);
        var data = {
            companyId: this.companyId,
            userId: this.userId,
            fullName: this.employeeForm.get('fullName').value,
            relationship: this.employeeForm.get('relationship').value,
            primaryContact: this.employeeForm.get('primaryContact').value,
            contact: {
                workPhone: this.employeeForm.get('workPhone').value,
                extention: this.employeeForm.get('extention').value,
                mobilePhone: this.employeeForm.get('mobilePhone').value,
                homePhone: this.employeeForm.get('homePhone').value,
                workMail: this.employeeForm.get('workMail').value,
                personalMail: this.employeeForm.get('personalMail').value,
            },
            address: {
                street1: this.employeeForm.get('street1').value,
                street2: this.employeeForm.get('street2').value,
                city: this.employeeForm.get('city').value,
                state: this.employeeForm.get('state').value,
                zipcode: this.employeeForm.get('zipcode').value,
                country: this.employeeForm.get('country').value,
            }
        };
        if (this.employeeForm.valid) {
            if (this.getSingleEmergencyContact && this.emergencyContactId) {
                this.myInfoService.editEmergencyContact(this.emergencyContactId, data).subscribe(function (res) {
                    console.log(res);
                    _this.employeeForm.reset();
                    _this.emergencyContactId = "";
                    _this.contactIds = [];
                    $("#myModal").modal("hide");
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.getAllEmergencyContacts();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.myInfoService.addEmergenctContact(data).subscribe(function (res) {
                    console.log(res);
                    _this.employeeForm.reset();
                    $("#myModal").modal("hide");
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.contactIds = [];
                    _this.getAllEmergencyContacts();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
        }
        else {
            this.employeeForm.get('fullName').markAsTouched();
            this.employeeForm.get('relationship').markAsTouched();
            this.employeeForm.get('workPhone').markAsTouched();
            this.employeeForm.get('extention').markAsTouched();
            this.employeeForm.get('mobilePhone').markAsTouched();
            this.employeeForm.get('homePhone').markAsTouched();
            this.employeeForm.get('workMail').markAsTouched();
            this.employeeForm.get('personalMail').markAsTouched();
            this.employeeForm.get('street1').markAsTouched();
            this.employeeForm.get('city').markAsTouched();
            this.employeeForm.get('state').markAsTouched();
            this.employeeForm.get('zipcode').markAsTouched();
            this.employeeForm.get('country').markAsTouched();
        }
    };
    // Author:Saiprakash G, Date:10-05-19
    // Get single employee emergency contact details
    EmergencyComponent.prototype.edit = function () {
        var _this = this;
        this.selectedEmergencyContact = this.emergencyContacts.filter(function (contactCheck) {
            return contactCheck.isChecked;
        });
        if (this.selectedEmergencyContact.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one contact to proceed", "", 'error');
        }
        else if (this.selectedEmergencyContact.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one contact to proceed", "", 'error');
        }
        else {
            this.openEdit.nativeElement.click();
            this.myInfoService.getEmergencyContact(this.companyId, this.userId, this.selectedEmergencyContact[0]._id)
                .subscribe(function (res) {
                console.log(res);
                _this.getSingleEmergencyContact = res.data;
                _this.emergencyContactId = res.data._id;
                _this.employeeForm.patchValue(_this.getSingleEmergencyContact);
                _this.employeeForm.get('city').patchValue(_this.getSingleEmergencyContact.address.city);
                _this.employeeForm.get('country').patchValue(_this.getSingleEmergencyContact.address.country);
                _this.employeeForm.get('state').patchValue(_this.getSingleEmergencyContact.address.state);
                _this.employeeForm.get('street1').patchValue(_this.getSingleEmergencyContact.address.street1);
                _this.employeeForm.get('street2').patchValue(_this.getSingleEmergencyContact.address.street2);
                _this.employeeForm.get('zipcode').patchValue(_this.getSingleEmergencyContact.address.zipcode);
                _this.employeeForm.get('extention').patchValue(_this.getSingleEmergencyContact.contact.extention);
                _this.employeeForm.get('homePhone').patchValue(_this.getSingleEmergencyContact.contact.homePhone);
                _this.employeeForm.get('mobilePhone').patchValue(_this.getSingleEmergencyContact.contact.mobilePhone);
                _this.employeeForm.get('personalMail').patchValue(_this.getSingleEmergencyContact.contact.personalMail);
                _this.employeeForm.get('workMail').patchValue(_this.getSingleEmergencyContact.contact.workMail);
                _this.employeeForm.get('workPhone').patchValue(_this.getSingleEmergencyContact.contact.workPhone);
            });
        }
    };
    // Author:Saiprakash G, Date:10-05-19
    // Delete employee emergency contacts
    EmergencyComponent.prototype.delete = function () {
        var _this = this;
        this.selectedEmergencyContact = this.emergencyContacts.filter(function (contactCheck) {
            return contactCheck.isChecked;
        });
        if (this.selectedEmergencyContact.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one contact to proceed", "", 'error');
        }
        else {
            // this.deleteSelectedContacts.push(this.selectedEmergencyContact.Id)
            var deleteData = {
                companyId: this.companyId,
                userId: this.userId,
                _ids: this.selectedEmergencyContact
            };
            console.log(deleteData);
            this.myInfoService.deleteEmergencyContact(deleteData).subscribe(function (res) {
                console.log(res);
                _this.contactIds = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getAllEmergencyContacts();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    EmergencyComponent.prototype.cancelEmergencyContact = function () {
        this.employeeForm.reset();
        this.contactIds = [];
        $("#myModal").modal("hide");
        this.getAllEmergencyContacts();
    };
    /* Description:accept only alphabets
    author : vipin reddy */
    EmergencyComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 &&
            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123);
    };
    EmergencyComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    EmergencyComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/compensation"]);
    };
    EmergencyComponent.prototype.checkContact = function (event, id) {
        console.log(event, id);
        if (event == true) {
            this.contactIds.push(id);
        }
        console.log(this.contactIds);
        if (event == false) {
            var index = this.contactIds.indexOf(id);
            if (index > -1) {
                this.contactIds.splice(index, 1);
            }
            console.log(this.contactIds);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EmergencyComponent.prototype, "openEdit", void 0);
    EmergencyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-emergency',
            template: __webpack_require__(/*! ./emergency.component.html */ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.html"),
            styles: [__webpack_require__(/*! ./emergency.component.css */ "./src/app/admin-dashboard/employee-management/emergency/emergency/emergency.component.css")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_5__["MyInfoService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], EmergencyComponent);
    return EmergencyComponent;
}());



/***/ })

}]);
//# sourceMappingURL=emergency-emergency-module.js.map