(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["leave-management-leave-management-module~scheduler-scheduler-module~work-schedules-work-schedules-mo~f3cda310"],{

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.css":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.css ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".open-close-time ul li{\n    display: inline-block;\n    padding: 0 25px 20px 0;\n    position: relative;\n}\n.emp-open-time{\n    width: 100%;\n    border: 0;\n    background: #f8f8f8;\n    height: 33px;\n    padding: 0 10px;\n}\n.open-close-time ul li .clock-icon{\n  font-size: 17px;\n  color: #439348;\n  position: absolute;\n  top: 40px;\n  right: 30px;\n  border-left: 1px solid #c7c4c4;\n  padding: 0 0 0 5px;\n}\n.mar-top-10 {\n    margin-top: 10px;\n    outline: none;\n    \n}\n.zenwork-input{\n width: 15%;\n}\n.dynamic-resource {\n    margin-bottom: 30px;\n    border-bottom: 1px solid #ecdddd;\n    padding-bottom: 20px;\n}\n.recap-table {margin: 40px 0 0 0;}\n.recap-table .table>thead>tr>th { color: #6f6f6f; font-weight: normal;  background: #eef7ff; border:none; font-size: 16px;\nborder-radius:3px;}\n.recap-table .table>tbody>tr>td, .recap-table .table>tfoot>tr>td, .recap-table .table>thead>tr>td{ background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#948e8e;}\n.recap-table .checkbox{ margin: 0;}\n.recap-table .checkbox label { padding-left: 0;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top:4px;height:80%; border-radius: 0 4px 4px 0; padding: 0; left: 0;}\n.recap-table .table>tbody>tr>td {padding: 15px 15px;}\n.time-intervals {\n    margin-top: 47px;\n    margin-left: 70px;\n}\n.time-intervals ul li{\n    padding: 7px 0 10px 0px;\n    margin-left: 80px;\n}\n.resource-count {\n    border: none;\n    outline: none;\n    box-shadow: none;\n    width: 40%;\n    /* background: #f8f8f8; */\n}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4 class=\"dynamic-resource\">Define Resources</h4>\n<div class=\"open-close-time\">\n  <ul class=\"list-unstyled\" style=\"margin-left: 100px\">\n    <li class=\"list-items\">\n      <label>Open Time</label><br>\n      <input type=\"text\" class=\"emp-open-time mar-top-10\" [(ngModel)]=\"openTime\" disabled>\n      <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n    </li>\n    <li class=\"list-items\">\n      <label>Close Time</label><br>\n      <input type=\"text\" class=\"emp-open-time mar-top-10\" [(ngModel)]=\"closeTime\" disabled>\n      <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n    </li>\n  </ul>\n</div>\n\n<div class=\"form-group schdule-type\" style=\"margin-left: 100px\">\n  <label>\n    Interval Definition\n  </label><br>\n  <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"intervalDefinition\" disabled>\n    <mat-option [value]=\"30\">\n      30 Min\n    </mat-option>\n    <mat-option [value]=\"60\">\n      60 Min\n    </mat-option>\n    <mat-option [value]=\"90\">\n      90 Min\n    </mat-option>\n  </mat-select>\n</div>\n<div class=\"col-xs-12\">\n  <!-- <div class=\"col-xs-3 time-intervals\">\n    <ul class=\"list-unstyled\">\n      <li>Intervals</li>\n      <li>Scheduled Time</li>\n      <li>Number of <br>Resources Needed</li>\n    </ul>\n  </div> -->\n\n  <div class=\"recap-table time-intervals col-xs-5\">\n    <table class=\"table\" style=\"margin-bottom:0px;\">\n      <thead>\n        <tr>\n          <th>Intervals</th>\n          <th>Scheduled Time</th>\n          <th>Number of Resources Needed</th>\n         </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor = \"let interval of intervals; let i=index;\">\n            <td>Int-{{i+1}}</td>\n            <td>{{interval.scheduledTime}}</td>\n            <td>\n              <mat-select *ngIf=\"!interval.resources\" class=\"form-control resource-count\" [(ngModel)]=\"interval.resourcesNeeded\" (ngModelChange)=\"selectNumberOfResources($event, interval.scheduledTime, i+1)\">\n                <mat-option *ngFor=\"let resource of resources\" [value]=\"resource\">{{resource}}</mat-option>\n    \n              </mat-select>\n       \n            </td>\n        </tr>\n       \n      </tbody>\n\n    </table>\n\n  </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<hr class=\"zenwork-margin-ten-zero\">\n<div class=\"row\" style=\"margin:30px 0px;\">\n    \n    <button mat-button (click)=\"onNoClick()\">Cancel</button>\n\n    <button mat-button class=\"submit-btn pull-right\" (click)=\"setIntervalAndResources()\">Submit</button>\n  </div>\n  <div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: DynamicResourceModelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DynamicResourceModelComponent", function() { return DynamicResourceModelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var DynamicResourceModelComponent = /** @class */ (function () {
    function DynamicResourceModelComponent(dialogRef, swalAlertService, data) {
        this.dialogRef = dialogRef;
        this.swalAlertService = swalAlertService;
        this.data = data;
        this.resources = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        this.intervals = [];
        this.setTimeIntervals = [];
    }
    DynamicResourceModelComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    DynamicResourceModelComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.data);
        if (this.data) {
            if (this.data.Open_Time) {
                this.openTime = this.data.Open_Time;
                var OT = this.openTime;
                this.timeConvertor(OT);
                console.log(this.timeConvertor(OT));
                this.openTimeMS = this.timeToMs(this.timeConvertor(OT));
                console.log(this.openTimeMS);
            }
            if (this.data.Close_Time) {
                this.closeTime = this.data.Close_Time;
                var CT = this.closeTime;
                this.timeConvertor(CT);
                this.closeTimeMS = this.timeToMs(this.timeConvertor(CT));
                console.log(this.closeTimeMS);
            }
            this.intervalDefinition = this.data.Interval_Definition;
            var intervalDiff = (this.intervalDefinition * 60) * 1000;
            console.log(intervalDiff);
            for (var i = this.openTimeMS; i < this.closeTimeMS; i = i + intervalDiff) {
                if (i < this.closeTimeMS) {
                    this.intervals.push({
                        scheduledTime: this.msToTime(i)
                    });
                }
                console.log(this.intervals);
            }
            var resourceNeeded = [];
            if (this.data.resourceInfo && this.data.resourceInfo.intervalResourcesInformation) {
                console.log(this.data.resourceInfo);
                for (var j = 0; j < this.intervals.length; j++) {
                    this.data.resourceInfo.intervalResourcesInformation.forEach(function (element) {
                        resourceNeeded.push(element.resourcesNeeded);
                        _this.intervals[j].resourcesNeeded = resourceNeeded[j];
                        _this.intervals[j].interval = j + 1;
                    });
                    console.log(this.intervals);
                }
            }
        }
    };
    DynamicResourceModelComponent.prototype.timeConvertor = function (time) {
        time = time.toUpperCase().replace(' ', '');
        var PM = time.match('PM') ? true : false;
        time = time.split(':');
        var min = parseInt(time[1], 10);
        if (PM) {
            var hour = parseInt(time[0], 10);
            if (hour < 12) {
                hour += 12;
            }
            // var sec = time[2].replace('PM', '')
        }
        else {
            var hour = parseInt(time[0], 10);
            if (hour == 12) {
                hour = 0;
            }
            // var sec = time[2].replace('AM', '')       
        }
        // console.log(hour + ':' + min + ':' + sec)
        console.log(hour + ':' + min);
        return hour + ':' + min;
    };
    DynamicResourceModelComponent.prototype.timeToMs = function (time) {
        var milliSec = Number(time.split(':')[0]) * 60 * 60 * 1000 + Number(time.split(':')[1]) * 60 * 1000;
        return milliSec;
    };
    // Author: Saiprakash G, Date: 25/06/19,
    // Description: Converted milliseconds to time
    DynamicResourceModelComponent.prototype.msToTime = function (i) {
        this.minutes = Math.floor((i / (1000 * 60)) % 60);
        this.hours = Math.floor((i / (1000 * 60 * 60)) % 24);
        this.hours = (this.hours < 10) ? "0" + this.hours : this.hours;
        this.minutes = (this.minutes < 10) ? "0" + this.minutes : this.minutes;
        var time = ['', ':', '', ' ', ''];
        if (this.hours < 12) {
            time[4] = 'AM';
        }
        else {
            time[4] = 'PM';
        }
        time[2] = String(this.minutes);
        if (time[2].length == 1) {
            time[2] = '0' + time[2];
        }
        time[0] = this.hours % 12 || 12;
        console.log(time.join(""));
        return time.join("");
        // return this.hours+":"+this.minutes;
    };
    // Author: Saiprakash G, Date: 25/06/19,
    // Description: Selected number of resources
    DynamicResourceModelComponent.prototype.selectNumberOfResources = function (number, interval, i) {
        console.log(number, interval);
        this.number = number;
        this.interval = interval;
        var intervalNumber = [];
        for (var j = 0; j < this.intervals.length; j++) {
            intervalNumber.push(j + 1);
            this.intervals[j].interval = intervalNumber[j];
            console.log(this.intervals);
        }
    };
    DynamicResourceModelComponent.prototype.setIntervalAndResources = function () {
        // if(this.intervals){
        //   for(var i=0; i<this.intervals.length; i++){
        //     if(this.intervals[i].resourceNeeded == this.intervals.length){
        //       this.dialogRef.close({
        //         Intervals: this.intervals
        //       })
        //     } else {
        //       this.swalAlertService.SweetAlertWithoutConfirmation( "Select all number of resources","", "error");
        //     }
        //   }
        // }
        this.dialogRef.close({
            Intervals: this.intervals
        });
    };
    DynamicResourceModelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dynamic-resource-model',
            template: __webpack_require__(/*! ./dynamic-resource-model.component.html */ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.html"),
            styles: [__webpack_require__(/*! ./dynamic-resource-model.component.css */ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.css")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"], Object])
    ], DynamicResourceModelComponent);
    return DynamicResourceModelComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".holidays-main-tab{\n    margin: 20px 0 0 30px;\n}\na{\n    color: #000;\n}\n.resumes-field-heading small{\n    font-size: 15px;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n.field-accordion .panel-default>.panel-heading{\n    background: #f8f8f8 !important;\n    position: relative;\n}\n.field-accordion .panel{\n    box-shadow: none;background: #f8f8f8;\n}\n.field-accordion .panel-title a{\n    display: inline-flex;\n}\n.field-accordion .panel-title { display: inline-block;}\n.field-accordion .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.main-punches-filters {\n    position: absolute;\n    top: 15px;\n    right: 28px;\n}\n.main-punches-filters .search-category .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n    /* margin: 5px; */\n    position: relative;\n    /* font-size: 12px; */\n}\n.main-punches-filters .search-category .list-items span{\n    cursor: pointer;\n    font-size: 15px;\n}\n.custom-tables { margin: 0 auto; float: none; padding: 15px 20px 0 0;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 14px;}\n.custom-fields .checkbox{ margin: 0;}\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n.heade-check .checkbox label::after {  color:#fff; background: #439348;width: 20px; height: 20px; padding: 3px 0 0 4px;border-radius: 3px;}\n.heade-check .checkbox{ margin: 0;}\n.heade-check .checkbox label { padding-left: 0;}\n.cont-check .checkbox label::before {width: 20px; height: 20px; top: -1px;}\n.cont-check .checkbox label::after { color:#fff; background: #439348;width: 20px; height: 20px; padding: 3px 0 0 4px;border-radius: 3px;}\n.cont-check .checkbox{ margin: 0;}\n.cont-check .checkbox label { padding-left: 0;}\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n.custom-tables .table>thead>tr>th.zenwork-assets-checked-border {position: relative;}\n.custom-tables .table>tbody>tr>td .form-control {    border: none; width: 110px; box-shadow: none; color: #000; background: #f8f8f8;}\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.custom-tables .table>thead>tr>th.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.select-drop select { background:#fff; border: none; width: 80%; height: 34px; color:#948e8e; padding: 6px 10px;}\n.custom-tables .btn { float: right; color:#ec6469; border:#ec6469 1px solid; padding:5px 25px; border-radius: 20px; background: transparent; font-size: 15px; margin: 20px 0 0;}\n.excel-sheet-icon{\n    display: inline-block;\n    padding: 0 0 0 8px;\n    cursor: pointer;\n  }\n/* first-tab-modal */\n.modal-body{\n    /* background:#f8f8f8; */\n    padding: 0;\n}\n.tabs-left .nav-tabs{\n  margin: 20px 0 0;\n    border-right: 0;\n}\n.tab-content{\n  margin: 0;\n  /* background: #fff; */\n}\n.sidenav-tabcontent{\n    background: #fff !important;\n    padding: 30px 0px; margin: 0 auto; float: none;\n\n}\n.tabs-customize{\n    background: #f8f8f8;\n    padding:0px!important;\n}\n.modal-body .container{\n    width:100%;\n}\n/* tabs-end */\n.modal-margintop{\n    margin-top:20px;\n}\n/* side nav css*/\n.tabs-left{\n    margin-top:0;\n}\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n  }\n.tabs-left > .nav-tabs {\n    float: left;\n    /* border-right: 1px solid #ddd; */\n  }\n.tabs-left > .nav-tabs > li > a {\n    font-size: 15px;\n    margin-right: -1px;\n    border-radius: 4px 0 0 4px;\n  }\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee;\n  }\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n/*side nav css ends*/\n.caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\ninput{\n  height:30px;\n  /* border:1px solid #e5e5e5; */\n}\n.hiredate{\n    width: 100%;\n    font-size: 14px;\n    padding-left: 13px;\n    border: 0;\n    background: #f8f8f8;\n  }\n.work-schedule .list-items{\n    display: inline-block;\n    width: 17%;\n    margin-right: 30px;\n    vertical-align: top;\n}\n.work-schedule .list-items .benefits-dropdown{\n    width: 100% !important;\n    background: #f8f8f8;\n}\n.onboarding-tabs li a:active,.onboarding-tabs li a:visited,.onboarding-tabs li a:focus{\n    border: 1px solid transparent!important;\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n}\n.sidenav-tabs-tabs>li.active>a,.sidenav-tabs-tabs>li.active>a:focus, .sidenav-tabs-tabs>li.active>a:hover{\n    border: 1px solid transparent!important;\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    /* background: #f8f8f8; */\n    font-size: 16px;\n}\n.sidenav-tabs-tabs li a:active,.sidenav-tabs-tabs li a:visited,.sidenav-tabs-tabs li a:focus{\n    /* background: #008f3d !important; */\n    border-radius: 40px !important;\n    color: #fff !important;\n    /* border: transparent !important; */\n}\nlabel{\n    font-weight: normal;\n    /* line-height: 30px; */\n    font-size: 15px;\n   margin: 0 0 0 2px;\n}\ninput{\n    border:0;\n    box-shadow: none;\n    font-size: 12px;\n}\nselect{\n    height:30px;\n    font-size: 12px;\n}\n.second-popup-body{\n    /* padding: 10px 150px; */\n    background: #fff;\n}\n.second-work-schedule ul li{\n    display: inline-block;vertical-align: top;\n}\n.second-work-schedule ul .work-title{\n    width: 25%;\n    margin-right: 30px;\n}\n.second-work-schedule ul{\n  margin: 0;\n  border-bottom: 1px solid #f8f8f8;\n}\n.work-schedule-title ul{\n    margin: 0;\n    width: 40%;\n}\n.second-work-schedule ul .work-title .template,.work-schedule-title ul .work-title .template{\n  background: #f8f8f8;\n  border: none;\n  width: 100%;\n  height: auto;\n  padding:11px 12px; font-size: 14px;\n}\n.second-work-schedule ul .hiredates-list{\n    position: relative;\n    padding: 0 0 0 15px;\n}\n.second-work-schedule ul .hiredates-list .second-popup-calendar-icon{\n    position: absolute;\n    width: 20px;\n    top: 38px;\n    right: 8px;\n}\n.schdule-type{\n    margin: 0 0 20px 0;\n}\n.schdule-type mat-select{\n    width: 17% !important;\n}\n.schdule-type label { display: block;}\n.emp-hours-working {\n    margin-top: 20px;\n}\n.schdule-type .form-control { height:auto; padding: 10px 12px;}\n.emp-hours-working .form-control {height:auto; padding: 10px 12px; font-size: 14px; box-shadow: none;}\n.request-time-off-popup .modal-body{\n    background: #fff !important;\n    padding: 30px !important;\n}\n.request-time-off-popup .modal-body .custom-tables .table>tbody>tr>td,.fist-popup-modal .custom-tables .table>tbody>tr>td{\n    background: #f8f8f8 !important;\n}\n.request-time-off-popup .field-accordion .panel-default > .panel-heading {\n    background: #fff !important;\n}\n.request-time-off-popup .custom-tables{\n    padding: 0 ! important;\n}\n.request-time-off-popup .category-field{\n    background: #f8f8f8;\n}\n.request-time-off-popup .field-accordion .panel{\n    background: #fff !important;\n}\n.request-time-off-popup .main-punches-filters{\n    position: absolute;\n    top: -24px;\n    right: -8px;\n}\n.emp-calendar{\n    padding: 0 20px 0 0px;\n    width: 98%;\n    margin: 0 0 30px 0;\n}\n.policy-type{\n    padding: 30px 20px 0 20px;\n    width: 70%;\n}\n.total-main-list{\n    margin: 0 !important;\n}\n.main-calendar-filters{\n    padding: 0 12px 0 0;\n}\n.day-emp-calendar{\n    margin: 10px 0 0 0;\n}\n.submit-buttons .list-item {\n    display: inline-block;\n    padding: 10px 20px 0 0;\n }\n.submit-buttons .btn-success {\n    background-color: #439348;\n }\n.btn-style {\n   padding: 4px 20px !important;\n   border-radius: 30px!important;\n   font-size: 14px;\n }\n.emp-time{\n   border: 0;\n   background: #fff;\n   outline: none;\n   padding-left: 15px;\n }\n.custom-tables tbody tr .start-time{\n   width: 25%;\n   position: relative;\n }\n.custom-tables tbody tr .end-time{\n        width: 25%;\n        position: relative;\n }\n.custom-tables tbody tr .start-time .clock-icon,.custom-tables tbody tr .end-time .clock-icon{\n  font-size: 20px;\n  color: #c7c4c4;\n  position: absolute;\n  top: 20px;\n  right: 80px;\n  border-left: 1px solid #c7c4c4;\n  padding: 0 0 0 5px;\n}\n.select-week-days{\n margin: 20px 0px;\n}\n.select-week-days .main-head-weekdays{\n    margin: 0 0 20px 0;\n }\n.open-close-time ul li{\n    display: inline-block;\n    padding: 0 25px 20px 0;\n    position: relative;\n}\n.emp-open-time{\n    width: 100%;\n    border: 0;\n    background: #f8f8f8;\n    padding: 0 10px;\n}\n.open-close-time ul li .clock-icon{\n  font-size: 17px;\n  color: #439348;\n  position: absolute;\n  top: 40px;\n  right: 30px;\n  border-left: 1px solid #c7c4c4;\n  padding: 0 0 0 5px;\n}\n.standard-schedule-table ul li{\n    display: inline-block;\n    padding: 0 20px 0 0;\n    width: 28%;\n}\n.standard-schedule-table ul li mat-select{\n    width: 100% !important;\n}\n.standard-schedule-table ul li .btn-define{\n    color: #fff;\n    background: #797d7a;\n}\n.assign-work-schedule-tab p{\n    font-size: 16px;\n}\n.education-date {\n    margin: 20px 5px 20px 0;\n}\n.education-date-list1 {\n    width: 43%;\n    position: relative;\n    display: inline-block;\n    margin: 0px;\n    padding: 0 10px;\n}\n.edu-ends-calendar-icon{\n    position: absolute;\n    top: 40px;\n    width: 15px;\n    right: 25px;\n}\n.birthdate{\n    font-size: 14px;\n    padding-left: 15px;\n    /* width: 178px;    */\n    box-shadow: none;\n}\n.margin-top-20 {\n    margin: 40px 0px;\n}\n.address-state-dropodown{\n    width: 100%;\n    border-radius: 0px!important;\n    height: 30px;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n}\n.select-employee-tab .block-out-heading{\n    padding: 0 0 0 20px;\n}\n.field-accordion .eligibility-panel .eligibility-panel-heading{\n    background: #fff !important;\n    padding: 5px 20px;\n}\n.checkbox_allemployees label::before ,.checkbox_allemployees label::after {\n    border-radius: 11px !important;\n   \n}\n.main-punches-filters .submit-buttons{\n    margin: 0;\n}\n.main-punches-filters ul li .checkbox_allemployees {\n    background: #edf7fe;\n    padding: 6px 23px;\n    margin: 0;\n    border-radius: 21px;\n}\n.checkbox_allemployees label{\n    padding: 0 0 0 5px !important;\n}\n.eligibility-criteria-list  {\n    margin: 20px 0 0 0;\n}\n.eligibility-criteria-panel-body{\n    background: #fff;\n    padding: 0px 20px;\n    /* margin: 20px 0 0 0; */\n}\n.eligibility-criteria-list ul{\n    background: #f8f8f8;\n}\n.eligibility-criteria-list ul li{\n    padding: 2px 0px;\n    border-right: 1px solid #ededed;\n}\n.eligibility-criteria-select-all{\n    position: absolute;\n    top: 0px;\n    right: 28px;\n}\n.eligibility-criteria-list .second-eligibility-list li{\n    border-right: none;\n}\n.field-accordion .eligibility-panel .panel-body{\n    background: #fff;\n}\n.mar-top-10 {\n    margin-top: 10px;\n    outline: none;\n}\n.import-date-btn {\n    background-color: #797d7a;\n    border: 1px solid #797d7a;\n    padding: 5px 14px;\n    border-radius: 50px;\n    color: #fff;\n    outline: none;\n    font-size:12px;\n  }\n.group-list .table>tbody>tr>td {\n    background: #f8f8f8;\n    border: none;\n    vertical-align: middle;\n    border-top: none;\n    border-bottom: #c3c3c3 1px solid;\n    color: #948e8e;\n  }\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;\n    margin: 0 30px 0 0; \n  }\n.mar-top-10 {\n      margin-top: 10px;\n  }\n.selected-days .list-days {\n    margin-bottom: 5px;\n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.back-btn {\n    border: 1px solid #e6d4d4;\n    border-radius: 25px;\n    padding: 0px 25px;\n}\n.date1 { height: auto !important; line-height:inherit !important; width:100%;display: block;\n    background: #f8f8f8; margin-top: 5px; }\n.date1 button { display: inline-block; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\n  height: 40px; min-width: auto; margin-left: 10px;}\n.date1 span { border: none !important; padding: 0 !important;}\n.date1 .form-control {  height: auto; padding: 12px 0 12px 10px; display: inline-block;\n    vertical-align: middle; }\n/* .date .tier-width { width:25%; height: auto; padding: 12px 0 12px 10px;} */\n.date1 .form-control:focus { box-shadow: none; border: none;}\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.recap-table {margin:20px 20px 0 0;}\n.recap-table .table>thead>tr>th { color: #6f6f6f; font-weight: normal;  background: #eef7ff; border:none; font-size: 16px;\nborder-radius:3px;}\n.recap-table .table>tbody>tr>td, .recap-table .table>tfoot>tr>td, .recap-table .table>thead>tr>td{ background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#948e8e;}\n.recap-table .checkbox{ margin: 0;}\n.recap-table .checkbox label { padding-left: 0;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top:4px;height:80%; border-radius: 0 4px 4px 0; padding: 0; left: 0;}\n.recap-table .table>tbody>tr>td {padding: 10px 5px;}\n.recap-table1 {    margin: 20px 20px 0 0;width: 850px;overflow-x: auto;}\n.recap-table1 .table>thead>tr>th { color: #6f6f6f; font-weight: normal;  background: #eef7ff; border:none; font-size: 16px;\nborder-radius:3px;}\n.recap-table1 .table>tbody>tr>td, .recap-table1 .table>tfoot>tr>td, .recap-table1 .table>thead>tr>td{ background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#948e8e;}\n.recap-table1 .checkbox{ margin: 0;}\n.recap-table1 .checkbox label { padding-left: 0;}\n.recap-table1 .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n.recap-table1 .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top:4px;height:80%; border-radius: 0 4px 4px 0; padding: 0; left: 0;}\n.recap-table1 .table>tbody>tr>td {padding: 10px 5px;}\n.example-list {\n    width: 1000px;\n    max-width: 100%;\n    border: solid 1px #ccc;\n    min-height: 60px;\n    display: flex;\n    flex-direction: row;\n    background: white;\n    border-radius: 4px;\n    overflow: hidden;\n  }\n.horizontal-box {\n    padding:15px 10px;\n    background: #fff;\n  }\n.error{\n    font-size:14px;padding:5px 0 0;\n    color: #f44336;  \n}\n/* .cdk-drag-preview {\n    box-sizing: border-box;\n    border-radius: 4px;\n    box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n                0 8px 10px 1px rgba(0, 0, 0, 0.14),\n                0 3px 14px 2px rgba(0, 0, 0, 0.12);\n  }\n  \n  .cdk-drag-placeholder {\n    opacity: 0;\n  }\n  \n  .cdk-drag-animating {\n    transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  }\n  \n  .example-box:last-child {\n    border: none;\n  }\n  \n  .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\n    transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  }\n   */\n.structure-tbl-body {\n    background: #f8f8f8;\n    width: 45%;\n  }\n.height-300 {\n    height: 300px;\n    overflow: auto;\n  }\n.structure-list .list-items{\n    padding: 15px 15px;\n    border-bottom: 2px solid #eee;\n  }\n.structure-list .list-items-values{\n    padding: 17px 37px;\n    border-bottom: 2px solid #eee;\n  }\n.arrow-position {\n    position: relative;\n  }\n.arrow-align-one {\n    font-size: 40px;\n    position: absolute;\n    top: 145px;\n    left: 30px;\n    font-weight: lighter;\n    color: #439348; \n  }\n.arrow-align-two {\n    font-size: 40px;\n    position: absolute;\n    top: 145px;\n    right: 0;\n    left: 0px;\n    font-weight: lighter;\n    color: #439348;\n  }\n.margin-btm {\n    border-bottom: 1px solid #fdeded;\n    padding-bottom: 20px;\n  }\n.employees-data{\n    padding: 0px 30px;\n  }\n.date1 { height: auto; line-height:inherit; width:100%;background:#f8f8f8;}\n.date1 a { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date1 a:focus { border: none; outline: none;}\n.date1 span { border: none !important; padding: 0 !important;}\n.date1 .form-control { width:65%; height: auto; padding: 12px 0 12px 10px; background: #f8f8f8;}\n.date1 .form-control:focus { box-shadow: none; border: none;}\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.sidenav-right {padding: 30px 0 0;}\n.tabs-left > .nav-tabs > li > a:hover, .tabs-left > .nav-tabs > li > a:focus{ border-color:transparent; background: transparent;}\n.schedule-creation { padding: 0 10px 0 0;}\n.days-right { margin: 0 16px 0 0;}\n.mar-top-5 {\n  height: 40px;\n  margin-top: 5px;\n}\n.dual-list .listbox button{\n  background: #008f3d !important;\n}\n.time-picker-color{\n  --body-background-color: #fff;\n  --primary-font-family: 'Roboto',sans-serif;\n  --button-color: #439348;\n  --dial-active-color: #fff;\n  --dial-inactive-color: rgba(255, 255, 255, .5);\n  --dial-background-color: #439348;\n  --clock-face-time-active-color: #fff;\n  --clock-face-time-inactive-color: #6c6c6c;\n  --clock-face-inner-time-inactive-color: #929292;\n  --clock-face-time-disabled-color: #c5c5c5;\n  --clock-face-background-color: #f0f0f0;\n  --clock-hand-color: #439348;\n}\n.dept-date {\n  padding: 0px 0 0 10px;\n  width: 95%;\n  height: auto 0px;\n  height: 35px;\n  background: #f8f8f8;\n}\n.example-box {\n  padding:5px;\n  border-right: solid 1px #ccc;\n  color: rgba(0, 0, 0, 0.87);\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: space-between;\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 14px;\n  flex-grow: 1;\n  flex-basis: 0;\n}\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n              0 8px 10px 1px rgba(0, 0, 0, 0.14),\n              0 3px 14px 2px rgba(0, 0, 0, 0.12);\n}\n.cdk-drag-placeholder {\n  opacity: 0;\n}\n.cdk-drag-animating {\n  transition: -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n.example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\n  transition: -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n.intervals{\n  width: 100%;\n}\n.working {\n  border-left: 3px #787c7a solid;\n  padding: 4px 6px;\n  background: #fff;\n  border-right: none;\n}\n.lunch {\n  border-left: 3px #787c7a solid;\n  padding: 4px 6px;\n  background: #ffc550;\n  border-right: none;\n}\n/* .lunch::after{ content:''; border-left: 3px #787c7a solid; height: 100%; width: 3px; position: absolute; top: 0; left: 0;} */\n.work-lunch {\n  \n  padding: 4px 6px;\n  background: #fff;\n  border-right: none;position: relative;\n}\n/* .work-lunch::after{ content:''; border-left: 3px #787c7a solid; height: 100%; width: 3px; position: absolute; top: 0; left: 0;} */\n.not-working {\n  border-left: 3px #787c7a solid;\n  padding: 4px 6px;\n  background: #83c5de;\n  border-right: none;\n}\n"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"holidays-main-tab\">\n  <div class=\"field-accordion\">\n    <div class=\"panel-group\" id=\"accordion\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse_workschedule\">\n              <div class=\"resumes-field-heading\">\n                <p>Position Work Schedules</p>\n              </div>\n            </a>\n          </h4>\n          <div class=\"main-punches-filters\">\n\n            <ng-template #template1>\n              <div class=\"modal-header modal-margintop\">\n                <h4 class=\"modal-title pull-left\">Add / Edit Position Schedule</h4>\n                <div class=\"clearfix\"></div>\n              </div>\n              <div class=\"modal-body\">\n             \n                <div class=\"container\">\n                  <div class=\"row\">\n\n                    <div class=\"tabbable tabs-left sidenav-tabs-tabs\">\n\n                      <div class=\" sidenav-tabcontent col-md-11\">\n                        <div class=\"tab-pane fade in active\" id=\"workSchedule\">\n\n                          <div class=\"second-popup-body\">\n\n                         <form (ngSubmit)=\"createWorkPosition(createEmployeeForm)\" #createEmployeeForm=\"ngForm\">\n                            \n                            <span class=\"second-work-schedule\">\n                              <ul class=\"list-unstyled hire-date-list\">\n                                <li class=\"list-items form-group work-title\">\n                                  <label>Position Schedule Title</label>\n                                  <input type=\"text\" name=\"nameoftemplate\" class=\"template mar-top-10\" [(ngModel)]=\"positionSchedule.scheduleTitle\" name=\"title\" #title=\"ngModel\" required\n                                  placeholder=\"Schedule Title\">\n                                  <p *ngIf=\"!positionSchedule.scheduleTitle && title.touched || ( !positionSchedule.scheduleTitle && isValid)\" class=\"error\">Enter schedule title</p>\n                                </li>\n                                <li class=\"list-items hiredates-list work-title\">\n                                  <label>Position Schedule Start Date</label>\n                                  <div class=\"date1\">\n                                    <input matInput [matDatepicker]=\"picker5\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)]=\"positionSchedule.scheduleStartDate\"\n                                    name=\"startdate\" #startDate=\"ngModel\" required>\n                                    <mat-datepicker #picker5></mat-datepicker>\n                                    <button mat-raised-button (click)=\"picker5.open()\">\n                                      <span>\n                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                      </span>\n                                    </button>\n                                    <div class=\"clearfix\"></div>\n                                  </div>\n                                <p *ngIf=\"!positionSchedule.scheduleStartDate && startDate.touched || (!positionSchedule.scheduleStartDate && isValid)\" class=\"error\">Enter the start date</p>\n\n                                </li>\n                                <li class=\"list-items hiredates-list work-title\">\n                                  <label>Schedule Type </label>\n                                  <input type=\"text\"  class=\"template mar-top-10\" [(ngModel)]=\"positionSchedule.type\" name=\"type\" #type=\"ngModel\" required\n                                  placeholder=\"Schedule Title\" [disabled]=true>\n                                  <!-- <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"positionSchedule.type\" name=\"type\" #type=\"ngModel\" required\n                                  placeholder=\"Schedule type\">\n                                    <mat-option value=\"Standard\">\n                                      Standard\n                                    </mat-option>\n                                    <mat-option value=\"Employee Specific\">\n                                      Employee Specific\n                                    </mat-option>\n    \n                                  </mat-select> -->\n                              <p *ngIf=\"!positionSchedule.type && type.touched || (!positionSchedule.type && isValid)\" class=\"error\">Select schedule type</p>\n\n                                </li>\n\n                              </ul>\n                            </span>\n                            <div>\n                            <h4 style=\"font-size:17px;\">Assign Position Schedule</h4>\n                              <!-- <div class=\"schdule-type\">\n                                <label>\n                                  Assign Schedule during New Hire setup?\n                                </label>\n                                <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"positionSchedule.assign_hire\" name=\"assign_hire\"\n                                placeholder=\"Assign schedule setup\">\n                                  <mat-option [value]=\"true\">\n                                    Yes\n                                  </mat-option>\n                                  <mat-option [value]=\"false\">\n                                    No\n                                  </mat-option>\n  \n                                </mat-select>\n                              </div>\n                             \n                              <div class=\"schdule-type\">\n                                <label>\n                                  Assign to a Job Title\n                                </label>\n                                <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"positionSchedule.assign_job_title\" name=\"assign_job_title\"\n                                placeholder=\"Assign title\">\n                                  <mat-option [value]=\"true\">\n                                    Yes\n                                  </mat-option>\n                                  <mat-option [value]=\"false\">\n                                    No\n                                  </mat-option>\n                                </mat-select>\n                              </div>\n                              <div class=\"schdule-type\">\n                                <label>\n                                 Manually Assign to Individuals?\n                                </label>\n                                <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"positionSchedule.manually_assign\" name=\"manually_assign\"\n                                placeholder=\"Manually assign\">\n                                  <mat-option [value]=\"true\">\n                                 Yes\n                                  </mat-option>\n                                  <mat-option [value]=\"false\">\n                                    No\n                                  </mat-option>\n                                </mat-select>\n                              </div> --> \n                              <div class=\"schdule-type\" *ngIf=\"jobTitleShow\">\n                                  <label>\n                                    Job Title Name\n                                  </label>\n                                  <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"positionSchedule.jobTitle\" name=\"jobTitle\"\n                                  placeholder=\"Job title\">\n                                      <mat-option *ngFor = \"let job of allJobTitles; let i=index\" [value]=\"allJobTitles[i]._id\" >\n                                          {{job.name}}\n                                        </mat-option>\n                                  </mat-select>\n                                </div>\n                            </div>\n                            <hr class=\"zenwork-margin-ten-zero\">\n                            <div class=\"select-week-days\">\n                              <h4>Select Days of the Week Applicable</h4>\n                              <!-- <mat-checkbox [(ngModel)]=\"positionSchedule.selectFewDays\" name=\"selectFewDays\" (change)=\"selectAll($event)\">Monday-Friday</mat-checkbox> -->\n                              <ul class=\"list-unstyled selected-days\" *ngFor = \"let day of days;let i=index\">\n                                <li class=\"list-days\">  \n                                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"days[i].isChecked\" name=a{{i}} (ngModelChange)=\"selectDay($event, day.name, day.isChecked)\">\n                                    {{day.name}} \n                                 </mat-checkbox>\n                                 \n                                </li>\n                   \n                              </ul>\n                              \n                            </div>\n                            <hr class=\"zenwork-margin-ten-zero\">\n                            <div class=\"emp-hours-working\">\n                              <ul class=\"list-unstyled work-schedule padding-left-10\">\n                                <li class=\"list-items\">\n                                  <label>Total Hours per Day</label>\n                                  <input type=\"text\" class=\"form-control benefits-dropdown mar-top-10\" [(ngModel)]=\"positionSchedule.totalHrsPerDay\" (ngModelChange)=\"totalHoursDay($event)\" name=\"total_hrs_day\" #total_hrs_day=\"ngModel\" required\n                                  placeholder=\"Total hours day\" (keypress)=\"numbers($event)\">\n                                  <p *ngIf=\"!positionSchedule.totalHrsPerDay && total_hrs_day.touched || (!positionSchedule.totalHrsPerDay && isValid)\" class=\"error\">Enter total hours day</p>\n\n                                </li>\n                                <li class=\"list-items\">\n                                  <label>Total Hours per Week</label>\n                                  <input type=\"text\" class=\"form-control benefits-dropdown mar-top-10\" [(ngModel)]=\"positionSchedule.totalHrsPerWeek\" name=\"week\" #week=\"ngModel\" required disabled\n                                  placeholder=\"Total hours week\">\n                                  <p *ngIf=\"!positionSchedule.totalHrsPerWeek && week.touched || (!positionSchedule.totalHrsPerWeek && isValid)\" class=\"error\">Enter total hours week</p>\n\n                                </li>\n                                <li class=\"list-items\" *ngIf=\"jobTitleShow\">\n                                    <label>\n                                        Set Lunch Break Time?\n                                      </label>\n                                      <mat-select class=\"form-control zenwork-input mar-top-10\"  [(ngModel)]=\"positionSchedule.lunchBreakTime\" name=\"lunch_break_time\" #lunch_break_time=\"ngModel\"\n                                      placeholder=\"Lunch break time\" (ngModelChange)=\"setLunchTime($event)\">\n                                        <mat-option [value]='true'>\n                                          Yes\n                                        </mat-option>\n                                        <mat-option [value]='false'>\n                                          No\n                                        </mat-option>\n      \n                                      </mat-select>\n                               <!-- <p *ngIf=\"(!positionSchedule.lunchBreakTime && lunch_break_time.touched) || (!positionSchedule.lunchBreakTime && isValid)\" class=\"error\">Select lunch break time</p> -->\n\n                                </li>\n                              </ul>\n                            </div>\n                   \n                            <div class=\"standard-schedule-table\">\n                           \n                              <div class=\"fist-popup-modal\">\n                                <div class=\"custom-tables\">\n                                  <table class=\"table\">\n                                    <thead>\n                                      <tr>\n                                        \n                                        <th>Day</th>\n\n                                        <th>Start Time</th>\n                                        <th *ngIf=\"lunchTime && jobTitleShow\">Lunch Start Time</th>\n                                        <th *ngIf=\"lunchTime && jobTitleShow\">Lunch End Time</th>\n                                        <th>End Time</th>\n                                        <!-- <th>\n                                         <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n\n                                            <mat-menu #menu1=\"matMenu\">\n                                \n                                                <button mat-menu-item> Edit </button>\n                        \n                                              </mat-menu>\n                                        </th> -->\n                                      </tr>\n                                    </thead>\n                                    <tbody>\n\n                                      <tr *ngFor = \"let day of weekDays;  let j=index;\">\n                                        \n                                        <td>\n                                             {{day.day}}\n                                        </td>\n                                       \n                                        <td class=\"start-time\">\n                                            <input class=\"emp-time\" [ngxTimepicker]=\"i\"  name=\"b{{j}}\" [(ngModel)]=\"weekDays[j].shiftStartTime\" (ngModelChange)=\"timeStartDay($event,j)\">\n                                            <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                                            <ngx-material-timepicker #i class=\"time-picker-color\"></ngx-material-timepicker>\n    \n                                        </td>\n                                        <td class=\"start-time\" *ngIf=\"lunchTime && jobTitleShow\">\n                                            <input class=\"emp-time\" [ngxTimepicker]=\"ii\"  name=\"c{{j}}\" [(ngModel)]=\"weekDays[j].lunchStartTime\">\n                                          <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                                           <ngx-material-timepicker #ii class=\"time-picker-color\"></ngx-material-timepicker>\n  \n                                        </td>\n                                        <td class=\"end-time\" *ngIf=\"lunchTime && jobTitleShow\">\n                                            <input class=\"emp-time\" [ngxTimepicker]=\"iii\"  name=\"d{{j}}\" [(ngModel)]=\"weekDays[j].lunchEndTime\">\n                                            <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                                            <ngx-material-timepicker #iii class=\"time-picker-color\"></ngx-material-timepicker>\n\n                                        </td>\n                                        <td class=\"end-time\">\n                                            <input class=\"emp-time\" [ngxTimepicker]=\"iv\"  name=\"e{{j}}\" [(ngModel)]=\"weekDays[j].shiftEndTime\" (ngModelChange)=\"endTimeDays($event,day)\">\n                                            <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                                            <ngx-material-timepicker #iv class=\"time-picker-color\"></ngx-material-timepicker>\n                                      \n                                        </td>\n                                        \n                                      </tr>\n                       \n                                    </tbody>\n                                  </table>\n                                  <hr class=\"zenwork-margin-ten-zero\">\n                                </div>\n                              </div>\n                              <!-- <h4 style=\"margin-top:30px;\">Preview Employee List Report\n                                  <button class=\"import-date-btn pull-right text-report\">Run Report</button>\n\n                                </h4>\n                              <div class=\"custom-tables group-list\">\n                                  <table class=\"table\">\n                                    <thead>\n                                      <tr>\n                                        <th>\n                                          Employee Name\n                                        </th>\n                                        <th>Employee ID</th>\n                                        <th>Manager Name</th>\n                                        <th>Department</th>\n                                        <th>Effective Date</th>\n                                        <th>Position Schedule Title</th>\n\n                                      </tr>\n                                    </thead>\n                                    <tbody>\n                                      <tr>\n                                        <td>Zenwork</td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td>12/01/2018</td>\n                                        <td></td>\n                                      </tr>\n                                      <tr>\n                                        <td>Test</td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                      </tr>\n                                      <tr>\n                                        <td>Test 1</td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                      </tr>\n                                  </table>\n                                  \n                                </div>\n                                <mat-paginator [length]=\"10\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" >\n                                </mat-paginator> -->\n                                <!-- <hr class=\"zenwork-margin-ten-zero\"> -->\n                                <div class=\"row\" style=\"margin:30px 0px;\">\n  \n                                    <button mat-button (click)=\"positionCancel()\">Cancel</button>\n            \n                                    <button mat-button class=\"submit-btn pull-right\">Submit</button>\n                                  </div>\n                                  <div class=\"clearfix\"></div>\n                               \n                            </div>\n                          </form>\n                          </div>\n                        </div>\n\n\n                   \n                      </div>\n                      <div class=\"clearfix\"></div>\n                    </div>\n\n                  </div>\n                </div>\n              </div>\n\n            </ng-template>\n          </div>\n\n        </div>\n\n        <div id=\"collapse_workschedule\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n            <div class=\"custom-tables\">\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>\n                    </th>\n                    <th>Work Schedule Title</th>\n                    <th>Schedule Type</th>\n                    <th>Days of Week</th>\n                    <th>Hours Per Day</th>\n                    <th>Assigned Employees</th>\n                    <th>\n                        <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                        <mat-menu #menu1=\"matMenu\">\n                          <button *ngIf=\"!checkPostionIds.length >=1\" mat-menu-item (click)=\"openModalWithClass1(template1)\"> Add  </button>\n                          <button *ngIf=\"(!checkPostionIds.length <=0) && (checkPostionIds.length ==1)\" mat-menu-item (click)=\"editWorkPosition(template1)\"> \n                             Edit \n                          </button>\n                          <button *ngIf=\"(!checkPostionIds.length <=0) && (checkPostionIds.length >=1)\" mat-menu-item (click)=\"deleteWorkPosition()\"> Delete </button>\n        \n                        </mat-menu>\n                    </th>\n                  </tr>\n                </thead>\n                <tbody>\n\n                  <tr *ngFor=\"let position of allPositions\">\n                    <td [ngClass]=\"{'zenwork-checked-border': position.isChecked}\">\n                        <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"position.isChecked\" (change)=\"selectWorkPosition($event, position._id)\">\n                     \n                          </mat-checkbox>\n                    </td>\n                    <td>{{position.scheduleTitle}}</td>\n                    <td>{{position.type}}</td>\n                    <td>\n                      {{position.daysOfWeek}}\n                    </td>\n                    <td>\n                      {{position.totalHrsPerDay}}\n                    </td>\n                    <td>\n                      \n                      <span class=\"excel-sheet-icon\" *ngIf=\"position.employeeIds.length\">\n                        {{position.employeeIds.length}} \n                        <!-- <img alt=\"img\" height=\"23\" src=\"../../../../assets/images/leave-management/excel.png\" width=\"25\"> -->\n                      </span>\n                    </td>\n                  \n                    <td>\n\n                    </td>\n                  </tr>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"field-accordion\">\n    <div class=\"panel-group\" id=\"accordion\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse_Departmentworkschedule\">\n              <div class=\"resumes-field-heading\">\n                <p>Department Work Schedule</p>\n              </div>\n            </a>\n          </h4>\n          <div class=\"main-punches-filters\">\n      \n            <ng-template #template>\n              <div class=\"modal-header modal-margintop\">\n                <h4 class=\"modal-title pull-left\">Add / Edit Department Work Schedule - Details</h4>\n                <div class=\"clearfix\"></div>\n              </div>\n              <div class=\"modal-body\">\n                <!-- sidenavbar starts -->\n                \n                    <!-- tabs -->\n                    <div class=\"tabbable tabs-left sidenav-tabs-tabs\">\n                      \n                      <ul class=\"nav nav-tabs col-md-3\">\n                        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'defineTeam' }\">\n                          <a href=\"#defineTeam\" data-toggle=\"tab\" #define_dept (click)=\"chooseFieldss('defineTeam')\">Define Department\n                            Work Schedule\n\n                            <span [ngClass]=\"{'caret': selectedNav == 'defineTeam' }\"></span>\n                          </a>\n                        </li>\n                        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'selectEmployees' }\">\n                          <a href=\"#selectEmployees\" data-toggle=\"tab\" #select_employees (click)=\"chooseFieldss('selectEmployees')\">Select Employees\n                            \n                            <span [ngClass]=\"{'caret': selectedNav == 'selectEmployees' }\"></span>\n                          </a>\n                        </li>\n                        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'scheduleBuilder' }\">\n                          <a href=\"#scheduleBuilder\" data-toggle=\"tab\" #schedule_builder (click)=\"chooseFieldss('scheduleBuilder')\">Schedule Builder\n                            \n                            <span [ngClass]=\"{'caret': selectedNav == 'scheduleBuilder' }\"></span>\n                          </a>\n                        </li>\n                        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'recap2' }\">\n                          <a href=\"#recap2\" data-toggle=\"tab\" #recap_all (click)=\"chooseFieldss('recap2')\">Recap\n                            <span [ngClass]=\"{'caret': selectedNav == 'recap2' }\"></span>\n                          </a>\n                        </li>\n                      </ul>\n\n                      <div class=\"tab-content sidenav-right col-md-9\">\n                        <div class=\"tab-pane fade in active\" id=\"defineTeam\" [ngClass]=\"{'active':selectedNav === 'defineTeam','in':selectedNav==='defineTeam'}\">\n\n                          <div class=\"second-popup-body\">\n\n\n                            <span class=\"work-schedule-title\">\n                              <ul class=\"list-unstyled hire-date-list\">\n                                <li class=\"list-items form-group work-title\">\n                                  <label>Department Work Schedule Name</label><br>\n                                  <input type=\"text\" name=\"nameoftemplate\" class=\"template mar-top-10\" [(ngModel)]=\"departmentSchedule.departmentScheduleName\">\n                                </li>\n                              </ul>\n                            </span>\n                          \n                            <div class=\"open-close-time\">\n                              <ul class=\"list-unstyled\">\n                                <li class=\"list-items\">\n                                  <label>Open Time</label>\n                                  <input class=\"emp-open-time mar-top-10\" [ngxTimepicker]=\"v\"  [(ngModel)]=\"departmentSchedule.openTime\" (ngModelChange)=\"editTime($event)\">\n                                  <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                                  <ngx-material-timepicker #v class=\"time-picker-color\"></ngx-material-timepicker>\n                                 \n                                </li>\n                                <li class=\"list-items\">\n                                  <label>Close Time</label>\n                                  <input class=\"emp-open-time mar-top-10\" [ngxTimepicker]=\"vi\"  [(ngModel)]=\"departmentSchedule.closeTime\" (ngModelChange)=\"editTime($event)\">\n                                  <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                                  <ngx-material-timepicker #vi class=\"time-picker-color\"></ngx-material-timepicker>\n                            \n                                </li>\n                              </ul>\n                            </div>\n\n                            <div class=\"form-group schdule-type\">\n                              <label>\n                                Interval Definition\n                              </label>\n                              <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"departmentSchedule.intervalSpan\" (ngModelChange)=\"changeInterval($event)\">\n                                <mat-option [value]=\"30\">\n                                  30 Min\n                                </mat-option>\n                                <mat-option [value]=\"60\">\n                                  60 Min\n                                </mat-option>\n                                <mat-option [value]=\"90\">\n                                  90 Min\n                                </mat-option>\n                              </mat-select>\n                            </div>\n                            <div class=\"standard-schedule-table\">\n                              <ul class=\"list-unstyled\">\n                                <li class=\"list-items form-group schdule-type\">\n                                  <label>\n                                    Resource Type\n                                  </label>\n                                  <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"departmentSchedule.resourceType\" (ngModelChange)=\"typeOfResource($event)\">\n                                    <mat-option value=\"Dynamic\">\n                                      Dynamic\n                                    </mat-option>\n                                    <mat-option value=\"Standard\">\n                                        Standard\n                                    </mat-option>\n                                  </mat-select>\n                                </li>\n                                <li class=\"define-resources list-items\" *ngIf=\"dynamicResourceHide\">\n                                  <button class=\"btn btn-define\" (click)=\"openDynamicResourceModel()\">Define Dynamic Resources</button>\n                                </li>\n                              </ul>\n                            </div>\n                            <div class=\"form-group schdule-type\" *ngIf=\"standardResourceHide\">\n                              <label>\n                                Standard Number of Resources Needed Per Interval\n                              </label>\n                              <mat-select class=\"form-control zenwork-input mar-top-10\" [(ngModel)]=\"departmentSchedule.standardNoOfResourcesPerInterval\" (ngModelChange)=\"standardResourceInterval($event)\">\n                                <mat-option [value]=\"11\">\n                                  11\n                                </mat-option>\n                                <mat-option [value]=\"10\">\n                                  10\n                                </mat-option>\n                              </mat-select>\n                            </div>\n                            <div class=\"select-week-days row\">\n\n                              <div class=\"col-xs-5\">\n                                  <h5>Select Days of the Week Applicable</h5>\n                                  <ul class=\"list-unstyled selected-days\" *ngFor = \"let day of days2\">\n                                    <li class=\"list-items\">\n                                      <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"day.isChecked\" (ngModelChange)=\"DepartmentSelectDay($event, day.name, day.isChecked)\">\n                                        {{day.name}}\n                                     </mat-checkbox>\n                                    </li>\n                       \n                                  </ul>\n                             \n                              </div>\n                              <div class=\"education-dates col-xs-7\">\n                                <h4 style=\"font-size:16px;\">Apply Department Work Schedule Date Range</h4>\n                                  <ul class=\"list-unstyled education-date\">\n                                    <li class=\"education-date-list1 padding10-listitems\">\n                                      <label>Start Date</label><br>\n                                      <div class=\"date1\">\n                                       \n                                          <input matInput [matDatepicker]=\"picker3\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"departmentSchedule.startDate\"\n                                          [max]=\"departmentSchedule.endDate\">\n                                          <mat-datepicker #picker3></mat-datepicker>\n                                          <button mat-raised-button (click)=\"picker3.open()\">\n                                            <span>\n                                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                            </span>\n                                          </button>\n                                        \n                                        </div>\n                                    </li>\n                                    <li class=\"education-date-list1 padding10-listitems\">\n                                      <label>End Date</label><br>\n                                      <div class=\"date1\">\n                                       \n                                          <input matInput [matDatepicker]=\"picker4\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"departmentSchedule.endDate\"\n                                          [min]=\"departmentSchedule.startDate\">\n                                          <mat-datepicker #picker4></mat-datepicker>\n                                          <button mat-raised-button (click)=\"picker4.open()\">\n                                            <span>\n                                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                            </span>\n                                          </button>\n                                        \n                                        </div>\n                                    </li>\n                                  </ul>\n                                </div>\n                              \n                            </div>\n                            <hr class=\"zenwork-margin-ten-zero\">\n                            <div class=\"row\" style=\"margin:30px 0px;\">\n  \n                                <button mat-button (click)=\"decline()\">Cancel</button>\n        \n                                <button mat-button class=\"submit-btn pull-right\" (click)=\"clickSelectEmployees()\">Proceed</button>\n                              </div>\n                              <div class=\"clearfix\"></div>\n\n                          </div>\n                        </div>\n  \n                        <div class=\"tab-pane\" id=\"selectEmployees\" [ngClass]=\"{'active':selectedNav === 'selectEmployees','in':selectedNav==='selectEmployees'}\">\n                          <div class=\"select-employee-tab\">\n                          \n                            <div class=\"Eligiblity-criteria-tab\">\n                              <div class=\"field-accordion\">\n                                <div class=\"panel-group\" id=\"accordion\">\n                                  <div class=\"panel panel-default eligibility-panel\">\n                                    <div class=\"panel-heading  eligibility-panel-heading\">\n                                      <h4 class=\"panel-title\">\n                                        <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\"\n                                          href=\"#eligibility-criteria\">\n                                          <div class=\"resumes-field-heading\">\n                                            <p>Eligibility Criteria</p>\n                                          </div>\n                                        </a>\n                                      </h4>\n                                      <!-- <div class=\"main-punches-filters eligibility-criteria-select-all\">\n                                        <ul class=\"list-unstyled submit-buttons search-category\">\n                                          <li class=\"list-items\">\n                                            <span class=\"checkbox checkbox-success checkbox_allemployees\">\n                                              <input id=\"checkbox_allemployees\" type=\"checkbox\" class=\"team-checkbox\">\n                                              <label for=\"checkbox_allemployees\">Select All Emplyees</label>\n                                            </span>\n                                          </li>\n                                        </ul>\n                                      </div> -->\n                                    </div>\n                                    <div id=\"eligibility-criteria\" class=\"panel-collapse collapse in\">\n                                      <div class=\"panel-body eligibility-criteria-panel-body employees-data\">\n                                        <div class=\"col-sm-12 margin-btm\">\n                                            <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                                                <ul class=\"list-unstyled structure-list\" *ngFor=\"let fields of structureFields\">\n                      \n                                                  <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                                                    <div>\n                      \n                                                      <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"fields.isChecked\"\n                                                        [value]=\"fields.name\"\n                                                        (ngModelChange)=\"eligibilityArray = [];getStructureSubFields(fields.name,fields.isChecked )\">\n                                                        {{fields.name}}\n                                                      </mat-checkbox>\n                                                    </div>\n                      \n                                                  </li>\n                      \n                                                </ul>\n                                              </div>\n                                              <div class=\"col-sm-1 text-center arrow-position\">\n                                                  <i class=\"material-icons arrow-align-one\">\n                                                    keyboard_arrow_right\n                                                  </i>\n                                                  <i class=\"material-icons arrow-align-two\">\n                                                    keyboard_arrow_right\n                                                  </i>\n                                                </div>\n                                                <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                                                    <div *ngIf=\"StructureValues\">\n                                                      <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of StructureValues\">\n                          \n                                                        <li class=\"list-items-values\">\n                                                          <mat-checkbox class=\"checkbox-success\" \n                                                            (change)=\"subFieldsStructure($event,values.name)\" [(ngModel)]=\"values.isChecked\" [value]=\"values.name\">\n                                                            {{values.name}}\n                                                          </mat-checkbox>\n                          \n                                                        </li>\n                          \n                                                      </ul>\n                          \n                                                    </div>\n                                                    <div *ngIf=\"customValues\">\n                                                      <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of customValues\">\n                          \n                                                        <li class=\"list-items-values\">\n                                                          <mat-checkbox class=\"checkbox-success\"\n                                                            (change)=\"subFieldsStructure($event,values.name)\" [(ngModel)]=\"values.isChecked\" [value]=\"values.name\">\n                                                            {{values.name}}\n                                                          </mat-checkbox>\n                                                        </li>\n                          \n                                                      </ul>\n                          \n                                                    </div>\n                                                  </div>\n                                         \n                                        </div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>\n                              </div>\n                            </div>\n                            <hr class=\"zenwork-margin-ten-zero\">\n                            <div class=\"specific-employee\">\n                              <div class=\"field-accordion\">\n                                <div class=\"panel-group\" id=\"accordion\">\n                                  <div class=\"panel panel-default eligibility-panel\">\n                                    <div class=\"panel-heading  eligibility-panel-heading\">\n                                      <h4 class=\"panel-title\">\n                                        <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\"\n                                          href=\"#specific-employee\">\n                                          <div class=\"resumes-field-heading\">\n                                            <p> Specific Employee</p>\n                                          </div>\n                                        </a>\n                                      </h4>\n                                    </div>\n                                    <div id=\"specific-employee\" class=\"panel-collapse collapse in\">\n                                      <div class=\"panel-body\">\n                                        <!-- <div class=\"include-specific-person col-md-6\">\n                                            <h4>Include a Specific Person</h4>\n                                            <ng-multiselect-dropdown [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                                              [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                                              (onSelectAll)=\"onSelectAll($event)\">\n                                            </ng-multiselect-dropdown>\n                                        </div>\n                                        <div class=\"include-specific-person col-md-6\">\n                                            <h4>Exclude a Specific Person</h4>\n                                          <ng-multiselect-dropdown [placeholder]=\"'custom placeholder'\"\n                                            [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                                            [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                                            (onSelectAll)=\"onSelectAll($event)\">\n                                          </ng-multiselect-dropdown>\n                                        </div> -->\n                                        <div>\n                                            <h5 style=\"float: left;\">Include a Specific Person</h5>\n                                            <h5 style=\"float: right;\">Exclude a Specific Person</h5>\n                                            <div class=\"clearfix\"></div>\n                                            <dual-list [source]=\"dropdownList\" [(destination)]=\"selectedUsers\" [format]=\"format\" filter=true\n                                            height='300px' (click)=\"onSelectUsers(selectedUsers)\">\n                                          </dual-list>\n                                          </div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>\n                              </div>\n                            </div>\n                            <h4 style=\"margin-top:30px; font-size:16px;\">Preview Employee List Report\n                                <button class=\"import-date-btn pull-right text-report\">Run Report</button>\n\n                            </h4>\n                            <div class=\"custom-tables group-list\">\n                                <table class=\"table\">\n                                  <thead>\n                                    <tr>\n                                      <th>\n                                        Employee Name\n                                      </th>\n                                      <th>Employee ID</th>\n                                      <th>Manager Name</th>\n                                      <th>Department</th>\n                                      <th>Effective Date</th>\n                                      <th>Department Work Schedule Name</th>\n\n                                    </tr>\n                                  </thead>\n                                  <tbody>\n                                      <tr *ngFor=\"let list of previewList\">\n                                          <td>{{list.personal.name.firstName}}</td>\n                                          <td>{{list.job.employeeId}}</td>\n                                          <td>\n                                            <span *ngIf=\"list.job.ReportsTo\">\n                                                {{list.job.ReportsTo.personal.name.firstName}}\n                                            </span>\n                                            \n                                          </td>\n                                          <td>{{list.job.department}}</td>\n                                          <td></td>\n                                          <td></td>\n                                        </tr>\n\n                                </table>\n                                \n                              </div>\n                              <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" >\n                              </mat-paginator>\n                              <hr class=\"zenwork-margin-ten-zero\">\n                              <div class=\"row\" style=\"margin:30px 0px;\">\n                                 <button mat-button class=\"back-btn\" (click)=\"goToDept()\">Back</button>\n                                  \n                                  <button mat-button (click)=\"decline()\">Cancel</button>\n          \n                                  <button mat-button class=\"submit-btn pull-right\" (click)=\"clickSchedulerBuilder()\">Proceed</button>\n                                </div>\n                                <div class=\"clearfix\"></div>\n                            \n                          </div>\n                        </div>\n                        <div class=\"tab-pane\" id=\"scheduleBuilder\" [ngClass]=\"{'active':selectedNav === 'scheduleBuilder','in':selectedNav==='scheduleBuilder'}\">\n                          <div class=\"row\">\n                              <ul class=\"list-unstyled col-md-12\">\n                                  <li class=\"list-items col-md-3 schedule-creation\">\n                                      <label>Schedule Creation Date</label><br>\n                                  <input type=\"text\" name=\"nameoftemplate\" class=\"template mar-top-10 dept-date\" [(ngModel)]=\"departmentCreationDate\" [disabled]=\"true\">\n\n                                      <!-- <div class=\"date1\" style=\"margin-bottom: 12px;\">\n                                         \n                                            <input matInput [matDatepicker]=\"picker\" placeholder=\"mm/dd/yy\" class=\"form-control\" >\n                                            <mat-datepicker #picker></mat-datepicker>\n                                            <button mat-raised-button (click)=\"picker.open()\">\n                                              <span>\n                                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                              </span>\n                                            </button>\n                                          \n                                          </div> -->\n                                  </li>\n                                  <li class=\"list-items col-md-3 schedule-creation\">\n                                    <label>\n                                      Resource Type\n                                    </label><br>\n                                    <mat-select class=\"form-control zenwork-input mar-top-5\" [(ngModel)]=\"departmentSchedule.resourceType\">\n                                      <mat-option value=\"Dynamic\">\n                                          Dynamic\n                                      </mat-option>\n                                      <mat-option value=\"Standard\">\n                                          Standard\n                                      </mat-option>\n                                    </mat-select>\n                                  </li>\n                                  </ul>\n                          </div>\n                            \n                              <!-- <div class=\"form-group schdule-type\">\n                                 \n                                </div> -->\n                              <div class=\"open-close-time row\">\n                                  <ul class=\"list-unstyled col-md-12\">\n                                    <li class=\"list-items\">\n                                        <label>Schedule Start Date</label><br>\n                                        <div class=\"date1\">\n                                          \n                                              <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"departmentSchedule.startDate\"\n                                              [max]=\"departmentSchedule.endDate\">\n                                              <mat-datepicker #picker1></mat-datepicker>\n                                              <button mat-raised-button (click)=\"picker1.open()\">\n                                                <span>\n                                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                                </span>\n                                              </button>\n                                              <!-- <div class=\"clearfix\"></div> -->\n                                            </div>\n                                    </li>\n                                    <li class=\"list-items\">\n                                        <label>Schedule End Date</label><br>\n                                        <div class=\"date1\">\n                                           \n                                              <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"departmentSchedule.endDate\"\n                                              [min]=\"departmentSchedule.startDate\">\n                                              <mat-datepicker #picker2></mat-datepicker>\n                                              <button mat-raised-button (click)=\"picker2.open()\">\n                                                <span>\n                                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                                </span>\n                                              </button>\n                                              <!-- <div class=\"clearfix\"></div> -->\n                                            </div>\n                                    </li>\n                                  </ul>\n                                </div>\n\n                                <div class=\"pull-right col-md-2 days-right\">\n                                   \n                                    <mat-select class=\"form-control zenwork-input mar-top-10\" placeholder=\"Day\" [(ngModel)]=\"departmentDay\">\n                                        \n                                        <mat-option *ngFor=\"let day of departmentSelectDays; let i=index;\" [value]=\"day\" (click)=\"dayWiseEmployeeStatus(day)\">\n                                            {{day}}\n                                          </mat-option>\n                                        <!-- <mat-option *ngFor=\"let day of uniqueDays; let i=index;\" [value]=\"day\" (click)=\"dayWiseEmployeeStatus(day)\">\n                                          {{day}}\n                                        </mat-option> -->\n                                 \n                                      </mat-select>\n                                      \n                                </div>\n                                <div class=\"clearfix\"></div>\n                                \n                                <div class=\"recap-table\">\n                                    <table class=\"table\" style=\"margin-bottom:0px;\">\n                                      <thead>\n                                        <tr>\n                                  \n                                          <th>EE Name</th>\n                                          <th>Schedule Name</th>\n                                          <th>Schedule Type</th>\n                                          <th>Start Time</th>\n                                          <th>Lunch</th>\n                                          <th>End Time</th>\n  \n                                        </tr>\n                                      </thead>\n                                      <tbody>\n                    \n                                        <tr *ngFor=\"let employee of employeeList\">\n                                          <td>{{employee.personal.name.preferredName}}</td>\n                                          <td>\n                                           <span *ngIf=\"employee.position_work_schedule_id\">{{employee.position_work_schedule_id.scheduleTitle}}</span> \n                                          </td>\n                                          <td>\n                                           <span *ngIf=\"employee.position_work_schedule_id\">{{employee.position_work_schedule_id.type}}</span> \n\n                                          </td>\n                                          <td>\n                                              <span *ngIf=\"employee.position_work_schedule_id.time\">{{employee.position_work_schedule_id.time.shiftStartTime}}</span>\n\n                                          </td>\n                                          <td>\n                                              <span *ngIf=\"employee.position_work_schedule_id.time\">{{employee.position_work_schedule_id.time.lunchStartTime}}</span>\n\n                                          </td>\n                                          <td>\n                                              <span *ngIf=\"employee.position_work_schedule_id.time\">{{employee.position_work_schedule_id.time.shiftEndTime}}</span>\n\n                                          </td>\n                                          \n                    \n                                        </tr>\n                            \n        \n                                    </table>\n                                    <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                                      </mat-paginator>\n                                    </div>\n                            <h4>Schedule Builder</h4>\n\n                            <div class=\"intervals\">\n                                <div class=\"recap-table1\">\n         \n                                    <table class=\"table\" style=\"margin-bottom:0px;\">\n                                     <thead>\n                                       <tr>\n   \n                                         <th># of Resource Needed</th>\n                                         <th *ngFor=\"let interval of timeIntervals;\">{{interval.resourcesNeeded}}</th>\n                                         \n   \n                                       </tr>\n                                     </thead>\n                                     <tbody>\n                   \n                                       <tr>\n                                         <td>EE Name</td>\n                                        <td *ngFor=\"let interval of timeIntervals;\">{{interval.scheduledTime}}</td>\n                                         \n                                       </tr>\n                                       \n                                           <tr *ngFor=\"let employee of employeeList; let i=index;\" cdkDropList cdkDropListOrientation=\"horizontal\" (cdkDropListDropped)=\"drop($event,i)\">\n                                               \n                                               <td>\n                                                 {{employee.personal.name.preferredName}}</td>\n                                               <td class=\"horizontal-box\" *ngFor=\"let status of employee.status\" cdkDrag style=\"cursor: pointer;\">\n                                                 <span class=\"example-box\" [ngClass]=\"{'working': status == 'Working', 'lunch': status == 'Lunch', 'work-lunch': status == 'Working/Lunch', 'not-working': status == 'Not Working'}\"> {{status}}</span>\n                                                \n                                               </td>\n                         \n                                             </tr>\n                                             \n                                               <tr>\n                                 \n                                                   <td># of Resource Available</td>\n                                                   <td *ngFor=\"let interval of timeIntervals; let j=index;\">\n                                                     {{interval.resourceAvailable}}\n                                                   </td>\n   \n             \n                                                 </tr>\n                                        \n       \n                                   </table> \n                                 \n                               </div>\n                               <div style=\"margin-top:10px;\">\n                                  <button mat-button class=\"submit-btn pull-right\" (click)=\"dayWiseSchedules(employeeList)\">Save</button>\n    \n                              </div>\n                            </div>\n                         \n                               \n                          <hr style=\"margin-top:60px;\">\n                          <div class=\"row\" style=\"margin:30px 0px;\">\n                             <button mat-button class=\"back-btn\" (click)=\"clickSelectEmployees()\">Back</button>\n                              \n                              <button mat-button (click)=\"decline()\">Cancel</button>\n      \n                              <button *ngIf=\"departmentEmployeesScheduleInfo.length\" mat-button class=\"submit-btn pull-right\" (click)=\"recapTab()\">Proceed</button>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                        </div>\n                        <div class=\"tab-pane\" id=\"recap2\" [ngClass]=\"{'active':selectedNav === 'recap2','in':selectedNav==='recap2'}\">\n                          <hr class=\"zenwork-margin-ten-zero\">\n                          <div class=\"row\" style=\"margin:30px 0px;\">\n                             <button mat-button class=\"back-btn\" (click)=\"clickSchedulerBuilder()\">Back</button>\n                              \n                              <button mat-button (click)=\"decline()\">Cancel</button>\n      \n                              <button mat-button class=\"submit-btn pull-right\" (click)=\"createDepartmentSchedule()\">Submit</button>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                        </div>\n\n                      </div>\n                      <div class=\"clearfix\"></div>\n                    </div>\n\n                    <!-- /tabs -->                 \n               </div>\n\n            </ng-template>\n          </div>\n        </div>\n        <div id=\"collapse_Departmentworkschedule\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n            <div class=\"custom-tables\">\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>\n                   \n                    </th>\n                    <th>Work Schedule Title</th>\n                    <th>Resource Type</th>\n                    <th>Days of Week</th>\n                    <th>Hours Per Day</th>\n                    <th>Assigned Employees</th>\n                   \n                    <th>\n                        <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n                        <mat-menu #menu2=\"matMenu\">\n                \n                          <button *ngIf=\"!checkDeptScheduleIds.length >=1\" mat-menu-item (click)=\"openModalWithClass(template)\"> Add </button>\n                          <button *ngIf=\"(!checkDeptScheduleIds.length <=0) && (checkDeptScheduleIds.length ==1)\" mat-menu-item (click)=\"editDepartmentSchedule(template)\"> Edit </button>\n                          <button *ngIf=\"(!checkDeptScheduleIds.length <=0) && (checkDeptScheduleIds.length >=1)\" mat-menu-item (click)=\"deleteDepartmentSchedule()\"> Delete </button>\n        \n                        </mat-menu>\n                    </th>\n                  </tr>\n                </thead>\n                <tbody>\n\n                  <tr *ngFor=\"let department of allDepartments\">\n                    <td [ngClass]=\"{'zenwork-checked-border': department.isChecked}\">\n                      <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"department.isChecked\" (change)=\"checkedDeptSchedules($event, department._id)\">\n                   \n                        </mat-checkbox>\n                  </td>\n                    <td>{{department.departmentScheduleName}}</td>\n                    <td>{{department.resourceType}}</td>\n                    <td> {{department.daysOfWeekApplicable}}</td>\n                   \n                    <td>\n                      {{department.totalHrsPerDay}}\n                    </td>\n                    <td>\n                      {{department.assignedEmployeesCount}}\n                      <!-- <span class=\"excel-sheet-icon\">\n                        <img alt=\"img\" height=\"23\" src=\"../../../../assets/images/leave-management/excel.png\" width=\"25\">\n                      </span> -->\n                    </td>\n                  \n                    <td>\n\n                    </td>\n                  </tr>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: WorkSchedulesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkSchedulesComponent", function() { return WorkSchedulesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var amazing_time_picker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! amazing-time-picker */ "./node_modules/amazing-time-picker/amazing-time-picker.es5.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _dynamic_resource_model_dynamic_resource_model_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./dynamic-resource-model/dynamic-resource-model.component */ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var WorkSchedulesComponent = /** @class */ (function () {
    function WorkSchedulesComponent(modalService, leaveManagementService, atp, accessLocalStorageService, siteAccessRolesService, swalAlertService, myInfoService, companySettingsService, dialog, fb) {
        this.modalService = modalService;
        this.leaveManagementService = leaveManagementService;
        this.atp = atp;
        this.accessLocalStorageService = accessLocalStorageService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.swalAlertService = swalAlertService;
        this.myInfoService = myInfoService;
        this.companySettingsService = companySettingsService;
        this.dialog = dialog;
        this.fb = fb;
        this.workSchedule = true;
        this.assignWork = false;
        this.recap = false;
        //secoond-popup variables start
        this.defineTeam = true;
        this.applyData = false;
        this.selectEmployees = false;
        this.scheduleBuilder = false;
        this.recap2 = false;
        this.dropdownList = [];
        this.dropdownSettings = {};
        this.selectedItems = [];
        this.weekDays = [];
        this.allPositions = [];
        this.employeeData = [];
        this.employeeIds = [];
        this.selectedPostions = [];
        this.selectedDays = [];
        this.departmentSelectDays = [];
        this.allSelectedDepartmentDays = [];
        this.disableFiled = true;
        this.customJobTitles = [];
        this.defaultJobTitles = [];
        this.allJobTitles = [];
        this.allSelectedDays = [];
        this.timeIntervals = [];
        this.assignedEmployees = [];
        this.isValid = false;
        this.checkPostionIds = [];
        this.lunchTime = false;
        this.jobTitleShow = true;
        this.positionSchedule = {
            scheduleTitle: '',
            scheduleStartDate: '',
            type: 'Standard',
            jobTitle: '',
            totalHrsPerDay: '',
            totalHrsPerWeek: '',
            lunchBreakTime: '',
        };
        this.StructureValues = [];
        this.customValues = [];
        this.eligibilityArray = [];
        this.selectedUsers = [];
        this.structureFields = [];
        this.allDepartments = [];
        this.selectedDepartments = [];
        this.schedulesStatus = [];
        this.employeeList = [];
        this.scheduleStatus = [];
        this.getDepartmentSchedule = {
            Open_Time: ''
        };
        this.intervalStatus = [];
        this.status = [];
        this.schedule_Builder = [];
        this.uniqueDays = [];
        this.employeeStatusList = [];
        this.getemployeeList = [];
        this.previewList = [];
        this.dynamicResourceHide = false;
        this.standardResourceHide = false;
        this.departmentEmployeesScheduleInfo = [];
        this.checkDeptScheduleIds = [];
        this.dayWiseList = [];
        this.usersList = [];
        this.departmentSchedule = {
            departmentScheduleName: '',
            openTime: '',
            closeTime: '',
            intervalSpan: '',
            resourceType: '',
            standardNoOfResourcesPerInterval: '',
            startDate: '',
            endDate: '',
        };
        this.days = [
            { name: 'Monday', isChecked: false },
            { name: 'Tuesday', isChecked: false },
            { name: 'Wednesday', isChecked: false },
            { name: 'Thursday', isChecked: false },
            { name: 'Friday', isChecked: false },
            { name: 'Saturday', isChecked: false },
            { name: 'Sunday', isChecked: false },
        ];
        this.days2 = [
            { name: 'Monday', isChecked: false },
            { name: 'Tuesday', isChecked: false },
            { name: 'Wednesday', isChecked: false },
            { name: 'Thursday', isChecked: false },
            { name: 'Friday', isChecked: false },
            { name: 'Saturday', isChecked: false },
            { name: 'Sunday', isChecked: false },
        ];
        this.format = {
            add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
            direction: angular_dual_listbox__WEBPACK_IMPORTED_MODULE_13__["DualListComponent"].LTR, draggable: true, locale: 'da',
        };
        this.weekDayNames = [];
    }
    // Author: Saiprakash G, Date: 17/06/19
    // Add Time picker
    WorkSchedulesComponent.prototype.open = function (event) {
        console.log(event);
        var amazingTimePicker = this.atp.open();
        amazingTimePicker.afterClose().subscribe(function (time) {
            console.log(time);
        });
    };
    WorkSchedulesComponent.prototype.openModalWithClass1 = function (template1) {
        this.selectedNav = 'workSchedule';
        this.modalRef = this.modalService.show(template1, Object.assign({}, { class: 'gray modal-lg' }));
    };
    WorkSchedulesComponent.prototype.openModalWithClass = function (template1) {
        this.selectedNav = 'defineTeam';
        this.modalRef = this.modalService.show(template1, Object.assign({}, { class: 'gray modal-lg' }));
    };
    WorkSchedulesComponent.prototype.confirm = function () {
        this.message = 'Confirmed!';
        this.modalRef.hide();
    };
    WorkSchedulesComponent.prototype.decline = function () {
        var _this = this;
        this.message = 'Declined!';
        this.modalRef.hide();
        this.departmentSchedule = '';
        this.getAllDepartmentSchedules();
        this.checkDeptScheduleIds = [];
        this.selectedUsers = [];
        this.employeeList = [];
        var index = this.departmentSelectDays.map(function (e) {
            var i = _this.days2.findIndex(function (ele) { return ele.name === e; });
            if (i >= 0) {
                _this.days2[i].isChecked = false;
            }
        });
        this.getDepartmentSchedule = "";
        this.departmentSelectDays = [];
        console.log(this.departmentSelectDays);
    };
    WorkSchedulesComponent.prototype.ngOnInit = function () {
        this.departmentCreationDate = new Date().toLocaleDateString();
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.getStandardAndCustomStructureFields();
        this.getAllWorkPostions();
        if (this.getDepartmentSchedule.eligibility_criteria && this.getDepartmentSchedule.eligibility_criteria.selected_field) {
            this.getStructureSubFields(this.getDepartmentSchedule.eligibility_criteria.selected_field, true, this.getDepartmentSchedule.eligibility_criteria.sub_fields);
        }
        this.getAllEmp();
        this.getFieldsForRoles();
        this.getAllDepartmentSchedules();
        // this.dropdownSettings = {
        //   singleSelection: false,
        //   idField: 'item_id',
        //   textField: 'item_text',
        //   selectAllText: 'Select All',
        //   unSelectAllText: 'UnSelect All',
        //   itemsShowLimit: 0,
        //   maxHeight: 150,
        //   defaultOpen: true,
        //   allowSearchFilter: true
        // };
    };
    WorkSchedulesComponent.prototype.positionCancel = function () {
        var _this = this;
        this.message = 'Declined!';
        this.modalRef.hide();
        this.positionSchedule = {
            scheduleTitle: '',
            scheduleStartDate: '',
            jobTitle: '',
            totalHrsPerDay: '',
            totalHrsPerWeek: '',
            lunchBreakTime: ''
        };
        // this.isValid = false;
        this.weekDays = [];
        var index = this.getPositionScheduler.daysOfWeek.map(function (e) {
            var i = _this.days.findIndex(function (ele) { return ele.name === e; });
            console.log(i);
            if (i >= 0) {
                console.log('days', _this.days, _this.days[i]);
                _this.days[i].isChecked = false;
            }
        });
        this.checkPostionIds = [];
        this.selectedDays = [];
        this.getPositionScheduler = "";
        this.getAllWorkPostions();
        console.log(this.selectedDays);
    };
    WorkSchedulesComponent.prototype.setLunchTime = function (event) {
        console.log(event);
        if (event == true) {
            this.lunchTime = true;
        }
        else {
            this.lunchTime = false;
        }
    };
    // Author:Saiprakash, Date:20/06/2019
    // Get Standard And Custom Structure Fields
    WorkSchedulesComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customJobTitles = _this.getAllStructureFields['Job Title'].custom;
            _this.defaultJobTitles = _this.getAllStructureFields['Job Title'].default;
            _this.allJobTitles = _this.customJobTitles.concat(_this.defaultJobTitles);
        });
    };
    // async selectAll(e?) {
    //   console.log(e);
    //   this.selectedDays = [];
    //   this.days.forEach(element => {
    //     element.isChecked = this.positionSchedule.selectFewDays;
    //   });
    //   await this.days.forEach(element => {
    //     if (element.isChecked == true) {
    //       this.selectedDays.push(element.name );
    //     }
    //   });
    //   console.log(this.selectedDays);
    // }
    // async selectDay(event, day, data) {
    //   this.selectedDays = [];
    //   if (data) {
    //     this.dayCheck = 0;
    //     this.days.forEach(element => {
    //       if (!element.isChecked) {
    //         this.dayCheck = 1;
    //       }
    //     });
    //     if (!this.dayCheck) {
    //       this.positionSchedule.selectFewDays = true;
    //     }
    //   }
    //   else {
    //     this.positionSchedule.selectFewDays = false;
    //   }
    //   await this.days.forEach(element => {
    //     if (element.isChecked) {
    //       this.selectedDays.push(element.name);
    //     }
    //   });
    //   console.log(this.selectedDays);
    // }
    // Author: Saiprakash G, Date: 17/06/19
    // Select week days
    WorkSchedulesComponent.prototype.selectDay = function (event, day, isChecked) {
        console.log(event, day);
        this.selectDayOfWeek = day;
        if (event == true) {
            if (this.selectedDays) {
                this.selectedDays.push(day);
            }
            this.weekDays.push({
                day: day,
                shiftStartTime: '',
                lunchStartTime: '',
                lunchEndTime: '',
                shiftEndTime: '',
            });
        }
        else if (event == false) {
            for (var i = 0; i < this.selectedDays.length; i++) {
                if (this.selectedDays[i] === day) {
                    this.selectedDays.splice(i, 1);
                }
            }
            for (var i = 0; i < this.weekDays.length; i++) {
                if (this.weekDays[i].day === day) {
                    this.weekDays.splice(i, 1);
                }
            }
        }
        console.log(this.weekDays);
        console.log(this.selectedDays);
        if (this.getPositionScheduler && this.getPositionScheduler.daysOfWeek) {
            // this.allSelectedDays = this.selectedDays.concat(this.getPositionScheduler.daysOfWeek)
            this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * this.getPositionScheduler.totalHrsPerDay;
        }
        if (this.totalDayEvent) {
            this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * this.totalDayEvent;
        }
        // console.log(this.allSelectedDays); 
    };
    // Author: Saiprakash G, Date: 31/07/19
    // totalHoursDay
    WorkSchedulesComponent.prototype.totalHoursDay = function (event) {
        console.log("dayyyyyyyyy", event);
        this.totalDayEvent = event;
        if (this.selectedDays.length) {
            var num = event;
            var min, hrs;
            this.positionSchedule.totalHrsPerWeek = [];
            this.weekDays.shiftStartTime = [];
            this.weekDays.shiftEndTime = [];
            if (num) {
                min = num.split(':');
            }
            // console.log(min[0], "minutes afcter");
            hrs = min[0] * 60;
            if (min[1] && min[1] !== '') {
                console.log(min, "minutes afcter");
                hrs = parseInt(hrs) + parseInt(min[1]);
                console.log("hrssssss", hrs);
            }
            this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * hrs / 60;
            this.weekDays.shiftStartTime = [];
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select days of the week applicable", "Error");
        }
    };
    // Author: Saiprakash G, Date: 31/07/19
    // timeStartDay
    WorkSchedulesComponent.prototype.timeStartDay = function (event, i) {
        console.log("time starttt", event);
        this.startTimeValue = event;
    };
    // Author: Saiprakash G, Date: 31/07/19
    // endTimeDays
    WorkSchedulesComponent.prototype.endTimeDays = function (event, i) {
        var clearTime = i;
        console.log("endddddd", event, i);
        console.log("weekday iiiiii", this.startTimeValue);
        var t0 = this.startTimeValue;
        console.log("check t00000", t0);
        var t1 = event;
        console.log("t00000", t0, t1);
        var diff = this.getTimeDifference(t0, t1);
        console.log("timeeee difffff", diff);
        var d = parseInt(diff.split(':')[0]);
        var p = parseInt(this.positionSchedule.totalHrsPerDay);
        console.log(d, typeof d, typeof d);
        console.log(d > p || d < p);
        if (d !== p) {
            console.log(this.weekDays);
            clearTime.shiftEndTime = null;
            this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Please check the correct time duration", "error");
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Verify time duration successfully", "success");
        }
    };
    WorkSchedulesComponent.prototype.timeStringToMins = function (s) {
        s = s.split(':');
        s[0] = /m$/i.test(s[1]) && s[0] == 12 ? 0 : s[0];
        return s[0] * 60 + parseInt(s[1]) + (/pm$/i.test(s[1]) ? 720 : 0);
    };
    // Return difference between two times in hh:mm[am/pm] format as hh:mm
    WorkSchedulesComponent.prototype.getTimeDifference = function (t0, t1) {
        // Small helper function to padd single digits
        function z(n) { return (n < 10 ? '0' : '') + n; }
        // Get difference in minutes
        var diff = this.timeStringToMins(t1) - this.timeStringToMins(t0);
        // Format difference as hh:mm and return
        return z(diff / 60 | 0) + ':' + z(diff % 60);
    };
    WorkSchedulesComponent.prototype.DepartmentSelectDay = function (event, day, isChecked) {
        if (isChecked == true) {
            if (this.departmentSelectDays) {
                this.departmentSelectDays.push(day);
            }
        }
        else if (isChecked == false) {
            for (var i = 0; i < this.departmentSelectDays.length; i++) {
                if (this.departmentSelectDays[i] === day) {
                    this.departmentSelectDays.splice(i, 1);
                }
            }
        }
        console.log(this.departmentSelectDays);
        // if(this.getDepartmentSchedule.Days_Week_Applicable){
        //   this.allSelectedDepartmentDays = this.departmentSelectDays.concat(this.getDepartmentSchedule.Days_Week_Applicable)
        // }
    };
    // Author: Saiprakash G, Date: 18/06/19
    // Select schedule type for get employess list
    // selectScheduleType(event){
    //   console.log(event);
    //   if(event == 'Standard'){
    //     var data = {
    //       name: ''
    //     }
    //     this.siteAccessRolesService.getAllEmployees(data)
    //       .subscribe((res: any) => {
    //         console.log("employees", res);
    //         this.employeeData = res.data;
    //         this.employeeData.forEach(element => {
    //           this.employeeIds.push(element._id)
    //         });
    //       console.log(this.employeeIds);
    //       },
    //         (err) => {
    //           console.log(err);
    //         })
    //   }
    // }
    // Author: Saiprakash G, Date: 17/06/19
    // Create work postion scheduler
    WorkSchedulesComponent.prototype.createWorkPosition = function (createEmployeeForm) {
        var _this = this;
        var data = {
            scheduleTitle: this.positionSchedule.scheduleTitle,
            scheduleStartDate: this.positionSchedule.scheduleStartDate,
            type: this.positionSchedule.type,
            jobTitle: this.positionSchedule.jobTitle,
            daysOfWeek: this.selectedDays,
            weekApplicable: this.weekDays,
            totalHrsPerDay: this.positionSchedule.totalHrsPerDay,
            totalHrsPerWeek: this.positionSchedule.totalHrsPerWeek,
            lunchBreakTime: this.positionSchedule.lunchBreakTime
        };
        if (!this.positionSchedule.scheduleTitle) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.scheduleStartDate) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.type) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.totalHrsPerDay) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.totalHrsPerWeek) {
            this.isValid = true;
        }
        // else if (!this.positionSchedule.lunchBreakTime) {
        //   this.isValid = true;
        // }
        else if (this.getPositionScheduler && this.getPositionScheduler._id) {
            data._id = this.getPositionScheduler._id;
            //  data.daysOfWeek = this.allSelectedDays;
            this.leaveManagementService.updatePosition(data).subscribe(function (res) {
                console.log(res);
                _this.message = 'Confirmed!';
                _this.modalRef.hide();
                createEmployeeForm.resetForm();
                _this.positionSchedule = {
                    scheduleTitle: '',
                    scheduleStartDate: '',
                    jobTitle: '',
                    // type: '',
                    totalHrsPerDay: '',
                    totalHrsPerWeek: '',
                    lunchBreakTime: ''
                };
                _this.isValid = false;
                _this.weekDays = [];
                _this.selectedDays = [];
                _this.checkPostionIds = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getAllWorkPostions();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.leaveManagementService.createPosition(data).subscribe(function (res) {
                console.log(res);
                _this.message = 'Confirmed!';
                _this.modalRef.hide();
                createEmployeeForm.resetForm();
                _this.positionSchedule = {
                    scheduleTitle: '',
                    scheduleStartDate: '',
                    jobTitle: '',
                    // type: '',
                    totalHrsPerDay: '',
                    totalHrsPerWeek: '',
                    lunchBreakTime: ''
                };
                _this.isValid = false;
                _this.weekDays = [];
                _this.selectedDays = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getAllWorkPostions();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 18/06/19
    // Get all position work scheduler
    WorkSchedulesComponent.prototype.getAllWorkPostions = function () {
        var _this = this;
        this.leaveManagementService.getAllPosition().subscribe(function (res) {
            console.log(res);
            _this.allPositions = res.data;
            _this.positionSchedule.type = "Standard";
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    WorkSchedulesComponent.prototype.selectWorkPosition = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkPostionIds.push(id);
        }
        console.log(this.checkPostionIds);
        if (event.checked == false) {
            var index = this.checkPostionIds.indexOf(id);
            if (index > -1) {
                this.checkPostionIds.splice(index, 1);
            }
            console.log(this.checkPostionIds);
        }
    };
    // Author: Saiprakash G, Date: 18/06/19
    // Get position work scheduler
    WorkSchedulesComponent.prototype.editWorkPosition = function (template1) {
        var _this = this;
        this.selectedPostions = this.allPositions.filter(function (positionCheck) {
            return positionCheck.isChecked;
        });
        if (this.selectedPostions.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one position work schedule to proceed", "", 'error');
        }
        else if (this.selectedPostions.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one position work schedule to proceed", "", 'error');
        }
        else {
            this.selectedNav = 'workSchedule';
            this.modalRef = this.modalService.show(template1, Object.assign({}, { class: 'gray modal-lg' }));
            this.leaveManagementService.getPosition(this.selectedPostions[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getPositionScheduler = res.data;
                console.log(_this.getPositionScheduler);
                _this.positionSchedule = _this.getPositionScheduler;
                _this.weekDays = _this.getPositionScheduler.weekApplicable;
                _this.positionSchedule.jobTitle = _this.getPositionScheduler.jobTitle;
                console.log(_this.positionSchedule.jobTitle);
                if (_this.positionSchedule.type == 'Employee Specific') {
                    _this.jobTitleShow = false;
                }
                else {
                    _this.jobTitleShow = true;
                }
                if (_this.positionSchedule.lunchBreakTime == true) {
                    _this.lunchTime = true;
                }
                else {
                    _this.lunchTime = false;
                }
                //  this.weekDays.filter(item=>{
                //   item.edit= true;
                // });
                if (_this.getPositionScheduler.daysOfWeek && _this.getPositionScheduler.daysOfWeek.length) {
                    console.log(_this.getPositionScheduler.daysOfWeek);
                    _this.selectedDays = _this.getPositionScheduler.daysOfWeek;
                    var index = _this.getPositionScheduler.daysOfWeek.map(function (e) {
                        var i = _this.days.findIndex(function (ele) { return ele.name === e; });
                        console.log(i);
                        if (i >= 0) {
                            console.log('days', _this.days, _this.days[i]);
                            _this.days[i].isChecked = true;
                        }
                    });
                    // this.allSelectedDays = this.selectedDays.concat(this.getPositionScheduler.daysOfWeek);
                }
                // console.log(this.allSelectedDays);
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 19/06/19
    // Delete position work scheduler
    WorkSchedulesComponent.prototype.deleteWorkPosition = function () {
        var _this = this;
        this.selectedPostions = this.allPositions.filter(function (postionCheck) {
            return postionCheck.isChecked;
        });
        if (this.selectedPostions.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one position work schedule to proceed", "", 'error');
        }
        else {
            var data = {
                _id: this.selectedPostions
            };
            this.leaveManagementService.deletePosition(data).subscribe(function (res) {
                console.log(res);
                _this.checkPostionIds = [];
                _this.getAllWorkPostions();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 24/06/19,
    // Description: Open dynamic resource model
    WorkSchedulesComponent.prototype.openDynamicResourceModel = function () {
        var _this = this;
        var dynamicData = {
            Open_Time: this.departmentSchedule.openTime,
            Close_Time: this.departmentSchedule.closeTime,
            Interval_Definition: this.departmentSchedule.intervalSpan,
            resourceInfo: this.getDepartmentSchedule
        };
        var dialogRef = this.dialog.open(_dynamic_resource_model_dynamic_resource_model_component__WEBPACK_IMPORTED_MODULE_10__["DynamicResourceModelComponent"], {
            width: '1200px',
            height: '700px',
            data: dynamicData
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
            if (result) {
                _this.timeIntervals = result.Intervals;
                console.log(_this.timeIntervals);
            }
        });
    };
    WorkSchedulesComponent.prototype.standardResourceInterval = function (value) {
        var _this = this;
        console.log(value);
        var openTimeMS = this.timeToMs(this.timeConvertor(this.departmentSchedule.openTime));
        console.log(openTimeMS);
        var closeTimeMS = this.timeToMs(this.timeConvertor(this.departmentSchedule.closeTime));
        console.log(closeTimeMS);
        var intervalDiff = (this.departmentSchedule.intervalSpan * 60) * 1000;
        console.log(intervalDiff);
        for (var i = openTimeMS; i < closeTimeMS; i = i + intervalDiff) {
            if (i < closeTimeMS) {
                this.timeIntervals.push({
                    scheduledTime: this.msToTime(i),
                    resourcesNeeded: value,
                });
            }
            for (var j = 0; j < this.timeIntervals.length; j++) {
                this.timeIntervals[j].interval = j + 1;
            }
            console.log(this.timeIntervals);
        }
        var resourceNeeded = [];
        if (this.getDepartmentSchedule && this.getDepartmentSchedule.intervalResourcesInformation) {
            console.log(this.getDepartmentSchedule);
            for (var j = 0; j < this.timeIntervals.length; j++) {
                this.getDepartmentSchedule.intervalResourcesInformation.forEach(function (element) {
                    resourceNeeded.push(element.resourcesNeeded);
                    _this.timeIntervals[j].resourcesNeeded = resourceNeeded[j];
                    _this.timeIntervals[j].interval = j + 1;
                });
                console.log(this.timeIntervals);
            }
        }
    };
    WorkSchedulesComponent.prototype.msToTime = function (i) {
        this.minutes = Math.floor((i / (1000 * 60)) % 60);
        this.hours = Math.floor((i / (1000 * 60 * 60)) % 24);
        this.hours = (this.hours < 10) ? "0" + this.hours : this.hours;
        this.minutes = (this.minutes < 10) ? "0" + this.minutes : this.minutes;
        var time = ['', ':', '', ' ', ''];
        if (this.hours < 12) {
            time[4] = 'AM';
        }
        else {
            time[4] = 'PM';
        }
        time[2] = String(this.minutes);
        if (time[2].length == 1) {
            time[2] = '0' + time[2];
        }
        time[0] = this.hours % 12 || 12;
        console.log(time.join(""));
        return time.join("");
        // return this.hours+":"+this.minutes;
    };
    WorkSchedulesComponent.prototype.typeOfResource = function (event) {
        console.log(event);
        if (event == "Dynamic") {
            this.dynamicResourceHide = true;
            this.standardResourceHide = false;
        }
        else {
            this.dynamicResourceHide = false;
            this.standardResourceHide = true;
        }
    };
    WorkSchedulesComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.previewEmployeeList();
    };
    WorkSchedulesComponent.prototype.getAllEmp = function () {
        var _this = this;
        var listallUsers;
        var postData = {
            name: ''
        };
        console.log("data comes");
        this.siteAccessRolesService.getAllEmployees(postData)
            .subscribe(function (res) {
            console.log("resonse for all employees", res);
            listallUsers = res.data;
            for (var i = 0; i < listallUsers.length; i++) {
                _this.dropdownList.push({ _id: listallUsers[i]._id, _name: listallUsers[i].personal.name.preferredName });
            }
            console.log("DropDownList", _this.dropdownList);
        }, function (err) {
            console.log(err);
        });
    };
    WorkSchedulesComponent.prototype.previewEmployeeList = function () {
        var _this = this;
        var finalPreviewUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalPreviewUsers.push(element._id);
        });
        var data = {
            per_page: this.perPage,
            page_no: this.pageNo,
            _ids: finalPreviewUsers
        };
        this.companySettingsService.prviewEmployeeList(data).subscribe(function (res) {
            console.log(res);
            _this.previewList = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 25/06/19,
    // Slect and deselect employees for include and exclude
    // onItemSelect(item: any) {
    //   this.selectedUsers = []
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   for (var i = 0; i < this.selectedItems.length; i++) {
    //     this.selectedUsers.push(this.selectedItems[i].item_id);
    //   }
    //   console.log('selcetd users', this.selectedUsers);
    // }
    // OnItemDeSelect(item: any) {
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
    //   console.log('selcetd users', this.selectedUsers);
    // }
    // onSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   for (var i = 0; i < items.length; i++) {
    //     this.selectedUsers.push(items[i].item_id);
    //   }
    //   console.log("users", this.selectedUsers);
    // }
    // onDeSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   console.log('selcetd users', this.selectedUsers);
    // }
    // Author: Saiprakash G, Date: 24/06/19,
    // Description: Get stricture fields for roles
    WorkSchedulesComponent.prototype.getFieldsForRoles = function () {
        var _this = this;
        this.companySettingsService.getFieldsForRoles()
            .subscribe(function (res) {
            console.log("12122123", res);
            _this.structureFields = res.data;
            _this.structureFields[0].isChecked = false;
            if (_this.getDepartmentSchedule.eligibility_criteria && _this.getDepartmentSchedule.eligibility_criteria.selected_field) {
                var index = _this.structureFields.findIndex(function (e) { return e.name === _this.getDepartmentSchedule.eligibility_criteria.selected_field; });
                console.log(index, 'index');
                if (index >= 0) {
                    _this.structureFields[index].isChecked = true;
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    WorkSchedulesComponent.prototype.resourceAvailableCount = function () {
        for (var k = 0; k < this.timeIntervals.length; k++) {
            console.log(k);
            var count = 0;
            this.employeeList.forEach(function (data) {
                if (data.status[k] == 'Working' || data.status[k] == 'Working/Lunch') {
                    count++;
                }
            });
            this.timeIntervals[k].resourceAvailable = count;
        }
    };
    // Author: Saiprakash G, Date: 24/06/19,
    // Description: Get structure sub fields
    WorkSchedulesComponent.prototype.getStructureSubFields = function (fields, isChecked, sub_fields) {
        var _this = this;
        console.log(fields, isChecked);
        this.fieldsName = fields;
        // this.eligibilityArray = [];
        this.checked = isChecked;
        if (isChecked == true) {
            this.structureFields.forEach(function (element) {
                if (element.name !== fields) {
                    element.isChecked = false;
                }
            });
            this.companySettingsService.getStuctureFields(fields, 0)
                .subscribe(function (res) {
                _this.EmployeeListSummary();
                console.log(res, name);
                _this.StructureValues = res.default;
                _this.customValues = res.custom;
                console.log(_this.StructureValues);
                if (sub_fields && sub_fields.length) {
                    sub_fields.forEach(function (element) {
                        var structureIndex = _this.StructureValues.findIndex(function (e) { return e.name === element; });
                        var customValuesIndex = _this.customValues.findIndex(function (e) { return e.name === element; });
                        if (structureIndex >= 0) {
                            _this.StructureValues[structureIndex].isChecked = true;
                        }
                        if (customValuesIndex >= 0) {
                            _this.customValues[customValuesIndex].isChecked = true;
                        }
                    });
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Sub structure fields
    WorkSchedulesComponent.prototype.subFieldsStructure = function ($event, value) {
        console.log($event, value);
        // this.eventName = $event;
        // this.eligibilityArray = [];
        if ($event.checked == true) {
            this.eligibilityArray.push(value);
        }
        console.log(this.eligibilityArray, "123321");
        if ($event.checked == false) {
            var index = this.eligibilityArray.indexOf(value);
            if (index > -1) {
                this.eligibilityArray.splice(index, 1);
            }
            console.log(this.eligibilityArray);
        }
        this.EmployeeListSummary();
    };
    // Author: Saiprakash G, Date: 25/06/19,
    // Description: Employee list summary
    WorkSchedulesComponent.prototype.EmployeeListSummary = function () {
        var _this = this;
        var postdata = {
            field: this.fieldsName,
            chooseEmployeesForRoles: this.eligibilityArray
        };
        console.log(postdata);
        this.siteAccessRolesService.chooseEmployeesData(postdata)
            .subscribe(function (res) {
            console.log("emp data", res);
            _this.dropdownList = [];
            for (var i = 0; i < res.data.length; i++) {
                // if (this.eventName.checked == true){
                _this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName });
                // }
            }
            console.log(_this.dropdownList, "employee dropdown");
            var result = _this.selectedUsers.filter(function (item) { return (_this.dropdownList.findIndex(function (e) { return e.item_id === item; })) >= 0; });
            _this.selectedUsers = result;
            console.log(result, _this.selectedUsers);
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 21/06/19,
    // Description: Selected active tabs
    WorkSchedulesComponent.prototype.chooseFieldss = function (event) {
        this.selectedNav = event;
        if (event == "defineTeam") {
            this.defineTeam = true;
            this.selectEmployees = false;
            this.scheduleBuilder = false;
            this.recap2 = false;
        }
        else if (event == "selectEmployees") {
            this.defineTeam = false;
            this.selectEmployees = true;
            this.scheduleBuilder = false;
            this.recap2 = false;
        }
        else if (event == "scheduleBuilder") {
            this.defineTeam = false;
            this.selectEmployees = false;
            this.scheduleBuilder = true;
            this.recap2 = false;
            this.dayWiseEmployeeStatus(this.departmentSelectDays[0]);
        }
        else if (event == "recap2") {
            this.defineTeam = false;
            this.selectEmployees = false;
            this.scheduleBuilder = false;
            this.recap2 = true;
        }
    };
    WorkSchedulesComponent.prototype.clickSelectEmployees = function () {
        this.selectedNav = "selectEmployees";
    };
    // Author: Saiprakash G, Date: 24/07/19,
    // Description: Get position scheduler for scheduler builder tab
    WorkSchedulesComponent.prototype.clickSchedulerBuilder = function () {
        this.selectedNav = "scheduleBuilder";
        this.dayWiseEmployeeStatus(this.departmentSelectDays[0]);
    };
    // Author: Saiprakash G, Date: 24/07/19,
    // Description: Assigned status of employee for different time intervals
    WorkSchedulesComponent.prototype.statusOfEmployee = function (day) {
        var _this = this;
        this.employeeList.forEach(function (element) {
            // let ds_Obj:any={
            //   userId:element._id,
            //   day: this.departmentDay,
            //   intervalsInfo:[]
            // }
            element.timeIntervals = _this.timeIntervals;
            element.status = [];
            element.position_work_schedule_id.weekApplicable.forEach(function (days) {
                element.position_work_schedule_id.time = days;
                console.log(days);
                if (days.day == day) {
                    console.log(days.shiftStartTime, days.shiftEndTime, days.lunchStartTime);
                    _this.employeeStartTime = days.shiftStartTime;
                    _this.employeeLunchTime = days.lunchStartTime;
                    _this.employeeEndTmie = days.shiftEndTime;
                    if (days.shiftStartTime) {
                        _this.startTime = _this.timeToMs(_this.timeConvertor(days.shiftStartTime));
                    }
                    if (days.lunchStartTime) {
                        _this.lunchStartTime = _this.timeToMs(_this.timeConvertor(days.lunchStartTime));
                    }
                    if (days.lunchEndTime) {
                        _this.lunchEndTime = _this.timeToMs(_this.timeConvertor(days.lunchEndTime));
                    }
                    if (days.shiftEndTime) {
                        _this.endTime = _this.timeToMs(_this.timeConvertor(days.shiftEndTime));
                    }
                    console.log(_this.startTime, _this.endTime);
                }
            });
            element.timeIntervals.forEach(function (intervals) {
                intervals.resourceAvailable = _this.employeeList.length;
                _this.newTime = _this.timeToMs(_this.timeConvertor(intervals.scheduledTime));
                console.log(_this.newTime);
                if (_this.startTime > _this.newTime) {
                    element.status.push("Not Working");
                }
                else if (_this.newTime >= _this.startTime && _this.newTime < _this.lunchStartTime) {
                    element.status.push("Working");
                }
                else if (_this.newTime >= _this.lunchStartTime && _this.newTime < _this.lunchEndTime) {
                    if ((_this.lunchEndTime - _this.lunchStartTime) > 1800000) {
                        element.status.push("Lunch");
                    }
                    else if ((_this.lunchEndTime - _this.lunchStartTime) <= 1800000) {
                        element.status.push("Working/Lunch");
                    }
                }
                else if (_this.newTime >= _this.lunchEndTime && _this.newTime <= _this.endTime) {
                    element.status.push("Working");
                }
                else if (_this.endTime < _this.newTime) {
                    element.status.push("Not Working");
                }
                console.log(element.status);
            });
            // for(var n=0; n<this.timeIntervals.length; n++){
            //   let interval_status_info:any = {
            //     status :element.status[n],
            //    interval: this.timeIntervals[n].interval,
            //   scheduledTime : element.timeIntervals[n].scheduledTime
            //   }
            //   ds_Obj.intervalsInfo.push(interval_status_info)
            // }
            // this.departmentEmployeesScheduleInfo.push(ds_Obj)
        });
        console.log(this.employeeList);
        //  console.log(this.departmentEmployeesScheduleInfo);
        this.resourceAvailableCount();
    };
    // Author: Saiprakash G, Date: 25/07/19,
    // Description: Day wise employee status showed
    WorkSchedulesComponent.prototype.dayWiseEmployeeStatus = function (day) {
        console.log(this.weekDayNames);
        if (this.weekDayNames.length) {
            var dayindex = this.weekDayNames.findIndex(function (e) { return e.day == day; });
            console.log('dayindex', dayindex, this.weekDayNames);
            if (dayindex < 0) {
                this.getNewEmployList(day);
            }
            else {
                var oldEmploy = this.weekDayNames[dayindex].employList;
                this.employeeList = oldEmploy;
            }
        }
        else {
            this.getNewEmployList(day);
        }
    };
    WorkSchedulesComponent.prototype.getNewEmployList = function (day) {
        console.log(day);
        this.departmentDay = day;
        this.employeeList = [];
        if (this.getDepartmentSchedule && this.getDepartmentSchedule._id) {
            this.getUsersList();
        }
        else {
            for (var _i = 0, _a = this.previewList; _i < _a.length; _i++) {
                var employee = _a[_i];
                if (employee.position_work_schedule_id) {
                    if (employee.position_work_schedule_id.daysOfWeek.includes(day)) {
                        this.employeeList.push(employee);
                    }
                }
            }
            console.log(this.employeeList);
            this.statusOfEmployee(day);
        }
    };
    // Author: Saiprakash G, Date: 23/07/19,
    // Description: Converted time to milliseconds
    WorkSchedulesComponent.prototype.timeToMs = function (time) {
        var milliSec = Number(time.split(':')[0]) * 60 * 60 * 1000 + Number(time.split(':')[1]) * 60 * 1000;
        return milliSec;
    };
    WorkSchedulesComponent.prototype.timeConvertor = function (time) {
        time = time.toUpperCase().replace(' ', '');
        var PM = time.match('PM') ? true : false;
        time = time.split(':');
        var min = parseInt(time[1], 10);
        if (PM) {
            var hour = parseInt(time[0], 10);
            if (hour < 12) {
                hour += 12;
            }
            // var sec = time[2].replace('PM', '')
        }
        else {
            var hour = parseInt(time[0], 10);
            if (hour == 12) {
                hour = 0;
            }
            // var sec = time[2].replace('AM', '')       
        }
        // console.log(hour + ':' + min + ':' + sec)
        console.log(hour + ':' + min);
        return hour + ':' + min;
    };
    // Author: Saiprakash G, Date: 23/07/19,
    // Description: Drag and drop of employee status for particular time intervals
    WorkSchedulesComponent.prototype.drop = function (event, j) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_8__["moveItemInArray"])(this.employeeList[j].status, event.previousIndex, event.currentIndex);
        console.log("drag event", event, this.employeeList[j].status);
        this.resourceAvailableCount();
    };
    WorkSchedulesComponent.prototype.goToDept = function () {
        this.selectedNav = "defineTeam";
    };
    WorkSchedulesComponent.prototype.recapTab = function () {
        this.selectedNav = "recap2";
    };
    WorkSchedulesComponent.prototype.dayWiseSchedules = function (employList) {
        var _this = this;
        var emplist = employList;
        var daynames = {
            day: this.departmentDay,
            employList: emplist
        };
        var dayIndex = this.weekDayNames.findIndex(function (e) { return e.day == _this.departmentDay; });
        if (dayIndex < 0) {
            this.weekDayNames.push(daynames);
        }
        else {
            this.weekDayNames[dayIndex] = daynames;
        }
        console.log('weekDayNames', this.weekDayNames);
        if (this.getDepartmentSchedule && this.getDepartmentSchedule._id) {
            this.employeeList.forEach(function (element) {
                element.scheduleStatus = _this.scheduleStatus;
                for (var i = 0; i < element.scheduleStatus.length; i++) {
                    if (element.scheduleStatus[i].userId._id.indexOf(element._id) >= 0) {
                        var ds_Obj = {
                            userId: element._id,
                            _id: element.scheduleStatus[i]._id,
                            day: _this.departmentDay,
                            intervalsInfo: []
                        };
                        for (var n = 0; n < _this.timeIntervals.length; n++) {
                            var interval_status_info = {
                                status: element.status[n],
                                interval: _this.timeIntervals[n].interval,
                                scheduledTime: _this.timeIntervals[n].scheduledTime
                            };
                            ds_Obj.intervalsInfo.push(interval_status_info);
                        }
                        var userFilter = _this.departmentEmployeesScheduleInfo.filter(function (x) { return x.userId == element._id; });
                        var dayFilter = userFilter.filter(function (x) { return x.day == _this.departmentDay; });
                        if (!dayFilter[0]) {
                            _this.departmentEmployeesScheduleInfo.push(ds_Obj);
                        }
                        else {
                            var index = _this.departmentEmployeesScheduleInfo.indexOf(dayFilter[0]);
                            _this.departmentEmployeesScheduleInfo[index] = ds_Obj;
                            console.log(index);
                        }
                    }
                }
            });
        }
        else {
            this.employeeList.forEach(function (element) {
                var ds_Obj = {
                    userId: element._id,
                    day: _this.departmentDay,
                    intervalsInfo: []
                };
                for (var n = 0; n < _this.timeIntervals.length; n++) {
                    var interval_status_info = {
                        status: element.status[n],
                        interval: _this.timeIntervals[n].interval,
                        scheduledTime: _this.timeIntervals[n].scheduledTime
                    };
                    ds_Obj.intervalsInfo.push(interval_status_info);
                }
                var userFilter = _this.departmentEmployeesScheduleInfo.filter(function (x) { return x.userId == element._id; });
                var dayFilter = userFilter.filter(function (x) { return x.day == _this.departmentDay; });
                if (!dayFilter[0]) {
                    _this.departmentEmployeesScheduleInfo.push(ds_Obj);
                }
                else {
                    var index = _this.departmentEmployeesScheduleInfo.indexOf(dayFilter[0]);
                    _this.departmentEmployeesScheduleInfo[index] = ds_Obj;
                    console.log(index);
                }
            });
        }
        this.swalAlertService.SweetAlertWithoutConfirmation("Successfully saved day wise status", "", "success");
        console.log(this.departmentEmployeesScheduleInfo);
    };
    // Author: Saiprakash G, Date: 26/07/19,
    // Description: Create and update department work schedule
    WorkSchedulesComponent.prototype.createDepartmentSchedule = function () {
        var _this = this;
        var intervalResourcesInformation = [];
        // var departmentEmployeesScheduleInfo = [];
        for (var n = 0; n < this.timeIntervals.length; n++) {
            intervalResourcesInformation.push({
                interval: this.timeIntervals[n].interval,
                scheduledTime: this.timeIntervals[n].scheduledTime,
                resourcesNeeded: this.timeIntervals[n].resourcesNeeded,
                resourcesAvailable: this.timeIntervals[n].resourceAvailable
            });
        }
        //  this.employeeList.forEach(element => {
        //   let ds_Obj:any={
        //     userId:element._id,
        //     day: this.departmentDay,
        //     intervalsInfo:[]
        //   }
        //   for(var n=0; n<this.timeIntervals.length; n++){
        //     let interval_status_info:any = {
        //       status :element.status[n],
        //      interval: this.timeIntervals[n].interval,
        //     scheduledTime : element.timeIntervals[n].scheduledTime
        //     }
        //     ds_Obj.intervalsInfo.push(interval_status_info)
        //   }
        //  this.departmentEmployeesScheduleInfo.push(ds_Obj)
        // });
        console.log(intervalResourcesInformation);
        console.log(this.departmentEmployeesScheduleInfo);
        var data = {
            departmentScheduleName: this.departmentSchedule.departmentScheduleName,
            openTime: this.departmentSchedule.openTime,
            closeTime: this.departmentSchedule.closeTime,
            intervalSpan: this.departmentSchedule.intervalSpan,
            resourceType: this.departmentSchedule.resourceType,
            standardNoOfResourcesPerInterval: this.departmentSchedule.standardNoOfResourcesPerInterval,
            daysOfWeekApplicable: this.departmentSelectDays,
            startDate: this.departmentSchedule.startDate,
            endDate: this.departmentSchedule.endDate,
            intervalResourcesInformation: intervalResourcesInformation,
            departmentEmployeesScheduleInfo: this.departmentEmployeesScheduleInfo
        };
        if (this.getDepartmentSchedule && this.getDepartmentSchedule._id) {
            data._id = this.getDepartmentSchedule._id;
            // data.specific_employee.include = this.selectedItems
            this.leaveManagementService.updateDepartment(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getAllDepartmentSchedules();
                _this.modalRef.hide();
                _this.getDepartmentSchedule._id = "";
                _this.checkDeptScheduleIds = [];
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.leaveManagementService.createDepartment(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getAllDepartmentSchedules();
                _this.modalRef.hide();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 27/06/19,
    // Description: Get all department work schedules
    WorkSchedulesComponent.prototype.getAllDepartmentSchedules = function () {
        var _this = this;
        this.leaveManagementService.getAllDepartment().subscribe(function (res) {
            console.log(res);
            _this.allDepartments = res.result;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 27/06/19,
    // Description: Edit single department work schedule
    WorkSchedulesComponent.prototype.editDepartmentSchedule = function (template1) {
        var _this = this;
        this.selectedDepartments = this.allDepartments.filter(function (positionCheck) {
            return positionCheck.isChecked;
        });
        if (this.selectedDepartments.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one department work schedule to proceed", "", 'error');
        }
        else if (this.selectedDepartments.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one department work schedule to proceed", "", 'error');
        }
        else {
            this.selectedNav = 'defineTeam';
            this.modalRef = this.modalService.show(template1, Object.assign({}, { class: 'gray modal-lg' }));
            this.leaveManagementService.getDepartmnet(this.selectedDepartments[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getDepartmentSchedule = res.data.departmentSchedule;
                console.log(_this.getDepartmentSchedule);
                _this.departmentSchedule = _this.getDepartmentSchedule;
                if (_this.getDepartmentSchedule.resourceType == 'Dynamic') {
                    _this.dynamicResourceHide = true;
                    _this.standardResourceHide = false;
                }
                else {
                    _this.dynamicResourceHide = false;
                    _this.standardResourceHide = true;
                }
                _this.timeIntervals = _this.getDepartmentSchedule.intervalResourcesInformation;
                console.log(_this.timeIntervals);
                _this.scheduleStatus = res.data.departmentEmployeesScheduleInfo;
                _this.assignedEmployees = res.data.assignedEmployees;
                _this.departmentSelectDays = _this.getDepartmentSchedule.daysOfWeekApplicable;
                // this.departmentDay =  this.departmentSelectDays[0];
                var data = {
                    per_page: _this.perPage,
                    page_no: _this.pageNo,
                    _ids: res.data.assignedEmployees
                };
                _this.companySettingsService.prviewEmployeeList(data).subscribe(function (res) {
                    console.log(res);
                    _this.previewList = res.data;
                    _this.count = res.total_count;
                    for (var i = 0; i < _this.previewList.length; i++) {
                        _this.selectedUsers.push({ _id: _this.previewList[i]._id, _name: _this.previewList[i].personal.name.preferredName });
                        console.log(_this.selectedUsers, "selectedUsers");
                    }
                }, function (err) {
                    console.log(err);
                });
                if (_this.getDepartmentSchedule.daysOfWeekApplicable && _this.getDepartmentSchedule.daysOfWeekApplicable.length) {
                    _this.departmentSelectDays = _this.getDepartmentSchedule.daysOfWeekApplicable;
                    console.log(_this.getDepartmentSchedule.daysOfWeekApplicable);
                    var index = _this.getDepartmentSchedule.daysOfWeekApplicable.map(function (e) {
                        var i = _this.days2.findIndex(function (ele) { return ele.name === e; });
                        if (i >= 0) {
                            _this.days2[i].isChecked = true;
                        }
                    });
                    // this.allSelectedDepartmentDays = this.departmentSelectDays.concat(this.getDepartmentSchedule.daysOfWeekApplicable)
                }
                if (_this.getDepartmentSchedule.eligibility_criteria && _this.getDepartmentSchedule.eligibility_criteria.sub_fields.length) {
                    _this.eligibilityArray = _this.getDepartmentSchedule.eligibility_criteria.sub_fields;
                    _this.getStructureSubFields(_this.getDepartmentSchedule.eligibility_criteria.selected_field, true, _this.getDepartmentSchedule.eligibility_criteria.sub_fields);
                }
                _this.getFieldsForRoles();
                if (_this.getDepartmentSchedule.specific_employee) {
                    _this.selectedItems = _this.getDepartmentSchedule.specific_employee.include;
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    WorkSchedulesComponent.prototype.getUsersList = function () {
        var _this = this;
        for (var _i = 0, _a = this.previewList; _i < _a.length; _i++) {
            var employee = _a[_i];
            if (employee.position_work_schedule_id) {
                if (employee.position_work_schedule_id.daysOfWeek.includes(this.departmentDay)) {
                    this.employeeList.push(employee);
                }
            }
        }
        for (var i = 0; i < this.employeeList.length; i++) {
            this.employeeList[i].status = [];
            for (var j = 0; j < this.scheduleStatus.length; j++) {
                if (this.scheduleStatus[j].userId._id.indexOf(this.employeeList[i]._id) >= 0) {
                    if (this.scheduleStatus[j].day == this.departmentDay) {
                        this.scheduleStatus[j].intervalsInfo.forEach(function (element) {
                            _this.employeeList[i].status.push(element.status);
                        });
                    }
                    else {
                        this.statusOfEmployee(this.departmentDay);
                    }
                }
            }
        }
        this.employeeList.forEach(function (element) {
            element.position_work_schedule_id.weekApplicable.forEach(function (days) {
                element.position_work_schedule_id.time = days;
            });
        });
        console.log(this.employeeList);
        this.resourceAvailableCount();
    };
    WorkSchedulesComponent.prototype.changeInterval = function (value) {
        console.log("interval", value);
        if (this.getDepartmentSchedule && this.getDepartmentSchedule._id) {
            if (value == 30 || value == 60 || value == 90) {
                this.getDepartmentSchedule = "";
                this.timeIntervals = [];
                this.employeeList = [];
            }
        }
        console.log("123", this.getDepartmentSchedule, "1234", this.employeeList);
    };
    WorkSchedulesComponent.prototype.editTime = function (time) {
        console.log(time);
        if (this.getDepartmentSchedule && this.getDepartmentSchedule._id) {
            if (time == time) {
                this.getDepartmentSchedule = "";
                this.timeIntervals = [];
                this.employeeList = [];
            }
        }
    };
    // Author: Saiprakash G, Date: 27/06/19,
    // Description: Delete department work schedule
    WorkSchedulesComponent.prototype.deleteDepartmentSchedule = function () {
        var _this = this;
        this.selectedDepartments = this.allDepartments.filter(function (postionCheck) {
            return postionCheck.isChecked;
        });
        if (this.selectedDepartments.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one department work schedule to proceed", "", 'error');
        }
        else {
            var data = {
                _id: this.checkDeptScheduleIds
            };
            this.leaveManagementService.deleteDepartment(data).subscribe(function (res) {
                console.log(res);
                _this.getAllDepartmentSchedules();
                _this.checkDeptScheduleIds = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date:30/08/19,
    // Description: Preview employee list
    WorkSchedulesComponent.prototype.onSelectUsers = function (selectSpecificPerson) {
        console.log("users", selectSpecificPerson);
        console.log('selcetd users', this.selectedUsers);
        this.previewEmployeeList();
    };
    WorkSchedulesComponent.prototype.numbers = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    WorkSchedulesComponent.prototype.checkedDeptSchedules = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkDeptScheduleIds.push(id);
        }
        console.log(this.checkDeptScheduleIds);
        if (event.checked == false) {
            var index = this.checkDeptScheduleIds.indexOf(id);
            if (index > -1) {
                this.checkDeptScheduleIds.splice(index, 1);
            }
            console.log(this.checkDeptScheduleIds);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('define_dept'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WorkSchedulesComponent.prototype, "define_dept", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('select_employees'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WorkSchedulesComponent.prototype, "select_employees", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('schedule_builder'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WorkSchedulesComponent.prototype, "schedule_builder", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('recap_all'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WorkSchedulesComponent.prototype, "recap_all", void 0);
    WorkSchedulesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-work-schedules',
            template: __webpack_require__(/*! ./work-schedules.component.html */ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.html"),
            styles: [__webpack_require__(/*! ./work-schedules.component.css */ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementService"],
            amazing_time_picker__WEBPACK_IMPORTED_MODULE_3__["AmazingTimePickerService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__["SiteAccessRolesService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__["SwalAlertService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_7__["MyInfoService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__["CompanySettingsService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__["MatDialog"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormBuilder"]])
    ], WorkSchedulesComponent);
    return WorkSchedulesComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.module.ts ***!
  \******************************************************************************************/
/*! exports provided: WorkSchedulesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkSchedulesModule", function() { return WorkSchedulesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _work_schedules_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./work-schedules.routing */ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.routing.ts");
/* harmony import */ var _work_schedules_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./work-schedules.component */ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var amazing_time_picker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! amazing-time-picker */ "./node_modules/amazing-time-picker/amazing-time-picker.es5.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _dynamic_resource_model_dynamic_resource_model_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dynamic-resource-model/dynamic-resource-model.component */ "./src/app/admin-dashboard/leave-management/work-schedules/dynamic-resource-model/dynamic-resource-model.component.ts");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var ngx_material_timepicker__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-material-timepicker */ "./node_modules/ngx-material-timepicker/fesm5/ngx-material-timepicker.js");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var WorkSchedulesModule = /** @class */ (function () {
    function WorkSchedulesModule() {
    }
    WorkSchedulesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _work_schedules_routing__WEBPACK_IMPORTED_MODULE_2__["WorkSchedulesRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModuleModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_7__["NgMultiSelectDropDownModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                amazing_time_picker__WEBPACK_IMPORTED_MODULE_9__["AmazingTimePickerModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_10__["DragDropModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_12__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_13__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_15__["MatDatepickerModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_16__["MatCheckboxModule"],
                ngx_material_timepicker__WEBPACK_IMPORTED_MODULE_17__["NgxMaterialTimepickerModule"],
                angular_dual_listbox__WEBPACK_IMPORTED_MODULE_18__["AngularDualListBoxModule"]
            ],
            declarations: [_work_schedules_component__WEBPACK_IMPORTED_MODULE_3__["WorkSchedulesComponent"], _dynamic_resource_model_dynamic_resource_model_component__WEBPACK_IMPORTED_MODULE_11__["DynamicResourceModelComponent"]],
            entryComponents: [_dynamic_resource_model_dynamic_resource_model_component__WEBPACK_IMPORTED_MODULE_11__["DynamicResourceModelComponent"]]
        })
    ], WorkSchedulesModule);
    return WorkSchedulesModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.routing.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.routing.ts ***!
  \*******************************************************************************************/
/*! exports provided: WorkSchedulesRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkSchedulesRouting", function() { return WorkSchedulesRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _work_schedules_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./work-schedules.component */ "./src/app/admin-dashboard/leave-management/work-schedules/work-schedules.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { BlackOutDatesComponent } from './black-out-dates.component';
var routes = [
    { path: '', component: _work_schedules_component__WEBPACK_IMPORTED_MODULE_2__["WorkSchedulesComponent"] }
];
var WorkSchedulesRouting = /** @class */ (function () {
    function WorkSchedulesRouting() {
    }
    WorkSchedulesRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], WorkSchedulesRouting);
    return WorkSchedulesRouting;
}());



/***/ })

}]);
//# sourceMappingURL=leave-management-leave-management-module~scheduler-scheduler-module~work-schedules-work-schedules-mo~f3cda310.js.map