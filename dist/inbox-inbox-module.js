(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["inbox-inbox-module"],{

/***/ "./src/app/admin-dashboard/inbox/inbox.component.css":
/*!***********************************************************!*\
  !*** ./src/app/admin-dashboard/inbox/inbox.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inbox-main { float: none; margin: 0 auto; padding:30px 0 0;}\n.inbox-main h4 { margin: 0;}\n.category{ margin: 10px -50px 20px; padding: 0;}\n.category label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 15px 15px 0;}\n.category .form-control { border: none;box-shadow: none;padding: 12px 12px;height: auto;\nwidth:65%; font-size: 15px;background: #fff; display: inline-block;}\n.all-email { background: #edf7fe; padding: 5px 15px;}\n.current-mails{ margin: 20px 0 0;}\n.current-mails .panel-default>.panel-heading { padding: 0; background-color:transparent !important; border: none; position: relative;}\n.current-mails .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px; display: block;}\n.current-mails .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.current-mails .panel-title>.small, .current-mails .panel-title>.small>a, .current-mails .panel-title>a, .current-mails .panel-title>small, .current-mails .panel-title>small>a { text-decoration:none;}\n.current-mails .panel { background: none; box-shadow: none; border-radius: 0;}\n.current-mails .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.current-mails .panel-heading .accordion-toggle{ position: absolute; top: 0;}\n.current-mails .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.current-mails .panel-group .panel-heading+.panel-collapse>.list-group, .current-mails .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.current-mails .panel-heading .accordion-toggle:after { margin:7px 15px 0; font-size: 13px; line-height: 13px;}\n.current-mails .panel-body { padding: 0 0 5px;}\n.current-mails .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.current-mails .panel-default { border-color:transparent; background:#fff; padding: 15px 5px 0;}\n.current-mails .panel-group .panel+.panel { margin-top: 20px;}\n.current-mails .btn.approve {border-radius: 20px;font-size: 16px; padding:6px 32px;\nbackground: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.current-mails .btn.cancel { color:#e44a49; background:transparent; padding:22px 0 0 30px;font-size: 16px;}\n.current-mails .btn.reject { color:#e44a49; background:transparent;border:#b7b3b3 1px solid; padding:6px 32px; \nborder-radius: 20px;font-size: 16px; border: #e44a49 1px solid; margin: 0 20px 0 0;}\n.right-approve { margin:20px 0 0;}\n.mails-btm { float: none; margin: 0 auto; padding:20px 0 30px;}\n.mails-accord {border-bottom:#ccc 1px solid; padding: 0 0 10px;}\n.mails-accord ul { display: block;}\n.mails-accord ul li { display:inline-block; border-right:#ccc 1px solid;}\n.mails-accord ul li small { display: block; padding:17px 0;font-size: 17px;}\n.mails-accord ul li p { display: block; padding:11px 0 6px;}\n.mails-accord ul li.border{ border: none;}\n.mails-accord ul li small em { display: inline-block; font-size: 13px; padding:0 5px 0 0; color: #008f3d; vertical-align: middle;}\n.mails-accord ul li.approve small em { display: inline-block; font-size: 13px; padding:0 5px 0 0; color: #008f3d;}\n.mails-accord ul li span em { display: inline-block; font-size: 13px; padding:0 5px 0 0; color: #ccc;}\n.mails-accord > ul > li > span { vertical-align: middle; padding: 10px 0 0; display: block; font-size: 17px; color: #777;}\n.mails-accord ul li span em .fa { vertical-align: top;}\n.mails-accord ul li var { display: block; padding:17px 0;font-size: 17px;font-style: normal;}\n.mails-accord ul li var em { display: inline-block; font-size: 13px; padding:0 5px 0 0; color: #ff0a0a;font-style: normal;}\n.mails-accord ul li var em .fa { vertical-align: top;}\n.managers { height: 48px;}\n.managers span { vertical-align: middle; display: inline-block; padding: 0;}\n.manager-right { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;}\n.manager-right h4 { font-size: 15px; margin: 0 0 5px;}\n.manager-right h5 {  color: #ccc;margin: 0;}\n.current-mails-cont { margin: 10px auto 0; float: none; padding: 0 0 15px;}\n.current-mails-cont label {  color: #4c4848;font-size:16px; padding: 0 0 5px;}\n.current-mails-cont p {  color: #ccc;margin: 0 0 4px; font-size: 15px;}\n.current-mails-cont h5 {margin: 0 0 7px;}\n.current-mails-cont .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff;}\n.current-mails .current-mails-cont .btn.cancel { color:#e44a49; background:transparent;font-size: 16px; padding: 0;}\ntextarea.form-control { margin: 0 0 20px; resize: none;}\n.email-head {padding:15px 0; background: #fff;}\n.email-head ul { display: block;}\n.email-head ul li { display:inline-block;}\n.email-head ul li small { display: block; font-size: 16px;}\n.email-head ul li.border{ border: none;}\n.email-head ul li small a {color: #929191;font-size: 15px;}\n.mat-paginator-page-size-label { font-size: 15px;}\n.mail-check{ display: inline-block; position: absolute; top:10px; left:20px;}\n.mail-check em{position: relative;}\n.mail-check em::after{ content:''; position: absolute; top:-12px; right:-40px; height:50px;border-right:#ccc 1px solid;}\n.zenwork-checked-border::after {top: -19px !important;left: -73px !important;border-right: #83c7a1 5px solid !important;\nheight: 60px !important;right: auto !important;}\n.emplployee-icon{\n    font-size: 50px;\n    color: #ccc;  \n}"

/***/ }),

/***/ "./src/app/admin-dashboard/inbox/inbox.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin-dashboard/inbox/inbox.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"inbox-main col-md-11\">\n\n  <h4>Current Emails</h4>\n\n  <div class=\"category col-md-4 pull-right\">\n    <label>Category</label>\n    <mat-select placeholder=\"Inbox- All Emails\" class=\"form-control\" [(ngModel)]=\"categoryName\" (ngModelChange)=\"categoryFilter(categoryName)\">\n      <mat-option  *ngFor=\"let list of lists\" [value]=\"list.name\">{{list.name}}</mat-option>\n    </mat-select>\n  </div>\n  <div class=\"clearfix\"></div>\n\n  <div class=\"all-email\" >\n    <h5 class=\"pull-left\">Inbox - All Emails ( {{count}} )</h5>\n    <span class=\"pull-right\" >\n      <button mat-icon-button [matMenuTriggerFor]=\"menu1\" aria-label=\"Example icon-button with a menu\">\n        <mat-icon>more_vert</mat-icon>\n      </button>\n      <mat-menu #menu1=\"matMenu\">\n        <button mat-menu-item>\n          <a (click)=\"archiveMails()\">Archive</a>\n        </button>\n       \n        <button mat-menu-item>\n          <a (click)=\"deleteEmails()\">Delete</a>\n        </button>\n      </mat-menu>\n    </span>\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n  <div class=\"email-head\">\n    <ul>\n      <li class=\"col-md-1\">\n\n      </li>\n      <li class=\"col-md-2\">\n        <small>From  </small>\n      </li>\n      <li class=\"col-md-2\">\n        <small>Subject</small>\n      </li>\n      <li class=\"col-md-2\">\n        <small>Date</small>\n      </li>\n      <li class=\"col-md-2\">\n        <small>Time</small>\n      </li>\n      <li class=\"col-md-2 pull-right border\">\n        <small>Status</small>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n  <div class=\"current-mails\">\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\" *ngFor=\"let data of dataList;let i=index;\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n             <div class=\"mail-check\">\n                \n                <mat-checkbox [ngClass]=\"{'zenwork-checked-border':data.isChecked}\"  class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\"\n                (ngModelChange)=\"selectData(data._id, data.taskStatus)\" disabled=\"{{data.taskStatus=='Pending' || data.request_status=='Pending'}}\"></mat-checkbox>\n                <em></em>\n             </div>\n            <a data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne{{i+1}}\" (click)=\"markAsRead(data._id)\">\n\n              <div class=\"mails-accord\">\n                <ul>\n                  <li class=\"col-md-1\">\n                \n                  </li>\n                  <li class=\"col-md-2\">\n                    <div class=\"managers\">\n                      <span>\n                          <i class=\"material-icons emplployee-icon\">\n                              account_circle\n                            </i>\n                        <!-- <img src=\"../../../assets/images/Manager Self Service/employee.png\"> -->\n                      </span>\n                      <div class=\"manager-right\" *ngIf=\"data.from\">\n                        <h4 *ngIf = \"data.from.type == 'company'\">System Admin</h4>\n                        <h4 *ngIf = \"data.from.type == 'employee'\">{{data.from.personal.name.preferredName}}</h4>\n                        <!-- <h5>Manager</h5> -->\n                      </div>\n                    </div>\n                  </li>\n\n                  <li class=\"col-md-2\">\n                    <small>{{data.subject}}</small>\n\n                  </li>\n\n                  <li class=\"col-md-2\">\n                    <small>{{data.createdAt | date:\"MM/dd/yy\"}}</small>\n\n                  </li>\n\n                  <li class=\"col-md-2\">\n                    <small>{{data.createdAt | date:\"shortTime\"}}</small>\n\n                  </li> \n\n                  <li *ngIf= \"data.subject == 'On-boarding Tasks' || data.subject == 'Off-boarding Tasks'\"  class=\"col-md-2 pull-right border\">\n                    <span *ngIf=\"data.taskStatus=='Pending'\"> <em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.taskStatus}}</span>\n                    <small *ngIf=\"data.taskStatus=='Completed'\"> <em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.taskStatus}}</small>\n                  </li>\n                  <li *ngIf= \"data.subject == 'Manager Requests'\" class=\"col-md-2 pull-right border\">\n                    <small *ngIf=\"data.request_status=='Approved'\"><em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.request_status}}</small>\n                    <var *ngIf=\"data.request_status=='Rejected'\"><em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.request_status}}</var>\n                    <span *ngIf=\"data.request_status=='Pending'\"> <em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.request_status}}</span>\n                  </li>\n\n                </ul>\n                <div class=\"clearfix\"></div>\n              </div>\n\n            </a>\n\n          </h4>\n        </div>\n\n        <div id=\"collapseOne{{i+1}}\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"current-mails-cont col-md-10\" >\n            \n              <label *ngIf=\"data.subject !== 'Manager Requests'\">Task Name: {{data.taskName}}</label>\n              <label *ngIf=\"data.subject == 'Manager Requests'\"> {{data.subject}}</label>\n\n              <p>Impacted Employee: {{data.impactedEmployee.personal.name.preferredName}}</p> \n\n              <p *ngIf=\"data.onboarding_template_id\">Task Assignment Date: {{data.onboarding_template_id.assigned_date | date : 'MM/dd/yy'}}</p>\n              <p *ngIf=\"data.offboarding_template_id\">Task Assignment Date: {{data.offboarding_template_id.assigned_date | date : 'MM/dd/yy'}}</p>\n              <p *ngIf=\"data.manager_request_id\">Request Date: {{data.manager_request_id.requestedDate | date : 'MM/dd/yy'}}</p>\n\n\n              <h5>Add Notes:</h5>\n              <form [formGroup]=\"currentEmails\" (ngSubmit)=\"currentEmailSubmit(data._id)\">\n              <textarea rows=\"4\" class=\"form-control\" formControlName=\"textarea\"></textarea>\n\n              <button class=\"btn pull-left cancel\">Cancel</button>\n              <button class=\"btn pull-right save\" type=\"submit\">Mark as Complete</button>\n              <div class=\"clearfix\"></div>\n            </form>\n            </div>\n\n          </div>\n        </div>\n      </div>\n     \n\n      <div class=\"col-md-11 mails-btm\">\n      <button class=\"btn pull-left cancel\">Cancel</button>\n      <div class=\"pull-right right-approve\">\n        <button class=\"btn pull-left reject\">Reject</button>\n        <button class=\"btn pull-right approve\">Approve</button>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"clearfix\"></div>\n     </div>\n\n      <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\"\n        (page)=\"pageEvents($event)\"></mat-paginator>\n\n\n    </div>\n\n\n  </div>\n\n  <br>\n\n  <h4>Archived Emails</h4>\n\n  <div class=\"category col-md-4 pull-right\">\n    <label>Category</label>\n    <mat-select placeholder=\"Inbox- All Emails\" class=\"form-control\" [(ngModel)]=\"categoryName\" (ngModelChange)=\"archiveCategoryFilter(categoryName)\">\n      <mat-option  *ngFor=\"let list of lists\" [value]=\"list.name\">{{list.name}}</mat-option>\n    </mat-select>\n  </div>\n  <div class=\"clearfix\"></div>\n\n  <div class=\"all-email\">\n    <h5 class=\"pull-left\">Inbox - All Emails ( {{archiveCount}} )</h5>\n    <span class=\"pull-right\" >\n      <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n        <mat-icon>more_vert</mat-icon>\n      </button>\n      <mat-menu #menu=\"matMenu\">\n        <button mat-menu-item>\n          <a (click)=\"archiveDelete()\">Delete</a>\n        </button>\n        <button mat-menu-item>\n          <a (click)=\"restoreEmails()\">Restore</a>\n        </button>\n      </mat-menu>\n    </span>\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n  <div class=\"email-head\">\n    <ul>\n      <li class=\"col-md-1\">\n\n      </li>\n      <li class=\"col-md-2\">\n        <small>From  </small>\n      </li>\n      <li class=\"col-md-2\">\n        <small>Subject </small>\n      </li>\n      <li class=\"col-md-2\">\n        <small>Date </small>\n      </li>\n      <li class=\"col-md-2\">\n        <small>Time</small>\n      </li>\n      <li class=\"col-md-2 pull-right border\">\n        <small>Status</small>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n  <div class=\"current-mails\">\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\" *ngFor=\"let data of archiveDataList;let i=index;\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n             <div class=\"mail-check\">\n                <mat-checkbox [ngClass]=\"{'zenwork-checked-border':data.isChecked}\"  class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\"\n                (ngModelChange)=\"selectData(data._id)\"></mat-checkbox>\n                <em></em>\n             </div>\n            <a data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n\n              <div class=\"mails-accord\">\n                <ul>\n                  <li class=\"col-md-1\">\n                \n                  </li>\n                  <li class=\"col-md-2\">\n                    <div class=\"managers\">\n                      <span>\n                          <i class=\"material-icons emplployee-icon\">\n                              account_circle\n                            </i>\n                      </span>\n                      <div class=\"manager-right\" *ngIf=\"data && data.from\">\n                        <h4 *ngIf = \"data.from.type == 'company'\">System Admin</h4>\n                        <h4 *ngIf = \"data.from.type == 'employee'\">{{data.from.personal.name.preferredName}}</h4>\n                      </div>\n                    </div>\n                  </li>\n\n                  <li class=\"col-md-2\">\n                    <small>{{data.subject}}</small>\n\n                  </li>\n\n                  <li class=\"col-md-2\">\n                    <small>{{data.createdAt | date:\"MM/dd/yy\"}}</small>\n\n                  </li>\n\n                  <li class=\"col-md-2\">\n                    <small>{{data.createdAt | date:\"shortTime\"}}</small>\n\n                  </li>\n\n\n                  <li *ngIf= \"data.subject == 'On-boarding Tasks' || data.subject == 'Off-boarding Tasks'\"  class=\"col-md-2 pull-right border\">\n                    <span *ngIf=\"data.taskStatus=='Pending'\"> <em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.taskStatus}}</span>\n                    <small *ngIf=\"data.taskStatus=='Completed'\"> <em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.taskStatus}}</small>\n                  </li>\n                  <li *ngIf= \"data.subject == 'Manager Requests'\" class=\"col-md-2 pull-right border\">\n                    <small *ngIf=\"data.request_status=='Approved'\"><em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.request_status}}</small>\n                    <var *ngIf=\"data.request_status=='Rejected'\"><em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.request_status}}</var>\n                    <span *ngIf=\"data.request_status=='Pending'\"> <em><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>{{data.request_status}}</span>\n                  </li>\n               \n                </ul>\n                <div class=\"clearfix\"></div>\n              </div>\n\n            </a>\n\n          </h4>\n        </div>\n\n      </div>\n\n      <mat-paginator [length]=\"archiveCount\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\"\n        (page)=\"archivePageEvents($event)\"></mat-paginator>\n\n\n    </div>\n\n\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/inbox/inbox.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin-dashboard/inbox/inbox.component.ts ***!
  \**********************************************************/
/*! exports provided: InboxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboxComponent", function() { return InboxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_inbox_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/inbox.service */ "./src/app/services/inbox.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InboxComponent = /** @class */ (function () {
    function InboxComponent(inboxService, accessLocalStorageService, swalService) {
        this.inboxService = inboxService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalService = swalService;
        this.selectID = {
            _id: ''
        };
        this.selectEmailId = [];
        this.taskCatsArchived = [];
        this.taskOffBoardingsArchived = [];
        this.tasknamesArchived = [];
        this.archiveDataList = [];
        this.lists = [
            { name: 'Off-boarding Tasks' },
            { name: 'On-boarding Tasks' },
            { name: 'Employee Requests' },
            { name: 'Manager Requests' },
            { name: 'Workflow Approvals' },
            { name: 'Benefit Requests' },
            { name: 'Leave Management Requests' },
            { name: 'Performance Requests' },
        ];
        this.tasknames = [];
        this.taskCats = [];
        this.taskOffBoardings = [];
        this.dataList = [];
        this.isChecked = false;
        this.selectCurrentEmailsId = [];
        this.singlePerson = false;
        // MatPaginator Inputs
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [2, 5, 10, 25, 100];
        this.tName = [];
        this.tNameArchive = [];
    }
    InboxComponent.prototype.ngOnInit = function () {
        this.currentEmails = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            textarea: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
        });
        // this.taskStatusCompleted = 'Completed'
        this.cID = localStorage.getItem('companyId');
        console.log("inbox IDD", this.cID);
        this.cID = this.cID.replace(/^"|"$/g, "");
        console.log("cin222222222", this.cID);
        this.userID = this.accessLocalStorageService.get('employeeId');
        console.log("inbox userr", this.userID);
        this.getAllEmails();
        this.getAllAchived();
    };
    // Author:Suresh M, Date:06-06-19
    // Page Event
    InboxComponent.prototype.pageEvents = function (event) {
        console.log("222222222222", event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllEmails();
    };
    // Author:Suresh M, Date:03-07-19
    // archivePageEvents
    InboxComponent.prototype.archivePageEvents = function (event) {
        console.log("1111111111111", event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllAchived();
    };
    // Author:Saiprakash G, Date:08-06-19
    // Add notes and mark as complete
    InboxComponent.prototype.currentEmailSubmit = function (id) {
        var _this = this;
        var data = {
            notif_id: id,
            notes: this.currentEmails.get('textarea').value
        };
        this.inboxService.markAsComplete(data).subscribe(function (res) {
            console.log(res);
            _this.getAllEmails();
            _this.currentEmails.reset();
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash G, Date:08-06-19
    // Category filter 
    InboxComponent.prototype.categoryFilter = function (event) {
        console.log("category", event);
        this.categoryName = event;
        this.getAllEmails();
    };
    // Author:Suresh M, Date:03-07-19
    // archiveCategoryFilter
    InboxComponent.prototype.archiveCategoryFilter = function (event) {
        console.log("category", event);
        this.categoryName = event;
        this.getAllAchived();
    };
    // Author:Suresh M, Date:06-06-19
    // get All Components names
    InboxComponent.prototype.getAllEmails = function () {
        var _this = this;
        var AllIds = {
            companyId: this.cID,
            userId: this.userID,
            category: this.categoryName,
            archive: false,
            per_page: this.perPage,
            page_no: this.pageNo
        };
        this.inboxService.getAllComponents(AllIds)
            .subscribe(function (res) {
            console.log("All Components names", res);
            _this.dataList = res.data;
            _this.count = res.total_count;
            var categorynames = [];
            for (var i = 0; i < _this.dataList.length; i++) {
                _this.tName.push(_this.dataList[i].task_category);
                if (_this.dataList[i].onboarding_template_id) {
                    _this.taskCats = _this.dataList[i].onboarding_template_id['Onboarding Tasks'][_this.tName[i]];
                    for (var j = 0; j < _this.taskCats.length; j++) {
                        if (_this.dataList[i].task_id == _this.taskCats[j]._id) {
                            console.log(_this.dataList[i].task_id);
                            console.log(_this.taskCats[j]._id);
                            _this.tasknames.push({
                                taskName: _this.taskCats[j].taskName,
                                taskStatus: _this.taskCats[j].status
                            });
                        }
                    }
                }
                if (_this.dataList[i].offboarding_template_id) {
                    _this.taskOffBoardings = _this.dataList[i].offboarding_template_id['offBoardingTasks'][_this.tName[i]];
                    for (var k = 0; k < _this.taskOffBoardings.length; k++) {
                        if (_this.dataList[i].task_id == _this.taskOffBoardings[k]._id) {
                            console.log(_this.dataList[i].task_id);
                            console.log(_this.taskOffBoardings[k]._id);
                            _this.tasknames.push({
                                taskName: _this.taskOffBoardings[k].taskName,
                                taskStatus: _this.taskOffBoardings[k].status
                            });
                        }
                    }
                }
                if (_this.dataList[i].manager_request_id) {
                    _this.tasknames.push({
                        taskStatus: _this.dataList[i].request_status
                    });
                }
            }
            console.log("catnames", _this.tasknames);
            for (var i = 0; i < _this.dataList.length; i++) {
                Object.assign(_this.dataList[i], _this.tasknames[i]);
                console.log("task namesssssssss", _this.tasknames[i], _this.dataList[i]);
            }
            console.log("1111111", _this.dataList);
        });
    };
    // Author:Suresh M, Date:06-06-19
    // get All Components names
    InboxComponent.prototype.getAllAchived = function () {
        var _this = this;
        var AllIds = {
            companyId: this.cID,
            userId: this.userID,
            category: this.categoryName,
            archive: true,
            per_page: this.perPage,
            page_no: this.pageNo
        };
        this.inboxService.getAllComponents(AllIds)
            .subscribe(function (res) {
            console.log("All Archived Emails", res);
            _this.archiveDataList = res.data;
            console.log("Archiveeeeee", _this.archiveDataList);
            _this.archiveCount = res.total_count;
            for (var i = 0; i < _this.archiveDataList.length; i++) {
                _this.tNameArchive.push(_this.archiveDataList[i].task_category);
                if (_this.archiveDataList[i].onboarding_template_id) {
                    _this.taskCatsArchived = _this.archiveDataList[i].onboarding_template_id['Onboarding Tasks'][_this.tNameArchive[i]];
                    console.log('true valuesssss', _this.taskCatsArchived);
                    if (_this.taskCatsArchived && _this.taskCatsArchived.length) {
                        for (var j = 0; j < _this.taskCatsArchived.length; j++) {
                            if (_this.archiveDataList[i].task_id == _this.taskCatsArchived[j]._id) {
                                console.log(_this.archiveDataList[i].task_id);
                                console.log(_this.taskCatsArchived[j]._id);
                                _this.tasknamesArchived.push({
                                    taskName: _this.taskCatsArchived[j].taskName,
                                    taskStatus: _this.taskCatsArchived[j].status
                                });
                            }
                        }
                    }
                }
                if (_this.archiveDataList[i].offboarding_template_id) {
                    _this.taskOffBoardingsArchived = _this.archiveDataList[i].offboarding_template_id['offBoardingTasks'][_this.tNameArchive[i]];
                    console.log("archive filessssssssssss", _this.taskOffBoardingsArchived);
                    for (var k = 0; k < _this.taskOffBoardingsArchived.length; k++) {
                        if (_this.archiveDataList[i].task_id == _this.taskOffBoardingsArchived[k]._id) {
                            console.log(_this.archiveDataList[i].task_id);
                            console.log(_this.taskOffBoardingsArchived[k]._id);
                            _this.tasknamesArchived.push({
                                taskName: _this.taskOffBoardingsArchived[k].taskName,
                                taskStatus: _this.taskOffBoardingsArchived[k].status
                            });
                        }
                    }
                }
            }
            for (var i = 0; i < _this.archiveDataList.length; i++) {
                Object.assign(_this.archiveDataList[i], _this.tasknames[i]);
            }
        });
    };
    InboxComponent.prototype.getKeys = function (data) {
        return Object.keys(data);
    };
    // Author:Suresh M, Date:03-07-19
    // delete mails
    InboxComponent.prototype.deleteEmails = function () {
        var _this = this;
        this.selectEmailId = this.dataList.filter(function (emailCheck) {
            return emailCheck.isChecked;
        });
        var deletaData = {
            companyId: this.cID,
            _ids: this.selectID
        };
        if (this.selectEmailId == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Select any Data", "", "");
        }
        else {
            this.inboxService.deleteEmail(deletaData)
                .subscribe(function (res) {
                console.log("Delete mailss", res);
                _this.getAllEmails();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Notifications successfully Delete", "success");
                }
            });
        }
    };
    // Author:Suresh M, Date:03-07-19
    // archiveMails
    InboxComponent.prototype.archiveMails = function () {
        var _this = this;
        this.selectEmailId = this.dataList.filter(function (archiveCheck) {
            return archiveCheck.isChecked;
        });
        var archiveData = {
            companyId: this.cID,
            _ids: this.selectID
        };
        if (this.selectEmailId > 1) {
            this.swalService.SweetAlertWithoutConfirmation("Archived", "Select only one", "Request");
        }
        else if (this.selectEmailId == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Archived", "Select one data", "Request");
        }
        else {
            this.inboxService.emailsArchive(archiveData)
                .subscribe(function (res) {
                console.log("Archive mailsss", res);
                _this.getAllEmails();
                _this.getAllAchived();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Archive", "Successfully Archive data", "suceess");
                }
            });
        }
    };
    // Author:Suresh M, Date:03-07-19
    // restoreEmails
    InboxComponent.prototype.restoreEmails = function () {
        var _this = this;
        this.selectEmailId = this.archiveDataList.filter(function (archiveCheck) {
            return archiveCheck.isChecked;
        });
        var restoreData = {
            companyId: this.cID,
            _ids: this.selectID,
        };
        if (this.selectEmailId == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Select any Data", "", "");
        }
        else {
            this.inboxService.restoreMails(restoreData)
                .subscribe(function (res) {
                console.log("restore mails", res);
                _this.getAllAchived();
                _this.getAllEmails();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Restore", "Successfully Restore mails", "success");
                }
            });
        }
    };
    // Author:Suresh M, Date:03-07-19
    // archive Delete
    InboxComponent.prototype.archiveDelete = function () {
        var _this = this;
        this.selectEmailId = this.archiveDataList.filter(function (archiveCheck) {
            return archiveCheck.isChecked;
        });
        var archiveDelete = {
            companyId: this.cID,
            _ids: this.selectID
        };
        if (this.selectEmailId == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Select any data", "", "");
        }
        else {
            this.inboxService.deleteEmail(archiveDelete)
                .subscribe(function (res) {
                console.log("archive deleteee", res);
                _this.getAllAchived();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully Delete archive", "success");
                }
            });
        }
    };
    // Author:Suresh M, Date:03-07-19
    // selectData Id
    InboxComponent.prototype.selectData = function (id, status) {
        console.log("idddd", id, status);
        this.selectID = id;
        console.log("id111111", this.selectID);
        this.taskStatusCompleted = status;
        console.log("statusssss", this.taskStatusCompleted);
    };
    // Author:Suresh M, Date:03-07-19
    // Mark As Read
    InboxComponent.prototype.markAsRead = function (id) {
        console.log("mark assss", id);
        this.markasReadId = id;
        console.log("mark readdd", this.markasReadId);
        var markasreadData = {
            companyId: this.cID,
            _id: this.markasReadId
        };
        this.inboxService.markasReadData(markasreadData)
            .subscribe(function (res) {
            console.log("mark as success response", res);
        });
    };
    InboxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inbox',
            template: __webpack_require__(/*! ./inbox.component.html */ "./src/app/admin-dashboard/inbox/inbox.component.html"),
            styles: [__webpack_require__(/*! ./inbox.component.css */ "./src/app/admin-dashboard/inbox/inbox.component.css")]
        }),
        __metadata("design:paramtypes", [_services_inbox_service__WEBPACK_IMPORTED_MODULE_1__["InboxService"], _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"], _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], InboxComponent);
    return InboxComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/inbox/inbox.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/admin-dashboard/inbox/inbox.module.ts ***!
  \*******************************************************/
/*! exports provided: InboxModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboxModule", function() { return InboxModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _inbox_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./inbox.component */ "./src/app/admin-dashboard/inbox/inbox.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    { path: '', component: _inbox_component__WEBPACK_IMPORTED_MODULE_3__["InboxComponent"], pathMatch: 'full' }
];
var InboxModule = /** @class */ (function () {
    function InboxModule() {
    }
    InboxModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_inbox_component__WEBPACK_IMPORTED_MODULE_3__["InboxComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes), _angular_material_select__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_5__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"], _angular_material_paginator__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], InboxModule);
    return InboxModule;
}());



/***/ }),

/***/ "./src/app/services/inbox.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/inbox.service.ts ***!
  \*******************************************/
/*! exports provided: InboxService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboxService", function() { return InboxService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InboxService = /** @class */ (function () {
    function InboxService(http) {
        this.http = http;
    }
    InboxService.prototype.markAsComplete = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].version + "/notifications/markAsComplete", data);
    };
    InboxService.prototype.getAllComponents = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].version + "/notifications/list", data);
    };
    InboxService.prototype.deleteEmail = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].version + "/notifications/delete", id);
    };
    InboxService.prototype.emailsArchive = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].version + "/notifications/archive", id);
    };
    InboxService.prototype.restoreMails = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].version + "/notifications/restore", id);
    };
    InboxService.prototype.markasReadData = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].version + "/notifications/markAsRead", id);
    };
    InboxService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], InboxService);
    return InboxService;
}());



/***/ })

}]);
//# sourceMappingURL=inbox-inbox-module.js.map