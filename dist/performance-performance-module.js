(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["performance-performance-module"],{

/***/ "./src/app/admin-dashboard/employee-management/performance/performance.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/performance/performance.module.ts ***!
  \***************************************************************************************/
/*! exports provided: PerformanceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerformanceModule", function() { return PerformanceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _performance_performance_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./performance/performance.component */ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.ts");
/* harmony import */ var _performance_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./performance.routing */ "./src/app/admin-dashboard/employee-management/performance/performance.routing.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var PerformanceModule = /** @class */ (function () {
    function PerformanceModule() {
    }
    PerformanceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _performance_routing__WEBPACK_IMPORTED_MODULE_3__["PerformanceRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot()
            ],
            declarations: [_performance_performance_component__WEBPACK_IMPORTED_MODULE_2__["PerformanceComponent"]]
        })
    ], PerformanceModule);
    return PerformanceModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/performance/performance.routing.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/performance/performance.routing.ts ***!
  \****************************************************************************************/
/*! exports provided: PerformanceRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerformanceRouting", function() { return PerformanceRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _performance_performance_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./performance/performance.component */ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/performance",pathMatch:"full" },
    { path: '', component: _performance_performance_component__WEBPACK_IMPORTED_MODULE_2__["PerformanceComponent"] },
];
var PerformanceRouting = /** @class */ (function () {
    function PerformanceRouting() {
    }
    PerformanceRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PerformanceRouting);
    return PerformanceRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.css":
/*!*******************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/performance/performance/performance.component.css ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal-head{\n    margin: 10px;\n}\n.main-head .list-items {\n    display: inline-block;\n    padding: 0px 5px;    \n}\n.zenwork-emergency-tab{\n    padding: 0 30px 0 0;\n}\n.zenwork-padding-20-zero{\n    padding:10px 0px!important;\n}\n.emergency-contact{\n    margin: 20px 0;\n}\n.img-personal-icon{\n    width: 15px;\n}\na{\n    color: #000;\n    text-decoration: none;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.hire-wizard{\n    width:100%;\n\n}\n.nav>li>a {\n    padding: 5px 15px;\n}\n.nav-tabs>li{\n    padding: 0px 10px;\n}\n.tradational-tab{\n    padding: 0 0 0 10px;\n}\n.current-review-heading{\n    margin: 30px 0 0 0;\n}\n.onboarding-tabs li a:active,.onboarding-tabs li a:visited,.onboarding-tabs li a:focus{\n    /* border: 1px solid transparent!important; */\n    /* border-bottom: 4px solid #439348!important; */\n    border-radius: 0px!important;\n    color: #353131!important;\n}\n.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{\n    border: 1px solid transparent!important;\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    background: #fff;\n}\n.performance-score-table{\n    background: #fff;\n    margin: 30px 0;\n\n}\n.performance-icon{\n    width: 60px;\n    padding: 20px 0px;\n}\n.score-list .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n    vertical-align: middle\n}\n.employee-score{\n    font-size: 30px;\n    font-weight: 700;\n    color: #3bafba;\n}\n.individual-score{\n\n}\n.year-performance{\n    font-size: 14px;\n    color: #acb1ae;\n}\n.employee-details{\n    margin:40px 0 0 0;\n    /* border-bottom: 1px solid #e4eae7; */\n    padding: 0px 0 20px;\n}\n.total-employee{\n    margin: 0 auto;\n}\n.previous-score-list{\n    margin: 0;\n    background: #fff;\n}\n.previous-score-list .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n    vertical-align: middle\n}\n.previous-tabs{\n\n}\n.previous-tabs li a{\n    /* border-top: 4px solid #439348!important; */\n    padding: 5px 15px;\n    margin-right: 2px;\n    line-height: 1.42857143;\n    border: 1px solid transparent;\n    border-radius: 4px 4px 0 0;\n    position: relative;\n    display: block;\n    position: relative;\n    \n}\n.previous-tabs li{\n   margin: 0 0 0 25px;\n    width: 13%;\n    float: left;\n    margin-bottom: -1px;\n    position: relative;\n    display: block; \n    padding-left: 0;\n    margin-bottom: 0;\n    list-style: none;\n    text-align: center;\n}\n.previous-tabs .active{\n    border: 1px solid transparent!important;\n    border-top: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    background: #f8f8f8;\n\n}\n.link-heading{\n    padding: 0 0 0 25px;\n}\n.search-border{\n    border : 1px solid #c4cbd1;\n    border-radius: 25px;\n    /* height: 25px; */\n}\n.search-border .form-control{\n    height:25px;\n    box-shadow: none;\n    padding: 6px 60px 6px 15px;\n   \n}\n.links-heading{\n    background: #edf7fe;\n    padding: 5px 0px;\n}\n.border-none{\n    border: none;\n    background: transparent;\n}\n.find-links{\n    margin: 0;\n}\n.find-links .list-items{\n    display: inline-block;\n    padding: 0px;\n    vertical-align: middle;\n    float: none;\n\n}\n.add-link{\n    margin: 5px 15px 0 0;\n}\n.table-body{\n    background: #fff;\n    padding: 10px;\n    color: #bbb7b7;\n    font-size: 12px;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    font-size: 12px;\n   margin: 0 0 0 2px;\n}\ninput{\n    height:30px;\n    border:0;\n    box-shadow: none;\n    font-size: 12px;\n \n}\nselect{\n    height:30px;\n    font-size: 12px;\n}\n.modern-list li{\n    padding: 0 0 15px 25px;\n}\n.review-custom-width{\n    width: 30%;\n    border: none;\n    box-shadow: none;\n}\n.date-input{\n    width: 100%;\n    border: none;\n    box-shadow: none;\n}\n.finishing{\n    position: relative;\n    display: inline-block;\n    width: 30%;\n}\n.calendar-icon{\n    position: absolute;\n    top: 7px;\n    right: 6px;\n    width: 17px;\n\n}\n.save-changes{\n    padding: 0 0 0 15px;\n}\n.submit-buttons .btn-success {\n    background-color: #439348;\n}\n.submit-buttons  .list-item{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.margin-top{\n    margin-top: 40px;\n}\n.btn-style{\n    padding: 2px 12px!important;\n    border-radius: 30px!important;\n    font-size: 12px;\n}\n.zenwork-emergency-tab { float:none; margin: 0 auto; padding: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/performance/performance/performance.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-emergency-tab col-md-11\">\n  <div class=\"personal-head\">\n    <ul class=\"list-unstyled main-head\">\n      <li class=\"list-items\">\n        <img src=\"../../../assets/images/directory_tabs/Performance/icon-1.png\" class=\"img-personal-icon\" alt=\"personal-icon\">\n      </li>\n      <li class=\"list-items\">\n        <span>Performance</span>\n      </li>\n    </ul>\n  </div>\n  <hr class=\"zenwork-margin-ten-zero\">\n  <div class=\"Performance-tabs\">\n    <div class=\"hire-wizard pull-left\">\n      <ul class=\"nav nav-tabs onboarding-tabs\">\n        <li class=\"active\"><a data-toggle=\"tab\" href=\"#tradational\">Tradational</a></li>\n        <li><a data-toggle=\"tab\" href=\"#thirdparty\">3rd Party</a></li>\n        <li><a data-toggle=\"tab\" href=\"#modern\">Modern</a></li>\n      </ul>\n      <div class=\"tab-content\">\n        <div id=\"tradational\" class=\"tab-pane fade in active tradational-tab\">\n          <div class=\"current-review-heading\">\n            <span class=\"green\">Current Review Score</span>\n            <span class=\"caret\"></span>\n            <div class=\"performance-score-table text-center\">\n              <ul class=\"list-unstyled score-list\">\n                <li class=\"list-items\">\n                  <img src=\"../../../assets/images/directory_tabs/Performance/icon.png\" class=\"performance-icon\" alt=\"score-icon\">\n                </li>\n                <li class=\"list-items\">\n                  <h4 class=\"employee-score\">\n                    4.6\n                  </h4>\n                  <small class=\"year-performance\">2018 Performance Score</small>\n                </li>\n              </ul>\n            </div>\n            <hr class=\"zenwork-margin-ten-zero\">\n          </div>\n          <div class=\"current-review-heading\">\n            <span class=\"green\">Previous Review Score</span>\n            <span class=\"caret\"></span>\n            <div class=\"employee-details\">\n\n              <div class=\"tab-content\">\n\n                <div id=\"total\" class=\"tab-pane fade in active text-center content-of-employee\">\n                  <div class=\"text-center\">\n                    <ul class=\"list-unstyled previous-score-list\">\n                      <li class=\"list-items\">\n                        <img src=\"../../../assets/images/directory_tabs/Performance/icon.png\" class=\"performance-icon\"\n                          alt=\"score-icon\">\n                      </li>\n                      <li class=\"list-items\">\n                        <h4 class=\"employee-score\">\n                          4.6\n                        </h4>\n                        <small class=\"year-performance\">2018 Performance Score</small>\n                      </li>\n                    </ul>\n                  </div>\n\n                </div>\n\n                <div id=\"employeeStatus\" class=\"tab-pane fade\">\n\n                </div>\n                <div id=\"location\" class=\"tab-pane fade\">\n\n                </div>\n                <div id=\"employeeType\" class=\"tab-pane fade\">\n                </div>\n                <div id=\"department\" class=\"tab-pane fade\">\n\n                </div>\n              </div>\n\n              <ul class=\"previous-tabs\">\n                <li class=\"active\"><a data-toggle=\"tab\" href=\"#total\">2017</a></li>\n                <li><a data-toggle=\"tab\" href=\"#employeeStatus\">2016</a></li>\n                <li><a data-toggle=\"tab\" href=\"#location\">2015</a></li>\n                <li><a data-toggle=\"tab\" href=\"#employeeType\">2014</a></li>\n                <li><a data-toggle=\"tab\" href=\"#department\">2013</a></li>\n                <li><a data-toggle=\"tab\" href=\"#department\">2012</a></li>\n              </ul>\n            </div>\n\n          </div>\n        </div>\n        <div id=\"thirdparty\" class=\"tab-pane fade\">\n          <div class=\"current-review-heading\">\n            <span class=\"green link-heading\">Links</span>\n            <span class=\"caret\"></span>\n            <div class=\"links-table\">\n              <div class=\"links-heading\">\n                <ul class=\"list-unstyled find-links pull-left col-md-6\">\n                  <li class=\"list-items col-md-2\">\n                    Find Links\n                  </li>\n                  <li class=\"list-items search-bar col-md-6\">\n                    <div class=\"search-border\">\n                      <input type=\"text\" class=\"border-none form-control\" placeholder=\"Search\">\n                    </div>\n                  </li>\n                </ul>\n                <span class=\"pull-right add-link\">\n                  <small class=\"green\"> +</small> Add Link\n                </span>\n                <span class=\"clearfix\"></span>\n              </div>\n              <div class=\"table-body\">\n                <span>No Link Information entries have been added.</span>\n              </div>\n            </div>\n          </div>\n\n        </div>\n        <div id=\"modern\" class=\"tab-pane fade\">\n          <div class=\"current-review-heading\">\n            <span class=\"green link-heading\">Reviews</span>\n            <span class=\"caret\"></span>\n            <div class=\"add-contact-list\">\n\n              <ul class=\"list-unstyled modern-list\">\n                <li class=\"list-items col-xs-7\">\n                  <label>How do you feel this week</label><br>\n                  <select class=\"form-control review-custom-width\" placeholder=\"country\">\n                    <option>United States</option>\n                    <option>India</option>\n                  </select>\n                </li>\n                <li class=\"list-items col-xs-7\">\n                  <label>What did you work on?</label><br>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Text message\">\n                </li>\n\n                <li class=\"list-items col-xs-7 birth-date\">\n                  <label>How close are you finishing?</label><br>\n                  <div class=\"finishing\">\n                    <input class=\"form-control date-input\"  #dpMDY=\"bsDatepicker\"\n                      bsDatepicker formControlName=\"myDateMDY\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                      placeholder=\"mm/dd/yyyy\" name=\"birth-date\">\n                    <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon\" alt=\"Company-settings icon\"\n                      (click)=\"dpMDY.toggle()\" [attr.aria-expanded]=\"dpMDY.isOpen\">\n                  </div>\n                </li>\n                <li class=\"list-items col-xs-7\">\n                  <label>Do you need support</label><br>\n                  <select class=\"form-control review-custom-width\" placeholder=\"country\">\n                    <option>Yes</option>\n                    <option>No</option>\n                  </select>\n                </li>\n                <li class=\"list-items col-xs-7\">\n                  <label>What was the most </label><br>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"text field\">\n                </li>\n                <li class=\"list-items col-xs-7\">\n                  <label>What was the most rewarding assignment you faced this week?</label><br>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"text field\">\n                </li>\n                <li class=\"list-items col-xs-7\">\n                  <label>What would you like learn more about at the position / company?</label><br>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"text field\">\n                </li>\n                <li class=\"list-items col-xs-7\">\n                    <label>What would you like to see changed at the position / company?</label><br>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"text field\">\n                  </li>\n                  <li class=\"list-items col-xs-7\">\n                      <label>NPS score</label><br>\n                      <select class=\"form-control review-custom-width\" placeholder=\"country\">\n                        <option>09</option>\n                        <option>08</option>\n                      </select>\n                    </li>\n              </ul>\n             \n            </div>\n          </div>\n          <div class=\"clearfix\"></div>\n          <div class=\"save-changes\">\n              <div class=\"margin-top\">\n                <ul class=\"submit-buttons list-unstyled\">\n                  <li class=\"list-item\">\n                    <button class=\"btn btn-success btn-style\"> Save Changes</button>\n                  </li>\n                  <li class=\"list-item\">\n                    <button class=\"btn btn-style\"> cancel </button>\n                  </li>\n                </ul>\n              </div>\n            </div>\n        </div>\n\n      </div>\n    </div>\n    <div class=\"clearfix\"></div>\n  </div>\n \n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/performance/performance/performance.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: PerformanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerformanceComponent", function() { return PerformanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PerformanceComponent = /** @class */ (function () {
    function PerformanceComponent() {
    }
    PerformanceComponent.prototype.ngOnInit = function () {
    };
    PerformanceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-performance',
            template: __webpack_require__(/*! ./performance.component.html */ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.html"),
            styles: [__webpack_require__(/*! ./performance.component.css */ "./src/app/admin-dashboard/employee-management/performance/performance/performance.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PerformanceComponent);
    return PerformanceComponent;
}());



/***/ })

}]);
//# sourceMappingURL=performance-performance-module.js.map