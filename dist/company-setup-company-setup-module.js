(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["company-setup-company-setup-module"],{

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.css":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.css ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".email-templates {\n    background: #f8f8f8;\n    padding: 15px;\n}\n.policy-bg{\n   background: #fff;\n   padding: 15px 20px;\n   margin-top: 50px;\n}\n.policy-heading {\n    margin-top: 15px;\n}\n.policy-title {\n    margin-bottom: 25px;\n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.mar-right-15 {\n    margin-right: 15px;\n}\n#fileUpload {\n    height: 0;\n    display: none;\n    width: 0;\n  }\n.width-100 {\n    width: 100%;\n    padding-top: 13px;\n    margin-left: -13px;\n}\na:focus {\n    outline: none\n}\na {\n  color: black;\n}\n.import-btn {\n    border: 1px solid #797d7b;\n    background-color: #797d7b;\n    color: #fff;\n    border-radius: 4px;\n    outline: none;\n  \n}\n.align-btn {\n    margin: 30px 0px;\n}\n.training-modal .modal-dialog { width: 50%;}\n.training-modal .modal-content { border-radius: 5px !important;}\n.training-modal .modal-header {padding: 15px 15px 20px;}\n.training-modal .modal-header h4 { padding: 0 0 0 30px;}\n.training-class { margin:30px 30px 40px;}\n.training-class ul { display: block; border-bottom:#ccc 1px solid; padding: 0 0 30px; margin: 0 0 30px;}\n.training-class ul li { margin: 0 0 40px; width:35%;}\n.training-class ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-class ul li .form-control{ border: none; box-shadow: none; background: #f8f8f8;padding: 10px 12px; height: auto;}\n.training-class ul li .form-control:focus{ box-shadow: none;}\n.training-class ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#484848;\n  }\n.training-class ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484848;\n  }\n.training-class ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484848;\n  }\n.training-class ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484848;\n  }\n.training-class .cancel { color: #E85453; font-size: 15px; float: left; border: none; outline: none; background: transparent; margin: 6px 0 0;}\n.training-class .save { color: #fff; font-size: 15px; float: right;background: #099247; padding: 10px 40px; border-radius: 30px;border: none; outline: none;}\n.error {\n  color: #e85e5e;\n}\n.download-icon {\n  color: #ab8686;\n  position: absolute;\n  left: 15px;\n}\n.upload {\n  color: #fff;\n  background: #099247;\n  outline: none;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"policy-bg\">\n  <div class=\"policy-heading\">   \n    <h4 class=\"policy-title\">\n    Company Policies\n \n    <span class=\"pull-right\">\n      <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n      <mat-menu #menu=\"matMenu\" class=\"option-position\">\n\n          <button *ngIf=\"!checkIds.length >=1\" mat-menu-item class=\"edit-color\" >\n              <a data-toggle=\"modal\" data-target=\"#myModal\">Add</a>\n          </button>\n      \n        <button mat-menu-item class=\"edit-color\" *ngIf=\"(!checkIds.length <=0) && (checkIds.length ==1)\">\n            <a (click)=\"editCompanyPolicy()\" >Edit</a>\n            <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit></a>\n        </button>\n        <button *ngIf=\"(!checkIds.length <=0) && (checkIds.length >=1)\" mat-menu-item class=\"edit-color\" (click)=\"deleteCompanyPolicy()\">Delete</button>\n      </mat-menu>\n    </span>   </h4>\n\n  </div>\n \n      <div class=\"form-group\">\n        <p class=\"email-templates\" *ngFor=\"let file of getFileName\" [ngClass]=\"{'zenwork-checked-border': file.isChecked}\">\n            <span class=\"mar-right-15\">\n                <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"file.isChecked\" (change)=\"checkedIds($event, file._id)\">\n                </mat-checkbox>\n              </span>\n          \n                  {{file.givenFileName}}\n               \n              <span style=\"position: relative\">\n                  <a href=\"{{file.url}}\" target=\"_blank\">\n                  <i class=\"material-icons download-icon\">\n                      cloud_download\n                      </i>\n                    </a>\n              </span>   \n        </p>\n\n      </div>\n    </div>\n\n\n    <div class=\"training-modal\">\n\n      <!-- Modal -->\n      <div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n          <div class=\"modal-dialog\">\n        \n            <!-- Modal content-->\n            <div class=\"modal-content\">\n              <div class=\"modal-header\">\n               <h4>Upload Company Policy</h4>\n              </div>\n              <div class=\"modal-body\">\n                \n                <div class=\"training-class\">\n                 <form (ngSubmit)=\"uploadPolicy(policyForm)\" #policyForm=\"ngForm\">\n                  <ul>\n      \n                    <li>\n                        <label>Select File*</label>\n                        <input type=\"file\" id=\"fileUpload\" accept=\".doc,.docx,application/msword,.xlsx, .xls,application/pdf\" (change)=\"fileChangeEvent($event)\" [(ngModel)]=\"files\" name=\"emailTemplate\"  #emailTemplate=\"ngModel\" required>\n                        <label for=\"fileUpload\" class=\"browse-btn\">\n                      <a mat-menu-item class=\"edit-color upload\">Browse</a>   \n                       </label>\n                       <span *ngIf=\"fileName\">{{fileName}}</span>\n                       <p *ngIf=\"!fileName && isValid\" class=\"error\"> Please select one file to upload </p>\n\n                    </li>\n                    \n                    \n                    <li>\n                        <label>Policy Name*</label>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"givenFileName\" name=\"emailFileName\" #emailFileName=\"ngModel\" (keypress)=\"alphabets($event)\" maxlength=\"40\" minlength=\"40\" required>\n                      <p *ngIf=\"!givenFileName && emailFileName.touched || (!givenFileName && isValid)\" class=\"error\">Please provide policy name</p>\n\n                    </li>\n                    \n                  </ul>\n                  \n      \n                  <button type=\"submit\" class=\"cancel\" data-dismiss=\"modal\" (click)=\"cancelPolicies()\">Cancel</button>\n                  <button type=\"submit\" class=\"save\" >Submit</button>\n                  <div class=\"clearfix\"></div>\n                </form>\n                </div>\n               \n              \n              </div>\n              \n            </div>\n        \n          </div>\n        </div>\n      \n      </div>\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: CompanyPolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyPolicyComponent", function() { return CompanyPolicyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CompanyPolicyComponent = /** @class */ (function () {
    function CompanyPolicyComponent(companySettingsService, swalAlertService) {
        this.companySettingsService = companySettingsService;
        this.swalAlertService = swalAlertService;
        this.selectedPolicy = [];
        this.isValid = false;
        this.getFileName = [
            {
                isChecked: '',
                givenFileName: '',
                url: ''
            }
        ];
        this.checkIds = [];
    }
    CompanyPolicyComponent.prototype.ngOnInit = function () {
        this.getUploadPolicies();
    };
    // Author: Saiprakash , Date: 15/04/19
    // Get files for company policies
    CompanyPolicyComponent.prototype.getUploadPolicies = function () {
        var _this = this;
        this.companySettingsService.getUploadPolicies().subscribe(function (res) {
            console.log(res);
            _this.getFileName = res.data;
            console.log(_this.getFileName);
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    CompanyPolicyComponent.prototype.fileChangeEvent = function (e) {
        this.files = e.target.files[0];
        console.log(this.files.name);
        this.fileName = this.files.name;
    };
    // Author: Saiprakash , Date: 15/04/19
    // Upload file and edit for company policies
    CompanyPolicyComponent.prototype.uploadPolicy = function (policyForm) {
        var _this = this;
        var formData = new FormData();
        formData.append('file', this.files);
        formData.append('givenFileName', this.givenFileName.trim());
        this.isValid = true;
        if (this.fileName && this.givenFileName) {
            if (this.policyData && this.policyData._id) {
                formData.append('_id', this.policyData._id);
                formData.append('s3Path', this.policyData.s3Path);
                formData.append('originalFilename', this.fileName);
                this.companySettingsService.editUploadPolicy(formData).subscribe(function (res) {
                    console.log(res);
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.checkIds = [];
                    _this.policyData._id = "";
                    policyForm.resetForm();
                    _this.isValid = false;
                    _this.getUploadPolicies();
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.companySettingsService.uploadPolicy(formData).subscribe(function (res) {
                    console.log(res);
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.checkIds = [];
                    policyForm.resetForm();
                    _this.getUploadPolicies();
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
        }
    };
    CompanyPolicyComponent.prototype.checkedIds = function (event, id) {
        if (event.checked == true) {
            this.checkIds.push(id);
        }
        console.log(this.checkIds);
        if (event.checked == false) {
            var index = this.checkIds.indexOf(id);
            if (index > -1) {
                this.checkIds.splice(index, 1);
            }
            console.log(this.checkIds);
        }
    };
    CompanyPolicyComponent.prototype.editCompanyPolicy = function () {
        var _this = this;
        this.selectedPolicy = this.getFileName.filter(function (policyData) {
            return policyData.isChecked;
        });
        if (this.selectedPolicy.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Policy", "Select only one policy on to Proceed", 'error');
        }
        else if (this.selectedPolicy.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Policy", "Select a policy on to Proceed", 'error');
        }
        else {
            this.openEdit.nativeElement.click();
            this.companySettingsService.getsinglePolicy(this.selectedPolicy[0]._id).subscribe(function (res) {
                console.log(res);
                _this.policyData = res.data;
                _this.fileName = res.data.originalFilename;
                _this.givenFileName = res.data.givenFileName;
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash , Date: 15/04/19
    // Company policies delete
    CompanyPolicyComponent.prototype.deleteCompanyPolicy = function () {
        var _this = this;
        this.selectedPolicy = this.getFileName.filter(function (policyData) {
            return policyData.isChecked;
        });
        if (this.selectedPolicy.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete Policy", "Select a policy to Proceed", 'error');
        }
        else {
            this.companySettingsService.deletePolicies(this.selectedPolicy).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Record deleted successfully', "", 'success');
                _this.checkIds = [];
                _this.getUploadPolicies();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    CompanyPolicyComponent.prototype.cancelPolicies = function () {
        this.givenFileName = "";
        this.fileName = "";
        this.isValid = false;
    };
    CompanyPolicyComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CompanyPolicyComponent.prototype, "openEdit", void 0);
    CompanyPolicyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-company-policy',
            template: __webpack_require__(/*! ./company-policy.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.html"),
            styles: [__webpack_require__(/*! ./company-policy.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.css")]
        }),
        __metadata("design:paramtypes", [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__["CompanySettingsService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"]])
    ], CompanyPolicyComponent);
    return CompanyPolicyComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.css":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n/*--------- This is my code -------*/\n\n.zenwork-setting { display: block;}\n\n.zenwork-tabs {display: block;padding: 20px 0 0;margin: 0 auto; float: none;}\n\n.zenwork-tabs .tab-content { margin:40px 0 0;}\n\n.zenwork-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border: none;background: transparent;}\n\n.zenwork-tabs .nav-tabs {border-bottom:#dbdbdb 2px solid;}\n\n.zenwork-tabs .nav-tabs > li { margin: 0 30px;}\n\n.zenwork-tabs .nav-tabs > li > a {font-size: 18px;line-height: 17px;font-weight:normal;color:#484747;border: none;padding: 10px 0 15px !important;margin: 0;}\n\n.zenwork-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;}\n\n.zenwork-tabs .nav-tabs > li.active{border-bottom:#439348 4px solid;margin: 0px 30px -1px 30px;color:#000;}\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n      <span class=\"green mr-7\">\n        <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n      </span>Back\n    </button>\n    <small>\n      <img src=\"../../../assets/images/company-settings/company-setup/company-setup.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n    </small>\n    <em>Company Setup</em>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n\n\n<div class=\"zenwork-tabs col-md-11\">\n\n  <ul class=\"nav nav-tabs\">\n    <li  routerLinkActive=\"active\" style=\"margin-left:0\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/general-settings\">General Settings</a>\n    </li>\n  \n    <li  routerLinkActive=\"active\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/field-maintenance\">Field Maintenance</a>\n    </li>\n    <li  routerLinkActive=\"active\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/structure\">Structure</a>\n    </li>\n    <li  routerLinkActive=\"active\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/training\">Training</a>\n    </li>\n    <li  routerLinkActive=\"active\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/payroll\">Payroll</a>\n    </li>\n    <li  routerLinkActive=\"active\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/email-templates\">Email Templates</a>\n    </li>\n    <li  routerLinkActive=\"active\">\n      <a routerLink=\"/admin/admin-dashboard/company-settings/company-setup/company-policy\">Company Policy</a>\n    </li>\n\n\n  </ul>\n<div >\n  <router-outlet></router-outlet>\n</div>\n\n</div>\n\n\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: CompanySetupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanySetupComponent", function() { return CompanySetupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { Router } from '@angular/router';





var CompanySetupComponent = /** @class */ (function () {
    function CompanySetupComponent(modalService, router, loaderService, companySetupService, swalAlertService, messageService) {
        var _this = this;
        this.modalService = modalService;
        this.router = router;
        this.loaderService = loaderService;
        this.companySetupService = companySetupService;
        this.swalAlertService = swalAlertService;
        this.messageService = messageService;
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
        this.subscription = this.messageService.getMessage().subscribe(function (message) {
            _this.message = message.text;
            console.log("message", _this.message);
            _this.activeTab = _this.message;
        });
    }
    CompanySetupComponent.prototype.ngOnInit = function () {
    };
    CompanySetupComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationStart"]) {
            this.loaderService.loader(true);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"]) {
            this.loaderService.loader(false);
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationCancel"]) {
            this.loaderService.loader(false);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationError"]) {
            this.loaderService.loader(false);
        }
    };
    CompanySetupComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    CompanySetupComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/company-settings']);
    };
    CompanySetupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-company-setup',
            template: __webpack_require__(/*! ./company-setup.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.html"),
            styles: [__webpack_require__(/*! ./company-setup.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], CompanySetupComponent);
    return CompanySetupComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/company-setup.module.ts ***!
  \****************************************************************************************/
/*! exports provided: CompanySetupModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanySetupModule", function() { return CompanySetupModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _company_setup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./company-setup.component */ "./src/app/admin-dashboard/company-settings/company-setup/company-setup.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _general_settings_general_settings_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./general-settings/general-settings.component */ "./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.ts");
/* harmony import */ var _field_maintenance_field_maintenance_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./field-maintenance/field-maintenance.component */ "./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.ts");
/* harmony import */ var _structure_structure_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./structure/structure.component */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.ts");
/* harmony import */ var _training_training_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./training/training.component */ "./src/app/admin-dashboard/company-settings/company-setup/training/training.component.ts");
/* harmony import */ var _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./payroll/payroll.component */ "./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.ts");
/* harmony import */ var _email_templates_email_templates_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./email-templates/email-templates.component */ "./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.ts");
/* harmony import */ var _company_policy_company_policy_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./company-policy/company-policy.component */ "./src/app/admin-dashboard/company-settings/company-setup/company-policy/company-policy.component.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _structure_structure_field_dialog_structure_field_dialog_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./structure/structure-field-dialog/structure-field-dialog.component */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.ts");
/* harmony import */ var _training_assign_employees_assign_employees_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./training/assign-employees/assign-employees.component */ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
/* harmony import */ var _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../../shared-module/shared.module */ "./src/app/shared-module/shared.module.ts");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var routes = [
    {
        path: '', component: _company_setup_component__WEBPACK_IMPORTED_MODULE_2__["CompanySetupComponent"], children: [
            { path: '', redirectTo: 'general-settings', pathMatch: 'full' },
            { path: 'general-settings', component: _general_settings_general_settings_component__WEBPACK_IMPORTED_MODULE_6__["GeneralSettingsComponent"] },
            { path: 'field-maintenance', component: _field_maintenance_field_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["FieldMaintenanceComponent"] },
            { path: 'structure', component: _structure_structure_component__WEBPACK_IMPORTED_MODULE_8__["StructureComponent"] },
            { path: 'training', component: _training_training_component__WEBPACK_IMPORTED_MODULE_9__["TrainingComponent"] },
            { path: 'payroll', component: _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_10__["PayrollComponent"] },
            { path: 'email-templates', component: _email_templates_email_templates_component__WEBPACK_IMPORTED_MODULE_11__["EmailTemplatesComponent"] },
            { path: 'company-policy', component: _company_policy_company_policy_component__WEBPACK_IMPORTED_MODULE_12__["CompanyPolicyComponent"] }
        ]
    },
];
var CompanySetupModule = /** @class */ (function () {
    function CompanySetupModule() {
    }
    CompanySetupModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_13__["MatCheckboxModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_14__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_15__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_16__["MatIconModule"], _angular_material_paginator__WEBPACK_IMPORTED_MODULE_17__["MatPaginatorModule"], _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_18__["MaterialModuleModule"],
                _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_23__["SharedModule"],
                angular_dual_listbox__WEBPACK_IMPORTED_MODULE_24__["AngularDualListBoxModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_21__["NgMultiSelectDropDownModule"].forRoot(), ngx_mask__WEBPACK_IMPORTED_MODULE_22__["NgxMaskModule"].forRoot({
                    showMaskTyped: true,
                })
            ],
            declarations: [_company_setup_component__WEBPACK_IMPORTED_MODULE_2__["CompanySetupComponent"],
                _general_settings_general_settings_component__WEBPACK_IMPORTED_MODULE_6__["GeneralSettingsComponent"],
                _field_maintenance_field_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["FieldMaintenanceComponent"],
                _structure_structure_component__WEBPACK_IMPORTED_MODULE_8__["StructureComponent"],
                _training_training_component__WEBPACK_IMPORTED_MODULE_9__["TrainingComponent"],
                _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_10__["PayrollComponent"],
                _email_templates_email_templates_component__WEBPACK_IMPORTED_MODULE_11__["EmailTemplatesComponent"],
                _company_policy_company_policy_component__WEBPACK_IMPORTED_MODULE_12__["CompanyPolicyComponent"],
                _structure_structure_field_dialog_structure_field_dialog_component__WEBPACK_IMPORTED_MODULE_19__["StructureFieldDialogComponent"],
                _training_assign_employees_assign_employees_component__WEBPACK_IMPORTED_MODULE_20__["AssignEmployeesComponent"]],
            entryComponents: [
                _structure_structure_field_dialog_structure_field_dialog_component__WEBPACK_IMPORTED_MODULE_19__["StructureFieldDialogComponent"],
                _training_assign_employees_assign_employees_component__WEBPACK_IMPORTED_MODULE_20__["AssignEmployeesComponent"]
            ]
        })
    ], CompanySetupModule);
    return CompanySetupModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.css":
/*!**************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".field-accordion p {color: #565555;font-size: 16px;}\n.field-accordion { padding: 0;}\n.field-accordion .panel-default>.panel-heading { padding: 20px 0 20px; background-color:transparent !important; border: none;}\n.field-accordion .panel-title { color: #3c3c3c; display: inline-block; font-size:18px;}\n.field-accordion .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.field-accordion .panel-title a:focus{color: #3c3c3c;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:6px 10px 0; font-size: 12px; line-height: 12px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n.field-accordion .panel-heading { position: relative;}\n.field-accordion .panel-default {\n    background: #fff;\n    padding: 10px 20px;\n    \n}\n.email-templates {\n    background: #f8f8f8;\n    padding: 15px;\n}\n.import-btn {\n    border: 1px solid #797d7b;\n    background-color: #797d7b;\n    color: #fff;\n    border-radius: 4px;\n    outline: none;\n  \n}\n.align-btn {\n    margin: 30px 0px;\n}\n.zenwork-checked-border:after { \n  content: '';\n  position: absolute;\n  top: 0;\n  left: -50px;\n  border-right: #83c7a1 4px solid;\n  height: 100%;\n  border-radius: 4px;\n  padding: 25px;\n    }\n.mar-right-15 {\n    margin-right: 15px;\n}\n.training-modal .modal-dialog { width: 50%;}\n.training-modal .modal-content { border-radius: 5px !important;}\n.training-modal .modal-header {padding: 15px 15px 20px;}\n.training-modal .modal-header h4 { padding: 0 0 0 30px;}\n.training-class { margin:30px 30px 40px;}\n.training-class ul { display: block; border-bottom:#ccc 1px solid; padding: 0 0 30px; margin: 0 0 30px;}\n.training-class ul li { margin: 0 0 40px; width:35%;}\n.training-class ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-class ul li .form-control{ border: none; box-shadow: none; background: #f8f8f8;padding: 10px 12px; height: auto;}\n.training-class ul li .form-control:focus{ box-shadow: none;}\n.training-class ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#484848;\n  }\n.training-class ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484848;\n  }\n.training-class ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484848;\n  }\n.training-class ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484848;\n  }\n.training-class .cancel { color: #E85453; font-size: 15px; float: left; border: none; outline: none; background: transparent; margin: 6px 0 0;}\n.training-class .save { color: #fff; font-size: 15px; float: right;background: #099247; padding: 10px 40px; border-radius: 30px;border: none; outline: none;}\n#fileUpload {\n  height: 0;\n  display: none;\n  width: 0;\n}\n.width-100 {\n  width: 100%;\n  padding-top: 10px;\n}\na:focus {\n  outline: none\n}\na {\n  color: #000;\n}\n.buttons{\n  margin: 30px 0 0;\n}\n.buttons ul li{\n  display: inline-block;\n  padding: 0px 10px;\n}\n.buttons ul li .back{\n  border: 1px solid gray;\n  padding: 7px 30px;\n  border-radius: 31px;\n  color: #000;\n  cursor: pointer;\n}\n.buttons ul li .save{\n\n  border: 1px solid gray;\n  padding: 7px 30px;\n  border-radius: 31px;\n  color: #fff;\nbackground: #099247;font-size: 16px;\n}\n.buttons ul li .cancel{\n  color:#e5423d;\n  cursor: pointer;\n}\n.error {\n  color: #e85e5e;\n}\n.download-icon {\n  color: #ab8686;\n  position: absolute;\n  left: 15px;\n}\n.upload {\n  color: #fff;\n  background: #099247;\n  outline: none;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.html":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"align-btn\">\n  <button mat-raised-button class=\"import-btn\" data-toggle=\"modal\" data-target=\"#myModal\">Load Email Templates</button>\n</div>\n\n<div class=\"field-accordion\">\n\n  <div class=\"panel-group\" id=\"accordion1\">\n\n    <div class=\"panel panel-default border-bottom\">\n      <div class=\"panel-heading\">\n         \n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n            Employee Management\n          </a></h4>\n        <span class=\"pull-right\">\n          <mat-icon *ngIf=\"emailTemplate1.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n          <mat-menu #menu1=\"matMenu\" class=\"option-position\">\n            <button *ngIf=\"(!emailTemplate1.length <=0) && (emailTemplate1.length ==1)\" mat-menu-item class=\"edit-color\" >\n                <a (click)=\"editEmailTemplate1()\">Edit</a>\n                <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit1></a>\n              </button> \n            <button *ngIf=\"(!emailTemplate1.length <=0) && (emailTemplate1.length >=1)\" mat-menu-item class=\"edit-color\" (click)=\"deleteEmailTemplate()\">Delete</button>\n          </mat-menu>\n        </span>\n\n      </div>\n      <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n          <div class=\"form-group\">\n            <p *ngFor= \"let employee of employeeManagement\" class=\"email-templates\" [ngClass]=\"{'zenwork-checked-border': employee.isChecked}\">\n                <span class=\"mar-right-15\">\n                    <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"employee.isChecked\" (change)=\"checkEmailTemplate1($event, employee._id)\">\n                    </mat-checkbox>\n                  </span>\n                 \n                    {{employee.givenFileName}}\n                \n                <span style=\"position: relative\">\n                    <a href=\"{{employee.url}}\" target=\"_blank\">\n                    <i class=\"material-icons download-icon\">\n                        cloud_download\n                        </i>\n                      </a>\n                </span>\n            </p>\n\n          </div>\n\n        </div>\n\n      </div>\n    </div>\n  \n\n      <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#company-info\">\n              Recruitment\n            </a>\n          </h4>\n          <span class=\"pull-right\">\n              <mat-icon *ngIf=\"emailTemplate2.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n              <mat-menu #menu2=\"matMenu\" class=\"option-position\">\n                <button *ngIf=\"(!emailTemplate2.length <=0) && (emailTemplate2.length ==1)\" mat-menu-item class=\"edit-color\">\n                    <a (click)=\"editEmailTemplate2()\">Edit</a>\n                <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit2></a>\n                </button>\n                <button *ngIf=\"(!emailTemplate2.length <=0) && (emailTemplate2.length >=1)\" mat-menu-item class=\"edit-color\" (click)=\"deleteEmailTemplate()\">Delete</button>\n              </mat-menu>\n            </span>\n        </div>\n        <div id=\"company-info\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n              <div class=\"form-group\">\n                <p class=\"email-templates\" *ngFor=\"let recruitment of recruitments\" [ngClass]=\"{'zenwork-checked-border': recruitment.isChecked}\">\n                  <span class=\"mar-right-15\">\n                      <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"recruitment.isChecked\" (change)=\"checkEmailTemplate2($event, recruitment._id)\">\n                      </mat-checkbox>\n                    </span>\n                   \n                      {{recruitment.givenFileName}}\n                  \n                  <span style=\"position: relative\">\n                      <a href=\"{{recruitment.url}}\" target=\"_blank\">\n                      <i class=\"material-icons download-icon\">\n                          cloud_download\n                          </i>\n                        </a>\n                  </span>\n              \n              </p>\n      \n                </div>\n          </div>\n        </div>\n      </div>\n\n\n      <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#company-info1\">\n              Benefits Administration\n            </a>\n          </h4>\n          <span class=\"pull-right\">\n              <mat-icon *ngIf=\"emailTemplate3.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu3\">more_vert</mat-icon>\n              <mat-menu #menu3=\"matMenu\" class=\"option-position\">\n                <button *ngIf=\"(!emailTemplate3.length <=0) && (emailTemplate3.length ==1)\" mat-menu-item class=\"edit-color\">\n                    <a (click)=\"editEmailTemplate3()\">Edit</a>\n                    <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit3></a>\n                </button>\n                <button *ngIf=\"(!emailTemplate3.length <=0) && (emailTemplate3.length >=1)\" mat-menu-item class=\"edit-color\" (click)=\"deleteEmailTemplate()\">Delete</button>\n              </mat-menu>\n            </span>\n        </div>\n        <div id=\"company-info1\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n              <div class=\"form-group\">\n                <p class=\"email-templates\" *ngFor=\"let benefits of benefitsAdministration\" [ngClass]=\"{'zenwork-checked-border': benefits.isChecked}\">\n                  <span class=\"mar-right-15\">\n                      <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"benefits.isChecked\" (change)=\"checkEmailTemplate3($event, benefits._id)\">\n                      </mat-checkbox>\n                    </span>\n                  \n                      {{benefits.givenFileName}}\n                  \n                  <span style=\"position: relative\">\n                      <a href=\"{{benefits.url}}\" target=\"_blank\">\n                      <i class=\"material-icons download-icon\">\n                          cloud_download\n                          </i>\n                        </a>\n                  </span>\n                  \n              </p>\n                </div>\n          </div>\n        </div>\n      </div>\n    \n\n      <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#company-info2\">\n              Performance Management\n            </a>\n          </h4>\n          <span class=\"pull-right\">\n              <mat-icon *ngIf=\"emailTemplate4.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu4\">more_vert</mat-icon>\n              <mat-menu #menu4=\"matMenu\" class=\"option-position\">\n                <button *ngIf=\"(!emailTemplate4.length <=0) && (emailTemplate4.length ==1)\" mat-menu-item class=\"edit-color\">\n                    <a (click)=\"editEmailTemplate4()\">Edit</a>\n                <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit4></a>\n                </button>\n                <button *ngIf=\"(!emailTemplate4.length <=0) && (emailTemplate4.length >=1)\" mat-menu-item class=\"edit-color\" (click)=\"deleteEmailTemplate()\">Delete</button>\n              </mat-menu>\n            </span>\n        </div>\n        <div id=\"company-info2\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n              <div class=\"form-group\">\n                <p class=\"email-templates\" *ngFor=\"let performance of performanceManagement\" [ngClass]=\"{'zenwork-checked-border': performance.isChecked}\" >\n                  <span class=\"mar-right-15\">\n                      <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"performance.isChecked\" (change)=\"checkEmailTemplate4($event, performance._id)\">\n                      </mat-checkbox>\n                    </span>\n                    \n                      {{performance.givenFileName}}\n                  \n                  <span style=\"position: relative\">\n                      <a href=\"{{performance.url}}\" target=\"_blank\">\n                      <i class=\"material-icons download-icon\">\n                          cloud_download\n                          </i>\n                        </a>\n                  </span>\n              </p>\n                </div>\n          </div>\n        </div>\n      </div>\n    \n\n      <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#company-info3\">\n              Leave Management\n            </a>\n          </h4>\n          <span class=\"pull-right\">\n              <mat-icon *ngIf=\"emailTemplate5.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu5\">more_vert</mat-icon>\n              <mat-menu #menu5=\"matMenu\" class=\"option-position\">\n                <button *ngIf=\"(!emailTemplate5.length <=0) && (emailTemplate5.length ==1)\" mat-menu-item class=\"edit-color\">\n                    <a (click)=\"editEmailTemplate5()\">Edit</a>\n                <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit5></a>\n                </button>\n                <button *ngIf=\"(!emailTemplate5.length <=0) && (emailTemplate5.length >=1)\" mat-menu-item class=\"edit-color\" (click)=\"deleteEmailTemplate()\">Delete</button>\n              </mat-menu>\n            </span>\n        </div>\n        <div id=\"company-info3\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n              <div class=\"form-group\">\n                <p class=\"email-templates\" *ngFor=\"let leaves of leaveManagement\" [ngClass]=\"{'zenwork-checked-border': leaves.isChecked}\">\n                  <span class=\"mar-right-15\">\n                      <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"leaves.isChecked\" (change)=\"checkEmailTemplate5($event, leaves._id)\">\n                      </mat-checkbox>\n                    </span>\n                  \n                      {{leaves.givenFileName}}\n                  \n                  <span style=\"position: relative\">\n                      <a href=\"{{leaves.url}}\" target=\"_blank\">\n                      <i class=\"material-icons download-icon\">\n                          cloud_download\n                          </i>\n                        </a>\n                  </span>\n              </p>\n                </div>\n          </div>\n        </div>\n      </div>\n    \n    \n  </div>\n\n</div>\n\n\n<div class=\"training-modal\">\n\n  <!-- Modal -->\n  <div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n    \n        <!-- Modal content-->\n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n           <h4>Load Email Templates</h4>\n          </div>\n          <div class=\"modal-body\">\n            \n            <div class=\"training-class\">\n             <form (ngSubmit)=\"uploadEmailTemplate(emailForm)\" #emailForm=\"ngForm\">\n              <ul>\n  \n                <li>\n                    <label>Upload a file*</label>\n\n                    <input type=\"file\" id=\"fileUpload\" accept=\".doc,.docx,application/msword,.xlsx, .xls,application/pdf\" (change)=\"fileChangeEvent($event)\" [(ngModel)]=\"files\" name=\"emailTemplate\"  #emailTemplate=\"ngModel\" required>\n                    <label for=\"fileUpload\" class=\"browse-btn\">\n                  <a mat-menu-item class=\"edit-color upload\">Browse</a>   \n                   </label>\n                   <span *ngIf=\"fileName\">{{fileName}}</span>\n                    \n                    <p *ngIf=\"!fileName && isValid\" class=\"error\">Please select one file to upload</p>\n\n                </li>\n                \n                <li>\n                    <label>Select Module to store file*</label>\n                    <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Select Module\" [(ngModel)]=\"module\" name=\"selctModule\" #selctModule =\"ngModel\" (ngModelChange)=\"moduleChange()\" required>\n     \n                        <mat-option value=\"Employee Management\">Employee Management</mat-option>\n                        <mat-option value=\"Recruitment\">Recruitment</mat-option>\n                        <mat-option value=\"Benefits Administration\"> Benefits Administration</mat-option>\n                        <mat-option value=\"Performance Management\">Performance Management</mat-option>\n                        <mat-option value=\"Leave Management\">Leave Management</mat-option>\n\n                    </mat-select>\n                    <p *ngIf=\"!module && selctModule.touched || (!module && isValid)\" class=\"error\">Please select one module</p>\n\n                </li>\n                \n                <li>\n                    <label>Name Email Templates*</label>\n                    <input type=\"text\" class=\"form-control\" (keypress)=\"alphabets($event)\" [(ngModel)] =\"givenFileName\" name=\"emailFileName\" #emailFileName=\"ngModel\" maxlength=\"40\" minlength=\"40\" required>\n                    <p *ngIf=\"!givenFileName && emailFileName.touched || (!givenFileName && isValid)\" class=\"error\">Please enter an email template name</p>\n\n                </li>\n                \n              </ul>\n              \n  \n              <button type=\"submit\" class=\"cancel\" data-dismiss=\"modal\" (click)=\"cancelEmailForm()\">Cancel</button>\n              <button type=\"submit\" class=\"save\">Submit</button>\n              <div class=\"clearfix\"></div>\n            </form>\n            </div>\n           \n          \n          </div>\n          \n        </div>\n    \n      </div>\n    </div>\n  \n  </div>\n  <div class=\"buttons\">\n    <ul class=\"list-unstyled\">\n     \n      <li class=\"list-items pull-right\">\n        <button class='save' type=\"submit\" (click)=\"saveandNext()\">\n          Save Changes\n        </button>\n      </li>\n  \n      <div class=\"clearfix\"></div>\n    </ul>\n  </div>\n  <!-- <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit></a> -->\n"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: EmailTemplatesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailTemplatesComponent", function() { return EmailTemplatesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shared-module/confirmation/confirmation.component */ "./src/app/shared-module/confirmation/confirmation.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EmailTemplatesComponent = /** @class */ (function () {
    function EmailTemplatesComponent(companySettingsService, swalAlertService, router, dialog) {
        this.companySettingsService = companySettingsService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.dialog = dialog;
        this.checkedStructure = {
            checked: ''
        };
        this.selectedEmailTemplate = [];
        this.selectedRecruitmentTemplate = [];
        this.selectedAdministateTemplate = [];
        this.selectedPerformanceTemplate = [];
        this.selectedLeaveTemplate = [];
        this.employeeManagement = [];
        this.recruitments = [];
        this.benefitsAdministration = [];
        this.performanceManagement = [];
        this.leaveManagement = [];
        this.isValid = false;
        this.emailTemplate1 = [];
        this.emailTemplate2 = [];
        this.emailTemplate3 = [];
        this.emailTemplate4 = [];
        this.emailTemplate5 = [];
    }
    EmailTemplatesComponent.prototype.ngOnInit = function () {
        this.getAllEmialTempates();
    };
    // Author: Saiprakash , Date: 15/04/19
    // Upload email templates
    EmailTemplatesComponent.prototype.fileChangeEvent = function (e) {
        this.files = e.target.files[0];
        console.log(this.files.name);
        this.fileName = this.files.name;
    };
    EmailTemplatesComponent.prototype.getAllEmialTempates = function () {
        var _this = this;
        this.companySettingsService.getAllEmialTempates()
            .subscribe(function (res) {
            console.log(res);
            _this.employeeManagement = res.grouped['Employee Management'];
            _this.recruitments = res.grouped['Recruitment'];
            _this.benefitsAdministration = res.grouped['Benefits Administration'];
            _this.performanceManagement = res.grouped['Performance Management'];
            _this.leaveManagement = res.grouped['Leave Management'];
        }, function (err) {
            console.log(err);
        });
    };
    EmailTemplatesComponent.prototype.moduleChange = function () {
        console.log(this.module);
    };
    // Author: Saiprakash , Date: 15/04/19
    // Upload for email templates
    EmailTemplatesComponent.prototype.uploadEmailTemplate = function (emailForm) {
        var _this = this;
        var formData = new FormData();
        formData.append('file', this.files);
        formData.append('module', this.module);
        formData.append('givenFileName', this.givenFileName.trim());
        this.isValid = true;
        if (this.fileName && this.module && this.givenFileName) {
            if (this.emailData && this.emailData._id) {
                formData.append('_id', this.emailData._id);
                formData.append('s3Path', this.emailData.s3Path);
                formData.append('originalFilename', this.fileName);
                this.companySettingsService.editEmailTemplate(formData).subscribe(function (res) {
                    console.log(res);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.isValid = false;
                    emailForm.resetForm();
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.module = '';
                    _this.emailTemplate1 = [];
                    _this.emailData._id = '';
                    _this.getAllEmialTempates();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else if (this.recruitmentData && this.recruitmentData._id) {
                formData.append('_id', this.recruitmentData._id);
                formData.append('s3Path', this.recruitmentData.s3Path);
                formData.append('originalFilename', this.fileName);
                this.companySettingsService.editEmailTemplate(formData).subscribe(function (res) {
                    console.log(res);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.isValid = false;
                    emailForm.resetForm();
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.module = '';
                    _this.emailTemplate2 = [];
                    _this.recruitmentData._id = '';
                    _this.getAllEmialTempates();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else if (this.administatetData && this.administatetData._id) {
                formData.append('_id', this.administatetData._id);
                formData.append('s3Path', this.administatetData.s3Path);
                formData.append('originalFilename', this.fileName);
                this.companySettingsService.editEmailTemplate(formData).subscribe(function (res) {
                    console.log(res);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.isValid = false;
                    emailForm.resetForm();
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.module = '';
                    _this.emailTemplate3 = [];
                    _this.administatetData._id = '';
                    _this.getAllEmialTempates();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else if (this.performanceEmailData && this.performanceEmailData._id) {
                formData.append('_id', this.performanceEmailData._id);
                formData.append('s3Path', this.performanceEmailData.s3Path);
                formData.append('originalFilename', this.fileName);
                this.companySettingsService.editEmailTemplate(formData).subscribe(function (res) {
                    console.log(res);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.isValid = false;
                    emailForm.resetForm();
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.module = '';
                    _this.emailTemplate4 = [];
                    _this.performanceEmailData._id = '';
                    _this.getAllEmialTempates();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else if (this.leaveEmailData && this.leaveEmailData._id) {
                formData.append('_id', this.leaveEmailData._id);
                formData.append('s3Path', this.leaveEmailData.s3Path);
                formData.append('originalFilename', this.fileName);
                this.companySettingsService.editEmailTemplate(formData).subscribe(function (res) {
                    console.log(res);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.isValid = false;
                    emailForm.resetForm();
                    jQuery("#myModal").modal("hide");
                    _this.fileName = "";
                    _this.givenFileName = '';
                    _this.module = '';
                    _this.emailTemplate5 = [];
                    _this.leaveEmailData._id = '';
                    _this.getAllEmialTempates();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.companySettingsService.addEmailTemplates(formData).subscribe(function (res) {
                    console.log(res);
                    jQuery("#myModal").modal("hide");
                    _this.fileName = '';
                    _this.givenFileName = '';
                    _this.module = '';
                    _this.isValid = false;
                    emailForm.resetForm();
                    _this.emailTemplate1 = [];
                    _this.emailTemplate2 = [];
                    _this.emailTemplate3 = [];
                    _this.emailTemplate4 = [];
                    _this.emailTemplate5 = [];
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Email template uploaded successfully", "", "success");
                    _this.getAllEmialTempates();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
        }
    };
    // Author: Vipin Reddy , Date: 16/04/19
    // next page moving
    EmailTemplatesComponent.prototype.saveandNext = function () {
        this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/company-policy"]);
    };
    // Author: saiprakash , Date: 16/04/19
    // Edit single Emailtamplate
    EmailTemplatesComponent.prototype.editEmailTemplate1 = function () {
        var _this = this;
        this.selectedEmailTemplate = this.employeeManagement.filter(function (templateData) {
            return templateData.isChecked;
        });
        if (this.selectedEmailTemplate.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error');
        }
        else if (this.selectedEmailTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error');
        }
        else {
            this.openEdit1.nativeElement.click();
            this.companySettingsService.singleEmailTemplate(this.selectedEmailTemplate[0]._id).subscribe(function (res) {
                console.log(res);
                _this.emailData = res.data;
                _this.fileName = res.data.originalFilename;
                _this.givenFileName = res.data.givenFileName;
                _this.module = res.data.module;
            }, function (err) {
                console.log(err);
            });
        }
    };
    EmailTemplatesComponent.prototype.editEmailTemplate2 = function () {
        var _this = this;
        this.selectedRecruitmentTemplate = this.recruitments.filter(function (recruitData) {
            return recruitData.isChecked;
        });
        if (this.selectedRecruitmentTemplate.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error');
        }
        else if (this.selectedRecruitmentTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error');
        }
        else {
            this.openEdit2.nativeElement.click();
            this.companySettingsService.singleEmailTemplate(this.selectedRecruitmentTemplate[0]._id).subscribe(function (res) {
                console.log(res);
                _this.recruitmentData = res.data;
                _this.fileName = res.data.originalFilename;
                _this.givenFileName = res.data.givenFileName;
                _this.module = res.data.module;
            }, function (err) {
                console.log(err);
            });
        }
    };
    EmailTemplatesComponent.prototype.editEmailTemplate3 = function () {
        var _this = this;
        this.selectedAdministateTemplate = this.benefitsAdministration.filter(function (benefitData) {
            return benefitData.isChecked;
        });
        if (this.selectedAdministateTemplate.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error');
        }
        else if (this.selectedAdministateTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error');
        }
        else {
            this.openEdit3.nativeElement.click();
            this.companySettingsService.singleEmailTemplate(this.selectedAdministateTemplate[0]._id).subscribe(function (res) {
                console.log(res);
                _this.administatetData = res.data;
                _this.fileName = res.data.originalFilename;
                _this.givenFileName = res.data.givenFileName;
                _this.module = res.data.module;
            }, function (err) {
                console.log(err);
            });
        }
    };
    EmailTemplatesComponent.prototype.editEmailTemplate4 = function () {
        var _this = this;
        this.selectedPerformanceTemplate = this.performanceManagement.filter(function (performanceData) {
            return performanceData.isChecked;
        });
        if (this.selectedPerformanceTemplate.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error');
        }
        else if (this.selectedPerformanceTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error');
        }
        else {
            this.openEdit4.nativeElement.click();
            this.companySettingsService.singleEmailTemplate(this.selectedPerformanceTemplate[0]._id).subscribe(function (res) {
                console.log(res);
                _this.performanceEmailData = res.data;
                _this.fileName = res.data.originalFilename;
                _this.givenFileName = res.data.givenFileName;
                _this.module = res.data.module;
            }, function (err) {
                console.log(err);
            });
        }
    };
    EmailTemplatesComponent.prototype.editEmailTemplate5 = function () {
        var _this = this;
        this.selectedLeaveTemplate = this.leaveManagement.filter(function (leaveData) {
            return leaveData.isChecked;
        });
        if (this.selectedLeaveTemplate.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select only one email template on to Proceed", 'error');
        }
        else if (this.selectedLeaveTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Email Template", "Select a email template on to Proceed", 'error');
        }
        else {
            console.log(this.selectedLeaveTemplate);
            this.openEdit5.nativeElement.click();
            this.companySettingsService.singleEmailTemplate(this.selectedLeaveTemplate[0]._id).subscribe(function (res) {
                console.log(res);
                _this.leaveEmailData = res.data;
                _this.fileName = res.data.originalFilename;
                _this.givenFileName = res.data.givenFileName;
                _this.module = res.data.module;
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: saiprakash , Date: 16/04/19
    // Delete email tamplate
    EmailTemplatesComponent.prototype.deleteEmailTemplate1 = function () {
        var _this = this;
        this.selectedEmailTemplate = this.employeeManagement.filter(function (policyData) {
            return policyData.isChecked;
        });
        if (this.selectedEmailTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error');
        }
        else {
            this.companySettingsService.deleteEmailTemplates(this.selectedEmailTemplate).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success');
                _this.emailTemplate1 = [];
                _this.getAllEmialTempates();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    EmailTemplatesComponent.prototype.deleteEmailTemplate2 = function () {
        var _this = this;
        this.selectedRecruitmentTemplate = this.recruitments.filter(function (policyData) {
            return policyData.isChecked;
        });
        if (this.selectedRecruitmentTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error');
        }
        else {
            this.companySettingsService.deleteEmailTemplates(this.selectedRecruitmentTemplate).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success');
                _this.emailTemplate2 = [];
                _this.getAllEmialTempates();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    EmailTemplatesComponent.prototype.deleteEmailTemplate3 = function () {
        var _this = this;
        this.selectedAdministateTemplate = this.benefitsAdministration.filter(function (policyData) {
            return policyData.isChecked;
        });
        if (this.selectedAdministateTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error');
        }
        else {
            this.companySettingsService.deleteEmailTemplates(this.selectedAdministateTemplate).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success');
                _this.emailTemplate3 = [];
                _this.getAllEmialTempates();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    EmailTemplatesComponent.prototype.deleteEmailTemplate4 = function () {
        var _this = this;
        this.selectedPerformanceTemplate = this.performanceManagement.filter(function (policyData) {
            return policyData.isChecked;
        });
        if (this.selectedPerformanceTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error');
        }
        else {
            this.companySettingsService.deleteEmailTemplates(this.selectedPerformanceTemplate).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success');
                _this.emailTemplate4 = [];
                _this.getAllEmialTempates();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    EmailTemplatesComponent.prototype.deleteEmailTemplate5 = function () {
        var _this = this;
        this.selectedLeaveTemplate = this.leaveManagement.filter(function (element) {
            return element.isChecked;
        });
        if (this.selectedLeaveTemplate.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select one email template to Proceed", 'error');
        }
        else {
            this.companySettingsService.deleteEmailTemplates(this.selectedLeaveTemplate).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Deleted successfully', "", 'success');
                _this.emailTemplate5 = [];
                _this.getAllEmialTempates();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    EmailTemplatesComponent.prototype.checkEmailTemplate1 = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.emailTemplate1.push(id);
        }
        console.log(this.emailTemplate1);
        if (event.checked == false) {
            var index = this.emailTemplate1.indexOf(id);
            if (index > -1) {
                this.emailTemplate1.splice(index, 1);
            }
            console.log(this.emailTemplate1);
        }
    };
    EmailTemplatesComponent.prototype.checkEmailTemplate2 = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.emailTemplate2.push(id);
        }
        console.log(this.emailTemplate2);
        if (event.checked == false) {
            var index = this.emailTemplate2.indexOf(id);
            if (index > -1) {
                this.emailTemplate2.splice(index, 1);
            }
            console.log(this.emailTemplate2);
        }
    };
    EmailTemplatesComponent.prototype.checkEmailTemplate3 = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.emailTemplate3.push(id);
        }
        console.log(this.emailTemplate3);
        if (event.checked == false) {
            var index = this.emailTemplate3.indexOf(id);
            if (index > -1) {
                this.emailTemplate3.splice(index, 1);
            }
            console.log(this.emailTemplate3);
        }
    };
    EmailTemplatesComponent.prototype.checkEmailTemplate4 = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.emailTemplate4.push(id);
        }
        console.log(this.emailTemplate4);
        if (event.checked == false) {
            var index = this.emailTemplate4.indexOf(id);
            if (index > -1) {
                this.emailTemplate4.splice(index, 1);
            }
            console.log(this.emailTemplate4);
        }
    };
    EmailTemplatesComponent.prototype.checkEmailTemplate5 = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.emailTemplate5.push(id);
        }
        console.log(this.emailTemplate5);
        if (event.checked == false) {
            var index = this.emailTemplate5.indexOf(id);
            if (index > -1) {
                this.emailTemplate5.splice(index, 1);
            }
            console.log(this.emailTemplate5);
        }
    };
    EmailTemplatesComponent.prototype.deleteEmailTemplate = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmationComponent"], {
            width: '350px',
            panelClass: 'confirm-dialog',
            data: {
                text: 'delete'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            if (result) {
                if (_this.emailTemplate1 && _this.emailTemplate1.length) {
                    _this.deleteEmailTemplate1();
                }
                else if (_this.emailTemplate2 && _this.emailTemplate2.length) {
                    _this.deleteEmailTemplate2();
                }
                else if (_this.emailTemplate3 && _this.emailTemplate3.length) {
                    _this.deleteEmailTemplate3();
                }
                else if (_this.emailTemplate4 && _this.emailTemplate4.length) {
                    _this.deleteEmailTemplate4();
                }
                else if (_this.emailTemplate5 && _this.emailTemplate5.length) {
                    _this.deleteEmailTemplate5();
                }
            }
        });
    };
    EmailTemplatesComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32);
    };
    EmailTemplatesComponent.prototype.cancelEmailForm = function () {
        this.fileName = '';
        this.givenFileName = '';
        this.module = '';
        this.isValid = false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit1'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EmailTemplatesComponent.prototype, "openEdit1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit2'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EmailTemplatesComponent.prototype, "openEdit2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit3'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EmailTemplatesComponent.prototype, "openEdit3", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit4'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EmailTemplatesComponent.prototype, "openEdit4", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit5'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EmailTemplatesComponent.prototype, "openEdit5", void 0);
    EmailTemplatesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-templates',
            template: __webpack_require__(/*! ./email-templates.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.html"),
            styles: [__webpack_require__(/*! ./email-templates.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/email-templates/email-templates.component.css")]
        }),
        __metadata("design:paramtypes", [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__["CompanySettingsService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"]])
    ], EmailTemplatesComponent);
    return EmailTemplatesComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.css":
/*!******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.css ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".field-maintenanance{\n    height:auto;\n}\n.zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n.format{\n    width:20%; \n}\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n.zenwork-currentpage span{ display: inline-block;}\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important; font-size: 17px;\n}\n.zenwork-general-form-settings{\n    width: 18%!important;\n    border-radius: 0px!important; border: none; box-shadow: none;\n}\n.zenwork-br-zero{\n    border-radius: 0px!important;border: none;box-shadow: none; height: 120px; resize: none; padding: 10px; line-height: 20px;\n}\n.zenwork-text-area-settings{\n    width:55%!important;\n}\n.zenwork-import-structure{\n    margin:20px 0 ;\n}\n.ml-20{\n    margin-left: 20px;\n}\n.zenwork-customized-table.table>thead>tr.info>td{\n    padding: 13px;\n}\n.zenwork-customized-table.table>tbody>tr>td{\n    vertical-align: middle!important;\n    padding: 10px;\n}\n.zenwork-structure-settings{\n    width: 50%!important;\n    border-radius: 0px!important;\n}\n.zenwork-company-contact-form{\n    width: 65%!important;\n    border-radius: 0px!important;\n}\n.field-maintenance-wrapper .mat-button-toggle-standalone{\n    border-radius: 25px;\n}\n.zenwork-btn-toggle-wrapper{\n    position: relative;\n}\n.field-select-alignment{\n    position: absolute;\n    top: -8px;\n    left: 5px;\n    border: 1px solid #ccc;\n    border-radius: 50%;\n    padding: 2px;\n    height: 20px;\n    width: 20px;\n    text-align: center;\n    background: #fff;\n    z-index: 999999999999999;\n}\n.field-selected{\n    background: #777d7a;\n    color: #fff;\n    border: 1px solid #777d7a;\n}\n.field-maintenance-wrapper .mat-list .mat-list-item{\n    height: inherit!important;\n    padding: 20px;\n}\n.field-maintenance-wrapper .mat-list{\n    padding-top: 0px!important;\n}\n.mat-expansion-panel:not{\n    box-shadow: none!important;\n}\n/*--------- This is my code -------*/\n.field-accordion { padding:20px 0 0;}\n.field-accordion p {color: #565555;font-size: 15px;}\n.field-accordion .panel-default>.panel-heading { padding: 20px 0; background-color:transparent !important; border: none;}\n.field-accordion .panel-title { color: #5a5959; display: inline-block; font-size: 18px;}\n.field-accordion .panel-title a:hover { text-decoration: none;color: #5a5959;}\n.field-accordion .panel-title a:focus{color: #5a5959;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:6px 10px 0; font-size: 12px; line-height: 12px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top:0;}\n.reports-table { display: block; width: 100%;}\n.reports-table .table { margin-bottom: 30px;}\n.reports-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 13px; background: #eef7ff; border-bottom:none;}\n.reports-table .table>tbody>tr>td, .reports-table .table>tfoot>tr>td, .reports-table .table>thead>tr>td{ padding: 13px; background:#fff;border-top: none;}\n.bottom-border { width: 100%;border-top:1px solid #ccc;}\n.reports-table tr.bottom-border td{ border-bottom: none;}\n.reports-table .table>tbody>tr>td{background: #f8f8f8; border-bottom:1px solid #ccc;}\n.zenwork-green{background:#777d7a; padding:8px 20px; color:#fff;font-size:15px;}\n.field-accordion .panel-heading { position: relative;}\n.excel-report { position: absolute; top: 0; right:72px;}\n.excel-report a { cursor: pointer; display: inline-block;}\n.excel-report a.btn{ padding: 0; height: 30px; line-height: 30px; color:#fff; text-align: center; border-radius: 3px; background:#008f3d; padding: 0 20px; }\n.field-settings { display: block;border-bottom: #ccc 1px solid; margin:10px 0 30px; padding: 0 0 30px; position: relative;}\n.field-settings h4 { font-weight: normal;}\n.select:active, .select:focus, .select:hover { background:#eef7ff;}\n.standard-fields .modal-content { border: none; padding:30px 0 50px;}\n.employee { display: block;}\n.employee ul { display: block;}\n.employee ul li { display: inline-block;margin: 0 25px 15px 0; width: 20%;}\n.employee ul li p {color: #565555;font-size: 15px;}\n.employee ul li .form-control {font-size: 15px; line-height: 15px;}\n.employee-settings { display: block; width: 20%;}\n.first-number{\n    border: none;\n    height: 40px;\n    padding: 10px;\n}\n.emp-btns{\n    margin: 20px 0;\n}\n.emp-btns ul li{\n    display: inline-block;\n    padding: 0px 15px;\n}\n.emp-btns .submit-btn a{\n    border: 1px solid #439348;\n    border-radius: 25px;\n    padding: 7px 20px;\n    color: #439348;\n    cursor: pointer; background: #fff;\n}\n.emp-btns .cancel-btn a{\n    color :#716e6e;\n    cursor: pointer;\n}\n.employee-search-heading{\n    margin: 20px 0\n}\n.employee-search-heading li{\n    display: inline-block;\n    padding: 0px 15px 0 0;\n}\n.search-list{\n    width: 31%;\n}\n.search-filter{\n    width: 100%;\n    height: auto;\n    padding: 12px 10px;\n    border: none;\n    box-shadow: none;\n    font-size: 16px;\n\n}\n.search-filter:focus{border-color:transparent; box-shadow: none;}\n.btn-run-report{\n    padding: 3px 25px;\n    display: inline-block;\n    border: 1px solid #c5b9b9;\n    border-radius: 24px;\n    color: #000;\n    cursor: pointer;\n}\n/* modalcss */\n.modal-margintop{\n    margin-top:40px;\n}\n/* side nav css*/\n.tabs-left{\n    margin-top:0;\n}\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n    color: #000;\n  }\n.tabs-left > .nav-tabs {\n    float: left;\n    border-right: 1px solid #ddd;\n  }\n.tabs-left > .nav-tabs > li > a {\n    font-size: 16px;\n    margin-right: -1px;\n    border-radius: 4px 0 0 4px;\n  }\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color:transparent;\n  }\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n/*side nav css ends*/\nlabel{\n    font-weight:normal;\n    line-height: 30px;\n    font-size: 16px;\n}\ninput{\n  height:30px;\n  border:1px solid #e5e5e5;\n}\n.customize-form{\n    margin-top:20px;\n}\n.customize-form .template{\n    height:30px;\n    width:40%;\n    border: 0;\n    background: #f8f8f8;\n}\n.modal-body{\n    background:#f8f8f8;\n    padding: 0;\n}\n.tabs-left .nav-tabs{\n    padding: 20px;\n    border-right: 0;\n}\n.tab-content{\n  margin: 0;\n  background: #fff;\n  padding: 30px;\n}\n.tabs-customize{\n    background: #f8f8f8;\n    padding:0px!important;\n}\n.modal-body .container{\n    width:100%;\n}\n.field-maintenance-wrapper .mat-button-toggle-standalone{\n    border-radius: 25px;\n    box-shadow: none;\n    border: 1px solid #dedede;\n}\n.zenwork-btn-toggle-wrapper{\n    position: relative;\n    \n}\n.field-select-alignment{\n    position: absolute;\n    top: -8px;\n    left: 5px;\n    border: 1px solid #ccc;\n    border-radius: 50%;\n    padding: 2px;\n    height: 20px;\n    width: 20px;\n    text-align: center;\n    background: #fff;\n    z-index: 999999999999999;\n}\n.field-selected{\n    background: #777d7a;\n    color: #fff;\n    border: 1px solid #777d7a;\n}\n.personaldata-fields-list{\n    margin-top: 30px;\n}\n.personal-data-listitems{\n    display: inline-block;\n    padding: 25px 8px;\n}\n.caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n.mat-button-toggle-label-content{\n  color: #000;\n  font-size: 12px\n  }\n.onboarding-tabs li a:active,.onboarding-tabs li a:visited,.onboarding-tabs li a:focus{\n    border: 1px solid transparent!important;\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n}\n.onboarding-tabs>li.active>a, .onboarding-tabs>li.active>a:focus, .onboarding-tabs>li.active>a:hover{\n    border: 1px solid transparent!important;\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    background: #f8f8f8;\n}\n.buttons{\n    margin: 20px 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid #dcdbdb;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #636262;\n    cursor: pointer;\n}\n.buttons ul li .save{\n\n    border:none;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background:#429348; font-size: 16px;\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n.custom-fields-table{\n    padding: 50px;\n}\n.modal-customfields{\n    padding: 30px !important;\n}\n.add-custom-field{\n    padding: 50px;\n}\n.field-name-input{\n    border: none;\n    background: #f8f8f8;\n    box-shadow: none;\n    height: 35px;\n    width: 45%;\n}\n.edit-field-buttons{\n    margin: 20px 0 0 0;\n    padding: 20px 0 0 0;\n    border-top: 1px solid #ccc;\n}\n.dropdown-listoptions{\n    padding: 20px;\n    box-shadow: 0px 0px 13px -1px rgba(0,0,0,0.75);\n}\n.dropdown-listoptions h4,.dropdown-listoptions ul{\n    margin: 20px 0px;\n}\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;\n}\n.filed-popup { display: block;}\n.filed-popup .modal.in .modal-dialog { width:80%;}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"field-maintenanance\">\n  <div class=\"field-accordion\">\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#employeeIdentification\">\n              Employee Identification Settings\n            </a>\n          </h4>\n        </div>\n        <div id=\"employeeIdentification\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"employee-settings\">\n\n              <div class=\"form-group\">\n                <p>\n                  Do you want to mask SSN?\n                </p>\n\n                <mat-select placeholder=\"Mask SSN\" [(ngModel)]=\"employeeIdentity.maskSSN\"\n                  (ngModelChange)=\"maskSsnChange()\" name=\"maskSSN\" #maskSSN=\"ngModel\" required>\n                  <mat-option>--select--</mat-option>\n                  <mat-option [value]=true>Yes</mat-option>\n                  <mat-option [value]=false>No</mat-option>\n                </mat-select>\n                <p *ngIf=\"!employeeIdentity.maskSSN && maskSSN.touched && isValid|| (!employeeIdentity.maskSSN && isValid)\"\n                  class=\"error\">Please fill this field</p>\n               \n\n              </div>\n\n              <div class=\"form-group\">\n                <p>\n                  Employee ID Number Settings\n                </p>\n\n                <mat-select placeholder=\"Number Settings\" [(ngModel)]=\"employeeIdentity.idNumber.numberType\"\n                  (ngModelChange)=\"empIdNumberSettingsChange()\" name=\"numberType\" #numberType=\"ngModel\" required>\n                  <mat-option>--select--</mat-option>\n\n                  <mat-option value='system Generated'>System Generated</mat-option>\n                  <mat-option value='Manual'>Manual</mat-option>\n                </mat-select>\n                <p *ngIf=\"!employeeIdentity.idNumber.numberType && numberType.touched && isValid|| (!employeeIdentity.idNumber.numberType && isValid)\"\n                  class=\"error\">Please fill this field</p>\n\n\n              </div>\n\n            </div>\n\n            <div *ngIf=\"employeeIdentity.idNumber.numberType == 'system Generated'\" class=\"employee\">\n              <ul>\n                <li>\n                  <p>Select Format</p>\n\n                  <mat-select placeholder=\"Digits\" [(ngModel)]=\"employeeIdentity.idNumber.digits\"\n                    (ngModelChange)=\"employeeIdformatChange($event)\">\n                    <mat-option *ngFor=\"let digit of digits\" [value]=\"digit.value\">{{digit.viewVlaue}}</mat-option>\n                  </mat-select>\n                  <p *ngIf=\"!employeeIdentity.idNumber.digits && digits.touched && isValid|| (!employeeIdentity.idNumber.digits && isValid)\"\n                    class=\"error\">Please fill this field</p>\n\n                </li>\n\n                <li>\n                  <p>What should be the first number?</p>\n                  <input type=\"text\" class=\"form-control first-number\"\n                    [(ngModel)]=\"employeeIdentity.idNumber.firstNumber\" (keypress)=\"keyPress3($event)\"\n                    [attr.maxlength]=\"maxLength\" placeholder=\"first-number\" name=\"firstNumber\" #firstNumber=\"ngModel\"\n                    required>\n                  <p *ngIf=\"!employeeIdentity.idNumber.firstNumber && firstNumber.touched && isValid || (!employeeIdentity.idNumber.firstNumber && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                </li>\n\n              </ul>\n              <div class=\"clearfix\"></div>\n            </div>\n            <div class=\"emp-btns\">\n              <ul class=\"list-unstyled\">\n                <li class='submit-btn'>\n                  <a (click)=\"empIdentitySettings()\">Submit</a>\n                </li>\n                <li class=\"cancel-btn\">\n                  <a (click)=\"empIdSettingsCancel()\">Cancel</a>\n                </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n\n\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwo\">\n              Employee Search\n            </a>\n          </h4>\n\n          <!-- <div class=\"excel-report\">\n          <a class=\"btn\">Run Excel Report</a>\n        </div> -->\n\n        </div>\n\n        <div id=\"collapseTwo\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n            <ul class=\"list-unstyled employee-search-heading\">\n              <li class=\"list-items search-list\">\n                <input type=\"text\" class=\"form-control search-filter\" placeholder=\"Search by name...\"\n                  [(ngModel)]=\"searchString\" (ngModelChange)=\"searchFilter()\">\n              </li>\n              <li class=\"list-items\">\n                <span>(Total Employee count: {{count}})</span>\n              </li>\n              <li class=\"list-items pull-right\">\n                <!-- <a class=\"btn-run-report\">\n                  Run Report\n                </a> -->\n                <div class=\"clearfix\"></div>\n              </li>\n            </ul>\n            <div class=\"reports-table\">\n\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>Employee Name</th>\n                    <th>Employee ID Number</th>\n                    <th>Hire Date</th>\n                    <th>Termination Date</th>\n                    <th>Status</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let employee of allEmployees\">\n                    <td>{{employee.personal.name.preferredName}}</td>\n                    <td>{{employee.job.employeeId}}</td>\n                    <td>{{employee.job.hireDate | date: 'MM/dd/yyyy'}}</td>\n                    <td>--</td>\n                    <td>{{employee.job.current_employment_status.status}}</td>\n                  </tr>\n\n\n              </table>\n              <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\"\n                (page)=\"pageEvents($event)\">\n              </mat-paginator>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseThree\">\n              General Field Settings\n            </a>\n          </h4>\n        </div>\n\n        <div id=\"collapseThree\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"field-settings\">\n\n              <a data-toggle=\"modal\" data-target=\"#myModal\">\n                <button class=\"btn zenwork-green\" (click)=\"openModalWithClass(template)\">\n                  View / Edit - Standard Fields\n                </button>\n              </a>\n\n              <div class=\"filed-popup\">\n\n\n                <ng-template #template>\n                  <div class=\"modal-header modal-margintop\">\n\n                    <h4 *ngIf=\"chooseSteps\" class=\"modal-title pull-left\"><b>Standard Fields - Choose Sections</b></h4>\n                    <h4 *ngIf=\"chooseFields\" class=\"modal-title pull-left\"><b>Standard Fields - Format &\n                        Requirements</b></h4>\n\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n                  <div class=\"modal-body\">\n\n                    <!-- sidenavbar starts -->\n\n                    <!-- tabs -->\n                    <div class=\"tabbable tabs-left\">\n\n                      <ul class=\"nav nav-tabs col-md-3\">\n                        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'chooseSteps' }\">\n                          <a href=\"#chooseSteps\" data-toggle=\"tab\" (click)=\"chooseFieldss('chooseSteps')\">Choose\n                            Sections\n                            <span [ngClass]=\"{'caret': selectedNav == 'chooseSteps' }\"></span>\n                          </a>\n                        </li>\n                        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'chooseFields' }\">\n                          <a href=\"#chooseFields\" data-toggle=\"tab\" (click)=\"chooseFieldss('chooseFields')\">Format &\n                            Requirements\n                            <span [ngClass]=\"{'caret': selectedNav == 'chooseFields' }\"></span>\n                          </a>\n                        </li>\n\n\n                      </ul>\n\n                      <div class=\"tab-content col-md-9\">\n                        <div class=\"tab-pane\"\n                          [ngClass]=\"{'active':selectedNav === 'chooseSteps','in':selectedNav==='chooseSteps'}\"\n                          id=\"chooseSteps\">\n                          <div class=\"chooseFields-form\">\n                            <div class=\"field-maintenance-wrapper\">\n                              <div class=\"zenwork-btn-toggle-wrapper\">\n                                <span class=\"field-select\">\n                                  <i class=\"fa field-select-alignment\"\n                                    [ngClass]=\"{'fa-check': toggleSelectAll , 'field-selected':toggleSelectAll}\"\n                                    aria-hidden=\"true\"></i>\n                                </span>\n                                <mat-button-toggle [value]=toggleSelectAll (change)=\"toggleValueAll($event)\">\n                                  Select All Sections\n                                </mat-button-toggle>\n                              </div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n\n                            <div class=\"clearfix\"></div>\n                            <hr class=\"zenwork-margin-ten-zero\">\n                            <ul class=\"personaldata-fields-list list-unstyled\">\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Personal.hide , 'field-selected':responseObj.Personal.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Personal.hide\n                                      (change)=\"toggleValue($event,'personal')\" [disabled]=\"true\">\n                                      Personal\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Job.hide , 'field-selected':responseObj.Job.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Job.hide\n                                      (change)=\"toggleValue($event,'job')\">\n                                      Job\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Time_Off.hide , 'field-selected':responseObj.Time_Off.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Time_Off.hide\n                                      (change)=\"toggleValue($event,'timeOff')\">\n                                      Time Off\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Emergency_Contact.hide , 'field-selected':responseObj.Emergency_Contact.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Emergency_Contact.hide\n                                      (change)=\"toggleValue($event,'emergencyContact')\">\n                                      Emergency Contact\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Performance.hide , 'field-selected':responseObj.Performance.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Performance.hide\n                                      (change)=\"toggleValue($event,'performance')\">\n                                      Performance\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Compensation.hide , 'field-selected':responseObj.Compensation.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Compensation.hide\n                                      (change)=\"toggleValue($event,'compensation')\">\n                                      Compensation\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Notes.hide , 'field-selected':responseObj.Notes.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Notes.hide\n                                      (change)=\"toggleValue($event,'notes')\">\n                                      Notes\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Benefits.hide , 'field-selected':responseObj.Benefits.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Benefits.hide\n                                      (change)=\"toggleValue($event,'benefits')\">\n                                      Benefits\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Training.hide , 'field-selected':responseObj.Training.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Training.hide\n                                      (change)=\"toggleValue($event,'training')\">\n                                      Training\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Documents.hide , 'field-selected':responseObj.Documents.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Documents.hide\n                                      (change)=\"toggleValue($event,'documnets')\">\n                                      Documents\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Assets.hide , 'field-selected':responseObj.Assets.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Assets.hide\n                                      (change)=\"toggleValue($event,'assets')\">\n                                      Assets\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Onboarding.hide , 'field-selected':responseObj.Onboarding.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Onboarding.hide\n                                      (change)=\"toggleValue($event,'onBoarding')\">\n                                      Onboarding\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                              <li class=\"personal-data-listitems\">\n                                <div class=\"field-maintenance-wrapper\">\n                                  <div class=\"zenwork-btn-toggle-wrapper\">\n                                    <span class=\"field-select\">\n                                      <i class=\"fa field-select-alignment\"\n                                        [ngClass]=\"{'fa-check': responseObj.Offboarding.hide , 'field-selected':responseObj.Offboarding.hide}\"\n                                        aria-hidden=\"true\"></i>\n                                    </span>\n                                    <mat-button-toggle [value]=responseObj.Offboarding.hide\n                                      (change)=\"toggleValue($event,'offBoarding')\">\n                                      Offboarding\n                                    </mat-button-toggle>\n                                  </div>\n                                </div>\n                              </li>\n                            </ul>\n                            <hr class=\"zenwork-margin-fourty-zero\">\n\n                            <!-- <div class=\"margin-top\">\n                                <ul class=\"submit-buttons list-unstyled\">\n                                  <li class=\"list-item\">\n                                    <button class=\"btn btn-success btn-style\"> Submit </button>\n                                  </li>\n                                  <li class=\"list-item\">\n                                    <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> cancel </button>\n                                  </li>\n                                </ul>\n  \n                              </div> -->\n                            <div class=\"buttons\">\n                              <ul class=\"list-unstyled\">\n                                <li class=\"list-items\">\n                                  <a class=\"back\" (click)=\"modalRef.hide()\">Back</a>\n                                </li>\n                                <li class=\"list-items\">\n                                  <a class=\"cancel\" (click)=\"modalRef.hide()\">Cancel</a>\n                                </li>\n                                <li class=\"list-items pull-right\">\n                                  <button class='save' type=\"submit\" (click)=\"postShowFields()\">\n                                    Save Changes\n                                  </button>\n                                </li>\n\n                                <div class=\"clearfix\"></div>\n                              </ul>\n                            </div>\n                          </div>\n                        </div>\n\n                        <div class=\"tab-pane\"\n                          [ngClass]=\"{'active':selectedNav === 'chooseFields','in':selectedNav==='chooseFields'}\"\n                          id=\"chooseFields\">\n                          <div class=\"field-accordion\">\n                            <div class=\"panel-group\" id=\"formats\">\n\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#formats\"\n                                      href=\"#personalSection\">\n                                      Personal Section\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"personalSection\" class=\"panel-collapse collapse in\">\n                                  <div class=\"panel-body reports-table\">\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th [ngClass]=\"{'zenwork-checked-border': allFields}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              (change)=\"fieldName($event,'All')\">\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>Field Name</th>\n\n                                          <th class=\"format\">Format</th>\n                                          <th>Disabled</th>\n                                          <th>Required</th>\n                                          <th>\n                                            <!-- <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a (click)=\"editFields()\">Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div> -->\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.First_Name.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.First_Name.show\"\n                                              (change)=\"fieldName($event,'First_Name')\"></mat-checkbox>\n                                          </td>\n                                          <td>First Name</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChange($event,'First_Name')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.First_Name.format\"\n                                              [disabled]=!responseObj.Personal.fields.First_Name.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <!-- <mat-option value=\"number\">Numeric</mat-option> -->\n                                              <!-- <mat-option value=\"text\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.First_Name.hide\"\n                                              (change)=\"fieldDisableStatus($event,'First_Name')\"\n                                              [disabled]=!responseObj.Personal.fields.First_Name.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.First_Name.required\"\n                                              (change)=\"fieldRequiredStatus($event,'First_Name')\"\n                                              [disabled]=\"!responseObj.Personal.fields.First_Name.show || responseObj.Personal.fields.First_Name.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Middle_Name.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Middle_Name.show\"\n                                              (change)=\"fieldName($event,'Middle_Name')\"></mat-checkbox>\n                                          </td>\n                                          <td>Middle Name</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChange($event,'Middle_Name')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Middle_Name.format\"\n                                              [disabled]=!responseObj.Personal.fields.Middle_Name.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <!-- <mat-option value=\"number\">Numeric</mat-option> -->\n                                              <!-- <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Middle_Name.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Middle_Name')\"\n                                              [disabled]=!responseObj.Personal.fields.Middle_Name.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Middle_Name.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Middle_Name')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Middle_Name.show || responseObj.Personal.fields.Middle_Name.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Last_Name.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Last_Name.show\"\n                                              (change)=\"fieldName($event,'Last_Name')\"></mat-checkbox>\n                                          </td>\n                                          <td>Last Name</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChange($event,'Last_Name')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Last_Name.format\"\n                                              [disabled]=!responseObj.Personal.fields.Last_Name.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <!-- <mat-option value=\"number\">Numeric</mat-option> -->\n                                              <!-- <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Last_Name.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Last_Name')\"\n                                              [disabled]=!responseObj.Personal.fields.Last_Name.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Last_Name.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Last_Name')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Last_Name.show || responseObj.Personal.fields.Last_Name.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Preferred_Name.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Preferred_Name.show\"\n                                              (change)=\"fieldName($event,'Preferred_Name')\"></mat-checkbox>\n                                          </td>\n                                          <td>Preferred Name</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChange($event,'Preferred_Name')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Preferred_Name.format\"\n                                              [disabled]=!responseObj.Personal.fields.Preferred_Name.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <!-- <mat-option value=\"number\">Numeric</mat-option> -->\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Preferred_Name.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Preferred_Name')\"\n                                              [disabled]=!responseObj.Personal.fields.Preferred_Name.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Preferred_Name.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Preferred_Name')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Preferred_Name.show || responseObj.Personal.fields.Preferred_Name.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Salutation.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Salutation.show\"\n                                              (change)=\"fieldName($event,'Salutation')\"></mat-checkbox>\n                                          </td>\n                                          <td>Salutation</td>\n                                          <td>\n                                            <!-- <mat-select placeholder=\"type\" (ngModelChange)=\"formatChange($event,'Salutation')\" [(ngModel)]=\"responseObj.Personal.fields.Salutation.format\" [disabled]=!responseObj.Personal.fields.Salutation.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select> -->\n                                            Alpha-Numeric\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Salutation.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Salutation')\"\n                                              [disabled]=!responseObj.Personal.fields.Salutation.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Salutation.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Salutation')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Salutation.show || responseObj.Personal.fields.Salutation.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Date_of_Birth.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Date_of_Birth.show\"\n                                              (change)=\"fieldName($event,'Date_of_Birth')\"></mat-checkbox>\n                                          </td>\n                                          <td>Date of Birth</td>\n                                          <td>\n                                            <!-- <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChange($event,'Date_of_Birth')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Date_of_Birth.format\"\n                                              [disabled]=!responseObj.Personal.fields.Date_of_Birth.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select> -->\n                                            Date\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Date_of_Birth.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Date_of_Birth')\"\n                                              [disabled]=!responseObj.Personal.fields.Date_of_Birth.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Date_of_Birth.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Date_of_Birth')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Date_of_Birth.show || responseObj.Personal.fields.Date_of_Birth.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Gender.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Gender.show\"\n                                              (change)=\"fieldName($event,'Gender')\"></mat-checkbox>\n                                          </td>\n                                          <td>Gender</td>\n                                          <td>\n                                            <!-- <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChange($event,'Gender')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Gender.format\"\n                                              [disabled]=!responseObj.Personal.fields.Gender.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select> -->\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Gender.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Gender')\"\n                                              [disabled]=!responseObj.Personal.fields.Gender.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Gender.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Gender')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Gender.show || responseObj.Personal.fields.Gender.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Maritial_Status.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Maritial_Status.show\"\n                                              (change)=\"fieldName($event,'Maritial_Status')\"></mat-checkbox>\n                                          </td>\n                                          <td>Marital Status</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Maritial_Status.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Maritial_Status')\"\n                                              [disabled]=!responseObj.Personal.fields.Maritial_Status.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Maritial_Status.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Maritial_Status')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Maritial_Status.show || responseObj.Personal.fields.Maritial_Status.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Maritial_Status_Effective_Date.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Maritial_Status_Effective_Date.show\"\n                                              (change)=\"fieldName($event,'Maritial_Status_Effective_Date')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Marital Status Efective Date</td>\n                                          <td>\n                                            Date\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Maritial_Status_Effective_Date.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Maritial_Status_Effective_Date')\"\n                                              [disabled]=!responseObj.Personal.fields.Maritial_Status_Effective_Date.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Maritial_Status_Effective_Date.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Maritial_Status_Effective_Date')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Maritial_Status_Effective_Date.show || responseObj.Personal.fields.Maritial_Status_Effective_Date.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Veteran_Status.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Veteran_Status.show\"\n                                              (change)=\"fieldName($event,'Veteran_Status')\"></mat-checkbox>\n                                          </td>\n                                          <td>Veteran Status</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Veteran_Status.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Veteran_Status')\"\n                                              [disabled]=!responseObj.Personal.fields.Veteran_Status.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Veteran_Status.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Veteran_Status')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Veteran_Status.show || responseObj.Personal.fields.Veteran_Status.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.SSN.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.SSN.show\"\n                                              (change)=\"fieldName($event,'SSN')\"></mat-checkbox>\n                                          </td>\n                                          <td>SSN</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\" (ngModelChange)=\"formatChange($event,'SSN')\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.SSN.format\"\n                                              [disabled]=!responseObj.Personal.fields.SSN.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.SSN.hide\"\n                                              (change)=\"fieldDisableStatus($event,'SSN')\"\n                                              [disabled]=!responseObj.Personal.fields.SSN.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.SSN.required\"\n                                              (change)=\"fieldRequiredStatus($event,'SSN')\"\n                                              [disabled]=\"!responseObj.Personal.fields.SSN.show || responseObj.Personal.fields.SSN.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Race.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Race.show\"\n                                              (change)=\"fieldName($event,'Race')\"></mat-checkbox>\n                                          </td>\n                                          <td>Race</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Race.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Race')\"\n                                              [disabled]=!responseObj.Personal.fields.Race.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Race.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Race')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Race.show || responseObj.Personal.fields.Race.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Personal.fields.Ethnicity.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Ethnicity.show\"\n                                              (change)=\"fieldName($event,'Ethnicity')\"></mat-checkbox>\n                                          </td>\n                                          <td>Ethnicity</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Ethnicity.hide\"\n                                              (change)=\"fieldDisableStatus($event,'Ethnicity')\"\n                                              [disabled]=!responseObj.Personal.fields.Ethnicity.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Personal.fields.Ethnicity.required\"\n                                              (change)=\"fieldRequiredStatus($event,'Ethnicity')\"\n                                              [disabled]=\"!responseObj.Personal.fields.Ethnicity.show || responseObj.Personal.fields.Ethnicity.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                    </table>\n\n                                  </div>\n                                </div>\n                              </div>\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#formats\"\n                                      href=\"#jobsection\">\n                                      Job Section\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"jobsection\" class=\"panel-collapse collapse\">\n                                  <div class=\"panel-body reports-table\">\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n\n\n                                          <th [ngClass]=\"{'zenwork-checked-border': allFields}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              (change)=\"fieldNameForJob($event,'All')\"></mat-checkbox>\n                                          </th>\n                                          <th>Field Name</th>\n\n                                          <th class=\"format\">Format</th>\n                                          <th>Disabled</th>\n                                          <th>Required</th>\n                                          <th>\n\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Employee_Id.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Id.show\"\n                                              (change)=\"fieldNameForJob($event,'Employee_Id')\"></mat-checkbox>\n                                          </td>\n                                          <td>Employee Id</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforJob($event,'Employee_Id')\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Id.format\"\n                                              [disabled]=!responseObj.Job.fields.Employee_Id.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Id.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Employee_Id')\"\n                                              [disabled]=!responseObj.Job.fields.Employee_Id.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Id.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Employee_Id')\"\n                                              [disabled]=\"!responseObj.Job.fields.Employee_Id.show || responseObj.Job.fields.Employee_Id.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Employee_Type.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Type.show\"\n                                              (change)=\"fieldNameForJob($event,'Employee_Type')\"></mat-checkbox>\n                                          </td>\n                                          <td>Employee Type</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Type.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Employee_Type')\"\n                                              [disabled]=!responseObj.Job.fields.Employee_Type.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Type.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Employee_Type')\"\n                                              [disabled]=\"!responseObj.Job.fields.Employee_Type.show || responseObj.Job.fields.Employee_Type.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Employee_Self_Service_Id.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Self_Service_Id.show\"\n                                              (change)=\"fieldNameForJob($event,'Employee_Self_Service_Id')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Employee Self Service Id</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforJob($event,'Employee_Self_Service_Id')\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Self_Service_Id.format\"\n                                              [disabled]=!responseObj.Job.fields.Employee_Self_Service_Id.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Self_Service_Id.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Employee_Self_Service_Id')\"\n                                              [disabled]=!responseObj.Job.fields.Employee_Self_Service_Id.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Employee_Self_Service_Id.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Employee_Self_Service_Id')\"\n                                              [disabled]=\"!responseObj.Job.fields.Employee_Self_Service_Id.show || responseObj.Job.fields.Employee_Self_Service_Id.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Work_Location.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Work_Location.show\"\n                                              (change)=\"fieldNameForJob($event,'Work_Location')\"></mat-checkbox>\n                                          </td>\n                                          <td>Work Location</td>\n                                          <td>\n                                            Alpha-Numeric\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Work_Location.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Work_Location')\"\n                                              [disabled]=!responseObj.Job.fields.Work_Location.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Work_Location.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Work_Location')\"\n                                              [disabled]=\"!responseObj.Job.fields.Work_Location.show || responseObj.Job.fields.Work_Location.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Reporting_Location.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Reporting_Location.show\"\n                                              (change)=\"fieldNameForJob($event,'Reporting_Location')\"></mat-checkbox>\n                                          </td>\n                                          <td>Reporting Location</td>\n                                          <td>\n                                            Alpha-Numeric\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Reporting_Location.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Reporting_Location')\"\n                                              [disabled]=!responseObj.Job.fields.Reporting_Location.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Reporting_Location.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Reporting_Location')\"\n                                              [disabled]=\"!responseObj.Job.fields.Reporting_Location.show || responseObj.Job.fields.Reporting_Location.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Hire_Date.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Hire_Date.show\"\n                                              (change)=\"fieldNameForJob($event,'Hire_Date')\"></mat-checkbox>\n                                          </td>\n                                          <td>Hire Date</td>\n                                          <td>\n                                            Date\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Hire_Date.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Hire_Date')\"\n                                              [disabled]=!responseObj.Job.fields.Hire_Date.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Hire_Date.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Hire_Date')\"\n                                              [disabled]=!responseObj.Job.fields.Hire_Date.show></mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Rehire_Date.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Rehire_Date.show\"\n                                              (change)=\"fieldNameForJob($event,'Rehire_Date')\"></mat-checkbox>\n                                          </td>\n                                          <td>Rehire Date</td>\n                                          <td>\n                                            Date\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Rehire_Date.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Rehire_Date')\"\n                                              [disabled]=!responseObj.Job.fields.Rehire_Date.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Rehire_Date.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Rehire_Date')\"\n                                              [disabled]=\"!responseObj.Job.fields.Rehire_Date.show || responseObj.Job.fields.Rehire_Date.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Termination_Date.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Termination_Date.show\"\n                                              (change)=\"fieldNameForJob($event,'Termination_Date')\"></mat-checkbox>\n                                          </td>\n                                          <td>Termination Date</td>\n                                          <td>\n                                            Date\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Termination_Date.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Termination_Date')\"\n                                              [disabled]=!responseObj.Job.fields.Termination_Date.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Termination_Date.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Termination_Date')\"\n                                              [disabled]=\"!responseObj.Job.fields.Termination_Date.show || responseObj.Job.fields.Termination_Date.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Termination_Reason.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Termination_Reason.show\"\n                                              (change)=\"fieldNameForJob($event,'Termination_Reason')\"></mat-checkbox>\n                                          </td>\n                                          <td>Termination Reason</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Termination_Reason.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Termination_Reason')\"\n                                              [disabled]=!responseObj.Job.fields.Termination_Reason.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Termination_Reason.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Termination_Reason')\"\n                                              [disabled]=\"!responseObj.Job.fields.Termination_Reason.show || responseObj.Job.fields.Termination_Reason.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Rehire_Status.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Rehire_Status.show\"\n                                              (change)=\"fieldNameForJob($event,'Rehire_Status')\"></mat-checkbox>\n                                          </td>\n                                          <td>Rehire Status</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Rehire_Status.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Rehire_Status')\"\n                                              [disabled]=!responseObj.Job.fields.Rehire_Status.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Rehire_Status.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Rehire_Status')\"\n                                              [disabled]=\"!responseObj.Job.fields.Rehire_Status.show || responseObj.Job.fields.Rehire_Status.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Archive_Status.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Archive_Status.show\"\n                                              (change)=\"fieldNameForJob($event,'Archive_Status')\"></mat-checkbox>\n                                          </td>\n                                          <td>Archive Status</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Archive_Status.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Archive_Status')\"\n                                              [disabled]=!responseObj.Job.fields.Archive_Status.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Archive_Status.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Archive_Status')\"\n                                              [disabled]=\"!responseObj.Job.fields.Archive_Status.show || responseObj.Job.fields.Archive_Status.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.FLSA_Code.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.FLSA_Code.show\"\n                                              (change)=\"fieldNameForJob($event,'FLSA_Code')\"></mat-checkbox>\n                                          </td>\n                                          <td>FLSA Code</td>\n                                          <td>\n                                            Alpha-Numeric\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.FLSA_Code.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'FLSA_Code')\"\n                                              [disabled]=!responseObj.Job.fields.FLSA_Code.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.FLSA_Code.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'FLSA_Code')\"\n                                              [disabled]=\"!responseObj.Job.fields.FLSA_Code.show ||responseObj.Job.fields.FLSA_Code.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.EEO_Job_Category.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.EEO_Job_Category.show\"\n                                              (change)=\"fieldNameForJob($event,'EEO_Job_Category')\"></mat-checkbox>\n                                          </td>\n                                          <td>EEO Job Category</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.EEO_Job_Category.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'EEO_Job_Category')\"\n                                              [disabled]=!responseObj.Job.fields.EEO_Job_Category.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.EEO_Job_Category.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'EEO_Job_Category')\"\n                                              [disabled]=\"!responseObj.Job.fields.EEO_Job_Category.show || responseObj.Job.fields.EEO_Job_Category.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Vendor_Id.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Vendor_Id.show\"\n                                              (change)=\"fieldNameForJob($event,'Vendor_Id')\"></mat-checkbox>\n                                          </td>\n                                          <td>Vendor Id</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforJob($event,'Vendor_Id')\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Vendor_Id.format\"\n                                              [disabled]=!responseObj.Job.fields.Vendor_Id.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Vendor_Id.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Vendor_Id')\"\n                                              [disabled]=!responseObj.Job.fields.Vendor_Id.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Vendor_Id.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Vendor_Id')\"\n                                              [disabled]=\"!responseObj.Job.fields.Vendor_Id.show || responseObj.Job.fields.Vendor_Id.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Specific_Workflow_Approval.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Specific_Workflow_Approval.show\"\n                                              (change)=\"fieldNameForJob($event,'Specific_Workflow_Approval')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Specific Workflow Approval</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Specific_Workflow_Approval.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Specific_Workflow_Approval')\"\n                                              [disabled]=!responseObj.Job.fields.Specific_Workflow_Approval.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Specific_Workflow_Approval.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Specific_Workflow_Approval')\"\n                                              [disabled]=\"!responseObj.Job.fields.Specific_Workflow_Approval.show || responseObj.Job.fields.Specific_Workflow_Approval.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Site_AccessRole.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Site_AccessRole.show\"\n                                              (change)=\"fieldNameForJob($event,'Site_AccessRole')\"></mat-checkbox>\n                                          </td>\n                                          <td>Site AccessRole</td>\n                                          <td>\n                                            Alpha-Numeric\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Site_AccessRole.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Site_AccessRole')\"\n                                              [disabled]=!responseObj.Job.fields.Site_AccessRole.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Site_AccessRole.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Site_AccessRole')\"\n                                              [disabled]=\"!responseObj.Job.fields.Site_AccessRole.show || responseObj.Job.fields.Site_AccessRole.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Type_of_User_Role_type.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Type_of_User_Role_type.show\"\n                                              (change)=\"fieldNameForJob($event,'Type_of_User_Role_type')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Type of User Role</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforJob($event,'Type_of_User_Role_type')\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Type_of_User_Role_type.format\"\n                                              [disabled]=!responseObj.Job.fields.Type_of_User_Role_type.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Type_of_User_Role_type.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Type_of_User_Role_type')\"\n                                              [disabled]=!responseObj.Job.fields.Type_of_User_Role_type.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Type_of_User_Role_type.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Type_of_User_Role_type')\"\n                                              [disabled]=\"!responseObj.Job.fields.Type_of_User_Role_type.show || responseObj.Job.fields.Type_of_User_Role_type.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Custom_1.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_1.show\"\n                                              (change)=\"fieldNameForJob($event,'Custom_1')\"></mat-checkbox>\n                                          </td>\n                                          <td>Custom 1</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforJob($event,'Custom_1')\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_1.format\"\n                                              [disabled]=!responseObj.Job.fields.Custom_1.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_1.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Custom_1')\"\n                                              [disabled]=!responseObj.Job.fields.Custom_1.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_1.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Custom_1')\"\n                                              [disabled]=\"!responseObj.Job.fields.Custom_1.show || responseObj.Job.fields.Custom_1.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Job.fields.Custom_2.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_2.show\"\n                                              (change)=\"fieldNameForJob($event,'Custom_2')\"></mat-checkbox>\n                                          </td>\n                                          <td>Custom 2</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforJob($event,'Custom_2')\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_2.format\"\n                                              [disabled]=!responseObj.Job.fields.Custom_2.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_2.hide\"\n                                              (change)=\"fieldDisableStatusforJob($event,'Custom_2')\"\n                                              [disabled]=!responseObj.Job.fields.Custom_2.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Job.fields.Custom_2.required\"\n                                              (change)=\"fieldRequiredStatusforJob($event,'Custom_2')\"\n                                              [disabled]=\"!responseObj.Job.fields.Custom_2.show || responseObj.Job.fields.Custom_2.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                      </tbody>\n                                    </table>\n                                  </div>\n                                </div>\n                              </div>\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#formats\"\n                                      href=\"#compensation\">\n                                      Compensation\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"compensation\" class=\"panel-collapse collapse\">\n                                  <div class=\"panel-body reports-table\">\n                                    <table class=\"table\">\n                                      <thead>\n\n                                        <tr>\n\n                                          <th [ngClass]=\"{'zenwork-checked-border': allFields}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              (change)=\"fieldNameForCompensation($event,'All')\"></mat-checkbox>\n\n                                          </th>\n                                          <th>Field Name</th>\n\n                                          <th class=\"format\">Format</th>\n                                          <th>Disabled</th>\n                                          <th>Required</th>\n                                          <th>\n                                            <!-- <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a (click)=\"editFields()\">Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div> -->\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Compensation.fields.Nonpaid_Record.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Nonpaid_Record.show\"\n                                              (change)=\"fieldNameForCompensation($event,'Nonpaid_Record')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Nonpaid Record</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Nonpaid_Record.hide\"\n                                              (change)=\"fieldDisableStatusforCompensation($event,'Nonpaid_Record')\"\n                                              [disabled]=!responseObj.Compensation.fields.Nonpaid_Record.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Nonpaid_Record.required\"\n                                              (change)=\"fieldRequiredStatusforCompensation($event,'Nonpaid_Record')\"\n                                              [disabled]=\"!responseObj.Compensation.fields.Nonpaid_Record.show || responseObj.Compensation.fields.Nonpaid_Record.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Compensation.fields.Highly_compensated_Employee_type.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Highly_compensated_Employee_type.show\"\n                                              (change)=\"fieldNameForCompensation($event,'Highly_compensated_Employee_type')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Highly compensated Employee</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Highly_compensated_Employee_type.hide\"\n                                              (change)=\"fieldDisableStatusforCompensation($event,'Highly_compensated_Employee_type')\"\n                                              [disabled]=!responseObj.Compensation.fields.Highly_compensated_Employee_type.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Highly_compensated_Employee_type.required\"\n                                              (change)=\"fieldRequiredStatusforCompensation($event,'Highly_compensated_Employee_type')\"\n                                              [disabled]=\"!responseObj.Compensation.fields.Highly_compensated_Employee_type.show || responseObj.Compensation.fields.Highly_compensated_Employee_type.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Compensation.fields.Pay_group.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Pay_group.show\"\n                                              (change)=\"fieldNameForCompensation($event,'Pay_group')\"></mat-checkbox>\n                                          </td>\n                                          <td>Pay group</td>\n                                          <td>\n                                            Alpha </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Pay_group.hide\"\n                                              (change)=\"fieldDisableStatusforCompensation($event,'Pay_group')\"\n                                              [disabled]=!responseObj.Compensation.fields.Pay_group.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Pay_group.required\"\n                                              (change)=\"fieldRequiredStatusforCompensation($event,'Pay_group')\"\n                                              [disabled]=\"!responseObj.Compensation.fields.Pay_group.show || responseObj.Compensation.fields.Pay_group.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Compensation.fields.Pay_frequency.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Pay_frequency.show\"\n                                              (change)=\"fieldNameForCompensation($event,'Pay_frequency')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Pay frequency</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Pay_frequency.hide\"\n                                              (change)=\"fieldDisableStatusforCompensation($event,'Pay_frequency')\"\n                                              [disabled]=!responseObj.Compensation.fields.Pay_frequency.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Pay_frequency.required\"\n                                              (change)=\"fieldRequiredStatusforCompensation($event,'Pay_frequency')\"\n                                              [disabled]=\"!responseObj.Compensation.fields.Pay_frequency.show || responseObj.Compensation.fields.Pay_frequency.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Compensation.fields.Salary_Grade.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Salary_Grade.show\"\n                                              (change)=\"fieldNameForCompensation($event,'Salary_Grade')\"></mat-checkbox>\n                                          </td>\n                                          <td>Salary Grade</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Salary_Grade.hide\"\n                                              (change)=\"fieldDisableStatusforCompensation($event,'Salary_Grade')\"\n                                              [disabled]=!responseObj.Compensation.fields.Salary_Grade.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Salary_Grade.required\"\n                                              (change)=\"fieldRequiredStatusforCompensation($event,'Salary_Grade')\"\n                                              [disabled]=\"!responseObj.Compensation.fields.Salary_Grade.show || responseObj.Compensation.fields.Salary_Grade.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Compensation.fields.Compensation_ratio.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Compensation_ratio.show\"\n                                              (change)=\"fieldNameForCompensation($event,'Compensation_ratio')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Compensation ratio</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforCompensation($event,'Compensation_ratio')\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Compensation_ratio.format\"\n                                              [disabled]=!responseObj.Compensation.fields.Compensation_ratio.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Compensation_ratio.hide\"\n                                              (change)=\"fieldDisableStatusforCompensation($event,'Compensation_ratio')\"\n                                              [disabled]=!responseObj.Compensation.fields.Compensation_ratio.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Compensation.fields.Compensation_ratio.required\"\n                                              (change)=\"fieldRequiredStatusforCompensation($event,'Compensation_ratio')\"\n                                              [disabled]=\"!responseObj.Compensation.fields.Compensation_ratio.show || responseObj.Compensation.fields.Compensation_ratio.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n\n\n                                      </tbody>\n                                    </table>\n                                  </div>\n                                </div>\n                              </div>\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#formats\"\n                                      href=\"#emergency\">\n                                      Emergency Contact\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"emergency\" class=\"panel-collapse collapse\">\n                                  <div class=\"panel-body reports-table\">\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th [ngClass]=\"{'zenwork-checked-border': allFields}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'All')\"></mat-checkbox>\n                                          </th>\n                                          <th>Field Name</th>\n\n                                          <th class=\"format\">Format</th>\n                                          <th>Disabled</th>\n                                          <th>Required</th>\n                                          <th>\n                                            <!-- <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a (click)=\"editFields()\">Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div> -->\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Name.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Name.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Name')\"></mat-checkbox>\n                                          </td>\n                                          <td>Name</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Name')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Name.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Name.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <!-- <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Name.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Name')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Name.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Name.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Name')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Name.show || responseObj.Emergency_Contact.fields.Name.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Relationship.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Relationship.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Relationship')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Realtionship</td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Relationship.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Relationship')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Relationship.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Relationship.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Relationship')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Relationship.show || responseObj.Emergency_Contact.fields.Relationship.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Work_Phone.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Phone.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Work_Phone')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Work Phone</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Work_Phone')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Phone.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Work_Phone.show>\n                                              <!-- <mat-option value=\"text\">Alpha</mat-option> -->\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <!-- <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Phone.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Work_Phone')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Work_Phone.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Phone.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Work_Phone')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Work_Phone.show || responseObj.Emergency_Contact.fields.Work_Phone.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Ext.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Ext.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Ext')\"></mat-checkbox>\n                                          </td>\n                                          <td>Ext</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Ext')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Ext.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Ext.show>\n                                              <!-- <mat-option value=\"text\">Alpha</mat-option> -->\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <!-- <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Ext.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Ext')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Ext.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Ext.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Ext')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Ext.show || responseObj.Emergency_Contact.fields.Ext.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Mobile_Phone.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Mobile_Phone.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Mobile_Phone')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Mobile Phone</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Mobile_Phone')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Mobile_Phone.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Mobile_Phone.show>\n                                              <!-- <mat-option value=\"text\">Alpha</mat-option> -->\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <!-- <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Mobile_Phone.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Mobile_Phone')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Mobile_Phone.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Mobile_Phone.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Mobile_Phone')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Mobile_Phone.show || responseObj.Emergency_Contact.fields.Mobile_Phone.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Home_Phone.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Phone.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Home_Phone')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Home Phone</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Home_Phone')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Phone.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Home_Phone.show>\n                                              <!-- <mat-option value=\"text\">Alpha</mat-option> -->\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <!-- <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option> -->\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Phone.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Home_Phone')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Home_Phone.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Phone.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Home_Phone')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Home_Phone.show || responseObj.Emergency_Contact.fields.Home_Phone.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Work_Email.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Email.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Work_Email')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Work_Email</td>\n                                          <td>\n                                            Email\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Email.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Work_Email')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Work_Email.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Work_Email.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Work_Email')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Work_Email.show || responseObj.Emergency_Contact.fields.Work_Email.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Home_Email.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Email.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Home_Email')\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>Home Email</td>\n                                          <td>\n                                            Email\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Email.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Home_Email')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Home_Email.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Home_Email.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Home_Email')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Home_Email.show ||responseObj.Emergency_Contact.fields.Home_Email.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Street_1.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_1.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Street_1')\"></mat-checkbox>\n                                          </td>\n                                          <td>Street 1</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Street_1')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_1.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Street_1.show>\n                                              <mat-option value=\"Alpha\">Alpha</mat-option>\n                                              <mat-option value=\"Numeric\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_1.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Street_1')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Street_1.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_1.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Street_1')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Street_1.show || responseObj.Emergency_Contact.fields.Street_1.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Street_2.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_2.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Street_2')\"></mat-checkbox>\n                                          </td>\n                                          <td>Street 2</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Street_2')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_2.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Street_2.show>\n                                              <mat-option value=\"Alpha\">Alpha</mat-option>\n                                              <mat-option value=\"Numeric\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_2.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Street_2')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Street_2.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Street_2.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Street_2')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Street_2.show || responseObj.Emergency_Contact.fields.Street_2.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.City.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.City.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'City')\"></mat-checkbox>\n                                          </td>\n                                          <td>City </td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'City')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.City.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.City.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.City.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'City')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.City.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.City.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'City')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.City.show || responseObj.Emergency_Contact.fields.City.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.State.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.State.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'State')\"></mat-checkbox>\n                                          </td>\n                                          <td>State </td>\n                                          <td>\n                                            Alpha\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.State.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'State')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.State.show>\n                                            </mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.State.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'State')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.State.show ||responseObj.Emergency_Contact.fields.State.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n                                        <tr>\n                                          <td\n                                            [ngClass]=\"{'zenwork-checked-border': responseObj.Emergency_Contact.fields.Zip.show}\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Zip.show\"\n                                              (change)=\"fieldNameForEmergencyContact($event,'Zip')\"></mat-checkbox>\n                                          </td>\n                                          <td>Zip</td>\n                                          <td>\n                                            <mat-select placeholder=\"type\"\n                                              (ngModelChange)=\"formatChangeforEmergencyContact($event,'Zip')\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Zip.format\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Zip.show>\n                                              <mat-option value=\"text\">Alpha</mat-option>\n                                              <mat-option value=\"number\">Numeric</mat-option>\n                                              <mat-option value=\"AlphaNumeric\">Alpha-Numeric</mat-option>\n                                            </mat-select>\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Zip.hide\"\n                                              (change)=\"fieldDisableStatusforEmergencyContact($event,'Zip')\"\n                                              [disabled]=!responseObj.Emergency_Contact.fields.Zip.show></mat-checkbox>\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"responseObj.Emergency_Contact.fields.Zip.required\"\n                                              (change)=\"fieldRequiredStatusforEmergencyContact($event,'Zip')\"\n                                              [disabled]=\"!responseObj.Emergency_Contact.fields.Zip.show || responseObj.Emergency_Contact.fields.Zip.hide\">\n                                            </mat-checkbox>\n                                          </td>\n                                          <td>\n\n                                          </td>\n                                        </tr>\n\n\n\n                                      </tbody>\n                                    </table>\n                                  </div>\n                                </div>\n                              </div>\n                            </div>\n                            <div class=\"buttons\">\n                              <ul class=\"list-unstyled\">\n                                <li class=\"list-items\">\n                                  <a class=\"back\" (click)=\"modalRef.hide()\">Back</a>\n                                </li>\n                                <li class=\"list-items\">\n                                  <a class=\"cancel\" (click)=\"modalRef.hide()\">Cancel</a>\n                                </li>\n                                <li class=\"list-items pull-right\">\n                                  <button class='save' type=\"submit\" (click)=\"saveFormats()\">\n                                    Save Changes\n                                  </button>\n                                </li>\n\n                                <div class=\"clearfix\"></div>\n                              </ul>\n                            </div>\n                          </div>\n                        </div>\n\n\n\n\n\n                      </div>\n\n                    </div>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n                </ng-template>\n              </div>\n\n            </div>\n\n\n            <div class=\"field-settings no-bor\">\n\n              <a data-toggle=\"modal\">\n                <button class=\"btn zenwork-green\" (click)=\"openModalWithClass1(template1)\">\n                  Add / View / Edit - Custom Fields\n                </button>\n              </a>\n              <ng-template #template1>\n                <div class=\"modal-header modal-customfields\">\n                  <h4 class=\"modal-title pull-left\">Custom Fields</h4>\n\n                </div>\n                <div class=\"reports-table custom-fields-table\">\n                  <table class=\"table\">\n                    <thead>\n                      <tr>\n                        <th>\n\n                        </th>\n                        <th>Field Name</th>\n                        <th>Custom Field Location</th>\n                        <th>Format</th>\n                        <th>Disabled</th>\n                        <th>Required</th>\n                        <th>\n                          <div class=\"setting-drop\">\n                            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                            </a>\n                            <ul class=\"dropdown-menu\">\n                              <li *ngIf=\"selectedPackageDetails.length == 0\">\n                                <a (click)=\"openModalWithClass2(template2)\">Add</a>\n                              </li>\n                              <li *ngIf=\"selectedPackageDetails.length == 1\">\n                                <a (click)=\"editSingleCustomField(template2)\">Edit</a>\n                              </li>\n                              <li *ngIf=\"selectedPackageDetails.length > 0\">\n                                <a (click)=\"deleteFields()\">Delete</a>\n                              </li>\n\n                            </ul>\n                          </div>\n                        </th>\n                      </tr>\n                    </thead>\n                    <tbody>\n\n                      <tr *ngFor=\"let field of customFieldsData\">\n                        <td [ngClass]=\"{'zenwork-checked-border': field.show}\">\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"\n                            (change)=\"fieldNameForCustom($event,field._id)\"></mat-checkbox>\n                        </td>\n                        <td>{{field.fieldName}}</td>\n                        <td>{{field.fieldLocation}}</td>\n                        <td>{{field.fieldType}}</td>\n                        <td>\n                          <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"field.hide\"\n                            (change)=\"fieldDisableStatusforCustom($event,field)\" [disabled]=!field.show></mat-checkbox>\n                        </td>\n\n                        <td>\n                          <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"field.required\"\n                            (change)=\"fieldRequiredStatusforCustom($event,field)\" [disabled]=!field.show></mat-checkbox>\n                        </td>\n                        <td>\n\n                        </td>\n                      </tr>\n\n                    </tbody>\n                  </table>\n                </div>\n\n\n              </ng-template>\n              <ng-template #template2>\n                <div class=\"modal-header modal-customfields\">\n                  <h4 class=\"modal-title pull-left\">Add / Edit - Custom Fields</h4>\n\n                </div>\n                <div class=\"add-custom-field\">\n\n                  <div class=\"col-xs-4\">\n                    <div class=\"row\">\n                      <div class=\"form-group\">\n                        <label>Custom Field Name</label>\n                        <input type=\"text\" class=\"form-control field-name-input\" [(ngModel)]=\"customFields.fieldName\"\n                          placeholder=\"fieldname\" name=\"fieldName\" #fieldName=\"ngModel\" maxlength=\"22\" required>\n                        <p *ngIf=\"!customFields.fieldName && fieldName.touched || (fieldName.touched && !customFields.fieldName && isValid)\"\n                          class=\"error\">Please fill this field</p>\n                      </div>\n                      <div class=\"form-group edit-custom-field\">\n                        <label>Field Data Type</label>\n                        <mat-select placeholder=\"Select field type\" [(ngModel)]=\"customFields.fieldType\"\n                          (ngModelChange)=\"fieldTypeChange()\" name=\"fieldType\" #fieldType=\"ngModel\" required>\n                          <mat-option value=\"Dropdown\">Dropdown</mat-option>\n                          <mat-option value=\"Text\">Text</mat-option>\n                          <mat-option value=\"Numeric\">Numeric</mat-option>\n                          <mat-option value=\"Yes/No\">Yes/No</mat-option>\n                          <mat-option value=\"Date\">Date</mat-option>\n                        </mat-select>\n                        <p *ngIf=\"!customFields.fieldType && fieldType.touched || (fieldType.touched  && !customFields.fieldType && isValid)\"\n                          class=\"error\">Please fill this field</p>\n\n                      </div>\n                      <div class=\"form-group edit-custom-field\">\n                        <label>Field Location</label>\n                        <mat-select placeholder=\"Select field location\">\n                          <mat-option value=\"custom\">custom</mat-option>\n                        </mat-select>\n\n                      </div>\n                      <div class=\"form-group\">\n                        <label>Custom Field Code</label>\n                        <input type=\"text\" class=\"form-control field-name-input\" [(ngModel)]=\"customFields.fieldCode\"\n                          placeholder=\"fieldname\" name=\"fieldCode\" #fieldCode=\"ngModel\" required>\n                        <p *ngIf=\"!customFields.fieldCode && fieldCode.touched || (fieldCode.touched  && !customFields.fieldCode && isValid)\"\n                          class=\"error\">Please fill this field</p>\n                      </div>\n                    </div>\n\n\n                  </div>\n                  <div *ngIf=\"customFields.fieldType == 'Dropdown'\" class=\"col-xs-4 \">\n                    <div class=\"row\">\n                      <div class=\"dropdown-listoptions\">\n                        <h4>Drop Down List Options</h4>\n                        <ul class=\"list-unstyled list-inline\">\n                          <li class=\"list-items list-inline-item\">\n                            No.Of Options\n                          </li>\n                          <li class=\"list-items list-inline-item\">\n                            <input type=\"text\" class=\"form-control\" [attr.value]=\"inputFields.length\" readonly>\n                          </li>\n                          <li class=\"list-items list-inline-item green\">\n                            <i class=\"fa fa-plus\" style=\"cursor: pointer;\" aria-hidden=\"true\" (click)=\"addInput()\"></i>\n\n                          </li>\n                          <li class=\"list-items list-inline-item green\">\n                            <i class=\"fa fa-minus\" style=\"cursor: pointer;\" aria-hidden=\"true\"\n                              (click)=\"removeInput()\"></i>\n                          </li>\n\n                        </ul>\n                        <div class=\"option-values\">\n                          <ul *ngFor=\"let input of inputFields; let i = index\" class=\"list-unstyled list-inline\">\n                            <li class=\"list-inline-item\">\n                              Option {{i + 1}}\n                            </li>\n                            <li class=\"list-inline-item\">\n                              <input type=\"text\" [(ngModel)]=\"input.value\" class=\"form-control\">\n\n                            </li>\n\n                          </ul>\n                        </div>\n                      </div>\n\n\n                    </div>\n                  </div>\n                  <div class=\"clearfix\"></div>\n                  <div class=\"buttons edit-field-buttons\">\n                    <ul class=\"list-unstyled \">\n\n                      <li class=\"list-items list-inline-item\">\n                        <a class=\"cancel\" (click)=\"modalRef.hide()\">Cancel</a>\n                      </li>\n                      <li class=\"list-items pull-right list-inline-item\">\n                        <button class='save' type=\"submit\" (click)=\"fieldAdd()\">\n                          Submit\n                        </button>\n                      </li>\n\n                      <div class=\"clearfix\"></div>\n                    </ul>\n\n                  </div>\n                </div>\n\n\n              </ng-template>\n\n\n\n\n            </div>\n          </div>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n\n<div class=\"buttons\">\n  <ul class=\"list-unstyled\">\n\n    <li class=\"list-items pull-right\">\n      <button class='save' type=\"submit\" (click)=\"saveandNext()\">\n        Save Changes\n      </button>\n    </li>\n\n    <div class=\"clearfix\"></div>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: FieldMaintenanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FieldMaintenanceComponent", function() { return FieldMaintenanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/employee.service */ "./src/app/services/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FieldMaintenanceComponent = /** @class */ (function () {
    function FieldMaintenanceComponent(modalService, companySettingsService, swalAlertService, router, employeeService) {
        this.modalService = modalService;
        this.companySettingsService = companySettingsService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.employeeService = employeeService;
        this.responseObj = {
            Personal: {
                hide: false,
                fields: {
                    First_Name: { format: "Alpha", hide: false, required: false, show: false },
                    Middle_Name: { format: "Alpha", hide: false, required: false, show: false },
                    Last_Name: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Preferred_Name: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Salutation: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Date_of_Birth: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Gender: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Maritial_Status: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Maritial_Status_Effective_Date: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Veteran_Status: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    SSN: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Race: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    },
                    Ethnicity: {
                        format: "Alpha",
                        hide: false,
                        required: false,
                        show: false
                    }
                }
            },
            Job: {
                hide: false,
                fields: {
                    Archive_Status: { show: false, format: "Alpha", hide: true, required: true },
                    Custom_1: { show: false, format: "Alpha", hide: true, required: true },
                    Custom_2: { show: false, format: "Alpha", hide: true, required: true },
                    EEO_Job_Category: { show: false, format: "Alpha", hide: true, required: true },
                    Employee_Id: { show: false, format: "Alpha", hide: true, required: true },
                    Employee_Self_Service_Id: { show: false, format: "Alpha", hide: true, required: true },
                    Employee_Type: { show: false, format: "Alpha", hide: true, required: true },
                    FLSA_Code: { show: false, format: "Alpha", hide: true, required: true },
                    Hire_Date: { show: false, format: "Alpha", hide: true, required: true },
                    Rehire_Date: { show: false, format: "Alpha", hide: true, required: true },
                    Rehire_Status: { show: false, format: "Alpha", hide: true, required: true },
                    Reporting_Location: { show: false, format: "Alpha", hide: true, required: true },
                    Site_AccessRole: { show: false, format: "Alpha", hide: true, required: true },
                    Specific_Workflow_Approval: { show: false, format: "Alpha", hide: true, required: true },
                    Termination_Date: { show: false, format: "Alpha", hide: true, required: true },
                    Termination_Reason: { show: false, format: "Alpha", hide: true, required: true },
                    Type_of_User_Role_type: { show: false, format: "Alpha", hide: true, required: true },
                    Vendor_Id: { show: false, format: "Alpha", hide: true, required: true },
                    Work_Location: { show: false, format: "Alpha", hide: true, required: true },
                }
            },
            Compensation: {
                hide: false,
                fields: {
                    Compensation_ratio: { show: false, format: "Alpha", hide: true, required: true },
                    Highly_compensated_Employee_type: { show: false, format: "Alpha", hide: true, required: true },
                    Nonpaid_Record: { show: false, format: "Alpha", hide: false, required: true },
                    Pay_frequency: { show: false, format: "Alpha", hide: true, required: true },
                    Pay_group: { show: false, format: "Alpha", hide: true, required: true },
                    Salary_Grade: { show: false, format: "Alpha", hide: true, required: true },
                }
            },
            Emergency_Contact: {
                hide: false,
                fields: {
                    City: { show: false, format: "Alpha", hide: false, required: false },
                    Ext: { show: false, format: "Alpha", hide: false, required: false },
                    Home_Email: { show: false, format: "Alpha", hide: false, required: false },
                    Home_Phone: { show: false, format: "Alpha", hide: false, required: false },
                    Mobile_Phone: { show: false, format: "Alpha", hide: false, required: false },
                    Name: { show: false, format: "Alpha", hide: false, required: false },
                    Relationship: { show: false, format: "Alpha", hide: false, required: false },
                    State: { show: false, format: "Alpha", hide: false, required: false },
                    Street_1: { show: false, format: "Alpha", hide: false, required: false },
                    Street_2: { show: false, format: "Alpha", hide: false, required: false },
                    Work_Email: { show: false, format: "Alpha", hide: false, required: false },
                    Work_Phone: { show: false, format: "Alpha", hide: false, required: false },
                    Zip: { show: false, format: "Alpha", hide: false, required: false },
                }
            },
            Offboarding: {
                hide: false
            },
            Onboarding: {
                hide: false
            },
            Performance: {
                hide: false
            },
            Time_Off: {
                hide: false
            },
            Training: {
                hide: false
            },
            Notes: {
                hide: false
            },
            Documents: {
                hide: false
            },
            Benefits: {
                hide: false
            },
            Assets: {
                hide: false
            }
        };
        this.chooseFields = true;
        this.chooseSteps = false;
        this.isValid = false;
        this.allFields = false;
        this.toggleSelectAll = false;
        this.employeeIdentity = {
            maskSSN: false,
            idNumber: {
                numberType: '',
                digits: '',
                firstNumber: ''
            }
        };
        this.customFields = {
            fieldName: '',
            fieldType: '',
            fieldCode: '',
            hide: false,
            required: true
        };
        this.selectEnable = true;
        this.empIdentityres = {
            maskSSN: '',
            idNumber: {
                numberType: '',
                digits: '',
                firstNumber: ''
            }
        };
        this.digits = [
            //   { value: 1, viewVlaue: '1 Digit' },
            // { value: 2, viewVlaue: '2 Digits' },
            { value: 3, viewVlaue: '3 Digits' },
            { value: 4, viewVlaue: '4 Digits' },
            { value: 5, viewVlaue: '5 Digits' },
            { value: 6, viewVlaue: '6 Digits' }
        ];
        this.selectedFieldName = [];
        this.inputFields = [];
        this.customArr = [];
        this.selectedPackageDetails = [];
        this.allEmployees = [];
    }
    FieldMaintenanceComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.listEmployees();
        this.selectedNav = 'chooseSteps';
        this.getEmpSettings();
    };
    /* Description: Get General Settings
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.getEmpSettings = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        this.companySettingsService.getEmpIdentitySettings(cId)
            .subscribe(function (res) {
            console.log(res);
            _this.empIdentityres = res.data;
            _this.employeeIdentity.maskSSN = _this.empIdentityres.maskSSN;
            console.log(_this.empIdentityres, _this.empIdentityres.idNumber);
            if (_this.empIdentityres.idNumber.numberType == 'Manual') {
                _this.employeeIdentity.idNumber.numberType = _this.empIdentityres.idNumber.numberType;
            }
            if (_this.empIdentityres.idNumber.numberType == 'system Generated') {
                _this.employeeIdentity.idNumber = _this.empIdentityres.idNumber;
            }
        }, function (err) {
        });
    };
    FieldMaintenanceComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.listEmployees();
    };
    // Author: Saiprakash G, Date: 23/05/2019
    // Search employees
    FieldMaintenanceComponent.prototype.searchFilter = function () {
        console.log(this.searchString);
        this.listEmployees();
    };
    /* Description: maskSsnChange field change
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.maskSsnChange = function () {
    };
    /* Description: employee IdNumber Settings Change
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.empIdNumberSettingsChange = function () {
    };
    // Author: Saiprakash G, Date: 23/05/2019
    // Get All Employees
    FieldMaintenanceComponent.prototype.listEmployees = function () {
        var _this = this;
        var data = {
            per_page: this.perPage,
            page_no: this.pageNo,
            search_query: this.searchString
        };
        this.employeeService.getOnboardedEmployeeDetails(data).subscribe(function (res) {
            console.log(res);
            _this.allEmployees = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description:  employeeId number length Change
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.employeeIdformatChange = function (event) {
        console.log(event);
        this.isValid = false;
        this.employeeIdentity.idNumber.firstNumber = '';
        this.maxLength = event;
    };
    FieldMaintenanceComponent.prototype.empIdSettingsCancel = function () {
        this.isValid = true;
        this.employeeIdentity.maskSSN = '';
        this.employeeIdentity.idNumber.numberType = '';
        this.employeeIdentity.idNumber.digits = '';
        this.employeeIdentity.idNumber.digits = '';
    };
    FieldMaintenanceComponent.prototype.keyPress3 = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    /* Description:  modal popup open for View/Edit Stanadard fields
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.openModalWithClass = function (template) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
        this.getShowFields();
    };
    /* Description: In side modal popup View/Edit Stanadard get showing checked/unchecked fields
     author : vipin reddy */
    FieldMaintenanceComponent.prototype.getShowFields = function () {
        var _this = this;
        this.companySettingsService.getShowFeilds()
            .subscribe(function (res) {
            console.log("res of get", res);
            _this.getresponse = res;
            _this.responseObj = res.data.myInfo_sections;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: In side modal popup View/Edit Stanadard send checked/unchecked fields
   author : vipin reddy */
    FieldMaintenanceComponent.prototype.postShowFields = function () {
        var _this = this;
        console.log("11111111111", this.responseObj);
        var data = {
            myInfo_sections: this.responseObj
        };
        this.companySettingsService.postShowFeilds(data)
            .subscribe(function (res) {
            console.log("res of get", res);
            if (res.status == true) {
                _this.selectedNav == 'chooseFields';
                _this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success");
                // this.selectedNav == 'chooseFields'
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description:  modal popup open for Add /View/Edit Custom fields
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.openModalWithClass1 = function (template1) {
        this.selectedPackageDetails = [];
        this.modalRef = this.modalService.show(template1, Object.assign({}, { class: 'gray modal-lg' }));
        this.getAllCustomFields();
    };
    /* Description:  modal popup open for Add single customfield
  author : vipin reddy */
    FieldMaintenanceComponent.prototype.openModalWithClass2 = function (template2) {
        this.modalRef = this.modalService.show(template2, Object.assign({}, { class: 'gray modal-lg' }));
    };
    /* Description: view/edit standard fields select the checkbox
    checking/unchecking function on Edit functionality
    1)personal section
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldName = function (event, fieldname) {
        console.log(event, fieldname);
        if (fieldname == 'All') {
            this.allFields = event.checked;
            this.responseObj.Personal.fields.First_Name.show = event.checked;
            this.responseObj.Personal.fields.Middle_Name.show = event.checked;
            this.responseObj.Personal.fields.Last_Name.show = event.checked;
            this.responseObj.Personal.fields.Preferred_Name.show = event.checked;
            this.responseObj.Personal.fields.Salutation.show = event.checked;
            this.responseObj.Personal.fields.Date_of_Birth.show = event.checked;
            this.responseObj.Personal.fields.Gender.show = event.checked;
            this.responseObj.Personal.fields.Maritial_Status.show = event.checked;
            this.responseObj.Personal.fields.Maritial_Status_Effective_Date.show = event.checked;
            this.responseObj.Personal.fields.Veteran_Status.show = event.checked;
            this.responseObj.Personal.fields.SSN.show = event.checked;
            this.responseObj.Personal.fields.Race.show = event.checked;
            this.responseObj.Personal.fields.Ethnicity.show = event.checked;
        }
        else {
            var toChange = fieldname;
            this.responseObj.Personal.fields[toChange].show = event.checked;
        }
    };
    /* 2)job section */
    FieldMaintenanceComponent.prototype.fieldNameForJob = function (event, fieldname) {
        console.log(event, fieldname);
        if (fieldname == 'All') {
            this.allFields = event.checked;
            this.responseObj.Job.fields.Employee_Id.show = event.checked;
            this.responseObj.Job.fields.Employee_Type.show = event.checked;
            this.responseObj.Job.fields.Employee_Self_Service_Id.show = event.checked;
            this.responseObj.Job.fields.Work_Location.show = event.checked;
            this.responseObj.Job.fields.Reporting_Location.show = event.checked;
            this.responseObj.Job.fields.Hire_Date.show = event.checked;
            this.responseObj.Job.fields.Rehire_Date.show = event.checked;
            this.responseObj.Job.fields.Termination_Date.show = event.checked;
            this.responseObj.Job.fields.Termination_Reason.show = event.checked;
            this.responseObj.Job.fields.Rehire_Status.show = event.checked;
            this.responseObj.Job.fields.Archive_Status.show = event.checked;
            this.responseObj.Job.fields.FLSA_Code.show = event.checked;
            this.responseObj.Job.fields.EEO_Job_Category.show = event.checked;
            this.responseObj.Job.fields.Vendor_Id.show = event.checked;
            this.responseObj.Job.fields.Specific_Workflow_Approval.show = event.checked;
            this.responseObj.Job.fields.Site_AccessRole.show = event.checked;
            this.responseObj.Job.fields.Type_of_User_Role_type.show = event.checked;
            this.responseObj.Job.fields.Custom_1.show = event.checked;
            this.responseObj.Job.fields.Custom_2.show = event.checked;
        }
        else {
            var toChange = fieldname;
            this.responseObj.Job.fields[toChange].show = event.checked;
        }
    };
    /*3)compensation */
    FieldMaintenanceComponent.prototype.fieldNameForCompensation = function (event, fieldname) {
        console.log(event, fieldname);
        if (fieldname == 'All') {
            this.allFields = event.checked;
            this.responseObj.Compensation.fields.Nonpaid_Record.show = event.checked;
            this.responseObj.Compensation.fields.Highly_compensated_Employee_type.show = event.checked;
            this.responseObj.Compensation.fields.Pay_group.show = event.checked;
            this.responseObj.Compensation.fields.Pay_frequency.show = event.checked;
            this.responseObj.Compensation.fields.Salary_Grade.show = event.checked;
            this.responseObj.Compensation.fields.Compensation_ratio.show = event.checked;
        }
        else {
            var toChange = fieldname;
            this.responseObj.Compensation.fields[toChange].show = event.checked;
        }
    };
    /* 3)fieldsNameforEmergency */
    FieldMaintenanceComponent.prototype.fieldNameForEmergencyContact = function (event, fieldname) {
        if (fieldname == 'All') {
            this.allFields = event.checked;
            this.responseObj.Emergency_Contact.fields.City.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Ext.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Home_Email.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Home_Phone.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Mobile_Phone.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Name.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Relationship.show = event.checked;
            this.responseObj.Emergency_Contact.fields.State.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Street_1.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Street_2.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Work_Email.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Work_Phone.show = event.checked;
            this.responseObj.Emergency_Contact.fields.Zip.show = event.checked;
        }
        else {
            var toChange = fieldname;
            this.responseObj.Emergency_Contact.fields[toChange].show = event.checked;
        }
    };
    /* Description: view/edit standard fields select the checkbox
    checking/unchecking for disable property field
    1)personal section
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldDisableStatus = function (event, fieldname) {
        console.log(fieldname, event);
        var toChange = fieldname;
        this.responseObj.Personal.fields[toChange].hide = event.checked;
        this.responseObj.Personal.fields[toChange].required = !event.checked;
        console.log(this.responseObj.Personal.fields[toChange].hide);
    };
    /*2)job section*/
    FieldMaintenanceComponent.prototype.fieldDisableStatusforJob = function (event, fieldname) {
        console.log(fieldname, event);
        var toChange = fieldname;
        this.responseObj.Job.fields[toChange].hide = event.checked;
        this.responseObj.Job.fields[toChange].required = !event.checked;
        console.log(this.responseObj.Job.fields[toChange].hide);
    };
    /* 3)compensation section */
    FieldMaintenanceComponent.prototype.fieldDisableStatusforCompensation = function (event, fieldname) {
        var toChange = fieldname;
        this.responseObj.Compensation.fields[toChange].hide = event.checked;
        this.responseObj.Compensation.fields[toChange].required = !event.checked;
    };
    /*4)emergency contact*/
    FieldMaintenanceComponent.prototype.fieldDisableStatusforEmergencyContact = function (event, fieldname) {
        var toChange = fieldname;
        this.responseObj.Emergency_Contact.fields[toChange].hide = event.checked;
        this.responseObj.Emergency_Contact.fields[toChange].required = !event.checked;
    };
    /* Description: view/edit standard fields select the checkbox
  checking/unchecking for required field
  1)personal section
  author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldRequiredStatus = function (event, fieldName) {
        var toChange = fieldName;
        this.responseObj.Personal.fields[toChange].required = event.checked;
    };
    /* 2) Job section */
    FieldMaintenanceComponent.prototype.fieldRequiredStatusforJob = function (event, fieldname) {
        var toChange = fieldname;
        this.responseObj.Job.fields[toChange].required = event.checked;
    };
    /* 3) compensation section */
    FieldMaintenanceComponent.prototype.fieldRequiredStatusforCompensation = function (event, fieldname) {
        var toChange = fieldname;
        this.responseObj.Compensation.fields[toChange].required = event.checked;
    };
    /* 4)emergenccy contact */
    FieldMaintenanceComponent.prototype.fieldRequiredStatusforEmergencyContact = function (event, fieldname) {
        var toChange = fieldname;
        this.responseObj.Emergency_Contact.fields[toChange].required = event.checked;
    };
    /* Description: view/edit standard fields select the
    format slecting dropdown field
    1)personal fields
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.formatChange = function ($event, fieldName) {
        console.log($event);
        var toChange = fieldName;
        this.responseObj.Personal.fields[toChange].format = $event;
    };
    /* 2)Jobfields*/
    FieldMaintenanceComponent.prototype.formatChangeforJob = function ($event, fieldname) {
        console.log($event);
        var toChange = fieldname;
        this.responseObj.Job.fields[toChange].format = $event;
    };
    /*3)Compensation*/
    FieldMaintenanceComponent.prototype.formatChangeforCompensation = function ($event, fieldName) {
        var toChange = fieldName;
        this.responseObj.Compensation.fields[toChange].format = $event;
    };
    /*4)emergency contacct*/
    FieldMaintenanceComponent.prototype.formatChangeforEmergencyContact = function ($event, fieldName) {
        var toChange = fieldName;
        this.responseObj.Emergency_Contact.fields[toChange].format = $event;
    };
    /* Description: view/edit standard fields each section show/hide functionality
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.toggleValue = function (event, name) {
        console.log(event, name);
        this.selectedField = name;
        if (this.selectedField == 'offBoarding') {
            this.responseObj.Offboarding.hide = !event.value;
        }
        else if (this.selectedField == 'personal') {
            this.responseObj.Personal.hide = !event.value;
        }
        else if (this.selectedField == 'job') {
            this.responseObj.Job.hide = !event.value;
        }
        else if (this.selectedField == 'timeOff') {
            this.responseObj.Time_Off.hide = !event.value;
        }
        else if (this.selectedField == 'emergencyContact') {
            this.responseObj.Emergency_Contact.hide = !event.value;
        }
        else if (this.selectedField == 'performance') {
            this.responseObj.Performance.hide = !event.value;
        }
        else if (this.selectedField == 'compensation') {
            this.responseObj.Compensation.hide = !event.value;
        }
        else if (this.selectedField == 'notes') {
            this.responseObj.Notes.hide = !event.value;
        }
        else if (this.selectedField == 'benefits') {
            this.responseObj.Benefits.hide = !event.value;
        }
        else if (this.selectedField == 'training') {
            this.responseObj.Training.hide = !event.value;
        }
        else if (this.selectedField == 'documnets') {
            this.responseObj.Documents.hide = !event.value;
        }
        else if (this.selectedField == 'assets') {
            this.responseObj.Assets.hide = !event.value;
        }
        else if (this.selectedField == 'onBoarding') {
            this.responseObj.Onboarding.hide = !event.value;
        }
    };
    /* Description: view/edit standard fields show/hide functionality for all tabs
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.toggleValueAll = function (event) {
        console.log(event);
        // if(this.personalField == false || this.jobField == false){
        //   this.personalField = !this.personalField;
        //   this.jobField = !this.jobField;
        // }
        if (event.value == false || event.value == true) {
            console.log(this.toggleSelectAll);
            this.toggleSelectAll = !event.value;
            this.responseObj.Personal.hide = !event.value;
            this.responseObj.Job.hide = !event.value;
            this.responseObj.Offboarding.hide = !event.value;
            this.responseObj.Time_Off.hide = !event.value;
            this.responseObj.Emergency_Contact.hide = !event.value;
            this.responseObj.Performance.hide = !event.value;
            this.responseObj.Compensation.hide = !event.value;
            this.responseObj.Notes.hide = !event.value;
            this.responseObj.Benefits.hide = !event.value;
            this.responseObj.Training.hide = !event.value;
            this.responseObj.Documents.hide = !event.value;
            this.responseObj.Assets.hide = !event.value;
            this.responseObj.Onboarding.hide = !event.value;
        }
    };
    /* Description: side nav activation in  view/edit standard fields popup
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.chooseFieldss = function (event) {
        this.selectedNav = event;
        if (event == "chooseFields") {
            this.chooseFields = true;
            this.chooseSteps = false;
        }
        else {
            this.chooseFields = false;
            this.chooseSteps = true;
        }
    };
    /* Description:Post the employee identification settings
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.empIdentitySettings = function () {
        var _this = this;
        this.isValid = true;
        if (this.employeeIdentity.idNumber.numberType) {
            if (this.employeeIdentity.idNumber.numberType == 'system Generated') {
                if (this.employeeIdentity.idNumber.digits && this.employeeIdentity.idNumber.firstNumber &&
                    this.employeeIdentity.idNumber.digits == this.employeeIdentity.idNumber.firstNumber.length) {
                    console.log(this.employeeIdentity, this.employeeIdentity.idNumber.firstNumber.length);
                    this.companySettingsService.empSettings(this.employeeIdentity)
                        .subscribe(function (res) {
                        console.log("EI Settingss", res);
                        if (res.status == true) {
                            _this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "success");
                            _this.getEmpSettings();
                            // this.employeeIdentity = {
                            //   maskSSN: false,
                            //   idNumber: {
                            //     numberType: 'Manual',
                            //     digits: '',
                            //     firstNumber: ''
                            //   }
                            // }
                        }
                        else {
                            _this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "error");
                        }
                    }, function (err) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", err.message, "error");
                    });
                }
                else {
                    this.swalAlertService.SweetAlertWithoutConfirmation('Employee ID Number', 'please enter selected format length of digits', 'error');
                }
            }
            if (this.employeeIdentity.idNumber.numberType == 'Manual') {
                console.log(this.employeeIdentity);
                this.companySettingsService.empSettings(this.employeeIdentity)
                    .subscribe(function (res) {
                    console.log("EI Settingss", res);
                    if (res.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "success");
                        _this.getEmpSettings();
                        // this.employeeIdentity = {
                        //   maskSSN: false,
                        //   idNumber: {
                        //     numberType: 'Manual',
                        //     digits: '',
                        //     firstNumber: ''
                        //   }
                        // }
                    }
                    else {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", res.message, "error");
                    }
                }, function (err) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Employee Identification Settings", err.message, "error");
                });
            }
        }
    };
    /* Description:Post the In side modal popup View/Edit Stanadard format&requirments each field data post
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.saveFormats = function () {
        var _this = this;
        console.log("11111 post daata", this.responseObj);
        var postData = {
            myInfo_sections: this.responseObj
        };
        this.companySettingsService.postShowFeilds(postData)
            .subscribe(function (res) {
            console.log("res of get", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success");
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description:adding custom fields if dropdown adding dynamically
    each dropdown value adding to array
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.addInput = function () {
        this.inputFields.push({ value: '' });
    };
    /* Description:removing custom fields if dropdown adding dynamically
    each dropdown value adding to array
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.removeInput = function () {
        this.inputFields.pop();
    };
    /* Description:Post the cudtom-fields your adding
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldAdd = function () {
        var _this = this;
        this.isValid = true;
        if (this.customFields.fieldName && this.customFields.fieldType && this.customFields.fieldCode) {
            if (this.customFields.fieldType == 'Dropdown') {
                var x = this.customFields;
                x['dropDownOptions'] = this.inputFields;
            }
            if (this.singleCustomFieldId) {
                var y = this.customFields;
                y['_id'] = this.singleCustomFieldId;
            }
            console.log(this.customFields, this.inputFields.length);
            if (this.customFields.fieldType == 'Dropdown' && this.inputFields.length > 0) {
                this.companySettingsService.addCustomFields(this.customFields)
                    .subscribe(function (res) {
                    console.log("res of custom", res);
                    if (res.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success");
                        _this.customFields.fieldCode = '',
                            _this.customFields.fieldName = '',
                            _this.customFields.fieldType = '',
                            _this.inputFields = [];
                        _this.modalRef.hide();
                        _this.getAllCustomFields();
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            if (this.customFields.fieldType == 'Dropdown' && this.inputFields.length == 0) {
                this.swalAlertService.SweetAlertWithoutConfirmation('CustomFields', 'add dropdown options', 'error');
            }
            if (this.customFields.fieldType != 'Dropdown') {
                this.companySettingsService.addCustomFields(this.customFields)
                    .subscribe(function (res) {
                    console.log("res of custom", res);
                    if (res.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Update Fields", res.message, "success");
                        _this.customFields.fieldCode = '',
                            _this.customFields.fieldName = '',
                            _this.customFields.fieldType = '',
                            _this.inputFields = [];
                        _this.modalRef.hide();
                        _this.getAllCustomFields();
                    }
                }, function (err) {
                    console.log(err);
                });
            }
        }
    };
    /* Description:Get all cudtom-fields
  author : vipin reddy */
    FieldMaintenanceComponent.prototype.getAllCustomFields = function () {
        var _this = this;
        this.companySettingsService.getAllCustomFields()
            .subscribe(function (res) {
            console.log("res of custom", res);
            _this.customFieldsData = res.data.customFields;
        }, function (err) {
            console.log("err res", err);
        });
    };
    /* Description:single cudtom-field disable status
  author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldDisableStatusforCustom = function (event, data) {
        console.log(event, data);
        this.customFields = data;
        for (var i = 0; i < this.customFieldsData.length; i++) {
            if (this.customFieldsData[i]._id == data._id) {
                this.customFieldsData[i].hide = event.checked;
                this.customFieldsData[i].required = !event.checked;
            }
        }
        var y = this.customFields;
        y['_id'] = data._id;
        console.log("2525236", this.customFields);
        this.companySettingsService.addCustomFields(this.customFields)
            .subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description:single cudtom-field required status
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldRequiredStatusforCustom = function (event, data) {
        console.log(event, data);
        this.customFields = data;
        for (var i = 0; i < this.customFieldsData.length; i++) {
            if (this.customFieldsData[i]._id == data._id) {
                this.customFieldsData[i].required = event.checked;
            }
        }
        var y = this.customFields;
        y['_id'] = data._id;
        console.log("2525236", this.customFields);
        this.companySettingsService.addCustomFields(this.customFields)
            .subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description:single cudtom-field EDIT
    author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldNameForCustom = function (event, id) {
        console.log(event, id);
        // this.selectedPackageDetails = [];
        for (var i = 0; i < this.customFieldsData.length; i++) {
            if (this.customFieldsData[i]._id == id) {
                this.customFieldsData[i].show = event.checked;
                if (event.checked == true) {
                    this.selectedPackageDetails.push(this.customFieldsData[i]._id);
                    console.log('222222', this.selectedPackageDetails);
                }
                if (event.checked == false) {
                    for (var j = 0; j < this.selectedPackageDetails.length; j++) {
                        if (this.selectedPackageDetails[j] === id) {
                            this.selectedPackageDetails.splice(j, 1);
                            console.log(this.selectedPackageDetails, "155555555");
                        }
                    }
                }
            }
        }
        // this.singleCustomFieldId = id;   
    };
    /* Description:single single cudtom-field type change to dropdown
       dropdownfield array empty
     author : vipin reddy */
    FieldMaintenanceComponent.prototype.fieldTypeChange = function () {
        console.log('111111', this.inputFields);
        if (this.customFields.fieldType != 'Dropdown')
            this.inputFields = [];
        console.log('111111', this.inputFields);
    };
    /* Description:single cudtom-field EDIT
     author : vipin reddy */
    FieldMaintenanceComponent.prototype.editSingleCustomField = function (template2) {
        var _this = this;
        if (this.selectedPackageDetails.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Field", "Select only one Field to Proceed", 'error');
        }
        else if (this.selectedPackageDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Field", "Select a Field to Proceed", 'error');
        }
        else {
            this.companySettingsService.getSingleCustomFieldData(this.selectedPackageDetails[0])
                .subscribe(function (res) {
                console.log('res of single field', res);
                _this.customFields = res.data;
                console.log("singlefield data", _this.customFields);
                if (res.data.fieldType == 'Dropdown') {
                    _this.inputFields = res.data.dropDownOptions;
                }
                _this.openModalWithClass2(template2);
            }, function (err) {
                console.log(err);
            });
        }
    };
    /* Description:single cudtom-field Delete multiple
   author : vipin reddy */
    FieldMaintenanceComponent.prototype.deleteFields = function () {
        var _this = this;
        console.log('123123123', this.selectedPackageDetails);
        this.companySettingsService.deleteCustomFieldData(this.selectedPackageDetails)
            .subscribe(function (res) {
            console.log("delete res", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Delete Fields', res.message, 'success');
                _this.selectedPackageDetails = [];
                _this.getAllCustomFields();
            }
        }, function (err) {
        });
    };
    /* Description:move next page
  author : vipin reddy */
    FieldMaintenanceComponent.prototype.saveandNext = function () {
        this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/structure"]);
    };
    FieldMaintenanceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-field-maintenance',
            template: __webpack_require__(/*! ./field-maintenance.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.html"),
            styles: [__webpack_require__(/*! ./field-maintenance.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/field-maintenance/field-maintenance.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_employee_service__WEBPACK_IMPORTED_MODULE_5__["EmployeeService"]])
    ], FieldMaintenanceComponent);
    return FieldMaintenanceComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.css":
/*!****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.css ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;  \n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-general-form-settings{\n    width: 18%!important;\n    border-radius: 0px!important; border: none; box-shadow: none; height:auto;padding: 10px 12px;\n}\n\n.zenwork-br-zero{\n    border-radius: 0px!important;border: none;box-shadow: none; height: 120px; resize: none; padding: 10px; line-height: 20px;\n}\n\n/*--------- This is my code -------*/\n\n.field-accordion { padding:20px 0;}\n\n.field-accordion p {color: #565555;font-size: 15px;}\n\n.field-accordion .panel-default>.panel-heading { padding: 20px 0; background-color:transparent !important; border: none;}\n\n.field-accordion .panel-title { color: #5a5959; display: inline-block; font-size: 18px;}\n\n.field-accordion .panel-title a:hover { text-decoration: none;color: #5a5959;}\n\n.field-accordion .panel-title a:focus{color: #5a5959;}\n\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.field-accordion .panel-heading .accordion-toggle:after { margin:6px 10px 0; font-size: 12px; line-height: 10px;}\n\n.field-accordion .panel-body { padding: 0 0 5px;}\n\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.field-accordion .panel-default { border-color:transparent;}\n\n.field-accordion .panel-group .panel+.panel { margin-top:0;}\n\n.field-accordion .panel-heading { position: relative;}\n\n.administration-contact { display: block; padding: 0 0 20px;}\n\n.administration-contact ul { margin: 0 0 30px;}\n\n.administration-contact ul li {margin: 0 20px 15px 0; padding: 0;}\n\n.administration-contact ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n\n.administration-contact ul li .phone { display: block; background:#fff; padding: 0 10px; height:45px;  clear: left;}\n\n.administration-contact ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n\n.administration-contact ul li .phone span .fa { color: #636060;\n    font-size: 23px;\n    line-height: 20px;\n    width: 18px;\n    text-align: center;}\n\n.administration-contact ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 80%; border: none; background:transparent; box-shadow: none; padding:12px;line-height:inherit;}\n\n.administration-contact ul li .form-control { display: inline-block;vertical-align: middle; width: 40%; border: none; background:#fff; box-shadow: none; height: 40px; line-height: 40px; padding: 0 10px;}\n\n.administration-contact ul li.phone-width {  clear:left;width: 16%}\n\n.administration-contact .btn {border-radius: 20px; height:40px; line-height:38px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:30px 0 0; float:left;}\n\n.administration-contact .btn:hover {background: #008f3d; color:#fff;}\n\n.company-legal-info{\n    margin: 0 30px 0 0;\n}\n\n.company-legal-name{\n    border: none;\n    box-shadow: none;\n    height: auto;\n    padding: 12px;\n}\n\n.panel-border-remove{\n    border-bottom:none !important;\n}\n\n.company-address-city{\n    width: 16%;\n    margin: 0 15px 0 0;\n}\n\n.company-address-state{\n    width: 16%;\n    margin: 0 15px 0 0;\n}\n\n.verify-email-btn{\n    display: inline-block;\n    padding: 10px 0 0 0;\n    background: none;\n    box-shadow: none;\n    border: 0;\n    color: #ccc;\n}\n\n.verify-email-btn.disabled {\n    color: #f00;\n}\n\n.emailverification{\n    color: #000;\n}\n\n.buttons{\n    margin:0 0 30px;\n}\n\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n\n.buttons ul li .back{\n    border: 1px solid #a9a6a6;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    background: #fff;display: block;\n}\n\n.buttons ul li .save{\n\n    border:none;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background:#429348; font-size: 16px;\n}\n\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n\n.buttons ul li .save:focus{outline: none;border: none;}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"field-accordion\">\n\n  <div class=\"panel-group\" id=\"accordion1\">\n\n    <div class=\"panel panel-default border-bottom\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n            General\n          </a>\n        </h4>\n      </div>\n      <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n          <div class=\"form-group\">\n            <p>\n              Do you want to lock access to the site ?\n            </p>\n            <mat-select class=\"form-control zenwork-general-form-settings\"\n              [(ngModel)]=\"companyContactInfo.settings.general.lockAccessNeeded\" name=\"lockAccessNeeded\"\n              #lockAccessNeeded=\"ngModel\" required>\n\n              <mat-option [value]=true>Yes</mat-option>\n              <mat-option [value]=false>No</mat-option>\n            </mat-select>\n            \n          </div>\n\n          <div class=\"form-group\">\n            <p>\n              Do you want to boot all users currently in the site ?\n            </p>\n\n            <mat-select class=\"form-control zenwork-general-form-settings\"\n              [(ngModel)]=\"companyContactInfo.settings.general.bootAllUsersNeeded\" name=\"bootAllUsersNeeded\"\n              #bootAllUsersNeeded=\"ngModel\" required>\n              <mat-option [value]=true>Yes</mat-option>\n              <mat-option [value]=false>No</mat-option>\n            </mat-select>\n           \n          </div>\n          <div class=\"form-group\">\n            <p>\n              Do you want to post a message on the front page of the site before logging in?\n            </p>\n            <mat-select class=\"form-control zenwork-general-form-settings\"\n              [(ngModel)]=\"companyContactInfo.settings.general.messageOnFrontPageNeeded\" name=\"messageOnFrontPageNeeded\"\n              #messageOnFrontPageNeeded=\"ngModel\" required>\n              <mat-option [value]=true>Yes</mat-option>\n              <mat-option [value]=false>No</mat-option>\n            </mat-select>\n           \n          </div>\n          <div *ngIf=\"companyContactInfo.settings.general.messageOnFrontPageNeeded == true\" class=\"form-group\">\n            <textarea class=\"form-control zenwork-br-zero zenwork-text-area-settings\" rows=\"4\"\n              placeholder=\"Front page message\" [(ngModel)]=\"companyContactInfo.settings.general.messageOnFrontPage\"\n              name=\"messageOnFrontPage\" #messageOnFrontPage=\"ngModel\" maxlength=\"1000\" required></textarea>\n            <p *ngIf=\"!companyContactInfo.settings.general.messageOnFrontPage && messageOnFrontPage.touched || (!companyContactInfo.settings.general.messageOnFrontPage && isValid)\"\n              class=\"error\">Please fill this field</p>\n          </div>\n\n        </div>\n\n      </div>\n    </div>\n\n    <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#company-info\">\n            Company Information\n          </a>\n        </h4>\n      </div>\n      <div id=\"company-info\" class=\"panel-collapse collapse\">\n        <div class=\"panel-body\">\n          <div class=\"col-xs-12\">\n            <div class=\"row\">\n              <div class=\"col-xs-4 company-legal-info\">\n                <div class=\"row\">\n                  <div class=\"form-group\">\n                    <p>Company Legal Name*</p>\n                    <input type=\"text\" class=\"form-control company-legal-name\"\n                      [(ngModel)]=\"companyContactInfo.companyDetails.name\" name=\"name\" #name=\"ngModel\" required>\n                    <p *ngIf=\"!companyContactInfo.companyDetails.name && name.touched || (name.touched && !companyContactInfo.companyDetails.name && isValid)\"\n                      class=\"error\">Please fill this field</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-xs-4 company-legal-info\">\n                <div class=\"row\">\n                  <p>FEIN(Federal Employer ID Number)*</p>\n                  <input class=\"form-control company-legal-name\" [(ngModel)]=\"companyContactInfo.companyDetails.fein\"\n                    name=\"fein\" #fein=\"ngModel\" mask=\"00-00000000\" maxlength=\"10\" minlength=\"10\"\n                    [dropSpecialCharacters]=\"false\" required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.fein && fein.touched || (fein.touched && !companyContactInfo.companyDetails.fein && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"panel panel-default border-bottom\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#cmpny-address\">\n            Company Address\n          </a>\n        </h4>\n      </div>\n      <div id=\"cmpny-address\" class=\"panel-collapse collapse\">\n        <div class=\"panel-body\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-12\">\n              <div class=\"row\">\n                <div class=\"col-xs-4 company-legal-info\">\n                  <div class=\"row\">\n                    <div class=\"form-group\">\n                      <input type=\"text\" placeholder=\"Street 1 *\" class=\" form-control company-legal-name\"\n                        [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.address1\" name=\"address1\"\n                        #address1=\"ngModel\" required>\n                      <p *ngIf=\"!companyContactInfo.companyDetails.primaryAddress.address1 && address1.touched || (address1.touched && !companyContactInfo.companyDetails.primaryAddress.address1 && isValid)\"\n                        class=\"error\">Please fill this field</p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-xs-12\">\n              <div class=\"row\">\n                <div class=\"col-xs-4 company-legal-info\">\n                  <div class=\"row\">\n                    <div class=\"form-group\">\n                      <input type=\"text\" placeholder=\"Street 2 *\" class=\" form-control company-legal-name\"\n                        [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.address2\" name=\"address2\"\n                        #address2=\"ngModel\" required>\n\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-xs-12 company-address\">\n              <div class=\"row\">\n                <div class=\"col-xs-2 company-address-city\">\n                  <div class=\"row\">\n                    <div class=\"form-group\">\n                      <input type=\"text\" placeholder=\"City *\" class=\" form-control company-legal-name\" onkeypress=\"return (event.charCode > 64 && \n                          event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)\"\n                        [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.city\" name=\"city\" #city=\"ngModel\"\n                        required>\n                      <p *ngIf=\"!companyContactInfo.companyDetails.primaryAddress.city && city.touched || (city.touched && !companyContactInfo.companyDetails.primaryAddress.city && isValid)\"\n                        class=\"error\">Please fill this field</p>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col-xs-2 company-address-state\">\n                  <div class=\"row\">\n                    <mat-select class=\"form-control\" placeholder=\"State\"\n                      [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.state\"\n                      (ngModelChange)=\"comapanyConactCountrty()\" name=\"state\" #state=\"ngModel\" required>\n                      <mat-option *ngFor=\"let data of stateArray\" value='data.name'>\n                          {{data.name}}</mat-option>\n                    </mat-select>\n                    <p *ngIf=\"!companyContactInfo.companyDetails.primaryAddress.state && state.touched || (!companyContactInfo.companyDetails.primaryAddress.state && isValid)\"\n                      class=\"error\">Please fill this field</p>\n                  </div>\n                </div>\n                <div class=\"col-xs-2\">\n                  <div class=\"row\">\n                    <div class=\"form-group\">\n                      <input type=\"text\" placeholder=\"Zip *\" maxlength=\"5\" (keypress)=\"keyPress($event)\"\n                        class=\" form-control company-legal-name\"\n                        [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.zipcode\" name=\"zipcode\"\n                        #zipcode=\"ngModel\" (ngModelChange)=\"phoneNumberChange('primaryAddress','zipcode')\" required>\n                      <p *ngIf=\"!companyContactInfo.companyDetails.primaryAddress.zipcode && zipcode.touched || (zipcode.touched && !companyContactInfo.companyDetails.primaryAddress.zipcode && isValid)\"\n                        class=\"error\">Please fill this field</p>\n                        <p *ngIf = \"zipcodeCompanyAddress\" class=\"error\"> Please enter numeric values</p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n\n            <div class=\"col-xs-12\">\n              <div class=\"row\">\n                <div class=\"col-xs-2 company-address-state\">\n                  <div class=\"row company-address\">\n                    <mat-select class=\"form-control\" placeholder=\"Country\"\n                      [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.country\" name=\"country\"\n                      #country=\"ngModel\" required>\n                      <mat-option *ngFor=\"let data of countryArray\" [value]='data.name'>\n                          {{data.name}}</mat-option>\n                    </mat-select>\n                    <p *ngIf=\"!companyContactInfo.companyDetails.primaryAddress.country && country.touched || (country.touched && !companyContactInfo.companyDetails.primaryAddress.country && isValid)\"\n                      class=\"error\">Please fill this field</p>\n                  </div>\n                </div>\n                <div class=\"col-xs-2 company-address-state\">\n                  <div class=\"row\">\n                    <div class=\"form-group\">\n                      <input type=\"text\" [(ngModel)]=\"companyContactInfo.companyDetails.primaryAddress.phone\"\n                        placeholder=\"Telephone Number *\" maxlength=\"10\" (ngModelChange)=\"phoneNumberChange('primaryAddress','phone')\" (keypress)=\"keyPress($event)\"\n                        class=\" form-control company-legal-name\" name=\"phone\" #phone=\"ngModel\" required>\n                      <p *ngIf=\"!companyContactInfo.companyDetails.primaryAddress.phone && phone.touched || (phone.touched && !companyContactInfo.companyDetails.primaryAddress.phone && isValid)\"\n                        class=\"error\">Please fill this field</p>\n                        <p *ngIf = \"phoneCompanyAddress\" class=\"error\">Please enter numeric values </p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#compny-info-primary\">\n            Company Contact (Primary)\n          </a>\n        </h4>\n      </div>\n      <div id=\"compny-info-primary\"\n        class=\"panel-collapse                                                                                                                                                                                                                                                 collapse\">\n        <div class=\"panel-body\">\n          <div class=\"administration-contact\">\n\n            <ul>\n              <li>\n                <h4>Email</h4>\n              </li>\n              <li class=\"col-md-3\">\n                <div class=\"phone\">\n                  <span>\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Work Email*\"\n                    pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\"\n                    [(ngModel)]=\"companyContactInfo.companyDetails.primaryContact.email\"\n                    (ngModelChange)=\"emailStatus($event)\" name=\"email1\" #email1=\"ngModel\" required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.primaryContact.email && email1.touched || (!companyContactInfo.companyDetails.primaryContact.email && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                  <p *ngIf=companyContactInfo.companyDetails.primaryContact.email\n                    [hidden]=\"companyContactInfo.companyDetails.primaryContact.email.untouched || email1.valid \"\n                    class=\"error\">Please\n                    enter a valid email </p>\n                </div>\n              </li>\n\n              <span *ngIf=\"companyContactInfo.companyDetails.primaryContact.isEmailVerified\"\n                class=\"verify-email-btn\">Verified</span>\n              <button class=\"verify-email-btn\" [ngClass]=\"{'emailverification':!emialFirstDisable}\"\n                *ngIf=\"!companyContactInfo.companyDetails.primaryContact.isEmailVerified\" [disabled]=\"emialFirstDisable\"\n                (click)=\"sendVerificationEmail(companyContactInfo.companyDetails.primaryContact.email)\">\n                Verify </button>\n\n              <div class=\"clearfix\"></div>\n              <li class=\"col-md-3 phone-width\">\n                <div class=\"phone\">\n                  <span>\n\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"text\" (keypress)=\"keyPress($event)\" maxlength=\"10\" class=\"form-control\"\n                    placeholder=\"Work Phone*\" [(ngModel)]=\"companyContactInfo.companyDetails.primaryContact.phone\"\n                    name=\"phone1\" #phone1=\"ngModel\" (ngModelChange)=\"phoneNumberChange('primaryContact','phone')\"\n                    required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.primaryContact.phone && phone1.touched || (phone1.touched && !companyContactInfo.companyDetails.primaryContact.phone && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                  <p *ngIf=\"phoneNumeric\" class=\"error\">Please enter numeric values</p>\n                </div>\n              </li>\n              <li class=\"col-md-2 \">\n\n                <input type=\"text\" (keypress)=\"keyPress($event)\" class=\"form-control\" maxlength=\"4\" placeholder=\"Extn\"\n                  [(ngModel)]=\"companyContactInfo.companyDetails.primaryContact.extention\"\n                  (ngModelChange)=\"phoneNumberChange('primaryContact','extention')\" name=\"extention\"\n                  #extention=\"ngModel\" required>\n                <p *ngIf=\"!companyContactInfo.companyDetails.primaryContact.extention && extention.touched || (extention.touched && !companyContactInfo.companyDetails.primaryContact.extention && isValid)\"\n                  class=\"error\">Please fill this field</p>\n                <p *ngIf=\"extentionNumeric\" class=\"error\">Please enter numeric values</p>\n              </li>\n\n              <div class=\"clearfix\"></div>\n              <li class=\"col-md-3 phone-width\">\n                <div class=\"phone\">\n                  <span>\n                    <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                  </span>\n                  <input type=\"text\" (keypress)=\"keyPress($event)\" maxlength=\"10\" class=\"form-control\"\n                    placeholder=\"Mobile Phone*\"\n                    [(ngModel)]=\"companyContactInfo.companyDetails.primaryContact.mobilePhone\" name=\"mobilePhone\"\n                    #mobilePhone=\"ngModel\" (ngModelChange)=\"phoneNumberChange('primaryContact','mobilePhone')\" required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.primaryContact.mobilePhone && mobilePhone.touched || (mobilePhone.touched && !companyContactInfo.companyDetails.primaryContact.mobilePhone && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                  <p *ngIf=\"mobilePhoneNumeric\" class=\"error\">Please enter numeric values</p>\n                </div>\n              </li>\n            </ul>\n\n            <ul>\n\n              <!-- <li>\n                      <h4>Phone</h4>\n                    </li>\n    \n                    <li class=\"col-md-3 phone-width\">\n                      <div class=\"phone\">\n                        <span>\n                          <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                            height=\"16\" alt=\"img\">\n                        </span>\n                        <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyContactInfo.workEmail\">\n                      </div>\n                    </li>\n    \n                    <li class=\"col-md-3 phone-width\">\n                      <div class=\"phone\">\n                        <span>\n                          <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                        </span>\n                        <input type=\"email\" class=\"form-control\" placeholder=\"Home Email\" [(ngModel)]=\"companyContactInfo.homeEmail\">\n                      </div>\n                    </li> -->\n            </ul>\n          </div>\n\n\n\n        </div>\n      </div>\n    </div>\n\n    <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#compny-info-secondary\">\n            Company Contact (Secondary)\n          </a>\n        </h4>\n      </div>\n      <div id=\"compny-info-secondary\" class=\"panel-collapse collapse\">\n        <div class=\"panel-body\">\n          <div class=\"administration-contact\">\n\n            <ul>\n              <li>\n                <h4>Email</h4>\n              </li>\n              <li class=\"col-md-3\">\n                <div class=\"phone\">\n                  <span>\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Work Email*\"\n                    [(ngModel)]=\"companyContactInfo.companyDetails.secondaryContact.email\"\n                    (ngModelChange)=\"emailStatus2()\" name=\"email\" #email=\"ngModel\" required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.secondaryContact.email && email.touched || (!companyContactInfo.companyDetails.secondaryContact.email && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                </div>\n              </li>\n              <span *ngIf=\"companyContactInfo.companyDetails.secondaryContact.isEmailVerified\"\n                class=\"verify-email-btn\">Verified</span>\n              <span class=\"disable\">\n                <button [ngClass]=\"{'emailverification':!emialSecondDisable}\" class=\"verify-email-btn\"\n                  data-title=\"Dieser Link führt zu Google\"\n                  *ngIf=\"!companyContactInfo.companyDetails.secondaryContact.isEmailVerified\"\n                  [disabled]=\"emialSecondDisable\"\n                  (click)=\"sendVerificationEmail(companyContactInfo.companyDetails.secondaryContact.email)\">\n                  Verify </button>\n              </span>\n              <li class=\"col-md-3 phone-width\">\n                <div class=\"phone\">\n                  <span>\n\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"text\" (keypress)=\"keyPress($event)\" maxlength=\"10\" class=\"form-control\"\n                    placeholder=\"Work Phone*\" [(ngModel)]=\"companyContactInfo.companyDetails.secondaryContact.phone\"\n                    name=\"phone2\" (ngModelChange)=\"phoneNumberChange('secondaryContact','phone')\" #phone2=\"ngModel\"\n                    required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.secondaryContact.phone && phone2.touched || (!companyContactInfo.companyDetails.secondaryContact.phone && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                  <p *ngIf=\"phoneSecondary\" class=\"error\">please enter numeric values</p>\n                </div>\n              </li>\n              <li class=\"col-md-2 \">\n\n                <input type=\"text\" class=\"form-control\" (keypress)=\"keyPress($event)\" maxlength=\"4\" placeholder=\"Extn\"\n                  [(ngModel)]=\"companyContactInfo.companyDetails.secondaryContact.extention\"\n                  (ngModelChange)=\"phoneNumberChange('secondaryContact','extention')\" name=\"extention2\"\n                  #extention2=\"ngModel\" required>\n                <p *ngIf=\"!companyContactInfo.companyDetails.secondaryContact.extention && extention2.touched || (extention2.touched && !companyContactInfo.companyDetails.secondaryContact.extention && isValid)\"\n                  class=\"error\">Please fill this field</p>\n                <p *ngIf=\"extentionSecondary\" class=\"error\">please enter numeric values</p>\n              </li>\n              <div class=\"clearfix\"></div>\n              <li class=\"col-md-3 phone-width\">\n                <div class=\"phone\">\n                  <span>\n                    <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                  </span>\n                  <input type=\"text\" class=\"form-control\" (keypress)=\"keyPress($event)\" maxlength=\"10\"\n                    placeholder=\"Mobile Phone*\"\n                    [(ngModel)]=\"companyContactInfo.companyDetails.secondaryContact.mobilePhone\"\n                    (ngModelChange)=\"phoneNumberChange('secondaryContact','mobilePhone')\" name=\"mobilePhone2\"\n                    #mobilePhone2=\"ngModel\" required>\n                  <p *ngIf=\"!companyContactInfo.companyDetails.secondaryContact.mobilePhone && mobilePhone2.touched || (mobilePhone2.touched && !companyContactInfo.companyDetails.secondaryContact.mobilePhone && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                  <p *ngIf=\"mobilephoneSecondary\" class=\"error\">please enter numeric values</p>\n                </div>\n              </li>\n            </ul>\n\n            <ul>\n\n              <!-- <li>\n                        <h4>Phone</h4>\n                      </li>\n      \n                      <li class=\"col-md-3 phone-width\">\n                        <div class=\"phone\">\n                          <span>\n                            <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                              height=\"16\" alt=\"img\">\n                          </span>\n                          <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyContactInfo.workEmail\">\n                        </div>\n                      </li>\n      \n                      <li class=\"col-md-3 phone-width\">\n                        <div class=\"phone\">\n                          <span>\n                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                          </span>\n                          <input type=\"email\" class=\"form-control\" placeholder=\"Home Email\" [(ngModel)]=\"companyContactInfo.homeEmail\">\n                        </div>\n                      </li> -->\n            </ul>\n          </div>\n\n\n\n        </div>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n\n\n<div class=\"buttons\">\n  <ul class=\"list-unstyled\">\n    <li class=\"list-items\">\n      <a class=\"back\" (click)=\"previousPage()\">Back</a>\n    </li>\n    <li class=\"list-items\">\n      <a class=\"cancel\" (click)=\"cancelReset()\">Cancel</a>\n    </li>\n    <li class=\"list-items pull-right\">\n      <button class='save' type=\"submit\" (click)=\"saveCompanySetup()\">\n        Save Changes\n      </button>\n    </li>\n\n    <div class=\"clearfix\"></div>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: GeneralSettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralSettingsComponent", function() { return GeneralSettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared-module/confirmation/confirmation.component */ "./src/app/shared-module/confirmation/confirmation.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var GeneralSettingsComponent = /** @class */ (function () {
    function GeneralSettingsComponent(modalService, router, companySetupService, swalAlertService, messageService, dialog, onboardingService) {
        this.modalService = modalService;
        this.router = router;
        this.companySetupService = companySetupService;
        this.swalAlertService = swalAlertService;
        this.messageService = messageService;
        this.dialog = dialog;
        this.onboardingService = onboardingService;
        this.selectedField = false;
        this.isValid = false;
        this.emialSecondDisable = true;
        this.emialFirstDisable = true;
        this.companyContactInfo = {
            companyDetails: {
                primaryContact: {
                    email: '',
                    extention: "",
                    isEmailVerified: '',
                    // name: "test",
                    phone: "",
                    mobilePhone: '',
                },
                secondaryContact: {
                    extention: "",
                    isEmailVerified: false,
                    // name: "test",
                    email: "",
                    phone: "",
                    mobilePhone: ''
                },
                primaryAddress: {
                    address1: '',
                    address2: '',
                    city: '',
                    country: '',
                    state: '',
                    zipcode: '',
                    phone: ''
                },
                name: '',
                fein: ''
            },
            settings: {
                general: {
                    bootAllUsersNeeded: false,
                    lockAccessNeeded: false,
                    messageOnFrontPage: '',
                    messageOnFrontPageNeeded: false
                }
            }
        };
        this.mobilePhoneNumeric = false;
        this.phoneNumeric = false;
        this.extentionNumeric = false;
        this.mobilephoneSecondary = false;
        this.extentionSecondary = false;
        this.phoneSecondary = false;
        this.phoneCompanyAddress = false;
        this.zipcodeCompanyAddress = false;
        this.countryArray = [];
        this.stateArray = [];
    }
    GeneralSettingsComponent.prototype.ngOnInit = function () {
        this.getGeneralSettings();
        var cId = JSON.parse(localStorage.getItem('companyId'));
        this.getStandardAndCustomFieldsforStructure(cId);
    };
    /*Description: go to previous page
  author : vipin reddy */
    GeneralSettingsComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/company-settings']);
    };
    GeneralSettingsComponent.prototype.keyPress1 = function (event) {
        var pattern = /^[a-zA-Z]*$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    GeneralSettingsComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    /* Description: Get General Settings
    author : vipin reddy */
    GeneralSettingsComponent.prototype.getGeneralSettings = function () {
        var _this = this;
        this.companySetupService.getGeneralSettings()
            .subscribe(function (data) {
            if (data.status == true) {
                console.log("res", data);
                _this.companyContactInfo.settings.general = data.settings.general;
                _this.companyContactInfo.companyDetails.name = data.companyDetails.name;
                _this.companyContactInfo.companyDetails.fein = data.companyDetails.fein;
                _this.companyContactInfo.companyDetails.primaryAddress = data.companyDetails.primaryAddress;
                _this.companyContactInfo.companyDetails.primaryContact = data.companyDetails.primaryContact;
                _this.companyContactInfo.companyDetails.secondaryContact = data.companyDetails.secondaryContact;
                _this.tempFirstMail = data.companyDetails.secondaryContact;
                _this.tempSecondMail = data.companyDetails.secondaryContact;
                // this.swalAlertService.SweetAlertWithoutConfirmation("General Settings", data.message, "success");
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("General Settings", data.message, "error");
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("General Settings", err.error.message, "error");
        });
    };
    /* Get General Settings Ends*/
    /*
    author : vipin reddy
    Description:country change function starts*/
    GeneralSettingsComponent.prototype.comapanyConactCountrty = function () {
    };
    /*country chnage ends*/
    /* author : vipin reddy
    Description: save and update general settings data*/
    GeneralSettingsComponent.prototype.saveCompanySetup = function () {
        var _this = this;
        console.log(this.companyContactInfo);
        this.isValid = true;
        // this.companyContactInfo.companyDetails.primaryContact.name = "test";
        // this.companyContactInfo.companyDetails.secondaryContact.name = "test"
        if (this.companyContactInfo.companyDetails.primaryAddress.address1 &&
            this.companyContactInfo.companyDetails.primaryAddress.city &&
            this.companyContactInfo.companyDetails.primaryAddress.country && this.companyContactInfo.companyDetails.primaryAddress.phone &&
            this.companyContactInfo.companyDetails.primaryAddress.state && this.companyContactInfo.companyDetails.primaryAddress.zipcode &&
            this.companyContactInfo.companyDetails.fein && this.companyContactInfo.companyDetails.name &&
            this.companyContactInfo.companyDetails.primaryContact.email && this.companyContactInfo.companyDetails.primaryContact.extention &&
            this.companyContactInfo.companyDetails.primaryContact.mobilePhone &&
            this.companyContactInfo.companyDetails.primaryContact.phone && this.companyContactInfo.companyDetails.secondaryContact.phone &&
            this.companyContactInfo.companyDetails.secondaryContact.mobilePhone &&
            this.companyContactInfo.companyDetails.secondaryContact.extention && !this.phoneNumeric && !this.mobilePhoneNumeric && !this.extentionNumeric &&
            !this.mobilephoneSecondary && !this.extentionSecondary && !this.phoneSecondary && !this.zipcodeCompanyAddress && !this.phoneCompanyAddress) {
            this.companySetupService.updateGeneralSettings(this.companyContactInfo)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    localStorage.setItem('nextTab', 'in');
                    _this.emialSecondDisable = false;
                    _this.emialFirstDisable = false;
                    // this.messageService.sendMessage('general')
                    _this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/field-maintenance"]);
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    /*save and update generalsetings ends*/
    /* author : vipin reddy
    Description: Email verification on general-settings (primary and secondary)*/
    GeneralSettingsComponent.prototype.sendVerificationEmail = function (email) {
        var _this = this;
        var data = {
            email: email,
            name: this.companyContactInfo.companyDetails.name
        };
        console.log(data);
        this.companySetupService.sendVerificationEmail(data)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Send Verification Link", res.message, "success");
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Send Verification Link", err.message, "error");
        });
    };
    /*Email verification on general-settings (primary and secondary) Ends*/
    GeneralSettingsComponent.prototype.emailStatus2 = function () {
        console.log("111111111123", this.companyContactInfo.companyDetails.secondaryContact.email);
        if (this.tempSecondMail != this.companyContactInfo.companyDetails.secondaryContact.email) {
            this.companyContactInfo.companyDetails.secondaryContact.isEmailVerified = false;
            // this.emialSecondDisable = false;
        }
        if (this.tempSecondMail == this.companyContactInfo.companyDetails.secondaryContact.email) {
            this.companyContactInfo.companyDetails.secondaryContact.isEmailVerified = true;
        }
    };
    GeneralSettingsComponent.prototype.emailStatus = function (event) {
    };
    GeneralSettingsComponent.prototype.cancelReset = function () {
        this.text = "The data entered will be lost,";
        this.note = "Would you like to proceed";
        this.openDialog();
    };
    GeneralSettingsComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_6__["ConfirmationComponent"], {
            width: '550px',
            data: { txt: this.text, note: this.note },
            panelClass: 'confirm-class'
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            if (result == true) {
                _this.getGeneralSettings();
            }
            // if (data == 'delete') {
            //   this.deleteSingleCategory(id)
            // }
        });
    };
    GeneralSettingsComponent.prototype.phoneNumberChange = function (contact, data) {
        console.log(contact, data);
        // console.log(this.companyContactInfo.companyDetails.primaryContact.mobilePhone);
        // console.log(this.companyContactInfo.companyDetails.primaryContact.extention);
        // console.log(this.companyContactInfo.companyDetails.primaryContact.phone);
        // if(contact == 'secondaryContact'){
        //   var str = this.companyContactInfo.companyDetails[contact][data]
        // }
        var str = this.companyContactInfo.companyDetails[contact][data];
        console.log(str);
        if (str.match(/[a-z]/i)) {
            console.log('coming 11');
            // alphabet letters found
            if (data == 'phone' && contact == 'primaryContact') {
                this.phoneNumeric = true;
            }
            if (data == 'extention' && contact == 'primaryContact') {
                this.extentionNumeric = true;
            }
            if (data == 'mobilePhone' && contact == 'primaryContact') {
                this.mobilePhoneNumeric = true;
            }
            if (data == 'mobilePhone' && contact == 'secondaryContact') {
                this.mobilephoneSecondary = true;
            }
            if (data == 'extention' && contact == 'secondaryContact') {
                this.extentionSecondary = true;
            }
            if (data == 'phone' && contact == 'secondaryContact') {
                this.phoneSecondary = true;
            }
            if (data == 'phone' && contact == 'primaryAddress') {
                this.phoneCompanyAddress = true;
            }
            if (data == 'zipcode' && contact == 'primaryAddress') {
                this.zipcodeCompanyAddress = true;
            }
        }
        else {
            console.log('coming 222');
            if (data == 'phone' && contact == 'primaryContact') {
                this.phoneNumeric = false;
            }
            if (data == 'extention' && contact == 'primaryContact') {
                this.extentionNumeric = false;
            }
            if (data == 'mobilePhone' && contact == 'primaryContact') {
                this.mobilePhoneNumeric = false;
            }
            if (data == 'mobilePhone' && contact == 'secondaryContact') {
                this.mobilephoneSecondary = false;
            }
            if (data == 'extention' && contact == 'secondaryContact') {
                this.extentionSecondary = false;
            }
            if (data == 'phone' && contact == 'secondaryContact') {
                this.phoneSecondary = false;
            }
            if (data == 'phone' && contact == 'primaryAddress') {
                this.phoneCompanyAddress = false;
            }
            if (data == 'zipcode' && contact == 'primaryAddress') {
                this.zipcodeCompanyAddress = false;
            }
        }
    };
    GeneralSettingsComponent.prototype.getStandardAndCustomFieldsforStructure = function (cID) {
        var _this = this;
        this.onboardingService.getStructureFields(cID)
            .subscribe(function (res) {
            console.log("structure fields", res);
            _this.allStructureFieldsData = res.data;
            if (_this.allStructureFieldsData['Country'].custom.length > 0) {
                _this.allStructureFieldsData['Country'].custom.forEach(function (element) {
                    _this.countryArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Country'].default.length > 0) {
                _this.allStructureFieldsData['Country'].default.forEach(function (element) {
                    _this.countryArray.push(element);
                });
                console.log("22222country", _this.countryArray);
            }
            if (_this.allStructureFieldsData['State'].custom.length > 0) {
                _this.allStructureFieldsData['State'].custom.forEach(function (element) {
                    _this.stateArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['State'].default.length > 0) {
                _this.allStructureFieldsData['State'].default.forEach(function (element) {
                    _this.stateArray.push(element);
                });
                console.log("22222", _this.stateArray);
            }
        }, function (err) {
            console.log(err);
        });
    };
    GeneralSettingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-general-settings',
            template: __webpack_require__(/*! ./general-settings.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.html"),
            styles: [__webpack_require__(/*! ./general-settings.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/general-settings/general-settings.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__["CompanySettingsService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_5__["MessageService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialog"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_8__["OnboardingService"]])
    ], GeneralSettingsComponent);
    return GeneralSettingsComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".payroll { margin: 30px 0 0;}\n.payroll .panel-default>.panel-heading { padding: 0 0 30px; background-color:transparent !important; border: none;}\n.payroll .panel-title { color: #3c3c3c; display: inline-block; font-size: 18px;}\n.payroll .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.payroll .panel-title>.small, .payroll .panel-title>.small>a, .payroll .panel-title>a, .payroll .panel-title>small, .payroll .panel-title>small>a { text-decoration:none;}\n.payroll .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.payroll .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.payroll .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.payroll .panel-group .panel-heading+.panel-collapse>.list-group, .payroll .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.payroll .panel-heading .accordion-toggle:after { margin:7px 10px 0; font-size: 10px; line-height: 10px;}\n.payroll .panel-body { padding: 0 0 5px;}\n.payroll .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.payroll .panel-default { border-color:transparent;}\n.payroll .panel-group .panel+.panel { margin-top: 20px;}\n.payroll-cont { display: block;}\n.payroll-cont .form-group { position: relative; background:#fff; padding: 0;}\n.payroll-cont .form-control { height: auto; border: none; box-shadow: none; padding: 10px 12px; width:94%; font-size: 15px;}\n.payroll-cont .form-control:focus { box-shadow: none;}\n.payroll-cont span { position: absolute; top:12px; right:10px; cursor: pointer; width: 5%;}\n.payroll-cont span .material-icons { color: #8e8e8e; font-size:20px; font-weight: normal;}\n.payroll-cont .table{ background:#fff;}\n.payroll-cont .table>tbody>tr>th, .payroll-cont .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.payroll-cont .table>tbody>tr>td, .payroll-cont .table>tfoot>tr>td, .payroll-cont .table>tfoot>tr>th, \n.payroll-cont .table>thead>tr>td {color: #484747;padding:20px 15px;font-size:15px; vertical-align: middle;}\n.payroll-cont button.mat-menu-item a { color:#ccc;}\n.payroll-cont button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.payroll-cont .table>tbody>tr>th a, .payroll-cont .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.payroll-cont .table>tbody>tr>th a .fa, .payroll-cont .table>thead>tr>th a .fa { color: #6f6d6d;}\n.payroll-cont button { border: none; outline: none; background:transparent; float: right;}\n.zenwork-customized-checkbox { margin:0 0 0 20px;}\n.panel-group { margin-bottom: 0;}\n.date1 { height: auto !important; line-height:inherit !important;background:#fff; width: 140px;}\n.date1 button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none;\nheight: 30px; min-width: auto;border-radius: 0; float:none; vertical-align:top; margin: 4px 0 0;}\n.date1 span { border: none !important; padding: 0 !important; position: static; width: auto;border: none !important;}\n.date1 .form-control { width:70%; height: auto; padding: 12px 0 12px 10px; border: none; box-shadow: none; display: inline-block;}\n.date1 .form-control:focus { box-shadow: none; border: none;}\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.paygroup { display: block;}\n.paygroup .modal-dialog { width:70%;}\n.paygroup .modal-content {border-radius: 5px; border: none; padding: 0 0 40px;}\n.paygroup .modal-header {padding:30px;}\n.paygroup .modal-body { padding: 0; background: #fff;border-radius: 5px;}\n.paygroup-in {padding:40px 0 0; float: none; margin: 0 auto;}\n.paygroup-in ul {border-bottom:#c3bebe 1px solid; margin: 0 0 30px; padding: 0 0 40px; display: inline-block; width: 100%;}\n.paygroup-in ul li { margin: 0 0 25px; padding: 0;}\n.paygroup-in ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.paygroup-in ul li .form-control{ border: none; box-shadow: none; padding: 13px 12px; height: auto; width:25%; background: #f8f8f8; font-size: 15px;}\n.paygroup-in ul li .form-control:focus{ box-shadow: none;}\n.paygroup-in .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 30px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.paygroup-in .btn.cancel { color:#e44a49; background:transparent; padding:0 0 0 30px;font-size: 16px;}\n.add-calendars.paygroup-in ul li .form-control { width: 90%;}\n.date2 { height: auto !important; line-height:inherit !important; width:70%; background: #f8f8f8;}\n.date2 button { display: inline-block;background:transparent; border: none; outline: none; padding: 0 0 35px; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;vertical-align: top; margin: 4px 0 0;border-left: #bdbdbd 1px solid; border-radius:0;}\n.date2 span {padding: 0;}\n.add-calendars.paygroup-in ul li .date2 .form-control { width:70%; height: auto; padding: 13px 12px; display: inline-block;}\n.add-calendars.paygroup-in ul li .date2 .form-control:focus { box-shadow: none; border: none;}\n.date2 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.add-calendars.paygroup-in ul { border-bottom: none; padding: 0; margin: 0;}\n.paygroup-in a { background: #585858; border-radius: 20px; color: #fff; padding: 10px 30px; margin:20px 0 0;}\n.border-btm {border-bottom:#c3bebe 1px solid; padding:50px 0 0; margin: 0 0 40px;}\n.border-btm1 {border-bottom:#c3bebe 1px solid; padding:30px 0 0; margin: 0 0 30px;}\n.next-cycle { display: block;}\n.next-cycle .modal-dialog { width:80%;}\n.next-cycle .modal-content {border-radius: 5px; border: none; padding:40px 0;}\n.next-cycle .modal-header {padding:30px;}\n.next-cycle .modal-body { padding: 0; background: #fff;border-radius: 5px;}\n.next-cycle-in { text-align: center; float: none;}\n.next-cycle-in img { margin: 0 auto 20px;}\n.next-cycle-in small { display: block; color: #696868; font-size: 18px; padding: 0 0 15px;}\n.next-cycle-in p { color: #444; font-size: 18px;font-weight: bold;}\n.next-cycle-in p em { font-style: normal;}\n.next-cycle-in .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 30px;background: #008f3d; color:#fff; margin: 0 20px 0 0;\ndisplay: inline-block; margin: 30px 0 0;}\n.next-cycle-in .btn.cancel { color:#e44a49; background:transparent; padding:0 30px 0 0;font-size: 16px;display: inline-block;margin: 30px 0 0;}\n.paygroup-in .table{ background:#fff;}\n.paygroup-in .table>tbody>tr>th, .paygroup-in .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.paygroup-in .table>tbody>tr>td, .paygroup-in .table>tfoot>tr>td, .paygroup-in .table>tfoot>tr>th, \n.paygroup-in .table>thead>tr>td {color: #484747;padding:20px 15px;font-size:15px; vertical-align: middle; background:#f3f1f1;}\n.paygroup-in button.mat-menu-item a { color:#ccc;}\n.paygroup-in button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.paygroup-in .table>tbody>tr>th a, .paygroup-in .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.paygroup-in .table>tbody>tr>th a .fa, .paygroup-in .table>thead>tr>th a .fa { color: #6f6d6d;}\n.paygroup-in button { border: none; outline: none; background:transparent; float: right;}\n.salary-grade.paygroup .modal-dialog { width:83%;}\n.paygroup-in .table>tbody>tr>td .form-control { height:auto; padding: 10px 12px; border: none;box-shadow: none;}\n.paygroup-in .table>tbody>tr>td .form-control:focus{ border-color: transparent; box-shadow: none;}\n.salary-grade .paygroup-in ul li .form-control { width:75%;}\n.salarygrade-in.paygroup-in .table>tbody>tr>td, .salarygrade-in.paygroup-in .table>tfoot>tr>td, .salarygrade-in.paygroup-in .table>tfoot>tr>th, \n.salarygrade-in.paygroup-in .table>thead>tr>td { vertical-align:top;}\n.zenwork-customized-checkbox.addsalary-check { vertical-align: top; padding: 6px 0 0; display: block;}\n.error{\n    font-size:14px;\n    color: #e44a49; display: block;\n}\n.preview-calander{\n    cursor: pointer;\n}\n.paycycle{\n    margin: 40px 0 0 0;\n}\n.salarygrade-in ul li{\n    width: 25%;\n    display: inline-block; vertical-align: top;\n}\n.grade-dollar{\n    background: #fff;\n    padding: 0 12px;\n}\n.grade-dollar em{\n    display: inline-block; \n    vertical-align: middle;\n    font-style: normal;\n}\n.grade-dollar .salary-form{\n    display: inline-block;\n    vertical-align: middle;\n    padding: 10px 0px 0 4px;\n    width: 88%;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"payroll\">\n\n  <div class=\"panel-group\" id=\"accordion3\">\n\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseSeven\">\n            Payroll Groups\n          </a>\n        </h4>\n      </div>\n      <div id=\"collapseSeven\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n\n          <div class=\"payroll-cont\">\n\n            <table class=\"table\">\n              <thead>\n                <tr>\n                  <th>\n                    <!-- <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox> -->\n                  </th>\n                  <th> Payroll Group Name</th>\n                  <th>Pay Frequency</th>\n                  <th>Pay Group Code</th>\n                  <th>Status</th>\n                  <th>\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n                      <mat-icon>more_vert</mat-icon>\n                    </button>\n                    <mat-menu #menu=\"matMenu\">\n                      <button *ngIf=\"!deleteIds.length >=1\" mat-menu-item>\n                        <a data-toggle=\"modal\" data-target=\"#myModal\">Add</a>\n                      </button>\n                      <button *ngIf=\"deleteIds.length == 1\" mat-menu-item>\n                        <span data-toggle=\"modal\" (click)=\"editPayrollGroupName()\">Edit</span>\n                        <!-- <a data-toggle=\"modal\" data-target=\"#myModal1\" #openEdit></a> -->\n                      </button>\n                      <!-- <button [disabled]=\"deleteId.length < 1\" mat-menu-item>\n                        <span (click)=\"deleteTemplate()\">Delete</span>\n                      </button> -->\n                      <!-- <button mat-menu-item>\n                        <a href=\"#\">Archive</a>\n                      </button> -->\n                      <button *ngIf=\"!deleteIds.length <=0\" (click)=\"deletePayrollGroupName()\" mat-menu-item>\n                        <a>Delete</a>\n                      </button>\n                    </mat-menu>\n                  </th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let data of payrollGroups\">\n                  <td>\n                    <mat-checkbox class=\"zenwork-customized-checkbox\"\n                      (change)=\"showOptions($event,data._id,'paygroup')\">\n                    </mat-checkbox>\n                  </td>\n                  <td>{{data.payroll_group_name}}</td>\n                  <td>{{data.pay_frequency}}</td>\n                  <td>{{data.pay_group_code}}</td>\n                  <td>{{data.status}}</td>\n                  <td></td>\n\n                </tr>\n\n              </tbody>\n            </table>\n\n            <mat-paginator [length]=\"payrollGroupCount\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n              (page)=\"pageEvents($event)\"></mat-paginator>\n\n          </div>\n\n\n        </div>\n      </div>\n\n    </div>\n\n\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseOne\">\n            Payroll Calendars\n          </a>\n        </h4>\n      </div>\n      <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n\n          <div class=\"payroll-cont\">\n\n            <table class=\"table\">\n              <thead>\n                <tr>\n                  <th>\n                    <!-- <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox> -->\n                  </th>\n                  <th>Calendar Name</th>\n                  <th>Number of Cycles</th>\n                  <th>Active Cycle</th>\n                  <th>Current Cycle Begin Date</th>\n                  <th>Current Cycle End Date</th>\n                  <th>\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu1\" aria-label=\"Example icon-button with a menu\">\n                      <mat-icon>more_vert</mat-icon>\n                    </button>\n                    <mat-menu #menu1=\"matMenu\">\n\n                      <button *ngIf=\"!calanderdeleteIds.length <= 0\" (click)=\"startNextCycle()\" mat-menu-item>\n                        <a data-toggle=\"modal\"> <small><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></small> Start Next\n                          Cycle</a>\n                      </button>\n\n                      <button *ngIf=\"!calanderdeleteIds.length <= 0\" (click)=\"reopenPreviousCycle()\" mat-menu-item>\n                        <a data-toggle=\"modal\"> <small><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></small> Reopen\n                          Previous Cycle</a>\n                      </button>\n\n                      <button *ngIf=\"!calanderdeleteIds.length >= 1\" mat-menu-item style=\"border-top:#ccc 1px solid;\">\n                        <a data-toggle=\"modal\" (click)=\"payrollCalenderOpen()\" data-target=\"#myModal3\">Add</a>\n                      </button>\n\n\n                      <button *ngIf=\"!calanderdeleteIds.length <= 0\" mat-menu-item>\n                        <a (click)=\"payrollCalenderEdit()\">Edit</a>\n\n                      </button>\n                      <button *ngIf=\"!calanderdeleteIds.length <= 0\" mat-menu-item>\n                        <a (click)=\"payrollCalenderCopy()\">copy</a>\n\n                      </button>\n                      <!-- <button mat-menu-item>\n                        <a href=\"#\">Archive</a>\n                      </button> -->\n                      <button *ngIf=\"!calanderdeleteIds.length <= 0\" (click)=\"deletePayrollCalander()\" mat-menu-item>\n                        <a>Delete</a>\n                      </button>\n                    </mat-menu>\n                  </th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let data of payrollCalanderList\">\n                  <td>\n                    <mat-checkbox [(ngModel)]=\"data.isChecked\" class=\"zenwork-customized-checkbox\"\n                      (change)=\"showOptions($event,data._id,'calander')\"></mat-checkbox>\n                  </td>\n                  <td>{{data.calendar_name}}</td>\n                  <td>{{data.number_of_cycles}}</td>\n                  <td *ngIf=\"data.active_cycle\">{{data.active_cycle.pay_cycle_name}}</td>\n                  <td *ngIf=\"!data.active_cycle\">--</td>\n\n                  <td *ngIf=\"data.active_cycle\">\n                    {{data.active_cycle.pay_cycle_start_date | date}}\n                  </td>\n                  <td *ngIf=\"!data.active_cycle\">\n                    --\n                  </td>\n                  <td *ngIf=\"data.active_cycle\">\n                    {{data.active_cycle.pay_cycle_end_date | date}}\n                  </td>\n                  <td *ngIf=\"!data.active_cycle\">\n                    --\n                  </td>\n                  <td></td>\n                </tr>\n\n              </tbody>\n            </table>\n\n            <mat-paginator [length]=\"payrollCalanderCount\" (page)=\"pageEventsCalander($event)\" [pageSize]=\"5\"\n              [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\n\n          </div>\n\n\n        </div>\n      </div>\n\n    </div>\n\n\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseTwo\">\n            Salary Grades\n          </a>\n        </h4>\n      </div>\n      <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n\n          <div class=\"payroll-cont\">\n\n            <table class=\"table\">\n              <thead>\n                <tr>\n                  <th>\n                    <!-- <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox> -->\n                  </th>\n                  <th> Salary Grade Name</th>\n                  <th></th>\n                  <th></th>\n                  <th></th>\n                  <th>\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu2\" aria-label=\"Example icon-button with a menu\">\n                      <mat-icon>more_vert</mat-icon>\n                    </button>\n                    <mat-menu #menu2=\"matMenu\">\n                      <button *ngIf=\"salaryGradedeleteIds.length == 0\" mat-menu-item>\n                        <a data-toggle=\"modal\" data-target=\"#myModal6\" (click)=\"openFormCalender()\">Add</a>\n                      </button>\n\n                      <button *ngIf=\"salaryGradedeleteIds.length == 1\" mat-menu-item>\n                        <a (click)=\"editSalryGradeName()\">Edit</a>\n\n                      </button>\n                      <!-- <button mat-menu-item>\n                        <a href=\"#\">Archive</a>\n                      </button> -->\n                      <button *ngIf=\"salaryGradedeleteIds.length >= 1\" (click)=\"deleteSAlaryGrades()\" mat-menu-item>\n                        <a>Delete</a>\n                      </button>\n                    </mat-menu>\n                  </th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let data of salaryGradeList\">\n                  <td>\n                    <mat-checkbox class=\"zenwork-customized-checkbox\"  [(ngModel)]=\"data.isChecked\"\n                      (change)=\"showOptions($event,data._id,'salaryGrade')\"></mat-checkbox>\n                  </td>\n                  <td>{{data.salary_grade_name}}</td>\n                  <td></td>\n                  <td></td>\n                  <td></td>\n                  <td></td>\n                </tr>\n\n              </tbody>\n            </table>\n\n            <mat-paginator [length]=\"salaryGradeCount\" (page)=\"pageEventsSalaryGrade($event)\" [pageSize]=\"5\"\n              [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\n\n          </div>\n\n\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n\n\n<!-- Payroll Groups -->\n<div class=\"paygroup\">\n\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Add Payroll Groups</h4>\n        </div>\n\n        <div class=\"modal-body\">\n\n          <div class=\"paygroup-in col-md-10\">\n\n\n            <ul>\n\n              <li>\n                <label>Payroll Group Name *</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"Payroll Group Name\" [(ngModel)]=\"payrollGroup.name\"\n                  (ngModelChange)=\"payrollGroupName()\" maxlength=\"32\" name=\"name\" #name=\"ngModel\" required>\n                <p *ngIf=\"!payrollGroup.name && name.touched || (!payrollGroup.name && isValid)\" class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n              <li>\n                <label>Pay Frequency *</label>\n\n                <mat-select class=\"form-control\" placeholder=\"Frequency\" [(ngModel)]=\"payrollGroup.frequency\"\n                  name=\"frequency\" #frequency=\"ngModel\" required>\n                  <mat-option value='Weekly'>Weekly</mat-option>\n                  <mat-option value='Bi-Weekly'>Bi-Weekly</mat-option>\n                  <mat-option value='Semi-Monthly'>Semi-Monthly</mat-option>\n                  <mat-option value='Monthly'>Monthly</mat-option>\n\n                </mat-select>\n                <p *ngIf=\"!payrollGroup.frequency && frequency.touched  && isValid || (!payrollGroup.frequency && isValid)\"\n                  class=\"error\">Please fill this field</p>\n              </li>\n\n              <li>\n                <label>Pay Group Code *</label>\n                <input type=\"text\" maxlength=\"8\" class=\"form-control\" placeholder=\"Pay Group Code\"\n                  [(ngModel)]=\"payrollGroup.code\" (keypress)=\"keyPress1($event)\" (ngModelChange)=\"payGroupCode()\"\n                  name=\"code\" #code=\"ngModel\" required>\n                <p *ngIf=\"!payrollGroup.code && code.touched  && isValid || (!payrollGroup.code && isValid)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n            </ul>\n\n            <button class=\"btn cancel pull-left\" data-dismiss=\"modal\">Cancel</button>\n            <button class=\"btn save pull-right\" (click)=\"payrollGroupAdd()\">Submit</button>\n            <div class=\"clearfix\"></div>\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n\n\n<!-- Edit Payroll Groups -->\n<div class=\"paygroup\">\n\n  <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Edit Payroll Groups</h4>\n        </div>\n\n        <div class=\"modal-body\">\n\n          <div class=\"paygroup-in col-md-10\">\n\n\n            <ul>\n\n              <li>\n                <label>Payroll Group Name *</label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"SinglePayrollGroup.payroll_group_name\"\n                  placeholder=\"Payroll Group Name\" maxlength=\"32\" name=\"payroll_group_name\"\n                  #payroll_group_name=\"ngModel\" required>\n                <p *ngIf=\"!SinglePayrollGroup.payroll_group_name && payroll_group_name.touched || (!SinglePayrollGroup.payroll_group_name && isValid)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n              <li>\n                <label>Pay Frequency *</label>\n\n                <mat-select class=\"form-control\" [(ngModel)]=\"SinglePayrollGroup.pay_frequency\" placeholder=\"Frequency\"\n                  name=\"pay_frequency\" #pay_frequency=\"ngModel\" required>\n                  <mat-option value='Weekly'>Weekly</mat-option>\n                  <mat-option value='Bi-Weekly'>Bi-Weekly</mat-option>\n                  <mat-option value='Semi-Monthly'>Semi-Monthly</mat-option>\n                  <mat-option value='Monthly'>Monthly</mat-option>\n                </mat-select>\n                <p *ngIf=\"!SinglePayrollGroup.pay_frequency && pay_frequency.touched || (!SinglePayrollGroup.pay_frequency && isValid)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n              <li>\n                <label>Pay Group Code *</label>\n                <input type=\"text\" class=\"form-control\" maxlength=\"8\" [(ngModel)]=\"SinglePayrollGroup.pay_group_code\"\n                  placeholder=\"Pay Group Code\" name=\"pay_group_code\" #pay_group_code=\"ngModel\"\n                  (keypress)=\"keyPress1($event)\" required>\n                <p *ngIf=\"!SinglePayrollGroup.pay_group_code && pay_group_code.touched || (!SinglePayrollGroup.pay_group_code && isValid)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n              <li>\n                <label>Status *</label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"SinglePayrollGroup.status\" placeholder=\"Status\"\n                  name=\"status\" #status=\"ngModel\" required>\n                  <mat-option value='Active'>Active</mat-option>\n                  <mat-option value='Inactive'>Inactive</mat-option>\n                </mat-select>\n                <p *ngIf=\"!SinglePayrollGroup.status && status.touched || (!SinglePayrollGroup.status  && isValid)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n            </ul>\n\n\n            <!-- <ul>\n\n              <li>\n                <label>Assign Payroll Calendar?</label>\n                <mat-select class=\"form-control\" placeholder=\"Payroll Calendar\">\n                  <mat-option [value]='true'>Yes</mat-option>\n                  <mat-option [value]='false'>No</mat-option>\n                </mat-select>\n              </li>\n\n              <li>\n                <label>Payroll Calendar?</label>\n                <mat-select class=\"form-control\" placeholder=\"Payroll Calendar\">\n                  <mat-option [value]='true'>Yes</mat-option>\n                  <mat-option [value]='false'>No</mat-option>\n                </mat-select>\n              </li>\n\n\n            </ul> -->\n\n            <button class=\"btn cancel pull-left\" data-dismiss=\"modal\">Cancel</button>\n            <button class=\"btn save pull-right\" (click)=\"updatePayrollGroup()\">Submit</button>\n            <div class=\"clearfix\"></div>\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n\n\n\n<!-- Payroll Calendars -->\n<div class=\"paygroup\">\n\n  <div class=\"modal fade\" id=\"myModal3\" role=\"dialog\">\n    <!-- <form #f=\"ngForm\"> -->\n\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Add / Edit Payroll Calendars</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"paygroup-in col-md-11 add-calendars\">\n\n            <ul>\n\n              <li class=\"col-md-3\">\n                <label>Calendar Name *</label>\n                <input type=\"text\" [(ngModel)]=\"payrollCalendar.calendar_name\" class=\"form-control\"\n                  placeholder=\"Calendar Name\" maxlength=\"26\" name=\"calendar_name\" #calendar_name=\"ngModel\" required>\n                <p *ngIf=\"!payrollCalendar.calendar_name && calendar_name.touched  && isValidCalander || (!payrollCalendar.calendar_name  && isValidCalander)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n\n\n              <li class=\"col-md-3\">\n                <label>Pay Group Assignment *</label>\n\n                <mat-select class=\"form-control\" [(ngModel)]=\"payrollCalendar.assigned_payroll_group\"\n                  placeholder=\"Group Assignment\" (ngModelChange)=\"payGroupAssign()\" name=\"assigned_payroll_group\"\n                  #assigned_payroll_group=\"ngModel\">\n                  <mat-option value=''>-- Select value --\n                  </mat-option>\n                  <mat-option *ngFor=\"let data of payrollGroupsDropdown\" [value]='data._id'>{{data.payroll_group_name}}\n                  </mat-option>\n                  <!-- <mat-option value=''>No</mat-option> -->\n                </mat-select>\n                <p *ngIf=\"!payrollCalendar.assigned_payroll_group && assigned_payroll_group.touched  && isValidCalander || (!payrollCalendar.assigned_payroll_group  && isValidCalander)\"\n                  class=\"error\">Please\n                  fill this field</p>\n\n              </li>\n\n            </ul>\n\n            <ul>\n\n\n\n\n              <li class=\"col-md-3\">\n                <label>Number of Days in a Cycle *</label>\n\n                <input type=\"text\" [(ngModel)]=\"payrollCalendar.number_of_days_in_a_cycle\" (keypress)=\"keyPress($event)\"\n                  class=\"form-control\" placeholder=\"Number of Days\" maxlength=\"3\" name=\"number_of_days_in_a_cycle\"\n                  #number_of_days_in_a_cycle=\"ngModel\" required readonly>\n                <p *ngIf=\"!payrollCalendar.number_of_days_in_a_cycle && number_of_days_in_a_cycle.touched && isValidCalander || (!payrollCalendar.number_of_days_in_a_cycle  && isValidCalander)\"\n                  class=\"error\">Please\n                  fill this field</p>\n              </li>\n\n              <li class=\"col-md-3\">\n                <label>Number of Cycles *</label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"payrollCalendar.number_of_cycles\"\n                  (keypress)=\"keyPress($event)\" (ngModelChange)=\"numberOfCycle()\" maxlength=\"3\"\n                  placeholder=\"Number of Cycles\" name=\"number_of_cycles\" #number_of_cycles=\"ngModel\" required>\n                <p *ngIf=\"!payrollCalendar.number_of_cycles && number_of_cycles.touched && isValidCalander || (!payrollCalendar.number_of_cycles  && isValidCalander)\"\n                  class=\"error\">Please\n                  fill this field</p>\n                <!-- <p class=\"error\" *ngIf=\"showWeeksError\">Please enter a value bellow 54</p> -->\n              </li>\n\n            </ul>\n\n            <ul>\n\n              <li class=\"col-md-3\">\n                <label>First Cycle Start Date *</label>\n\n                <div class=\"date2\">\n                  <input matInput [matDatepicker]=\"picker4\" [(ngModel)]=\"payrollCalendar.first_cycle_start_date\"\n                    placeholder=\"Start Date\" (ngModelChange)=\"stattsDateChange()\" \n                    name=\"first_cycle_start_date\" #first_cycle_start_date=\"ngModel\" required class=\"form-control\">\n                  <mat-datepicker #picker4></mat-datepicker>\n                  <button mat-raised-button (click)=\"picker4.open()\">\n                    <span>\n                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                    </span>\n                  </button>\n                  <div class=\"clearfix\"></div>\n                </div>\n                <p *ngIf=\"!payrollCalendar.first_cycle_start_date && first_cycle_start_date.touched && isValidCalander || (!payrollCalendar.first_cycle_start_date && isValidCalander)\"\n                  class=\"error\">Please\n                  fill this field</p>\n\n              </li>\n\n\n              <li class=\"col-md-3\">\n                <label>First Cycle End Date</label>\n\n                <div class=\"date2\">\n                  <input matInput [matDatepicker]=\"picker5\" [(ngModel)]=\"payrollCalendar.first_cycle_end_date\"\n                    placeholder=\"End Date\" class=\"form-control\" name=\"first_cycle_end_date\"\n                    #first_cycle_end_date=\"ngModel\" required disabled>\n                  <mat-datepicker #picker5></mat-datepicker>\n                  <button mat-raised-button (click)=\"picker5.open()\">\n                    <!-- <span>\n                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                    </span> -->\n                  </button>\n                  <div class=\"clearfix\"></div>\n                </div>\n\n              </li>\n\n\n            </ul>\n\n            <ul>\n              <li class=\"col-md-3\">\n                <label>Calendar Year *</label>\n\n\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"payrollCalendar.calendar_year\"\n                  (keypress)=\"keyPress($event)\" placeholder=\"Calendar Year\" name=\"calendar_year\"\n                  #calendar_year=\"ngModel\" required readonly>\n\n                <p *ngIf=\"!payrollCalendar.calendar_year && calendar_year.touched  && isValidCalander || (!payrollCalendar.calendar_year  && isValidCalander)\"\n                  class=\"error\">Please\n                  fill this field</p>\n\n              </li>\n\n            </ul>\n\n\n            <a class=\"preview-calander\" (click)=\"previewCalander()\">Preview the Calendar</a>\n\n            <div class=\"clearfix\"></div>\n\n            <div class=\"payroll-cont paycycle\">\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>\n                      S.No\n                    </th>\n                    <th>\n                      Paycycle Name\n                    </th>\n                    <th>\n                      Paycycle Start-date\n                    </th>\n                    <th>\n                      Paycycle End-date\n                    </th>\n                  </tr>\n                </thead>\n\n                <tbody>\n                  <tr *ngFor=\"let data of payrollCalanderCycle\">\n                    <td>\n                      {{data.pay_cycle_number}}\n                    </td>\n                    <td>\n                      <input type=\"text\" [(ngModel)]=\"data.pay_cycle_name\" class=\"form-control\" readonly>\n                    </td>\n                    <td>\n\n                      <div>\n                        <div class=\"date2\">\n                          <input matInput [matDatepicker]=\"picker101\" [(ngModel)]=\"data.pay_cycle_start_date\"\n                            placeholder=\"End Date\" class=\"form-control\" style=\"width:70%;display: inline-block;\">\n                          <mat-datepicker #picker101></mat-datepicker>\n                          <button mat-raised-button (click)=\"picker101.open()\"\n                            style=\"margin:0;line-height: inherit;float: none;\">\n                            <span>\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                            </span>\n                          </button>\n                          <div class=\"clearfix\"></div>\n                        </div>\n                      </div>\n                    </td>\n                    <td>\n\n                      <div class=\"date2\">\n                        <input matInput [matDatepicker]=\"picker102\" [(ngModel)]=\"data.pay_cycle_end_date\"\n                          placeholder=\"End Date\" class=\"form-control\" style=\"width:70%;display: inline-block;\">\n                        <mat-datepicker #picker102></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker102.open()\"\n                          style=\"margin:0;line-height: inherit;float: none;\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </td>\n                  </tr>\n\n                </tbody>\n              </table>\n            </div>\n\n            <div class=\"border-btm\"></div>\n            <button class=\"btn cancel pull-left\" data-dismiss=\"modal\">Cancel</button>\n            <button *ngIf=\"!payrollCalenderUpdate\" class=\"btn save pull-right\"\n              (click)=\"singlePayrollCalander()\">Submit</button>\n            <button *ngIf=\"payrollCalenderUpdate\" class=\"btn save pull-right\"\n              (click)=\"singlePayrollCalanderUpdate()\">update</button>\n\n            <div class=\"clearfix\"></div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n    <!-- </form> -->\n\n  </div>\n</div>\n\n\n<!-- Next Cycle -->\n<div class=\"next-cycle\">\n\n  <div class=\"modal fade\" id=\"myModal4\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-body\">\n\n          <div class=\"next-cycle-in col-md-11\">\n\n            <img src=\"../../../../../assets/images/Manager Self Service/error.png\" alt=\"img\">\n\n            <small>Please confirm that you want to finish the current cycle and start the next cycle.</small>\n            <p *ngIf=\"!currentCylcle\">Current Cycle: <span> cycle number:{{currentPayCycleDates.pay_cycle_number}}\n              </span> <em>Cycle Begin Date{{currentPayCycleDates.pay_cycle_start_date | date}} - Cycle End\n                Date{{currentPayCycleDates.pay_cycle_end_date | date}}</em></p>\n\n            <p *ngIf=\"!nextCylcle\">Next Cycle: <span> cycle number:{{nextPayCycleDates.pay_cycle_number}} </span>\n              <em>Cycle Begin Date {{nextPayCycleDates.pay_cycle_start_date | date}} - Cycle End\n                Date {{nextPayCycleDates.pay_cycle_end_date | date}}</em> </p>\n\n\n            <button class=\"btn cancel\" data-dismiss=\"modal\">Cancel</button>\n            <button class=\"btn save\" (click)=\"cycleDataSend()\">Submit</button>\n            <div class=\"clearfix\"></div>\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n\n<!-- Previous Cycle -->\n<div class=\"next-cycle\">\n\n  <div class=\"modal fade\" id=\"myModal5\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-body\">\n\n          <div class=\"next-cycle-in col-md-11\">\n\n            <img src=\"../../../../../assets/images/Manager Self Service/error.png\" alt=\"img\">\n\n            <small>Please confirm that you want to reopen the previous cycle and abort the current cycle.</small>\n\n            <p *ngIf=\"!currentCylcle1\">Previous Cycle: <span> cycle number:{{currentPayCycleDates1.pay_cycle_number}}\n              </span> <em>Cycle Begin Date{{currentPayCycleDates1.pay_cycle_start_date | date}} - Cycle End\n                Date{{currentPayCycleDates1.pay_cycle_end_date | date}}></em> </p>\n\n            <p *ngIf=\"!nextCylcle1\">Current Cycle: <span> cycle number: </span>\n              <em>{{nextPayCycleDates1.pay_cycle_start_date | date}} - Cycle End\n                Date{{nextPayCycleDates1.pay_cycle_end_date | date}}</em> </p>\n\n\n            <button class=\"btn cancel\" data-dismiss=\"modal\">Cancel</button>\n            <button class=\"btn save\" (click)=\"reopenCycleSubmit()\">Submit</button>\n            <div class=\"clearfix\"></div>\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n\n\n<!-- Salary grade Brand -->\n<div class=\"paygroup salary-grade\">\n\n  <div class=\"modal fade\" id=\"myModal6\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Add Salary Grade Bands</h4>\n        </div>\n\n        <div class=\"modal-body\">\n\n          <div class=\"paygroup-in col-md-11 salarygrade-in\">\n\n            <ul style=\"margin:0;\">\n              <li>\n                <label>Salary Grade Name *</label>\n                <input type=\"text\" [(ngModel)]=\"salGrade.name\" class=\"form-control\" name=\"name1\" #name=\"ngModel\"\n                  required placeholder=\"Salary Grade Name\">\n                <p *ngIf=\"!salGrade.name && name.touched && isValidSal || (!salGrade.name && isValidSal)\" class=\"error\">\n                 Enter grade name\n                </p>\n              </li>\n              <!-- <li>\n                <label>Job Title</label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"SinglePayrollGroup.status\" placeholder=\"Status\"\n                  name=\"status\" #status=\"ngModel\" required>\n               \n                </mat-select>\n\n              </li> -->\n              <li>\n                <label>Salary Grade Pay Frequency *</label>\n\n                <mat-select class=\"form-control\" placeholder=\"Frequency\" [(ngModel)]=\"payrollGroup.frequency\"\n                  name=\"frequency\" #frequency=\"ngModel\" required>\n                  <mat-option value='Weekly'>Weekly</mat-option>\n                  <mat-option value='Bi-Weekly'>Bi-Weekly</mat-option>\n                  <mat-option value='Semi-Monthly'>Semi-Monthly</mat-option>\n                  <mat-option value='Monthly'>Monthly</mat-option>\n                  <mat-option value='Annual'>Annual</mat-option>\n\n                </mat-select>\n                <p *ngIf=\"!payrollGroup.frequency && frequency.touched && isValidSal || (!payrollGroup.frequency && isValidSal)\"\n                  class=\"error\">\n                  Enter frequency\n                </p>\n\n              </li>\n            </ul>\n<!-- {{salGradeGradeBands.length}} -->\n            <table class=\"table\">\n              <thead>\n                <tr>\n                  <th>\n                   \n                  </th>\n                  <th>Grade</th>\n                  <th>Min</th>\n                  <th>Mid</th>\n                  <th>Max</th>\n                  <th>Average Salary</th>\n                  <th>Average Market value</th>\n                  <th>\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu5\" aria-label=\"Example icon-button with a menu\">\n                      <mat-icon>more_vert</mat-icon>\n                    </button>\n                    <mat-menu #menu5=\"matMenu\">\n                      <button *ngIf=\"salGradeGradeBands.length== 0\" (click)=\"addSalaryGrade()\" mat-menu-item>\n                        <a>Add</a>\n                      </button>\n\n                      <button *ngIf=\"salGradeGradeBands.length >=1\" (click)=\"deleteSalGradeBands()\" mat-menu-item>\n                        <a>Delete</a>\n                      </button>\n                    </mat-menu>\n                  </th>\n                </tr>\n              </thead>\n\n              <tbody>\n                <tr *ngFor=\"let data of salryGrades;let i = index\">\n                  <td [ngClass]=\"{'zenwork-checked-border':data.isChecked}\">\n                    <mat-checkbox [(ngModel)]=\"data.isChecked\" (ngModelChange)=\"selectedSalGrade(i,$event,data)\"\n                      class=\"zenwork-customized-checkbox addsalary-check\"></mat-checkbox>\n                  </td>\n                  <td>\n                    <input type=\"text\" [(ngModel)]=\"data.grade\" name=\"grade\" #grade=\"ngModel\" required\n                      class=\"form-control\">\n                    <span *ngIf=\"!data.grade && grade.touched && isValidSal || (!data.grade && isValidSal)\"\n                      class=\"error\">Enter grade\n                    </span>\n\n                  </td>\n                  <td>\n                    <div class=\"grade-dollar\">\n                      <em>$</em>\n                      <input type=\"text\" [(ngModel)]=\"data.min\" (ngModelChange)=\"minValue(i,$event)\"\n                        (keypress)=\"keyPress($event)\" class=\"form-control salary-form\" name=\"min\" #min=\"ngModel\"\n                        required>\n                    </div>\n                    <span *ngIf=\"!data.min && min.touched && isValidSal || (!data.min && isValidSal)\"\n                      class=\"error\">Enter min value\n                    </span>\n\n                    <span *ngIf=\"data.minSalValueError\" class=\"error\">Enter less than\n                      mid value\n                    </span>\n\n                  </td>\n\n                  <td>\n                    <div class=\"grade-dollar\">\n                      <em>$</em>\n                      <input type=\"text\" [(ngModel)]=\"data.mid\" (ngModelChange)=\"midValue(i,$event)\" name=\"mid\"\n                        #mid=\"ngModel\" (keypress)=\"keyPress($event)\" required class=\"form-control salary-form\">\n                    </div>\n                    <span *ngIf=\"!data.mid && mid.touched && isValidSal|| (!data.mid && isValidSal)\"\n                      class=\"error\">Enter min value\n                    </span>\n                    <span *ngIf=\"data.midSalValueError\" class=\"error\">Enter greater than\n                      min value\n                    </span>\n                  </td>\n                  <td>\n                    <div class=\"grade-dollar\">\n                      <em>$</em>\n                      <input type=\"text\" [(ngModel)]=\"data.max\" (ngModelChange)=\"maxValue(i,$event)\"\n                        (keypress)=\"keyPress($event)\" name=\"max\" #max=\"ngModel\" required\n                        class=\"form-control salary-form\">\n                    </div>\n                    <span *ngIf=\"!data.max && max.touched || (!data.max && isValidSal)\" class=\"error\">Enter max value\n                    </span>\n                    <span *ngIf=\"data.maxSalValueError\" class=\"error\">Enter greater than\n                      mid&max value\n                    </span>\n                  </td>\n                  <td>\n                    <div class=\"grade-dollar\">\n                      <em>$</em>\n                      <input type=\"text\" [(ngModel)]=\"data.average_salary\" (keypress)=\"keyPress($event)\"\n                        class=\"form-control salary-form\">\n                    </div>\n                  </td>\n\n                  <td>\n                    <div class=\"grade-dollar\">\n                      <em>$</em>\n                      <input type=\"text\" [(ngModel)]=\"data.average_market_value\" (keypress)=\"keyPress($event)\"\n                        class=\"form-control salary-form\">\n                    </div>\n                  </td>\n                  <td> </td>\n\n                </tr>\n\n              </tbody>\n            </table>\n\n            <mat-paginator [length]=\"salaryGradeBands\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n              (page)=\"salaryGradepageEvents($event)\"></mat-paginator>\n\n\n            <div class=\"border-btm1\"></div>\n            <button class=\"btn cancel pull-left\" (click)=\"salaryGradeCancel()\">Cancel</button>\n            <button *ngIf=\"!updateSalGrade\" class=\"btn save pull-right\" (click)=\"salaryGradeSubmit()\" [disabled]=\"addSaralySubmit\" >Submit</button>\n            <button *ngIf=\"updateSalGrade\" class=\"btn save pull-right\" (click)=\"salaryGradeUpdate()\" >Update</button>\n\n            <div class=\"clearfix\"></div>\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: PayrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollComponent", function() { return PayrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/pay-roll.service */ "./src/app/services/pay-roll.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shared-module/confirmation/confirmation.component */ "./src/app/shared-module/confirmation/confirmation.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PayrollComponent = /** @class */ (function () {
    function PayrollComponent(payRollService, swalAlertService, onboardingService, dialog) {
        this.payRollService = payRollService;
        this.swalAlertService = swalAlertService;
        this.onboardingService = onboardingService;
        this.dialog = dialog;
        this.addSaralySubmit = true;
        this.payrollGroup = {
            name: '',
            frequency: '',
            code: ''
        };
        this.isValid = false;
        this.deleteIds = [];
        this.payFrequency = [];
        this.payrollGroups = [];
        this.salaryGradedeleteIds = [];
        this.pageEvents1 = {
            perpage: 5,
            pageno: 1,
            salaryGradePerpage: 5,
            salaryGradePageNo: 1,
            calanderPerPage: 5,
            calanderPageNo: 1,
            perpageSalaryGradeBand: 10,
            pagenoSalaryGradeBand: 1
        };
        this.SinglePayrollGroup = {
            pay_frequency: '',
            pay_group_code: "",
            payroll_group_name: "",
            status: ""
        };
        this.salryGrades = [];
        this.chooseSalGrades = [];
        this.salGrade = {
            name: ''
        };
        this.isValidSal = false;
        this.salaryGradeList = [];
        this.deleteSalGradeBand = [];
        this.SingleSalaryGrade = [
            {
                grades: []
            }
        ];
        this.updateSalGrade = false;
        this.salGradeGradeBands = [];
        this.payrollCalendar = {
            number_of_cycles: "",
            calendar_name: "",
            number_of_days_in_a_cycle: "",
            first_cycle_start_date: "",
            first_cycle_end_date: "",
            assigned_payroll_group: '',
            calendar_year: ''
        };
        this.payrollCalanderCycle = [];
        this.isValidCalander = false;
        this.showWeeksError = false;
        this.calanderdeleteIds = [];
        this.payrollCalenderUpdate = false;
        this.currentPayCycleDates = {
            pay_cycle_number: '',
            pay_cycle_start_date: '',
            pay_cycle_end_date: ''
        };
        this.currentCylcle = false;
        this.nextCylcle = false;
        this.nextPayCycleDates = {
            pay_cycle_number: '',
            pay_cycle_start_date: '',
            pay_cycle_end_date: ''
        };
        this.currentCylcle1 = false;
        this.currentPayCycleDates1 = {
            pay_cycle_number: '',
            pay_cycle_start_date: '',
            pay_cycle_end_date: ''
        };
        this.nextPayCycleDates1 = {
            pay_cycle_number: '',
            pay_cycle_start_date: '',
            pay_cycle_end_date: ''
        };
        this.nextCylcle1 = false;
        this.midSalValueError = false;
        this.gradeBandSubmit = false;
    }
    PayrollComponent.prototype.ngOnInit = function () {
        this.companyId = JSON.parse(localStorage.getItem('companyId'));
        this.getPayrollGroup();
        // this.getpayFrequency()
        this.getsalrayGrades();
        this.getPayrollCalandereList();
        this.getStructureJobTitleFields();
    };
    /*Description: start Date change
  author : vipin reddy */
    PayrollComponent.prototype.startDatechange = function () {
        console.log(this.startDate.toISOString(), "startDate");
    };
    PayrollComponent.prototype.getStructureJobTitleFields = function () {
        // this.onboardingService.getSingleStructureFields()
        // .subscribe((res: any) => {
        //   console.log(res);
        // },
        // (err)=>{
        // })
    };
    /*Description: get payrollgroup list
  author : vipin reddy */
    PayrollComponent.prototype.getPayrollGroup = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            per_page: this.pageEvents1.perpage,
            page_no: this.pageEvents1.pageno
        };
        this.payRollService.getPayrollGroup(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.payrollGroups = res.data;
                _this.payrollGroupCount = res.total_count;
            }
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.updatePayrollGroup = function () {
        var _this = this;
        this.SinglePayrollGroup['companyId'] = this.companyId;
        this.SinglePayrollGroup['_id'] = this.deleteIds[0];
        console.log(this.SinglePayrollGroup, 'update data');
        if (this.SinglePayrollGroup.pay_frequency && this.SinglePayrollGroup.pay_group_code && this.SinglePayrollGroup.payroll_group_name &&
            this.SinglePayrollGroup.status) {
            this.SinglePayrollGroup.pay_group_code = this.SinglePayrollGroup.pay_group_code.trim();
            this.SinglePayrollGroup.payroll_group_name = this.SinglePayrollGroup.payroll_group_name.trim();
            this.payRollService.updatePayrollGroup(this.SinglePayrollGroup)
                .subscribe(function (res) {
                console.log(res);
                if (res.status = true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation('PayrollGroup', res.message, 'success');
                    jQuery("#myModal1").modal("hide");
                    _this.deleteIds = [];
                    _this.getPayrollGroup();
                }
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation('PayrollGroup', err.error.message, 'error');
            });
        }
    };
    PayrollComponent.prototype.showOptions = function ($event, id, data) {
        if (data == 'paygroup') {
            console.log("1222222", $event.checked, id);
            this.payrollGroupId = id;
            if ($event.checked == true) {
                this.deleteIds.push(id);
            }
            console.log(this.deleteIds, "123321");
            if ($event.checked == false) {
                var index = this.deleteIds.indexOf(id);
                if (index > -1) {
                    this.deleteIds.splice(index, 1);
                }
                console.log(this.deleteIds);
            }
        }
        if (data == 'salaryGrade') {
            console.log("1222222", $event.checked, id);
            this.payrollGroupId = id;
            if ($event.checked == true) {
                if (this.salaryGradedeleteIds) {
                    this.salaryGradedeleteIds = [id];
                    this.salaryGradeList.forEach(function (element) {
                        if (id == element._id) {
                            element.isChecked = true;
                        }
                        else {
                            element.isChecked = false;
                        }
                    });
                }
                else {
                    this.salaryGradedeleteIds.push(id);
                }
                // this.salaryGradedeleteIds.push(id)
            }
            console.log(this.salaryGradedeleteIds, "123321");
            if ($event.checked == false) {
                var index = this.salaryGradedeleteIds.indexOf(id);
                if (index > -1) {
                    this.salaryGradedeleteIds.splice(index, 1);
                }
                console.log(this.salaryGradedeleteIds);
            }
            if (this.salaryGradedeleteIds.length == 0) {
                this.updateSalGrade = false;
            }
        }
        if (data == 'calander') {
            console.log("1222222", $event.checked, id);
            this.calanderGroupId = id;
            if ($event.checked == true) {
                if (this.calanderdeleteIds) {
                    this.calanderdeleteIds = [id];
                    this.payrollCalanderList.forEach(function (element) {
                        if (id == element._id) {
                            element.isChecked = true;
                        }
                        else {
                            element.isChecked = false;
                        }
                    });
                }
                else {
                    this.calanderdeleteIds.push(id);
                }
            }
            console.log(this.calanderdeleteIds, "123321");
            if ($event.checked == false) {
                var index = this.calanderdeleteIds.indexOf(id);
                if (index > -1) {
                    this.calanderdeleteIds.splice(index, 1);
                }
                console.log(this.calanderdeleteIds);
            }
        }
    };
    PayrollComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageEvents1.pageno = event.pageIndex + 1;
        this.pageEvents1.perpage = event.pageSize;
        this.getPayrollGroup();
    };
    PayrollComponent.prototype.payrollGroupAdd = function () {
        var _this = this;
        this.isValid = true;
        if (this.payrollGroup.name && this.payrollGroup.frequency && this.payrollGroup.code) {
            var postData = {
                companyId: this.companyId,
                payroll_group_name: this.payrollGroup.name.trim(),
                pay_frequency: this.payrollGroup.frequency,
                pay_group_code: this.payrollGroup.code.trim()
            };
            this.payRollService.addPayrollGroup(postData)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Group", res.message, "success");
                    jQuery("#myModal").modal("hide");
                    _this.getPayrollGroup();
                    _this.payrollGroup.code = '';
                    _this.payrollGroup.frequency = '';
                    _this.payrollGroup.name = '';
                    _this.isValid = false;
                }
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Group", err.error.message, "error");
            });
        }
    };
    PayrollComponent.prototype.payrollGroupName = function () {
        console.log(this.payrollGroup.name);
    };
    PayrollComponent.prototype.payGroupCode = function () {
        console.log(this.payrollGroup.code);
    };
    PayrollComponent.prototype.editPayrollGroupName = function () {
        if (this.deleteIds.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Payrollgroup', "please select only one payroll Group", "error");
        }
        if (this.deleteIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Payrollgroup', "please select one payroll Group", "error");
        }
        if (this.deleteIds.length == 1) {
            jQuery("#myModal1").modal("show");
            this.getSinglePayrollGroup(this.deleteIds[0]);
        }
    };
    PayrollComponent.prototype.getSinglePayrollGroup = function (id) {
        var _this = this;
        this.payRollService.getSinglePayrollGroup(this.companyId, id)
            .subscribe(function (res) {
            console.log(res);
            _this.SinglePayrollGroup = res.data;
            var result_frequency = _this.SinglePayrollGroup.pay_frequency;
            if (result_frequency == 'Weekly') {
                _this.payrollCalendar.number_of_days_in_a_cycle = 7;
            }
            if (result_frequency == 'Bi-Weekly') {
                _this.payrollCalendar.number_of_days_in_a_cycle = 14;
            }
            if (result_frequency == 'Semi-Monthly') {
                _this.payrollCalendar.number_of_days_in_a_cycle = 15;
            }
            if (result_frequency == 'Monthly') {
                _this.payrollCalendar.number_of_days_in_a_cycle = 31;
            }
            _this.stattsDateChange();
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.deletePayrollGroupName = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            _ids: this.deleteIds
        };
        this.payRollService.deletePaygroups(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Group", res.message, "success");
                _this.deleteIds = [];
            }
            _this.getPayrollGroup();
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Group', err.error.message, 'error');
        });
    };
    // ========================================================================salaryGrade=================================================
    PayrollComponent.prototype.addSalaryGrade = function () {
        var x = {
            isChecked: false,
            grade: '',
            min: '',
            mid: '',
            max: '',
            average_salary: '',
            average_market_value: '',
            midSalValueError: false,
            minSalValueError: false,
            maxSalValueError: false,
        };
        this.isValidSal = false;
        this.addSaralySubmit = false;
        this.salryGrades.push(x);
        console.log(this.salryGrades, "this.salryGrades");
    };
    PayrollComponent.prototype.selectedSalGrade = function (i, event, data) {
        var _this = this;
        console.log(i, event, data);
        // var deleteSalGradeBand = []
        this.deleteSalGradeBand = [];
        if (event == true) {
            this.chooseSalGrades.push(i);
            if (data._id) {
                this.salGradeGradeBands.push(data);
            }
        }
        if (event == false) {
            this.chooseSalGrades.splice(this.chooseSalGrades[i], 1);
            if (data._id) {
                this.salGradeGradeBands.splice(data, 1);
            }
        }
        console.log(this.chooseSalGrades, this.salGradeGradeBands, "this.chooseSalGrades");
        this.salGradeGradeBands.forEach(function (element) {
            if (element._id) {
                _this.deleteSalGradeBand.push(element._id);
            }
        });
    };
    PayrollComponent.prototype.deleteSalGradeBands = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            salary_grade_id: this.salaryGradedeleteIds[0],
            _ids: this.deleteSalGradeBand
        };
        this.payRollService.deleteSalaryGradeBands(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, 'success');
                _this.getSingleSalaryGrade(_this.salaryGradedeleteIds[0]);
                // this.salaryGradedeleteIds = [];
                _this.salGradeGradeBands = [];
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', err.error.message, 'error');
        });
    };
    PayrollComponent.prototype.deleteSalaryGrades = function () {
        console.log(this.chooseSalGrades, this.salryGrades);
        for (var i = 0; i < this.chooseSalGrades.length; i++) {
            this.salryGrades.splice(this.chooseSalGrades[i], 1);
        }
        console.log(this.chooseSalGrades, this.salryGrades, "after");
    };
    PayrollComponent.prototype.salaryGradeSubmit = function () {
        var _this = this;
        this.isValidSal = true;
        this.salryGrades.forEach(function (element) {
            if (element.min == '' || element.mid == '' || element.max == '' || element.grade == '') {
                _this.gradeBandSubmit = true;
            }
        });
        console.log(this.gradeBandSubmit);
        if (this.salGrade.name && this.payrollGroup.frequency) {
            var postData = {
                companyId: this.companyId,
                salary_grade_name: this.salGrade.name,
                pay_frequency: this.payrollGroup.frequency,
                grades: []
            };
            if (this.salryGrades.length >= 1) {
                postData['grades'] = this.salryGrades;
            }
            postData.grades = postData.grades.map(function (x) {
                delete x.isChecked;
                delete x.minSalValueError;
                delete x.midSalValueError;
                delete x.maxSalValueError;
                return x;
            });
            console.log(postData);
            console.log(postData, "sending data");
            if (!this.gradeBandSubmit) {
                this.payRollService.addSalaryGrade(postData)
                    .subscribe(function (res) {
                    console.log(res);
                    if (res.status = true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, "success");
                        _this.salryGrades = [];
                        _this.salGrade.name = '';
                        jQuery("#myModal6").modal("hide");
                        _this.getsalrayGrades();
                        _this.isValidSal = false;
                        _this.salaryGradedeleteIds = [];
                    }
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', err.error.message, "error");
                });
            }
        }
    };
    PayrollComponent.prototype.salaryGradeUpdate = function () {
        var _this = this;
        this.isValidSal = true;
        this.salryGrades.forEach(function (element) {
            if (element.min == '' || element.mid == '' || element.max == '' || element.grade == '') {
                _this.gradeBandSubmit = true;
            }
        });
        if (this.salGrade.name && this.payrollGroup.frequency) {
            // if(this.salryGrades[i].maxSalValueError)
            var postData = {
                _id: this.salaryGradedeleteIds[0],
                companyId: this.companyId,
                salary_grade_name: this.salGrade.name,
                pay_frequency: this.payrollGroup.frequency,
                grades: []
            };
            if (this.salryGrades.length >= 1) {
                postData['grades'] = this.salryGrades;
            }
            postData.grades = postData.grades.map(function (x) {
                delete x.isChecked;
                delete x.minSalValueError;
                delete x.midSalValueError;
                delete x.maxSalValueError;
                return x;
            });
            // this.salryGrades.forEach(element => {
            //   if(element.min != '' && element.mid && element.max && element.grade){
            //   }
            // });
            console.log(postData, "sending data");
            if (this.salryGrades.length && !this.gradeBandSubmit) {
                this.payRollService.updateSalaryGrade(postData)
                    .subscribe(function (res) {
                    console.log(res);
                    if (res.status = true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, "success");
                        _this.salryGrades = [];
                        _this.salaryGradedeleteIds = [];
                        _this.salGradeGradeBands = [];
                        _this.salGrade.name = '';
                        jQuery("#myModal6").modal("hide");
                        _this.isValidSal = false;
                        _this.updateSalGrade = false;
                        _this.getsalrayGrades();
                    }
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade band', err.error.message, "error");
                });
            }
            if (this.salryGrades.length == 0) {
                this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade band', 'Please add the Grade', 'error');
            }
        }
    };
    PayrollComponent.prototype.getsalrayGrades = function () {
        var _this = this;
        var postData = {
            "companyId": this.companyId,
            "per_page": this.pageEvents1.salaryGradePerpage,
            "page_no": this.pageEvents1.salaryGradePageNo,
            "archive": false
        };
        this.payRollService.getSalrayGradeList(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.salaryGradeList = res.data;
            _this.salaryGradeCount = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.pageEventsSalaryGrade = function (event) {
        console.log(event);
        this.pageEvents1.salaryGradePageNo = event.pageIndex + 1;
        this.pageEvents1.salaryGradePerpage = event.pageSize;
        this.getsalrayGrades();
    };
    PayrollComponent.prototype.editSalryGradeName = function () {
        this.updateSalGrade = true;
        this.salGradeGradeBands = [];
        if (this.salaryGradedeleteIds.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', "please select only one Salary Grade", "error");
        }
        if (this.salaryGradedeleteIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', "please select one Salary Grade", "error");
        }
        if (this.salaryGradedeleteIds.length == 1) {
            jQuery("#myModal6").modal("show");
            this.getSingleSalaryGrade(this.salaryGradedeleteIds[0]);
        }
    };
    PayrollComponent.prototype.getSingleSalaryGrade = function (id) {
        var _this = this;
        this.salryGrades = [];
        this.payRollService.getSingleSalaryGrade(this.companyId, id)
            .subscribe(function (res) {
            console.log(res);
            _this.SingleSalaryGrade = res.data;
            _this.salaryGradeBands = res.grades_total_count;
            _this.salGrade.name = res.data.salary_grade_name;
            _this.payrollGroup.frequency = res.data.pay_frequency;
            _this.SingleSalaryGrade.grades.forEach(function (element) {
                element['isChecked'] = false;
                _this.salryGrades.push(element);
            });
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.deleteSAlaryGrades = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            _ids: this.salaryGradedeleteIds
        };
        this.payRollService.deleteSalaryGrades(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', res.message, 'success');
            _this.getsalrayGrades();
            _this.salGradeGradeBands = [];
            _this.salaryGradedeleteIds = [];
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation('Salary Grade', err.error.message, 'error');
        });
    };
    PayrollComponent.prototype.minValue = function (i, event) {
        console.log(i, event);
        this.salrayGradeMinValue = event;
        if (this.salrayGradeMinValue > this.salrayGradeMidValue && this.salrayGradeMinValue > this.salrayGradeMaxValue) {
            console.log("min value");
            this.salryGrades[i].minSalValueError = true;
        }
        if (this.salrayGradeMinValue < this.salrayGradeMidValue && this.salrayGradeMinValue < this.salrayGradeMaxValue) {
            console.log("min value");
            this.salryGrades[i].minSalValueError = false;
        }
        this.gradeBandSubmit = false;
    };
    PayrollComponent.prototype.maxValue = function (i, event) {
        console.log(i, event);
        this.salrayGradeMaxValue = event;
        if (this.salrayGradeMaxValue < this.salrayGradeMidValue && this.salrayGradeMaxValue < this.salrayGradeMinValue) {
            console.log("min value");
            this.salryGrades[i].maxSalValueError = true;
        }
        if (this.salrayGradeMaxValue > this.salrayGradeMidValue && this.salrayGradeMaxValue > this.salrayGradeMinValue) {
            console.log("min value");
            this.salryGrades[i].maxSalValueError = false;
        }
        console.log(this.salrayGradeMaxValue, this.salrayGradeMinValue);
        this.gradeBandSubmit = false;
    };
    PayrollComponent.prototype.midValue = function (i, event) {
        console.log(i, event);
        this.salrayGradeMidValue = event;
        if (this.salrayGradeMinValue > this.salrayGradeMidValue) {
            console.log("min value");
            this.salryGrades[i].midSalValueError = true;
        }
        if (this.salrayGradeMinValue < this.salrayGradeMidValue) {
            console.log("min value");
            this.salryGrades[i].midSalValueError = false;
        }
        this.gradeBandSubmit = false;
    };
    PayrollComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    PayrollComponent.prototype.keyPress1 = function (event) {
        var pattern = /^[A-Za-z0-9]+$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    PayrollComponent.prototype.salaryGradepageEvents = function (event) {
        this.pageEvents1.pagenoSalaryGradeBand = event.pageIndex + 1;
        this.pageEvents1.perpageSalaryGradeBand = event.pageSize;
        this.getSingleSalaryGradeBands(this.salaryGradedeleteIds[0]);
    };
    PayrollComponent.prototype.getSingleSalaryGradeBands = function (id) {
        var _this = this;
        var postData = {
            "companyId": this.companyId,
            "salary_grade_id": id,
            "per_page": this.pageEvents1.perpageSalaryGradeBand,
            "page_no": this.pageEvents1.pagenoSalaryGradeBand
        };
        this.payRollService.getSingleSalaraygradeBandsPagination(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.salryGrades = [];
            _this.SingleSalaryGrade = res.data;
            _this.salaryGradeBands = res.total_count;
            _this.salGrade.name = res.data.salary_grade_name;
            _this.SingleSalaryGrade.forEach(function (element) {
                element['isChecked'] = false;
                _this.salryGrades.push(element);
            });
        }, function (err) {
        });
    };
    // ====================================================================================payroll calandre===========================================================================================
    PayrollComponent.prototype.previewCalander = function () {
        var _this = this;
        this.isValidCalander = true;
        if (this.payrollCalendar.calendar_name && this.payrollCalendar.first_cycle_start_date && this.payrollCalendar.number_of_cycles && this.payrollCalendar.number_of_days_in_a_cycle) {
            console.log(this.payrollCalendar);
            // if (parseInt(this.payrollCalendar.number_of_cycles) <= 53) {
            this.payRollService.previewCalander(this.payrollCalendar)
                .subscribe(function (res) {
                console.log(res);
                _this.payrollCalanderCycle = res.data;
            }, function (err) {
            });
        }
        // }
    };
    PayrollComponent.prototype.numberOfCycle = function () {
        console.log(this.payrollCalendar.number_of_cycles);
        if (parseInt(this.payrollCalendar.number_of_cycles) >= 53) {
            this.showWeeksError = true;
        }
        if (parseInt(this.payrollCalendar.number_of_cycles) <= 53) {
            this.showWeeksError = false;
        }
    };
    PayrollComponent.prototype.singlePayrollCalander = function () {
        var _this = this;
        var postData = {};
        postData = Object.assign(postData, this.payrollCalendar);
        postData['companyId'] = this.companyId;
        postData['paycycle_dates'] = this.payrollCalanderCycle;
        console.log(postData, "sending postdaata");
        if (!postData['assigned_payroll_group']) {
            delete postData['assigned_payroll_group'];
        }
        this.isValidCalander = true;
        if (this.payrollCalendar.calendar_name && this.payrollCalendar.first_cycle_start_date && this.payrollCalendar.number_of_cycles && this.payrollCalendar.number_of_days_in_a_cycle) {
            console.log(this.payrollCalendar);
            if (parseInt(this.payrollCalendar.number_of_cycles) <= 53) {
                this.payRollService.singlePayrollCalanderAdd(postData)
                    .subscribe(function (res) {
                    console.log(res);
                    if (res.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", res.message, 'success');
                        jQuery("#myModal3").modal("hide");
                        _this.getPayrollCalandereList();
                        _this.payrollCalendar.calendar_name = '';
                        _this.payrollCalendar.calendar_year = '';
                        _this.payrollCalendar.first_cycle_end_date = '';
                        _this.payrollCalendar.first_cycle_start_date = '';
                        _this.payrollCalendar.number_of_cycles = '';
                        _this.payrollCalendar.number_of_days_in_a_cycle = '';
                        _this.payrollCalendar.assigned_payroll_group = '';
                        // this.payrollCalendar={};
                        // myForm.reset();
                        _this.isValidCalander = false;
                        _this.payrollCalanderCycle = [];
                    }
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", err.error.message, 'error');
                });
            }
        }
    };
    PayrollComponent.prototype.singlePayrollCalanderUpdate = function () {
        var _this = this;
        var postData = {};
        postData = Object.assign(postData, this.payrollCalendar);
        postData['companyId'] = this.companyId;
        postData['paycycle_dates'] = this.payrollCalanderCycle;
        postData['_id'] = this.calanderdeleteIds[0];
        console.log(postData, "sending postdaata");
        this.payRollService.singlePayrollCalanderUpdate(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", res.message, 'success');
                jQuery("#myModal3").modal("hide");
                _this.getPayrollCalandereList();
                _this.payrollCalendar.calendar_name = '';
                _this.payrollCalendar.calendar_year = '';
                _this.payrollCalendar.first_cycle_end_date = '';
                _this.payrollCalendar.first_cycle_start_date = '';
                _this.payrollCalendar.number_of_cycles = '';
                _this.payrollCalendar.number_of_days_in_a_cycle = '';
                _this.payrollCalendar.assigned_payroll_group = '';
                // this.payrollCalendar={};
                // myForm.reset();
                _this.isValidCalander = false;
                _this.payrollCalanderCycle = [];
                _this.calanderdeleteIds = [];
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Payroll Calander", err.error.message, 'error');
        });
    };
    PayrollComponent.prototype.stattsDateChange = function () {
        var result = new Date(this.payrollCalendar.first_cycle_start_date);
        console.log(result, this.payrollCalendar.number_of_days_in_a_cycle);
        result.setDate(result.getDate() + parseInt(this.payrollCalendar.number_of_days_in_a_cycle));
        // return result;
        console.log(result);
        this.payrollCalendar.first_cycle_end_date = result.toISOString();
        console.log(new Date(this.payrollCalendar.first_cycle_end_date).getFullYear());
        this.payrollCalendar.calendar_year = new Date(this.payrollCalendar.first_cycle_end_date).getFullYear();
    };
    PayrollComponent.prototype.getPayrollCalandereList = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            per_page: this.pageEvents1.calanderPerPage,
            page_no: this.pageEvents1.calanderPageNo,
            archive: false
        };
        this.payRollService.pageEvents1(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.payrollCalanderList = res.data;
            _this.payrollCalanderCount = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.pageEventsCalander = function (event) {
        console.log(event);
        this.pageEvents1.calanderPageNo = event.pageIndex + 1;
        this.pageEvents1.calanderPerPage = event.pageSize;
        this.getPayrollCalandereList();
    };
    PayrollComponent.prototype.deletePayrollCalander = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            _ids: this.calanderdeleteIds
        };
        this.payRollService.deleteCalander(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation('Payroll calander', res.message, 'success');
            _this.getPayrollCalandereList();
            _this.calanderdeleteIds = [];
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation('Payroll calander', err.error.message, 'error');
        });
    };
    PayrollComponent.prototype.payGroupAssign = function () {
        console.log("vpin", this.payrollCalendar.assigned_payroll_group);
        this.getSinglePayrollGroup(this.payrollCalendar.assigned_payroll_group);
        // var result_frequency = this.SinglePayrollGroup.pay_frequency
        // if (result_frequency == 'Weekly') {
        //   this.payrollCalendar.number_of_days_in_a_cycle = 7
        // }
        // if (result_frequency == 'Bi-Weekly') {
        //   this.payrollCalendar.number_of_days_in_a_cycle = 14
        // }
        // if (result_frequency == 'Semi-Monthly') {
        //   this.payrollCalendar.number_of_days_in_a_cycle = 15
        // }
        // if (result_frequency == 'Monthly') {
        //   this.payrollCalendar.number_of_days_in_a_cycle = 31
        // }
        // this.stattsDateChange()
    };
    PayrollComponent.prototype.openFormCalender = function () {
        this.addSaralySubmit = true;
        // this.salGradeGradeBands = [];
        this.salGrade.name = '';
        this.payrollGroup.frequency = '';
        this.salryGrades = [];
        this.payrollCalenderUpdate = false;
        this.salGradeGradeBands = [];
        console.log(this.salaryGradeBands);
    };
    PayrollComponent.prototype.payrollCalenderOpen = function () {
        this.payrollCalendar.calendar_name = '';
        this.payrollCalendar.assigned_payroll_group = '';
        this.payrollCalendar.number_of_days_in_a_cycle = '';
        this.payrollCalendar.number_of_cycles = '';
        this.payrollCalendar.first_cycle_start_date = '';
        this.payrollCalendar.first_cycle_end_date = '';
        this.payrollCalendar.calendar_year = '';
        this.payrollCalanderCycle = [];
        this.payrollCalenderUpdate = false;
        this.today = new Date();
        this.paygroup();
    };
    PayrollComponent.prototype.payrollCalenderEdit = function () {
        // jQuery("#myModal3").modal("show");
        if (this.calanderdeleteIds.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select only one payroll Calender", "error");
        }
        if (this.calanderdeleteIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select one payroll Calender", "error");
        }
        if (this.calanderdeleteIds.length == 1) {
            this.getSinglePayrollCalender(this.calanderdeleteIds[0]);
        }
    };
    PayrollComponent.prototype.startNextCycle = function () {
        var _this = this;
        this.currentCylcle = false;
        this.nextCylcle = false;
        jQuery("#myModal4").modal("show");
        this.payRollService.StartNextCycle(this.companyId, this.calanderdeleteIds[0])
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.nextCycle = res.data;
                if (!_this.nextCycle.current_paycycle) {
                    console.log('1');
                    _this.currentCylcle = true;
                }
                if (_this.nextCycle.current_paycycle) {
                    console.log('2');
                    _this.currentPayCycleDates = _this.nextCycle.current_paycycle;
                }
                if (!_this.nextCycle.next_pay_cycle) {
                    console.log('3');
                    _this.nextCylcle = true;
                }
                if (_this.nextCycle.next_pay_cycle) {
                    console.log('4');
                    _this.nextPayCycleDates = _this.nextCycle.next_pay_cycle;
                }
            }
        }, function (err) {
        });
    };
    PayrollComponent.prototype.reopenPreviousCycle = function () {
        if (this.calanderdeleteIds.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select only one payroll Calender", "error");
        }
        if (this.calanderdeleteIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', "please select one payroll Calender", "error");
        }
        if (this.calanderdeleteIds.length == 1) {
            this.reopenPreviousCyclePopup();
        }
    };
    PayrollComponent.prototype.reopenPreviousCyclePopup = function () {
        var _this = this;
        this.payRollService.reopenPreviousCycle(this.companyId, this.calanderdeleteIds[0])
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                jQuery("#myModal5").modal("show");
                _this.nextCycle1 = res.data;
                if (!_this.nextCycle1.previous_cycle) {
                    console.log('1');
                    _this.currentCylcle1 = true;
                }
                if (_this.nextCycle1.previous_cycle) {
                    console.log('2');
                    _this.currentPayCycleDates1 = _this.nextCycle1.previous_cycle;
                }
                if (!_this.nextCycle1.active_cycle) {
                    console.log('3');
                    _this.nextCylcle1 = true;
                }
                if (_this.nextCycle1.active_cycle) {
                    console.log('4');
                    _this.nextPayCycleDates1 = _this.nextCycle1.active_cycle;
                }
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Previuos Cycle", err.error.message, 'error');
        });
    };
    PayrollComponent.prototype.reopenCycleSubmit = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            _id: this.calanderdeleteIds[0],
        };
        this.payRollService.reopenPreviousCycleSubmit(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                jQuery("#myModal5").modal("hide");
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation('Previuos Cycle', err.error.message, 'error');
        });
    };
    PayrollComponent.prototype.payrollCalenderCopy = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            _id: this.calanderdeleteIds[0]
        };
        this.payRollService.copyCalender(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                // this.swalAlertService.SweetAlertWithoutConfirmation('Payroll Calender', res.message, 'success')
                _this.payrollCalenderUpdate = false;
                _this.paygroup();
                jQuery("#myModal3").modal("show");
                _this.payrollCalendar = res.data;
                _this.calanderdeleteIds = [];
                _this.getPayrollCalandereList();
            }
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.cycleDataSend = function () {
        var _this = this;
        var postData = {
            "companyId": this.companyId,
            "_id": this.calanderdeleteIds[0],
        };
        // if (this.calanderGroupId) {
        //   postData['active_cycle_number'] = 2;
        // }
        this.payRollService.cycleDataSend(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Cycle Date", res.message, 'success');
                jQuery("#myModal4").modal("hide");
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Cycle Date", err.error.message, 'success');
        });
    };
    PayrollComponent.prototype.paygroup = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
        };
        this.payRollService.getPayrollGroup(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.payrollGroupsDropdown = res.data;
                // this.payrollGroupCount = res.total_count;
            }
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.getSinglePayrollCalender = function (id) {
        var _this = this;
        this.paygroup();
        this.payRollService.getPayrollCalender(this.companyId, id)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.payrollCalendar = res.data;
                _this.payrollCalenderUpdate = true;
                _this.payrollCalendar.assigned_payroll_group = res.data.assigned_payroll_group._id;
                if (res.data.paycycle_dates) {
                    _this.payrollCalanderCycle = res.data.paycycle_dates;
                }
                jQuery("#myModal3").modal("show");
            }
        }, function (err) {
            console.log(err);
        });
    };
    PayrollComponent.prototype.salaryGradeCancel = function () {
        var dialogRef = this.dialog.open(_shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_5__["ConfirmationComponent"], {
            width: '350px',
            panelClass: 'confirm-dialog',
            data: {
                text: 'cancel'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
            if (result) {
                jQuery("#myModal6").modal("hide");
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"])
    ], PayrollComponent.prototype, "form", void 0);
    PayrollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payroll',
            template: __webpack_require__(/*! ./payroll.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.html"),
            styles: [__webpack_require__(/*! ./payroll.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/payroll/payroll.component.css")]
        }),
        __metadata("design:paramtypes", [_services_pay_roll_service__WEBPACK_IMPORTED_MODULE_1__["PayRollService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_4__["OnboardingService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]])
    ], PayrollComponent);
    return PayrollComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.css":
/*!**************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.css ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".stucture-border {\n    border-bottom: 2px solid #eee;\n    padding-bottom: 15px;\n    margin-bottom: 25px;\n}\n.mar-left-25 {\n    margin-left: 25px;\n}\n.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {\n    border-bottom: 1px solid #fff;\n    border-top: 1px solid #fff;\n}\n.table {\n    border-collapse: separate;\n    border-spacing: 0 7px;\n    border-bottom: 2px solid #eee;\n    padding-bottom: 40px;\n    margin-bottom: 25px;\n}\n.tbl-head {\n    background: #ecf7ff\n}\n.rows-bg {\n    background: #f8f8f8;\n}\n.text-fields {\n    width: 100%;\n    border: 1px solid #fff;\n    box-shadow: none;\n    background: #fff;\n    padding: 0 0px 0 5px;\n}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.active-structure {\n    border-left: 4px solid #5ddb97;\n    border-radius: 5px;\n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.cont-check{\n        margin-left: 15px;\n        margin-top: 7px;\n    }\n.width-55 {\n    width: 100%;\n}\n.structure-table{\n    width: 60%;\n    margin: 0 auto;\n}\n.cancel { color:#e5423d;}\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;\n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.html ***!
  \***************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-12\">\n  <div class=\"stucture-border\">\n    <h4>Structure Fields&nbsp;-&nbsp;Modify Value</h4>\n  </div>\n  <div>\n    <h4>Field <span class=\"mar-left-25\">{{structureFieldName}}</span></h4>\n  </div>\n  <div>\n    <table class=\"table\">\n      <thead class=\"tbl-head\">\n        <tr>\n          <th style=\"width:5%;\">\n          </th>\n          <th style=\"width:15%;\">Name</th>\n          <th style=\"width:15%;\">Job Code</th>\n          <th *ngIf=\"jobtitle\" style=\"width:15%;\"> Salary Grade</th>\n         \n          <th *ngIf=\"jobtitle\" style=\"width:15%;\"> Salary Grade Bands</th>\n          <th *ngIf=\"jobtitle\" style=\"width:15%;\"> EEO Job Category</th>\n\n          \n          <th style=\"width:15%;\">Status</th>\n\n          <th style=\"width:5%;\">\n            <span class=\"option-position\">\n              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n              <mat-menu #menu=\"matMenu\">\n                <button *ngIf=\"!checkFiledIds.length >=1\" mat-menu-item (click)=\"addStructureField()\">Add</button>\n                <button *ngIf=\"(!checkFiledIds.length <=0) && (checkFiledIds.length ==1)\" mat-menu-item (click)=\"editStructureField()\">Edit</button>\n                <button *ngIf=\"(!checkFiledIds.length <=0) && (checkFiledIds.length >=1)\" mat-menu-item (click)=\"deleteStructureField()\">Delete</button>\n              </mat-menu>\n            </span>\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr class=\"rows-bg\" *ngFor=\"let structure of structureValues\">\n          <td>\n            <!-- <div class=\"cont-check\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"structure.isChecked\">\n                      \n                  </mat-checkbox>\n              </div> -->\n\n          </td>\n          <td>\n            <input type=\"text\" class=\"form-control text-fields\" [(ngModel)]=\"structure.name\" [readOnly]=\"editField1\" >\n          </td>\n          <td>\n            {{structure.code}}\n            <!-- <input type=\"text\" class=\"form-control text-fields\" [(ngModel)]=\"structure.code\" [readOnly]=\"editField1\"> -->\n          </td>\n          <td *ngIf=\"jobtitle\">\n            <mat-select class=\"form-control text-fields select-btn width-55\"\n              (selectionChange)=\"selectedSalGrade($event,structure._id)\">\n              <mat-option *ngFor=\"let data of salarygradelist\" [value]=\"data._id\">{{data.salary_grade_name}}\n              </mat-option>\n\n            </mat-select>\n          </td>\n         \n          <td *ngIf=\"jobtitle\">\n            <mat-select class=\"form-control text-fields select-btn width-55\"\n              (selectionChange)=\"selectedSalGradeBand($event,structure._id)\">\n              <mat-option *ngFor=\"let data of salGradeBands\" [value]=\"data._id\">{{data.grade}} </mat-option>\n\n            </mat-select>\n          </td>\n          <td *ngIf=\"jobtitle\">\n            <mat-select class=\"form-control text-fields select-btn width-55\"\n              (selectionChange)=\"eeoJobCategorySend($event,structure._id)\">\n              <mat-option *ngFor=\"let data of EEOFields\" [value]=\"data.code\">{{data.name}} </mat-option>\n\n            </mat-select>\n          </td>\n        \n          <td>\n            <!-- <mat-select class=\"form-control text-fields select-btn width-55\" disabled>\n                  <mat-option >{{structureStatus}}</mat-option>\n                  \n                </mat-select>  -->\n            <select class=\"form-control text-fields width-55\" disabled>\n              <option value=\"\">{{structureStatus}}</option>\n\n            </select>\n\n\n          </td>\n          <td></td>\n\n\n        </tr>\n        <tr class=\"rows-bg\" *ngFor=\"let values of customValues;let i = index\">\n          <td [ngClass]=\"{'zenwork-checked-border': values.isChecked}\">\n            <div class=\"cont-check\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"values.isChecked\" (change)=\"checkFields($event, values._id)\">\n\n              </mat-checkbox>\n            </div>\n\n          </td>\n          <td>\n            <input type=\"text\" class=\"form-control text-fields\" placeholder=\"Name*\" [(ngModel)]=\"values.name\" [readOnly]=\"values.editField\" maxlength=\"40\">\n          </td>\n          <td>\n            {{values.prefix}}{{values.code}}\n            <!-- <input type=\"text\" class=\"form-control text-fields\" [(ngModel)]=\"values.code\" [readOnly]=\"values.editField\"> -->\n          </td>\n          <td *ngIf=\"jobtitle\">\n            <mat-select class=\"form-control text-fields select-btn width-55\" [(ngModel)]=\"values.salary_grade\"\n              (ngModelChange)=\"selectedSalGrade($event,values._id,i)\">\n              <mat-option *ngFor=\"let data of salarygradelist\" [value]=\"data._id\">{{data.salary_grade_name}}\n              </mat-option>\n\n            </mat-select>\n          </td>\n          \n          <td *ngIf=\"jobtitle\">\n            <mat-select class=\"form-control text-fields select-btn width-55\" [(ngModel)]=\"values.grade_band\"\n              (selectionChange)=\"selectedSalGradeBand($event,values._id,i)\">\n              <mat-option *ngFor=\"let data of values.salGradeBands\" [value]=\"data._id\">{{data.grade}} </mat-option>\n\n            </mat-select>\n \n            <p *ngIf=\"values.isValidBand\" class=\"error\">This field is mandatory</p>\n\n          </td>\n          <td *ngIf=\"jobtitle\">\n            <mat-select class=\"form-control text-fields select-btn width-55\" [(ngModel)]=\"values.EeoJobCategoryCode\"\n              (selectionChange)=\"eeoJobCategorySend($event,values._id)\">\n              <mat-option *ngFor=\"let data of EEOFields\" [value]=\"data.code\">{{data.name}} </mat-option>\n\n            </mat-select>\n          </td>\n         \n          <td>\n\n            <mat-select class=\"form-control text-fields select-btn width-55\" placeholder=\"Status*\" [(ngModel)]=\"values.status\"\n              [disabled]=\"values.editField\" (ngModelChange)=\"fieldStatus(values.status)\">\n              <!-- <mat-option value=\"\">Select</mat-option> -->\n              <mat-option [value]=\"1\">Active</mat-option>\n              <mat-option [value]=\"0\">Inactive</mat-option>\n            </mat-select>\n\n          </td>\n          <td></td>\n        </tr>\n\n\n      </tbody>\n    </table>\n    <div class=\"row\">\n\n      <button mat-button (click)=\"cancel()\" class=\"cancel\">Cancel</button>\n\n      <button *ngIf=\"!jobtitle\" mat-button class=\"submit-btn pull-right\" (click)=\"submitStructure()\">Submit</button>\n      <button *ngIf=\"jobtitle\" mat-button class=\"submit-btn pull-right\" (click)=\"submitStructureJob()\">Submit</button>\n\n    </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.ts":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.ts ***!
  \*************************************************************************************************************************************/
/*! exports provided: StructureFieldDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StructureFieldDialogComponent", function() { return StructureFieldDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/pay-roll.service */ "./src/app/services/pay-roll.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var StructureFieldDialogComponent = /** @class */ (function () {
    function StructureFieldDialogComponent(dialogRef, swalAlertService, companySettingsService, payRollService, data) {
        this.dialogRef = dialogRef;
        this.swalAlertService = swalAlertService;
        this.companySettingsService = companySettingsService;
        this.payRollService = payRollService;
        this.data = data;
        this.editField = true;
        this.editField1 = true;
        this.structureValues = [];
        this.customValues = [];
        this.structureStatus = "Active";
        this.jobtitle = false;
        this.EEOFields = [];
        this.checkFiledIds = [];
        this.JobStructue = false;
    }
    StructureFieldDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    StructureFieldDialogComponent.prototype.ngOnInit = function () {
        this.companyId = JSON.parse(localStorage.getItem('companyId'));
        console.log(this.data);
        if (this.data.fieldsName == 'Job Title') {
            this.jobtitle = true;
            this.getEEOJobcategoryFields();
            this.getSalaryGradelist();
        }
        this.structureValues = this.data.StructureValues;
        this.customValues = this.data.customValues;
        this.customValues.filter(function (item) {
            console.log(item);
            item.editField = true;
            // item.isValidBand = false;
        });
        this.structureFieldName = this.data.fieldsName;
        // this.addStructureField()
    };
    StructureFieldDialogComponent.prototype.getEEOJobcategoryFields = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('EEO Job Category', 0)
            .subscribe(function (res) {
            console.log(res);
            _this.EEOFields = res.default;
            res.custom.forEach(function (element) {
                _this.EEOFields.push(element);
            });
            console.log(_this.EEOFields, "eeofields");
        }, function (err) {
            console.log(err);
        });
    };
    StructureFieldDialogComponent.prototype.getSalaryGradelist = function () {
        var _this = this;
        var postData = {
            "companyId": this.companyId,
        };
        this.payRollService.getSalrayGradeList(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.salarygradelist = res.data;
            for (var i = 0; i < _this.customValues.length; i++) {
                if (_this.customValues[i].salary_grade)
                    _this.salarygradelistChange(_this.customValues[i].salary_grade, i);
            }
        }, function (err) {
            console.log(err);
        });
    };
    StructureFieldDialogComponent.prototype.salarygradelistChange = function (id, i) {
        var _this = this;
        this.selectedSalarayGrade = id;
        console.log(this.selectedSalarayGrade);
        this.payRollService.getSingleSalaryGrade(this.companyId, this.selectedSalarayGrade)
            .subscribe(function (res) {
            console.log(res);
            _this.customValues[i].salGradeBands = res.data.grades;
            // this.customValues[i].isValidBand = false;
        }, function (err) {
            console.log(err);
        });
        // this.customValues.forEach(element => {
        //   if (element.grade_band == this.salGradeBands._id) {
        //     var index = this.salGradeBands.indexOf(element.grade_band);
        //     console.log(index);
        //     if (index > -1) {
        //       this.salGradeBands.splice(index, 1);
        //     }
        //   }
        // })
        console.log(this.customValues[i].salGradeBands);
    };
    StructureFieldDialogComponent.prototype.salarygradeBandlistChange = function () {
        console.log(this.selectedSalarayGradeBand);
    };
    StructureFieldDialogComponent.prototype.addStructureField = function () {
        this.customValues.push({ name: '', status: '', parentRef: this.structureFieldName });
        console.log(this.customValues);
        this.editField = false;
    };
    StructureFieldDialogComponent.prototype.fieldStatus = function (status) {
        console.log(status);
        this.status = status;
    };
    StructureFieldDialogComponent.prototype.editStructureField = function () {
        this.selectedStructureDetails = this.customValues.filter(function (structureData) {
            return structureData.isChecked;
        });
        if (this.selectedStructureDetails.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit structure", "Please select only one record to proceed", 'error');
        }
        else if (this.selectedStructureDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit structure", "Please select one record to proceed", 'error');
        }
        else {
            if (this.selectedStructureDetails[0]._id) {
                this.selectedStructureDetails[0].editField = false;
            }
            console.log(this.selectedStructureDetails[0]._id);
        }
    };
    // Author: Saiprakash, Date: 04/04/19 
    // Add and edit structure fields
    StructureFieldDialogComponent.prototype.submitStructure = function () {
        var _this = this;
        // var tempSF = [];
        // this.customValues.forEach(sf => {
        //   if (sf && sf.name && sf.status) {
        //     tempSF.push(sf);
        //   }
        // });
        // this.customValues = tempSF;
        console.log(this.customValues);
        this.companySettingsService.addUpdateStructureData(this.customValues).subscribe(function (res) {
            console.log(res);
            _this.checkFiledIds = [];
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            var resdata;
            resdata = res;
            resdata.field = _this.structureFieldName;
            console.log(resdata, _this.structureFieldName);
            console.log(res, "111111111111111");
            _this.dialogRef.close(resdata);
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    StructureFieldDialogComponent.prototype.submitStructureJob = function () {
        var _this = this;
        console.log(this.customValues);
        for (var i = 0; i < this.customValues.length; i++) {
            if (this.customValues[i].salary_grade && !this.customValues[i].grade_band)
                this.customValues[i].isValidBand = true;
        }
        for (var i = 0; i < this.customValues.length; i++) {
            if (this.customValues[i].isValidBand == true) {
                console.log(i);
                this.JobStructue = true;
            }
        }
        console.log(this.customValues, this.JobStructue);
        if (!this.JobStructue) {
            this.companySettingsService.addUpdateStructureData(this.customValues).subscribe(function (res) {
                console.log(res);
                _this.checkFiledIds = [];
                if (res.status == true) {
                    var resdata;
                    resdata = res;
                    resdata.field = _this.structureFieldName;
                    console.log(resdata, _this.structureFieldName);
                    console.log(res, "111111111111111");
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                    _this.dialogRef.close(resdata);
                }
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Job Title', err.error.message, 'error');
            });
        }
    };
    StructureFieldDialogComponent.prototype.eeoJobCategorySend = function (event, id) {
        console.log(event, id);
        this.customValues.forEach(function (element) {
            if (element._id == id) {
                element['EeoJobCategoryCode'] = event.value;
            }
        });
        this.structureValues.forEach(function (element) {
            if (element._id == id) {
                element['EeoJobCategoryCode'] = event.value;
            }
        });
        console.log(this.customValues);
    };
    StructureFieldDialogComponent.prototype.selectedSalGrade = function (event, id, i) {
        console.log(event, id, i);
        this.salarygradelistChange(event, i);
        this.customValues.forEach(function (element) {
            if (element._id == id) {
                element['salary_grade'] = event;
            }
        });
        this.structureValues.forEach(function (element) {
            if (element._id == id) {
                element['salary_grade'] = event;
            }
        });
        console.log(this.customValues);
    };
    StructureFieldDialogComponent.prototype.selectedSalGradeBand = function (event, id, i) {
        console.log(event, id);
        this.customValues.forEach(function (element) {
            if (element._id == id) {
                element['grade_band'] = event.value;
            }
        });
        this.structureValues.forEach(function (element) {
            if (element._id == id) {
                element['grade_band'] = event.value;
            }
        });
        this.customValues[i].isValidBand = false;
        this.JobStructue = false;
        console.log(this.customValues);
    };
    // Author: Saiprakash, Date: 04/04/19 
    // Delete structure fields
    StructureFieldDialogComponent.prototype.deleteStructureField = function () {
        var _this = this;
        this.selectedStructureDetails = this.customValues.filter(function (structureData) {
            return structureData.isChecked;
        });
        if (this.selectedStructureDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete structure", "Please select one record to proceed", 'error');
        }
        else {
            var fieldsList = [];
            var data = {
                _ids: this.selectedStructureDetails
            };
            this.companySettingsService.deleteStructure(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.customValues.forEach(function (element) {
                    console.log(_this.checkFiledIds);
                    if (_this.checkFiledIds.indexOf(element._id) < 0) {
                        fieldsList.push(element);
                    }
                    console.log(fieldsList);
                });
                _this.customValues = fieldsList;
                _this.checkFiledIds = [];
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    StructureFieldDialogComponent.prototype.checkFields = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkFiledIds.push(id);
            console.log(this.checkFiledIds);
        }
        if (event.checked == false) {
            var index = this.checkFiledIds.indexOf(id);
            if (index > -1) {
                this.checkFiledIds.splice(index, 1);
            }
            console.log(this.checkFiledIds);
        }
    };
    StructureFieldDialogComponent.prototype.cancel = function () {
        console.log(this.structureFieldName);
        this.dialogRef.close(this.structureFieldName);
    };
    StructureFieldDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-structure-field-dialog',
            template: __webpack_require__(/*! ./structure-field-dialog.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.html"),
            styles: [__webpack_require__(/*! ./structure-field-dialog.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.css")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_4__["PayRollService"], Object])
    ], StructureFieldDialogComponent);
    return StructureFieldDialogComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".import-btn {\n    border: 1px solid #797d7b;\n    background-color: #797d7b;\n    color: #fff;\n    border-radius: 4px;\n    outline: none;\n  \n}\n.mar-top-30 {\n    margin-top: 30px;\n}\n.mar-left-25 {\n    margin-left: 25px;\n}\n.mar-left-25 a {\n    cursor: pointer;\n}\n.table-body{\n    background: #fff;\n    padding: 35px 50px;\n    margin-top: 30px;\n}\n.structure-list .list-items{\n    padding: 15px 15px;\n    border-bottom: 2px solid #eee;\n}\n.structure-list .list-items-values{\n    padding: 17px 37px;\n    border-bottom: 2px solid #eee;\n}\n.checkbox label{\n    padding: 0 0 0 20px;\n}\n.checkbox-success input[type=\"checkbox\"]:checked + label::before {\n    background-color: #797d7b;\n    border-color: #797d7b;\n  }\n.checkbox-success input[type=\"checkbox\"]:checked + label::after {\n    color: #fff;\n  }\n.checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]{\n      display: none;\n  }\n.structure-head {\n    margin-left: 40px;\n    margin-top: 30px;\n  }\n.structure-tbl-body {\n    background: #f8f8f8;\n    width: 45%;\n  }\n.bg-color {\n      background: #ecf7ff;\n      padding: 15px 10px;\n  }\n.mar-left-40 {\n      margin-left: 40px;\n  }\n.active-structure {\n      border-left: 4px solid #5ddb97;\n      border-radius: 5px;\n  }\n.arrow-position {\n    position: relative;\n}\n.arrow-align-one {\n    font-size: 40px;\n    position: absolute;\n    top: 145px;\n    left: 30px;\n    font-weight: lighter;\n    color: #439348;\n    \n    \n}\n.arrow-align-two {\n    font-size: 40px;\n    position: absolute;\n    top: 145px;\n    right: 0;\n    left: 26px;\n    font-weight: lighter;\n    color: #439348;\n}\n#fileUpload {\n    height: 0;\n    display: none;\n    width: 0;\n  }\n.upload{\n    text-decoration: none;\n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.height-300 {\n    height: 300px;\n    overflow: auto;\n}\n.buttons{\n    margin: 20px 0 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    cursor: pointer;\n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background:#008f3d; font-size: 16px;display: block; margin: 20px 0 0;\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-12\">\n  <div class=\"row\">\n    <div class=\"mar-top-30\">\n        <input type=\"file\" id=\"fileUpload\" (change)=\"fileChangeEvent($event)\">\n        <label for=\"fileUpload\">\n            <a mat-raised-button class=\"import-btn upload\">Import Structure</a>\n            \n          </label>\n         \n      <!-- <button class=\"import-btn\">Import Structure</button> -->\n      <span class=\"mar-left-25\">Looking for Excel template?\n        <a href=\"../assets/docs/Structure Fields and Values.xlsx\" target=\"_blank\" class=\"green\">Click here to Download.</a>\n      </span>\n    </div>\n    <div>\n      <div class=\"col-sm-12 table-body\">\n        <div class=\"row\">\n          <div class=\"col-sm-6 structure-tbl-body height-300\">\n            <div class=\"row\">\n              <div class=\"bg-color\">\n                <h4 class=\"mar-left-40\">Structure Fields</h4>\n              </div>\n            \n            <div>\n              <ul class=\"list-unstyled structure-list\" *ngFor=\"let fields of structureFields\">\n\n                <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                  <div>\n                    \n                      <mat-checkbox class=\"checkbox-success\"  [(ngModel)]=\"fields.isChecked\" [value]=\"fields.name\" (ngModelChange)=\"getStructureSubFields(fields.name,fields.isChecked )\">\n                      {{fields.name}}\n                      </mat-checkbox>\n                  </div>\n                 \n                </li>\n             \n              </ul>\n            </div>\n            </div>\n          </div>\n          <div class=\"col-sm-1 text-center arrow-position\">\n            <i class=\"material-icons arrow-align-one\">\n              keyboard_arrow_right\n              </i>\n              <i class=\"material-icons arrow-align-two\">\n                keyboard_arrow_right\n                </i>\n          </div>\n          <div class=\"col-sm-6 structure-tbl-body height-300\">\n            <div class=\"row\">\n              <div class=\"bg-color\">\n                <h4 class=\"mar-left-40\">Current Values\n                  <span class=\"pull-right\">\n                    <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n                    <mat-menu #menu=\"matMenu\" class=\"option-position\">\n                      <button mat-menu-item class=\"edit-color\" (click)=\"openDialog()\">Edit</button>\n                    </mat-menu>\n                  </span>\n                </h4>\n              </div>\n            </div>\n            <div *ngIf=\"StructureValues\">\n              <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of StructureValues\">\n\n                <li class=\"list-items-values\">\n                {{values.name}}\n                </li>\n            \n              </ul>\n\n            </div>\n            <div *ngIf=\"customValues\">\n                <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of customValues\">\n  \n                  <li class=\"list-items-values\">\n                  {{values.name}}\n                  </li>\n              \n                </ul>\n  \n              </div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n  </div>\n</div>\n<div class=\"buttons\">\n  <ul class=\"list-unstyled\">\n   \n    <li class=\"list-items pull-right\">\n      <button class='save' type=\"submit\" (click)=\"saveandNext()\">\n        Save Changes\n      </button>\n    </li>\n\n    <div class=\"clearfix\"></div>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: StructureComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StructureComponent", function() { return StructureComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _structure_field_dialog_structure_field_dialog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./structure-field-dialog/structure-field-dialog.component */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure-field-dialog/structure-field-dialog.component.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StructureComponent = /** @class */ (function () {
    function StructureComponent(dialog, companySettingsService, router) {
        this.dialog = dialog;
        this.companySettingsService = companySettingsService;
        this.router = router;
        this.StructureValues = [];
        this.customValues = [];
        this.structureFields = [
            {
                name: 'Assets',
                isChecked: true
            }
        ];
    }
    StructureComponent.prototype.ngOnInit = function () {
        this.getFields();
        this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
    };
    // Author: Saiprakash, Date: 13/04/19 
    // Get structure fields
    StructureComponent.prototype.getFields = function () {
        var _this = this;
        this.companySettingsService.getFields().subscribe(function (res) {
            console.log(res);
            _this.structureFields = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash, Date: 04/04/19 
    // Get structure sub docs fields
    StructureComponent.prototype.getStructureSubFields = function (fields, isChecked) {
        var _this = this;
        console.log(fields, isChecked);
        this.fieldsName = fields;
        this.checked = isChecked;
        if (isChecked == true) {
            this.structureFields.forEach(function (element) {
                if (element.name !== fields) {
                    element.isChecked = false;
                }
            });
            this.companySettingsService.getStuctureFields(fields, 0).subscribe(function (res) {
                console.log(res, name);
                _this.StructureValues = res.default;
                _this.customValues = res.custom;
                console.log(_this.StructureValues);
            }, function (err) {
                console.log(err);
            });
        }
    };
    StructureComponent.prototype.openDialog = function () {
        var _this = this;
        var data = {
            StructureValues: this.StructureValues,
            fieldsName: this.fieldsName,
            customValues: this.customValues
        };
        var dialogRef = this.dialog.open(_structure_field_dialog_structure_field_dialog_component__WEBPACK_IMPORTED_MODULE_2__["StructureFieldDialogComponent"], {
            width: '1300px',
            height: '700px',
            backdropClass: 'structure-model',
            data: data,
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            if (result.field) {
                _this.getStructureSubFields(result.field, _this.checked);
            }
            else {
                console.log(result);
                _this.getStructureSubFields(result, _this.checked);
            }
        });
    };
    // Author: Saiprakash, Date: 03/04/19 
    // Upload to structure xlsx sile
    StructureComponent.prototype.fileChangeEvent = function (e) {
        var _this = this;
        var files = e.target.files[0];
        console.log(files.name);
        var formData = new FormData();
        formData.append('file', files);
        this.companySettingsService.structureUpload(formData).subscribe(function (res) {
            console.log(res);
            _this.getStructureSubFields(_this.fieldsName, _this.checked);
        }, function (err) {
            console.log(err);
        });
    };
    StructureComponent.prototype.saveandNext = function () {
        this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/training"]);
    };
    StructureComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-structure',
            template: __webpack_require__(/*! ./structure.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.html"),
            styles: [__webpack_require__(/*! ./structure.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/structure/structure.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__["CompanySettingsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], StructureComponent);
    return StructureComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.css":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* side nav css*/\n.tabs-left{\n    margin-top:0;\n}\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n    color: #000;\n    border: none;\n    background: #f8f8f8;\n  }\n.tabs-left > .nav-tabs {\n    border-right: 1px solid #ddd;\n    padding: 20px 20px 0 20px;\n    height: 700px;\n  }\n.tabs-left > .nav-tabs > li > a {\n    font-size: 16px;\n    margin-right: -1px;\n    outline: none;\n    border-radius: 4px 0 0 4px;\n  }\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee;\n  }\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n/*side nav css ends*/\n.caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n.create-custom-sidenav{\n    background: #f8f8f8;\n    padding: 20px 0;\n  }\n.main-template{\n    padding: 0 !important;\n  }\n.main-template .modal-header{\n    padding: 20px 0;\n    border-bottom: 1px solid #ccc;\n  }\n.select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n\n}\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\n.tab-content{\n    padding: 20px 0;\n}\n.user-type label{\n  line-height: 40px;\n\n}\n.company-legal-name{\n  border: none;\n  padding: 10px;\n  background: #f8f8f8;\n  box-shadow: none;\n  height: 40px;\n}\n.custom-scroll{\n  height: 700px;\n  overflow-y: auto;\n}\ntextarea{\n  resize: none;\n  border: 0;\n  box-shadow: none;\n  background: #f8f8f8;\n}\n.buttons{\n  margin: 150px 30px 0;\n}\n.buttons ul li{\n  display: inline-block;\n  padding: 0px 10px;\n}\n.buttons ul li .back{\n  border: 1px solid gray;\n  padding: 7px 30px;\n  border-radius: 31px;\n  color: #000;\n  cursor: pointer;\n}\n.buttons ul li .save{\n\n  border: 1px solid gray;\n  padding: 7px 30px;\n  border-radius: 31px;\n  color: #fff;\n  background: green;\n\n}\n.buttons ul li .cancel{\n  color:#e5423d;\n  cursor: pointer;\n}\n.system-admin{\n  padding: 40px 0 0 40px;\n}\n.modules-fields{\n  padding: 20px;\n}\n.module-table table{\n  width: 100%;\n}\n.module-table table thead th{\n  padding: 20px 40px 20px 15px;\n  font-size: 16px;\n  background: #f8f8f8;\n  border-right: 1px solid #ccc;\n  border-bottom: 1px solid #ccc;\n}\n.module-table table tbody tr td{\n  padding: 20px 40px 20px 15px;\n  font-size: 16px;\n  background: #f8f8f8;\n  border-right: 1px solid #ccc;\n  border-bottom: 1px solid #ccc;\n}\n.module-table-padding{\n  padding: 15px 30px 30px 15px;\n  border-bottom: 1px solid #ccc;\n}\n.dashboard-table-dropdown{\n  background: #fff !important;\n}\n.module-table table thead th:last-child, .module-table table tbody tr td:last-child {\n  border-right: 0;\n  \n}\n.module-table table tbody tr:last-child td:last-child,.module-table table tbody tr:last-child td:first-child{\n  border-bottom: 0;\n}\n/* accrodian css*/\n.field-accordion p {color: #565555;font-size: 15px;}\n.field-accordion .panel-default>.panel-heading { padding: 20px 0 20px; background-color:transparent !important; border: none;}\n.field-accordion .panel-title { color: #008f3d; display: inline-block;padding: 15px 0 0 0;}\n.field-accordion .panel-title a:hover { text-decoration: none;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n/* accrodian css ends */\n/* personal table data */\n.person-tables { margin: 0 auto; float: none; padding: 0;}\n.person-tables h3 { font-size: 15px; line-height: 15px; color:#5d5d5d; margin: 0 0 30px;}\n.person-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; font-size: 16px; border-radius:3px;}\n.person-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.person-tables .table>thead>tr>th a .fa {  font-size: 15px; line-height: 15px;}\n.person-tables .table>tbody>tr>td, .person-tables .table>tfoot>tr>td, .person-tables .table>thead>tr>td{ padding: 15px; background:#f8f8f8;vertical-align:middle; border-right:#e6e6e6 1px solid;border-bottom:#e6e6e6 1px solid; color: #656464;text-align: center;}\n.person-fields .checkbox{ margin: 0;}\n.person-tables .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top: 0;height: 100%;}\n.person-tables .table>tbody>tr>td.zenwork-checked-border {position: relative;text-align: left}\n.person-tables .cont-check .checkbox label { padding-left:5px;}\n.person-tables .cont-check.label-pad .checkbox label { padding-left: 0;}\n.person-tables table thead tr th{\n  border-right: 1px solid #ccc;\n  \n}\n.person-tables table thead tr th:last-child{\n  border-right: none;\n}\n.person-tables table tbody tr td:last-child{\n  border-right: none; \n}\n.person-tables table tbody tr:last-child td{\n  border-bottom: none; \n}\n.myinfo-fields-controll{\n  padding: 15px 30px 30px 15px;\n}\n.main-onboarding .setting-drop .dropdown-menu{\n  left: -76px !important;\n  min-width: 109px !important;\n}\n.setting-drop .dropdown-menu>li>a{\n    font-size: 14px !important;\n    line-height: 14px !important;\n    padding: 8px 20px !important;\n    display:block;\n}\n/* structure-field css */\n.structure-tbl-body {\n  background: #f8f8f8;\n  width: 45%;\n}\n.bg-color {\n    background: #ecf7ff;\n    padding: 15px 10px;\n}\n.mar-left-40 {\n    margin-left: 40px;\n}\n.active-structure {\n    border-left: 4px solid #5ddb97;\n    border-radius: 5px;\n}\n.arrow-position {\n  position: relative;\n}\n.arrow-align-one {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  left: 30px;\n  font-weight: lighter;\n  color: #439348;\n  \n  \n}\n.arrow-align-two {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  right: 0;\n  left: 0px;\n  font-weight: lighter;\n  color: #439348;\n}\n#fileUpload {\n  height: 0;\n  display: none;\n  width: 0;\n}\n.upload{\n  text-decoration: none;\n}\n.zenwork-checked-border:after { \n  content: '';\n  position: absolute;\n  top: 0;\n  left: -50px;\n  border-right: #83c7a1 4px solid;\n  height: 100%;\n  border-radius: 4px;\n  padding: 25px;\n  }\n.height-300 {\n  height: 300px;\n  overflow: auto;\n}\n/* structure-fieldds css ends */\n.structure-list .list-items{\n  padding: 15px 15px;\n  border-bottom: 2px solid #eee;\n}\n.structure-list .list-items-values{\n  padding: 17px 37px;\n  border-bottom: 2px solid #eee;\n}\n.arrow-position {\n  position: relative;\n}\n.structure-tbl-body {\n  background: #f8f8f8;\n  width: 45%;\n}\n.height-300 {\n  height: 300px;\n  overflow: auto;\n}\n.bg-color {\n  background: #ecf7ff;\n  padding: 15px 10px;\n}\n.mar-left-40 {\n  margin-left: 40px;\n}\n.heading-eligibility{\n  padding: 15px 0 0 30px !important;\n}\n.field-accordion .eligibilty-border-bottom{\n  padding: 0 0 40px !important;\n}\n.employees-data{\n  padding: 30px;\n}\n.error{\n  font-size:12px !important;\n  color: #f44336 !important;\n}\n.employees-table{\n  margin: 60px 0 0 0;\n}\n.recap-tab{\n  padding: 40px;\n}\n.import-date-btn {\n  background-color: #797d7a;\n  border: 1px solid #797d7a;\n  padding: 5px 14px;\n  border-radius: 50px;\n  color: #fff;\n  outline: none;\n  font-size:12px;\n}\n.custom-tables { margin: 0 auto; float: none; padding: 15px 30px 0 0px;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 14px;}\n.group-list .table>tbody>tr>td {\n  background: #f8f8f8;\n  border: none;\n  vertical-align: middle;\n  border-top: none;\n  border-bottom: #c3c3c3 1px solid;\n  color: #948e8e;\n}\n.submit-btn {\n  background-color: #439348;\n  border: 1px solid #439348;\n  border-radius: 35px;\n  color: #fff;\n  padding: 0 25px;    \n}\n.cancel { color:#e5423d;}\n.dual-list .listbox button{\n  background: #008f3d !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.html ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n  <span style=\"font-size:17px;\">Assign Training to Employees</span>\n</div>\n\n        <div class=\"employees-data\">\n            <div class=\"field-accordion\">\n              <div class=\"panel-group\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <!-- <div class=\"panel-heading\">\n                    <h4 class=\"panel-title heading-eligibility\">\n                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\"\n                        href=\"#eligibility-info\">\n                        Eligibility Criteria\n                      </a>\n                    </h4>\n                  </div> -->\n                  <div id=\"eligibility-info\" class=\"panel-collapse collapse in\">\n                    <div class=\"panel-body eligibilty-border-bottom\">\n\n                      <div class=\"employees-data\">\n                        <div class=\"col-sm-12 table-body\">\n                          <div class=\"row\">\n                            <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                              <ul class=\"list-unstyled structure-list\" *ngFor=\"let fields of structureFields\">\n\n                                <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                                  <div>\n\n                                    <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"fields.isChecked\"\n                                      [value]=\"fields.name\"\n                                      (ngModelChange)=\"getStructureSubFields(fields.name,fields.isChecked )\">\n                                      {{fields.name}}\n                                    </mat-checkbox>\n                                  </div>\n\n                                </li>\n\n                              </ul>\n                            </div>\n\n                            <div class=\"col-sm-1 text-center arrow-position\">\n                              <i class=\"material-icons arrow-align-one\">\n                                keyboard_arrow_right\n                              </i>\n                              <i class=\"material-icons arrow-align-two\">\n                                keyboard_arrow_right\n                              </i>\n                            </div>\n                            <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                              <div *ngIf=\"StructureValues\">\n                                <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of StructureValues\">\n\n                                  <li class=\"list-items-values\">\n                                    <mat-checkbox class=\"checkbox-success\"\n                                      (change)=\"subFieldsStructure($event,values.name)\" [value]=\"values.name\">\n                                      {{values.name}}\n                                    </mat-checkbox>\n\n                                  </li>\n\n                                </ul>\n\n                              </div>\n                              <div *ngIf=\"customValues\">\n                                <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of customValues\">\n\n                                  <li class=\"list-items-values\">\n                                    <mat-checkbox class=\"checkbox-success\"\n                                      (change)=\"subFieldsStructure($event,values.name)\" [value]=\"values.name\">\n                                      {{values.name}}\n                                    </mat-checkbox>\n                                  </li>\n\n                                </ul>\n\n                              </div>\n                            </div>\n\n                          </div>\n                        </div>\n\n\n                      </div>\n\n                    </div>\n                  </div>\n                </div>\n                <div class=\"employees-table modal-header\">\n                  <!-- <div class=\"col-xs-6\">\n                    <h4>Include a Specific Person</h4>\n                    <ng-multiselect-dropdown [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                      [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                      (onDeSelect)=\"OnItemDeSelect($event)\" (onSelectAll)=\"onSelectAll($event)\"\n                      (onDeSelectAll)=\"onDeSelectAll($event)\">\n                    </ng-multiselect-dropdown>\n                  </div> -->\n                  <!-- <div class=\"col-xs-6\">\n                    <h4>Exclude a Specific Person</h4>\n                    <ng-multiselect-dropdown [placeholder]=\"'custom placeholder'\" [data]=\"dropdownList\"\n                      [(ngModel)]=\"selectedItems\" [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                      (onSelectAll)=\"onSelectAll($event)\">\n                    </ng-multiselect-dropdown>\n                  </div> -->\n                  <div>\n                    <h4 style=\"float: left;\">Include a Specific Person</h4>\n                    <h4 style=\"float: right;\">Exclude a Specific Person</h4>\n                    <div class=\"clearfix\"></div>\n                    <dual-list [source]=\"dropdownList\" [(destination)]=\"selectedUsers\" [format]=\"format\" filter=true\n                    height='300px' (click)=\"onSelectUsers(selectedUsers)\">\n                  </dual-list>\n                  </div>\n                </div>\n\n                <h4 style=\"margin-top:30px; font-size:16px;\">Preview Employee List Report\n                  <!-- <button class=\"import-date-btn pull-right text-report\">Run Report</button> -->\n\n              </h4>\n              <div class=\"custom-tables group-list\">\n                  <table class=\"table\">\n                    <thead>\n                      <tr>\n                        <th>\n                          First Name\n                        </th>\n                        <th>Last Name</th>\n                        <th>Employee ID</th>\n                        <th>Department</th>\n                        <th>Manager Name</th>\n                        \n\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let list of previewList\">\n                        <td>{{list.personal.name.firstName}}</td>\n                        <td>{{list.personal.name.lastName}}</td>\n                        <td>{{list.job.employeeId}}</td>\n                        <td>{{list.job.department}}</td>\n                        <td>\n                            <span *ngIf=\"list.job.ReportsTo\">\n                                {{list.job.ReportsTo.personal.name.firstName}}\n                            </span>\n                        </td>\n                        \n                      </tr>\n                  \n                  </table>\n                  \n                </div>\n\n                <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                </mat-paginator>\n\n                <hr class=\"zenwork-margin-ten-zero\">\n                <div class=\"row\" style=\"margin:30px 0px;\">\n\n                    <button mat-button (click)=\"onNoClick()\" class=\"cancel\">Cancel</button>\n\n                    <button mat-button class=\"submit-btn pull-right\" (click)=\"assignTraining()\">Submit</button>\n                </div>\n                  <div class=\"clearfix\"></div>\n\n              </div>\n            </div>\n\n          </div>\n"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.ts ***!
  \************************************************************************************************************************/
/*! exports provided: AssignEmployeesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssignEmployeesComponent", function() { return AssignEmployeesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var AssignEmployeesComponent = /** @class */ (function () {
    function AssignEmployeesComponent(dialogRef, companySettingsService, siteAccessRolesService, accessLocalStorageService, swalAlertService, data) {
        this.dialogRef = dialogRef;
        this.companySettingsService = companySettingsService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.data = data;
        this.StructureValues = [];
        this.customValues = [];
        this.structureFields = [
            {
                name: 'Business Unit',
                isChecked: true
            }
        ];
        this.eligibilityArray = [];
        this.dropdownSettings = {};
        this.selectedUsers = [];
        this.selectedItems = [];
        this.dropdownList = [];
        this.previewList = [];
        this.format = {
            add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
            direction: angular_dual_listbox__WEBPACK_IMPORTED_MODULE_6__["DualListComponent"].LTR, draggable: true, locale: 'da',
        };
    }
    AssignEmployeesComponent.prototype.ngOnInit = function () {
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        console.log(this.data);
        this.getFieldsForRoles();
        this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
        this.pageNo = 1;
        this.perPage = 10;
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 0,
            maxHeight: 150,
            defaultOpen: true,
            allowSearchFilter: true
        };
    };
    AssignEmployeesComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.previewEmployeeList();
    };
    AssignEmployeesComponent.prototype.previewEmployeeList = function () {
        var _this = this;
        var finalPreviewUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalPreviewUsers.push(element._id);
        });
        var data = {
            per_page: this.perPage,
            page_no: this.pageNo,
            _ids: finalPreviewUsers
        };
        this.companySettingsService.prviewEmployeeList(data).subscribe(function (res) {
            console.log(res);
            _this.previewList = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // onItemSelect(item: any) {
    //   this.selectedUsers = []
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   for (var i = 0; i < this.selectedItems.length; i++) {
    //     this.selectedUsers.push(this.selectedItems[i].item_id);
    //   }
    //   console.log('selcetd users', this.selectedUsers);
    //    this.previewEmployeeList();
    // }
    // OnItemDeSelect(item: any) {
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // onSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   for (var i = 0; i < items.length; i++) {
    //     this.selectedUsers.push(items[i].item_id);
    //   }
    //   console.log("users", this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // onDeSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    AssignEmployeesComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    // Author: Saiprakash , Date: 15/05/19
    // Get fields for roles
    AssignEmployeesComponent.prototype.getFieldsForRoles = function () {
        var _this = this;
        this.companySettingsService.getFieldsForRoles()
            .subscribe(function (res) {
            console.log("12122123", res);
            _this.structureFields = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash , Date: 15/05/19
    // Get Structure Fields
    AssignEmployeesComponent.prototype.getStructureSubFields = function (fields, isChecked) {
        var _this = this;
        console.log(fields, isChecked);
        this.fieldsName = fields;
        this.checked = isChecked;
        if (isChecked == true) {
            this.eligibilityArray = [];
            this.structureFields.forEach(function (element) {
                if (element.name !== fields) {
                    element.isChecked = false;
                }
            });
            this.companySettingsService.getStuctureFields(fields, 0)
                .subscribe(function (res) {
                console.log(res, name);
                _this.StructureValues = res.default;
                _this.customValues = res.custom;
                console.log(_this.StructureValues);
            }, function (err) {
                console.log(err);
            });
        }
    };
    AssignEmployeesComponent.prototype.subFieldsStructure = function ($event, value) {
        var _this = this;
        console.log($event, value);
        // this.eligibilityArray = [];
        if ($event.checked == true) {
            this.eligibilityArray.push(value);
        }
        console.log(this.eligibilityArray, "123321");
        if ($event.checked == false) {
            var index = this.eligibilityArray.indexOf(value);
            if (index > -1) {
                this.eligibilityArray.splice(index, 1);
            }
            console.log(this.eligibilityArray);
        }
        var postdata = {
            field: this.fieldsName,
            chooseEmployeesForRoles: this.eligibilityArray
        };
        console.log(postdata);
        this.siteAccessRolesService.chooseEmployeesData(postdata)
            .subscribe(function (res) {
            console.log("emp data", res);
            _this.dropdownList = [];
            for (var i = 0; i < res.data.length; i++) {
                if ($event.checked == true)
                    _this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName });
            }
            console.log(_this.dropdownList, "employee dropdown");
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash , Date: 15/05/19
    // Assign Training To Employees
    AssignEmployeesComponent.prototype.assignTraining = function () {
        var _this = this;
        var finalSelectedUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalSelectedUsers.push(element._id);
        });
        var assignData = {
            companyId: this.companyId,
            trainingId: this.data,
            _ids: finalSelectedUsers
        };
        this.companySettingsService.assignTrainingToEmployees(assignData).subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.onNoClick();
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    AssignEmployeesComponent.prototype.onSelectUsers = function (selectSpecificPerson) {
        console.log("users", selectSpecificPerson);
        console.log('selcetd users', this.selectedUsers);
        this.previewEmployeeList();
    };
    AssignEmployeesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-assign-employees',
            template: __webpack_require__(/*! ./assign-employees.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.html"),
            styles: [__webpack_require__(/*! ./assign-employees.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.css")]
        }),
        __param(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_3__["SiteAccessRolesService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"], Object])
    ], AssignEmployeesComponent);
    return AssignEmployeesComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/training/training.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/training/training.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".training { margin: 30px 0 0;}\n.training .panel-default>.panel-heading { padding: 0 0 40px; background-color:transparent !important; border: none;}\n.training .panel-title { color: #3c3c3c; display: inline-block; font-size: 18px;}\n.training .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.training .panel-title>.small, .training .panel-title>.small>a, .training .panel-title>a, .training .panel-title>small, .training .panel-title>small>a { text-decoration:none;}\n.training .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.training .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.training .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.training .panel-group .panel-heading+.panel-collapse>.list-group, .training .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.training .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.training .panel-body { padding: 0 0 5px;}\n.training .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.training .panel-default { border-color:transparent;}\n.training .panel-group .panel+.panel { margin-top: 20px;}\n.training-cont { display: block;}\n.training-cont .form-group { position: relative; background:#fff; padding: 0; border-radius:5px;}\n.training-cont .form-control { height: auto; border: none; box-shadow: none; padding:12px; width:94%; font-size: 15px;}\n.training-cont .form-control:focus { box-shadow: none;}\n.training-cont span { position: absolute; top:15px; right:10px; cursor: pointer;}\n.training-cont span .material-icons { color: #8e8e8e; font-size:20px; font-weight: normal;}\n.training-cont .form-group.form-group1 { background: transparent; margin:-34px 0 0 30px;}\n.training-cont label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-cont .form-control1{ border: none; box-shadow: none; background: #fff;padding: 10px 12px; height: auto;}\n.training-cont .table{ background:#fff; margin: 10px 0 0;}\n.training-cont .table>tbody>tr>th, .training-cont .table>thead>tr>th {padding: 10px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.training-cont .table>tbody>tr>td, .training-cont .table>tfoot>tr>td, .training-cont .table>tfoot>tr>th, \n.training-cont .table>thead>tr>td {color: #484747;padding:20px 15px;font-size:15px;}\n.training-cont button.mat-menu-item a { color:#ccc;}\n.training-cont button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.training-cont .table>tbody>tr>th a, .training-cont .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.training-cont .table>tbody>tr>th a .fa, .training-cont .table>thead>tr>th a .fa { color: #6f6d6d;}\n.training-cont button { border: none; outline: none; background:transparent;}\n.training-cont .table>tbody>tr>th .dropdown-menu, .training-cont .table>thead>tr>th .dropdown-menu { top:50px; border:none; margin: 0; padding: 0;}\n.dropdown-menu>li>a { padding: 6px 20px; color:#ccc;}\n.dropdown-menu>li>a:hover {color:#fff !important; background: #099247;}\n.training-modal .modal-dialog { width: 50%;}\n.training-modal .modal-content { border-radius: 5px !important;}\n.training-modal .modal-header {padding: 15px 15px 20px;}\n.training-modal .modal-header h4 { padding: 0 0 0 30px;}\n.training-class { margin:10px 0 0 20px;}\n.training-class ul { display: block; border-bottom:#ccc 1px solid; padding: 0 0 30px; margin: 0 0 30px;}\n.training-class ul li { margin: 0 0 20px;}\n.training-class ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-class ul li .form-control{ border: none; box-shadow: none; background: #f8f8f8;padding: 10px 12px; height: auto;}\n.training-class ul li .form-control:focus{ box-shadow: none;}\n.training-class ul li.instructor { padding: 0 15px;}\n.training-class ul li.instructor .form-control { width:31%;}\n.training-class .cancel { color: #E85453; font-size: 15px; float: left; border: none; outline: none; background: transparent; margin: 6px 0 0;}\n.training-class .save { color: #fff; font-size: 15px; float: right;background: #099247; padding: 10px 40px; border-radius: 30px;border: none; outline: none;}\n.training-emp { background:#fff; padding: 20px 15px; border-radius: 5px; margin: 0 0 20px;}\n.training-emp label {color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-emp a.btn{ background:#787c7a; color:#fff; text-align: center; display: block; margin: 20px 0 0; padding: 8px 0; font-size: 16px;}\n.training-class ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #484747;\n  }\n.training-class ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color:#484747;\n  }\n.training-class ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.training-class ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.buttons{\n    margin: 20px 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    cursor: pointer;\n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background:#008f3d;font-size: 16px;\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n.errors {color:#e44a49; padding: 5px 0 0; display: block;}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/training/training.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/training/training.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"training\">\n\n    <div class=\"panel-group\" id=\"accordion3\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseSeven\">\n              Training Library\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseSeven\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"training-cont\">\n\n              <div class=\"form-group col-md-4\">\n                <form [formGroup]=\"trainingSearch\">\n                <input type=\"mail\" class=\"form-control\" placeholder=\"Search by name, title, department, etc..\" formControlName=\"search\">\n                <span><i class=\"material-icons\">search</i></span>\n              </form>\n              </div>\n\n              <div class=\"clearfix\"></div>\n\n\n              <table class=\"table\">\n                  <thead>\n                    <tr>\n                      <th></th>\n                      <th>Training Name</th>\n                      <th>Training ID\n                      </th>\n                      <th>Training Subject\n\n                          <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n\n                          <ul class=\"dropdown-menu\">\n                              <li><a (click)=\"filter('')\">Show all</a></li> \n                              <li *ngFor=\"let subjectField of trainingSubjectData\"><a (click)=\"filter(subjectField.name)\">{{subjectField.name}}</a></li>\n                             \n                          </ul>\n\n                      </th>\n                      <th>Training Type\n                          <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n                          <ul class=\"dropdown-menu\">\n                              <li><a (click)=\"filterType('')\">Show all</a></li>\n                              <li *ngFor=\"let subjectField of trainingTypeData\"><a (click)=\"filterType(subjectField.name)\">{{subjectField.name}}</a></li>\n                              \n                            </ul>\n                      </th>\n                      <th>Average Time to Complete\n\n                          <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n                          <ul class=\"dropdown-menu\">\n                              <li><a (click)=\"filterTime('')\">Show all</a></li>\n                              <li *ngFor=\"let subjectField of trainingAverageTime\"><a (click)=\"filterTime(subjectField.name)\">{{subjectField.name}}</a></li>\n                              \n                            </ul>\n\n                      </th>\n                      <th>Status\n\n                        <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n                          <ul class=\"dropdown-menu\">\n                              <li><a (click)=\"statusClick('')\">Show all</a></li>\n                              <li><a (click)=\"statusClick('1')\">Active</a></li>\n                              <li><a (click)=\"statusClick('0')\">Inactive</a></li>\n                              \n                            </ul>\n\n                      </th>\n                      <th>\n                        <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n                          <mat-icon>more_vert</mat-icon>\n                        </button>\n                        <mat-menu #menu=\"matMenu\">\n                          <button mat-menu-item *ngIf=\"trainingCheckIdPush.length == 0\">\n                            <a data-toggle=\"modal\" data-target=\"#myModal\" >Add</a>\n                          </button>\n\n                           <button mat-menu-item *ngIf=\"trainingCheckIdPush.length >= 1\">\n                            <a (click)=\"clickArchive()\">Archive</a>\n                          </button>\n\n                          <button mat-menu-item *ngIf=\"trainingCheckIdPush.length == 0\">\n                            <a (click)=\"clickUnArchive()\">Unarchive</a>\n                          </button>\n                        \n                          <button mat-menu-item *ngIf=\"trainingCheckIdPush.length == 1\">\n                              <a (click)=\"edit()\" >Edit</a>\n                              <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit></a>\n                          </button>\n                          \n                          <button mat-menu-item *ngIf=\"trainingCheckIdPush.length >= 1 \">\n                              <a (click)=\"deleteData()\">Delete</a>\n                          </button>\n                        </mat-menu>\n                      </th>\n                    </tr>\n                  </thead>\n                  <tbody> \n                    <tr *ngFor=\"let training of trainingData\">\n                      <td [ngClass]=\"{'zenwork-checked-border':training.isChecked}\"><mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"training.isChecked\" (change)=\"trainingCheckID($event,training._id)\"></mat-checkbox></td>\n                      <td>{{training.trainingName}}</td>\n                      <td>{{training.trainingId}}</td>\n                      <td>{{training.trainingSubject}}</td>\n                      <td>{{training.trainingType}}</td>\n                      <td>{{training.timeToComplete}}</td>\n                      \n                      <td *ngIf=\"training.status === 0\"> Inactive</td>\n                      <td *ngIf=\"training.status === 1\"> Active</td>\n                     \n                    </tr>\n                    \n                  </tbody>\n              </table>\n\n                <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 15,25, 100]\" (page)=\"pageEvents($event)\"></mat-paginator>\n\n            </div>\n\n\n          </div>\n        </div>\n\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseEight\">\n              Assign Training to Employees\n            </a>\n          </h4>\n\n\n        </div>\n\n        <div id=\"collapseEight\" class=\"panel-collapse collapse\">\n          <div class=\"panel-body\">\n\n            <div class=\"training-emp col-md-3 clearfix\">\n              <label>Select Training</label>\n              \n              <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Select training\">\n                  <mat-option *ngFor=\"let training of activeTrainings\" [value]=\"training.trainingName\" (click)=\"selectTraining(training._id)\">\n                    {{training.trainingName}}\n                  </mat-option>\n                 \n              </mat-select>\n\n              <a *ngIf=\"trainingId\" class=\"btn\" (click)=\"openDialog()\">Assign Employees</a>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n    \n</div>\n\n\n<div class=\"training-modal\">\n\n<!-- Modal -->\n<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n  \n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n         <h4>Training Classes</h4>\n        </div>\n        <div class=\"modal-body\">\n          \n          <div class=\"training-class\">\n            <form [formGroup]=\"trainingForm\" (ngSubmit)=\"addTraining()\">\n            <ul>\n\n              <li class=\"col-md-4\">\n                <label>Training Name *</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"Training name\" formControlName=\"trainingName\" maxlength=\"32\" (keypress)=\"alphabets($event)\"\n                >\n                <span *ngIf=\"trainingForm.get('trainingName').invalid && trainingForm.get('trainingName').touched\" class=\"errors\">Please provide training name</span>\n              </li>\n              <li class=\"col-md-5\">\n                  <label>Training ID Number *</label>\n                  <input class=\"form-control\" placeholder=\"Training ID \" formControlName=\"trainingId\" (keypress)=\"keyPress($event)\" maxlength=\"15\">\n                  <span *ngIf=\"trainingForm.get('trainingId').invalid && trainingForm.get('trainingId').touched\" class=\"errors\">Please provide Training ID number</span>\n                </li>\n              <div class=\"clearfix\"></div>  \n\n              <li class=\"instructor\">\n                  <label>Instructor Name</label>\n                  <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Instructor name\" formControlName=\"instructorName\">\n                      \n                      <mat-option *ngFor=\"let employess of employeeData\" [value]=\"employess._id\">{{employess.personal.name.preferredName}}</mat-option>\n\n                  </mat-select>\n                  \n\n              </li>\n\n              <li class=\"instructor\">\n                  <label>Training Subject</label>\n                  <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Training subject\" formControlName=\"trainingSubject\">\n                    \n                      <mat-option *ngFor=\"let subjectField of trainingSubjectData\" [value]=\"subjectField.name\">{{subjectField.name}}</mat-option>\n\n                  </mat-select>\n                  <span *ngIf=\"trainingForm.get('trainingSubject').invalid && trainingForm.get('trainingSubject').touched\" class=\"errors\">Please enter training subject</span>\n              </li>\n              \n              <li class=\"instructor\">\n                  <label>Training Type *</label>\n                  <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Training type \" formControlName=\"trainingType\">\n                     \n                      <mat-option *ngFor=\"let subjectField of trainingTypeData\" [value]=\"subjectField.name\">{{subjectField.name}}</mat-option>\n                      \n                  </mat-select>\n                  <span *ngIf=\"trainingForm.get('trainingType').invalid && trainingForm.get('trainingType').touched\" class=\"errors\">Please provide training type</span>\n              </li>\n              \n              <li class=\"instructor\">\n                  <label>Average Time to Complete *</label>\n                  <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Average time \" formControlName=\"timeToComplete\">\n                      \n                      <mat-option *ngFor=\"let subjectField of trainingAverageTime\" [value]=\"subjectField.name\">{{subjectField.name}}</mat-option>\n                                        \n                  </mat-select>\n                  <span *ngIf=\"trainingForm.get('timeToComplete').invalid && trainingForm.get('timeToComplete').touched\" class=\"errors\">Please provide Average time to complete</span>\n              </li>\n\n              <li class=\"instructor\">\n                <label>Status *</label>\n                  <mat-select class=\"form-control\" placeholder=\"Status\" formControlName=\"status\">\n                      <mat-option [value]=\"1\">Active</mat-option>\n                      <mat-option [value]=\"0\">Inactive</mat-option>\n                    </mat-select>\n                    <span *ngIf=\"trainingForm.get('status').invalid && trainingForm.get('status').touched\" class=\"errors\">Please provide status</span>\n              </li>\n\n              <li class=\"col-md-6\">\n                  <label>Training URL *</label>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Training URL\" formControlName=\"trainingUrl\">\n                  <span *ngIf=\"trainingForm.get('trainingUrl').invalid && trainingForm.get('trainingUrl').touched\" class=\"errors\">Please provide URL</span>\n              </li>\n              <div class=\"clearfix\"></div>\n\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <button type=\"submit\" class=\"cancel\" data-dismiss=\"modal\" (click)=\"cancelTraining()\">Cancel</button>\n            <button type=\"submit\" class=\"save\" >Submit</button>\n            <div class=\"clearfix\"></div>\n          </form>\n          </div>\n        \n        </div>\n        \n      </div>\n  \n    </div>\n  </div>\n\n</div>\n<div class=\"buttons\">\n  <ul class=\"list-unstyled\">\n   \n    <li class=\"list-items pull-right\">\n      <button class='save' type=\"submit\" (click)=\"saveandNext()\">\n        Save Changes\n      </button>\n    </li>\n\n    <div class=\"clearfix\"></div>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/company-setup/training/training.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/company-setup/training/training.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: TrainingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainingComponent", function() { return TrainingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _assign_employees_assign_employees_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./assign-employees/assign-employees.component */ "./src/app/admin-dashboard/company-settings/company-setup/training/assign-employees/assign-employees.component.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var TrainingComponent = /** @class */ (function () {
    function TrainingComponent(trainingService, activate, swalService, router, dialog, siteAccessRolesService) {
        this.trainingService = trainingService;
        this.activate = activate;
        this.swalService = swalService;
        this.router = router;
        this.dialog = dialog;
        this.siteAccessRolesService = siteAccessRolesService;
        this.trainingData = [];
        this.selectTrainingData = [];
        this.datatrain = false;
        this.employeeData = [];
        this.activeTrainings = [];
        this.trainingCheckDeleteID = [];
        this.trainingCheckIdPush = [];
        this.archiveLength = [];
        // MatPaginator Inputs
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [5, 10, 15, 25, 100];
        this.trainingDisable = false;
    }
    TrainingComponent.prototype.ngOnInit = function () {
        this.activeStatus = "";
        this.trainingSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("")
        });
        this.trainingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            trainingName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            instructorName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingSubject: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            timeToComplete: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingUrl: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
        this.pageNo = 1;
        this.perPage = 10;
        this.filterName = '';
        this.filterTypeName = '';
        this.filterTimeName = '';
        this.getAllData();
        this.getActiveTrainings();
        this.onChanges();
        this.employeeSearch();
        this.getSubjectData();
        this.companyID = localStorage.getItem("companyId");
        console.log("company ID", this.companyID);
        this.companyID = this.companyID.replace(/^"|"$/g, "");
        console.log("company IDDDD", this.companyID);
        this.userID = localStorage.getItem("employeeId");
        console.log("user iddddddddd", this.userID);
        this.userID = this.userID.replace(/^"|"$/g, "");
    };
    TrainingComponent.prototype.selectTraining = function (id) {
        this.trainingId = id;
    };
    TrainingComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
            console.log(_this.employeeData);
        }, function (err) {
            console.log(err);
        });
    };
    TrainingComponent.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(_assign_employees_assign_employees_component__WEBPACK_IMPORTED_MODULE_6__["AssignEmployeesComponent"], {
            width: '1200px',
            height: '800px',
            data: this.trainingId
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
        });
    };
    //Page Event
    TrainingComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllData();
    };
    //Search on filter
    TrainingComponent.prototype.onChanges = function () {
        var _this = this;
        this.trainingSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            _this.getAllData();
        });
    };
    // Training classes Submit
    TrainingComponent.prototype.addTraining = function () {
        var _this = this;
        console.log("Trainings", this.trainingForm.value);
        if (this.trainingForm.valid) {
            if (this.trainingDataEdit && this.trainingDataEdit._id) {
                var updateDate = this.trainingForm.value;
                updateDate._id = this.trainingDataEdit._id;
                this.trainingService.updateTrainingData(updateDate)
                    .subscribe(function (res) {
                    console.log("Updatinggg", res);
                    jQuery("#myModal").modal("hide");
                    if (res.status == true) {
                        _this.swalService.SweetAlertWithoutConfirmation("Updated", "Training updated successfully", "success");
                    }
                    _this.getAllData();
                    _this.getActiveTrainings();
                    _this.trainingForm.reset();
                    _this.trainingCheckIdPush = [];
                });
            }
            else {
                this.trainingService.addTraining(this.trainingForm.value)
                    .subscribe(function (response) {
                    console.log("Add Trainings", response);
                    jQuery("#myModal").modal("hide");
                    if (response.status == true) {
                        _this.swalService.SweetAlertWithoutConfirmation("Training", "Training created successfully", "success");
                        _this.getAllData();
                        _this.getActiveTrainings();
                    }
                    _this.trainingForm.reset();
                }, function (err) {
                    _this.swalService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
        }
        else {
            this.trainingForm.get('trainingName').markAsTouched();
            this.trainingForm.get('trainingId').markAsTouched();
            this.trainingForm.get('trainingSubject').markAsTouched();
            this.trainingForm.get('trainingType').markAsTouched();
            this.trainingForm.get('timeToComplete').markAsTouched();
            this.trainingForm.get('trainingUrl').markAsTouched();
            this.trainingForm.get('status').markAsTouched();
        }
    };
    //Filter Training Subject
    TrainingComponent.prototype.filter = function (event) {
        this.filterName = event;
        console.log("Eventttt", event);
        this.getAllData();
    };
    //Filter Training Type
    TrainingComponent.prototype.filterType = function (event) {
        this.filterTypeName = event;
        this.getAllData();
    };
    //Filter Training Time to Complete
    TrainingComponent.prototype.filterTime = function (event) {
        this.filterTimeName = event;
        this.getAllData();
    };
    // Get the Training Data
    TrainingComponent.prototype.getAllData = function () {
        var _this = this;
        console.log("Pagesss", this.pageNo, this.perPage);
        this.trainingService.viewTrainingData({
            searchQuery: this.trainingSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage,
            trainingSubject: this.filterName, trainingType: this.filterTypeName, timeToComplete: this.filterTimeName, status: this.activeStatus, archive: false
        })
            .subscribe(function (res) {
            console.log("Get the Training Data", res);
            _this.count = res.count;
            _this.trainingData = res.data;
            // this.trainingData.forEach(element => {
            //   if(element.archive == true){
            //     this.archiveLength.push(element)
            //   }
            // });
        });
    };
    TrainingComponent.prototype.getActiveTrainings = function () {
        var _this = this;
        this.trainingService.viewTrainingData({
            status: 1
        }).subscribe(function (res) {
            console.log(res);
            _this.activeTrainings = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    // Delete the Training Data
    TrainingComponent.prototype.deleteData = function () {
        var _this = this;
        console.log("Edit IDDDDD");
        this.selectTrainingData = this.trainingData.filter(function (trainingCheck) {
            return trainingCheck.isChecked;
        });
        if (this.selectTrainingData.length == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Select any Data", "", "");
        }
        else {
            var deleteData = {
                companyId: this.companyID,
                _ids: this.trainingCheckIdPush
            };
            this.trainingService.TrainingDelete(deleteData)
                .subscribe(function (res) {
                console.log("Deletee", res);
                _this.getAllData();
                _this.getActiveTrainings();
                _this.trainingCheckIdPush = [];
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Deleted successfully", "success");
                }
            });
        }
    };
    //Edit the Training Data
    TrainingComponent.prototype.edit = function () {
        var _this = this;
        console.log("Edit IDDDDD");
        this.selectTrainingData = this.trainingData.filter(function (trainingCheck) {
            return trainingCheck.isChecked;
        });
        console.log("AAAAAAAAA", this.selectTrainingData);
        if (this.selectTrainingData.length > 1) {
            this.swalService.SweetAlertWithoutConfirmation("Select only one data", "", "");
        }
        else if (this.selectTrainingData.length == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Select any one", "", "");
        }
        else {
            this.openEdit.nativeElement.click();
            this.trainingService.EditTrainingData(this.selectTrainingData[0]._id)
                .subscribe(function (res) {
                console.log("Edittt IDD", res);
                _this.trainingDataEdit = res.data;
                console.log("54545454545454545454", _this.trainingDataEdit);
                _this.trainingForm.patchValue(_this.trainingDataEdit);
                console.log("12345678", _this.trainingForm);
            });
        }
    };
    TrainingComponent.prototype.saveandNext = function () {
        this.router.navigate(["/admin/admin-dashboard/company-settings/company-setup/payroll"]);
    };
    // Author:Suresh M, Date:15-04-19
    // Get all data in Training dropdownn
    TrainingComponent.prototype.getSubjectData = function () {
        var _this = this;
        this.trainingService.getAllTraniningData()
            .subscribe(function (res) {
            console.log("Get All Subjects Dataa", res);
            _this.subjectFields = res.temp;
            _this.trainingSubjectData = _this.subjectFields['Training Subject'],
                _this.trainingTypeData = _this.subjectFields['Training Type'],
                _this.trainingAverageTime = _this.subjectFields['Training - Average Time to Complete'],
                console.log("Get All Subjects 21345768", _this.subjectFields);
        });
    };
    // Author:Suresh M, Date:10-08-19
    // cancelTraining
    TrainingComponent.prototype.cancelTraining = function () {
        this.trainingForm.reset();
    };
    // Author:Suresh M, Date:15-04-19
    // trainingCheckID
    TrainingComponent.prototype.trainingCheckID = function (event, id) {
        if (event.checked == true) {
            this.trainingCheckIdPush.push(id);
        }
        console.log("push iddddddd", this.trainingCheckIdPush);
        if (event.checked == false) {
            var index = this.trainingCheckIdPush.indexOf(id);
            if (index > -1) {
                this.trainingCheckIdPush.splice(index, 1);
            }
            console.log("uncheck idd", this.trainingCheckIdPush);
        }
    };
    // Author:Suresh M, Date:15-04-19
    // Alphabets
    TrainingComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32);
    };
    // Author:Suresh M, Date:15-04-19
    // keyPress
    TrainingComponent.prototype.keyPress = function (event) {
        var pattern = /^[A-Za-z0-9]+$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    TrainingComponent.prototype.statusClick = function (event) {
        console.log("activee", event);
        this.activeStatus = event;
        console.log("AAAAAAAAAA", this.activeStatus);
        this.getAllData();
    };
    // Author:Suresh M, Date:10-08-19
    // clickArchive
    TrainingComponent.prototype.clickArchive = function () {
        var _this = this;
        var archiveData = {
            companyId: this.companyID,
            _ids: this.trainingCheckIdPush,
            archive: true
        };
        console.log(archiveData);
        this.trainingService.archiveTraining(archiveData)
            .subscribe(function (res) {
            console.log("archive data", res);
            _this.getAllData();
            _this.trainingCheckIdPush = [];
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Suresh M, Date:10-08-19
    // clickUnArchive
    TrainingComponent.prototype.clickUnArchive = function () {
        var _this = this;
        this.trainingService.unArchiveTraining()
            .subscribe(function (res) {
            console.log("unarchive", res);
            _this.getAllData();
        }, function (err) {
            console.log(err);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        // MatPaginator Output
        )
    ], TrainingComponent.prototype, "openEdit", void 0);
    TrainingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-training',
            template: __webpack_require__(/*! ./training.component.html */ "./src/app/admin-dashboard/company-settings/company-setup/training/training.component.html"),
            styles: [__webpack_require__(/*! ./training.component.css */ "./src/app/admin-dashboard/company-settings/company-setup/training/training.component.css")]
        }),
        __metadata("design:paramtypes", [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_7__["SiteAccessRolesService"]])
    ], TrainingComponent);
    return TrainingComponent;
}());



/***/ }),

/***/ "./src/app/services/onboarding.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/onboarding.service.ts ***!
  \************************************************/
/*! exports provided: OnboardingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingService", function() { return OnboardingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OnboardingService = /** @class */ (function () {
    function OnboardingService(http) {
        this.http = http;
    }
    OnboardingService.prototype.getStandardOnboardingtemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.editDataSubit = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/editTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.addCustomOnboardTemplate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/addEditTemplate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllCustomTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getCustomTemplates")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getStandardTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getStandardTemplate")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.deleteCategory = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllJobDetails = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getFieldsForAddEmployee");
    };
    OnboardingService.prototype.selectOnboardTemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id);
    };
    OnboardingService.prototype.getModulesData = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplates", data);
    };
    OnboardingService.prototype.deleteTemplate = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTemplate", id);
    };
    OnboardingService.prototype.standardNewHireAllData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addEmployee", data);
    };
    OnboardingService.prototype.newHireHrSummaryReviewData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addUpdateCustomWizard", data);
    };
    OnboardingService.prototype.getAllNewHireHRData = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getAllCustomWizards");
    };
    OnboardingService.prototype.customNewHrDelete = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/deleteCustomWizards", id);
    };
    OnboardingService.prototype.getCustomNewHireWizard = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getCustomWizard/" + id);
    };
    OnboardingService.prototype.getSiteAccesRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id);
    };
    OnboardingService.prototype.getStructureFields = function (cID) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + cID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.preEmployeeIdGeneration = function (cId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getEmployeeId/" + cId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllWorkSchedule = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/listPositionSchedules", { params: data });
    };
    OnboardingService.prototype.standardScheduleData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getempschedule", data);
    };
    OnboardingService.prototype.createEmployeeData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/employeespecific", data);
    };
    OnboardingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OnboardingService);
    return OnboardingService;
}());



/***/ })

}]);
//# sourceMappingURL=company-setup-company-setup-module.js.map