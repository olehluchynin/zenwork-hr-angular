(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["workflow-approval-workflow-approval-module"],{

/***/ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.css":
/*!****************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.css ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-currentpage{\n\n}\n\n.zenwork-workflow-approval-wrapper{\n    padding: 35px 0 0; margin: 0 auto; float: none;\n}\n\n.zenwork-workflow-approval-wrapper ul>li{\n    display: inline-block;\n    margin-right: 10px;\n}\n\n.modal-header {\n    padding: 25px 35px;\n}\n\n/* side nav css*/\n\n.tabs-left{\n    margin-top:0;\n}\n\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n\n.tabs-left > .nav-tabs > li {\n    float: none; margin: 0 0 15px;\n  }\n\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px; margin: 0 20px;\n  }\n\n.tabs-left > .nav-tabs {\n    float: left;background:#f8f8f8; padding: 30px 0 0; height: 100vh;\n  }\n\n.tabs-left > .nav-tabs > li > a {\n    border-radius: 4px 0 0 4px;\n            font-size: 16px !important; color: #6f6969;\n  }\n\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee; background:#eeeeee; border-radius: 40px;\n  }\n\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n\n/*side nav css ends*/\n\n.caret{\n    margin-left: 50px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 8px 20px;\n    color: #525151;\n    font-size: 13px;\n    line-height: 13px;\n    border: 1px solid transparent;\n    margin: 0 20px 0 0;\n    vertical-align: middle;\n}\n\n.zenwork-currentpage small {\n  margin: 0 20px 0 0;\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.zenwork-inner-icon {\n  width: 20px;\n  height: auto;\n}\n\n.zenwork-currentpage em {\n  font-size: 18px;\n  line-height: 18px;\n  color: #484747;\n  font-style: normal;\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.mr-7{\n  margin-right: 7px;\n}\n\n.zenwork-green-btn:active, .zenwork-green-btn:hover { background: #696767 !important;color: #fff!important;\n  padding: 10px 20px!important;\n  border-radius: 4px!important; border:transparent 1px solid;\n}\n\n.zenwork-green-btn { background: #fff !important;color: #696767 !important; border:#eeeeee 1px solid;\n  padding: 10px 20px!important;\n  font-size: 15px!important;\n  border-radius: 4px!important;\n}\n\n.zenwork-request { margin:50px auto 0; float: none;}\n\n.zenwork-request ul { display: block; margin:50px 0 0;}\n\n.zenwork-request h4 { margin: 0; line-height: 20px;}\n\n.zenwork-request ul li {float: left; margin: 0 20px 0 0; padding: 0;}\n\n.zenwork-request ul li .selection { background:#f8f8f8; border-radius: 20px; padding: 0 30px; height:38px; line-height:38px; position: relative;}\n\n.zenwork-request ul li .selection .checkbox label { color:#484848; padding-left:0; font-size: 15px; line-height: 15px;}\n\n.zenwork-request ul li .selection .checkbox label::before { border-radius: 10px; width: 20px; height: 20px; top:-20px; left:-4px;}\n\n.zenwork-request ul li .selection .checkbox label::after { color:#fff; background: #008f3d;border-radius: 10px; width: 20px; height: 20px;\n     padding: 3px 0 0 4px; top: -20px; left:-4px;}\n\n.zenwork-request ul li .selection .checkbox{ margin: 0; padding: 0;}\n\n.zenwork-request .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:50px 0 0;}\n\n.zenwork-request .btn:hover {background: #008f3d; color:#fff;}\n\n.zenwork-approve { display: block; border-bottom:#d0d0d0 1px solid; padding: 0 0 10px;}\n\n.zenwork-approve > ul { display: block; margin: 0;}\n\n.zenwork-approve > ul > li { margin: 0;}\n\n.zenwork-manager { display: block;}\n\n.zenwork-manager > ul { display: block;}\n\n.zenwork-manager > ul > li { display: block; float:none; margin: 0 0 20px;}\n\n.zenwork-manager > ul > li > label { display: block; color:#696969; font-weight: normal; padding: 0 0 10px; font-size: 15px;}\n\n.workflow-manager { margin: 0 0 15px;}\n\n.workflow-manager .checkbox label::before {width: 20px; height: 20px; top: -1px;}\n\n.workflow-manager .checkbox label::after { color:#fff; background: #008f3d;width: 20px; height: 20px; padding: 3px 0 0 4px; border-radius:3px;}\n\n.workflow-manager .checkbox{ margin: 0; display: inline-block;}\n\n.workflow-manager .checkbox label { padding-left: 10px;}\n\n.workflow-select-drop select { background:#f8f8f8; border: none; width: 60%; height:40px; color:#525252; padding: 6px 10px;}\n\n.zenwork-Questions { margin:50px auto 0; float: none;}\n\n.zenwork-Questions h4 { margin: 0; line-height: 20px;}\n\n.status-block {border-bottom:#c3c3c3 1px solid; padding: 0 0 10px;display:inline-block; width: 100%;}\n\n.current-status { margin:50px 0 0;}\n\n.current-status .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 20px; background: #ffe5e3; border:none; font-size: 15px;\nborder-right: #c7c7c7 1px solid;}\n\n.new-status.current-status .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 20px; background: #c7e2d4; border:none; font-size: 15px;\n  border-right: #c7c7c7 1px solid;}\n\n.current-status .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 20px; background: #ffe5e3; border:none; font-size: 15px;\n  border-right: #c7c7c7 1px solid;}\n\n.current-status .table>thead>tr>th:nth-last-child(1) { border-right: none;}\n\n.select-drop { display: block;}\n\n.select-drop select { background:#fff; border: none; width:100%; height: 34px; color:#656464; padding: 6px 10px;}\n\n.current-status .table>tbody>tr>td, .current-status .table>tfoot>tr>td, .current-status .table>thead>tr>td{ padding: 10px 15px; background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#696868;border-right: #c7c7c7 1px solid;}\n\n.current-status .table>tbody>tr>td:nth-last-child(1) { border-right: none;}\n\n.date { display: block; background:#fff;height: 30px; line-height: 30px; width: 150px;}\n\n.date span { display: inline-block; vertical-align: middle; cursor: pointer;    border-left: #bdbdbd 1px solid;padding: 0 0 0 10px;}\n\n.date span .fa { color: #008f3d;\n    font-size:18px;\n    line-height:18px;\n    width: 18px;\n    text-align: center;}\n\n.date .form-control { display: inline-block;vertical-align: middle; width: 70%; border: none; background:transparent; box-shadow: none; padding: 0 0 0 10px;}\n\n.zenwork-Questions .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:50px 0 0;}\n\n.zenwork-Questions .btn:hover {background: #008f3d; color:#fff;}\n\n.additional-block { margin: 20px 0 0;}\n\n.additional-block ul { display: block;}\n\n.additional-block ul li { display: block; margin: 0 0 20px;}\n\n.additional-block ul li label { display: block; color:#696969; font-weight: normal; padding: 0 0 10px; font-size: 15px;}\n\n.additional-drop { display: block;}\n\n.additional-drop select { background:#f8f8f8; border: none; width:25%; height: 34px; color:#656464; padding: 6px 10px;}\n\n.additional-block ul li .form-control { min-height: 80px;background:#f8f8f8; padding:20px; resize:none; border: none; box-shadow: none; line-height: 20px;}\n\n.zenwork-review { margin: 50px auto 0; float: none;}\n\n.zenwork-review h4 { margin: 0 0 15px; line-height: 20px;color: #6f6969;}\n\n.zenwork-review .form-control {background:#f8f8f8;border: none; box-shadow: none; line-height:38px; height:38px; color:#000; margin: 0 0 25px; padding: 0 10px;\nwidth:28%;}\n\n.review-approve { display: block; margin: 0 0 30px;}\n\n.review-approve .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 20px; background: #eef7ff; border:none; font-size: 15px;\n  width: 50%;}\n\n.review-approve .table>tbody>tr>td, .review-approve .table>tfoot>tr>td, .review-approve .table>thead>tr>td{ padding:20px 15px; background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#696868;}\n\n.zenwork-review .current-status { margin:0;}\n\n.zenwork-review .current-status .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 20px; background: #ffe5e3; border:none; font-size: 15px;\nborder-right: #c7c7c7 1px solid;}\n\n.zenwork-review .current-status .table>tbody>tr>td, .zenwork-review .current-status .table>tfoot>tr>td, .zenwork-review .current-status .table>thead>tr>td{ padding: 10px 15px; background:#ffe5e3;border: none;vertical-align:middle; border-top:#c3c3c3 1px solid;border-bottom:#c3c3c3 1px solid; color:#484848;border-right: #c7c7c7 1px solid;}\n\n.zenwork-review .current-status .table>thead>tr>th:nth-last-child(1) { border-right: none;}\n\n.zenwork-review .current-status .table>tbody>tr>td:nth-last-child(1) { border-right: none;}\n\n.zenwork-review .summary-status.current-status .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 20px; background: #cce9d8; border:none; font-size: 15px;border-right: #c7c7c7 1px solid;}\n\n.zenwork-review .summary-status.current-status .table>tbody>tr>td, .zenwork-review .current-status .table>tfoot>tr>td, .zenwork-review .current-status .table>thead>tr>td{ padding: 10px 15px; background:#cce9d8;border: none;vertical-align:middle; border-top:#c3c3c3 1px solid;border-bottom:#c3c3c3 1px solid; color:#484848;border-right: #c7c7c7 1px solid;}\n\n.zenwork-review .summary-status.current-status .table>thead>tr>th:nth-last-child(1) { border-right: none;}\n\n.zenwork-review .summary-status.current-status .table>tbody>tr>td:nth-last-child(1) { border-right: none;}\n\n.zenwork-review .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:50px 0 0;}\n\n.zenwork-review .btn:hover {background: #008f3d; color:#fff;}\n\n.date input { border:none; width: 75%; display: inline-block; float: left; padding: 0 0 0 10px;}\n\n.date button { width: 25%; display: inline-block; float: right; background:transparent; border: none; outline: none; padding: 0 10px 0 0;}\n\n.zenwork-old-new-values { margin: 0 auto;}\n\n/*-- Author:Suresh M, Date:22-04-19  --*/\n\n/*-- Popup css code  --*/\n\n.work-flow { float: none; margin:40px auto 0;}\n\n.work-flow > ul { margin:50px 0 40px; width: 100%; display: inline-block;}\n\n.work-flow > ul > li {position: relative;}\n\n.work-flow > ul > li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n\n.work-flow > ul > li h2 {background: #f8f8f8;\n  border-radius: 20px;text-align:center;\n  padding: 12px 0;\n  font-size: 16px;\n  line-height: 16px; color: #484848;}\n\n.work-flow-lft { margin:30px 0;}\n\n.work-flow-lft ul { background:#f8f8f8; padding:20px 20px 0; margin: 30px 0 0;}\n\n.work-flow-lft ul li { border-bottom:#3a3a3a dashed 1px; margin: 0 0 15px; padding: 0 0 15px;}\n\n.work-flow-lft ul li h6{ margin: 0 0 5px; font-size: 15px;}\n\n.work-flow-lft ul li.border{ border: none;}\n\n.work-flow-lft ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%; background:#fff;}\n\n.work-flow-lft ul li .form-control:focus{ box-shadow: none;}\n\n.work-flow-lft ul li span { vertical-align:middle; display: inline-block; padding: 0 25px 0 0;}\n\n.work-flow-lft ul li span .fa{ color:#c1c1c1; font-size:20px; line-height:20px; vertical-align:text-bottom;}\n\n.work-flow .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n\n.work-flow .btn.cancel { color:#e44a49; background:transparent; padding:0 0 0 30px;font-size: 16px;}\n\n.example-box {\n  padding:10px;\n  color: rgba(0, 0, 0, 0.87);\n  display: inline-block;\n  width: 100%;\n  flex-direction: row;\n  align-items: center;\n  justify-content: space-between;\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 14px;\n  vertical-align: middle;\n  margin: 0 0 15px;\n}\n\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n              0 8px 10px 1px rgba(0, 0, 0, 0.14),\n              0 3px 14px 2px rgba(0, 0, 0, 0.12);\n}\n\n.cdk-drag-animating {\n  transition: -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n\n.example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\n  transition: -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n\n.title-level{\n  padding: 25px 0 0 64px;\n}\n\n.levels ul .list-item{\n  padding: 0 0 0 65px;\n  display: inline-block;\n}\n\n.buttons-levels{\n  padding: 30px 0 0 0;\n}\n\n.buttons-levels .list-item .btn{\nborder-radius: 20px;\nheight: 40px;\nline-height: 38px;\nfont-size: 16px;\npadding: 0 24px;\nbackground: #008f3d;\ncolor: #fff;\nmargin: 0 20px 0 0;\n}\n\n.error{\n  font-size:12px !important;\n  color: #f44336 !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n      <span class=\"green mr-7\">\n        <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n      </span>Back\n    </button>\n    <small> <img src=\"../../../assets/images/company-settings/workflow-approval/workflow-approval-green.png\"\n        class=\"zenwork-inner-icon\" alt=\"Company-settings icon\"></small>\n    <em>Workflow Approval</em>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n\n\n<div class=\"zenwork-workflow-approval-wrapper col-md-11\">\n  <ul class=\"list-unstyled\">\n    <li>\n      <button class=\"btn zenwork-green-btn\" (click)=\"workflowApprovalModal(demographicWorkflowTemplate)\">\n        Demographic Workflow\n      </button>\n    </li>\n    <li>\n      <button class=\"btn zenwork-green-btn zenwork-default-background zenwork-color-black\"\n        (click)=\"workflowApprovalModal(jobChangeWorkflowTemplate)\">\n        Job Change Workflow\n      </button>\n    </li>\n    <li>\n      <button class=\"btn zenwork-green-btn zenwork-default-background zenwork-color-black\"\n        (click)=\"workflowApprovalModal(compensationChangeWorkflowTemplate)\">\n        Compensation Change Workflow\n      </button>\n    </li>\n  </ul>\n</div>\n\n<div class=\"levels\">\n  <ul class=\"list-unstyled\">\n    <h4 class=\"title-level\">Declare the levels</h4>\n    <li class=\"list-item col-md-2\">\n      <label>Manager</label>\n      <input type=\"text\" type=\"text\" (keypress)=\"keyPress($event)\" maxlength=\"1\" [(ngModel)]=\"mangerLevels\" class=\"form-control\" \n      name=\"mangerLevels1\" #mangerLevels1=\"ngModel\" required>\n      <p *ngIf=\"!mangerLevels && mangerLevels1.touched || (!mangerLevels && isValidManagerlevel)\"\n      class=\"error\"> Please fill this field</p>\n    </li>\n    <li class=\"list-item col-md-2\">\n      <label>HR</label>\n      <input type=\"text\" type=\"text\" (keypress)=\"keyPress($event)\" maxlength=\"1\" [(ngModel)]=\"hrLevels\" class=\"form-control\" \n      name=\"hrLevels1\" #hrLevels1=\"ngModel\" required>\n      <p *ngIf=\"!hrLevels && hrLevels1.touched || (!hrLevels && isValidHrlevel)\"\n      class=\"error\"> Please fill this field</p>\n    </li>\n    <div class=\"clearfix\"></div>\n  </ul>\n\n  <ul class=\"buttons-levels list-unstyled\">\n    <li class=\"list-item col-md-2\">\n      <button class=\"btn save align-center\" (click)=\"levelnumberSubmitManager()\">Submit</button>\n    </li>\n    <li class=\"list-item col-md-2\">\n      <button class=\"btn save text-center\" (click)=\"levelnumberSubmitHr()\">Submit</button>\n\n    </li>\n  </ul>\n</div>\n\n<div>\n\n\n  <!-- Author:Suresh M, Date:22-04-19  -->\n  <!-- Popup Content -->\n  <ng-template #demographicWorkflowTemplate class=\"demographic-popup\">\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title pull-left\">Demographic Workflow</h4>\n      <div>\n\n      </div>\n      <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n\n    <div class=\"modal-body\">\n\n      <div class=\"work-flow col-md-11\">\n        <h4>Who can request the change?</h4>\n\n        <ul>\n          <li class=\"col-md-2\">\n            <h2>Employee</h2>\n          </li>\n          \n        </ul>\n        <div class=\"clearfix\"></div>\n\n        <h4>Who can Approve?</h4>\n\n        <div class=\"work-flow-lft col-md-5\">\n\n          <h4>Select a role for Approval. More than one role can be selected</h4>\n\n          <ul class=\"col-md-7\">\n            <li>\n              <h6>Roles</h6>\n            </li>\n            <li *ngIf=\"!updateWorkflow\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"managerName\" (ngModelChange)=\"empManagerPerson($event)\"\n                placeholder=\"reports To\">\n                <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n                </mat-option>\n              </mat-select>\n            </li>\n            <li *ngIf=\"updateWorkflow\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"managerName\"\n                (ngModelChange)=\"empManagerPersonUpdate($event)\" placeholder=\"reports To\">\n                <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n                </mat-option>\n              </mat-select>\n            </li>\n\n            <li *ngIf=\"!updateWorkflow\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"hrName\" (ngModelChange)=\"empHrPerson($event)\"\n                placeholder=\"HR\">\n                <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\"> {{data.levelName}}\n                </mat-option>\n              </mat-select>\n            </li>\n\n            <li *ngIf=\"updateWorkflow\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"hrName\" (ngModelChange)=\"empHrPersonUpdate($event)\"\n                placeholder=\"HR\">\n                <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\"> {{data.levelName}}\n                </mat-option>\n              </mat-select>\n            </li>\n\n            <li *ngIf=\"!updateWorkflow\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"splRole\" (ngModelChange)=\"empSplRole($event)\"\n                placeholder=\"special Role\">\n                <mat-option *ngFor=\"let data of splRoleData\" [value]=\"data._id\"> {{data.name}}\n                </mat-option>\n              </mat-select>\n            </li>\n\n            <li *ngIf=\"updateWorkflow\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"splRole\" (ngModelChange)=\"empSplRoleUpdate($event)\"\n                placeholder=\"special Role\">\n                <mat-option *ngFor=\"let data of splRoleData\" [value]=\"data._id\"> {{data.name}}\n                </mat-option>\n              </mat-select>\n            </li>\n\n            <li *ngIf=\"!updateWorkflow\" class=\"border\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"empName\" (ngModelChange)=\"empSpecialPerson($event)\"\n                placeholder=\"SpecificPerson\">\n                <mat-option *ngFor=\"let data of employeeData\" [value]=\"data._id\">{{data.personal.name.preferredName}}\n                </mat-option>\n              </mat-select>\n            </li>\n            <li *ngIf=\"updateWorkflow\" class=\"border\">\n              <mat-select class=\"form-control\" [(ngModel)]=\"empName\" (ngModelChange)=\"empSpecialPersonUpdate($event)\"\n                placeholder=\"SpecificPerson\">\n                <mat-option *ngFor=\"let data of employeeData\" [value]=\"data._id\">{{data.personal.name.preferredName}}\n                </mat-option>\n              </mat-select>\n            </li>\n          </ul>\n          <div class=\"clearfix\"></div>\n\n        </div>\n\n\n        <div class=\"work-flow-lft col-md-6 pull-right\">\n          <h4>Move the role up or down to the desired approval priority ranking.</h4>\n          <ul class=\"col-md-7\">\n\n            <li class=\"border\">\n              <h6>Approval Priority</h6>\n            </li>\n\n            <li cdkDropList class=\"example-list border\" (cdkDropListDropped)=\"drop($event)\">\n              <div class=\"example-box\" *ngFor=\"let data of priorityUsers\" cdkDrag><span><i class=\"fa fa-list-ul\"\n                    aria-hidden=\"true\"></i></span>{{data.name}}</div>\n            </li>\n\n          </ul>\n          <div class=\"clearfix\"></div>\n        </div>\n        <div class=\"clearfix\"></div>\n\n        <hr>\n\n        <button class=\"btn pull-left cancel\" (click)=\"modalRef.hide()\">Cancel</button>\n        <button *ngIf=\"!updateWorkflow\" class=\"btn pull-right save\" (click)=\"workflowSubmit()\">Submit</button>\n\n        <button *ngIf=\"updateWorkflow\" class=\"btn pull-right save\" (click)=\"updateDemographicWorkflow()\">update</button>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n    </div>\n  </ng-template>\n\n</div>\n\n<ng-template #jobChangeWorkflowTemplate class=\"demographic-popup\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">Job Change Workflow</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n\n    <div class=\"work-flow col-md-11\">\n      <h4>Who can request the change?</h4>\n\n      <ul>\n        <!-- <li class=\"col-md-2\">\n          <mat-checkbox>Employee</mat-checkbox>\n        </li> -->\n\n        <li class=\"col-md-2\">\n          <h2>Manager</h2>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n\n      <h4>Who can Approve?</h4>\n\n      <div class=\"work-flow-lft col-md-5\">\n\n        <h4>Select a role for Approval. More than one role can be selected</h4>\n\n        <ul class=\"col-md-7\">\n          <li>\n            <h6>Roles</h6>\n          </li>\n          <li *ngIf=\"!updateJobWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobManagerName\" (ngModelChange)=\"empJobManagerPerson($event)\"\n              placeholder=\"Reports To\">\n              <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"updateJobWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobManagerName\"\n              (ngModelChange)=\"empJobManagerPersonUpdate($event)\" placeholder=\"Reports To\">\n              <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"!updateJobWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobHrName\" (ngModelChange)=\"empJobHrPerson($event)\"\n              placeholder=\"HR\">\n              <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\"> {{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"updateJobWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobHrName\" (ngModelChange)=\"empJobHrPersonUpdate($event)\"\n              placeholder=\"HR\">\n              <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\"> {{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"!updateJobWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobSplRole\" (ngModelChange)=\"jobSpecialRole($event)\"\n              placeholder=\"special Role create\">\n              <mat-option *ngFor=\"let data of splRoleData\" [value]=\"data._id\"> {{data.name}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"updateJobWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobSplRole\" (ngModelChange)=\"jobSpeciaRoleUpdate($event)\"\n              placeholder=\"special Role update\">\n              <mat-option *ngFor=\"let data of splRoleData\" [value]=\"data._id\"> {{data.name}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"!updateJobWorkflow\" class=\"border\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobEmpName\" (ngModelChange)=\"empJobSpecialPerson($event)\"\n              placeholder=\"SpecificPerson\">\n              <mat-option *ngFor=\"let data of employeeData\" [value]=\"data._id\">{{data.personal.name.preferredName}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"updateJobWorkflow\" class=\"border\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"jobEmpName\"\n              (ngModelChange)=\"empJobSpecialPersonUpdate($event)\" placeholder=\"SpecificPerson\">\n              <mat-option *ngFor=\"let data of employeeData\" [value]=\"data._id\">{{data.personal.name.preferredName}}\n              </mat-option>\n            </mat-select>\n          </li>\n        </ul>\n\n\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n\n      <div class=\"work-flow-lft col-md-6 pull-right\">\n        <h4>Move the role up or down to the desired approval priority ranking.</h4>\n        <ul class=\"col-md-7\">\n\n          <li class=\"border\">\n            <h6>Approval Priority</h6>\n          </li>\n\n          <li cdkDropList class=\"example-list border\" (cdkDropListDropped)=\"dropForJob($event)\">\n            <div class=\"example-box\" *ngFor=\"let data of priorityUsersJob\" cdkDrag><span><i class=\"fa fa-list-ul\"\n                  aria-hidden=\"true\"></i></span>{{data.name}}</div>\n          </li>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"clearfix\"></div>\n\n      <hr>\n\n      <button class=\"btn pull-left cancel\" (click)=\"modalRef.hide()\">Cancel</button>\n      <button *ngIf=\"!updateJobWorkflow\" class=\"btn pull-right save\" (click)=\"jobWorkflowSubmit()\">Submit</button>\n\n      <button *ngIf=\"updateJobWorkflow\" class=\"btn pull-right save\" (click)=\"jobUpdateWorkflow()\">update</button>\n      <div class=\"clearfix\"></div>\n      <div class=\"clearfix\"></div>\n\n    </div>\n\n  </div>\n</ng-template>\n\n<ng-template #compensationChangeWorkflowTemplate class=\"demographic-popup\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">Compensation Change Workflow</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n\n  <div class=\"modal-body\">\n\n    <div class=\"work-flow col-md-11\">\n      <h4>Who can request the change?</h4>\n\n      <ul>\n        <!-- <li class=\"col-md-2\">\n          <mat-checkbox>Employee</mat-checkbox>\n        </li> -->\n\n        <li class=\"col-md-2\">\n          <h2>Manager</h2>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n\n      <h4>Who can Approve?</h4>\n\n      <div class=\"work-flow-lft col-md-5\">\n\n        <h4>Select a role for Approval. More than one role can be selected</h4>\n\n        <ul class=\"col-md-7\">\n          <li>\n            <h6>Roles</h6>\n          </li>\n          <li *ngIf=\"!updateCompensationWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationManagerName\"\n              (ngModelChange)=\"empCompensationManagerPerson($event)\" placeholder=\"Manager\">\n              <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"updateCompensationWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationManagerName\"\n              (ngModelChange)=\"empCompensationManagerPersonUpdate($event)\" placeholder=\"Manager\">\n              <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"!updateCompensationWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationHrName\"\n              (ngModelChange)=\"empCompensationHrPerson($event)\" placeholder=\"HR\">\n              <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\"> {{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"updateCompensationWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationHrName\"\n              (ngModelChange)=\"empCompensationHrPersonUpdate($event)\" placeholder=\"HR\">\n              <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\"> {{data.levelName}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"!updateCompensationWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationSplRole\"\n              (ngModelChange)=\"compensationSpecialRole($event)\" placeholder=\"special Role create\">\n              <mat-option *ngFor=\"let data of splRoleData\" [value]=\"data._id\"> {{data.name}}\n              </mat-option>\n            </mat-select>\n          </li>\n\n          <li *ngIf=\"updateCompensationWorkflow\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationSplRole\"\n              (ngModelChange)=\"compensationSpeciaRoleUpdate($event)\" placeholder=\"special Role update\">\n              <mat-option *ngFor=\"let data of splRoleData\" [value]=\"data._id\"> {{data.name}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"!updateCompensationWorkflow\" class=\"border\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationEmpName\"\n              (ngModelChange)=\"empCompensationSpecialPerson($event)\" placeholder=\"SpecificPerson\">\n              <mat-option *ngFor=\"let data of employeeData\" [value]=\"data._id\">{{data.personal.name.preferredName}}\n              </mat-option>\n            </mat-select>\n          </li>\n          <li *ngIf=\"updateCompensationWorkflow\" class=\"border\">\n            <mat-select class=\"form-control\" [(ngModel)]=\"compensationEmpName\"\n              (ngModelChange)=\"empCompensationSpecialPersonUpdate($event)\" placeholder=\"SpecificPerson\">\n              <mat-option *ngFor=\"let data of employeeData\" [value]=\"data._id\">{{data.personal.name.preferredName}}\n              </mat-option>\n            </mat-select>\n          </li>\n        </ul>\n\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n\n      <div class=\"work-flow-lft col-md-6 pull-right\">\n        <h4>Move the role up or down to the desired approval priority ranking.</h4>\n        <ul class=\"col-md-7\">\n\n          <li class=\"border\">\n            <h6>Approval Priority</h6>\n          </li>\n\n          <li cdkDropList class=\"example-list border\" (cdkDropListDropped)=\"dropCompensation($event)\">\n            <div class=\"example-box\" *ngFor=\"let data of priorityUsersCompensation\" cdkDrag><span><i\n                  class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></span>{{data.name}}</div>\n          </li>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"clearfix\"></div>\n\n      <hr>\n\n      <button class=\"btn pull-left cancel\" (click)=\"modalRef.hide()\">Cancel</button>\n      <button *ngIf=\"!updateCompensationWorkflow\" class=\"btn pull-right save\"\n        (click)=\"compensationWorkflowSubmit()\">Submit</button>\n\n      <button *ngIf=\"updateCompensationWorkflow\" class=\"btn pull-right save\"\n        (click)=\"compensationUpdateWorkflow()\">update</button>\n      <div class=\"clearfix\"></div>\n\n    </div>\n\n  </div>\n\n</ng-template>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: WorkflowApprovalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkflowApprovalComponent", function() { return WorkflowApprovalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_workflow_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/workflow.service */ "./src/app/services/workflow.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WorkflowApprovalComponent = /** @class */ (function () {
    function WorkflowApprovalComponent(modalService, router, siteAccessRolesService, workflowService, swalAlertService) {
        this.modalService = modalService;
        this.router = router;
        this.siteAccessRolesService = siteAccessRolesService;
        this.workflowService = workflowService;
        this.swalAlertService = swalAlertService;
        this.isValidHrlevel = false;
        this.isValidManagerlevel = false;
        this.employeeData = [];
        this.priorityUsers = [];
        this.priorityUsersJob = [];
        this.priorityUsersCompensation = [];
        this.updateWorkflow = false;
        this.updateJobWorkflow = false;
        this.updateCompensationWorkflow = false;
        this.persons = ["person1", "person2", "person3"];
    }
    // HrEmployees: any;
    // managerEmployees: any;
    WorkflowApprovalComponent.prototype.ngOnInit = function () {
        this.Cid = JSON.parse(localStorage.getItem('companyId'));
        this.employeeSearch();
        this.getAllBaseRoles();
        // this.getHrRoles()
        this.getManagerRoles();
        this.gettingLevelsHr();
        this.gettingLevelsManager();
        this.sysAdminRoles();
        this.priorities = ["First", "Second", "Third", "Fourth"];
        // this.employeeSearch()
        this.getDemographicWorkFlow(this.Cid);
        this.getJobWorkFlow(this.Cid);
        this.getCompensationWorkFlow(this.Cid);
    };
    WorkflowApprovalComponent.prototype.gettingLevelsHr = function () {
        var _this = this;
        var cmpnyId = JSON.parse(localStorage.getItem('companyId'));
        this.workflowService.gettingofHrlevels(cmpnyId)
            .subscribe(function (res) {
            console.log("roles", res);
            _this.hrData = res.data;
            _this.hrLevels = _this.hrData.length;
        }, function (err) {
            console.log(err);
        });
    };
    WorkflowApprovalComponent.prototype.gettingLevelsManager = function () {
        var _this = this;
        var cmpnyId = JSON.parse(localStorage.getItem('companyId'));
        this.workflowService.gettingofManagerlevels(cmpnyId)
            .subscribe(function (res) {
            console.log("roles", res);
            _this.managerData = res.data;
            _this.mangerLevels = _this.managerData.length;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get baseroles
    author : vipin reddy */
    WorkflowApprovalComponent.prototype.getAllBaseRoles = function () {
        var _this = this;
        this.siteAccessRolesService.getBaseRoles()
            .subscribe(function (res) {
            console.log("roles", res);
            _this.resRolesObj = res.data;
            for (var i = 0; i < _this.resRolesObj.length; i++) {
                console.log(_this.resRolesObj, "rolesssss");
                if (_this.resRolesObj[i].name == 'Manager') {
                    _this.managerId = _this.resRolesObj[i]._id;
                    console.log(_this.managerId);
                    // this.getManagerRoles();
                }
                if (_this.resRolesObj[i].name == "System Admin") {
                    _this.sysAdminId = _this.resRolesObj[i]._id;
                    // this.getHrRoles();
                }
                if (_this.resRolesObj[i].name == "Employee") {
                    _this.employeeBaseId = _this.resRolesObj[i]._id;
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get only Hr employees
   author : vipin reddy */
    // getHrRoles() {
    //   this.HrId = "5cbe98f8561562212689f748"
    //   this.workflowService.getHrRoleEmployees(this.Cid, this.HrId)
    //     .subscribe((res: any) => {
    //       console.log("HR roles", res);
    //       this.hrData = res.data;
    //     },
    //       (err) => {
    //         console.log(err);
    //       })
    // }
    /* Description: get only managers employees
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.getManagerRoles = function () {
        var _this = this;
        console.log("vippip");
        this.managerId = "5cbe98a3561562212689f747";
        this.workflowService.getManagerEmployees(this.Cid, this.managerId)
            .subscribe(function (res) {
            console.log("manager roles", res);
            _this.managerData1 = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    WorkflowApprovalComponent.prototype.sysAdminRoles = function () {
        var _this = this;
        console.log("vippip", this.sysAdminId);
        this.sysAdminId = "5cbe9922561562212689f74a",
            this.workflowService.getManagerEmployees(this.Cid, this.sysAdminId)
                .subscribe(function (res) {
                console.log("sys admin roles", res);
                _this.splRoleData = res.data;
                _this.managerData1.forEach(function (element) {
                    _this.splRoleData.push(element);
                });
                console.log(_this.splRoleData, 'splroledata');
            }, function (err) {
                console.log(err);
            });
    };
    /* Description: getting employees list
      author : vipin reddy */
    WorkflowApprovalComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get workflow details
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.getDemographicWorkFlow = function (Cid) {
        var _this = this;
        this.employeeSearch();
        this.workflowService.getWorkflowservice(Cid, "demographic")
            .subscribe(function (res) {
            console.log("get workflow", res);
            console.log(_this.priorityUsers, 'AAAARRRAAYY');
            if (res.data.length >= 1) {
                _this.workflowId = res.data[0]._id;
                _this.updateWorkflow = true;
                if (res.data[0].hr) {
                    _this.hrName = res.data[0].hr.levelId;
                    console.log(_this.hrData);
                    var index = _this.hrData.findIndex(function (userData) { return userData._id === res.data[0].hr.levelId; });
                    console.log(index);
                    var user = _this.hrData[index];
                    console.log(user);
                    var name = user.levelName;
                    var _id = user._id;
                    // user['userType'] = 'hr';
                    _this.priorityUsers[(res.data[0].hr.priority) - 1] = { name: name, _id: _id, userType: 'hr' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                    console.log("res get of workflow", user);
                }
                if (res.data[0].reportsTo) {
                    _this.managerName = res.data[0].reportsTo.levelId;
                    var indexManager = _this.managerData.findIndex(function (userData) { return userData._id === res.data[0].reportsTo.levelId; });
                    console.log(indexManager, _this.managerData);
                    var userManager = _this.managerData[indexManager];
                    // userManager['userType'] = 'manager';
                    var name = userManager.levelName;
                    var _id = userManager._id;
                    _this.priorityUsers[(res.data[0].reportsTo.priority) - 1] = { name: name, _id: _id, userType: 'manager' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                    console.log(_this.priorityUsers);
                    console.log("res get of workflow", userManager, res.data[0].specialPerson);
                }
                if (res.data[0].specialPerson) {
                    _this.empName = res.data[0].specialPerson.personId;
                    console.log(_this.employeeData, res.data[0].specialPerson.personId, "77777777777");
                    console.log(_this.priorityUsers);
                    var indexSpecialPerson1 = _this.employeeData.findIndex(function (userData) { return userData._id === res.data[0].specialPerson.personId; });
                    console.log("indexx value", indexSpecialPerson1);
                    var userSpecialManager = _this.employeeData[indexSpecialPerson1];
                    console.log(userSpecialManager, "11111");
                    console.log(userSpecialManager, "vivpin spl manaager");
                    var name = userSpecialManager.personal.name.preferredName;
                    var _id = userSpecialManager._id;
                    console.log("index3", res.data[0].specialPerson.priority);
                    _this.priorityUsers[(res.data[0].specialPerson.priority) - 1] = { name: name, _id: _id, userType: 'specialPerson' };
                    console.log(_this.priorityUsers[3], "users");
                    console.log("res get of workflow", { name: name, _id: _id });
                    console.log(_this.priorityUsers);
                }
                if (res.data[0].specialRole) {
                    _this.splRole = res.data[0].specialRole.roleId;
                    console.log(_this.splRoleData);
                    var indexSpecialRole = _this.splRoleData.findIndex(function (userData) { return userData._id === res.data[0].specialRole.roleId; });
                    console.log("indexx value", indexSpecialRole);
                    var userSpecialManagerole = _this.splRoleData[indexSpecialRole];
                    console.log(userSpecialManagerole, "11111");
                    var name = userSpecialManagerole.name;
                    var _id = userSpecialManagerole._id;
                    _this.priorityUsers[(res.data[0].specialRole.priority) - 1] = { name: name, _id: _id, userType: 'specialRole' };
                    console.log("res get of workflow", { name: name, _id: _id });
                    console.log(_this.priorityUsers);
                }
            }
        }, function (err) {
            console.log("error", err);
        });
    };
    /* Description: selcted person comes to drag and drop section for specialPerson employee only when create
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empSpecialPerson = function ($event) {
        var index = this.employeeData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.employeeData[index];
        console.log("event and data", this.empName, $event);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "specialPerson"; });
        var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsers[spIndex] = splPerson;
        }
        else {
            this.priorityUsers.push(splPerson);
        }
        console.log("spname", this.priorityUsers);
    };
    WorkflowApprovalComponent.prototype.empSplRole = function ($event) {
        var index = this.splRoleData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.splRoleData[index];
        console.log("event and data", this.splRole, $event);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "specialRole"; });
        var splPerson = { userType: "specialRole", name: user.name, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsers[spIndex] = splPerson;
        }
        else {
            this.priorityUsers.push(splPerson);
        }
        console.log("spname", this.priorityUsers);
    };
    WorkflowApprovalComponent.prototype.empSplRoleUpdate = function ($event) {
        var index = this.splRoleData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.splRoleData[index];
        console.log("event and data", this.splRole, $event);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "specialRole"; });
        var splPerson1 = { userType: "specialRole", name: user.name, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsers[this.priorityUsers.length] = splPerson1;
        }
        else {
            this.priorityUsers[spIndex] = splPerson1;
        }
        console.log("spname role update", this.priorityUsers);
    };
    /* Description: selcted person comes to drag and drop section for specialPeeson employee only when Update
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empSpecialPersonUpdate = function ($event) {
        var index = this.employeeData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.employeeData[index];
        console.log("event and data", this.empName, $event);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "specialPerson"; });
        var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsers[this.priorityUsers.length] = splPerson;
        }
        else {
            this.priorityUsers[spIndex] = splPerson;
        }
        console.log("spname", this.priorityUsers);
    };
    /* Description: selcted person comes to drag and drop section for manager employee only when create
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.empManagerPerson = function ($event) {
        console.log($event);
        var index = this.managerData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.managerData[index];
        console.log("event and data", this.managerName, $event);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "manager"; });
        var manager = { userType: "manager", name: user.levelName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsers[spIndex] = manager;
        }
        else {
            this.priorityUsers.push(manager);
        }
        console.log("spname", this.priorityUsers);
    };
    /* Description: selcted person comes to drag and drop section for Manager employee only when update
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empManagerPersonUpdate = function ($event) {
        console.log(this.priorityUsers);
        var index = this.managerData.findIndex(function (userData) { return userData._id === $event; });
        console.log(index, 'manager');
        var user = this.managerData[index];
        console.log("event and data", this.managerName, $event, user);
        var spIndex1 = this.priorityUsers.findIndex(function (e) { return e.userType === "manager"; });
        var manager = { userType: "manager", name: user.levelName, _id: user._id };
        console.log(spIndex1, manager);
        if (spIndex1 == -1) {
            this.priorityUsers[this.priorityUsers.length] = manager;
        }
        else {
            this.priorityUsers[spIndex1] = manager;
        }
    };
    /* Description: selcted person comes to drag and drop section for HR employee only when create
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empHrPerson = function ($event) {
        console.log($event);
        var index = this.hrData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.hrData[index];
        console.log("event and data", this.hrName, $event, user);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "hr"; });
        var hr = { name: user.levelName, _id: user._id, userType: "hr" };
        if (spIndex >= 0) {
            this.priorityUsers[spIndex] = hr;
        }
        else {
            this.priorityUsers.push(hr);
        }
        console.log("spname", this.priorityUsers);
    };
    /* Description: selcted person comes to drag and drop section for HR employee only when update
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.empHrPersonUpdate = function ($event) {
        console.log($event);
        var index = this.hrData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.hrData[index];
        console.log("event and data", this.hrName, $event);
        var spIndex = this.priorityUsers.findIndex(function (e) { return e.userType === "hr"; });
        var hr = { name: user.levelName, _id: user._id, userType: "hr", };
        if (spIndex == -1) {
            this.priorityUsers[this.priorityUsers.length] = hr;
        }
        else {
            this.priorityUsers[spIndex] = hr;
        }
        console.log("spname", this.priorityUsers);
    };
    /* Description: send (POST) demographic workflow
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.workflowSubmit = function () {
        var _this = this;
        console.log("asdasdasdas");
        for (var i = 0; i < this.priorityUsers.length; i++) {
            if (this.priorityUsers[i].userType == "manager") {
                var prioritymanager = i + 1;
                var pID = this.priorityUsers[i]._id;
            }
            if (this.priorityUsers[i].userType == "hr") {
                var priorityHr = i + 1;
                var pIdHR = this.priorityUsers[i]._id;
            }
            if (this.priorityUsers[i].userType == "specialPerson") {
                var prioritySP = i + 1;
                var pIdSP = this.priorityUsers[i]._id;
            }
            if (this.priorityUsers[i].userType == 'specialRole') {
                var prioritySR = i + 1;
                var pIdSR = this.priorityUsers[i]._id;
            }
        }
        var Cid = JSON.parse(localStorage.getItem('companyId'));
        var postdata = {
            workflowType: 'demographic',
            reportsTo: {
                priority: prioritymanager,
                levelId: pID
            },
            hr: {
                priority: priorityHr,
                levelId: pIdHR
            },
            specialPerson: {
                priority: prioritySP,
                personId: pIdSP
            },
            specialRole: {
                priority: prioritySR,
                roleId: pIdSR
            },
            // reportedBy: this.employeeBaseId,
            companyId: Cid,
        };
        console.log("postDATa", postdata);
        this.workflowService.createWorkFlow(postdata)
            .subscribe(function (res) {
            console.log("post weokflow", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('workflow approval', res.message, "success");
                _this.modalRef.hide();
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Workflow Approval", err.error.message, "error");
        });
    };
    /* Description: update (PUT) demographic workflow
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.updateDemographicWorkflow = function () {
        var _this = this;
        console.log(this.priorityUsers, 'users');
        for (var i = 0; i < this.priorityUsers.length; i++) {
            if (this.priorityUsers[i].userType == "manager") {
                console.log(i);
                var pID = this.priorityUsers[i]._id;
                var prioritymanager = i + 1;
            }
            if (this.priorityUsers[i].userType == "hr") {
                console.log(i);
                var pIdHR = this.priorityUsers[i]._id;
                var priorityHr = i + 1;
            }
            if (this.priorityUsers[i].userType == "specialPerson") {
                var pIdSP = this.priorityUsers[i]._id;
                var prioritySP = i + 1;
            }
            if (this.priorityUsers[i].userType == 'specialRole') {
                var pIdSR = this.priorityUsers[i]._id;
                var prioritySR = i + 1;
            }
        }
        console.log(pIdHR, 'hrpersonid', pIdSR, "specialperson id");
        var postdata = {
            workflowType: 'demographic',
            reportsTo: {
                priority: prioritymanager,
                levelId: pID
            },
            hr: {
                priority: priorityHr,
                levelId: pIdHR
            },
            specialPerson: {
                priority: prioritySP,
                personId: pIdSP
            },
            specialRole: {
                priority: prioritySR,
                roleId: pIdSR
            },
        };
        console.log("update work flow data", postdata);
        this.workflowService.updateDemographicWorkflow(this.workflowId, postdata)
            .subscribe(function (res) {
            console.log("update demographic workflow", res);
            if (res.status == true)
                _this.modalRef.hide();
        }, function (err) {
        });
    };
    /* Description: drag and drop for priority changes in demographic workflow
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.drop = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(this.priorityUsers, event.previousIndex, event.currentIndex);
        console.log("drag event", event, this.priorityUsers);
    };
    // ===================================================================JOB CHANGE WORK FLOW  ===============================================================================
    /* Description: get job workflow details
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.getJobWorkFlow = function (cid) {
        var _this = this;
        this.workflowService.getWorkflowservice(cid, "job")
            .subscribe(function (res) {
            console.log("get job workflow", res);
            if (res.data.length >= 1) {
                _this.jobWorkflowId = res.data[0]._id;
                _this.updateJobWorkflow = true;
                if (res.data[0].hr) {
                    _this.jobHrName = res.data[0].hr.levelId;
                    var index = _this.hrData.findIndex(function (userData) { return userData._id === res.data[0].hr.levelId; });
                    var user = _this.hrData[index];
                    // user['userType'] = 'hr';
                    var name = user.levelName;
                    var _id = user._id;
                    _this.priorityUsersJob[(res.data[0].hr.priority) - 1] = { name: name, _id: _id, userType: 'hr' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                    console.log("res get of workflow", user);
                }
                if (res.data[0].reportsTo) {
                    _this.jobManagerName = res.data[0].reportsTo.levelId;
                    var indexManager = _this.managerData.findIndex(function (userData) { return userData._id === res.data[0].reportsTo.levelId; });
                    var userManager = _this.managerData[indexManager];
                    // userManager['userType'] = 'manager';
                    var name = userManager.levelName;
                    var _id = userManager._id;
                    _this.priorityUsersJob[(res.data[0].reportsTo.priority) - 1] = { name: name, _id: _id, userType: 'manager' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                    console.log("res get of workflow", userManager);
                }
                if (res.data[0].specialPerson) {
                    _this.jobEmpName = res.data[0].specialPerson.personId;
                    var indexSpecialPerson = _this.employeeData.findIndex(function (userData) { return userData._id === res.data[0].specialPerson.personId; });
                    var userSpecialManager = _this.employeeData[indexSpecialPerson];
                    console.log(userSpecialManager, 'newqqqqqqqqqqqqq1');
                    var name = userSpecialManager.personal.name.preferredName;
                    var _id = userSpecialManager._id;
                    _this.priorityUsersJob[(res.data[0].specialPerson.priority) - 1] = { name: name, _id: _id, userType: 'specialPerson' };
                    console.log("res get of workflow", { name: name, _id: _id, userType: 'specialPerson' });
                }
                if (res.data[0].specialRole) {
                    _this.jobSplRole = res.data[0].specialRole.roleId;
                    console.log(_this.splRoleData);
                    var indexSpecialRole = _this.splRoleData.findIndex(function (userData) { return userData._id === res.data[0].specialRole.roleId; });
                    console.log("indexx value", indexSpecialRole);
                    var userSpecialManagerole = _this.splRoleData[indexSpecialRole];
                    console.log(userSpecialManagerole, "11111");
                    var name = userSpecialManagerole.name;
                    var _id = userSpecialManagerole._id;
                    _this.priorityUsersJob[(res.data[0].specialRole.priority) - 1] = { name: name, _id: _id, userType: 'specialRole' };
                    console.log("res get of workflow", { name: name, _id: _id });
                    console.log(_this.priorityUsersJob);
                }
            }
        }, function (err) {
            console.log("error", err);
        });
    };
    /* Description: selcted person comes to drag and drop section for manager  only when create(Job change workflow)
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empJobManagerPerson = function ($event) {
        console.log($event);
        var index = this.managerData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.managerData[index];
        console.log("event and data", this.jobManagerName, $event);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "manager"; });
        var manager = { userType: "manager", name: user.levelName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersJob[spIndex] = manager;
        }
        else {
            this.priorityUsersJob.push(manager);
        }
        console.log("spname", this.priorityUsersJob);
    };
    /* Description: selcted person comes to drag and drop section for manager  only when Update(Job change workflow)
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empJobManagerPersonUpdate = function ($event) {
        console.log(this.priorityUsers);
        var index = this.managerData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.managerData[index];
        console.log("event and data", this.jobManagerName, $event, user);
        var spIndex1 = this.priorityUsersJob.findIndex(function (e) { return e.userType === "manager"; });
        var manager = { userType: "manager", name: user.levelName, _id: user._id };
        console.log(spIndex1, manager);
        if (spIndex1 == -1) {
            this.priorityUsersJob[this.priorityUsersJob.length] = manager;
        }
        else {
            this.priorityUsersJob[spIndex1] = manager;
        }
    };
    /* Description: selcted person comes to drag and drop section for Hr  only when create(Job change workflow)
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empJobHrPerson = function ($event) {
        console.log($event);
        var index = this.hrData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.hrData[index];
        console.log("event and data", this.jobHrName, $event);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "hr"; });
        var hr = { userType: "hr", name: user.levelName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersJob[spIndex] = hr;
        }
        else {
            this.priorityUsersJob.push(hr);
        }
        console.log("spname", this.priorityUsersJob);
    };
    /* Description: selcted person comes to drag and drop section for HR only when Update(Job change workflow)
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empJobHrPersonUpdate = function ($event) {
        console.log($event);
        var index = this.hrData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.hrData[index];
        console.log("event and data", this.jobHrName, $event);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "hr"; });
        var hr = { userType: "hr", name: user.levelName, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsersJob[this.priorityUsersJob.length] = hr;
        }
        else {
            this.priorityUsersJob[spIndex] = hr;
        }
        console.log("spname", this.priorityUsersJob);
    };
    WorkflowApprovalComponent.prototype.jobSpecialRole = function ($event) {
        var index = this.splRoleData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.splRoleData[index];
        console.log("event and data", this.jobSplRole, $event, user);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "specialRole"; });
        var splPerson = { userType: "specialRole", name: user.name, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersJob[spIndex] = splPerson;
        }
        else {
            this.priorityUsersJob.push(splPerson);
        }
        console.log("spname", this.priorityUsers);
    };
    WorkflowApprovalComponent.prototype.jobSpeciaRoleUpdate = function ($event) {
        var index = this.splRoleData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.splRoleData[index];
        console.log("event and data", this.jobSplRole, $event);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "specialRole"; });
        var splPerson1 = { userType: "specialRole", name: user.name, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsersJob[this.priorityUsersJob.length] = splPerson1;
        }
        else {
            this.priorityUsersJob[spIndex] = splPerson1;
        }
        console.log("spname role update", this.priorityUsersJob);
    };
    /* Description: selcted person comes to drag and drop section for specialPerson  only when create(Job change workflow)
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empJobSpecialPerson = function ($event) {
        var index = this.employeeData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.employeeData[index];
        console.log("event and data", this.jobEmpName, $event);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "specialPerson"; });
        var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersJob[spIndex] = splPerson;
        }
        else {
            this.priorityUsersJob.push(splPerson);
        }
        console.log("spname", this.priorityUsersJob);
    };
    /* Description: selcted person comes to drag and drop section for specialPerson  only when Update(Job change workflow)
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.empJobSpecialPersonUpdate = function ($event) {
        var index = this.employeeData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.employeeData[index];
        console.log("event and data", this.jobEmpName, $event);
        var spIndex = this.priorityUsersJob.findIndex(function (e) { return e.userType === "specialPerson"; });
        var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsersJob[this.priorityUsersJob.length] = splPerson;
        }
        else {
            this.priorityUsersJob[spIndex] = splPerson;
        }
        console.log("spname", this.priorityUsersJob);
    };
    /* Description: drag and drop for priority changes in job change workflow
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.dropForJob = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(this.priorityUsersJob, event.previousIndex, event.currentIndex);
        console.log("drag event", event, this.priorityUsersJob);
    };
    /* Description: send (POST) job change workflow
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.jobWorkflowSubmit = function () {
        var _this = this;
        console.log("asdasdasdas");
        for (var i = 0; i < this.priorityUsersJob.length; i++) {
            if (this.priorityUsersJob[i].userType == "manager") {
                var prioritymanager = i + 1;
                var pID = this.priorityUsersJob[i]._id;
            }
            if (this.priorityUsersJob[i].userType == "hr") {
                var priorityHr = i + 1;
                var pIdHR = this.priorityUsersJob[i]._id;
            }
            if (this.priorityUsersJob[i].userType == "specialRole") {
                var prioritySR = i + 1;
                var pIdSR = this.priorityUsersJob[i]._id;
            }
            if (this.priorityUsersJob[i].userType == "specialPerson") {
                var prioritySP = i + 1;
                var pIdSP = this.priorityUsersJob[i]._id;
            }
        }
        var Cid = JSON.parse(localStorage.getItem('companyId'));
        var postdata = {
            workflowType: 'job',
            reportsTo: {
                priority: prioritymanager,
                levelId: pID
            },
            hr: {
                priority: priorityHr,
                levelId: pIdHR
            },
            specialPerson: {
                priority: prioritySP,
                personId: pIdSP
            },
            specialRole: {
                priority: prioritySR,
                roleId: pIdSR
            },
            // reportedBy: this.managerId,
            companyId: Cid,
        };
        console.log("postDATa", postdata);
        this.workflowService.createWorkFlow(postdata)
            .subscribe(function (res) {
            console.log("post weokflow", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('workflow approval', res.message, "success");
                _this.modalRef.hide();
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Workflow Approval", err.error.message, "error");
        });
    };
    /* Description: update (PUT) job change workflow
   author : vipin reddy */
    WorkflowApprovalComponent.prototype.jobUpdateWorkflow = function () {
        var _this = this;
        for (var i = 0; i < this.priorityUsersJob.length; i++) {
            console.log(this.priorityUsersJob[i]);
            if (this.priorityUsersJob[i].userType == "manager") {
                var prioritymanager = i + 1;
                var pID = this.priorityUsersJob[i]._id;
            }
            if (this.priorityUsersJob[i].userType == "hr") {
                var priorityHr = i + 1;
                var pIdHR = this.priorityUsersJob[i]._id;
            }
            if (this.priorityUsersJob[i].userType == "specialPerson") {
                var prioritySP = i + 1;
                var pIdSP = this.priorityUsersJob[i]._id;
            }
            if (this.priorityUsersJob[i].userType == "specialRole") {
                var prioritySR = i + 1;
                var pIdSR = this.priorityUsersJob[i]._id;
            }
        }
        var postdata = {
            workflowType: 'job',
            reportsTo: {
                priority: prioritymanager,
                levelId: pID
            },
            hr: {
                priority: priorityHr,
                levelId: pIdHR
            },
            specialPerson: {
                priority: prioritySP,
                personId: pIdSP
            },
            specialRole: {
                priority: prioritySR,
                roleId: pIdSR
            }
        };
        console.log("update job workflow", postdata);
        this.workflowService.updateDemographicWorkflow(this.jobWorkflowId, postdata)
            .subscribe(function (res) {
            console.log("update job workflow", res);
            if (res.status == true)
                _this.modalRef.hide();
        }, function (err) {
        });
    };
    // ===================================================compensation change workflow===================================================
    WorkflowApprovalComponent.prototype.empCompensationManagerPerson = function ($event) {
        console.log($event);
        var index = this.managerData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.managerData[index];
        console.log("event and data", this.compensationManagerName, $event);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "manager"; });
        var manager = { userType: "manager", name: user.levelName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersCompensation[spIndex] = manager;
        }
        else {
            this.priorityUsersCompensation.push(manager);
        }
        console.log("spname", this.priorityUsersCompensation);
    };
    WorkflowApprovalComponent.prototype.empCompensationManagerPersonUpdate = function ($event) {
        console.log(this.priorityUsersCompensation);
        var index = this.managerData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.managerData[index];
        console.log("event and data", this.compensationManagerName, $event, user);
        var spIndex1 = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "manager"; });
        var manager = { userType: "manager", name: user.levelName, _id: user._id };
        console.log(spIndex1, manager);
        if (spIndex1 == -1) {
            this.priorityUsersCompensation[this.priorityUsersCompensation.length] = manager;
        }
        else {
            this.priorityUsersCompensation[spIndex1] = manager;
        }
    };
    WorkflowApprovalComponent.prototype.empCompensationHrPerson = function ($event) {
        console.log($event);
        var index = this.hrData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.hrData[index];
        console.log("event and data", this.compensationHrName, $event);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "hr"; });
        var hr = { userType: "hr", name: user.levelName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersCompensation[spIndex] = hr;
        }
        else {
            this.priorityUsersCompensation.push(hr);
        }
        console.log("spname", this.priorityUsersCompensation);
    };
    WorkflowApprovalComponent.prototype.empCompensationHrPersonUpdate = function ($event) {
        console.log($event);
        var index = this.hrData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.hrData[index];
        console.log("event and data", this.compensationHrName, $event);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "hr"; });
        var hr = { userType: "hr", name: user.levelName, _id: user._id };
        console.log(spIndex, hr);
        if (spIndex == -1) {
            this.priorityUsersCompensation[this.priorityUsersCompensation.length] = hr;
        }
        else {
            this.priorityUsersCompensation[spIndex] = hr;
        }
        console.log("spname", this.priorityUsersCompensation);
    };
    WorkflowApprovalComponent.prototype.compensationSpecialRole = function ($event) {
        var index = this.splRoleData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.splRoleData[index];
        console.log("event and data", this.jobSplRole, $event, user);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "specialRole"; });
        var splPerson = { userType: "specialRole", name: user.name, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersCompensation[spIndex] = splPerson;
        }
        else {
            this.priorityUsersCompensation.push(splPerson);
        }
        console.log("spname", this.priorityUsers);
    };
    WorkflowApprovalComponent.prototype.compensationSpeciaRoleUpdate = function ($event) {
        var index = this.splRoleData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.splRoleData[index];
        console.log("event and data", this.jobSplRole, $event);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "specialRole"; });
        var splPerson1 = { userType: "specialRole", name: user.name, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsersCompensation[this.priorityUsersCompensation.length] = splPerson1;
        }
        else {
            this.priorityUsersCompensation[spIndex] = splPerson1;
        }
        console.log("spname role update", this.priorityUsersCompensation);
    };
    WorkflowApprovalComponent.prototype.empCompensationSpecialPerson = function ($event) {
        var index = this.employeeData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.employeeData[index];
        console.log("event and data", this.compensationEmpName, $event);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "specialPerson"; });
        var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id };
        if (spIndex >= 0) {
            this.priorityUsersCompensation[spIndex] = splPerson;
        }
        else {
            this.priorityUsersCompensation.push(splPerson);
        }
        console.log("spname", this.priorityUsersCompensation);
    };
    WorkflowApprovalComponent.prototype.empCompensationSpecialPersonUpdate = function ($event) {
        var index = this.employeeData.findIndex(function (userData) { return userData._id === $event; });
        var user = this.employeeData[index];
        console.log("event and data", this.compensationEmpName, $event);
        var spIndex = this.priorityUsersCompensation.findIndex(function (e) { return e.userType === "specialPerson"; });
        var splPerson = { userType: "specialPerson", name: user.personal.name.preferredName, _id: user._id };
        if (spIndex == -1) {
            this.priorityUsersCompensation[this.priorityUsersCompensation.length] = splPerson;
        }
        else {
            this.priorityUsersCompensation[spIndex] = splPerson;
        }
        console.log("spname", this.priorityUsersCompensation);
    };
    WorkflowApprovalComponent.prototype.getCompensationWorkFlow = function (cid) {
        var _this = this;
        this.workflowService.getWorkflowservice(cid, "compensation")
            .subscribe(function (res) {
            console.log("get workflow", res);
            if (res.data.length >= 1) {
                _this.compensationWorkflowId = res.data[0]._id;
                _this.updateCompensationWorkflow = true;
                if (res.data[0].hr) {
                    _this.compensationHrName = res.data[0].hr.levelId;
                    var index = _this.hrData.findIndex(function (userData) { return userData._id === res.data[0].hr.levelId; });
                    var user = _this.hrData[index];
                    // user['userType'] = 'hr';
                    var name = user.levelName;
                    var _id = user._id;
                    _this.priorityUsersCompensation[(res.data[0].hr.priority) - 1] = { name: name, _id: _id, userType: 'hr' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                    console.log("res get of workflow", user);
                }
                if (res.data[0].reportsTo) {
                    _this.compensationManagerName = res.data[0].reportsTo.levelId;
                    var indexManager = _this.managerData.findIndex(function (userData) { return userData._id === res.data[0].reportsTo.levelId; });
                    var userManager = _this.managerData[indexManager];
                    // userManager['userType'] = 'manager';
                    var name = userManager.levelName;
                    var _id = userManager._id;
                    _this.priorityUsersCompensation[(res.data[0].reportsTo.priority) - 1] = { name: name, _id: _id, userType: 'manager' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                    console.log("res get of workflow", userManager);
                }
                if (res.data[0].specialPerson) {
                    _this.compensationEmpName = res.data[0].specialPerson.personId;
                    var indexSpecialPerson = _this.employeeData.findIndex(function (userData) { return userData._id === res.data[0].specialPerson.personId; });
                    var userSpecialManager = _this.employeeData[indexSpecialPerson];
                    var name = userSpecialManager.personal.name.preferredName;
                    var _id = userSpecialManager._id;
                    _this.priorityUsersCompensation[(res.data[0].specialPerson.priority) - 1] = { name: name, _id: _id, userType: 'specialPerson' };
                    console.log("res get of workflow", { name: name, _id: _id, userType: 'specialPerson' });
                }
                if (res.data[0].specialPerson) {
                    _this.compensationSplRole = res.data[0].specialRole.roleId;
                    console.log(_this.splRoleData);
                    var indexSpecialRole = _this.splRoleData.findIndex(function (userData) { return userData._id === res.data[0].specialRole.roleId; });
                    console.log("indexx value", indexSpecialRole);
                    var userSpecialManagerole = _this.splRoleData[indexSpecialRole];
                    console.log(userSpecialManagerole, "11111");
                    var name = userSpecialManagerole.name;
                    var _id = userSpecialManagerole._id;
                    _this.priorityUsersCompensation[(res.data[0].specialRole.priority) - 1] = { name: name, _id: _id, userType: 'specialRole' };
                    console.log("res get of workflow", { name: name, _id: _id });
                    console.log(_this.priorityUsersJob);
                }
            }
        }, function (err) {
            console.log("error", err);
        });
    };
    WorkflowApprovalComponent.prototype.dropCompensation = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(this.priorityUsersCompensation, event.previousIndex, event.currentIndex);
        console.log("drag event", event, this.priorityUsersCompensation);
    };
    /* Description: send (POST) job change workflow
  author : vipin reddy */
    WorkflowApprovalComponent.prototype.compensationWorkflowSubmit = function () {
        var _this = this;
        console.log("asdasdasdas");
        for (var i = 0; i < this.priorityUsersCompensation.length; i++) {
            if (this.priorityUsersCompensation[i].userType == "manager") {
                var prioritymanager = i + 1;
                var pID = this.priorityUsersCompensation[i]._id;
            }
            if (this.priorityUsersCompensation[i].userType == "hr") {
                var priorityHr = i + 1;
                var pIdHR = this.priorityUsersCompensation[i]._id;
            }
            if (this.priorityUsersCompensation[i].userType == "specialPerson") {
                var prioritySP = i + 1;
                var pIdSP = this.priorityUsersCompensation[i]._id;
            }
            if (this.priorityUsersCompensation[i].userType == "specialRole") {
                var prioritySR = i + 1;
                var pIdSR = this.priorityUsersCompensation[i]._id;
            }
        }
        var Cid = JSON.parse(localStorage.getItem('companyId'));
        var postdata = {
            workflowType: 'compensation',
            reportsTo: {
                priority: prioritymanager,
                levelId: pID
            },
            hr: {
                priority: priorityHr,
                levelId: pIdHR
            },
            specialPerson: {
                priority: prioritySP,
                personId: pIdSP
            },
            specialRole: {
                priority: prioritySR,
                roleId: pIdSR
            },
            // reportedBy: this.managerId,
            companyId: Cid,
        };
        console.log("postDATa", postdata);
        this.workflowService.createWorkFlow(postdata)
            .subscribe(function (res) {
            console.log("post weokflow", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('workflow approval', res.message, "success");
                _this.modalRef.hide();
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Workflow Approval", err.error.message, "error");
        });
    };
    /* Description: update (PUT) job change workflow
    author : vipin reddy */
    WorkflowApprovalComponent.prototype.compensationUpdateWorkflow = function () {
        var _this = this;
        for (var i = 0; i < this.priorityUsersCompensation.length; i++) {
            console.log(this.priorityUsersCompensation[i]);
            if (this.priorityUsersCompensation[i].userType == "manager") {
                var prioritymanager = i + 1;
                var pID = this.priorityUsersCompensation[i]._id;
            }
            if (this.priorityUsersCompensation[i].userType == "hr") {
                var priorityHr = i + 1;
                var pIdHR = this.priorityUsersCompensation[i]._id;
            }
            if (this.priorityUsersCompensation[i].userType == "specialPerson") {
                var prioritySP = i + 1;
                var pIdSP = this.priorityUsersCompensation[i]._id;
            }
            if (this.priorityUsersCompensation[i].userType == "specialRole") {
                var prioritySR = i + 1;
                var pIdSR = this.priorityUsersCompensation[i]._id;
            }
        }
        var postdata = {
            workflowType: 'compensation',
            reportsTo: {
                priority: prioritymanager,
                levelId: pID
            },
            hr: {
                priority: priorityHr,
                levelId: pIdHR
            },
            specialPerson: {
                priority: prioritySP,
                personId: pIdSP
            },
            specialRole: {
                priority: prioritySR,
                roleId: pIdSR
            }
        };
        console.log("update job workflow", postdata);
        this.workflowService.updateDemographicWorkflow(this.compensationWorkflowId, postdata)
            .subscribe(function (res) {
            console.log("update compensation workflow", res);
            if (res.status == true)
                _this.modalRef.hide();
        }, function (err) {
        });
    };
    // ================================================== whole component main functions==================================================
    /* Description: before page routing
     author : vipin reddy */
    WorkflowApprovalComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/company-settings']);
    };
    /* Description: open modal for all 3 modals
    author : vipin reddy */
    WorkflowApprovalComponent.prototype.workflowApprovalModal = function (template) {
        this.selectedField = 'additionalQuestions';
        this.subHeader = "Who can request the change?";
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg demographic-popup' }));
    };
    /* Description: selected tabs for choosing one
    author : vipin reddy */
    WorkflowApprovalComponent.prototype.selectedTab = function (activeTab) {
        console.log(activeTab);
        this.selectedField = activeTab;
        if (this.selectedField == "requestChange") {
            this.subHeader = "Who can request the change?";
        }
        else if (this.selectedField == "approveChange") {
            this.subHeader = "Who can Approve?";
        }
        else if (this.selectedField == "additionalQuestions") {
            this.subHeader = "What is changed? / Additional Questions";
        }
        else if (this.selectedField == "summary") {
            this.subHeader = "Summary / Review";
        }
    };
    WorkflowApprovalComponent.prototype.levelnumberSubmitHr = function () {
        var _this = this;
        var postdata = {
            levelIndex: this.hrLevels,
            levelType: 'HR',
            companyId: JSON.parse(localStorage.getItem('companyId'))
        };
        console.log(postdata);
        this.workflowService.LevelCreation(postdata)
            .subscribe(function (res) {
            console.log("hr levels submit", res);
            if (res.status == true) {
                _this.gettingLevelsHr();
                _this.gettingLevelsManager();
                _this.swalAlertService.SweetAlertWithoutConfirmation("Levels", res.message, 'success');
                // this.hrLevels = ''
            }
        }, function (err) {
            console.log(err);
        });
    };
    WorkflowApprovalComponent.prototype.levelnumberSubmitManager = function () {
        var _this = this;
        this.isValidManagerlevel = true;
        var postdata = {
            levelIndex: this.mangerLevels,
            levelType: 'Manager',
            companyId: JSON.parse(localStorage.getItem('companyId'))
        };
        console.log(postdata);
        if (this.isValidManagerlevel) {
            this.workflowService.LevelCreation(postdata)
                .subscribe(function (res) {
                console.log("hr levels submit", res);
                if (res.status == true) {
                    // this.mangerLevels = ''
                    // this.mangerLevels = 
                    _this.gettingLevelsHr();
                    _this.gettingLevelsManager();
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Levels", res.message, 'success');
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    WorkflowApprovalComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    WorkflowApprovalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-workflow-approval',
            template: __webpack_require__(/*! ./workflow-approval.component.html */ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.html"),
            styles: [__webpack_require__(/*! ./workflow-approval.component.css */ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__["SiteAccessRolesService"],
            _services_workflow_service__WEBPACK_IMPORTED_MODULE_5__["WorkflowService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__["SwalAlertService"]])
    ], WorkflowApprovalComponent);
    return WorkflowApprovalComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.module.ts ***!
  \************************************************************************************************/
/*! exports provided: WorkflowApprovalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkflowApprovalModule", function() { return WorkflowApprovalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _workflow_approval_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./workflow-approval.component */ "./src/app/admin-dashboard/company-settings/workflow-approval/workflow-approval.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var router = [
    { path: '', redirectTo: 'workflow', pathMatch: 'full' },
    { path: 'workflow', component: _workflow_approval_component__WEBPACK_IMPORTED_MODULE_2__["WorkflowApprovalComponent"] }
];
var WorkflowApprovalModule = /** @class */ (function () {
    function WorkflowApprovalModule() {
    }
    WorkflowApprovalModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(router),
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatNativeDateModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_6__["MatCheckboxModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__["DragDropModule"]
            ],
            declarations: [_workflow_approval_component__WEBPACK_IMPORTED_MODULE_2__["WorkflowApprovalComponent"]]
        })
    ], WorkflowApprovalModule);
    return WorkflowApprovalModule;
}());



/***/ })

}]);
//# sourceMappingURL=workflow-approval-workflow-approval-module.js.map