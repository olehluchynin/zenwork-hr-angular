(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["termination-wizard-termination-wizard-module"],{

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard.module.ts":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard.module.ts ***!
  \**************************************************************************************************************************/
/*! exports provided: TerminationWizardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TerminationWizardModule", function() { return TerminationWizardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _termination_wizard_termination_wizard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./termination-wizard/termination-wizard.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.ts");
/* harmony import */ var _termination_wizard_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./termination-wizard.routing */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard.routing.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var TerminationWizardModule = /** @class */ (function () {
    function TerminationWizardModule() {
    }
    TerminationWizardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_2__["BsDatepickerModule"].forRoot(),
                _termination_wizard_routing__WEBPACK_IMPORTED_MODULE_4__["TerminationWizardRouting"]
            ],
            declarations: [_termination_wizard_termination_wizard_component__WEBPACK_IMPORTED_MODULE_3__["TerminationWizardComponent"]]
        })
    ], TerminationWizardModule);
    return TerminationWizardModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard.routing.ts":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard.routing.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: TerminationWizardRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TerminationWizardRouting", function() { return TerminationWizardRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _termination_wizard_termination_wizard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./termination-wizard/termination-wizard.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', redirectTo: "/termination-wizard", pathMatch: "full" },
    { path: 'termination-wizard', component: _termination_wizard_termination_wizard_component__WEBPACK_IMPORTED_MODULE_2__["TerminationWizardComponent"] }
];
var TerminationWizardRouting = /** @class */ (function () {
    function TerminationWizardRouting() {
    }
    TerminationWizardRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], TerminationWizardRouting);
    return TerminationWizardRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.css":
/*!*************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.css ***!
  \*************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 3px 10px;\n    font-size: 10px;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 14px!important;   \n}\n.mr-7{\n    margin-right: 7px;\n}\n.main-terminarion{\n    float: none;\n    margin: 0 auto;\n    padding: 0;\n}\n.zenwork-inner-icon{\n    width: 20px;\n    height: auto;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\na{\n    color:#000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.add-template-main{\n    width:100%;\n    border-bottom: 1px solid #eeeeee;\n\n}\n.add-template{\n    width:20%;\n    float: left;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    padding-bottom: 10px;\n}\n.add-template .btn-employee{\n        background-color: #439348 !important;\n        font-size: 12px;\n        padding: 3px 12px !important;\n    cursor: auto;\n}\n.add-template .btn-employee{\n    background-color: #439348 !important;\n    font-size: 12px;\n    padding: 3px 12px !important;\n}\n.add-fields{\n    width:80%;\n    float: right;\n    margin-top:20px;\n\n}\n.add-buttons{\n    display: inline-block;\n    float: right;\n}\n.list-buttons{\n    display: inline-block;\n    padding: 0px 20px;\n    font-size: 12px;\n    font-weight: 600;\n}\n.plus-icon{\n    color:#439348 !important;\n}\n.main-onboarding .panel-font{\n    color:#439348 !important;\n    font-weight: 600;\n    margin-top: 5px;\n}\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n.settings-dropdown{\n    float:right;\n}\n.main-onboarding .panel-heading .btn{\n    padding:2px 6px!important;\n    border-radius: 0!important;\n}\n.caret{\n    margin-bottom: 5px!important;\n}\n.task-details .taskname{\n    width: 90%;\n    height: 30px;\n    padding-left: 10px;\n    font-size: 12px;\n    border: 0;\n    background: #f8f8f8;\n    color: #c1c1c1;\n}\n.zenwork-margin-top20{\n    margin-top: 20px;\n}\nselect{\n    height: 30px;\n}\nlabel{\n    font-weight: normal;\n    font-size: 12px;\n  \n\n}\n.zenwork-structure-settings{\n    border-radius: 0px!important;\n    height:30px;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n    color: #c1c1c1;\n}\n.assign-items .list-items{\n    width: 40%;\n    display: inline-block;\n    padding: 0px 10px;\n    height: 30px;\n}\n.assign-items .duedate{\n    height: 30px;\n    width:70%;\n    border: 0;\n    color: #c1c1c1;\n    background: #f8f8f8;\n}\n.calendar-icon{\n    position: absolute;\n    width: 15px;\n    top: 7px;\n    right: 34%;\n}\n.text-area{\n    width: 90%;\n}\ntextarea{\n    resize: none;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n}\n.category select{\n    font-size: 12px;\n    width: 20%;\n}\n.add-task-btns .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n\n}\n.add-task{\n    padding-left:0px!important;\n}\n.add-task-btns{\n    padding-top: 10px;\n}\n.add-task-btns .btn{\n  \n    height: 25px;\n    font-size: 12px;\n    padding: 4px 12px !important;\n}\n.add-btn{\n    cursor: auto;\n    background-color: #439348 !important;\n}\n.add-task-setting-btns .task-btns{\n    font-size: 12px;\n    margin: 0px 6px\n}\n.panel-body{\n    padding:15px 15px 0px 15px !important;\n}\n.attach-file-btn{\n    \n    padding: 5px 10px;\n    font-size: 12px;\n    border-color: #439348;\n    text-align: left;\n    width: 191px;\n    background: url(\"/assets/images/Ofboarding/ic_attachment_24px.png\") no-repeat #439348;\n}\nsmall{\n    font-size: 9px;\n    color:#3e3e3ea3;\n}\n.hr-tasks{\n    font-size: 13px;\n    font-weight: 600;\n}\n.individual-hr-task{\n    font-size:12px;\n}\n.margin-top-zero{\n    margin-top:0px!important;\n}\n.padding-top-remove{\n    padding-top:0px!important;\n}\n/* team-text-card */\n.team-introdution-card{\n    background-color: #fcfcfc;\n    /* border: 1px solid #eee; */\n    border-radius: 2px;\n    padding: 10px;\n}\n.cancel-task .btn{\n    cursor: auto;\n}\n.margin-zero{\n    margin:0px!important;\n}\n.team-introdution-card .padding-top-remove small{\n    /* padding-left:25px;  */\n}\n.card-intro{\n    font-size: 11px;\n    padding-left: 30px;\n}\n.team-introdution-card .individual-hr-task{\n    font-weight: 600;\n    color: #439348;\n}\n.checkbox {\n    padding-left: 20px;\n    margin-top:0px !important;\n    margin-bottom:0px !important;\n  }\n.checkbox label {\n    display: inline-block;\n    vertical-align: middle;\n    position: relative;\n    padding-left: 5px;\n  }\n.checkbox label::before {\n    content: \"\";\n    display: inline-block;\n    position: absolute;\n    width: 17px;\n    height: 17px;\n    left: 0;\n    margin-left: -20px;\n    border: 1px solid #cccccc;\n    border-radius: 3px;\n    background-color: #fff;\n    transition: border 0.15s ease-in-out, color 0.15s ease-in-out;\n  }\n.checkbox label::after {\n    display: inline-block;\n    position: absolute;\n    width: 16px;\n    height: 16px;\n    left: 0;\n    top: 0;\n    margin-left: -20px;\n    padding-left: 3px;\n    padding-top: 1px;\n    font-size: 11px;\n    color: #555555;\n  }\n.checkbox input[type=\"checkbox\"]:checked + label::after {\n    font-family: 'FontAwesome';\n    content: \"\\f00c\";\n  }\n.checkbox-success input[type=\"checkbox\"]:checked + label::before {\n    background-color: #5cb85c;\n    border-color: #5cb85c;\n  }\n.checkbox-success input[type=\"checkbox\"]:checked + label::after {\n    color: #fff;\n  }\n.emp-image{\n    padding-top: 0;\n    width: 43px;\n    display: inline-block;\n    vertical-align: middle;\n  }\n.employee-details{\n    width: 70px;\n    display: inline-block;\n    vertical-align: middle;\n    text-align: left;\n    margin-left: 10px;\n  }\n.employee-image-info{\n    padding-left: 20px;\n  }\n.emp-name{\n    font-size: 11px;\n    padding-left: 0;\n    display: block;\n    line-height: 11px;\n  }\n.borderright{\n      border-right: 1px solid #eee;\n  }\n.team-text-area{\n    margin-top:10px;\n  }\ntextarea{\n      resize: none;\n  }\n.btn-color{\n    border: 1px solid #EF8086;\n    color: #ef8086;\n    cursor: auto;\n  }\n.margin-zero{\n      margin: 0px!important;\n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.html":
/*!**************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.html ***!
  \**************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing']\">\n      <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n    </a>\n    <span class=\"inner-icon-img\">\n      <img src=\"../../../assets/images/Offboarding/Group 831.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n    </span>\n    <small class=\"sub-title\">\n      <b>Offboarding</b>\n    </small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n<div class=\"col-md-11 main-terminarion col-md-11\">\n  <div class=\"add-template-main\">\n    <!-- <div class=\"add-template\">\n    <button class=\"btn btn-success btn-employee\">- Terminate Employee </button>\n  </div> -->\n    <div class=\"add-fields\">\n      <ul class=\"add-buttons list-unstyled\">\n        <li class=\"list-buttons\">\n          <span class=\"plus-icon\">+</span>\n          Add task</li>\n        <li class=\"list-buttons\">\n          <span class=\"plus-icon\">+</span>\n          Add Category</li>\n        <li class=\"list-buttons\">\n          <span class=\"plus-icon\">+</span>\n          Add Template</li>\n      </ul>\n    </div>\n    <div class=\"clearfix\"></div>\n  </div>\n  <div class=\"main-onboarding\">\n    <div class=\"panel panel-info\">\n      <div class=\"panel-heading\">\n        <h3 class=\"panel-title panel-font pull-left\">Offboarding Tasks</h3>\n        <button class=\"btn btn-default dropdown-toggle settings-dropdown\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\"\n          aria-haspopup=\"true\" aria-expanded=\"true\">\n          <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>&nbsp;\n          <span class=\"caret\"></span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"panel-body task-details\">\n        <div class=\"col-xs-12 zenwork-margin-top20\">\n          <div class='row'>\n            <div class=\"col-xs-6\">\n              <div class=\"row\">\n                <input type=\"text\" class=\"taskname\" name=\"taskName\" placeholder=\"Task Name*\">\n\n              </div>\n            </div>\n            <div class=\"col-xs-6\">\n              <div class=\"row\">\n                <ul class=\"list-unstyled assign-items\">\n                  <li class=\"list-items\">\n                    <select class=\"form-control zenwork-structure-settings\">\n                      <option>Auto</option>\n                      <option>Manual</option>\n                    </select>\n                  </li>\n                  <li class=\"list-items\">\n                    <input class=\"form-control\" #dpMDY=\"bsDatepicker\" bsDatepicker formControlName=\"myDateMDY\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                      placeholder=\"Due Date\" name=\"birth-date\" class=\"duedate\">\n                  </li>\n                  <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon\" alt=\"Company-settings icon\" (click)=\"dpMDY.toggle()\"\n                    [attr.aria-expanded]=\"dpMDY.isOpen\">\n                </ul>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"text-area zenwork-margin-ten-zero\">\n          <div class=\"col-xs-12\">\n            <div class=\"row\">\n              <div class=\"col-xs-6\">\n                <div class=\"row\">\n                  <div class=\"form-group\">\n                    <textarea class=\"form-control rounded-0\" rows=\"3\"></textarea>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-xs-6\"></div>\n            </div>\n          </div>\n\n        </div>\n        <div class=\"category\">\n          <select class=\"form-control zenwork-structure-settings\">\n            <option>Category(Optional)</option>\n            <option>category</option>\n          </select>\n        </div>\n        <div class=\"category zenwork-margin-ten-zero\">\n          <label>Attach File (s)</label>\n          <br>\n          <!-- <select class=\"form-control zenwork-structure-settings\">\n          <option>Add Company Files</option>\n          <option>category</option>\n        </select> -->\n          <button class=\"btn btn-success attach-file-btn\">Add Company Files</button>\n        </div>\n        <div class=\"add-task-btns zenwork-margin-ten-zero\">\n          <ul class=\"list-unstyled\">\n            <li class=\"list-items add-task\">\n              <button class=\"btn btn-success add-btn\">Add a Task</button>\n            </li>\n            <li class=\"list-items cancel-task\">\n              <button class=\"btn\">Cancel</button>\n            </li>\n          </ul>\n        </div>\n\n      </div>\n    </div>\n  </div>\n  <div class=\"main-onboarding\">\n    <div class=\"panel panel-info\">\n      <div class=\"panel-heading\">\n        <h3 class=\"panel-title panel-font pull-left\">Offboarding Tasks</h3>\n        <div class=\"settings-dropdown add-task-setting-btns\">\n          <button class=\"btn btn-default task-btns\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">\n            + Add Tasks\n          </button>\n          <button class=\"btn btn-default task-btns dropdown-toggle\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n            aria-expanded=\"true\">\n            <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>&nbsp;\n            <span class=\"caret\"></span>\n          </button>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"panel-body\">\n        <p class=\"hr-tasks green\">HR Tasks</p>\n        <p class=\"individual-hr-task\">New Hire Orientation\n          <br>\n          <small>jennifer Caldwell-Jul 25,2018</small>\n        </p>\n      </div>\n      <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n      <div class=\"panel-body padding-top-remove\">\n        <p class=\"individual-hr-task\">Collect Employee ID Badge\n          <br>\n          <small>Shannon Anderson -Jul 25,2018</small>\n        </p>\n      </div>\n      <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n      <div class=\"panel-body padding-top-remove\">\n        <p class=\"individual-hr-task\">Check Vacation Payout/Accural\n          <br>\n          <small>Shannon Anderson -Jul 25,2018</small>\n        </p>\n      </div>\n    </div>\n    <div class=\"panel panel-info\">\n      <div class=\"panel-body\">\n        <p class=\"hr-tasks green\">IT Setup</p>\n        <p class=\"individual-hr-task\">Disable Email Access\n          <br>\n          <small>Eric Pasture - Jul 25,2018</small>\n        </p>\n      </div>\n      <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n      <div class=\"panel-body padding-top-remove\">\n        <p class=\"individual-hr-task\">Disable Access to Internal System & Shared Drive\n          <br>\n          <small>Ashley Adams -Jul 25,2018</small>\n        </p>\n      </div>\n      <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n      <div class=\"panel-body padding-top-remove\">\n        <p class=\"individual-hr-task\">Disable Access to Other Software Systems\n          <br>\n          <small>Ashley Adams -Jul 25,2018</small>\n        </p>\n      </div>\n    </div>\n    <div class=\"panel panel-info\">\n      <div class=\"panel-body\">\n        <p class=\"hr-tasks green\">Manager Tasks</p>\n        <p class=\"individual-hr-task\">Collect Company Assets\n          <br>\n          <small>Eric Pasture - Jul 25,2018</small>\n        </p>\n      </div>\n    </div>\n    <div class=\"panel panel-info\">\n      <div class=\"team-introdution-card\">\n        <div class=\"panel-body padding-top-remove\">\n          <div class=\"checkbox checkbox-success\">\n            <input id=\"checkbox3\" type=\"checkbox\" class=\"team-checkbox\">\n            <label for=\"checkbox3\">\n              <span class=\"individual-hr-task \">Collect Company Assets </span>\n              <br>\n              <small>Ashley Adams -Jul 25,2018</small>\n            </label>\n          </div>\n\n        </div>\n        <hr class=\"zenwork-margin-ten-zero\">\n        <p class=\"card-intro\">\n          please introduce the new hire to the team lead,and instruct the team lead to introduce the new hire to other individuals\n          the new hire to other individuals with inthe organization\n          <br> that will have a key role in their job responsibilities\n        </p>\n        <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n        <div class=\"employee-image-info\">\n          <img src=\"../../../assets/images/Onboarding/empoyee_2.png\" alt=\"employee-icon\" class=\"emp-image\">\n          <div class=\"employee-details borderright\">\n            <span class=\"emp-name\">Crane Andy </span>\n\n            <small class=\"emp-designation\">Manager</small>\n          </div>\n\n          <div class=\"employee-details\">\n            <span class=\"emp-name \">Today</span>\n            <small class=\"emp-designation\">10:07 AM</small>\n          </div>\n        </div>\n        <div class=\"team-text-area\">\n          <div class=\"form-group\">\n            <textarea class=\"form-control rounded-0\" placeholder=\"Add a note...\" rows=\"3\"></textarea>\n\n          </div>\n        </div>\n        <button class=\"btn zenwork-customized-back-btn pull-right btn-color\">\n          close\n\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.ts":
/*!************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.ts ***!
  \************************************************************************************************************************************************/
/*! exports provided: TerminationWizardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TerminationWizardComponent", function() { return TerminationWizardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TerminationWizardComponent = /** @class */ (function () {
    function TerminationWizardComponent() {
        this.minDate = new Date(100, 5, 10);
        this.maxDate = new Date(4000, 9, 15);
        this.myForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            myDateYMD: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](new Date()),
            myDateFull: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](new Date()),
            myDateMDY: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](new Date())
        });
    }
    TerminationWizardComponent.prototype.ngOnInit = function () {
    };
    TerminationWizardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-termination-wizard',
            template: __webpack_require__(/*! ./termination-wizard.component.html */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.html"),
            styles: [__webpack_require__(/*! ./termination-wizard.component.css */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard/termination-wizard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TerminationWizardComponent);
    return TerminationWizardComponent;
}());



/***/ })

}]);
//# sourceMappingURL=termination-wizard-termination-wizard-module.js.map