(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["employee-self-service-employee-self-service-module"],{

/***/ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.css":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 6px 15px;\n    font-size: 14px;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 14px!important; vertical-align: middle; padding: 0 0 0 5px;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-inner-icon{\n    width: 33px;\n    height: auto;\n    vertical-align: text-bottom;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 10px;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{\n    border:transparent !important;\n    border-bottom: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    background: #f8f8f8;\n    font-size: 16px;\n}\n.hire-wizard{\n    width:100%;\n}\n.main-tabs-heading{\n    \n    float: none;\n    margin: 0 auto; padding: 0;\n}\n.text-area{\n    margin:15px 30px;\n}\n.text-area textarea{\n    height: auto;\n    resize: none;\n    border: none;\n    box-shadow: none;\n}\n.text-area .form-control{\n    font-size: 15px;\n    line-height: 25px;\n    padding: 8px 30px 8px;\n    background: #fff;\n}\n.zenwork-buttons{\n    margin: 20px 25px;\n}\n.submit-btn{\n    font-size: 14px;\n    padding: 0px 22px !important;\n    border-radius: 25px;\n    margin: 0 10px 0 0;\n    border: 1px solid #439348;\n    color: #439348;background:#fff;\n}\n.cancel{\n    color: #e44a49;\n}\n.cancel-btn{\n    font-size: 12px;\n    padding: 5px 12px !important;\n    border-radius: 25px;\n}\n.personal { margin: 30px auto 0; float: none;}\n.personal .panel-default>.panel-heading { background-color:transparent !important; border: none;}\n.personal .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n.personal .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.personal .panel-title>.small, .personal .panel-title>.small>a, .personal .panel-title>a, .personal .panel-title>small, .personal .panel-title>small>a { text-decoration:none;}\n.personal .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.personal .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.personal .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.personal .panel-group .panel-heading+.panel-collapse>.list-group, .personal .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.personal .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.personal .panel-body { padding: 0 0 5px;padding: 15px;}\n.personal .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.personal .panel-default { border-color:transparent; background: #fff; \n    padding: 15px 25px;}\n.personal .panel-group .panel+.panel { margin-top: 20px;}\n.employee-mission ul li {\n    margin: 0px 30px 0 30px;\n}\n.employee-mission ul.list-inline{ margin-left: 0;}\n.employee-mission ul li small { display: inline-block; margin: 0 0 0 20px;}\n.employee-mission ul li small .form-control {border: none;\n    font-size: 16px;\n    box-shadow: none;\n    height: auto;\n    padding: 10px 12px;background-color: #fff;}\n.employee-mission ul li small.form-control:focus{ border-color:transparent;box-shadow: none;}\n.date { height: 38px !important; line-height:inherit !important; width:80% !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;vertical-align: top;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:70%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.border-bottom {\n    border-bottom: 1px solid #e8dddd;\n    padding-bottom: 15px;\n}\n.content-field {\n    margin-top: 10px;\n    border: none;\n    box-shadow: none;\n}\n.custom-tables { margin: 0 auto; float: none; padding: 0 30px 0 5px;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 13px;}\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n.custom-tables .table>tbody>tr>td .form-control { border: none; width: 80%; box-shadow: none; color:#3c3a3a;background: #f8f8f8;\nheight: auto; padding: 10px 12px; font-size: 16px;}\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.save-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.label-font {\n    font-size: 15px; padding: 0 0 15px;\n}\n.company-top { margin: 0 0 20px;background: #f8f8f8; padding: 20px 0 15px;}\n.zenwork-checked-border:after {\n    top:-11px;\n    left: -80px;\n    border-right: #83c7a1 4px solid;\n    right: auto;\n    height:55px;\n    border-radius: 4px;\n    padding: 25px;\n}\n.zenwork-customized-ccheckbox { margin: 0 0 0 30px;}\n.zenwork-checked { position: relative;}\n.zenwork-checked:after {\n    content:'';\n    position: absolute;\n    top:8px;\n    left: -48px;\n    border-right: #83c7a1 4px solid;\n    right: auto;\n    height:55px;\n    border-radius: 4px;\n    padding: 25px;\n}\n.error { color: #e44a49; padding: 4px 0 0;}\n.show { margin: 20px 0;}\n.manager-link-main {\n    border-bottom: 1px solid #e8dddd;\n    padding-bottom: 15px;\n    margin: 20px 0 0;\n}\n.manager-link-main label { padding: 0 0 15px; display: block; font-size: 17px;}\n.back { color: #e44a49; font-size: 14px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.html":
/*!****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management']\">\n      <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n    </a>\n    <span class=\"inner-icon-img\">\n      <img src=\"../../../assets/images/Employee Self Service/ess.png\" class=\"zenwork-inner-icon\"\n        alt=\"Company-settings icon\">\n    </span>\n    <small class=\"sub-title\"><b>Employee Self Service</b></small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n<div class=\"main-tabs-heading col-md-11\">\n  <div class=\"hire-wizard\">\n    <ul class=\"nav nav-tabs\">\n      <li class=\"active\"><a data-toggle=\"tab\" href=\"#home\">Employee Front Page Setup</a></li>\n    </ul>\n  </div>\n\n\n\n  <div class=\"tab-content\">\n    <div id=\"home\" class=\"tab-pane fade in active\">\n      <div class=\"personal\">\n        \n      <div class=\"panel-group\" id=\"accordion3\">\n\n        <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseTwo\">\n                    Employee Announcement\n                </a>\n                <span class=\"pull-right\">\n                    <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                    <mat-menu #menu1=\"matMenu\">\n                      <button mat-menu-item (click)=\"companyAdd()\">Add</button>\n                      <button mat-menu-item (click)=\"companyEdit()\">Edit</button>\n                      <button mat-menu-item (click)=\"companyDelete()\">Delete</button>\n                    </mat-menu>\n                  </span> \n              </h4>\n            </div>\n            <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n              <div class=\"panel-body\">\n                \n                <div class=\"company-top\" *ngFor=\"let list of companyAddList; let i =index\">\n\n                  <div class=\"employee-mission\">\n                    <ul class=\"list-unstyled list-inline border-bottom\">\n\n                      <li [ngClass]=\"{'zenwork-checked-border':list.isChecked}\">\n                          <mat-checkbox class=\"checkbox-success\" style=\"display: inline-block;\" [(ngModel)]=\"list.isChecked\" (change)=\"employeCheck($event,list._id)\"></mat-checkbox>\n                        <small>\n                          <input type=\"text\" class=\"form-control\" placeholder=\"Title\" [(ngModel)]=\"list.title\" [readonly]=\"list.edit\">\n                        </small>\n                      </li>\n                      \n                      <li>\n                        <label class=\"label-font\">Schedule Date</label>\n                      </li>\n                      <li style=\"margin:0px\">\n                          <div class=\"date\">\n                              <input matInput [matDatepicker]=\"picker3\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)] =\"list.scheduled_date\" [disabled]=\"list.edit\" [min]=\"pastDate\">\n                              <mat-datepicker #picker3></mat-datepicker>\n                              <button mat-raised-button (click)=\"picker3.open()\">\n                                <span>\n                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                </span>\n                              </button>\n                              <div class=\"clearfix\"></div>\n                            </div>\n                      </li>\n                      \n                      \n                      <li>\n                          <label class=\"label-font\">Expiration Date</label>\n                        </li>\n                        <li style=\"margin:0px\">\n                            <div class=\"date\">\n                                <input matInput [matDatepicker]=\"picker4\" placeholder=\"mm / dd / yy\" class=\"form-control\" [(ngModel)]=\"list.expiration_date\" [disabled]=\"list.edit\" [min]=\"list.scheduled_date\">\n                                <mat-datepicker #picker4></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker4.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                        </li>\n                       \n                      \n                    </ul>\n                  </div>\n\n                  <div class=\"text-area\">\n                      <textarea class=\"form-control\" rows=\"5\" placeholder=\"Description...\" [(ngModel)]=\"list.description\" [readonly]=\"list.edit\"></textarea>\n                    </div>\n                    <div class=\"zenwork-buttons pull-right\" *ngIf=\"!list.buttonDisabled\">\n\n                      <button mat-button class=\"cancel\" *ngIf=\"list._id\" (click)=\"resetClick()\">Reset</button>  \n                      <button mat-button class=\"cancel\" *ngIf=\"!list._id\" (click)=\"cancelClick(i)\">Cancel</button>\n                      <button mat-button class=\"submit-btn\" (click)=\"companySubmit(list)\">Submit</button>\n                    \n                    </div>\n                    <div class=\"clearfix\"></div>\n              </div>\n\n            </div>\n            </div>\n          </div>\n\n      </div>\n      </div>\n    <div>\n      \n      <h4>Additional Content</h4>\n      <div class=\"border-bottom\" style=\"margin-top: 20px;\">\n        <div class=\"col-xs-2\">\n          <label class=\"label-font\">Show Time?</label>\n          <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_time\">\n             <mat-option [value]=\"true\">Yes</mat-option>\n             <mat-option [value]=\"false\">No</mat-option>\n          </mat-select>\n        </div>\n        <div class=\"col-xs-2\">\n            <label class=\"label-font\">Show Benefits?</label>\n            <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_benifits\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n            </mat-select>\n          </div>\n          <div class=\"col-xs-2\">\n              <label class=\"label-font\">Show Training?</label>\n              <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_training\">\n                  <mat-option [value]=\"true\">Yes</mat-option>\n                  <mat-option [value]=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n            <div class=\"col-xs-3\">\n                <label class=\"label-font\">Show Company Policies?</label>\n                <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_company_policies\">\n                    <mat-option [value]=\"true\">Yes</mat-option>\n                    <mat-option [value]=\"false\">No</mat-option>\n                </mat-select>\n              </div>\n              <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"border-bottom\" style=\"margin-top: 20px;\">\n          <div class=\"col-xs-2\">\n            <label class=\"label-font\">Show Birthdays?</label>\n            <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_birthdays\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n            </mat-select>\n          </div>\n          <div class=\"col-xs-3\">\n              <label class=\"label-font\">Select Employees-Birthday</label>\n              <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.birthday_settings\">\n                 <mat-option value=\"My Team Only\">My Team Only</mat-option>\n                 <mat-option value=\"My Department Only\">My Department Only</mat-option>\n                 <mat-option value=\"Entire Company\">Entire Company</mat-option>\n              </mat-select>\n            </div>\n            \n            <div class=\"col-xs-3\">\n                <label class=\"label-font\">Show Work Anniversary?</label>\n                <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_work_anniversary\">\n                    <mat-option [value]=\"true\">Yes</mat-option>\n                    <mat-option [value]=\"false\">No</mat-option>\n                </mat-select>\n              </div>\n              <div class=\"col-xs-3\">\n                  <label class=\"label-font\">Select Employees-Work Anniversary</label>\n                  <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.work_anniversary_settings\">\n                      <mat-option value=\"My Team Only\">My Team Only</mat-option>\n                      <mat-option value=\"My Department Only\">My Department Only</mat-option>\n                      <mat-option value=\"Entire Company\">Entire Company</mat-option>\n                  </mat-select>\n                </div>\n\n                \n                <div class=\"col-xs-2 show\">\n                  \n                    <label class=\"label-font\">Show Company Links?</label>\n                    <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionalContents.show_company_links\">\n                        <mat-option [value]=\"true\">Yes</mat-option>\n                        <mat-option [value]=\"false\">No</mat-option>\n                    </mat-select>\n                  </div>\n                  \n                  <div class=\"clearfix\"></div>\n          </div>\n            <br>\n          <button mat-button class=\"save-btn\" (click)=\"managerSave()\">Save</button>\n\n        <div class=\"row manager-link-main\">\n           \n                <label class=\"label-font\">Company Links</label><br>\n                <div class=\"custom-tables\">\n                    <table class=\"table\">\n                      <thead>\n                        <tr>\n                          <th>\n                          \n                          </th>\n                          <th>Site Name</th>\n                          <th>Site Link</th>\n                          <th>\n                            <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n                            <mat-menu #menu2=\"matMenu\">\n                              <button mat-menu-item (click)=\"companyLinkAdd()\">Add</button>\n                              <button mat-menu-item (click)=\"companyLinkEdit()\">Edit</button>\n                              <button mat-menu-item (click)=\"companyLinksDelete()\">Delete</button>\n                            </mat-menu>\n                          </th>\n                        </tr>\n                      </thead>\n                      <tbody>\n\n                        \n              \n                        <tr *ngFor=\"let list of additionalContents.company_links\">\n\n                          <td [ngClass]=\"{'zenwork-checked' : list.isChecked }\">\n                              <mat-checkbox class=\"zenwork-customized-ccheckbox\" [(ngModel)]=\"list.isChecked\" (ngModelChange)=\"companyCheck(list._id,list)\"></mat-checkbox>\n                          </td>\n                          <td>\n                            <input type=\"text\" class=\"form-control\" placeholder=\"Site Name\" [(ngModel)]=\"list.site_name\" [readonly]=\"list.linkEdit\"\n                            name=\"sitename\" #siteName=\"ngModel\" required>\n                            <p *ngIf=\"!list.site_name && siteName.touched || (!list.site_name && isValid)\" class=\"error\">Enter the Site name</p>\n                          </td>\n                          <td>\n                              <input type=\"text\" class=\"form-control\" placeholder=\"Site Link\" [(ngModel)]=\"list.site_link\" [readonly]=\"list.linkEdit\"\n                              name=\"sitelink\" #siteLink=\"ngModel\" required>\n                              <p *ngIf=\"!list.site_link && siteLink.touched || (!list.site_link && isValid)\" class=\"error\">Enter the Site link</p>\n                          </td>\n                          <td></td>\n                        </tr>\n              \n              \n                    </table>\n                   \n                  </div>\n            \n            </div>\n            <div class=\"row\" style=\"margin:20px 0px;\">\n\n                <button mat-button (click)=\"essBackClick()\" class=\"back\">Back</button>\n          \n                <button mat-button class=\"save-btn pull-right\" (click)=\"companyLinksAdd()\">Save</button>\n          <div class=\"clear-fix\"></div>\n              </div>\n    </div>\n    \n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: EmployeeSelfServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeSelfServiceComponent", function() { return EmployeeSelfServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/employee-service.service */ "./src/app/services/employee-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmployeeSelfServiceComponent = /** @class */ (function () {
    function EmployeeSelfServiceComponent(employeeSelfService, swalService, router) {
        this.employeeSelfService = employeeSelfService;
        this.swalService = swalService;
        this.router = router;
        this.selectData = [];
        this.checkDeleteIds = [];
        this.companyAddList = [
            {
                edit: true,
                buttonDisabled: false,
                resetDisabled: false
            }
        ];
        this.managersData = [
            {
                linkEdit: true
            }
        ];
        this.additionalContents = {
            show_time: '',
            show_benifits: '',
            show_training: '',
            show_company_policies: '',
            show_birthdays: '',
            birthday_settings: '',
            show_work_anniversary: '',
            work_anniversary_settings: '',
            show_company_links: '',
        };
        this.managerLink = {};
        this.isValid = false;
    }
    EmployeeSelfServiceComponent.prototype.ngOnInit = function () {
        this.pastDate = new Date();
        this.companyID = localStorage.getItem("companyId");
        console.log("company ID", this.companyID);
        this.companyID = this.companyID.replace(/^"|"$/g, "");
        console.log("company IDDDD", this.companyID);
        this.userID = localStorage.getItem("employeeId");
        console.log("user id", this.userID);
        this.userID = this.userID.replace(/^"|"$/g, "");
        this.getCompanyAnnouncements();
        this.getManagerData();
    };
    // Author:Suresh M, Date:04-07-19
    // Company Adding
    EmployeeSelfServiceComponent.prototype.companyAdd = function () {
        this.companyAddList.push({ title: '', scheduled_date: '', expiration_date: '', description: '', buttonDisabled: false, });
        console.log("addlist", this.companyAddList);
    };
    // Author:Suresh M, Date:04-07-19
    // CompanyAnnouncements
    EmployeeSelfServiceComponent.prototype.getCompanyAnnouncements = function () {
        var _this = this;
        this.employeeSelfService.getCompanyList(this.companyID)
            .subscribe(function (res) {
            console.log("company listt", res);
            _this.companyAddList = res.data;
            console.log("companyy listtt", _this.companyAddList);
            _this.companyAddList.filter(function (check) {
                check.edit = true;
                check.buttonDisabled = true;
            });
        });
    };
    // Author:Suresh M, Date:04-07-19
    // Company Submit
    EmployeeSelfServiceComponent.prototype.companySubmit = function (list) {
        var _this = this;
        var employeeData = {
            companyId: this.companyID,
            title: list.title,
            description: list.description,
            scheduled_date: list.scheduled_date,
            expiration_date: list.expiration_date
        };
        if (list._id) {
            employeeData._id = list._id;
            this.employeeSelfService.companyEditData(employeeData)
                .subscribe(function (res) {
                console.log("compnay edit", res);
                _this.getCompanyAnnouncements();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Update Data", "Successfully Updated data", "success");
                }
            });
        }
        else {
            this.employeeSelfService.AddCompanyData(employeeData)
                .subscribe(function (res) {
                console.log("adding data", res);
                _this.getCompanyAnnouncements();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Add", "Suceessfully Added", "success");
                }
            }, (function (err) {
                _this.swalService.SweetAlertWithoutConfirmation(err.error.message, '', 'error');
            }));
        }
    };
    // Author:Suresh M, Date:04-07-19
    // Company Edit
    EmployeeSelfServiceComponent.prototype.companyEdit = function () {
        this.selectData = this.companyAddList.filter(function (companyCheck) {
            return companyCheck.isChecked;
        });
        if (this.selectData.length > 1) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one data", "error");
        }
        else if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one", "error");
        }
        else {
            if (this.selectData[0]._id) {
                this.selectData[0].edit = false;
            }
            this.selectData[0].buttonDisabled = false;
        }
    };
    // Author:Suresh M, Date:04-07-19
    // Company Delete
    EmployeeSelfServiceComponent.prototype.companyDelete = function () {
        var _this = this;
        this.selectData = this.companyAddList.filter(function (companyCheck) {
            return companyCheck.isChecked;
        });
        console.log("selectttt", this.selectData);
        if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Select any data", "error");
        }
        else {
            var deleteData = {
                companyId: this.companyID,
                _ids: this.checkDeleteIds
            };
            this.employeeSelfService.deleteCompanayData(deleteData)
                .subscribe(function (res) {
                console.log("delete data", res);
                _this.getCompanyAnnouncements();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully delete data", "success");
                }
            });
        }
    };
    // Author:Suresh M, Date:04-07-19
    // cancelClick
    EmployeeSelfServiceComponent.prototype.cancelClick = function (i) {
        console.log("removeeeee", i);
        this.companyAddList.splice(i, 1);
    };
    EmployeeSelfServiceComponent.prototype.resetClick = function () {
        console.log("resett datataa");
        this.getCompanyAnnouncements();
    };
    // Author:Suresh M, Date:04-07-19
    // Company check ID
    EmployeeSelfServiceComponent.prototype.employeCheck = function (event, id) {
        console.log("check idd", event, id);
        // this.selectCheckId = id;
        if (event.checked == true) {
            this.checkDeleteIds.push(id);
        }
        console.log("push iddddddd", this.checkDeleteIds);
        if (event.checked == false) {
            var index = this.checkDeleteIds.indexOf(id);
            if (index > -1) {
                this.checkDeleteIds.splice(index, 1);
                this.companyAddList[0].buttonDisabled = true;
                this.companyAddList[0].edit = true;
            }
            console.log("uncheck idd", this.checkDeleteIds);
        }
    };
    // Author:Suresh M, Date:04-07-19
    // Manager data
    EmployeeSelfServiceComponent.prototype.getManagerData = function () {
        var _this = this;
        this.employeeSelfService.getManagerSettings(this.companyID)
            .subscribe(function (res) {
            console.log("manager data", res);
            _this.managersData = res.data;
            _this.additionalContents = _this.managersData;
            _this.additionalContents.company_links.filter(function (item) {
                item.linkEdit = true;
            });
        });
    };
    // Author:Suresh M, Date:04-07-19
    // Manager submit
    EmployeeSelfServiceComponent.prototype.managerSave = function () {
        var _this = this;
        var managerAddData = {
            companyId: this.companyID,
            show_time: this.additionalContents.show_time,
            show_benifits: this.additionalContents.show_benifits,
            show_training: this.additionalContents.show_training,
            show_company_policies: this.additionalContents.show_company_policies,
            show_birthdays: this.additionalContents.show_birthdays,
            birthday_settings: this.additionalContents.birthday_settings,
            show_work_anniversary: this.additionalContents.show_work_anniversary,
            work_anniversary_settings: this.additionalContents.work_anniversary_settings,
            show_company_links: this.additionalContents.show_company_links
        };
        this.employeeSelfService.managerSettingsAdd(managerAddData)
            .subscribe(function (res) {
            console.log("add settings", res);
            if (res.status == true) {
                _this.swalService.SweetAlertWithoutConfirmation("Settings", "Successfully Added Settings", "success");
            }
        });
    };
    // Author:Suresh M, Date:04-07-19
    // company links
    EmployeeSelfServiceComponent.prototype.companyLinkAdd = function () {
        this.managerLink = {
            site_name: '',
            site_link: ''
        };
        if (!this.additionalContents.company_links) {
            this.additionalContents.company_links = [];
        }
        this.additionalContents.company_links.push(this.managerLink);
    };
    // Author:Suresh M, Date:04-07-19
    // company link submit
    EmployeeSelfServiceComponent.prototype.companyLinksAdd = function () {
        var _this = this;
        this.isValid = false;
        if (this.selectData && this.selectData.length && this.selectData[0]._id) {
            var companyEditData = {
                companyId: this.companyID,
                site_name: this.managerEditLink.site_name,
                site_link: this.managerEditLink.site_link,
                linkId: this.companyCheckID
            };
            this.employeeSelfService.companyLinkEditData(companyEditData)
                .subscribe(function (res) {
                console.log("update company links", res);
                _this.getManagerData();
                if (res.status == true) {
                    _this.selectData = [];
                    _this.swalService.SweetAlertWithoutConfirmation("Updated", "Successfully update the Data", "success");
                }
            });
        }
        else {
            var postData = [];
            for (var i = 0; i < this.additionalContents.company_links.length; i++) {
                if (!this.additionalContents.company_links[i]._id) {
                    postData.push(this.additionalContents.company_links[i]);
                }
            }
            console.log("postt", postData);
            if (postData.length == 0) {
                this.swalService.SweetAlertWithoutConfirmation("Add", "Please add the data", "error");
            }
            else {
                if (!this.managerLink.site_name) {
                    this.isValid = true;
                }
                else if (!this.managerLink.site_link) {
                    this.isValid = true;
                }
                else {
                    this.employeeSelfService.companyLinkAdding(postData)
                        .subscribe(function (res) {
                        console.log("company link addd data", res);
                        _this.additionalContents.company_links = [];
                        postData = [];
                        _this.getManagerData();
                        _this.isValid = true;
                        if (res.status == true) {
                            _this.swalService.SweetAlertWithoutConfirmation("Company Links", "Successfully Added links", "success");
                        }
                    });
                }
            }
        }
    };
    // Author:Suresh M, Date:04-07-19
    // company check id
    EmployeeSelfServiceComponent.prototype.companyCheck = function (id, list) {
        console.log("idddd", id);
        this.companyCheckID = id;
        this.managerEditLink = list;
        console.log("managerrrr", this.managerEditLink);
    };
    // Author:Suresh M, Date:04-07-19
    // company link edit
    EmployeeSelfServiceComponent.prototype.companyLinkEdit = function () {
        this.selectData = this.additionalContents.company_links.filter(function (companyCheck) {
            return companyCheck.isChecked;
        });
        if (this.selectData.length > 1) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select only one", "error");
        }
        else if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one data", "error");
        }
        else {
            if (this.selectData[0]._id) {
                this.selectData[0].linkEdit = false;
            }
        }
    };
    // Author:Suresh M, Date:04-07-19
    // company link delete
    EmployeeSelfServiceComponent.prototype.companyLinksDelete = function () {
        var _this = this;
        this.selectData = this.additionalContents.company_links.filter(function (companyCheck) {
            return companyCheck.isChecked;
        });
        var deleteData = {
            companyId: this.companyID,
            _ids: this.selectData
        };
        if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Select any Data", "error");
        }
        else {
            this.employeeSelfService.companyLinksDeleteData(deleteData)
                .subscribe(function (res) {
                console.log("company links delete", res);
                _this.getManagerData();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully delete the data", "success");
                }
            });
        }
    };
    EmployeeSelfServiceComponent.prototype.essBackClick = function () {
        this.router.navigate(['/admin/admin-dashboard/employee-management']);
        window.scroll(0, 0);
    };
    EmployeeSelfServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-self-service',
            template: __webpack_require__(/*! ./employee-self-service.component.html */ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.html"),
            styles: [__webpack_require__(/*! ./employee-self-service.component.css */ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.css")]
        }),
        __metadata("design:paramtypes", [_services_employee_service_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeServiceService"], _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], EmployeeSelfServiceComponent);
    return EmployeeSelfServiceComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.module.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.module.ts ***!
  \***********************************************************************************************************/
/*! exports provided: EmployeeSelfServiceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeSelfServiceModule", function() { return EmployeeSelfServiceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _employee_self_service_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-self-service.routing */ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.routing.ts");
/* harmony import */ var _employee_self_service_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-self-service.component */ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var EmployeeSelfServiceModule = /** @class */ (function () {
    function EmployeeSelfServiceModule() {
    }
    EmployeeSelfServiceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _employee_self_service_routing__WEBPACK_IMPORTED_MODULE_2__["EmployeeselfserviceRouting"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]
            ],
            declarations: [_employee_self_service_component__WEBPACK_IMPORTED_MODULE_3__["EmployeeSelfServiceComponent"]]
        })
    ], EmployeeSelfServiceModule);
    return EmployeeSelfServiceModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.routing.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.routing.ts ***!
  \************************************************************************************************************/
/*! exports provided: EmployeeselfserviceRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeselfserviceRouting", function() { return EmployeeselfserviceRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _employee_self_service_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-self-service.component */ "./src/app/admin-dashboard/employee-management/employee-self-service/employee-self-service.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', redirectTo: "/employee-self-service", pathMatch: "full" },
    { path: 'employee-self-service', component: _employee_self_service_component__WEBPACK_IMPORTED_MODULE_2__["EmployeeSelfServiceComponent"] },
];
var EmployeeselfserviceRouting = /** @class */ (function () {
    function EmployeeselfserviceRouting() {
    }
    EmployeeselfserviceRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EmployeeselfserviceRouting);
    return EmployeeselfserviceRouting;
}());



/***/ })

}]);
//# sourceMappingURL=employee-self-service-employee-self-service-module.js.map