(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["off-boarding-landing-off-boarding-landing-module"],{

/***/ "./node_modules/ng2-select/index.js":
/*!******************************************!*\
  !*** ./node_modules/ng2-select/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./select/common */ "./node_modules/ng2-select/select/common.js"));
__export(__webpack_require__(/*! ./select/off-click */ "./node_modules/ng2-select/select/off-click.js"));
__export(__webpack_require__(/*! ./select/select.module */ "./node_modules/ng2-select/select/select.module.js"));
__export(__webpack_require__(/*! ./select/select */ "./node_modules/ng2-select/select/select.js"));
__export(__webpack_require__(/*! ./select/select-item */ "./node_modules/ng2-select/select/select-item.js"));
__export(__webpack_require__(/*! ./select/select-pipes */ "./node_modules/ng2-select/select/select-pipes.js"));


/***/ }),

/***/ "./node_modules/ng2-select/select/common.js":
/*!**************************************************!*\
  !*** ./node_modules/ng2-select/select/common.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function escapeRegexp(queryToEscape) {
    return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
}
exports.escapeRegexp = escapeRegexp;


/***/ }),

/***/ "./node_modules/ng2-select/select/off-click.js":
/*!*****************************************************!*\
  !*** ./node_modules/ng2-select/select/off-click.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var OffClickDirective = (function () {
    function OffClickDirective() {
    }
    /* tslint:enable */
    OffClickDirective.prototype.onClick = function ($event) {
        $event.stopPropagation();
    };
    OffClickDirective.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { if (typeof document !== 'undefined') {
            document.addEventListener('click', _this.offClickHandler);
        } }, 0);
    };
    OffClickDirective.prototype.ngOnDestroy = function () {
        if (typeof document !== 'undefined') {
            document.removeEventListener('click', this.offClickHandler);
        }
    };
    OffClickDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[offClick]'
                },] },
    ];
    /** @nocollapse */
    OffClickDirective.ctorParameters = function () { return []; };
    OffClickDirective.propDecorators = {
        'offClickHandler': [{ type: core_1.Input, args: ['offClick',] },],
        'onClick': [{ type: core_1.HostListener, args: ['click', ['$event'],] },],
    };
    return OffClickDirective;
}());
exports.OffClickDirective = OffClickDirective;


/***/ }),

/***/ "./node_modules/ng2-select/select/select-item.js":
/*!*******************************************************!*\
  !*** ./node_modules/ng2-select/select/select-item.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var SelectItem = (function () {
    function SelectItem(source) {
        var _this = this;
        if (typeof source === 'string') {
            this.id = this.text = source;
        }
        if (typeof source === 'object') {
            this.id = source.id || source.text;
            this.text = source.text;
            if (source.children && source.text) {
                this.children = source.children.map(function (c) {
                    var r = new SelectItem(c);
                    r.parent = _this;
                    return r;
                });
                this.text = source.text;
            }
        }
    }
    SelectItem.prototype.fillChildrenHash = function (optionsMap, startIndex) {
        var i = startIndex;
        this.children.map(function (child) {
            optionsMap.set(child.id, i++);
        });
        return i;
    };
    SelectItem.prototype.hasChildren = function () {
        return this.children && this.children.length > 0;
    };
    SelectItem.prototype.getSimilar = function () {
        var r = new SelectItem(false);
        r.id = this.id;
        r.text = this.text;
        r.parent = this.parent;
        return r;
    };
    return SelectItem;
}());
exports.SelectItem = SelectItem;


/***/ }),

/***/ "./node_modules/ng2-select/select/select-pipes.js":
/*!********************************************************!*\
  !*** ./node_modules/ng2-select/select/select-pipes.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! ./common */ "./node_modules/ng2-select/select/common.js");
var HighlightPipe = (function () {
    function HighlightPipe() {
    }
    HighlightPipe.prototype.transform = function (value, query) {
        if (query.length < 1) {
            return value;
        }
        if (query) {
            var tagRE = new RegExp('<[^<>]*>', 'ig');
            // get ist of tags
            var tagList = value.match(tagRE);
            // Replace tags with token
            var tmpValue = value.replace(tagRE, '$!$');
            // Replace search words
            value = tmpValue.replace(new RegExp(common_1.escapeRegexp(query), 'gi'), '<strong>$&</strong>');
            // Reinsert HTML
            for (var i = 0; value.indexOf('$!$') > -1; i++) {
                value = value.replace('$!$', tagList[i]);
            }
        }
        return value;
    };
    HighlightPipe.decorators = [
        { type: core_1.Pipe, args: [{ name: 'highlight' },] },
    ];
    /** @nocollapse */
    HighlightPipe.ctorParameters = function () { return []; };
    return HighlightPipe;
}());
exports.HighlightPipe = HighlightPipe;
function stripTags(input) {
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
    var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, '');
}
exports.stripTags = stripTags;


/***/ }),

/***/ "./node_modules/ng2-select/select/select.js":
/*!**************************************************!*\
  !*** ./node_modules/ng2-select/select/select.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var platform_browser_1 = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var select_item_1 = __webpack_require__(/*! ./select-item */ "./node_modules/ng2-select/select/select-item.js");
var select_pipes_1 = __webpack_require__(/*! ./select-pipes */ "./node_modules/ng2-select/select/select-pipes.js");
var common_1 = __webpack_require__(/*! ./common */ "./node_modules/ng2-select/select/common.js");
var styles = "\n  .ui-select-toggle {\n    position: relative;\n  }\n\n  /* Fix caret going into new line in Firefox */\n  .ui-select-placeholder {\n    float: left;\n  }\n  \n  /* Fix Bootstrap dropdown position when inside a input-group */\n  .input-group > .dropdown {\n    /* Instead of relative */\n    position: static;\n  }\n  \n  .ui-select-match > .btn {\n    /* Instead of center because of .btn */\n    text-align: left !important;\n  }\n  \n  .ui-select-match > .caret {\n    position: absolute;\n    top: 45%;\n    right: 15px;\n  }\n  \n  .ui-disabled {\n    background-color: #eceeef;\n    border-radius: 4px;\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    z-index: 5;\n    opacity: 0.6;\n    top: 0;\n    left: 0;\n    cursor: not-allowed;\n  }\n  \n  .ui-select-choices {\n    width: 100%;\n    height: auto;\n    max-height: 200px;\n    overflow-x: hidden;\n    margin-top: 0;\n  }\n  \n  .ui-select-multiple .ui-select-choices {\n    margin-top: 1px;\n  }\n  .ui-select-choices-row>a {\n      display: block;\n      padding: 3px 20px;\n      clear: both;\n      font-weight: 400;\n      line-height: 1.42857143;\n      color: #333;\n      white-space: nowrap;\n  }\n  .ui-select-choices-row.active>a {\n      color: #fff;\n      text-decoration: none;\n      outline: 0;\n      background-color: #428bca;\n  }\n  \n  .ui-select-multiple {\n    height: auto;\n    padding:3px 3px 0 3px;\n  }\n  \n  .ui-select-multiple input.ui-select-search {\n    background-color: transparent !important; /* To prevent double background when disabled */\n    border: none;\n    outline: none;\n    box-shadow: none;\n    height: 1.6666em;\n    padding: 0;\n    margin-bottom: 3px;\n    \n  }\n  .ui-select-match .close {\n      font-size: 1.6em;\n      line-height: 0.75;\n  }\n  \n  .ui-select-multiple .ui-select-match-item {\n    outline: 0;\n    margin: 0 3px 3px 0;\n  }\n  .ui-select-toggle > .caret {\n      position: absolute;\n      height: 10px;\n      top: 50%;\n      right: 10px;\n      margin-top: -2px;\n  }\n";
var SelectComponent = (function () {
    function SelectComponent(element, sanitizer) {
        this.sanitizer = sanitizer;
        this.allowClear = false;
        this.placeholder = '';
        this.idField = 'id';
        this.textField = 'text';
        this.childrenField = 'children';
        this.multiple = false;
        this.data = new core_1.EventEmitter();
        this.selected = new core_1.EventEmitter();
        this.removed = new core_1.EventEmitter();
        this.typed = new core_1.EventEmitter();
        this.opened = new core_1.EventEmitter();
        this.options = [];
        this.itemObjects = [];
        this.onChange = Function.prototype;
        this.onTouched = Function.prototype;
        this.inputMode = false;
        this._optionsOpened = false;
        this.inputValue = '';
        this._items = [];
        this._disabled = false;
        this._active = [];
        this.element = element;
        this.clickedOutside = this.clickedOutside.bind(this);
    }
    Object.defineProperty(SelectComponent.prototype, "items", {
        set: function (value) {
            var _this = this;
            if (!value) {
                this._items = this.itemObjects = [];
            }
            else {
                this._items = value.filter(function (item) {
                    if ((typeof item === 'string') || (typeof item === 'object' && item && item[_this.textField] && item[_this.idField])) {
                        return item;
                    }
                });
                this.itemObjects = this._items.map(function (item) { return (typeof item === 'string' ? new select_item_1.SelectItem(item) : new select_item_1.SelectItem({ id: item[_this.idField], text: item[_this.textField], children: item[_this.childrenField] })); });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SelectComponent.prototype, "disabled", {
        get: function () {
            return this._disabled;
        },
        set: function (value) {
            this._disabled = value;
            if (this._disabled === true) {
                this.hideOptions();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SelectComponent.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (selectedItems) {
            var _this = this;
            if (!selectedItems || selectedItems.length === 0) {
                this._active = [];
            }
            else {
                var areItemsStrings_1 = typeof selectedItems[0] === 'string';
                this._active = selectedItems.map(function (item) {
                    var data = areItemsStrings_1
                        ? item
                        : { id: item[_this.idField], text: item[_this.textField] };
                    return new select_item_1.SelectItem(data);
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SelectComponent.prototype, "optionsOpened", {
        get: function () {
            return this._optionsOpened;
        },
        set: function (value) {
            this._optionsOpened = value;
            this.opened.emit(value);
        },
        enumerable: true,
        configurable: true
    });
    SelectComponent.prototype.sanitize = function (html) {
        return this.sanitizer.bypassSecurityTrustHtml(html);
    };
    SelectComponent.prototype.inputEvent = function (e, isUpMode) {
        if (isUpMode === void 0) { isUpMode = false; }
        // tab
        if (e.keyCode === 9) {
            return;
        }
        if (isUpMode && (e.keyCode === 37 || e.keyCode === 39 || e.keyCode === 38 ||
            e.keyCode === 40 || e.keyCode === 13)) {
            e.preventDefault();
            return;
        }
        // backspace
        if (!isUpMode && e.keyCode === 8) {
            var el = this.element.nativeElement
                .querySelector('div.ui-select-container > input');
            if (!el.value || el.value.length <= 0) {
                if (this.active.length > 0) {
                    this.remove(this.active[this.active.length - 1]);
                }
                e.preventDefault();
            }
        }
        // esc
        if (!isUpMode && e.keyCode === 27) {
            this.hideOptions();
            this.element.nativeElement.children[0].focus();
            e.preventDefault();
            return;
        }
        // del
        if (!isUpMode && e.keyCode === 46) {
            if (this.active.length > 0) {
                this.remove(this.active[this.active.length - 1]);
            }
            e.preventDefault();
        }
        // left
        if (!isUpMode && e.keyCode === 37 && this._items.length > 0) {
            this.behavior.first();
            e.preventDefault();
            return;
        }
        // right
        if (!isUpMode && e.keyCode === 39 && this._items.length > 0) {
            this.behavior.last();
            e.preventDefault();
            return;
        }
        // up
        if (!isUpMode && e.keyCode === 38) {
            this.behavior.prev();
            e.preventDefault();
            return;
        }
        // down
        if (!isUpMode && e.keyCode === 40) {
            this.behavior.next();
            e.preventDefault();
            return;
        }
        // enter
        if (!isUpMode && e.keyCode === 13) {
            if (this.active.indexOf(this.activeOption) === -1) {
                this.selectActiveMatch();
                this.behavior.next();
            }
            e.preventDefault();
            return;
        }
        var target = e.target || e.srcElement;
        if (target && target.value) {
            this.inputValue = target.value;
            this.behavior.filter(new RegExp(common_1.escapeRegexp(this.inputValue), 'ig'));
            this.doEvent('typed', this.inputValue);
        }
        else {
            this.open();
        }
    };
    SelectComponent.prototype.ngOnInit = function () {
        this.behavior = (this.firstItemHasChildren) ?
            new ChildrenBehavior(this) : new GenericBehavior(this);
    };
    SelectComponent.prototype.remove = function (item) {
        if (this._disabled === true) {
            return;
        }
        if (this.multiple === true && this.active) {
            var index = this.active.indexOf(item);
            this.active.splice(index, 1);
            this.data.next(this.active);
            this.doEvent('removed', item);
        }
        if (this.multiple === false) {
            this.active = [];
            this.data.next(this.active);
            this.doEvent('removed', item);
        }
    };
    SelectComponent.prototype.doEvent = function (type, value) {
        if (this[type] && value) {
            this[type].next(value);
        }
        this.onTouched();
        if (type === 'selected' || type === 'removed') {
            this.onChange(this.active);
        }
    };
    SelectComponent.prototype.clickedOutside = function () {
        this.inputMode = false;
        this.optionsOpened = false;
    };
    Object.defineProperty(SelectComponent.prototype, "firstItemHasChildren", {
        get: function () {
            return this.itemObjects[0] && this.itemObjects[0].hasChildren();
        },
        enumerable: true,
        configurable: true
    });
    SelectComponent.prototype.writeValue = function (val) {
        this.active = val;
        this.data.emit(this.active);
    };
    SelectComponent.prototype.validate = function (c) {
        var controlValue = c ? c.value : [];
        if (!controlValue) {
            controlValue = [];
        }
        return (controlValue.length > 0) ? null : {
            ng2SelectEmptyError: {
                valid: false
            }
        };
    };
    SelectComponent.prototype.registerOnChange = function (fn) { this.onChange = fn; };
    SelectComponent.prototype.registerOnTouched = function (fn) { this.onTouched = fn; };
    SelectComponent.prototype.matchClick = function (e) {
        if (this._disabled === true) {
            return;
        }
        this.inputMode = !this.inputMode;
        if (this.inputMode === true && ((this.multiple === true && e) || this.multiple === false)) {
            this.focusToInput();
            this.open();
        }
    };
    SelectComponent.prototype.mainClick = function (event) {
        if (this.inputMode === true || this._disabled === true) {
            return;
        }
        if (event.keyCode === 46) {
            event.preventDefault();
            this.inputEvent(event);
            return;
        }
        if (event.keyCode === 8) {
            event.preventDefault();
            this.inputEvent(event, true);
            return;
        }
        if (event.keyCode === 9 || event.keyCode === 13 ||
            event.keyCode === 27 || (event.keyCode >= 37 && event.keyCode <= 40)) {
            event.preventDefault();
            return;
        }
        this.inputMode = true;
        var value = String
            .fromCharCode(96 <= event.keyCode && event.keyCode <= 105 ? event.keyCode - 48 : event.keyCode)
            .toLowerCase();
        this.focusToInput(value);
        this.open();
        var target = event.target || event.srcElement;
        target.value = value;
        this.inputEvent(event);
    };
    SelectComponent.prototype.selectActive = function (value) {
        this.activeOption = value;
    };
    SelectComponent.prototype.isActive = function (value) {
        return this.activeOption.id === value.id;
    };
    SelectComponent.prototype.removeClick = function (value, event) {
        event.stopPropagation();
        this.remove(value);
    };
    SelectComponent.prototype.focusToInput = function (value) {
        var _this = this;
        if (value === void 0) { value = ''; }
        setTimeout(function () {
            var el = _this.element.nativeElement.querySelector('div.ui-select-container > input');
            if (el) {
                el.focus();
                el.value = value;
            }
        }, 0);
    };
    SelectComponent.prototype.open = function () {
        var _this = this;
        this.options = this.itemObjects
            .filter(function (option) { return (_this.multiple === false ||
            _this.multiple === true && !_this.active.find(function (o) { return option.text === o.text; })); });
        if (this.options.length > 0) {
            this.behavior.first();
        }
        this.optionsOpened = true;
    };
    SelectComponent.prototype.hideOptions = function () {
        this.inputMode = false;
        this.optionsOpened = false;
    };
    SelectComponent.prototype.selectActiveMatch = function () {
        this.selectMatch(this.activeOption);
    };
    SelectComponent.prototype.selectMatch = function (value, e) {
        if (e === void 0) { e = void 0; }
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        if (this.options.length <= 0) {
            return;
        }
        if (this.multiple === true) {
            this.active.push(value);
            this.data.next(this.active);
        }
        if (this.multiple === false) {
            this.active[0] = value;
            this.data.next(this.active[0]);
        }
        this.doEvent('selected', value);
        this.hideOptions();
        if (this.multiple === true) {
            this.focusToInput('');
        }
        else {
            this.focusToInput(select_pipes_1.stripTags(value.text));
            this.element.nativeElement.querySelector('.ui-select-container').focus();
        }
    };
    SelectComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'ng-select',
                    styles: [styles],
                    providers: [
                        {
                            provide: forms_1.NG_VALUE_ACCESSOR,
                            /* tslint:disable */
                            useExisting: core_1.forwardRef(function () { return SelectComponent; }),
                            /* tslint:enable */
                            multi: true
                        },
                        {
                            provide: forms_1.NG_VALIDATORS,
                            useExisting: core_1.forwardRef(function () { return SelectComponent; }),
                            multi: true,
                        }
                    ],
                    template: "\n  <div tabindex=\"0\"\n     *ngIf=\"multiple === false\"\n     (keyup)=\"mainClick($event)\"\n     [offClick]=\"clickedOutside\"\n     class=\"ui-select-container dropdown open\">\n    <div [ngClass]=\"{'ui-disabled': disabled}\"></div>\n    <div class=\"ui-select-match\"\n         *ngIf=\"!inputMode\">\n      <span tabindex=\"-1\"\n          class=\"btn btn-default btn-secondary form-control ui-select-toggle\"\n          (click)=\"matchClick($event)\"\n          style=\"outline: 0;\">\n        <span *ngIf=\"active.length <= 0\" class=\"ui-select-placeholder text-muted\">{{placeholder}}</span>\n        <span *ngIf=\"active.length > 0\" class=\"ui-select-match-text pull-left\"\n              [ngClass]=\"{'ui-select-allow-clear': allowClear && active.length > 0}\"\n              [innerHTML]=\"sanitize(active[0].text)\"></span>\n        <i class=\"dropdown-toggle pull-right\"></i>\n        <i class=\"caret pull-right\"></i>\n        <a *ngIf=\"allowClear && active.length>0\" class=\"btn btn-xs btn-link pull-right\" style=\"margin-right: 10px; padding: 0;\" (click)=\"removeClick(active[0], $event)\">\n           <i class=\"glyphicon glyphicon-remove\"></i>\n        </a>\n      </span>\n    </div>\n    <input type=\"text\" autocomplete=\"false\" tabindex=\"-1\"\n           (keydown)=\"inputEvent($event)\"\n           (keyup)=\"inputEvent($event, true)\"\n           [disabled]=\"disabled\"\n           class=\"form-control ui-select-search\"\n           *ngIf=\"inputMode\"\n           placeholder=\"{{active.length <= 0 ? placeholder : ''}}\">\n     <!-- options template -->\n     <ul *ngIf=\"optionsOpened && options && options.length > 0 && !firstItemHasChildren\"\n          class=\"ui-select-choices dropdown-menu\" role=\"menu\">\n        <li *ngFor=\"let o of options\" role=\"menuitem\">\n          <div class=\"ui-select-choices-row\"\n               [class.active]=\"isActive(o)\"\n               (mouseenter)=\"selectActive(o)\"\n               (click)=\"selectMatch(o, $event)\">\n            <a href=\"javascript:void(0)\" class=\"dropdown-item\">\n              <div [innerHtml]=\"sanitize(o.text | highlight:inputValue)\"></div>\n            </a>\n          </div>\n        </li>\n      </ul>\n  \n      <ul *ngIf=\"optionsOpened && options && options.length > 0 && firstItemHasChildren\"\n          class=\"ui-select-choices dropdown-menu\" role=\"menu\">\n        <li *ngFor=\"let c of options; let index=index\" role=\"menuitem\">\n          <div class=\"divider dropdown-divider\" *ngIf=\"index > 0\"></div>\n          <div class=\"dropdown-header\">{{c.text}}</div>\n  \n          <div *ngFor=\"let o of c.children\"\n               class=\"ui-select-choices-row\"\n               [class.active]=\"isActive(o)\"\n               (mouseenter)=\"selectActive(o)\"\n               (click)=\"selectMatch(o, $event)\"\n               [ngClass]=\"{'active': isActive(o)}\">\n            <a href=\"javascript:void(0)\" class=\"dropdown-item\">\n              <div [innerHtml]=\"sanitize(o.text | highlight:inputValue)\"></div>\n            </a>\n          </div>\n        </li>\n      </ul>\n  </div>\n\n  <div tabindex=\"0\"\n     *ngIf=\"multiple === true\"\n     (keyup)=\"mainClick($event)\"\n     (focus)=\"focusToInput('')\"\n     [offClick]=\"clickedOutside\"\n     class=\"ui-select-container ui-select-multiple dropdown form-control open\">\n    <div [ngClass]=\"{'ui-disabled': disabled}\"></div>\n    <span class=\"ui-select-match\">\n        <span *ngFor=\"let a of active\">\n            <span class=\"ui-select-match-item btn btn-default btn-secondary btn-xs\"\n                  tabindex=\"-1\"\n                  type=\"button\"\n                  [ngClass]=\"{'btn-default': true}\">\n               <a class=\"close\"\n                  style=\"margin-left: 5px; padding: 0;\"\n                  (click)=\"removeClick(a, $event)\">&times;</a>\n               <span [innerHtml]=\"sanitize(a.text)\"></span>\n           </span>\n        </span>\n    </span>\n    <input type=\"text\"\n           (keydown)=\"inputEvent($event)\"\n           (keyup)=\"inputEvent($event, true)\"\n           (click)=\"matchClick($event)\"\n           [disabled]=\"disabled\"\n           autocomplete=\"false\"\n           autocorrect=\"off\"\n           autocapitalize=\"off\"\n           spellcheck=\"false\"\n           class=\"form-control ui-select-search\"\n           placeholder=\"{{active.length <= 0 ? placeholder : ''}}\"\n           role=\"combobox\">\n     <!-- options template -->\n     <ul *ngIf=\"optionsOpened && options && options.length > 0 && !firstItemHasChildren\"\n          class=\"ui-select-choices dropdown-menu\" role=\"menu\">\n        <li *ngFor=\"let o of options\" role=\"menuitem\">\n          <div class=\"ui-select-choices-row\"\n               [class.active]=\"isActive(o)\"\n               (mouseenter)=\"selectActive(o)\"\n               (click)=\"selectMatch(o, $event)\">\n            <a href=\"javascript:void(0)\" class=\"dropdown-item\">\n              <div [innerHtml]=\"sanitize(o.text | highlight:inputValue)\"></div>\n            </a>\n          </div>\n        </li>\n      </ul>\n  \n      <ul *ngIf=\"optionsOpened && options && options.length > 0 && firstItemHasChildren\"\n          class=\"ui-select-choices dropdown-menu\" role=\"menu\">\n        <li *ngFor=\"let c of options; let index=index\" role=\"menuitem\">\n          <div class=\"divider dropdown-divider\" *ngIf=\"index > 0\"></div>\n          <div class=\"dropdown-header\">{{c.text}}</div>\n  \n          <div *ngFor=\"let o of c.children\"\n               class=\"ui-select-choices-row\"\n               [class.active]=\"isActive(o)\"\n               (mouseenter)=\"selectActive(o)\"\n               (click)=\"selectMatch(o, $event)\"\n               [ngClass]=\"{'active': isActive(o)}\">\n            <a href=\"javascript:void(0)\" class=\"dropdown-item\">\n              <div [innerHtml]=\"sanitize(o.text | highlight:inputValue)\"></div>\n            </a>\n          </div>\n        </li>\n      </ul>\n  </div>\n  "
                },] },
    ];
    /** @nocollapse */
    SelectComponent.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
        { type: platform_browser_1.DomSanitizer, },
    ]; };
    SelectComponent.propDecorators = {
        'allowClear': [{ type: core_1.Input },],
        'placeholder': [{ type: core_1.Input },],
        'idField': [{ type: core_1.Input },],
        'textField': [{ type: core_1.Input },],
        'childrenField': [{ type: core_1.Input },],
        'multiple': [{ type: core_1.Input },],
        'items': [{ type: core_1.Input },],
        'disabled': [{ type: core_1.Input },],
        'active': [{ type: core_1.Input },],
        'data': [{ type: core_1.Output },],
        'selected': [{ type: core_1.Output },],
        'removed': [{ type: core_1.Output },],
        'typed': [{ type: core_1.Output },],
        'opened': [{ type: core_1.Output },],
    };
    return SelectComponent;
}());
exports.SelectComponent = SelectComponent;
var Behavior = (function () {
    function Behavior(actor) {
        this.optionsMap = new Map();
        this.actor = actor;
    }
    Behavior.prototype.fillOptionsMap = function () {
        var _this = this;
        this.optionsMap.clear();
        var startPos = 0;
        this.actor.itemObjects
            .map(function (item) {
            startPos = item.fillChildrenHash(_this.optionsMap, startPos);
        });
    };
    Behavior.prototype.ensureHighlightVisible = function (optionsMap) {
        if (optionsMap === void 0) { optionsMap = void 0; }
        var container = this.actor.element.nativeElement.querySelector('.ui-select-choices-content');
        if (!container) {
            return;
        }
        var choices = container.querySelectorAll('.ui-select-choices-row');
        if (choices.length < 1) {
            return;
        }
        var activeIndex = this.getActiveIndex(optionsMap);
        if (activeIndex < 0) {
            return;
        }
        var highlighted = choices[activeIndex];
        if (!highlighted) {
            return;
        }
        var posY = highlighted.offsetTop + highlighted.clientHeight - container.scrollTop;
        var height = container.offsetHeight;
        if (posY > height) {
            container.scrollTop += posY - height;
        }
        else if (posY < highlighted.clientHeight) {
            container.scrollTop -= highlighted.clientHeight - posY;
        }
    };
    Behavior.prototype.getActiveIndex = function (optionsMap) {
        if (optionsMap === void 0) { optionsMap = void 0; }
        var ai = this.actor.options.indexOf(this.actor.activeOption);
        if (ai < 0 && optionsMap !== void 0) {
            ai = optionsMap.get(this.actor.activeOption.id);
        }
        return ai;
    };
    return Behavior;
}());
exports.Behavior = Behavior;
var GenericBehavior = (function (_super) {
    __extends(GenericBehavior, _super);
    function GenericBehavior(actor) {
        return _super.call(this, actor) || this;
    }
    GenericBehavior.prototype.first = function () {
        this.actor.activeOption = this.actor.options[0];
        _super.prototype.ensureHighlightVisible.call(this);
    };
    GenericBehavior.prototype.last = function () {
        this.actor.activeOption = this.actor.options[this.actor.options.length - 1];
        _super.prototype.ensureHighlightVisible.call(this);
    };
    GenericBehavior.prototype.prev = function () {
        var index = this.actor.options.indexOf(this.actor.activeOption);
        this.actor.activeOption = this.actor
            .options[index - 1 < 0 ? this.actor.options.length - 1 : index - 1];
        _super.prototype.ensureHighlightVisible.call(this);
    };
    GenericBehavior.prototype.next = function () {
        var index = this.actor.options.indexOf(this.actor.activeOption);
        this.actor.activeOption = this.actor
            .options[index + 1 > this.actor.options.length - 1 ? 0 : index + 1];
        _super.prototype.ensureHighlightVisible.call(this);
    };
    GenericBehavior.prototype.filter = function (query) {
        var _this = this;
        var options = this.actor.itemObjects
            .filter(function (option) {
            return select_pipes_1.stripTags(option.text).match(query) &&
                (_this.actor.multiple === false ||
                    (_this.actor.multiple === true && _this.actor.active.map(function (item) { return item.id; }).indexOf(option.id) < 0));
        });
        this.actor.options = options;
        if (this.actor.options.length > 0) {
            this.actor.activeOption = this.actor.options[0];
            _super.prototype.ensureHighlightVisible.call(this);
        }
    };
    return GenericBehavior;
}(Behavior));
exports.GenericBehavior = GenericBehavior;
var ChildrenBehavior = (function (_super) {
    __extends(ChildrenBehavior, _super);
    function ChildrenBehavior(actor) {
        return _super.call(this, actor) || this;
    }
    ChildrenBehavior.prototype.first = function () {
        this.actor.activeOption = this.actor.options[0].children[0];
        this.fillOptionsMap();
        this.ensureHighlightVisible(this.optionsMap);
    };
    ChildrenBehavior.prototype.last = function () {
        this.actor.activeOption =
            this.actor
                .options[this.actor.options.length - 1]
                .children[this.actor.options[this.actor.options.length - 1].children.length - 1];
        this.fillOptionsMap();
        this.ensureHighlightVisible(this.optionsMap);
    };
    ChildrenBehavior.prototype.prev = function () {
        var _this = this;
        var indexParent = this.actor.options
            .findIndex(function (option) { return _this.actor.activeOption.parent && _this.actor.activeOption.parent.id === option.id; });
        var index = this.actor.options[indexParent].children
            .findIndex(function (option) { return _this.actor.activeOption && _this.actor.activeOption.id === option.id; });
        this.actor.activeOption = this.actor.options[indexParent].children[index - 1];
        if (!this.actor.activeOption) {
            if (this.actor.options[indexParent - 1]) {
                this.actor.activeOption = this.actor
                    .options[indexParent - 1]
                    .children[this.actor.options[indexParent - 1].children.length - 1];
            }
        }
        if (!this.actor.activeOption) {
            this.last();
        }
        this.fillOptionsMap();
        this.ensureHighlightVisible(this.optionsMap);
    };
    ChildrenBehavior.prototype.next = function () {
        var _this = this;
        var indexParent = this.actor.options
            .findIndex(function (option) { return _this.actor.activeOption.parent && _this.actor.activeOption.parent.id === option.id; });
        var index = this.actor.options[indexParent].children
            .findIndex(function (option) { return _this.actor.activeOption && _this.actor.activeOption.id === option.id; });
        this.actor.activeOption = this.actor.options[indexParent].children[index + 1];
        if (!this.actor.activeOption) {
            if (this.actor.options[indexParent + 1]) {
                this.actor.activeOption = this.actor.options[indexParent + 1].children[0];
            }
        }
        if (!this.actor.activeOption) {
            this.first();
        }
        this.fillOptionsMap();
        this.ensureHighlightVisible(this.optionsMap);
    };
    ChildrenBehavior.prototype.filter = function (query) {
        var options = [];
        var optionsMap = new Map();
        var startPos = 0;
        for (var _i = 0, _a = this.actor.itemObjects; _i < _a.length; _i++) {
            var si = _a[_i];
            var children = si.children.filter(function (option) { return query.test(option.text); });
            startPos = si.fillChildrenHash(optionsMap, startPos);
            if (children.length > 0) {
                var newSi = si.getSimilar();
                newSi.children = children;
                options.push(newSi);
            }
        }
        this.actor.options = options;
        if (this.actor.options.length > 0) {
            this.actor.activeOption = this.actor.options[0].children[0];
            _super.prototype.ensureHighlightVisible.call(this, optionsMap);
        }
    };
    return ChildrenBehavior;
}(Behavior));
exports.ChildrenBehavior = ChildrenBehavior;


/***/ }),

/***/ "./node_modules/ng2-select/select/select.module.js":
/*!*********************************************************!*\
  !*** ./node_modules/ng2-select/select/select.module.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var select_1 = __webpack_require__(/*! ./select */ "./node_modules/ng2-select/select/select.js");
var select_pipes_1 = __webpack_require__(/*! ./select-pipes */ "./node_modules/ng2-select/select/select-pipes.js");
var off_click_1 = __webpack_require__(/*! ./off-click */ "./node_modules/ng2-select/select/off-click.js");
var SelectModule = (function () {
    function SelectModule() {
    }
    SelectModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [common_1.CommonModule],
                    declarations: [select_1.SelectComponent, select_pipes_1.HighlightPipe, off_click_1.OffClickDirective],
                    exports: [select_1.SelectComponent, select_pipes_1.HighlightPipe, off_click_1.OffClickDirective]
                },] },
    ];
    /** @nocollapse */
    SelectModule.ctorParameters = function () { return []; };
    return SelectModule;
}());
exports.SelectModule = SelectModule;


/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.css":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-currentpage{\n    padding: 25px 0 0 !important;\n}\n.zenwork-margin-zero b{\n    font-size: 14px!important;\n    color: #404040;\n}\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding:6px 15px;\n    font-size: 14px;\n    margin:  0 10px 0 0;color: #000;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.mr-7 .fa{ font-size:15px;}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.sub-title{\n    display: inline-block;\n    margin: 5px 0 0;\n    vertical-align:sub;\n}\n.zenwork-inner-icon{\n    width: 30px;\n    height: auto;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important;\n\n}\n.zenwork-margin-ten-zero {\n    margin: 15px 0px !important;\n}\n.template-name{\n    margin:20px 0 0 0;\n}\n.template-name label{\n    line-height: 40px;\n}\n.text-field{border: none; width: 25%;padding: 11px 12px; height: 40px; box-shadow: none; background: #fff;}\n.standard { margin: 0 auto; float: none;}\n.off-boarding{ display: block; margin: 20px 0 0;}\n.tasks{ display: block;}\n.tasks-in{ position: relative;background: #edf7fe; padding: 14px 20px; margin: 20px 0 0;}\n.tasks h3{color: #3a3a3a;\n    margin: 0;\n    font-size: 18px;}\n.top-menu{position: absolute; top:-3px; right: 0;}\n.tasks ul { display: block; background: #fff; padding:0 20px;}\n.tasks ul li { display: block; padding: 20px 0;}\n.tasks ul li h4{color: #3a3a3a;margin: 0 0 15px;font-size: 16px; font-weight:600;}\n.task-names{ border-bottom:#ccc 1px solid; padding: 12px 0;}\n.task-names span { display: block;color:#777474;font-size:14px;}\n.task-names small { display: block;color: #ccc;font-size:12px; margin: 5px 0 0 26px;}\n.standard .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.standard .btn.cancel { color:#e44a49; background:transparent; padding:10px 0 0 30px;font-size: 16px;}\n.termination-border {border-top:none !important; margin:30px 30px 30px 0; padding: 0;}\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;\n}\n.add-category{\n    cursor: pointer;\n}\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"standard col-md-11\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <a routerLink=\"/admin/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing\">\n        <button class=\"btn zenwork-customized-back-btn\">\n          <span class=\"green mr-7\">\n            <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n          </span>Back\n        </button>\n      </a>\n      <span class=\"inner-icon-img\">\n        <img src=\"../../../assets/images/Offboarding/Group 831.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </span>\n      <small class=\"sub-title\"><b>Offboarding Template</b></small>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"off-boarding\">\n\n    <div class=\"heading\">\n      <h4>View / Edit Custom Offboarding Template</h4>\n    </div>\n\n    <div class=\"template-name\">\n      <label>Offboarding Template Name</label><br>\n\n      <input type=\"text\" class=\"form-control text-field\" [(ngModel)]=\"customOffboardingtemplateName\"\n        placeholder=\"Template Name\" name=\"templateName\" #templateName=\"ngModel\" required>\n      <p *ngIf=\"!customOffboardingtemplateName && templateName.touched || (!customOffboardingtemplateName && isValidMainSubmit)\"\n        class=\"error\"> Please fill this field</p>\n\n    </div>\n\n    <div class=\"tasks\">\n\n      <div class=\"tasks-in\">\n        <h3>Offboarding Tasks</h3>\n        <div class=\"top-menu\">\n          <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\">\n            <button  *ngIf=\"!isDisabled\" mat-menu-item>\n              <a data-toggle=\"modal\" data-target=\"#myModal\">Add</a>\n            </button>\n            <button  *ngIf=\"isDisabled\" mat-menu-item>\n              <a data-toggle=\"modal\" data-target=\"#myModal1\">Edit</a>\n            </button>\n            <button  *ngIf=\"isDisabled\" mat-menu-item>\n              <a (click)=\"deleteTask()\">Delete</a>\n            </button>\n          </mat-menu>\n        </div>\n      </div>\n\n      <ul *ngIf=\"customTemplateSend\">\n        <span>\n          <li *ngFor=\"let task of getKeys((customTemplateSend.offBoardingTasks))\">\n\n            <h4 *ngIf=\"customTemplateSend['offBoardingTasks'][task].length\"  >{{task}}</h4>\n\n            <div class=\"task-names\" *ngFor=\"let data of customTemplateSend['offBoardingTasks'][task]\">\n\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\"\n                (ngModelChange)=\"singleTaskChoose(task,data.taskName,data.isChecked)\"><span>{{data.taskName}}</span>\n              </mat-checkbox>\n\n              <small>{{data.assigneeEmail}} -- {{data.Date | date}}</small>\n            </div>\n\n\n\n\n          </li>\n        </span>\n\n\n      </ul>\n\n\n    </div>\n\n\n  </div>\n\n  <div class=\"termination-border\">\n    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n    <button *ngIf=\"!customtemp\" class=\"btn pull-right save\" (click)=\"customTemplateCreate()\">Submit</button>\n\n    <button *ngIf=\"customtemp\" class=\"btn pull-right save\" (click)=\"customTemplateUpdate()\">Update</button>\n\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n</div>\n\n\n\n<!-- Author:Suresh M, Date:09-05-19  -->\n<!-- Standard Termination Wizard -->\n<div class=\"offboard-add\">\n\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Offboarding Add Task</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"offboard-edit col-md-10\">\n\n            <ul>\n\n              <li class=\"col-md-4 offboarding-width\">\n                <label>Task Name</label>\n                <input type=\"text\" [(ngModel)]=\"customTask.taskName\" class=\"form-control\" placeholder=\"Task Name\"\n                  name=\"taskName\" #taskName=\"ngModel\" required>\n                <p *ngIf=\"!customTask.taskName && taskName.touched || (!customTask.taskName && isValid)\" class=\"error\">\n                  Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Task Assignee</label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"customTask.taskAssignee\"\n                  (ngModelChange)=\"empEmail($event)\" placeholder=\"Assignee\" name=\"taskAssignee\" #taskAssignee=\"ngModel\"\n                  required>\n                  <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>{{data.personal.name.preferredName}}\n                  </mat-option>\n                </mat-select>\n                <p *ngIf=\"!customTask.taskAssignee && taskAssignee.touched || (!customTask.taskAssignee && isValid)\"\n                  class=\"error\"> Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Assignee Email Address</label>\n                <input type=\"text\" [(ngModel)]=\"customTask.assigneeEmail\" class=\"form-control\"\n                  placeholder=\"Email Address\">\n              </li>\n\n              <li class=\"offboarding-width col-md-8\">\n                <label>Task Description</label>\n                <textarea [(ngModel)]=\"customTask.taskDesription\" class=\"form-control\" rows=\"5\"\n                  placeholder=\"Description\" name=\"taskDesription\" #taskDesription=\"ngModel\" required></textarea>\n                <p *ngIf=\"!customTask.taskDesription && taskDesription.touched || (!customTask.taskDesription && isValid)\"\n                  class=\"error\"> Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Expected Task Completion Time</label>\n                <mat-select [(ngModel)]=\"customTask.timeToComplete.time\" class=\"form-control\" placeholder=\"Time Period\"\n                  name=\"time\" #time=\"ngModel\" required>\n                  <mat-option *ngFor=\"let data of taskCOmpletionTime\" [value]=\"data\">{{data}}</mat-option>\n                </mat-select>\n                <p *ngIf=\"!customTask.timeToComplete.time && time.touched || (!customTask.timeToComplete.time && isValid)\"\n                  class=\"error\"> Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label></label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"customTask.timeToComplete.unit\" placeholder=\"Time period\"\n                  name=\"unit\" #unit=\"ngModel\" required>\n                  <mat-option value='Day'>Day</mat-option>\n                  <mat-option value='Week'>Week</mat-option>\n                  <mat-option value='Month'>Month</mat-option>\n\n                </mat-select>\n                <p *ngIf=\"!customTask.timeToComplete.unit && unit.touched || (!customTask.timeToComplete.unit && isValid)\"\n                  class=\"error\"> Please fill this field</p>\n              </li>\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <ul>\n\n              <li class=\"col-md-4\">\n                <label>Task Category</label>\n                <span *ngIf=\"!categoryAddInput\">\n                  <mat-select [(ngModel)]=\"customTask.category\" class=\"form-control\" placeholder=\"Assignee\"\n                    name=\"category\" #category=\"ngModel\" required>\n                    <mat-option *ngFor=\"let task of getKeys((customTemplateSend.offBoardingTasks))\" [value]='task'>\n                      {{task}}</mat-option>\n                  </mat-select>\n\n                  <p *ngIf=\"!customTask.category && category.touched || (!customTask.category && isValid)\"\n                    class=\"error\"> Please fill this field</p>\n                </span>\n                <span *ngIf=\"categoryAddInput\">\n                  <input type=\"text\" [(ngModel)]=\"customTask.category\" class=\"form-control category\"\n                    placeholder=\"Category\" name=\"category\" #category=\"ngModel\" required>\n\n                  <p *ngIf=\"!customTask.category && category.touched || (!customTask.category && isValid)\"\n                    class=\"error\"> Please fill this field</p>\n                </span>\n\n              </li>\n\n              <li class=\"col-md-4\">\n                <label></label>\n                <a (click)=\"categoryInputAdd()\"><span><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></span> Add\n                  Category</a>\n              </li>\n\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <div class=\"termination-border\">\n              <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n              <button class=\"btn pull-right save\" (click)=\"offboardingTask()\">Submit</button>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n\n\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n    </div>\n  </div>\n\n  <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Edit Task</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"offboard-edit col-md-10\">\n\n            <ul>\n\n              <li class=\"col-md-4 offboarding-width\">\n                <label>Task Name</label>\n                <input type=\"text\" [(ngModel)]=\"customTaskEdit.taskName\" class=\"form-control\" placeholder=\"Task Name\"\n                  name=\"taskName\" #taskName=\"ngModel\" required>\n\n                <p *ngIf=\"!customTaskEdit.taskName && taskName.touched || (!customTaskEdit.taskName && isValid)\"\n                  class=\"error\">\n                  Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Task Assignee</label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"customTaskEdit.taskAssignee\"\n                  (ngModelChange)=\"empEmailForEdit($event)\" placeholder=\"Assignee\" name=\"taskAssignee\" #taskAssignee=\"ngModel\"\n                  required>\n                  <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>{{data.personal.name.preferredName}}\n                  </mat-option>\n                </mat-select>\n                <p *ngIf=\"!customTaskEdit.taskAssignee && taskAssignee.touched || (!customTaskEdit.taskAssignee && isValid)\"\n                  class=\"error\">\n                  Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Assignee Email Address</label>\n                <input type=\"text\" [(ngModel)]=\"customTaskEdit.assigneeEmail\" class=\"form-control\"\n                  placeholder=\"Email Address\">\n              </li>\n\n              <li class=\"offboarding-width col-md-8\">\n                <label>Task Description</label>\n                <textarea [(ngModel)]=\"customTaskEdit.taskDesription\" class=\"form-control\" rows=\"5\"\n                  placeholder=\"Description\" name=\"taskDesription\" #taskDesription=\"ngModel\" required></textarea>\n                <p *ngIf=\"!customTaskEdit.taskDesription && taskDesription.touched || (!customTaskEdit.taskDesription && isValid)\"\n                  class=\"error\">\n                  Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Expected Task Completion Time</label>\n                <mat-select [(ngModel)]=\"customTaskEdit.timeToComplete.time\" class=\"form-control\"\n                  placeholder=\"Time Period\" name=\"time\" #time=\"ngModel\" required>\n                  <mat-option *ngFor=\"let data of taskCOmpletionTime\" [value]=\"data\">{{data}}</mat-option>\n                </mat-select>\n                <p *ngIf=\"!customTaskEdit.timeToComplete.time && time.touched || (!customTaskEdit.timeToComplete.time && isValid)\"\n                  class=\"error\">\n                  Please fill this field</p>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label></label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"customTaskEdit.timeToComplete.unit\"\n                  placeholder=\"Time period\" name=\"unit\" #unit=\"ngModel\" required>\n                  <mat-option value='Day'>Day</mat-option>\n                  <mat-option value='Week'>Week</mat-option>\n                  <mat-option value='Month'>Month</mat-option>\n                </mat-select>\n                <p *ngIf=\"!customTaskEdit.timeToComplete.unit && unit.touched || (!customTaskEdit.timeToComplete.unit && isValid)\"\n                  class=\"error\">\n                  Please fill this field</p>\n              </li>\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <ul>\n\n              <li class=\"col-md-4\">\n                <label>Task Category</label>\n                <span *ngIf=\"!categoryInput\">\n                  <mat-select [(ngModel)]=\"customTaskEdit.category\" (ngModelChange) = \"categoryChange()\" class=\"form-control\" placeholder=\"category\"\n                    name=\"category\" #category=\"ngModel\" required>\n                    <mat-option *ngFor=\"let task of getKeys((customTemplateSend.offBoardingTasks))\" [value]='task'>\n                      {{task}}</mat-option>\n                  </mat-select>\n                  <p *ngIf=\"!customTaskEdit.category && category.touched || (!customTaskEdit.category && isValid)\"\n                  class=\"error\"> Please fill this field</p>\n                </span>\n\n                \n                <span *ngIf=\"categoryInput\">\n                  <input type=\"text\" [(ngModel)]=\"customTaskEdit.category\" class=\"form-control category\"\n                    placeholder=\"Category\" name=\"category\" #category=\"ngModel\" required>\n\n                  <p *ngIf=\"!customTaskEdit.category && category.touched || (!customTaskEdit.category && isValid)\"\n                    class=\"error\"> Please fill this field</p>\n                </span>\n              </li>\n\n              <li class=\"col-md-4 add-category\">\n                <label></label>\n                <a (click)=\"categoryAdd()\"><span><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></span> Add Category</a>\n              </li>\n\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <div class=\"termination-border\">\n              <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n              <button class=\"btn pull-right save\" (click)=\"customTaskUpdate()\">Update</button>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n\n\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.ts":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: CustomOffboardingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomOffboardingComponent", function() { return CustomOffboardingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_off_boarding_template_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/off-boarding-template.service */ "./src/app/services/off-boarding-template.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CustomOffboardingComponent = /** @class */ (function () {
    function CustomOffboardingComponent(siteAccessRolesService, offBoardingTemplateService, swalAlertService, route) {
        this.siteAccessRolesService = siteAccessRolesService;
        this.offBoardingTemplateService = offBoardingTemplateService;
        this.swalAlertService = swalAlertService;
        this.route = route;
        this.taskCOmpletionTime = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        this.customTask = {
            taskName: '',
            taskAssignee: '',
            assigneeEmail: '',
            taskDesription: '',
            timeToComplete: {
                time: '',
                unit: ''
            },
            category: '',
            task_assigned_date: new Date().toISOString(),
            Date: new Date().toISOString()
        };
        this.pushData = {
            taskName: '',
            taskAssignee: '',
            assigneeEmail: '',
            taskDesription: '',
            timeToComplete: {
                time: '',
                unit: ''
            },
            category: '',
            task_assigned_date: new Date().toISOString(),
            Date: new Date().toISOString()
        };
        this.pushData1 = {
            taskName: '',
            taskAssignee: '',
            assigneeEmail: '',
            taskDesription: '',
            timeToComplete: {
                time: '',
                unit: ''
            },
            category: '',
            task_assigned_date: new Date().toISOString(),
            Date: new Date().toISOString()
        };
        this.customTaskEdit = {
            taskName: '',
            taskAssignee: '',
            assigneeEmail: '',
            taskDesription: '',
            timeToComplete: {
                time: '',
                unit: ''
            },
            category: '',
            task_assigned_date: new Date().toISOString(),
            Date: new Date().toISOString()
        };
        this.customTemplateSend = {
            companyId: '',
            templateType: '',
            templateName: '',
            offBoardingTasks: {},
            createdBy: '',
            updatedBy: ''
        };
        this.customTemplateSend1 = {
            companyId: '',
            templateType: '',
            templateName: '',
            offBoardingTasks: {},
            createdBy: '',
            updatedBy: ''
        };
        this.objName = [];
        this.customtemp = false;
        this.categoryInput = false;
        this.categoryAddInput = false;
        this.isValid = false;
        this.isValidMainSubmit = false;
        this.isDisabled = false;
    }
    CustomOffboardingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.employeeSearch();
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log(_this.id);
        });
        if (this.id) {
            this.customtemp = true;
            this.getCustomOffboardingtemplate(this.id);
        }
    };
    CustomOffboardingComponent.prototype.categoryChange = function () {
        var _this = this;
        console.log(this.customTaskEdit.category, this.tempTaskName, '122');
        if (this.tempCategory != this.customTaskEdit.category) {
            this.customTemplateSend['offBoardingTasks'][this.tempCategory].forEach(function (element) {
                console.log(element, 'taskssss');
                if (element.taskName == _this.tempTaskName) {
                    var index = _this.customTemplateSend['offBoardingTasks'][_this.tempCategory].indexOf(_this.customTaskEdit);
                    if (index > -1) {
                        _this.customTemplateSend['offBoardingTasks'][_this.tempCategory].splice(index, 1);
                        delete _this.customTaskEdit['isChecked'];
                        // this.customTemplateSend['offBoardingTasks'][this.customTaskEdit.category].push(this.customTaskEdit)
                    }
                }
            });
            console.log(this.customTemplateSend['offBoardingTasks'], "vipin");
        }
    };
    CustomOffboardingComponent.prototype.getCustomOffboardingtemplate = function (id) {
        var _this = this;
        this.offBoardingTemplateService.getcustomOffboardingTemplatedata(id)
            .subscribe(function (res) {
            console.log("get custom template data", res);
            _this.customTemplateSend = res.data[0];
            _this.customOffboardingtemplateName = res.data[0].templateName;
            console.log(_this.customTemplateSend);
        }, function (err) {
            console.log(err);
        });
    };
    CustomOffboardingComponent.prototype.singleTaskChoose = function (taskArray, taskName, isChecked) {
        var _this = this;
        console.log(taskArray, taskName, isChecked);
        this.tempCategory = taskArray;
        this.tempTaskName = taskName;
        if (isChecked == false) {
            this.customTaskEdit = {};
            this.customTaskEdit.timeToComplete = {};
            this.isDisabled = false;
        }
        if (isChecked == true) {
            for (var task in this.customTemplateSend["offBoardingTasks"]) {
                console.log(task);
                this.customTemplateSend["offBoardingTasks"][task].forEach(function (element) {
                    console.log(element);
                    if (element.taskName === taskName) {
                        element.isChecked = true;
                        _this.customTaskEdit = element;
                        // this.addbtn = element;
                        console.log("popdaata", _this.customTaskEdit);
                        _this.addbtn = element;
                        _this.isDisabled = true;
                    }
                    else {
                        element.isChecked = false;
                    }
                });
            }
        }
    };
    CustomOffboardingComponent.prototype.deleteTask = function () {
        var _this = this;
        this.customTemplateSend["offBoardingTasks"][this.tempCategory].forEach(function (element) {
            console.log(element);
            if (element.taskName === _this.tempTaskName) {
                element.isChecked = true;
                console.log(element, '1234567');
                // delete this.customTaskEdi
                delete _this.customTemplateSend.offBoardingTasks[_this.tempCategory].element;
                var index = _this.customTemplateSend.offBoardingTasks[_this.tempCategory].indexOf(element);
                console.log("index", index);
                if (index > -1) {
                    _this.customTemplateSend['offBoardingTasks'][_this.tempCategory].splice(index, 1);
                }
                console.log("popdaata", _this.customTemplateSend.offBoardingTasks);
            }
            else {
                element.isChecked = false;
            }
            _this.customTemplateUpdate();
        });
    };
    CustomOffboardingComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    /* Description: getting employees list
    author : vipin reddy */
    CustomOffboardingComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    CustomOffboardingComponent.prototype.empEmail = function (event) {
        var _this = this;
        console.log(event);
        this.employeeData.forEach(function (element) {
            if (element._id == event) {
                _this.customTask.assigneeEmail = element.email;
            }
            console.log(_this.customTask.assigneeEmail);
        });
    };
    CustomOffboardingComponent.prototype.empEmailForEdit = function (event) {
        var _this = this;
        console.log(event);
        this.employeeData.forEach(function (element) {
            if (element._id == event) {
                _this.customTaskEdit.assigneeEmail = element.email;
            }
            console.log(_this.customTaskEdit.assigneeEmail);
        });
    };
    CustomOffboardingComponent.prototype.customTaskUpdate = function () {
        var _this = this;
        delete this.customTaskEdit['isChecked'];
        this.pushData1 = this.customTaskEdit;
        this.customTaskEdit = {};
        this.customTaskEdit.timeToComplete = {};
        console.log(this.tempCategory, this.pushData1.category, this.customTemplateSend['offBoardingTasks']);
        if (this.pushData1.category != this.tempCategory) {
            console.log("not equal");
            this.customTemplateSend['offBoardingTasks'][this.pushData1.category].push(this.pushData1);
        }
        if (this.pushData1.category == this.tempCategory) {
            console.log("equal");
            this.customTemplateSend['offBoardingTasks'][this.pushData1.category] = [this.pushData1];
        }
        console.log(this.customTemplateSend, this.pushData1, "12121");
        jQuery("#myModal1").modal("hide");
        console.log(this.customTemplateSend);
        this.offBoardingTemplateService.updateCustomTemplate(this.customTemplateSend)
            .subscribe(function (res) {
            console.log("custom template create", res);
            if (res.status == true) {
                _this.isDisabled = false;
                _this.swalAlertService.SweetAlertWithoutConfirmation('Custom template', res.message, 'success');
            }
        }, function (err) {
            console.log(err);
        });
    };
    CustomOffboardingComponent.prototype.offboardingTask = function () {
        this.isValid = true;
        jQuery("#myModal").modal("hide");
        console.log(this.customTask);
        this.pushData = this.customTask;
        this.customTask = {};
        this.customTask.timeToComplete = {};
        if (this.customTemplateSend['offBoardingTasks'][this.pushData.category]) {
            this.customTemplateSend['offBoardingTasks'][this.pushData.category].push(this.pushData);
        }
        else {
            this.customTemplateSend['offBoardingTasks'][this.pushData.category] = [this.pushData];
        }
        console.log(this.customTemplateSend, this.pushData, "12121");
        this.isValid = false;
        this.categoryAddInput = false;
    };
    // tempform() {
    //   this.customTask.category = '';
    //   this.customTask.taskName = '';
    //   this.customTask.assigneeEmail = '';
    //   this.customTask.taskAssignee = '';
    //   this.customTask.timeToComplete.time = '';
    //   this.customTask.timeToComplete.unit = '';
    //   this.customTask.taskDesription = '';
    //   this.customTask.Date = '';
    //   this.customTask.task_assigned_date = '';
    // }
    CustomOffboardingComponent.prototype.customTemplateCreate = function () {
        var _this = this;
        this.isValidMainSubmit = true;
        if (Object.keys(this.customTemplateSend['offBoardingTasks']).length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Offboarding template", "Add atleast one category", "error");
        }
        if (this.customOffboardingtemplateName && Object.keys(this.customTemplateSend['offBoardingTasks']).length > 0) {
            var cmpnyId = JSON.parse(localStorage.getItem('companyId'));
            this.customTemplateSend.companyId = cmpnyId;
            this.customTemplateSend.createdBy = cmpnyId;
            this.customTemplateSend.updatedBy = cmpnyId;
            this.customTemplateSend.templateType = 'custom';
            this.customTemplateSend.templateName = this.customOffboardingtemplateName;
            console.log(this.customTemplateSend, 'vipin121212');
            this.offBoardingTemplateService.createcustomTemplate(this.customTemplateSend)
                .subscribe(function (res) {
                console.log("custom template create", res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation('customtemplate', res.message, "success");
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    CustomOffboardingComponent.prototype.customTemplateUpdate = function () {
        var _this = this;
        console.log(this.customTemplateSend);
        this.offBoardingTemplateService.updateCustomTemplate(this.customTemplateSend)
            .subscribe(function (res) {
            console.log("custom template create", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Custom template', res.message, 'success');
            }
        }, function (err) {
            console.log(err);
        });
    };
    CustomOffboardingComponent.prototype.categoryAdd = function () {
        this.categoryInput = !this.categoryInput;
    };
    CustomOffboardingComponent.prototype.categoryInputAdd = function () {
        this.categoryAddInput = !this.categoryAddInput;
    };
    CustomOffboardingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-custom-offboarding',
            template: __webpack_require__(/*! ./custom-offboarding.component.html */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.html"),
            styles: [__webpack_require__(/*! ./custom-offboarding.component.css */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.css")]
        }),
        __metadata("design:paramtypes", [_services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_1__["SiteAccessRolesService"],
            _services_off_boarding_template_service__WEBPACK_IMPORTED_MODULE_2__["OffBoardingTemplateService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], CustomOffboardingComponent);
    return CustomOffboardingComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing.module.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing.module.ts ***!
  \*********************************************************************************************************/
/*! exports provided: OffBoardingLandingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffBoardingLandingModule", function() { return OffBoardingLandingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _off_boarding_landing_off_boarding_landing_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./off-boarding-landing/off-boarding-landing.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.ts");
/* harmony import */ var _off_boarding_landing_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./off-boarding-landing.routing */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing.routing.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng2-select */ "./node_modules/ng2-select/index.js");
/* harmony import */ var ng2_select__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ng2_select__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _standard_offboarding_standard_offboarding_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./standard-offboarding/standard-offboarding.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.ts");
/* harmony import */ var _services_off_boarding_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../services/off-boarding.service */ "./src/app/services/off-boarding.service.ts");
/* harmony import */ var _custom_offboarding_custom_offboarding_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./custom-offboarding/custom-offboarding.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var OffBoardingLandingModule = /** @class */ (function () {
    function OffBoardingLandingModule() {
    }
    OffBoardingLandingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _off_boarding_landing_routing__WEBPACK_IMPORTED_MODULE_3__["OffBoardingLandingRouting"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_6__["BsDatepickerModule"].forRoot(),
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"], ng2_select__WEBPACK_IMPORTED_MODULE_13__["SelectModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__["MatCheckboxModule"]
            ],
            declarations: [_off_boarding_landing_off_boarding_landing_component__WEBPACK_IMPORTED_MODULE_2__["OffBoardingLandingComponent"], _standard_offboarding_standard_offboarding_component__WEBPACK_IMPORTED_MODULE_15__["StandardOffboardingComponent"], _custom_offboarding_custom_offboarding_component__WEBPACK_IMPORTED_MODULE_17__["CustomOffboardingComponent"]],
            providers: [_services_off_boarding_service__WEBPACK_IMPORTED_MODULE_16__["OffBoardingService"]]
        })
    ], OffBoardingLandingModule);
    return OffBoardingLandingModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing.routing.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing.routing.ts ***!
  \**********************************************************************************************************/
/*! exports provided: OffBoardingLandingRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffBoardingLandingRouting", function() { return OffBoardingLandingRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _off_boarding_landing_off_boarding_landing_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./off-boarding-landing/off-boarding-landing.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.ts");
/* harmony import */ var _standard_offboarding_standard_offboarding_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./standard-offboarding/standard-offboarding.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.ts");
/* harmony import */ var _custom_offboarding_custom_offboarding_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./custom-offboarding/custom-offboarding.component */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/custom-offboarding/custom-offboarding.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: '', redirectTo: "/off-boarding-landing", pathMatch: "full" },
    { path: 'off-boarding-landing', component: _off_boarding_landing_off_boarding_landing_component__WEBPACK_IMPORTED_MODULE_2__["OffBoardingLandingComponent"] },
    { path: 'termination-wizard', loadChildren: "./termination-wizard/termination-wizard.module#TerminationWizardModule" },
    { path: 'standard-offboard', component: _standard_offboarding_standard_offboarding_component__WEBPACK_IMPORTED_MODULE_3__["StandardOffboardingComponent"] },
    { path: 'custom-offboard', component: _custom_offboarding_custom_offboarding_component__WEBPACK_IMPORTED_MODULE_4__["CustomOffboardingComponent"] },
    { path: 'custom-offboard/:id', component: _custom_offboarding_custom_offboarding_component__WEBPACK_IMPORTED_MODULE_4__["CustomOffboardingComponent"] }
];
var OffBoardingLandingRouting = /** @class */ (function () {
    function OffBoardingLandingRouting() {
    }
    OffBoardingLandingRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OffBoardingLandingRouting);
    return OffBoardingLandingRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.css":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.css ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-jumbotron-bg{\n    background-color: #fff;\n    box-shadow: -1px 1px 37px -5px rgba(0,0,0,0.37);\n    padding: 35px;\n    border-radius: 10px;\n    position: relative;\n}\n\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n\n.main-employee{\n    width: 22px\n}\n\n.zenwork-custom-company-settings{\n    float: none;\n    margin: 40px auto;\n}\n\n.zenwork-margin-zero{\nmargin-top: 20px !important;\n}\n\n.zenwork-margin-zero b{\n    font-size: 14px!important;\n    color: #404040;\n}\n\n.zenwork-custom-company-settings a{\n    color : #3e3e3ea3;\n    text-decoration: none;\n}\n\n.jumbotron p{\n    font-size:19px!important;\n}\n\n.hire-heading{\n    font-weight: 400;\n    font-size: 25px; margin: 0 0 40px;\n}\n\n.zenwork-settings-wrapper{\n}\n\n.zenwork-settings-wrapper a{\n    cursor: pointer;\n}\n\n.border-right{\n    border-right: 1px solid #e4eae7;\n    height:110px;\n}\n\n.margin-bottom-20{\n    margin:0 0 20px;\n}\n\n.new-sub-heading{\n    font-weight: normal;\n    font-size: 18px;\n    color: #545454;\n    display: block;\n}\n\n.border-top{\n    border-top: 1px solid #e4eae7;\n}\n\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding:6px 15px;\n    font-size: 14px;\n    margin:  0 10px 0 0;\n}\n\n.margin-top-40{\n    margin: 40px 0 0 0;\n}\n\n.nav-link {\n    color:#3e3e3ea3!important;\n}\n\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.mr-7 .fa{ font-size:15px;}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.zenwork-inner-icon{\n    width: 30px;\n    height: auto;\n}\n\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n\n.sub-title{\n    display: inline-block;\n    margin: 5px 0 0;\n    vertical-align:sub;\n}\n\na{\n    color: #000;\n}\n\na:hover{\n    text-decoration: none;\n}\n\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important;\n\n}\n\n.modal-margintop{\n    margin-top:40px;\n}\n\n.modal-body .container{\n    width: 100%;\n}\n\n.modal-body{\n    background: #f8f8f8;\n    padding: 0;\n}\n\n/* side nav css*/\n\n.tabs-left{\n    margin-top:0px;\n}\n\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n    border-right: 0!important;;\n    padding: 20px;\n    margin-top: 10px;\n  }\n\n.tab-content{\n      /* border-left: 1px solid #ddd; */\n      margin: 0;\n      background: #fff;\n      /* padding: 30px; */\n  \n  }\n\n.compensation-tab{\n      background: #f8f8f8 !important\n\n  }\n\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n  }\n\n.tabs-left > .nav-tabs {\n    float: left;\n    border-right: 1px solid #ddd;\n  }\n\n.tabs-left > .nav-tabs > li > a {\n    font-size: 12px;\n    margin-right: -1px;\n    border-radius: 4px 0 0 4px;\n  }\n\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee;\n  }\n\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n\n/*side nav css ends*/\n\n.zenwork-side-nav>.zenwork-sidenav-activetab>a:focus, .zenwork-side-nav>zenwork-sidenav-activetab>a:hover{\n    background: #439348 !important;\n    border-radius: 40px !important;\n    color: #fff !important;\n    border: transparent !important;\n}\n\n.zenwork-sidenav-activetab .caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n\n.tab-content{\n    /* border-left: 1px solid #ddd; */\n    margin: 0;\n    background: #fff;\n    /* padding: 30px; */\n\n}\n\n.schdule-type{\n    padding: 0 0 0 10px;\n}\n\n.schdule-type mat-select{\n    width: 25% !important;\n}\n\n.emp-terminate-filters{\n    margin: 20px 0 0 0;\n}\n\n.emp-terminate-filters label{\n    font-weight: normal;\n    padding: 0 0 5px;\n    font-size: 12px;\n}\n\n.emp-terminate-filters .termination-date{\n    position: relative;\n}\n\n.hiredate{\n    width: 183px;\n    height: 34px;\n    font-size: 12px;\n    padding-left: 13px;\n    border: 0;\n    background: #f8f8f8;\n  }\n\n.calendar-icon{\n    position: absolute;\n    width: 17px;\n    top: 34px;\n    left: 22%;\n}\n\ntextarea{\n    resize: none;\n    width: 55%;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n}\n\n.attach-file-btn{\n    position: relative;\n    padding: 5px 10px;\n    font-size: 12px;\n    /* border-color: #439348; */\n    text-align: left;\n    width: 191px;\n    background: url(\"/assets/images/Ofboarding/ic_attachment_24px.png\") no-repeat #439348;\n}\n\n.attach-file-btn .clip-icon{\n    color: #fff;\n    float: right;\n    font-size: 17px;\n}\n\n.documents-list ul li {\n    font-size: 12px;\n    line-height: 20px;\n}\n\n.documents-list ul li small{\n    padding: 0 10px 0 0;\n}\n\n.documents-list ul li small i {\n    color: #e5423d;\n    font-size: 15px;\n}\n\n.submit-buttons .list-item{\n    display: inline-block;\n    padding: 0px 10px;\n  }\n\n.submit-buttons .btn-success {\n      background-color: #439348;\n  }\n\n.btn-style{\n    padding: 4px 20px !important;\n    border-radius: 30px!important;\n    font-size: 12px;\n}\n\n/* tabel */\n\n.custom-fields { display: block;}\n\n.custom-fields .modal-header { width: 90%; margin:0 auto; padding:0 50px 10px; border-bottom: 1px solid #c5c3c3;}\n\n.custom-fields .modal-title { color:#008f3d;}\n\n.custom-fields .modal-dialog { width:70%;}\n\n.custom-fields .modal-content { border: none; padding:30px 0 50px;}\n\n.custom-tables { margin: 0 auto; float: none; padding: 0 30px 0 5px;}\n\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 12px; border-radius:3px;}\n\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#f8f8f8;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 12px;}\n\n.custom-fields .checkbox{ margin: 0;}\n\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n\n.heade-check .checkbox label::after { color:#616161;width: 20px; height: 20px; padding:2px 0 0 4px; font-size: 12px;}\n\n.heade-check .checkbox{ margin: 0;}\n\n.heade-check .checkbox label { padding-left: 0;}\n\n.cont-check .checkbox label::before {width: 20px; height: 20px; top: -1px;}\n\n.cont-check .checkbox label::after { color:#fff; background: #439348;width: 20px; height: 20px; padding: 3px 0 0 4px;border-radius: 3px;}\n\n.cont-check .checkbox{ margin: 0;}\n\n.cont-check .checkbox label { padding-left: 0;}\n\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n\n.custom-tables .table>tbody>tr>td .form-control { border: none; width: 80%; box-shadow: none; color:#000;}\n\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n\n.select-drop select { background:#fff; border: none; width: 80%; height: 34px; color:#948e8e; padding: 6px 10px;}\n\n.custom-tables .btn { float: right; color:#ec6469; border:#ec6469 1px solid; padding:5px 25px; border-radius: 20px; background: transparent; font-size: 15px; margin: 20px 0 0;}\n\n.time-off-data{\n      margin: 40px 0 0 0;\n  }\n\n.zenwork-margin-30{\n      margin: 30px 0;\n  }\n\n.direct-report .zenwork-input{\n      background: #fff !important;\n  }\n\n.direct-report table tr td .schdule-type{\n      padding: 10px 0 0 0 !important;\n  }\n\n.direct-report table tr td .schdule-type .mat-select{\n    width: 100% !important; \n  }\n\n.direct-report .table tbody tr td{\n      padding: 0px 15px;\n  }\n\n.company-property .custom-tables .table thead tr th{\n      font-size: 11px !important;\n  }\n\n.main-onboarding .panel-font{\n    color:#439348 !important;\n    font-weight: 600;\n    margin-top: 5px;\n}\n\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n\n.settings-dropdown{\n    float: right;\n}\n\n.main-onboarding .panel-heading .btn{\n    padding: 4px 15px!important;\n    border: none;\n}\n\n.caret{\n    margin-bottom: 5px!important;\n}\n\n.task-details .taskname{\n    width: 90%;\n    height: 30px;\n    padding-left: 10px;\n    font-size: 12px;\n    border: 0;\n    background: #f8f8f8;\n    color: #c1c1c1;\n}\n\n.zenwork-margin-top20{\n    margin-top: 20px;\n}\n\nselect{\n    height: 30px;\n}\n\nlabel{\n    font-weight: normal;\n    font-size: 12px;\n  \n\n}\n\n.zenwork-structure-settings{\n    border-radius: 0px!important;\n    height:30px;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n    color: #c1c1c1;\n}\n\n.assign-items .list-items{\n    width: 40%;\n    display: inline-block;\n    padding: 0px 10px;\n    height: 30px;\n}\n\n.assign-items .duedate{\n    height: 30px;\n    width:70%;\n    border: 0;\n    color: #c1c1c1;\n    background: #f8f8f8;\n}\n\n.calendar-icon{\n    position: absolute;\n    width: 20px;\n    top: 33px;\n    right: 34%;\n}\n\n.text-area{\n    width: 90%;\n}\n\ntextarea{\n    resize: none;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n}\n\n.category select{\n    font-size: 12px;\n    width: 20%;\n}\n\n.add-task-btns .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n\n}\n\n.add-task{\n    padding-left:0px!important;\n}\n\n.add-task-btns{\n    padding-top: 10px;\n}\n\n.add-task-btns .btn{\n  \n    height: 25px;\n    font-size: 12px;\n    padding: 4px 12px !important;\n}\n\n.add-btn{\n    cursor: auto;\n    background-color: #439348 !important;\n}\n\n.add-task-setting-btns .task-btns{\n    font-size: 12px;\n    margin: 0px 6px\n}\n\n.panel-body{\n    padding:15px 15px 0px 15px !important;\n}\n\n.attach-file-btn{\n    \n    padding: 5px 10px;\n    font-size: 12px;\n    border-color: #439348;\n    text-align: left;\n    width: 191px;\n    background: url(\"/assets/images/Ofboarding/ic_attachment_24px.png\") no-repeat #439348;\n}\n\nsmall{\n    font-size: 9px;\n    color:#3e3e3ea3;\n}\n\n.hr-tasks{\n    font-size: 13px;\n    font-weight: 600;\n}\n\n.individual-hr-task{\n    font-size:12px;\n}\n\n.margin-top-zero{\n    margin-top:0px!important;\n}\n\n.padding-top-remove{\n    padding-top:0px!important;\n}\n\n/* team-text-card */\n\n.team-introdution-card{\n    background-color: #fcfcfc;\n    /* border: 1px solid #eee; */\n    border-radius: 2px;\n    padding: 10px;\n}\n\n.cancel-task .btn{\n    cursor: auto;\n}\n\n.margin-zero{\n    margin:0px!important;\n}\n\n.team-introdution-card .padding-top-remove small{\n    /* padding-left:25px;  */\n}\n\n.card-intro{\n    font-size: 11px;\n    padding-left: 30px;\n}\n\n.team-introdution-card .individual-hr-task{\n    font-weight: 600;\n    color: #439348;\n}\n\n.checkbox {\n    padding-left: 20px;\n    margin-top:0px !important;\n    margin-bottom:0px !important;\n  }\n\n.checkbox label {\n    display: inline-block;\n    vertical-align: middle;\n    position: relative;\n    padding-left: 5px;\n  }\n\n.checkbox label::before {\n    content: \"\";\n    display: inline-block;\n    position: absolute;\n    width: 17px;\n    height: 17px;\n    left: 0;\n    margin-left: -20px;\n    border: 1px solid #cccccc;\n    border-radius: 3px;\n    background-color: #fff;\n    transition: border 0.15s ease-in-out, color 0.15s ease-in-out;\n  }\n\n.checkbox label::after {\n    display: inline-block;\n    position: absolute;\n    width: 16px;\n    height: 16px;\n    left: 0;\n    top: 0;\n    margin-left: -20px;\n    padding-left: 3px;\n    padding-top: 1px;\n    font-size: 11px;\n    color: #555555;\n  }\n\n.checkbox input[type=\"checkbox\"]:checked + label::after {\n    font-family: 'FontAwesome';\n    content: \"\\f00c\";\n  }\n\n.checkbox-success input[type=\"checkbox\"]:checked + label::before {\n    background-color: #5cb85c;\n    border-color: #5cb85c;\n  }\n\n.checkbox-success input[type=\"checkbox\"]:checked + label::after {\n    color: #fff;\n  }\n\n.emp-image{\n    padding-top: 0;\n    width: 43px;\n    display: inline-block;\n    vertical-align: middle;\n  }\n\n.employee-details{\n    width: 70px;\n    display: inline-block;\n    vertical-align: middle;\n    text-align: left;\n    margin-left: 10px;\n  }\n\n.employee-image-info{\n    padding-left: 20px;\n  }\n\n.emp-name{\n    font-size: 11px;\n    padding-left: 0;\n    display: block;\n    line-height: 11px;\n  }\n\n.borderright{\n      border-right: 1px solid #eee;\n  }\n\n.team-text-area{\n    margin-top:10px;\n  }\n\ntextarea{\n      resize: none;\n  }\n\n.btn-color{\n    border: 1px solid #EF8086;\n    color: #ef8086;\n    cursor: auto;\n  }\n\n.margin-zero{\n      margin: 0px!important;\n  }\n\n.benefits-offboarding{\n      margin: 30px 0 0 0;\n  }\n\n.offboardingtask{\n      background: #f8f8f8;\n  }\n\n.offboardingtask .done-not-icons{\n      padding: 30px 30px 0 0;\n  }\n\n.offboardingtask .done-not-icons ul li{\n      display: inline-block;\n      padding: 0px 10px;\n  }\n\n.offboardingtask .done-not-icons ul li .cross-icon{\n      color: #e5423d;\n\n  }\n\n/*-- Author:Suresh M, Date:06-05-19  --*/\n\n/*-- Termination Wizard css code  --*/\n\n.top-menu { position: absolute; top:-10px; right:-10px;}\n\n.new-wizard { display: block;}\n\n.new-wizard .modal-dialog { width: 75%;}\n\n.new-wizard .modal-content {border-radius: 5px;}\n\n.new-wizard .modal-header {padding: 20px 30px;}\n\n.new-wizard .modal-body { padding: 0; background:transparent !important;}\n\n.wizard-tabs { display: block;}\n\n.wizard-lft { background: #f8f8f8; padding: 15px 15px 75px 0;border-radius: 0 0 0 5px; height:77vh;}\n\n.wizard-lft.custom-wizard{height:70vh;}\n\n.wizard-lft .nav-tabs{ border:none;}\n\n.wizard-lft .nav-tabs>li { float: none; margin: 0 0 15px;}\n\n.wizard-lft .nav-tabs > li.active > a, .wizard-lft .nav-tabs > li.active > a:focus, .wizard-lft .nav-tabs > li.active > a:hover \n{ border: none !important;background:transparent; color:#a5a3a3; border-radius: 30px;}\n\n.wizard-lft .nav-tabs>li>a { font-size: 17px; margin: 0 0 0 20px; color: #a5a3a3;}\n\n.wizard-lft .nav>li>a:focus, .wizard-lft .nav>li>a:hover { background: #787c7a; color:#fff; border-radius: 30px;}\n\n.wizard-rgt { padding:0 ;}\n\n.wizard-hight { height: 550px; overflow-y: auto;}\n\n.terminination { height: 500px; overflow-y: auto;}\n\n.terminination ul { display:block;}\n\n.terminination ul li { margin: 0 0 25px; padding: 0 5px;}\n\n.terminination ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n\n.terminination ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%; background:#f8f8f8;}\n\n.terminination ul li .form-control:focus{ box-shadow: none;}\n\n.terminination ul li a {color: #484747; font-weight: normal; font-size: 16px; display:inline-block; cursor: pointer; text-decoration: underline;}\n\n.terminination ul li .date { height: auto !important; line-height:inherit !important; width:94% !important;background:#f8f8f8;}\n\n.terminination ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.terminination ul li .date span { border: none !important; padding: 0 !important;}\n\n.terminination ul li .date .form-control { width:77%; height: auto; padding: 12px 0 12px 10px;}\n\n.terminination ul li .date .form-control:focus { box-shadow: none; border: none;}\n\n.terminination ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.terminination ul li.details-width { clear: both; float: none;}\n\n.details-main {padding:30px 0 30px 20px;}\n\n.details-main .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n\n.details-main .btn.cancel { color:#e44a49; background:transparent; padding:10px 0 0 30px;font-size: 16px;}\n\n.details-main .btn.back { color:#4c4c4c; background:transparent;border:#b7b3b3 1px solid; padding: 8px 35px; \nborder-radius: 20px;font-size: 16px;}\n\n.termination-border {border-top:#ccc 1px solid !important; padding:25px 0 0; margin: 0 30px  0 0;}\n\n.terminination ul li.procees .form-control { width: 50%;}\n\n.tasks {margin: 0 0 20px;}\n\n.tasks h3{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block; margin: 0;}\n\n.tasks .form-control { width:30%; height: auto; padding: 10px 0 10px 10px; background: #f8f8f8; border: none; box-shadow:none;}\n\n.reports {margin: 0 0 50px;}\n\n.reports h3{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block; margin: 0;}\n\n.reports .form-control{ border: none;margin: 0 0 20px; box-shadow: none; padding: 11px 12px; height: auto; width:25%; background:#f8f8f8;}\n\n.reports-table .table { margin-bottom: 30px;}\n\n.reports-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 13px; background: #eef7ff; border-bottom:none;}\n\n.reports-table .table>tbody>tr>td, .reports-table .table>tfoot>tr>td, .reports-table .table>thead>tr>td{ padding: 13px; background:#fff;\nborder-top: none;}\n\n.reports-table .table>tbody>tr>td{background: #f8f8f8;border-bottom:1px solid #ccc; vertical-align: middle;}\n\n.reports-table .form-control{ border: none;margin: 0; box-shadow: none; padding:6px 12px; height: auto; width:100%; background:#fff;}\n\n.company-property .table{ background:#fff;}\n\n.company-property .table>tbody>tr>th, .company-property .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:16px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n\n.company-property .table>tbody>tr>td, .company-property .table>tfoot>tr>td, .company-property .table>tfoot>tr>th, \n.company-property .table>thead>tr>td {color: #484747;padding:15px;font-size:15px;background: #f8f8f8;\nborder-bottom: 1px solid #ddd;}\n\n.company-property button.mat-menu-item a { color:#ccc;}\n\n.company-property button.mat-menu-item a:hover { color:#fff; background: #099247;}\n\n.company-property .table>tbody>tr>th a, .company-property .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n\n.company-property .table>tbody>tr>th a .fa, .company-property .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.company-property button { border: none; outline: none; background:transparent;}\n\n.terminination ul li .file-upload {\n    position: relative;\n    display: inline-block; background: #787d7a;border-radius: .4em; cursor: pointer; padding: 7px 20px;\n  }\n\n.terminination ul li .file-upload span { display: inline-block;vertical-align: middle; padding: 0 0 0 25px;}\n\n.terminination ul li .file-upload span .fa{ color: #fff; font-size: 20px;}\n\n.terminination ul li .file-upload__label {\n    display: inline-block;\n    color: #fff; padding: 0;cursor: pointer;\n  }\n\n.terminination ul li .file-upload__input {\n    position: absolute;cursor: pointer;\n    left: 0;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    font-size: 1;\n    width: 0;\n    height: 100%;\n    opacity: 0;\n  }\n\n.custom-offboarding-temp{\n      margin: 25px 0 0 0;\n  }\n\n.terminination .emp{\n      width: 25%;\n  }\n\n.error{\n    color:#e44a49;\n    padding: 5px 0 0;\n  }\n\n/* ===========================================================================template in wizardpopup=============================================================== */\n\n.off-boarding{ display: block; margin: 20px 0 0;}\n\n.tasks{ display: block;}\n\n.tasks-in{ position: relative;background: #edf7fe; padding: 14px 20px; margin: 20px 0 0;}\n\n.tasks h3{color: #3a3a3a;\n    margin: 0;\n    font-size: 18px;}\n\n.top-menu{position: absolute; top:4px; right: 0;}\n\n.tasks ul { display: block; background: #fff; padding:0 20px;}\n\n.tasks ul li { display: block; padding: 20px 0;}\n\n.tasks ul li h4{color: #3a3a3a;margin: 0 0 15px;font-size: 16px; font-weight:600;}\n\n.task-names{ border-bottom:#ccc 1px solid; padding: 12px 0;}\n\n.task-names span { display: block;color:#777474;font-size:14px;}\n\n.task-names small { display: block;color: #ccc;font-size:12px; margin: 5px 0 0 0px;}\n\n/* =============================================================================offboardingwizard--summary-review========================================== */\n\n.summary-review { display: block;}\n\n.summary-review {height: 570px; overflow-y: auto;}\n\n.summary-review h2 { margin: 0 0 35px 20px; color: #317b36;font-size: 20px;}\n\n.personal1{ height: auto; margin: 20px 0 0;}\n\n.summary-review .terminination{\n    height: auto !important;\n}\n\n.cross-icon{\n    color: #f00;\n    cursor: pointer;\n}\n\n.recap-last {    padding: 0 0 30px;\n    display: inline-block;\n    width: 100%;}\n\n.empworkflow{\n\n    }"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.html":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.html ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management']\">\n        <button class=\"btn zenwork-customized-back-btn\">\n          <span class=\"green mr-7\">\n            <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n          </span>Back\n        </button>\n      </a>\n      <span class=\"inner-icon-img\">\n        <img src=\"../../../assets/images/Offboarding/Group 831.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </span>\n      <small class=\"sub-title\"><b>Offboarding</b></small>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"zenwork-custom-company-settings col-md-11\">\n    <div class=\"tron zenwork-jumbotron-bg\">\n      <div class=\"col-xs-12 text-center\">\n        <div class=\"row\">\n          <p class=\"hire-heading\">Termination Wizard</p>\n        </div>\n      </div>\n\n      <div class=\"text-center\">\n        <div class=\"zenwork-settings-wrapper\">\n\n          <a data-toggle=\"modal\" data-target=\"#myModal\">\n            <img src=\"../../../../../assets/images/employee-management/off-board.png\" alt=\"Directory icon\">\n            <p class=\"zenwork-margin-zero\">\n              <span class='new-sub-heading'>Standard Termination Wizard</span>\n            </p>\n          </a>\n        </div>\n      </div>\n\n    </div>\n  </div>\n\n\n  <hr class=\"zenwork-margin-ten-zero\">\n\n\n  <div class=\"zenwork-custom-company-settings col-md-11\">\n    <div class=\"tron zenwork-jumbotron-bg\">\n      <div class=\"col-xs-12 text-center\">\n        <div class=\"row\">\n          <p class=\"hire-heading\">Offboarding Template</p>\n        </div>\n\n        <div class=\"top-menu\">\n          <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\">\n            <button mat-menu-item>\n              <a (click)=\"customTemplateAdd()\">Add</a>\n            </button>\n\n            <button mat-menu-item>\n              <a>Copy</a>\n            </button>\n            <button mat-menu-item>\n              <a (click)=\"customTemplateDelete()\">Delete</a>\n            </button>\n          </mat-menu>\n        </div>\n\n      </div>\n      <div class=\"col-xs-12\">\n        <div class=\"col-xs-6 text-center\">\n          <div class=\"zenwork-settings-wrapper border-right\">\n\n            <a (click)=\"templateRedirect()\">\n              <img src=\"../../../../../assets/images/employee-management/off-board.png\" alt=\"Directory icon\">\n\n              <p class=\"zenwork-margin-zero\">\n                <span class='new-sub-heading'>{{standardOffboardTemplate.templateName}}</span>\n              </p>\n            </a>\n          </div>\n        </div>\n\n        <div *ngFor=\"let data of customOffboardTemplate\" class=\"col-xs-6 text-center custom-offboarding-temp\">\n\n          <div class=\"zenwork-settings-wrapper\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\"\n              (change)=\"showOptions($event,data._id)\">\n            </mat-checkbox>\n\n            <a (click)=\"customTemplateRedirect(data._id)\">\n              <img src=\"../../../../../assets/images/employee-management/off-board.png\" alt=\"Directory icon\">\n\n              <p class=\"zenwork-margin-zero\">\n\n                <span class='new-sub-heading'>{{data.templateName}}</span>\n\n              </p>\n            </a>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"clearfix\"></div>\n\n    </div>\n  </div>\n\n\n  <!-- <div class=\"zenwork-custom-company-settings col-md-11\">\n    <div class=\"tron zenwork-jumbotron-bg\">\n      <div class=\"col-xs-12 text-center\">\n        <div class=\"row\">\n          <p class=\"hire-heading\">Active Termination Wizard</p>\n        </div>\n      </div>\n\n      <div class=\"col-xs-6 text-center \">\n        <div class=\"zenwork-settings-wrapper border-right margin-bottom-20\">\n\n          <a (click)=\"openModalWithClass(template)\">\n            <img src=\"../../../assets/images/Onboarding/on_boarding2_grey.png\" alt=\"Directory icon\" class=\"img-responsive center-block settings-icon \">\n            <p class=\"zenwork-margin-zero\">\n              <span class='new-sub-heading'>Voluntary Termination</span>           \n            </p>\n          </a>\n          <ng-template #template>\n            <div class=\"modal-header modal-margintop\">\n              <b class=\"green\">Voluntary Termination</b>\n            </div>\n            <div class=\"modal-body\">\n              <div class=\"container\">\n                <div class=\"row\">\n                  <div class=\"tabbable tabs-left\">\n                    <ul class=\"nav nav-tabs col-md-3 zenwork-side-nav\">\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'terminationDetails' }\">\n                        <a href=\"#terminationDetails\" data-toggle=\"tab\" (click)=\"activeFieldss('terminationDetails')\">\n                          Termination Details\n                          <span [ngClass]=\"{'caret': selectedNav == 'terminationDetails' }\"></span>\n                        </a>\n                      </li>\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'timeOffData' }\">\n                        <a href=\"#timeOffData\" data-toggle=\"tab\" (click)=\"activeFieldss('timeOffData')\">Time Off\n                          Data\n                          <span [ngClass]=\"{'caret': selectedNav == 'timeOffData' }\"></span>\n                        </a>\n                      </li>\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'directReport' }\">\n                        <a href=\"#directReport\" data-toggle=\"tab\" (click)=\"activeFieldss('directReport')\"> Direct\n                          Report\n                          <span [ngClass]=\"{'caret': selectedNav == 'directReport' }\"></span>\n                        </a>\n                      </li>\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'companyProperty' }\">\n                        <a href=\"#companyProperty\" data-toggle=\"tab\" (click)=\"activeFieldss('companyProperty')\">\n                          Company Property\n                          <span [ngClass]=\"{'caret': selectedNav == 'companyProperty' }\"></span>\n                        </a>\n                      </li>\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'offboardingTask' }\">\n                        <a href=\"#offboardingTask\" data-toggle=\"tab\" (click)=\"activeFieldss('offboardingTask')\">\n                          Offboarding Task\n                          <span [ngClass]=\"{'caret': selectedNav == 'offboardingTask' }\"></span>\n                        </a>\n                      </li>\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'benefits' }\">\n                        <a href=\"#benefits\" data-toggle=\"tab\" (click)=\"activeFieldss('benefits')\"> Benefits\n                          <span [ngClass]=\"{'caret': selectedNav == 'benefits' }\"></span>\n                        </a>\n                      </li>\n                      <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'recap' }\">\n                        <a href=\"#recap\" data-toggle=\"tab\" (click)=\"activeFieldss('recap')\"> Recap\n                          <span [ngClass]=\"{'caret': selectedNav == 'recap' }\"></span>\n                        </a>\n                      </li>\n                    </ul>\n\n                    <div class=\"tab-content col-md-9\">\n                      <div class=\"tab-pane active\" id=\"terminationDetails\">\n                        <div class=\"emloyee-termination\">\n                          <div class=\"emp-terminate-filters\">\n                            <div class=\"form-group schdule-type\">\n                              <label>\n                                Select Employee\n                              </label><br>\n                              <mat-select class=\"form-control zenwork-input\" placeholder=\"Search-Employee\">\n                                <mat-option value=\"support\">\n                                  Employee 1\n                                </mat-option>\n                                <mat-option value=\"sr-support\">\n                                  Employee 2\n                                </mat-option>\n\n                              </mat-select>\n                            </div>\n                            <div class=\"form-group schdule-type\">\n                              <label>\n                                Employment Status\n                              </label><br>\n                              <mat-select class=\"form-control zenwork-input\" placeholder=\"Status\">\n                                <mat-option value=\"support\">\n                                  Terminated\n                                </mat-option>\n                                <mat-option value=\"sr-support\">\n                                  Active\n                                </mat-option>\n                              </mat-select>\n                            </div>\n                            <div class=\"form-group schdule-type  termination-date\">\n                              <label>Termination Date</label><br>\n                              <input class=\"form-control\"  #dpMDY=\"bsDatepicker\"\n                                bsDatepicker formControlName=\"myDateMDY\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                                placeholder=\"mm/dd/yyyy\" name=\"birth-date\" class=\"hiredate\">\n                              <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon\"\n                                alt=\"Company-settings icon\" (click)=\"dpMDY.toggle()\" [attr.aria-expanded]=\"dpMDY.isOpen\">\n                            </div>\n                            <div class=\"form-group schdule-type\">\n                              <label>\n                                Termination Reason\n                              </label><br>\n                              <mat-select class=\"form-control zenwork-input\" placeholder=\"A-Absent\">\n                                <mat-option value=\"support\">\n                                  Terminated\n                                </mat-option>\n                                <mat-option value=\"sr-support\">\n                                  Active\n                                </mat-option>\n                              </mat-select>\n                            </div>\n                            <div class=\"form-group schdule-type  termination-date\">\n                              <label>Last Day Worked</label><br>\n                              <input class=\"form-control\"  #dpMDYLDAY=\"bsDatepicker\"\n                                bsDatepicker formControlName=\"myDateMDY\" [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                                placeholder=\"mm/dd/yyyy\" name=\"birth-date\" class=\"hiredate\">\n                              <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon\"\n                                alt=\"Company-settings icon\" (click)=\"dpMDYLDAY.toggle()\" [attr.aria-expanded]=\"dpMDYLDAY.isOpen\">\n                            </div>\n                            <div class=\"form-group schdule-type\">\n                              <label>\n                                Eligible for Reason\n                              </label><br>\n                              <mat-select class=\"form-control zenwork-input\" placeholder=\"A-Absent\">\n                                <mat-option value=\"support\">\n                                  Terminated\n                                </mat-option>\n                                <mat-option value=\"sr-support\">\n                                  Active\n                                </mat-option>\n                              </mat-select>\n                            </div>\n                            <div class=\"form-group schdule-type text-area\">\n                              <label>Comments</label><br>\n                              <textarea class=\"form-control rounded-0\" rows=\"3\"></textarea>\n                            </div>\n                            <div class=\"form-group schdule-type \">\n                              <label>Attach File(s) Max-5 Files</label><br>\n                              <button class=\"btn btn-success attach-file-btn\">Add Files\n                                <i class=\"fa fa-paperclip clip-icon\" aria-hidden=\"true\"></i>\n                                <div class=\"clearfix\"></div>\n                              </button>\n                            </div>\n                            <div class=\"form-group schdule-type documents-list\">\n                              <ul class=\"list-unstyled\">\n                                <li class=\"list-items\">\n                                  <small><i class=\"fa fa-times\" aria-hidden=\"true\"></i></small>\n                                  <span>Documents</span>\n                                </li>\n                                <li class=\"list-items\">\n                                  <small><i class=\"fa fa-times\" aria-hidden=\"true\"></i></small>\n                                  <span>Documents</span>\n                                </li>\n                                <li>\n\n                                </li>\n                                <li>\n\n                                </li>\n                                <li>\n\n                                </li>\n                              </ul>\n                            </div>\n                            <div class=\"form-group schdule-type\">\n                              <ul class=\"submit-buttons list-unstyled\">\n                                <li class=\"list-item\">\n                                  <button class=\"btn btn-success btn-style\"> Proceed </button>\n                                </li>\n                                <li class=\"list-item\">\n                                  <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> cancel </button>\n                                </li>\n                              </ul>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"tab-pane\" id=\"timeOffData\">\n                        <div class=\"custom-tables time-off-data\">\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n                                <th>\n                                  <div class=\"heade-check\">\n                                    <div class=\"checkbox\">\n                                      <input id=\"checkbox20\" type=\"checkbox\">\n                                      <label for=\"checkbox20\"></label>\n                                    </div>\n                                  </div>\n                                </th>\n                                <th>Policy Name</th>\n                                <th>Current Balance</th>\n                                <th>Balance as of Termination Date</th>\n                                <th>\n                                  <a>\n                                    <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n                                  </a>\n                                  <a>\n                                    <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n                                  </a>\n                                </th>\n                              </tr>\n                            </thead>\n                            <tbody>\n                              <tr>\n                                <td class=\"zenwork-assets-checked-border\">\n\n                                  <div class=\"cont-check\">\n                                    <div class=\"checkbox\">\n                                      <input id=\"checkbox_21\" type=\"checkbox\">\n                                      <label for=\"checkbox_21\"></label>\n                                    </div>\n                                  </div>\n\n                                </td>\n                                <td>Policy 1</td>\n                                <td>05:30</td>\n                                <td>\n                                  09:30\n                                </td>\n                                <td>\n\n                                </td>\n                              </tr>\n                              <tr>\n                                <td class=\"zenwork-assets-checked-border\">\n\n                                  <div class=\"cont-check\">\n                                    <div class=\"checkbox\">\n                                      <input id=\"checkbox_21\" type=\"checkbox\">\n                                      <label for=\"checkbox_21\"></label>\n                                    </div>\n                                  </div>\n\n                                </td>\n                                <td>Policy 1</td>\n                                <td>05:30</td>\n                                <td>\n                                  09:30\n                                </td>\n                                <td>\n\n                                </td>\n                              </tr>\n                              <tr>\n                                <td class=\"zenwork-assets-checked-border\">\n\n                                  <div class=\"cont-check\">\n                                    <div class=\"checkbox\">\n                                      <input id=\"checkbox_21\" type=\"checkbox\">\n                                      <label for=\"checkbox_21\"></label>\n                                    </div>\n                                  </div>\n\n                                </td>\n                                <td>Policy 1</td>\n                                <td>05:30</td>\n                                <td>\n                                  09:30\n                                </td>\n                                <td>\n                                </td>\n                              </tr>\n                            </tbody>\n                          </table>\n                        </div>\n                        <div class=\"form-group schdule-type zenwork-margin-30\">\n                          <ul class=\"submit-buttons list-unstyled\">\n                            <li class=\"list-item\">\n                              <button class=\"btn btn-success btn-style\"> Proceed </button>\n                            </li>\n                            <li class=\"list-item\">\n                              <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> cancel </button>\n                            </li>\n                          </ul>\n                        </div>\n                      </div>\n                      <div class=\"tab-pane\" id=\"directReport\">\n                        <div class=\"custom-tables time-off-data direct-report\">\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n\n                                <th>Employee Names</th>\n                                <th>Job</th>\n                                <th>Department</th>\n                                <th>Location</th>\n                                <th>New Manager</th>\n\n                              </tr>\n                            </thead>\n                            <tbody>\n                              <tr>\n                                <td>Employee 1</td>\n                                <td>Manager</td>\n                                <td>\n                                  Marketing\n                                </td>\n                                <td>\n                                  US\n                                </td>\n                                <td>\n                                  <div class=\"form-group schdule-type\">\n                                    <mat-select class=\"form-control zenwork-input\" placeholder=\"Manager\">\n                                      <mat-option value=\"support\">\n                                        Manager 2\n                                      </mat-option>\n                                      <mat-option value=\"sr-support\">\n                                        Manager 3\n                                      </mat-option>\n                                    </mat-select>\n                                  </div>\n                                </td>\n                              </tr>\n                              <tr>\n                                <td>Employee 1</td>\n                                <td>Manager</td>\n                                <td>\n                                  Marketing\n                                </td>\n                                <td>\n                                  US\n                                </td>\n                                <td>\n                                  <div class=\"form-group schdule-type\">\n                                    <mat-select class=\"form-control zenwork-input\" placeholder=\"Manager\">\n                                      <mat-option value=\"support\">\n                                        Manager 2\n                                      </mat-option>\n                                      <mat-option value=\"sr-support\">\n                                        Manager 3\n                                      </mat-option>\n                                    </mat-select>\n                                  </div>\n                                </td>\n                              </tr>\n                              <tr>\n                                <td>Employee 1</td>\n                                <td>Manager</td>\n                                <td>\n                                  Marketing\n                                </td>\n                                <td>\n                                  US\n                                </td>\n                                <td>\n                                  <div class=\"form-group schdule-type\">\n                                    <mat-select class=\"form-control zenwork-input\" placeholder=\"Manager\">\n                                      <mat-option value=\"support\">\n                                        Manager 2\n                                      </mat-option>\n                                      <mat-option value=\"sr-support\">\n                                        Manager 3\n                                      </mat-option>\n                                    </mat-select>\n                                  </div>\n                                </td>\n                              </tr>\n                            </tbody>\n                          </table>\n\n                        </div>\n                        <div class=\"form-group zenwork-margin-30\">\n                          <ul class=\"submit-buttons list-unstyled\">\n                            <li class=\"list-item\">\n                              <button class=\"btn btn-success btn-style\"> Proceed </button>\n                            </li>\n                            <li class=\"list-item\">\n                              <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> cancel </button>\n                            </li>\n                          </ul>\n                        </div>\n                      </div>\n                      <div class=\"tab-pane\" id=\"companyProperty\">\n                        <div class=\"company-property\">\n                          <div class=\"custom-tables time-off-data \">\n                            <table class=\"table\">\n                              <thead>\n                                <tr>\n                                  <th>\n                                    <div class=\"heade-check\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox20\" type=\"checkbox\">\n                                        <label for=\"checkbox20\"></label>\n                                      </div>\n                                    </div>\n                                  </th>\n                                  <th>Asset category</th>\n                                  <th>Asset description</th>\n                                  <th>Serial#</th>\n                                  <th>Date loaned</th>\n                                  <th>Date returned</th>\n                                  <th>\n                                    <a>\n                                      <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n                                    </a>\n                                  </th>\n                                </tr>\n                              </thead>\n                              <tbody>\n                                <tr>\n                                  <td class=\"zenwork-assets-checked-border\">\n\n                                    <div class=\"cont-check\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox_21\" type=\"checkbox\">\n                                        <label for=\"checkbox_21\"></label>\n                                      </div>\n                                    </div>\n\n                                  </td>\n                                  <td>CellPhone</td>\n                                  <td>Assets</td>\n                                  <td>\n                                    24399-3426\n                                  </td>\n                                  <td>\n                                    07/24/2018\n                                  </td>\n                                  <td>\n                                    07/24/2018\n                                  </td>\n                                  <td>\n\n                                  </td>\n                                </tr>\n                                <tr>\n                                  <td class=\"zenwork-assets-checked-border\">\n\n                                    <div class=\"cont-check\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox_21\" type=\"checkbox\">\n                                        <label for=\"checkbox_21\"></label>\n                                      </div>\n                                    </div>\n\n                                  </td>\n                                  <td>CellPhone</td>\n                                  <td>Assets</td>\n                                  <td>\n                                    24399-3426\n                                  </td>\n                                  <td>\n                                    07/24/2018\n                                  </td>\n                                  <td>\n                                    07/24/2018\n                                  </td>\n                                  <td>\n\n                                  </td>\n                                </tr>\n                                <tr>\n                                  <td class=\"zenwork-assets-checked-border\">\n\n                                    <div class=\"cont-check\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox_21\" type=\"checkbox\">\n                                        <label for=\"checkbox_21\"></label>\n                                      </div>\n                                    </div>\n\n                                  </td>\n                                  <td>CellPhone</td>\n                                  <td>Assets</td>\n                                  <td>\n                                    24399-3426\n                                  </td>\n                                  <td>\n                                    07/24/2018\n                                  </td>\n                                  <td>\n                                    07/24/2018\n                                  </td>\n                                  <td>\n\n                                  </td>\n                                </tr>\n\n                              </tbody>\n                            </table>\n                          </div>\n                        </div>\n                        <div class=\"form-group zenwork-margin-30\">\n                          <ul class=\"submit-buttons list-unstyled\">\n                            <li class=\"list-item\">\n                              <button class=\"btn btn-success btn-style\"> Proceed </button>\n                            </li>\n                            <li class=\"list-item\">\n                              <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> cancel </button>\n                            </li>\n                          </ul>\n                        </div>\n                      </div>\n                      <div class=\"tab-pane\" id=\"offboardingTask\">\n\n                      </div>\n                      <div class=\"tab-pane\" id=\"benefits\">\n                        <div class=\"benefits-offboarding main-onboarding\">\n                          <div class=\"panel panel-info\">\n                            <div class=\"panel-heading\">\n                              <h3 class=\"panel-title panel-font pull-left\">Offboarding Tasks</h3>\n                              <div class=\"settings-dropdown add-task-setting-btns\">\n                                <button class=\"btn btn-default task-btns\" type=\"button\" id=\"dropdownMenu1\"\n                                  data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">\n                                  + Add Tasks\n                                </button>\n                                <button class=\"btn btn-default task-btns dropdown-toggle\" type=\"button\" id=\"dropdownMenu1\"\n                                  data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">\n                                  <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>&nbsp;\n                                  <span class=\"caret\"></span>\n                                </button>\n                              </div>\n                              <div class=\"clearfix\"></div>\n                            </div>\n                            <div class=\"offboardingtask\">\n                              <div class=\"panel-body\">\n                                <p class=\"hr-tasks green\">HR Tasks</p>\n                                <p class=\"individual-hr-task pull-left\">Cobra<br>\n                                  <small>Assigned to:Ashley Adams<br>\n                                    <span>Completed Date</span>\n                                  </small>\n                                </p>\n                                <span class=\"done-not-icons pull-right\">\n                                  <ul class=\"list-unstyled\">\n                                    <li class=\"list-items\">\n                                      <i class=\"fa fa-times cross-icon\" aria-hidden=\"true\"></i>\n                                    </li>\n                                    <li class=\"list-items\">\n                                      <i class=\"fa fa-check-square green\" aria-hidden=\"true\"></i>\n                                    </li>\n                                  </ul>\n                                </span>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                              <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n                              <div class=\"panel-body padding-top-remove\">\n                                <p class=\"individual-hr-task pull-left\">Collect Employee ID Badge<br>\n                                  <small>Assigned to:Ashley Adams<br>\n                                    <span>Completed Date</span>\n                                  </small>\n                                </p>\n                                <span class=\"done-not-icons pull-right\">\n                                  <ul class=\"list-unstyled\">\n                                    <li class=\"list-items\">\n                                      <i class=\"fa fa-times cross-icon\" aria-hidden=\"true\"></i>\n                                    </li>\n                                    <li class=\"list-items\">\n                                      <i class=\"fa fa-check-square green\" aria-hidden=\"true\"></i>\n                                    </li>\n                                  </ul>\n                                </span>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                              <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n                              <div class=\"panel-body padding-top-remove\">\n                                <p class=\"individual-hr-task pull-left\">Check Vacation Payout/Accural<br>\n                                  <small>Assigned to:Ashley Adams<br>\n                                    <span>Completed Date</span>\n                                  </small>\n                                </p>\n                                <span class=\"done-not-icons pull-right\">\n                                  <ul class=\"list-unstyled\">\n                                    <li class=\"list-items\">\n                                      <i class=\"fa fa-times cross-icon\" aria-hidden=\"true\"></i>\n                                    </li>\n                                    <li class=\"list-items\">\n                                      <i class=\"fa fa-check-square green\" aria-hidden=\"true\"></i>\n                                    </li>\n                                  </ul>\n                                </span>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </div>\n                          </div>\n                          <div class=\"panel panel-info\">\n                            <div class=\"offboardingtask\">\n                              <div class=\"panel-body\">\n                                <p class=\"hr-tasks green\">IT Setup</p>\n                                <p class=\"individual-hr-task pull-left\">Disable Email Access<br>\n                                  <small>Assigned to:Ashley Adams<br>\n                                    <span>Completed Date</span>\n                                  </small>\n                                </p>\n                                <span class=\"done-not-icons pull-right\">\n                                    <ul class=\"list-unstyled\">\n                                      <li class=\"list-items\">\n                                          <i class=\"fa fa-times cross-icon\" aria-hidden=\"true\"></i>\n                                      </li>\n                                      <li class=\"list-items\">\n                                          <i class=\"fa fa-check-square green\" aria-hidden=\"true\"></i>\n                                      </li>\n                                    </ul>\n                                  </span>\n                                  <div class=\"clearfix\"></div>\n                              </div>\n                            </div>\n                            \n                          </div>\n                          <div class=\"panel panel-info\">\n                            <div class=\"offboardingtask\">\n                              <div class=\"panel-body\">\n                                <p class=\"hr-tasks green\">Manager Tasks</p>\n                                <p class=\"individual-hr-task pull-left\">Collect Company Assets<br>\n                                  <small>Assigned to:Ashley Adams<br>\n                                    <span>Completed Date</span>\n                                  </small>\n                                </p>\n                                <span class=\"done-not-icons pull-right\">\n                                    <ul class=\"list-unstyled\">\n                                      <li class=\"list-items\">\n                                          <i class=\"fa fa-times cross-icon\" aria-hidden=\"true\"></i>\n                                      </li>\n                                      <li class=\"list-items\">\n                                          <i class=\"fa fa-check-square green\" aria-hidden=\"true\"></i>\n                                      </li>\n                                    </ul>\n                                  </span>\n                                  <div class=\"clearfix\"></div>\n                              </div>\n                            </div>\n                          </div>\n                          <div class=\"form-group zenwork-margin-30\">\n                            <ul class=\"submit-buttons list-unstyled\">\n                              <li class=\"list-item\">\n                                <button class=\"btn btn-success btn-style\"> Proceed </button>\n                              </li>\n                              <li class=\"list-item\">\n                                <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> cancel </button>\n                              </li>\n                            </ul>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"tab-pane\" id=\"recap\">\n\n                      </div>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </ng-template>\n        </div>\n      </div>\n      \n      <div class=\"col-xs-6 text-center\">\n        <div class=\"zenwork-settings-wrapper\">\n\n          <a [routerLink]=\"[]\">\n            <img src=\"../../../assets/images/Onboarding/on_boarding1_grey.png\" alt=\"on-boarding icon\" class=\"img-responsive center-block settings-icon\">\n            <p class=\"zenwork-margin-zero\">\n              <span class='new-sub-heading'>Involuntary Termination</span>\n            </p>\n          </a>\n        </div>\n      </div>\n\n      <div class=\"clearfix\"></div>\n\n      <div class=\"col-xs-12 text-center\">\n        <div class=\"zenwork-settings-wrapper border-top\">\n          <div class=\"col-xs-12\">\n            <div class=\"row\">\n              <p class=\"hire-heading\">Modify / Create Termination Wizard</p>\n            </div>\n          </div>\n\n          <a [routerLink]=\"['/admin/admin-dashboard/employee-management/off-boarding-landing/termination-wizard/termination-wizard']\">\n            <img src=\"../../../assets/images/Onboarding/on_boarding3_grey.png\" alt=\"employee-self icon\" class=\"img-responsive center-block settings-icon\">\n            <p class=\"zenwork-margin-zero\">\n              <span class='new-sub-heading'>Termination Wizard</span>\n            </p>\n\n          </a>\n        </div>\n      </div>\n\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>-->\n\n\n  <!-- Author:Suresh M, Date:06-05-19  -->\n  <!-- Standard Termination Wizard -->\n\n  <div class=\"new-wizard\">\n\n    <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n\n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <h4 class=\"modal-title\">Standard Termination Wizard <br>\n              (Add Employee Termination - Details)</h4>\n          </div>\n          <div class=\"modal-body\">\n\n            <div class=\"wizard-tabs\">\n\n              <div class=\"wizard-lft col-md-3\">\n                <ul class=\"nav nav-tabs tabs-left\">\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'ClientInfo' }\">\n                    <a href=\"#ClientInfo\" data-toggle=\"tab\" #client_Info\n                      (click)=\"terminationDetails('ClientInfo')\">Termination Details</a></li>\n\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab2' }\">\n                    <a href=\"#tab2\" data-toggle=\"tab\" #offboardTask (click)=\"terminationDetails('tab2')\">Offboarding\n                      Tasks</a></li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab3' }\">\n                    <a href=\"#tab3\" data-toggle=\"tab\" #directReports (click)=\"terminationDetails('tab3')\">Direct\n                      Reports</a></li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tasks' }\">\n                    <a href=\"#tasks\" data-toggle=\"tab\" #directTasks (click)=\"terminationDetails('tasks')\">Direct\n                      Tasks</a></li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab4' }\">\n                    <a href=\"#tab4\" data-toggle=\"tab\" #companyProperty (click)=\"terminationDetails('tab4')\">Company\n                      Property</a></li>\n\n                  <li><a href=\"#tab5\" data-toggle=\"tab\">Leave Management</a></li>\n                  <li><a href=\"#tab6\" data-toggle=\"tab\">Benefits</a></li>\n\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab7' }\">\n                    <a href=\"#tab7\" data-toggle=\"tab\" (click)=\"terminationDetails('tab7')\">Recap</a></li>\n                </ul>\n              </div>\n\n              <div class=\"wizard-rgt col-md-9\">\n\n\n                <div class=\"tab-content\">\n\n                  <div class=\"tab-pane\" id=\"ClientInfo\"\n                    [ngClass]=\"{'active':selectedNav === 'ClientInfo','in':selectedNav==='ClientInfo'}\">\n\n                    <div class=\"details-main\">\n\n                      <form [formGroup]=\"terminationForm\">\n\n                        <div class=\"terminination\">\n\n                          <ul>\n                            <li class=\"emp\" style=\"width:25%;\">\n                              <label>Select Employee *</label>\n                              <!-- \n                              <ng-select [allowClear]=\"true\" [items]=\"items\" [disabled]=\"disable\"\n                                formControlName=\"Employee\" (data)=\"refreshValue($event)\" (selected)=\"selected($event)\"\n                                (removed)=\"removed($event)\" (typed)=\"typed($event)\" placeholder=\"Select Employee\">\n                              </ng-select> -->\n                              <mat-select class=\"form-control\" placeholder=\"Select Employee\"\n                                (ngModelChange)=\"employeeForDirectReports()\" formControlName=\"Employee\" required>\n                                <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>\n                                  {{data.personal.name.preferredName}}</mat-option>\n                              </mat-select>\n                              <span\n                                *ngIf=\"terminationForm.get('Employee').invalid && terminationForm.get('Employee').touched\"\n                                class=\"error\">You must select a employee</span>\n                              <div *ngIf=\"empWorkFlow\" class=\"error empworkflow pull-right\">This employee is assigned in\n                                one or more workflows.\n                              </div>\n                              <div class=\"clearfix\"></div>\n                            </li>\n\n                            <li class=\"col-md-3\">\n                              <label>Employment Status *</label>\n                              <mat-select class=\"form-control\" placeholder=\"Status\" formControlName=\"Status\">\n                                <mat-option *ngFor=\"let data of allEmployeeStatusArray\" [value]='data.name'>\n                                  {{data.name}}</mat-option>\n                              </mat-select>\n                              <span\n                                *ngIf=\"terminationForm.get('Status').invalid && terminationForm.get('Status').touched\"\n                                class=\"error\">You must select a employee status</span>\n                            </li>\n                            <li class=\"col-md-3\">\n                              <label>Termination Type *</label>\n                              <mat-select class=\"form-control\" placeholder=\"Type\" formControlName=\"Type\">\n                                <mat-option *ngFor=\"let data of allTerminationTypeArray\" [value]='data.name'>\n                                  {{data.name}}</mat-option>\n\n                              </mat-select>\n                              <span *ngIf=\"terminationForm.get('Type').invalid && terminationForm.get('Type').touched\"\n                                class=\"error\">You must select a termination type</span>\n                            </li>\n                            <div class=\"clearfix\"></div>\n                          </ul>\n\n\n                          <ul>\n\n                            <li class=\"col-md-3\">\n                              <label>Termination Reason *</label>\n                              <mat-select class=\"form-control\" placeholder=\"Reason\" formControlName=\"Reason\">\n                                <mat-option *ngFor=\"let data of allTerminationReasonArray\" [value]='data.name'>\n                                  {{data.name}}</mat-option>\n                              </mat-select>\n                              <span\n                                *ngIf=\"terminationForm.get('Reason').invalid && terminationForm.get('Reason').touched\"\n                                class=\"error\">You must select a termination reason</span>\n                            </li>\n                            <li class=\"col-md-3\">\n                              <label>Eligible for Rehire *</label>\n                              <mat-select class=\"form-control\" placeholder=\"Status\" formControlName=\"Eligible\">\n                                <mat-option value='yes'>Yes</mat-option>\n                                <mat-option value='no'>No</mat-option>\n                              </mat-select>\n                              <span\n                                *ngIf=\"terminationForm.get('Eligible').invalid && terminationForm.get('Eligible').touched\"\n                                class=\"error\">You must select a eligibility for rehire</span>\n                            </li>\n                            <div class=\"clearfix\"></div>\n                          </ul>\n\n\n                          <ul>\n                            <li class=\"col-md-3\">\n                              <label>Termination Date *</label>\n                              <div class=\"date\">\n                                <input matInput [matDatepicker]=\"picker\" [max]=\"lastWorkingDate\"\n                                  (ngModelChange)=\"terminationDateChange()\" placeholder=\"mm / dd / yy\"\n                                  class=\"form-control\" formControlName=\"Date1\">\n                                <mat-datepicker #picker></mat-datepicker>\n\n                                <button mat-raised-button (click)=\"picker.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n\n                                <div class=\"clearfix\"></div>\n\n                              </div>\n                              <span *ngIf=\"terminationForm.get('Date1').invalid && terminationForm.get('Date1').touched\"\n                                class=\"error\">You must select a Terminate date</span>\n                            </li>\n\n                            <li class=\"col-md-3\">\n                              <label>Last Day Worked *</label>\n                              <div class=\"date\">\n                                <input matInput [matDatepicker]=\"picker1\" (ngModelChange)=\"lastWorkingDateChange()\"\n                                  [min]=\"terminationDate\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  formControlName=\"Date2\">\n                                <mat-datepicker #picker1></mat-datepicker>\n\n                                <button mat-raised-button (click)=\"picker1.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n\n                                <div class=\"clearfix\"></div>\n\n                              </div>\n                              <span *ngIf=\"terminationForm.get('Date2').invalid && terminationForm.get('Date2').touched\"\n                                class=\"error\">You must select a last working date</span>\n                            </li>\n                            <div class=\"clearfix\"></div>\n                          </ul>\n\n                          <ul>\n\n                            <li class=\"details-width col-md-4\">\n                              <label>Offboarding Template Assigned *</label>\n                              <mat-select class=\"form-control\" placeholder=\"Template\" (ngModelChange)=\"templateChange()\"\n                                formControlName=\"Template\">\n                                <mat-option *ngFor=\"let data of tempTemplateTotal\" [value]='data._id'>\n                                  {{data.templateName}}</mat-option>\n                              </mat-select>\n                              <span\n                                *ngIf=\"terminationForm.get('Template').invalid && terminationForm.get('Template').touched\"\n                                class=\"error\">You must select a Template</span>\n\n                            </li>\n\n                            <li class=\"procees col-md-8\">\n                              <label>Terminate any assigned Performance Review in procees?</label>\n                              <mat-select class=\"form-control\" placeholder=\"Procees\" formControlName=\"Procees\">\n                                <mat-option value=yes>Yes</mat-option>\n                                <mat-option value=no>No</mat-option>\n                              </mat-select>\n                              <span style=\"display: block;\"\n                                *ngIf=\"terminationForm.get('Procees').invalid && terminationForm.get('Procees').touched\"\n                                class=\"error\">You must select a review process</span>\n                            </li>\n\n                            <li class=\"details-width col-md-8\">\n                              <label>Comments *</label>\n                              <textarea class=\"form-control\" rows=\"5\" formControlName=\"Comments\"></textarea>\n                              <span\n                                *ngIf=\"terminationForm.get('Comments').invalid && terminationForm.get('Comments').touched\"\n                                class=\"error\">please enter a comment</span>\n                            </li>\n\n                            <li>\n                              <div class=\"file-upload\">\n                                <label for=\"upload\" class=\"file-upload__label\">Add Files</label>\n                                <span><i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i></span>\n                                <input id=\"upload\" class=\"file-upload__input\" (change)=\"fileChange($event)\" type=\"file\"\n                                  name=\"file-upload\" multiple>\n                              </div>\n                            </li>\n                            <li *ngFor=\"let data of filesArray; let i = index\">\n                              <i class=\"fa fa-times cross-icon\" (click)=\"removeFile(i,data)\"></i>&nbsp;&nbsp;\n                              {{data}}\n                            </li>\n\n\n                          </ul>\n\n                        </div>\n                      </form>\n\n                      <div class=\"termination-border\">\n\n                        <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                        <button class=\"btn pull-right save\" (click)=\"terminationProceed()\">Proceed</button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n                    </div>\n\n                  </div>\n\n                  <div class=\"tab-pane\" id=\"tab2\"\n                    [ngClass]=\"{'active':selectedNav === 'tab2','in':selectedNav==='tab2'}\">\n                    <div class=\"details-main\">\n\n                      <div class=\"tasks\">\n                        <h3>Onboarding Template Name </h3>\n                        <!-- <input type=\"text\" class=\"form-control\" value=\"\" placeholder=\"\"> -->\n                        <mat-select [disabled]=true class=\"form-control\" placeholder=\"Template\"\n                          [(ngModel)]=\"OffboardingTemplateName\">\n                          <mat-option *ngFor=\"let data of tempTemplateTotal\" [value]='data._id'>\n                            {{data.templateName}}</mat-option>\n                        </mat-select>\n                      </div>\n\n                      <div class=\"off-boarding\">\n\n                        <div class=\"tasks\">\n\n                          <div class=\"tasks-in\">\n                            <h3>Offboarding Tasks</h3>\n\n                          </div>\n\n                          <ul>\n\n                            <li *ngFor=\"let task of getKeys((standardOffboardTemplate['offBoardingTasks']))\">\n                              <h4>{{task}}</h4>\n                              <!-- <h4>Hr tasks</h4> -->\n                              <div *ngFor=\"let data of standardOffboardTemplate['offBoardingTasks'][task]\"\n                                class=\"task-names\">\n\n                                <span>{{data.taskName}}</span>\n                                <small>{{data.assigneeEmail}} -- {{data.Date | date}}</small>\n                              </div>\n\n                            </li>\n                          </ul>\n\n\n                        </div>\n\n\n                      </div>\n\n\n                      <div class=\"termination-border\"></div>\n                      <!-- <button class=\"btn pull-left back\" (click)=\"goToTermination()\">Back</button> -->\n                      <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                      <button class=\"btn pull-right save\" (click)=\"taskProceed()\">Proceed</button>\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n\n                  </div>\n\n\n                  <div class=\"tab-pane\" id=\"tab3\"\n                    [ngClass]=\"{'active':selectedNav === 'tab3','in':selectedNav==='tab3'}\">\n\n                    <div class=\"details-main\">\n\n                      <div class=\"reports\">\n                        <h3>Does the employee have direct reports assigned?</h3>\n                        <mat-select class=\"form-control\" placeholder=\"Reports\">\n                          <mat-option value=Yes>Yes</mat-option>\n                          <mat-option value=No>No</mat-option>\n                        </mat-select>\n\n                        <div class=\"reports-table\">\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n                                <th>Employee Names</th>\n                                <th>Job</th>\n                                <th>Department</th>\n                                <th>Location</th>\n                                <th>New Manager</th>\n                              </tr>\n                            </thead>\n                            <tbody>\n                              <tr *ngFor=\"let data of directReportsArray\">\n                                <td>{{data.personal.name.preferredName}}</td>\n                                <td>{{data.job.jobTitle}}</td>\n                                <td>{{data.job.department}}</td>\n                                <td>{{data.personal.address.state}}</td>\n                                <td>\n                                  <mat-select class=\"form-control\" (selectionChange)=\"managerChange(data._id,$event)\"\n                                    placeholder=\"Managers\">\n                                    <mat-option *ngFor=\"let data of manageremployees\" [value]='data._id'>\n                                      {{data.personal.name.preferredName}}</mat-option>\n                                  </mat-select>\n                                </td>\n                              </tr>\n\n\n\n                          </table>\n\n                        </div>\n                      </div>\n                      <div class=\"termination-border\"></div>\n                      <button class=\"btn pull-left back\" (click)=\"goToOffboard()\">Back</button>\n                      <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                      <button class=\"btn pull-right save\" (click)=\"reportsProceed()\">Proceed</button>\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n\n                  </div>\n\n                  <div class=\"tab-pane\" id=\"tasks\"\n                    [ngClass]=\"{'active':selectedNav === 'tasks','in':selectedNav==='tasks'}\">\n\n                    <div class=\"details-main\">\n\n                      <div class=\"reports\">\n                        <h3>Does the employee have direct tasks assigned?</h3>\n                        <mat-select class=\"form-control\" placeholder=\"Reports\">\n                          <mat-option value=Yes>Yes</mat-option>\n                          <mat-option value=No>No</mat-option>\n                        </mat-select>\n\n                        <div class=\"reports-table\">\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n                                <th>Assigned Tasks</th>\n                                <th>TaskType</th>\n                                <th>TaskCategory</th>\n\n                                <th>New Employee</th>\n                              </tr>\n                            </thead>\n\n                            <tbody>\n                              <tr *ngFor=\"let data of directTasksArray;let i = index\">\n                                <td> {{data.taskName}}</td>\n\n                                <td>{{data.taskType}}</td>\n                                <td> {{data.taskCategory}}</td>\n                                <!-- <td></td> -->\n                                <td>\n                                  <mat-select class=\"form-control\" (selectionChange)=\"employeeChange(data,$event,i)\"\n                                    placeholder=\"Employees\">\n\n                                    <mat-option *ngFor=\"let data of employeeData\" [value]='data'>\n                                      {{data.personal.name.preferredName}}</mat-option>\n                                  </mat-select>\n                                  <span *ngIf=\"data.validate\" class=\"error\">Please select employee </span>\n                                </td>\n                              </tr>\n\n\n\n                          </table>\n\n                        </div>\n                      </div>\n                      <div class=\"termination-border\"></div>\n                      <button class=\"btn pull-left back\">Back</button>\n                      <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                      <button class=\"btn pull-right save\" (click)=\"directTasksEmp()\">Proceed</button>\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n\n                  </div>\n\n\n                  <div class=\"tab-pane\" id=\"tab4\"\n                    [ngClass]=\"{'active':selectedNav === 'tab4','in':selectedNav==='tab4'}\">\n\n                    <div class=\"details-main\">\n\n                      <div class=\"reports\">\n                        <h3>Does the employee have direct company Property?</h3>\n                        <mat-select class=\"form-control\" placeholder=\"Property\">\n                          <mat-option value=Yes>Yes</mat-option>\n                          <mat-option value=No>No</mat-option>\n                        </mat-select>\n\n                        <div class=\"company-property\">\n\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n                                <!-- <th>\n                                  <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                                </th> -->\n                                <th>Asset category</th>\n                                <th>Asset description</th>\n                                <th>Serial#</th>\n                                <th>Date loaned</th>\n                                <th>Date returned</th>\n                              </tr>\n                            </thead>\n                            <tbody>\n                              <tr *ngFor=\"let data of assetsData\">\n                                <!-- <td>\n                                  <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                                </td> -->\n                                <td>{{data.category}}</td>\n                                <td>{{data.description}}</td>\n                                <td>{{data.serial_number}}</td>\n                                <td>{{data.date_loaned | date}}</td>\n                                <td>\n\n                                  <span *ngIf=\"data.date_returned\">{{data.date_returned | date}}</span>\n                                  <span *ngIf=\"!data.date_returned\"> -- </span>\n\n                                </td>\n                              </tr>\n\n                            </tbody>\n                          </table>\n\n                        </div>\n\n                      </div>\n                      <div class=\"termination-border\"></div>\n                      <button class=\"btn pull-left back\" (click)=\"goToDirect()\">Back</button>\n                      <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                      <button class=\"btn pull-right save\" (click)=\"companyProceed()\">Proceed</button>\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n\n                  </div>\n\n\n\n\n                  <div class=\"tab-pane\" id=\"tab5\">\n\n\n\n                  </div>\n\n                  <div class=\"tab-pane\" id=\"tab6\">\n\n\n\n                  </div>\n\n                  <div class=\"tab-pane\" id=\"tab7\"\n                    [ngClass]=\"{'active':selectedNav === 'tab7','in':selectedNav==='tab7'}\">\n                    <div class=\"summary-review\">\n                      <div class=\"tab-pane\">\n                        <div class=\"personal personal1\">\n                          <h2>Termination Details</h2>\n                          <div class=\"details-main\">\n                            <form [formGroup]=\"terminationForm\">\n\n                              <div class=\"terminination\">\n\n                                <ul>\n                                  <li class=\"emp\" style=\"width:25%;\">\n                                    <label>Select Employee *</label>\n                                    <!-- \n                                      <ng-select [allowClear]=\"true\" [items]=\"items\" [disabled]=\"disable\"\n                                        formControlName=\"Employee\" (data)=\"refreshValue($event)\" (selected)=\"selected($event)\"\n                                        (removed)=\"removed($event)\" (typed)=\"typed($event)\" placeholder=\"Select Employee\">\n                                      </ng-select> -->\n                                    <mat-select class=\"form-control\" placeholder=\"Select Employee\"\n                                      formControlName=\"Employee\" required [disabled]=true>\n                                      <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>\n                                        {{data.personal.name.preferredName}}</mat-option>\n                                    </mat-select>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Employee').invalid && terminationForm.get('Employee').touched\"\n                                      class=\"error\">You must select a employee</span>\n\n\n                                  </li>\n\n                                  <li class=\"col-md-3\">\n                                    <label>Employment Status *</label>\n                                    <mat-select class=\"form-control\" placeholder=\"Status\" formControlName=\"Status\"\n                                      [disabled]=true>\n                                      <mat-option *ngFor=\"let data of allEmployeeStatusArray\" [value]='data.name'>\n                                        {{data.name}}</mat-option>\n                                    </mat-select>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Status').invalid && terminationForm.get('Status').touched\"\n                                      class=\"error\">You must select a employee status</span>\n                                  </li>\n                                  <li class=\"col-md-3\">\n                                    <label>Termination Type *</label>\n                                    <mat-select class=\"form-control\" placeholder=\"Type\" formControlName=\"Type\"\n                                      [disabled]=true>\n                                      <mat-option *ngFor=\"let data of allTerminationTypeArray\" [value]='data.name'>\n                                        {{data.name}}</mat-option>\n\n                                    </mat-select>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Type').invalid && terminationForm.get('Type').touched\"\n                                      class=\"error\">You must select a termination type</span>\n                                  </li>\n                                  <div class=\"clearfix\"></div>\n                                </ul>\n\n\n                                <ul>\n\n                                  <li class=\"col-md-3\">\n                                    <label>Termination Reason *</label>\n                                    <mat-select class=\"form-control\" placeholder=\"Reason\" formControlName=\"Reason\"\n                                      [disabled]=true>\n                                      <mat-option *ngFor=\"let data of allTerminationReasonArray\" [value]='data.name'>\n                                        {{data.name}}</mat-option>\n                                    </mat-select>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Reason').invalid && terminationForm.get('Reason').touched\"\n                                      class=\"error\">You must select a termination reason</span>\n                                  </li>\n                                  <li class=\"col-md-3\">\n                                    <label>Eligible for Rehire *</label>\n                                    <mat-select class=\"form-control\" placeholder=\"Status\" formControlName=\"Eligible\"\n                                      [disabled]=true>\n                                      <mat-option value='yes'>Yes</mat-option>\n                                      <mat-option value='no'>No</mat-option>\n                                    </mat-select>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Eligible').invalid && terminationForm.get('Eligible').touched\"\n                                      class=\"error\">You must select a eligibility for rehire</span>\n                                  </li>\n                                  <div class=\"clearfix\"></div>\n                                </ul>\n\n\n                                <ul>\n                                  <li class=\"col-md-3\">\n                                    <label>Termination Date *</label>\n                                    <div class=\"date\">\n                                      <input placeholder=\"mm / dd / yy\" class=\"form-control\" formControlName=\"Date1\"\n                                        readonly>\n\n\n\n\n                                      <div class=\"clearfix\"></div>\n\n                                    </div>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Date1').invalid && terminationForm.get('Date1').touched\"\n                                      class=\"error\">You must select a Terminate date</span>\n                                  </li>\n\n                                  <li class=\"col-md-3\">\n                                    <label>Last Day Worked *</label>\n                                    <div class=\"date\">\n                                      <input placeholder=\"mm / dd / yy\" class=\"form-control\" formControlName=\"Date2\"\n                                        readonly>\n\n                                      <button mat-raised-button (click)=\"picker1.open()\">\n                                        <span>\n                                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                        </span>\n                                      </button>\n\n                                      <div class=\"clearfix\"></div>\n\n                                    </div>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Date2').invalid && terminationForm.get('Date2').touched\"\n                                      class=\"error\">You must select a last working date</span>\n                                  </li>\n                                  <div class=\"clearfix\"></div>\n                                </ul>\n\n                                <ul>\n\n                                  <li class=\"details-width col-md-4\">\n                                    <label>Offboarding Template Assigned *</label>\n                                    <mat-select class=\"form-control\" placeholder=\"Template\"\n                                      (ngModelChange)=\"templateChange()\" formControlName=\"Template\" [disabled]=true>\n                                      <mat-option *ngFor=\"let data of tempTemplateTotal\" [value]='data._id'>\n                                        {{data.templateName}}</mat-option>\n                                    </mat-select>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Template').invalid && terminationForm.get('Template').touched\"\n                                      class=\"error\">You must select a Template</span>\n\n                                  </li>\n\n                                  <li class=\"procees col-md-8\">\n                                    <label>Terminate any assigned Performance Review in procees?</label>\n                                    <mat-select class=\"form-control\" placeholder=\"Procees\" formControlName=\"Procees\"\n                                      [disabled]=true>\n                                      <mat-option value=yes>Yes</mat-option>\n                                      <mat-option value=no>No</mat-option>\n                                    </mat-select>\n                                    <span style=\"display: block;\"\n                                      *ngIf=\"terminationForm.get('Procees').invalid && terminationForm.get('Procees').touched\"\n                                      class=\"error\">You must select a review process</span>\n                                  </li>\n\n                                  <li class=\"details-width col-md-8\">\n                                    <label>Comments *</label>\n                                    <textarea class=\"form-control\" rows=\"5\" formControlName=\"Comments\"\n                                      readonly></textarea>\n                                    <span\n                                      *ngIf=\"terminationForm.get('Comments').invalid && terminationForm.get('Comments').touched\"\n                                      class=\"error\">please enter a comment</span>\n                                  </li>\n\n                                  <li>\n                                    <div class=\"file-upload\">\n                                      <label for=\"upload\" class=\"file-upload__label\">Add Files</label>\n                                      <span><i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i></span>\n                                      <input id=\"upload\" class=\"file-upload__input\" (change)=\"fileChange($event)\"\n                                        type=\"file\" name=\"file-upload\" multiple [disabled]=true>\n                                    </div>\n                                  </li>\n                                  <li *ngFor=\"let data of filesArray\">{{data}}</li>\n\n\n                                </ul>\n\n                              </div>\n                            </form>\n                          </div>\n\n\n                        </div>\n                      </div>\n                      <div class=\"tab-pane personal personal1\">\n                        <h2>Offboarding Tasks</h2>\n                        <div class=\"details-main\">\n\n                          <div class=\"tasks\">\n                            <h3>Onboarding Template Name </h3>\n                            <!-- <input type=\"text\" class=\"form-control\" value=\"\" placeholder=\"\"> -->\n                            <mat-select [disabled]=true class=\"form-control\" placeholder=\"Template\"\n                              [(ngModel)]=\"OffboardingTemplateName\" [disabled]=true>\n                              <mat-option *ngFor=\"let data of tempTemplateTotal\" [value]='data._id'>\n                                {{data.templateName}}</mat-option>\n                            </mat-select>\n                          </div>\n\n                          <div class=\"off-boarding\">\n\n                            <div class=\"tasks\">\n\n                              <div class=\"tasks-in\">\n                                <h3>Offboarding Tasks</h3>\n\n                              </div>\n\n                              <ul>\n\n                                <li *ngFor=\"let task of getKeys((standardOffboardTemplate['offBoardingTasks']))\">\n                                  <h4>{{task}}</h4>\n                                  <!-- <h4>Hr tasks</h4> -->\n                                  <div *ngFor=\"let data of standardOffboardTemplate['offBoardingTasks'][task]\"\n                                    class=\"task-names\">\n\n                                    <span>{{data.taskName}}</span>\n                                    <small>{{data.assigneeEmail}} -- {{data.Date | date}}</small>\n                                  </div>\n\n                                </li>\n                              </ul>\n\n                            </div>\n\n                          </div>\n\n                        </div>\n\n\n                      </div>\n\n                      <div class=\"tab-pane personal personal1\">\n                        <h2>Direct Reports</h2>\n                        <div class=\"details-main\">\n\n                          <div class=\"reports\">\n                            <h3>Does the employee have direct tasks assigned?</h3>\n                            <mat-select class=\"form-control\" placeholder=\"Reports\">\n                              <mat-option value=Yes>Yes</mat-option>\n                              <mat-option value=No>No</mat-option>\n                            </mat-select>\n\n                            <div class=\"reports-table\">\n                              <table class=\"table\">\n                                <thead>\n                                  <tr>\n                                    <th>Assigned Tasks</th>\n                                    <th>TaskType</th>\n                                    <th>TaskCategory</th>\n\n                                    <th>New Employee</th>\n                                  </tr>\n                                </thead>\n\n                                <tbody>\n                                  <tr *ngFor=\"let data of directTasksArray\">\n                                    <td> {{data.taskName}}</td>\n\n                                    <td>{{data.taskType}}</td>\n                                    <td> {{data.taskCategory}}</td>\n                                    <!-- <td></td> -->\n                                    <td>\n                                      <mat-select class=\"form-control\"\n                                        (selectionChange)=\"employeeChange(data.templateId,$event)\"\n                                        placeholder=\"Employees\">\n\n                                        <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>\n                                          {{data.personal.name.preferredName}}</mat-option>\n                                      </mat-select>\n\n                                    </td>\n                                  </tr>\n\n\n\n                              </table>\n\n                            </div>\n                          </div>\n                          <div class=\"termination-border\"></div>\n                          <button class=\"btn pull-left back\">Back</button>\n                          <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                          <button class=\"btn pull-right save\">Proceed</button>\n                          <div class=\"clearfix\"></div>\n\n                        </div>\n                      </div>\n\n\n                      <div class=\"tab-pane personal personal1\">\n                        <h2>Direct Tasks</h2>\n                        <div class=\"details-main\">\n\n                          <div class=\"reports\">\n                            <h3>Does the employee have direct reports assigned?</h3>\n                            <mat-select class=\"form-control\" placeholder=\"Reports\" [disabled]=true>\n                              <mat-option value=Yes>Yes</mat-option>\n                              <mat-option value=No>No</mat-option>\n                            </mat-select>\n\n                            <div class=\"reports-table\">\n                              <table class=\"table\">\n                                <thead>\n                                  <tr>\n                                    <th>Employee Names</th>\n                                    <th>Job</th>\n                                    <th>Department</th>\n                                    <th>Location</th>\n                                    <th>New Manager</th>\n                                  </tr>\n                                </thead>\n                                <tbody>\n                                  <tr *ngFor=\"let data of directReportsArray\">\n                                    <td>{{data.personal.name.preferredName}}</td>\n                                    <td>{{data.job.jobTitle}}</td>\n                                    <td>{{data.job.department}}</td>\n                                    <td>{{data.personal.address.state}}</td>\n                                    <td>\n                                      <mat-select class=\"form-control\"\n                                        (selectionChange)=\"managerChange(data._id,$event)\" placeholder=\"Managers\">\n                                        <mat-option *ngFor=\"let data of manageremployees\" [value]='data._id'>\n                                          {{data.personal.name.preferredName}}</mat-option>\n                                      </mat-select>\n                                    </td>\n                                  </tr>\n\n\n\n                              </table>\n\n                            </div>\n                          </div>\n\n                        </div>\n                      </div>\n\n                      <div class=\"tab-pane personal personal1\">\n                        <h2>Company Property</h2>\n                        <div class=\"details-main\">\n\n                          <div class=\"reports\">\n                            <h3>Does the employee have direct company Property?</h3>\n                            <mat-select class=\"form-control\" placeholder=\"Property\" [disabled]=true>\n                              <mat-option value=Yes>Yes</mat-option>\n                              <mat-option value=No>No</mat-option>\n                            </mat-select>\n\n                            <div class=\"company-property\">\n\n                              <table class=\"table\">\n                                <thead>\n                                  <tr>\n                                    <!-- <th>\n                                      <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                                    </th> -->\n                                    <th>Asset category</th>\n                                    <th>Asset description</th>\n                                    <th>Serial#</th>\n                                    <th>Date loaned</th>\n                                    <th>Date returned</th>\n                                  </tr>\n                                </thead>\n                                <tbody>\n                                  <tr *ngFor=\"let data of assetsData\">\n                                    <!-- <td>\n                                      <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                                    </td> -->\n                                    <td>{{data.category}}</td>\n                                    <td>{{data.description}}</td>\n                                    <td>{{data.serial_number}}</td>\n                                    <td>{{data.date_loaned | date}}</td>\n                                    <td>\n\n                                      <span *ngIf=\"data.date_returned\">{{data.date_returned | date}}</span>\n                                      <span *ngIf=\"!data.date_returned\"> -- </span>\n\n                                    </td>\n                                  </tr>\n\n                                </tbody>\n                              </table>\n\n                            </div>\n\n                          </div>\n\n\n                        </div>\n                      </div>\n\n\n                    </div>\n\n                    <div class=\"termination-border\"></div>\n                    <span class=\"details-main recap-last\">\n                      <button class=\"btn pull-left back\" (click)=\"goToCompany()\">Back</button>\n                      <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                      <button class=\"btn pull-right save\" (click)=\"recapSubmit()\">Submit</button>\n                    </span>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n\n                </div>\n\n              </div>\n              <div class=\"clearfix\"></div>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.ts":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: OffBoardingLandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffBoardingLandingComponent", function() { return OffBoardingLandingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_off_boarding_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/off-boarding.service */ "./src/app/services/off-boarding.service.ts");
/* harmony import */ var _services_off_boarding_template_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/off-boarding-template.service */ "./src/app/services/off-boarding-template.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_workflow_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../services/workflow.service */ "./src/app/services/workflow.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var OffBoardingLandingComponent = /** @class */ (function () {
    function OffBoardingLandingComponent(offBoarding, router, offBoardingTemplateService, swalAlertService, onboardingService, siteAccessRolesService, myInfoService, workflowService) {
        this.offBoarding = offBoarding;
        this.router = router;
        this.offBoardingTemplateService = offBoardingTemplateService;
        this.swalAlertService = swalAlertService;
        this.onboardingService = onboardingService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.myInfoService = myInfoService;
        this.workflowService = workflowService;
        this.items = [];
        this.value = {};
        this._disabledV = '0';
        this.disable = false;
        this.allEmployeeStatusArray = [];
        this.allTerminationTypeArray = [];
        this.allTerminationReasonArray = [];
        this.filesArray = [];
        this.tempStandardTemplate = {};
        this.tempTemplateTotal = [];
        this.directReportsArray = [];
        this.manageremployees = [];
        this.directReportsSend = [];
        this.directTaskSend = [];
        this.file = [];
        this.files = [];
        this.directTasksArray = [];
        this.ClientInfo = false;
        this.tab2 = false;
        this.tab3 = false;
        this.tab4 = false;
        this.tab7 = false;
        this.tasks = false;
        this.standardOffboardTemplate = {
            templateName: '',
            offBoardingTasks: {
                HrTasks: [],
                ITSetup: [],
                ManagerTasks: []
            }
        };
        this.customTemplateSend = {
            companyId: '',
            templateType: '',
            templateName: '',
            offBoardingTasks: {},
            createdBy: '',
            updatedBy: ''
        };
        this.employees = [];
        this.tempTotalTemplates = [];
        this.assetsData = [];
        this.empWorkFlow = false;
    }
    // private get disabledV(): string {
    //   return this._disabledV;
    // }
    // private set disabledV(value: string) {
    //   this._disabledV = value;
    //   this.disable = this._disabledV === '1';
    // }
    OffBoardingLandingComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
    };
    OffBoardingLandingComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    OffBoardingLandingComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    OffBoardingLandingComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    OffBoardingLandingComponent.prototype.ngOnInit = function () {
        console.log("firsttt");
        this.perPage = 10;
        this.pageNo = 1;
        this.perPageAssets = 10;
        this.pageNoAssets = 1;
        this.employeeSearch();
        this.selectedNav = "ClientInfo";
        this.terminationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            Employee: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Reason: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Eligible: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Date1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Date2: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Procees: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            Comments: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Template: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
        this.getStandardTemplate();
        this.getCustomTemplate();
        var cId = JSON.parse(localStorage.getItem('companyId'));
        this.getStandardAndCustomFieldsforStructure(cId);
        this.getMangersUsersOnly(cId);
    };
    OffBoardingLandingComponent.prototype.getMangersUsersOnly = function (cId) {
        var _this = this;
        var managerId = '5cbe98a3561562212689f747';
        this.offBoardingTemplateService.getManagersEmployeesOnly(cId, managerId)
            .subscribe(function (res) {
            console.log("manager employees", res);
            _this.manageremployees = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: getting employees list
    author : vipin reddy */
    OffBoardingLandingComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    OffBoardingLandingComponent.prototype.getStandardAndCustomFieldsforStructure = function (cID) {
        var _this = this;
        this.onboardingService.getStructureFields(cID)
            .subscribe(function (res) {
            console.log("structure fields", res);
            _this.allStructureFieldsData = res.data;
            //employement-status
            if (_this.allStructureFieldsData['Employment Status'].custom.length > 0) {
                _this.allStructureFieldsData['Employment Status'].custom.forEach(function (element) {
                    _this.allEmployeeStatusArray.push(element);
                    console.log("22222111111", _this.allEmployeeStatusArray);
                });
            }
            if (_this.allStructureFieldsData['Employment Status'].default.length > 0) {
                _this.allStructureFieldsData['Employment Status'].default.forEach(function (element) {
                    _this.allEmployeeStatusArray.push(element);
                });
                console.log("22222", _this.allEmployeeStatusArray);
            }
            if (_this.allStructureFieldsData['Termination Type'].custom.length > 0) {
                _this.allStructureFieldsData['Termination Type'].custom.forEach(function (element) {
                    _this.allTerminationTypeArray.push(element);
                    console.log("22222111111", _this.allTerminationTypeArray);
                });
            }
            if (_this.allStructureFieldsData['Termination Type'].default.length > 0) {
                _this.allStructureFieldsData['Termination Type'].default.forEach(function (element) {
                    _this.allTerminationTypeArray.push(element);
                });
                console.log("22222", _this.allTerminationTypeArray);
            }
            // Termination Reason
            if (_this.allStructureFieldsData['Termination Reason'].custom.length > 0) {
                _this.allStructureFieldsData['Termination Reason'].custom.forEach(function (element) {
                    _this.allTerminationReasonArray.push(element);
                    console.log("22222111111", _this.allTerminationReasonArray);
                });
            }
            if (_this.allStructureFieldsData['Termination Reason'].default.length > 0) {
                _this.allStructureFieldsData['Termination Reason'].default.forEach(function (element) {
                    _this.allTerminationReasonArray.push(element);
                });
                console.log("22222", _this.allTerminationReasonArray);
            }
        }, function (err) {
        });
    };
    OffBoardingLandingComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    OffBoardingLandingComponent.prototype.getStandardTemplate = function () {
        var _this = this;
        this.offBoardingTemplateService.getStandardOffboardingTemplate()
            .subscribe(function (res) {
            console.log("offboarding standard template", res);
            _this.standardOffboardTemplate = res.data;
            _this.tempStandardTemplate = _this.standardOffboardTemplate;
            // this.standardOffboardTemplate.offBoardingTasks.HrTasks = this.standardOffboardTemplate.offBoardingTasks.HrTasks.map(function (el) {
            //   var o = Object.assign({}, el);
            //   o.isChecked = false;
            //   return o;
            // })
            console.log(_this.standardOffboardTemplate.offBoardingTasks.HrTasks);
        }, function (err) {
        });
    };
    OffBoardingLandingComponent.prototype.getCustomTemplate = function () {
        var _this = this;
        var cID = JSON.parse(localStorage.getItem('companyId'));
        this.offBoardingTemplateService.getcustomOffboardingTemplate(cID)
            .subscribe(function (res) {
            console.log("offboarding custom template", res);
            _this.customOffboardTemplate = res.data;
            _this.tempCustomTemplate = _this.customOffboardTemplate;
            console.log(_this.tempCustomTemplate, _this.tempStandardTemplate);
            _this.tempTemplateTotal.push(_this.standardOffboardTemplate);
            if (_this.tempCustomTemplate) {
                _this.tempCustomTemplate.forEach(function (element) {
                    _this.tempTemplateTotal.push(element);
                });
            }
            console.log(_this.tempTemplateTotal);
            _this.customOffboardTemplate = _this.customOffboardTemplate.map(function (el) {
                var o = Object.assign({}, el);
                o.isChecked = false;
                return o;
            });
            console.log(_this.customOffboardTemplate, "1111");
        }, function (err) {
        });
    };
    OffBoardingLandingComponent.prototype.getCustomOffboardingtemplate = function () {
        var _this = this;
        var id = this.OffboardingTemplateName;
        this.offBoardingTemplateService.getcustomOffboardingTemplatedata(id)
            .subscribe(function (res) {
            console.log("get custom template data", res);
            _this.standardOffboardTemplate = res.data[0];
            console.log(_this.standardOffboardTemplate);
        }, function (err) {
            console.log(err);
        });
    };
    OffBoardingLandingComponent.prototype.templateChange = function () {
        console.log(this.terminationForm.get('Template').value);
        this.OffboardingTemplateName = this.terminationForm.get('Template').value;
        console.log(this.OffboardingTemplateName);
        this.getCustomOffboardingtemplate();
    };
    OffBoardingLandingComponent.prototype.showOptions = function ($event, id) {
        var _this = this;
        console.log("1222222", $event.checked, id);
        if ($event.checked == false) {
            this.customTempId = '';
        }
        if ($event.checked == true) {
            this.customOffboardTemplate.forEach(function (element) {
                if (element._id == id) {
                    element.isChecked = true;
                    _this.customTempId = id;
                }
                else {
                    element.isChecked = false;
                }
            });
        }
        console.log(this.customTempId);
    };
    OffBoardingLandingComponent.prototype.customTemplateRedirect = function (id) {
        this.router.navigate(['/admin/admin-dashboard/employee-management/off-boarding-landing/custom-offboard/' + id]);
    };
    OffBoardingLandingComponent.prototype.terminationDateChange = function () {
        console.log(this.terminationForm.get('Date1').value);
        this.terminationDate = this.terminationForm.get('Date1').value;
    };
    OffBoardingLandingComponent.prototype.lastWorkingDateChange = function () {
        console.log(this.terminationForm.get('Date2').value);
        this.lastWorkingDate = this.terminationForm.get('Date2').value;
    };
    OffBoardingLandingComponent.prototype.terminationDetails = function (event) {
        console.log("sdfsdfsdfsdfsdf", event);
        this.selectedNav = event;
        if (event == "ClientInfo") {
            this.ClientInfo = true;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab7 = false;
            this.tasks = false;
        }
        else if (event == "tab2") {
            this.ClientInfo = false;
            this.tab2 = true;
            this.tab3 = false;
            this.tab4 = false;
            this.tab7 = false;
            this.tasks = false;
        }
        else if (event == "tab3") {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = true;
            this.tab4 = false;
            this.tab7 = false;
            this.tasks = false;
        }
        else if (event == "tab4") {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = true;
            this.tab7 = false;
            this.tasks = false;
        }
        else if (event == "tab7") {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tasks = false;
            this.tab7 = true;
        }
        else if (event == 'tasks') {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab7 = false;
            this.tasks = true;
        }
    };
    OffBoardingLandingComponent.prototype.goToTermination = function () {
        this.client_Info.nativeElement.click();
    };
    OffBoardingLandingComponent.prototype.goToOffboard = function () {
        this.offboardTask.nativeElement.click();
    };
    OffBoardingLandingComponent.prototype.goToDirect = function () {
        this.directReports.nativeElement.click();
    };
    OffBoardingLandingComponent.prototype.goToCompany = function () {
        this.companyProperty.nativeElement.click();
    };
    OffBoardingLandingComponent.prototype.terminationProceed = function () {
        console.log("TerminationDetails", this.terminationForm.value);
        if (this.terminationForm.valid && !this.empWorkFlow) {
            var d1 = this.terminationForm.get('Date1').value;
            var d2 = this.terminationForm.get('Date2').value;
            this.terminationForm.patchValue({ Date1: d1 });
            this.terminationForm.patchValue({ Date2: d2 });
            this.selectedNav = "tab2";
        }
        // Status: new FormControl("", Validators.required),
        // Type: new FormControl("", Validators.required),
        // Reason: new FormControl("", Validators.required),
        // Eligible: new FormControl("", Validators.required),
        // Date1: new FormControl("", Validators.required),
        // Date2: new FormControl("", Validators.required),
        // Procees: new FormControl("", Validators.required),
        // Comments: new FormControl("", Validators.required),
        // Comments: new FormControl("", Validators.required)
        else {
            this.terminationForm.get('Employee').markAsTouched();
            this.terminationForm.get('Status').markAsTouched();
            this.terminationForm.get('Type').markAsTouched();
            this.terminationForm.get('Reason').markAsTouched();
            this.terminationForm.get('Eligible').markAsTouched();
            this.terminationForm.get('Date1').markAsTouched();
            this.terminationForm.get('Date2').markAsTouched();
            this.terminationForm.get('Procees').markAsTouched();
            this.terminationForm.get('Comments').markAsTouched();
            this.terminationForm.get('Template').markAsTouched();
        }
    };
    OffBoardingLandingComponent.prototype.employeeForDirectReports = function () {
        var _this = this;
        console.log("dasdas");
        var uID = this.terminationForm.get('Employee').value;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        console.log(this.terminationForm.get('Employee').value);
        // this.offBoardingTemplateService.
        this.myInfoService.getOtherUser(cId, uID)
            .subscribe(function (res) {
            console.log("get termiinsate emp data", res);
            _this.terminateEmpData = res.data;
            _this.terminationForm.get('Status').patchValue(_this.terminateEmpData.job.current_employment_status.status);
        }, function (err) {
        });
        this.offBoardingTemplateService.getDirectReports(cId, uID, this.pageNo, this.perPage)
            .subscribe(function (res) {
            console.log("get directr reports", res);
            _this.directReportsArray = res.data;
        }, function (err) {
        });
        var postAssetsData = {
            "companyId": cId,
            "userId": uID,
            "per_page": this.perPageAssets,
            "page_no": this.pageNoAssets
        };
        this.myInfoService.getAllAssets(postAssetsData)
            .subscribe(function (res) {
            console.log("assets Data", res);
            _this.assetsData = res.data;
        }, function (err) {
            console.log(err);
        });
        this.offBoardingTemplateService.getDirectTasks(uID)
            .subscribe(function (res) {
            console.log("get directr tasks", res);
            _this.directTasksArray = res.data;
            if (_this.directTasksArray.length <= 1) {
                var result = _this.directTasksArray.map(function (el) {
                    var o = Object.assign({}, el);
                    o.validate = false;
                    return o;
                });
                _this.directTasksArray = result;
            }
        }, function (err) {
        });
        this.workflowService.checkEmpInWorkflow(uID)
            .subscribe(function (res) {
            console.log("Emp workflow check", res);
            _this.empWorkFlow = res.data.isAssignedInWorkflow;
        }, function (err) {
            console.log(err);
        });
    };
    OffBoardingLandingComponent.prototype.managerChange = function (empId, event) {
        console.log(empId, event.value);
        if (this.directReportsSend.length == 0) {
            this.directReportsSend.push({ '_id': empId, 'ReportsTo': event.value });
        }
        else {
            for (var i = 0; i < this.directReportsSend.length; i++) {
                if (empId == this.directReportsSend[i]._id) {
                    var index = i;
                    if (index > -1) {
                        this.directReportsSend.splice(index, 1);
                    }
                }
            }
            this.directReportsSend.push({ '_id': empId, 'ReportsTo': event.value });
        }
        console.log(this.directReportsSend, "finalSend Directreports");
    };
    OffBoardingLandingComponent.prototype.employeeChange = function (data, event, j) {
        console.log(data, event);
        this.directTasksArray[j].validate = false;
        if (this.directTaskSend.length == 0) {
            this.directTaskSend.push({
                'templateId': data.templateId,
                'taskType': data.taskType,
                'taskCategory': data.taskCategory,
                'taskId': data.taskId,
                'taskName': data.taskName,
                'taskAssignee': event.value._id,
                'assigneeEmail': event.value.email
            });
        }
        else {
            for (var i = 0; i < this.directTaskSend.length; i++) {
                if (data.taskId == this.directTaskSend[i].taskId) {
                    var index = i;
                    if (index > -1) {
                        this.directTaskSend.splice(index, 1);
                    }
                }
            }
            this.directTaskSend.push({
                'templateId': data.templateId,
                'taskType': data.taskType,
                'taskCategory': data.taskCategory,
                'taskId': data.taskId,
                'taskName': data.taskName,
                'taskAssignee': event.value._id,
                'assigneeEmail': event.value.email
            });
        }
        console.log(this.directTaskSend, "finalSend DirectTaskss");
    };
    OffBoardingLandingComponent.prototype.taskProceed = function () {
        this.selectedNav = "tab3";
    };
    OffBoardingLandingComponent.prototype.reportsProceed = function () {
        this.selectedNav = "tasks";
    };
    OffBoardingLandingComponent.prototype.directTasksEmp = function () {
        console.log("data", this.directTasksArray, this.directTaskSend, this.directTaskSend.length, this.directTasksArray.length);
        if (this.directTasksArray.length > 0 && (this.directTaskSend.length != this.directTasksArray.length)) {
            for (var i = 0; i < this.directTasksArray.length; i++) {
                if (!this.directTasksArray[i].taskAssignee) {
                    if (this.directTaskSend.length != this.directTasksArray.length)
                        this.directTasksArray[i].validate = true;
                }
            }
        }
        else {
            this.selectedNav = "tab4";
        }
    };
    OffBoardingLandingComponent.prototype.companyProceed = function () {
        this.selectedNav = "tab7";
    };
    OffBoardingLandingComponent.prototype.recapSubmit = function () {
        var _this = this;
        var uID = this.terminationForm.get('Employee').value;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        // var file = ,
        var data = {
            direct_tasks: this.directTaskSend,
            "direct_reports": this.directReportsSend,
            "companyId": cId,
            "userId": uID,
            "off_boarding_template_assigned": this.terminationForm.get('Template').value,
            "employmentStatus": this.terminationForm.get('Status').value,
            "terminationDate": this.terminationForm.get('Date1').value,
            "terminationType": this.terminationForm.get('Type').value,
            "terminationReason": this.terminationForm.get('Reason').value,
            "eligibleForRehire": this.terminationForm.get('Eligible').value,
            "last_day_of_work": this.terminationForm.get('Date2').value,
            "termination_comments": this.terminationForm.get('Comments').value
        };
        var formdata = new FormData();
        for (var i = 0; i < this.files.length; i++) {
            formdata.append('file', this.files[i]);
        }
        formdata.append('data', JSON.stringify(data));
        console.log(formdata, 'latest final');
        if (!this.empWorkFlow) {
            this.offBoardingTemplateService.terminateEmployee(formdata)
                .subscribe(function (res) {
                console.log("terminate employee", res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Terminate Employee", res.message, "success");
                    jQuery("#myModal").modal("hide");
                    _this.terminationForm.reset(_this.terminationForm.value);
                    _this.selectedNav = "ClientInfo";
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    OffBoardingLandingComponent.prototype.offboardDelete = function () {
    };
    OffBoardingLandingComponent.prototype.templateRedirect = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/off-boarding-landing/standard-offboard"]);
    };
    OffBoardingLandingComponent.prototype.customTemplateAdd = function () {
        console.log("sadsadasdasdasd");
        this.router.navigate(["/admin/admin-dashboard/employee-management/off-boarding-landing/custom-offboard"]);
    };
    OffBoardingLandingComponent.prototype.customTemplateDelete = function () {
        var _this = this;
        this.offBoardingTemplateService.deleteOffboardingTemplate(this.customTempId)
            .subscribe(function (res) {
            console.log("delete custom template data", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Custom Template', res.message, "success");
                _this.getCustomTemplate();
            }
        }, function (err) {
        });
    };
    OffBoardingLandingComponent.prototype.fileChange = function (event) {
        console.log(event);
        var x = event.target.files;
        this.file = [];
        for (var i = 0; i < x.length; i++) {
            this.file.push(x[i]);
        }
        console.log(this.file);
        if (this.filesArray.length < 5) {
            console.log(this.file);
            for (var i = 0; i < this.file.length; i++) {
                this.filesArray.push(this.file[i].name);
                this.files.push(this.file[i]);
            }
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Termination", "You can select up to 5 files.", "error");
        }
        // let fileList: FileList = event.target.files;
        // console.log(fileList);
        // if (fileList.length > 0) {
        //   this.file = fileList[0];
        //   console.log(this.file);
        //   this.fileName = this.file.name;
        //   this.filesArray.push(this.fileName)
        // }
    };
    OffBoardingLandingComponent.prototype.removeFile = function (i, data) {
        console.log(i, data);
        this.filesArray.splice(i, 1);
        this.files.splice(i, 1);
        console.log(this.filesArray, this.files);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('client_Info'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OffBoardingLandingComponent.prototype, "client_Info", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('offboardTask'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OffBoardingLandingComponent.prototype, "offboardTask", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('directReports'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OffBoardingLandingComponent.prototype, "directReports", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('companyProperty'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OffBoardingLandingComponent.prototype, "companyProperty", void 0);
    OffBoardingLandingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-off-boarding-landing',
            template: __webpack_require__(/*! ./off-boarding-landing.component.html */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.html"),
            styles: [__webpack_require__(/*! ./off-boarding-landing.component.css */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing/off-boarding-landing.component.css")]
        }),
        __metadata("design:paramtypes", [_services_off_boarding_service__WEBPACK_IMPORTED_MODULE_2__["OffBoardingService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_off_boarding_template_service__WEBPACK_IMPORTED_MODULE_3__["OffBoardingTemplateService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_6__["OnboardingService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_7__["SiteAccessRolesService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_8__["MyInfoService"],
            _services_workflow_service__WEBPACK_IMPORTED_MODULE_9__["WorkflowService"]])
    ], OffBoardingLandingComponent);
    return OffBoardingLandingComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.css":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.css ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-currentpage{\n    padding: 25px 0 0 !important;\n}\n.zenwork-margin-zero b{\n    font-size: 14px!important;\n    color: #404040;\n}\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding:6px 15px;\n    font-size: 14px;\n    margin:  0 10px 0 0;color: #000;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.mr-7 .fa{ font-size:15px;}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.sub-title{\n    display: inline-block;\n    margin: 5px 0 0;\n    vertical-align:sub;\n}\n.zenwork-inner-icon{\n    width: 30px;\n    height: auto;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important;\n\n}\n.zenwork-margin-ten-zero {\n    margin: 15px 0px !important;\n}\n.template-name{\n    margin:20px 0 0 0;\n}\n.template-name label{\n    line-height: 40px;\n}\n.text-field{\nborder: none; \nwidth: 25%;\npadding: 11px 12px; \nheight: 40px; \nbox-shadow: none; background: #fff;\n}\n.standard { margin: 0 auto; float: none;}\n.off-boarding{ display: block; margin: 20px 0 0;}\n.tasks{ display: block;}\n.tasks-in{ position: relative;background: #edf7fe; padding: 14px 20px; margin: 20px 0 0;}\n.tasks h3{color: #3a3a3a;\n    margin: 0;\n    font-size: 18px;}\n.top-menu{position: absolute; top:4px; right: 0;}\n.tasks ul { display: block; background: #fff; padding:0 20px;}\n.tasks ul li { display: block; padding: 20px 0;}\n.tasks ul li h4{color: #3a3a3a;margin: 0 0 15px;font-size: 16px; font-weight:600;}\n.task-names{ border-bottom:#ccc 1px solid; padding: 12px 0;}\n.task-names span { display: block;color:#777474;font-size:14px;}\n.task-names small { display: block;color: #ccc;font-size:12px; margin: 5px 0 0 26px;}\n.btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.btn.cancel { color:#e44a49; background:transparent; padding:10px 0 0 30px;font-size: 16px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.html":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.html ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"standard col-md-11\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <a routerLink=\"/admin/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing\">\n        <button class=\"btn zenwork-customized-back-btn\">\n          <span class=\"green mr-7\">\n            <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n          </span>Back\n        </button>\n      </a>\n      <span class=\"inner-icon-img\">\n        <img src=\"../../../assets/images/Offboarding/Group 831.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </span>\n      <small class=\"sub-title\"><b>Standard Offboarding Template</b></small>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"off-boarding\">\n\n    <div class=\"heading\">\n      <h4>View / Edit Standard Offboarding Template</h4>\n    </div>\n\n    <div class=\"template-name\">\n      <label>Offboarding Template Name</label><br>\n\n      <input type=\"text\" [(ngModel)]=\"standardOffboardTemplate.templateName\" class=\"form-control text-field\"\n        placeholder=\"Template Name\" readonly>\n    </div>\n\n    <div class=\"tasks\">\n\n      <div class=\"tasks-in\">\n        <h3>Offboarding Tasks</h3>\n        <div class=\"top-menu\">\n          <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu=\"matMenu\">\n            <button mat-menu-item>\n              <a data-toggle=\"modal\" data-target=\"#myModal\">Edit</a>\n            </button>\n          </mat-menu>\n        </div>\n      </div>\n\n      <ul>\n\n        <li *ngFor=\"let task of getKeys((standardOffboardTemplate['offBoardingTasks']))\">\n          <h4>{{task}}</h4>\n          <!-- <h4>Hr tasks</h4> -->\n          <div *ngFor=\"let data of standardOffboardTemplate['offBoardingTasks'][task]\" class=\"task-names\">\n\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\"\n              (ngModelChange)=\"singleTaskChoose(task,data.taskName,data.isChecked)\">\n              <span>{{data.taskName}}</span></mat-checkbox>\n            <small>{{data.assigneeEmail}} -- {{data.Date | date}}</small>\n          </div>\n\n        </li>\n      </ul>\n\n\n    </div>\n    <div class=\"termination-border\">\n      <button class=\"btn pull-left cancel\">Cancel</button>\n      <button class=\"btn pull-right save\" (click)=\"updateStandardTemplate()\">Submit</button>\n      <div class=\"clearfix\"></div>\n    </div>\n\n\n  </div>\n\n\n</div>\n\n\n<!-- Author:Suresh M, Date:09-05-19  -->\n<!-- Standard Termination Wizard -->\n<div class=\"offboard-add\">\n\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Offboarding Add / Edit Task</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"offboard-edit col-md-10\">\n\n            <ul>\n\n              <li class=\"col-md-4 offboarding-width\">\n                <label>Task Name</label>\n                <input type=\"text\" [(ngModel)]=\"popupData.taskName\" class=\"form-control\" placeholder=\"Task Name\">\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Task Assignee</label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"popupData.taskAssignee\" placeholder=\"Assignee\"\n                  (ngModelChange)=\"empEmail($event)\">\n                  <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>{{data.personal.name.preferredName}}\n                  </mat-option>\n\n                </mat-select>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Assignee Email Address</label>\n                <input type=\"text\" [(ngModel)]=\"popupData.assigneeEmail\" class=\"form-control\"\n                  placeholder=\"Email Address\" readonly>\n              </li>\n\n              <li class=\"offboarding-width col-md-8\">\n                <label>Task Description</label>\n                <textarea [(ngModel)]=\"popupData.taskDesription\" class=\"form-control\" rows=\"5\"\n                  placeholder=\"Description\"></textarea>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label>Expected Task Completion Time</label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"popupData.timeToComplete.time\" placeholder=\"time period\">\n                  <mat-option *ngFor=\"let data of taskCOmpletionTime\" [value]=\"data\">{{data}}</mat-option>\n\n                </mat-select>\n              </li>\n\n              <li class=\"col-md-4\">\n                <label></label>\n                <mat-select class=\"form-control\" [(ngModel)]=\"popupData.timeToComplete.unit\" placeholder=\"time period\">\n                  <mat-option value='Day'>Day</mat-option>\n                  <mat-option value='Week'>Week</mat-option>\n                  <mat-option value='Month'>Month</mat-option>\n                </mat-select>\n              </li>\n\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <ul>\n              <li class=\"col-md-4\">\n                <label>Task Category</label>\n                <input type=\"text\" [(ngModel)]=\"popupData.category\" class=\"form-control category\"\n                  placeholder=\"Category\" readonly>\n              </li>\n            </ul>\n            <div class=\"clearfix\"></div>\n\n            <div class=\"termination-border\">\n              <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n              <button class=\"btn pull-right save\" (click)=\"editStandardOffboarding()\">Submit</button>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n\n\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.ts":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: StandardOffboardingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StandardOffboardingComponent", function() { return StandardOffboardingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_off_boarding_template_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/off-boarding-template.service */ "./src/app/services/off-boarding-template.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StandardOffboardingComponent = /** @class */ (function () {
    function StandardOffboardingComponent(offBoardingTemplateService, siteAccessRolesService, swalAlertService) {
        this.offBoardingTemplateService = offBoardingTemplateService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.swalAlertService = swalAlertService;
        this.standardOffboardTemplate = {
            templateName: '',
            offBoardingTasks: {
                HrTasks: [],
                ITSetup: [],
                ManagerTasks: []
            }
        };
        this.popupData = {
            taskName: '',
            taskAssignee: '',
            assigneeEmail: '',
            taskDesription: '',
            timeToComplete: {
                time: '',
                unit: ''
            },
            category: '',
        };
        this.addShowHide = false;
        this.taskCOmpletionTime = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    }
    StandardOffboardingComponent.prototype.ngOnInit = function () {
        this.getStandardTemplate();
        this.employeeSearch();
    };
    /* Description: getting employees list
      author : vipin reddy */
    StandardOffboardingComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    StandardOffboardingComponent.prototype.getStandardTemplate = function () {
        var _this = this;
        this.offBoardingTemplateService.getStandardOffboardingTemplate()
            .subscribe(function (res) {
            console.log("offboarding standard template", res);
            _this.standardOffboardTemplate = res.data;
            _this.standardOffboardTemplate.offBoardingTasks.HrTasks = _this.standardOffboardTemplate.offBoardingTasks.HrTasks.map(function (el) {
                var o = Object.assign({}, el);
                o.isChecked = false;
                return o;
            });
            console.log(_this.standardOffboardTemplate.offBoardingTasks.HrTasks);
        }, function (err) {
        });
    };
    StandardOffboardingComponent.prototype.empEmail = function (event) {
        var _this = this;
        console.log(event);
        this.employeeData.forEach(function (element) {
            if (element._id == event) {
                _this.popupData.assigneeEmail = element.email;
            }
        });
    };
    StandardOffboardingComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    StandardOffboardingComponent.prototype.singleTaskChoose = function (taskArray, taskName, isChecked) {
        var _this = this;
        console.log(taskArray, taskName, isChecked);
        if (isChecked == false) {
            this.popupData = {
                taskName: '',
                taskAssignee: '',
                assigneeEmail: '',
                taskDesription: '',
                timeToComplete: {
                    time: '',
                    unit: ''
                },
                category: '',
            };
            this.addShowHide = false;
        }
        if (isChecked == true) {
            for (var task in this.standardOffboardTemplate["offBoardingTasks"]) {
                console.log(task);
                this.standardOffboardTemplate["offBoardingTasks"][task].forEach(function (element) {
                    if (element.taskName === taskName) {
                        element.isChecked = true;
                        _this.popupData = element;
                        _this.addShowHide = true;
                        console.log("popdaata", _this.popupData);
                    }
                    else {
                        element.isChecked = false;
                    }
                });
            }
        }
    };
    StandardOffboardingComponent.prototype.editStandardOffboarding = function () {
        console.log(this.popupData);
        delete this.popupData['isChecked'];
        jQuery("#myModal").modal("hide");
        if (this.standardOffboardTemplate['offBoardingTasks'][this.popupData.category]) {
            this.standardOffboardTemplate['offBoardingTasks'][this.popupData.category] = [this.popupData];
        }
        //  else {
        //   this.standardOffboardTemplate['offBoardingTasks'][this.popupData.category] = [this.popupData];
        // }
        console.log(this.standardOffboardTemplate, "12121");
    };
    StandardOffboardingComponent.prototype.updateStandardTemplate = function () {
        var _this = this;
        console.log(this.standardOffboardTemplate);
        this.offBoardingTemplateService.updateStandardTemplate(this.standardOffboardTemplate)
            .subscribe(function (res) {
            console.log("offboarding standard template update", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("standard", res.message, "success");
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("standard", err.error.message, "error");
        });
    };
    StandardOffboardingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-standard-offboarding',
            template: __webpack_require__(/*! ./standard-offboarding.component.html */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.html"),
            styles: [__webpack_require__(/*! ./standard-offboarding.component.css */ "./src/app/admin-dashboard/employee-management/off-boarding-landing/standard-offboarding/standard-offboarding.component.css")]
        }),
        __metadata("design:paramtypes", [_services_off_boarding_template_service__WEBPACK_IMPORTED_MODULE_1__["OffBoardingTemplateService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_2__["SiteAccessRolesService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"]])
    ], StandardOffboardingComponent);
    return StandardOffboardingComponent;
}());



/***/ }),

/***/ "./src/app/services/off-boarding-template.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/off-boarding-template.service.ts ***!
  \***********************************************************/
/*! exports provided: OffBoardingTemplateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffBoardingTemplateService", function() { return OffBoardingTemplateService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OffBoardingTemplateService = /** @class */ (function () {
    function OffBoardingTemplateService(http) {
        this.http = http;
    }
    OffBoardingTemplateService.prototype.getStandardOffboardingTemplate = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/base/get")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.updateStandardTemplate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.createcustomTemplate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/create", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.updateCustomTemplate = function (data) {
        console.log(data);
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.getcustomOffboardingTemplate = function (cID) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/company/get/" + cID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.deleteOffboardingTemplate = function (cID) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/delete/" + cID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.getcustomOffboardingTemplatedata = function (tID) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/template/get/" + tID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.getDirectReports = function (cId, uId, pageNo, perPage) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/getDirectReports/" + cId + '/' + uId + '/' + pageNo + '/' + perPage)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.getManagersEmployeesOnly = function (cId, mId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/users/" + cId + '/' + mId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.terminateEmployee = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/terminateEmployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService.prototype.getDirectTasks = function (uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/assignedTasks/" + uId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OffBoardingTemplateService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OffBoardingTemplateService);
    return OffBoardingTemplateService;
}());



/***/ }),

/***/ "./src/app/services/off-boarding.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/off-boarding.service.ts ***!
  \**************************************************/
/*! exports provided: OffBoardingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffBoardingService", function() { return OffBoardingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OffBoardingService = /** @class */ (function () {
    function OffBoardingService(http) {
        this.http = http;
    }
    OffBoardingService.prototype.getAllTerminationEmployee = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "");
    };
    OffBoardingService.prototype.getAllOffboardTemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "" + id);
    };
    OffBoardingService.prototype.getAllOffboardingTasks = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "");
    };
    OffBoardingService.prototype.getAllDirectReports = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "");
    };
    OffBoardingService.prototype.getAllCompanyProperty = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "");
    };
    OffBoardingService.prototype.terminationWizardSubmit = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "", data);
    };
    OffBoardingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OffBoardingService);
    return OffBoardingService;
}());



/***/ }),

/***/ "./src/app/services/onboarding.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/onboarding.service.ts ***!
  \************************************************/
/*! exports provided: OnboardingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingService", function() { return OnboardingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OnboardingService = /** @class */ (function () {
    function OnboardingService(http) {
        this.http = http;
    }
    OnboardingService.prototype.getStandardOnboardingtemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.editDataSubit = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/editTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.addCustomOnboardTemplate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/addEditTemplate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllCustomTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getCustomTemplates")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getStandardTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getStandardTemplate")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.deleteCategory = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllJobDetails = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getFieldsForAddEmployee");
    };
    OnboardingService.prototype.selectOnboardTemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id);
    };
    OnboardingService.prototype.getModulesData = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplates", data);
    };
    OnboardingService.prototype.deleteTemplate = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTemplate", id);
    };
    OnboardingService.prototype.standardNewHireAllData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addEmployee", data);
    };
    OnboardingService.prototype.newHireHrSummaryReviewData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addUpdateCustomWizard", data);
    };
    OnboardingService.prototype.getAllNewHireHRData = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getAllCustomWizards");
    };
    OnboardingService.prototype.customNewHrDelete = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/deleteCustomWizards", id);
    };
    OnboardingService.prototype.getCustomNewHireWizard = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getCustomWizard/" + id);
    };
    OnboardingService.prototype.getSiteAccesRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id);
    };
    OnboardingService.prototype.getStructureFields = function (cID) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + cID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.preEmployeeIdGeneration = function (cId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getEmployeeId/" + cId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllWorkSchedule = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/listPositionSchedules", { params: data });
    };
    OnboardingService.prototype.standardScheduleData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getempschedule", data);
    };
    OnboardingService.prototype.createEmployeeData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/employeespecific", data);
    };
    OnboardingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OnboardingService);
    return OnboardingService;
}());



/***/ })

}]);
//# sourceMappingURL=off-boarding-landing-off-boarding-landing-module.js.map