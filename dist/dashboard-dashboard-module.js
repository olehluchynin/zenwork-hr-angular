(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./src/app/admin-dashboard/dashboard/dashboard.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/dashboard.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dashboard { float: none; margin:25px auto 0; padding:15px 0 0;}\n.dashboard ul { padding: 10px 0 0;}\n.dashboard ul li{ float: left; margin: 0 20px 0 0;}\n.dashboard > ul > li > a{ color: #5f5f5f; padding:10px 15px; font-size: 15px; background: #fff; border-radius: 4px; border: #ccc 1px solid;}\n.dashboard a{ color: #5f5f5f; padding:10px 15px; font-size: 15px; background: #fff; border-radius: 4px;border: #ccc 1px solid;}\n.dashboard ul li.active a { color: #fff; background: #5f5f5f; border: none;}"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/dashboard.component.html":
/*!********************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/dashboard.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"dashboard col-md-11\">\n\n  <ul class=\"pull-left\">\n    <li *ngIf=\"empType == 'Employee' || empType == 'Manager' || empType == 'HR'\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/dashboard/employee-view']\">Employee View</a>\n    </li>\n    <li *ngIf=\"empType == 'Manager'\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/dashboard/manager-view']\">Manager View</a>\n    </li>\n    <li *ngIf=\"empType == 'HR'\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/dashboard/hr-view']\">HR View</a>\n    </li>\n    <li *ngIf=\"type == 'company'\" routerLinkActive=\"active\">\n      <a [routerLink]=\"['/admin/admin-dashboard/dashboard/system-admin-view']\">System Administrator View</a>\n    </li>\n  </ul>\n\n  <!-- <a [routerLink]=\"['/admin/admin-dashboard/dashboard/manager-view']\" class=\"pull-right\">Manager Dashboard</a> -->\n  <div class=\"clearfix\"></div>\n</div>\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/dashboard.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/dashboard.component.ts ***!
  \******************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.empType = JSON.parse(localStorage.getItem('employeeType'));
        this.type = JSON.parse(localStorage.getItem('type'));
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/admin-dashboard/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/admin-dashboard/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/dashboard.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/dashboard.module.ts ***!
  \***************************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/admin-dashboard/dashboard/dashboard.component.ts");
/* harmony import */ var _employee_view_employee_view_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employee-view/employee-view.component */ "./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.ts");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _manager_view_manager_view_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./manager-view/manager-view.component */ "./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.ts");
/* harmony import */ var _hr_view_hr_view_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./hr-view/hr-view.component */ "./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.ts");
/* harmony import */ var _system_admin_view_system_admin_view_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./system-admin-view/system-admin-view.component */ "./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.ts");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var routes = [
    {
        path: '', component: _dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
        children: [
            { path: '', redirectTo: 'employee-view', pathMatch: 'full' },
            { path: 'employee-view', component: _employee_view_employee_view_component__WEBPACK_IMPORTED_MODULE_4__["EmployeeViewComponent"] },
            { path: 'manager-view', component: _manager_view_manager_view_component__WEBPACK_IMPORTED_MODULE_11__["ManagerViewComponent"] },
            { path: 'hr-view', component: _hr_view_hr_view_component__WEBPACK_IMPORTED_MODULE_12__["HrViewComponent"] },
            { path: 'system-admin-view', component: _system_admin_view_system_admin_view_component__WEBPACK_IMPORTED_MODULE_13__["SystemAdminViewComponent"] },
        ]
    },
];
var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
                _employee_view_employee_view_component__WEBPACK_IMPORTED_MODULE_4__["EmployeeViewComponent"],
                _manager_view_manager_view_component__WEBPACK_IMPORTED_MODULE_11__["ManagerViewComponent"],
                _hr_view_hr_view_component__WEBPACK_IMPORTED_MODULE_12__["HrViewComponent"],
                _system_admin_view_system_admin_view_component__WEBPACK_IMPORTED_MODULE_13__["SystemAdminViewComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes), _angular_material_menu__WEBPACK_IMPORTED_MODULE_5__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"], _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_10__["MaterialModuleModule"]
            ],
            providers: [_services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_14__["LeaveManagementService"], _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_15__["AccessLocalStorageService"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".emp-time { padding: 25px 0 0;}\n\n.emp-time-lft { text-align: center}\n\n.emp-time-lft-in {  background: #fff; padding: 15px 0 100px; border-radius: 5px;}\n\n.emp-time-lft h4 { padding:0 0 35px;}\n\n.emp-time-lft ul { display: block;}\n\n.emp-time-lft ul li { margin: 0 0 30px; cursor: pointer;}\n\n.emp-time-lft ul li a { display: block;}\n\n.emp-time-lft ul li a figure img { margin: 0 auto 20px;}\n\n.emp-time-lft ul li a small { color:#737070; padding: 6px 15px; font-size: 14px; \nborder: #ccc 1px solid; border-radius:15px;}\n\n.emp-time-rgt {display: inline-block;}\n\n.emp-time-rgt-in {  background: #fff; padding: 15px 0 25px;border-radius: 5px;box-shadow: 0 0 8px #c5c5c5; margin: 0 0 25px;}\n\n.emp-time-rgt-in h4 { padding:0 0 10px 20px;}\n\n.emp-time-rgt ul { display: block; text-align: center;}\n\n.emp-time-rgt ul li { float: none; display: inline-block;}\n\n.emp-time-rgt ul li a { display: block;}\n\n.emp-time-rgt ul li a h5 { color:#737070; padding:0 0 10px; font-size: 18px;}\n\n.emp-time-rgt ul li a figure img { margin: 0 auto 20px;}\n\n.vacation { position: relative; text-align: left; padding: 0 0 0 40px;}\n\n.vacation span { display: inline-block; font-size: 14px; color: #ccc;}\n\n.vacation span b { display: inline-block; font-size: 22px; color: #bfbcbc; padding: 0 7px 0 0;}\n\n.vacation small { display:block; font-size: 14px; color: #737070;}\n\n.vacation:after { content:''; position: absolute; top:5px; left:20px; border-left: #23a4db 5px solid; height:90%;}\n\n.vacation.sick:after { content:''; position: absolute; top:5px; left:20px; border-left: #fad32b 5px solid; height:90%;}\n\n.vacation.breave:after { content:''; position: absolute; top:5px; left:20px; border-left: #7ac650 5px solid; height:90%;}\n\n.vacation.duty:after { content:''; position: absolute; top:5px; left:20px; border-left: #52d3c8 5px solid; height:90%;}\n\n.training { padding: 0 5px; margin: 0 0 30px;}\n\n.training h3 { vertical-align: middle; margin: 0;}\n\n.training h3 img { vertical-align: middle; display: inline-block;}\n\n.training h3 span { vertical-align: middle;display: inline-block; font-size: 16px; padding:0 0 0 10px;}\n\n.training a {display: inline-block; font-size: 14px;color:  #737070; margin: -10px 0 0;}\n\n.training ul { text-align: left;background: #fff; padding:0px 10px 0px;margin: 10px 0 0;}\n\n.training ul li { display: block; width: auto; border-bottom: #ccc 1px solid; padding:6px 0;}\n\n.training ul li:nth-last-child(1) { border: none;}\n\n.training ul li .training-cont { padding: 0;}\n\n.training ul li .training-cont label{color: #484747; font-weight: normal; font-size: 14px;display: block;}\n\n.training ul li .training-cont .date { width: auto;}\n\n.training ul li .training-cont .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; \nbox-shadow:none; width:auto;height:auto; min-width: auto;}\n\n.training ul li .training-cont .date button span { border: none !important; padding: 0 !important;}\n\n.training ul li .training-cont .date .form-control { width:77%; height: auto; padding: 0 0 0 10px;}\n\n.training ul li .training-cont .date .form-control:focus { box-shadow: none; border: none;}\n\n.training ul li .training-cont .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.training > ul > li > a {\n    color: #f0848a;\n    padding: 3px 15px;\n    border-radius: 20px;\n    text-align: center;\n    border: #f0848a 1px solid;\n    margin: 13px 0 0px;\n    font-size: 12px;\n}\n\n.training > ul > li .training-cont > a { color: #ccc;}\n\n.training.company h3{ padding: 0 5px 10px;}\n\n.training.company ul li .training-cont label { padding: 0 0 8px;}\n\n.training.company ul li { padding: 12px 0;}\n\n.training.policy ul li {padding:15px 0;}\n\n.card-align {\n    margin: 0 15px 30px 15px;\n}\n\n.card-shadow{\n    box-shadow: none;\n}\n\n.bg-color {\n    background: #f8f8f8;\n    margin-bottom: 10px;\n}\n\n.date-align {\n    text-align: center;\n    margin-top: 10px;\n    font-size: 20px;\n    color: green;\n    border-right: 1px solid #ece3e3;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"emp-time\">\n\n  <div class=\"card-align\">\n    <mat-card class=\"card-shadow\">\n      <h4>Company Announcements and News ({{companyAddList.length}})</h4>\n      <mat-card class=\"card-shadow bg-color\">\n        <div class=\"row\">\n          <div class=\"col-xs-1\">\n            <p class=\"date-align\">24 <br> MAR</p>\n          </div>\n          <div *ngFor=\"let data of companyAddList\" class=\"col-xs-11\">\n            <p>{{data.description}}</p>\n          </div>\n        </div>\n      </mat-card>\n      \n\n    </mat-card>\n  </div>\n\n\n\n\n\n\n  <div class=\"emp-time-lft col-md-3\">\n    <div class=\"emp-time-lft-in\">\n      <h4>My Time</h4>\n      <ul>\n        <li>\n          <a (click)=\"punchIn()\">\n            <figure><img src=\"../../../../assets/images/dashboard/punch-in.png\" alt=\"img\"></figure>\n            <small>Punch In</small>\n          </a>\n        </li>\n        <li>\n          <a (click)=\"punchOut()\">\n            <figure><img src=\"../../../../assets/images/dashboard/punch-in.png\" alt=\"img\"></figure>\n            <small>Punch Out</small>\n          </a>\n        </li>\n        <li>\n          <a routerLink='/admin/admin-dashboard/employee-management/profile/profile-view/time-schedule/work-schedule'>\n            <figure><img src=\"../../../../assets/images/dashboard/approve.png\" alt=\"img\"></figure>\n            <small>Approve / View Time Sheet</small>\n          </a>\n        </li>\n      </ul>\n    </div>\n  </div>\n\n\n  <div class=\"emp-time-rgt col-md-9\">\n\n    <div class=\"emp-time-rgt-in\">\n      <h4>My Time Off</h4>\n\n      <div class=\"time-off\">\n\n        <ul>\n          <li class=\"col-md-3\">\n            <a href=\"#\">\n              <h5>Vacation</h5>\n              <figure><img src=\"../../../../assets/images/dashboard/vacation.png\" alt=\"img\"></figure>\n\n              <div class=\"vacation\">\n                <span><b>0.0</b>Hrs Used (YTD)</span>\n                <small>0.0 Hrs Scheduled</small>\n              </div>\n\n            </a>\n          </li>\n\n          <li class=\"col-md-3\">\n            <a href=\"#\">\n              <h5>Sick</h5>\n              <figure><img src=\"../../../../assets/images/dashboard/sick.png\" alt=\"img\"></figure>\n\n              <div class=\"vacation sick\">\n                <span><b>0.0</b>Hrs Used (YTD)</span>\n                <small>0.0 Hrs Scheduled</small>\n              </div>\n\n            </a>\n          </li>\n\n          <li class=\"col-md-3\">\n            <a href=\"#\">\n              <h5>Breavement</h5>\n              <figure><img src=\"../../../../assets/images/dashboard/breavment.png\" alt=\"img\"></figure>\n\n              <div class=\"vacation breave\">\n                <span><b>0.0</b>Hrs Used (YTD)</span>\n                <small>0.0 Hrs Scheduled</small>\n              </div>\n\n            </a>\n          </li>\n\n          <li class=\"col-md-3\">\n            <a href=\"#\">\n              <h5>Jury Duty</h5>\n              <figure><img src=\"../../../../assets/images/dashboard/duty.png\" alt=\"img\"></figure>\n\n              <div class=\"vacation duty\">\n                <span><b>0.0</b>Hrs Used (YTD)</span>\n                <small>0.0 Hrs Scheduled</small>\n              </div>\n\n            </a>\n          </li>\n\n        </ul>\n\n      </div>\n    </div>\n\n    <div *ngIf=\"managersData.show_training\" class=\"training col-md-6\">\n\n      <h3>\n        <img src=\"../../../../assets/images/dashboard/training.png\" alt=\"img\">\n        <span>Training</span>\n      </h3>\n\n      <a href=\"#\" class=\"pull-right\">View My Training</a>\n      <div class=\"clearfix\"></div>\n\n      <ul>\n\n        <li>\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>New Hire On Board</label>\n            <div class=\"date\">\n              <button mat-raised-button (click)=\"picker.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <input matInput [matDatepicker]=\"picker\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n              <mat-datepicker #picker></mat-datepicker>\n\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n\n          <a href=\"#\" class=\"pull-right\">Past Due</a>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n        <li>\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>New Hire On Board</label>\n            <div class=\"date\">\n              <button mat-raised-button (click)=\"picker1.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n              <mat-datepicker #picker1></mat-datepicker>\n\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n\n          <a href=\"#\" class=\"pull-right\">Past Due</a>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n      </ul>\n\n    </div>\n\n    <div *ngIf=\"managersData.show_company_links\" class=\"training col-md-6 company\">\n\n      <h3>\n        <img src=\"../../../../assets/images/dashboard/company.png\" alt=\"img\">\n        <span>Compnay Links</span>\n      </h3>\n\n      <ul>\n\n        <li *ngFor=\"let data of managersData.company_links\">\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>{{data.site_name}}</label>\n            <a target=\"_blank\" [href]=\"data.site_link\">{{data.site_link}}</a>\n          </div>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n        <li>\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>New Hire On Board</label>\n            <a href=\"#\">Zenwork.com</a>\n          </div>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n\n      </ul>\n\n    </div>\n\n    <div class=\"clearfix\"></div>\n\n\n\n    <div *ngIf=\"managersData.show_company_policies\" class=\"training col-md-6 policy\">\n\n      <h3>\n        <img src=\"../../../../assets/images/dashboard/training.png\" alt=\"img\">\n        <span>Company Policies</span>\n      </h3>\n\n      <ul>\n\n        <li>\n          <div class=\"training-cont\">\n            <label>Company Handbook</label>\n          </div>\n        </li>\n\n        <li>\n          <div class=\"training-cont\">\n            <label>Company Handbook</label>\n          </div>\n        </li>\n\n\n      </ul>\n\n    </div>\n\n\n  </div>\n\n  <div class=\"clearfix\"></div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.ts ***!
  \************************************************************************************/
/*! exports provided: EmployeeViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeViewComponent", function() { return EmployeeViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_employee_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/employee-service.service */ "./src/app/services/employee-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EmployeeViewComponent = /** @class */ (function () {
    function EmployeeViewComponent(leaveManagementService, accessLocalStorageService, swalAlertService, employeeServiceService) {
        this.leaveManagementService = leaveManagementService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.employeeServiceService = employeeServiceService;
        this.managersData = {
            company_links: [],
            show_company_policies: false,
            show_company_links: false,
            show_training: false
        };
        this.companyAddList = [];
    }
    EmployeeViewComponent.prototype.ngOnInit = function () {
        this.companyID = JSON.parse(localStorage.getItem('companyId'));
        this.userId = this.accessLocalStorageService.get('employeeId');
        console.log(this.userId);
        this.getManagerData();
        this.getCompanyAnnouncements();
    };
    // Author: Saiprakash G, Date: 01/07/19,
    // Description: Employee punch in
    EmployeeViewComponent.prototype.punchIn = function () {
        var _this = this;
        var data = {
            userId: this.userId
        };
        this.leaveManagementService.punchIn(data).subscribe(function (res) {
            console.log(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    EmployeeViewComponent.prototype.getCompanyAnnouncements = function () {
        var _this = this;
        this.employeeServiceService.getCompanyList(this.companyID)
            .subscribe(function (res) {
            console.log("company annoucments", res);
            _this.companyAddList = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    EmployeeViewComponent.prototype.getManagerData = function () {
        var _this = this;
        this.employeeServiceService.getManagerSettings(this.companyID)
            .subscribe(function (res) {
            console.log("manager data", res);
            _this.managersData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 01/07/19,
    // Description: Employee punch out
    EmployeeViewComponent.prototype.punchOut = function () {
        var data = {
            userId: this.userId
        };
        this.leaveManagementService.punchOut(data).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 01/07/19,
    // Description: Approve time sheet
    EmployeeViewComponent.prototype.approveTimeSheet = function () {
        var data = {
            userId: this.userId
        };
        this.leaveManagementService.timeSheetApproval(data).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    EmployeeViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-view',
            template: __webpack_require__(/*! ./employee-view.component.html */ "./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.html"),
            styles: [__webpack_require__(/*! ./employee-view.component.css */ "./src/app/admin-dashboard/dashboard/employee-view/employee-view.component.css")]
        }),
        __metadata("design:paramtypes", [_services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_1__["LeaveManagementService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _services_employee_service_service__WEBPACK_IMPORTED_MODULE_3__["EmployeeServiceService"]])
    ], EmployeeViewComponent);
    return EmployeeViewComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  hr-view works!\n</p>\n"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.ts ***!
  \************************************************************************/
/*! exports provided: HrViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HrViewComponent", function() { return HrViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HrViewComponent = /** @class */ (function () {
    function HrViewComponent() {
    }
    HrViewComponent.prototype.ngOnInit = function () {
    };
    HrViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hr-view',
            template: __webpack_require__(/*! ./hr-view.component.html */ "./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.html"),
            styles: [__webpack_require__(/*! ./hr-view.component.css */ "./src/app/admin-dashboard/dashboard/hr-view/hr-view.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HrViewComponent);
    return HrViewComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".emp-time { padding: 25px 0 0;}\n.card-shadow { box-shadow: none; }\n.emplployee-icon{\n    font-size: 50px;\n    color: #ccc;  \n}\n.managers { height: 48px;}\n.managers span { vertical-align: middle; display: inline-block;}\n.manager-right { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;}\n.manager-right h4 { font-size: 15px; margin: 0 0 5px;}\n.manager-right h5 {  color: #ccc;margin: 0;}\n.send-box {\n    border: 1px solid #eae0e0;\n    padding: 6px 0px 5px 13px;\n    width: 10%;\n    border-radius: 5px;\n}\n.rotate-icon {\n    -webkit-transform: rotate(-40deg);\n            transform: rotate(-40deg);\n}\n.row-align {\n    margin: 25px 0px;\n    border-bottom: 1px solid #f5eded;\n}\n.row-margin {\n    margin: 10px -15px 10px -15px;\n}\n.row-align ul li p { display: block;}\n.row-align ul li p b { display: inline-block; color: #c1c1c1; font-size: 25px; padding: 0 5px 0 0; line-height: 25px;}\n.row-align ul li p span { display:block; color: #a5a5a5; font-size:14px;}\n.row-align ul li p small { display:inline-block; color: #ccc; font-size:16px;}\n.training { padding: 0 5px; margin: 0 0 30px;}\n.training h3 { vertical-align: middle; margin: 0;}\n.training h3 img { vertical-align: middle; display: inline-block;}\n.training h3 span { vertical-align: middle;display: inline-block; font-size: 16px; padding:0 0 0 10px;}\n.training a {display: inline-block; font-size: 14px;color:  #737070; margin: -10px 0 0;}\n.training ul { text-align: left;background: #fff; padding:0px 10px 0px;margin: 10px 0 0;}\n.training ul li { display: block; width: auto; border-bottom: #ccc 1px solid; padding:6px 0;}\n.training ul li:nth-last-child(1) { border: none;}\n.training ul li .training-cont { padding: 0;}\n.training ul li .training-cont label{color: #484747; font-weight: normal; font-size: 14px;display: block;}\n.training ul li .training-cont .date { width: auto;}\n.training ul li .training-cont .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; \nbox-shadow:none; width:auto;height:auto; min-width: auto;}\n.training ul li .training-cont .date button span { border: none !important; padding: 0 !important;}\n.training ul li .training-cont .date .form-control { width:77%; height: auto; padding: 0 0 0 10px;}\n.training ul li .training-cont .date .form-control:focus { box-shadow: none; border: none;}\n.training ul li .training-cont .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.training > ul > li > a {\n    color: #f0848a;\n    padding: 3px 15px;\n    border-radius: 20px;\n    text-align: center;\n    border: #f0848a 1px solid;\n    margin: 13px 0 0px;\n    font-size: 12px;\n}\n.training > ul > li .training-cont > a { color: #ccc;}\n.training.company h3{ padding: 0 5px 10px;}\n.training.company ul li .training-cont label { padding: 0 0 8px;}\n.training.company ul li { padding: 12px 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"emp-time\">\n  <div class=\"row\" style=\"margin: 0px;\">\n    <div *ngIf=\"FrontPageControls.show_work_anniversary\" class=\"col-xs-4\">\n      <mat-card class=\"card-shadow\">\n        <h4>Work Anniversary</h4>\n        <div class=\"row row-align\">\n          <ul class=\"list-inline\">\n\n            <li class=\"col-xs-6\">\n              <div class=\"managers\">\n                <span>\n                  <i class=\"material-icons emplployee-icon\">\n                    account_circle\n                  </i>\n                </span>\n                <div class=\"manager-right\">\n                  <h4>Crane Andy</h4>\n                  <h5>Manager</h5>\n                </div>\n              </div>\n            </li>\n\n            <li class=\"col-xs-4\">\n              \n                <p><b>26</b> <small>June</small>  <span> Anniversary</span></p>\n             \n            </li>\n\n            <li class=\"col-xs-2 send-box pull-right\">\n              <img src=\"./assets/images/newpages/ic_send_24px.png\" class=\"rotate-icon\">\n            </li>\n          </ul>\n        </div>\n\n        <div class=\"row row-align\">\n          <ul class=\"list-inline\">\n            <li class=\"col-xs-6\">\n              <div class=\"managers\">\n                <span>\n                  <i class=\"material-icons emplployee-icon\">\n                    account_circle\n                  </i>\n                </span>\n                <div class=\"manager-right\">\n                  <h4>Crane Andy</h4>\n                  <h5>Manager</h5>\n                </div>\n              </div>\n            </li>\n            <li class=\"col-xs-4\">\n              \n                <p><b>28</b> <small>June</small>  <span> Anniversary</span></p>\n             \n            </li>\n            <li class=\"col-xs-2 send-box pull-right\">\n              <img src=\"./assets/images/newpages/ic_send_24px.png\" class=\"rotate-icon\">\n            </li>\n          </ul>\n        </div>\n\n        <div class=\"row row-align\">\n          <ul class=\"list-inline\">\n            <li class=\"col-xs-6\">\n              <div class=\"managers\">\n                <span>\n                  <i class=\"material-icons emplployee-icon\">\n                    account_circle\n                  </i>\n                </span>\n                <div class=\"manager-right\">\n                  <h4>Crane Andy</h4>\n                  <h5>Manager</h5>\n                </div>\n              </div>\n            </li>\n            <li class=\"col-xs-4\">\n              \n                <p><b>30</b> <small>June</small>  <span> 3rd Anniversary</span></p>\n             \n            </li>\n            <li class=\"col-xs-2 send-box pull-right\">\n              <img src=\"./assets/images/newpages/ic_send_24px.png\" class=\"rotate-icon\">\n            </li>\n          </ul>\n        </div>\n\n      </mat-card>\n    </div>\n    <div *ngIf=\"FrontPageControls.show_birthdays\" class=\"col-xs-4\">\n      <mat-card class=\"card-shadow\">\n        <h4>Birthdays</h4>\n\n        <div class=\"row row-align\">\n          <ul class=\"list-inline\">\n            <li class=\"col-xs-6\">\n              <div class=\"managers\">\n                <span>\n                  <i class=\"material-icons emplployee-icon\">\n                    account_circle\n                  </i>\n                </span>\n                <div class=\"manager-right\">\n                  <h4>Crane Andy</h4>\n                  <h5>Manager</h5>\n                </div>\n              </div>\n            </li>\n            <li class=\"col-xs-4\">\n              \n                <p><b>30</b> <small>June</small>  <span> Birthdays</span></p>\n             \n            </li>\n            <li class=\"col-xs-2 send-box pull-right\">\n              <img src=\"./assets/images/newpages/ic_send_24px.png\" class=\"rotate-icon\">\n            </li>\n          </ul>\n        </div>\n\n        <div class=\"row row-align\">\n          <ul class=\"list-inline\">\n            <li class=\"col-xs-6\">\n              <div class=\"managers\">\n                <span>\n                  <i class=\"material-icons emplployee-icon\">\n                    account_circle\n                  </i>\n                </span>\n                <div class=\"manager-right\">\n                  <h4>Crane Andy</h4>\n                  <h5>Manager</h5>\n                </div>\n              </div>\n            </li>\n            <li class=\"col-xs-4\">\n              \n                <p><b>30</b> <small>June</small>  <span> Birthdays</span></p>\n             \n            </li>\n            <li class=\"col-xs-2 send-box pull-right\">\n              <img src=\"./assets/images/newpages/ic_send_24px.png\" class=\"rotate-icon\">\n            </li>\n          </ul>\n        </div>\n\n        <div class=\"row row-align\">\n          <ul class=\"list-inline\">\n            <li class=\"col-xs-6\">\n              <div class=\"managers\">\n                <span>\n                  <i class=\"material-icons emplployee-icon\">\n                    account_circle\n                  </i>\n                </span>\n                <div class=\"manager-right\">\n                  <h4>Crane Andy</h4>\n                  <h5>Manager</h5>\n                </div>\n              </div>\n            </li>\n            <li class=\"col-xs-4\">\n              \n                <p><b>30</b> <small>June</small>  <span> Birthdays</span></p>\n             \n            </li>\n            <li class=\"col-xs-2 send-box pull-right\">\n              <img src=\"./assets/images/newpages/ic_send_24px.png\" class=\"rotate-icon\">\n            </li>\n          </ul>\n        </div>\n      </mat-card>\n    </div>\n    <div *ngIf=\"FrontPageControls.show_to_do_list\" class=\"col-xs-4\">\n      <mat-card class=\"card-shadow\">\n        <h4>ToDo List</h4>\n        <div class=\"row row-margin\">\n          <div class=\"col-xs-6\">\n            <mat-checkbox>New Employee Intro</mat-checkbox>\n          </div>\n          <div class=\"col-xs-6\" style=\"color: #ccc;\">\n            Due: 13 June,2019\n          </div>\n\n        </div>\n        <div class=\"row row-margin\">\n          <div class=\"col-xs-6\">\n            <mat-checkbox>New Logo Design</mat-checkbox>\n          </div>\n          <div class=\"col-xs-6\" style=\"color: #ccc;\">\n            Due: 13 June,2019\n          </div>\n\n        </div>\n        <div class=\"row row-margin\">\n          <div class=\"col-xs-6\">\n            <mat-checkbox>New Employee Intro</mat-checkbox>\n          </div>\n          <div class=\"col-xs-6\" style=\"color: #ccc;\">\n            Due: 13 June,2019\n          </div>\n\n        </div>\n        <div class=\"row row-margin\">\n          <div class=\"col-xs-6\">\n            <mat-checkbox>New Employee Intro</mat-checkbox>\n          </div>\n          <div class=\"col-xs-6\" style=\"color: #ccc;\">\n            Due: 13 June,2019\n          </div>\n\n        </div>\n        <div class=\"row row-margin\">\n          <div class=\"col-xs-6\">\n            <mat-checkbox>New Employee Intro</mat-checkbox>\n          </div>\n          <div class=\"col-xs-6\" style=\"color: #ccc;\">\n            Due: 13 June,2019\n          </div>\n\n        </div>\n      </mat-card>\n    </div>\n  </div>\n  <div class=\"row\" style=\"margin: 20px 15px 0 15px;\">\n\n    <div class=\"training col-md-6 company\">\n\n      <h3>\n        <img src=\"../../../../assets/images/dashboard/company.png\" alt=\"img\">\n        <span>Company Links</span>\n      </h3>\n\n      <ul *ngIf=\"FrontPageControls.show_manager_links\">\n\n        <li *ngFor=\"let data of FrontPageControls.manager_links\">\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>{{data.site_name}}</label>\n            <a target=\"_blank\" [href]=\"data.site_link\">{{data.site_link}}</a>\n          </div>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n\n\n      </ul>\n\n    </div>\n\n\n    <div class=\"training col-md-6\">\n\n      <h3>\n        <img src=\"../../../../assets/images/dashboard/training.png\" alt=\"img\">\n        <span>Training</span>\n      </h3>\n\n      <a class=\"pull-right\">View My Training</a>\n      <div class=\"clearfix\"></div>\n\n      <ul>\n\n        <li>\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>New Hire On Board</label>\n            <div class=\"date\">\n              <button mat-raised-button (click)=\"picker.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <input matInput [matDatepicker]=\"picker\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n              <mat-datepicker #picker></mat-datepicker>\n\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n\n          <a class=\"pull-right\">Past Due</a>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n        <li>\n\n          <div class=\"training-cont pull-left col-md-8\">\n            <label>New Hire Training</label>\n            <div class=\"date\">\n              <button mat-raised-button (click)=\"picker1.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n              <mat-datepicker #picker1></mat-datepicker>\n\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n\n          <a class=\"pull-right\">Past Due</a>\n          <div class=\"clearfix\"></div>\n\n        </li>\n\n      </ul>\n\n    </div>\n\n    <div class=\"clearfix\"></div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ManagerViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerViewComponent", function() { return ManagerViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_frontpage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/manager-frontpage.service */ "./src/app/services/manager-frontpage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ManagerViewComponent = /** @class */ (function () {
    function ManagerViewComponent(managerFrontpageService) {
        this.managerFrontpageService = managerFrontpageService;
        this.FrontPageControls = {
            show_work_anniversary: false,
            show_birthdays: false,
            show_to_do_list: false,
            manager_links: []
        };
    }
    ManagerViewComponent.prototype.ngOnInit = function () {
        this.companyId = JSON.parse(localStorage.getItem('companyId'));
        this.managerFrontPageControls();
    };
    ManagerViewComponent.prototype.managerFrontPageControls = function () {
        var _this = this;
        this.managerFrontpageService.getAllAdditionalData(this.companyId)
            .subscribe(function (res) {
            console.log("get manager controls", res);
            _this.FrontPageControls = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    ManagerViewComponent.prototype.goToLink = function (url) {
        window.open(url, "_blank");
    };
    ManagerViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manager-view',
            template: __webpack_require__(/*! ./manager-view.component.html */ "./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.html"),
            styles: [__webpack_require__(/*! ./manager-view.component.css */ "./src/app/admin-dashboard/dashboard/manager-view/manager-view.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_frontpage_service__WEBPACK_IMPORTED_MODULE_1__["ManagerFrontpageService"]])
    ], ManagerViewComponent);
    return ManagerViewComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  system-admin-view works!\n</p>\n"

/***/ }),

/***/ "./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.ts ***!
  \********************************************************************************************/
/*! exports provided: SystemAdminViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemAdminViewComponent", function() { return SystemAdminViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SystemAdminViewComponent = /** @class */ (function () {
    function SystemAdminViewComponent() {
    }
    SystemAdminViewComponent.prototype.ngOnInit = function () {
    };
    SystemAdminViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-system-admin-view',
            template: __webpack_require__(/*! ./system-admin-view.component.html */ "./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.html"),
            styles: [__webpack_require__(/*! ./system-admin-view.component.css */ "./src/app/admin-dashboard/dashboard/system-admin-view/system-admin-view.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SystemAdminViewComponent);
    return SystemAdminViewComponent;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map