(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["on-boarding-on-boarding-module"],{

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.css":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal{padding: 0;\n    float : none; margin: 0 auto;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important;\n\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.zenwork-margin-zero{\n    margin: 20px 0 !important;\n}\n.zenwork-margin-zero b{\n    font-size: 16px!important;\n    color: #404040;\n}\n.zenwork-padding-10{\n    padding: 10px;\n}\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 6px 16px;\n    font-size: 14px;\n    margin:  0 10px 0 0;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.zenwork-inner-icon{\n    width: 20px;\n    height: auto;\n}\n.text-field{\n    border: none; \n    width: 20%;\n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n}\n.template-name{\n    margin: 30px 0 0 0;\n}\n.template-name label{\n    line-height: 40px;\n}\n.add-template-main{\n    width:100%;\n    border-bottom: 1px solid #eeeeee;\n\n}\n.field-accordion p {color: #565555;    font-size: 15px;\n    background: #fff;\n    margin: 0px 0 0 0px;}\n.field-accordion .panel-default>.panel-heading { padding: 20px 0 20px; border: none;}\n.field-accordion .panel-title {  display: inline-block; padding: 0 0 0 7px;}\n.field-accordion .panel-title a:hover { text-decoration: none;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;background: #fff;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n.add-fields{\n    width:80%;\n    float: right;\n    margin-top:20px;\n\n}\n.main-onboarding .panel-heading{\n    padding:10px 0px!important;\n}\n.main-onboarding{\n    width:100%;\n    margin-top: 20px;\n}\n.main-onboarding .panel-font{\n   \n    font-weight: 600;\n    padding: 0 0 0 20px;    \n}\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n.settings-dropdown{\n    float:right;\n    background: #fff;\n    border: none;\n}\n.main-onboarding .panel-heading .btn{\n    padding:2px 6px!important;\n    border-radius: 0!important;\n}\n.caret{\n    margin-bottom: 5px!important;\n}\n.panel-body{\n    padding:0px 15px 0px 15px !important;\n}\n.hr-tasks{\n    font-size: 16px;\n    font-weight: 600;\n    padding: 10px 0 0 40px;\n}\n.hire-forms{\n    font-size: 16px;\n    font-weight: 600;\n    padding: 0px 0 0 27px;\n}\n.individual-hr-task{\n    font-size:15px;\n    padding: 15px 0;\n    margin: 0;\n}\n.main-onboarding .setting-drop .dropdown-menu{\n    left: -76px !important;\n    min-width: 109px !important;\n}\n.individual-hr-task small{\n    padding: 0 0 0 28px;\n    color: #ccc;\n}\n.margin-top-zero{\n    margin-top:0px!important;\n}\n.padding-top-remove{\n    padding-top:0px!important;\n}\n.team-introdution-card{\n    background-color: #fcfcfc;\n    border-radius: 2px;\n    padding: 10px;\n}\n.margin-zero{\n    margin:0px!important;\n}\n.team-introdution-card .padding-top-remove small{\n    padding-left:25px; \n    color: #ccc;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.card-intro{\n    font-size: 15px;\n    padding-left: 30px;\n}\n.team-introdution-card .mat-checkbox{\n    display: block;\n}\n.team-introdution-card .individual-hr-task{\n    font-weight: 600;\n    display: block;\n    font-size: 16px;\n}\n.team-introdution-card small{\n    font-size: 14px;\n}\n.emp-image{\n    padding-top: 0;\n    width: 43px;\n    display: inline-block;\n    vertical-align: middle;\n  }\n.employee-details{\n    width: 70px;\n    display: inline-block;\n    vertical-align: middle;\n    text-align: left;\n    margin-left: 10px;\n  }\n.employee-image-info{\n    padding-left: 20px;\n  }\n.emp-name{\n    font-size: 11px;\n    padding-left: 0;\n    display: block;\n    line-height: 11px;\n  }\n.borderright{\n      border-right: 1px solid #eee;\n  }\n.team-text-area{\n    margin-top:10px;\n  }\ntextarea{\n     resize: none;\n     border: 0;\n     box-shadow: none;\n     background: #f8f8f8;\n  }\n.btn-color{\n    color: #ef8086;\n  }\n.new-emp-paperwork{\n    margin-top: 20px;\n    padding-left: 15px;\n  }\n.new-emp-paperwork .emp-paperwork-heading{\n    font-size: 12px;\n    font-weight: 600;\n    padding: 10px 0 0 0;\n  }\n.newhire-packets{\n    display: inline-block;\n    width:234px;\n\n}\n.newhire-checkbox{\n    display: inline-block;\n    vertical-align: top;\n    margin-top: 10px;\n}\n.newhire-content{\n    display: inline-block;\n    vertical-align: middle;\n  }\n.insert-drive{\n  width: 11px;\n  display: inline-block;\n  vertical-align: middle;\n  }\n.image-items{\n      display: inline-block;\n      padding:0px 7px;\n  }\n.open>.settings-dropdown.btn-default{\n    background: #fff;\n}\n.settings-dropdown:hover,.settings-dropdown:active{\n    background: #fff !important;\n}\n.buttons{\n    margin: 20px 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;\n}\n.sub-title { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"personal col-md-11\">\n  <!-- <div class=\"zenwork-currentpage\"> -->\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management/on-boarding']\">\n      <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n    </a>\n    <span class=\"inner-icon-img\">\n      <img src=\"../../../assets/images/Onboarding/Group 828.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n    </span>\n    <small class=\"sub-title\"><b>Onboarding Template - Custom Onboarding Template</b></small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n  <!-- </div> -->\n  <div class=\"heading\">\n    <h4>Add / Edit - Custom Onboarding Template</h4>\n  </div>\n  <div class=\"template-name\">\n    <label>Onboarding template Name</label><br>\n\n    <input type=\"text\" [(ngModel)]=\"template.templateName\" class=\"form-control text-field\" placeholder=\"Template Name\"\n      name=\"templateName\" #templateName=\"ngModel\" required>\n    <p *ngIf=\"!template.templateName && templateName.touched || (!template.templateName && isValid)\" class=\"error\">\n      Please fill this field</p>\n  </div>\n  <div class=\"main-onboarding on-boarding-template\">\n    <!-- <div class=\"panel panel-info\"> -->\n    <div class=\"panel-heading\">\n\n      <div class=\"field-accordion\">\n\n        <div class=\"panel-group\" id=\"accordion1\">\n\n          <div class=\"panel panel-default border-bottom\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n                  <span class=\"panel-title panel-font pull-left\">Onboarding Tasks </span>\n                </a>\n              </h4>\n              <span class=\"pull-right\">\n                <div class=\"setting-drop\">\n                  <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                    <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                  </a>\n                  <ul class=\"dropdown-menu\">\n\n                    <li *ngIf=\"!isDisabled\">\n                      <a (click)=\"openDialog('add')\">Add</a>\n                    </li>\n\n                    <li *ngIf=\"isDisabled\">\n                      <a (click)=\"openDialog('edit')\">Edit</a>\n                    </li>\n                    <li *ngIf=\"isDisabled\">\n                      <a (click)=\"deleteCategory()\">Delete</a>\n                    </li>\n\n                  </ul>\n                </div>\n              </span>\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n          <div id=\"collapseOne\" class=\"panel-collapse collapse in\"\n            *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\">\n            <p *ngIf=\"template['Onboarding Tasks'][task].length\" class=\"hr-tasks\">{{task}}</p>\n\n            <!-- <div *ngIf=\"template['Onboarding Tasks'][task].length === 0\" class=\"panel-body\">\n\n              <p class=\"individual-hr-task\">\n                No Task Assign For This Section\n              </p>\n              <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n\n            </div> -->\n            <div *ngFor=\"let data of template['Onboarding Tasks'][task]\" class=\"panel-body\">\n\n              <p class=\"individual-hr-task\">\n                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\"\n                  (ngModelChange)=\"singleTaskChoose(task,data.taskName,data.isChecked)\">{{data.taskName}}</mat-checkbox>\n                <br>\n                <small>{{data.taskAssignee}}-{{data.Date | date}}</small>\n              </p>\n\n              <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n\n            </div>\n\n          </div>\n\n        </div>\n      </div>\n    </div>\n\n    <div class=\"clearfix\"></div>\n  <p *ngIf=\" (getKeys((template['Onboarding Tasks'])).length < 0 && isValid)\" class=\"error\">\n      Please fill this field</p>\n\n    <!-- <hr class=\"zenwork-margin-ten-zero margin-zero\"> -->\n\n\n  </div>\n  <div class=\"buttons\">\n    <ul class=\"list-unstyled\">\n\n      <li class=\"list-items\">\n        <a class=\"cancel\"  [routerLink]=\"['/admin/admin-dashboard/employee-management/on-boarding']\">Cancel</a>\n      </li>\n      <li class=\"list-items pull-right\">\n        <button class='save' (click)=\"customOnboardTemplateAdd()\" type=\"submit\">\n          Submit\n        </button>\n      </li>\n\n      <div class=\"clearfix\"></div>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.ts":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: CustomOnboardTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomOnboardTemplateComponent", function() { return CustomOnboardTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../edit-standard-onboarding-template/edit-standard-onboarding-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CustomOnboardTemplateComponent = /** @class */ (function () {
    function CustomOnboardTemplateComponent(dialog, swalAlertService, onboardingService, route) {
        this.dialog = dialog;
        this.swalAlertService = swalAlertService;
        this.onboardingService = onboardingService;
        this.route = route;
        this.allTasks = [];
        this.isValid = false;
        this.isDisabled = false;
        this.popupData = "customOnboardTemplate";
        this.template = {
            templateName: "",
            templateType: "base",
            "Onboarding Tasks": {
            // "HR Tasks": [
            //   // {
            //     // category: "HR Tasks",
            //     // taskName: "tttt",
            //     // timeToComplete: { time: 1, unit: "Day" },
            //     // _id: "5cb9775328e5a5231db1f8c7"
            //   // }
            // ],
            // "New Hire Forms": [
            // ],
            // "IT Setup": [
            // ],
            // "Manager Tasks": [
            // ]
            }
        };
    }
    CustomOnboardTemplateComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.popupData = 'customOnboardTemplate';
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log(_this.id);
        });
        if (this.id)
            this.getStandardOnboardingtemplate(this.id);
    };
    /* Description: to get a custom onboarding template data
     author : vipin reddy */
    CustomOnboardTemplateComponent.prototype.getStandardOnboardingtemplate = function (id) {
        var _this = this;
        this.onboardingService.getStandardOnboardingtemplate(id)
            .subscribe(function (res) {
            console.log(res);
            _this.template = res.data;
            _this.templateId = res.data._id;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: to show task names in ngfor
     author : vipin reddy */
    CustomOnboardTemplateComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    /* Description: select only one task
     author : vipin reddy */
    CustomOnboardTemplateComponent.prototype.singleTaskChoose = function (taskArray, taskName, isChecked) {
        var _this = this;
        console.log(taskArray, taskName, isChecked);
        if (isChecked == false) {
            this.popupData = '';
            this.isDisabled = false;
        }
        if (isChecked == true) {
            for (var task in this.template["Onboarding Tasks"]) {
                console.log(task);
                this.template["Onboarding Tasks"][task].forEach(function (element) {
                    console.log(element);
                    if (element.taskName === taskName) {
                        element.isChecked = true;
                        _this.popupData = element;
                        _this.addbtn = element;
                        _this.isDisabled = true;
                        console.log("popdaata", _this.popupData);
                    }
                    else {
                        element.isChecked = false;
                    }
                });
            }
        }
        this.template["Onboarding Tasks"][taskArray];
    };
    /* Description: open a popup for custom template add/delete
     author : vipin reddy */
    CustomOnboardTemplateComponent.prototype.openDialog = function (field) {
        var _this = this;
        // var data = this.popupData
        console.log(field);
        if (field == 'add') {
            this.popupData = 'customOnboardTemplate';
            var data = {
                id: this.templateId,
                data: this.popupData,
                category: this.catArray
            };
            if (data.data != '') {
                var dialogRef = this.dialog.open(_edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_3__["EditStandardOnboardingTemplateComponent"], {
                    width: '1300px',
                    backdropClass: 'structure-model',
                    data: data,
                });
                dialogRef.afterClosed().subscribe(function (result) {
                    console.log("resultttt", result);
                    if (_this.template['Onboarding Tasks'][result.category]) {
                        _this.template['Onboarding Tasks'][result.category].push(result);
                    }
                    else {
                        _this.template['Onboarding Tasks'][result.category] = [result];
                    }
                    console.log(_this.template, "12121");
                    _this.catArray = _this.template['Onboarding Tasks'];
                    // for(var i=0)
                    // if(result.category == )
                    console.log('The dialog was closed', _this.catArray);
                });
            }
            else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", "Atleast select one task", "error");
            }
        }
        if (field == 'edit') {
            var data1 = {
                _id: this.templateId,
                data: this.popupData,
                template: this.template
            };
            if (data1._id != '' || data1._id == undefined) {
                console.log("data coming");
                var dialogRef = this.dialog.open(_edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_3__["EditStandardOnboardingTemplateComponent"], {
                    width: '1300px',
                    backdropClass: 'structure-model',
                    data: data1,
                });
                dialogRef.afterClosed().subscribe(function (result) {
                    console.log("resultttt", result);
                    _this.isDisabled = false;
                    // if (this.template['Onboarding Tasks'][result.category]) {
                    //   this.template['Onboarding Tasks'][result.category].push(result);
                    // } else {
                    //   this.template['Onboarding Tasks'][result.category] = [result];
                    // }
                    if (_this.id != undefined) {
                        _this.getStandardOnboardingtemplate(_this.id);
                    }
                    console.log(_this.template, "12121");
                    // for(var i=0)
                    // if(result.category == )
                    console.log('The dialog was closed');
                });
            }
            else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", "Atleast select one task", "error");
            }
        }
    };
    /* Description: data submit for custom onboarding template
     author : vipin reddy */
    CustomOnboardTemplateComponent.prototype.customOnboardTemplateAdd = function () {
        var _this = this;
        this.isValid = true;
        console.log(this.template['Onboarding Tasks']);
        if (Object.keys(this.template['Onboarding Tasks']).length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Onboarding template", "Add atleast one category", "error");
        }
        if (this.template.templateName && Object.keys(this.template['Onboarding Tasks']).length > 0)
            this.onboardingService.addCustomOnboardTemplate(this.template)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Custom Onboarding Template", res.message, "success");
                    _this.getStandardOnboardingtemplate(res.data._id);
                }
                else {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Custom Onboarding Template", res.message, "error");
                }
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Custom Onboarding Template", err.error.message, "error");
            });
    };
    /* Description: delete a category
     author : vipin reddy */
    CustomOnboardTemplateComponent.prototype.deleteCategory = function () {
        var _this = this;
        var data = {
            _id: this.templateId,
            data: this.popupData
        };
        this.onboardingService.deleteCategory(data)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Delete Category", res.message, "success");
                _this.getStandardOnboardingtemplate(_this.id);
            }
        }, function (err) {
            console.log(err);
        });
    };
    CustomOnboardTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-custom-onboard-template',
            template: __webpack_require__(/*! ./custom-onboard-template.component.html */ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.html"),
            styles: [__webpack_require__(/*! ./custom-onboard-template.component.css */ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_1__["OnboardingService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], CustomOnboardTemplateComponent);
    return CustomOnboardTemplateComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.css":
/*!***************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.css ***!
  \***************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".model-header{\n    border-bottom: 1px solid gray;\n    font-size: 18px;\n}\n.company-legal-name{\n    border: none;\n    padding: 10px;\n    background: #f8f8f8;\n    box-shadow: none;\n    height: 40px;\n}\n.modal-body{\n    position: relative;\n    padding: 30px 55px;\n}\nlabel{\n    line-height: 30px;\n}\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\n.select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n    width: 95%;\n}\ntextarea{\n    resize: none;\n    border: 0;\n    box-shadow: none;\n    background: #f8f8f8;\n }\n.addcategory{\n    background: #fff;\n    box-shadow: none;\n    border: none;\n}\n.buttons{\n    margin: 20px 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 20px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;\n}\n.custom-template-sroll { overflow-y: auto; height: 500px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.html":
/*!****************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.html ***!
  \****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"main-template\">\n\n  <div class=\"model-header\">\n    <span *ngIf=\"!customTemplateAdd && !standardData && !editStandardData\">Custom Template</span>\n    <span *ngIf=\"standardData\" class=\"\">Standard Onboarding Template</span>\n    <span *ngIf=\"!customTemplateAdd && editStandardData && !standardData\" class=\"\">Custom Onboarding Template</span>\n    <p *ngIf=\"!customTemplateAdd && !standardData && !editStandardData\">(Onboarding Template - Add Task)</p>\n    <p *ngIf=\"standardData\" class=\"\">(Standard Onboarding Template - Edit Task)</p>\n    <p *ngIf=\"!customTemplateAdd && editStandardData && !standardData\" class=\"\">(Custom Onboarding Template - Edit Task)\n    </p>\n  </div>\n  <div class=\"modal-body\">\n      \n    <div class=\"custom-template-sroll\">\n\n    <div class=\"task-name col-xs-3\">\n      <div class=\"form-group\">\n        <label>Task Name *</label>\n        <span *ngIf=\"!customTemplateAdd && editStandardData\">\n          <input type=\"text\" placeholder=\"Task Name\" [(ngModel)]=\"data.data.taskName\"\n            class=\" form-control company-legal-name\" name=\"taskName\" maxlength=\"18\" #taskName=\"ngModel\" required>\n          <p *ngIf=\"!data.data.taskName && taskName.touched || (!data.data.taskName && isValid)\" class=\"error\">\n            Please fill this field</p>\n        </span>\n        <span *ngIf=\"!customTemplateAdd && !standardData &&  !editStandardData\">\n          <input type=\"text\" placeholder=\"Task Name\" maxlength=\"18\" [(ngModel)]=\"customOnboard.taskName\"\n            class=\"form-control company-legal-name\" name=\"taskName\" #taskName=\"ngModel\" required>\n          <p *ngIf=\"!customOnboard.taskName && taskName.touched || (!customOnboard.taskName && isValid)\" class=\"error\">\n            Please fill this field</p>\n        </span>\n\n      </div>\n    </div>\n    <div class=\"clearfix\"></div>\n    <div class=\"col-xs-12\">\n      <div class=\"task-name col-xs-3\">\n        <div class=\"row\">\n          <div class=\"form-group\">\n            <label>Task Assignee *</label>\n            <span *ngIf=\"!customTemplateAdd  && editStandardData\">\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"data.data.taskAssignee\"\n                placeholder=\"Select employee\" (ngModelChange)=\"empEmail($event,'data')\" name=\"taskAssignee\"\n                #taskAssignee=\"ngModel\" required>\n                <mat-option *ngFor=\"let data of employeeData\" [value]=\"data.email\">{{data.personal.name.preferredName}}\n                </mat-option>\n                <!-- <mat-option value=\"Job2\">Job2</mat-option> -->\n              </mat-select>\n              <p *ngIf=\"!data.data.taskAssignee && taskAssignee.touched || (!data.data.taskAssignee && isValid)\"\n                class=\"error\">\n                Please fill this field</p>\n            </span>\n            <span *ngIf=\"!customTemplateAdd && !editStandardData\">\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"customOnboard.taskAssignee\"\n                placeholder=\"Select employee\" (ngModelChange)=\"empEmail($event,'custom')\" name=\"taskAssignee\"\n                #taskAssignee=\"ngModel\" required>\n                <mat-option *ngFor=\"let data of employeeData\" [value]=\"data.email\" value=\"Job1\">\n                  {{data.personal.name.preferredName}}</mat-option>\n                <!-- <mat-option value=\"Job2\">Job2</mat-option> -->\n              </mat-select>\n              <p *ngIf=\"!customOnboard.taskAssignee && taskAssignee.touched || (!customOnboard.taskAssignee && isValid)\"\n                class=\"error\">\n                Please fill this field</p>\n            </span>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"task-name col-xs-3\">\n        <div class=\"row\">\n          <div class=\"form-group\">\n            <label>Assignee Email Address</label>\n            <span *ngIf=\"!customTemplateAdd && editStandardData\">\n              <input type=\"text\" placeholder=\"Email\" [(ngModel)]=\"data.data.assigneeEmail\"\n                class=\" form-control company-legal-name\" name=\"assigneeEmail\" #assigneeEmail=\"ngModel\" required\n                readonly>\n              <p *ngIf=\"!data.data.assigneeEmail && assigneeEmail.touched || (!data.data.assigneeEmail && isValid)\"\n                class=\"error\">\n                Please fill this field</p>\n            </span>\n            <span *ngIf=\"!customTemplateAdd && !editStandardData\">\n              <input type=\"text\" placeholder=\"Email\" [(ngModel)]=\"customOnboard.assigneeEmail\"\n                class=\" form-control company-legal-name\" name=\"assigneeEmail\" #assigneeEmail=\"ngModel\" required\n                readonly>\n              <p *ngIf=\"!customOnboard.assigneeEmail && assigneeEmail.touched || (!customOnboard.assigneeEmail && isValid)\"\n                class=\"error\">\n                Please fill this field</p>\n            </span>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"task-name col-xs-6\">\n      <label>Task Description *</label>\n      <div class=\"form-group\">\n        <span *ngIf=\"!customTemplateAdd && editStandardData\">\n          <textarea class=\"form-control rounded-0\" [(ngModel)]=\"data.data.taskDesription\" rows=\"6\" name=\"taskDesription\"\n            #taskDesription=\"ngModel\" required></textarea>\n          <p *ngIf=\"!data.data.taskDesription && taskDesription.touched || (!data.data.taskDesription && isValid)\"\n            class=\"error\"> Please fill this field</p>\n        </span>\n        <span *ngIf=\"!customTemplateAdd && !editStandardData\">\n          <textarea class=\"form-control rounded-0\" [(ngModel)]=\"customOnboard.taskDesription\" rows=\"6\"\n            name=\"taskDesription\" #taskDesription=\"ngModel\" required></textarea>\n          <p *ngIf=\"!customOnboard.taskDesription && taskDesription.touched || (!customOnboard.taskDesription && isValid)\"\n            class=\"error\"> Please fill this field</p>\n        </span>\n      </div>\n\n    </div>\n    <div class=\"col-xs-12\">\n      <div class=\"task-name col-xs-3\">\n        <div class=\"row\">\n          <div class=\"form-group\">\n            <label>Expected Task Completion Time*</label>\n            <span *ngIf=\"!customTemplateAdd && editStandardData\">\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"data.data.timeToComplete.time\"\n                placeholder=\"Time\" name=\"time\" #time=\"ngModel\" required>\n                <mat-option *ngFor=\"let time of taskCOmpletionTime\" [value]=\"time\">{{time}}</mat-option>\n                <p *ngIf=\"!data.data.timeToComplete.time && time.touched || (!data.data.timeToComplete.time && isValid)\"\n                  class=\"error\"> Please fill this field</p>\n                <!-- <mat-option value=\"2\">2</mat-option> -->\n              </mat-select>\n            </span>\n            <span *ngIf=\"!customTemplateAdd && !editStandardData\">\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"customOnboard.timeToComplete.time\"\n                placeholder=\"Time\" name=\"time\" #time=\"ngModel\" required>\n                <mat-option *ngFor=\"let time of taskCOmpletionTime\" [value]=\"time\">{{time}}</mat-option>\n                <!-- <mat-option value=\"2\">2</mat-option> -->\n              </mat-select>\n              <p *ngIf=\"!customOnboard.timeToComplete.time && time.touched || (!customOnboard.timeToComplete.time && isValid)\"\n                class=\"error\"> Please fill this field</p>\n            </span>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"task-name col-xs-3\">\n        <div class=\"row\">\n          <div class=\"form-group\">\n            <label> Time Period *</label>\n            <span *ngIf=\"!customTemplateAdd  && editStandardData\">\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"data.data.timeToComplete.unit\"\n                placeholder=\"Time\" name=\"unit\" #unit=\"ngModel\" required>\n                <mat-option *ngFor=\"let days of timeTypes\" [value]='days'>{{days}}</mat-option>\n                <!-- <mat-option value=\"Week\">Week</mat-option> -->\n                <!-- <mat-option value=\"Month\">Month</mat-option>  -->\n              </mat-select>\n              <p *ngIf=\"!data.data.timeToComplete.unit && unit.touched || (!data.data.timeToComplete.unit && isValid)\"\n                class=\"error\"> Please fill this field</p>\n            </span>\n            <span *ngIf=\"!customTemplateAdd && !editStandardData\">\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"customOnboard.timeToComplete.unit\"\n                placeholder=\"Unit\" name=\"unit\" #unit=\"ngModel\" required>\n\n                <mat-option *ngFor=\"let days of timeTypes\" [value]='days'>{{days}}</mat-option>\n                <!-- <mat-option value=\"Week\">Week</mat-option> -->\n                <!-- <mat-option value=\"Month\">Month</mat-option>  -->\n              </mat-select>\n              <p *ngIf=\"!customOnboard.timeToComplete.unit && unit.touched || (!customOnboard.timeToComplete.unit && isValid)\"\n                class=\"error\"> Please fill this field</p>\n            </span>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"task-name col-xs-3\">\n      <!-- <div class=\"row\"> -->\n      <div class=\"form-group\">\n        <label> Task Category*</label>\n        <span *ngIf=\"standardData\">\n          <!-- <mat-select [disabled]=\"standardData\" class=\"form-control text-field select-btn\"\n            [(ngModel)]=\"data.data.category\" placeholder=\"Select category\" name=\"category\" #category=\"ngModel\" required>\n            <mat-option value=\"HR Tasks\">Hr-tasks</mat-option>\n            <mat-option value=\"New Hire Forms\">New Hire Forms</mat-option>\n            <mat-option value=\"IT Setup\">IT Setup</mat-option>\n            <mat-option value=\"Manager Tasks\">Manager Tasks</mat-option>\n          </mat-select> -->\n\n          <input type=\"text\" [readonly]=\"standardData\" class=\"form-control text-field select-btn\"\n            [(ngModel)]=\"data.data.category\" placeholder=\"Select category\" name=\"category\" #category=\"ngModel\" required>\n          <p *ngIf=\"!data.data.category && category.touched || (!data.data.category && isValid)\" class=\"error\">\n            Please fill this field</p>\n        </span>\n        <span *ngIf=\"!categoryInput && !customTemplateAdd && !standardData && !editStandardData\">\n          <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"customOnboard.category\"\n            placeholder=\"Select categoryadddddd\" name=\"category\" #category=\"ngModel\" required>\n            <mat-option *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\" [value]=\"task\">{{task}}\n            </mat-option>\n          </mat-select>\n          <p *ngIf=\"!customOnboard.category && category.touched || (!customOnboard.category && isValid)\" class=\"error\">\n            Please fill this field</p>\n        </span>\n        <span *ngIf=\"!customTemplateAdd && editStandardData && !standardData\">\n          <mat-select class=\"form-control text-field select-btn\" (ngModelChange)=\"categoryChange($event)\"\n            [(ngModel)]=\"data.data.category\" placeholder=\"Select category\" name=\"category\" #category=\"ngModel\" required>\n            <mat-option *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\" [value]=\"task\">{{task}}\n            </mat-option>\n          </mat-select>\n          <p *ngIf=\"!data.data.category && category.touched || (!data.data.category && isValid)\" class=\"error\">\n            Please fill this field</p>\n        </span>\n      </div>\n\n    </div>\n    <div *ngIf=\"!customTemplateAdd && !editStandardData\" class=\"col-xs-3\" style=\"padding:35px 0 0 0;\">\n      <button class=\"addcategory\" (click)=\"addCategory()\"><span class=\"green\"><i class=\"fa fa-plus\"></i></span>&nbsp;Add\n        category</button>\n    </div>\n    <div class=\"col-xs-12\">\n      <div *ngIf=\"categoryInput\" class=\"form-group col-xs-3\">\n        <div class=\"row\">\n          <input type=\"text\" [(ngModel)]=\"customOnboard.category\" class=\" form-control company-legal-name\"\n            name=\"category\" #category=\"ngModel\" required>\n          <p *ngIf=\"!customOnboard.category && category.touched || (!customOnboard.category && isValid)\" class=\"error\">\n            Please fill this field</p>\n        </div>\n      </div>\n    </div>\n    <div class=\"clearfix\"></div>\n    <hr class=\"zenwork-margin-ten-zero\">\n    <div class=\"buttons\">\n      <ul class=\"list-unstyled\">\n\n        <li class=\"list-items\">\n          <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n        </li>\n        <li *ngIf=\"!customTemplateAdd && editStandardData\" class=\"list-items pull-right\">\n          <button class='save' (click)=\"editData()\" type=\"submit\">\n            Submit\n          </button>\n        </li>\n        <li *ngIf=\"!customTemplateAdd && !standardData && !editStandardData\" class=\"list-items pull-right\">\n          <button class='save' (click)=\"addCustomOnboard()\" type=\"submit\">\n            Submit\n          </button>\n        </li>\n\n        <div class=\"clearfix\"></div>\n      </ul>\n    </div>\n\n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.ts":
/*!**************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.ts ***!
  \**************************************************************************************************************************************************/
/*! exports provided: EditStandardOnboardingTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditStandardOnboardingTemplateComponent", function() { return EditStandardOnboardingTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../..../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var EditStandardOnboardingTemplateComponent = /** @class */ (function () {
    function EditStandardOnboardingTemplateComponent(dialogRef, swalAlertService, onboardingService, siteAccessRolesService, data) {
        this.dialogRef = dialogRef;
        this.swalAlertService = swalAlertService;
        this.onboardingService = onboardingService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.data = data;
        this.categoryInput = false;
        this.customTemplateAdd = false;
        this.editStandardData = false;
        this.standardData = false;
        this.isValid = false;
        this.newCategory = [];
        this.taskCOmpletionTime = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        this.timeTypes = ["Day", "Week", "Month"];
        this.customOnboard = {
            taskName: '',
            taskAssignee: '',
            assigneeEmail: '',
            taskDesription: '',
            timeToComplete: {
                time: '',
                unit: ''
            },
            category: '',
        };
        this.template = {
            "templateName": "Onboarding Template",
            "Onboarding Tasks": {}
        };
    }
    EditStandardOnboardingTemplateComponent.prototype.ngOnInit = function () {
        console.log("viiviiiii hgxhgsvah", this.data);
        if (this.data.data == 'customOnboardTemplate') {
            console.log(this.data.category);
            if (this.data.category != undefined) {
                this.template['Onboarding Tasks'] = this.data.category;
            }
            this.customTemplateAdd = (this.data == 'customOnboardTemplate');
            // this.standardData = !this.standardData;
            if (this.data.id) {
                this.getStandardOnboardingtemplate(this.data.id);
                console.log(this.customTemplateAdd);
            }
        }
        if (this.data.data.templatename == 'standard') {
            this.standardData = !this.standardData;
        }
        if (this.data._id) {
            console.log(this.data._id);
            this.editStandardData = !this.editStandardData;
            this.getStandardOnboardingtemplate(this.data._id);
            // this.template = this.data.template;
            this.tempcategory = this.data.data.category;
            this.tempTaskname = this.data.data.taskName;
        }
        if (this.data._id == undefined && this.data.data != 'customOnboardTemplate') {
            this.editStandardData = !this.editStandardData;
            this.tempcategory = this.data.data.category;
            this.tempTaskname = this.data.data.taskName;
            this.data.data = this.data.data;
            this.template['Onboarding Tasks'] = this.data.template['Onboarding Tasks'];
        }
        this.employeeSearch();
    };
    /* Description: getting employees list
       author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: data auto complete in email field
     author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.empEmail = function ($event, e) {
        console.log($event, "sd");
        if (e == 'data') {
            this.data.data.assigneeEmail = $event;
            // this.data.data.taskAssignee = $event.personal.name.preferredName;
        }
        else
            (e == 'custom');
        {
            this.customOnboard.assigneeEmail = $event;
        }
    };
    EditStandardOnboardingTemplateComponent.prototype.categoryChange = function (event) {
        var _this = this;
        console.log("sadas", event, this.data.data, this.tempTaskname, this.template['Onboarding Tasks'][this.tempcategory]);
        console.log(this.tempcategory, this.data.data.category, "2222222222222");
        if (this.tempcategory != event) {
            this.template['Onboarding Tasks'][this.tempcategory].forEach(function (element) {
                console.log(element, 'taskssss');
                if (element.taskName == _this.tempTaskname) {
                    console.log(element.taskName, _this.tempTaskname, "1dhjhjffdjs`");
                    var index = _this.template['Onboarding Tasks'][_this.tempcategory].indexOf(element);
                    console.log("index", index);
                    _this.template['Onboarding Tasks'][event].push(element);
                    if (index > -1) {
                        _this.template['Onboarding Tasks'][_this.tempcategory].splice(index, 1);
                        delete _this.data.data['isChecked'];
                    }
                    // this.template['Onboarding Tasks'][this.data.data.category].push(element)
                }
            });
        }
        console.log(this.template['Onboarding Tasks'], "vipin");
    };
    /* Description: After editing a popup data(Task) Submition
      author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.editData = function () {
        var _this = this;
        console.log(this.data.data.category);
        // this.categoryChange(this.data.data.category)
        this.data.data.isChecked = false;
        if (this.data.data.taskName && this.data.data.taskAssignee && this.data.data.assigneeEmail &&
            this.data.data.taskDesription && this.data.data.timeToComplete.time && this.data.data.timeToComplete.unit &&
            this.data.data.category) {
            // this.template['Onboarding Tasks'][this.data.data.category]['isChecked'] = false;
            // this.template['Onboarding Tasks'][this.data.data] =  Object.assign(this.template['Onboarding Tasks'][this.data.data.category], { isChecked: false })
            console.log(this.template['Onboarding Tasks'][this.data.data.category], this.data.data);
            if (this.template['Onboarding Tasks'][this.data.data.category]) {
                console.log("data come in 1");
                var count1 = 0;
                var count = 0;
                this.template['Onboarding Tasks'][this.data.data.category].forEach(function (element) {
                    if (count1 < 1 && count < 1) {
                        console.log('count inner', _this.data.data._id, element._id);
                        if (element._id == _this.data.data._id) {
                            console.log("====1");
                            var index = _this.template['Onboarding Tasks'][_this.data.data.category].indexOf(element);
                            console.log("index", index);
                            if (index > -1) {
                                _this.template['Onboarding Tasks'][_this.data.data.category].splice(index, 1);
                                delete _this.data.data['isChecked'];
                            }
                            _this.template['Onboarding Tasks'][_this.data.data.category].push(_this.data.data);
                            count1 = count1 + 1;
                        }
                    }
                });
                this.template['Onboarding Tasks'][this.data.data.category].forEach(function (element) {
                    if (count < 1 && count1 < 0) {
                        if (element._id != _this.data.data._id) {
                            console.log("push in else");
                            element = [_this.data.data];
                            count = count + 1;
                            console.log(count, 'count');
                        }
                    }
                });
            }
            else {
                console.log("data come in 2");
                this.template['Onboarding Tasks'][this.data.data.category] = [this.data.data];
            }
            console.log('senfingdata', this.template);
            if (this.data._id != undefined) {
                this.onboardingService.addCustomOnboardTemplate(this.template)
                    .subscribe(function (res) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", res.message, "success");
                    console.log(res, "edit res");
                    _this.dialogRef.close();
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", err.error.message, "error");
                });
            }
            if (this.data._id == undefined) {
                this.dialogRef.close();
            }
        }
    };
    /* Description: To get a data and show in popup
        author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.getStandardOnboardingtemplate = function (id) {
        var _this = this;
        this.onboardingService.getStandardOnboardingtemplate(id)
            .subscribe(function (res) {
            console.log(res);
            _this.template = res.data;
            _this.templateId = res.data._id;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: to get a task names in ngFor loop
     author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    /* Description: adding custom onboarding template
       author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.addCustomOnboard = function () {
        console.log(this.customOnboard, "vipin 4355");
        this.isValid = true;
        if (this.customOnboard.taskName && this.customOnboard.taskAssignee && this.customOnboard.assigneeEmail &&
            this.customOnboard.taskDesription && this.customOnboard.timeToComplete.time &&
            this.customOnboard.timeToComplete.unit && this.customOnboard.category) {
            this.dialogRef.close(this.customOnboard);
        }
        console.log(this.newCategory);
    };
    /* Description: new add category in popup
       author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.addCategory = function () {
        this.categoryInput = !this.categoryInput;
    };
    /* Description: close the popup
       author : vipin reddy */
    EditStandardOnboardingTemplateComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    EditStandardOnboardingTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-standard-onboarding-template',
            template: __webpack_require__(/*! ./edit-standard-onboarding-template.component.html */ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.html"),
            styles: [__webpack_require__(/*! ./edit-standard-onboarding-template.component.css */ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.css")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_3__["OnboardingService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__["SiteAccessRolesService"], Object])
    ], EditStandardOnboardingTemplateComponent);
    return EditStandardOnboardingTemplateComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-jumbotron-bg{\n    background-color: #fff;\n    box-shadow: -1px 1px 37px -5px rgba(0,0,0,0.37);\n    padding: 35px;\n}\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 6px 16px;\n    font-size: 14px;\n    margin:  0 10px 0 0;\n}\n.margin-top-40{\n    margin: 40px 0 0 0;\n}\n.nav-link {\n    color:#3e3e3ea3!important;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n.zenwork-inner-icon{\n    width: 30px;\n    height: auto;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.sub-title{\n    display: inline-block;\n    margin: 5px 0 0;\n}\na:hover{\n    text-decoration: none;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important;\n    vertical-align: middle;\n}\n.main-tabs-heading{\n    margin: 0 auto;\n    float: none;\n    padding: 0;\n}\n::ng-deep .ngx-tabs-customstyle a{\n    width: 33%!important;\n}\n.onboarding-tabs li a:active,.onboarding-tabs li a:visited,.onboarding-tabs li a:focus{\n\n    border-radius: 0px!important;\n    color: #353131!important;\n}\n.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{\n    border: 1px solid transparent!important;\n    border-bottom: 4px solid #439348;\n    background: #f8f8f8;\n}\n.hire-wizard{\n    width:100%;\n    float:left; margin: 30px 0 0;\n}\n.hire-wizard-empty{\n    width:100%;\n    border-bottom: 1px solid #ddd;\n    float:right;\n    margin-top:45px;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.main-employee{\n    width: 22px\n}\n.zenwork-custom-company-settings{\n    padding-top: 40px;\n    width:100%;\n}\n.zenwork-margin-zero b{\n    font-size: 14px!important;\n    color: #404040;\n}\n.zenwork-custom-company-settings a{\n    color : #3e3e3ea3;\n    text-decoration: none;\n}\n.jumbotron p{\n    font-size:25px !important; font-weight: 600;color: #3f3e3f;\n}\n.zenwork-settings-wrapper{\n    padding: 20px; \n}\nsmall{\n    font-size: 9px;\n    color:#3e3e3ea3;\n}\n.border-top{\n    border-top: 1px solid #e4eae7;\n}\n.border-right{\n    border-right: 1px solid #e4eae7;\n    height:110px;\n}\n/* \ndiv.border-right:nth-child(odd) { border-right:#000 10px solid;} */\n.margin-bottom-20{\n    margin:0 0 20px;\n}\n.hire-heading{\n    font-weight: 400;\n    font-size: 20px; padding: 15px 0 30px;\n}\n.new-sub-heading{\n    font-weight: 600;\n    font-size: 18px; color:#636363; padding: 15px 0 0; display: block;\n}\n.add-template-main{\n    width:100%;\n    border-bottom: 1px solid #eeeeee;\n\n}\n.add-template{\n    width:20%;\n    float: left;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    padding-bottom: 10px;\n}\n.add-template .btn-employee{\n    background-color: #439348 !important;\n    font-size: 12px;\n    padding: 3px 12px !important;\n}\n.add-fields{\n    width:80%;\n    float: right;\n    margin-top:20px;\n\n}\n.add-buttons{\n    display: inline-block;\n    float: right;\n}\n.list-buttons{\n    display: inline-block;\n    padding: 0px 20px;\n    font-size: 12px;\n    font-weight: 600;\n}\n.plus-icon{\n    color:#439348 !important;\n}\n.main-onboarding .panel-heading{\n    padding:10px 15px!important;\n}\n.main-onboarding{\n    width:100%;\n    margin-top: 40px;\n}\n.main-onboarding .panel-font{\n    color:#439348 !important;\n    font-weight: 600;\n    margin-top: 5px;\n}\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n.settings-dropdown{\n    float:right;\n    background: #fff;\n    border: none;\n}\n.main-onboarding .panel-heading .btn{\n    padding:2px 6px!important;\n    border-radius: 0!important;\n}\n.caret{\n    margin-bottom: 5px!important;\n}\n.panel-body{\n    padding:15px 15px 0px 15px !important;\n}\n.hr-tasks{\n    font-size: 13px;\n    font-weight: 600;\n}\n.individual-hr-task{\n    font-size:12px;\n}\n.margin-top-zero{\n    margin-top:0px!important;\n}\n.padding-top-remove{\n    padding-top:0px!important;\n}\n.team-introdution-card{\n    background-color: #fcfcfc;\n    border-radius: 2px;\n    padding: 10px;\n}\n.margin-zero{\n    margin:0px!important;\n}\n.team-introdution-card .padding-top-remove small{\n    padding-left:25px; \n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.card-intro{\n    font-size: 11px;\n    padding-left: 30px;\n}\n.team-introdution-card .individual-hr-task{\n    font-weight: 600;\n    color: #439348;\n}\n.checkbox {\n    padding-left: 20px;\n    margin-top:0px !important;\n    margin-bottom:0px !important;\n  }\n.checkbox label {\n    display: inline-block;\n    vertical-align: middle;\n    position: relative;\n    padding-left: 5px;\n  }\n.checkbox label::before {\n    content: \"\";\n    display: inline-block;\n    position: absolute;\n    width: 17px;\n    height: 17px;\n    left: 0;\n    top: 7px;\n    margin-left: -20px;\n    border: 1px solid #cccccc;\n    border-radius: 3px;\n    background-color: #fff;\n    transition: border 0.15s ease-in-out, color 0.15s ease-in-out;\n  }\n.checkbox label::after {\n    display: inline-block;\n    position: absolute;\n    width: 16px;\n    height: 16px;\n    left: 0;\n    top: 7px;\n    margin-left: -20px;\n    padding-left: 3px;\n    padding-top: 1px;\n    font-size: 11px;\n    color: #555555;\n  }\n.checkbox input[type=\"checkbox\"] {\n    opacity: 0;\n    z-index: 1;\n  }\n.checkbox input[type=\"checkbox\"]:focus + label::before {\n    outline: thin dotted;\n    outline: 5px auto -webkit-focus-ring-color;\n    outline-offset: -2px;\n  }\n.checkbox input[type=\"checkbox\"]:checked + label::after {\n    font-family: 'FontAwesome';\n    content: \"\\f00c\";\n  }\n.checkbox input[type=\"checkbox\"]:disabled + label {\n    opacity: 0.65;\n  }\n.checkbox input[type=\"checkbox\"]:disabled + label::before {\n    background-color: #eeeeee;\n    cursor: not-allowed;\n  }\n.checkbox.checkbox-inline {\n    margin-top: 0;\n  }\n.checkbox-success input[type=\"checkbox\"]:checked + label::before {\n    background-color: #439348 !important;\n    border-color: #439348 !important;\n  }\n.checkbox-success input[type=\"checkbox\"]:checked + label::after {\n    color: #fff;\n  }\n.emp-image{\n    padding-top: 0;\n    width: 43px;\n    display: inline-block;\n    vertical-align: middle;\n  }\n.employee-details{\n    width: 70px;\n    display: inline-block;\n    vertical-align: middle;\n    text-align: left;\n    margin-left: 10px;\n  }\n.employee-image-info{\n    padding-left: 20px;\n  }\n.emp-name{\n    font-size: 11px;\n    padding-left: 0;\n    display: block;\n    line-height: 11px;\n  }\n.borderright{\n      border-right: 1px solid #eee;\n  }\n.team-text-area{\n    margin-top:10px;\n  }\ntextarea{\n     resize: none;\n     border: 0;\n     box-shadow: none;\n     background: #f8f8f8;\n  }\n.btn-color{\n    border: 1px solid #EF8086;\n    color: #ef8086;\n  }\n.new-emp-paperwork{\n    margin-top: 20px;\n    padding-left: 15px;\n  }\n.new-emp-paperwork .emp-paperwork-heading{\n    font-size: 12px;\n    font-weight: 600;\n    padding: 10px 0 0 0;\n  }\n.newhire-packets{\n    display: inline-block;\n    width:234px;\n\n}\n.newhire-checkbox{\n    display: inline-block;\n    vertical-align: top;\n    margin-top: 10px;\n}\n.newhire-content{\n    display: inline-block;\n    vertical-align: middle;\n  }\n.insert-drive{\n  width: 11px;\n  display: inline-block;\n  vertical-align: middle;\n  }\n.image-items{\n      display: inline-block;\n      padding:0px 7px;\n  }\n.open>.settings-dropdown.btn-default{\n    background: #fff;\n}\n.settings-dropdown:hover,.settings-dropdown:active{\n    background: #fff !important;\n}\n/*-- Author:Suresh M, Date:18-04-19  --*/\n/*-- Tab css code  --*/\n.new-hire-wizard { position: relative;}\n.top-menu { position: absolute; top:-20px; right:-10px;}\n.new-wizard { display: block;}\n.new-wizard .modal-dialog { width: 75%;}\n.new-wizard .modal-content {border-radius: 5px;}\n.new-wizard .modal-header {padding: 20px 30px;}\n.new-wizard .modal-body { padding: 0;}\n.wizard-tabs { display: block;}\n.wizard-lft { background: #f8f8f8; padding: 15px 15px 75px 0;border-radius: 0 0 0 5px; height:80vh;}\n.wizard-lft.custom-wizard{height:70vh;}\n.wizard-lft .nav-tabs{ border:none;}\n.wizard-lft .nav-tabs>li { float: none; margin: 0 0 15px;}\n.wizard-lft .nav-tabs > li.active > a, .wizard-lft .nav-tabs > li.active > a:focus, .wizard-lft .nav-tabs > li.active > a:hover \n{ border-bottom: none !important;background:transparent; color:#a5a3a3; border-radius: 30px;}\n.wizard-lft .nav-tabs>li>a { font-size: 17px; margin: 0 0 0 20px; color: #a5a3a3;}\n.wizard-lft .nav>li>a:focus, .wizard-lft .nav>li>a:hover { background: transparent; color:#a5a3a3;}\n.wizard-rgt { padding:0 0 30px 30px;}\n.wizard-hight { height: 550px; overflow-y: auto;}\n.personal { margin: 30px auto 40px; float: none; height: 500px; overflow-y: auto;}\n.personal .panel-default>.panel-heading { padding: 0 0 25px; background-color:transparent !important; border: none;}\n.personal .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n.personal .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.personal .panel-title>.small, .personal .panel-title>.small>a, .personal .panel-title>a, .personal .panel-title>small, .personal .panel-title>small>a { text-decoration:none;}\n.personal .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.personal .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.wizard-popup {border-bottom:#ccc 1px solid !important; margin: 0 0 25px;}\n.personal .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.personal .panel-group .panel-heading+.panel-collapse>.list-group, .personal .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.personal .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.personal .panel-body { padding: 0 0 5px;}\n.personal .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.personal .panel-default { border-color:transparent;}\n.personal .panel-group .panel+.panel { margin-top: 20px;}\n.personal .panel.border{ border: none !important;}\n.personal-cont { display:block; padding: 0 0 30px;}\n.personal-cont ul { display:inline-block; width:100%;}\n.personal-cont ul li { margin: 0 0 25px; padding: 0;}\n.personal-cont ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.personal-cont ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%; background:#f8f8f8;}\n.personal-cont ul li .form-control:focus{ box-shadow: none;}\n.personal-cont ul li a {\n    color: #fff;\n    font-weight: normal;\n    font-size: 16px;\n    display: inline-block;\n    cursor: pointer;\n    background: #484747;\n    margin:34px 0 0;\n    padding: 8px 10px;\n}\n.personal-cont ul li .btn {\n    color: #fff;\n    font-weight: normal;\n    font-size: 16px;\n    display: inline-block;\n    background: #484747;\n    margin:34px 0 0;\n    padding: 8px 10px;\n}\n.personal-cont ul li .date { height: auto !important; line-height:inherit !important; width:95%;background:#f8f8f8;}\n.personal-cont ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.personal-cont ul li .date span { border: none !important; padding: 0 !important;}\n.personal-cont ul li .date .form-control { width:77%; height: auto; padding: 12px 0 12px 10px;}\n.personal-cont ul li .date .form-control:focus { box-shadow: none; border: none;}\n.personal-cont ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.personal-cont ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#484747;\n  }\n.personal-cont ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484747;\n  }\n.personal-cont ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.personal-cont ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.personal-cont ul li.veteran, .personal-cont ul li.personal-dob { float: none; clear: both;}\n.personal-cont ul li em { display: inline-block; width:12%; background:#f8f8f8; float: left; font-style: normal; padding: 11px 0; text-align: center;}\n.personal-cont ul li .form-control.payrate{ width: 83%; display: inline-block;float:left; padding: 11px 12px 11px 0;}\n.administration-address { padding: 0 0 20px;}\n.administration-address ul { display: inline-block; width: 100%;}\n.administration-address ul li { padding: 0; margin: 0 20px 20px 0; float: none;}\n.administration-address ul li.details-width{ width: 25%;}\n.administration-address ul li .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none; height: auto; width: 100%; padding: 12px;background: #f8f8f8;}\n.administration-address ul li select { background:#fff; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n.administration-address ul li.city { width: 20%;float: left;}\n.administration-address ul li.state { width: 20%; float: left;}\n.administration-address ul li.zip { width: 20%; float: left; padding: 0;}\n.administration-address ul li.united-state {width: 20%; padding: 0; clear: left;}\n.administration-contact { display: block;}\n.administration-contact ul { margin: 0 0 30px;display: inline-block; width: 100%;}\n.administration-contact ul li {margin: 0 20px 15px 0; padding: 0;}\n.administration-contact ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n.administration-contact ul li .phone { display: block; background:#f8f8f8; padding:8px 10px;clear: left;}\n.administration-contact ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding:4px 10px;}\n.administration-contact ul li .phone span .fa { color: #636060;font-size: 23px;line-height: 20px;width: 18px;text-align: center;}\n.administration-contact ul li .phone .form-control { display: inline-block;vertical-align:middle; width: 80%; border: none; background:#f8f8f8; box-shadow: none; padding: 0 0 0 10px;\nheight: auto; line-height: inherit;}\n.administration-contact ul li .form-control { display: inline-block;vertical-align: middle; width: 40%; border: none;background: #f8f8f8; box-shadow: none; \nheight: 40px; line-height: 40px; padding: 0 10px; font-size: 15px; margin: 0;}\n.administration-contact ul li.phone-width {  clear:left;width:33%}\n.administration-contact ul li.email-width { float: none;}\n.social.administration-contact ul li.phone-width {  clear:left;width:25%}\n.administration-contact ul li .phone.email-phone {display: inline-block;vertical-align: middle;width: 42%;}\n.administration-contact ul li var {display: inline-block;vertical-align: middle;padding: 0 0 0 20px;font-style: normal; color:#a5a3a3; }\n.wizard-rgt .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.wizard-rgt .btn.cancel { color:#e44a49; background:transparent; padding:10px 0 0 30px;font-size: 16px;}\n.wizard-rgt .btn.back { color:#4c4c4c; background:transparent;border:#b7b3b3 1px solid; padding: 8px 35px; border-radius: 20px;font-size: 16px;}\n.personal .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.personal .btn.cancel { color:#e44a49; background:transparent; padding:10px 0 0 30px;font-size: 16px;}\n.personal .btn.back { color:#4c4c4c; background:transparent;border:#b7b3b3 1px solid; padding: 8px 35px; border-radius: 20px;font-size: 16px;}\n.personal-cont .benefits > ul { margin:0 0 20px;}\n.personal-cont .benefits > ul > li {position: relative;}\n.personal-cont .benefits > ul > li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.personal-cont ul li.personal-dob .date .form-control{ width:80%;}\n.personal-cont ul li.personal-dob .date{ width: 70%;}\n.personal-cont ul li.personal-dob .form-control { width: 50%;}\n/*-- Author:Suresh M, Date:18-04-19  --*/\n/*-- Tab css code  --*/\n.on-board{ padding:0;}\n.on-board label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.on-board .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:25%; background:#f8f8f8; margin: 0 0 20px;}\n.on-board .panel-default>.panel-heading { padding:15px 0 15px 30px; background-color:#edf7fe; border: none;}\n.on-board .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n.on-board .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.on-board .panel-title>.small, .on-board .panel-title>.small>a, .on-board .panel-title>a, .on-board .panel-title>small, .on-board .panel-title>small>a { text-decoration:none;}\n.on-board .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid;}\n.on-board .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.on-board .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.on-board .panel-group .panel-heading+.panel-collapse>.list-group, .on-board .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.on-board .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.on-board .panel-body { padding: 0 !important;}\n.on-board .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto;}\n.on-board .panel-default { border-color:transparent;}\n.on-board .panel-group .panel+.panel { margin-top: 20px;}\n.on-board-cont { background: #f8f8f8; padding: 20px 0 20px 30px;}\n.on-board-cont h4 { margin: 0 0 20px; color:#484747;}\n.on-board-cont ul { display: block;}\n.on-board-cont ul li { border-bottom:#ccc 1px solid; padding: 0 0 15px; margin: 0 0 15px;}\n.on-board-cont ul li h5 { margin: 0 0 5px;color:#484747;}\n.on-board-cont ul li small { font-size: 14px; color:#ccc;}\n.leave-schedule { display: block;}\n.leave-schedule h3 {color:#484747; font-size: 18px; margin: 0 0 20px;}\n.leave-schedule .table{ background:#fff; margin-bottom: 60px;}\n.leave-schedule .table>tbody>tr>th, .leave-schedule .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.leave-schedule .table>tbody>tr>td, .leave-schedule .table>tfoot>tr>td, .leave-schedule .table>tfoot>tr>th, \n.leave-schedule .table>thead>tr>td {color: #484747;padding:15px;font-size:15px;background: #f8f8f8;border-bottom: 1px solid #ddd;}\n.leave-schedule button.mat-menu-item a { color:#ccc;}\n.leave-schedule button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.leave-schedule .table>tbody>tr>th a, .leave-schedule .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.leave-schedule .table>tbody>tr>th a .fa, .leave-schedule .table>thead>tr>th a .fa { color: #6f6d6d;}\n.leave-schedule button { border: none; outline: none; background:transparent;}\n.checkbox-for-custom{ display: inline-block;vertical-align:super; padding: 0 20px 0 0;}\n.zenwork-custom-company-settings a { display: inline-block; vertical-align:text-bottom;}\n.zenwork-custom-company-settings a img { cursor: pointer;}\n.field-accordion .panel-default>.panel-heading { padding:10px 0 10px 14px; border: none;background-color: #edf7fe !important;}\n.field-accordion .panel-title {  display: inline-block; padding: 0 0 0 7px;}\n.field-accordion .panel-title a:hover { text-decoration: none;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;background: #fff;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n.field-accordion span { padding: 0 0 0 30px;}\n.hr-tasks{\n    font-size: 16px;\n    font-weight: 600;\n    padding: 10px 0 0 30px;\n}\n.hire-forms{\n    font-size: 16px;\n    font-weight: 600;\n    padding: 0px 0 0 27px;\n}\n.individual-hr-task{ \n    font-size:15px;\n    padding: 15px 0;\n    margin: 0;\n}\n.individual-hr-task small{\n    padding: 0 0 0 28px;\n    color: #ccc; font-size: 12px;\n}\n.summary-review { display: block;}\n.summary-review {height: 570px; overflow-y: auto;}\n.summary-review h2 { margin: 0 0 35px; color: #317b36;font-size: 20px;}\n.personal1{ height: auto; margin: 20px 0 0;}\n.newhire-summary {height: 570px; overflow-y: auto;margin: 20px 0 0;}\n.newhire-summary h2 { margin: 0 0 35px; color: #317b36;font-size: 20px;}\n.edit-work { float: none; margin: 0 auto;height:700px;overflow-y: auto; padding: 0;}\n.edit-work ul {border-bottom: #e5e5e5 1px solid; padding:25px 0; display: inline-block; width: 100%;}\n.edit-work ul li {display:block; margin: 0 0 10px;}\n.edit-work ul li label{color: #484747; font-weight: normal; font-size: 17px; padding: 0 0 10px; display: block;}\n.edit-work ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%;\nbackground:#f8f8f8;}\n.edit-work ul li .form-control:focus{ box-shadow: none;}\n.edit-work ul li .date { height: auto; line-height:inherit; width:75%;background:#f8f8f8;}\n.edit-work ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.edit-work ul li .date span { border: none !important; padding: 0 !important;}\n.edit-work ul li .date .form-control { width:65%; height: auto; padding: 12px 0 12px 10px; display: inline-block;}\n.edit-work ul li .date a { display: inline-block;}\n.edit-work ul li .date a:focus {outline: none;border:none;}\n.edit-work ul li .date .form-control:focus { box-shadow: none; border: none;}\n.edit-work ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.date1 { height: auto !important; line-height:inherit !important; width:66% !important;background:#fff;}\n.edit-work .date1 button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none;\nwidth: 40px;height: 30px; min-width: auto; border-left: #ccc 1px solid; border-radius: 0;}\n.date1 span { border: none !important; padding: 0 !important;}\n.date1 .form-control { width:73%; height: auto; padding: 12px 0 12px 10px; border: none; box-shadow: none; display: inline-block;}\n.date1 .form-control:focus { box-shadow: none; border: none;}\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.edit-work .table{ background:#fff; margin:20px 0 10px;}\n.edit-work .table>tbody>tr>th, .edit-work .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:17px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.edit-work .table>tbody>tr>td, .edit-work .table>tfoot>tr>td, .edit-work .table>tfoot>tr>th, \n.edit-work .table>thead>tr>td {color: #484747;padding:20px 15px;font-size:15px; background: #f7f7f7;}\n.edit-work button.mat-menu-item a { color:#ccc;}\n.edit-work button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.edit-work .table>tbody>tr>th a, .edit-work .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n.edit-work .table>tbody>tr>th a .fa, .edit-work .table>thead>tr>th a .fa { color: #6f6d6d;}\n.edit-work button { border: none; outline: none; background:transparent;}\n.scroll { display: block;}\n.border-btm {border-top: #bdb7b7 1px solid; padding:30px 0 0; float: none; margin: 40px auto 30px;}\n.scroll .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n.scroll .btn.cancel {color:#e44a49; background:transparent; padding:0 0 0 30px;font-size: 16px;}\n.edit-work .table>tbody>tr>td .form-control { display: inline-block;border: none; height:auto; padding:10px; box-shadow: none;}\n.edit-work .table>tbody>tr>td .fa { display: inline-block;font-size: 20px;top: 5px; position: absolute;\n    right: 10px;\n    border-left: #ccc 1px solid;\n    padding: 6px 0 6px 10px; color: #ccc;}\n.edit-work .table>tbody>tr>td .form-control:focus{ border-color:transparent;box-shadow: none;}\n.start-time { position: relative; width:30%;}\n.edit-work ul.total-hours { padding:20px 0;}\n.edit-work ul.total-hours li { margin: 0 0 25px;}\n.main-works { display: block;}\n.errors {color:#e44a49; padding: 5px 0 0; display: block;}\n.disableTab{\n    pointer-events: none;\n}\n.skip-template{\n    padding: 30px 0 0 0 !important;\n}\n.skip-temp-float{\n    float: left !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management']\">\n        <button class=\"btn zenwork-customized-back-btn\">\n          <span class=\"green mr-7\">\n            <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n          </span>Back\n        </button>\n      </a>\n      <span class=\"inner-icon-img\">\n        <img src=\"../../../assets/images/employee-management/onboard.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </span>\n      <small class=\"sub-title\">\n        <b>\n\n          Onboarding\n\n        </b>\n      </small>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero zenwork-border-bottom\">\n  </div>\n\n  <div class=\"main-tabs-heading col-md-11\">\n\n    <div class=\"hire-wizard\">\n      <!-- Author:Suresh M, Date:18-04-19  -->\n      <!-- Tab content -->\n      <div class=\"tab-content\">\n\n        <div class=\"tab-pane fade in active\">\n          <div class=\"zenwork-custom-company-settings\">\n            <div class=\"jumbotron zenwork-jumbotron-bg\">\n              <div class=\"col-xs-12 text-center new-hire-wizard\">\n                <div class=\"row\">\n                  <p class=\"hire-heading\">New Hire Wizard</p>\n                </div>\n\n                <div class=\"top-menu\">\n                  <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n                    <mat-icon>more_vert</mat-icon>\n                  </button>\n                  <mat-menu #menu=\"matMenu\">\n                    <button mat-menu-item *ngIf=\"deleteId.length == 0\">\n                      <a data-toggle=\"modal\" data-target=\"#myModal1\">Add</a>\n                    </button>\n\n                    <button mat-menu-item>\n                      <a (click)=\"customEdit()\">Edit</a>\n                      <a data-toggle=\"modal\" data-target=\"#myModal1\" #openEdit></a>\n                    </button>\n                    <button mat-menu-item>\n                      <a (click)=\"customHrDelete()\">Delete</a>\n                    </button>\n                  </mat-menu>\n                </div>\n\n              </div>\n              <div class=\"col-xs-6 text-center \">\n                <div class=\"zenwork-settings-wrapper border-right margin-bottom-20\">\n\n                  <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\">\n                    <img src=\"../../../assets/images/employee-management/onboard.png\" alt=\"Directory icon\"\n                      class=\"center-block\">\n                    <p class=\"zenwork-margin-zero\">\n                      <span class='new-sub-heading'>Standard New Hire Wizard</span>\n                    </p>\n\n                  </a>\n                </div>\n              </div>\n\n\n              <div *ngFor=\"let customNew of getAllCustomHR\" class=\"col-xs-6 text-center\">\n                <div class=\"zenwork-settings-wrapper border-right margin-bottom-20\">\n                  <div class=\"checkbox-for-custom\">\n                    <mat-checkbox class=\"zenwork-customized-checkbox\" (change)=\"showOptions($event,customNew._id)\">\n                    </mat-checkbox>\n                  </div>\n                  <a>\n                    <img src=\"../../../assets/images/employee-management/onboard.png\" alt=\"Directory icon\"\n                      class=\"center-block \" data-toggle=\"modal\" data-target=\"#myModal\"\n                      (click)=\"openstandpopupedit(customNew._id)\">\n\n                    <p class=\"zenwork-margin-zero\">\n                      <span class='new-sub-heading'>{{customNew.module_settings.wizardName}}</span>\n                    </p>\n\n                  </a>\n                </div>\n              </div>\n\n\n              <div class=\"clearfix\"></div>\n\n\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"tab-pane fade in active\">\n          <div class=\"zenwork-custom-company-settings\">\n            <div class=\"jumbotron zenwork-jumbotron-bg\">\n              <div class=\"col-xs-12 text-center new-hire-wizard\">\n                <div class=\"row\">\n                  <p class=\"hire-heading\">Onboard Template</p>\n                </div>\n\n                <div class=\"top-menu\">\n                  <button mat-icon-button [matMenuTriggerFor]=\"menu1\">\n                    <mat-icon>more_vert</mat-icon>\n                  </button>\n                  <mat-menu #menu1=\"matMenu\">\n                    <button [disabled]=\"deleteId.length > 0\" mat-menu-item>\n                      <span (click)=\"customTemplate()\">Add</span>\n                    </button>\n\n                    <!-- <button [disabled]=\"true\" mat-menu-item>\n                      <span>Edit</span>\n                    </button> -->\n                    <button [disabled]=\"deleteId.length < 1\" mat-menu-item>\n                      <span (click)=\"deleteTemplate()\">Delete</span>\n                    </button>\n                  </mat-menu>\n                </div>\n\n              </div>\n              <div class=\"col-xs-6 text-center \">\n                <div class=\"zenwork-settings-wrapper border-right margin-bottom-20\">\n\n                  <a (click)=\"templateRedirect()\">\n                    <img src=\"../../../assets/images/employee-management/onboard.png\" alt=\"Directory icon\"\n                      class=\"center-block \">\n                    <p class=\"zenwork-margin-zero\">\n                      <span class='new-sub-heading'>{{standardTemplate.templateName}}</span>\n                    </p>\n\n                  </a>\n                </div>\n              </div>\n              <div *ngFor=\"let data of templates\" class=\"col-xs-6 text-center \">\n                <div class=\"zenwork-settings-wrapper border-right margin-bottom-20\">\n                  <div class=\"checkbox-for-custom\">\n                    <mat-checkbox class=\"zenwork-customized-checkbox\" (change)=\"showOptions($event,data._id)\">\n                    </mat-checkbox>\n                  </div>\n                  <a (click)=\"customTemplateRedirect(data._id)\">\n                    <img src=\"../../../assets/images/employee-management/onboard.png\" alt=\"Directory icon\"\n                      class=\"center-block \">\n                    <p class=\"zenwork-margin-zero\">\n                      <span class='new-sub-heading'>{{data.templateName}}</span>\n                    </p>\n\n                  </a>\n                </div>\n              </div>\n\n              <div class=\"clearfix\"></div>\n\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class=\"clearfix\"></div>\n  </div>\n</div>\n\n\n<!-- Author:Suresh M, Date:19-04-19  -->\n<!-- Standard Wizard -->\n<div class=\"new-wizard\">\n\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Standard New Hire Wizard <br>\n            (Add Employee - Personal Fields)</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"wizard-tabs\">\n\n            <div class=\"wizard-lft col-md-3\">\n              <ul class=\"nav nav-tabs tabs-left\">\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'ClientInfo' }\">\n                  <a href=\"#ClientInfo\" data-toggle=\"tab\" #client_info (click)=\"personalFieldss('ClientInfo')\">Personal\n                    Fields</a></li>\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab2' }\">\n                  <a data-toggle=\"tab\" #job_compensation (click)=\"personalFieldss('tab2')\">Job &\n                    Compensation</a>\n\n                </li>\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab3' }\">\n                  <a data-toggle=\"tab\" #module_Settings (click)=\"personalFieldss('tab3')\">Module\n                    Settings</a></li>\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab4' }\">\n                  <a data-toggle=\"tab\" #view_Onboard (click)=\"personalFieldss('tab4')\">View\n                    Onboarding\n                    Tasks</a></li>\n                <li><a data-toggle=\"tab\">Benefits</a></li>\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab6' }\">\n                  <a data-toggle=\"tab\" #leave_management (click)=\"personalFieldss('tab6')\">Leave\n                    Management</a></li>\n                <li><a data-toggle=\"tab\">Performance Management</a></li>\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab8' }\">\n                  <a data-toggle=\"tab\" (click)=\"personalFieldss('tab8')\">Summary / Review</a></li>\n              </ul>\n            </div>\n\n            <div class=\"wizard-rgt col-md-9\">\n\n\n              <div class=\"tab-content\">\n\n                <div class=\"tab-pane\" id=\"ClientInfo\"\n                  [ngClass]=\"{'active':selectedNav === 'ClientInfo','in':selectedNav==='ClientInfo'}\">\n                  <div class=\"personal-main\">\n                    <div class=\"personal\">\n\n                      <form [formGroup]=\"personalForm\">\n\n                        <div class=\"panel-group\" id=\"accordion3\">\n\n                          <div class=\"panel panel-default\">\n                            <div class=\"panel-heading\">\n                              <h4 class=\"panel-title\">\n                                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                  href=\"#collapseOne\">\n                                  Personal\n                                </a>\n                              </h4>\n                            </div>\n                            <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n                              <div class=\"panel-body\">\n\n                                <div class=\"personal-cont\">\n                                  <div formGroupName=\"personal\">\n                                    <div formGroupName=\"name\">\n                                      <ul>\n                                        <li class=\"col-md-3\">\n                                          <label>First Name <span>*</span></label>\n                                          <input type=\"text\" class=\"form-control\" placeholder=\"First Name\"\n                                            formControlName=\"firstName\" [(ngModel)]=\"firstName\" maxlength=\"40\"\n                                            onkeypress=\"return (event.charCode > 64 && \n                                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('name').get('firstName').invalid && personalForm.get('personal').get('name').get('firstName').touched\"\n                                            class=\"errors\">Enter First name</span>\n                                        </li>\n\n                                        <li class=\"col-md-3\">\n                                          <label>Middle Name <span>*</span></label>\n                                          <input type=\"text\" class=\"form-control\" placeholder=\"Middle Name\"\n                                            formControlName=\"middleName\" [(ngModel)]=\"middleName\" maxlength=\"40\"\n                                            onkeypress=\"return (event.charCode > 64 && \n                                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('name').get('middleName').invalid && personalForm.get('personal').get('name').get('middleName').touched\"\n                                            class=\"errors\">Enter Middle name</span>\n                                        </li>\n\n                                        <li class=\"col-md-3\">\n                                          <label>Last Name <span>*</span></label>\n                                          <input type=\"text\" class=\"form-control\" placeholder=\"Last Name\"\n                                            formControlName=\"lastName\" [(ngModel)]=\"lastName\" maxlength=\"40\" onkeypress=\"return (event.charCode > 64 && \n                                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('name').get('lastName').invalid && personalForm.get('personal').get('name').get('lastName').touched\"\n                                            class=\"errors\">Enter Last name</span>\n                                        </li>\n\n                                        <li class=\"col-md-3\">\n                                          <label>Preferred Name <span>*</span></label>\n                                          <input type=\"text\" class=\"form-control\" placeholder=\"Preferred Name\"\n                                            formControlName=\"preferredName\" [(ngModel)]=\"preferredName\" maxlength=\"40\"\n                                            onkeypress=\"return (event.charCode > 64 && \n                                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('name').get('preferredName').invalid && personalForm.get('personal').get('name').get('preferredName').touched\"\n                                            class=\"errors\">Enter Preferred name</span>\n                                        </li>\n\n                                      </ul>\n                                    </div>\n                                  </div>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"personal\">\n                                      <li class=\"col-md-4 personal-dob\">\n                                        <label>Date of Birth <span>*</span></label>\n\n                                        <div class=\"date\">\n                                          <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm / dd / yy\"\n                                            class=\"form-control\" formControlName=\"dob\" [(ngModel)]=\"dob\" readonly\n                                            [max]=\"dobDate\">\n                                          <mat-datepicker #picker2></mat-datepicker>\n                                          <button mat-raised-button (click)=\"picker2.open()\">\n                                            <span>\n                                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                            </span>\n                                          </button>\n                                          <div class=\"clearfix\"></div>\n                                        </div>\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('dob').invalid && personalForm.get('personal').get('dob').touched\"\n                                          class=\"errors\">Enter Date of birth</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Gender <span>*</span></label>\n                                        <mat-select class=\"form-control\" placeholder=\"Gender\" formControlName=\"gender\"\n                                          [(ngModel)]=\"gender\">\n                                          <mat-option value=\"\">Gender</mat-option>\n                                          <mat-option value=\"male\">Male</mat-option>\n                                          <mat-option value=\"female\">Female</mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('gender').invalid && personalForm.get('personal').get('gender').touched\"\n                                          class=\"errors\">Enter gender</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"personal\">\n                                      <li class=\"col-md-3\">\n                                        <label>Marital Status <span>*</span></label>\n                                        <mat-select class=\"form-control\" formControlName=\"maritalStatus\"\n                                          placeholder=\"Marital Status\" [(ngModel)]=\"maritalStatus\">\n                                          <mat-option *ngFor=\"let data of allMaritalStatusArray\" [value]=\"data.name\">\n                                            {{data.name}}</mat-option>\n\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('maritalStatus').invalid && personalForm.get('personal').get('maritalStatus').touched\"\n                                          class=\"errors\">Enter Marital status</span>\n                                      </li>\n\n                                      <li *ngIf=\"maritalStatus != 'Single'\" class=\"col-md-3\">\n                                        <label>Marital Effective Date <span>*</span></label>\n\n                                        <div class=\"date\">\n                                          <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\"\n                                            class=\"form-control\" formControlName=\"effectiveDate\"\n                                            [(ngModel)]=\"effectiveDate\" [required]=\"maritalStatus!='Single'\" readonly\n                                            [min]=\"dob\">\n                                          <mat-datepicker #picker1></mat-datepicker>\n                                          <button mat-raised-button (click)=\" picker1.open()\">\n                                            <span>\n                                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                            </span>\n                                          </button>\n                                          <div class=\"clearfix\"></div>\n                                        </div>\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('effectiveDate').invalid && personalForm.get('personal').get('effectiveDate').touched\"\n                                          class=\"errors\">Enter Effective date</span>\n                                      </li>\n\n\n                                      <li class=\"col-md-3 veteran\">\n                                        <label>Veteran Status <span>*</span></label>\n                                        <mat-select class=\"form-control\" formControlName=\"veteranStatus\"\n                                          placeholder=\"Veteran Status\" [(ngModel)]=\"veteranStatus\">\n                                          <mat-option *ngFor=\"let data of allVeteranStatusArray\" [value]=\"data.name\">\n                                            {{data.name}}</mat-option>\n\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('veteranStatus').invalid && personalForm.get('personal').get('veteranStatus').touched\"\n                                          class=\"errors\">Enter Veteran status</span>\n                                      </li>\n\n                                      <li class=\"col-md-3 veteran\">\n                                        <label>SSN <span>*</span></label>\n                                        <input class=\"form-control\" placeholder=\"SSN\" formControlName=\"ssn\"\n                                          [(ngModel)]=\"ssn\" mask=\"000-00-0000\" [dropSpecialCharacters]=\"false\">\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('ssn').invalid && personalForm.get('personal').get('ssn').touched\"\n                                          class=\"errors\">Enter SSN</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"personal\">\n                                      <li class=\"col-md-5\">\n                                        <label>Ethnicity <span>*</span></label>\n                                        <mat-select class=\"form-control\" formControlName=\"ethnicity\"\n                                          placeholder=\"Ethnicity\" [(ngModel)]=\"ethnicity\">\n                                          <mat-option *ngFor=\"let data of ethnicityArray\" [value]=\"data.name\">\n                                            {{data.name}}</mat-option>\n\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"personalForm.get('personal').get('ethnicity').invalid && personalForm.get('personal').get('ethnicity').touched\"\n                                          class=\"errors\">Enter Ethnicity</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                </div>\n\n                                <div class=\"clearfix\"></div>\n\n                              </div>\n                            </div>\n\n                          </div>\n\n                          <div class=\"panel panel-default\">\n                            <div class=\"panel-heading\">\n                              <h4 class=\"panel-title\">\n                                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                  href=\"#collapseTwo\">\n                                  Address\n                                </a>\n                              </h4>\n\n                            </div>\n\n                            <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n                              <div class=\"panel-body\">\n\n                                <div class=\"administration-address\">\n                                  <div formGroupName=\"personal\">\n                                    <div formGroupName=\"address\">\n                                      <ul>\n                                        <li class=\"col-md-5\">\n                                          <input type=\"text\" placeholder=\"Street1 *\" maxlength=\"60\" class=\"form-control\"\n                                            formControlName=\"street1\" [(ngModel)]=\"street1\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('address').get('street1').invalid && personalForm.get('personal').get('address').get('street1').touched\"\n                                            class=\"errors\">Enter Street</span>\n                                        </li>\n                                        <li class=\"col-md-5\">\n                                          <input type=\"text\" placeholder=\"Street2\" maxlength=\"60\" class=\"form-control\"\n                                            formControlName=\"street2\" [(ngModel)]=\"street2\">\n\n                                        </li>\n                                      </ul>\n\n                                      <ul>\n                                        <li class=\"united-state\" style=\"float:left\">\n                                          <mat-select class=\"form-control\" placeholder=\"Country*\"\n                                            formControlName=\"country\" [(ngModel)]=\"country\">\n                                            <mat-option *ngFor=\"let data of countryArray\" [value]='data.name'>\n                                              {{data.name}}</mat-option>\n\n                                          </mat-select>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('address').get('country').invalid && personalForm.get('personal').get('address').get('country').touched\"\n                                            class=\"errors\">Enter Country</span>\n                                        </li>\n\n\n                                        <li class=\"city\">\n                                          <mat-select class=\"form-control\" placeholder=\"State *\" formControlName=\"state\"\n                                            [(ngModel)]=\"state\">\n                                            <mat-option *ngFor=\"let data of stateArray\" [value]='data.name'>\n                                              {{data.name}}</mat-option>\n                                          </mat-select>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('address').get('state').invalid && personalForm.get('personal').get('address').get('state').touched\"\n                                            class=\"errors\">Enter State</span>\n                                        </li>\n                                      </ul>\n                                      <ul>\n                                        <li class=\"state\">\n                                          <input type=\"text\" placeholder=\"City *\" maxlength=\"24\" class=\"form-control\"\n                                            formControlName=\"city\" [(ngModel)]=\"city\"\n                                            (keypress)=\"omit_special_char($event)\" onkeypress=\"return (event.charCode > 64 && \n                                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('address').get('city').invalid && personalForm.get('personal').get('address').get('city').touched\"\n                                            class=\"errors\">Enter City</span>\n                                        </li>\n\n                                        <li class=\"col-md-3  zip\">\n                                          <input placeholder=\"ZIP *\" class=\"form-control\" formControlName=\"zipcode\"\n                                            [(ngModel)]=\"zipcode\" minlength=\"6\" maxlength=\"6\"\n                                            (keypress)=\"keyPress($event)\">\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('address').get('zipcode').invalid && personalForm.get('personal').get('address').get('zipcode').touched\"\n                                            class=\"errors\">Enter Zip</span>\n                                        </li>\n\n\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n                                    </div>\n                                  </div>\n                                </div>\n                              </div>\n                            </div>\n                          </div>\n\n                          <div class=\"panel panel-default border\">\n                            <div class=\"panel-heading\">\n                              <h4 class=\"panel-title\">\n                                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                  href=\"#collapseThree\">\n                                  Contact\n                                </a>\n                              </h4>\n                            </div>\n\n                            <div id=\"collapseThree\" class=\"panel-collapse collapse in\">\n                              <div class=\"panel-body\">\n\n                                <div class=\"administration-contact\">\n                                  <div formGroupName=\"personal\">\n                                    <div formGroupName=\"contact\">\n                                      <ul>\n                                        <li>\n                                          <h4>Phone</h4>\n                                        </li>\n                                        <li class=\"col-md-4\">\n                                          <div class=\"phone\">\n                                            <span>\n                                              <img\n                                                src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\"\n                                                width=\"18\" height=\"16\" alt=\"img\">\n                                            </span>\n                                            <input class=\"form-control\" placeholder=\"Work Phone\"\n                                              formControlName=\"workPhone\" [(ngModel)]=\"workPhone\"\n                                              (keypress)=\"keyPress($event)\" minlength=10 maxlength=10>\n                                          </div>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('contact').get('workPhone').invalid && personalForm.get('personal').get('contact').get('workPhone').touched\"\n                                            class=\"errors\">Enter Work phone</span>\n                                        </li>\n                                        <li class=\"col-md-3\">\n\n                                          <input class=\"form-control\" placeholder=\"Extn\" formControlName=\"extention\"\n                                            [(ngModel)]=\"extention\" (keypress)=\"keyPress1($event)\" minlength=2\n                                            maxlength=4>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('contact').get('extention').invalid && personalForm.get('personal').get('contact').get('extention').touched\"\n                                            class=\"errors\">Enter Extn</span>\n                                        </li>\n\n                                        <li class=\"col-md-4 phone-width\">\n                                          <div class=\"phone\">\n                                            <span>\n                                              <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                                            </span>\n                                            <input class=\"form-control\" placeholder=\"Mobile Phone\"\n                                              formControlName=\"mobilePhone\" (keypress)=\"keyPress($event)\" minlength=10\n                                              maxlength=10 [(ngModel)]=\"mobilePhone\">\n                                          </div>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('contact').get('mobilePhone').invalid && personalForm.get('personal').get('contact').get('mobilePhone').touched\"\n                                            class=\"errors\">Enter Mobile phone</span>\n                                        </li>\n                                        <li class=\"col-md-4 phone-width\">\n                                          <div class=\"phone\">\n                                            <span>\n                                              <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                                            </span>\n                                            <input class=\"form-control\" placeholder=\"Home Phone\"\n                                              formControlName=\"homePhone\" mask=\"(000) 000-0000\" [(ngModel)]=\"homePhone\">\n                                          </div>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('contact').get('homePhone').invalid && personalForm.get('personal').get('contact').get('homePhone').touched\"\n                                            class=\"errors\">Enter Home phone</span>\n                                        </li>\n                                      </ul>\n\n                                      <ul>\n\n                                        <li>\n                                          <h4>Email</h4>\n                                        </li>\n\n                                        <li class=\"col-md-12 email-width\">\n\n                                          <div class=\"phone email-phone\">\n                                            <span>\n                                              <img\n                                                src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\"\n                                                width=\"18\" height=\"16\" alt=\"img\">\n                                            </span>\n                                            <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\"\n                                              formControlName=\"workMail\"\n                                              pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\"\n                                              [(ngModel)]=\"workMail\">\n\n                                          </div>\n                                          <var>Login credentials will be sent to this email</var>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('contact').get('workMail').invalid && personalForm.get('personal').get('contact').get('workMail').touched\"\n                                            class=\"errors\">Enter Work email</span>\n\n                                        </li>\n\n\n                                        <li class=\"col-md-12 email-width\">\n                                          <div class=\"phone email-phone\">\n                                            <span>\n                                              <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                                            </span>\n                                            <input type=\"email\" class=\"form-control\" placeholder=\"Home Email\"\n                                              formControlName=\"personalMail\"\n                                              pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\"\n                                              [(ngModel)]=\"personalMail\">\n                                          </div>\n                                          <span\n                                            *ngIf=\"personalForm.get('personal').get('contact').get('personalMail').invalid && personalForm.get('personal').get('contact').get('personalMail').touched\"\n                                            class=\"errors\">Enter Home email</span>\n                                        </li>\n                                      </ul>\n\n                                    </div>\n                                  </div>\n                                </div>\n                              </div>\n                            </div>\n\n                          </div>\n\n                        </div>\n                      </form>\n                    </div>\n\n                    <div class=\"wizard-popup\"></div>\n\n                    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                    <button class=\"btn pull-right save\" (click)=\"personalFieldSubmit()\">Proceed</button>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab2\" [ngClass]=\"{'active':selectedNav === 'tab2','in':selectedNav==='tab2'}\">\n                  <div class=\"personal-main\">\n                    <div class=\"personal\">\n\n                      <form [formGroup]=\"jobForm\">\n\n                        <div class=\"panel-group\" id=\"accordion4\">\n\n                          <div class=\"panel panel-default\">\n                            <div class=\"panel-heading\">\n                              <h4 class=\"panel-title\">\n                                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\"\n                                  href=\"#collapseFour\">\n                                  Job Data\n                                </a>\n                              </h4>\n                            </div>\n                            <div id=\"collapseFour\" class=\"panel-collapse collapse in\">\n                              <div class=\"panel-body\">\n\n                                <div class=\"personal-cont\">\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n\n                                      <li class=\"col-md-3\">\n                                        <label>Assign Employee ID </label>\n                                        <span class=\"form-control\">{{assignEmployee.numberType}}</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\"\n                                        *ngIf=\"assignEmployee.numberType == 'Manual' || assignEmployee.numberType == 'system Generated'\">\n                                        <label>Employee#</label>\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Employee ID\"\n                                          formControlName=\"employeeId\"\n                                          [required]=\"assignEmployee.numberType == 'Manual'\">\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('employeeId').invalid && jobForm.get('job').get('employeeId').touched\"\n                                          class=\"errors\">Enter Employee ID</span>\n                                      </li>\n\n                                      <!-- <li class=\"col-md-3\" *ngIf=\"assignEmployee.numberType == 'Manual'\">\n                                              <label>Employee#</label>\n                                              <input type=\"text\" class=\"form-control\" placeholder=\"Employee ID\" formControlName=\"employeeId\" >\n                                              <span *ngIf=\"jobForm.get('job').get('employeeId').invalid && jobForm.get('job').get('employeeId').touched\"\n                                              style=\"color:#e44a49; padding: 5px 0 0;\">Enter Employee ID</span>\n                                          </li> -->\n\n                                    </div>\n\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>Hire Date</label>\n                                        <div class=\"date\">\n                                          <input matInput [matDatepicker]=\"picker7\" placeholder=\"mm / dd / yy\"\n                                            class=\"form-control\" formControlName=\"hireDate\" [(ngModel)]=\"hireDate\"\n                                            readonly>\n                                          <mat-datepicker #picker7></mat-datepicker>\n                                          <button mat-raised-button (click)=\"picker7.open()\">\n                                            <span>\n                                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                            </span>\n                                          </button>\n                                          <div class=\"clearfix\"></div>\n                                        </div>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('hireDate').invalid && jobForm.get('job').get('hireDate').touched\"\n                                          class=\"errors\">Enter Hire date</span>\n                                      </li>\n\n                                      <li class=\"col-md-3 pa\">\n                                        <label>Employee Type </label>\n                                        <mat-select class=\"form-control\" formControlName=\"employeeType\"\n                                          [(ngModel)]=\"employeeType\" placeholder=\"Employee Type\">\n                                          <mat-option value=''>Employee Type</mat-option>\n                                          <mat-option [value]='emp.name' *ngFor=\"let emp of empType\">{{emp.name}}\n                                          </mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('employeeType').invalid && jobForm.get('job').get('employeeType').touched\"\n                                          class=\"errors\">Enter Employee type</span>\n                                      </li>\n                                      <li class=\"col-md-3\">\n                                        <label>Work Hours</label>\n\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Work Hours\"\n                                          formControlName=\"workHours\" (keypress)=\"keyPress2($event)\" maxlength=\"5\"\n                                          [(ngModel)]=\"workHours\">\n                                        <div class=\"clearfix\"></div>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('workHours').invalid && jobForm.get('job').get('workHours').touched\"\n                                          class=\"errors\">Enter Work hours</span>\n\n\n                                      </li>\n\n\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>Job Title </label>\n                                        <mat-select class=\"form-control\" formControlName=\"jobTitle\"\n                                          [(ngModel)]=\"jobTitle1\" (ngModelChange)=\"jobTitleForPayrollData()\"\n                                          placeholder=\"Job Title\">\n                                          <mat-option value=''>Job Title</mat-option>\n                                          <mat-option [value]='job._id' *ngFor=\"let job of jobTitle\">{{job.name}}\n                                          </mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('jobTitle').invalid && jobForm.get('job').get('jobTitle').touched\"\n                                          class=\"errors\">Enter Jobtitle</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Department </label>\n                                        <mat-select class=\"form-control\" formControlName=\"department\"\n                                          [(ngModel)]=\"department1\" placeholder=\"Department\">\n                                          <mat-option value=''>Department</mat-option>\n                                          <mat-option [value]=\"depart.name\" *ngFor=\"let depart of department\">\n                                            {{depart.name}}</mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('department').invalid && jobForm.get('job').get('department').touched\"\n                                          class=\"errors\">Enter Department</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>Location </label>\n                                        <mat-select class=\"form-control\" placeholder=\"Location\"\n                                          formControlName=\"location\" [(ngModel)]=\"location1\">\n                                          <mat-option value=''>Location</mat-option>\n                                          <mat-option [value]='locations.name' *ngFor=\"let locations of location\">\n                                            {{locations.name}}</mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('location').invalid && jobForm.get('job').get('location').touched\"\n                                          class=\"errors\">Enter Location</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Business Unit </label>\n                                        <mat-select class=\"form-control\" formControlName=\"businessUnit\"\n                                          [(ngModel)]=\"businessUnit\" placeholder=\"Business Unit\">\n                                          <mat-option value=\"\">Business Unit</mat-option>\n                                          <mat-option [value]=\"busines.name\" *ngFor=\"let busines of business\">\n                                            {{busines.name}}</mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('businessUnit').invalid && jobForm.get('job').get('businessUnit').touched\"\n                                          class=\"errors\">Enter Business unit</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Union Fields </label>\n                                        <mat-select class=\"form-control\" placeholder=\"Union Fields\"\n                                          formControlName=\"unionFields\" [(ngModel)]=\"unionFields\">\n                                          <mat-option value=''>Union Fileds</mat-option>\n                                          <mat-option [value]='unions.name' *ngFor=\"let unions of union\">{{unions.name}}\n                                          </mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('unionFields').invalid && jobForm.get('job').get('unionFields').touched\"\n                                          class=\"errors\">Enter Union fields</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>FLSA Code </label>\n                                        <mat-select class=\"form-control\" formControlName=\"FLSA_Code\"\n                                          [(ngModel)]=\"FLSA_Code\" placeholder=\"FLSA Code\">\n                                          <mat-option value=''>FLSA Code</mat-option>\n                                          <mat-option [value]='flsa.name' *ngFor=\"let flsa of flsaCode\">{{flsa.name}}\n                                          </mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('FLSA_Code').invalid && jobForm.get('job').get('FLSA_Code').touched\"\n                                          class=\"errors\">Enter FLSA code</span>\n                                      </li>\n\n                                      <li class=\"col-md-5\">\n                                        <label>EEO Job Category </label>\n                                        <input class=\"form-control\" formControlName=\"EEO_Job_Category\"\n                                          [(ngModel)]=\"EEO_Job_Category\" placeholder=\"EEO Job\" readonly>\n\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('EEO_Job_Category').invalid && jobForm.get('job').get('EEO_Job_Category').touched\"\n                                          class=\"errors\">Enter Job category</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>Site Access Role </label>\n                                        <mat-select class=\"form-control\" placeholder=\"Site Access\"\n                                          formControlName=\"Site_AccessRole\" [(ngModel)]=\"Site_AccessRole\"\n                                          (ngModelChange)=\"accessRoleId($event)\">\n                                          <mat-option *ngFor=\"let role of roles\" [value]='role._id'>{{role.name}}\n                                          </mat-option>\n\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('Site_AccessRole').invalid && jobForm.get('job').get('Site_AccessRole').touched\"\n                                          class=\"errors\">Enter Role</span>\n                                      </li>\n\n                                      <!-- <li class=\"col-md-3\">\n                                    <label>Type of User Role </label>\n                                    <mat-select class=\"form-control\" placeholder=\"User Role\" formControlName=\"UserRole\">\n                                      <mat-option value='12345'>Title1</mat-option>\n                                      <mat-option value='0012'>Title2</mat-option>\n                                    </mat-select>\n                                    <span *ngIf=\"jobForm.get('job').get('UserRole').invalid && jobForm.get('job').get('UserRole').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter User Role</span>\n                                  </li> -->\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>Vendor ID</label>\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Vendor ID\"\n                                          formControlName=\"VendorId\" [(ngModel)]=\"VendorId\" maxlength=\"25\">\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('VendorId').invalid && jobForm.get('job').get('VendorId').touched\"\n                                          class=\"errors\">Enter User Vendor ID</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Specific Workflow Approval </label>\n                                        <mat-select class=\"form-control\" placeholder=\"Workflow Approval\"\n                                          formControlName=\"Specific_Workflow_Approval\"\n                                          [(ngModel)]=\"Specific_Workflow_Approval\">\n                                          <mat-option value='Yes'>Yes</mat-option>\n                                          <mat-option value='No'>No</mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('Specific_Workflow_Approval').invalid && jobForm.get('job').get('Specific_Workflow_Approval').touched\"\n                                          class=\"errors\">Enter User Approval</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n                                  <ul>\n                                    <div>\n                                      <li class=\"col-md-4 skip-template \">\n                                        <mat-checkbox [(ngModel)]=\"skipReportsto\" (change)=\"skipReportsChange()\">\n                                          Skip HR Contact and Reports to</mat-checkbox>\n                                      </li>\n                                    </div>\n                                  </ul>\n\n\n                                  <ul>\n                                    <div formGroupName=\"job\">\n                                      <li class=\"col-md-3\">\n                                        <label>HR Contact </label>\n                                        <mat-select [disabled]=\"skipReportsto\" class=\"form-control\" placeholder=\"HR Contact\"\n                                          formControlName=\"HR_Contact\" [(ngModel)]=\"HR_Contact\" >\n                                          <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\">\n                                            {{data.personal.name.preferredName}}</mat-option>\n\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('HR_Contact').invalid && jobForm.get('job').get('HR_Contact').touched\"\n                                          class=\"errors\">Enter User HR contact</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Reports to </label>\n                                        <mat-select [disabled]=\"skipReportsto\"  class=\"form-control\" placeholder=\"Reports to\"\n                                          formControlName=\"ReportsTo\" [(ngModel)]=\"ReportsTo\">\n                                          <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>\n                                            {{data.personal.name.preferredName}}</mat-option>\n\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('job').get('ReportsTo').invalid && jobForm.get('job').get('ReportsTo').touched\"\n                                          class=\"errors\">Enter User Reports</span>\n                                      </li>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n\n                                </div>\n\n                              </div>\n                            </div>\n\n                          </div>\n\n                          <div class=\"panel panel-default border\">\n                            <div class=\"panel-heading\">\n                              <h4 class=\"panel-title\">\n                                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\"\n                                  href=\"#collapseFive\">\n                                  Compensation Data\n                                </a>\n                              </h4>\n                            </div>\n                            <div id=\"collapseFive\" class=\"panel-collapse collapse in\">\n                              <div class=\"panel-body\">\n\n                                <div class=\"personal-cont\">\n\n                                  <ul>\n                                    <div formGroupName=\"compensation\">\n                                      <li class=\"col-md-5 personal-dob\">\n                                        <label>Nonpaid position </label>\n                                        <mat-select class=\"form-control\" placeholder=\"Nonpaid position\"\n                                          formControlName=\"NonpaidPosition\" [(ngModel)]=\"NonpaidPosition\"\n                                          (ngModelChange)=\"nonPaidPositionChange()\">\n                                          <mat-option [value]=true>Yes</mat-option>\n                                          <mat-option [value]=false>No</mat-option>\n                                        </mat-select>\n                                        <span\n                                          *ngIf=\"jobForm.get('compensation').get('NonpaidPosition').invalid && jobForm.get('compensation').get('NonpaidPosition').touched\"\n                                          class=\"errors\">Enter Position</span>\n                                      </li>\n                                      <span *ngIf=\"NonpaidPosition == false\">\n                                        <li class=\"col-md-3\">\n                                          <label>Pay Group </label>\n                                          <mat-select class=\"form-control\" placeholder=\"Pay Group\"\n                                            formControlName=\"Pay_group\" [(ngModel)]=\"Pay_group\"\n                                            (ngModelChange)=\"payGroupChange()\">\n                                            <mat-option *ngFor=\"let data of payrollGroups\" [value]='data._id'>\n                                              {{data.payroll_group_name}}</mat-option>\n\n                                          </mat-select>\n\n                                          <!-- <tr *ngFor=\"let data of payrollGroups\">\n                                            <td>\n                                              <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                                (change)=\"showOptions($event,data._id,'paygroup')\">\n                                              </mat-checkbox>\n                                            </td>\n                                            <td>{{data.payroll_group_name}}</td>\n                                            <td>{{data.pay_frequency}}</td>\n                                            <td>{{data.pay_group_code}}</td>\n                                            <td>{{data.status}}</td>\n                                            <td></td>\n                          \n                                          </tr> -->\n\n                                          <span\n                                            *ngIf=\"jobForm.get('compensation').get('Pay_group').invalid && jobForm.get('compensation').get('Pay_group').touched\"\n                                            class=\"errors\">Enter Pay group</span>\n                                        </li>\n\n                                        <li class=\"col-md-3\">\n                                          <label>Pay Frequency </label>\n                                          <mat-select class=\"form-control\" placeholder=\"Pay Frequency\"\n                                            formControlName=\"Pay_frequency\" [(ngModel)]=\"Pay_frequency\" [disabled]=true>\n                                            <mat-option value=''>Frequency</mat-option>\n                                            <mat-option value='Weekly'>Weekly</mat-option>\n                                            <mat-option value='Bi-Weekly'>Bi-Weekly</mat-option>\n                                            <mat-option value='Semi-Monthly'>Semi-Monthly</mat-option>\n                                            <mat-option value='Monthly'>Monthly</mat-option>\n                                          </mat-select>\n                                          <span\n                                            *ngIf=\"jobForm.get('compensation').get('Pay_frequency').invalid && jobForm.get('compensation').get('Pay_frequency').touched\"\n                                            class=\"errors\">Enter Frequency</span>\n                                        </li>\n\n                                        <li class=\"col-md-4 personal-dob\">\n                                          <label>1st Pay Date</label>\n                                          <div class=\"date\">\n                                            <input matInput [matDatepicker]=\"picker9\" placeholder=\"mm / dd / yy\"\n                                              class=\"form-control\" formControlName=\"First_Pay_Date\"\n                                              [(ngModel)]=\"First_Pay_Date\" readonly>\n                                            <mat-datepicker #picker9></mat-datepicker>\n                                            <button mat-raised-button (click)=\"picker9.open()\">\n                                              <span>\n                                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                              </span>\n                                            </button>\n                                            <div class=\"clearfix\"></div>\n                                          </div>\n                                          <span\n                                            *ngIf=\"jobForm.get('compensation').get('First_Pay_Date').invalid && jobForm.get('compensation').get('First_Pay_Date').touched\"\n                                            class=\"errors\">Enter Pay date</span>\n                                        </li>\n                                      </span>\n                                    </div>\n                                  </ul>\n                                  <div class=\"clearfix\"></div>\n                                  <div *ngIf=\"NonpaidPosition == false\">\n                                    <ul formGroupName=\"compensation\">\n                                      <li class=\"col-md-3\">\n                                        <label>Salary Grade </label>\n                                        <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Grade\"\n                                      formControlName=\"Salary_Grade\" [(ngModel)]=\"Salary_Grade\"> -->\n                                        <mat-select class=\"form-control\" placeholder=\"Grade\"\n                                          formControlName=\"Salary_Grade\" [(ngModel)]=\"Salary_Grade\" [disabled]=true>\n                                          <!-- <mat-option value=''>Pay Type</mat-option> -->\n                                          <mat-option *ngFor=\"let data of salaryGradeList\" [value]='data._id'>\n                                            {{data.salary_grade_name}}</mat-option>\n                                        </mat-select>\n\n                                        <span\n                                          *ngIf=\"jobForm.get('compensation').get('Salary_Grade').invalid && jobForm.get('compensation').get('Salary_Grade').touched\"\n                                          class=\"errors\">Enter Grade</span>\n                                      </li>\n\n                                      <li class=\"col-md-3\">\n                                        <label>Salary Grade Band</label>\n                                        <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Grade\"\n                                      formControlName=\"Salary_Grade\" [(ngModel)]=\"Salary_Grade\"> -->\n                                        <mat-select class=\"form-control\" placeholder=\"Grade\"\n                                          formControlName=\"grade_band\" [(ngModel)]=\"grade_band\" [disabled]=true>\n                                          <!-- <mat-option value=''>Pay Type</mat-option> -->\n                                          <mat-option *ngFor=\"let data of salGradeBands\" [value]='data._id'>\n                                            {{data.grade}}</mat-option>\n\n                                        </mat-select>\n\n\n                                      </li>\n\n                                    </ul>\n                                  </div>\n\n                                  <div *ngIf=\"NonpaidPosition == false\">\n                                    <ul>\n                                      <div formGroupName=\"compensation\">\n                                        <div>\n                                          <span formGroupName=\"payRate\">\n                                            <li class=\"col-md-3\">\n                                              <label>Pay Type </label>\n                                              <mat-select class=\"form-control\" placeholder=\"Pay Type\"\n                                                formControlName=\"payType\" [(ngModel)]=\"payType1\"\n                                                (ngModelChange)=\"payRateChangeforCompensation()\">\n                                                <mat-option value=''>Pay Type</mat-option>\n                                                <!-- <mat-option [value]='payTypes.name' *ngFor=\"let payTypes of payType\">\n                                                {{payTypes.name}}</mat-option> -->\n                                                <mat-option value=\"Salary\">Salary</mat-option>\n                                                <mat-option value=\"Hourly\">Hourly</mat-option>\n                                              </mat-select>\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('payRate').get('payType').invalid && jobForm.get('compensation').get('payRate').get('payType').touched\"\n                                                class=\"errors\">Enter Pay type</span>\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Pay Rate </label>\n                                              <em>$</em>\n                                              <input type=\"text\" class=\"form-control payrate\" placeholder=\"Pay Rate\"\n                                                formControlName=\"amount\" [(ngModel)]=\"amount\"\n                                                (keypress)=\"keyPress($event)\" maxlength=\"6\"\n                                                (ngModelChange)=\"payRateChangeforCompensation()\">\n                                              <div class=\"clearfix\"></div>\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('payRate').get('amount').invalid && jobForm.get('compensation').get('payRate').get('amount').touched\"\n                                                class=\"errors\">Enter Pay rate</span>\n\n                                              <span *ngIf=\"compensationError \" class=\"errors\">Compensation does not fall\n                                                under the specified grade band </span>\n                                            </li>\n                                          </span>\n\n\n\n                                        </div>\n                                      </div>\n                                    </ul>\n                                    <div class=\"clearfix\"></div>\n\n\n                                    <ul>\n                                      <div formGroupName=\"compensation\">\n                                        <li class=\"col-md-3\">\n                                          <label>Compensation Ratio </label>\n                                          <input type=\"text\" class=\"form-control\" placeholder=\"Ratio\"\n                                            formControlName=\"compensationRatio\" [(ngModel)]=\"compensationRatio\"\n                                            readonly>\n\n                                        </li>\n\n\n                                      </div>\n                                    </ul>\n                                    <div class=\"clearfix\"></div>\n                                  </div>\n                                </div>\n\n                              </div>\n                            </div>\n\n                          </div>\n                        </div>\n                      </form>\n                    </div>\n                  </div>\n\n                  <div class=\"wizard-popup\"></div>\n                  <button class=\"btn pull-left back\" (click)=\"goToPersonel()\">Back</button>\n                  <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                  <button class=\"btn pull-right save\" (click)=\"jobSubmit()\">Proceed</button>\n                  <div class=\"clearfix\"></div>\n\n                </div>\n\n\n                <div class=\"tab-pane\" id=\"tab3\" [ngClass]=\"{'active':selectedNav === 'tab3','in':selectedNav==='tab3'}\">\n                  <div class=\"personal-main\">\n                    <div class=\"personal\">\n                      <form [formGroup]=\"moduleForm\">\n                        <div class=\"personal-cont\">\n\n                          <ul class=\"col-md-12\">\n\n                            <div formGroupName=\"module_settings\">\n                              <li class=\"col-md-6 personal-dob skip-temp-float\">\n                                <label>Make Eligible for Benefits? </label>\n                                <mat-select class=\"form-control\" placeholder=\"Benefits\"\n                                  formControlName=\"eligible_for_benifits\" [(ngModel)]=\"eligible_for_benifits\">\n                                  <mat-option [value]=true>Yes</mat-option>\n                                  <mat-option [value]=false>No</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm.get('module_settings').get('eligible_for_benifits').invalid && moduleForm.get('module_settings').get('eligible_for_benifits').touched\"\n                                  class=\"errors\">Enter Benefits</span>\n\n                              </li>\n                              <li class=\"col-md-3 skip-template \">\n                                <mat-checkbox formControlName=\"skip_template_assignment\"\n                                  [(ngModel)]=\"skip_template_assignment\" (change)=\"skipTemplate($event)\">Skip\n                                  onboarding-template assignment</mat-checkbox>\n                              </li>\n\n                              <li class=\"col-md-6 personal-dob\">\n                                <label>Assign Leave Management Rules now? </label>\n                                <mat-select class=\"form-control\" placeholder=\"Rules\"\n                                  formControlName=\"assign_leave_management_rules\"\n                                  [(ngModel)]=\"assign_leave_management_rules\">\n                                  <mat-option [value]=true>Yes</mat-option>\n                                  <mat-option [value]=false>No</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm.get('module_settings').get('assign_leave_management_rules').invalid && moduleForm.get('module_settings').get('assign_leave_management_rules').touched\"\n                                  class=\"errors\">Enter Rules</span>\n                              </li>\n\n                              <li class=\"col-md-6 personal-dob\">\n                                <label>Assign Performance Management now? </label>\n                                <mat-select class=\"form-control\" placeholder=\"Performance\"\n                                  formControlName=\"assign_performance_management\"\n                                  [(ngModel)]=\"assign_performance_management\">\n                                  <mat-option [value]=true>Yes</mat-option>\n                                  <mat-option [value]=false>No</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm.get('module_settings').get('assign_performance_management').invalid && moduleForm.get('module_settings').get('assign_performance_management').touched\"\n                                  class=\"errors\">Enter Performance</span>\n                              </li>\n\n                              <!-- <li class=\"col-md-6 personal-dob\">\n                                <label>How will you assign Employee Number (manual or automatic)? </label>\n                                <mat-select class=\"form-control\" placeholder=\"Employee Number\"\n                                  formControlName=\"assaign_employee_number\" [(ngModel)]=\"assaign_employee_number\">\n                                  <mat-option value='System Generated'>System Generated</mat-option>\n                                  <mat-option value='Manual'>Manual</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm.get('module_settings').get('assaign_employee_number').invalid && moduleForm.get('module_settings').get('assaign_employee_number').touched\"\n                                  style=\"color:#e44a49; padding: 5px 0 0;display: block\">Enter Number</span>\n                              </li> -->\n\n                              <li class=\"col-md-8 personal-dob\">\n                                <label>Select the Onboarding Template to apply </label>\n                                <mat-select class=\"form-control\" placeholder=\"Template\"\n                                  formControlName=\"onboarding_template\" (selectionChange)=\"onboardTemplate($event)\"\n                                  [(ngModel)]=\"onboarding_template\" [disabled]=\"skipOnboardTemplate\">\n                                  <!-- <mat-option value=''>Onboard Template</mat-option> -->\n                                  <mat-option value={{getModules._id}} *ngFor=\"let getModules of getModulesTemplates\">\n                                    {{getModules.templateName}}</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm.get('module_settings').get('onboarding_template').invalid && moduleForm.get('module_settings').get('onboarding_template').touched\"\n                                  class=\"errors\">Enter Template</span>\n                              </li>\n\n                            </div>\n                            <div class=\"col-md-6 pull-right\">\n                              <ul>\n\n                              </ul>\n                            </div>\n                          </ul>\n\n                          <div class=\"clearfix\"></div>\n\n                        </div>\n\n                      </form>\n                    </div>\n\n                    <div class=\"wizard-popup\"></div>\n                    <button class=\"btn pull-left back\" (click)=\"goToJob()\">Back</button>\n                    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                    <button class=\"btn pull-right save\" (click)=\"moduleSubmit()\">Proceed</button>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                </div>\n\n\n                <div class=\"tab-pane\" id=\"tab4\" [ngClass]=\"{'active':selectedNav === 'tab4','in':selectedNav==='tab4'}\">\n                  <div class=\"personal-cont\">\n                    <div class=\"personal\">\n\n                      <div class=\"on-board\">\n                        <label>Onboarding Template Name </label>\n                        <input type=\"text\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"template.templateName\">\n\n                        <div class=\"field-accordion\">\n\n                          <div class=\"panel-group\" id=\"accordion1\">\n\n                            <div class=\"panel panel-default border-bottom\">\n                              <div class=\"panel-heading\">\n                                <h4 class=\"panel-title\">\n                                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\"\n                                    href=\"#collapseOne\">\n                                    <span class=\"panel-title panel-font pull-left\">Onboarding Tasks </span>\n                                  </a>\n                                </h4>\n\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </div>\n                            <div id=\"collapseOne\" class=\"panel-collapse collapse in\"\n                              *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\">\n                              <p class=\"hr-tasks\">{{task}}</p>\n                              <div *ngFor=\"let data of template['Onboarding Tasks'][task]\" class=\"panel-body\">\n\n                                <p class=\"individual-hr-task\">\n                                  <span>{{data.taskName}}</span><br>\n                                  <small>{{data.taskAssignee}}-{{data.Date | date}}</small>\n                                </p>\n\n                                <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n\n                              </div>\n                            </div>\n                          </div>\n                        </div>\n\n                      </div>\n                    </div>\n\n                    <div class=\"wizard-popup\"></div>\n                    <button class=\"btn pull-left back\" (click)=\"goToModule()\">Back</button>\n                    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                    <button class=\"btn pull-right save\" (click)=\"viewOnboard()\">Proceed</button>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n                </div>\n\n\n                <div class=\"tab-pane\" id=\"tab5\">\n\n                  <div class=\"personal\">\n\n                    <div class=\"personal-cont\">\n\n                      <div class=\"benefits\">\n\n                        <ul>\n                          <li>\n                            <label>Select the Benefit Eligibility Group</label>\n                          </li>\n                          <li class=\"col-md-3\">\n                            <mat-checkbox>Full Time</mat-checkbox>\n                          </li>\n\n                          <li class=\"col-md-3\">\n                            <mat-checkbox>Part Time</mat-checkbox>\n                          </li>\n\n                          <li class=\"col-md-3\">\n                            <mat-checkbox>Retires</mat-checkbox>\n                          </li>\n                        </ul>\n                        <div class=\"clearfix\"></div>\n\n                      </div>\n\n                      <ul>\n                        <li class=\"col-md-4 personal-dob\">\n                          <label>Benefits Eligibility Group Effective Date </label>\n                          <div class=\"date\">\n                            <input matInput [matDatepicker]=\"picker10\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                            <mat-datepicker #picker10></mat-datepicker>\n                            <button mat-raised-button (click)=\"picker10.open()\">\n                              <span>\n                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                              </span>\n                            </button>\n                            <div class=\"clearfix\"></div>\n                          </div>\n                        </li>\n\n                        <li class=\"col-md-6 personal-dob\">\n                          <label>administrator makes enrollments or allow EE to make enrollments?</label>\n                          <mat-select class=\"form-control\" placeholder=\"Rules\">\n                            <mat-option value='administrator'>administrator</mat-option>\n                            <mat-option value='administrator'>administrator1</mat-option>\n                          </mat-select>\n                        </li>\n\n                        <li class=\"col-md-6 personal-dob\">\n                          <label>Apply New Hire Enrollments Settings?</label>\n                          <mat-select class=\"form-control\" placeholder=\"Performance\">\n                            <mat-option value='yes'>Yes</mat-option>\n                            <mat-option value='no'>No</mat-option>\n                          </mat-select>\n                        </li>\n\n                      </ul>\n\n                    </div>\n\n                    <hr>\n\n                    <button class=\"btn pull-left back\">Back</button>\n                    <button class=\"btn pull-left cancel\">Cancel</button>\n                    <button class=\"btn pull-right save\">Proceed</button>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab6\" [ngClass]=\"{'active':selectedNav === 'tab6','in':selectedNav==='tab6'}\">\n                  <div class=\"personal\">\n\n                    <div class=\"personal-cont\">\n\n                      <ul>\n                        <li class=\"col-md-3 veteran\">\n                          <label>Apply Position Schedule? </label>\n                          <mat-select class=\"form-control\" placeholder=\"Apply Schedule\"\n                            (selectionChange)=\"applyWorkSchedule($event)\" [(ngModel)]=\"WorkScheduleData\" name=\"work\"\n                            #schedule=\"ngModel\" required>\n                            <mat-option [value]='true'>Yes</mat-option>\n                            <mat-option value='false'>No</mat-option>\n                          </mat-select>\n                          <div *ngIf=\"!WorkScheduleData && schedule.touched || (!WorkScheduleData && isNotValid)\"\n                            class=\"errors\">Enter apply position schedule</div>\n                        </li>\n\n                      </ul>\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n\n\n                    <div *ngIf=\"leaveConditon\">\n\n                      <div class=\"personal-cont\">\n\n                        <ul>\n\n                          <li class=\"col-md-3\">\n                              <label>Schedule Type</label>\n                              <mat-select class=\"form-control\" placeholder=\"Schedule Type\"\n                                (selectionChange)=\"scheduleType($event)\" [(ngModel)]=\"workScheduleType\" name=\"type\"\n                                #type=\"ngModel\" required>\n                                <mat-option value='Standard'>Standard</mat-option>\n                                <mat-option value='Employee Specific'>Employee Specific</mat-option>\n                              </mat-select>\n                              <div *ngIf=\"!workScheduleType && type.touched || (!workScheduleType && isNotValid)\"\n                                class=\"errors\">Enter schedule type</div>\n                          </li>\n\n\n                          <li class=\"col-md-3\" *ngIf=\"positionScheduleShow\">\n                              <label>Position Schedules</label>\n                              <mat-select class=\"form-control\" placeholder=\"Work Schedule\" [(ngModel)]=\"scheduletitle\"\n                                name=\"name\" #title=\"ngModel\"  required>\n                                <mat-option [value]='work._id' *ngFor=\"let work of workSchedule\" (click)=\"getPositionSchedule(work._id)\">\n                                  {{work.scheduleTitle}}</mat-option>\n                              </mat-select>\n                              <div *ngIf=\"!scheduletitle && title.touched || (!scheduletitle && isNotValid)\"\n                                class=\"errors\">Enter postion schedule</div>\n                           \n                          </li>\n\n                          <li class=\"col-md-5\" *ngIf=\"createEmployee\">\n                            <a href=\"#\" class=\"btn\" data-toggle=\"modal\" data-target=\"#myModal5\" >Create Employee Specific\n                              Schedule</a>\n                          </li>\n\n                        </ul>\n                        <div class=\"clearfix\"></div>\n\n                      </div>\n\n                      <div class=\"leave-schedule\" *ngIf=\"positionScheduleShow\">\n                        <h3>Standard Schedule</h3>\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>Day</th>\n                              <th>Start Time</th>\n                              <th>End Time</th>\n   \n                            </tr>\n                          </thead>\n                          <tbody>\n                            <tr *ngFor=\"let schedule of scheduleData\">\n                             \n                              <td>{{schedule.day}}</td>\n                              <td>\n                                {{schedule.shiftStartTime}} \n                              </td>\n                              <td>\n                                {{schedule.shiftEndTime}} \n                              </td>\n                            \n                            </tr>\n\n                          </tbody>\n                        </table>\n\n                      </div>\n\n                      <hr>\n                    </div>\n                      <button class=\"btn pull-left back\" (click)=\"goToView()\">Back</button>\n                      <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                      <button class=\"btn pull-right save\" (click)=\"standardProceed()\">Proceed</button>\n                      <div class=\"clearfix\"></div>\n\n                   \n\n                  </div>\n                </div>\n\n\n\n                <div class=\"tab-pane\" id=\"tab7\">\n                  Tab7\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab8\" [ngClass]=\"{'active':selectedNav === 'tab8','in':selectedNav==='tab8'}\">\n                  <div class=\"tab-content\">\n\n                    <div class=\"summary-review\">\n\n                      <div class=\"tab-pane\">\n\n                        <div class=\"personal personal1\">\n\n                          <h2>Personal Fields</h2>\n\n                          <form [formGroup]=\"personalForm\">\n\n                            <div class=\"panel-group\" id=\"accordion3\">\n\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                      href=\"#collapseOne1\">\n                                      Personal\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"collapseOne1\" class=\"panel-collapse collapse in\">\n                                  <div class=\"panel-body\">\n\n                                    <div class=\"personal-cont\">\n                                      <div formGroupName=\"personal\">\n                                        <div formGroupName=\"name\">\n                                          <ul>\n                                            <li class=\"col-md-3\">\n                                              <label>First Name <span>*</span></label>\n                                              <input type=\"text\" class=\"form-control\" placeholder=\"First Name\"\n                                                formControlName=\"firstName\" [(ngModel)]=\"firstName\" readonly>\n\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Middle Name <span>*</span></label>\n                                              <input type=\"text\" class=\"form-control\" placeholder=\"Middle Name\"\n                                                formControlName=\"middleName\" [(ngModel)]=\"middleName\" readonly>\n\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Last Name <span>*</span></label>\n                                              <input type=\"text\" class=\"form-control\" placeholder=\"Last Name\"\n                                                formControlName=\"lastName\" [(ngModel)]=\"lastName\" readonly>\n\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Prefered Name <span>*</span></label>\n                                              <input type=\"text\" class=\"form-control\" placeholder=\"Preferred Name\"\n                                                formControlName=\"preferredName\" [(ngModel)]=\"preferredName\" readonly>\n\n                                            </li>\n\n                                          </ul>\n                                        </div>\n                                      </div>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"personal\">\n                                          <li class=\"col-md-4 personal-dob\">\n                                            <label>Date of Birth <span>*</span></label>\n\n                                            <div class=\"date\">\n                                              <input matInput [matDatepicker]=\"picker21\" placeholder=\"mm / dd / yy\"\n                                                class=\"form-control\" formControlName=\"dob\" [(ngModel)]=\"dob\" readonly>\n                                              <!-- <mat-datepicker #picker21></mat-datepicker>\n                                              <button mat-raised-button (click)=\"picker21.open()\">\n                                                <span>\n                                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                                </span>\n                                              </button>\n                                              <div class=\"clearfix\"></div> -->\n                                            </div>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\" readonly>\n                                            <label>Gender <span>*</span></label>\n                                            <mat-select class=\"form-control\" formControlName=\"gender\"\n                                              [(ngModel)]=\"gender\" [disabled]=\"true\">\n                                              <mat-option value=\"\">Gender</mat-option>\n                                              <mat-option value=\"male\">Male</mat-option>\n                                              <mat-option value=\"female\">Female</mat-option>\n                                            </mat-select>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"personal\">\n                                          <li class=\"col-md-3\">\n                                            <label>Marital Status <span>*</span></label>\n                                            <mat-select class=\"form-control\" formControlName=\"maritalStatus\"\n                                              [(ngModel)]=\"maritalStatus\" [disabled]=\"true\">\n                                              <mat-option *ngFor=\"let data of allMaritalStatusArray\" [value]=\"data.name\">\n                                                  {{data.name}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\" *ngIf=\"maritalStatus !='Single'\">\n                                            <label>Effective Date <span>*</span></label>\n\n                                            <div class=\"date\">\n                                              <input matInput [matDatepicker]=\"picker31\" placeholder=\"mm / dd / yy\"\n                                                class=\"form-control\" formControlName=\"effectiveDate\"\n                                                [(ngModel)]=\"effectiveDate\" readonly>\n                                              <!-- <mat-datepicker #picker31></mat-datepicker>\n                                              <button mat-raised-button (click)=\"picker31.open()\">\n                                                <span>\n                                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                                </span>\n                                              </button> -->\n                                              <div class=\"clearfix\"></div>\n                                            </div>\n\n                                          </li>\n\n\n                                          <li class=\"col-md-3 veteran\">\n                                            <label>Veteran Status <span>*</span></label>\n                                            <mat-select class=\"form-control\" formControlName=\"veteranStatus\"\n                                              [(ngModel)]=\"veteranStatus\" [disabled]=\"true\">\n                                              <mat-option *ngFor=\"let data of allVeteranStatusArray\" [value]=\"data.name\">\n                                                  {{data.name}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3 veteran\">\n                                            <label>SSN <span>*</span></label>\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"SSN\"\n                                              formControlName=\"ssn\" [(ngModel)]=\"ssn\" readonly>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"personal\">\n                                          <li class=\"col-md-5\">\n                                            <label>Ethnicity <span>*</span></label>\n                                            <mat-select class=\"form-control\" formControlName=\"ethnicity\"\n                                              [(ngModel)]=\"ethnicity\" [disabled]=\"true\">\n                                              <mat-option *ngFor=\"let data of ethnicityArray\" [value]=\"data.name\">\n                                                  {{data.name}}</mat-option>\n      \n                                            </mat-select>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                    </div>\n\n                                    <div class=\"clearfix\"></div>\n\n                                  </div>\n                                </div>\n\n                              </div>\n\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                      href=\"#collapseTwo2\">\n                                      Address\n                                    </a>\n                                  </h4>\n\n                                </div>\n\n                                <div id=\"collapseTwo2\" class=\"panel-collapse collapse in\">\n                                  <div class=\"panel-body\">\n\n                                    <div class=\"administration-address\">\n                                      <div formGroupName=\"personal\">\n                                        <div formGroupName=\"address\">\n                                          <ul>\n                                            <li class=\"col-md-5\">\n                                              <input type=\"text\" placeholder=\"Street1 *\" class=\"form-control\"\n                                                formControlName=\"street1\" [(ngModel)]=\"street1\" readonly>\n                                              <span\n                                                *ngIf=\"personalForm.get('personal').get('address').get('street1').invalid && personalForm.get('personal').get('address').get('street1').touched\">Enter\n                                                Street</span>\n                                            </li>\n                                            <li class=\"col-md-5\">\n                                              <input type=\"text\" placeholder=\"Street2\" class=\"form-control\"\n                                                formControlName=\"street2\" [(ngModel)]=\"street2\" readonly>\n\n                                            </li>\n                                          </ul>\n\n                                          <ul>\n                                            <li class=\"state\">\n                                              <input type=\"text\" placeholder=\"City *\" class=\"form-control\"\n                                                formControlName=\"city\" [(ngModel)]=\"city\" readonly>\n\n                                            </li>\n                                            <li class=\"city\">\n                                                <mat-select class=\"form-control\" placeholder=\"State *\" formControlName=\"state\"\n                                                  [(ngModel)]=\"state\" [disabled]=\"true\">\n                                                  <mat-option *ngFor=\"let data of stateArray\" [value]='data.name'>\n                                                    {{data.name}}</mat-option>\n                                                </mat-select>\n                                               \n                                              </li>\n\n                                            <li class=\"zip\">\n                                              <input type=\"text\" placeholder=\"ZIP *\" class=\"form-control\"\n                                                formControlName=\"zipcode\" [(ngModel)]=\"zipcode\" readonly>\n\n                                            </li>\n\n                                            <li class=\"col-md-3 united-state\">\n                                              <mat-select class=\"form-control\" placeholder=\"Country\"\n                                                formControlName=\"country\" [(ngModel)]=\"country\" [disabled]=\"true\">\n                                                <mat-option *ngFor=\"let data of countryArray\" [value]='data.name'>\n                                                  {{data.name}}</mat-option>\n                                              </mat-select>\n\n                                            </li>\n                                          </ul>\n                                          <div class=\"clearfix\"></div>\n                                        </div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>\n                              </div>\n\n                              <div class=\"panel panel-default border\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                      href=\"#collapseThree3\">\n                                      Contact\n                                    </a>\n                                  </h4>\n                                </div>\n\n                                <div id=\"collapseThree3\" class=\"panel-collapse collapse in\">\n                                  <div class=\"panel-body\">\n\n                                    <div class=\"administration-contact\">\n                                      <div formGroupName=\"personal\">\n                                        <div formGroupName=\"contact\">\n                                          <ul>\n                                            <li>\n                                              <h4>Phone</h4>\n                                            </li>\n                                            <li class=\"col-md-4\">\n                                              <div class=\"phone\">\n                                                <span>\n                                                  <img\n                                                    src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\"\n                                                    width=\"18\" height=\"16\" alt=\"img\">\n                                                </span>\n                                                <input type=\"number\" class=\"form-control\" placeholder=\"Work Phone\"\n                                                  formControlName=\"workPhone\" (keypress)=\"keyPress($event)\" minlength=10\n                                                  maxlength=10 [(ngModel)]=\"workPhone\" readonly>\n                                              </div>\n\n                                            </li>\n                                            <li class=\"col-md-2\">\n\n                                              <input type=\"number\" class=\"form-control\" placeholder=\"Extn\"\n                                                formControlName=\"extention\" [(ngModel)]=\"extention\" readonly>\n\n                                            </li>\n\n                                            <li class=\"col-md-4 phone-width\">\n                                              <div class=\"phone\">\n                                                <span>\n                                                  <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                                                </span>\n                                                <input type=\"number\" class=\"form-control\" placeholder=\"Mobile Phone\"\n                                                  formControlName=\"mobilePhone\" (keypress)=\"keyPress($event)\"\n                                                  minlength=10 maxlength=10 [(ngModel)]=\"mobilePhone\" readonly>\n                                              </div>\n\n                                            </li>\n                                            <li class=\"col-md-4 phone-width\">\n                                              <div class=\"phone\">\n                                                <span>\n                                                  <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                                                </span>\n                                                <input type=\"number\" class=\"form-control\" placeholder=\"Home Phone\"\n                                                  formControlName=\"homePhone\" (keypress)=\"keyPress($event)\" minlength=10\n                                                  maxlength=10 [(ngModel)]=\"homePhone\" readonly>\n                                              </div>\n\n                                            </li>\n                                          </ul>\n\n                                          <ul>\n\n                                            <li>\n                                              <h4>Email</h4>\n                                            </li>\n\n                                            <li class=\"col-md-5 email-width\">\n                                              <div class=\"phone\">\n                                                <span>\n                                                  <img\n                                                    src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\"\n                                                    width=\"18\" height=\"16\" alt=\"img\">\n                                                </span>\n                                                <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\"\n                                                  formControlName=\"workMail\"\n                                                  pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\"\n                                                  [(ngModel)]=\"workMail\" readonly>\n                                              </div>\n\n                                            </li>\n\n                                            <li class=\"col-md-5 email-width\">\n                                              <div class=\"phone\">\n                                                <span>\n                                                  <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                                                </span>\n                                                <input type=\"email\" class=\"form-control\" placeholder=\"Home Email\"\n                                                  formControlName=\"personalMail\"\n                                                  pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\"\n                                                  [(ngModel)]=\"personalMail\" readonly>\n                                              </div>\n\n                                            </li>\n                                          </ul>\n\n                                        </div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>\n\n                              </div>\n\n                            </div>\n                          </form>\n\n                        </div>\n\n                      </div>\n\n                      <div class=\"tab-pane\">\n\n                        <div class=\"personal personal1\">\n\n                          <h2>Job & Compensation</h2>\n\n                          <form [formGroup]=\"jobForm\">\n\n                            <div class=\"panel-group\" id=\"accordion4\">\n\n                              <div class=\"panel panel-default\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\"\n                                      href=\"#collapseFour1\">\n                                      Job Data\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"collapseFour1\" class=\"panel-collapse collapse in\">\n                                  <div class=\"panel-body\">\n\n                                    <div class=\"personal-cont\">\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n\n                                          <li class=\"col-md-3\">\n                                            <label>Assign Employee ID </label>\n                                            <span class=\"form-control\">{{assignEmployee.numberType}}</span>\n                                          </li>\n\n                                          <li class=\"col-md-3\"\n                                            *ngIf=\"assignEmployee.numberType == 'Manual' && assignEmployee.numberType == 'system Generated'\">\n                                            <label>Employee#</label>\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Employee ID\"\n                                              formControlName=\"employeeId\"\n                                              [required]=\"assignEmployee.numberType == 'Manual'\">\n                                            <span\n                                              *ngIf=\"jobForm.get('job').get('employeeId').invalid && jobForm.get('job').get('employeeId').touched\">Enter\n                                              Employee ID</span>\n                                          </li>\n\n\n                                          <!-- <li class=\"col-md-3\" *ngIf=\"assignEmployee.numberType == 'system Generated'\">\n                                            <label>Employee#</label>\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Employee ID\"\n                                              formControlName=\"employeeId\"\n                                              [required]=\"assignEmployee.numberType == 'system Generated'\">\n                                            <span\n                                              *ngIf=\"jobForm.get('job').get('employeeId').invalid && jobForm.get('job').get('employeeId').touched\"\n                                              style=\"color:#e44a49; padding: 5px 0 0;\">Enter Employee ID</span>\n                                          </li> -->\n\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>Employee Type </label>\n                                            <mat-select class=\"form-control\" formControlName=\"employeeType\"\n                                              [(ngModel)]=\"employeeType\" [disabled]=\"true\">\n                                              <mat-option value=''>Employee Type</mat-option>\n                                              <mat-option [value]='emp.name' *ngFor=\"let emp of empType\">{{emp.name}}\n                                              </mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\">\n                                            <label>Hire Date</label>\n\n                                            <div class=\"date\">\n                                              <input matInput [matDatepicker]=\"picker71\" placeholder=\"mm / dd / yy\"\n                                                class=\"form-control\" formControlName=\"hireDate\" [(ngModel)]=\"hireDate\"\n                                                readonly>\n                                              <!-- <mat-datepicker #picker71></mat-datepicker>\n                                              <button mat-raised-button (click)=\"picker71.open()\">\n                                                <span>\n                                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                                </span>\n                                              </button> -->\n                                              <div class=\"clearfix\"></div>\n                                            </div>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>Job Title </label>\n                                            <mat-select class=\"form-control\" formControlName=\"jobTitle\"\n                                              [(ngModel)]=\"jobTitle1\" (ngModelChange)=\"jobTitleForPayrollData()\"\n                                              placeholder=\"Job Title\">\n                                              <mat-option value=''>Job Title</mat-option>\n                                              <mat-option [value]='job._id' *ngFor=\"let job of jobTitle\">{{job.name}}\n                                              </mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\">\n                                            <label>Department </label>\n                                            <mat-select class=\"form-control\" formControlName=\"department\"\n                                              [(ngModel)]=\"department1\" [disabled]=\"true\">\n                                              <mat-option value=''>Department</mat-option>\n                                              <mat-option [value]=\"depart.name\" *ngFor=\"let depart of department\">\n                                                {{depart.name}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>Location </label>\n                                            <mat-select class=\"form-control\" placeholder=\"Location\"\n                                              formControlName=\"location\" [(ngModel)]=\"location1\" [disabled]=\"true\">\n                                              <mat-option value=''>Location</mat-option>\n                                              <mat-option [value]='locations.name' *ngFor=\"let locations of location\">\n                                                {{locations.name}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\">\n                                            <label>Business Unit </label>\n                                            <mat-select class=\"form-control\" formControlName=\"businessUnit\"\n                                              [(ngModel)]=\"businessUnit\" [disabled]=\"true\">\n                                              <mat-option value=\"\">Business Unit</mat-option>\n                                              <mat-option [value]=\"busines.name\" *ngFor=\"let busines of business\">\n                                                {{busines.name}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\">\n                                            <label>Union Fileds </label>\n                                            <mat-select class=\"form-control\" placeholder=\"Union Fileds\"\n                                              formControlName=\"unionFields\" [(ngModel)]=\"unionFields\" [disabled]=\"true\">\n                                              <mat-option value=''>Union Fileds</mat-option>\n                                              <mat-option [value]='unions.name' *ngFor=\"let unions of union\">\n                                                {{unions.name}}\n                                              </mat-option>\n                                            </mat-select>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>FLSA Code </label>\n                                            <mat-select class=\"form-control\" formControlName=\"FLSA_Code\"\n                                              [(ngModel)]=\"FLSA_Code\" [disabled]=\"true\">\n                                              <mat-option value=''>FLSA Code</mat-option>\n                                              <mat-option [value]='flsa.name' *ngFor=\"let flsa of flsaCode\">\n                                                {{flsa.name}}\n                                              </mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-5\">\n                                            <label>EEO Job Category </label>\n\n                                            <input class=\"form-control\" formControlName=\"EEO_Job_Category\"\n                                          [(ngModel)]=\"EEO_Job_Category\" placeholder=\"EEO Job\" readonly>\n<!-- \n                                            <mat-select class=\"form-control\" formControlName=\"EEO_Job_Category\"\n                                              [(ngModel)]=\"EEO_Job_Category\" [disabled]=\"true\">\n                                              <mat-option *ngFor=\"let eeo of eeoJobCategoryArray\" [value]='eeo.code'>\n                                                {{eeo.name}}\n                                              </mat-option>\n                                            </mat-select> -->\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>Site Access Role </label>\n\n                                          <mat-select class=\"form-control\" placeholder=\"Site Access\"\n                                          formControlName=\"Site_AccessRole\" [(ngModel)]=\"Site_AccessRole\"\n                                          [disabled]=\"true\">\n                                          <mat-option *ngFor=\"let role of roles\" [value]='role._id'>{{role.name}}\n                                          </mat-option>\n\n                                        </mat-select>\n\n                                          </li>\n\n                                          <!-- <li class=\"col-md-3\">\n                                          <label>Type of User Role </label>\n                                          <mat-select class=\"form-control\" placeholder=\"User Role\" formControlName=\"UserRole\">\n                                            <mat-option value='12345'>Title1</mat-option>\n                                            <mat-option value='0012'>Title2</mat-option>\n                                          </mat-select>\n                                          <span *ngIf=\"jobForm.get('job').get('UserRole').invalid && jobForm.get('job').get('UserRole').touched\" style=\"color:#e44a49; padding: 5px 0 0;\">Enter User Role</span>\n                                        </li> -->\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>Vendor ID</label>\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Vendor ID\"\n                                              formControlName=\"VendorId\" [(ngModel)]=\"VendorId\" readonly>\n\n                                          </li>\n\n                                          <li class=\"col-md-4\">\n                                            <label>Specific Workflow Approval </label>\n                                            <mat-select class=\"form-control\" placeholder=\"Workflow Approval\"\n                                              formControlName=\"Specific_Workflow_Approval\"\n                                              [(ngModel)]=\"Specific_Workflow_Approval\" [disabled]=\"true\">\n                                              <mat-option value='Yes'>Yes</mat-option>\n                                              <mat-option value='No'>No</mat-option>\n                                            </mat-select>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n\n                                      <ul>\n                                        <div formGroupName=\"job\">\n                                          <li class=\"col-md-3\">\n                                            <label>HR Contact </label>\n                                            <mat-select class=\"form-control\" placeholder=\"HR Contact\"\n                                              formControlName=\"HR_Contact\" [(ngModel)]=\"HR_Contact\" [disabled]=\"true\">\n                                              <mat-option *ngFor=\"let data of hrData\" [value]=\"data._id\">\n                                                  {{data.personal.name.preferredName}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n\n                                          <li class=\"col-md-3\">\n                                            <label>Reports to </label>\n                                            <mat-select class=\"form-control\" placeholder=\"Reports to\"\n                                              formControlName=\"ReportsTo\" [(ngModel)]=\"ReportsTo\" [disabled]=\"true\">\n                                              <mat-option *ngFor=\"let data of employeeData\" [value]='data._id'>\n                                                {{data.personal.name.preferredName}}</mat-option>\n                                            </mat-select>\n\n                                          </li>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n                                    </div>\n\n                                  </div>\n                                </div>\n\n                              </div>\n\n\n                              <div class=\"panel panel-default border\">\n                                <div class=\"panel-heading\">\n                                  <h4 class=\"panel-title\">\n                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\"\n                                      href=\"#collapseFive\">\n                                      Compensation Data\n                                    </a>\n                                  </h4>\n                                </div>\n                                <div id=\"collapseFive\" class=\"panel-collapse collapse in\">\n                                  <div class=\"panel-body\">\n\n                                    <div class=\"personal-cont\">\n\n                                      <ul>\n                                        <div formGroupName=\"compensation\">\n                                          <li class=\"col-md-5 personal-dob\">\n                                            <label>Nonpaid position </label>\n                                            <mat-select class=\"form-control\" placeholder=\"Nonpaid position\"\n                                              formControlName=\"NonpaidPosition\" [(ngModel)]=\"NonpaidPosition\" [disabled]=\"true\">\n                                              <mat-option [value]=true>Yes</mat-option>\n                                              <mat-option [value]=false>No</mat-option>\n                                            </mat-select>\n                                            <span\n                                              *ngIf=\"jobForm.get('compensation').get('NonpaidPosition').invalid && jobForm.get('compensation').get('NonpaidPosition').touched\"\n                                              class=\"errors\">Enter Position</span>\n                                          </li>\n                                          <span *ngIf=\"NonpaidPosition == 'false'\">\n                                            <li class=\"col-md-3\">\n                                              <label>Pay Group </label>\n                                              <mat-select class=\"form-control\" placeholder=\"Pay Group\"\n                                                formControlName=\"Pay_group\" [(ngModel)]=\"Pay_group\"\n                                                (ngModelChange)=\"payGroupChange()\">\n                                                <mat-option *ngFor=\"let data of payrollGroups\" [value]='data._id'>\n                                                  {{data.payroll_group_name}}</mat-option>\n\n                                              </mat-select>\n\n                                              <!-- <tr *ngFor=\"let data of payrollGroups\">\n                                                <td>\n                                                  <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                                    (change)=\"showOptions($event,data._id,'paygroup')\">\n                                                  </mat-checkbox>\n                                                </td>\n                                                <td>{{data.payroll_group_name}}</td>\n                                                <td>{{data.pay_frequency}}</td>\n                                                <td>{{data.pay_group_code}}</td>\n                                                <td>{{data.status}}</td>\n                                                <td></td>\n                              \n                                              </tr> -->\n\n\n\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('Pay_group').invalid && jobForm.get('compensation').get('Pay_group').touched\"\n                                                class=\"errors\">Enter Pay group</span>\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Pay Frequency </label>\n                                              <mat-select class=\"form-control\" placeholder=\"Pay Frequency\"\n                                                formControlName=\"Pay_frequency\" [(ngModel)]=\"Pay_frequency\"\n                                                [disabled]=true>\n                                                <mat-option value=''>Frequency</mat-option>\n                                                <mat-option value='Weekly'>Weekly</mat-option>\n                                                <mat-option value='Bi-Weekly'>Bi-Weekly</mat-option>\n                                                <mat-option value='Semi-Monthly'>Semi-Monthly</mat-option>\n                                                <mat-option value='Monthly'>Monthly</mat-option>\n                                              </mat-select>\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('Pay_frequency').invalid && jobForm.get('compensation').get('Pay_frequency').touched\"\n                                                class=\"errors\">Enter Frequency</span>\n                                            </li>\n\n                                            <li class=\"col-md-4 personal-dob\">\n                                              <label>1st Pay Date</label>\n                                              <div class=\"date\">\n                                                <input matInput [matDatepicker]=\"picker9\" placeholder=\"mm / dd / yy\"\n                                                  class=\"form-control\" formControlName=\"First_Pay_Date\"\n                                                  [(ngModel)]=\"First_Pay_Date\" readonly>\n                                                <!-- <mat-datepicker #picker91></mat-datepicker>\n                                                <button mat-raised-button (click)=\"picker91.open()\">\n                                                  <span>\n                                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                                  </span>\n                                                </button> -->\n                                                <div class=\"clearfix\"></div>\n                                              </div>\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('First_Pay_Date').invalid && jobForm.get('compensation').get('First_Pay_Date').touched\"\n                                                class=\"errors\">Enter Pay date</span>\n                                            </li>\n                                          </span>\n                                        </div>\n                                      </ul>\n                                      <div class=\"clearfix\"></div>\n\n                                      <span *ngIf=\"NonpaidPosition == 'false'\">\n                                        <ul>\n                                          <div formGroupName=\"compensation\">\n                                            <div formGroupName=\"payRate\">\n                                              <li class=\"col-md-3\">\n                                                <label>Pay Rate </label>\n                                                <em>$</em>\n                                                <input type=\"text\" class=\"form-control payrate\" placeholder=\"Pay Rate\"\n                                                  formControlName=\"amount\" [(ngModel)]=\"amount\">\n                                                <div class=\"clearfix\"></div>\n                                                <span\n                                                  *ngIf=\"jobForm.get('compensation').get('payRate').get('amount').invalid && jobForm.get('compensation').get('payRate').get('amount').touched\"\n                                                  class=\"errors\">Enter Pay rate</span>\n                                              </li>\n\n                                              <li class=\"col-md-3\">\n                                                <label>Pay Type </label>\n                                                <mat-select class=\"form-control\" placeholder=\"Pay Type\"\n                                                  formControlName=\"payType\" [(ngModel)]=\"payType1\">\n                                                  <mat-option value=''>Pay Type</mat-option>\n                                                  <mat-option [value]='payTypes.name' *ngFor=\"let payTypes of payType\">\n                                                    {{payTypes.name}}</mat-option>\n                                                </mat-select>\n                                                <span\n                                                  *ngIf=\"jobForm.get('compensation').get('payRate').get('payType').invalid && jobForm.get('compensation').get('payRate').get('payType').touched\"\n                                                  class=\"errors\">Enter Pay type</span>\n                                              </li>\n                                            </div>\n                                          </div>\n                                        </ul>\n                                        <div class=\"clearfix\"></div>\n\n\n                                        <ul>\n                                          <div formGroupName=\"compensation\">\n                                            <li class=\"col-md-3\">\n                                              <label>Compensation Ratio </label>\n                                              <input type=\"text\" class=\"form-control\" placeholder=\"Ratio\"\n                                                formControlName=\"compensationRatio\" [(ngModel)]=\"compensationRatio\">\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('compensationRatio').invalid && jobForm.get('compensation').get('compensationRatio').touched\"\n                                                class=\"errors\">Enter Ratio</span>\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Salary Grade </label>\n                                              <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Grade\"\n                                                formControlName=\"Salary_Grade\" [(ngModel)]=\"Salary_Grade\"> -->\n                                              <mat-select class=\"form-control\" placeholder=\"Grade\"\n                                                formControlName=\"Salary_Grade\" [(ngModel)]=\"Salary_Grade\"\n                                                [disabled]=true>\n                                                <!-- <mat-option value=''>Pay Type</mat-option> -->\n                                                <mat-option *ngFor=\"let data of salaryGradeList\" [value]='data._id'>\n                                                  {{data.salary_grade_name}}</mat-option>\n                                              </mat-select>\n\n                                              <span\n                                                *ngIf=\"jobForm.get('compensation').get('Salary_Grade').invalid && jobForm.get('compensation').get('Salary_Grade').touched\"\n                                                class=\"errors\">Enter Grade</span>\n                                            </li>\n\n                                            <li class=\"col-md-3\">\n                                              <label>Salary Grade Band</label>\n                                              <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Grade\"\n                                                formControlName=\"Salary_Grade\" [(ngModel)]=\"Salary_Grade\"> -->\n                                              <mat-select class=\"form-control\" placeholder=\"Grade\"\n                                                formControlName=\"grade_band\" [(ngModel)]=\"grade_band\" [disabled]=true>\n                                                <!-- <mat-option value=''>Pay Type</mat-option> -->\n                                                <mat-option *ngFor=\"let data of salGradeBands\" [value]='data._id'>\n                                                  {{data.grade}}</mat-option>\n\n                                              </mat-select>\n\n\n                                            </li>\n                                          </div>\n                                        </ul>\n                                        <div class=\"clearfix\"></div>\n                                      </span>\n                                    </div>\n\n                                  </div>\n                                </div>\n\n                              </div>\n                            </div>\n                          </form>\n\n                        </div>\n\n                      </div>\n\n                      <div class=\"tab-pane\">\n\n                        <div class=\"personal personal1\">\n\n                          <h2>Module Settings</h2>\n\n                          <form [formGroup]=\"moduleForm\">\n                            <div class=\"personal-cont\">\n\n                              <ul>\n                                <div formGroupName=\"module_settings\">\n                                  <li class=\"col-md-6 personal-dob skip-temp-float\">\n                                    <label>Make Eligible for Benefits? </label>\n                                    <mat-select class=\"form-control\" placeholder=\"Benefits\"\n                                      formControlName=\"eligible_for_benifits\" [(ngModel)]=\"eligible_for_benifits\"\n                                      [disabled]=\"true\">\n                                      <mat-option [value]='true'>Yes</mat-option>\n                                      <mat-option [value]='false'>No</mat-option>\n                                    </mat-select>\n\n\n                                  </li>\n                                  <li class=\"col-md-3 skip-template \">\n                                    <mat-checkbox formControlName=\"skip_template_assignment\"\n                                      [(ngModel)]=\"skip_template_assignment\" (change)=\"skipTemplate($event)\"\n                                      [disabled]=true>Skip onboarding-template assignment</mat-checkbox>\n                                  </li>\n\n                                  <li class=\"col-md-6 personal-dob\">\n                                    <label>Assign Leave Management Rules now? </label>\n                                    <mat-select class=\"form-control\" placeholder=\"Rules\"\n                                      formControlName=\"assign_leave_management_rules\"\n                                      [(ngModel)]=\"assign_leave_management_rules\" [disabled]=\"true\">\n                                      <mat-option [value]='true'>Yes</mat-option>\n                                      <mat-option [value]='false'>No</mat-option>\n                                    </mat-select>\n\n                                  </li>\n\n                                  <li class=\"col-md-6 personal-dob\">\n                                    <label>Assign Performance Management now? </label>\n                                    <mat-select class=\"form-control\" placeholder=\"Performance\"\n                                      formControlName=\"assign_performance_management\"\n                                      [(ngModel)]=\"assign_performance_management\" [disabled]=\"true\">\n                                      <mat-option [value]='true'>Yes</mat-option>\n                                      <mat-option [value]='false'>No</mat-option>\n                                    </mat-select>\n\n                                  </li>\n\n                                  <!-- <li class=\"col-md-6 personal-dob\">\n                                    <label>How will you assign Employee Number (manual or automatic)? </label>\n                                    <mat-select class=\"form-control\" placeholder=\"Employee Number\"\n                                      formControlName=\"assaign_employee_number\" [(ngModel)]=\"assaign_employee_number\"\n                                      [disabled]=\"true\">\n                                      <mat-option value='System Generated'>System Generated</mat-option>\n                                      <mat-option value='Manual'>Manual</mat-option>\n                                    </mat-select>\n\n                                  </li> -->\n\n                                  <li class=\"col-md-8 personal-dob\">\n                                    <label>Select the Onboarding Template to apply </label>\n                                    <mat-select class=\"form-control\" placeholder=\"Template\"\n                                      formControlName=\"onboarding_template\" (selectionChange)=\"onboardTemplate($event)\"\n                                      [(ngModel)]=\"onboarding_template\" [disabled]=\"true\">\n                                      <!-- <mat-option value=''>Onboard Template</mat-option> -->\n                                      <mat-option value={{getModules._id}}\n                                        *ngFor=\"let getModules of getModulesTemplates\">\n                                        {{getModules.templateName}}</mat-option>\n                                    </mat-select>\n\n                                  </li>\n\n                                </div>\n                              </ul>\n\n                              <div class=\"clearfix\"></div>\n\n                            </div>\n\n                          </form>\n\n                        </div>\n                      </div>\n\n                      <div class=\"tab-pane\">\n\n                        <div class=\"personal personal1\">\n\n                          <h2>View Onboarding Task</h2>\n\n                          <div class=\"on-board\">\n                            <label>Onboarding Template Name </label>\n                            <input type=\"text\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"template.templateName\">\n\n                            <div class=\"field-accordion\">\n\n                              <div class=\"panel-group\" id=\"accordion1\">\n\n                                <div class=\"panel panel-default border-bottom\">\n                                  <div class=\"panel-heading\">\n                                    <h4 class=\"panel-title\">\n                                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\"\n                                        href=\"#collapseOne\">\n                                        <span class=\"panel-title panel-font pull-left\">Onboarding Tasks </span>\n                                      </a>\n                                    </h4>\n\n                                    <div class=\"clearfix\"></div>\n                                  </div>\n                                </div>\n                                <div id=\"collapseOne\" class=\"panel-collapse collapse in\"\n                                  *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\">\n                                  <p class=\"hr-tasks\">{{task}}</p>\n                                  <div *ngFor=\"let data of template['Onboarding Tasks'][task]\" class=\"panel-body\">\n\n                                    <p class=\"individual-hr-task\">\n                                      <span>{{data.taskName}}</span><br>\n                                      <small>{{data.taskAssignee}}-{{data.Date | date}}</small>\n                                    </p>\n\n                                    <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n\n                                  </div>\n                                </div>\n                              </div>\n                            </div>\n\n                          </div>\n\n                        </div>\n                      </div>\n\n                      <div class=\"tab-pane\">\n\n                        <div class=\"personal\">\n\n                          <h2>Leave Management</h2>\n\n                          <div class=\"personal-cont\">\n\n                            <ul>\n                              <li class=\"col-md-3 veteran\">\n                                <label>Apply Position Schedule? </label>\n                                <mat-select class=\"form-control\" placeholder=\"Apply Schedule\"\n                                  (selectionChange)=\"applyWorkSchedule($event)\" [(ngModel)]=\"WorkScheduleData\" [disabled]=true>\n                                  <mat-option [value]='true'>Yes</mat-option>\n                                  <mat-option [value]='false'>No</mat-option>\n                                </mat-select>\n                              </li>\n\n                            </ul>\n                            <div class=\"clearfix\"></div>\n\n                          </div>\n\n\n                          <div *ngIf=\"leaveConditon\">\n\n                            <div class=\"personal-cont\">\n\n                              <ul>\n\n                                <li class=\"col-md-3\">\n                                  <label>Schedule Type</label>\n                                  <mat-select class=\"form-control\" placeholder=\"Schedule Type\"\n                                    (selectionChange)=\"scheduleType($event)\" [(ngModel)]=\"workScheduleType\" name=\"type\"\n                                    #type=\"ngModel\" required>\n                                    <mat-option value='Standard'>Standard</mat-option>\n                                    <mat-option value='Employee Specific'>Employee Specific</mat-option>\n                                  </mat-select>\n                                  <div *ngIf=\"!workScheduleType && type.touched || (!workScheduleType && isNotValid)\"\n                                    class=\"errors\">Enter schedule type</div>\n                              </li>\n    \n    \n                              <li class=\"col-md-3\" *ngIf=\"positionScheduleShow\">\n                                  <label>Position Schedules</label>\n                                  <mat-select class=\"form-control\" placeholder=\"Work Schedule\" [(ngModel)]=\"scheduletitle\"\n                                    name=\"name\" #title=\"ngModel\"  required>\n                                    <mat-option [value]='work._id' *ngFor=\"let work of workSchedule\" (click)=\"getPositionSchedule(work._id)\">\n                                      {{work.scheduleTitle}}</mat-option>\n                                  </mat-select>\n                                  <div *ngIf=\"!scheduletitle && title.touched || (!scheduletitle && isNotValid)\"\n                                    class=\"errors\">Enter postion schedule</div>\n                               \n                              </li>\n\n                                <li class=\"col-md-5\" *ngIf=\"createEmployee\">\n                                  <button type=\"submit\" class=\"btn\" [disabled]=\"true\">Create Employee Specific\n                                    Schedule</button>\n                                </li>\n\n                              </ul>\n                              <div class=\"clearfix\"></div>\n\n                            </div>\n\n                            <div class=\"leave-schedule\" *ngIf=\"positionScheduleShow\">\n                              <h3>Standard Schedule</h3>\n                              <table class=\"table\">\n                                <thead>\n                                  <tr>\n                                   \n                                    <th>Day</th>\n                                    <th>Start Time</th>\n                                    <th>End Time</th>\n                                   \n                                  </tr>\n                                </thead>\n                                <tbody>\n                                  <tr *ngFor=\"let schedule of scheduleData\">\n                                   \n                                    <td>{{schedule.day}}</td>\n                                    <td>\n                                      {{schedule.shiftStartTime}} \n                                    </td>\n                                    <td>\n                                      {{schedule.shiftEndTime}}\n                                    </td>\n                                    \n                                  </tr>\n\n                                </tbody>\n                              </table>\n\n                            </div>\n\n\n                          </div>\n\n                        </div>\n\n                      </div>\n\n                    </div>\n\n                    <div class=\"wizard-popup\"></div>\n                    <button class=\"btn pull-left back\" (click)=\"goToOnboard()\">Back</button>\n                    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                    <button class=\"btn pull-right save\" (click)=\"standardSummarySubmit()\">Submit</button>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n                </div>\n\n              </div>\n\n            </div>\n            <div class=\"clearfix\"></div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n\n\n\n<!-- Author:Suresh M, Date:02-05-19  -->\n<!-- Custom New Hire & Standard Wizard -->\n\n\n\n<!-- Author:Suresh M, Date:19-04-19  -->\n<!-- Custom New Hire HR Popup -->\n\n<div class=\"new-wizard\">\n\n  <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">New Hire Wizard</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"wizard-tabs\">\n\n            <div class=\"wizard-lft col-md-3\">\n              <ul class=\"nav nav-tabs tabs-left\">\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNewHire == 'ClientInfo1' }\">\n                  <a data-toggle=\"tab\" #client_info1 (click)=\"chooseSteps('ClientInfo1')\">Choose\n                    Steps</a></li>\n                <li>\n                  <a data-toggle=\"tab\" (click)=\"customNewHire?'':chooseSteps('tab12')\">Benefits</a></li>\n                <li><a data-toggle=\"tab\" (click)=\"customNewHire?'':chooseSteps('tab13')\">Leave Management</a></li>\n                <li><a data-toggle=\"tab\" (click)=\"customNewHire?'':chooseSteps('tab14')\">Performance Management</a></li>\n                <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNewHire == 'tab15' }\">\n                  <a href=\"#tab15\" data-toggle=\"tab\" (click)=\"customNewHire?'':chooseSteps('tab15')\">Summary /\n                    Review</a></li>\n              </ul>\n            </div>\n\n            <div class=\"wizard-rgt col-md-9\">\n\n              <div class=\"tab-content\">\n\n                <div class=\"tab-pane\" id=\"ClientInfo1\"\n                  [ngClass]=\"{'active':selectedNewHire === 'ClientInfo1','in':selectedNewHire==='ClientInfo1'}\">\n\n                  <div class=\"personal-main\">\n                    <form [formGroup]=\"moduleForm1\">\n\n                      <div class=\"personal\">\n                        <div class=\"personal-cont\">\n\n                          <ul>\n                            <div formGroupName=\"module_settings\">\n                              <li class=\"col-md-6 personal-dob\">\n                                <label>Name the Wizard *</label>\n                                <input type=\"text\" class=\"form-control\" placeholder=\"Name the Wizard\"\n                                  formControlName=\"wizardName\" [(ngModel)]=\"wizardName\">\n                                <span\n                                  *ngIf=\"moduleForm1.get('module_settings').get('wizardName').invalid && moduleForm1.get('module_settings').get('wizardName').touched\"\n                                  class=\"errors\">Enter Name</span>\n                              </li>\n\n\n                              <li class=\"col-md-6 personal-dob\">\n                                <a (click)=\"goToFieldMaintains()\">Review Standard & Custom Required Fields</a>\n                              </li>\n\n                              <li class=\"col-md-6 personal-dob\">\n                                <label>Make Eligible for Benefits? *</label>\n                                <mat-select class=\"form-control\" placeholder=\"Benefits\"\n                                  formControlName=\"eligible_for_benifits\" [(ngModel)]=\"eligible_for_benifits\">\n                                  <mat-option [value]=true>Yes</mat-option>\n                                  <mat-option [value]=false>No</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm1.get('module_settings').get('eligible_for_benifits').invalid && moduleForm1.get('module_settings').get('eligible_for_benifits').touched\"\n                                  class=\"errors\">Enter Benefits</span>\n                              </li>\n\n                              <li class=\"col-md-6 personal-dob\">\n                                <label>Assign Leave Management Rules now? *</label>\n                                <mat-select class=\"form-control\" placeholder=\"Rules\"\n                                  formControlName=\"assign_leave_management_rules\"\n                                  [(ngModel)]=\"assign_leave_management_rules\">\n                                  <mat-option [value]=true>Yes</mat-option>\n                                  <mat-option [value]=false>No</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm1.get('module_settings').get('assign_leave_management_rules').invalid && moduleForm1.get('module_settings').get('assign_leave_management_rules').touched\"\n                                  class=\"errors\">Enter Rules</span>\n                              </li>\n\n                              <li class=\"col-md-6 personal-dob\">\n                                <label>Assign Performance Management now? *</label>\n                                <mat-select class=\"form-control\" placeholder=\"Performance\"\n                                  formControlName=\"assign_performance_management\"\n                                  [(ngModel)]=\"assign_performance_management\">\n                                  <mat-option [value]=true>Yes</mat-option>\n                                  <mat-option [value]=false>No</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm1.get('module_settings').get('assign_performance_management').invalid && moduleForm1.get('module_settings').get('assign_performance_management').touched\"\n                                  class=\"errors\">Enter Performance</span>\n                              </li>\n                              <!-- \n                              <li class=\"col-md-6 personal-dob\">\n                                <label>How will you assign Employee Number (manual or automatic)? </label>\n                                <mat-select class=\"form-control\" placeholder=\"Employee Number\"\n                                  formControlName=\"assaign_employee_number\" [(ngModel)]=\"assaign_employee_number\">\n                                  <mat-option value='System Generated'>System Generated</mat-option>\n                                  <mat-option value='Manual'>Manual</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm1.get('module_settings').get('assaign_employee_number').invalid && moduleForm1.get('module_settings').get('assaign_employee_number').touched\"\n                                  style=\"color:#e44a49; padding: 5px 0 0;display: block\">Enter Number</span>\n                              </li> -->\n\n\n                              <li class=\"col-md-8 personal-dob\">\n                                <label>Select the Onboarding Template to apply *</label>\n                                <mat-select class=\"form-control\" placeholder=\"Template\"\n                                  (selectionChange)=\"onboardTemplate($event)\" formControlName=\"onboarding_template\"\n                                  [(ngModel)]=\"onboarding_template\">\n                                  <mat-option value={{getModules._id}} *ngFor=\"let getModules of getModulesTemplates\">\n                                    {{getModules.templateName}}</mat-option>\n                                </mat-select>\n                                <span\n                                  *ngIf=\"moduleForm1.get('module_settings').get('onboarding_template').invalid && moduleForm1.get('module_settings').get('onboarding_template').touched\"\n                                  class=\"errors\">Enter Template</span>\n                              </li>\n                            </div>\n                          </ul>\n                        </div>\n\n                      </div>\n                    </form>\n\n                    <div class=\"wizard-popup\"></div>\n                    <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                    <button class=\"btn pull-right save\" (click)=\"chooseStepSubmit()\">Proceed</button>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab12\">\n\n                  <div class=\"personal\">\n                    <div class=\"personal-cont\">\n\n                      <div class=\"benefits\">\n\n                        <ul>\n                          <li>\n                            <label>Select the Benefit Eligibility Group</label>\n                          </li>\n                          <li class=\"col-md-3\">\n                            <mat-checkbox>Full Time</mat-checkbox>\n                          </li>\n\n                          <li class=\"col-md-3\">\n                            <mat-checkbox>Part Time</mat-checkbox>\n                          </li>\n\n                          <li class=\"col-md-3\">\n                            <mat-checkbox>Retires</mat-checkbox>\n                          </li>\n                        </ul>\n                        <div class=\"clearfix\"></div>\n\n                      </div>\n\n                      <ul>\n                        <li class=\"col-md-4 personal-dob\">\n                          <label>Benefits Eligibility Group Effective Date </label>\n                          <div class=\"date\">\n                            <input matInput [matDatepicker]=\"picker15\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                            <mat-datepicker #picker15></mat-datepicker>\n                            <button mat-raised-button (click)=\"picker15.open()\">\n                              <span>\n                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                              </span>\n                            </button>\n                            <div class=\"clearfix\"></div>\n                          </div>\n                        </li>\n\n                        <li class=\"col-md-6 personal-dob\">\n                          <label>administrator makes enrollments or allow EE to make enrollments?</label>\n                          <mat-select class=\"form-control\" placeholder=\"Rules\">\n                            <mat-option value='administrator'>administrator</mat-option>\n                            <mat-option value='administrator'>administrator1</mat-option>\n                          </mat-select>\n                        </li>\n\n                        <li class=\"col-md-6 personal-dob\">\n                          <label>Apply New Hire Enrollments Settings?</label>\n                          <mat-select class=\"form-control\" placeholder=\"Performance\">\n                            <mat-option value='yes'>Yes</mat-option>\n                            <mat-option value='no'>No</mat-option>\n                          </mat-select>\n                        </li>\n\n                      </ul>\n\n                    </div>\n\n\n                  </div>\n\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab13\">\n                  Tab3\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab14\">\n                  Tab4\n                </div>\n\n\n                <div class=\"tab-pane\" id=\"tab15\"\n                  [ngClass]=\"{'active':selectedNewHire === 'tab15','in':selectedNewHire==='tab15'}\">\n\n                  <div class=\"tab-content\">\n\n                    <div class=\"newhire-summary\">\n\n                      <h2>Choose Steps</h2>\n\n                      <form [formGroup]=\"moduleForm1\">\n\n                        <div class=\"personal personal1\">\n                          <div class=\"personal-cont\">\n\n                            <ul>\n                              <div formGroupName=\"module_settings\">\n                                <li class=\"col-md-6 personal-dob\">\n                                  <label>Name the Wizard</label>\n                                  <input type=\"text\" class=\"form-control\" placeholder=\"Name the Wizard\"\n                                    formControlName=\"wizardName\" [(ngModel)]=\"wizardName\" readonly>\n\n                                </li>\n\n                                <li class=\"col-md-6 personal-dob\">\n                                  <label>Make Eligible for Benefits? </label>\n                                  <mat-select class=\"form-control\" placeholder=\"Benefits\"\n                                    formControlName=\"eligible_for_benifits\" [(ngModel)]=\"eligible_for_benifits\"\n                                    [disabled]=true>\n                                    <mat-option [value]=true>Yes</mat-option>\n                                    <mat-option [value]='false'>No</mat-option>\n                                  </mat-select>\n\n                                </li>\n\n                                <li class=\"col-md-6 personal-dob\">\n                                  <label>Assign Leave Management Rules now? </label>\n                                  <mat-select class=\"form-control\" placeholder=\"Rules\"\n                                    formControlName=\"assign_leave_management_rules\"\n                                    [(ngModel)]=\"assign_leave_management_rules\" [disabled]=true>\n                                    <mat-option [value]=true>Yes</mat-option>\n                                    <mat-option [value]=false>No</mat-option>\n                                  </mat-select>\n\n                                </li>\n\n                                <li class=\"col-md-6 personal-dob\">\n                                  <label>Assign Performance Management now? </label>\n                                  <mat-select class=\"form-control\" placeholder=\"Performance\"\n                                    formControlName=\"assign_performance_management\"\n                                    [(ngModel)]=\"assign_performance_management\" [disabled]=true>\n                                    <mat-option [value]=true>Yes</mat-option>\n                                    <mat-option [value]=false>No</mat-option>\n                                  </mat-select>\n\n                                </li>\n\n                                <li class=\"col-md-6 personal-dob\">\n                                  <label>How will you assign Employee Number (manual or automatic)? </label>\n                                  <mat-select class=\"form-control\" placeholder=\"Employee Number\"\n                                    formControlName=\"assaign_employee_number\" [(ngModel)]=\"assaign_employee_number\"\n                                    [disabled]=true>\n                                    <mat-option value='System Generated'>System Generated</mat-option>\n                                    <mat-option value='Manual'>Manual</mat-option>\n                                  </mat-select>\n\n                                </li>\n\n\n                                <li class=\"col-md-8 personal-dob\">\n                                  <label>Select the Onboarding Template to apply </label>\n                                  <mat-select class=\"form-control\" placeholder=\"Template\"\n                                    (selectionChange)=\"onboardTemplate($event)\" formControlName=\"onboarding_template\"\n                                    [(ngModel)]=\"onboarding_template\" [disabled]=true>\n                                    <mat-option value={{getModules._id}} *ngFor=\"let getModules of getModulesTemplates\">\n                                      {{getModules.templateName}}</mat-option>\n                                  </mat-select>\n\n                                </li>\n                              </div>\n                            </ul>\n                          </div>\n\n                        </div>\n                      </form>\n\n                    </div>\n\n                  </div>\n\n                  <div class=\"wizard-popup\"></div>\n                  <button class=\"btn pull-left back\" (click)=\"goToChoose()\">Back</button>\n                  <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                  <button class=\"btn pull-right save\" (click)=\"customSummarySubmit()\">Submit</button>\n                  <div class=\"clearfix\"></div>\n                </div>\n\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n    </div>\n  </div>\n</div>\n\n\n\n\n<!-- Create Employee Specific Schedule -->\n\n<div class=\"work-history\">\n\n  <div class=\"modal fade\" id=\"myModal5\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Create Employee Specific Schedule</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"scroll\">\n\n            <div class=\"main-works\">\n\n              <form (ngSubmit)=\"createWorkPosition(createEmployeeForm)\" #createEmployeeForm=\"ngForm\">\n\n                <div class=\"edit-work col-md-11\">\n\n                  <ul>\n                    <li class=\"col-md-3\">\n                      <label>Work Schedule Title</label>\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Schedule Title\"\n                        [(ngModel)]=\"positionSchedule.scheduleTitle\" name=\"title\" #title=\"ngModel\" required>\n                      <p *ngIf=\"!positionSchedule.scheduleTitle && title.touched || (!positionSchedule.scheduleTitle && isValid)\"\n                        class=\"errors\">\n                        Enter schedule title</p>\n\n                    </li>\n\n                    <li class=\"col-md-3\">\n                      <label>Work Schedule Start Date</label>\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker012\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                          [(ngModel)]=\"positionSchedule.scheduleStartDate\" name=\"date\" #date=\"ngModel\" required>\n                        <mat-datepicker #picker012></mat-datepicker>\n                        <a mat-raised-button (click)=\"picker012.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </a>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                      <p *ngIf=\"!positionSchedule.scheduleStartDate && date.touched || (!positionSchedule.scheduleStartDate && isValid)\"\n                        class=\"errors\">\n                        Enter schedule start date</p>\n                    </li>\n\n                  </ul>\n                  <div class=\"clearfix\"></div>\n                  <h4>Select Days of the Week Applicable</h4>\n\n                  <ul>\n\n                    <li *ngFor=\"let day of days\">\n\n                      <mat-checkbox class=\"zenwork-customized-checkbox\" name=\"day-list\" [(ngModel)]=\"day.isChecked\"\n                        (ngModelChange)=\"selectDay($event, day.name, day.isChecked)\">\n                        {{day.name}}\n                      </mat-checkbox>\n                    </li>\n\n                  </ul>\n\n\n                  <ul class=\"total-hours\">\n                    <li class=\"col-md-2\">\n                      <label>Total Hours per Day</label>\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Total Hours Day\"\n                        [(ngModel)]=\"positionSchedule.totalHrsPerDay\" name=\"day\" #day=\"ngModel\" required\n                        (ngModelChange)=\"totalHoursDay($event)\">\n                      <p *ngIf=\"!positionSchedule.totalHrsPerDay && day.touched || (!positionSchedule.totalHrsPerDay && isValid)\"\n                        class=\"errors\">\n                        Enter hours per day</p>\n                    </li>\n\n                    <li class=\"col-md-3\">\n                      <label>Total Hours per Week</label>\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Total Hours Week\"\n                        [(ngModel)]=\"positionSchedule.totalHrsPerWeek\" name=\"week\" #week=\"ngModel\" required\n                        [disabled]='true'>\n                      <p *ngIf=\"!positionSchedule.totalHrsPerWeek && week.touched || (!positionSchedule.totalHrsPerWeek && isValid)\"\n                        class=\"errors\">\n                        Enter hours per week</p>\n                    </li>\n                    <div class=\"clearfix\"></div>\n\n                    <!-- <li class=\"col-md-3\">\n                      <label>Schedule Type</label>\n                      <mat-select class=\"form-control\" placeholder=\"Type\" [(ngModel)]=\"positionSchedule.type\"\n                        name=\"type\" #type=\"ngModel\" required>\n                        <mat-option value='Standard'>Standard</mat-option>\n                        <mat-option value='Employee Specific'>Employee Specific</mat-option>\n                      </mat-select>\n                      <p *ngIf=\"!positionSchedule.type && type.touched || (!positionSchedule.type && isValid)\"\n                        class=\"errors\">\n                        Enter schedule type</p>\n                    </li>\n\n                    <li class=\"col-md-3\">\n                      <label>Schedule Type</label>\n                      <mat-select class=\"form-control\" placeholder=\"Type\" [(ngModel)]=\"positionSchedule.type\"\n                        name=\"type\" #type=\"ngModel\" required>\n                        <mat-option value='Standard'>Standard</mat-option>\n                        <mat-option value='Employee Specific'>Employee Specific</mat-option>\n                      </mat-select>\n                      <p *ngIf=\"!positionSchedule.type && type.touched || (!positionSchedule.type && isValid)\"\n                        class=\"errors\">\n                        Enter schedule type</p>\n                    </li> -->\n\n\n                  </ul>\n                  <div class=\"clearfix\"></div>\n\n\n                  <table class=\"table\">\n                    <thead>\n                      <tr>\n                        <th></th>\n                        <th>Day</th>\n                        <th>Start Time</th>\n                        <th>End Time</th>\n\n\n                      </tr>\n                    </thead>\n\n                    <tbody>\n                      <tr *ngFor=\"let day of weekDays; let i=index; let j=index;\">\n\n                        <td></td>\n\n                        <td style=\"padding:30px 0 0;\">{{day.day}}</td>\n\n                        <td>\n                          <div class=\"start-time\">\n                            <input class=\"form-control\" [ngxTimepicker]=\"i\" name=\"starttime\" [(ngModel)]=\"day.shiftStartTime\"\n                              [value]=\"day.shiftStartTime\" (ngModelChange)=\"startTimeDay($event,j)\">\n                            <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                            <ngx-material-timepicker #i></ngx-material-timepicker>\n                          </div>\n\n\n                        </td>\n                        <td>\n                          <div class=\"start-time\">\n                            <input class=\"form-control\" [ngxTimepicker]=\"ii\" name=\"end_time\" [(ngModel)]=\"day.shiftEndTime\"\n                              [value]=\"day.shiftEndTime\" (ngModelChange)=\"endTimeDays($event,j)\">\n                            <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                            <ngx-material-timepicker #ii></ngx-material-timepicker>\n                          </div>\n                        </td>\n                      </tr>\n\n                    </tbody>\n                  </table>\n\n\n                </div>\n\n                <div class=\"border-btm col-md-11\">\n\n                  <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                  <button type=\"submit\" class=\"btn pull-right save\">Submit</button>\n                  <div class=\"clearfix\"></div>\n\n                </div>\n\n              </form>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n    </div>\n\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.ts ***!
  \******************************************************************************************/
/*! exports provided: OnBoardingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnBoardingComponent", function() { return OnBoardingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../..../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_workflow_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/workflow.service */ "./src/app/services/workflow.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var amazing_time_picker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! amazing-time-picker */ "./node_modules/amazing-time-picker/amazing-time-picker.es5.js");
/* harmony import */ var _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/pay-roll.service */ "./src/app/services/pay-roll.service.ts");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { Router } from '@angular/router';











var OnBoardingComponent = /** @class */ (function () {
    function OnBoardingComponent(router, onboardingService, swalAlertService, route, workflowService, siteAccessRolesService, atp, payRollService, leaveManagementService, loaderService) {
        var _this = this;
        this.router = router;
        this.onboardingService = onboardingService;
        this.swalAlertService = swalAlertService;
        this.route = route;
        this.workflowService = workflowService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.atp = atp;
        this.payRollService = payRollService;
        this.leaveManagementService = leaveManagementService;
        this.loaderService = loaderService;
        this.onboardNewHire = true;
        this.customNewHire = true;
        this.deleteId = [];
        this.template = {
            "templateName": "Onboarding Template",
            "Onboarding Tasks": {}
        };
        this.standardTemplate = {
            templateName: ''
        };
        this.scheduleData = [];
        this.payrollGroups = [];
        this.allTasks = [];
        this.popupData = {};
        this.assignEmployee = {
            numberType: ''
        };
        this.ClientInfo = false;
        this.tab2 = false;
        this.tab3 = false;
        this.tab4 = false;
        this.tab6 = false;
        this.tab8 = false;
        this.ClientInfo1 = false;
        this.tab15 = false;
        this.tab12 = false;
        this.tab13 = false;
        this.tab14 = false;
        this.skipReportsto = false;
        this.allMaritalStatusArray = [];
        this.allVeteranStatusArray = [];
        this.ethnicityArray = [];
        this.employeeTypeArray = [];
        this.jobTitleArray = [];
        this.departmentArray = [];
        this.locationArray = [];
        this.businessUnitArray = [];
        this.unionArray = [];
        this.flsaCodeArray = [];
        this.eeoJobCategoryArray = [];
        this.payTypeArray = [];
        this.payFrequencyArray = [];
        this.countryArray = [];
        this.stateArray = [];
        this.workSchedule = [];
        this.leaveConditon = false;
        this.createEmployee = false;
        this.positionScheduleShow = false;
        this.selectedDays = [];
        this.weekDays = [];
        this.allSelectedDays = [];
        this.employeeIds = [];
        this.isValid = false;
        this.isNotValid = false;
        this.allDays = [];
        this.compensationError = false;
        this.resRolesObj = [];
        this.days = [
            { name: 'Monday', isChecked: false },
            { name: 'Tuesday', isChecked: false },
            { name: 'Wednesday', isChecked: false },
            { name: 'Thursday', isChecked: false },
            { name: 'Friday', isChecked: false },
            { name: 'Saturday', isChecked: false },
            { name: 'Sunday', isChecked: false },
        ];
        this.positionSchedule = {
            scheduleTitle: '',
            scheduleStartDate: '',
            type: 'Employee Specific',
            totalHrsPerDay: '',
            totalHrsPerWeek: '',
        };
        this.skipOnboardTemplate = false;
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
    }
    OnBoardingComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationStart"]) {
            this.loaderService.loader(true);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]) {
            this.loaderService.loader(false);
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationCancel"]) {
            this.loaderService.loader(false);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationError"]) {
            this.loaderService.loader(false);
        }
    };
    OnBoardingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dobDate = new Date();
        this.companyID = localStorage.getItem('companyId');
        console.log("iddd111111", this.companyID);
        this.companyID = this.companyID.replace(/^"|"$/g, "");
        console.log("iddd222222222", this.companyID);
        var cId = JSON.parse(localStorage.getItem('companyId'));
        this.getStandardAndCustomFieldsforStructure(cId);
        this.getHrRoles();
        this.getBaseRoles();
        this.selectedNav = "ClientInfo";
        this.selectedNewHire = "ClientInfo1";
        // this.selectedNav="tab2";
        // this.ClientInfo=true;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log(_this.id);
        });
        // this.id="5cb870f2b494651a2bd98750"
        // this.getStandardOnboardingtemplate(this.id);
        this.getStandardOnBoardTemplate();
        this.getCustomOnboardTemplates();
        this.personalForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            personal: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                    firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    middleName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    preferredName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                }),
                dob: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                maritalStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                effectiveDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                veteranStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                ssn: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                ethnicity: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                address: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                    street1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    street2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                    city: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    state: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    country: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                }),
                contact: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                    workPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    extention: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    mobilePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    homePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    workMail: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]),
                    personalMail: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]),
                }),
            }),
        });
        this.personalValueChanges();
        // Author:Suresh M, Date:23-04-19
        // Job & Compensation Validation
        this.jobForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            job: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                employeeId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                employeeType: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                hireDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                jobTitle: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                department: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                location: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                businessUnit: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                unionFields: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                FLSA_Code: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                EEO_Job_Category: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                Site_AccessRole: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                // UserRole: new FormControl("", Validators.required),
                VendorId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                Specific_Workflow_Approval: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                HR_Contact: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                ReportsTo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                // skipReportsto: new FormControl('',Validators.required)
                workHours: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            }),
            compensation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                NonpaidPosition: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                Pay_group: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                Pay_frequency: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                First_Pay_Date: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                compensationRatio: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                Salary_Grade: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                grade_band: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
                payRate: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                    amount: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                    payType: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                })
            }),
        });
        this.compensationValueChanges();
        this.getAllJobData();
        this.moduleForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            module_settings: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                eligible_for_benifits: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                assign_leave_management_rules: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                assign_performance_management: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                // assaign_employee_number: new FormControl("", Validators.required),
                onboarding_template: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                skip_template_assignment: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            })
        });
        this.moduleValueChanges();
        this.moduleForm1 = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            module_settings: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                wizardName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                eligible_for_benifits: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                assign_leave_management_rules: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                assign_performance_management: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
                // assaign_employee_number: new FormControl("", Validators.required),
                onboarding_template: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            })
        });
        this.getTemplates();
        this.GetAllCustomData();
        var cID = localStorage.getItem('companyId');
        console.log("cidddddddddd", cID);
        cID = cID.replace(/^"|"$/g, "");
        console.log("cin222222222", cID);
        this.onboardingService.getSiteAccesRole(cID)
            .subscribe(function (res) {
            console.log("Site Access Roless", res);
            _this.roles = res.data;
            for (var i = 0; i < _this.resRolesObj.length; i++) {
                console.log("sldkjfkldsj");
                _this.roles.push(_this.resRolesObj[i]);
            }
        });
        this.employeeSearch();
        this.today = new Date().toISOString();
        // this.getAllWorkSchdules();
        this.getPayrollGroup();
        this.getsalrayGrades();
    };
    OnBoardingComponent.prototype.personalValueChanges = function () {
        // var mrStatus = this.personalForm.get('personal').get('maritalStatus')
        var eDate = this.personalForm.get('personal').get('effectiveDate');
        this.personalForm.get('personal').get('maritalStatus').valueChanges.subscribe(function (val) {
            console.log(val);
            if (val === 'Single') {
                eDate.clearValidators();
            }
            else if (val === 'Married' || val === 'Divorced' || val === "Registered Parternship") {
                eDate.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            }
            eDate.updateValueAndValidity();
        });
    };
    OnBoardingComponent.prototype.moduleValueChanges = function () {
        var skiptemp = this.moduleForm.get('module_settings').get('onboarding_template');
        this.moduleForm.get('module_settings').get('skip_template_assignment').valueChanges.subscribe(function (val) {
            console.log(val);
            if (val == true) {
                skiptemp.clearValidators();
            }
            else if (val == false) {
                skiptemp.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            }
            skiptemp.updateValueAndValidity();
        });
    };
    OnBoardingComponent.prototype.compensationValueChanges = function () {
        var FpayDate = this.jobForm.get('compensation').get('First_Pay_Date');
        var pGroup = this.jobForm.get('compensation').get('Pay_group');
        var pType = this.jobForm.get('compensation').get('payRate').get('payType');
        var pRate = this.jobForm.get('compensation').get('payRate').get('amount');
        this.jobForm.get('compensation').get('NonpaidPosition').valueChanges.subscribe(function (val) {
            console.log(val, typeof val);
            if (val == false) {
                FpayDate.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
                pGroup.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
                pType.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
                pRate.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            }
            else {
                FpayDate.clearValidators();
                pGroup.clearValidators();
                pType.clearValidators();
                pRate.clearValidators();
            }
            FpayDate.updateValueAndValidity();
            pGroup.updateValueAndValidity();
            pType.updateValueAndValidity();
            pRate.updateValueAndValidity();
        });
    };
    OnBoardingComponent.prototype.getStandardAndCustomFieldsforStructure = function (cID) {
        var _this = this;
        this.onboardingService.getStructureFields(cID)
            .subscribe(function (res) {
            console.log("structure fields", res);
            _this.allStructureFieldsData = res.data;
            //matital-status
            if (_this.allStructureFieldsData['Marital Status'].custom.length > 0) {
                _this.allStructureFieldsData['Marital Status'].custom.forEach(function (element) {
                    _this.allMaritalStatusArray.push(element);
                    console.log("22222111111", _this.allMaritalStatusArray);
                });
            }
            if (_this.allStructureFieldsData['Marital Status'].default.length > 0) {
                _this.allStructureFieldsData['Marital Status'].default.forEach(function (element) {
                    _this.allMaritalStatusArray.push(element);
                });
                console.log("22222", _this.allMaritalStatusArray);
            }
            if (_this.allStructureFieldsData['Veteran Status'].custom.length > 0) {
                _this.allStructureFieldsData['Veteran Status'].custom.forEach(function (element) {
                    _this.allVeteranStatusArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Veteran Status'].default.length > 0) {
                _this.allStructureFieldsData['Veteran Status'].default.forEach(function (element) {
                    _this.allVeteranStatusArray.push(element);
                });
                console.log("22222", _this.allVeteranStatusArray);
            }
            if (_this.allStructureFieldsData['Ethnicity'].custom.length > 0) {
                _this.allStructureFieldsData['Ethnicity'].custom.forEach(function (element) {
                    _this.ethnicityArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Ethnicity'].default.length > 0) {
                _this.allStructureFieldsData['Ethnicity'].default.forEach(function (element) {
                    _this.ethnicityArray.push(element);
                });
                console.log("22222ethnicity", _this.ethnicityArray);
            }
            if (_this.allStructureFieldsData['Employee Type'].custom.length > 0) {
                _this.allStructureFieldsData['Employee Type'].custom.forEach(function (element) {
                    _this.employeeTypeArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Employee Type'].default.length > 0) {
                _this.allStructureFieldsData['Employee Type'].default.forEach(function (element) {
                    _this.employeeTypeArray.push(element);
                });
                console.log("22222EMpType", _this.employeeTypeArray);
            }
            if (_this.allStructureFieldsData['Job Title'].custom.length > 0) {
                _this.allStructureFieldsData['Job Title'].custom.forEach(function (element) {
                    _this.jobTitleArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Job Title'].default.length > 0) {
                _this.allStructureFieldsData['Job Title'].default.forEach(function (element) {
                    _this.jobTitleArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Department'].custom.length > 0) {
                _this.allStructureFieldsData['Department'].custom.forEach(function (element) {
                    _this.departmentArray.push(element);
                });
                console.log("22222", _this.jobTitleArray);
            }
            if (_this.allStructureFieldsData['Department'].default.length > 0) {
                _this.allStructureFieldsData['Department'].default.forEach(function (element) {
                    _this.departmentArray.push(element);
                });
                console.log("22222", _this.departmentArray);
            }
            if (_this.allStructureFieldsData['Location'].custom.length > 0) {
                _this.allStructureFieldsData['Location'].custom.forEach(function (element) {
                    _this.locationArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Location'].default.length > 0) {
                _this.allStructureFieldsData['Location'].default.forEach(function (element) {
                    _this.locationArray.push(element);
                });
                console.log("22222", _this.locationArray);
            }
            if (_this.allStructureFieldsData['Business Unit'].custom.length > 0) {
                _this.allStructureFieldsData['Business Unit'].custom.forEach(function (element) {
                    _this.businessUnitArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Business Unit'].default.length > 0) {
                _this.allStructureFieldsData['Business Unit'].default.forEach(function (element) {
                    _this.businessUnitArray.push(element);
                });
                console.log("22222", _this.businessUnitArray);
            }
            if (_this.allStructureFieldsData['Union'].custom.length > 0) {
                _this.allStructureFieldsData['Union'].custom.forEach(function (element) {
                    _this.unionArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Union'].default.length > 0) {
                _this.allStructureFieldsData['Union'].default.forEach(function (element) {
                    _this.unionArray.push(element);
                });
                console.log("22222union", _this.unionArray);
            }
            if (_this.allStructureFieldsData['FLSA Code'].custom.length > 0) {
                _this.allStructureFieldsData['FLSA Code'].custom.forEach(function (element) {
                    _this.flsaCodeArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['FLSA Code'].default.length > 0) {
                _this.allStructureFieldsData['FLSA Code'].default.forEach(function (element) {
                    _this.flsaCodeArray.push(element);
                });
                console.log("22222", _this.flsaCodeArray);
            }
            if (_this.allStructureFieldsData['EEO Job Category'].custom.length > 0) {
                _this.allStructureFieldsData['EEO Job Category'].custom.forEach(function (element) {
                    _this.eeoJobCategoryArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['EEO Job Category'].default.length > 0) {
                _this.allStructureFieldsData['EEO Job Category'].default.forEach(function (element) {
                    _this.eeoJobCategoryArray.push(element);
                });
                console.log("22222", _this.eeoJobCategoryArray);
            }
            if (_this.allStructureFieldsData['Country'].custom.length > 0) {
                _this.allStructureFieldsData['Country'].custom.forEach(function (element) {
                    _this.countryArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['Country'].default.length > 0) {
                _this.allStructureFieldsData['Country'].default.forEach(function (element) {
                    _this.countryArray.push(element);
                });
                console.log("22222country", _this.countryArray);
            }
            if (_this.allStructureFieldsData['State'].custom.length > 0) {
                _this.allStructureFieldsData['State'].custom.forEach(function (element) {
                    _this.stateArray.push(element);
                });
            }
            if (_this.allStructureFieldsData['State'].default.length > 0) {
                _this.allStructureFieldsData['State'].default.forEach(function (element) {
                    _this.stateArray.push(element);
                });
                console.log("22222", _this.stateArray);
            }
        }, function (err) {
        });
    };
    /* Description: getting employees list
     author : vipin reddy */
    OnBoardingComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get only Hr employees
    author : vipin reddy */
    OnBoardingComponent.prototype.getHrRoles = function () {
        var _this = this;
        this.HrId = "5cbe98f8561562212689f748";
        var Cid = JSON.parse(localStorage.getItem('companyId'));
        this.workflowService.getHrRoleEmployeesOnly(Cid, this.HrId)
            .subscribe(function (res) {
            console.log("HR employees", res);
            _this.hrData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: custom onboard template create
    author : vipin reddy Date:17-04-2019*/
    OnBoardingComponent.prototype.customTemplate = function () {
        this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding/custom-onboard-template']);
    };
    /* Description: delete custom template
    author : vipin reddy Date:17-04-2019*/
    OnBoardingComponent.prototype.deleteTemplate = function () {
        var _this = this;
        console.log(this.deleteId);
        this.onboardingService.deleteTemplate(this.deleteId)
            .subscribe(function (res) {
            console.log("delete", res);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Delete", res.message, "success");
            _this.getCustomOnboardTemplates();
            _this.deleteId = [];
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Delete", err.error.message, 'error');
        });
    };
    /* Description: get custom onboard template data
    author : vipin reddy Date:17-04-2019*/
    OnBoardingComponent.prototype.getCustomOnboardTemplates = function () {
        var _this = this;
        this.onboardingService.getAllCustomTemplates()
            .subscribe(function (res) {
            console.log(res);
            _this.templates = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get standard onboard template data
     author : vipin reddy Date:17-04-2019*/
    OnBoardingComponent.prototype.getStandardOnBoardTemplate = function () {
        var _this = this;
        this.onboardingService.getStandardTemplates()
            .subscribe(function (res) {
            console.log(res);
            _this.standardTemplate = res.data;
            _this.standradTemplateId = res.data._id;
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Suresh M, Date:23-04-19
    // showOptions
    OnBoardingComponent.prototype.showOptions = function ($event, id) {
        console.log("1222222", $event.checked, id);
        this.custonWizardID = id;
        if ($event.checked == true) {
            this.deleteId.push(id);
        }
        console.log(this.deleteId, "123321");
        if ($event.checked == false) {
            var index = this.deleteId.indexOf(id);
            if (index > -1) {
                this.deleteId.splice(index, 1);
            }
            console.log(this.deleteId);
        }
    };
    /* Description: standard onboarding template open
    author : vipin reddy Date:17-04-2019*/
    OnBoardingComponent.prototype.templateRedirect = function () {
        this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding/onboardingTemplate/' + this.standradTemplateId]);
    };
    /* Description: custom onboarding template open
    author : vipin reddy Date:17-04-2019*/
    OnBoardingComponent.prototype.customTemplateRedirect = function (id) {
        this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding/custom-onboard-template//' + id]);
    };
    OnBoardingComponent.prototype.goToPersonel = function () {
        console.log('clickkkkk');
        this.client_info.nativeElement.click();
        // this.scrollTop.nativeElement.scrollIntoView();
    };
    OnBoardingComponent.prototype.goToJob = function () {
        this.job_compensation.nativeElement.click();
    };
    OnBoardingComponent.prototype.goToModule = function () {
        this.module_Settings.nativeElement.click();
    };
    OnBoardingComponent.prototype.goToView = function () {
        this.view_Onboard.nativeElement.click();
    };
    OnBoardingComponent.prototype.goToOnboard = function () {
        this.leave_management.nativeElement.click();
    };
    OnBoardingComponent.prototype.goToChoose = function () {
        console.log('clickkkkk');
        this.client_info1.nativeElement.click();
        // this.scrollTop.nativeElement.scrollIntoView();
    };
    // Author:Suresh M, Date:23-04-19
    // Personal Fields Add Api
    OnBoardingComponent.prototype.personalFieldSubmit = function () {
        var _this = this;
        // this.selectedNav='tab2';
        console.log("personal Fieldsss", this.assignEmployee.numberType, this.personalForm.value);
        if (this.personalForm.valid) {
            if (this.assignEmployee.numberType == 'system Generated') {
                var cId = JSON.parse(localStorage.getItem('companyId'));
                this.onboardingService.preEmployeeIdGeneration(cId)
                    .subscribe(function (res) {
                    console.log("Get the job Compensations", res);
                    _this.jobForm.get('job').get('employeeId').patchValue(res.data.employeeId);
                    _this.onboardNewHire = false;
                }, function (err) {
                    console.log(err);
                });
            }
            this.selectedNav = "tab2";
        }
        else {
            this.personalForm.get('personal').get('name').get('firstName').markAsTouched();
            this.personalForm.get('personal').get('name').get('middleName').markAsTouched();
            this.personalForm.get('personal').get('name').get('lastName').markAsTouched();
            this.personalForm.get('personal').get('name').get('preferredName').markAsTouched();
            this.personalForm.get('personal').get('dob').markAsTouched();
            this.personalForm.get('personal').get('gender').markAsTouched();
            this.personalForm.get('personal').get('maritalStatus').markAsTouched();
            // if (this.maritalStatus != 'Single') {
            //   this.personalForm.get('personal').get('effectiveDate').markAsTouched();
            // }
            this.personalForm.get('personal').get('effectiveDate').markAsTouched();
            this.personalForm.get('personal').get('veteranStatus').markAsTouched();
            this.personalForm.get('personal').get('ssn').markAsTouched();
            this.personalForm.get('personal').get('ethnicity').markAsTouched();
            this.personalForm.get('personal').get('address').get('street1').markAsTouched();
            // this.personalForm.get('personal').get('address').get('street2').markAsTouched();
            this.personalForm.get('personal').get('address').get('city').markAsTouched();
            this.personalForm.get('personal').get('address').get('state').markAsTouched();
            this.personalForm.get('personal').get('address').get('zipcode').markAsTouched();
            this.personalForm.get('personal').get('address').get('country').markAsTouched();
            this.personalForm.get('personal').get('contact').get('workPhone').markAsTouched();
            this.personalForm.get('personal').get('contact').get('extention').markAsTouched();
            this.personalForm.get('personal').get('contact').get('mobilePhone').markAsTouched();
            this.personalForm.get('personal').get('contact').get('homePhone').markAsTouched();
            this.personalForm.get('personal').get('contact').get('workMail').markAsTouched();
            this.personalForm.get('personal').get('contact').get('personalMail').markAsTouched();
        }
    };
    // Author:Suresh M, Date:23-04-19
    // Job & Compensation post Api
    OnBoardingComponent.prototype.jobSubmit = function () {
        console.log("jobbbbbbbb", this.jobForm.value);
        if (this.jobForm.valid) {
            console.log("111111111111", this.jobForm.valid);
            if (this.skipReportsto == false) {
                this.jobObj = this.jobForm.get('job').value;
            }
            this.selectedNav = "tab3";
            console.log(this.jobObj, "jobOBj");
        }
        else {
            console.log("NOT VALID", this.jobForm.get('job').get('employeeId'));
            this.jobForm.get('job').get('employeeId').markAsTouched();
            this.jobForm.get('job').get('employeeType').markAsTouched();
            this.jobForm.get('job').get('hireDate').markAsTouched();
            this.jobForm.get('job').get('jobTitle').markAsTouched();
            this.jobForm.get('job').get('department').markAsTouched();
            this.jobForm.get('job').get('location').markAsTouched();
            this.jobForm.get('job').get('businessUnit').markAsTouched();
            this.jobForm.get('job').get('unionFields').markAsTouched();
            this.jobForm.get('job').get('FLSA_Code').markAsTouched();
            // this.jobForm.get('job').get('EEO_Job_Category').markAsTouched();
            this.jobForm.get('job').get('Site_AccessRole').markAsTouched();
            // this.jobForm.get('job').get('UserRole').markAsTouched();
            this.jobForm.get('job').get('VendorId').markAsTouched();
            this.jobForm.get('job').get('Specific_Workflow_Approval').markAsTouched();
            this.jobForm.get('job').get('HR_Contact').markAsTouched();
            this.jobForm.get('job').get('ReportsTo').markAsTouched();
            this.jobForm.get('compensation').get('NonpaidPosition').markAsTouched();
            this.jobForm.get('compensation').get('Pay_group').markAsTouched();
            // this.jobForm.get('compensation').get('Pay_frequency').markAsTouched();
            this.jobForm.get('compensation').get('First_Pay_Date').markAsTouched();
            // this.jobForm.get('compensation').get('compensationRatio').markAsTouched();
            this.jobForm.get('compensation').get('Salary_Grade').markAsTouched();
            // this.jobForm.get('compensation').get('grade_band').markAsTouched();
            this.jobForm.get('compensation').get('payRate').get('amount').markAsTouched();
            // this.jobForm.get('compensation').get('payRate').get('payType').markAsTouched();
            // this.jobForm.get('compensation').get('workHours').markAsTouched();
        }
    };
    // Author:Suresh M, Date:24-04-19
    // Get the Api in Job & Compensation
    OnBoardingComponent.prototype.getAllJobData = function () {
        var _this = this;
        this.onboardingService.getAllJobDetails()
            .subscribe(function (res) {
            console.log("Get the job Compensations", res);
            _this.business = res.data['Business Unit'];
            _this.department = res.data['Department'];
            _this.eeoJob = res.data['EEO Job Category'];
            _this.empType = res.data['Employee Type'];
            _this.flsaCode = res.data['FLSA Code'];
            _this.jobTitle = res.data['Job Title'];
            _this.location = res.data['Location'];
            _this.frequency = res.data['Pay Frequency'];
            _this.payType = res.data['Pay Type'];
            _this.union = res.data['Union'];
            _this.assignEmployee = res['settings']['idNumber'];
        });
        console.log(this.jobTitle);
    };
    // Author:Suresh M, Date:26-04-19
    // Validate in Module Settings
    OnBoardingComponent.prototype.moduleSubmit = function () {
        console.log("Modulessssss", this.moduleForm.value);
        if (this.skipOnboardTemplate && this.moduleForm.valid) {
            this.selectedNav = "tab6";
        }
        if (this.moduleForm.valid && !this.skipOnboardTemplate) {
            this.selectedNav = "tab4";
            this.onboardNewHire = false;
        }
        else {
            this.moduleForm.get('module_settings').get('eligible_for_benifits').markAsTouched();
            this.moduleForm.get('module_settings').get('assign_leave_management_rules').markAsTouched();
            this.moduleForm.get('module_settings').get('assign_performance_management').markAsTouched();
            // this.moduleForm.get('module_settings').get('assaign_employee_number').markAsTouched();
            this.moduleForm.get('module_settings').get('onboarding_template').markAsTouched();
        }
    };
    // Author:Suresh M, Date:30-04-19
    // new hire Hr validation
    OnBoardingComponent.prototype.chooseStepSubmit = function () {
        console.log("ChooseSubmitModulessss", this.moduleForm1.value);
        if (this.moduleForm1.valid) {
            this.selectedNewHire = "tab15";
        }
        else {
            this.moduleForm1.get('module_settings').get('wizardName').markAsTouched();
            this.moduleForm1.get('module_settings').get('eligible_for_benifits').markAsTouched();
            this.moduleForm1.get('module_settings').get('assign_leave_management_rules').markAsTouched();
            this.moduleForm1.get('module_settings').get('assign_performance_management').markAsTouched();
            // this.moduleForm1.get('module_settings').get('assaign_employee_number').markAsTouched();
            this.moduleForm1.get('module_settings').get('onboarding_template').markAsTouched();
        }
    };
    // Author:Suresh M, Date:30-04-19
    // Standard Onboarding Summary All Data  
    OnBoardingComponent.prototype.standardSummarySubmit = function () {
        var _this = this;
        console.log(this.jobObj);
        var postData = {
            personal: this.personalForm.value.personal,
            job: this.jobObj,
            compensation: this.jobForm.value.compensation,
            module_settings: this.moduleForm.value.module_settings,
            position_work_schedule_id: this.positionWorkScheduleId
        };
        console.log("personalllll", postData);
        this.onboardingService.standardNewHireAllData(postData)
            .subscribe(function (res) {
            console.log("Sandard All Tabssss Dataaa", res);
            if (res.status == true) {
                _this.skipReportsto = false;
                _this.swalAlertService.SweetAlertWithoutConfirmation("Onboard", "Standard Onboarding Successfully Added", "success");
                jQuery("#myModal").modal("hide");
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Standard Added Successfully", "", "error");
            }
            jQuery("#myModal").modal("hide");
            _this.personalForm.reset();
            _this.jobForm.reset();
            _this.moduleForm.reset();
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Standard Added Successfully", err.error.message, "error");
        });
    };
    // Author:Suresh M, Date:30-04-19
    // new hire Hr Proceed Submit
    OnBoardingComponent.prototype.customSummarySubmit = function () {
        var _this = this;
        console.log("New Hire Dataa", this.moduleForm1.value);
        if (this.custonWizardID) {
            this.moduleForm1.value['_id'] = this.custonWizardID;
            console.log("Update iDDDDDDD", this.moduleForm1.value);
            this.onboardingService.newHireHrSummaryReviewData(this.moduleForm1.value)
                .subscribe(function (res) {
                console.log("New Hire All Tabs Dataaaaaa", res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Custom edited successfully", "success");
                    jQuery("#myModal1").modal("hide");
                }
                else {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Something wrong!", "Error");
                }
                jQuery("#myModal1").modal("hide");
                _this.GetAllCustomData();
                _this.moduleForm1.reset();
            });
        }
        else {
            this.onboardingService.newHireHrSummaryReviewData(this.moduleForm1.value)
                .subscribe(function (res) {
                console.log("New Hire All Tabs Dataaaaaa", res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Add", "Custom added successfully", "success");
                }
                else {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Add", "Something wrong!", "Error");
                }
                jQuery("#myModal1").modal("hide");
                _this.GetAllCustomData();
                _this.moduleForm1.reset();
            });
        }
    };
    // Author:Suresh M, Date:01-05-19
    // Goto FiledMaintaince Routerlink
    OnBoardingComponent.prototype.goToFieldMaintains = function () {
        this.router.navigate(['/admin/admin-dashboard/company-settings/company-setup/field-maintenance']);
        jQuery("#myModal1").modal("hide");
    };
    // Author:Suresh M, Date:01-05-19
    // Get All New Hire hr data
    OnBoardingComponent.prototype.GetAllCustomData = function () {
        var _this = this;
        this.onboardingService.getAllNewHireHRData()
            .subscribe(function (res) {
            console.log("Get All Custom New hire hrrrrrr", res);
            _this.getAllCustomHR = res.data;
        });
    };
    // Author:Suresh M, Date:01-05-19
    // Custom new hire hr Deletee
    OnBoardingComponent.prototype.customHrDelete = function () {
        var _this = this;
        if (this.deleteId.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete", "Select any data", "error");
        }
        else {
            this.onboardingService.customNewHrDelete(this.deleteId)
                .subscribe(function (res) {
                console.log("Custom Deletee", res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Delete", "Custom deleted successfully", "success");
                }
                else {
                }
                _this.GetAllCustomData();
            });
        }
    };
    // Author:Suresh M, Date:01-05-19
    // Custom new hire hr Edittt
    OnBoardingComponent.prototype.customEdit = function () {
        if (this.deleteId.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Select only one", "error");
        }
        else if (this.deleteId.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit", "Select any one", "error");
        }
        else {
            this.openEdit.nativeElement.click();
            this.getCustomeWizardData(this.deleteId);
        }
    };
    OnBoardingComponent.prototype.openstandpopupedit = function (id) {
        // this.openEdit.nativeElement.click();
        this.getCustomeWizardData(id);
    };
    OnBoardingComponent.prototype.getCustomeWizardData = function (id) {
        var _this = this;
        this.onboardingService.getCustomNewHireWizard(id)
            .subscribe(function (res) {
            console.log("Get the Dataaaaaa", res);
            _this.customNewhrEdit = res.data.module_settings;
            _this.moduleForm1.get('module_settings').patchValue(_this.customNewhrEdit);
            console.log("patch valueeee", _this.moduleForm1.get('module_settings').value, _this.customNewhrEdit);
            // this.moduleForm1.get('module_settings').reset();
        });
    };
    // Author:Suresh M, Date:24-04-19
    // Phone number function
    OnBoardingComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    // Author:Suresh M, Date:24-04-19
    // Phone number function
    OnBoardingComponent.prototype.keyPress1 = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    OnBoardingComponent.prototype.keyPress2 = function (event) {
        var pattern = /[0-9\+\.\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    // Author:Suresh M, Date:29-04-19
    // Api hit in ID Module Settings
    OnBoardingComponent.prototype.onboardTemplate = function ($event) {
        var _this = this;
        console.log("onboardingggg", $event);
        this.tempId = $event.value;
        console.log("temp IDDDD", this.tempId);
        this.onboardingService.selectOnboardTemplate(this.tempId)
            .subscribe(function (res) {
            console.log("Module Setting IDDDDD", res);
            _this.template = res.data;
        });
    };
    // Author:Suresh M, Date:29-04-19
    // view on board procceed
    OnBoardingComponent.prototype.viewOnboard = function () {
        this.selectedNav = "tab6";
        this.onboardNewHire = false;
    };
    // Author:Suresh M, Date:02-05-19
    // view on board procceed
    OnBoardingComponent.prototype.getTemplates = function () {
        var _this = this;
        this.onboardingService.getModulesData(this.moduleForm.value)
            .subscribe(function (res) {
            console.log("Get Templatesss", res);
            _this.getModulesTemplates = res.data;
        });
    };
    // Author:Suresh M, Date:02-05-19
    // Site Access Roles in company ID
    OnBoardingComponent.prototype.accessRoleId = function ($event) {
        console.log("Roless IDDDD", $event);
    };
    // Author:Suresh M, Date:26-04-19
    // Tab active in Standard Wizard
    OnBoardingComponent.prototype.personalFieldss = function (event) {
        this.selectedNav = event;
        if (event == "ClientInfo") {
            this.ClientInfo = true;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab8 = false;
        }
        else if (event == 'tab2') {
            this.ClientInfo = false;
            this.tab2 = true;
            this.tab3 = false;
            this.tab4 = false;
            this.tab8 = false;
        }
        else if (event == 'tab3') {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = true;
            this.tab4 = false;
            this.tab8 = false;
        }
        else if (event == 'tab4') {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = true;
            this.tab8 = false;
        }
        else if (event == 'tab6') {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab6 = true;
            this.tab8 = false;
        }
        else if (event == 'tab8') {
            this.ClientInfo = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab8 = true;
            console.log(this.personalForm);
        }
    };
    // Author:Suresh M, Date:26-04-19
    // Choose steps 
    OnBoardingComponent.prototype.chooseSteps = function (event) {
        this.selectedNewHire = event;
        if (event == "ClientInfo1") {
            this.ClientInfo1 = true;
            this.tab15 = false;
        }
        else if (event == 'tab15') {
            this.ClientInfo1 = false;
            this.tab15 = true;
        }
    };
    /* Description: standard onboardinng template open
       author : vipin reddy */
    OnBoardingComponent.prototype.getStandardOnboardingtemplate = function (id) {
        var _this = this;
        this.onboardingService.getStandardOnboardingtemplate(id)
            .subscribe(function (res) {
            console.log(res);
            _this.template = res.data;
            _this.templateId = res.data._id;
        }, function (err) {
            console.log(err);
        });
    };
    OnBoardingComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    // Author:Suresh M, Date:26-04-19
    // getAllWorkSchdules
    OnBoardingComponent.prototype.getAllWorkSchdules = function () {
        var _this = this;
        this.onboardingService.getAllWorkSchedule({ jobTitle: this.jobTitle1 })
            .subscribe(function (res) {
            console.log("work schudles", res);
            _this.workSchedule = res.data;
            console.log("work schdule 1111111", _this.workSchedule);
        });
    };
    // Author:Suresh M, Date:26-04-19
    // applyWorkSchedule
    OnBoardingComponent.prototype.applyWorkSchedule = function (event) {
        console.log("applayyy", event);
        this.applyWorks = event.value;
        console.log("eventtsssss", this.applyWorks);
        if (this.applyWorks == true) {
            this.leaveConditon = true;
        }
        else {
            this.leaveConditon = false;
        }
    };
    // Author:Suresh M, Date:26-04-19
    // scheduleType chnage events
    OnBoardingComponent.prototype.scheduleType = function (event) {
        console.log("typeeeeeee", event);
        this.scheduleTypeEvents = event.value;
        if (this.scheduleTypeEvents == "Standard") {
            this.createEmployee = false;
            this.positionScheduleShow = true;
        }
        else {
            this.createEmployee = true;
            this.positionScheduleShow = false;
        }
        console.log("schedule Typeeeee", this.scheduleTypeEvents);
        // var standardData = {
        //   schedulename: this.scheduletitle,
        //   scheduletype: this.scheduleTypeEvents
        // }
        // if (this.scheduleTypeEvents == 'Standard') {
        //   this.onboardingService.standardScheduleData(standardData)
        //     .subscribe((res: any) => {
        //       console.log("standard dataaa", res);
        //       this.createEmployee = false;
        //       this.standardSchedule = res.result[0].week_applicable
        //       console.log("standardsssss", this.standardSchedule);
        //       this.standardID = res.result[0]._id;
        //       console.log("standardiddd", this.standardID);
        //     });
        // } else {
        //   this.createEmployee = true;
        // }
    };
    OnBoardingComponent.prototype.getPositionSchedule = function (id) {
        var _this = this;
        this.positionWorkScheduleId = id;
        this.leaveManagementService.getPosition(id).subscribe(function (res) {
            console.log(res);
            _this.scheduleData = res.data.weekApplicable;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Suresh M, Date: 17/06/19
    // Select week days
    OnBoardingComponent.prototype.selectDay = function (event, day, isChecked) {
        console.log(event, day);
        this.allDays = event;
        console.log("dayssss offfff", this.allDays);
        this.selectDayOfWeek = day;
        if (event == true) {
            if (this.selectedDays) {
                this.selectedDays.push(day);
            }
            this.weekDays.push({
                day: day,
                shiftStartTime: '',
                shiftEndTime: '',
            });
        }
        else if (event == false) {
            for (var i = 0; i < this.selectedDays.length; i++) {
                if (this.selectedDays[i] === day) {
                    this.selectedDays.splice(i, 1);
                }
            }
            for (var i = 0; i < this.weekDays.length; i++) {
                if (this.weekDays[i].day === day) {
                    this.weekDays.splice(i, 1);
                }
            }
        }
        console.log(this.weekDays);
        console.log("dayssssssssssssssss", this.selectedDays);
    };
    // Author: Suresh M, Date: 17/06/19
    // createWorkPosition submit
    OnBoardingComponent.prototype.createWorkPosition = function (createEmployeeForm) {
        var _this = this;
        var data = {
            scheduleTitle: this.positionSchedule.scheduleTitle,
            scheduleStartDate: this.positionSchedule.scheduleStartDate,
            type: this.positionSchedule.type,
            totalHrsPerDay: this.positionSchedule.totalHrsPerDay,
            totalHrsPerWeek: this.positionSchedule.totalHrsPerWeek,
            weekApplicable: this.weekDays,
            daysOfWeek: this.selectedDays,
        };
        if (!this.positionSchedule.scheduleTitle) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.scheduleStartDate) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.type) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.totalHrsPerDay) {
            this.isValid = true;
        }
        else if (!this.positionSchedule.totalHrsPerWeek) {
            this.isValid = true;
        }
        else {
            this.leaveManagementService.createPosition(data)
                .subscribe(function (res) {
                console.log("create submit dataaaa", res);
                jQuery("#myModal5").modal("hide");
                createEmployeeForm.resetForm();
                _this.positionSchedule = {
                    scheduleTitle: '',
                    scheduleStartDate: '',
                    type: '',
                    totalHrsPerDay: '',
                    totalHrsPerWeek: '',
                };
                _this.selectedDays = [];
                _this.isValid = false;
                _this.weekDays = [];
                _this.positionWorkScheduleId = res.data._id;
                console.log("create idddddd", _this.positionWorkScheduleId);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Suresh M, Date: 17/06/19
    // standardProceed
    OnBoardingComponent.prototype.standardProceed = function () {
        console.log("RRRRRRRRRRR", this.WorkScheduleData, this.scheduletitle, this.workScheduleType);
        if (this.applyWorks == true) {
            if (!this.WorkScheduleData) {
                this.isNotValid = true;
            }
            // else if (!this.scheduletitle) {
            //   this.isNotValid = true;
            // }
            else if (!this.workScheduleType) {
                this.isNotValid = true;
            }
            else {
                console.log("wwwwwwwwww", this.selectedNav);
                this.selectedNav = "tab8";
                this.onboardNewHire = false;
            }
        }
        else {
            console.log("wwwwwwwwww", this.selectedNav);
            this.selectedNav = "tab8";
            this.onboardNewHire = false;
        }
    };
    // Author: Suresh M, Date: 17/06/19
    // totalHoursDay
    OnBoardingComponent.prototype.totalHoursDay = function (event) {
        console.log("hoursss", event);
        if (this.selectedDays.length) {
            var num = event;
            var min, hrs;
            this.positionSchedule.totalHrsPerWeek = [];
            this.weekDays.shiftStartTime = [];
            this.weekDays.shiftEndTime = [];
            if (num) {
                min = num.split(':');
            }
            // console.log(min[0], "minutes afcter");
            hrs = min[0] * 60;
            if (min[1] && min[1] !== '') {
                console.log(min, "minutes afcter");
                hrs = parseInt(hrs) + parseInt(min[1]);
            }
            this.positionSchedule.totalHrsPerWeek = this.selectedDays.length * hrs / 60;
            this.weekDays.shiftStartTime = [];
        }
        // else if (event.length){
        //   console.log("eventssss", event.length);
        //   this.positionSchedule.total_hrs_week = this.selectedDays.length * hrs / 60;
        // }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("", "Select days of the week applicable", "Error");
        }
        console.log(hrs);
    };
    // Author: Suresh M, Date: 17/06/19
    // startTimeDay
    OnBoardingComponent.prototype.startTimeDay = function (event, i) {
        console.log("timeeeeee", i);
        this.startTime = event;
        console.log("starttt", this.startTime);
        //  this.weekDays[i].end_time ="";
        console.log("11111111111", this.weekDays);
    };
    // Author: Suresh M, Date: 17/06/19
    // endTimeDays
    OnBoardingComponent.prototype.endTimeDays = function (event, i) {
        console.log("endddddd", event);
        this.endTimeValue = event;
        var t0 = this.weekDays[i].shiftStartTime;
        var t1 = event;
        console.log("t00000", t0, t1);
        var diff = this.getTimeDifference(t0, t1);
        console.log("timeeee difffff", diff);
        var d = parseInt(diff.split(':')[0]);
        this.startTimeDifference = d;
        var p = parseInt(this.positionSchedule.totalHrsPerDay);
        console.log(d, typeof d, typeof d);
        console.log(d > p || d < p);
        // this.weekDays[i].end_time=event;
        if (d !== p) {
            console.log(this.weekDays);
            // this.weekDays[i].end_time='';
            console.log(this.weekDays);
            this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Please check the correct time duration ", "error");
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Time Duration", "Verify time duration sucessfully", "success");
        }
    };
    OnBoardingComponent.prototype.timeStringToMins = function (s) {
        s = s.split(':');
        s[0] = /m$/i.test(s[1]) && s[0] == 12 ? 0 : s[0];
        return s[0] * 60 + parseInt(s[1]) + (/pm$/i.test(s[1]) ? 720 : 0);
    };
    // Return difference between two times in hh:mm[am/pm] format as hh:mm
    OnBoardingComponent.prototype.getTimeDifference = function (t0, t1) {
        // Small helper function to padd single digits
        function z(n) { return (n < 10 ? '0' : '') + n; }
        // Get difference in minutes
        var diff = this.timeStringToMins(t1) - this.timeStringToMins(t0);
        // Format difference as hh:mm and return
        return z(diff / 60 | 0) + ':' + z(diff % 60);
    };
    OnBoardingComponent.prototype.omit_special_char = function (event) {
        var k;
        k = event.charCode; //         k = event.keyCode;  (Both can be used)
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    };
    OnBoardingComponent.prototype.jobTitleForPayrollData = function () {
        console.log(this.jobTitle1, this.jobForm.get('job').get('jobTitle').value);
        this.amount = '';
        // this.jobForm.get('compensation').get('amount').patchValue('')
        this.calculateCompensation = '';
        // this.jobForm.get('compensation').get('compensationRatio').patchValue('')
        this.getJobSalarayLinks();
        this.getAllWorkSchdules();
    };
    OnBoardingComponent.prototype.getJobSalarayLinks = function () {
        var _this = this;
        if (this.jobTitle1) {
            this.payRollService.getJobLinks(this.jobTitle1)
                .subscribe(function (res) {
                console.log(res);
                if (res.data.EeoJobCategoryDetails) {
                    _this.EEO_Job_Category = res.data.EeoJobCategoryDetails.name;
                    _this.jobForm.get('job').get('EEO_Job_Category').patchValue(res.data.EeoJobCategoryDetails.name);
                }
                if (res.data.salary_grade) {
                    _this.jobForm.get('compensation').get('Salary_Grade').patchValue(res.data.salary_grade._id);
                    _this.Salary_Grade = res.data.salary_grade._id;
                    _this.salarygradelistChange(res.data.salary_grade._id);
                }
                if (!res.data.salary_grade) {
                    _this.Salary_Grade = '';
                    _this.jobForm.get('compensation').get('Salary_Grade').patchValue('');
                    _this.jobForm.get('compensation').get('grade_band').patchValue('');
                    _this.grade_band = '';
                }
                if (!res.data.EeoJobCategoryDetails) {
                    _this.EEO_Job_Category = '';
                    _this.jobForm.get('job').get('EEO_Job_Category').patchValue('');
                }
                // this.jobForm.get('compensation').get('grade_band').patchValue(res.data.grade_band._id)
                console.log(_this.EEO_Job_Category, _this.jobForm.get('job').get('EEO_Job_Category').value);
            }, function (err) {
                console.log(err);
            });
        }
    };
    OnBoardingComponent.prototype.getJobSalarayLinks1 = function () {
        var _this = this;
        this.payRollService.getJobLinks(this.jobTitle1)
            .subscribe(function (res) {
            console.log(res, res.data.EeoJobCategoryCode);
            _this.jobForm.get('compensation').get('grade_band').patchValue(res.data.grade_band._id);
            _this.grade_band = res.data.grade_band._id;
        }, function (err) {
            console.log(err);
        });
        console.log(this.jobForm.get('compensation').get('grade_band').value, "12121212vipn");
    };
    OnBoardingComponent.prototype.salarygradelistChange = function (id) {
        var _this = this;
        this.payRollService.getSingleSalaryGrade(this.companyID, id)
            .subscribe(function (res) {
            console.log(res);
            _this.salGradeBands = res.data.grades;
        }, function (err) {
            console.log(err);
        });
        this.getJobSalarayLinks1();
    };
    OnBoardingComponent.prototype.getPayrollGroup = function () {
        var _this = this;
        var postData = {
            companyId: this.companyID,
        };
        this.payRollService.getPayrollGroup(postData)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.payrollGroups = res.data;
                // this.payrollGroupCount = res.total_count;
            }
        }, function (err) {
            console.log(err);
        });
    };
    OnBoardingComponent.prototype.payGroupChange = function () {
        var _this = this;
        console.log(this.Pay_group);
        if (this.Pay_group) {
            this.payRollService.getSinglePayrollGroup(this.companyID, this.Pay_group)
                .subscribe(function (res) {
                console.log(res);
                _this.Pay_frequency = res.data.pay_frequency;
                _this.jobForm.get('compensation').get('Pay_frequency').patchValue(res.data.pay_frequency);
            }, function (err) {
                console.log(err);
            });
        }
    };
    OnBoardingComponent.prototype.getsalrayGrades = function () {
        var _this = this;
        var postData = {
            "companyId": this.companyID,
        };
        this.payRollService.getSalrayGradeList(postData)
            .subscribe(function (res) {
            console.log(res);
            _this.salaryGradeList = res.data;
            // this.salaryGradeCount = res.total_count
        }, function (err) {
            console.log(err);
        });
    };
    OnBoardingComponent.prototype.payRateChangeforCompensation = function () {
        var _this = this;
        this.compensationRatio = '';
        this.jobForm.get('compensation').get('compensationRatio').patchValue('');
        var postData = {
            "payRate": this.amount,
            "payType": this.payType1,
            "pay_frequency": this.Pay_frequency,
            "grade_band": "",
            "workHours": this.workHours
        };
        if (this.grade_band) {
            postData.grade_band = this.grade_band;
        }
        if (postData.payRate && postData.payType && postData.pay_frequency) {
            this.payRollService.compensationRationCalculate(postData)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.compensationError = false;
                    _this.calculateCompensation = res.data;
                    _this.jobForm.get('compensation').get('compensationRatio').patchValue(_this.calculateCompensation.compensationRatio);
                    _this.compensationRatio = _this.calculateCompensation.compensationRatio;
                }
            }, function (err) {
                console.log(err);
                _this.compensationError = true;
                // this.swalAlertService.SweetAlertWithoutConfirmation('Compensation Ratio',err.error.message,'error');
            });
        }
    };
    OnBoardingComponent.prototype.nonPaidPositionChange = function () {
        console.log(this.NonpaidPosition);
        this.compensationObj = this.jobForm.get('compensation').value;
        if (this.NonpaidPosition == false) {
            this.compensationError = false;
        }
        else {
            // delete this.compensationObj.Salary_Grade 
            // delete this.compensationObj.Pay_group 
            // delete this.compensationObj.Pay_frequency 
            // delete this.compensationObj.compensationRatio 
            // delete this.compensationObj.grade_band 
            // delete this.compensationObj.payRate.amount
            // delete this.compensationObj.payRate.payType 
            // delete this.compensationObj.workHours
            // this.jobForm.get('compensation').get('Pay_group').markAsTouched();
            // this.jobForm.get('compensation').get('Pay_frequency').markAsTouched();
            // this.jobForm.get('compensation').get('First_Pay_Date').markAsTouched();
            // this.jobForm.get('compensation').get('compensationRatio').markAsTouched();
            // this.jobForm.get('compensation').get('Salary_Grade').markAsTouched();
            // this.jobForm.get('compensation').get('grade_band').markAsTouched();
            // this.jobForm.get('compensation').get('payRate').get('amount').markAsTouched();
            // this.jobForm.get('compensation').get('payRate').get('payType').markAsTouched();
            // this.jobForm.get('compensation').get('workHours').markAsTouched();
        }
    };
    OnBoardingComponent.prototype.getBaseRoles = function () {
        var _this = this;
        this.siteAccessRolesService.getBaseRoles()
            .subscribe(function (res) {
            console.log("roles", res);
            _this.resRolesObj = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    OnBoardingComponent.prototype.skipTemplate = function (event) {
        console.log(event);
        this.skipOnboardTemplate = event.checked;
        this.onboarding_template = '';
        if (event.checked == true) {
            this.moduleForm.get('module_settings').get('onboarding_template').patchValue('');
        }
    };
    OnBoardingComponent.prototype.skipReportsChange = function () {
        console.log(this.skipReportsto);
        var HContact = this.jobForm.get('job').get('HR_Contact');
        var Reporter = this.jobForm.get('job').get('ReportsTo');
        this.skipReportsto = !this.skipReportsto;
        // this.jobForm.get('job').get('HR_Contact').
        if (this.skipReportsto == true) {
            HContact.clearValidators();
            Reporter.clearValidators();
            HContact.updateValueAndValidity();
            Reporter.updateValueAndValidity();
            this.ReportsTo = '';
            this.HR_Contact = '';
            this.jobObj = this.jobForm.get('job').value;
            console.log(this.jobObj);
            delete this.jobObj.HR_Contact;
            delete this.jobObj.ReportsTo;
            console.log(this.jobObj, 'when checked no value');
        }
        if (this.skipReportsto == false) {
            HContact.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            Reporter.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            HContact.updateValueAndValidity();
            Reporter.updateValueAndValidity();
            this.jobObj = this.jobForm.get('job').value;
            console.log(this.jobObj);
            this.jobObj.HR_Contact = this.jobForm.get('job').get('HR_Contact').value;
            this.jobObj.ReportsTo = this.jobForm.get('job').get('ReportsTo').value;
            console.log(this.jobObj, 'when unchecked value in');
        }
        // console.log(this.jobObj)
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('client_info'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "client_info", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('scrollTop'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "scrollTop", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('job_compensation'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "job_compensation", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('module_Settings'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "module_Settings", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('view_Onboard'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "view_Onboard", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('leave_management'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "leave_management", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('client_info1'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "client_info1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], OnBoardingComponent.prototype, "openEdit", void 0);
    OnBoardingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-on-boarding',
            template: __webpack_require__(/*! ./on-boarding.component.html */ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.html"),
            styles: [__webpack_require__(/*! ./on-boarding.component.css */ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_2__["OnboardingService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_1__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _services_workflow_service__WEBPACK_IMPORTED_MODULE_5__["WorkflowService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_6__["SiteAccessRolesService"],
            amazing_time_picker__WEBPACK_IMPORTED_MODULE_7__["AmazingTimePickerService"],
            _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_8__["PayRollService"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_9__["LeaveManagementService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_10__["LoaderService"]])
    ], OnBoardingComponent);
    return OnBoardingComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.module.ts ***!
  \***************************************************************************************/
/*! exports provided: OnBoardingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnBoardingModule", function() { return OnBoardingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _on_boarding_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./on-boarding.routing */ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.routing.ts");
/* harmony import */ var _on_boarding_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./on-boarding.component */ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.ts");
/* harmony import */ var _standard_onboard_template_standard_onboard_template_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./standard-onboard-template/standard-onboard-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./edit-standard-onboarding-template/edit-standard-onboarding-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _custom_onboard_template_custom_onboard_template_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./custom-onboard-template/custom-onboard-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.ts");
/* harmony import */ var amazing_time_picker__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! amazing-time-picker */ "./node_modules/amazing-time-picker/amazing-time-picker.es5.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var ngx_material_timepicker__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-material-timepicker */ "./node_modules/ngx-material-timepicker/fesm5/ngx-material-timepicker.js");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var OnBoardingModule = /** @class */ (function () {
    function OnBoardingModule() {
    }
    OnBoardingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _on_boarding_routing__WEBPACK_IMPORTED_MODULE_2__["OnboardingRouting"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_9__["MatDatepickerModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], amazing_time_picker__WEBPACK_IMPORTED_MODULE_14__["AmazingTimePickerModule"], ngx_material_timepicker__WEBPACK_IMPORTED_MODULE_16__["NgxMaterialTimepickerModule"], ngx_mask__WEBPACK_IMPORTED_MODULE_17__["NgxMaskModule"].forRoot({
                    showMaskTyped: true,
                })
            ],
            declarations: [_on_boarding_component__WEBPACK_IMPORTED_MODULE_3__["OnBoardingComponent"], _standard_onboard_template_standard_onboard_template_component__WEBPACK_IMPORTED_MODULE_4__["StandardOnboardTemplateComponent"],
                _edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_11__["EditStandardOnboardingTemplateComponent"], _custom_onboard_template_custom_onboard_template_component__WEBPACK_IMPORTED_MODULE_13__["CustomOnboardTemplateComponent"]],
            entryComponents: [
                _edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_11__["EditStandardOnboardingTemplateComponent"]
            ],
            providers: [_services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_15__["LeaveManagementService"]]
        })
    ], OnBoardingModule);
    return OnBoardingModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.routing.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.routing.ts ***!
  \****************************************************************************************/
/*! exports provided: OnboardingRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingRouting", function() { return OnboardingRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _on_boarding_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./on-boarding.component */ "./src/app/admin-dashboard/employee-management/on-boarding/on-boarding.component.ts");
/* harmony import */ var _standard_onboard_template_standard_onboard_template_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./standard-onboard-template/standard-onboard-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.ts");
/* harmony import */ var _custom_onboard_template_custom_onboard_template_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./custom-onboard-template/custom-onboard-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/custom-onboard-template/custom-onboard-template.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    // { path:'',redirectTo:"/on-boarding",pathMatch:"full" },
    { path: '', component: _on_boarding_component__WEBPACK_IMPORTED_MODULE_2__["OnBoardingComponent"] },
    { path: 'onboardingTemplate/:id', component: _standard_onboard_template_standard_onboard_template_component__WEBPACK_IMPORTED_MODULE_3__["StandardOnboardTemplateComponent"] },
    { path: 'custom-onboard-template/:id', component: _custom_onboard_template_custom_onboard_template_component__WEBPACK_IMPORTED_MODULE_4__["CustomOnboardTemplateComponent"] },
    { path: 'custom-onboard-template', component: _custom_onboard_template_custom_onboard_template_component__WEBPACK_IMPORTED_MODULE_4__["CustomOnboardTemplateComponent"] }
];
var OnboardingRouting = /** @class */ (function () {
    function OnboardingRouting() {
    }
    OnboardingRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OnboardingRouting);
    return OnboardingRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.css":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.css ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal{\n    padding: 0px 40px;\n    float : none;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important;\n\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.zenwork-margin-zero{\n    margin: 20px 0 !important;\n}\n.zenwork-margin-zero b{\n    font-size: 16px!important;\n    color: #404040;\n}\n.zenwork-padding-10{\n    padding: 10px;\n}\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 3px 10px;\n    font-size: 10px;\n    margin:  0 10px 0 0;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.zenwork-inner-icon{\n    width: 20px;\n    height: auto;\n}\n.text-field{\n    border: none; \n    width: 20%;\n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n}\n.template-name{\n    margin: 30px 0 0 0;\n}\n.template-name label{\n    line-height: 40px;\n}\n.add-template-main{\n    width:100%;\n    border-bottom: 1px solid #eeeeee;\n\n}\n.field-accordion p {color: #565555;    font-size: 15px;\n    background: #fff;\n    margin: 0px 0 0 0px;}\n.field-accordion .panel-default>.panel-heading { padding: 20px 0 20px; border: none;}\n.field-accordion .panel-title {  display: inline-block; padding: 0 0 0 7px;}\n.field-accordion .panel-title a:hover { text-decoration: none;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;background: #fff;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n.add-fields{\n    width:80%;\n    float: right;\n    margin-top:20px;\n\n}\n.main-onboarding .panel-heading{\n    padding:10px 0px!important;\n}\n.main-onboarding{\n    width:100%;\n    margin-top: 20px;\n}\n.main-onboarding .panel-font{\n    font-weight: 600;\n    padding: 0 0 0 20px;    \n}\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n.settings-dropdown{\n    float:right;\n    background: #fff;\n    border: none;\n}\n.main-onboarding .panel-heading .btn{\n    padding:2px 6px!important;\n    border-radius: 0!important;\n}\n.caret{\n    margin-bottom: 5px!important;\n}\n.panel-body{\n    padding:0px 15px 0px 15px !important;\n}\n.hr-tasks{\n    font-size: 16px;\n    font-weight: 600;\n    padding: 10px 0 0 40px;\n}\n.hire-forms{\n    font-size: 16px;\n    font-weight: 600;\n    padding: 0px 0 0 27px;\n}\n.individual-hr-task{\n    font-size:15px;\n    padding: 15px 0;\n    margin: 0;\n}\n.main-onboarding .setting-drop .dropdown-menu{\n    left: -76px !important;\n    min-width: 109px !important;\n}\n.individual-hr-task small{\n    padding: 0 0 0 28px;\n    color: #ccc;\n}\n.margin-top-zero{\n    margin-top:0px!important;\n}\n.padding-top-remove{\n    padding-top:0px!important;\n}\n.team-introdution-card{\n    background-color: #fcfcfc;\n    border-radius: 2px;\n    padding: 10px;\n}\n.margin-zero{\n    margin:0px!important;\n}\n.team-introdution-card .padding-top-remove small{\n    padding-left:25px; \n    color: #ccc;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.card-intro{\n    font-size: 15px;\n    padding-left: 30px;\n}\n.team-introdution-card .mat-checkbox{\n    display: block;\n}\n.team-introdution-card .individual-hr-task{\n    font-weight: 600;\n    display: block;\n    font-size: 16px;\n}\n.team-introdution-card small{\n    font-size: 14px;\n}\n.emp-image{\n    padding-top: 0;\n    width: 43px;\n    display: inline-block;\n    vertical-align: middle;\n  }\n.employee-details{\n    width: 70px;\n    display: inline-block;\n    vertical-align: middle;\n    text-align: left;\n    margin-left: 10px;\n  }\n.employee-image-info{\n    padding-left: 20px;\n  }\n.emp-name{\n    font-size: 11px;\n    padding-left: 0;\n    display: block;\n    line-height: 11px;\n  }\n.borderright{\n      border-right: 1px solid #eee;\n  }\n.team-text-area{\n    margin-top:10px;\n  }\ntextarea{\n     resize: none;\n     border: 0;\n     box-shadow: none;\n     background: #f8f8f8;\n  }\n.btn-color{\n    color: #ef8086;\n  }\n.new-emp-paperwork{\n    margin-top: 20px;\n    padding-left: 15px;\n  }\n.new-emp-paperwork .emp-paperwork-heading{\n    font-size: 12px;\n    font-weight: 600;\n    padding: 10px 0 0 0;\n  }\n.newhire-packets{\n    display: inline-block;\n    width:234px;\n\n}\n.newhire-checkbox{\n    display: inline-block;\n    vertical-align: top;\n    margin-top: 10px;\n}\n.newhire-content{\n    display: inline-block;\n    vertical-align: middle;\n  }\n.insert-drive{\n  width: 11px;\n  display: inline-block;\n  vertical-align: middle;\n  }\n.image-items{\n      display: inline-block;\n      padding:0px 7px;\n  }\n.open>.settings-dropdown.btn-default{\n    background: #fff;\n}\n.settings-dropdown:hover,.settings-dropdown:active{\n    background: #fff !important;\n}\n.buttons{\n    margin: 20px 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n.error{\n    font-size:12px !important;\n    color: #f44336 !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.html":
/*!************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.html ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"personal\">\n  <!-- <div class=\"zenwork-currentpage\"> -->\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management/on-boarding']\">\n      <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n    </a>\n    <span class=\"inner-icon-img\">\n      <img src=\"../../../assets/images/Onboarding/Group 828.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n    </span>\n    <small class=\"sub-title\"><b>Onboarding Template - Standard Onboard Template</b></small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n  <!-- </div> -->\n\n\n  <div class=\"heading\">\n    <h4>Edit - Onboarding Template</h4>\n  </div>\n\n  <div class=\"template-name\">\n    <label>Onboarding Template Name</label><br>\n\n    <input type=\"text\" class=\"form-control text-field\" value = \"Standard Onboarding Template\" placeholder=\"Template Name\" readonly>\n  </div>\n\n  <div class=\"main-onboarding on-boarding-template\">\n    <!-- <div class=\"panel panel-info\"> -->\n    <div class=\"panel-heading\">\n\n      <div class=\"field-accordion\">\n\n        <div class=\"panel-group\" id=\"accordion1\">\n\n          <div class=\"panel panel-default border-bottom\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n                  <span class=\"panel-title panel-font pull-left\">Onboarding Tasks </span>\n                </a>\n              </h4>\n              <span class=\"pull-right\">\n                <div *ngIf = \"addShowHide\" class=\"setting-drop\">\n                  <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                    <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                  </a>\n                  <ul class=\"dropdown-menu\">\n\n                    <li > \n                      <a (click)=\"openDialog()\">Edit</a>\n                    </li>\n\n                  </ul>\n                </div>\n              </span>\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n          <div id=\"collapseOne\" class=\"panel-collapse collapse in\" *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\">\n            <p class=\"hr-tasks\">{{task}}</p>\n            <div *ngFor=\"let data of template['Onboarding Tasks'][task]\" class=\"panel-body\">\n\n              <p class=\"individual-hr-task\">\n                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\" (ngModelChange)=\"singleTaskChoose(task,data.taskName,data.isChecked)\">{{data.taskName}}</mat-checkbox><br>\n                <small>{{data.taskAssignee}}-{{data.Date | date}}</small>\n              </p>\n\n              <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n\n            </div>\n          </div>\n        </div>\n      </div>\n      \n    </div>\n\n    <div class=\"clearfix\"></div>\n\n   \n    <!-- <hr class=\"zenwork-margin-ten-zero margin-zero\"> -->\n\n\n  </div>\n\n  <div class=\"buttons\">\n    <ul class=\"list-unstyled\">\n\n      <li class=\"list-items\">\n        <a class=\"cancel\">Cancel</a>\n      </li>\n      <li class=\"list-items pull-right\">\n        <button class='save' type=\"submit\">\n          Submit\n        </button>\n      </li>\n\n      <div class=\"clearfix\"></div>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.ts":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.ts ***!
  \**********************************************************************************************************************************/
/*! exports provided: StandardOnboardTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StandardOnboardTemplateComponent", function() { return StandardOnboardTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_onboarding_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/onboarding.service */ "./src/app/services/onboarding.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../edit-standard-onboarding-template/edit-standard-onboarding-template.component */ "./src/app/admin-dashboard/employee-management/on-boarding/edit-standard-onboarding-template/edit-standard-onboarding-template.component.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var StandardOnboardTemplateComponent = /** @class */ (function () {
    function StandardOnboardTemplateComponent(dialog, swalAlertService, onboardingService, route) {
        this.dialog = dialog;
        this.swalAlertService = swalAlertService;
        this.onboardingService = onboardingService;
        this.route = route;
        this.allTasks = [];
        this.popupData = {};
        this.addShowHide = false;
        /* Description: dummy Object
        author : vipin reddy Date:17-04-2019*/
        this.template = {
            "templateName": "Onboarding Template",
            "templateType": "base",
            "Onboarding Tasks": {}
        };
    }
    /* Description: ngFor loop object
   author : vipin reddy Date:17-04-2019*/
    StandardOnboardTemplateComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    StandardOnboardTemplateComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.allTasks.push(this.Manager_Tasks, this.IT_Setup, this.HR_Tasks, this.New_Hire_Forms)
        console.log(this.allTasks, "allTasks");
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log(_this.id);
        });
        this.getStandardOnboardingtemplate(this.id);
    };
    /* Description: get onboarding tasks
    author : vipin reddy Date:17-04-2019*/
    StandardOnboardTemplateComponent.prototype.getStandardOnboardingtemplate = function (id) {
        var _this = this;
        this.onboardingService.getStandardOnboardingtemplate(id)
            .subscribe(function (res) {
            console.log(res);
            _this.template = res.data;
            _this.templateId = res.data._id;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: single checkbox choose
    author : vipin reddy Date:17-04-2019*/
    StandardOnboardTemplateComponent.prototype.singleTaskChoose = function (taskArray, taskName, isChecked) {
        var _this = this;
        console.log(taskArray, taskName, isChecked);
        if (isChecked == false) {
            this.popupData = '';
            this.addShowHide = false;
        }
        if (isChecked == true) {
            for (var task in this.template["Onboarding Tasks"]) {
                console.log(task);
                this.template["Onboarding Tasks"][task].forEach(function (element) {
                    if (element.taskName === taskName) {
                        element.isChecked = true;
                        _this.popupData = element;
                        _this.addShowHide = true;
                        console.log("popdaata", _this.popupData);
                    }
                    else {
                        element.isChecked = false;
                    }
                });
            }
        }
        this.template["Onboarding Tasks"][taskArray];
        console.log("");
    };
    /* Description: edit standard onboarding template open EDIT popup
    author : vipin reddy */
    StandardOnboardTemplateComponent.prototype.openDialog = function () {
        var _this = this;
        this.popupData['templatename'] = 'standard';
        var data = {
            _id: this.templateId,
            data: this.popupData
        };
        if (data._id != '') {
            var dialogRef = this.dialog.open(_edit_standard_onboarding_template_edit_standard_onboarding_template_component__WEBPACK_IMPORTED_MODULE_3__["EditStandardOnboardingTemplateComponent"], {
                width: '1300px',
                backdropClass: 'structure-model',
                data: data,
            });
            dialogRef.afterClosed().subscribe(function (result) {
                _this.getStandardOnboardingtemplate(_this.id);
                console.log('The dialog was closed');
            });
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Task", "Atleast select one task", "error");
        }
    };
    StandardOnboardTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-standard-onboard-template',
            template: __webpack_require__(/*! ./standard-onboard-template.component.html */ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.html"),
            styles: [__webpack_require__(/*! ./standard-onboard-template.component.css */ "./src/app/admin-dashboard/employee-management/on-boarding/standard-onboard-template/standard-onboard-template.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _services_onboarding_service__WEBPACK_IMPORTED_MODULE_1__["OnboardingService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], StandardOnboardTemplateComponent);
    return StandardOnboardTemplateComponent;
}());



/***/ }),

/***/ "./src/app/services/onboarding.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/onboarding.service.ts ***!
  \************************************************/
/*! exports provided: OnboardingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingService", function() { return OnboardingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OnboardingService = /** @class */ (function () {
    function OnboardingService(http) {
        this.http = http;
    }
    OnboardingService.prototype.getStandardOnboardingtemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.editDataSubit = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/editTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.addCustomOnboardTemplate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/addEditTemplate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllCustomTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getCustomTemplates")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getStandardTemplates = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getStandardTemplate")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.deleteCategory = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTask", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllJobDetails = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getFieldsForAddEmployee");
    };
    OnboardingService.prototype.selectOnboardTemplate = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplate/" + id);
    };
    OnboardingService.prototype.getModulesData = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/getTemplates", data);
    };
    OnboardingService.prototype.deleteTemplate = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/onboard-template/deleteTemplate", id);
    };
    OnboardingService.prototype.standardNewHireAllData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addEmployee", data);
    };
    OnboardingService.prototype.newHireHrSummaryReviewData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addUpdateCustomWizard", data);
    };
    OnboardingService.prototype.getAllNewHireHRData = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getAllCustomWizards");
    };
    OnboardingService.prototype.customNewHrDelete = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/deleteCustomWizards", id);
    };
    OnboardingService.prototype.getCustomNewHireWizard = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getCustomWizard/" + id);
    };
    OnboardingService.prototype.getSiteAccesRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id);
    };
    OnboardingService.prototype.getStructureFields = function (cID) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + cID)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.preEmployeeIdGeneration = function (cId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/getEmployeeId/" + cId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    OnboardingService.prototype.getAllWorkSchedule = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/listPositionSchedules", { params: data });
    };
    OnboardingService.prototype.standardScheduleData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getempschedule", data);
    };
    OnboardingService.prototype.createEmployeeData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/employeespecific", data);
    };
    OnboardingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OnboardingService);
    return OnboardingService;
}());



/***/ })

}]);
//# sourceMappingURL=on-boarding-on-boarding-module.js.map