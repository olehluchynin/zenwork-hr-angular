(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["offboardingtab-offboardingtab-module"],{

/***/ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: OffboardingtabModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffboardingtabModule", function() { return OffboardingtabModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _offboardingtab_offboardingtab_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./offboardingtab/offboardingtab.component */ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.ts");
/* harmony import */ var _offboardingtab_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./offboardingtab.routing */ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab.routing.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var OffboardingtabModule = /** @class */ (function () {
    function OffboardingtabModule() {
    }
    OffboardingtabModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _offboardingtab_routing__WEBPACK_IMPORTED_MODULE_3__["OffboardingTabRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"]
            ],
            declarations: [_offboardingtab_offboardingtab_component__WEBPACK_IMPORTED_MODULE_2__["OffboardingtabComponent"]]
        })
    ], OffboardingtabModule);
    return OffboardingtabModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab.routing.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab.routing.ts ***!
  \**********************************************************************************************/
/*! exports provided: OffboardingTabRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffboardingTabRouting", function() { return OffboardingTabRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _offboardingtab_offboardingtab_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./offboardingtab/offboardingtab.component */ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/offboardingtab",pathMatch:"full" },
    { path: '', component: _offboardingtab_offboardingtab_component__WEBPACK_IMPORTED_MODULE_2__["OffboardingtabComponent"] },
];
var OffboardingTabRouting = /** @class */ (function () {
    function OffboardingTabRouting() {
    }
    OffboardingTabRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OffboardingTabRouting);
    return OffboardingTabRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.css":
/*!****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.css ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.education-date-list {\n    width:23%;\n    position: relative;\n    display: inline-block;vertical-align: top;margin: 0px 20px 0 0;\n}\n.education-date-list span { display: inline-block;}\n.education-date-list span.disable { display: inline-block; margin: 0 0 0 20px;}\n.education-date-list:nth-child(3){ margin:20px 0 0;}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 14px;\n    padding: 0 0 0 2px;\n}\n.birthdate{\n    font-size: 12px;\n    padding-left: 15px;\n    width: 178px;   \n    box-shadow: none;\n}\n.address-state-dropodown{\n    width: 100%;\n    border-radius: 0px!important;\n    height: 35px;\n    border: 0;\n    box-shadow: none;\n    background: #fff;\n}\n.main-onboarding .panel-heading{\n    padding:10px 15px!important;\n}\n.main-onboarding{\n    width:100%;\n    margin-top: 40px;\n    padding: 0 30px 0 0;\n}\n.main-onboarding .panel-font{\n    font-weight: 600;\n    margin-top: 5px;\n}\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n.borderright{\n      border-right: 1px solid #eee;\n  }\n.team-text-area{\n    margin-top:10px;\n  }\ntextarea{\n     resize: none;\n     border: 0;\n     box-shadow: none;\n     background: #f8f8f8;\n  }\n.btn-color{\n    border: 1px solid #EF8086;\n    color: #ef8086;\n  }\n.new-emp-paperwork{\n    margin-top: 20px;\n    padding-left: 15px;\n  }\n.new-emp-paperwork .emp-paperwork-heading{\n    font-size: 12px;\n    font-weight: 600;\n    padding: 10px 0 0 0;\n  }\n.newhire-packets{\n    display: inline-block;\n    width:234px;\n\n}\n.newhire-checkbox{\n    display: inline-block;\n    vertical-align: top;\n    margin-top: 10px;\n}\n.newhire-content{\n    display: inline-block;\n    vertical-align: middle;\n  }\n.insert-drive{\n  width: 11px;\n  display: inline-block;\n  vertical-align: middle;\n  }\n.image-items{\n      display: inline-block;\n      padding:0px 7px;\n  }\n.zenwork-customized-back-btn{\n    background: #fff;\n    /* border: 1px solid #ccc; */\n    border-radius: 25px;\n    padding: 3px 10px;\n    font-size: 10px;\n    margin:  0 10px 0 0;\n}\n.padding-right-10{\n    padding: 0 20px 0 0;\n}\n.zenwork-documents-tab {\n  padding: 10px 35px;\n}\n.assign-field {\n  outline: none;\n  border: none;\n  height: 35px;\n  box-shadow: none;\n  background: #fff;\n}\n.next-btn {\n  background-color: #439348;\n  border: 1px solid #439348;\n  border-radius: 20px;\n  color: #fff;\n  padding: 0 30px;    \n}\n.date { height: 38px !important; line-height:inherit !important; width:80% !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:73%; height: auto; padding: 10px 0 10px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.btn-run-report{\n  padding: 6px 25px;\n  display: inline-block;\n  border: 1px solid #008f3d;\n  border-radius: 24px;\n  background: #fff;\n  color: #008f3d !important;\n  cursor: pointer;\n  /* margin: 0 0 0 20px; */\n\n}\n.btn-run-report span{\n  padding: 0px 0 0 10px;\n  display: inline-block;\n  vertical-align: middle;\n}\n.disable button:disabled {\n  background-color:#aeaeae;\n  border-color: #aeaeae;\n  color: #000 !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-documents-tab\" style=\"margin-left: 40px;\">\n\n  <div class=\"education-dates\">\n    <ul class=\"list-unstyled education-date\" style=\"margin-bottom: 20px;\">\n\n      <li class=\"education-date-list padding10-listitems\">\n        <label>Offboarding Template Assigned</label><br>\n        <input type=\"text\" class=\"text-field form-control assign-field\" [(ngModel)]=\"template.templateName\"\n          [readOnly]=\"disableField\">\n\n      </li>\n      <li class=\"education-date-list padding10-listitems\">\n        <label>Date</label><br>\n        <p class=\"form-control birthdate address-state-dropodown\">\n          {{assignedDate | date : 'MM/dd/yyyy'}}\n        </p>\n        <!-- <input class=\"form-control birthdate address-state-dropodown\" type=\"date\"\n              placeholder=\"mm/dd/yyyy\" [(ngModel)]=\"assignedDate\" [readOnly]=\"disableField\"> -->\n        <!-- <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\" alt=\"Company-settings icon\"\n              (click)=\"dpMDYTDATE.toggle()\" [attr.aria-expanded]=\"dpMDYTDATE.isOpen\"> -->\n      </li>\n\n    </ul>\n\n  </div>\n  <hr class=\"zenwork-margin-ten-zero\">\n\n  <div class=\"education-dates\">\n    <ul class=\"list-unstyled education-date\">\n\n      <li class=\"education-date-list\">\n        <label>Offboarding Complete</label><br>\n        <input type=\"text\" class=\"text-field form-control assign-field\" [(ngModel)]=\"template.offboarding_complete\"\n          [readOnly]=\"disableField\">\n\n        <!-- <mat-select class=\"address-city text-field form-control address-state-dropodown\" [(ngModel)]=\"template.onboarding_complete\">\n            <mat-option [value]=\"\">{{template.onboarding_complete}}</mat-option>\n            <mat-option [value]=\"No\">No</mat-option>\n          </mat-select> -->\n      </li>\n      <li class=\"education-date-list\">\n        <label>Date</label><br>\n        <p class=\"form-control birthdate address-state-dropodown\">\n          {{completeDate | date : 'MM/dd/yyyy'}}\n        </p>\n\n        <!-- <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\" alt=\"Company-settings icon\"\n          (click)=\"dpMDYTDATE.toggle()\" [attr.aria-expanded]=\"dpMDYTDATE.isOpen\"> -->\n      </li>\n      <li *ngIf=\"!offboardingTabView\" class=\"education-date-list\">\n        <span>\n          <mat-checkbox (change)=\"adminOverride($event)\" [(ngModel)]=\"overRide\" class=\"zenwork-customized-checkbox\">\n          </mat-checkbox>\n        </span>\n        <span class=\"disable\">\n          <button (click)=\"adminOverRideOfboarding()\" [disabled]='btnOverRide' class=\"btn-run-report\">\n            Administrator Override\n          </button>\n        </span>\n        <div class=\"clearfix\"></div>\n      </li>\n\n\n    </ul>\n\n  </div>\n  <!-- <hr class=\"zenwork-margin-ten-zero\"> -->\n  <div class=\"main-onboarding\">\n    <div class=\"panel panel-info\">\n      <div class=\"panel-heading\">\n        <span class=\"panel-title panel-font pull-left\">Offboarding Tasks </span>\n\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"row\" style=\"margin:0px;\">\n        <div class=\"panel-body col-xs-4\"></div>\n        <div class=\"panel-body col-xs-4\">\n          <p class=\"hr-tasks\">Task Status</p>\n        </div>\n        <div class=\"panel-body col-xs-4\">\n          <p class=\"hr-tasks\">Task Completion</p>\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class=\"panel panel-info\" *ngFor=\"let task of getKeys((template['offBoardingTasks']))\">\n      <p *ngIf=\"template['offBoardingTasks'][task].length > 0\" class=\"hr-tasks\" style=\"padding: 10px 0px 0px 13px;\">\n        {{task}}</p>\n      <div class=\"row\" style=\"margin:0px; border-bottom: 1px solid #efe0e0;\"\n        *ngFor=\"let data of template['offBoardingTasks'][task]\">\n        <div class=\"panel-body col-xs-4\">\n\n          <p class=\"individual-hr-task\">{{data.taskName}}<br>\n            <small>{{data.assigneeEmail}} - {{data.task_assigned_date | date:'MM/dd/yyyy'}}</small>\n          </p>\n        </div>\n        <div class=\"panel-body col-xs-4\">\n\n          <p class=\"individual-hr-task\">{{data.status}} </p>\n        </div>\n        <div class=\"panel-body col-xs-4\">\n\n          <p class=\"individual-hr-task\">{{data.task_completion_date | date:'MM/dd/yyyy' }} </p>\n        </div>\n        <!-- <hr class=\"zenwork-margin-ten-zero margin-zero\"> -->\n      </div>\n\n\n    </div>\n\n    <!-- <div class=\"panel panel-info\">\n          <div class=\"team-introdution-card\">\n            <div class=\"panel-body padding-top-remove\">\n              <div class=\"checkbox checkbox-success\">\n                <input id=\"checkbox3\" type=\"checkbox\" class=\"team-checkbox\">\n                <label for=\"checkbox3\">\n                  <span class=\"individual-hr-task\">Team Introduction </span>\n                  <small>Ashley Adams -Jul 25,2018</small>\n                </label>\n              </div>\n  \n             \n  \n            </div>\n            <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n            <p class=\"card-intro\">\n              please introduce the new hire to the team lead,and instruct the team lead to introduce the new hire\n              to\n              other individuals the new hire to other individuals with inthe organization<br>\n              that will have a key role in their job responsibilities\n            </p>\n            <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n            <div class=\"employee-image-info\">\n              <img src=\"../../../assets/images/Onboarding/empoyee_2.png\" alt=\"employee-icon\" class=\"emp-image\">\n              <div class=\"employee-details borderright\">\n                <span class=\"emp-name\">Crane Andy </span>\n  \n                <small class=\"emp-designation\">Manager</small>\n              </div>\n  \n              <div class=\"employee-details\">\n                <span class=\"emp-name \">Today</span>\n                <small class=\"emp-designation\">10:07 AM</small>\n              </div>\n            </div>\n            <div class=\"team-text-area\">\n              <div class=\"form-group\">\n                <textarea class=\"form-control rounded-0\" rows=\"3\"></textarea>\n              \n              </div>\n            </div>\n            <button class=\"btn zenwork-customized-back-btn pull-right btn-color\">\n              close\n  \n            </button>\n            <div class=\"clearfix\"></div>\n          </div>\n        </div> -->\n    <!-- <div class=\"panel panel-info\">\n          <div class=\"new-emp-paperwork\">\n            <p class=\"emp-paperwork-heading green\">\n              New Employee Paperwork\n            </p>\n            <div class=\"newhire-packets pull-left\">\n              <input type=\"checkbox\" class=\"newhire-checkbox\">\n              <span class=\"panel-body padding-top-remove newhire-content\">\n                <p class=\"individual-hr-task\">New Hire Packet- UK<br>\n                  <small>Adam Jones- Jul 25,2018</small>\n                </p>\n              </span>\n              <img src=\"../../../assets/images/Onboarding/ic_insert_drive_file_24px.png\" alt=\"employee-icon\" class=\"insert-drive\">\n  \n            </div>\n            <div class=\"padding-right-10\">\n              <ul class=\"list-unstyled pull-right\">\n                <li class=\"image-items\">\n                  <img src=\"../../../assets/images/Onboarding/ic_border_color_24px.png\" alt=\"employee-icon\" class=\"insert-drive\">\n                </li>\n                <li class=\"image-items\">\n                  <img src=\"../../../assets/images/Onboarding/ic_delete_24px.png\" alt=\"employee-icon\" class=\"insert-drive\">\n                </li>\n              </ul>\n              <div class=\"clearfix\"></div>\n            </div>\n           \n          </div>\n  \n        </div> -->\n\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: OffboardingtabComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffboardingtabComponent", function() { return OffboardingtabComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OffboardingtabComponent = /** @class */ (function () {
    function OffboardingtabComponent(accessLocalStorageService, myInfoService, router, swalAlertService) {
        this.accessLocalStorageService = accessLocalStorageService;
        this.myInfoService = myInfoService;
        this.router = router;
        this.swalAlertService = swalAlertService;
        this.disableField = true;
        this.overRide = false;
        this.btnOverRide = true;
        this.template = {
            "templateName": "Onboarding Template",
            "templateType": "base",
            "offBoardingTasks": {}
        };
        this.pagesAccess = [];
        this.offboardingTabView = false;
        this.assignedDate = new Date();
    }
    OffboardingtabComponent.prototype.ngOnInit = function () {
        this.roletype = JSON.parse(localStorage.getItem('type'));
        if (this.roletype != 'company') {
            this.pageAccesslevels();
        }
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        this.templateId = this.employeeDetails.job.off_boarding_template_assigned;
        console.log(this.templateId);
        this.getOffboardingDetails();
    };
    OffboardingtabComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    OffboardingtabComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Off-Boarding" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.offboardingTabView = true;
                    console.log('loggss', _this.offboardingTabView);
                }
            });
        }, function (err) {
        });
    };
    //  Author: Saiprakash, Date: 18/05/19
    //  Get onboarding details
    OffboardingtabComponent.prototype.getOffboardingDetails = function () {
        var _this = this;
        this.myInfoService.getOffboardingDetails(this.companyId, this.templateId).subscribe(function (res) {
            console.log(res);
            _this.template = res.data;
            _this.assignedDate = new Date(_this.template.assigned_date);
            // console.log(this.assignedDate);
            if (_this.template.completion_date) {
                _this.completeDate = new Date(_this.template.completion_date);
                // console.log(this.assignedDate);
            }
        }, function (err) {
            console.log(err);
        });
    };
    OffboardingtabComponent.prototype.adminOverride = function (event) {
        console.log(event);
        this.btnOverRide = !event.checked;
    };
    OffboardingtabComponent.prototype.adminOverRideOfboarding = function () {
        var _this = this;
        console.log("vpiin");
        var uId = JSON.parse(localStorage.getItem('employee'));
        this.myInfoService.adminOffBoardingOverRide(uId._id)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Admin Override", res.message, "success");
                _this.getOffboardingDetails();
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Admin Override", err.error.message, "error");
        });
    };
    OffboardingtabComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-offboardingtab',
            template: __webpack_require__(/*! ./offboardingtab.component.html */ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.html"),
            styles: [__webpack_require__(/*! ./offboardingtab.component.css */ "./src/app/admin-dashboard/employee-management/offboardingtab/offboardingtab/offboardingtab.component.css")]
        }),
        __metadata("design:paramtypes", [_services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__["AccessLocalStorageService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], OffboardingtabComponent);
    return OffboardingtabComponent;
}());



/***/ })

}]);
//# sourceMappingURL=offboardingtab-offboardingtab-module.js.map