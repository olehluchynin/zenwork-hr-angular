(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["assets-assets-module"],{

/***/ "./src/app/admin-dashboard/employee-management/assets/assets.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets.module.ts ***!
  \*****************************************************************************/
/*! exports provided: AssetsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetsModule", function() { return AssetsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _assets_assets_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./assets/assets.component */ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.ts");
/* harmony import */ var _assets_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./assets.routing */ "./src/app/admin-dashboard/employee-management/assets/assets.routing.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _assets_add_asset_dialog_add_asset_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./assets/add-asset-dialog/add-asset-dialog.component */ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AssetsModule = /** @class */ (function () {
    function AssetsModule() {
    }
    AssetsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _assets_routing__WEBPACK_IMPORTED_MODULE_3__["AssetsRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"]
            ],
            declarations: [_assets_assets_component__WEBPACK_IMPORTED_MODULE_2__["AssetsComponent"], _assets_add_asset_dialog_add_asset_dialog_component__WEBPACK_IMPORTED_MODULE_6__["AddAssetDialogComponent"]],
            entryComponents: [_assets_add_asset_dialog_add_asset_dialog_component__WEBPACK_IMPORTED_MODULE_6__["AddAssetDialogComponent"]],
            providers: [_services_my_info_service__WEBPACK_IMPORTED_MODULE_7__["MyInfoService"]]
        })
    ], AssetsModule);
    return AssetsModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets.routing.ts":
/*!******************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets.routing.ts ***!
  \******************************************************************************/
/*! exports provided: AssetsRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetsRouting", function() { return AssetsRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _assets_assets_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./assets/assets.component */ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/assets",pathMatch:"full" },
    { path: '', component: _assets_assets_component__WEBPACK_IMPORTED_MODULE_2__["AssetsComponent"] },
];
var AssetsRouting = /** @class */ (function () {
    function AssetsRouting() {
    }
    AssetsRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AssetsRouting);
    return AssetsRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.css":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.css ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".stucture-border {\n    border-bottom: 2px solid #eee;\n    padding-bottom: 15px;\n    margin-bottom: 25px;\n}\n.mar-left-25 {\n    margin-left: 25px;\n}\n.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {\n    border-bottom: 1px solid #fff;\n    border-top: 1px solid #fff;\n}\n.table {\n    border-collapse: separate;\n    border-spacing: 0 7px;\n    border-bottom: 2px solid #eee;\n    padding-bottom: 40px;\n    margin-bottom: 25px;\n}\n.tbl-head {\n    background: #ecf7ff\n}\n.rows-bg {\n    background: #f8f8f8;\n}\n.text-fields {\n    width: 87%;\n    border: 1px solid #fff;\n    box-shadow: none;\n    background: #fff;\n    padding: 4px 3px 0 7px;\n    height: 38px;\n}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.active-structure {\n    border-left: 4px solid #5ddb97;\n    border-radius: 5px;\n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.cont-check{\n        margin-left: 15px;\n        margin-top: 7px;\n    }\n.width-50 {\n    width: 50%;\n}\n.date { height: 38px !important; line-height:inherit !important; width:80% !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:76%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.html":
/*!********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-12\">\n  <div class=\"stucture-border\">\n    <h4>{{data._id ? 'Edit Asset' : 'Add New Asset'}}</h4>\n  </div>\n\n  <div>\n    <table class=\"table\">\n      <thead class=\"tbl-head\">\n        <tr>\n          <th>\n            Asset category\n          </th>\n          <th>Asset description</th>\n          <th>Serial#</th>\n          <th>Date loaned</th>\n\n          <th>Date returned\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr class=\"rows-bg\">\n         \n          <td>\n\n               <mat-select class=\"form-control text-fields select-btn\" [(ngModel)]=\"employeeAssets.category\">\n                  <mat-option *ngFor=\"let assets of allAssets\" [value]=\"assets.name\">{{assets.name}}</mat-option>\n                </mat-select> \n \n          </td>\n          <td>\n            \n            <input type=\"text\" class=\"form-control text-fields\" [(ngModel)]=\"employeeAssets.description\" maxlength=\"40\" minlength=\"40\">\n          </td>\n          <td>\n              <input type=\"text\" class=\"form-control text-fields\" [(ngModel)]=\"employeeAssets.serial_number\" maxlength=\"40\" minlength=\"40\">\n          </td>\n          <td *ngIf=\"data._id\" style=\"padding-top:20px;\">\n              <!-- <input type=\"date\" class=\"form-control text-fields\" [(ngModel)]=\"employeeAssets.date_loaned\"> -->\n\n              {{employeeAssets.date_loaned | date:'MM/dd/yyy'}}\n          </td>\n          <td *ngIf=\"!data._id\">\n            <div class=\"date\">\n              <input  matInput [matDatepicker]=\"picker\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"employeeAssets.date_loaned\"\n              [max]=\"employeeAssets.date_returned\">\n              <!-- <input *ngIf=\"data._id\" matInput [matDatepicker]=\"picker\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"employeeAssets.date_loaned\"\n              [max]=\"employeeAssets.date_returned\" [disabled]=\"disableDateLoaned\"> -->\n              <mat-datepicker #picker></mat-datepicker>\n              <button mat-raised-button (click)=\"picker.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <div class=\"clearfix\"></div>\n            </div>\n          </td>\n          <td>\n            <div class=\"date\">\n              <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"employeeAssets.date_returned\"\n              [min]=\"employeeAssets.date_loaned\">\n              <mat-datepicker #picker1></mat-datepicker>\n              <button mat-raised-button (click)=\"picker1.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <div class=\"clearfix\"></div>\n            </div>\n          </td>\n        </tr>\n\n\n      </tbody>\n    </table>\n    <div class=\"row\">\n\n      <button mat-button (click)=\"cancel()\">Cancel</button>\n\n      <button mat-button class=\"submit-btn pull-right\" (click)=\"addAsset()\">Submit</button>\n\n    </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: AddAssetDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAssetDialogComponent", function() { return AddAssetDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var AddAssetDialogComponent = /** @class */ (function () {
    function AddAssetDialogComponent(myInfoService, accessLocalStorageService, swalAlertService, dialogRef, data) {
        this.myInfoService = myInfoService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.employeeAssets = {
            category: '',
            description: '',
            serial_number: '',
            date_loaned: '',
            date_returned: ''
        };
        this.customAssets = [];
        this.defaultAssets = [];
        this.allAssets = [];
        this.disableDateLoaned = true;
    }
    AddAssetDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    AddAssetDialogComponent.prototype.ngOnInit = function () {
        console.log(this.data);
        if (this.data) {
            this.employeeAssets = this.data;
        }
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.getStandardAndCustomStructureFields();
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
        }
        console.log(this.userId);
    };
    // Author:Saiprakash, Date:24/05/2019
    // Get Standard And Custom Structure Fields
    AddAssetDialogComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customAssets = _this.getAllStructureFields['Assets'].custom;
            _this.defaultAssets = _this.getAllStructureFields['Assets'].default;
            _this.allAssets = _this.customAssets.concat(_this.defaultAssets);
        });
    };
    // Author:Saiprakash, Date:13/05/2019
    // Add and edit employee assets
    AddAssetDialogComponent.prototype.addAsset = function () {
        var _this = this;
        var addData = {
            companyId: this.companyId,
            userId: this.userId,
            category: this.employeeAssets.category,
            description: this.employeeAssets.description,
            serial_number: this.employeeAssets.serial_number,
            date_loaned: this.employeeAssets.date_loaned,
            date_returned: this.employeeAssets.date_returned
        };
        if (this.data && this.data._id) {
            this.myInfoService.editAsset(this.data._id, addData).subscribe(function (res) {
                console.log(res);
                _this.employeeAssets.category = '';
                _this.employeeAssets.description = '';
                _this.employeeAssets.serial_number = '';
                _this.employeeAssets.date_loaned = '';
                _this.employeeAssets.date_returned = '';
                _this.data._id = '';
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.dialogRef.close(res.data);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.myInfoService.addAssets(addData).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.employeeAssets = "";
                _this.dialogRef.close(res.data);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    AddAssetDialogComponent.prototype.cancel = function () {
        this.employeeAssets.category = '';
        this.employeeAssets.description = '';
        this.employeeAssets.serial_number = '';
        this.employeeAssets.date_loaned = '';
        this.employeeAssets.date_returned = '';
        this.data._id = '';
        this.dialogRef.close();
    };
    AddAssetDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-asset-dialog',
            template: __webpack_require__(/*! ./add-asset-dialog.component.html */ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.html"),
            styles: [__webpack_require__(/*! ./add-asset-dialog.component.css */ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.css")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], AddAssetDialogComponent);
    return AddAssetDialogComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets/assets.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-compensation-tab{\n    padding: 0 35px 0 0;\n}\n.personal-head{\n    margin: 10px;\n}\n.main-head .list-items {\n    display: inline-block;\n    padding: 0px 5px;\n}\n.main-head .list-items span{\n    vertical-align: middle;\n}\n.zenwork-padding-20-zero{\n    padding:10px !important;\n}\n.img-personal-icon{\n    width: 25px;\n}\n/* tabel */\n.custom-fields { display: block;}\n.custom-fields .modal-header { width: 90%; margin:0 auto; padding:0 50px 10px; border-bottom: 1px solid #c5c3c3;}\n.custom-fields .modal-title { color:#008f3d;}\n.custom-fields .modal-dialog { width:70%;}\n.custom-fields .modal-content { border: none; padding:30px 0 50px;}\n.custom-tables { margin: 0 auto; float: none; padding: 0 30px 0 5px;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 13px;}\n.custom-fields .checkbox{ margin: 0;}\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n.heade-check .checkbox label::after { color:#616161;width: 20px; height: 20px; padding:2px 0 0 4px; font-size: 12px;}\n.heade-check .checkbox{ margin: 0;}\n.heade-check .checkbox label { padding-left: 0;}\n.cont-check .checkbox label::before {width: 20px; height: 20px; top: -1px;}\n.cont-check .checkbox label::after { color:#fff; background: #439348;width: 20px; height: 20px; padding: 3px 0 0 4px;border-radius: 3px;}\n.cont-check .checkbox{ margin: 0;}\n.cont-check .checkbox label { padding-left: 0;}\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n.custom-tables .table>tbody>tr>td .form-control { border: none; width: 80%; box-shadow: none; color:#000;}\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.select-drop select { background:#fff; border: none; width: 80%; height: 34px; color:#948e8e; padding: 6px 10px;}\n.custom-tables .btn { float: right; color:#ec6469; border:#ec6469 1px solid; padding:5px 25px; border-radius: 20px; background: transparent; font-size: 15px; margin: 20px 0 0;}\n.assets-table{\n    padding: 0px 20px;\n  }\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.next-btn {\n      background-color: #439348;\n      border: 1px solid #439348;\n      border-radius: 20px;\n      color: #fff;\n      padding: 0 30px;    \n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets/assets.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-documents-tab\" style=\"margin-left: 50px;\">\n  <!-- <div class=\"personal-head\">\n    <ul class=\"list-unstyled main-head\">\n      <li class=\"list-items\">\n        <img src=\"../../../assets/images/directory_tabs/Assets/assets.png\" class=\"img-personal-icon\" alt=\"documents-icon\">\n      </li>\n      <li class=\"list-items\">\n        <span>Assets</span>\n      </li>\n    </ul>\n  </div>\n  <hr class=\"zenwork-margin-ten-zero\"> -->\n  <div class=\"assets-table\">\n    <div class=\"zenwork-padding-20-zero pull-left\">\n     <p style=\"font-size:17px;\">Assets</p> \n    </div>\n    <div class=\"clearfix\"></div>\n    <div class=\"custom-tables\">\n      <table class=\"table\">\n        <thead>\n          <tr>\n            <th>\n            \n            </th>\n            <th>Asset category</th>\n            <th>Asset description</th>\n            <th>Serial #</th>\n            <th>Date loaned</th>\n            <th>Date returned</th>\n            <th *ngIf=\"!assetsTabView\">\n              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n              <mat-menu #menu=\"matMenu\">\n                <button *ngIf=\"!checkAssetIds.length >=1\" mat-menu-item (click)=\"openDialog()\">Add</button>\n                <button *ngIf=\"(!checkAssetIds.length <=0) && (checkAssetIds.length ==1)\" mat-menu-item (click)=\"editAsset()\">Edit</button>\n                <button *ngIf=\"(!checkAssetIds.length <=0) && (checkAssetIds.length >=1)\" mat-menu-item (click)=\"deleteAssets()\">Delete</button>\n              </mat-menu>\n            </th>\n          </tr>\n        </thead>\n        <tbody>\n\n          <tr *ngFor=\"let assets of allAssets\">\n            <td [ngClass]=\"{'zenwork-checked-border': assets.isChecked}\">\n\n                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"assets.isChecked\" (change)=\"checkedAssets($event, assets._id)\">\n                      \n                </mat-checkbox>\n            </td>\n            <td>{{assets.category}}</td>\n            <td>{{assets.description}}</td>\n            <td>{{assets.serial_number}}</td>\n            <td>{{assets.date_loaned | date : 'MM/dd/yyyy'}}</td>\n            <td>{{assets.date_returned | date : 'MM/dd/yyyy'}}</td>\n            <td></td>\n          </tr>\n\n\n      </table>\n      <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n      </mat-paginator>\n    </div>\n  </div>\n  <hr class=\"zenwork-margin-ten-zero\">\n  <div *ngIf=\"!assetsTabView\" style=\"margin:20px 40px;\">\n  \n      <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Save Changes</button>\n    <div class=\"clear-fix\"></div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/assets/assets/assets.component.ts ***!
  \***************************************************************************************/
/*! exports provided: AssetsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetsComponent", function() { return AssetsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _assets_add_asset_dialog_add_asset_dialog_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../assets/add-asset-dialog/add-asset-dialog.component */ "./src/app/admin-dashboard/employee-management/assets/assets/add-asset-dialog/add-asset-dialog.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AssetsComponent = /** @class */ (function () {
    function AssetsComponent(dialog, accessLocalStorageService, myInfoService, swalAlertService, router) {
        this.dialog = dialog;
        this.accessLocalStorageService = accessLocalStorageService;
        this.myInfoService = myInfoService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.allAssets = [];
        this.selectedAssets = [];
        this.checkAssetIds = [];
        this.pagesAccess = [];
        this.assetsTabView = false;
    }
    AssetsComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
            this.pageAccesslevels();
        }
        console.log(this.userId);
        this.getAllAssets();
    };
    AssetsComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllAssets();
    };
    AssetsComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Assets" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.assetsTabView = true;
                    console.log('loggss', _this.assetsTabView);
                }
            });
        }, function (err) {
        });
    };
    AssetsComponent.prototype.openDialog = function () {
        var _this = this;
        if (this.getSingleAsset) {
            var dialogRef = this.dialog.open(_assets_add_asset_dialog_add_asset_dialog_component__WEBPACK_IMPORTED_MODULE_1__["AddAssetDialogComponent"], {
                width: '1300px',
                backdropClass: 'structure-model',
                data: this.getSingleAsset,
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.getAllAssets();
                _this.checkAssetIds = [];
            });
        }
        else {
            var dialogRef = this.dialog.open(_assets_add_asset_dialog_add_asset_dialog_component__WEBPACK_IMPORTED_MODULE_1__["AddAssetDialogComponent"], {
                width: '1300px',
                backdropClass: 'structure-model',
                data: {},
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.getAllAssets();
                _this.checkAssetIds = [];
            });
        }
    };
    // Author:Saiprakash, Date:13/05/2019
    // Get all assets
    AssetsComponent.prototype.getAllAssets = function () {
        var _this = this;
        var getAssets = {
            companyId: this.companyId,
            userId: this.userId,
            per_page: this.perPage,
            page_no: this.pageNo
        };
        this.myInfoService.getAllAssets(getAssets).subscribe(function (res) {
            console.log(res);
            _this.allAssets = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash, Date:13/05/2019
    // Get single assets
    AssetsComponent.prototype.editAsset = function () {
        var _this = this;
        this.selectedAssets = this.allAssets.filter(function (assetCheck) {
            return assetCheck.isChecked;
        });
        if (this.selectedAssets.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one asset to proceed", "", 'error');
        }
        else if (this.selectedAssets.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one asset to proceed", "", 'error');
        }
        else {
            this.myInfoService.getAsset(this.companyId, this.userId, this.selectedAssets[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getSingleAsset = res.data;
                _this.openDialog();
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author:Saiprakash, Date:13/05/2019
    // Delete multiple assets
    AssetsComponent.prototype.deleteAssets = function () {
        var _this = this;
        this.selectedAssets = this.allAssets.filter(function (assetCheck) {
            return assetCheck.isChecked;
        });
        if (this.selectedAssets.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select assets to proceed", "", 'error');
        }
        else {
            var deleteAsset = {
                companyId: this.companyId,
                userId: this.userId,
                _ids: this.selectedAssets
            };
            this.myInfoService.deleteAsset(deleteAsset).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Deleted asset Successfully", "", "success");
                _this.getAllAssets();
                _this.checkAssetIds = [];
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    AssetsComponent.prototype.checkedAssets = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkAssetIds.push(id);
        }
        console.log(this.checkAssetIds);
        if (event.checked == false) {
            var index = this.checkAssetIds.indexOf(id);
            if (index > -1) {
                this.checkAssetIds.splice(index, 1);
            }
            console.log(this.checkAssetIds);
        }
    };
    AssetsComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/onboardingtab"]);
    };
    AssetsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-assets',
            template: __webpack_require__(/*! ./assets.component.html */ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.html"),
            styles: [__webpack_require__(/*! ./assets.component.css */ "./src/app/admin-dashboard/employee-management/assets/assets/assets.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__["MyInfoService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], AssetsComponent);
    return AssetsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=assets-assets-module.js.map