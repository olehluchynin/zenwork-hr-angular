(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["super-admin-dashboard-super-admin-dashboard-module"],{

/***/ "./src/app/super-admin-dashboard/clients/clients.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/clients/clients.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-template { padding: 0;}\n\n.zenwork-create-client-wrapper .modal-header {\n    padding: 0 0 24px 0;\n    border-bottom: 1px solid #e5e5e5;\n}\n\n.zenwork-custom-proceed-btn{\n    background: green !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 25px;\n}\n\n.zenworkers-wrapper{\n    background: #f8f8f8; margin:0 auto; float: none; padding: 0;height: 100vh;\n}\n\n.zenwork-margin-zero { font-size: 20px; line-height: 20px; vertical-align: middle; color:#008f3d;}\n\n.zenwork-currentpage{\n    padding:40px 0 0 0;\n}\n\n.zenwork-margin-ten-zero {\n    margin:30px 0px!important;\n}\n\n.zenwork-currentpage small { display: inline-block; vertical-align:text-top; margin: 0 15px 0 0;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 15px;\n    width:22px;\n    color:#9a9898;\n    font-size: 13px; cursor: pointer;\n}\n\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n\n.zenwork-customized-green-btn{\n    background: #797d7b!important;\n    color: #fff!important;\n    padding:3px 35px!important;\n    font-size:15px!important;\n}\n\n.zenwork-customized-green-btn small { display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n\n.zenwork-customized-green-btn small .fa { color:#fff; font-size: 15px;}\n\n.zenwork-custom-select{\n    background: #fff!important;\n}\n\n.border-none{\n    border:0 none !important;\n}\n\n.box-shadow-none{\n    box-shadow: none !important;\n}\n\n.zenwork-custom-progress-bar{\n    width: 30% !important;\n    height: 7px !important;\n    margin-bottom: 0px !important;\n    display: inline-block !important;\n}\n\n.client-details{\n    font-size: 10px;\n    /* float: none; */\n    border-right: 1px solid gray;\n}\n\n.client-details-wrapper{\n    background: #fff;\n    padding: 20px 20px;\n    position: relative;\n}\n\n.client-details-margin{\n    margin: 6px 0px;\n    display: inline-block;\n    /* float: none; */\n}\n\n.menu-icon{\n    font-size: 19px !important;\n    cursor: pointer;\n}\n\n.menu-dropdown{\n    background: #fff;\n    border: none;\n}\n\n.menu-dropdown-list{\n    right: 0px !important;\n    left: auto;\n}\n\n.menu-dropdown-list>li>a:focus, .menu-dropdown-list>li>a:hover{\n    background-color: #008f3d; \n    color: #fff;\n}\n\n.client-details .media-body ul li{\n    display: inline-block;\n    vertical-align: middle;\n}\n\n.client-details .media-body ul{\n    margin: 0;                                      \n}\n\n.client-details .media-body ul li .reseller-btn{\n    background: #f3616c;\n    width: 30px;\n    padding: 5px 0px;\n    display: inline-block;\n    font-size: 15px;\n    margin: 0px 10px 0 0;\n    text-align: center;\n}\n\n.client-details .media-body ul li .direct-client-btn{\n    background: #1aa4db;\n    width: 30px;\n    padding: 5px 0px;\n    display: inline-block;\n    font-size: 15px;\n    margin: 0px 10px 0 0;\n    text-align: center;\n}\n\n.client-details .media-body ul li .reseller-direct-client-btn{\n    background: #fbd437;\n    width: 30px;\n    padding: 5px 0px;\n    display: inline-block;\n    font-size: 15px;\n    margin: 0px 10px 0 0;\n    text-align: center;\n}\n\n.right-divide{\n    border-right: 1px solid gray;\n    height: 5px;\n}\n\n.media-left{\n    padding-right: 25px;\n}\n\n.zenwork-client-type-position-bottom{\n    position: absolute;\n    bottom: 10px;\n    right: 10px;\n}\n\n.zenwork-client-type-position-top{\n    position: absolute;\n    top: 10px;\n    right: 10px;\n}\n\n.zenwork-client-action-button-position-middle{\n    position: absolute;\n    right: 10px;\n    top: 38%;\n}\n\n.zenwork-client-detail-icon{\n    width: 17px !important;\n    height: auto;\n    margin-right: 6px;\n}\n\n.client-type{\n    padding: 10px 0px 0px 45px; \n    text-align: right;\n}\n\n.search-form .form-control {padding:12px 40px 12px 20px; height: auto;}\n\n.media-heading  { font-size: 20px;margin-bottom: 10px;\n    color: #676565;}\n\n.media-body { vertical-align: top;}\n\n.media-right { margin: 10px 0 0;}\n\n.zenwork-client-type-position-top .btn, .zenwork-client-type-position-bottom .btn { color:#008f3d !important; font-size:17px;}\n\n.client-details-margin span { font-size: 12px; display: inline-block; margin: 0 5px 0 0; color: #5a5959;}\n\n.zenwork-custom-progress-bar .green { font-size:12px; display: inline-block;}\n\n.client-details-margin small { font-size: 14px; display: inline-block; color: #797979; padding: 0 0 0 10px; vertical-align: middle;}\n\n.client-details-margin em { font-style: normal; display: inline-block; color:#008f3d; font-size: 12px; margin: 0 0 0 6px;}\n\n.restrict-text{\n    text-overflow: ellipsis;\n    -webkit-line-clamp: 2;\n    \n    /* overflow: hidden;\n    line-height: 16px;\n    \n    -webkit-box-orient: vertical;\n    padding: 15px;\n    margin-top: 3px; */\n}\n\n.restrict-text h4{\n    -webkit-line-clamp: 2;\n    text-overflow: ellipsis;\n    overflow: hidden;\n    /* -webkit-box-orient: vertical; */\n    max-width: 325px;\n}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/clients/clients.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/clients/clients.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper col-md-11\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <small>\n        <img src=\"../../../assets/images/Directory/ic_domain_24px.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </small>\n      Zenwork Clients ({{zenworkClients}})\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n  <div class=\"zenwork-clients-wrapper\">\n\n    <div class=\"col-xs-4 padding-left-zero\">\n      <div class=\"search-border text-left\">\n        <form [formGroup]=\"clientsSearch\" class=\"search-form\">\n          <input type=\"text\" class=\"border-none box-shadow-none form-control\" formControlName=\"search\"\n            placeholder=\"Search by name,title, etc\">\n        </form>\n        <span>\n          <span class=\"glyphicon glyphicon-search search-icon\" aria-hidden=\"true\"></span>\n        </span>\n      </div>\n    </div>\n\n    <div class=\"col-xs-7 pull-right\">\n      <span class=\"col-xs-3 client-type\">Client Type</span>\n      <div class=\"col-xs-6\">\n        <div class=\"input-group\">\n\n          <span class=\"input-group-addon zenwork-client-group-addon zenwork-input-group-addon\">\n            <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" alt=\"First Name Icon\"\n              class=\"zenwork-demo-input-image-group zenwork-company-group\">\n          </span>\n          <mat-select class=\"form-control zenwork-input zenwork-custom-select\" placeholder=\"All Clients\"\n            [(ngModel)]=\"selectedClient\" (ngModelChange)=\"filterForClient()\">\n            <mat-option value=\"\">\n              All\n            </mat-option>\n            <mat-option value=\"company\">\n              Direct-clients\n            </mat-option>\n            <mat-option value=\"reseller\">\n              Resellers\n            </mat-option>\n            <mat-option value=\"reseller-client\">\n              Reseller-clients\n            </mat-option>\n          </mat-select>\n        </div>\n      </div>\n      <div class=\"col-xs-3\">\n        <a *ngIf='!restrictLevel1' [routerLink]=\"['/super-admin/super-admin-dashboard/client-details']\" mat-button\n          class=\"btn zenwork-customized-green-btn\">\n          <small><i aria-hidden=\"true\" class=\"fa fa-plus\"></i></small> Add Client\n        </a>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n\n    <div class=\"clearfix\"></div>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"client-details-wrapper form-group\" *ngFor=\"let company of companyDetails\">\n    <div class=\"client-details col-xs-4\">\n      <div class=\"media-body\">\n        <ul class=\"list-unstyled\">\n          <li *ngIf=\"company.type =='reseller'\" class=\"list-items\">\n            <span class=\"reseller-btn\">R</span>\n          </li>\n          <li *ngIf=\"company.type =='company'\" class=\"list-items\">\n            <span class=\"direct-client-btn\">DC</span>\n          </li>\n          <li *ngIf=\"company.type =='reseller-client'\" class=\"list-items\">\n            <span class=\"reseller-direct-client-btn\">RC</span>\n          </li>\n          <li class=\"list-items restrict-text\">\n            <h4> {{company.name}} </h4>\n          </li>\n        </ul>\n\n      </div>\n    </div>\n\n    <div class=\"client-details-margin col-xs-8\">\n      <span>Info</span>\n      <div class=\"progress zenwork-custom-progress-bar\">\n        <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\"\n          aria-valuemax=\"100\" style=\"width: 40%\">\n          <span class=\"sr-only\"><em>40%</em> Completed</span>\n        </div>\n      </div>\n      <span><em>40%</em> Completed</span>\n      <span class=\"menu-icon pull-right\">\n\n        <div *ngIf='!restrictLevel1' class=\"dropdown\">\n          <button class=\"menu-dropdown dropdown-toggle\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n            aria-expanded=\"true\">\n            <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n\n          </button>\n          <ul class=\"dropdown-menu menu-dropdown-list\" aria-labelledby=\"dropdownMenu1\">\n            <li><a *ngIf=\"company.type =='reseller'\"\n                (click)=\"resellerDashboard(company._id,company.userId,company.name)\">view as\n                Reseller Admin</a></li>\n            <li><a (click)=\"editClientBillingHistory(company,'package')\">Billing History</a></li>\n            <li><a (click)=\"editClientBillingHistory(company,'create')\">Edit</a></li>\n            <li><a>Archive</a></li>\n          </ul>\n        </div>\n      </span>\n      <div class=\"clearfix\"></div>\n    </div>\n    <div class=\"clearfix\"></div>\n  </div>\n  <mat-paginator [length]=\"zenworkClients\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n    (page)=\"pageEvents($event)\">\n  </mat-paginator>\n</div>\n\n\n<!-- <div class=\"client-details-wrapper form-group\" *ngFor=\"let company of companyDetails\">\n    <div class=\"client-details\">\n      <div class=\"media\">\n        <div class=\"media-left\">\n          <img class=\"media-object\" src=\"../../../assets/images/Directory/ic_domain_24px.png\" alt=\"Company Logo\">\n        </div>\n        <div class=\"media-body\">\n          <h4 class=\"media-heading\">\n            {{company.name}}\n          </h4>\n          <div>\n            <small>\n              {{company.city}}\n            </small>\n          </div>\n          <div class=\"client-details-margin\">\n            <span>Info</span>\n            <div class=\"progress zenwork-custom-progress-bar\">\n              <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\"\n                aria-valuemax=\"100\" style=\"width: 40%\">\n                <span class=\"sr-only\"><em>40%</em> Completed</span>\n              </div>\n            </div>\n            <span><em>40%</em> Completed</span>\n          </div>\n          <div class=\"client-details-margin\">\n            <img src=\"../../../assets/images/Side Menu  - Sick/ic_email_24px.png\" class=\"zenwork-inner-icon zenwork-client-detail-icon\"\n              alt=\"Company-settings icon\"> <small> {{company.email}}</small>\n          </div>\n          <div class=\"client-details-margin\">\n            <img src=\"../../../assets/images/Side Menu  - Sick/ic_domain_24px.png\" class=\"zenwork-inner-icon zenwork-client-detail-icon\"\n              alt=\"Company-settings icon\"> <small>{{company.primaryContact.phone}}\n              Extn-{{company.primaryContact.extention}}</small>\n          </div>\n          <div class=\"client-details-margin\">\n            <img src=\"../../../assets/images/Side Menu  - Sick/ic_smartphone_24px.png\" class=\"zenwork-inner-icon zenwork-client-detail-icon\"\n              alt=\"Company-settings icon\"> <small> {{company.billingContact.phone || \"NA\"}}\n              Extn-{{company.billingContact .extention || \"NA\"}}</small>\n          </div>\n        </div>\n        <div class=\"media-right\">\n          <div class=\"zenwork-client-type-position-top\">\n            <button class=\"btn zenwork-customized-transparent-btn green\">\n              View as Reseller Admin\n            </button>\n          </div>\n          <div class=\"clearfix\"></div>\n          <div class=\"zenwork-client-action-button-position-middle\">\n            <a href=\"javascript:void(0)\" class=\"btn zenwork-customized-transparent-btn\">\n              <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n            </a>\n            <button class=\"btn zenwork-customized-transparent-btn\" (click)=\"editCompany(company)\">\n              <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n            </button>\n            <button class=\"btn zenwork-customized-transparent-btn\">\n              <i class=\"fa fa-archive\" aria-hidden=\"true\"></i>\n            </button>\n          </div>\n          <div class=\"clearfix\"></div>\n          <div class=\"zenwork-client-type-position-bottom\">\n            <button class=\"btn zenwork-customized-transparent-btn green\">\n              Client Type : {{company.type}}\n            </button>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n  </div> -->"

/***/ }),

/***/ "./src/app/super-admin-dashboard/clients/clients.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/clients/clients.component.ts ***!
  \********************************************************************/
/*! exports provided: ClientsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientsComponent", function() { return ClientsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ClientsComponent = /** @class */ (function () {
    function ClientsComponent(companyService, accessLocalStorageService, router, messageService, loaderService) {
        this.companyService = companyService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.router = router;
        this.messageService = messageService;
        this.loaderService = loaderService;
        /* =[
          {
            "_id" : ("5c148f917e51851f83869616"),
            "createdDate" : ("2018-12-15T03:43:09.874Z"),
            "updatedDate" : ("2018-12-15T03:43:09.874Z"),
            "name" : "MTW LABS",
            "tradeName" : "MTW INNOVATIONS PVT LTD",
            "employeCount" : 50,
            "email" : "venkatesh.g@mtwlabs.com",
            "type" : "reseller",
            "primaryContact" : {
                "name" : "Venkatesh",
                "phone" : "8989898989",
                "extention" : "91"
            },
            "billingContact" : {
                "name" : "",
                "email" : "",
                "phone" : "",
                "extention" : ""
            },
            "address" : {
                "address1" : "Ayyappa Society",
                "address2" : "Madhapur",
                "city" : "State",
                "state" : "",
                "zipcode" : "530026",
                "country" : "United States"
            },
            "isPrimaryContactIsBillingContact" : true,
            "isPrimaryAddressIsBillingAddress" : true,
            "createdBy" : ("5c139b85a69f895486534058"),
            "__v" : 0,
            "userId" : ("5c148f917e51851f83869615")
        }
        ] ; */
        this.pageNo = 1;
        this.perPage = 10;
        this.selectedClient = '';
        this.restrictLevel1 = false;
    }
    ClientsComponent.prototype.ngOnInit = function () {
        this.loaderService.loader(true);
        this.type = this.accessLocalStorageService.get("type");
        console.log(this.type, "2w2w");
        if (this.type == 'administrator' || this.type == 'zenworker') {
            this.acceslevel = this.accessLocalStorageService.get('zenworkerId');
            console.log(this.acceslevel);
            if (this.acceslevel.roleId.name == 'Level 1') {
                this.restrictLevel1 = true;
            }
        }
        this.clientsSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.messageService.sendMessage('super-admin');
        this.onChanges();
        this.getClientsData();
    };
    /* Description: clients list for filter
     author : vipin reddy */
    ClientsComponent.prototype.filterForClient = function () {
        console.log(this.selectedClient);
        this.getClientsData();
    };
    /* Description: mat pagination for client list
 author : vipin reddy */
    ClientsComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getClientsData();
    };
    ClientsComponent.prototype.onChanges = function () {
        var _this = this;
        this.clientsSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            // console.log(this.zenworkersSearch.get('search').value);
            _this.getClientsData();
        });
    };
    /* Description: get clients list
 author : vipin reddy */
    ClientsComponent.prototype.getClientsData = function () {
        var _this = this;
        this.companyService.getAllCompanies({ searchQuery: this.clientsSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, type: this.selectedClient })
            .subscribe(function (response) {
            _this.loaderService.loader(false);
            if (response.status) {
                _this.zenworkClients = response.count;
                _this.companyDetails = response.data;
            }
            console.log("Response", response);
        });
    };
    /* Description: edit comapny routing to redirection
author : vipin reddy */
    ClientsComponent.prototype.editCompany = function (company) {
        this.accessLocalStorageService.set('editCompany', company);
        this.router.navigate(['/super-admin/super-admin-dashboard/edit-company']);
    };
    /* Description: redirection to reseller dashboard
      author : vipin reddy */
    ClientsComponent.prototype.resellerDashboard = function (comapny_id, id, comapanyName) {
        localStorage.setItem('resellerName', comapanyName);
        localStorage.setItem('resellerId', comapny_id);
        localStorage.setItem('resellercmpnyID', id);
        this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id]);
        this.messageService.sendMessage('reseller');
    };
    /* Description: redirection to cilent billing history shown
     author : vipin reddy */
    ClientsComponent.prototype.editClientBillingHistory = function (company, tab) {
        console.log("comapnay", company);
        this.accessLocalStorageService.set('editCompany', company);
        this.accessLocalStorageService.set('viewTab', tab);
        var id = company.userId;
        this.router.navigate(['/super-admin/super-admin-dashboard/edit-company/' + id]);
    };
    ClientsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-clients',
            template: __webpack_require__(/*! ./clients.component.html */ "./src/app/super-admin-dashboard/clients/clients.component.html"),
            styles: [__webpack_require__(/*! ./clients.component.css */ "./src/app/super-admin-dashboard/clients/clients.component.css")]
        }),
        __metadata("design:paramtypes", [_services_company_service__WEBPACK_IMPORTED_MODULE_2__["CompanyService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_5__["MessageService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]])
    ], ClientsComponent);
    return ClientsComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/create-company/create-company.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/create-company/create-company.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-currentpage {\n    padding: 40px 0 0 0;\n}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.zenworkers-wrapper{\n    padding:0; float: none;margin: 0 auto 30px;\n}\n\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 12px;\n    width: 13px;\n    color: #000;\n    font-size: 10px;\n}\n\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n\n.zenwork-customized-green-btn{\n    background: #439348!important;\n    color: #fff!important;\n    padding: 7px 12px!important;\n    font-size: 12px!important;\n    border-radius: 0px!important;\n}\n\n.zenwork-custom-select{\n    background: #fff!important;\n}\n\n.border-none{\n    border:0 none !important;\n}\n\n.address-wrapper { padding: 0 0 20px;}\n\n.address-wrapper ul { display: inline-block; width: 100%;}\n\n.address-wrapper ul li { margin: 0 0px 20px 0; padding: 0;}\n\n.address-wrapper ul li.details-width{ width: 100%;}\n\n.address-wrapper ul li .form-control { font-size: 15px; line-height:15px;border: none; box-shadow: none; height: auto;padding:13px 0 13px 10px;}\n\n.address-wrapper ul li select { background:#fff; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n\n.address-wrapper ul li.united-state { margin: 0; clear: left;}\n\n.address-wrapper ul li.state, .address-wrapper ul li.city { padding: 0 10px 0 0;}\n\n.contact-wrapper { display: block; padding: 0 0 20px;}\n\n.contact-wrapper ul { margin: 0 0 30px; padding: 0;}\n\n.contact-wrapper ul li {margin: 0 0 15px 0; padding: 0;}\n\n.contact-wrapper ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n\n.contact-wrapper ul li .phone { display: block; background:#fff; padding: 0 10px;clear: left;}\n\n.contact-wrapper ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n\n.contact-wrapper ul li .phone span .fa { color: #636060;\nfont-size: 23px;\nline-height: 20px;\nwidth: 18px;\ntext-align: center;}\n\n.contact-wrapper ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 84%; border: none; background:transparent; box-shadow: none; padding:13px 0 13px 10px;}\n\n.contact-wrapper ul li .form-control { display: inline-block;vertical-align: middle; width:100%; border: none; background:#fff; box-shadow: none;padding:13px 10px;\nheight: auto;}\n\n.contact-wrapper ul li.phone-width { float: none; clear:left;}\n\n.contact-wrapper ul li.phone-width .phone .form-control { width: 100%; padding: 13px 10px 13px 10px;}\n\n.zenwork-custom-input{\n    background: #fff !important; padding: 13px 15px; height: auto;\n}\n\n.company-details-wrapper , .contact-wrapper{\n    padding:30px 0px;\n    border-bottom: 1px dotted;\n}\n\n.address-wrapper{\n    padding: 40px 0px;\n}\n\n.zenwork-custom-proceed-btn{\n    background:#008f3d !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 30px;\n    margin: 0 0 0 40px;\n}\n\n.zenwork-customized-transparent-btn { margin: 0 0 0 20px;}\n\n.zenwork-margin-zero small { display:inline-block; vertical-align: middle; font-size: 17px; margin: 0 10px 0 0;}\n\n.zenwork-margin-zero{ font-size: 18px; color:#585858;}\n\n.company-details h2 { color:#008f3d; font-size: 18px; line-height: 18px; display: inline-block; margin: 0 0 20px;}\n\n.company-details label { color:#5f5c5c; font-size: 15px; padding: 0 0 10px; font-weight: normal;}\n\n.right-text { margin: 0 0 0 20px !important;}\n\n.contact-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n\n.contact-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n\n.address-wrapper h2 { color:#636060; font-size: 17px; line-height: 17px; display: inline-block; margin: 0 0 20px;}\n\n.address-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n\n.address-wrapper .form-group { margin: 15px 0 0;}\n\n.address-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n\n.client-tabs {padding:10px 0 0;}\n\n.client-tabs .tab-content { margin:40px 0 0;}\n\n.client-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border: none;background: transparent;}\n\n.client-tabs .nav-tabs {border-bottom:#dbdbdb 2px solid;}\n\n.client-tabs .nav-tabs > li { padding:0 30px;}\n\n.client-tabs .nav-tabs > li > a {font-size: 18px;line-height: 18px;font-weight:normal;color:#484747;border: none;padding: 10px 0 15px !important;margin: 0;}\n\n.client-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;}\n\n.client-tabs .nav-tabs > li.active{border-bottom:#439348 5px solid;padding:0px 30px 0 30px;color:#000;}\n\n/* package-billing css */\n\n.package-billing-main{\n    margin: 40px 0 0 0;\n}\n\n.package-billing-main .billing-block h2 { padding: 20px 20px 5px;}\n\n.discount-block{\n    padding: 0;\n}\n\n.billing-package-block{\n    float: none;\n}\n\n.border-bottom-green{\n    border-bottom: 2px solid #439348;\n    padding: 0 0px 5px 0;\n}\n\n.discount-heading .list-items{\n    display: inline-block;\n    padding: 0px 20px 5px 0;\n    font-size: 16px; float: left;\n}\n\n.discount-heading .list-items span{\n    display: block;\n}\n\n.package-table { margin: 0 auto; float: none; padding: 0 30px 0 0px;}\n\n.package-table h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n\n.package-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;text-align: center; width: 34%;}\n\n.package-table .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n\n.package-table .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n\n.package-table .table>tbody>tr>td, .package-table .table>tfoot>tr>td, .package-table .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-bottom: #e0dada 1px solid;font-size: 15px;text-align: center;}\n\n.package-table .table>tbody>tr>td .mat-select-placeholder{\n    font-size: 15px;\n    text-align: left;\n}\n\n.package-table .table>tbody>tr>td .package-table-select{\n    background: #f8f8f8;\n}\n\n.add-on-cancel{\n    float: left;\n    padding:5px 3px 0 0;\n}\n\n.package-table .table>tbody>tr>td .add-on-select{\n    float: right;\n    width: 86% !important;\n}\n\n.package-table .table>tbody>tr .add-on-type{\n    width:40%;\n}\n\n.package-table .table>tbody>tr .package-name{\n    width: 21%;\n}\n\n.payment { margin: 50px 0 0;}\n\n.payment-lft { padding:30px 15px 0 0;}\n\n.payment-lft h2 {color:#008f3d; font-size: 17px; line-height: 17px;margin: 0; float: left; padding: 5px 0 0; font-weight: 600;}\n\n.payment-lft > a {color:#008f3d; font-size: 16px; line-height: 17px;margin: 0; cursor: pointer; float: right; border-radius: 30px; padding: 10px 20px; border:#008f3d 1px solid;}\n\n.payment-method { margin: 30px 0 0;}\n\n.payment-method .panel-default>.panel-heading {background-color:#fff !important; border: none; position: relative; padding:0; box-shadow: 0 0 10px #ccc; display: block;}\n\n.payment-method .panel-title { color:#808080; display:block; font-size: 15px; font-weight: 600;}\n\n.payment-method .panel-title span { display: inline-block; padding:0 10px 0 0; margin: 0 10px 0 0; border-right:#ccc 1px solid;}\n\n.payment-method .panel-title a:hover { text-decoration: none;}\n\n.payment-method .panel-title>.small, .payment-method .panel-title>.small>a, .payment-method .panel-title>a, .payment-method .panel-title>small, .payment-method .panel-title>small>a { text-decoration:none; display: block; padding: 15px;}\n\n.payment-method .panel { background: none; box-shadow: none; border-radius: 0;}\n\n.payment-method .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color:#798993;\n}\n\n.payment-method .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.payment-method .panel-group .panel-heading+.panel-collapse>.list-group, .payment-method .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.payment-method .panel-heading .accordion-toggle:after { margin:4px 0 0; font-size: 12px; line-height: 10px;}\n\n.payment-method .panel-body { padding: 0 0 5px;}\n\n.payment-method .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.payment-method .panel-default { border-color:transparent;}\n\n.payment-method .panel-group .panel+.panel { margin-top: 20px;}\n\n.ach-payment { margin: 20px 0 0 0;}\n\n.credit-payment { margin: 20px 0 0;}\n\n.paypal-payment { margin: 20px 0 0;}\n\n.payment-rgt { padding:50px 0 0 20px;}\n\n.payment-rgt h3 { color:#008f3d; font-size: 16px; line-height: 17px; display: inline-block; margin: 0 0 20px;}\n\n.billing-block { background:#fff;}\n\n.billing-block h2 { background:#f0be19; padding: 20px; border-radius:5px 5px 0 0;margin:0;}\n\n.billing-block h2 span { float: left; color:#3e3e3e; font-size: 16px; line-height: 16px;}\n\n.billing-block h2 small { float:right; color:#3e3e3e; font-size: 16px; line-height: 16px;}\n\n.annualy-block { float: right;}\n\n.annualy-block p { float:left; color:#3e3e3e; font-size: 16px; line-height: 16px; border-bottom:#3e3e3e 1px solid; display:inline-block; margin: 0 30px 0 0;}\n\n.billing-block > ul { padding:10px 0; margin: 0;}\n\n.billing-block > ul > li { border-bottom:#b8c0c6 1px dashed; padding:13px 0 10px;}\n\n.billing-price { padding: 0 20px;}\n\n.billing-price > ul {  padding:0;}\n\n.billing-price > ul > li { margin: 0 0 7px; border-bottom:none;}\n\n.billing-price ul li h5 { color:#008f3d; font-size: 15px; line-height: 15px; margin: 0 0 3px; border-bottom:#008f3d 1px solid; display: inline-block;}\n\n.billing-price ul li span { float: left; color:#777; font-size: 15px; line-height: 15px;}\n\n.billing-price ul li small { float:right; color:#008f3d; font-size: 15px; line-height: 15px;}\n\n.billing-price ul li a { float: left; cursor: pointer; display: inline-block; background:#b8c0c6; font-size: 13px; line-height: 13px; padding:4px 10px; color:#fff;\nborder-radius: 3px; margin: -3px 0 0 15px;}\n\n.billing-price ul li var { float: left;; color:#008f3d; font-size: 16px; line-height: 15px; margin: 0 0 0 10px; font-style: normal;}\n\n.billing-btm { margin:0 20px;}\n\n.billing-btm .form-control { float: left; border:#d2cbcb 1px solid; width: 30%; padding: 15px 10px; border-radius: 5px; box-shadow: none;text-transform: uppercase;}\n\n.billing-btm a.apply { float: left; color:#92b8ce; margin:6px 0 0 10px; font-size: 14px; cursor: pointer;}\n\n.billing-btm em { float: right; color:#777; font-size: 15px; line-height: 15px; font-style: normal; padding: 10px 0 0;}\n\n.billing-block h4 { background:#eef7ff; padding: 20px;margin:0;}\n\n.billing-block h4 h1 { float: left; color:#008f3d; font-size: 18px; line-height: 16px; font-weight: bold; margin: 0;}\n\n.billing-block h4 p { float:right; color:#008f3d; font-size: 18px; line-height: 16px;font-weight: bold; margin: 0;}\n\n.payment-rgt a.btn {border-radius: 20px;color:#616060; font-size: 15px; padding:6px 25px; background:transparent; margin:40px 0 0; float:left; position: static;}\n\n.payment-rgt a.btn:hover {background: #008f3d; color:#fff;}\n\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n\n.heade-check .checkbox label::after { color:#008f3d;width: 20px; height: 20px; padding:0 0 0 4px; font-size: 12px;}\n\n.heade-check .checkbox{ margin: 0;}\n\n.heade-check .checkbox label { padding-left: 0; color:#948e8e; font-size: 13px; line-height: 20px; padding: 0 0 0 10px;}\n\n.payment-rgt .cont-check { margin:20px 20px 0;}\n\n.payment-rgt .cont-check .checkbox label::after { padding:2px 0 0 4px;border-radius: 3px;}\n\n.payment-rgt .cont-check .checkbox label { padding-left:15px;color:#6d6d6d; font-size: 15px;line-height: 17px;}\n\n.payment-rgt .cont-check .checkbox label small { color:#008f3d;font-size: 15px; }\n\n.date { height: auto !important; line-height:inherit !important;}\n\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0;}\n\n.date span { border: none !important; padding: 0 !important;}\n\n.date .form-control { width: 70% !important;}\n\n.date .form-control:focus { box-shadow: none; border: none;}\n\n/* packahe-billing-css-ends */\n\n.discount-block .btn {\n    background: #008f3d!important;\n    color: #fff!important;\n    padding: 8px 20px!important;\n    font-size: 15px!important; margin: 0 0 0 20px;\n}\n\n.discount-block small { padding:0 10px 0 0;}\n\n.discount-block small .fa { font-size: 14px;}\n\n/* ==========================edit-company-starts========================== */\n\n.setting-table{height: 100vh;}\n\n.setting-table .table>thead>tr>th { padding: 18px 15px;background: #eec700}\n\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td { padding: 17px 15px;}\n\n.payment-method .panel-group .credit-card-payment {\n    background: #fff;\n    box-shadow: -2px 0px 21px -1px rgba(0,0,0,0.75);\n    margin: 10px 0 0 0;\n}\n\n.card-buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n\n.card-buttons ul li .my-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #777d7a;\n    padding: 8px 30px; \n}\n\n.card-buttons ul li .my-cards-btn:hover,.card-buttons ul li .my-cards-btn:active,.card-buttons ul li .my-cards-btn:visited{\n    border-radius: 20px;\n    background: #777d7a;\n    color: #fff;\n    padding: 8px 30px;\n\n}\n\n.card-buttons ul li .secure-cards-btn:hover,.card-buttons ul li .secure-cards-btn:active,.card-buttons ul li .secure-cards-btn:visited{\n    border-radius: 20px;\n    background: #439348;\n    color: #fff;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n\n.card-buttons ul li .secure-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #439348;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n\n.credit-card-details-date ul li{\n    display: inline-block;\n    padding: 0px 10px 0 0;\n    width: 29.5%;\n}\n\n.wrapper{\n    padding: 10px 25px;\n}\n\n.agree-policy{\n    padding: 25px 40px;\n}\n\n.card-buttons {\n    display: block;\n    padding: 10px 20px;\n}\n\n.wrapper{\n    padding: 10px 25px;\n}\n\n.card-buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n\n.card-buttons ul li .my-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #777d7a;\n    padding: 8px 30px; \n}\n\n.card-buttons ul li .my-cards-btn:hover,.card-buttons ul li .my-cards-btn:active,.card-buttons ul li .my-cards-btn:visited{\n    border-radius: 20px;\n    background: #777d7a;\n    color: #fff;\n    padding: 8px 30px;\n\n}\n\n.card-buttons ul li .secure-cards-btn:hover,.card-buttons ul li .secure-cards-btn:active,.card-buttons ul li .secure-cards-btn:visited{\n    border-radius: 20px;\n    background: #439348;\n    color: #fff;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n\n.card-buttons ul li .secure-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #439348;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n\n.left-padding { padding: 0;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/create-company/create-company.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/create-company/create-company.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper col-md-11\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <small>\n        <img src=\"../../../assets/images/employee-management/Green/employee_4.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </small>\n      Edit Client - Reseller\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"create-client-wrapper\">\n\n    <!-- <mat-horizontal-stepper  #stepper=\"matHorizontalStepper\">\n      <mat-step [stepControl]=\"firstFormGroup\">\n        <form [formGroup]=\"firstFormGroup\">\n          <ng-template matStepLabel><div class=\"zenwork-dot\"></div></ng-template>\n          <mat-form-field>\n            <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\n          </mat-form-field>\n          <div>\n            <button mat-button matStepperNext>Next</button>\n          </div>\n        </form>\n      </mat-step>\n      <mat-step [stepControl]=\"secondFormGroup\">\n        <form [formGroup]=\"secondFormGroup\">\n          <ng-template matStepLabel><div class=\"zenwork-dot\"></div></ng-template>\n          <mat-form-field>\n            <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\n          </mat-form-field>\n          <div>\n            <button mat-button matStepperPrevious>Back</button>\n            <button mat-button matStepperNext>Next</button>\n          </div>\n        </form>\n      </mat-step>\n      <mat-step>\n        <ng-template matStepLabel>Done</ng-template>\n        You are now done.\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button (click)=\"stepper.reset()\">Reset</button>\n        </div>\n      </mat-step>\n    </mat-horizontal-stepper> -->\n\n    <div class=\"client-tabs\">\n\n      <ul class=\"nav nav-tabs\">\n        <li style=\"cursor:pointer\" [ngClass]=\"{ 'active':activeTab==='create'}\">\n          <a data-toggle=\"tab\" (click)=\"activeTab='create'\">Client Details</a>\n        </li>\n        <li style=\"cursor:pointer\" [ngClass]=\"{ 'active':activeTab==='package'}\">\n          <a data-toggle=\"tab\" (click)=\"activeTab='package'\">Billing History</a>\n        </li>\n        <li style=\"cursor:pointer\" [ngClass]=\"{ 'active':activeTab==='payment'}\">\n          <a data-toggle=\"tab\" (click)=\"activeTab='payment'\">Upgrade/Downgrade Package</a>\n        </li>\n        <li style=\"cursor:pointer\" [ngClass]=\"{ 'active':activeTab==='finished'}\">\n          <a data-toggle=\"tab\" (click)=\"activeTab='finished'\">Payment</a>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n    </div>\n\n\n    <div class=\"tab-content\">\n\n      <div id=\"tab1\" class=\"tab-pane\" [ngClass]=\"{ 'active':activeTab==='create','in':activeTab==='create'}\">\n\n        <div class=\"company-details-wrapper\">\n          <div class=\"company-details\">\n            <div class=\"col-xs-4\">\n\n              <div class=\"form-group\">\n                <label>Company Name*</label>\n                <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Company Name\"\n                  [(ngModel)]=\"companyDetails.name\" maxlength=\"32\" name=\"name\" required #companyName=\"ngModel\">\n                <span *ngIf=\"!companyDetails.name && isValid || (!companyName.name && companyName.touched)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span>\n              </div>\n              <div class=\"form-group\">\n                <label>Trade Name*</label>\n                <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Trade Name\"\n                  [(ngModel)]=\"companyDetails.tradeName\" maxlength=\"32\" name=\"name1\" required #tradeName=\"ngModel\">\n                <span *ngIf=\"!companyDetails.tradeName && tradeName.touched || (!companyDetails.tradeName && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter Trade name</span>\n              </div>\n              <div class=\"form-group\">\n                <label>No. of employees</label>\n                <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" (keypress)=\"keyPress($event)\"\n                  placeholder=\"No. of Employees\" maxlength=\"4\" [(ngModel)]=\"companyDetails.employeCount\"\n                  name=\"employeCount\" required #employeCount=\"ngModel\">\n                <span\n                  *ngIf=\"!companyDetails.employeCount && employeCount.touched || (!companyDetails.employeCount && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter employees</span>\n              </div>\n              <div class=\"form-group\">\n                \n                  <label>Is the Client Reseller?</label>\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                    placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.type\"\n                    (ngModelChange)=\"wholesaleDiscountChange($event)\">\n                    <mat-option value=\"reseller\">\n                      Yes\n                    </mat-option>\n                    <mat-option value=\"company\">\n                      No\n                    </mat-option>\n                  </mat-select>\n\n                <div *ngIf=\"companyDetails.type == 'company'\">\n                  <div class=\"form-group\">\n                    <label>Client belongs to Reseller?</label>\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                      placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.belongsToReseller\">\n                      <mat-option [value]=true>\n                        Yes\n                      </mat-option>\n                      <mat-option [value]=false>\n                        No\n                      </mat-option>\n                    </mat-select>\n                  </div>\n                  <div class=\"form-group\" *ngIf=\"companyDetails.belongsToReseller != false\">\n                    <label>Select Reseller</label>\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                      placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.resellerId\"\n                      (ngModelChange)=\"selectedReseller($event)\" [ngModelOptions]=\"{standalone:true}\">\n                      <mat-option value=\"{{reseller._id}}\" *ngFor=\"let reseller of resellersList\">\n                        {{reseller.name}}\n                      </mat-option>\n\n                    </mat-select>\n                  </div>\n                </div>\n\n                <div class=\"clearfix\"></div>\n              </div>\n            </div>\n            <div class=\"col-xs-4\">\n              <div class=\"form-group col-md-6\">\n                <label>Client Status</label>\n                <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"active status\"\n                  [(ngModel)]=\"clientStatus\" name=\"status\" required #status=\"ngModel\" readonly>\n\n              </div>\n              <div class=\"form-group col-md-6\">\n                <label>Inactive Date</label>\n                <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"End Date\"\n                  [(ngModel)]=\"comapnyInactiveDate\" name=\"status\" required #inactiveData=\"ngModel\" readonly>\n\n              </div>\n            </div>\n            <div class=\"clearfix\"></div>\n          </div>\n        </div>\n\n        <div class=\"contact-wrapper\">\n          <div class=\"col-xs-8\">\n            <ul class=\"zenwork-margin-bottom-zero col-xs-6\" *ngIf=\"companyDetails.hasOwnProperty('primaryContact')\">\n              <li>\n                <h4>Primary Company Contact*</h4>\n              </li>\n              <li class=\"phone-width\">\n                <div class=\"phone\">\n                  <!-- <span>\n                    <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                  </span> -->\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\"\n                    [(ngModel)]=\"companyDetails.primaryContact.name\" maxlength=\"32\" name=\"contact\" required\n                    #contact=\"ngModel\">\n\n                </div>\n                <span\n                  *ngIf=\"!companyDetails.primaryContact.name && contact.touched || (!companyDetails.primaryContact.name && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter Primary contact</span>\n              </li>\n              <li>\n                <h4>Email</h4>\n              </li>\n              <li class=\"col-xs-12\">\n                <div class=\"phone\">\n                  <span>\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\"\n                    [(ngModel)]=\"companyDetails.primaryContact.email\" name=\"email\" required #primaryEmail=\"ngModel\">\n\n                </div>\n                <span\n                  *ngIf=\"!companyDetails.primaryContact.email && primaryEmail.touched || (!companyDetails.primaryContact.email && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter email</span>\n              </li>\n              <div class=\"clearfix\"></div>\n              <li>\n                <h4>Phone</h4>\n              </li>\n              <li class=\"col-md-8\">\n                <div class=\"phone\">\n                  <span>\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Work Phone\" (keypress)=\"keyPress($event)\"\n                    [(ngModel)]=\"companyDetails.primaryContact.phone\" maxlength=\"10\" name=\"phone\" required\n                    #phoneNumber=\"ngModel\">\n\n                </div>\n                <span\n                  *ngIf=\"!companyDetails.primaryContact.phone && phoneNumber.touched || (!companyDetails.primaryContact.phone && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter phone</span>\n              </li>\n              <li class=\"col-md-2 right-text\">\n\n                <input type=\"text\" class=\"form-control\" placeholder=\"Ext\" maxlength=\"4\" (keypress)=\"keyPress($event)\"\n                  [(ngModel)]=\"companyDetails.primaryContact.extention\" name=\"ext\" required #ext=\"ngModel\">\n\n              </li>\n              <span\n                *ngIf=\"!companyDetails.primaryContact.extention && ext.touched || (!companyDetails.primaryContact.extention && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter ext</span>\n\n              <div class=\"clearfix\"></div>\n\n            </ul>\n            <div class=\"form-group col-xs-7 left-padding\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\"\n                [(ngModel)]=\"companyDetails.isPrimaryContactIsBillingContact\"></mat-checkbox>\n              <small>Is Primary Contact same as billing Contact ?</small>\n            </div>\n          </div>\n          <div class=\"col-xs-4\">\n            <ul\n              *ngIf=\"!companyDetails.isPrimaryContactIsBillingContact && companyDetails.hasOwnProperty('billingContact')\">\n              <li>\n                <h4>Billing Company Contact</h4>\n              </li>\n              <li class=\"phone-width\">\n                <div class=\"phone\">\n                  <!-- <span>\n                    <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                  </span> -->\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\"\n                    [(ngModel)]=\"companyDetails.billingContact.name\">\n                </div>\n              </li>\n              <li>\n                <h4>Email</h4>\n              </li>\n              <li class=\"col-md-9\">\n                <div class=\"phone\">\n                  <span>\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\"\n                    [(ngModel)]=\"companyDetails.billingContact.email\">\n                </div>\n              </li>\n              <div class=\"clearfix\"></div>\n              <li>\n                <h4>Phone</h4>\n              </li>\n              <li class=\"col-md-8\">\n                <div class=\"phone\">\n                  <span>\n                    <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                      height=\"16\" alt=\"img\">\n                  </span>\n                  <input type=\"text\" class=\"form-control\" (keypress)=\"keyPress($event)\" maxlength=\"10\"\n                    placeholder=\"Work Phone\" [(ngModel)]=\"companyDetails.billingContact.phone\">\n                </div>\n              </li>\n              <li class=\"col-md-2 right-text\">\n\n                <input type=\"text\" class=\"form-control\" (keypress)=\"keyPress($event)\" minlength=\"2\" maxlength=\"4\"\n                  placeholder=\"Ext\" [(ngModel)]=\"companyDetails.billingContact.extention\">\n\n              </li>\n\n              <div class=\"clearfix\"></div>\n\n            </ul>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n\n        <div class=\"address-wrapper\">\n          <div class=\"clearfix\"></div>\n          <div class=\"col-xs-5\">\n            <h2>\n              Company Address*\n            </h2>\n            <ul class=\"zenwork-margin-bottom-zero\">\n              <li class=\"col-md-12\">\n                <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" maxlength=\"32\"\n                  [(ngModel)]=\"companyDetails.primaryAddress.address1\" name=\"address\" required #CompanyAdd=\"ngModel\">\n                <span\n                  *ngIf=\"!companyDetails.primaryAddress.address1 && CompanyAdd.touched || (!companyDetails.primaryAddress.address1 && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter Company address</span>\n              </li>\n              <li class=\"col-md-12\">\n                <input type=\"text\" placeholder=\"Street2\" class=\"form-control\"\n                  [(ngModel)]=\"companyDetails.primaryAddress.address2\" maxlength=\"32\" name=\"address1\" required\n                  #CompanyAddress=\"ngModel\">\n                <span\n                  *ngIf=\"!companyDetails.primaryAddress.address2 && CompanyAddress.touched || (!companyDetails.primaryAddress.address2 && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter Company address</span>\n              </li>\n            </ul>\n\n            <ul class=\"zenwork-margin-bottom-zero\">\n              <li class=\"state col-md-4\">\n                <input type=\"text\" placeholder=\"City\" class=\"form-control\"\n                  [(ngModel)]=\"companyDetails.primaryAddress.city\" name=\"city\" maxlength=\"24\" required #city=\"ngModel\">\n                <span\n                  *ngIf=\"!companyDetails.primaryAddress.city && city.touched || (!companyDetails.primaryAddress.city && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter city</span>\n              </li>\n              <li class=\"city col-md-4\">\n                <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                  placeholder=\"State\" [(ngModel)]=\"companyDetails.primaryAddress.state\" name=\"state\" required\n                  #states=\"ngModel\">\n                  <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                    {{data.name}}\n                  </mat-option>\n                  <!-- <mat-option value=\"Washington\">\n                    Washington\n                  </mat-option>\n                  <mat-option value=\"Florida\">\n                    Florida\n                  </mat-option> -->\n                </mat-select>\n                <span\n                  *ngIf=\"!companyDetails.primaryAddress.state && states.touched || (!companyDetails.primaryAddress.state && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter state</span>\n              </li>\n              <li class=\"zip col-md-4\">\n                <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" (keypress)=\"keyPress($event)\" maxlength=\"5\"\n                  [(ngModel)]=\"companyDetails.primaryAddress.zipcode\" name=\"zip\" required #zip=\"ngModel\">\n                <span\n                  *ngIf=\"!companyDetails.primaryAddress.zipcode && zip.touched || (!companyDetails.primaryAddress.zipcode && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter zipcode</span>\n              </li>\n\n              <li class=\"col-md-6 united-state\">\n                <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                  placeholder=\" Country\" [(ngModel)]=\"companyDetails.primaryAddress.country\" name=\"country\" required\n                  #countryName=\"ngModel\">\n                  <mat-option *ngFor=\"let data of allCountryes\" [value]=\"data.name\">\n                    {{data.name}}\n                  </mat-option>\n                </mat-select>\n                <span\n                  *ngIf=\"!companyDetails.primaryAddress.country && countryName.touched || (!companyDetails.primaryAddress.country && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter country</span>\n              </li>\n              <div class=\"clearfix\"></div>\n            </ul>\n            <div class=\"clearfix\"></div>\n            <div class=\"form-group\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\"\n                [(ngModel)]=\"companyDetails.isPrimaryAddressIsBillingAddress\"></mat-checkbox>\n              <small>Is Primary Address same as billing address ?</small>\n            </div>\n          </div>\n\n          <div class=\"col-xs-5\" *ngIf=\"!companyDetails.isPrimaryAddressIsBillingAddress\">\n            <h2>\n              Billing Company Address*\n            </h2>\n            <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('billingAddress')\">\n              <li class=\"col-md-12\">\n                <input type=\"text\" placeholder=\"Street1\" class=\"form-control\"\n                  [(ngModel)]=\"companyDetails.billingAddress.address1\">\n              </li>\n              <li class=\"col-md-12\">\n                <input type=\"text\" placeholder=\"Street2\" class=\"form-control\"\n                  [(ngModel)]=\"companyDetails.billingAddress.address2\">\n              </li>\n            </ul>\n\n            <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('billingAddress')\">\n              <li class=\"state col-md-4\">\n                <input type=\"text\" placeholder=\"City\" class=\"form-control\"\n                  [(ngModel)]=\"companyDetails.billingAddress.city\">\n              </li>\n              <li class=\"city col-md-4\">\n                <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                  placeholder=\"State\" [(ngModel)]=\"companyDetails.billingAddress.state\">\n                  <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                    {{data.name}}\n                  </mat-option>\n                  <!-- <mat-option value=\"Washington\">\n                    Washington\n                  </mat-option>\n                  <mat-option value=\"Florida\">\n                    Florida\n                  </mat-option> -->\n                </mat-select>\n              </li>\n              <li class=\"zip col-md-4\">\n                <input type=\"text\" placeholder=\"Zip\" class=\"form-control\"\n                  [(ngModel)]=\"companyDetails.billingAddress.zipcode\">\n              </li>\n\n              <li class=\"col-md-4 united-state\">\n                <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                  placeholder=\"Country\" [(ngModel)]=\"companyDetails.billingAddress.country\">\n                  <mat-option *ngFor=\"let data of allCountryes\" [value]=\"data.name\">\n                    {{data.name}}\n                  </mat-option>\n                </mat-select>\n              </li>\n              <div class=\"clearfix\"></div>\n            </ul>\n            <div class=\"clearfix\"></div>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n\n        <div class=\"col-xs-12\">\n\n          <button type=\"button\" class=\"btn btn-success zenwork-custom-proceed-btn\" (click)=\"saveEditDetails()\">Save &\n            continue\n          </button>\n          <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\" (click)=\"cancel()\">Cancel</button>\n\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n\n      <div id=\"tab2\" class=\"tab-pane\" [ngClass]=\"{ 'active':activeTab==='package','in':activeTab==='package'}\">\n\n        <div class=\"package-billing-main\">\n\n\n          <div class=\"package-billing-history\">\n            Biling History of {{companyNameValue}}\n            <div class=\"setting-table\">\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>\n                      Features/Package\n                    </th>\n                    <th>Add-Ons</th>\n                    <th>Start Date</th>\n                    <th>Price</th>\n                    <th>Status</th>\n                    <th>End Date</th>\n\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let package of billingHistoryPackages\">\n                    <td *ngFor=\"let pack of package\">{{pack.name}}</td>\n                    <td></td>\n                    <td *ngFor=\"let pack of package\">{{pack.startDate | date}}</td>\n                    <td *ngFor=\"let pack of package\">{{pack.price}}</td>\n                    <td *ngFor=\"let pack of package\"> {{pack.payment_status}}</td>\n                    <td *ngFor=\"let pack of package\">{{pack.endDate | date}}</td>\n                  </tr>\n                  <tr *ngFor=\"let adons of billingHistoryAddons\">\n                    <td></td>\n                    <td *ngFor=\" let adon of adons\">{{adon.id.name}}</td>\n                    <td *ngFor=\" let adon of adons\">{{adon.startDate | date}}</td>\n                    <td *ngFor=\" let adon of adons\">{{adon.price || NA}}</td>\n\n                    <td *ngFor=\" let adon of adons\">{{adon.payment_status}} </td>\n                    <td *ngFor=\" let adon of adons\">{{adon.endDate | date}}</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n\n        </div>\n\n\n        <div class=\"clearfix\"></div>\n      </div>\n\n\n      <div id=\"tab3\" class=\"tab-pane\" [ngClass]=\"{'active':activeTab ==='payment','in':activeTab ==='payment'}\">\n\n        <div class=\"package-billing-main\">\n\n          <div class=\"col-md-7 discount-block\">\n            <ul class=\"list-unstyled discount-heading\">\n              <li class=\"list-items\">\n                <span [ngClass]=\"{'border-bottom-green':!selectedPackage.wholeSaleDiscount}\">Whole sale discount\n                  <br>\n                  <small>(Reseller is billed)</small>\n                </span>\n\n              </li>\n              <li class=\"list-items\">\n                <mat-slide-toggle [(ngModel)]=\"selectedPackage.wholeSaleDiscount\"></mat-slide-toggle>\n              </li>\n              <li class=\"list-items\">\n                <span [ngClass]=\"{'border-bottom-green':selectedPackage.wholeSaleDiscount}\">Direct discount\n                  <br>\n                  <small>(Client is billed)</small>\n                </span>\n\n              </li>\n              <li class=\"list-items\">\n                <button [disabled]=\"discountEnable\" class=\"btn\" (click)=\"addAnotherAddOn()\">\n                  <small>\n                    <i aria-hidden=\"true\" class=\"fa fa-plus\"></i>\n                  </small> Add-On\n                </button>\n              </li>\n            </ul>\n            <div class=\"package-table\">\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>Package</th>\n                    <th>Add-On\n                      <span class=\"green\">+</span>\n                    </th>\n                    <th>\n                      Discount\n                    </th>\n                    <th>Start Date</th>\n                    <th>End Date</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let package of currentPlanPackage\">\n                    <td>\n                      {{package.name}}\n                    </td>\n                    <td>\n\n                    </td>\n                    <td>\n\n                    </td>\n                    <td>\n                      {{package.startDate | date}}\n                    </td>\n                    <td>\n                      {{package.endDate | date}}\n                    </td>\n                  </tr>\n                  <tr>\n                    <td class=\"package-name\">\n                      <mat-select class=\"form-control zenwork-input package-table-select\"\n                        [(ngModel)]=\"selectedPackage.name\" placeholder=\"Select Package\"\n                        (ngModelChange)=\"getSpecificPackageDetails()\">\n                        <mat-option [value]=\"package.name\" *ngFor=\"let package of packages\">\n                          {{package.name}}\n                        </mat-option>\n                      </mat-select>\n                    </td>\n                    <td>\n\n                    </td>\n                    <td>\n                      <input type=\"text\" (keypress)=\"keyPress($event)\"\n                        class=\"form-control zenwork-input package-table-select\"\n                        [(ngModel)]=\"discountValue\" [disabled]=\"discountEnable\" maxlength=\"3\" placeholder=\"discount\"\n                        (ngModelChange)=\"discountBasePrice()\">\n                    </td>\n                    <td>\n                      <div class=\"date\">\n                        {{selectedPackage.startDate | date}}\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </td>\n                    <td>\n\n                      <div class=\"date\">\n                        {{selectedPackage.endDate | date}}\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n                    </td>\n                  </tr>\n                  <tr *ngFor=\"let packageAddon of selectedPackage.adons\">\n                    <td>\n\n                    </td>\n                    <td class=\"add-on-type\">\n                      <span class=\"add-on-cancel\" (click)=\"addonRemove(packageAddon.selectedAddOn)\">\n                        <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                      </span>\n                      <mat-select class=\"form-control zenwork-input package-table-select add-on-select\"\n                        placeholder=\"Select Add-on\" [(ngModel)]=\"packageAddon.selectedAddOn\"\n                        (ngModelChange)=\"addOnChange(packageAddon.selectedAddOn)\">\n                        <mat-option [value]=\"addOn.name\" *ngFor=\"let addOn of allAddOns\">\n                          {{addOn.name}}\n                        </mat-option>\n                      </mat-select>\n                    </td>\n                    <td>\n\n                    </td>\n                    <td>\n                      {{selectedPackage.startDate | date }}\n                    </td>\n                    <td>\n                      {{selectedPackage.endDate | date}}\n                    </td>\n                  </tr>\n                  <!-- <tr>\n                      <td>\n  \n                      </td>\n                      <td class=\"add-on-type\">\n                        <span class=\"add-on-cancel\">\n                          <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                        </span>\n                        <mat-select class=\"form-control zenwork-input package-table-select add-on-select\" placeholder=\"BioMetric Access/Time Stamp\">\n                          <mat-option value=\"ACA\">\n                            BioMetric Access/Time Stamp\n                          </mat-option>\n                          <mat-option value=\"BioMetric\">\n                            ACA Comliance\n                          </mat-option>\n                          <mat-option value=\"Basic\">\n                            Advanced Training\n                          </mat-option>\n                        </mat-select>\n                      </td>\n                      <td>\n                        12/12/2018\n                      </td>\n                      <td>\n                        12/12/2019\n                      </td>\n                    </tr> -->\n                </tbody>\n              </table>\n            </div>\n          </div>\n\n          <div class=\"payment-rgt col-md-5\">\n\n\n            <div class=\"billing-block\">\n              <h2>\n                <span>Billing</span>\n                <div class=\"annualy-block\">\n                  <p>Monthly</p>\n                  <mat-slide-toggle (change)=\"toggleDateChange($event)\"></mat-slide-toggle>\n                  <small>Annually</small>\n                  <div class=\"clearfix\"></div>\n                </div>\n                <div class=\"clearfix\"></div>\n              </h2>\n\n              <ul>\n\n                <li>\n                  <div class=\"billing-price\">\n                    <ul>\n                      <li>\n                        <h5>Package</h5>\n                      </li>\n                      <li>\n                        <span>{{selectedPackage.name}}</span>\n                        <!-- <small>$ {{selectedPackage.price}}</small> -->\n\n                        <div class=\"clearfix\"></div>\n                      </li>\n\n                      <li>\n                        <span>Base Price</span>\n                        <small>$ {{selectedPackage.price}}</small>\n                        <div class=\"clearfix\"></div>\n                      </li>\n                    </ul>\n                  </div>\n                </li>\n\n                <li>\n                  <div class=\"billing-price\">\n                    <ul>\n\n                      <li>\n                        <span>Number of Employees</span>\n                        <var>{{addedCompanyData.employeCount}}</var>\n                        <div class=\"clearfix\"></div>\n                      </li>\n                      <li>\n                        <span>Total Employee Cost</span>\n                        <small>$ {{selectedPackage.costPerEmployee * addedCompanyData.employeCount}}</small>\n                        <div class=\"clearfix\"></div>\n                      </li>\n                    </ul>\n                  </div>\n                </li>\n\n                <li>\n                  <div class=\"billing-price\">\n                    <ul>\n                      <li>\n                        <h5>Add-on</h5>\n                      </li>\n                      <li *ngFor=\"let adon of selectedPackage.adons\">\n                        <span>{{adon.selectedAddOn}}</span>\n                        <small>{{adon.price}}</small>\n                        <div class=\"clearfix\"></div>\n                      </li>\n                    </ul>\n                  </div>\n                </li>\n\n                <li class=\"no-bor\">\n                  <div class=\"billing-price\">\n                    <ul>\n                      <li>\n                        <span>Discount</span>\n                        <small>{{discountValue}}%</small>\n                        <div class=\"clearfix\"></div>\n                      </li>\n                      <li>\n                        <span>Total Price</span>\n                        <small>$ {{selectedPackage.totalPrice}}</small>\n                        <div class=\"clearfix\"></div>\n                      </li>\n\n                      <li>\n                        <span>Tax Rate @ {{taxrate.tax_rate}} %</span>\n                        <small>$ {{(selectedPackage.totalPrice * (taxrate.tax_rate))/100}}</small>\n                        <div class=\"clearfix\"></div>\n                      </li>\n                    </ul>\n                  </div>\n                </li>\n\n              </ul>\n\n              <h4>\n                <h1>Grand Price</h1>\n                <p>$ {{ selectedPackage.totalPrice + selectedPackage.totalPrice*(taxrate.tax_rate)/100}}</p>\n                <!-- <p>$ {{ selectedPackage.totalPrice + selectedPackage.totalPrice*0.08}}</p> -->\n                <div class=\"clearfix\"></div>\n              </h4>\n\n            </div>\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox2\" type=\"checkbox\">\n                <label for=\"checkbox2\">Make me the\n                  <small> Master Admin</small> of this client's company.</label>\n              </div>\n            </div>\n\n            <!-- <a class=\"btn\" (click)=\"savePackageDetails(stepper)\">Save & Continue</a>\n            <a class=\"btn\">Cancel</a> -->\n            <div class=\"clearfix\"></div>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n        <div class=\"col-xs-12\" style=\"margin: 20px 0 0 0\">\n\n\n          <button type=\"button\" class=\"btn zenwork-customized-transparent-btn pull-left\"\n            (click)=\"cancel()\">Cancel</button>\n          <button [disabled]=\"discountEnable\" type=\"button\"\n            class=\"btn btn-success zenwork-custom-proceed-btn pull-right\"\n            (click)=\"savePackageDetails($event)\">Save</button>\n        </div>\n        <div class=\"clearfix\"></div>\n\n\n\n      </div>\n\n\n      <div id=\"tab4\" class=\"tab-pane\" [ngClass]=\"{'active':activeTab ==='finished','in':activeTab ==='finished'}\">\n\n        <div class=\"payment-lft col-md-7\">\n          <h2>Select Payment Method</h2>\n          <!-- <a>Secure Payment Details</a> -->\n          <div class=\"clearfix\"></div>\n\n          <div class=\"payment-method\">\n\n            <div class=\"panel-group\" id=\"accordion1\">\n\n              <div class=\"panel panel-default\">\n\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwo\">\n                      <span></span> Credit Card Payment\n                    </a>\n                  </h4>\n                </div>\n\n                <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body credit-card-payment\">\n\n                    <div class=\"credit-payment\">\n                      <!-- Credit Card PaymentCredit Card PaymentCredit Card PaymentCredit Card Payment -->\n                      <div class=\"card-buttons\">\n                        <ul class=\"list-unstyled\">\n                          <li class=\"list-items pull-left\">\n                            <a class=\"btn btn-default my-cards-btn \">My Cards</a>\n                          </li>\n                          <li class=\"list-items pull-left\">\n                            <a class=\"btn btn-default my-cards-btn \">Add New Card</a>\n                          </li>\n                          <li class=\"list-items pull-right\">\n                            <a class=\"btn btn-default secure-cards-btn \">Secure Credit Card Payment Details</a>\n                          </li>\n\n                        </ul>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </div>\n                    <div class=\"wrapper\">\n                      <ul class=\"list-unstyled\">\n                        <li class=\"list-items\">\n                          <div class=\"form-group col-xs-7\">\n                            <label>Name On Card*</label>\n                            <input type=\"text\" placeholder=\"Name\" [(ngModel)]=\"cardDetails.cardName\"\n                              class=\"form-control\" maxlength=\"18\" maxlength=\"18\" name=\"cardName\" required\n                              #cardName=\"ngModel\">\n                            <span\n                              *ngIf=\"!cardDetails.cardName && cardName.touched || (!cardDetails.cardName && isValidcard)\"\n                              style=\"color:#e44a49; padding:6px 0 0;\">Enter name on card</span>\n                          </div>\n                        </li>\n                        <li class=\"list-items\">\n                          <div class=\"form-group col-xs-7\">\n                            <label>Card Number*</label>\n                            <input type=\"text\" placeholder=\"Card Number\" [(ngModel)]=\"cardDetails.cardNumber\"\n                              class=\"form-control\" (keypress)=\"keyPress($event)\" maxlength=\"16\" name=\"cardNumber\"\n                              required #cardNumber=\"ngModel\">\n                            <span\n                              *ngIf=\"!cardDetails.cardNumber && cardNumber.touched || (!cardDetails.cardNumber && isValidcard)\"\n                              style=\"color:#e44a49; padding:6px 0 0;\">Enter card number</span>\n                          </div>\n                        </li>\n                        <li class=\"list-items credit-card-details-date\">\n                          <ul class=\"list-unstyled col-xs-8\">\n                            <li class=\"in-list-items\">\n                              <div class=\"form-group\">\n                                <label>Card expiry month*</label>\n                                <!-- <input type=\"text\" placeholder=\"Expiration Month\" maxlength=\"02\"\n                                  [(ngModel)]=\"cardDetails.expMonth\" (keypress)=\"keyPress($event)\" class=\"form-control\"\n                                  name=\"expMonth\" required #expMonth=\"ngModel\"> -->\n                                <mat-select class=\"form-control\" placeholder=\"Expiration Month\"\n                                  [(ngModel)]=\"cardDetails.expMonth\" name=\"expMonth\" required #expMonth=\"ngModel\">\n                                  <mat-option *ngFor=\"let data of monthsArray\" [value]=\"data\">\n                                    {{data}}\n                                  </mat-option>\n\n                                </mat-select>\n                                <span\n                                  *ngIf=\"!cardDetails.expMonth && expMonth.touched || (!cardDetails.expMonth && isValidcard)\"\n                                  style=\"color:#e44a49; padding:6px 0 0;\">Enter expire month</span>\n                              </div>\n                            </li>\n                            <li class=\"in-list-items\">\n                              <div class=\"form-group\">\n                                <!-- <input type=\"text\" placeholder=\"Expiration Year\" maxlength=\"4\"\n                                  [(ngModel)]=\"cardDetails.expYear\" (keypress)=\"keyPress($event)\" class=\"form-control\"\n                                  name=\"expYear\" required #expYear=\"ngModel\"> -->\n\n                                <mat-select class=\"form-control\" placeholder=\"Expiration Year\"\n                                  [(ngModel)]=\"cardDetails.expYear\" name=\"expYear\" required #expYear=\"ngModel\">\n                                  <mat-option *ngFor=\"let data of yearsArray\" [value]=\"data\">\n                                    {{data}}\n                                  </mat-option>\n\n                                </mat-select>\n\n                                <span\n                                  *ngIf=\"!cardDetails.expYear && expYear.touched || (!cardDetails.expYear && isValidcard)\"\n                                  style=\"color:#e44a49; padding:6px 0 0;\">Enter expire year</span>\n                              </div>\n                            </li>\n                            <li class=\"in-list-items\">\n                              <div class=\"form-group\">\n                                <label>CVV*</label>\n                                <input type=\"text\" placeholder=\"CVV\" maxlength=\"4\" (keypress)=\"keyPress($event)\"\n                                  [(ngModel)]=\"cardDetails.cvv\" class=\"form-control\" name=\"cvv\" required #cvv=\"ngModel\">\n                                <span *ngIf=\"!cardDetails.cvv && cvv.touched || (!cardDetails.cvv && isValidcard)\"\n                                  style=\"color:#e44a49; padding:6px 0 0;\">Enter cvv number</span>\n                              </div>\n                            </li>\n\n                          </ul>\n                        </li>\n                      </ul>\n                      <!-- <div class=\"form-group\">\n                            <button (click)=\"chargeCreditCard()\">Submit</button>\n                          </div> -->\n                    </div>\n                    <div class=\"agree-policy col-xs-7\">\n                      <ul class=\"list-unstyled terms\">\n                        <li class=\"list-items\">\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"> I have read accept terms and privacy\n                            policy</mat-checkbox>\n\n                        </li>\n                        <li class=\"list-items\">\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"> I agree to allow this credit card to be\n                            used for automatic recurring payments</mat-checkbox>\n                        </li>\n                      </ul>\n                    </div>\n\n                  </div>\n                </div>\n              </div>\n              <div class=\"panel panel-default\">\n\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n                      <span></span> ECheck/ACH Payment\n                    </a>\n                  </h4>\n                </div>\n\n                <div id=\"collapseOne\" class=\"panel-collapse collapse \">\n                  <div class=\"panel-body\">\n\n                    <div class=\"ach-payment\">\n                      <!-- ECheck/ACH Payment ECheck/ACH Payment ECheck/ACH Payment -->\n                    </div>\n\n\n                  </div>\n                </div>\n              </div>\n\n\n              <div class=\"panel panel-default\">\n\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseThree\">\n                      <span></span> PayPal Payment\n                    </a>\n                  </h4>\n                </div>\n\n                <div id=\"collapseThree\" class=\"panel-collapse collapse\">\n                  <div class=\"panel-body\">\n\n                    <div class=\"credit-payment\">\n                      <!-- PayPal PaymentPayPal PaymentPayPal PaymentPayPal PaymentPayPal Payment -->\n                    </div>\n\n                  </div>\n                </div>\n              </div>\n\n\n\n\n\n            </div>\n\n          </div>\n\n\n\n        </div>\n\n        <div class=\"col-xs-12\" style=\"margin: 20px 0 0 0\">\n\n\n          <button type=\"button\" class=\"btn zenwork-customized-transparent-btn pull-left\"\n            (click)=\"cancel()\">Cancel</button>\n          <button type=\"button\" class=\"btn btn-success zenwork-custom-proceed-btn pull-right\"\n            (click)=\"chargeCreditCard()\">Save</button>\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n\n    </div>\n\n\n\n\n  </div>\n</div>\n<div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/create-company/create-company.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/create-company/create-company.component.ts ***!
  \**********************************************************************************/
/*! exports provided: CreateCompanyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCompanyComponent", function() { return CreateCompanyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/purchasePackage.service */ "./src/app/services/purchasePackage.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/chardgeCreditCard.service */ "./src/app/services/chardgeCreditCard.service.ts");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/message-service.service */ "./src/app/services/message-service.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var CreateCompanyComponent = /** @class */ (function () {
    function CreateCompanyComponent(companyService, swalAlertService, router, activatedRoute, packageAndAddOnService, purchasePackageService, accessLocalStorageService, creditCardChargingService, messageService, companySettingsService, ngZone) {
        this.companyService = companyService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.packageAndAddOnService = packageAndAddOnService;
        this.purchasePackageService = purchasePackageService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.creditCardChargingService = creditCardChargingService;
        this.messageService = messageService;
        this.companySettingsService = companySettingsService;
        this.ngZone = ngZone;
        this.selectedPackage = {
            clientId: '',
            name: '',
            wholeSaleDiscount: false,
            discount: 0,
            costPerEmployee: 0,
            price: 0,
            startDate: new Date(),
            endDate: new Date(new Date().setMonth(new Date().getMonth() + 1)),
            adons: [],
            totalPrice: 0,
            packages: [],
            billingFrequency: 'monthly'
        };
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: 0,
            email: '',
            type: '',
            belongsToReseller: false,
            resellerId: '',
            primaryContact: { name: '', email: '', phone: '', extention: '' },
            billingContact: { name: '', email: '', phone: '', extention: '' },
            companyDetails: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            primaryAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            isPrimaryContactIsBillingContact: true,
            isPrimaryAddressIsBillingAddress: true
        };
        this.selectedPackageData = {};
        this.addedCompanyData = {};
        this.billingHistory = {};
        this.currentPlan = {};
        this.discountEnable = true;
        this.validity = false;
        this.billingHistoryAddons = [];
        this.billingHistoryPackages = [];
        this.currentPlanAddons = [];
        this.currentPlanPackage = [];
        this.resellersList = [];
        this.adonsPrice = 0;
        this.dicountArray = [
            { value: 10, viewValue: '10%' },
            { value: 20, viewValue: '20%' },
            { value: 30, viewValue: '30%' },
            { value: 40, viewValue: '40%' },
            { value: 50, viewValue: '50%' },
            { value: 60, viewValue: '60%' },
            { value: 70, viewValue: '70%' },
            { value: 80, viewValue: '80%' },
            { value: 90, viewValue: '90%' },
        ];
        this.monthsArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        this.yearsArray = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028];
        this.editCompamnyDetails = false;
        this.taxrate = {
            tax_rate: '',
            _id: ''
        };
        this.allStates = [];
        this.isValidcard = false;
        this.allCountryes = [];
        this.cardDetails = {
            cardNumber: '',
            expMonth: '',
            expYear: '',
            cvv: '',
            cardName: ''
        };
    }
    CreateCompanyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activeTab = 'create';
        this.discountValue = '';
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: 0,
            email: '',
            type: '',
            belongsToReseller: false,
            resellerId: '',
            primaryContact: { name: '', email: '', phone: '', extention: '' },
            billingContact: { name: '', email: '', phone: '', extention: '' },
            companyDetails: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            primaryAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            isPrimaryContactIsBillingContact: true,
            isPrimaryAddressIsBillingAddress: true
        };
        this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        if (this.addedCompanyData == null) {
            this.addedCompanyData = {};
        }
        this.activatedRoute.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log("iddddd", _this.id, params);
            if (_this.id) {
                _this.editCompamnyDetails = true;
                _this.activeTab = JSON.parse(localStorage.getItem('viewTab'));
            }
        });
        console.log('activeTab', this.activeTab);
        this.getSingleUser();
        this.getAllPackages();
        this.getAllAddons();
        this.getSingleCompanyBillingHistory();
        this.getSingleComapnyCurrentPlan();
        this.getAllresellers();
        this.getTaxRate();
        this.getAllUSstates();
        this.getAllCountry();
    };
    /* Description: get all us-states form dtructure
   author : vipin reddy */
    CreateCompanyComponent.prototype.getAllUSstates = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('State', 0)
            .subscribe(function (res) {
            console.log(res);
            if (res.custom.length > 0) {
                res.custom.forEach(function (element) {
                    _this.allStates.push(element);
                    console.log("22222111111", _this.allStates);
                });
            }
            if (res.default.length > 0) {
                res.default.forEach(function (element) {
                    _this.allStates.push(element);
                });
                console.log("22222", _this.allStates);
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get taxrate
     author : vipin reddy */
    CreateCompanyComponent.prototype.getTaxRate = function () {
        var _this = this;
        this.packageAndAddOnService.getTaxRate()
            .subscribe(function (res) {
            console.log("Response", res);
            _this.taxrate = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get single client details
   author : vipin reddy */
    CreateCompanyComponent.prototype.getSingleUser = function () {
        var _this = this;
        this.companyService.editClient(this.id)
            .subscribe(function (res) {
            console.log("editres", res);
            _this.companyDetails = res.data;
            if (_this.companyDetails.type == 'reseller-client') {
                var x = _this.companyDetails.type;
                _this.companyDetails.type = 'company';
                _this.companyDetails.belongsToReseller = true;
            }
            if (_this.companyDetails.type == "company" && x != 'reseller-client') {
                _this.companyDetails.type = 'company';
                _this.companyDetails.belongsToReseller = false;
            }
            if (_this.companyDetails.employeCount == null) {
                _this.companyDetails.employeCount = 0;
            }
            if (_this.companyDetails.billingAddress == undefined) {
                _this.companyDetails.primaryContact = {
                    name: '',
                    email: '',
                    phone: '',
                    extention: ''
                };
                _this.companyDetails.primaryAddress = {
                    address1: "",
                    address2: "",
                    city: "",
                    country: "",
                    state: "",
                    zipcode: ""
                };
                _this.companyDetails.billingAddress = {
                    address1: "",
                    address2: "",
                    city: "",
                    country: "",
                    state: "",
                    zipcode: ""
                };
            }
            else {
                _this.companyDetails.primaryContact = res.data.primaryContact;
                _this.companyDetails.billingContact = res.data.billingAddress;
                _this.companyDetails.primaryAddress = res.data.primaryAddress;
                _this.companyDetails.billingAddress = res.data.billingAddress;
            }
            if (res.order) {
                var date = new Date(res.order.endDate);
                console.log(date);
                _this.comapnyInactiveDate = date.toISOString();
                console.log(_this.comapnyInactiveDate);
                var today = new Date().toISOString();
                if (today < _this.comapnyInactiveDate) {
                    _this.clientStatus = "Active";
                }
                else {
                    _this.clientStatus = "Inactive";
                }
                if (res.order == null) {
                    _this.clientStatus = "Inactive";
                    _this.comapnyInactiveDate = "Inactive";
                }
                else {
                    _this.comapnyInactiveDate = res.order.endDate;
                    var d = new Date(_this.comapnyInactiveDate);
                    _this.comapnyInactiveDate = d.toLocaleDateString();
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get all us-states
     author : vipin reddy */
    CreateCompanyComponent.prototype.getAllCountry = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('Country', 0)
            .subscribe(function (res) {
            console.log(res);
            if (res.custom.length > 0) {
                res.custom.forEach(function (element) {
                    _this.allCountryes.push(element);
                    console.log("countryes", _this.allCountryes);
                });
            }
            if (res.default.length > 0) {
                res.default.forEach(function (element) {
                    _this.allCountryes.push(element);
                });
                console.log("countryes", _this.allCountryes);
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get a billing history of edited company
   author : vipin reddy */
    CreateCompanyComponent.prototype.getSingleCompanyBillingHistory = function () {
        var _this = this;
        this.addedCompanyData = this.accessLocalStorageService.get('editCompany');
        console.log(this.addedCompanyData);
        this.companyNameValue = this.addedCompanyData.name;
        console.log(this.companyNameValue);
        this.id = this.addedCompanyData._id;
        this.companyService.getBillingHistory(this.id)
            .subscribe(function (res) {
            console.log("billinghistory", res);
            if (res.data) {
                _this.billingHistory = res.data;
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].adons.length >= 1) {
                        if (res.data[i].adons.length == 1) {
                            _this.billingHistoryAddons.push(res.data[i].adons);
                        }
                        if (res.data[i].adons.length > 1) {
                            console.log(res.data[i].adons);
                            for (var j = 0; j < res.data[i].adons.length; j++) {
                                _this.billingHistoryAddons.push([res.data[i].adons[j]]);
                            }
                        }
                    }
                    if (res.data[i].packages.length >= 1) {
                        _this.billingHistoryPackages.push(res.data[i].packages);
                    }
                    console.log("adins,packages", _this.billingHistoryAddons, _this.billingHistoryPackages);
                }
            }
            else {
                _this.billingHistoryPackages = [];
                _this.billingHistoryAddons = [];
            }
        }, function (err) {
        });
    };
    // this.companyDetails = {
    //   name: '',
    //   tradeName: '',
    //   employeCount: 0,
    //   email: '',
    //   type: '',
    //   belongsToReseller: false,
    //   resellerId: '',
    //   primaryContact: { name: '', email: '', phone: '', extention: '' },
    //   billingContact: { name: '', email: '', phone: '', extention: '' },
    //   
    //   primaryAddress: {
    //     address1: '',
    //     address2: '',
    //     city: '',
    //     state: '',
    //     zipcode: '',
    //     country: ''
    //   },
    //   billingAddress: {
    //     address1: '',
    //     address2: '',
    //     city: '',
    //     state: '',
    //     zipcode: '',
    //     country: ''
    //   },
    //   isPrimaryContactIsBillingContact: true,
    //   isPrimaryAddressIsBillingAddress: true
    // }
    /* Description: save edited contact details
 author : vipin reddy */
    CreateCompanyComponent.prototype.saveEditDetails = function () {
        var _this = this;
        this.isValid = true;
        var comapnyData = this.accessLocalStorageService.get('editCompany');
        console.log(this.companyDetails);
        this.id = comapnyData._id;
        if (this.companyDetails.name && this.companyDetails.tradeName && this.companyDetails.employeCount &&
            this.companyDetails.type && this.companyDetails.primaryContact.name && this.companyDetails.primaryContact.email &&
            this.companyDetails.primaryContact.phone && this.companyDetails.primaryContact.extention && this.companyDetails.primaryAddress.address1 &&
            this.companyDetails.primaryAddress.address2 && this.companyDetails.primaryAddress.city && this.companyDetails.primaryAddress.state &&
            this.companyDetails.primaryAddress.zipcode && this.companyDetails.primaryAddress.country) {
            this.companyService.editDataSave(this.companyDetails, this.id)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Client Details", res.message, 'success');
                    _this.activeTab = "package";
                    console.log("this.activeTab", _this.activeTab);
                }
                console.log(_this.activeTab);
            }, function (err) {
            });
        }
    };
    /* Description: save single comapny current plan
   author : vipin reddy */
    CreateCompanyComponent.prototype.getSingleComapnyCurrentPlan = function () {
        var _this = this;
        var comapnyData = this.accessLocalStorageService.get('editCompany');
        console.log(comapnyData);
        this.id = comapnyData._id;
        this.companyService.currentPlan(this.id)
            .subscribe(function (res) {
            console.log("currentPlan", res);
            if (res.data) {
                _this.currentPlan = res.data;
                _this.currentPlanAddons = res.data.adons;
                _this.currentPlanPackage = res.data.packages;
            }
            else {
                _this.currentPlan = [];
                _this.currentPlanAddons = [];
                _this.currentPlanPackage = [];
            }
        }, function (err) {
        });
    };
    // discountBasePrice() {
    //   console.log(this.discountValue);
    //   if (this.discountValue <= 100) {
    //     console.log(x, this.dummy2);
    //     var x = (this.dummy2 * this.discountValue / 100)
    //     this.selectedPackage.totalPrice = this.dummy2 - x;
    //     console.log(this.selectedPackage.totalPrice);
    //     // this.getSpecificPackageDetails();
    //   }
    //   else {
    //     this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error')
    //     this.discountValue = 0
    //     this.getSpecificPackageDetails();
    //     this.addOnChange();
    //   }
    // }
    /* Description: discont price when editing
   author : vipin reddy */
    CreateCompanyComponent.prototype.discountBasePrice = function () {
        console.log(this.discountValue);
        if (this.discountValue <= 100) {
            var x = (this.tempforDiscountChange * this.discountValue / 100);
            this.selectedPackage.totalPrice = this.tempforDiscountChange - x;
            // this.getSpecificPackageDetails();
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error');
            this.discountValue = '';
            this.getSpecificPackageDetails();
            this.addOnChange();
        }
    };
    /* Description: time-period change in packages and billing monthly and yearly
   author : vipin reddy */
    CreateCompanyComponent.prototype.toggleDateChange = function (event) {
        console.log(event.checked);
        if (event.checked == true) {
            this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
            this.selectedPackage.billingFrequency = 'annually';
            this.selectedPackage.costPerEmployee = this.selectedPackage.costPerEmployee * 12;
            this.validity = true;
        }
        else {
            this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
            this.selectedPackage.costPerEmployee = this.selectedPackage.costPerEmployee;
            this.validity = false;
            this.selectedPackage.billingFrequency = 'monthly';
        }
        this.getSpecificPackageDetails();
        // if (this.selectedPackage.adons.lenght > 0) {
        //   // this.get
        // }
    };
    /* Description: upgrade downgrade package payment for stripe
 author : vipin reddy */
    CreateCompanyComponent.prototype.chargeCreditCard = function () {
        var _this = this;
        this.isValidcard = true;
        window.Stripe.card.createToken({
            number: this.cardDetails.cardNumber,
            exp_month: this.cardDetails.expMonth,
            exp_year: this.cardDetails.expYear,
            cvc: this.cardDetails.cvv
        }, function (status, response) {
            if (status === 200) {
                var token = response.id;
                console.log(token);
                // this.chargeCard(token);
                var id = JSON.parse(localStorage.getItem("editCompany"));
                console.log(id);
                var amount = (_this.selectedPackage.totalPrice + (_this.taxrate.tax_rate)) / 100;
                // var amount = this.selectedPackage.totalPrice + this.selectedPackage.totalPrice * 0.08
                amount = Math.round(amount * 100);
                console.log(amount);
                _this.creditCardChargingService.createCharge(token, id._id, amount, _this.orderId)
                    .subscribe(function (response) {
                    console.log(response);
                    if (response.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success');
                        // this.messageService.sendMessage('super-admin');
                        // localStorage.removeItem('viewTab');
                        _this.cardDetails.cardNumber = '';
                        _this.cardDetails.expMonth = '';
                        _this.cardDetails.expYear = '';
                        _this.cardDetails.cvv = '';
                        _this.ngZone.run(function () {
                            _this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                console.log(response.error.message);
            }
        });
    };
    CreateCompanyComponent.prototype.cancel = function () {
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: 0,
            email: '',
            type: '',
            belongsToReseller: false,
            resellerId: '',
            primaryContact: { name: '', email: '', phone: '', extention: '' },
            billingContact: { name: '', email: '', phone: '', extention: '' },
            companyDetails: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            primaryAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            isPrimaryContactIsBillingContact: true,
            isPrimaryAddressIsBillingAddress: true
        };
    };
    /* Description: get all packages
author : vipin reddy */
    CreateCompanyComponent.prototype.getAllPackages = function () {
        var _this = this;
        this.packageAndAddOnService.getAllPackages()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.packages = res.data;
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: discount toggle setup
    author : vipin reddy */
    CreateCompanyComponent.prototype.wholesaleDiscountChange = function ($event) {
        console.log($event, this.companyDetails.type);
        if (this.companyDetails.type == 'reseller') {
            this.selectedPackage.wholeSaleDiscount = false;
        }
        else {
            this.selectedPackage.wholeSaleDiscount = true;
        }
    };
    /* Description: get selected package details
    author : vipin reddy */
    CreateCompanyComponent.prototype.getSpecificPackageDetails = function () {
        var _this = this;
        this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        console.log(this.addedCompanyData);
        if (this.editCompamnyDetails) {
            console.log("sdasdas");
            this.addedCompanyData = this.accessLocalStorageService.get('editCompany');
        }
        console.log(this.selectedPackage.name, this.packages);
        this.packages.forEach(function (packageData) {
            if (packageData.name == _this.selectedPackage.name) {
                _this.selectedPackage.packages = _this.packages.filter(function (data) { return data.name == _this.selectedPackage.name; });
                console.log(_this.selectedPackage.packages);
                if (_this.validity == false) {
                    _this.selectedPackage.price = packageData.price;
                }
                else {
                    _this.selectedPackage.price = (packageData.price * 12);
                }
                if (_this.validity == false)
                    _this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
            }
        });
        for (var i = 0; i < this.allAddOns.length; i++) {
            console.log(this.allAddOns);
            for (var j = 0; j < this.selectedPackage.adons.length; j++) {
                console.log(this.selectedPackage.adons[j]);
                if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn) {
                    if (this.validity == false) {
                        this.getAllAddons();
                        this.adonsPrice = 0;
                        if (this.allAddOns[i].name == this.selectedPackage.adons[j].selectedAddOn)
                            this.selectedPackage.adons[j].price = this.allAddOns[i].price;
                    }
                    else {
                        this.selectedPackage.adons[j].price = this.selectedPackage.adons[j].price * 12;
                    }
                    // this.selectedPackage.costPerEmployee = this.allAddOns[i].costPerEmployee;
                }
            }
        }
        //   this.allAddOns.forEach(element => {
        //   this.selectedPackage.adons.forEach(selectAdonsData => {
        //     if (element.name == selectAdonsData.selectedAddOn) {
        //       if (this.validity == false) {
        //         this.selectedPackage.adons = selectAdonsData.price;
        //       }
        //       else {
        //         this.selectedPackage.adons = (selectAdonsData.price * 12);
        //         // selectAdonsData.price
        //       }
        //     }
        //   });
        // });
        this.adonsPrice = 0;
        this.selectedPackage.adons.forEach(function (element) {
            console.log(element);
            _this.adonsPrice = _this.adonsPrice + element.price;
        });
        console.log(this.adonsPrice);
        this.selectedPackage.totalPrice = this.selectedPackage.price + this.adonsPrice + this.selectedPackage.costPerEmployee * this.addedCompanyData.employeCount;
        // if (this.basePriceDiscount > 0) {
        //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
        // }
        this.discountEnable = false;
        /* this.selectedPackageData = packageData; */
        /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              if (res.status) {
                this.selectedPackageData = res.data;
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err)
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          ) */
        this.dummy = this.selectedPackage.totalPrice;
        this.tempforDiscountChange = this.selectedPackage.totalPrice;
    };
    /* Description: get all addons
    author : vipin reddy */
    CreateCompanyComponent.prototype.getAllAddons = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                console.log(_this.allAddOns);
                _this.allAddOns.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: selecting reseller edit
  author : vipin reddy */
    CreateCompanyComponent.prototype.selectedReseller = function (id) {
        console.log(id);
    };
    /* Description: get all resellers
  author : vipin reddy */
    CreateCompanyComponent.prototype.getAllresellers = function () {
        var _this = this;
        this.companyService.getresellers()
            .subscribe(function (res) {
            console.log("reselerrsList", res);
            _this.resellersList = res.data;
        }, function (err) {
        });
    };
    /* Description: get all addons
  author : vipin reddy */
    CreateCompanyComponent.prototype.getAllAddOnDetails = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                _this.allAddOns.forEach(function (addon) {
                    addon.isSelected = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: add another addon to order
   author : vipin reddy */
    CreateCompanyComponent.prototype.addAnotherAddOn = function () {
        console.log('adons add', this.selectedPackage);
        this.selectedPackage.adons.push({
            id: '',
            selectedAddOn: '',
            price: 0,
            index: this.selectedPackage.adons.length + 1
        });
        // this.selectedPackage.addOns = []
    };
    /* Description: addon change
 author : vipin reddy */
    CreateCompanyComponent.prototype.addOnChange = function () {
        // console.log(addOn);
        var _this = this;
        console.log("before", this.selectedPackage.adons);
        var _loop_1 = function (i) {
            this_1.selectedPackage.adons.forEach(function (addon) {
                console.log(addon, _this.allAddOns[i]);
                if (addon.selectedAddOn == _this.allAddOns[i].name) {
                    addon.price = _this.allAddOns[i].price;
                    if (_this.validity == true) {
                        addon.price = addon.price * 12;
                    }
                    addon.id = _this.allAddOns[i]._id;
                    console.log(addon.id);
                    _this.selectedAddons = _this.selectedPackage.adons[i];
                    // this.selectedAddons['id'] = addon.id;
                    console.log(_this.selectedAddons);
                }
            });
        };
        var this_1 = this;
        // for(let ref of this.selectedPackage.addOns){
        //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
        // }
        // console.log("after",this.selectedPackage.addOns);
        // this.selectedPackage.totalPrice = 0
        for (var i = 0; i < this.allAddOns.length; i++) {
            _loop_1(i);
        }
        var price = 0;
        this.selectedPackage.adons.forEach(function (data) {
            price += data.price;
        });
        console.log(price);
        // this.adonsPrice = 0;
        if (price > 0) {
            this.selectedPackage.totalPrice = this.dummy + price;
        }
        this.tempforDiscountChange = this.selectedPackage.totalPrice;
        if (this.discountValue) {
            this.discountBasePrice();
        }
        console.log(this.selectedPackage.totalPrice);
    };
    /* Description: addon remove
    author : vipin reddy */
    CreateCompanyComponent.prototype.addonRemove = function (addon) {
        var _this = this;
        // if (addon == '') {
        //   this.selectedPackage.addOns = []
        // }
        console.log("addon", addon);
        // for (let i = 0; i < this.allAddOns.length; i++) {
        //   this.selectedPackage.addOns.forEach(addon => {
        //     console.log(addon, this.allAddOns[i])
        //     if (addon == this.allAddOns[i].name) {
        //    }
        //   });
        // }
        this.selectedPackage.adons.forEach(function (data) {
            console.log(data);
            if (data.selectedAddOn == addon) {
                console.log(data.price);
                _this.selectedPackage.totalPrice = _this.selectedPackage.totalPrice - data.price;
            }
        });
        console.log(this.selectedPackage.totalPrice);
        this.selectedPackage.adons = this.selectedPackage.adons.filter(function (data) { return addon != data.selectedAddOn; });
        if (this.discountValue) {
            this.discountBasePrice();
        }
        // this.addOnChange()
    };
    /* Description: save selected packaege details
   author : vipin reddy */
    CreateCompanyComponent.prototype.savePackageDetails = function (event) {
        var _this = this;
        console.log("this.selectedPackage", this.selectedPackage);
        var comapnyData = this.accessLocalStorageService.get('editCompany');
        this.selectedPackage.clientId = comapnyData._id;
        // this.invoiceadons = this.selectedPackage.adons;
        // var postData = this.selectedPackage
        // delete postData.adons
        // postData['adons'] = this.invoiceadons;
        // console.log(postData);
        this.purchasePackageService.purchasePackage(this.selectedPackage)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status == true) {
                _this.orderId = res.data._id;
                // localStorage.setItem('ordreId',this.orderId);
                event.target.classList.remove('active'); // To Remove
                // this.activeTab = "";
                _this.activeTab = "finished";
                console.log(_this.activeTab);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success');
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error');
        });
    };
    /* Description: prinit numbers only
 author : vipin reddy */
    CreateCompanyComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    CreateCompanyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-company',
            template: __webpack_require__(/*! ./create-company.component.html */ "./src/app/super-admin-dashboard/create-company/create-company.component.html"),
            styles: [__webpack_require__(/*! ./create-company.component.css */ "./src/app/super-admin-dashboard/create-company/create-company.component.css")]
        }),
        __metadata("design:paramtypes", [_services_company_service__WEBPACK_IMPORTED_MODULE_1__["CompanyService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_4__["PackageAndAddOnService"],
            _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_5__["PurchasePackageService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__["AccessLocalStorageService"],
            _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_7__["CreditCardChargingService"],
            _services_message_service_service__WEBPACK_IMPORTED_MODULE_8__["MessageService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__["CompanySettingsService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], CreateCompanyComponent);
    return CreateCompanyComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.css":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.css ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-create-client-wrapper .modal-header {\n    padding: 0 0 24px 0;\n    border-bottom: 1px solid #e5e5e5;\n}\n\n.zenwork-custom-proceed-btn{\n    background: green !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 25px; margin:0 0 0 30px;\n}\n\n.create-client-form-wrapper { padding: 30px 0 45px;}\n\n.create-client-form-wrapper label { font-weight: normal; color:#524f4f; padding: 0 0 10px; font-size: 16px;}\n\n.create-client-form-wrapper .form-control { padding:10px 15px; height: auto; border-radius: 3px; color:#524f4f;}\n\n.create-client-form-wrapper .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #5e5e5e;\n  }"

/***/ }),

/***/ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-create-client-wrapper\">\n  <div class=\"modal-header\">\n    <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button> -->\n    <h4 class=\"modal-title green\">New Zenworker</h4>\n  </div>\n  <form #loginForm=\"ngForm\">\n    <div class=\"modal-body\">\n      <div class=\"create-client-form-wrapper\">\n\n\n        <div class=\"col-xs-5\">\n\n          <div class=\"form-group\">\n            <label>\n              Name<sup class=\"mandatory-red\">*</sup>\n            </label>\n            <input type=\"text\" class=\"form-control zenwork-input\" maxlength=\"32\" placeholder=\"Enter Name\"\n              [(ngModel)]=\"createZenworker.name\" name=\"name\" required #ZenworkName=\"ngModel\">\n            <span *ngIf=\"!createZenworker.name && ZenworkName.touched || (!createZenworker.name && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span>\n          </div>\n\n          <div *ngIf=\"!data1\"  class=\"form-group\">\n            <label>\n              Email<sup class=\"mandatory-red\">*</sup>\n            </label>\n            <input type=\"email\" class=\"form-control zenwork-input\" placeholder=\"Enter Email\"\n              [(ngModel)]=\"createZenworker.email\" name=\"email\"\n              pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\" required #ZenworkEmail=\"ngModel\">\n\n\n\n            <span *ngIf=\"!createZenworker.email && ZenworkEmail.touched || (!createZenworker.email && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter email</span>\n\n          </div>\n          <div  *ngIf=\"data1\" class=\"form-group\">\n            <label>\n              Email<sup class=\"mandatory-red\">*</sup>\n            </label>\n\n            <input type=\"email\" class=\"form-control zenwork-input\" placeholder=\"Enter Email\"\n              [(ngModel)]=\"createZenworker.email\" name=\"email\"\n              pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\" required #ZenworkEmail=\"ngModel\" readonly>\n\n\n\n            <span *ngIf=\"!createZenworker.email && ZenworkEmail.touched || (!createZenworker.email && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter email</span>\n\n          </div>\n\n\n\n\n          <div class=\"form-group\">\n            <label>\n              Profile<sup class=\"mandatory-red\">*</sup>\n            </label>\n            <mat-select class=\"form-control zenwork-input\" placeholder=\"Profile\" [(ngModel)]=\"createZenworker.roleId\"\n              (naModelChange)=\"profileChange()\" name=roleId required #ZenworkProfile=\"ngModel\">\n              <mat-option value={{profile._id}} *ngFor=\"let profile of userProfile\">\n                {{profile.name}}\n              </mat-option>\n            </mat-select>\n\n            <span *ngIf=\"!createZenworker.roleId && ZenworkProfile.touched || (!createZenworker.roleId && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter profile</span>\n\n          </div>\n\n          <div class=\"form-group\">\n            <label>\n              Location<sup class=\"mandatory-red\">*</sup>\n            </label>\n            <mat-select class=\"form-control zenwork-input\" placeholder=\"Location\"\n              [(ngModel)]=\"createZenworker.address.state\" name=address required #ZenworkLocation=\"ngModel\">\n              <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                {{data.name}}\n              </mat-option>\n              <!-- <mat-option value=\"India\">\n              India\n            </mat-option> -->\n            </mat-select>\n\n            <span\n              *ngIf=\"!createZenworker.address.state && ZenworkLocation.touched || (!createZenworker.address.state && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter location</span>\n\n          </div>\n\n          <div class=\"form-group\">\n            <label>\n              Status<sup class=\"mandatory-red\">*</sup>\n            </label>\n            <mat-select class=\"form-control zenwork-input\" placeholder=\"Status\" [(ngModel)]=\"createZenworker.status\"\n              name=status required #ZenworkStatus=\"ngModel\">\n              <mat-option value=\"active\">\n                Active\n              </mat-option>\n              <mat-option value=\"inactive\">\n                InActive\n              </mat-option>\n            </mat-select>\n\n            <span *ngIf=\"!createZenworker.status && ZenworkStatus.touched || (!createZenworker.status && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter status</span>\n\n          </div>\n\n        </div>\n\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n      <div>\n        <button type=\"submit\" class=\"btn btn-success zenwork-custom-proceed-btn\"\n          (click)=\"addZenworker()\">Save</button>\n        <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\"\n          (click)=\"close()\">Cancel</button>\n      </div>\n\n    </div>\n  </form>\n\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: CreateZenworkerTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateZenworkerTemplateComponent", function() { return CreateZenworkerTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared-module/confirmation/confirmation.component */ "./src/app/shared-module/confirmation/confirmation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var CreateZenworkerTemplateComponent = /** @class */ (function () {
    function CreateZenworkerTemplateComponent(dialogRef, data, zenworkersService, swalAlertService, loaderService, companySettingsService, dialog) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.zenworkersService = zenworkersService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.companySettingsService = companySettingsService;
        this.dialog = dialog;
        this.createZenworker = {
            name: '',
            email: '',
            roleId: '',
            address: { state: '' },
            status: ''
        };
        this.isValid = false;
        this.allStates = [];
        this.data1 = false;
    }
    CreateZenworkerTemplateComponent.prototype.ngOnInit = function () {
        if (this.data) {
            this.data1 = true;
            this.createZenworker = this.data;
        }
        this.getAllRoles();
        this.getAllUSstates();
    };
    /* Description: get all us-states from structure
 author : vipin reddy */
    CreateZenworkerTemplateComponent.prototype.getAllUSstates = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('State', 0)
            .subscribe(function (res) {
            console.log(res);
            if (res.custom.length > 0) {
                res.custom.forEach(function (element) {
                    _this.allStates.push(element);
                    console.log("22222111111", _this.allStates);
                });
            }
            if (res.default.length > 0) {
                res.default.forEach(function (element) {
                    _this.allStates.push(element);
                });
                console.log("22222", _this.allStates);
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: add zenworker submit
author : vipin reddy */
    CreateZenworkerTemplateComponent.prototype.addZenworker = function () {
        var _this = this;
        console.log("FFFFFFF");
        this.isValid = true;
        // this.loaderService.loader(true);
        /* if (!this.createZenworker.name) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Name", 'error')
        } else if (!this.createZenworker.email) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Valid Email", 'error')
        } else if (!this.createZenworker.type) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Select Valid Zenworker Role", 'error')
        } else if (!this.createZenworker.address.state) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Add Address", 'error')
        } else if (!this.createZenworker.status) {
          this.swalAlertService.Swe
          etAlertWithoutConfirmation("Zenworker", "Please Select Status", 'error')
        } else { */
        if (this.createZenworker.name && this.createZenworker.email && this.createZenworker.address.state &&
            this.createZenworker.roleId && this.createZenworker.status) {
            if (this.createZenworker.hasOwnProperty('_id')) {
                this.zenworkersService.editZenworker(this.createZenworker)
                    .subscribe(function (res) {
                    console.log("res", res);
                    _this.dialogRef.close();
                }, function (err) {
                    console.log("err", err);
                });
            }
            else {
                console.log("22222222222senddata", this.createZenworker);
                this.zenworkersService.addZenworker(this.createZenworker)
                    .subscribe(function (res) {
                    console.log("res", res);
                    _this.dialogRef.close();
                    _this.loaderService.loader(false);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                }, function (err) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
                });
            }
        }
        /* } */
    };
    /* Description: get all levels (level1 & level2 & billing)
   author : vipin reddy */
    CreateZenworkerTemplateComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.zenworkersService.getAllroles()
            .subscribe(function (res) {
            console.log("111111111", res);
            if (res.status == true) {
                // this.loaderService.loader(false)
                _this.userProfile = res.data;
                _this.userProfile.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    CreateZenworkerTemplateComponent.prototype.profileChange = function () {
    };
    /* Description: modal close
   author : vipin reddy */
    CreateZenworkerTemplateComponent.prototype.close = function () {
        this.text = "The data entered will be lost,";
        this.note = "Would you like to proceed";
        if (this.createZenworker.name == '' && this.createZenworker.email == '' && this.createZenworker.address.state == '' &&
            this.createZenworker.roleId == '' && this.createZenworker.status == '') {
            this.dialogRef.close();
        }
        else {
            this.openDialog();
        }
        // this.dialogRef.close();
    };
    /* Description: open popup for adding zenworker
   author : vipin reddy */
    CreateZenworkerTemplateComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_shared_module_confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_6__["ConfirmationComponent"], {
            width: '550px',
            data: { txt: this.text, note: this.note },
            panelClass: 'confirm-class'
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            if (result == true) {
                _this.dialogRef.close();
            }
            // if (data == 'delete') {
            //   this.deleteSingleCategory(id)
            // }
        });
    };
    CreateZenworkerTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-client-component',
            template: __webpack_require__(/*! ./create-zenworker-template-component.component.html */ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.html"),
            styles: [__webpack_require__(/*! ./create-zenworker-template-component.component.css */ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_2__["ZenworkersService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_5__["CompanySettingsService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], CreateZenworkerTemplateComponent);
    return CreateZenworkerTemplateComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/edit-company/edit-company.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/edit-company/edit-company.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n.zenworkers-wrapper{\n    padding: 20px 0 0 25px;\n}\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 12px;\n    width: 13px;\n    color: #000;\n    font-size: 10px;\n}\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n.zenwork-customized-green-btn{\n    background: #439348!important;\n    color: #fff!important;\n    padding: 7px 12px!important;\n    font-size: 12px!important;\n    border-radius: 0px!important;\n}\n.zenwork-custom-select{\n    background: #fff!important;\n}\n.border-none{\n    border:0 none !important;\n}\n.address-wrapper { padding:25px 0 70px;}\n.address-wrapper ul { display: inline-block; width: 100%;}\n.address-wrapper ul li { margin: 0 0px 20px 0; padding: 0;}\n.address-wrapper ul li.details-width{ width: 100%;}\n.address-wrapper ul li .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none;height: auto;padding:13px 10px 13px 10px;}\n.address-wrapper ul li select { background:#fff; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n.address-wrapper ul li.united-state { margin: 0; clear: left;}\n.address-wrapper ul li.state, .address-wrapper ul li.city { padding: 0 10px 0 0;}\n.contact-wrapper { display: block; padding: 0 0 20px;}\n.contact-wrapper ul { margin: 0 0 30px;}\n.contact-wrapper ul li {margin: 0 20px 15px 0; padding: 0;}\n.contact-wrapper ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n.contact-wrapper ul li .phone { display: block; background:#fff; padding:0 10px;clear: left; height: auto;}\n.contact-wrapper ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n.contact-wrapper ul li .phone span .fa { color: #636060;\n    font-size: 23px;\n    line-height: 20px;\n    width: 18px;\n    text-align: center;}\n.contact-wrapper ul li .phone .form-control { display: inline-block;vertical-align: middle; width:90%; border: none; background:transparent; box-shadow: none; padding:13px 0 13px 10px; height: auto;}\n.contact-wrapper ul li .form-control { display: inline-block;vertical-align: middle; width:90%; border: none; background:#fff; box-shadow: none; \n    padding:13px 10px; height: auto;}\n.contact-wrapper ul li.phone-width { float: none; clear:left;}\n.zenwork-custom-input{\n    background: #fff !important;padding: 13px 15px; height: auto;}\n.company-details-wrapper{\n    padding:10px 0 10px;\n    border-bottom:1px solid #c3c3c3;\n}\n.contact-wrapper {padding: 40px 0px 10px;\n    border-bottom:1px solid #c3c3c3;}\n.zenwork-custom-proceed-btn{\n    background:#008f3d !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 30px;\n}\n.zenwork-customized-transparent-btn { margin: 0 0 0 20px;}\n.zenwork-margin-zero small { display:inline-block; vertical-align:text-bottom; font-size: 17px; margin: 0 10px 0 0;}\n.zenwork-margin-zero{ font-size: 18px; color:#585858;}\n.contact-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n.contact-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n.address-wrapper h2 { color:#636060; font-size: 16px; line-height: 16px; display: inline-block; margin: 0 0 20px;}\n.address-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n.address-wrapper .form-group { margin: 15px 0 0;}\n.address-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n.company-details label { color:#5f5c5c; font-size: 15px; padding: 0 0 10px; font-weight: normal;}\n.reseller-block { margin:60px 0 0;}\n\n"

/***/ }),

/***/ "./src/app/super-admin-dashboard/edit-company/edit-company.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/edit-company/edit-company.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <small>\n        <img src=\"../../../assets/images/employee-management/Green/employee_4.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n      </small>\n      Edit Client Details (Existing Client)\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"zenwork-clients-wrapper\">\n    \n    <mat-tab-group>\n\n      <mat-tab label=\"Client Details\">\n          \n        <div class=\"zenworkers-wrapper\">\n          <!-- <div class=\"zenwork-currentpage\">\n            <p class=\"zenwork-margin-zero\">\n              <small>\n                <img src=\"../../../assets/images/employee-management/Green/employee_4.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n              </small>\n              Add Client - Reseller\n            </p>\n            <hr class=\"zenwork-margin-ten-zero\">\n          </div> -->\n\n          \n          <div class=\"create-client-wrapper\">\n\n            <!-- <mat-horizontal-stepper  #stepper=\"matHorizontalStepper\">\n              <mat-step [stepControl]=\"firstFormGroup\">\n                <form [formGroup]=\"firstFormGroup\">\n                  <ng-template matStepLabel><div class=\"zenwork-dot\"></div></ng-template>\n                  <mat-form-field>\n                    <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\n                  </mat-form-field>\n                  <div>\n                    <button mat-button matStepperNext>Next</button>\n                  </div>\n                </form>\n              </mat-step>\n              <mat-step [stepControl]=\"secondFormGroup\">\n                <form [formGroup]=\"secondFormGroup\">\n                  <ng-template matStepLabel><div class=\"zenwork-dot\"></div></ng-template>\n                  <mat-form-field>\n                    <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\n                  </mat-form-field>\n                  <div>\n                    <button mat-button matStepperPrevious>Back</button>\n                    <button mat-button matStepperNext>Next</button>\n                  </div>\n                </form>\n              </mat-step>\n              <mat-step>\n                <ng-template matStepLabel>Done</ng-template>\n                You are now done.\n                <div>\n                  <button mat-button matStepperPrevious>Back</button>\n                  <button mat-button (click)=\"stepper.reset()\">Reset</button>\n                </div>\n              </mat-step>\n            </mat-horizontal-stepper> -->\n          \n\n            <div class=\"company-details-wrapper\">\n              <div class=\"company-details\">\n                <div class=\"col-xs-6\">\n                  <!-- <div class=\"green\">\n                    Company Details\n                  </div> -->\n                  <div class=\"form-group\">\n                    <label>Company Name*</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Company Name\" [(ngModel)]=\"companyDetails.name\">\n                   \n                  </div>\n\n                  <div class=\"form-group\">\n                    <label>Trade Name*</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Trade Name\" [(ngModel)]=\"companyDetails.tradeName\">\n                  </div>\n\n                  <div class=\"form-group\">\n                    <label>No. of employees</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"No. of Employees\" [(ngModel)]=\"companyDetails.employeCount\">\n                  </div>\n\n                  <div class=\"reseller-block\">\n                    <div class=\"form-group\">\n                      <div class=\"col-xs-6 padding-left-zero\">\n                        <label>Is the Client Reseller?</label>\n                        <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Client/Reseller\" [(ngModel)]=\"companyDetails.type\">\n                          <mat-option value=\"reseller\">\n                            Yes\n                          </mat-option>\n                          <mat-option value=\"client\">\n                            No\n                          </mat-option>\n                        </mat-select>\n                      </div>\n                      <div class=\"col-xs-6 padding-right-zero\" *ngIf=\"companyDetails.type != 'reseller'\">\n                        <div class=\"form-group\">\n                          <label>Client belongs to Reseller?</label>\n                          <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Client/Reseller\" [(ngModel)]=\"companyDetails.belongsToReseller\">\n                            <mat-option value=\"true\">\n                              Yes\n                            </mat-option>\n                            <mat-option value=\"false\">\n                              No\n                            </mat-option>\n                          </mat-select>\n                        </div>\n                        <div class=\"form-group\" *ngIf=\"companyDetails.belongsToReseller\">\n                          <label>Select Reseller</label>\n                          <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Client/Reseller\" [(ngModel)]=\"companyDetails.reseller\">\n                            <mat-option value=\"Reseller1\">\n                              Reseller 1\n                            </mat-option>\n                            <mat-option value=\"Reseller2\">\n                              Reseller 2\n                            </mat-option>\n                          </mat-select>\n                        </div>\n                      </div>\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"clearfix\"></div>\n              </div>\n            </div>\n\n\n            <div class=\"contact-wrapper\">\n              <div class=\"col-xs-6\">\n                <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('primaryContact')\">\n                  <li>\n                    <h4>Primary Company Contact</h4>\n                  </li>\n                  <li class=\"col-md-10 phone-width\">\n                    <div class=\"phone\">\n                      <!-- <span>\n                        <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                      </span> -->\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\" [(ngModel)]=\"companyDetails.primaryContact.name\">\n                    </div>\n                  </li>\n                  <li>\n                    <h4>Email</h4>\n                  </li>\n                  <li class=\"col-md-8\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyDetails.email\" required>\n                    </div>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                  <li>\n                    <h4>Phone</h4>\n                  </li>\n                  <li class=\"col-md-8\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input type=\"number\" class=\"form-control\" placeholder=\"Work Phone\" [(ngModel)]=\"companyDetails.primaryContact.phone\">\n                    </div>\n                  </li>\n                  <li class=\"col-md-2\">\n\n                    <input type=\"text\" class=\"form-control\" placeholder=\"Ext\" [(ngModel)]=\"companyDetails.primaryContact.extention \">\n\n                  </li>\n\n                  <div class=\"clearfix\"></div>\n\n                </ul>\n                <div class=\"form-group\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"companyDetails.isPrimaryContactIsBillingContact\"></mat-checkbox>\n                  <small> Is Primary Contact same as billing Contact ?</small>\n                </div>\n              </div>\n              <div class=\"col-xs-6\">\n                <ul *ngIf=\"!companyDetails.isPrimaryContactIsBillingContact && companyDetails.hasOwnProperty('billingContact')\">\n                  <li>\n                    <h4>Billing Company Contact</h4>\n                  </li>\n                  <li class=\"col-md-10 phone-width\">\n                    <div class=\"phone\">\n                      <!-- <span>\n                        <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                      </span> -->\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\" [(ngModel)]=\"companyDetails.billingContact.name\">\n                    </div>\n                  </li>\n                  <li>\n                    <h4>Email</h4>\n                  </li>\n                  <li class=\"col-md-8\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyDetails.billingContact.email\">\n                    </div>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                  <li>\n                    <h4>Phone</h4>\n                  </li>\n                  <li class=\"col-md-8\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input type=\"number\" class=\"form-control\" placeholder=\"Work Phone\" [(ngModel)]=\"companyDetails.billingContact.phone\">\n                    </div>\n                  </li>\n                  <li class=\"col-md-2\">\n\n                    <input type=\"number\" class=\"form-control\" placeholder=\"Ext\" [(ngModel)]=\"companyDetails.billingContact.extention \">\n\n                  </li>\n\n                  <div class=\"clearfix\"></div>\n\n                </ul>\n              </div>\n              <div class=\"clearfix\"></div>\n            </div>\n\n\n            <div class=\"address-wrapper\">\n              <div class=\"clearfix\"></div>\n              <div class=\"col-xs-6\">\n                <h2>\n                  Company Address*\n                </h2>\n                <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('address')\">\n                  <li class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" [(ngModel)]=\"companyDetails.address.address1\">\n                  </li>\n                  <li class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" [(ngModel)]=\"companyDetails.address.address2\">\n                  </li>\n                </ul>\n\n                <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('address')\">\n                  <li class=\"state col-md-4\">\n                    <input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]=\"companyDetails.address.city\">\n                  </li>\n                  <li class=\"city col-md-4\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"State\" [(ngModel)]=\"companyDetails.address.state\">\n                      <mat-option value=\"newyork\">\n                        New York\n                      </mat-option>\n                      <mat-option value=\"washington\">\n                        Washington\n                      </mat-option>\n                      <mat-option value=\"florida\">\n                        Florida\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"zip col-md-4\">\n                    <input type=\"number\" placeholder=\"ZIP\" class=\"form-control\" [(ngModel)]=\"companyDetails.address.zipcode\">\n                  </li>\n\n                  <li class=\"col-md-4 united-state\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Country\" [(ngModel)]=\"companyDetails.address.country\">\n                      <mat-option value=\"United States\">\n                        United States\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                </ul>\n                <div class=\"clearfix\"></div>\n                <div class=\"form-group\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"companyDetails.isPrimaryAddressIsBillingAddress\"></mat-checkbox>\n                  <small> Primary Address same as billing address ?</small>\n                </div>\n              </div>\n              <div class=\"col-xs-6\" *ngIf=\"!companyDetails.isPrimaryAddressIsBillingAddress\">\n                <h2>\n                  Billing Company Address*\n                </h2>\n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" [(ngModel)]=\"companyDetails.billingAddress.address1\">\n                  </li>\n                  <li class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" [(ngModel)]=\"companyDetails.billingAddress.address2\">\n                  </li>\n                </ul>\n\n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li class=\"state col-md-4\">\n                    <input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]=\"companyDetails.billingAddress.city\">\n                  </li>\n                  <li class=\"city col-md-4\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"State\" [(ngModel)]=\"companyDetails.billingAddress.state\">\n                      <mat-option value=\"newyork\">\n                        New York\n                      </mat-option>\n                      <mat-option value=\"washington\">\n                        Washington\n                      </mat-option>\n                      <mat-option value=\"florida\">\n                        Florida\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <li class=\"zip col-md-4\">\n                    <input type=\"number\" placeholder=\"ZIP\" class=\"form-control\" [(ngModel)]=\"companyDetails.billingAddress.zipcode\">\n                  </li>\n\n                  <li class=\"col-md-4 united-state\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Country\" [(ngModel)]=\"companyDetails.billingAddress.country\">\n                      <mat-option value=\"United States\">\n                        United States\n                      </mat-option>\n                    </mat-select>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                </ul>\n                <div class=\"clearfix\"></div>\n              </div>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n        \n\n        </div>\n        <div class=\"clearfix\"></div>\n\n        <div class=\"col-xs-12\">\n          <div class=\"col-xs-12\">\n            <button type=\"submit\" class=\"btn btn-success zenwork-custom-proceed-btn\" (click)=\"updateCompany()\">Save & continue</button>\n            <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\">Cancel</button>\n            <!-- (click)=\"cancel()\" -->\n          </div>\n        </div>\n        <div class=\"clearfix\"></div>\n        \n      </mat-tab>\n\n      <mat-tab label=\"Billing History\">Content 2</mat-tab>\n      <mat-tab label=\"Upgrade/Downgrade Package\">Content 1</mat-tab>\n      <mat-tab label=\"Payment Mode\">Content 2</mat-tab>\n    </mat-tab-group>\n    <div class=\"clearfix\"></div>\n    <!-- <hr class=\"zenwork-margin-ten-zero\"> -->\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/edit-company/edit-company.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/edit-company/edit-company.component.ts ***!
  \******************************************************************************/
/*! exports provided: EditCompanyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCompanyComponent", function() { return EditCompanyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditCompanyComponent = /** @class */ (function () {
    function EditCompanyComponent(companyService, accessLocalStorageService, swalAlertService, router) {
        this.companyService = companyService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: '',
            email: '',
            type: '',
            belongsToReseller: '',
            reseller: '',
            primaryContact: { name: '', phone: '', extention: '' },
            billingContact: { name: '', email: '', phone: '', extention: '' },
            address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            isPrimaryContactIsBillingContact: false,
            isPrimaryAddressIsBillingAddress: false
        };
    }
    EditCompanyComponent.prototype.ngOnInit = function () {
        this.companyDetails = this.accessLocalStorageService.get('editCompany');
        if (!this.companyDetails.hasOwnProperty('billingAddress')) {
            this.companyDetails.billingAddress = {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            };
        }
    };
    EditCompanyComponent.prototype.updateCompany = function () {
        var _this = this;
        console.log(this.companyDetails);
        this.companyService.editCompany(this.companyDetails)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                _this.companyDetails = {
                    name: '',
                    tradeName: '',
                    employeCount: '',
                    email: '',
                    type: '',
                    belongsToReseller: '',
                    reseller: '',
                    primaryContact: { name: '', phone: '', extention: '' },
                    billingContact: { name: '', email: '', phone: '', extention: '' },
                    address: {
                        address1: '',
                        address2: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        country: ''
                    },
                    billingAddress: {
                        address1: '',
                        address2: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        country: ''
                    },
                    isPrimaryContactIsBillingContact: true,
                    isPrimaryAddressIsBillingAddress: true
                };
                _this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    EditCompanyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-company',
            template: __webpack_require__(/*! ./edit-company.component.html */ "./src/app/super-admin-dashboard/edit-company/edit-company.component.html"),
            styles: [__webpack_require__(/*! ./edit-company.component.css */ "./src/app/super-admin-dashboard/edit-company/edit-company.component.css")]
        }),
        __metadata("design:paramtypes", [_services_company_service__WEBPACK_IMPORTED_MODULE_2__["CompanyService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], EditCompanyComponent);
    return EditCompanyComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/invoice/invoice.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/invoice/invoice.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".invoice-main { margin:40px auto 0; float: none; padding: 0;}\n.zenwork-customized-back-btn{\nbackground:#fff;\nborder-radius: 25px;\npadding:8px 20px;\nfont-size: 10px;\ncolor:#525151;\nfont-size: 13px; line-height: 13px; border: 1px solid transparent;\nmargin: 0 20px 0 0; vertical-align: middle;\n}\n.zenwork-currentpage { padding: 0;}\n.zenwork-currentpage small { margin: 0 10px 0 0; display: inline-block; vertical-align: top;}\n.mr-7{margin-right: 7px;}\n.zenwork-margin-zero { color:#008f3d; font-size: 18px; line-height: 18px;display: inline-block; vertical-align:middle;}\n.zenwork-margin-ten-zero {\n    margin: 15px 0px!important;\n}\n.invoice-serach { margin: 30px 0 0; border-bottom: 1px solid #eee; padding: 0 0 20px;}\n.invoice-serach ul { display: block;}\n.invoice-serach ul li { padding: 0 15px 0 0;}\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 15px;\n    width:22px;\n    color:#9a9898;\n    font-size: 13px; cursor: pointer;\n}\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc; border-radius: 0;\n}\n.zenwork-customized-green-btn{\n    background: #008f3d!important;\n    color: #fff!important;\n    padding:8px 35px!important;\n    font-size:15px!important;\n}\n.zenwork-customized-green-btn small { display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n.zenwork-customized-green-btn small .fa { color:#fff; font-size: 15px;}\n.zenwork-custom-select{\n    background: #fff!important;\n}\n.border-none{\n    border:0 none !important;\n}\n.box-shadow-none{\n    box-shadow: none !important;\n}\n.zenwork-custom-progress-bar{\n    width: 30% !important;\n    height: 7px !important;\n    margin-bottom: 0px !important;\n    display: inline-block !important;\n}\n.search-form .form-control {padding:12px 40px 12px 20px; height: auto;}\n.input-group .form-control { height: auto; padding: 12px;}\n.invoice-serach ul li label { font-weight: normal; color:#5f5e5e; float: left; padding:12px 0 0;}\n.status-paid { float: right; width: 80%;}\n.invoice-serach ul li .date { height: auto !important; line-height:inherit !important; width:100% !important;}\n.invoice-serach ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none;}\n.invoice-serach ul li .date span { border: none !important; padding: 0 !important;}\n.invoice-serach ul li .date .form-control { width:61% !important; height: auto; padding: 12px 0 12px 10px;}\n.invoice-serach ul li .date .form-control:focus { box-shadow: none; border: none;}\n.invoice-details { margin:35px 0 0;}\n.invoice-details h2 { color:#404040; font-weight:600; font-size: 18px; line-height:18px; display: inline-block; vertical-align: middle; margin: 0 0 30px;}\n.invoice-details > h2 > span { display: inline-block; vertical-align:text-top; position: relative; margin: 0 20px 0 0;}\n.invoice-details h2 span small { position: absolute; bottom: -6px; right:-7px;}\n.invoice-billing { background: #eef7ff; padding: 10px 20px;}\n.invoice-billing > ul { margin: 0;}\n.invoice-billing > ul > li { display: inline-block; vertical-align: middle; width: 12%;}\n.invoice-billing > ul > li:nth-child(1) { width: 25%;}\n.invoice-billing > ul > li h3 {color:#565656; font-weight: normal; font-size: 15px; line-height:18px; display: inline-block; vertical-align: middle; margin: 0;}\n.invoice-billing > ul > li:nth-last-child(1) { width: 3%;}\n.invoice-billing > ul > li .cont-check .checkbox label { font-size: 16px; padding-left:30px; color: #565656;}\n.invoice-billing > ul > li.average { width: 11%;}\n.invoice-clients { margin: 10px 0;}\n.invoice-left { width: 25%; float: left; padding:25px 7px 20px 15px; background:#fff;position: relative;}\n.invoice-left h2 { color:#4a4747; font-weight: normal; font-size: 17px; line-height:17px; display: inline-block; vertical-align: middle; margin: 0; width : 300px;}\n.invoice-left h2 span { margin: 0 5px 0 0;}\n.invoice-left p { color:#008f3d; border-top:1px dashed #cec7c7; margin: 15px 0 0; padding: 15px 0 0; font-size: 14px; font-weight: 600;}\n.invoice-left .cont-check { display: inline-block; vertical-align: top; width:30px; }\n.invoice-left:after{\n    content: '';\n    position: absolute;\n    top: 0;\n    left: -4px;\n    border-left: #83c7a1 5px solid;\n    height: 100%;\n    border-radius: 5px 0 0 5px;\n}\n.invoice-right {width: 74.5%;float: left; background:#fff; padding: 15px 10px;}\n.invoice-right > ul { margin: 0; padding: 0 0 14px;}\n.invoice-right > ul > li { display: inline-block; vertical-align: middle; width: 15.6%;}\n.invoice-right > ul > li h5 {color:#4a4747; font-weight: normal; font-size: 15px; line-height:18px; display: inline-block; vertical-align: middle; margin: 0;}\n.invoice-right > ul > li h5 em { color: #008f3d; font-style: normal;}\n.invoice-right > ul > li h5 small { display: inline-block; vertical-align: middle; margin: 0 10px 0 0;}\n.invoice-right span, .invoice-right small { color:#4a4747; font-size: 14px; line-height: 14px;}\n.cont-check .success-checkbox label ::before{\n    border-radius: 20px !important;\n}\n.invoice-left h2 span {background:  #1aa4db;width: 26px;display: inline-block;font-size: 15px;margin: 0px 10px 0 15px;text-align: center; \nheight: 26px; line-height: 26px; border-radius: 4px; cursor: pointer; vertical-align: bottom;}\n.invoice-left h2 small {background: #f3616c;width: 26px;display: inline-block;font-size: 15px;margin: 0px 10px 0 15px;text-align: center; \nheight: 26px; line-height: 26px; border-radius: 4px;cursor: pointer;vertical-align: bottom;}\n.invoice-left h2 em {background: #fbd437;width: 26px;display: inline-block;font-size: 15px;margin: 0px 10px 0 15px;text-align: center;\nheight: 26px; line-height: 26px; border-radius: 4px;cursor: pointer; vertical-align: bottom;}\n.invoice-left h2 var{ width: 236px; display: inline-block; font-style: normal;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/invoice/invoice.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/invoice/invoice.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"invoice-main col-md-11\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <small>\n        <img src=\"../../../assets/images/Side Menu/Green/green-billings.png\" alt=\"Company-settings icon\">\n      </small>\n      Invoice\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"invoice-serach\">\n\n    <ul>\n      <li class=\"col-md-3\">\n        <div class=\"padding-left-zero\">\n          <div class=\"search-border text-left\">\n            <form [formGroup]=\"invoiceSearch\" class=\"search-form\">\n              <input type=\"text\" class=\"border-none box-shadow-none form-control\" formControlName=\"search\"\n                placeholder=\"Search by name,title, etc\">\n            </form>\n            <span>\n              <span class=\"glyphicon glyphicon-search search-icon\" aria-hidden=\"true\"></span>\n            </span>\n          </div>\n        </div>\n      </li>\n\n      <li class=\"col-md-2\">\n        <div class=\"input-group\">\n          <span class=\"input-group-addon zenwork-client-group-addon zenwork-input-group-addon\">\n            <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" alt=\"First Name Icon\"\n              class=\"zenwork-demo-input-image-group zenwork-company-group\">\n          </span>\n          <mat-select class=\"form-control zenwork-input zenwork-custom-select\" placeholder=\"All Clients\"\n            [(ngModel)]=\"invoiceSelect\" [(ngModel)]=\"clientType\" (ngModelChange)=\"invoiceClientsData($event)\">\n            <mat-option value=\"all\">\n              All Clients\n            </mat-option>\n            <mat-option value=\"company\">\n              Direct Client\n            </mat-option>\n            <mat-option value=\"reseller\">\n              Resellers\n            </mat-option>\n            <mat-option value=\"reseller-client\">\n              Reseller Client\n            </mat-option>\n          </mat-select>\n        </div>\n      </li>\n\n\n      <li class=\"col-md-3\">\n        <label>Status</label>\n\n        <div class=\"status-paid\">\n          <div class=\"input-group\">\n            <span class=\"input-group-addon zenwork-client-group-addon zenwork-input-group-addon\">\n              <img src=\"../../../assets/images/Side Menu/Green/green-billings.png\" alt=\"First Name Icon\">\n            </span>\n            <mat-select class=\"form-control zenwork-input zenwork-custom-select\" [(ngModel)]=\"status\"\n              placeholder=\"Paid / Unpaid\" [(ngModel)]=\"invoicePadiUnpaid\" (ngModelChange)=\"invoicePaymentData()\">\n              <mat-option value=\"\">\n                All\n              </mat-option>\n              <mat-option value=\"initialized\">\n                Initialized\n              </mat-option>\n              <mat-option value=\"pending\">\n                Pending\n              </mat-option>\n\n              <mat-option value=\"failed\">\n                Failed\n              </mat-option>\n              <mat-option value=\"success\">\n                Success\n              </mat-option>\n            </mat-select>\n          </div>\n        </div>\n      </li>\n\n      <li class=\"col-md-2\">\n        <div class=\"date\">\n          <input matInput [(ngModel)]=\"fromDate\" (ngModelChange)=\"formmdatefun()\" [max]='toDate'\n            [matDatepicker]=\"picker\" placeholder=\"dd / mm / yy\" class=\"form-control\">\n          <mat-datepicker #picker></mat-datepicker>\n          <button mat-raised-button (click)=\"picker.open()\">\n            <span>\n              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n            </span>\n          </button>\n          <div class=\"clearfix\"></div>\n        </div>\n      </li>\n\n      <li class=\"col-md-2\">\n        <div class=\"date\">\n          <input matInput [(ngModel)]=\"toDate\" [min]='fromDate' (ngModelChange)=\"todatefun()\" [matDatepicker]=\"picker1\"\n            placeholder=\"dd / mm / yy\" class=\"form-control\">\n          <mat-datepicker #picker1></mat-datepicker>\n          <button mat-raised-button (click)=\"picker1.open()\">\n            <span>\n              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n            </span>\n          </button>\n          <div class=\"clearfix\"></div>\n        </div>\n      </li>\n\n    </ul>\n\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n\n  <div class=\"invoice-details\">\n\n    <h2>\n      <span><img src=\"../../../assets/images/Side Menu/Green/green-billings.png\" alt=\"First Name Icon\">\n        <small><img src=\"../../../assets/images/Side Menu/Grey/unpaid.png\" alt=\"First Name Icon\"></small>\n      </span>\n      All Clients-Paid/Unpaid Billing Details\n    </h2>\n\n    <div class=\"invoice-billing\">\n      <ul>\n        <li>\n          <!-- <div class=\"cont-check\">\n            <div class=\"checkbox\"> -->\n          <!-- <input id=\"checkbox1\" type=\"checkbox\"> -->\n          <label for=\"checkbox1\">All Clients Billing</label>\n          <!-- </div>\n          </div> -->\n        </li>\n        <li>\n          <h3>Billing<br>Frequency</h3>\n        </li>\n        <li>\n          <h3>Month</h3>\n        </li>\n        <li>\n          <h3>Payment<br>Type</h3>\n        </li>\n        <li>\n          <h3>Average Employee Count</h3>\n        </li>\n        <!-- <li class=\"average\"><h3>Average<br>Employee Count</h3></li> -->\n        <li>\n          <h3>Total<br>Amount</h3>\n        </li>\n        <li>\n          <h3>Status</h3>\n        </li>\n        <li>\n          <div class=\"setting-drop\">\n            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </a>\n            <ul class=\"dropdown-menu\">\n              <li><a (click)=\"editClientBillingHistory('package')\">Billing History</a></li>\n              <li>\n                <a>Download</a>\n              </li>\n\n            </ul>\n          </div>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n    </div>\n\n    <div class=\"invoice-clients\" *ngFor=\"let invoice of invoices\">\n\n      <div class=\"invoice-left\" >\n        <mat-checkbox [(ngModel)]=\"invoice.isChecked\"\n          (ngModelChange)=\"singleTaskChoose(invoice.company,invoice.isChecked)\"></mat-checkbox>\n\n        <h2 *ngIf=\"invoice.company.type =='company'\"> <span>DC</span><var>{{invoice.company.name}} </var></h2>\n        <h2 *ngIf=\"invoice.company.type =='reseller'\"> <small>R</small><var> {{invoice.company.name}} </var></h2>\n        <h2 *ngIf=\"invoice.company.type =='reseller-client'\"> <em>RC</em><var> {{invoice.company.name}} </var></h2>\n\n\n      </div>\n\n      <div class=\"invoice-right pull-right\">\n\n        <ul>\n          <li>\n            <h5>{{invoice.billingFrequency}}</h5>\n          </li>\n          <li>\n            <h5>{{invoice.createdDate | date}}</h5>\n          </li>\n          <li>\n            <h5>{{invoice.paymentType}} </h5>\n          </li>\n          <li>\n            <h5>{{invoice.company.employeCount}}</h5>\n          </li>\n          <li>\n            <h5><em>${{invoice.amount/100 || 'NA'}}</em></h5>\n          </li>\n          <li *ngIf=\"invoice.paymentStatus == 'initialized'\">\n            <h5><small><img src=\"../../../assets/images/Side Menu/Grey/unpaid.png\"\n                  alt=\"First Name Icon\"></small>{{invoice.paymentStatus}}</h5>\n          </li>\n          <li *ngIf=\"invoice.paymentStatus == 'success'\">\n            <h5><small>\n                <div class=\"cont-check1\">\n\n                </div>\n              </small>{{invoice.paymentStatus}}</h5>\n          </li>\n        </ul>\n        <div class=\"clearfix\"></div>\n\n        <span class=\"pull-left\">Invoice Date:{{invoice.updatedDate | date}}</span>\n        <small class=\"pull-right\">Invoice Number:{{invoice.id}}</small>\n        <div class=\"clearfix\"></div>\n\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n    <mat-paginator [length]=\"totalCount\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n      (page)=\"pageEvents($event)\">\n    </mat-paginator>\n    <!-- <div class=\"invoice-clients\">\n\n      <div class=\"invoice-left \">\n          <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox2\" type=\"checkbox\">\n                <label for=\"checkbox2\"></label>\n              </div>\n            </div>\n            <h2>\n                <span><img src=\"../../../assets/images/Side Menu/Grey/logo.png\" alt=\"First Name Icon\"></span>\n                Mockingbird Utilities LLC\n            </h2>  \n          <p>Client Type: Reseller</p>   \n      </div>\n      <div class=\"invoice-right pull-right\">\n        \n          <ul>\n              <li><h5>Annual</h5></li>\n              <li><h5>November</h5></li>\n              <li><h5>Credit Card</h5></li>\n              <li><h5>$0</h5></li>\n              <li><h5>35</h5></li>\n              <li><h5><em>$10</em></h5></li>\n              <li><h5><small><img src=\"../../../assets/images/Side Menu/Grey/unpaid.png\" alt=\"First Name Icon\"></small>Unpaid</h5></li>\n          </ul>\n          <div class=\"clearfix\"></div>\n\n          <span class=\"pull-left\">Invoice Date:11/24/2018-09:26AM</span>\n          <small class=\"pull-right\">Invoice Number:XXXXXXXXXX123</small>\n          <div class=\"clearfix\"></div>\n\n      </div>\n      <div class=\"clearfix\"></div>\n  </div> -->\n\n\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/invoice/invoice.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/invoice/invoice.component.ts ***!
  \********************************************************************/
/*! exports provided: InvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceComponent", function() { return InvoiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_invoice_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/invoice.service */ "./src/app/services/invoice.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var InvoiceComponent = /** @class */ (function () {
    function InvoiceComponent(invoiceBillingService, accessLocalStorageService, router, swalAlertService) {
        this.invoiceBillingService = invoiceBillingService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.router = router;
        this.swalAlertService = swalAlertService;
        this.invoices = [];
        this.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        this.deleteId = [];
        this.billingOpen = false;
    }
    InvoiceComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.clientType = '';
        this.status = '';
        this.fromDate = '';
        this.toDate = '';
        this.invoicePadiUnpaid = '';
        this.invoiceSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
        });
        this.company = '';
        this.onChange();
    };
    InvoiceComponent.prototype.onChange = function () {
        var _this = this;
        this.invoiceSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            console.log(val);
            _this.searchQuery = val;
            _this.getInvoiceDetails();
        });
        console.log(this.searchQuery);
        if (this.searchQuery == '' || this.searchQuery == undefined) {
            console.log(this.searchQuery);
            this.getInvoiceDetails();
        }
    };
    /* Description: mat pagination for invoice list
   author : vipin reddy */
    InvoiceComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getInvoiceDetails();
    };
    /* Description: filters for invoice list
    author : vipin reddy */
    InvoiceComponent.prototype.invoiceClientsData = function (event) {
        console.log(this.clientType, event);
        this.getInvoiceDetails();
    };
    /* Description: filters for invoice list
    author : vipin reddy */
    InvoiceComponent.prototype.invoicePaymentData = function () {
        console.log(this.invoicePadiUnpaid);
        this.getInvoiceDetails();
    };
    /* Description: get invoice list
    author : vipin reddy */
    InvoiceComponent.prototype.getInvoiceDetails = function () {
        var _this = this;
        console.log("Hiiii");
        var postData = {
            "per_page": this.perPage,
            "page_no": this.pageNo,
            "from_date": this.fromDate,
            "to_date": this.toDate,
            "search_query": this.invoiceSearch.controls['search'].value,
            "client_type": this.clientType,
            "paymentStatus": this.invoicePadiUnpaid
        };
        console.log(postData);
        this.invoices = [];
        this.invoiceBillingService.getInvoiceBillingDetails(postData)
            .subscribe(function (res) {
            console.log('responsee', res);
            _this.totalCount = res.total_count;
            _this.invoices = [];
            res.data.forEach(function (element) {
                if (element.company) {
                    element.isChecked = false;
                    _this.invoices.push(element);
                }
            });
            console.log(_this.invoices);
        }, function (err) {
            console.log("Errrr", err);
        });
    };
    /* Description: filters for invoice list
    author : vipin reddy */
    InvoiceComponent.prototype.formmdatefun = function () {
        console.log(this.fromDate.toISOString());
        this.fromDate.toISOString();
        this.getInvoiceDetails();
    };
    /* Description: filters for invoice list
   author : vipin reddy */
    InvoiceComponent.prototype.todatefun = function () {
        console.log(this.fromDate.toISOString());
        this.toDate.toISOString();
        this.getInvoiceDetails();
    };
    /* Description: single company billing history showing
   author : vipin reddy */
    InvoiceComponent.prototype.editClientBillingHistory = function (tab) {
        console.log("compnay", tab);
        if (this.billingOpen) {
            // this.accessLocalStorageService.set('editCompany', company);
            this.accessLocalStorageService.set('viewTab', tab);
            this.company = this.accessLocalStorageService.get('editCompany');
            console.log(this.company);
            if (this.company) {
                var id = this.company.userId;
                this.router.navigate(['/super-admin/super-admin-dashboard/edit-company/' + id]);
            }
        }
        if (this.company == null || this.company == undefined || this.company == '') {
            console.log("vipoiv");
            this.swalAlertService.SweetAlertWithoutConfirmation('Invoice', "please select a client", 'error');
        }
    };
    /* Description: selecting single invoice
   author : vipin reddy */
    InvoiceComponent.prototype.singleTaskChoose = function (taskArray, event) {
        var _this = this;
        console.log(taskArray, event);
        this.billingOpen = event;
        if (event == false) {
            localStorage.removeItem('editCompany');
        }
        if (event == true) {
            this.invoices.forEach(function (element) {
                if (element.company.userId == taskArray.userId) {
                    _this.accessLocalStorageService.set('editCompany', element.company);
                    element.isChecked = true;
                }
                else {
                    element.isChecked = false;
                }
            });
            // for (var task in this.standardOffboardTemplate["offBoardingTasks"]) {
            //     console.log(task);
            //     this.standardOffboardTemplate["offBoardingTasks"][task].forEach(element => {
            //         if (element.taskName === taskName) {
            //             element.isChecked = true;
            //             this.popupData = element;
            //             this.addShowHide = true;
            //             console.log("popdaata", this.popupData);
            //         } else {
            //             element.isChecked = false;
            //         }
            //     });
            // }
        }
    };
    InvoiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice',
            template: __webpack_require__(/*! ./invoice.component.html */ "./src/app/super-admin-dashboard/invoice/invoice.component.html"),
            styles: [__webpack_require__(/*! ./invoice.component.css */ "./src/app/super-admin-dashboard/invoice/invoice.component.css")]
        }),
        __metadata("design:paramtypes", [_services_invoice_service__WEBPACK_IMPORTED_MODULE_1__["InvoiceService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"]])
    ], InvoiceComponent);
    return InvoiceComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/super-admin-dashboard.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/super-admin-dashboard.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-sidebar-menu, .zenwork-customized-template { padding: 0;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/super-admin-dashboard.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/super-admin-dashboard.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Main Navigation Starts-->\n<div>\n  <app-navbar></app-navbar>\n</div>\n<!-- Main Navigation Ends-->\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"zenwork-full\">\n      <!-- Side Navigation Starts -->\n      <div class=\"col-sm-3 zenwork-customized-sidebar-menu sidebar zenwork-pl-zero\">\n        <div >\n          <app-side-nav [getUserData]=\"userData\"></app-side-nav>\n\n        </div>\n      \n      </div>\n      <!-- Side Navigation Ends -->\n      <div class=\"col-sm-9 zenwork-customized-template\">\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/super-admin-dashboard.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/super-admin-dashboard.component.ts ***!
  \**************************************************************************/
/*! exports provided: SuperAdminDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperAdminDashboardComponent", function() { return SuperAdminDashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_message_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/message-service.service */ "./src/app/services/message-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SuperAdminDashboardComponent = /** @class */ (function () {
    function SuperAdminDashboardComponent(messageService) {
        this.messageService = messageService;
        this.clientsSpecification = false;
        this.userData = {
            role: "super-admin"
        };
    }
    SuperAdminDashboardComponent.prototype.ngOnDestroy = function () {
        // this.subscription.unsubscribe();
    };
    SuperAdminDashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-super-admin-dashboard',
            template: __webpack_require__(/*! ./super-admin-dashboard.component.html */ "./src/app/super-admin-dashboard/super-admin-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./super-admin-dashboard.component.css */ "./src/app/super-admin-dashboard/super-admin-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_services_message_service_service__WEBPACK_IMPORTED_MODULE_1__["MessageService"]])
    ], SuperAdminDashboardComponent);
    return SuperAdminDashboardComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/super-admin-dashboard.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/super-admin-dashboard/super-admin-dashboard.module.ts ***!
  \***********************************************************************/
/*! exports provided: SuperAdminDashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperAdminDashboardModule", function() { return SuperAdminDashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _super_admin_dashboard_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./super-admin-dashboard.routing */ "./src/app/super-admin-dashboard/super-admin-dashboard.routing.ts");
/* harmony import */ var _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-module/shared.module */ "./src/app/shared-module/shared.module.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _create_zenworker_template_component_create_zenworker_template_component_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./create-zenworker-template-component/create-zenworker-template-component.component */ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var SuperAdminDashboardModule = /** @class */ (function () {
    function SuperAdminDashboardModule() {
    }
    SuperAdminDashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_create_zenworker_template_component_create_zenworker_template_component_component__WEBPACK_IMPORTED_MODULE_10__["CreateZenworkerTemplateComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_8__["MatSlideToggleModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _super_admin_dashboard_routing__WEBPACK_IMPORTED_MODULE_4__["SuperAdminRouting"],
                _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_12__["MaterialModuleModule"]
            ],
            providers: []
        })
    ], SuperAdminDashboardModule);
    return SuperAdminDashboardModule;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/super-admin-dashboard.routing.ts":
/*!************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/super-admin-dashboard.routing.ts ***!
  \************************************************************************/
/*! exports provided: SuperAdminRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperAdminRouting", function() { return SuperAdminRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _super_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./super-admin-dashboard.component */ "./src/app/super-admin-dashboard/super-admin-dashboard.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _zenworkers_zenworkers_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./zenworkers/zenworkers.component */ "./src/app/super-admin-dashboard/zenworkers/zenworkers.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _create_zenworker_template_component_create_zenworker_template_component_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./create-zenworker-template-component/create-zenworker-template-component.component */ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.ts");
/* harmony import */ var _clients_clients_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./clients/clients.component */ "./src/app/super-admin-dashboard/clients/clients.component.ts");
/* harmony import */ var _create_company_create_company_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./create-company/create-company.component */ "./src/app/super-admin-dashboard/create-company/create-company.component.ts");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _edit_company_edit_company_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./edit-company/edit-company.component */ "./src/app/super-admin-dashboard/edit-company/edit-company.component.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../shared-module/shared.module */ "./src/app/shared-module/shared.module.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/super-admin-dashboard/invoice/invoice.component.ts");
/* harmony import */ var _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../services/chardgeCreditCard.service */ "./src/app/services/chardgeCreditCard.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_tokenInterceptor_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../services/tokenInterceptor.service */ "./src/app/services/tokenInterceptor.service.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




/* import { SideNavComponent } from '../shared-module/side-nav-component/side-nav.component'; */





















var routes = [
    { path: '', redirectTo: 'super-admin-dashboard', pathMatch: "full" },
    {
        path: 'super-admin-dashboard', component: _super_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["SuperAdminDashboardComponent"], children: [
            { path: '', redirectTo: 'clients', pathMatch: "full" },
            { path: 'zenworkers', component: _zenworkers_zenworkers_component__WEBPACK_IMPORTED_MODULE_5__["ZenworkersComponent"] },
            { path: 'clients', component: _clients_clients_component__WEBPACK_IMPORTED_MODULE_11__["ClientsComponent"] },
            { path: 'edit-company/:id', component: _create_company_create_company_component__WEBPACK_IMPORTED_MODULE_12__["CreateCompanyComponent"] },
            { path: 'edit-company', component: _edit_company_edit_company_component__WEBPACK_IMPORTED_MODULE_15__["EditCompanyComponent"] },
            { path: 'settings', loadChildren: './package-creation/package-creation.module#PackageCreationModule' },
            { path: 'invoice', component: _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_19__["InvoiceComponent"] },
            { path: 'client-details', loadChildren: './client-details/client-details.module#ClientDetailsModule' },
            { path: 'reseller/:id', loadChildren: '../reseller-dashboard/reseller-dashboard.module#ResellerDashboardModule' },
        ]
    },
];
var SuperAdminRouting = /** @class */ (function () {
    function SuperAdminRouting() {
    }
    SuperAdminRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_18__["SharedModule"],
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__["ModalModule"].forRoot(),
                _angular_material_select__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_13__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatInputModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_16__["MatTabsModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_17__["MatSlideToggleModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatNativeDateModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_21__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_23__["MaterialModuleModule"]
            ],
            declarations: [
                _super_admin_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["SuperAdminDashboardComponent"],
                _zenworkers_zenworkers_component__WEBPACK_IMPORTED_MODULE_5__["ZenworkersComponent"],
                _clients_clients_component__WEBPACK_IMPORTED_MODULE_11__["ClientsComponent"],
                _create_company_create_company_component__WEBPACK_IMPORTED_MODULE_12__["CreateCompanyComponent"],
                _edit_company_edit_company_component__WEBPACK_IMPORTED_MODULE_15__["EditCompanyComponent"],
                _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_19__["InvoiceComponent"],
            ],
            entryComponents: [
                _create_zenworker_template_component_create_zenworker_template_component_component__WEBPACK_IMPORTED_MODULE_10__["CreateZenworkerTemplateComponent"]
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ],
            providers: [_services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_20__["CreditCardChargingService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_21__["HTTP_INTERCEPTORS"],
                    useClass: _services_tokenInterceptor_service__WEBPACK_IMPORTED_MODULE_22__["TokenInterceptor"],
                    multi: true
                },]
        })
    ], SuperAdminRouting);
    return SuperAdminRouting;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/zenworkers/zenworkers.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/zenworkers/zenworkers.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenworkers-wrapper{\n    background: #f8f8f8;\n    height: 100vh;\n}\n\n.zenwork-currentpage {\n    padding: 40px 0 0 0;\n}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.zenworkers-wrapper{\n    padding: 0px; float: none; margin: 0 auto;\n}\n\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 15px;\n    width:22px;\n    color:#9a9898;\n    font-size: 13px; cursor: pointer;\n}\n\n.zenwork-margin-ten-zero {\n    margin: 20px 0px!important;\n}\n\n.zenwork-margin-zero { font-size: 17px; line-height: 15px; vertical-align: middle; color:#008f3d;}\n\n.zenwork-margin-zero small{ display: inline-block; vertical-align:text-bottom; padding: 0 10px 0 0;}\n\n.zenwork-inner-icon { vertical-align: text-top; width: 30px;}\n\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n\n.zenwork-custom-select{\n    background: #fff!important;\n}\n\n.border-none{\n    border:0 none !important;\n}\n\n.box-shadow-none{\n    box-shadow: none !important;\n}\n\n.zenwork-table-customized{\n    background: #fff;\n}\n\n.zenwork-table-customized.table-condensed>tbody>tr>td{\n    padding: 20px; border-top: none;border-bottom: #efefef 1px solid; color: #807d7d;\n}\n\n.zenwork-table-customized.table-condensed>thead>tr>th{\n    padding: 15px 20px; background:#eef7ff;border-bottom:none; color: #3e3c3c; font-weight: normal;\n}\n\n.search-form .form-control {padding:12px 40px 12px 20px; height: auto;}\n\n.zenwork-customized-green-btn{\n    background: #797d7b!important;\n    color: #fff!important;\n    padding:8px 35px!important;\n    font-size:15px!important;\n}\n\n.zenwork-customized-green-btn small { display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n\n.zenwork-customized-green-btn small .fa { color:#fff; font-size: 15px;}\n\n.zenwork-customized-transparent-btn:focus, .zenwork-customized-transparent-btn:active{ outline: none;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/zenworkers/zenworkers.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/zenworkers/zenworkers.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper col-md-11\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <small>\n        <img src=\"../../../assets/images/company-settings/site-access/site-access-icon.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </small>\n      Zenworkers\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"zenwork-clients-wrapper\">\n    <div class=\"col-xs-4 padding-left-zero\">\n      <div class=\"search-border text-left\">\n        <form [formGroup]=\"zenworkersSearch\" class=\"search-form\">\n          <input type=\"text\" class=\"border-none box-shadow-none form-control\" formControlName=\"search\"\n            placeholder=\"Search by name,title, etc\">\n        </form>\n        <span>\n          <span class=\"glyphicon glyphicon-search search-icon\" aria-hidden=\"true\"></span>\n        </span>\n      </div>\n    </div>\n    <div class=\"col-xs-6 col-xs-offset-2\">\n      <div class=\"col-xs-8\">\n        <div class=\"input-group\">\n          <span class=\"input-group-addon zenwork-client-group-addon zenwork-input-group-addon\">\n            <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" alt=\"First Name Icon\"\n              class=\"zenwork-demo-input-image-group zenwork-company-group\">\n          </span>\n          <mat-select class=\"form-control zenwork-input zenwork-custom-select\" placeholder=\"All Clients\"\n            [(ngModel)]=\"selectedZenworkRole\" (ngModelChange)=\"getZenworkersData();\">\n            <mat-option value=\"all\">\n              Select Profile\n            </mat-option>\n            <mat-option *ngFor=\"let data of userProfile\" [value]=\"data._id\">\n              {{data.name}}\n            </mat-option>\n\n          </mat-select>\n        </div>\n      </div>\n      <div *ngIf='!restrictLevel1' class=\"col-xs-4\">\n\n        <button (click)=\"addZenworker('')\" class=\"btn zenwork-customized-green-btn\">\n          <small><i aria-hidden=\"true\" class=\"fa fa-plus\"></i></small> Add Zenworker\n        </button>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n    <div class=\"clearfix\"></div>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"clients-tablle-wrapper\">\n    <table class=\"table table-responsive table-condensed zenwork-table-customized\">\n      <thead>\n        <tr class=\"info\">\n\n          <th>\n            <div class=\"cont-check\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"allZenworkers\"\n                (change)=\"selectAllZenworkers($event)\"></mat-checkbox>\n            </div>\n          </th>\n          <th>Name</th>\n          <th>\n            Email\n          </th>\n          <th>\n            Profile\n          </th>\n          <th>\n            <div *ngIf='!restrictLevel1' class=\"setting-drop\">\n              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n              </a>\n              <ul *ngIf=\"selectedOne.length > 0\" class=\"dropdown-menu\">\n                <li>\n                  <a (click)=\"selectOnlyOne()\">Edit</a>\n                </li>\n                <li>\n                  <a (click)=\"deleteZenworkers()\">Delete</a>\n                </li>\n\n              </ul>\n            </div>\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let zenworker of zenworkerDetails\">\n          <td [ngClass]=\"{'zenwork-checked-border':zenworker.isChecked}\">\n            <div class=\"cont-check\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" (ngModelChange)=\"selectOne($event,zenworker)\"\n                [(ngModel)]=\"zenworker.isChecked\"></mat-checkbox>\n            </div>\n          </td>\n          <td>{{zenworker.name}}</td>\n          <td>\n            {{zenworker.email}}\n          </td>\n          <td>\n            {{zenworker.roleId.name}}\n          </td>\n          <td>\n\n            <!-- <button class=\"zenwork-customized-transparent-btn\" (click)=\"deleteZenworker(zenworker)\">\n              <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n            </button> -->\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/zenworkers/zenworkers.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/zenworkers/zenworkers.component.ts ***!
  \**************************************************************************/
/*! exports provided: ZenworkersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZenworkersComponent", function() { return ZenworkersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _create_zenworker_template_component_create_zenworker_template_component_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../create-zenworker-template-component/create-zenworker-template-component.component */ "./src/app/super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ZenworkersComponent = /** @class */ (function () {
    function ZenworkersComponent(dialog, zenworkersService, swalAlertService, loaderService, accessLocalStorageService) {
        this.dialog = dialog;
        this.zenworkersService = zenworkersService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.allZenworkers = false;
        this.userProfile = [];
        this.selectedOne = [];
        this.restrictLevel1 = false;
    }
    ZenworkersComponent.prototype.ngOnInit = function () {
        this.loaderService.loader(true);
        this.type = this.accessLocalStorageService.get("type");
        console.log(this.type, "2w2w");
        if (this.type == 'administrator' || this.type == 'zenworker') {
            this.acceslevel = this.accessLocalStorageService.get('zenworkerId');
            console.log(this.acceslevel);
            if (this.acceslevel.roleId.name == 'Level 1') {
                this.restrictLevel1 = true;
            }
        }
        this.zenworkersSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('')
        });
        this.selectedZenworkRole = "all";
        this.perPage = 10;
        this.pages = [10, 25, 50];
        this.pageNo = 1;
        // this.zenworkerDetails = [{ name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false },
        // { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false },
        // { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false }]
        this.selectedZenworkerDetails = { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false };
        this.onChanges();
        this.getZenworkersData();
        this.getAllRoles();
    };
    ZenworkersComponent.prototype.onChanges = function () {
        var _this = this;
        this.zenworkersSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            // console.log(this.zenworkersSearch.get('search').value);
            _this.getZenworkersData();
        });
    };
    /* Description: get zenworkers data list
   author : vipin reddy */
    ZenworkersComponent.prototype.getZenworkersData = function () {
        var _this = this;
        console.log("change", this.selectedZenworkRole);
        this.loaderService.loader(true);
        var zenworker;
        if (this.selectedZenworkRole == "all") {
            zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage };
        }
        else {
            this.selectedRole = this.selectedZenworkRole;
            zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, role: this.selectedZenworkRole };
        }
        console.log(zenworker);
        this.zenworkersService.getZenworkersDetails(zenworker)
            .subscribe(function (response) {
            _this.loaderService.loader(false);
            if (response.status) {
                console.log(response.data);
                // this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')
                _this.zenworkerDetails = response.data;
                _this.zenworkerDetails.forEach(function (zenworker) {
                    zenworker.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Zenworkers", "Data Not Found", 'info');
            }
        }, function (err) {
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: select only one for zenworker
   author : vipin reddy */
    ZenworkersComponent.prototype.selectOnlyOne = function () {
        var _this = this;
        // this.addZenworker(this.addedData)
        if (this.selectedOne.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select only one Zenworker on to Proceed", 'error');
        }
        else if (this.selectedOne.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select a Zenworker to Proceed", 'error');
        }
        if (this.selectedOne.length == 1) {
            console.log("ssss");
            this.zenworkersService.getSingleZenworker(this.selectedOne[0])
                .subscribe(function (res) {
                console.log(res);
                _this.addedData = res.data;
                _this.addZenworker(_this.addedData);
            }, function (err) {
            });
        }
    };
    /* Description: selected one zenworker
   author : vipin reddy */
    ZenworkersComponent.prototype.selectOne = function (event, data) {
        var _this = this;
        console.log(event, data._id);
        if (event == true) {
            this.selectedOne.push(data._id);
            this.addedData = data;
            // if (this.selectedOne.length > 1) {
            //   this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select only one Zenworker on to Proceed", 'error')
            // } else if (this.selectedOne.length == 0) {
            //   this.swalAlertService.SweetAlertWithoutConfirmation("Edit Zenworker", "Select a Zenworker to Proceed", 'error')
            // } else {
            //   this.addedData = data
            // }
        }
        if (event == false) {
            this.selectedOne.forEach(function (element) {
                if (data._id == element) {
                    var index = _this.selectedOne.indexOf(data._id);
                    if (index > -1) {
                        _this.selectedOne.splice(index, 1);
                        // delete this.selectedOne['isChecked']
                    }
                }
            });
        }
        console.log(this.selectedOne);
    };
    /* Description: selecting all zenworkers
   author : vipin reddy */
    ZenworkersComponent.prototype.selectAllZenworkers = function (event) {
        var _this = this;
        // console.log("Selecting all", this.allZenworkers);
        console.log(event);
        if (event.checked == true) {
            this.zenworkerDetails.forEach(function (zenworker) {
                console.log(zenworker);
                _this.selectedOne.push(zenworker._id);
                zenworker.isChecked = true;
            });
            console.log(this.selectedOne);
        }
        if (event.checked == false) {
            this.selectedOne = [];
            this.zenworkerDetails.forEach(function (zenworker) {
                console.log(zenworker);
                zenworker.isChecked = false;
            });
        }
    };
    /* Description: deleting zenworkers multiple
 author : vipin reddy */
    ZenworkersComponent.prototype.deleteZenworkers = function () {
        var _this = this;
        var postdata = {
            _ids: this.selectedOne
        };
        this.zenworkersService.deleteZenworkers(postdata)
            .subscribe(function (res) {
            console.log("111111111", res);
            if (res.status == true)
                _this.swalAlertService.SweetAlertWithoutConfirmation("Delete", res.message, 'success');
            _this.getZenworkersData();
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: pagination for zenworkers
      author : vipin reddy */
    ZenworkersComponent.prototype.changePage = function (p) {
        this.pageNo = p;
        this.getZenworkersData();
    };
    /* Description: add zenworker popup
    author : vipin reddy */
    ZenworkersComponent.prototype.addZenworker = function (data) {
        var _this = this;
        var dialogRef = this.dialog.open(_create_zenworker_template_component_create_zenworker_template_component_component__WEBPACK_IMPORTED_MODULE_4__["CreateZenworkerTemplateComponent"], {
            height: '700px',
            width: '890px',
            data: data,
            autoFocus: false
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Result", result);
            _this.selectedOne = [];
            _this.getZenworkersData();
        });
    };
    /* Description: add zenworker popup
    author : vipin reddy */
    ZenworkersComponent.prototype.deleteZenworker = function (zenworker) {
    };
    /* Description: get all zenworker levels
    author : vipin reddy */
    ZenworkersComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.zenworkersService.getAllroles()
            .subscribe(function (res) {
            console.log("111111111", res);
            if (res.status == true) {
                // this.loaderService.loader(false)
                _this.userProfile = res.data;
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ZenworkersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-zenworkers',
            template: __webpack_require__(/*! ./zenworkers.component.html */ "./src/app/super-admin-dashboard/zenworkers/zenworkers.component.html"),
            styles: [__webpack_require__(/*! ./zenworkers.component.css */ "./src/app/super-admin-dashboard/zenworkers/zenworkers.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__["ZenworkersService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_7__["AccessLocalStorageService"]])
    ], ZenworkersComponent);
    return ZenworkersComponent;
}());



/***/ })

}]);
//# sourceMappingURL=super-admin-dashboard-super-admin-dashboard-module.js.map