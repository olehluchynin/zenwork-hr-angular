(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["manager-approval-manager-approval-module"],{

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.css":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.css ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".compensation-block{ margin: 0 auto; float: none; padding: 0;}\n\n.compensation { padding:30px 0 0;}\n\n.compensation .panel-default>.panel-heading { padding: 0 0 20px; background-color:transparent !important; border: none;}\n\n.compensation .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n\n.compensation .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n\n.compensation .panel-title>.small, .compensation .panel-title>.small>a, .compensation .panel-title>a, .compensation .panel-title>small, .compensation .panel-title>small>a { text-decoration:none;}\n\n.compensation .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n\n.compensation .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n\n.compensation .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.compensation .panel-group .panel-heading+.panel-collapse>.list-group, .compensation .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.compensation .panel-heading .accordion-toggle:after { margin:7px 15px 0; font-size: 13px; line-height: 13px;}\n\n.compensation .panel-body { padding: 0 0 5px;}\n\n.compensation .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.compensation .panel-default { border-color:transparent;}\n\n.compensation .panel-group .panel+.panel { margin-top: 20px;}\n\n.compensation-main { display: block;}\n\n.compensation-changes {display: block;}\n\n.compensation-changes .table{ background:#fff;}\n\n.compensation-changes .table>tbody>tr>th, .compensation-changes .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n\n.compensation-changes .table>tbody>tr>td, .compensation-changes .table>tfoot>tr>td, .compensation-changes .table>tfoot>tr>th, \n.compensation-changes .table>thead>tr>td {color: #484747;padding:15px;font-size:15px;}\n\n.compensation-changes button.mat-menu-item a { color:#ccc;}\n\n.compensation-changes button.mat-menu-item a:hover { color:#fff; background: #099247;}\n\n.compensation-changes .table>tbody>tr>th a, .compensation-changes .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n\n.compensation-changes .table>tbody>tr>th a .fa, .compensation-changes .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.compensation-changes button { border: none; outline: none; background:transparent;}\n\n.top-history{ margin: 10px 0 20px;}\n\n.top-history ul{ display: block;}\n\n.top-history ul li{ padding: 0 5px;}\n\n.top-history ul li.select-date{ margin: 25px 0 0;}\n\n.top-history ul li label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 7px 15px 0;}\n\n.top-history ul li .form-control { border: none;box-shadow: none;padding: 12px 12px;height: auto;\nwidth:50%; font-size: 15px;background: #fff; display: inline-block;}\n\n.example-form { position: relative;}\n\n.top-history ul li small { position: absolute; top:4px; right:5px; cursor: pointer;}\n\n.top-history ul li small .material-icons { color: #8e8e8e; font-size: 20px; font-weight: normal;}\n\n.top-history ul li .range{ margin: 0 5px;}\n\n.top-history ul li .range label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 0 7px; display: block;}\n\n.top-history ul li .range .date { height: auto !important; line-height:inherit !important;background:#fff; width: 165px;}\n\n.top-history ul li .range .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.top-history ul li .range .date span { border: none !important; padding: 0 !important;}\n\n.top-history ul li .range .date .form-control { width:72%; height: auto; padding: 12px 0 12px 10px;}\n\n.top-history ul li .range .date .form-control:focus { box-shadow: none; border: none;}\n\n.top-history ul li .range .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.history-search { margin:28px 0 0;}\n\n.history-search .form-group{ margin-bottom: 0;}\n\n.mdl-history { display: block;}\n\n.mdl-history ul{ margin: 0 0 10px;}\n\n.mdl-history ul li{ padding: 0 15px; display: inline-block;}\n\n.mdl-history ul li a{color: #484747; font-weight: normal; font-size: 14px;cursor: pointer;}\n\n.mdl-active{    \n    border-bottom: 3px solid #439348;\n    padding: 0 0 9px 0;}\n\n.mdl-active .fa{\n        color: #008f3d !important;\n    }\n\n.mdl-activered{\n    border-bottom: 3px solid #f00;\n    padding: 0 0 9px 0;}\n\n.mdl-activered .fa{\n        color: #f00 !important;\n}\n\n.mdl-history ul li.active a { border-bottom:#008f3d 2px solid;}\n\n.mdl-history ul li a span, .mdl-history ul li a small { vertical-align:text-bottom; padding: 0 0 0 5px;}\n\n.mdl-history ul li a span .fa{ color:#ccc; font-size:8px;}\n\n.mdl-history ul li a small .fa{ color:#ccc; font-size:8px;}\n\n.mdl-history .table{ background:#fff;}\n\n.mdl-history .table>tbody>tr>th, .mdl-history .table>thead>tr>th {padding: 15px; color: #484747; font-weight: normal; \nfont-size:16px;vertical-align: middle; position: relative; background: #edf7ff;}\n\n.mdl-history .table>tbody>tr>td, .mdl-history .table>tfoot>tr>td, .mdl-history .table>tfoot>tr>th, \n.mdl-history .table>thead>tr>td {color: #484747;padding:15px;font-size:15px; vertical-align: middle; border-bottom:#ccc 1px solid;}\n\n.mdl-history .table>tbody>tr>th a, .mdl-history .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n\n.mdl-history .table>tbody>tr>th a .fa, .mdl-history .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.managers { display: block;}\n\n.managers span { vertical-align: middle; display: inline-block;}\n\n.manager-right { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;}\n\n.manager-right h4 { font-size: 15px; margin: 0 0 5px;}\n\n.manager-right h5 {  color: #ccc;margin: 0;}\n\n.mdl-history .table>tbody>tr>td small { font-size: 15px; vertical-align: middle;}\n\n.mdl-history .table>tbody>tr>td small .fa{ color:#008f3d; font-size: 10px; vertical-align: middle;}\n\n.date1 { height: auto !important; line-height:inherit !important;background:#f8f8f8; width: 160px;}\n\n.date1 button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.date1 .form-control { width:65% !important; height: auto; padding: 12px 0 12px 10px; display: inline-block;}\n\n.date1 .form-control:focus { box-shadow: none; border: none;}\n\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.date1 span { display: inline-block; vertical-align: middle; cursor: pointer;border-left: #bdbdbd 1px solid; padding: 0 0 0 15px !important;}\n\n.date1 span .fa { color: #008f3d;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.approval .modal{\n    top:50%;\n}\n\n.approval span h4{\n    /* font-weight: 600; */\n    padding: 10px 0 10px 5px;  \n}\n\n.location-request .form-control{\n    width: 94% !important;\n}\n\n.approval .modal-dialog{\n    width: 900px;\n}\n\n.modal-approval{\n    padding: 50px;\n    text-align: center;\n    font-size: 16px;\n    line-height: 30px;\n}\n\n.modal-approval p {\n    margin: 20px 0 0 0;\n}\n\n.footer{\n    margin: 20px auto 0;\n    float: none;\n}\n\n.footer .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n\n.footer .btn.cancel { color:#e44a49; background:transparent; padding:8px 0 0 30px;font-size: 16px;}\n\n.text-area{\n    resize: none;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.html":
/*!************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.html ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"compensation-block col-md-11\">\n\n  <div class=\"compensation\">\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n              Compensation Changes - Pending\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"compensation-main\">\n\n              <div class=\"searching\">\n                <div class=\"form-group col-md-4 pull-right\">\n\n                  <input type=\"mail\" class=\"form-control\" placeholder=\"Search Employee by Name, Job #, Job Title etc...\"\n                    [(ngModel)]=\"searchFilter\" (ngModelChange)=\"searchfilterChange()\">\n                  <span><i class=\"material-icons\">search</i></span>\n\n                </div>\n                <div class=\"clearfix\"></div>\n              </div>\n\n              <div class=\"compensation-changes\">\n\n                <table class=\"table\">\n                  <thead>\n                    <tr>\n                      <th>\n                        <!-- <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox> -->\n                      </th>\n                      <th>Employee Name</th>\n                      <th>Submission Request Date</th>\n                      <th>Department</th>\n\n                      <th>Approval Status &nbsp;\n                        <span>\n                          <!-- <mat-slide-toggle></mat-slide-toggle> -->\n                        </span>\n                      </th>\n                      <th>\n                        <button [disabled]=\"supAdminReadonly\" mat-icon-button [matMenuTriggerFor]=\"menu1\"\n                          aria-label=\"Example icon-button with a menu\">\n                          <mat-icon>more_vert</mat-icon>\n                        </button>\n                        <mat-menu #menu1=\"matMenu\">\n                          <button mat-menu-item>\n                            <a data-toggle=\"modal\" data-target=\"#myModal2\" (click)=\"compensationEdit()\">Edit Request</a>\n                          </button>\n                        </mat-menu>\n                      </th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <!-- <tr *ngFor=\"let data of compensationData\"> -->\n                    <tr *ngFor=\"let data of compensationData\">\n                      <!-- <td>\n                        <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\" (ngModelChange)=\"empSelect()\"></mat-checkbox>\n                      </td> -->\n                      <td>\n                        <mat-checkbox [(ngModel)]=\"data.isChecked\"\n                          (ngModelChange)=\"selectRequest(data.isChecked,data._id,data.manager_request_id)\"\n                          class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                      </td>\n                      <td>{{data.impactedEmployee.personal.name.preferredName}}</td>\n                      <td>{{data.manager_request.requestedDate | date}}</td>\n                      <td>{{data.impactedEmployee.job.department}}</td>\n                      <td *ngIf=\"data.next_priority\">\n                        <small *ngIf=\"data.next_priority.personal\">\n                          <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                            (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,data.next_priority.personal.name.preferredName,data.manager_request_id)\">\n                          </mat-slide-toggle>\n                        </small>\n                        <small *ngIf=\"data.next_priority.name\">\n                          <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                            (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,data.next_priority.name,data.manager_request_id)\">\n                          </mat-slide-toggle>\n                        </small>\n                      </td>\n\n                      <td *ngIf=\"!data.next_priority\">\n\n                        <small *ngIf=\"!data.next_priority\">\n                          <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                            (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,'----',data.manager_request_id)\">\n                          </mat-slide-toggle>\n                        </small>\n                      </td>\n                      <td></td>\n                    </tr>\n\n                    <tr *ngIf=\"compensationData.length == 0\">\n                      <td></td>\n                      <td></td>\n                      <td>No records found</td>\n\n                    </tr>\n\n                  </tbody>\n                </table>\n\n                <mat-paginator [length]=\"count\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n                  (page)=\"pageEvents($event)\"></mat-paginator>\n\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwo\">\n              Compensation Changes - History\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"compensation-main\">\n\n\n              <div class=\"top-history\">\n\n                <ul>\n\n                  <!-- <li class=\"col-md-3 select-date\">\n                    <label>Select Date Range</label>\n                    <mat-select placeholder=\"Select Range\" class=\"form-control\">\n                      <mat-option value=\"\">Last Week</mat-option>\n                      <mat-option value=\"\">Last Month</mat-option>\n                    </mat-select>\n                  </li> -->\n\n                  <li class=\"col-lg-5 col-md-7\">\n\n                    <div class=\"range pull-left\">\n                      <label>Begin Date Range(T1)</label>\n                      <div class=\"date\">\n                        <input matInput [(ngModel)]=\"beginDate\" [max]=\"endDate\" (ngModelChange)=\"beginDateChange()\"\n                          [matDatepicker]=\"picker10\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                        <mat-datepicker #picker10></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker10.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </div>\n\n                    <div class=\"range pull-left\">\n                      <label>End Date Range(T2)</label>\n                      <div class=\"date\">\n                        <input matInput [(ngModel)]=\"endDate\" [min]=\"beginDate\" (ngModelChange)=\"endDateChange()\"\n                          [matDatepicker]=\"picker11\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                        <mat-datepicker #picker11></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker11.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </div>\n\n                    <div class=\"clearfix\"></div>\n                  </li>\n\n                  <li class=\"col-md-2 select-date location-request\">\n\n                    <!-- <mat-select class=\"form-control\" placeholder=\"location\" [(ngModel)]=\"filterlocation\"\n                      class=\"form-control\">\n                      <mat-option value=\"hyderabad\">Hyderabad</mat-option>\n                      <mat-option value=\"newdelhi\">New delhi</mat-option>\n                    </mat-select> -->\n\n                    <!-- <div class=\"searching history-search\">\n                          <div class=\"form-group col-md-12\">\n                            <form>\n                              <input type=\"mail\" class=\"form-control\" placeholder=\"Search Employee\">\n                              <span><i class=\"material-icons\">search</i></span>\n                            </form>\n                          </div>\n\n                        </div> -->\n\n                  </li>\n\n                  <li class=\"col-md-3 pull-right\">\n\n                    <div class=\"searching history-search\">\n                      <div class=\"form-group col-md-12\">\n\n                        <input type=\"mail\" [(ngModel)]=\"searchFilterHistory\"\n                          (ngModelChange)=\"searchFilterHistoryChange()\" class=\"form-control\"\n                          placeholder=\"Search Employee by Name, Job\">\n                        <span><i class=\"material-icons\">search</i></span>\n\n                      </div>\n\n                    </div>\n\n                  </li>\n\n                </ul>\n                <div class=\"clearfix\"></div>\n\n              </div>\n\n              <div class=\"mdl-history\">\n\n                <ul>\n                  <li><a [ngClass]=\"{'mdl-active' : requestStatus == '' }\" (click)=\"requestType('')\">Show All</a></li>\n                  <li><a [ngClass]=\"{'mdl-active' : requestStatus == 'Approved' }\"\n                      (click)=\"requestType('Approved')\">Approved <span><i class=\"fa fa-circle\"\n                          aria-hidden=\"true\"></i></span></a></li>\n                  <li><a [ngClass]=\"{'mdl-activered' : requestStatus == 'Rejected' }\"\n                      (click)=\"requestType('Rejected')\">Rejected <small><i class=\"fa fa-circle\"\n                          aria-hidden=\"true\"></i></small></a></li>\n                </ul>\n                <div class=\"clearfix\"></div>\n\n\n                <table class=\"table\">\n                  <thead>\n                    <tr>\n\n                      <th>Employee Name</th>\n                      <th>Submission Request Date</th>\n                      <th>Submission Complete Date</th>\n                      <th>Department</th>\n                      <th>Request Status</th>\n\n                    </tr>\n                  </thead>\n\n                  <tbody>\n\n                    <tr *ngFor=\"let data of compensationDataHistory\"\n                      (click)=\"showCompensationHistory(data.manager_request_id)\">\n                      <td>\n                        <div class=\"managers\">\n                          <!-- <span><img src=\"../../../../assets/images/Manager Self Service/employee.png\"></span> -->\n                          <div class=\"manager-right\">\n                            <h4>{{data.impactedEmployee.personal.name.preferredName}}</h4>\n                            <!-- <h5>Manager</h5> -->\n                          </div>\n                        </div>\n                      </td>\n                      <td>{{data.manager_request.requestedDate | date}}</td>\n                      <td *ngIf=\"data.manager_request.completedate\"> {{data.manager_request.completedate | date }}</td>\n                      <td *ngIf=\"!data.manager_request.completedate\"> -- </td>\n                      <!-- <td [ngIf] = \"!data.manager_request.completedate\" >  -- </td> -->\n\n                      <td>{{data.impactedEmployee.job.department}}</td>\n                      <td *ngIf=\"data.request_status == 'Approved'\"><small> <i class=\"fa fa-circle\"\n                            aria-hidden=\"true\"></i> &nbsp;\n                          {{data.request_status}}</small>\n                      </td>\n                      <td *ngIf=\"data.request_status == 'Rejected'\"><small> <i class=\"fa fa-circle\" aria-hidden=\"true\"\n                            style=\"color:#f00;\"></i> &nbsp;\n                          {{data.request_status}}</small>\n                      </td>\n                      <td *ngIf=\"data.request_status == 'Pending'\"><small> <i class=\"fa fa-circle\" aria-hidden=\"true\"\n                            style=\"color:#f00;\"></i> &nbsp;\n                          {{data.request_status}}</small>\n                      </td>\n                    </tr>\n                    <tr *ngIf=\"compensationDataHistory.length == 0\">\n                      <td></td>\n                      <td></td>\n                      <td>No records found</td>\n                      <td></td>\n                      <td></td>\n                    </tr>\n\n                  </tbody>\n                </table>\n\n                <mat-paginator [length]=\"countHistory\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n                  (page)=\"pageEventsHistory($event)\"></mat-paginator>\n\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"approval\">\n      <div class=\"modal fade\" id=\"myModal3\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n\n          <div class=\"modal-content\">\n            <div class=\"modal-body modal-approval\">\n              <!-- <em>\n                <i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i></em> -->\n              <span><img src=\"../../../../assets/images/Manager Self Service/error.png\"></span>\n              <p *ngIf=\"popupName != '----'\"> Are you sure you want to approve?<br>\n                The next approval priority is <span style=\"text-decoration:underline\">{{popupName}}</span></p>\n              <p *ngIf=\"popupName == '----'\">\n                Are you sure you want to approve?<br>\n                You are the last person in the approval priority\n              </p>\n\n              <div class=\"footer col-md-4\">\n                <button class=\"btn save\" type=\"submit\" (click)=\"taskApproved()\">Proceed</button>\n                <button class=\"btn pull-left cancel\" (click)=\"modalClose()\">Cancel</button>\n                <div class=\"clearfix\"></div>\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</div>\n\n\n\n<!-- Compansation Change Popup-->\n\n<div class=\"job-change-popup\">\n\n  <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Manager Approval - Pending Request</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"job-main\">\n\n            <div class=\"job-change\">\n\n              <div class=\"job-change-in col-md-11\">\n\n                <ul>\n\n                  <li>\n                    <label>Select Employee Name</label>\n                    <!-- [(ngModel)]= \"compensationRes.\" -->\n                    <input class=\"form-control\" placeholder=\"Employee Name\"\n                      [(ngModel)]=\"compensationRes.requestedFor.personal.name.preferredName\" readonly>\n                  </li>\n                  <li class=\"col-md-6\">\n                    <label>Current Status</label>\n                    <div class=\"job-status\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>Filed Name</th>\n                            <th>Current Value</th>\n                            <th>Effective Date</th>\n                          </tr>\n                        </thead>\n\n                        <tbody>\n\n                          <tr>\n                            <td>Pay Rate</td>\n                            <td>\n                              <input class=\"form-control\"\n                                [(ngModel)]=\"compensationRes.oldStatus.payRate.old_value.amount\"\n                                placeholder=\"Current Value\" readonly>\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.payRate.effective_date\"\n                                  [matDatepicker]=\"picker71\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker71 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker71.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                          <tr>\n                            <td>Frequency</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.oldStatus.Pay_frequency.old_value\"\n                                class=\"form-control\" placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.Pay_frequency.effective_date\"\n                                  [matDatepicker]=\"picker81\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker81 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker81.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n\n                          <tr>\n                            <td>Pay Type</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.oldStatus.payType.old_value\" class=\"form-control\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.payType.effective_date\"\n                                  [matDatepicker]=\"picker91\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker91 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker91.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                          <tr>\n                            <td>% Change Difference</td>\n                            <td *ngIf=\"compensationRes.oldStatus.percentage_change.old_value\">\n                              <input [(ngModel)]=\"compensationRes.oldStatus.percentage_change.old_value\"\n                                class=\"form-control\" placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td *ngIf=\"!compensationRes.oldStatus.percentage_change.old_value\">\n                              <input value=\"--\" class=\"form-control\" placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.percentage_change.effective_date\"\n                                  [matDatepicker]=\"picker1111\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker1111 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker1111.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                        </tbody>\n\n                      </table>\n\n                    </div>\n                  </li>\n                  <li class=\"col-md-6\">\n                    <label>New Status</label>\n                    <div class=\"job-status new\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>Filed Name</th>\n                            <th>New Value</th>\n                            <th>Effective Date</th>\n                          </tr>\n                        </thead>\n                        <tbody>\n\n                          <tr>\n                            <td>Pay Rate</td>\n                            <td>\n                              <input class=\"form-control\"\n                                [(ngModel)]=\"compensationRes.newStatus.payRate.new_value.amount\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.payRate.effective_date\"\n                                  [matDatepicker]=\"picker72\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker72 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker72.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                          <tr>\n                            <td>Frequency</td>\n                            <td>\n                              <input class=\"form-control\"\n                                [(ngModel)]=\"compensationRes.newStatus.Pay_frequency.new_value\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.Pay_frequency.effective_date\"\n                                  [matDatepicker]=\"picker82\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker82 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker82.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n\n                          <tr>\n                            <td>Pay Type</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.newStatus.payType.new_value\" class=\"form-control\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.payType.effective_date\"\n                                  [matDatepicker]=\"picker92\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker92 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker92.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                          <tr>\n                            <td>% Change Difference</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.newStatus.percentage_change.new_value\"\n                                class=\"form-control\" placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.percentage_change.effective_date\"\n                                  [matDatepicker]=\"picker0120\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker0120 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker0120.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                        </tbody>\n                      </table>\n\n\n                    </div>\n                  </li>\n                  <div class=\"clearfix\"></div>\n\n\n                  <li>\n                    <label>Change Reason</label>\n                    <input [(ngModel)]=\"compensationRes.changeReason\" class=\"form-control\" placeholder=\"Reason\" [disabled]=true>\n\n                  </li>\n\n                  <li class=\"text-area\">\n                    <label>Note Section</label>\n                    <textarea [(ngModel)]=\"compensationRes.notes\" rows=\"5\" class=\"form-control text-area\" id=\"comment\"\n                      [disabled]=\"true\"></textarea>\n                  </li>\n\n                  <li *ngIf=\"!approveRejectHide\">\n\n                    <p class=\"pull-left col-md-2\">\n                      <mat-checkbox [(ngModel)]=\"statusApprove\" (change)=\"statusChange($event,'approve')\"\n                        class=\"zenwork-customized-checkbox\">Approve\n                      </mat-checkbox>\n                    </p>\n\n                    <small class=\"pull-left\">\n                      <mat-checkbox [(ngModel)]=\"statusReject\" (change)=\"statusChange($event,'reject')\"\n                        class=\"zenwork-customized-checkbox\">Reject\n                      </mat-checkbox>\n                    </small>\n                    <div class=\"clearfix\"></div>\n                  </li>\n\n\n\n                  <li class=\"text-area\">\n                    <label>Approval</label>\n                    <div class=\"approval\">\n                      <span class=\"pull-left\">\n                        <h4>Approval Priority</h4>\n                      </span>\n                      <span class=\"pull-right col-md-5\">\n                        <h4>Approval Date</h4>\n                      </span>\n                      <div class=\"clearfix\"></div>\n                      <div cdkDropList class=\"example-list\">\n                        <div class=\"example-box\" *ngFor=\"let timePeriod of timePeriods\">\n                          <span *ngIf=\"timePeriod.name != 'deleted role'\" class=\"pull-left\"\n                            style=\"background: #fff;padding: 0;\">\n                            {{timePeriod.name}}\n                          </span>\n                          <span *ngIf=\"timePeriod.name == 'deleted role'\" class=\"pull-left\"\n                            style=\"background: #fff;color:#f00;padding: 0;\"> {{timePeriod.name}}\n                          </span>\n                          <span *ngIf=\"timePeriod.approval_date\" class=\"pull-right col-md-5\"\n                            style=\"background: #fff;padding: 0;\"> {{timePeriod.approval_date | date}}</span>\n                          <span *ngIf=\"!timePeriod.approval_date\" class=\"pull-right col-md-5\"\n                            style=\"background: #fff;padding: 0 0 0 5px;\"> --</span>\n\n                          <div class=\"clearfix\"></div>\n\n                        </div>\n                      </div>\n\n                    </div>\n                  </li>\n\n                  <li class=\"col-md-3 submission\">\n                    <label>Submission Request Date</label>\n                    <div class=\"date1\">\n                      <input matInput [(ngModel)]=\"compensationRes.requestedDate\" [matDatepicker]=\"picker721\"\n                        placeholder=\"mm / dd / yy\" class=\"form-control\" [disabled]=true>\n\n\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n                  <li class=\"col-md-3 submission\">\n                    <label>Submission Complete Date</label>\n                    <div class=\"date1\">\n                      <input matInput [(ngModel)]=\"submissionCompleteDate\" [matDatepicker]=\"picker722\"\n                        placeholder=\"mm / dd / yy\" class=\"form-control\" [disabled]=true>\n\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n\n                </ul>\n\n              </div>\n\n            </div>\n\n            <div class=\"termination-border col-md-11\">\n\n              <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n              <button *ngIf=\"!approveRejectHide\" class=\"btn pull-right save\" (click)=\"approvalsending()\">Submit</button>\n\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.ts":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.ts ***!
  \**********************************************************************************************************************************/
/*! exports provided: CompensationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompensationComponent", function() { return CompensationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/manager-approval.service */ "./src/app/services/manager-approval.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CompensationComponent = /** @class */ (function () {
    function CompensationComponent(compensationService, swalAlertService) {
        this.compensationService = compensationService;
        this.swalAlertService = swalAlertService;
        this.timePeriods = [];
        this.compensationData = [];
        this.compensationDataHistory = [];
        this.approvalStatus = false;
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.options = ['One', 'Two', 'Three'];
        this.supAdminReadonly = false;
        this.approveRejectHide = false;
        // pageNo: any;
        // perPage: any;
        this.singlePerson = false;
        // MatPaginator Inputs
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [2, 5, 10, 25, 100];
        this.compensationRes = {
            changeReason: '',
            notes: '',
            compensationRes: '',
            approvalPriority: [],
            requestedFor: {
                personal: {
                    name: {
                        preferredName: ''
                    }
                }
            },
            oldStatus: {
                Pay_frequency: {
                    effective_date: "",
                    old_value: {
                        amount: ''
                    }
                },
                payRate: {
                    old_value: {
                        amount: ''
                    },
                    effective_date: ""
                },
                payType: {
                    old_value: '',
                    effective_date: ""
                },
                percentage_change: {
                    old_value: '',
                    effective_date: ""
                },
            },
            newStatus: {
                Pay_frequency: {
                    effective_date: "",
                    new_value: {
                        amount: ''
                    }
                },
                payRate: {
                    new_value: {
                        amount: ''
                    },
                    effective_date: ""
                },
                payType: {
                    new_value: '',
                    effective_date: ""
                },
                percentage_change: {
                    new_value: '',
                    effective_date: ""
                },
            },
        };
    }
    CompensationComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return this.options.filter(function (option) { return option.toLowerCase().includes(filterValue); });
    };
    CompensationComponent.prototype.drop = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(this.timePeriods, event.previousIndex, event.currentIndex);
    };
    CompensationComponent.prototype.ngOnInit = function () {
        this.Pendingperpage = 5;
        this.PendingPageno = 1;
        this.HistoryPageno = 1;
        this.Historyperpage = 5;
        this.requestStatus = '';
        this.beginDate = '';
        this.endDate = '',
            this.searchFilterHistory = '';
        this.searchFilter = '';
        this.getCompensationPendingData();
        this.getCompensationHistory();
    };
    /* Description:  mat-paginatoin in manager pending history list
    author : vipin reddy */
    CompensationComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.PendingPageno = event.pageIndex + 1;
        this.Pendingperpage = event.pageSize;
        this.getCompensationPendingData();
    };
    /* Description: mat-paginatoin in manager approal and pending history list
    author : vipin reddy */
    CompensationComponent.prototype.pageEventsHistory = function (event) {
        console.log(event);
        this.HistoryPageno = event.pageIndex + 1;
        this.Historyperpage = event.pageSize;
        this.getCompensationHistory();
    };
    /* Description: get manager requests list date filter
  author : vipin reddy */
    CompensationComponent.prototype.beginDateChange = function () {
        console.log(this.beginDate);
        this.beginDate = this.beginDate.toISOString();
        this.getCompensationHistory();
    };
    /* Description: get manager requests list date filter
    author : vipin reddy */
    CompensationComponent.prototype.endDateChange = function () {
        console.log(this.endDate);
        this.endDate = this.endDate.toISOString();
        this.getCompensationHistory();
    };
    /* Description: manager requests Showall,approal and pending history list active tabs
    author : vipin reddy */
    CompensationComponent.prototype.requestType = function (data) {
        this.requestStatus = data;
        this.getCompensationHistory();
    };
    /* Description: manager requests list search filter
  author : vipin reddy */
    CompensationComponent.prototype.searchFilterHistoryChange = function () {
        console.log(this.searchFilterHistory);
        this.getCompensationHistory();
    };
    /* Description: manager requests list single request showing in popup
     author : vipin reddy */
    CompensationComponent.prototype.selectRequest = function (isChecked, id, mId) {
        var _this = this;
        console.log(isChecked, id, mId);
        this.compensationData.forEach(function (element) {
            if (id == element._id) {
                element.isChecked = true;
                _this.selectedId = mId;
            }
            else {
                element.isChecked = false;
            }
            console.log(_this.selectedId);
        });
    };
    /* Description: get manager requests list s
  author : vipin reddy */
    CompensationComponent.prototype.getCompensationHistory = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'));
        var type = JSON.parse(localStorage.getItem('type'));
        var siteAccessRoleId = JSON.parse(localStorage.getItem('siteAccessRoleId'));
        var postData = {
            companyId: cId,
            userId: eId,
            request_status: this.requestStatus,
            changeRequestType: 'Salary',
            per_page: this.Historyperpage,
            page_no: this.HistoryPageno,
            from_date: this.beginDate,
            to_date: this.endDate,
            roleId: siteAccessRoleId,
            search_query: this.searchFilterHistory,
        };
        if (postData.userId == null) {
            this.supAdminReadonly = true;
        }
        if (type == 'company') {
            delete postData['userId'];
            delete postData['roleId'];
            this.supAdminReadonly = true;
        }
        console.log(postData, 'compensation history');
        this.compensationService.getAllPendingData(postData)
            .subscribe(function (res) {
            if (res.status == true) {
                console.log("Compensation history", res);
                _this.compensationDataHistory = res.data;
                _this.countHistory = res.total_count;
                _this.compensationDataHistory = _this.compensationDataHistory.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                });
            }
            console.log(_this.compensationDataHistory);
            (function (err) {
                console.log(err);
            });
        });
    };
    /* Description: manager requests pending list search filter
    author : vipin reddy */
    CompensationComponent.prototype.searchfilterChange = function () {
        console.log(this.searchFilter);
        this.getCompensationPendingData();
    };
    /* Description: get manager requests pending list
    author : vipin reddy */
    CompensationComponent.prototype.getCompensationPendingData = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'));
        var type = JSON.parse(localStorage.getItem('type'));
        var siteAccessRoleId = JSON.parse(localStorage.getItem('siteAccessRoleId'));
        var postData = {
            companyId: cId,
            userId: eId,
            request_status: 'Pending',
            changeRequestType: 'Salary',
            per_page: this.Pendingperpage,
            page_no: this.PendingPageno,
            roleId: siteAccessRoleId,
            search_query: this.searchFilter,
        };
        if (postData.userId == null) {
            this.supAdminReadonly = true;
        }
        if (type == 'company') {
            delete postData['userId'];
            delete postData['roleId'];
            this.supAdminReadonly = true;
        }
        this.compensationService.getAllPendingData(postData)
            .subscribe(function (res) {
            if (res.status == true) {
                console.log("Compensation pending", res);
                _this.compensationData = res.data;
                _this.count = res.total_count;
                _this.compensationData = _this.compensationData.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    o.approvalStatus = false;
                    return o;
                });
            }
            console.log(_this.compensationData);
            (function (err) {
                console.log(err);
            });
        });
    };
    ;
    /* Description: manager requests approve or reject request
    author : vipin reddy */
    CompensationComponent.prototype.statusChange = function (data, status) {
        console.log(data);
        if (status == 'approve') {
            this.statusReject = false;
            this.statusApprove = true;
        }
        if (status == 'reject') {
            this.statusApprove = false;
            this.statusReject = true;
        }
        console.log(this.statusReject, this.statusApprove);
    };
    /* Description: manager single request open in popup and approve or reject
    author : vipin reddy */
    CompensationComponent.prototype.compensationEdit = function () {
        this.approveRejectHide = false;
        this.editCompensation();
    };
    /* Description: manager single request open in popup for view only
    author : vipin reddy */
    CompensationComponent.prototype.showCompensationHistory = function (mId) {
        this.selectedId = mId;
        jQuery("#myModal2").modal('show');
        this.approveRejectHide = true;
        this.editCompensation();
    };
    /* Description: manager get single request
    author : vipin reddy */
    CompensationComponent.prototype.editCompensation = function () {
        var _this = this;
        this.statusApprove = false;
        this.statusReject = false;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var mId = this.selectedId;
        this.submissionCompleteDate = "";
        this.compensationService.editCompensationPending(cId, mId)
            .subscribe(function (res) {
            console.log("Edit Compensation pending ", res);
            _this.compensationRes = res.data;
            if (_this.compensationRes.approvalPriority[3].request_status == 'Approved') {
                _this.submissionCompleteDate = _this.compensationRes.approvalPriority[3].approval_date;
            }
            console.log(_this.submissionCompleteDate);
            _this.compensationRes.approvalPriority = res.data.approvalPriority;
            // this.priorityUsers[(res.data[0].reportsTo.priority) - 1]
            // let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
            // console.log(indexManager, this.managerData);
            // let userManager = this.managerData[indexManager]
            // postdata.job = Object.assign(this.jobData, finalObj)
            for (var i = 0; i < _this.compensationRes.approvalPriority.length; i++) {
                if (_this.compensationRes.approvalPriority[i].personId) {
                    _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = { name: (_this.compensationRes.approvalPriority[i].personId.personal.name.preferredName) };
                    if (_this.compensationRes.approvalPriority[i].approval_date) {
                        _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(_this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: (_this.compensationRes.approvalPriority[i].approval_date) });
                    }
                }
                else {
                    if (_this.compensationRes.approvalPriority[i].roleId) {
                        _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = { name: (_this.compensationRes.approvalPriority[i].roleId.name) };
                        if (_this.compensationRes.approvalPriority[i].approval_date) {
                            _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(_this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: (_this.compensationRes.approvalPriority[i].approval_date) });
                        }
                    }
                    else {
                        _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = { name: ('deleted role') };
                        if (_this.compensationRes.approvalPriority[i].approval_date) {
                            _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(_this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: '--' });
                        }
                    }
                }
            }
        });
        // this.approveRejectHide = false;
        console.log(this.timePeriods);
    };
    /* Description: wrok flow approval priority setting
  author : vipin reddy */
    CompensationComponent.prototype.approvalPriority = function (id, approvalStatus, name, mId) {
        var _this = this;
        console.log(id, approvalStatus, name, mId);
        this.compensationData.forEach(function (element) {
            if (id == element._id) {
                if (approvalStatus == true)
                    _this.popupName = name;
                _this.tempId = id;
                _this.selectedId = mId;
                // element.approvalStatus = true;
                jQuery("#myModal3").modal('show');
            }
            else {
                // element.approvalStatus = false;
            }
        });
    };
    /* Description: manager request approve or reject request sending
  author : vipin reddy */
    CompensationComponent.prototype.approvalsending = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'));
        var postData = {
            companyId: cId,
            manager_request_id: this.selectedId,
            userId: eId,
            request_status: ''
        };
        if (this.statusReject == true) {
            postData.request_status = 'Rejected';
        }
        if (this.statusApprove == true) {
            postData.request_status = 'Approved';
        }
        this.compensationService.approveOrRejectRequest(postData)
            .subscribe(function (res) {
            console.log("Edit Compensation request ", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Request Updated", res.message, 'success');
                _this.getCompensationHistory();
                _this.getCompensationPendingData();
                jQuery("#myModal2").modal('hide');
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: manager request approving using toogle
     author : vipin reddy */
    CompensationComponent.prototype.taskApproved = function () {
        var _this = this;
        this.compensationData.forEach(function (element) {
            console.log(_this.tempId, element._id);
            if (_this.tempId == element._id) {
                var cId = JSON.parse(localStorage.getItem('companyId'));
                var eId = JSON.parse(localStorage.getItem('employeeId'));
                var postData = {
                    companyId: cId,
                    manager_request_id: _this.selectedId,
                    userId: eId,
                    request_status: 'Approved'
                };
                console.log(postData);
                _this.compensationService.approveOrRejectRequest(postData)
                    .subscribe(function (res) {
                    console.log("Edit Compensation request ", res);
                    if (res.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Request Updated", res.message, 'success');
                        _this.getCompensationHistory();
                        _this.getCompensationPendingData();
                        jQuery("#myModal2").modal('hide');
                    }
                }, function (err) {
                    console.log(err);
                });
                element.approvalStatus = true;
            }
        });
        jQuery("#myModal3").modal('hide');
    };
    /* Description: mosal close for toggle selection
   author : vipin reddy */
    CompensationComponent.prototype.modalClose = function () {
        var _this = this;
        this.compensationData.forEach(function (element) {
            if (_this.tempId == element._id) {
                element.approvalStatus = false;
            }
        });
        jQuery("#myModal3").modal('hide');
    };
    CompensationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-compensation',
            template: __webpack_require__(/*! ./compensation.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.html"),
            styles: [__webpack_require__(/*! ./compensation.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__["ManagerApprovalService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], CompensationComponent);
    return CompensationComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.css":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.css ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".compensation-block{ margin: 0 auto; float: none; padding: 0;}\n\n.compensation { padding:30px 0 0;}\n\n.compensation .panel-default>.panel-heading { padding: 0 0 20px; background-color:transparent !important; border: none;}\n\n.compensation .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n\n.compensation .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n\n.compensation .panel-title>.small, .compensation .panel-title>.small>a, .compensation .panel-title>a, .compensation .panel-title>small, .compensation .panel-title>small>a { text-decoration:none;}\n\n.compensation .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n\n.compensation .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n\n.compensation .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.compensation .panel-group .panel-heading+.panel-collapse>.list-group, .compensation .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.compensation .panel-heading .accordion-toggle:after { margin:7px 15px 0; font-size: 13px; line-height: 13px;}\n\n.compensation .panel-body { padding: 0 0 5px;}\n\n.compensation .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.compensation .panel-default { border-color:transparent;}\n\n.compensation .panel-group .panel+.panel { margin-top: 20px;}\n\n.compensation-main { display: block;}\n\n.compensation-changes {display: block;}\n\n.compensation-changes .table{ background:#fff;}\n\n.compensation-changes .table>tbody>tr>th, .compensation-changes .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n\n.compensation-changes .table>tbody>tr>td, .compensation-changes .table>tfoot>tr>td, .compensation-changes .table>tfoot>tr>th, \n.compensation-changes .table>thead>tr>td {color: #484747;padding:15px;font-size:15px;}\n\n.compensation-changes button.mat-menu-item a { color:#ccc;}\n\n.compensation-changes button.mat-menu-item a:hover { color:#fff; background: #099247;}\n\n.compensation-changes .table>tbody>tr>th a, .compensation-changes .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n\n.compensation-changes .table>tbody>tr>th a .fa, .compensation-changes .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.compensation-changes button { border: none; outline: none; background:transparent;}\n\n.top-history{ margin: 10px 0 20px;}\n\n.top-history ul{ display: block;}\n\n.top-history ul li{ padding: 0 5px;}\n\n.top-history ul li.select-date{ margin: 25px 0 0;}\n\n.top-history ul li label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 7px 15px 0;}\n\n.top-history ul li .form-control { border: none;box-shadow: none;padding: 12px 12px;height: auto;\nwidth:50%; font-size: 15px;background: #fff; display: inline-block;}\n\n.example-form { position: relative;}\n\n.top-history ul li small { position: absolute; top:4px; right:5px; cursor: pointer;}\n\n.top-history ul li small .material-icons { color: #8e8e8e; font-size: 20px; font-weight: normal;}\n\n.top-history ul li .range{ margin: 0 5px;}\n\n.top-history ul li .range label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 0 7px; display: block;}\n\n.top-history ul li .range .date { height: auto !important; line-height:inherit !important;background:#fff; width: 165px;}\n\n.top-history ul li .range .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.top-history ul li .range .date span { border: none !important; padding: 0 !important;}\n\n.top-history ul li .range .date .form-control { width:72%; height: auto; padding: 12px 0 12px 10px;}\n\n.top-history ul li .range .date .form-control:focus { box-shadow: none; border: none;}\n\n.top-history ul li .range .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.history-search { margin:28px 0 0;}\n\n.history-search .form-group{ margin-bottom: 0;}\n\n.mdl-history { display: block;}\n\n.mdl-history ul{ margin: 0 0 10px;}\n\n.mdl-history ul li{ padding: 0 15px; display: inline-block;}\n\n.mdl-history ul li a{color: #484747; font-weight: normal; font-size: 14px; cursor: pointer;}\n\n.mdl-history ul li.active a { border-bottom:#008f3d 2px solid;}\n\n.mdl-history ul li a span, .mdl-history ul li a small { vertical-align:text-bottom; padding: 0 0 0 5px;}\n\n.mdl-history ul li a span .fa{ color:#ccc; font-size:8px;}\n\n.mdl-history ul li a small .fa{ color:#ccc; font-size:8px;}\n\n.mdl-active{    \n    border-bottom: 3px solid #439348;\n    padding: 0 0 9px 0;}\n\n.mdl-active .fa{\n        color: #008f3d !important;\n    }\n\n.mdl-activered{    \n        border-bottom: 3px solid #f00;\n        padding: 0 0 9px 0;}\n\n.mdl-activered .fa{\n            color: #f00 !important;\n        }\n\n.mdl-history .table{ background:#fff;}\n\n.mdl-history .table>tbody>tr>th, .mdl-history .table>thead>tr>th {padding: 15px; color: #484747; font-weight: normal; \nfont-size:16px;vertical-align: middle; position: relative; background: #edf7ff;}\n\n.mdl-history .table>tbody>tr>td, .mdl-history .table>tfoot>tr>td, .mdl-history .table>tfoot>tr>th, \n.mdl-history .table>thead>tr>td {color: #484747;padding:15px;font-size:15px; vertical-align: middle; border-bottom:#ccc 1px solid;}\n\n.mdl-history .table>tbody>tr>th a, .mdl-history .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n\n.mdl-history .table>tbody>tr>th a .fa, .mdl-history .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.managers { display: block;}\n\n.managers span { vertical-align: middle; display: inline-block;}\n\n.manager-right { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;}\n\n.manager-right h4 { font-size: 15px; margin: 0 0 5px;}\n\n.manager-right h5 {  color: #ccc;margin: 0;}\n\n.mdl-history .table>tbody>tr>td small { font-size: 15px; vertical-align: middle;}\n\n.mdl-history .table>tbody>tr>td small .fa{ color:#008f3d; font-size: 10px; vertical-align: middle;}\n\n.date1 { height: auto !important; line-height:inherit !important;background:#f8f8f8; width: 160px;}\n\n.date1 button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.date1 .form-control { width:65% !important; height: auto; padding: 12px 0 12px 10px; display: inline-block;}\n\n.date1 .form-control:focus { box-shadow: none; border: none;}\n\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.date1 span { display: inline-block; vertical-align: middle; cursor: pointer;border-left: #bdbdbd 1px solid; padding: 0 0 0 15px !important;}\n\n.date1 span .fa { color: #008f3d;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.approval .modal{\n    top:50%;\n}\n\n.approval span h4{\n    /* font-weight: 600; */\n    padding: 10px 0 10px 5px;  \n}\n\n.location-request .form-control{\n    width: 94% !important;\n}\n\n.approval .modal-dialog{\n    width: 900px;\n}\n\n.modal-approval{\n    padding: 50px;\n    text-align: center;\n    font-size: 16px;\n    line-height: 30px;\n}\n\n.modal-approval p {\n    margin: 20px 0 0 0;\n}\n\n.footer{\n    margin: 20px auto 0;\n    float: none;\n}\n\n.footer .btn.save {border-radius: 20px; height:40px; line-height:38px;font-size: 16px; padding: 0 24px;background: #008f3d; color:#fff; margin: 0 20px 0 0;}\n\n.footer .btn.cancel { color:#e44a49; background:transparent; padding:8px 0 0 30px;font-size: 16px;}\n\n.text-area{\n    resize: none;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.html":
/*!******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"compensation-block col-md-11\">\n\n  <div class=\"compensation\">\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n              Job Changes - Pending\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"compensation-main\">\n\n              <div class=\"searching\">\n                <div class=\"form-group col-md-4 pull-right\">\n\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Search Employee by Name, Job #, Job Title etc...\"\n                    [(ngModel)]=\"searchFilter\" (ngModelChange)=\"searchfilterChange()\">\n                  <span><i class=\"material-icons\">search</i></span>\n\n                </div>\n                <div class=\"clearfix\"></div>\n              </div>\n\n              <div class=\"compensation-changes\">\n\n                <table class=\"table\">\n                  <thead>\n                    <tr>\n                      <th>\n                        <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                      </th>\n                      <th>Employee Name</th>\n                      <th>Submission Request Date</th>\n                      <th>Department</th>\n\n                      <th>Approval Status &nbsp;\n                        <span>\n                          <!-- <mat-slide-toggle></mat-slide-toggle> -->\n                        </span>\n                      </th>\n                      <th>\n                        <button [disabled]=\"supAdminReadonly\" mat-icon-button [matMenuTriggerFor]=\"menu1\"\n                          aria-label=\"Example icon-button with a menu\">\n                          <mat-icon>more_vert</mat-icon>\n                        </button>\n                        <mat-menu #menu1=\"matMenu\">\n                          <button mat-menu-item>\n                            <a data-toggle=\"modal\" data-target=\"#myModal2\" (click)=\"compensationEdit()\">Edit Request</a>\n                          </button>\n                        </mat-menu>\n                      </th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <!-- <tr *ngFor=\"let data of compensationData\"> -->\n                    <tr *ngFor=\"let data of compensationData\">\n                      <!-- <td>\n                        <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isChecked\" (ngModelChange)=\"empSelect()\"></mat-checkbox>\n                      </td> -->\n                      <td>\n                        <mat-checkbox [(ngModel)]=\"data.isChecked\"\n                          (ngModelChange)=\"selectRequest(data.isChecked,data._id,data.manager_request_id)\"\n                          class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                      </td>\n                      <td>{{data.impactedEmployee.personal.name.preferredName}}</td>\n                      <td>{{data.manager_request.requestedDate | date}}</td>\n                      <td>{{data.impactedEmployee.job.department}}</td>\n\n                      <td *ngIf=\"data.next_priority\">\n                        <small *ngIf=\"data.next_priority.personal\">\n\n                          <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                            (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,data.next_priority.personal.name.preferredName,data.manager_request_id)\">\n                          </mat-slide-toggle>\n                        </small>\n                        <small *ngIf=\"data.next_priority.name\">\n\n                          <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                            (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,data.next_priority.name,data.manager_request_id)\">\n                          </mat-slide-toggle>\n                        </small>\n\n                      </td>\n                      <td *ngIf=\"!data.next_priority\">\n                        <!-- <small *ngIf=\"data.next_priority\">\n  \n                            <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                              (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,data.next_priority.personal.name.preferredName,data.manager_request_id)\">\n                            </mat-slide-toggle>\n                          </small> -->\n\n                        <small *ngIf=\"!data.next_priority\">\n\n                          <mat-slide-toggle [disabled]=\"supAdminReadonly\" [(ngModel)]=\"data.approvalStatus\"\n                            (ngModelChange)=\"approvalPriority(data._id,data.approvalStatus,'----',data.manager_request_id)\">\n                          </mat-slide-toggle>\n                        </small>\n                      </td>\n                      <td></td>\n                    </tr>\n                    <tr *ngIf=\"compensationData.length == 0\">\n                      <td></td>\n                      <td></td>\n                      <td class=\"text-center\">No records found</td>\n                    </tr>\n\n                  </tbody>\n                </table>\n                <mat-paginator [length]=\"count\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n                  (page)=\"pageEvents($event)\"></mat-paginator>\n\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n\n    <div class=\"panel-group\" id=\"accordion1\">\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwo\">\n              Job Changes - History\n            </a>\n          </h4>\n        </div>\n        <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"compensation-main\">\n\n\n              <div class=\"top-history\">\n\n                <ul>\n\n                  <!-- <li class=\"col-md-3 select-date\">\n                    <label>Select Date Range</label>\n                    <mat-select placeholder=\"Select Range\" class=\"form-control\">\n                      <mat-option value=\"\">Last Week</mat-option>\n                      <mat-option value=\"\">Last Month</mat-option>\n                    </mat-select>\n                  </li> -->\n\n                  <li class=\"col-md-5 col-md-7\">\n\n                    <div class=\"range pull-left\">\n                      <label>Begin Date Range(T1)</label>\n                      <div class=\"date\">\n                        <input matInput [(ngModel)]=\"beginDate\" [max]=\"endDate\" (ngModelChange)=\"beginDateChange()\"\n                          [matDatepicker]=\"picker10\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                        <mat-datepicker #picker10></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker10.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </div>\n\n                    <div class=\"range pull-left\">\n                      <label>End Date Range(T2)</label>\n                      <div class=\"date\">\n                        <input matInput [(ngModel)]=\"endDate\" [min]=\"endDate\" (ngModelChange)=\"endDateChange()\"\n                          [matDatepicker]=\"picker11\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                        <mat-datepicker #picker11></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker11.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </div>\n\n                    <div class=\"clearfix\"></div>\n                  </li>\n\n                  <li class=\"col-md-2 select-date location-request\">\n\n                    <!-- <mat-select class=\"form-control\" placeholder=\"location\" class=\"form-control\">\n                      <mat-option value=\"\">Hyderabad</mat-option>\n                      <mat-option value=\"\">New delhi</mat-option>\n                    </mat-select> -->\n\n\n                  </li>\n                  <li class=\"col-md-3 pull-right\">\n\n                    <div class=\"searching history-search\">\n                      <div class=\"form-group col-md-12\">\n\n                        <input type=\"text\" [(ngModel)]=\"searchFilterHistory.search\"\n                          (ngModelChange)=\"searchFilterHistoryChange()\" class=\"form-control\"\n                          placeholder=\"Search Employee by Name, Job\">\n                        <span><i class=\"material-icons\">search</i></span>\n\n                      </div>\n\n                    </div>\n\n                  </li>\n\n                </ul>\n                <div class=\"clearfix\"></div>\n\n              </div>\n\n              <div class=\"mdl-history\">\n\n                <ul>\n                  <li><a [ngClass]=\"{'mdl-active' : requestStatus == '' }\" (click)=\"requestType('')\">Show All</a></li>\n                  <li><a [ngClass]=\"{'mdl-active' : requestStatus == 'Approved' }\"\n                      (click)=\"requestType('Approved')\">Approved <span><i class=\"fa fa-circle\"\n                          aria-hidden=\"true\"></i></span></a></li>\n                  <li><a [ngClass]=\"{'mdl-activered' : requestStatus == 'Rejected' }\"\n                      (click)=\"requestType('Rejected')\">Rejected <small><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></small></a></li>\n                </ul>\n                <div class=\"clearfix\"></div>\n\n\n                <table class=\"table\">\n                  <thead>\n                    <tr>\n\n                      <th>Employee Name</th>\n                      <th>Submission Request Date</th>\n                      <th>Submission Complete Date</th>\n                      <th>Department</th>\n                      <th>Request Status</th>\n\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let data of compensationDataHistory\"\n                      (click)=\"showCompensationHistory(data.manager_request_id)\">\n                      <td>\n                        <div class=\"managers\">\n                          <!-- <span><img src=\"../../../../assets/images/Manager Self Service/employee.png\"></span> -->\n                          <div class=\"manager-right\">\n                            <h4>{{data.impactedEmployee.personal.name.preferredName}}</h4>\n                            <!-- <h5>Manager</h5> -->\n                          </div>\n                        </div>\n                      </td>\n                      <td>{{data.manager_request.requestedDate | date}}</td>\n                      <td *ngIf=\"data.manager_request.completedate\"> {{data.manager_request.completedate | date }}</td>\n                      <td *ngIf=\"!data.manager_request.completedate\"> -- </td>\n                      <!-- <td [ngIf] = \"!data.manager_request.completedate\" >  -- </td> -->\n\n                      <td>{{data.impactedEmployee.job.department}}</td>\n                      <td *ngIf=\"data.request_status == 'Approved'\"><small> <i class=\"fa fa-circle\"\n                            aria-hidden=\"true\"></i> &nbsp;\n                          {{data.request_status}}</small>\n                      </td>\n                      <td *ngIf=\"data.request_status == 'Rejected'\"><small> <i class=\"fa fa-circle\" aria-hidden=\"true\"\n                            style=\"color:#f00;\"></i> &nbsp;\n                          {{data.request_status}}</small>\n                      </td>\n                      <td *ngIf=\"data.request_status == 'Pending'\"><small> <i class=\"fa fa-circle\" aria-hidden=\"true\"\n                            style=\"color:#f00;\"></i> &nbsp;\n                          {{data.request_status}}</small>\n                      </td>\n                    </tr>\n                    <tr *ngIf=\"compensationDataHistory.length == 0\">\n                      <td></td>\n                      <td></td>\n                      <td>No records found</td>\n                      <td></td>\n                      <td></td>\n                    </tr>\n                  </tbody>\n                </table>\n\n                <mat-paginator [length]=\"countHistory\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n                  (page)=\"pageEventsHistory($event)\"></mat-paginator>\n\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n    <div class=\"approval\">\n      <div class=\"modal fade\" id=\"myModal3\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n\n          <div class=\"modal-content\">\n            <div class=\"modal-body modal-approval\">\n              <!-- <em>\n                    <i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i></em> -->\n              <span><img src=\"../../../../assets/images/Manager Self Service/error.png\"></span>\n              <p *ngIf=\"popupName != '----'\"> Are you sure you want to approve?<br>\n                The next approval priority is <span style=\"text-decoration:underline\">{{popupName}}</span></p>\n              <p *ngIf=\"popupName == '----'\">\n                Are you sure you want to approve?<br>\n                You are the last person in the approval priority\n              </p>\n              <div class=\"footer col-md-4\">\n                <button class=\"btn save\" type=\"submit\" (click)=\"taskApproved()\">Proceed</button>\n                <button class=\"btn pull-left cancel\" (click)=\"modalClose()\">Cancel</button>\n                <div class=\"clearfix\"></div>\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</div>\n\n\n\n<!-- Compansation Change Popup-->\n\n<div class=\"job-change-popup\">\n\n  <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Manager Approval - Pending Request</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"job-main\">\n\n            <div class=\"job-change\">\n\n              <div class=\"job-change-in col-md-11\">\n\n                <ul>\n\n                  <li>\n                    <label>Select Employee Name</label>\n                    <!-- [(ngModel)]= \"compensationRes.\" -->\n                    <input class=\"form-control\" placeholder=\"Employee Name\"\n                      [(ngModel)]=\"compensationRes.requestedFor.personal.name.preferredName\" readonly>\n                  </li>\n                  <li class=\"col-md-6\">\n                    <label>Current Status</label>\n                    <div class=\"job-status\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>Filed Name</th>\n                            <th>Current Value</th>\n                            <th>Effective Date</th>\n                          </tr>\n                        </thead>\n\n                        <tbody>\n\n                          <tr>\n                            <td>Job Title</td>\n                            <td>\n                              <input class=\"form-control\" [(ngModel)]=\"compensationRes.oldStatus.jobTitle.old_value\"\n                                placeholder=\"Current Value\" readonly>\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.jobTitle.effective_date\"\n                                  [matDatepicker]=\"picker71\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker71 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker71.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                          <tr>\n                            <td>Department</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.oldStatus.department.old_value\" class=\"form-control\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.department.effective_date\"\n                                  [matDatepicker]=\"picker81\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker81 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker81.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n\n                          <tr>\n                            <td>Location</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.oldStatus.location.old_value\" class=\"form-control\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.oldStatus.location.effective_date\"\n                                  [matDatepicker]=\"picker91\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker91 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker91.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n\n\n                        </tbody>\n\n                      </table>\n\n                    </div>\n                  </li>\n                  <li class=\"col-md-6\">\n                    <label>New Status</label>\n                    <div class=\"job-status new\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>Filed Name</th>\n                            <th>New Value</th>\n                            <th>Effective Date</th>\n                          </tr>\n                        </thead>\n                        <tbody>\n\n                          <tr>\n                            <td>Job Title</td>\n                            <td>\n                              <input class=\"form-control\" [(ngModel)]=\"compensationRes.newStatus.jobTitle.new_value\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.jobTitle.effective_date\"\n                                  [matDatepicker]=\"picker72\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker72 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker72.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n                          <tr>\n                            <td>Department</td>\n                            <td>\n                              <input class=\"form-control\" [(ngModel)]=\"compensationRes.newStatus.department.new_value\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.department.effective_date\"\n                                  [matDatepicker]=\"picker82\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker82 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker82.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n\n                          <tr>\n                            <td>Location</td>\n                            <td>\n                              <input [(ngModel)]=\"compensationRes.newStatus.location.new_value\" class=\"form-control\"\n                                placeholder=\"Current Value\" [disabled]=true>\n\n                            </td>\n                            <td>\n                              <div class=\"date\">\n                                <input matInput [(ngModel)]=\"compensationRes.newStatus.location.effective_date\"\n                                  [matDatepicker]=\"picker92\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                                  [disabled]=true>\n                                <mat-datepicker #picker92 [disabled]=true></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker92.open()\">\n                                  <span>\n                                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                  </span>\n                                </button>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </td>\n                          </tr>\n\n\n\n                        </tbody>\n                      </table>\n\n\n                    </div>\n                  </li>\n                  <div class=\"clearfix\"></div>\n\n\n                  <li>\n                    <label>Change Reason</label>\n                    <input [(ngModel)]=\"compensationRes.changeReason\" class=\"form-control\" placeholder=\"Reason\" [disabled]=true >\n\n                  </li>\n\n                  <li class=\"text-area\">\n                    <label>Note Section</label>\n                    <textarea [(ngModel)]=\"compensationRes.notes\" class=\"form-control text-area\" rows=\"5\" id=\"comment\" [disabled]=true></textarea>\n                  </li>\n\n                  <li *ngIf=\"!approveRejectHide\">\n\n                    <p class=\"pull-left col-md-2\">\n                      <mat-checkbox [(ngModel)]=\"statusApprove\" (change)=\"statusChange($event,'approve')\"\n                        class=\"zenwork-customized-checkbox\">Approve\n                      </mat-checkbox>\n                    </p>\n\n                    <small class=\"pull-left\">\n                      <mat-checkbox [(ngModel)]=\"statusReject\" (change)=\"statusChange($event,'reject')\"\n                        class=\"zenwork-customized-checkbox\">Reject\n                      </mat-checkbox>\n                    </small>\n                    <div class=\"clearfix\"></div>\n                  </li>\n\n                  <li class=\"text-area\">\n                    <label>Approval</label>\n                    <div class=\"approval\">\n                      <span class=\"pull-left\">\n                        <h4>Approval Priority</h4>\n                      </span>\n                      <span class=\"pull-right col-md-5\">\n                        <h4>Approval Date</h4>\n                      </span>\n                      <div class=\"clearfix\"></div>\n                      <div cdkDropList class=\"example-list\">\n                        <div class=\"example-box\" *ngFor=\"let timePeriod of timePeriods\">\n                          <span *ngIf=\"timePeriod.name != 'deleted role'\" class=\"pull-left\"\n                            style=\"background: #fff;padding: 0;\">\n                            {{timePeriod.name}}\n                          </span>\n                          <span *ngIf=\"timePeriod.name == 'deleted role'\" class=\"pull-left\"\n                            style=\"background: #fff;color:#f00;padding: 0;\"> {{timePeriod.name}}\n                          </span>\n                          <span *ngIf=\"timePeriod.approval_date\" class=\"pull-right col-md-5\"\n                            style=\"background: #fff;padding: 0;\"> {{timePeriod.approval_date | date}}</span>\n                          <span *ngIf=\"!timePeriod.approval_date\" class=\"pull-right col-md-5\"\n                            style=\"background: #fff;padding: 0 0 0 5px;\"> --</span>\n\n                          <div class=\"clearfix\"></div>\n\n                        </div>\n                      </div>\n\n                    </div>\n                  </li>\n\n                  <li class=\"col-md-3 submission\">\n                    <label>Submission Request Date</label>\n                    <div class=\"date1\">\n                      <input matInput [(ngModel)]=\"compensationRes.requestedDate\" [matDatepicker]=\"picker721\"\n                        placeholder=\"mm / dd / yy\" class=\"form-control\" [disabled]=true>\n\n\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n                  <li class=\"col-md-3 submission\">\n                    <label>Submission Complete Date</label>\n                    <div class=\"date1\">\n                      <input [(ngModel)]=\"submissionCompleteDate\" matInput [matDatepicker]=\"picker722\"\n                        placeholder=\"mm / dd / yy\" class=\"form-control\" [disabled]=true>\n\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n\n                </ul>\n\n              </div>\n\n            </div>\n\n            <div class=\"termination-border col-md-11\">\n\n              <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n              <button *ngIf=\"!approveRejectHide\" class=\"btn pull-right save\" (click)=\"approvalsending()\">Submit</button>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: JobComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobComponent", function() { return JobComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/manager-approval.service */ "./src/app/services/manager-approval.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobComponent = /** @class */ (function () {
    function JobComponent(compensationService, swalAlertService) {
        this.compensationService = compensationService;
        this.swalAlertService = swalAlertService;
        this.timePeriods = [];
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.options = ['One', 'Two', 'Three'];
        this.singlePerson = false;
        // MatPaginator Inputs
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [2, 5, 10, 25, 100];
        this.searchFilterHistory = {
            search: ''
        };
        this.compensationDataHistory = [];
        this.compensationData = [];
        this.statusReject = false;
        this.statusApprove = true;
        this.approveRejectHide = false;
        this.supAdminReadonly = false;
        this.compensationRes = {
            changeReason: '',
            notes: '',
            compensationRes: '',
            approvalPriority: [],
            requestedFor: {
                personal: {
                    name: {
                        preferredName: ''
                    }
                }
            },
            oldStatus: {
                jobTitle: {
                    effective_date: "",
                    old_value: ''
                },
                department: {
                    old_value: '',
                    effective_date: ""
                },
                location: {
                    old_value: '',
                    effective_date: ""
                },
            },
            newStatus: {
                jobTitle: {
                    effective_date: "",
                    old_value: ''
                },
                department: {
                    old_value: '',
                    effective_date: ""
                },
                location: {
                    old_value: '',
                    effective_date: ""
                },
            },
        };
    }
    JobComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return this.options.filter(function (option) { return option.toLowerCase().includes(filterValue); });
    };
    JobComponent.prototype.drop = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(this.timePeriods, event.previousIndex, event.currentIndex);
    };
    JobComponent.prototype.ngOnInit = function () {
        this.Pendingperpage = 5;
        this.PendingPageno = 1;
        this.HistoryPageno = 1;
        this.Historyperpage = 5;
        this.requestStatus = '';
        this.searchFilter = '';
        this.beginDate = '';
        this.endDate = '',
            this.serchingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                search: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
            });
        this.getCompensationPendingData();
        this.getCompensationHistory();
    };
    //Page Event
    JobComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getCompensationPendingData();
    };
    JobComponent.prototype.pageEventsHistory = function (event) {
        console.log(event);
        this.HistoryPageno = event.pageIndex + 1;
        this.Historyperpage = event.pageSize;
        this.getCompensationHistory();
    };
    /* Description: get manager requests list date filter
    author : vipin reddy */
    JobComponent.prototype.beginDateChange = function () {
        console.log(this.beginDate);
        this.beginDate = this.beginDate.toISOString();
        this.getCompensationHistory();
    };
    /* Description: get manager requests list date filter
    author : vipin reddy */
    JobComponent.prototype.endDateChange = function () {
        console.log(this.endDate);
        this.endDate = this.endDate.toISOString();
        this.getCompensationHistory();
    };
    /* Description: get manager requests pending list
    author : vipin reddy */
    JobComponent.prototype.getCompensationPendingData = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'));
        var type = JSON.parse(localStorage.getItem('type'));
        var siteAccessRoleId = JSON.parse(localStorage.getItem('siteAccessRoleId'));
        var postData = {
            companyId: cId,
            userId: eId,
            request_status: 'Pending',
            changeRequestType: 'Job',
            per_page: this.Pendingperpage,
            page_no: this.PendingPageno,
            roleId: siteAccessRoleId,
            search_query: this.searchFilter,
        };
        if (postData.userId == null) {
            this.supAdminReadonly = true;
        }
        if (type == 'company') {
            delete postData['userId'];
            delete postData['roleId'];
            this.supAdminReadonly = true;
        }
        console.log(postData);
        this.compensationService.getAllPendingData(postData)
            .subscribe(function (res) {
            if (res.status == true) {
                console.log("Job pending", res);
                _this.compensationData = res.data;
                _this.count = res.total_count;
                _this.compensationData = _this.compensationData.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    o.approvalStatus = false;
                    return o;
                });
            }
            console.log(_this.compensationData);
            (function (err) {
                console.log(err);
            });
        });
    };
    ;
    /* Description: get manager requests list s
  author : vipin reddy */
    JobComponent.prototype.getCompensationHistory = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'));
        var type = JSON.parse(localStorage.getItem('type'));
        var siteAccessRoleId = JSON.parse(localStorage.getItem('siteAccessRoleId'));
        var postData = {
            companyId: cId,
            userId: eId,
            request_status: this.requestStatus,
            changeRequestType: 'Job',
            per_page: this.Historyperpage,
            page_no: this.HistoryPageno,
            from_date: this.beginDate,
            to_date: this.endDate,
            roleId: siteAccessRoleId,
            search_query: this.searchFilterHistory.search,
        };
        if (postData.userId == null) {
            this.supAdminReadonly = true;
        }
        if (type == 'company') {
            delete postData['userId'];
            delete postData['roleId'];
            this.supAdminReadonly = true;
        }
        this.compensationService.getAllPendingData(postData)
            .subscribe(function (res) {
            if (res.status == true) {
                console.log("Job history", res);
                _this.compensationDataHistory = res.data;
                _this.countHistory = res.total_count;
                _this.compensationDataHistory = _this.compensationDataHistory.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                });
            }
            console.log(_this.compensationDataHistory);
            (function (err) {
                console.log(err);
            });
        });
    };
    /* Description: manager requests Showall,approal and pending history list active tabs
    author : vipin reddy */
    JobComponent.prototype.requestType = function (data) {
        this.requestStatus = data;
        this.getCompensationHistory();
    };
    /* Description: manager requests list single request showing in popup
   author : vipin reddy */
    JobComponent.prototype.selectRequest = function (isChecked, id, mId) {
        var _this = this;
        console.log(isChecked, id, mId);
        this.compensationData.forEach(function (element) {
            if (id == element._id) {
                element.isChecked = true;
                _this.selectedId = mId;
            }
            else {
                element.isChecked = false;
            }
            console.log(_this.selectedId);
        });
    };
    /* Description: manager requests pending list search filter
    author : vipin reddy */
    JobComponent.prototype.searchfilterChange = function () {
        console.log(this.searchFilter);
        this.getCompensationPendingData();
    };
    /* Description: manager requests list search filter
  author : vipin reddy */
    JobComponent.prototype.searchFilterHistoryChange = function () {
        console.log(this.searchFilterHistory.search);
        this.getCompensationHistory();
    };
    /* Description: manager requests approve or reject request
     author : vipin reddy */
    JobComponent.prototype.statusChange = function (data, status) {
        console.log(data);
        if (status == 'approve') {
            this.statusReject = false;
            this.statusApprove = true;
        }
        if (status == 'reject') {
            this.statusApprove = false;
            this.statusReject = true;
        }
        console.log(this.statusReject, this.statusApprove);
    };
    /* Description: manager single request open in popup and approve or reject
   author : vipin reddy */
    JobComponent.prototype.compensationEdit = function () {
        this.approveRejectHide = false;
        this.editCompensation();
    };
    /* Description: manager single request open in popup for view only
     author : vipin reddy */
    JobComponent.prototype.showCompensationHistory = function (mId) {
        this.selectedId = mId;
        jQuery("#myModal2").modal('show');
        this.approveRejectHide = true;
        this.editCompensation();
    };
    /* Description: manager get single request
     author : vipin reddy */
    JobComponent.prototype.editCompensation = function () {
        var _this = this;
        this.statusApprove = false;
        this.statusReject = false;
        this.submissionCompleteDate = "";
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var mId = this.selectedId;
        this.compensationService.editCompensationPending(cId, mId)
            .subscribe(function (res) {
            console.log("Edit Compensation pending ", res);
            _this.compensationRes = res.data;
            if (_this.compensationRes.approvalPriority[3].request_status == 'Approved') {
                _this.submissionCompleteDate = _this.compensationRes.approvalPriority[3].approval_date;
            }
            console.log(_this.submissionCompleteDate);
            // this.timePeriods
            _this.compensationRes.approvalPriority = res.data.approvalPriority;
            // this.priorityUsers[(res.data[0].reportsTo.priority) - 1]
            // let indexManager = this.managerData.findIndex(userData => userData._id === res.data[0].reportsTo.levelId)
            // console.log(indexManager, this.managerData);
            // let userManager = this.managerData[indexManager]
            // postdata.job = Object.assign(this.jobData, finalObj)
            for (var i = 0; i < _this.compensationRes.approvalPriority.length; i++) {
                if (_this.compensationRes.approvalPriority[i].personId) {
                    _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = { name: (_this.compensationRes.approvalPriority[i].personId.personal.name.preferredName) };
                    if (_this.compensationRes.approvalPriority[i].approval_date) {
                        _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(_this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: (_this.compensationRes.approvalPriority[i].approval_date) });
                    }
                }
                else {
                    if (_this.compensationRes.approvalPriority[i].roleId) {
                        console.log(_this.compensationRes.approvalPriority[i].roleId);
                        _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = { name: (_this.compensationRes.approvalPriority[i].roleId.name) };
                        if (_this.compensationRes.approvalPriority[i].approval_date) {
                            _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(_this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: (_this.compensationRes.approvalPriority[i].approval_date) });
                        }
                    }
                    else {
                        _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = { name: ('deleted role') };
                        if (_this.compensationRes.approvalPriority[i].approval_date) {
                            _this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)] = Object.assign(_this.timePeriods[(_this.compensationRes.approvalPriority[i].priority - 1)], { approval_date: '--' });
                        }
                    }
                }
            }
        });
        // this.approveRejectHide = false;
        console.log(this.timePeriods);
    };
    /* Description: wrok flow approval priority setting
    author : vipin reddy */
    JobComponent.prototype.approvalPriority = function (id, approvalStatus, name, mId) {
        var _this = this;
        console.log(id, approvalStatus, name);
        this.compensationData.forEach(function (element) {
            if (id == element._id) {
                if (approvalStatus == true)
                    _this.popupName = name;
                _this.tempId = id;
                _this.selectedId = mId;
                // element.approvalStatus = true;
                jQuery("#myModal3").modal('show');
            }
            else {
                // element.approvalStatus = false;
            }
        });
    };
    /* Description: manager request approve or reject request sending
    author : vipin reddy */
    JobComponent.prototype.approvalsending = function () {
        var _this = this;
        var cId = JSON.parse(localStorage.getItem('companyId'));
        var eId = JSON.parse(localStorage.getItem('employeeId'));
        var postData = {
            companyId: cId,
            manager_request_id: this.selectedId,
            userId: eId,
            request_status: ''
        };
        if (this.statusReject == true) {
            postData.request_status = 'Rejected';
        }
        if (this.statusApprove == true) {
            postData.request_status = 'Approved';
        }
        console.log(postData);
        this.compensationService.approveOrRejectRequest(postData)
            .subscribe(function (res) {
            console.log("Edit Compensation request ", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Request Updated", res.message, 'success');
                _this.getCompensationHistory();
                _this.getCompensationPendingData();
                jQuery("#myModal2").modal('hide');
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: manager request approving using toogle
     author : vipin reddy */
    JobComponent.prototype.taskApproved = function () {
        var _this = this;
        this.compensationData.forEach(function (element) {
            console.log(_this.tempId, element._id);
            if (_this.tempId == element._id) {
                var cId = JSON.parse(localStorage.getItem('companyId'));
                var eId = JSON.parse(localStorage.getItem('employeeId'));
                var postData = {
                    companyId: cId,
                    manager_request_id: _this.selectedId,
                    userId: eId,
                    request_status: 'Approved'
                };
                console.log(postData);
                _this.compensationService.approveOrRejectRequest(postData)
                    .subscribe(function (res) {
                    console.log("Edit Compensation request ", res);
                    if (res.status == true) {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Request Updated", res.message, 'success');
                        _this.getCompensationHistory();
                        _this.getCompensationPendingData();
                        jQuery("#myModal2").modal('hide');
                    }
                }, function (err) {
                    console.log(err);
                });
                element.approvalStatus = true;
            }
        });
        jQuery("#myModal3").modal('hide');
    };
    /* Description: mosal close for toggle selection
   author : vipin reddy */
    JobComponent.prototype.modalClose = function () {
        var _this = this;
        this.compensationData.forEach(function (element) {
            if (_this.tempId == element._id) {
                element.approvalStatus = false;
            }
        });
        jQuery("#myModal3").modal('hide');
        this.approveRejectHide = false;
    };
    JobComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-job',
            template: __webpack_require__(/*! ./job.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.html"),
            styles: [__webpack_require__(/*! ./job.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__["ManagerApprovalService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], JobComponent);
    return JobComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.css":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".manager-approval { margin: 0 auto; float: none;padding: 0;}\n\n.approval-tabs {display: block;padding:20px 0 0;}\n\n.approval-tabs .tab-content { margin:40px 0 0;}\n\n.approval-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus\n{ border: none !important;background:#5b5e5c; color:#fff !important;border-radius: 4px !important;}\n\n.approval-tabs .nav-tabs {border-bottom:#ccc 1px solid ; padding: 0 0 25px;}\n\n.approval-tabs .nav-tabs > li { margin: 0 30px;}\n\n.approval-tabs .nav-tabs > li > a {font-size: 17px !important;line-height: 17px;font-weight:normal;color:#484747;\nborder: none;padding:13px 30px 14px !important;margin: 0; cursor: pointer;}\n\n.approval-tabs .nav > li > a:hover, .nav > li > a:focus { background:#787d7a; color: #fff !important;border-radius: 4px !important;}\n\n.approval-tabs .nav-tabs > li.active a{color:#fff !important;} "

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"manager-approval col-md-11\">\n\n \n  <div class=\"approval-tabs\">\n\n    <ul class=\"nav nav-tabs\">\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation\">Compensation</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-approval/job\">Job</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet\">Time Sheet</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation\">Vacation</a>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n\n  </div>\n\n\n  \n</div>\n\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: ManagerApprovalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerApprovalComponent", function() { return ManagerApprovalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ManagerApprovalComponent = /** @class */ (function () {
    function ManagerApprovalComponent() {
    }
    ManagerApprovalComponent.prototype.ngOnInit = function () {
    };
    ManagerApprovalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manager-approval',
            template: __webpack_require__(/*! ./manager-approval.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.html"),
            styles: [__webpack_require__(/*! ./manager-approval.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ManagerApprovalComponent);
    return ManagerApprovalComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.module.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.module.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: ManagerApprovalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerApprovalModule", function() { return ManagerApprovalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _manager_approval_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./manager-approval.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/manager-approval.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _compensation_compensation_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./compensation/compensation.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/compensation/compensation.component.ts");
/* harmony import */ var _job_job_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./job/job.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/job/job.component.ts");
/* harmony import */ var _time_sheet_time_sheet_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./time-sheet/time-sheet.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.ts");
/* harmony import */ var _vacation_vacation_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./vacation/vacation.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var router = [
    { path: '', component: _manager_approval_component__WEBPACK_IMPORTED_MODULE_2__["ManagerApprovalComponent"], children: [
            { path: '', redirectTo: 'compensation', pathMatch: 'full' },
            { path: 'compensation', component: _compensation_compensation_component__WEBPACK_IMPORTED_MODULE_4__["CompensationComponent"] },
            { path: 'job', component: _job_job_component__WEBPACK_IMPORTED_MODULE_5__["JobComponent"] },
            { path: 'time-sheet', component: _time_sheet_time_sheet_component__WEBPACK_IMPORTED_MODULE_6__["TimeSheetComponent"] },
            { path: 'vacation', component: _vacation_vacation_component__WEBPACK_IMPORTED_MODULE_7__["VacationComponent"] }
        ] }
];
var ManagerApprovalModule = /** @class */ (function () {
    function ManagerApprovalModule() {
    }
    ManagerApprovalModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_manager_approval_component__WEBPACK_IMPORTED_MODULE_2__["ManagerApprovalComponent"], _compensation_compensation_component__WEBPACK_IMPORTED_MODULE_4__["CompensationComponent"], _job_job_component__WEBPACK_IMPORTED_MODULE_5__["JobComponent"], _time_sheet_time_sheet_component__WEBPACK_IMPORTED_MODULE_6__["TimeSheetComponent"], _vacation_vacation_component__WEBPACK_IMPORTED_MODULE_7__["VacationComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(router), _angular_material_select__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"], _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_9__["MatSlideToggleModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_14__["DragDropModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_16__["MatAutocompleteModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_18__["MatPaginatorModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_19__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatInputModule"]
            ]
        })
    ], ManagerApprovalModule);
    return ManagerApprovalModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.css":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".time-sheet{ margin: 0 auto; float: none; padding: 0;}\n\n.time-block { padding:30px 0 0;}\n\n.time-sheet-main { display: block}\n\n.time-sheet-tabs {display: block;padding: 20px 0 0; margin: 0 0 7px;}\n\n.time-sheet-tabs .tab-content { margin:40px 0 0;}\n\n.time-sheet-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus\n{ border-bottom:#008f3d 4px solid !important;background:transparent; color:#484747 !important;\nborder-radius: 0 !important;padding: 15px 10px;}\n\n.time-sheet-tabs .nav-tabs {border-bottom:none;display: inline-block; background: #fff; width: 21%;}\n\n.time-sheet-tabs .nav-tabs > li { margin: 0 10px; border-right:#ccc 1px solid; padding: 0 10px;}\n\n.time-sheet-tabs .nav-tabs > li > a {font-size: 17px !important;line-height: 17px;font-weight:normal;color:#484747;border: none;\nmargin: 0; cursor: pointer; padding: 15px 10px;}\n\n.time-sheet-tabs .nav > li > a:hover, .nav > li > a:focus { background:transparent; color: #484747 !important;border-radius: 0 !important;}\n\n.time-sheet-tabs .nav-tabs > li.active{margin: 0px 10px -1px;color:#484747 !important;border-bottom:none !important; padding: 0 10px;}\n\n.time-sheet-tabs .nav-tabs > li:nth-last-child(1) { border-right: none;}\n\n.summery {background: #fff; height: 150px;}\n\n.list-search{ margin:-55px 0 0;}\n\n.list-search ul{ display: block;}\n\n.list-search ul li{ padding: 0 5px;}\n\n.list-search ul li.category{ margin:0;}\n\n.list-search ul li label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 10px 15px 0;}\n\n.list-search ul li .form-control { border: none;box-shadow: none;padding: 12px 12px;height: auto;font-size: 15px;background: #fff; display: inline-block;}\n\n.list-table .table{ background:#fff;}\n\n.list-table .table>tbody>tr>th, .list-table .table>thead>tr>th {padding: 10px 20px; color: #484747; font-weight: normal; \nfont-size:16px;vertical-align: middle; position: relative; background: #edf7ff;}\n\n.list-table .table>tbody>tr>td, .list-table .table>tfoot>tr>td, .list-table .table>tfoot>tr>th, \n.list-table .table>thead>tr>td {color: #484747;padding:15px 20px;font-size:15px;}\n\n.list-table button.mat-menu-item a { color:#ccc;}\n\n.list-table button.mat-menu-item a:hover { color:#fff; background: #099247;}\n\n.list-table .table>tbody>tr>th a, .list-table .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; padding: 0 0 0 10px;}\n\n.list-table .table>tbody>tr>th a .fa, .list-table .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.list-table button { border: none; outline: none; background:transparent;}\n\n.list-table .table>tbody>tr>td small { font-size: 15px; vertical-align: middle;}\n\n.list-table .table>tbody>tr>td small .fa{ color:#008f3d; font-size: 10px; vertical-align: middle;}\n\n.mdl-history .table{ background:#fff;}\n\n.mdl-history .table>tbody>tr>th, .mdl-history .table>thead>tr>th {padding: 15px; color: #484747; font-weight: normal; \nfont-size:16px;vertical-align: middle; position: relative; background: #edf7ff;}\n\n.mdl-history .table>tbody>tr>td, .mdl-history .table>tfoot>tr>td, .mdl-history .table>tfoot>tr>th, \n.mdl-history .table>thead>tr>td {color: #484747;padding:15px;font-size:15px; vertical-align: middle; border-bottom:#ccc 1px solid;}\n\n.mdl-history .table>tbody>tr>th a, .mdl-history .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; padding: 0 0 0 10px;}\n\n.mdl-history .table>tbody>tr>th a .fa, .mdl-history .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.mdl-history .table>tbody>tr>td small { font-size: 15px; vertical-align: middle;}\n\n.mdl-history .table>tbody>tr>td small .fa{ color:#008f3d; font-size: 10px; vertical-align: middle;}\n\n.time-block .top-history ul li .form-control { width: 60%;}\n\n.direct-reports { background: #fff; padding: 20px 0;}\n\n.direct-reports ul { display: block;}\n\n.direct-reports ul li { padding: 0; text-align: center;}\n\n.direct-reports ul li label { display: block; font-size: 15px; color: #676767;}\n\n.direct-reports ul li span { display: block;font-size: 15px;color: #676767;}\n\n.top-history{ margin: 10px 0 20px;}\n\n.top-history ul{ display: block;}\n\n.top-history ul li{ padding: 0 5px;}\n\n.top-history ul li.select-date{ margin: 25px 0 0;}\n\n.top-history ul li label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 7px 15px 0;}\n\n.top-history ul li .form-control { border: none;box-shadow: none;padding: 12px 12px;height: auto;\nwidth:50%; font-size: 15px;background: #fff; display: inline-block;}\n\n.top-history ul li .range{ margin: 0 5px;}\n\n.top-history ul li .range label{color: #484747; font-weight: normal; font-size: 15px; padding: 0 0 7px; display: block;}\n\n.top-history ul li .range .date { height: auto !important; line-height:inherit !important;background:#fff; width: 165px;}\n\n.top-history ul li .range .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.top-history ul li .range .date span { border: none !important; padding: 0 !important;}\n\n.top-history ul li .range .date .form-control { width:72%; height: auto; padding: 12px 0 12px 10px;}\n\n.top-history ul li .range .date .form-control:focus { box-shadow: none; border: none;}\n\n.top-history ul li .range .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n.history-search { margin:25px 0 0;}\n\n.history-search .form-group{ margin-bottom: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"time-sheet col-md-11\">\n\n    <div class=\"time-block\">\n\n        <div class=\"panel-group\" id=\"accordion10\">\n\n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion10\"\n                  href=\"#collapseTen\">\n                  Time Sheet Status\n                </a>\n              </h4>\n            </div>\n            <div id=\"collapseTen\" class=\"panel-collapse collapse in\">\n              <div class=\"panel-body\">\n\n                <div class=\"time-sheet-main\">\n\n                  <div class=\"time-sheet-tabs\">\n\n                    <ul class=\"nav nav-tabs\">\n                      <li>\n                        <a href=\"#tab21\" data-toggle=\"tab\">Summery</a>\n                      </li>\n                      <li class=\"active\">\n                        <a href=\"#tab22\" data-toggle=\"tab\">List</a>\n                      </li>\n                    </ul>\n                    <div class=\"clearfix\"></div>\n\n                  </div>\n\n\n                  <div class=\"tab-content\">\n\n                    <div class=\"tab-pane\" id=\"tab21\">\n\n                      <div class=\"summery\">\n\n\n\n                      </div>\n\n                    </div>\n\n                    <div class=\"tab-pane active\" id=\"tab22\">\n\n                      <div class=\"list\">\n\n                        <div class=\"list-search pull-right col-md-9\">\n\n                          <ul>\n\n                            <li class=\"col-md-6 pull-right\">\n\n                              <div class=\"searching\">\n                                <div class=\"form-group\">\n                                  <form>\n                                    <input type=\"mail\" class=\"form-control\"\n                                      placeholder=\"Search Employee by Name, Job #, Job Title etc...\">\n                                    <span><i class=\"material-icons\">search</i></span>\n                                  </form>\n                                </div>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                            </li>\n\n\n                            <li class=\"col-md-5 category pull-right\">\n                              <label>Search by Category</label>\n                              <mat-select placeholder=\"My Direct Reports\" class=\"form-control\">\n                                <mat-option value=\"\">Last Week</mat-option>\n                              </mat-select>\n                            </li>\n\n                          </ul>\n                          <div class=\"clearfix\"></div>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n\n                        <div class=\"list-table\">\n\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n                                <th>\n                                  <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                                </th>\n                                <th>Employee Name <a href=\"#\"><i aria-hidden=\"true\"\n                                      class=\"fa fa-filter\"></i></a></th>\n                                <th>Manager Name<a href=\"#\"><i aria-hidden=\"true\"\n                                      class=\"fa fa-filter\"></i></a></th>\n                                <th>Business Unit <a href=\"#\"><i aria-hidden=\"true\"\n                                      class=\"fa fa-filter\"></i></a></th>\n\n                                <th>Department <a href=\"#\"><i aria-hidden=\"true\"\n                                  class=\"fa fa-filter\"></i></a></th>\n                                <th>Status <a href=\"#\"><i aria-hidden=\"true\"\n                                  class=\"fa fa-filter\"></i></a></th>\n\n                                <th>\n                                  <button mat-icon-button [matMenuTriggerFor]=\"menu5\"\n                                    aria-label=\"Example icon-button with a menu\">\n                                    <mat-icon>more_vert</mat-icon>\n                                  </button>\n                                  <mat-menu #menu5=\"matMenu\">\n                                    <button mat-menu-item>\n                                      <a>Review Request</a>\n                                    </button>\n                                  </mat-menu>\n                                </th>\n                              </tr>\n                            </thead>\n                            <tbody>\n                              <tr>\n                                <td>\n                                  <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                                </td>\n                                <td>Suresh</td>\n                                <td>12/12/12</td>\n                                <td>-- --</td>\n                                <td>Sales and Marketing</td>\n                                <td><small> <i class=\"fa fa-circle\" aria-hidden=\"true\"></i> &nbsp; Approved</small>\n\n                              </tr>\n\n                            </tbody>\n                          </table>\n\n                        </div>\n\n                      </div>\n\n                    </div>\n\n                  </div>\n\n                </div>\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n\n\n        <div class=\"panel-group\" id=\"accordion10\">\n\n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion10\"\n                  href=\"#collapseEleven\">\n                  Missing Punches\n                </a>\n              </h4>\n            </div>\n            <div id=\"collapseEleven\" class=\"panel-collapse collapse in\">\n              <div class=\"panel-body\">\n\n\n                <div class=\"top-history\">\n\n                  <ul>\n\n                    <li class=\"col-md-4 select-date\">\n                      <label>Select Date Range</label>\n                      <mat-select placeholder=\"Specific date range\" class=\"form-control\">\n                        <mat-option value=\"\">Last Week</mat-option>\n                      </mat-select>\n                    </li>\n\n                    <li class=\"col-md-4\">\n\n                      <div class=\"range pull-left\">\n                        <label>Begin Date Range(T1)</label>\n                        <div class=\"date\">\n                          <input matInput [matDatepicker]=\"pickerOne\" placeholder=\"mm / dd / yy\"\n                            class=\"form-control\">\n                          <mat-datepicker #pickerOne></mat-datepicker>\n                          <button mat-raised-button (click)=\"pickerOne.open()\">\n                            <span>\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                            </span>\n                          </button>\n                          <div class=\"clearfix\"></div>\n                        </div>\n                      </div>\n\n                      <div class=\"range pull-left\">\n                        <label>End Date Range(T2)</label>\n                        <div class=\"date\">\n                          <input matInput [matDatepicker]=\"pickerTwo\" placeholder=\"mm / dd / yy\"\n                            class=\"form-control\">\n                          <mat-datepicker #pickerTwo></mat-datepicker>\n                          <button mat-raised-button (click)=\"pickerTwo.open()\">\n                            <span>\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                            </span>\n                          </button>\n                          <div class=\"clearfix\"></div>\n                        </div>\n                      </div>\n\n                      <div class=\"clearfix\"></div>\n                    </li>\n\n                    <li class=\"col-md-4 pull-right\">\n\n                      <div class=\"searching history-search\">\n                        <div class=\"form-group col-md-12\">\n                          <form>\n                            <input type=\"mail\" class=\"form-control\"\n                              placeholder=\"Search Employee by Name, Job\">\n                            <span><i class=\"material-icons\">search</i></span>\n                          </form>\n                        </div>\n\n                      </div>\n\n                    </li>\n\n                  </ul>\n                  <div class=\"clearfix\"></div>\n\n                </div>\n\n                <div class=\"mdl-history\">\n\n                  <table class=\"table\">\n                    <thead>\n                      <tr>\n                        <th>\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                        </th>\n                        <th>Employee Name <a href=\"#\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a></th>\n                        <th>Date Missing Punch In <a href=\"#\"><i aria-hidden=\"true\"\n                              class=\"fa fa-filter\"></i></a></th>\n                        <th>Date Missing Punch Out <a href=\"#\"><i aria-hidden=\"true\"\n                              class=\"fa fa-filter\"></i></a></th>\n                        <th>Scheduled Start Time</th>\n                        <th>Scheduled Finish Time</th>\n                        <th>\n                          <button mat-icon-button [matMenuTriggerFor]=\"menu6\"\n                            aria-label=\"Example icon-button with a menu\">\n                            <mat-icon>more_vert</mat-icon>\n                          </button>\n                          <mat-menu #menu6=\"matMenu\">\n                            <button mat-menu-item>\n                              <a>Review Request</a>\n                            </button>\n                          </mat-menu>\n                        </th>\n                      </tr>\n                    </thead>\n                    <tbody>\n\n                      <tr>\n                        <td>\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                        </td>\n                        <td>Sureshhhh</td>\n                        <td>19/10/2020</td>\n                        <td>-- -- </td>\n                        <td>10:00</td>\n                        <td>10:00</td>\n                        <td></td>\n                      </tr>\n\n\n\n                    </tbody>\n                  </table>\n\n                </div>\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n\n\n        <div class=\"panel-group\" id=\"accordion10\">\n\n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion10\"\n                  href=\"#collapseTwelve\">\n                  Time Sheet Summary\n                </a>\n              </h4>\n            </div>\n            <div id=\"collapseTwelve\" class=\"panel-collapse collapse in\">\n              <div class=\"panel-body\">\n\n\n                <div class=\"top-history\">\n\n                  <ul>\n\n                    <li class=\"col-md-4 select-date\">\n                      <label>Search by Category</label>\n                      <mat-select placeholder=\"Search Category\" class=\"form-control\">\n                        <mat-option value=\"\">Last Week</mat-option>\n                      </mat-select>\n                    </li>\n\n\n                    <li class=\"col-md-5 pull-right\">\n\n                      <div class=\"searching history-search\">\n                        <div class=\"form-group col-md-12\">\n                          <form>\n                            <input type=\"mail\" class=\"form-control\"\n                              placeholder=\"Search Employee by Name, Job\">\n                            <span><i class=\"material-icons\">search</i></span>\n                          </form>\n                        </div>\n\n                      </div>\n\n                    </li>\n\n                  </ul>\n                  <div class=\"clearfix\"></div>\n\n                </div>\n\n                <div class=\"mdl-history\">\n\n                  <table class=\"table\">\n                    <thead>\n                      <tr>\n                        <th>\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                        </th>\n                        <th>Name <a href=\"#\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a></th>\n                        <th>Daily Total Hours</th>\n                        <th>Weekly Total Hours</th>\n                        <th>Pay Cycle Total</th>\n                        <th>Approaching Over Time</th>\n                        <th>Approval Status</th>\n                        <th>\n                          <button mat-icon-button [matMenuTriggerFor]=\"menu7\"\n                            aria-label=\"Example icon-button with a menu\">\n                            <mat-icon>more_vert</mat-icon>\n                          </button>\n                          <mat-menu #menu7=\"matMenu\">\n                            <button mat-menu-item>\n                              <a>Review Request</a>\n                            </button>\n                          </mat-menu>\n                        </th>\n                      </tr>\n                    </thead>\n                    <tbody>\n\n                      <tr>\n                        <td>\n                          <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                        </td>\n                        <td>Sureshhhh</td>\n                        <td>17:50</td>\n                        <td>10:50 </td>\n                        <td>17:50</td>\n                        <td>10:50 </td>\n                        <td><small> <i class=\"fa fa-circle\" aria-hidden=\"true\"></i> &nbsp; Approved</small>\n                        </td>\n                        <td></td>\n                      </tr>\n\n\n\n                    </tbody>\n                  </table>\n\n                </div>\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n\n\n        <div class=\"panel-group\" id=\"accordion10\">\n\n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion10\"\n                  href=\"#collapseThirteen\">\n                  My Direct Reports - Total\n                </a>\n              </h4>\n            </div>\n            <div id=\"collapseThirteen\" class=\"panel-collapse collapse in\">\n              <div class=\"panel-body\">\n\n\n                <div class=\"direct-reports\">\n\n                  <ul>\n\n                    <li class=\"col-md-3\">\n                      <label>Total Number of Employees</label>\n                      <span>10</span>\n                    </li>\n\n                    <li class=\"col-md-3\">\n                      <label>Total Daily Hours</label>\n                      <span>10</span>\n                    </li>\n\n                    <li class=\"col-md-3\">\n                      <label>Total Weekly Hours</label>\n                      <span>10</span>\n                    </li>\n\n                    <li class=\"col-md-3\">\n                        <label>Total Pay Cycle Hours</label>\n                        <span>10</span>\n                    </li>\n\n                  </ul>\n                  <div class=\"clearfix\"></div>\n\n                </div>\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n\n      </div>\n\n</div>\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.ts":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: TimeSheetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeSheetComponent", function() { return TimeSheetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/manager-approval.service */ "./src/app/services/manager-approval.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TimeSheetComponent = /** @class */ (function () {
    function TimeSheetComponent(timeSheetService) {
    }
    TimeSheetComponent.prototype.ngOnInit = function () {
    };
    TimeSheetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-time-sheet',
            template: __webpack_require__(/*! ./time-sheet.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.html"),
            styles: [__webpack_require__(/*! ./time-sheet.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/time-sheet/time-sheet.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__["ManagerApprovalService"]])
    ], TimeSheetComponent);
    return TimeSheetComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.css":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".vacation{ margin: 0 auto; float: none; padding: 0;}\n\n.time-off { display: block;}\n\n.time-off-top { display: block;}\n\n.time-off-top ul { display: block;}\n\n.time-off-top ul li { padding: 0;}\n\n.time-off-top ul li .form-control{ border: none; box-shadow: none; height: auto; padding: 10px 12px;}\n\n.time-off-top ul li .form-control:focus{ border-color: none; box-shadow: none;}\n\n.mdl-history .table{ background:#fff;}\n\n.mdl-history .table>tbody>tr>th, .mdl-history .table>thead>tr>th {padding: 15px; color: #484747; font-weight: normal; \nfont-size:16px;vertical-align: middle; position: relative; background: #edf7ff;}\n\n.mdl-history .table>tbody>tr>td, .mdl-history .table>tfoot>tr>td, .mdl-history .table>tfoot>tr>th, \n.mdl-history .table>thead>tr>td {color: #484747;padding:15px;font-size:15px; vertical-align: middle; border-bottom:#ccc 1px solid;}\n\n.mdl-history .table>tbody>tr>th a, .mdl-history .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n\n.mdl-history .table>tbody>tr>th a .fa, .mdl-history .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.managers { display: block;}\n\n.managers span { vertical-align: middle; display: inline-block;}\n\n.manager-right { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;}\n\n.manager-right h4 { font-size: 15px; margin: 0 0 5px;}\n\n.manager-right h5 {  color: #ccc;margin: 0;}\n\n.mdl-history .table>tbody>tr>td p { font-size: 15px; vertical-align: middle;}\n\n.mdl-history .table>tbody>tr>td p .fa{ color:#008f3d; font-size: 10px; vertical-align: middle;}\n\n.leaves { display: block;}\n\n.leaves img { margin: 0 0 10px;}\n\n.leaves small { display: block;color:#929191;}\n\n.time-off > ul{ margin:10px 0; padding: 0;}\n\n.time-off ul li{ padding: 0 20px; display: inline-block;}\n\n.time-off ul li a{color: #484747; font-weight: normal; font-size: 14px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.html":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.html ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"vacation col-md-11\">\n\n    <div class=\"time-block\">\n\n        <div class=\"panel-group\" id=\"accordion11\">\n\n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n              <h4 class=\"panel-title\">\n                <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion11\"\n                  href=\"#collapseFourteen\">\n                  Time Off Requests Pending\n                </a>\n              </h4>\n            </div>\n            <div id=\"collapseFourteen\" class=\"panel-collapse collapse in\">\n              <div class=\"panel-body\">\n\n                  <div class=\"time-off\">\n\n                    <div class=\"time-off-top\">\n                       <ul>\n                        <li class=\"col-md-3\">\n                          <mat-select placeholder=\"Category\" class=\"form-control\">\n                              <mat-option value=\"\">Last Week</mat-option>\n                          </mat-select>\n                        </li>\n                        <li class=\"col-md-5 pull-right\">\n                          <div class=\"searching\">\n                              <div class=\"form-group\">\n                                <form>\n                                  <input type=\"mail\" class=\"form-control\"\n                                    placeholder=\"Search Employee by Name, Job #, Job Title etc...\">\n                                  <span><i class=\"material-icons\">search</i></span>\n                                </form>\n                              </div>\n                              <div class=\"clearfix\"></div>\n                            </div>\n                        </li>\n                       </ul>\n                       <div class=\"clearfix\"></div>\n                    </div>\n\n\n                    <div class=\"mdl-history\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n    \n                              <th>Employee Name</th>\n                              <th>Type of Leave Requested <a href=\"#\"><i aria-hidden=\"true\"\n                                class=\"fa fa-filter\"></i></a> </th>\n                              <th>Requested Date <a href=\"#\"><i aria-hidden=\"true\"\n                                class=\"fa fa-filter\"></i></a></th>\n                              <th>Time Off</th> \n                              <th>Action &nbsp;\n                                  <span>\n                                    <mat-slide-toggle></mat-slide-toggle>\n                                  </span>\n                                </th>\n    \n                            </tr>\n                          </thead>\n                          <tbody>\n    \n                            <tr>\n                              <td>\n                                <div class=\"managers\">\n                                  <span><img\n                                      src=\"../../../../assets/images/Manager Self Service/employee.png\"></span>\n                                  <div class=\"manager-right\">\n                                    <h4>Crane Andy</h4>\n                                    <h5>Manager</h5>\n                                  </div>\n                                </div>\n                              </td>\n                              <td>\n                                <div class=\"leaves\">\n                                    <img src=\"../../../../assets/images/Manager Self Service/vacation.png\">\n                                  <small>Vacation</small>\n                                </div>\n                              </td>\n                              <td>June 10</td>\n                              <td>09:30-13:00</td>\n                              <td><span>\n                                  <mat-slide-toggle></mat-slide-toggle>\n                                </span>\n                              </td>\n                            </tr>\n    \n                          </tbody>\n                        </table>\n    \n                      </div>\n\n                  </div>\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n\n\n        <div class=\"panel-group\" id=\"accordion11\">\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion11\"\n                    href=\"#collapseFifteen\">\n                    Historical Approvals and Non Approvals\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseFifteen\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n  \n                    <div class=\"time-off\">\n                      <h4>Requested Time Off</h4>\n\n                        <ul class=\"col-md-4\">\n                          <li><a href=\"#\">Show All</a></li>\n                          <li><a href=\"#\">Approved</a></li>\n                          <li><a href=\"#\">Not Approved</a></li>\n                        </ul>\n                        \n\n                      <div class=\"time-off-top col-md-8 pull-right\">\n                         <ul>\n                          <li class=\"col-md-4\">\n                            <mat-select placeholder=\"Category\" class=\"form-control\">\n                                <mat-option value=\"\">Last Week</mat-option>\n                            </mat-select>\n                          </li>\n                          <li class=\"col-md-8\">\n                            <div class=\"searching\">\n                                <div class=\"form-group\">\n                                  <form>\n                                    <input type=\"mail\" class=\"form-control\"\n                                      placeholder=\"Search Employee by Name, Job #, Job Title etc...\">\n                                    <span><i class=\"material-icons\">search</i></span>\n                                  </form>\n                                </div>\n                                <div class=\"clearfix\"></div>\n                              </div>\n                          </li>\n                         </ul>\n                         <div class=\"clearfix\"></div>\n                      </div>\n                    <div class=\"clearfix\"></div>\n  \n                      <div class=\"mdl-history\">\n\n                          <table class=\"table\">\n                            <thead>\n                              <tr>\n      \n                                <th>Employee Name</th>\n                                <th>Type of Leave Requested</th>\n                                <th>Requested Date</th>\n                                <th>Time Off</th> \n                                <th>Request Status &nbsp;\n                                    <span>\n                                      <mat-slide-toggle></mat-slide-toggle>\n                                    </span>\n                                  </th> \n                              </tr>\n                            </thead>\n                            <tbody>\n      \n                              <tr>\n                                <td>\n                                  <div class=\"managers\">\n                                    <span><img\n                                        src=\"../../../../assets/images/Manager Self Service/employee.png\"></span>\n                                    <div class=\"manager-right\">\n                                      <h4>Crane Andy</h4>\n                                      <h5>Manager</h5>\n                                    </div>\n                                  </div>\n                                </td>\n                                <td>\n                                  <div class=\"leaves\">\n                                      <img src=\"../../../../assets/images/Manager Self Service/vacation.png\">\n                                    <small>Vacation</small>\n                                  </div>\n                                </td>\n                                <td>June 10</td>\n                                <td>09:30-13:00</td>\n                                <td><p> <i class=\"fa fa-circle\" aria-hidden=\"true\"></i> &nbsp; Approved</p></td>\n                              </tr>\n      \n                            </tbody>\n                          </table>\n      \n                        </div>\n  \n                    </div>\n  \n                </div>\n              </div>\n            </div>\n  \n          </div>\n  \n\n\n\n\n    </div>\n      \n\n</div>\n\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.ts":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.ts ***!
  \**************************************************************************************************************************/
/*! exports provided: VacationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacationComponent", function() { return VacationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/manager-approval.service */ "./src/app/services/manager-approval.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VacationComponent = /** @class */ (function () {
    function VacationComponent(vacationService) {
    }
    VacationComponent.prototype.ngOnInit = function () {
    };
    VacationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vacation',
            template: __webpack_require__(/*! ./vacation.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.html"),
            styles: [__webpack_require__(/*! ./vacation.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-approval/vacation/vacation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_approval_service__WEBPACK_IMPORTED_MODULE_1__["ManagerApprovalService"]])
    ], VacationComponent);
    return VacationComponent;
}());



/***/ }),

/***/ "./src/app/services/manager-approval.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/manager-approval.service.ts ***!
  \******************************************************/
/*! exports provided: ManagerApprovalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerApprovalService", function() { return ManagerApprovalService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ManagerApprovalService = /** @class */ (function () {
    function ManagerApprovalService(http) {
        this.http = http;
    }
    ManagerApprovalService.prototype.getAllPendingData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-requests/requestsList", data);
    };
    ManagerApprovalService.prototype.approveOrRejectRequest = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-requests/edit", data);
    };
    ManagerApprovalService.prototype.editCompensationPending = function (cId, mId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-requests/get/" + cId + '/' + mId);
    };
    ManagerApprovalService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ManagerApprovalService);
    return ManagerApprovalService;
}());



/***/ })

}]);
//# sourceMappingURL=manager-approval-manager-approval-module.js.map