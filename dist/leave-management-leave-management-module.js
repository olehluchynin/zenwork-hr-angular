(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["leave-management-leave-management-module"],{

/***/ "./src/app/admin-dashboard/leave-management/leave-management.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave-management { display: block;}\n.zenwork-currentpage {\n    padding-top: 40px;\n}\n.zenwork-margin-zero { padding: 0 0 7px;}\n.zenwork-margin-zero span { display: inline-block; vertical-align: middle; padding: 0 0 0 20px; font-size: 17px; line-height: 17px;}\n.zenwork-margin-zero b { display: inline-block; font-weight:500; line-height: 20px; color: #656565;}\n.leave-dashboard { background:#fff; border-radius:5px; box-shadow: 0 0 10px #ccc; float: none; margin:50px auto 100px; padding: 0 35px; text-align: center;}\n.leave-dashboard ul { display:inline-block; border-bottom:#ccc 1px solid; width: 100%; padding:50px 0; margin: 0;}\n.leave-dashboard ul li { padding: 40px 0 0; position: relative;}\n.leave-dashboard ul li a { display: inline-block; cursor: pointer;}\n.leave-dashboard ul li a img { margin: 0 0 20px;}\n.leave-dashboard ul li a h2 { margin: 0 0 10px; font-size: 20px; line-height: 20px; color:#403e3e; font-weight: normal; }\n.leave-dashboard ul li a small {font-size:14px; line-height:16px; color:#656565; font-weight: normal;}\n.leave-dashboard ul li.time-policies { position: relative; padding:70px 0 0;}\n.time-policies::after { content:''; position: absolute; top:50px; right: 0;border-right:#ccc 1px solid; height:100%;}\n.time-schedule { border-right:#ccc 1px solid;}\n.leave-dashboard-report { display:block;margin: 0 auto 40px; float: none; padding: 0;}\n.work{margin: 0 0 20px;}\n.work h2 {font-size: 18px; line-height: 18px; font-weight: normal; color:#7b7b7b; margin: 0;}\n.employee-report-in { display: block;}\n.employee-report-top {background: #eef7ff; padding: 10px 20px;}\n.employee-report-lft {padding: 0;}\n.employee-report-lft ul { display: block;}\n.employee-report-lft ul li { float: left; margin: 0 15px 0 0; padding: 0;}\n.employee-report-lft ul li label { padding:10px 15px 0 0; color:#777777; font-weight: normal; font-size: 15px; line-height: 15px; float: left;}\n.employee-report-lft ul li select {background: #fff;border: none;width:56%;height: 34px;color: #948e8e;padding:0 0 0 6px; float: left;}\n.employee-report-rgt { padding: 0; text-align: right;}\n.employee-report-rgt ul { display: block;}\n.employee-report-rgt ul li { float: right; margin: 0 0 0 10px; padding: 0;}\n.employee-report-rgt ul li select {background: #fff;border: none;width: 100%;height: 34px;color: #948e8e;padding: 6px 10px;}\n.employee-self { background:#fff; padding: 30px 0;}\n.employee-data { display: block;}\n.employee-data ul { display: block;}\n.employee-data ul li { float: left; text-align: center;}\n.employee-data ul li img { margin: 0 auto 20px;}\n.employee-data ul li h4 { font-size: 18px; line-height: 18px; color:#5f5e5e; margin: 0 0 20px;}\n.employee-data ul li small { font-size: 13px; line-height: 13px; color:#c7c7c7;padding: 0 0 15px; display: block;}\n.employee-data ul li h2 { color:#bdb5b5; margin: 0 ; font-size: 30px; font-weight: normal;}\n.employee-data ul li a.btn { border:#a5a5a5 1px solid; border-radius: 20px; color:#525252; padding:8px 24px; font-size: 14px; border-radius:4px; text-transform: capitalize;}\n.employee-data ul li a.btn:hover { background: #008f3d; color:#fff; border: transparent 1px solid;}\n.employee-self .carousel-control.right {\n\tleft:auto;\n\tright:40px; top: 40%;\n}\n.employee-self .carousel-control.left {\n\tright:auto;\n\tleft:30px; top: 40%;\n}\n.employee-self .fa-2x {\n    font-size:30px; color: #008f3d;\n}\n.employee-self .fa-2x:hover {color: #d9534f;}\n.employee-self .carousel-inner { width: 90%; margin:0 auto;}\n.employee-self .carousel-control { opacity: 1; box-shadow: none; text-shadow: none; width: auto;}\n.integrate { float: none; margin:10px auto 0; padding: 0;}\n.integrate span { width: 70px; padding: 0 0 10px;}\n.integrate span .fa { font-size: 20px; color:#008f3d;}\n.integrate span var .fa { font-size: 20px; color:#e43d3c;}\n.integrate p { display: block; color:#008f3d; font-size: 14px; line-height: 14px;}\n.integrate em { display: block; color:#e43d3c; font-size: 14px; line-height: 14px; font-style: normal;}\n.integrate a.btn { display: block; color:#fff; font-size: 14px; line-height: 14px; background:#797d7a; text-transform: uppercase; padding: 11px 24px 10px;\nfont-weight: 600;}"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"leave-management\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <img src=\"../../../assets/images/leave-management/leave.png\" width=\"23\" height=\"24\" alt=\"Company-settings icon\">\n      <span>Leave Management</span>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"leave-dashboard col-md-11\">\n\n    <ul>\n      <li class=\"col-md-6 time-policies\">\n        <a routerLink=\"/admin/admin-dashboard/leave-management/time-policies\">\n          <img src=\"../../../assets/images/leave-management/timeOff.png\" alt=\"img\">\n          <h2>Time Off Policies</h2>\n          <small>Add or manage time off policies</small>\n        </a>\n        \n      </li>\n\n      <li class=\"col-md-6 no-bor\">\n          <a routerLink=\"/admin/admin-dashboard/leave-management/time-sheet\">\n        <img src=\"../../../assets/images/leave-management/timesheet.png\" width=\"49\" height=\"49\" alt=\"img\">\n        <h2>Time Sheet Approval and Maintenance</h2>\n        <small>Access employee timesheets</small>\n\n          <div class=\"integrate col-md-5\">\n              <span class=\"pull-left\">\n                <i aria-hidden=\"true\" class=\"fa fa-check-square\"></i>\n                <p>48</p>\n              </span>\n              <span class=\"pull-left\">\n              <var> <i aria-hidden=\"true\" class=\"fa fa-exclamation-triangle\"></i></var>\n                  <em>02</em>\n              </span>\n              <div class=\"clearfix\"></div>\n\n              <a class=\"btn\">integrate</a>\n          </div>\n\n        </a>\n      </li>\n    </ul>\n\n    <ul class=\"no-bor\">\n      <li class=\"col-md-6 time-schedule\">\n          <a routerLink=\"/admin/admin-dashboard/leave-management/scheduler/holidays\">\n        <img src=\"../../../assets/images/leave-management/schedulers.png\" width=\"49\" height=\"49\" alt=\"img\">\n        <h2>Scheduler</h2>\n        <small>Build or manage schedules <br> for your employees</small>\n        \n        </a>\n      </li>\n\n      <li class=\"col-md-6 no-bor\">\n          <a routerLink=\"/admin/admin-dashboard/leave-management/leave-main/eligibility\">\n        <img src=\"../../../assets/images/leave-management/leavegroups.png\" width=\"62\" height=\"50\" alt=\"img\">\n        <h2>Leave Eligibility Groups</h2>\n        <small>Set Leave Management Eligibility Rules</small>\n        </a>\n      </li>\n\n    </ul>\n\n    <div class=\"clearfix\"></div>\n\n  </div>\n\n\n  <div class=\"leave-dashboard-report col-md-11\">\n\n    <div class=\"work\">\n      <h2>Today's Report</h2>\n    </div>\n\n\n    <div class=\"employee-report-in\">\n\n      <div class=\"employee-report-top\">\n\n        <div class=\"employee-report-lft pull-left col-md-9\">\n\n          <ul>\n            <li class=\"col-md-3\">\n              <label>Location</label>\n              <select>\n                <option>All</option>\n                <option>All1</option>\n                <option>All2</option>\n              </select>\n              <div class=\"clearfix\"></div>\n            </li>\n\n            <li class=\"col-md-4\">\n              <label>Department</label>\n              <select>\n                <option>All</option>\n                <option>All1</option>\n                <option>All2</option>\n              </select>\n              <div class=\"clearfix\"></div>\n            </li>\n            <li class=\"col-md-3\">\n              <label>Shift</label>\n              <select>\n                <option>All</option>\n                <option>All1</option>\n                <option>All2</option>\n              </select>\n              <div class=\"clearfix\"></div>\n            </li>\n          </ul>\n          <div class=\"clearfix\"></div>\n        </div>\n\n        <div class=\"employee-report-rgt pull-right col-md-3\">\n          <ul>\n\n            <li class=\"col-md-4\">\n              <select>\n                <option>2016</option>\n                <option>2017</option>\n                <option>2018</option>\n              </select>\n            </li>\n\n            <li class=\"col-md-4\">\n              <select>\n                <option>June</option>\n                <option>April</option>\n                <option>March</option>\n              </select>\n            </li>\n\n          </ul>\n          <div class=\"clearfix\"></div>\n        </div>\n        <div class=\"clearfix\"></div>\n\n      </div>\n\n\n      <div class=\"employee-self\">\n\n        <div id=\"myCarousel\" class=\"carousel slide\">\n          <div class=\"carousel-inner\">\n\n            <div class=\"item active\">\n              <div class=\"employee-data\">\n                <ul>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon1.png\" width=\"29\" height=\"29\" alt=\"img\">\n                    <h4>Early Birds</h4>\n                    <h2>245</h2>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon2.png\" width=\"25\" height=\"29\" alt=\"img\">\n                    <h4>Late Arrivals</h4>\n                    <h2>213</h2>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon3.png\" width=\"44\" height=\"29\" alt=\"img\">\n                    <h4>EEs Missing Time Punches</h4>\n                    <h2>65</h2>\n                  </li>\n\n                </ul>\n              </div>\n\n\n            </div>\n\n            <div class=\"item\">\n              <div class=\"employee-data\">\n                <ul>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon1.png\" width=\"29\" height=\"29\" alt=\"img\">\n                    <h4>Early Birds</h4>\n                    <h2>245</h2>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon2.png\" width=\"25\" height=\"29\" alt=\"img\">\n                    <h4>Late Arrivals</h4>\n                    <h2>213</h2>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon3.png\" width=\"44\" height=\"29\" alt=\"img\">\n                    <h4>EEs Missing Time Punches</h4>\n                    <h2>65</h2>\n                  </li>\n\n                </ul>\n              </div>\n\n\n            </div>\n\n            <div class=\"item\">\n              <div class=\"employee-data\">\n                <ul>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon1.png\" width=\"29\" height=\"29\" alt=\"img\">\n                    <h4>Early Birds</h4>\n                    <h2>245</h2>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon2.png\" width=\"25\" height=\"29\" alt=\"img\">\n                    <h4>Late Arrivals</h4>\n                    <h2>213</h2>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <img src=\"../../../assets/images/leave-management/icon3.png\" width=\"44\" height=\"29\" alt=\"img\">\n                    <h4>EEs Missing Time Punches</h4>\n                    <h2>65</h2>\n                  </li>\n\n                </ul>\n              </div>\n\n\n            </div>\n\n\n          </div>\n          <a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">\n            <i class=\"fa fa-chevron-left fa-2x\"></i>\n          </a>\n          <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">\n            <i class=\"fa fa-chevron-right fa-2x\"></i>\n          </a>\n\n        </div>\n      </div>\n\n\n    </div>\n\n\n  </div>\n\n\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management.component.ts ***!
  \********************************************************************************/
/*! exports provided: LeaveManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementComponent", function() { return LeaveManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LeaveManagementComponent = /** @class */ (function () {
    function LeaveManagementComponent() {
    }
    LeaveManagementComponent.prototype.ngOnInit = function () {
        $(document).ready(function () {
            $('#myCarousel').carousel({
                interval: 3000
            });
        });
    };
    LeaveManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-leave-management',
            template: __webpack_require__(/*! ./leave-management.component.html */ "./src/app/admin-dashboard/leave-management/leave-management.component.html"),
            styles: [__webpack_require__(/*! ./leave-management.component.css */ "./src/app/admin-dashboard/leave-management/leave-management.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LeaveManagementComponent);
    return LeaveManagementComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management.module.ts ***!
  \*****************************************************************************/
/*! exports provided: LeaveManagementModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementModule", function() { return LeaveManagementModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _leave_management_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leave-management.component */ "./src/app/admin-dashboard/leave-management/leave-management.component.ts");
/* harmony import */ var _leave_management_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./leave-management.routing */ "./src/app/admin-dashboard/leave-management/leave-management.routing.ts");
/* harmony import */ var _time_policies_time_policies_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./time-policies/time-policies.component */ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _scheduler_scheduler_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./scheduler/scheduler.module */ "./src/app/admin-dashboard/leave-management/scheduler/scheduler.module.ts");
/* harmony import */ var _time_sheet_approval_time_sheet_approval_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./time-sheet-approval/time-sheet-approval.component */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _time_policies_tier_level_model_tier_level_model_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./time-policies/tier-level-model/tier-level-model.component */ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.ts");
/* harmony import */ var amazing_time_picker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! amazing-time-picker */ "./node_modules/amazing-time-picker/amazing-time-picker.es5.js");
/* harmony import */ var _time_sheet_approval_review_historical_timesheet_review_historical_timesheet_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var LeaveManagementModule = /** @class */ (function () {
    function LeaveManagementModule() {
    }
    LeaveManagementModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _leave_management_routing__WEBPACK_IMPORTED_MODULE_3__["LeaveManagementRouting"], _scheduler_scheduler_module__WEBPACK_IMPORTED_MODULE_6__["SchedulerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_8__["MaterialModuleModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_9__["BsDatepickerModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
                amazing_time_picker__WEBPACK_IMPORTED_MODULE_13__["AmazingTimePickerModule"]
            ],
            declarations: [_leave_management_component__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementComponent"], _time_policies_time_policies_component__WEBPACK_IMPORTED_MODULE_4__["TimePoliciesComponent"], _time_sheet_approval_time_sheet_approval_component__WEBPACK_IMPORTED_MODULE_7__["TimeSheetApprovalComponent"], _time_policies_tier_level_model_tier_level_model_component__WEBPACK_IMPORTED_MODULE_12__["TierLevelModelComponent"], _time_sheet_approval_review_historical_timesheet_review_historical_timesheet_component__WEBPACK_IMPORTED_MODULE_14__["ReviewHistoricalTimesheetComponent"],],
            providers: [_services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_10__["LeaveManagementService"]],
            entryComponents: [_time_policies_tier_level_model_tier_level_model_component__WEBPACK_IMPORTED_MODULE_12__["TierLevelModelComponent"], _time_sheet_approval_review_historical_timesheet_review_historical_timesheet_component__WEBPACK_IMPORTED_MODULE_14__["ReviewHistoricalTimesheetComponent"]]
        })
    ], LeaveManagementModule);
    return LeaveManagementModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management.routing.ts":
/*!******************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management.routing.ts ***!
  \******************************************************************************/
/*! exports provided: LeaveManagementRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementRouting", function() { return LeaveManagementRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _leave_management_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leave-management.component */ "./src/app/admin-dashboard/leave-management/leave-management.component.ts");
/* harmony import */ var _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared-module/shared.module */ "./src/app/shared-module/shared.module.ts");
/* harmony import */ var _time_policies_time_policies_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./time-policies/time-policies.component */ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.ts");
/* harmony import */ var _time_sheet_approval_time_sheet_approval_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./time-sheet-approval/time-sheet-approval.component */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    // { path: '', redirectTo: "leave-management", pathMatch: "full" },
    { path: '', component: _leave_management_component__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementComponent"] },
    { path: 'time-policies', component: _time_policies_time_policies_component__WEBPACK_IMPORTED_MODULE_4__["TimePoliciesComponent"] },
    { path: 'scheduler', loadChildren: "./scheduler/scheduler.module#SchedulerModule" },
    { path: 'time-sheet', component: _time_sheet_approval_time_sheet_approval_component__WEBPACK_IMPORTED_MODULE_5__["TimeSheetApprovalComponent"] },
    { path: 'leave-main', loadChildren: './leave-management-eligibility/leave-management-eligibility.module#LeaveManagementEligibilityModule' }
];
var LeaveManagementRouting = /** @class */ (function () {
    function LeaveManagementRouting() {
    }
    LeaveManagementRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes), _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], LeaveManagementRouting);
    return LeaveManagementRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.css":
/*!****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.css ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .import-balance-popup { display: block;} */\n/* .import-balance-popup .modal-header { padding:0 60px 25px; border-bottom: 1px solid #c5c3c3;}\n.import-balance-popup .modal-dialog { width:70%;}\n.import-balance-popup .modal-content { border: none; padding:30px 0 50px;} */\n.import-cont .btn {border-radius: 20px;color:#000; font-size: 15px; padding:6px 25px; background:transparent; margin:30px 0 0; float:left;}\n.import-cont .btn:hover {background: #008f3d; color:#fff;}\n.import-cont { margin:40px auto 0; float: none; padding: 0;}\n.margin-btm {\n    border-bottom: 1px solid #fdeded;\n}\n.padding-top-btm {\n    padding: 30px 0px;\n}\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 15px;\n    padding: 0 0 0 2px;\n}\n.tier-width {\n    width: 20%;\n    margin-top: 10px;\n}\n.tier-head {\n    font-size: 16px;\n    margin: 20px 0px;\n}\n.recap-table {margin: 40px 0 0 0;}\n.recap-table .table>thead>tr>th { color: #6f6f6f; font-weight: normal;  background: #eef7ff; border:none; font-size: 16px; \nborder-radius:3px; width: 21px;}\n.recap-table .table>tbody>tr>td, .recap-table .table>tfoot>tr>td, .recap-table .table>thead>tr>td{ background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#948e8e;}\n.recap-table .checkbox{ margin: 0;}\n.recap-table .checkbox label { padding-left: 0;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top:4px;height:80%; border-radius: 0 4px 4px 0; padding: 0; left: 0;}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.border-tier{\n    background-color: #fff;\n    box-shadow: none;\n}\n.border-top {\n    border-top: 1px solid #c5c3c3;\n    padding-top: 30px;\n}\n"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Add / Edit - Tier Level - Settings</h4>\n        </div>\n\n       \n\n          <div class=\"import-cont col-md-11\">\n\n              <div class=\"margin-btm row padding-top-btm\">\n                 \n                  <ul class=\"list-unstyled\">\n                    <li>\n                        <label>Select Date Type for Length of Service Calculation</label><br>\n                    \n                        <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"datetype_Of_Servicecalc\">\n                            <mat-option value=\"Hire Date\">Hire Date</mat-option>\n                            <mat-option value=\"Rehire Date\">Rehire Date</mat-option>\n                            <mat-option value=\"Custom\">Custom</mat-option>\n                          </mat-select>\n                    </li>\n                    <li>\n                        <label>Tier Level Entry Point </label><br>\n                    \n                        <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"tierLevel_Entry\">\n                            <mat-option value=\"Beginning of Pay Cycle\">Beginning of Pay Cycle</mat-option>\n                            <mat-option value=\"End of Pay Cycle\">End of Pay Cycle</mat-option>\n                            <mat-option value=\"Manual Adjustment\">Manual Adjustment</mat-option>\n                          </mat-select>\n                    </li>\n                    <li>\n                      <label>Number of Tier Levels</label><br>\n                      <input type=\"number\" class=\"form-control text-field tier-width\" [(ngModel)]=\"TierLevel_Ln\" (change)=\"pushTierLevels()\">\n                     \n                    </li>\n                   \n                  </ul>\n                </div>\n                <div class=\"margin-btm row padding-top-btm\">\n                  <h5 class=\"tier-head\">Define Tier Levels</h5>\n                    <div class=\"recap-table\">\n                        <table class=\"table\" style=\"margin-bottom:0px;\">\n                          <thead>\n                            <tr>\n                              <th>\n                             \n                              </th>\n                              <th>Tier Level</th>\n                              <th>Length of Service</th>\n                              <th></th>\n                              <th>Hours Earned Max</th>\n                              <th>Hours Earned Per Cycle</th>\n                              <th>Carry over allowed?</th>\n                              <th *ngIf=\"hideField\">Carry over max?</th>\n                              <th>\n        \n                                <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n                                <mat-menu #menu2=\"matMenu\" class=\"option-position\"> \n                                  <button mat-menu-item class=\"edit-color\" (click)=\"editTierLevels()\">Edit</button>\n                                  <button mat-menu-item class=\"edit-color\" (click)=\"deleteTierLevels()\">Delete</button>\n                                </mat-menu>\n                              </th>\n                            </tr>\n                          </thead>\n                          <tbody>\n        \n                            <tr *ngFor=\"let levels of tierLevels; let i=index;\">\n                              <td [ngClass]=\"{'zenwork-checked-border': levels.isChecked}\">\n        \n                                      <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"levels.isChecked\" >\n                        \n                                      </mat-checkbox>\n\n                              </td>\n                              <td (click)=\"levelNumber(i+1)\">Level {{i+1}}</td>\n                              <td>\n                                <input type=\"number\" class=\"form-control border-tier\" [(ngModel)]=\"levels.serviceLength.date\" [readOnly]=\"levels.edit\">\n                               \n                              </td>\n                              <td style=\"width:13%;\">\n                               \n                             <mat-select class=\"form-control border-tier\" [(ngModel)]=\"levels.serviceLength.duration\" [disabled]=\"levels.edit\">\n                                <mat-option value=\"Years\">Years</mat-option>\n                               <mat-option value=\"Months\">Months</mat-option>\n                                         \n                               </mat-select>\n                                \n                              </td>\n                              <td>\n                                <input type=\"number\" class=\"form-control border-tier\" [(ngModel)]=\"levels.hoursEarnedMax\" [readOnly]=\"levels.edit\">\n                              </td>\n                              <td>\n                                  <input type=\"number\" class=\"form-control border-tier\" [(ngModel)]=\"levels.hoursEarnedPerCycle\" [readOnly]=\"levels.edit\">\n                              </td>\n                              <td>\n                                  <mat-select class=\"form-control border-tier\" [(ngModel)]=\"levels.carryOverAllowed\" [disabled]=\"levels.edit\" (ngModelChange)=\"allowedCarryOver(levels.carryOverAllowed)\">\n                                      <mat-option value=\"Yes\">Yes</mat-option>\n                                      <mat-option value=\"No\">No</mat-option>\n                                     \n                                    </mat-select>\n                              </td>\n                              <td *ngIf=\"hideField\">\n                                  <input type=\"number\" class=\"form-control border-tier\" [(ngModel)]=\"levels.carryOverMax\" [readOnly]=\"levels.edit\">\n                              </td>\n                              <td></td>\n        \n                            </tr>\n        \n                          \n                        </table>\n                        </div>\n        \n                  \n                  </div>\n\n          </div>\n          <div class=\"col-md-12 border-top\">\n            <div class=\"col-md-12\">\n\n              <button mat-button (click)=\"onNoClick()\">Cancel</button>\n\n              <button mat-button class=\"submit-btn pull-right\" (click)=\"addTierLevel()\">Submit</button>\n            </div>\n\n\n          </div>\n\n          <div class=\"clearfix\"></div>\n\n        \n\n\n    \n\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: TierLevelModelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TierLevelModelComponent", function() { return TierLevelModelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var TierLevelModelComponent = /** @class */ (function () {
    function TierLevelModelComponent(dialogRef, leaveManagementService, accessLocalStorageService, swalAlertService, data) {
        this.dialogRef = dialogRef;
        this.leaveManagementService = leaveManagementService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.data = data;
        this.tierLevel1 = {
            serviceLength: {
                date: '',
                duration: ''
            },
            hoursEarnedMax: '',
            hoursEarnedPerCycle: '',
            carryOverAllowed: '',
            carryOverMax: ''
        };
        this.tierLevels = [];
        this.flag = false;
        this.selectedTierLevels = [];
        this.editField = true;
        this.hideField = true;
    }
    TierLevelModelComponent.prototype.ngOnInit = function () {
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        console.log(this.data);
        this.data.tierLevel.filter(function (item) {
            item.edit = true;
        });
        if (this.data) {
            this.TierLevel_Ln = this.data.TierLevel_Ln;
            this.datetype_Of_Servicecalc = this.data.datetype_Of_Servicecalc;
            this.tierLevel_Entry = this.data.tierLevel_Entry;
            this.tierLevels = this.data.tierLevel;
        }
    };
    TierLevelModelComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    // Author: Saiprakash G, Date: 03/06/19,
    // push tier levels
    TierLevelModelComponent.prototype.pushTierLevels = function () {
        console.log(this.TierLevel_Ln);
        this.editField = false;
        var newLength = this.TierLevel_Ln - this.tierLevels.length;
        var decreaseLength = this.tierLevels.length - this.TierLevel_Ln;
        console.log(decreaseLength);
        if (decreaseLength > 0) {
            this.tierLevels.splice(this.tierLevels.length - decreaseLength);
        }
        if (newLength > 0) {
            for (var i = 0; i < newLength; i++) {
                this.tierLevels.push({
                    serviceLength: {
                        date: '',
                        duration: ''
                    },
                    hoursEarnedMax: '',
                    hoursEarnedPerCycle: '',
                    carryOverAllowed: '',
                    carryOverMax: '',
                    edit: false
                });
                this.flag = true;
            }
        }
        console.log(this.tierLevels);
    };
    TierLevelModelComponent.prototype.levelNumber = function (levelNum) {
        console.log(levelNum);
    };
    // Author: Saiprakash G, Date: 29/06/19,
    // Create and update tier levels
    TierLevelModelComponent.prototype.addTierLevel = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            datetype_Of_Servicecalc: this.datetype_Of_Servicecalc,
            tierLevel_Entry: this.tierLevel_Entry,
            TierLevel_Ln: this.TierLevel_Ln,
            tierLevel: this.tierLevels
        };
        if (this.data._id) {
            var updateData = {
                tier_id: this.data._id,
                params: data
            };
            console.log(updateData, "tier levels data");
            this.leaveManagementService.updateTierLevel(updateData).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.datetype_Of_Servicecalc = null;
                _this.tierLevel_Entry = null;
                _this.TierLevel_Ln = null;
                _this.tierLevels = [];
                _this.dialogRef.close(res);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.leaveManagementService.createTierLevels(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.Message, "", "success");
                _this.datetype_Of_Servicecalc = null;
                _this.tierLevel_Entry = null;
                _this.TierLevel_Ln = null;
                _this.tierLevels = [];
                _this.dialogRef.close(res);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.Message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 31/05/19,
    // Edit tier level
    TierLevelModelComponent.prototype.editTierLevels = function () {
        this.selectedTierLevels = this.data.tierLevel.filter(function (checkTiers) {
            return checkTiers.isChecked;
        });
        console.log(this.selectedTierLevels);
        if (this.selectedTierLevels.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one tier level on to proceed", "", 'error');
        }
        else if (this.selectedTierLevels.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select a tier level on to Proceed", "", 'error');
        }
        else {
            if (this.selectedTierLevels[0]._id) {
                this.selectedTierLevels[0].edit = false;
            }
            console.log(this.selectedTierLevels[0]._id);
        }
    };
    // Author: Saiprakash G, Date: 31/05/19,
    // Delete tier level
    TierLevelModelComponent.prototype.deleteTierLevels = function () {
        var _this = this;
        this.selectedTierLevels = this.data.tierLevel.filter(function (checkTiers) {
            return checkTiers.isChecked;
        });
        if (this.selectedTierLevels.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select a tier level on to Proceed", "", 'error');
        }
        else {
            var data = {
                _id: this.data._id,
                tier_id: this.selectedTierLevels
            };
            this.leaveManagementService.deleteTierLevel(data).subscribe(function (res) {
                console.log(res);
                var tierLevelsList = [];
                _this.data.tierLevel.filter(function (element) {
                    if (_this.selectedTierLevels[0]._id != element._id) {
                        tierLevelsList.push(element);
                    }
                });
                _this.tierLevels = tierLevelsList;
                console.log(_this.data.tierLevel);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    TierLevelModelComponent.prototype.allowedCarryOver = function (event) {
        console.log(event);
        if (event == 'Yes') {
            this.hideField = true;
        }
        else {
            this.hideField = false;
        }
    };
    TierLevelModelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tier-level-model',
            template: __webpack_require__(/*! ./tier-level-model.component.html */ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.html"),
            styles: [__webpack_require__(/*! ./tier-level-model.component.css */ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.css")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"], Object])
    ], TierLevelModelComponent);
    return TierLevelModelComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.css":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".time-policies { display: block;}\n.zenwork-currentpage { padding-top: 40px;}\n.zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n.zenwork-currentpage span{ display: inline-block;}\n.mr-7{margin-right: 7px;}\n.time-policies-in { padding:40px 0 0; margin: 0 auto; float: none;}\n.time-policies-in a.btn { color:#fff; font-size: 16px; line-height: 16px; padding: 10px 20px;background:#797d7b; margin: 0 0 30px;}\n.policies-accordion { padding: 0 20px;}\n.policies-accordion .panel-default>.panel-heading {background-color:transparent !important; border: none; position: relative; padding: 10px 0;}\n.policies-accordion .panel-title { display: inline-block; font-size: 18px;}\n.policies-accordion .panel-title a:hover { text-decoration: none;}\n.policies-accordion .panel-title>.small, .policies-accordion .panel-title>.small>a, .policies-accordion .panel-title>a, .policies-accordion .panel-title>small, .policies-accordion .panel-title>small>a { text-decoration:none;}\n.policies-accordion .panel { background: none; box-shadow: none; border-radius: 0;}\n.policies-accordion .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.policies-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.policies-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .policies-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.policies-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 12px; line-height: 10px;}\n.policies-accordion .panel-body { padding: 0 0 5px;}\n.policies-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.policies-accordion .panel-default { border-color:transparent;}\n.policies-accordion .panel-group .panel+.panel { margin-top: 20px;}\n.new-policy { position: absolute; top: 0; right: 20px;}\n.new-policy a { display: inline-block; vertical-align: middle; cursor: pointer; color:#676767; font-size: 16px; line-height: 16px;}\n.new-policy a small { display: inline-block; padding: 0 15px 0 0;}\n.new-policy a small .fa { color:#008f3d; font-size: 14px; line-height: 14px;}\n.policies-table {margin: 40px 0 0 0;border-bottom:#fdeded 1px solid; padding: 0 0 60px;}\n.policies-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 10px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.policies-table .table>tbody>tr>td, .policies-table .table>tfoot>tr>td, .policies-table .table>thead>tr>td{ padding:20px 10px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#676565;}\n.policies-table .checkbox{ margin: 0;}\n/* .policies-table .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n.policies-table .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top:3px;  height:90%;} */\n.policies-table .table>tbody>tr>td span { display: inline-block; padding: 0 0 0 8px; cursor: pointer;}\n.table-icons { float: right;}\n.table-icons ul { display:block; margin: 0;}\n.table-icons ul li { display:inline-block; padding: 0 0 0 15px;}\n.table-icons ul li a { display: inline-block; cursor: pointer;}\n.table-icons ul li a .fa { color:#484747; font-size: 15px; line-height: 15px;}\n.cont-check .checkbox label { padding-left: 20px;}\n.import-balance-popup { display: block;}\n.import-balance-popup .modal-header { padding:0 60px 25px; border-bottom: 1px solid #c5c3c3;}\n/* .import-balance-popup .modal-title { color:#008f3d;} */\n.import-balance-popup .modal-dialog { width:70%;}\n.import-balance-popup .modal-content { border: none; padding:30px 0 50px;}\n.import-cont { margin:0 auto; float: none;}\n.import-cont-lft { padding: 0 0 50px; }\n.import-cont-lft label { font-weight: normal; padding: 0 0 20px; color:#616060; font-size: 17px;}\n.import-cont-rgt { padding:40px 0 0;}\n.import-cont-rgt a { display: inline-block; vertical-align:middle;}\n.import-cont-rgt a small { display: inline-block; vertical-align:middle; padding: 0 0 0 20px; font-size: 17px; color:#525151;}\n.import-cont .btn {border-radius: 20px;color:#000; font-size: 15px; padding:6px 25px; background:transparent; margin:30px 0 0; float:left;}\n.import-cont .btn:hover {background: #008f3d; color:#fff;}\n.time-policy-popup .modal-dialog { width: 85%;}\n.time-policy-popup { display: block;}\n.time-policy-popup .modal-header { padding:0 60px 25px; border-bottom: 1px solid #c5c3c3;}\n.ntimeew-policy-popup .modal-title { color:#008f3d;}\n.ntimeew-policy-popup .modal-dialog { width:70%;}\n.time-policy-popup .modal-content { border: none; padding:30px 0 0;}\n.time-policy { display: block;}\n.time-policy-popup .modal-body { padding: 0;}\n.policy-tabs { background:#f8f8f8; padding:30px 20px 0; height: 100vh;}\n.policy-tabs .nav-pills>li { float: none; margin: 0 0 10px;}\n.policy-tabs .nav-pills>li>a { color:#757272; font-size: 16px; padding: 11px 15px; display: block;}\n.policy-tabs .nav-pills>li>a small .fa { font-size: 15px; color:#fff; float: right; padding: 3px 10px 0 0; text-align: right;}\n.policy-tabs .nav>li>a:focus, .policy-tabs .nav>li>a:hover { border-radius: 30px; background: #797d7b; color:#fff;}\n.policy-tabs .nav-pills>li.active>a, .policy-tabs .nav-pills>li.active>a:focus, .policy-tabs .nav-pills>li.active>a:hover { background: #797d7b; border-radius: 30px; color:#fff;}\n.time-policy-cont { padding:30px 0 0;}\n.time-policy-type { float: none; margin: 0 auto;}\n.time-policy-type h3 { color: #008f3d; font-size: 16px; line-height: 15px; margin: 0 0 30px;}\n.policy-check { margin: 0 auto; float: none; padding:0 0 10px;border-bottom:1px solid #fdeded;}\n.policy-check ul { display: block;}\n.policy-check ul li {float: left; margin: 0 20px 0 0;}\n.policy-check ul li .selection { background:#f8f8f8; border-radius: 20px; padding: 0 33px; height:40px; line-height:40px; position: relative;}\n.policy-check ul li .selection .checkbox label { color:#636363; padding-left:0; font-size: 15px; line-height: 15px;}\n.policy-check ul li .selection .checkbox label::before { border-radius: 10px; width: 20px; height: 20px; top:-16px; left:-15px;}\n.policy-check ul li .selection .checkbox label::after { color:#fff; background: #008f3d;border-radius: 10px; width: 20px; height: 20px;\npadding: 3px 0 0 4px; top: -16px; left:-15px;}\n.policy-check ul li .selection .checkbox{ margin: 0; padding: 0;}\n.policy-check ul li .selection:active, .policy-check ul li .selection:focus, .policy-check ul li .selection:hover { background:#eef7ff;}\n/* .policy-check h4 {font-size: 16px; line-height: 15px; margin: 0; float: left; padding: 10px 0 0;} */\n.select { float:right; background:#eef7ff; border-radius:30px; padding: 0 15px; height:50px; line-height:50px; margin: 0 0 40px;}\n.select .checkbox label { color:#4c4c4c; padding-left: 10px; font-size: 16px; line-height: 20px;}\n.select .checkbox { margin: 0;}\n.select .checkbox label::before {border-radius: 10px; width: 20px; height: 20px;}\n.select .checkbox label::after { color:#fff; background: #008f3d;border-radius: 10px; width: 20px; height: 20px;padding:1px 0 0 4px;}\n.time-policy-type .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:50px 0 0;}\n.time-policy-type .btn:hover {background: #008f3d; color:#fff;}\n.policy-date { float: none; padding: 0;}\n.policy-date h5 {color: #545252; font-size: 17px;line-height: 30px;}\n.policy-apply {background:#f8f8f8; margin: 0 0 60px;}\n.policy-lft { padding: 0; border-right:#e0e0e0 1px solid;}\n.policy-lft h6 {color: #5a5757; font-size: 17px; line-height: 17px; margin: 0; background:#eef7ff; padding:20px 0 20px 45px;}\n.policy-select { margin: 0 0 20px;}\n.policy-select .checkbox label { color:#4c4c4c; padding-left: 10px; font-size: 14px; line-height: 20px;}\n.policy-select .checkbox { margin: 0;}\n.policy-select .checkbox label::before {border-radius:4px; width: 20px; height: 20px;}\n.policy-select .checkbox label::after { color:#fff; background: #008f3d;border-radius:4px; width: 20px; height: 20px;padding:1px 0 0 4px;}\n.following-hire { margin: 0 auto; float: none; padding: 0;}\n.policy-lft-in { padding: 0; float: none; margin:25px auto 0;}\n.after-select { padding:7px 0 0;}\n.policy-lft-in .following-hire .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none; background:#fff; width: 25%; margin: 0 0 0 20px;}\n.policy-lft-in .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none; background:#fff; width: 15%; margin: 0 0 0 20px;}\n.policy-lft-in small { display: block;color:#4c4c4c; padding-left: 10px; font-size: 14px; line-height: 20px; padding: 0 0 18px;}\n.policy-drop  { background:#fff; border: none; width:35%; height: 34px; color:#948e8e; padding: 6px 10px;}\n.policy-drop1 { margin: 0 0 0 20px; width:30%;}\n.policy-lft-in span {color:#4c4c4c; padding-left: 10px; font-size: 14px; line-height: 20px; padding:8px 0; display: block;}\n.employee-usage { display: block;}\n.employee-usage h2 { color: #008f3d; font-size: 16px; line-height: 15px; margin: 0 0 30px;}\n.employee-usage label {color: #545252; font-size: 17px; line-height: 17px;display: block; font-weight: normal; padding: 0 0 30px;}\n.policy-date .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:50px 0 0;}\n.policy-date .btn:hover {background: #008f3d; color:#fff;}\n.employee-usage .form-control { font-size: 15px; line-height:15px; color: #555; border: none; box-shadow: none; background:#f8f8f8; width:15%; margin: 0 0 0 40px;\npadding: 22px 10px;}\n.employee-recap { float: none;margin:0 auto; padding: 0;}\n.employee-recap a { vertical-align: middle; cursor: pointer;}\n.employee-recap a small { display: inline-block; vertical-align: middle;color: #545252; font-size: 17px; line-height: 17px; padding: 0 0 0 20px;}\n.recap-table {margin: 40px 0 0 0;}\n.recap-table .table>thead>tr>th { color: #6f6f6f; font-weight: normal;  background: #eef7ff; border:none; font-size: 16px;\nborder-radius:3px; vertical-align: middle;}\n.recap-table .table>tbody>tr>td, .recap-table .table>tfoot>tr>td, .recap-table .table>thead>tr>td{ background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#948e8e;}\n.recap-table .checkbox{ margin: 0;}\n.recap-table .checkbox label { padding-left: 0;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n.recap-table .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top:4px;height:80%; border-radius: 0 4px 4px 0; padding: 0; left: 0;}\n.employee-recap .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:50px 0 0;}\n.employee-recap .btn:hover {background: #008f3d; color:#fff;}\n.table-icons { float: right;}\n.table-icons ul { display:block; margin: 0;}\n.table-icons ul li { display:inline-block; padding: 0 0 0 15px;}\n.table-icons ul li a { display: inline-block; cursor: pointer;}\n.table-icons ul li a .fa { color:#868383; font-size: 15px; line-height: 15px;}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.border-top {\n    border-top: 1px solid #c5c3c3;\n    padding-top: 30px;\n}\n.date { height: auto !important; line-height:inherit !important; width:85% !important;     display: block;\n    background: #f8f8f8; }\n.date button { display: inline-block; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\n  height: 40px; min-width: auto; margin-left: 10px;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control {  height: auto; padding: 12px 0 12px 10px; display: inline-block;\n    vertical-align: middle; }\n.date .tier-width { width:25%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.list-width {\n    width: 30%;\n}\n#fileUpload {\n    height: 0;\n    display: none;\n    width: 0;\n  }\n.upload{\n    text-decoration: none;\n  }\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 15px;\n    padding: 0 0 0 2px;\n}\n.edu-ends-calendar-icon{\n    position: absolute;\n    top: 93px;\n    width: 15px;\n    left: 63%;\n}\n.address-state-dropodown{\n    width: 100%;\n    border-radius: 0px!important;\n    height: 40px;\n    border: 0;\n    box-shadow: none;\n}\n.birthdate{\n    font-size: 14px;\n    padding-left: 15px;\n    box-shadow: none;\n}\n.mar-top {\n    margin-top: -30px;\n}\n.mar-head {\n    margin: 35px 0px 25px 0px;\n}\n.list-width-time {\n    width:25%;\n}\n.time-bg {\n    background: #f8f8f8;\n    margin-top: 15px;\n    width: 87%;\n    padding: 10px;\n}\n.text-field1{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #fff;\n}\n.para-policy {\n    margin: 20px 0px;\n    word-spacing: 3px;\n    letter-spacing: 0.5px;\n}\n.margin-btm {\n    border-bottom: 1px solid #fdeded;\n}\n.tier-head {\n    font-size: 16px;\n    margin: 20px 0px;\n}\n.tier-width {\n    width: 20%;\n    margin-top: 10px;\n}\n.mar-top-20 {\n    margin-top: 10px;\n}\n.padding-top-btm {\n    padding: 30px 0px;\n}\n.border-tier {\n    border: none;\n    box-shadow: none;\n}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.work-flow > ul > li {position: relative;}\n.work-flow > ul > li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.wizard-lft { background: #f8f8f8; padding: 15px 15px 75px 0;border-radius: 0 0 0 5px; height:73vh;}\n.wizard-lft.custom-wizard{height:70vh;}\n.wizard-lft .nav-tabs{ border:none;}\n.wizard-lft .nav-tabs>li { float: none; margin: 0 0 15px;}\n.wizard-lft .nav-tabs > li.active > a, .wizard-lft .nav-tabs > li.active > a:focus, .wizard-lft .nav-tabs > li.active > a:hover \n{ border-bottom: none !important;background:transparent; color:#a5a3a3; border-radius: 30px;}\n.wizard-lft .nav-tabs>li>a { font-size: 17px; margin: 0 0 0 20px; color: #a5a3a3;}\n.wizard-lft .nav>li>a:focus, .wizard-lft .nav>li>a:hover { background: #787c7a; color:#fff; border-radius: 30px;}\n.wizard-lft .nav-tabs>li>a small .fa { font-size: 15px; color:#fff; float: right; padding: 3px 10px 0 0; text-align: right;}\n.back {\n    border: 1px solid #deb8b8;\n    border-radius: 25px;\n    padding: 0px 30px;\n}\n.margin-btm ul li { margin: 0 0 15px;}\n.margin-btm ul li label {display: block;}"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=time-policies>\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n      <small>\n        <img src=\"../../../../assets/images/leave-management/time.png\" width=\"24\" height=\"24\"\n          alt=\"Company-settings icon\">\n      </small>\n      <em>Time Off Policies</em>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"time-policies-in col-md-11\">\n\n\n    <div class=\"policies-accordion\">\n\n      <div class=\"panel-group\" id=\"accordion1\">\n\n        <div class=\"panel panel-default\">\n          <div class=\"panel-heading\">\n            <h4 class=\"panel-title\">\n              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n                Policies\n              </a>\n            </h4>\n\n\n            <div class=\"new-policy\">\n              <a href=\"#\" class=\"btn\" data-toggle=\"modal\" data-target=\"#myModal\">Import Balances</a>\n            </div>\n\n\n          </div>\n          <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n            <div class=\"panel-body\">\n\n\n              <div class=\"policies-table\">\n\n                <table class=\"table\">\n                  <thead>\n                    <tr>\n                      <th>\n                      </th>\n                      <th>Policy Name</th>\n                      <th>Employee Groups</th>\n                      <th>Effective Date</th>\n                      <th>Employee list</th>\n\n                      <th>\n\n                        <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n                        <mat-menu #menu=\"matMenu\" class=\"option-position\">\n                          <button mat-menu-item class=\"edit-color\">\n                            <a (click)=\"addTimePolicy()\" >Add</a>\n                            <a data-toggle=\"modal\" data-target=\"#myModal1\" #openEdit></a>\n                          </button>\n                          <button mat-menu-item class=\"edit-color\">\n                            <a (click)=\"editTimePolicy()\" >Edit</a>\n                            <a data-toggle=\"modal\" data-target=\"#myModal1\" #openEdit></a>\n                          </button>\n                          <button mat-menu-item class=\"edit-color\">Copy</button>\n                          <button mat-menu-item class=\"edit-color\" (click)=\"deleteTimeOffPolicy()\">Delete</button>\n                        </mat-menu>\n                      </th>\n                    </tr>\n                  </thead>\n                  <tbody>\n\n                    <tr *ngFor=\"let policy of getAllPolicies\">\n                      <td [ngClass]=\"{'zenwork-checked-border': policy.isChecked}\">\n                        <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"policy.isChecked\">\n                      \n                        </mat-checkbox>\n                      </td>\n                      <td>{{policy.policytype}}</td>\n                      <td><span *ngFor=\"let p of policy.leave_eligibility_group\">\n                        {{p.name}},\n                      </span></td>\n                      <td>{{policy.policy_effective_date | date: 'MM/dd/yyyy'}}</td>\n                      <td>\n                        {{policy.employee_count}} \n                        \n                        <span><img src=\"../../../../assets/images/leave-management/excel.png\" width=\"25\"\n                            height=\"23\" alt=\"img\"></span></td>\n                      <td></td>\n\n                    </tr>\n                </table>\n\n              </div>\n\n\n            </div>\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n\n  </div>\n\n\n\n  <div class=\"import-balance-popup\">\n\n    <!-- Modal -->\n    <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n\n        <!-- Modal content-->\n        <div class=\"modal-content\">\n\n          <div class=\"modal-header\">\n            <h4 class=\"modal-title\">Import Balances</h4>\n          </div>\n\n          <div class=\"modal-body\">\n\n            <div class=\"import-cont col-md-10\">\n\n              <div class=\"import-cont-lft pull-left col-md-3\">\n                <label>Select Policy Name</label>\n\n                <mat-select class=\"form-control text-field\" [(ngModel)]=\"timePolicy.policytype\" placeholder=\"Policy Name\">\n                  <mat-option *ngFor=\"let policy of allPolicies\" [value]=\"policy.name\">{{policy.name}}</mat-option>\n              \n                </mat-select>\n\n              </div>\n\n              <div class=\"import-cont-rgt pull-right\">\n              \n                  <input type=\"file\" id=\"fileUpload\" (change)=\"fileChangeEvent($event)\">\n                  <label for=\"fileUpload\">\n                    <a style=\"cursor: pointer;\"><img src=\"../../../../assets/images/leave-management/excel.png\" width=\"25\" height=\"23\"\n                      alt=\"img\">\n                    <small>Import Template</small></a>\n                      \n                    </label>\n              </div>\n              <div class=\"clearfix\"></div>\n\n\n            </div>\n            <div class=\"col-md-12 border-top\">\n              <div class=\"col-md-12\">\n\n                <button mat-button data-dismiss=\"modal\">Cancel</button>\n\n                <button mat-button class=\"submit-btn pull-right\" (click)=\"uploadImportBalance()\">Submit</button>\n              </div>\n\n\n            </div>\n\n            <div class=\"clearfix\"></div>\n\n          </div>\n\n\n        </div>\n\n      </div>\n    </div>\n\n  </div>\n\n\n  <div class=\"time-policy-popup\">\n\n    <!-- Modal -->\n    <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n\n        <!-- Modal content-->\n        <div class=\"modal-content\">\n\n          <div class=\"modal-header\">\n            <h4 class=\"modal-title\">Add / Edit Policy </h4>\n          </div>\n\n          <div class=\"modal-body\">\n\n            <div class=\"time-policy\">\n\n              <div class=\"wizard-lft col-md-3\">\n\n                <ul class=\"nav nav-tabs tabs-left\">\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab1' }\">\n                    <a href=\"#tab1\" data-toggle=\"tab\" #policy_type (click)=\"onPolicy('tab1')\">Policy Type\n                      <small><i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i></small>\n                      </a></li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab2' }\">\n                    <a href=\"#tab2\" data-toggle=\"tab\" #time_earned (click)=\"onPolicy('tab2')\">How is Time Earned?\n                        <small><i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i></small>\n                    </a></li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'tab3' }\">\n                    <a href=\"#tab3\" data-toggle=\"tab\" #recap (click)=\"onPolicy('tab3')\">Recap\n                        <small><i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i></small>\n                      </a></li>\n                    </ul>\n\n              </div>\n\n              <div class=\"time-policy-cont col-md-9\">\n\n                <div class=\"tab-content\">\n\n                  <div class=\"tab-pane\" id=\"tab1\" [ngClass]=\"{'active':selectedNav === 'tab1','in':selectedNav==='tab1'}\">\n\n                    <div class=\"time-policy-type col-md-11\">\n                      <h4>Select Policy Type</h4>\n                      <div class=\"policy-check\">\n\n                        <ul>\n\n                          <li class=\"list-width\">\n                            <mat-select class=\"form-control text-field\" [(ngModel)]=\"timePolicy.policytype\" placeholder=\"Policy type\">\n                              <mat-option *ngFor=\"let policy of allPolicies\" [value]=\"policy.name\">{{policy.name}}</mat-option>\n                          \n                            </mat-select>\n                          </li>\n\n                          <li class=\"list-width\" style=\"margin-top:-30px;\">\n                            <label>Policy Effective Date</label>\n                            \n                            <div class=\"date\">\n                              <input matInput [matDatepicker]=\"picker\" placeholder=\"mm / dd / yy\" [(ngModel)]=\"timePolicy.policy_effective_date\"\n                                class=\"form-control\">\n                              <mat-datepicker #picker></mat-datepicker>\n                              <button mat-raised-button (click)=\"picker.open()\" class=\"text-field\">\n                                <span>\n                                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                </span>\n                              </button>\n                            </div>\n                          </li>\n\n                        </ul>\n                        <div class=\"clearfix\"></div>\n\n\n                      </div>\n\n\n                      <div class=\"policy-check work-flow benefits\">\n\n                        <h4>Select Leave Eligibility Groups</h4>\n\n                        <div class=\"select\">\n                          <div class=\"checkbox\">\n                            <input id=\"checkbox10\" type=\"checkbox\" [(ngModel)]=\"timePolicy.selectall_eligibilitygroup\" (change)=\"selectAll($event)\">\n                            <label for=\"checkbox10\">Select All / Deselect All</label>\n                          </div>\n                        </div>\n\n                        <div class=\"clearfix\"></div>\n\n                        <ul>\n\n                          <li *ngFor=\"let group of leaveEligibilityGroups\" style=\"margin-top: 10px;\">\n                              <mat-checkbox [(ngModel)]=\"group.isChecked\" (change)=\"selecEligibilityGroup(group.isChecked)\">{{group.name}}</mat-checkbox>\n                      \n                          </li>\n\n                        </ul>\n                        <div class=\"clearfix\"></div>\n\n\n                      </div>\n                      <h4 class=\"mar-head\">Employee Policy Effective Date</h4>\n                      <div class=\"policy-date col-md-11\">\n\n                        <h5>When does the policy apply to an Employee</h5>\n\n                        <div class=\"policy-check\" style=\"background: #eef7ff;\">\n\n                          <div class=\"policy-lft col-md-6\">\n                            <h6>New Hire Policy Waiting Period</h6>\n\n                            <div class=\"policy-lft-in col-md-10\">\n\n                              <div class=\"policy-select\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox20\" type=\"checkbox\" [(ngModel)]=\"timePolicy.dateOfHire\" (ngModelChange)=\"dateOfHire(timePolicy.dateOfHire, 'DateOfHire')\" [disabled]=\"disableDateOfHire\">\n                                  <label for=\"checkbox20\">Date of hire</label>\n                                </div>\n                              </div>\n\n                              <div class=\"policy-select\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox21\" type=\"checkbox\" [(ngModel)]=\"timePolicy.firstMonth\" (ngModelChange)=\"dateOfHire(timePolicy.firstMonth,'FirstMonth')\" [disabled]=\"disablefirstMonth\">\n                                  <label for=\"checkbox21\">First of month..</label>\n                                </div>\n                              </div>\n\n                              <div class=\"following-hire\">\n                                <div class=\"policy-select\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox22\" type=\"checkbox\" [(ngModel)]=\"timePolicy.followingDOH\" [disabled]=\"disableFollowingDOH\">\n                                    <label for=\"checkbox22\">Following data of hire.</label>\n                                  </div>\n                                </div>\n\n                                <div class=\"policy-select after-select pull-left\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox23\" type=\"checkbox\" [(ngModel)]=\"timePolicy.after\" [disabled]=\"disableAfter\">\n                                    <label for=\"checkbox23\">after</label>\n                                  </div>\n                                </div>\n\n                                <input type=\"text\" class=\"form-control pull-left\" [(ngModel)]=\"timePolicy.afterDays\" [readOnly]=\"disableAfterDays\">\n                                <mat-select class=\"pull-left policy-drop policy-drop1\" [(ngModel)]=\"timePolicy.timerBased\" [disabled]=\"disableTimerBased\">\n                                    <mat-option value=\"Days\">Days</mat-option>\n                                    <mat-option value=\"Months\">Months</mat-option>\n                                    <mat-option value=\"Weeks\">Weeks</mat-option>\n                                  </mat-select>\n\n                                <div class=\"clearfix\"></div>\n                              </div>\n\n                              <small>Following date of hire.</small>\n\n                              <div class=\"policy-select after-select pull-left\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox25\" type=\"checkbox\" [(ngModel)]=\"timePolicy.selectDays\" (ngModelChange)=\"dateOfHire(timePolicy.selectDays,'SelectDays')\" [disabled]=\"disableSelectDays\">\n                                  <label for=\"checkbox25\"></label>\n                                </div>\n                              </div>\n\n                              <input type=\"text\" class=\"form-control pull-left\" [(ngModel)]=\"timePolicy.days\" [readOnly]=\"disableDays\">\n                              <mat-select class=\"pull-left policy-drop policy-drop1\" [(ngModel)]=\"timePolicy.timerDays\" [disabled]=\"disableTimerDays\">\n                                  <mat-option value=\"Days\">Days</mat-option>\n                                  <mat-option value=\"Months\">Months</mat-option>\n                                  <mat-option value=\"Weeks\">Weeks</mat-option>\n                                </mat-select>\n\n                              \n\n                              <div class=\"clearfix\"></div>\n\n                              <span>Following date of hire.</span>\n\n                              <div class=\"policy-select\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox26\" type=\"checkbox\" [(ngModel)]=\"timePolicy.others\" [disabled]=\"disableOther\" (ngModelChange)=\"dateOfHire(timePolicy.others,'Other')\">\n                                  <label for=\"checkbox26\">Other (Determind at the employee level)</label>\n                                </div>\n                              </div>\n\n                            </div>\n\n\n                          </div>\n\n                          <!-- <div class=\"policy-lft no-bor col-md-6\">\n                            <h6>Life Event Policy Waiting Period</h6>\n\n                            <div class=\"policy-lft-in col-md-10\">\n\n                              <div class=\"policy-select\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox30\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.dateOfHire\">\n                                  <label for=\"checkbox30\">Date of event</label>\n                                </div>\n                              </div>\n\n                              <div class=\"policy-select\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox31\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.firstMonth\">\n                                  <label for=\"checkbox31\">First of month..</label>\n                                </div>\n                              </div>\n\n                              <div class=\"following-hire col-md-10\">\n                                <div class=\"policy-select\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox32\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.followingDOH\">\n                                    <label for=\"checkbox32\">Following data of event.</label>\n                                  </div>\n                                </div>\n\n                                <div class=\"policy-select after-select pull-left\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox33\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.after\">\n                                    <label for=\"checkbox33\">after</label>\n                                  </div>\n                                </div>\n\n                                <input type=\"text\" class=\"form-control pull-left\" [(ngModel)]=\"lifeEvent.afterDays\">\n                                <select class=\"pull-right policy-drop\" [(ngModel)]=\"lifeEvent.timerBased\">\n                                  <option value=\"1\">1</option>\n                                  <option value=\"2\">2</option>\n                                  <option value=\"3\">3</option>\n                                </select>\n\n                                <div class=\"clearfix\"></div>\n                              </div>\n\n                              <small>Following data of event.</small>\n\n                              <div class=\"policy-select after-select pull-left\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox34\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.selectDays\">\n                                  <label for=\"checkbox34\"></label>\n                                </div>\n                              </div>\n\n                              <input type=\"text\" class=\"form-control pull-left\" [(ngModel)]=\"lifeEvent.days\">\n                              <select class=\"pull-left policy-drop policy-drop1\" [(ngModel)]=\"lifeEvent.timerDays\">\n                                <option value=\"Days\">Days</option>\n                                <option value=\"Months\">Months</option>\n                                <option value=\"Weeks\">Weeks</option>\n                              </select>\n\n                              <span class=\"pull-right\">Following data of hire.</span>\n\n                              <div class=\"clearfix\"></div>\n\n                              <div class=\"policy-select\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox35\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.others\">\n                                  <label for=\"checkbox35\">Other (Determind at the employee level)</label>\n                                </div>\n                              </div>\n\n                            </div>\n\n\n                          </div>  -->\n                          <div class=\"clearfix\"></div>\n                        </div>\n\n                        <div class=\"employee-usage policy-check\">\n\n                          <h4 class=\"mar-head\">Employee Usage</h4>\n\n                          <label>When can Employee Begin Using the Policy?</label>\n\n                          <mat-select placeholder=\"Number of Days\" class=\"pull-left\" [(ngModel)]=\"timePolicy.policyterm\" (ngModelChange)=\"selectEmployeeUsage($event)\">\n                            <mat-option value=\"Immediately\">Immediately</mat-option>\n                            <mat-option value=\"Number of Days\">Number of Days</mat-option>\n                            <mat-option value=\"Number of Months\">Number of Months</mat-option>\n                          </mat-select>\n                          <span *ngIf=\"showField\">\n                            <input type=\"text\" placeholder=\"\" class=\"form-control pull-left\" [(ngModel)]=\"timePolicy.policytermdays\">\n\n                          </span>\n                          <div class=\"clearfix\"></div>\n\n                        </div>\n\n                      </div>\n\n                      <div class=\"row\" style=\"margin:30px 0px;\">\n\n                        <button mat-button data-dismiss=\"modal\">Cancel</button>\n                        \n                       <button  mat-button class=\"submit-btn pull-right\" (click)=\"clickPolicyType()\">Proceed</button>\n                        \n                        \n                      </div>\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n\n                  </div>\n\n                  <div class=\"tab-pane\" id=\"tab2\" [ngClass]=\"{'active':selectedNav === 'tab2','in':selectedNav==='tab2'}\">\n                    <div class=\"time-policy-type col-md-11\">\n                      <label>How is Time Earned?</label>\n                      <div class=\"margin-btm\">\n\n                        <mat-select class=\"form-control text-field list-width\" [(ngModel)]=\"timePolicy.timeearned\">\n                          <mat-option *ngFor=\"let time of allTimeEarned\" [value]=\"time.name\">{{time.name}}</mat-option>\n                \n                        </mat-select>\n\n                        <div>\n                          <ul class=\"list-unstyled list-inline time-bg\">\n                            <li class=\"list-width-time\">\n                              <label>Pay Period Frequency</label>\n                              <mat-select class=\"form-control text-field1\" [(ngModel)]=\"timePolicy.pay_period_freq\" placeholder=\"Period frequency\">\n                                  <mat-option value=\"Weekly\">Weekly</mat-option>\n                                  <mat-option value=\"Bi-Weekly\">Bi-Weekly</mat-option>\n                                  <mat-option value=\"Semi-Monthly\">Semi-Monthly</mat-option>\n                                  <mat-option value=\"Monthly\">Monthly</mat-option>\n                              </mat-select>\n                            </li>\n                            <li class=\"list-width-time\">\n                              <label>Day of Week</label>\n                              <mat-select class=\"form-control text-field1\" [(ngModel)]=\"timePolicy.dayofweek\" placeholder=\"Day of Week\">\n                                <mat-option value=\"Monday\">Monday</mat-option>\n                                <mat-option value=\"Tuesday\">Tuesday</mat-option>\n                                <mat-option value=\"Wednesday\">Wednesday</mat-option>\n                                <mat-option value=\"Thursday\">Thursday</mat-option>\n                                <mat-option value=\"Friday\">Friday</mat-option>\n                                <mat-option value=\"Saturday\">Saturday</mat-option>\n                                <mat-option value=\"Sunday\">Sunday</mat-option>\n                             \n                              </mat-select>\n                            </li>\n                            <li class=\"list-width-time\">\n                              <label>Date of Month</label>\n                              <mat-select class=\"form-control text-field1\" [(ngModel)]=\"timePolicy.dateofMonth\" placeholder=\"Date of Month\">\n                                <mat-option value=\"First Day of Month\">First Day of Month</mat-option>                         \n                                <mat-option value=\"Last Day of Month\">Last Day of Month</mat-option>\n                              </mat-select>\n                            </li>\n                            <li class=\"list-width-time\">\n                              <label>Day of Month</label>\n                              <mat-select class=\"form-control text-field1\" [(ngModel)]=\"timePolicy.dayofmonth\" placeholder=\"Day of Month\">\n                                <mat-option value=\"First Day of Month\">First Day of Month</mat-option>                         \n                                <mat-option value=\"Last Day of Month\">Last Day of Month</mat-option>\n                              </mat-select>\n                            </li>\n                          </ul>\n                        </div>\n                        <p class=\"para-policy\"> <u>{{timePolicy.policytype}}</u>  Time is Earned <u>{{timePolicy.timeearned}} {{timePolicy.pay_period_freq}}</u>  on the <u>{{timePolicy.dateofMonth}}</u>  and <u>{{timePolicy.dayofmonth}}</u>  </p>\n                      </div>\n                      <div class=\"margin-btm\">\n                        <div>\n                            <h5 class=\"tier-head\">Tier Levels</h5>\n                            <label>User tier levels based on Years of Service</label><br>\n                            \n                            <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"timePolicy.yearsofservices\" (ngModelChange)=\"selectTierLevel($event)\">\n                                <mat-option [value]=\"true\">Yes</mat-option>\n                                <mat-option [value]=\"false\">No</mat-option>\n                              </mat-select>\n                        </div>\n                        <div *ngIf = \"showField1\">\n                            <h5 class=\"tier-head\">Tier Level Accrual Details</h5>\n                            <div class=\"policies-table\">\n                                <div class=\"recap-table\">\n                                <table class=\"table\" style=\"margin-bottom:0px;\">\n                                  <thead>\n                                    <tr>\n                                      <th>\n                                     \n                                      </th>\n                                      <th>Tier Level</th>\n                                      <th>Length of Service</th>\n                                      <th>Hours Earned Max</th>\n                                      <th>Hours Earned Rate</th>\n                                      <th>Carry over allowed?</th>\n                                      <th>Carry over max?</th>\n                                     \n                \n                                      <th>\n                \n                                        <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                                        <mat-menu #menu1=\"matMenu\" class=\"option-position\">\n                                          <button mat-menu-item class=\"edit-color\"(click)=\"tierLevelModel()\">Add</button>\n                                          <button mat-menu-item class=\"edit-color\"(click)=\"EditTierLevelModel()\">Edit</button>\n                                          <button mat-menu-item class=\"edit-color\" (click)=\"deleteTierLevels()\">Delete</button>\n                                        </mat-menu>\n                                      </th>\n                                    </tr>\n                                  </thead>\n                                  <tbody>\n                \n                                    <tr *ngFor=\"let tiers of allTierLevels; let i=index;\">\n                                      <td [ngClass]=\"{'zenwork-checked-border': tiers.isChecked}\">\n\n                                              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"tiers.isChecked\" (change)=\"checkedTierLevels($event, tiers._id)\">\n                                \n                                              </mat-checkbox>               \n                                      </td>\n                                      <td>Level {{i+1}}</td>\n                                      <td>{{tiers.serviceLength.date}}&nbsp;{{tiers.serviceLength.duration}}</td>\n                                      <td>{{tiers.hoursEarnedMax}}</td>\n                                      <td>{{tiers.hoursEarnedPerCycle}}</td>\n                                      <td>{{tiers.carryOverAllowed}}</td>\n                                      <td>{{tiers.carryOverMax}}</td>\n                                      <td></td>\n                \n                                    </tr>\n    \n                                </table>\n                                </div>\n                \n                              </div>\n                        </div>\n\n                      </div>\n                      <div class=\"margin-btm row padding-top-btm\">\n                        <div class=\"col-xs-3\">\n                          \n                              <label>Allow Negative Balance?</label>\n                            \n                              <mat-select class=\"form-control text-field mar-top-20\" [(ngModel)]=\"timePolicy.negativebalance\" (ngModelChange)=\"selectNegativeBalance($event)\"\n                              placeholder=\"Allow balance\">\n                                  <mat-option [value]=\"true\">Yes</mat-option>\n                                  <mat-option [value]=\"false\">No</mat-option>\n                                </mat-select>\n                         </div>\n                          <div class=\"col-xs-4\" *ngIf=\"showField2\">\n                              <label>Negative Balance Max -(Hours) </label>\n                              <input type=\"text\" class=\"form-control text-field mar-top-20\" [(ngModel)]=\"timePolicy.neg_max_hrs\" placeholder=\"Negative balance\">\n                          </div>\n                        \n                      </div>\n                      <div class=\"margin-btm row padding-top-btm\">\n                          <h5 class=\"tier-head\">Manage Unused Time</h5>\n                          <ul class=\"list-unstyled\">\n                            <li>\n                                <label>Does it expire?</label>\n                            \n                                <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"timePolicy.expiretime\" (ngModelChange)=\"selectExpire($event)\"\n                                placeholder=\"Does it expire\">\n                                    <mat-option [value]=\"true\">Yes</mat-option>\n                                    <mat-option [value]=\"false\">No</mat-option>\n                                  </mat-select>\n                            </li>\n                            <li *ngIf=\"showField3\">\n                                <label>Expiration Options</label>\n                            \n                                <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"timePolicy.expireoptions\" (ngModelChange)=\"selectExpireDate($event)\"\n                                placeholder=\"Options\">\n                                    <mat-option value=\"Expires at the end of the year (12/31)\">Expires at the end of the year (12/31)</mat-option>\n                                    <mat-option value=\"Expiration Date\">Expiration Date</mat-option>\n                                  </mat-select>\n                            </li>\n                            <li *ngIf=\"showField3 && showField4\" style=\"width: 25%;\">\n                              <label>Expiration Date</label>\n                              <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" [(ngModel)]=\"timePolicy.expiredate\">\n                                  <mat-datepicker #picker1></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker1.open()\" class=\"text-field\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                </div>\n                            </li>\n\n                          </ul>\n                        </div>\n                        <div class=\"row\" style=\"margin:30px 0px;\">\n\n                            <button mat-button class=\"pull-left back\" (click)=\"goToPolicy()\">Back</button>\n                            <button mat-button data-dismiss=\"modal\">Cancel</button>\n                          \n                            <button mat-button class=\"submit-btn pull-right\" (click)=\"clickTimeEarnerd()\">Proceed</button>\n                          \n                            \n                          </div>\n                          <div class=\"clearfix\"></div>\n                      \n\n                    </div>\n                  </div>\n\n\n                  <div class=\"tab-pane\" id=\"tab3\" [ngClass]=\"{'active':selectedNav === 'tab3','in':selectedNav==='tab3'}\">\n\n                    <div class=\"employee-recap col-md-11\">\n\n                      <div class=\"margin-btm\">\n\n                        <h5 class=\"tier-head\">Employee Summary</h5>\n\n                      <div class=\"recap-table\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                             \n                              <th>Employee Name</th>\n                              <th>Employee ID</th>\n                              <th>Manager name</th>\n                              <th>Department</th>\n                              <th>Effective Date</th>\n                              <th>Tier Level</th>\n                              <th>Policy Type</th>\n\n                            </tr>\n                          </thead>\n                          <tbody>\n\n                            <tr *ngFor=\"let employee of employeeList\">\n            \n                              <td>{{employee.personal.name.preferredName}}</td>\n                              <td>{{employee.job.employeeId}}</td>\n                              <td>--</td>\n                              <td>{{employee.job.department}}</td>\n                              <td>{{employee.personal.effectiveDate | date : 'MM/dd/yyyy'}}</td>\n                              <td> -- </td>\n                              <td>{{timePolicy.policytype}}</td>\n\n                            </tr>\n\n                        </table>\n                        <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                          </mat-paginator>\n                      </div>\n                    </div>\n                    <div class=\"margin-btm padding-top-btm\">\n                      <h5 class=\"tier-head\">Policy Setup Summary</h5>\n                      <uL>\n                          <li class=\"list-width\">\n                              <label> Policy Type</label>\n                              <mat-select class=\"form-control text-field\" [(ngModel)]=\"timePolicy.policytype\">\n                                <mat-option *ngFor=\"let policy of allPolicies\" [value]=\"policy.name\">{{policy.name}}</mat-option>\n                            \n                              </mat-select>\n                            </li>\n                      </uL>\n                   \n                    </div>\n                    <div class=\"margin-btm padding-top-btm\">\n                        <h4> Leave Eligibility Groups</h4>\n                        <div class=\"policy-check work-flow benefits\" style=\"border-bottom:none;\">\n                          <div class=\"select\">\n                            <div class=\"checkbox\">\n                              <input id=\"checkbox10\" type=\"checkbox\" [(ngModel)]=\"timePolicy.selectall_eligibilitygroup\" (change)=\"selectAll($event)\">\n                              <label for=\"checkbox10\">Select All / Deselect All</label>\n                            </div>\n                          </div>\n  \n                          <div class=\"clearfix\"></div>\n                        <ul>\n                          \n                          <li *ngFor=\"let group of leaveEligibilityGroups\" style=\"margin-top: 10px;\">\n                            <mat-checkbox [(ngModel)]=\"group.isChecked\" (change)=\"selecEligibilityGroup(group.isChecked)\">{{group.name}}</mat-checkbox>\n                    \n                        </li>\n                          \n\n                        </ul></div>\n                        <div class=\"clearfix\"></div>\n                    </div>\n\n                    <div class=\"margin-btm padding-top-btm\">\n\n                        <h5 class=\"tier-head\">Employee Policy Effective Date</h5>\n                        <div class=\"policy-date col-md-11\">\n  \n                          <h5>When does the policy apply to an Employee</h5>\n  \n                          <div class=\"policy-check\" style=\"background: #eef7ff; border-bottom: none;\">\n  \n                              <div class=\"policy-lft col-md-6\">\n                                  <h6>New Hire Policy Waiting Period</h6>\n      \n                                  <div class=\"policy-lft-in col-md-10\">\n      \n                                    <div class=\"policy-select\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox20\" type=\"checkbox\" [(ngModel)]=\"timePolicy.dateOfHire\" (ngModelChange)=\"dateOfHire(timePolicy.dateOfHire, 'DateOfHire')\" [disabled]=\"disableDateOfHire\">\n                                        <label for=\"checkbox20\">Date of hire</label>\n                                      </div>\n                                    </div>\n      \n                                    <div class=\"policy-select\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox21\" type=\"checkbox\" [(ngModel)]=\"timePolicy.firstMonth\" (ngModelChange)=\"dateOfHire(timePolicy.firstMonth,'FirstMonth')\" [disabled]=\"disablefirstMonth\">\n                                        <label for=\"checkbox21\">First of month..</label>\n                                      </div>\n                                    </div>\n      \n                                    <div class=\"following-hire\">\n                                      <div class=\"policy-select\">\n                                        <div class=\"checkbox\">\n                                          <input id=\"checkbox22\" type=\"checkbox\" [(ngModel)]=\"timePolicy.followingDOH\" [disabled]=\"disableFollowingDOH\">\n                                          <label for=\"checkbox22\">Following data of hire.</label>\n                                        </div>\n                                      </div>\n      \n                                      <div class=\"policy-select after-select pull-left\">\n                                        <div class=\"checkbox\">\n                                          <input id=\"checkbox23\" type=\"checkbox\" [(ngModel)]=\"timePolicy.after\" [disabled]=\"disableAfter\">\n                                          <label for=\"checkbox23\">after</label>\n                                        </div>\n                                      </div>\n      \n                                      <input type=\"text\" class=\"form-control pull-left\" [(ngModel)]=\"timePolicy.afterDays\" [readOnly]=\"disableAfterDays\">\n                                      <mat-select class=\"pull-left policy-drop policy-drop1\" [(ngModel)]=\"timePolicy.timerBased\" [disabled]=\"disableTimerBased\">\n                                          <mat-option value=\"Days\">Days</mat-option>\n                                          <mat-option value=\"Months\">Months</mat-option>\n                                          <mat-option value=\"Weeks\">Weeks</mat-option>\n                                        </mat-select>\n      \n                                      <div class=\"clearfix\"></div>\n                                    </div>\n      \n                                    <small>Following date of hire.</small>\n      \n                                    <div class=\"policy-select after-select pull-left\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox25\" type=\"checkbox\" [(ngModel)]=\"timePolicy.selectDays\" (ngModelChange)=\"dateOfHire(timePolicy.selectDays,'SelectDays')\" [disabled]=\"disableSelectDays\">\n                                        <label for=\"checkbox25\"></label>\n                                      </div>\n                                    </div>\n      \n                                    <input type=\"text\" class=\"form-control pull-left\" [(ngModel)]=\"timePolicy.days\" [readOnly]=\"disableDays\">\n                                    <mat-select class=\"pull-left policy-drop policy-drop1\" [(ngModel)]=\"timePolicy.timerDays\" [disabled]=\"disableTimerDays\">\n                                        <mat-option value=\"Days\">Days</mat-option>\n                                        <mat-option value=\"Months\">Months</mat-option>\n                                        <mat-option value=\"Weeks\">Weeks</mat-option>\n                                      </mat-select>\n      \n                                   \n                                    <div class=\"clearfix\"></div>\n\n                                    <span>Following date of hire.</span>\n      \n                                    <div class=\"policy-select\">\n                                      <div class=\"checkbox\">\n                                        <input id=\"checkbox26\" type=\"checkbox\" [(ngModel)]=\"timePolicy.others\" [disabled]=\"disableOther\" (ngModelChange)=\"dateOfHire(timePolicy.others,'Other')\">\n                                        <label for=\"checkbox26\">Other (Determind at the employee level)</label>\n                                      </div>\n                                    </div>\n      \n                                  </div>\n      \n      \n                                </div>\n  \n                            <!-- <div class=\"policy-lft no-bor col-md-6\">\n                              <h6>Life Event Policy Waiting Period</h6>\n  \n                              <div class=\"policy-lft-in col-md-10\">\n  \n                                <div class=\"policy-select\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox30\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.dateOfHire\">\n                                    <label for=\"checkbox30\">Date of event</label>\n                                  </div>\n                                </div>\n  \n                                <div class=\"policy-select\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox31\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.firstMonth\">\n                                    <label for=\"checkbox31\">First of month..</label>\n                                  </div>\n                                </div>\n  \n                                <div class=\"following-hire col-md-10\">\n                                  <div class=\"policy-select\">\n                                    <div class=\"checkbox\">\n                                      <input id=\"checkbox32\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.followingDOH\">\n                                      <label for=\"checkbox32\">Following data of event.</label>\n                                    </div>\n                                  </div>\n  \n                                  <div class=\"policy-select after-select pull-left\">\n                                    <div class=\"checkbox\">\n                                      <input id=\"checkbox33\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.after\">\n                                      <label for=\"checkbox33\">after</label>\n                                    </div>\n                                  </div>\n  \n                                  <input type=\"text\" placeholder=\"25\" class=\"form-control pull-left\" [(ngModel)]=\"lifeEvent.afterDays\">\n                                  <select class=\"pull-right policy-drop\" [(ngModel)]=\"lifeEvent.timerBased\">\n                                    <option value=\"1\">1</option>\n                                    <option value=\"2\">2</option>\n                                    <option value=\"3\">3</option>\n                                  </select>\n  \n                                  <div class=\"clearfix\"></div>\n                                </div>\n  \n                                <small>Following data of event.</small>\n  \n                                <div class=\"policy-select after-select pull-left\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox34\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.selectDays\">\n                                    <label for=\"checkbox34\"></label>\n                                  </div>\n                                </div>\n  \n                                <input type=\"text\" placeholder=\"25\" class=\"form-control pull-left\" [(ngModel)]=\"lifeEvent.days\">\n                                <select class=\"pull-left policy-drop policy-drop1\" [(ngModel)]=\"lifeEvent.timerDays\">\n                                  <option value=\"Days\">Days</option>\n                                <option value=\"Months\">Months</option>\n                                <option value=\"Weeks\">Weeks</option>\n                                </select>\n  \n                                <span class=\"pull-right\">Following data of hire.</span>\n  \n                                <div class=\"clearfix\"></div>\n  \n                                <div class=\"policy-select\">\n                                  <div class=\"checkbox\">\n                                    <input id=\"checkbox35\" type=\"checkbox\" [(ngModel)]=\"lifeEvent.others\">\n                                    <label for=\"checkbox35\">Other (Determind at the employee level)</label>\n                                  </div>\n                                </div>\n  \n                              </div>\n  \n  \n                            </div> -->\n                            <div class=\"clearfix\"></div>\n                          </div>\n  \n                          <div class=\"employee-usage policy-check\" style=\"border-bottom:none;\">\n  \n                            <h4 class=\"mar-head\">Employee Usage</h4>\n  \n                            <label>When can Employee Beging Using the Policy?</label>\n  \n                            <mat-select placeholder=\"\" class=\"pull-left\" [(ngModel)]=\"timePolicy.policyterm\" (ngModelChange)=\"selectEmployeeUsage($event)\">\n                              <mat-option value=\"Immediately\">Immediately</mat-option>\n                              <mat-option value=\"Number of Days\">Number of Days</mat-option>\n                              <mat-option value=\"Number of Months\">Number of Months</mat-option>\n                            </mat-select>\n                            <span *ngIf=\"showField\">\n                              <input type=\"text\"  class=\"form-control pull-left\" [(ngModel)]=\"timePolicy.policytermdays\">\n\n                            </span>\n                            <div class=\"clearfix\"></div>\n  \n                          </div>\n  \n                        </div>\n                      \n                      </div>\n\n                      <div class=\"margin-btm padding-top-btm\">\n                        <h5 class=\"tier-head\">How is Time Earned?</h5>\n                        <p class=\"para-policy\"> <u>{{timePolicy.policytype}}</u>  Time is Earned <u>{{timePolicy.timeearned}} {{timePolicy.pay_period_freq}}</u>  on the <u>{{timePolicy.dateofMonth}}</u>  and <u>{{timePolicy.dayofmonth}}</u>  </p>\n                      </div>\n                      <div class=\"margin-btm\">\n                          <div>\n                              <h5 class=\"tier-head\">Tier Levels</h5>\n                              <label>User tier levels based on Years of Service</label><br>\n                              \n                              <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"timePolicy.yearsofservices\" (ngModelChange)=\"selectTierLevel($event)\">\n                                  <mat-option [value]=\"true\">Yes</mat-option>\n                                  <mat-option [value]=\"false\">No</mat-option>\n                                </mat-select>\n                          </div>\n                          <div *ngIf=\"showField1\">\n                              <h5 class=\"tier-head\">Tier Level Accrual Details</h5>\n                              <div class=\"policies-table\">\n                                  <div class=\"recap-table\">\n                                  <table class=\"table\" style=\"margin-bottom:0px;\">\n                                    <thead>\n                                      <tr>\n                                      \n                                        <th>Tier Level</th>\n                                        <th>Length of Service</th>\n                                        <th>House Earned Max</th>\n                                        <th>House Earned Rate</th>\n                                        <th>Carry over allowed?</th>\n                                        <th>Carry over max?</th>\n\n                                      </tr>\n                                    </thead>\n                                    <tbody>\n                                      <tr *ngFor=\"let tiers of allTierLevels; let i=index;\">\n              \n                                        <td>Level {{i+1}}</td>\n                                        <td>{{tiers.serviceLength.date}}&nbsp;{{tiers.serviceLength.duration}}</td>\n                                        <td>{{tiers.hoursEarnedMax}}</td>\n                                        <td>{{tiers.hoursEarnedPerCycle}}</td>\n                                        <td>{{tiers.carryOverAllowed}}</td>\n                                        <td>{{tiers.carryOverMax}}</td>\n\n                                      </tr>\n                  \n                                  </table>\n                                  </div>\n                  \n                                </div>\n                          </div>\n  \n                        </div>\n                        <div class=\"margin-btm row padding-top-btm\">\n                          <div class=\"col-xs-3\">\n                            \n                                <label>Allow Negative Balance?</label><br>\n                              \n                                <mat-select class=\"form-control text-field mar-top-20\" [(ngModel)]=\"timePolicy.negativebalance\" (ngModelChange)=\"selectNegativeBalance($event)\">\n                                    <mat-option [value]=\"true\">Yes</mat-option>\n                                    <mat-option [value]=\"false\">No</mat-option>\n                                  </mat-select>\n                           </div>\n                            <div class=\"col-xs-3\" style=\"width:28%;\" *ngIf=\"showField2\">\n                                <label>Negative Balance Max -(Hours) </label><br>\n                              \n                                <input type=\"text\" class=\"form-control text-field mar-top-20\" [(ngModel)]=\"timePolicy.neg_max_hrs\">\n                                </div>\n                          \n                        </div>\n                        <div class=\"margin-btm row padding-top-btm\">\n                            <h5 class=\"tier-head\">Manage Unused Time</h5>\n                            <ul class=\"list-unstyled\">\n                              <li>\n                                  <label>Does it expire?</label><br>\n                              \n                                  <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"timePolicy.expiretime\" (ngModelChange)=\"selectExpire($event)\">\n                                      <mat-option [value]=\"true\">Yes</mat-option>\n                                      <mat-option [value]=\"false\">No</mat-option>\n                                    </mat-select>\n                              </li>\n                              <li *ngIf=\"showField3\">\n                                  <label>Expiration Options</label><br>\n                              \n                                  <mat-select class=\"form-control text-field tier-width\" [(ngModel)]=\"timePolicy.expireoptions\" (ngModelChange)=\"selectExpireDate($event)\">\n                                      <mat-option value=\"Expires at the end of the year (12/31)\">Expires at the end of the year (12/31)</mat-option>\n                                      <mat-option value=\"Expiration Date\">Expiration Date</mat-option>\n                                    </mat-select>\n                              </li>\n                              <li *ngIf=\"showField3 && showField4\" style=\"width: 25%;\">\n                                <label>Expiration Date</label>\n                                <div class=\"date\">\n                                    <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm / dd / yy\"\n                                      class=\"form-control\" [(ngModel)]=\"timePolicy.expiredate\">\n                                    <mat-datepicker #picker2></mat-datepicker>\n                                    <button mat-raised-button (click)=\"picker2.open()\" class=\"text-field\">\n                                      <span>\n                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                      </span>\n                                    </button>\n                                  </div>\n                              </li>\n  \n                            </ul>\n                          </div>\n                          <div class=\"row\" style=\"margin:30px 0px;\">\n                              <button mat-button class=\"pull-left back\" (click)=\"goToTimeEarned()\">Back</button>\n                              <button mat-button data-dismiss=\"modal\">Cancel</button>\n      \n                              <button mat-button class=\"submit-btn pull-right\" (click)=\"createTimeOffPolicy()\">Submit</button>\n                            </div>\n                            <div class=\"clearfix\"></div>\n\n                    </div>\n\n                  </div>\n\n                </div>\n\n              </div>\n              <div class=\"clearfix\"></div>\n\n\n            </div>\n\n          </div>\n\n\n        </div>\n\n      </div>\n    </div>\n\n  </div>\n  <!-- <div class=\"import-balance-popup\"> -->\n\n      <!-- Modal -->\n      <!-- <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\n        <div class=\"modal-dialog\"> -->\n  \n          <!-- Modal content-->\n          <!-- <div class=\"modal-content\">\n  \n            <div class=\"modal-header\">\n              <h4 class=\"modal-title\">Add / Edit - Tier Level - Settings</h4>\n            </div>\n  \n            <div class=\"modal-body\">\n  \n              <div class=\"import-cont col-md-10\">\n  \n                  <div class=\"margin-btm row padding-top-btm\">\n                     \n                      <ul class=\"list-unstyled\">\n                        <li>\n                            <label>Select Date Type for Length of Service Calculation</label><br>\n                        \n                            <mat-select class=\"form-control text-field tier-width\">\n                                <mat-option value=\"Hire Date\">Hire Date</mat-option>\n                                <mat-option value=\"Rehire Date\">Rehire Date</mat-option>\n                                <mat-option value=\"Custom\">Custom</mat-option>\n                              </mat-select>\n                        </li>\n                        <li>\n                            <label>Tier Level Entry Point </label><br>\n                        \n                            <mat-select class=\"form-control text-field tier-width\">\n                                <mat-option value=\"Beginning of Pay Cycle\">Beginning of Pay Cycle</mat-option>\n                                <mat-option value=\"End of Pay Cycle\">End of Pay Cycle</mat-option>\n                                <mat-option value=\"Manual Adjustment\">Manual Adjustment</mat-option>\n                              </mat-select>\n                        </li>\n                        <li>\n                          <label>Number of Tier Levels</label><br>\n                          <mat-select class=\"form-control text-field tier-width\">\n                              <mat-option value=\"Beginning of Pay Cycle\">2</mat-option>\n                              <mat-option value=\"End of Pay Cycle\">3</mat-option>\n                             \n                            </mat-select>\n                        </li>\n\n                      </ul>\n                    </div>\n                    <div class=\"margin-btm row padding-top-btm\">\n                      <h5 class=\"tier-head\">Define Tier Levels</h5>\n                        <div class=\"recap-table\">\n                            <table class=\"table\" style=\"margin-bottom:0px;\">\n                              <thead>\n                                <tr>\n                                  <th>\n                                 \n                                  </th>\n                                  <th>Tier Level</th>\n                                  <th>Length of Service</th>\n                                  <th></th>\n                                  <th>House Earned Max</th>\n                                  <th>House Earned Per Cycle</th>\n                                  <th>Carry over allowed?</th>\n                                  <th>Carry over max?</th>\n                                 \n            \n                                  <th>\n            \n                                    <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n                                    <mat-menu #menu2=\"matMenu\" class=\"option-position\"> \n                                      <button mat-menu-item class=\"edit-color\">Edit</button>\n                                      <button mat-menu-item class=\"edit-color\">Delete</button>\n                                    </mat-menu>\n                                  </th>\n                                </tr>\n                              </thead>\n                              <tbody>\n            \n                                <tr>\n                                  <td class=\"zenwork-checked-border\">\n            \n                                      <div class=\"cont-check\">\n                                          <mat-checkbox class=\"zenwork-customized-checkbox\" >\n                            \n                                          </mat-checkbox>\n                                        </div>\n            \n                                  </td>\n                                  <td>Level 1</td>\n                                  <td>\n                                    <input type=\"number\" class=\"form-control border-tier\">\n                                   \n                                  </td>\n                                  <td style=\"width:13%;\">\n                                   \n                                 <mat-select class=\"form-control border-tier\">\n                                    <mat-option value=\"Years\">Years</mat-option>\n                                   <mat-option value=\"Months\">Months</mat-option>\n                                             \n                                   </mat-select>\n                                    \n                                  </td>\n                                  <td>\n                                    <input type=\"number\" class=\"form-control border-tier\">\n                                  </td>\n                                  <td>\n                                      <input type=\"number\" class=\"form-control border-tier\">\n                                  </td>\n                                  <td>\n                                      <mat-select class=\"form-control border-tier\">\n                                          <mat-option value=\"Yes\">Yes</mat-option>\n                                          <mat-option value=\"No\">No</mat-option>\n                                         \n                                        </mat-select>\n                                  </td>\n                                  <td>\n                                      <input type=\"number\" class=\"form-control border-tier\">\n                                  </td>\n                                  <td></td>\n            \n                                </tr>\n            \n                              \n                            </table>\n                            </div>\n            \n                      \n                      </div>\n \n              </div>\n              <div class=\"col-md-12 border-top\">\n                <div class=\"col-md-12\">\n  \n                  <button mat-button data-dismiss=\"modal\">Cancel</button>\n  \n                  <button mat-button class=\"submit-btn pull-right\" (click)=\"addTierLevel()\">Submit</button>\n                </div>\n  \n  \n              </div>\n  \n              <div class=\"clearfix\"></div>\n  \n            </div>\n  \n  \n          </div>\n  \n        </div>\n      </div>\n  \n    </div> -->\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: TimePoliciesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimePoliciesComponent", function() { return TimePoliciesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _tier_level_model_tier_level_model_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tier-level-model/tier-level-model.component */ "./src/app/admin-dashboard/leave-management/time-policies/tier-level-model/tier-level-model.component.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var TimePoliciesComponent = /** @class */ (function () {
    function TimePoliciesComponent(router, myInfoService, accessLocalStorageService, leaveManagementService, dialog, swalAlertService) {
        this.router = router;
        this.myInfoService = myInfoService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.leaveManagementService = leaveManagementService;
        this.dialog = dialog;
        this.swalAlertService = swalAlertService;
        this.customPolicyType = [];
        this.defaultPolicyType = [];
        this.allPolicies = [];
        this.customTimeEarned = [];
        this.defaultTimeEarned = [];
        this.allTimeEarned = [];
        this.customPayFrequency = [];
        this.defaultPayFrequency = [];
        this.allPayFrequency = [];
        this.eligibleGroupsArray = [];
        this.employeeList = [];
        this.getAllPolicies = [];
        this.allTierLevels = [];
        this.selectedTierLevels = [];
        this.selectedPolicyType = [];
        this.selectedGroups = [];
        this.selectedGroupNames = [];
        this.leaveEligibilityGroups = [
        // {
        //   isChecked: false
        // }
        ];
        this.showField = true;
        this.showField1 = true;
        this.showField2 = true;
        this.showField3 = true;
        this.showField4 = true;
        this.tab1 = false;
        this.tab2 = false;
        this.tab3 = false;
        this.disableDateOfHire = false;
        this.disablefirstMonth = false;
        this.disableFollowingDOH = false;
        this.disableAfter = false;
        this.disableAfterDays = false;
        this.disableTimerBased = false;
        this.disableSelectDays = false;
        this.disableDays = false;
        this.disableTimerDays = false;
        this.disableOther = false;
        this.groups = [
            { name: 'Full Time', isChecked: false },
            { name: 'Part Time', isChecked: false },
            { name: 'Retirees', isChecked: false },
        ];
        this.timePolicy = {
            policytype: '',
            policy_effective_date: '',
            selectall_eligibilitygroup: false,
            dateOfHire: false,
            firstMonth: false,
            followingDOH: false,
            timerBased: '',
            selectDays: false,
            days: '',
            after: false,
            afterDays: '',
            timerDays: '',
            others: false,
            policyterm: '',
            policytermdays: '',
            timeearned: '',
            pay_period_freq: '',
            dayofweek: '',
            dateofMonth: '',
            dayofmonth: '',
            yearsofservices: '',
            negativebalance: '',
            neg_max_hrs: '',
            expiretime: '',
            expireoptions: '',
            expiredate: ''
        };
        this.lifeEvent = {
            dateOfHire: false,
            firstMonth: false,
            followingDOH: false,
            timerBased: '',
            selectDays: false,
            days: '',
            after: false,
            afterDays: '',
            timerDays: '',
            others: false,
        };
        this.selectedNav = "tab1";
        console.log(this.selectedNav);
    }
    TimePoliciesComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.getStandardAndCustomStructureFields();
        this.getAllLeaveEligibilityGroups();
        this.getAllTypePolicies();
        this.clickTimeEarnerd();
    };
    TimePoliciesComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.clickTimeEarnerd();
    };
    TimePoliciesComponent.prototype.dateOfHire = function (value, name) {
        console.log(value, name);
        if (value == true && name == 'DateOfHire') {
            this.disableDateOfHire = false;
            this.disablefirstMonth = true;
            this.disableFollowingDOH = true;
            this.disableAfter = true;
            this.disableAfterDays = true;
            this.disableTimerBased = true;
            this.disableSelectDays = true;
            this.disableDays = true;
            this.disableTimerDays = true;
            this.disableOther = true;
        }
        else if (value == true && name == 'FirstMonth') {
            this.disableDateOfHire = true;
            this.disablefirstMonth = false;
            this.disableFollowingDOH = false;
            this.disableAfter = false;
            this.disableAfterDays = false;
            this.disableTimerBased = false;
            this.disableSelectDays = true;
            this.disableDays = true;
            this.disableTimerDays = true;
            this.disableOther = true;
        }
        else if (value == true && name == 'SelectDays') {
            this.disableDateOfHire = true;
            this.disablefirstMonth = true;
            this.disableFollowingDOH = true;
            this.disableAfter = true;
            this.disableAfterDays = true;
            this.disableTimerBased = true;
            this.disableSelectDays = false;
            this.disableDays = false;
            this.disableTimerDays = false;
            this.disableOther = true;
        }
        else if (value == true && name == 'Other') {
            this.disableDateOfHire = true;
            this.disablefirstMonth = true;
            this.disableFollowingDOH = true;
            this.disableAfter = true;
            this.disableAfterDays = true;
            this.disableTimerBased = true;
            this.disableSelectDays = true;
            this.disableDays = true;
            this.disableTimerDays = true;
            this.disableOther = false;
        }
        else if (value == false) {
            this.disableDateOfHire = false;
            this.disablefirstMonth = false;
            this.disableFollowingDOH = false;
            this.disableAfter = false;
            this.disableAfterDays = false;
            this.disableTimerBased = false;
            this.disableSelectDays = false;
            this.disableDays = false;
            this.disableTimerDays = false;
            this.disableOther = false;
        }
    };
    // Author: Saiprakash G, Date: 07/06/19,
    // Active tabs in time off policy
    TimePoliciesComponent.prototype.onPolicy = function (event) {
        console.log(event);
        this.selectedNav = event;
        if (event == "tab1") {
            this.tab1 = true;
            this.tab2 = false;
            this.tab3 = false;
        }
        else if (event == 'tab2') {
            this.tab1 = false;
            this.tab2 = true;
            this.tab3 = false;
        }
        else if (event == 'tab3') {
            this.tab1 = false;
            this.tab2 = false;
            this.tab3 = true;
        }
    };
    // Author: Saiprakash G, Date: 07/06/19,
    // Active tab for policy type
    TimePoliciesComponent.prototype.clickPolicyType = function () {
        console.log("adsfsdfs");
        this.selectedNav = "tab2";
    };
    // Author: Saiprakash G, Date: 05/06/19,
    // Active tab for tiime earned and get employee list for specific eligibility groups
    TimePoliciesComponent.prototype.clickTimeEarnerd = function () {
        var _this = this;
        this.selectedNav = "tab3";
        var data = {
            benefitType: this.selectedGroups,
            page_no: this.pageNo,
            per_page: this.perPage
        };
        if (this.getPolicyData && this.getPolicyData._id) {
            data.benefitType = this.getPolicyData.leave_eligibility_group;
            this.leaveManagementService.getEmployeeList(data).subscribe(function (res) {
                console.log(res);
                _this.employeeList = res.EmployeeList;
                _this.totalLength = res.EmployeeList.length;
                _this.count = res.count;
                console.log(_this.count);
            }, function (err) {
                console.log(err);
            });
        }
        else {
            this.leaveManagementService.getEmployeeList(data).subscribe(function (res) {
                console.log(res);
                _this.employeeList = res.EmployeeList;
                // this.totalLength = res.EmployeeList.length 
                _this.count = res.count;
            }, function (err) {
                console.log(err);
            });
        }
    };
    TimePoliciesComponent.prototype.goToPolicy = function () {
        this.policy_type.nativeElement.click();
    };
    TimePoliciesComponent.prototype.goToTimeEarned = function () {
        this.time_earned.nativeElement.click();
    };
    // Author: Saiprakash G, Date: 30/05/19,
    // Get all time off policies
    TimePoliciesComponent.prototype.getAllTypePolicies = function () {
        var _this = this;
        this.leaveManagementService.getAllPolicies(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllPolicies = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash, Date:28/05/2019
    // Get Standard And Custom Structure Fields
    TimePoliciesComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customPolicyType = _this.getAllStructureFields['Policy Type'].custom;
            _this.defaultPolicyType = _this.getAllStructureFields['Policy Type'].default;
            _this.allPolicies = _this.customPolicyType.concat(_this.defaultPolicyType);
            // this.customPayFrequency = this.getAllStructureFields['Pay Frequency'].custom;
            // this.defaultPayFrequency = this.getAllStructureFields['Pay Frequency'].default;
            // this.allPayFrequency = this.customPayFrequency.concat(this.defaultPayFrequency);
            _this.customTimeEarned = _this.getAllStructureFields['Time Earned'].custom;
            _this.defaultTimeEarned = _this.getAllStructureFields['Time Earned'].default;
            _this.allTimeEarned = _this.customTimeEarned.concat(_this.defaultTimeEarned);
        });
    };
    // Author: Saiprakash G, Date: 02/07/19,
    // Get all leave eligibility groups
    TimePoliciesComponent.prototype.getAllLeaveEligibilityGroups = function () {
        var _this = this;
        this.leaveManagementService.getAllLeaveEligibilityGroup().subscribe(function (res) {
            console.log(res);
            _this.leaveEligibilityGroups = res.Result;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 07/06/19,
    // Restrict fields
    TimePoliciesComponent.prototype.selectEmployeeUsage = function (event) {
        if (event === "Immediately") {
            this.showField = false;
        }
        else {
            this.showField = true;
        }
    };
    TimePoliciesComponent.prototype.selectTierLevel = function (event) {
        if (event === true) {
            this.showField1 = true;
        }
        else {
            this.showField1 = false;
        }
    };
    TimePoliciesComponent.prototype.selectNegativeBalance = function (event) {
        if (event === true) {
            this.showField2 = true;
        }
        else {
            this.showField2 = false;
        }
    };
    TimePoliciesComponent.prototype.selectExpire = function (event) {
        if (event === true) {
            this.showField3 = true;
        }
        else {
            this.showField3 = false;
        }
    };
    TimePoliciesComponent.prototype.selectExpireDate = function (event) {
        if (event === "Expiration Date") {
            this.showField4 = true;
        }
        else {
            this.showField4 = false;
        }
    };
    // Author: Saiprakash G, Date: 07/06/19,
    // Select all and deselect all leave eligibility groups
    TimePoliciesComponent.prototype.selectAll = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.selectedGroups = [];
                        this.leaveEligibilityGroups.forEach(function (element) {
                            element.isChecked = _this.timePolicy.selectall_eligibilitygroup;
                        });
                        return [4 /*yield*/, this.leaveEligibilityGroups.forEach(function (element) {
                                if (element.isChecked) {
                                    _this.selectedGroups.push(element._id);
                                }
                            })];
                    case 1:
                        _a.sent();
                        console.log(this.selectedGroups);
                        return [2 /*return*/];
                }
            });
        });
    };
    // Author: Saiprakash G, Date: 07/06/19,
    // individual select leave eligibility groups
    TimePoliciesComponent.prototype.selecEligibilityGroup = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.selectedGroups = [];
                        if (data) {
                            this.groupCheck = 0;
                            this.leaveEligibilityGroups.forEach(function (element) {
                                if (!element.isChecked) {
                                    _this.groupCheck = 1;
                                }
                            });
                            if (!this.groupCheck) {
                                this.timePolicy.selectall_eligibilitygroup = true;
                            }
                        }
                        else {
                            this.timePolicy.selectall_eligibilitygroup = false;
                        }
                        return [4 /*yield*/, this.leaveEligibilityGroups.forEach(function (element) {
                                if (element.isChecked) {
                                    _this.selectedGroups.push(element._id);
                                }
                            })];
                    case 1:
                        _a.sent();
                        console.log(this.selectedGroups);
                        return [2 /*return*/];
                }
            });
        });
    };
    // Author: Saiprakash G, Date: 31/05/19,
    // Open tier level model
    TimePoliciesComponent.prototype.tierLevelModel = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_tier_level_model_tier_level_model_component__WEBPACK_IMPORTED_MODULE_6__["TierLevelModelComponent"], {
            width: '1200px',
            height: '800px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
            // this.tierLevelData = result.data;
            _this.tierLevelId = result.data._id;
            _this.getTierLevels();
        });
    };
    // Author: Saiprakash G, Date: 31/05/19,
    // Get tier levels
    TimePoliciesComponent.prototype.getTierLevels = function () {
        var _this = this;
        this.leaveManagementService.getTierLevel(this.companyId, this.tierLevelId).subscribe(function (res) {
            console.log(res);
            _this.tierLevelData = res.data[0];
            console.log(_this.tierLevelData);
            _this.allTierLevels = res.data[0].tierLevel;
            // this.selectedTierLevels = this.allTierLevels.filter(checkTiers =>{
            //   return checkTiers.isChecked
            // })
            // console.log(this.selectedTierLevels);
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 31/05/19,
    // Checked tier levels
    TimePoliciesComponent.prototype.checkedTierLevels = function (event, id) {
        console.log(event, id);
        for (var i = 0; i < this.allTierLevels.length; i++) {
            if (this.allTierLevels[i]._id == id) {
                this.allTierLevels[i].show = event.checked;
                if (event.checked == true) {
                    this.selectedTierLevels.push(this.allTierLevels[i]._id);
                    console.log('222222', this.selectedTierLevels);
                }
                if (event.checked == false) {
                    for (var j = 0; j < this.selectedTierLevels.length; j++) {
                        if (this.selectedTierLevels[j] === id) {
                            this.selectedTierLevels.splice(j, 1);
                            console.log(this.selectedTierLevels, "155555555");
                        }
                    }
                }
            }
        }
    };
    // Author: Saiprakash G, Date: 31/05/19,
    // Edit tier level model
    TimePoliciesComponent.prototype.EditTierLevelModel = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_tier_level_model_tier_level_model_component__WEBPACK_IMPORTED_MODULE_6__["TierLevelModelComponent"], {
            width: '1200px',
            height: '800px',
            data: this.getTierLevelData
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
            _this.getTierLevels();
        });
    };
    // Author: Saiprakash G, Date: 03/06/19,
    // Delete tier levels
    TimePoliciesComponent.prototype.deleteTierLevels = function () {
        var _this = this;
        this.selectedTierLevels = this.getTierLevelData.tierLevel.filter(function (checkTiers) {
            return checkTiers.isChecked;
        });
        if (this.selectedTierLevels.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select a tier level on to Proceed", "", 'error');
        }
        else {
            var data = {
                _id: this.getTierLevelData._id,
                tier_id: this.selectedTierLevels
            };
            this.leaveManagementService.deleteTierLevel(data).subscribe(function (res) {
                console.log(res);
                var tierLevelsList = [];
                _this.getTierLevelData.tierLevel.filter(function (element) {
                    if (_this.selectedTierLevels[0]._id != element._id) {
                        tierLevelsList.push(element);
                    }
                });
                _this.allTierLevels = tierLevelsList;
                console.log(_this.allTierLevels);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 29/05/19,
    // Create time off policy and update policy
    TimePoliciesComponent.prototype.createTimeOffPolicy = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            selectall_eligibilitygroup: this.timePolicy.selectall_eligibilitygroup,
            leave_eligibility_group: this.selectedGroups,
            policytype: this.timePolicy.policytype,
            employee_count: this.count,
            policy_effective_date: this.timePolicy.policy_effective_date,
            employee_policy_period: {
                new_hire: {
                    hire_date: {
                        selected: this.timePolicy.dateOfHire,
                        date: ""
                    },
                    first_month: {
                        selected: this.timePolicy.firstMonth,
                        following_DOH: this.timePolicy.followingDOH,
                        after_days: {
                            selected: this.timePolicy.after,
                            days: this.timePolicy.afterDays,
                            timerbased: this.timePolicy.timerBased
                        }
                    },
                    following_DOT: {
                        selected: this.timePolicy.selectDays,
                        days: this.timePolicy.days,
                        timerbased: this.timePolicy.timerDays
                    },
                    others: this.timePolicy.others
                },
                life_event: {
                    hire_date: {
                        selected: this.lifeEvent.dateOfHire,
                        date: ""
                    },
                    first_month: {
                        selected: this.lifeEvent.firstMonth,
                        following_DOH: this.lifeEvent.followingDOH,
                        after_days: {
                            selected: this.lifeEvent.after,
                            days: this.lifeEvent.afterDays,
                            timerbased: this.lifeEvent.timerBased
                        }
                    },
                    following_DOT: {
                        selected: this.lifeEvent.selectDays,
                        days: this.lifeEvent.days,
                        timerbased: this.lifeEvent.timerDays
                    },
                    others: this.lifeEvent.others
                },
                employee_usage: {
                    policyterm: this.timePolicy.policyterm,
                    policytermdays: this.timePolicy.policytermdays
                }
            },
            time_earned: {
                timeearned: this.timePolicy.timeearned,
                pay_period_freq: this.timePolicy.pay_period_freq,
                dayofweek: this.timePolicy.dayofweek,
                dateofMonth: this.timePolicy.dateofMonth,
                dayofmonth: this.timePolicy.dayofmonth,
                tireslevel: {
                    yearsofservices: this.timePolicy.yearsofservices,
                    tier_level_id: this.tierLevelId
                },
                negativebalance: this.timePolicy.negativebalance,
                neg_max_hrs: this.timePolicy.neg_max_hrs
            },
            expirationoptions: {
                expiretime: this.timePolicy.expiretime,
                expireoptions: this.timePolicy.expireoptions,
                expiredate: this.timePolicy.expiredate
            }
        };
        console.log("time-off-policy", data);
        if (this.getPolicyData && this.getPolicyData._id) {
            var updateData = {
                _id: this.getPolicyData._id,
                params: data,
            };
            updateData.params.leave_eligibility_group = this.getPolicyData.leave_eligibility_group;
            console.log(updateData.params.leave_eligibility_group);
            this.leaveManagementService.updateTimePolicy(updateData).subscribe(function (res) {
                console.log(res);
                jQuery("#myModal1").modal("hide");
                _this.getAllTypePolicies();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.leaveManagementService.createTimeOffPolicies(data).subscribe(function (res) {
                console.log(res);
                jQuery("#myModal1").modal("hide");
                _this.getAllTypePolicies();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.Message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.Message, "", "error");
            });
        }
    };
    TimePoliciesComponent.prototype.addTimePolicy = function () {
        this.selectedNav = "tab1";
        this.openEdit.nativeElement.click();
    };
    // Author: Saiprakash G, Date: 04/06/19,
    // edit time policy
    TimePoliciesComponent.prototype.editTimePolicy = function () {
        var _this = this;
        this.selectedNav = "tab1";
        this.selectedPolicyType = this.getAllPolicies.filter(function (policyCheck) {
            return policyCheck.isChecked;
        });
        if (this.selectedPolicyType.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one time off policy to Proceed", "", 'error');
        }
        else if (this.selectedPolicyType.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select a time off policy to Proceed", "", 'error');
        }
        else {
            this.openEdit.nativeElement.click();
            this.leaveManagementService.getTimeOffPolicy(this.selectedPolicyType[0]._id).subscribe(function (res) {
                console.log(res);
                // this.getTierLevels();
                _this.getPolicyData = res.policyData;
                if (res.tierLevelData) {
                    _this.getTierLevelData = res.tierLevelData[0];
                    console.log(_this.getTierLevelData);
                }
                _this.timePolicy = _this.getPolicyData;
                _this.timePolicy.selectall_eligibilitygroup = _this.getPolicyData.selectall_eligibilitygroup;
                if (_this.getPolicyData.selectall_eligibilitygroup === true) {
                    _this.groups.forEach(function (element) {
                        element.isChecked = _this.timePolicy.selectall_eligibilitygroup;
                    });
                }
                _this.leaveEligibilityGroups.forEach(function (element) {
                    console.log("iddddddddd", element._id);
                    if (_this.getPolicyData.leave_eligibility_group.indexOf(element._id) >= 0) {
                        element.isChecked = true;
                    }
                });
                console.log(_this.getPolicyData.leave_eligibility_group);
                _this.timePolicy.dateOfHire = _this.getPolicyData.employee_policy_period.new_hire.hire_date.selected;
                _this.timePolicy.firstMonth = _this.getPolicyData.employee_policy_period.new_hire.first_month.selected;
                _this.timePolicy.after = _this.getPolicyData.employee_policy_period.new_hire.first_month.after_days.selected;
                _this.timePolicy.afterDays = _this.getPolicyData.employee_policy_period.new_hire.first_month.after_days.days;
                _this.timePolicy.timerBased = _this.getPolicyData.employee_policy_period.new_hire.first_month.after_days.timerbased;
                _this.timePolicy.selectDays = _this.getPolicyData.employee_policy_period.new_hire.following_DOT.selected;
                _this.timePolicy.followingDOH = _this.getPolicyData.employee_policy_period.new_hire.first_month.following_DOH;
                _this.timePolicy.days = _this.getPolicyData.employee_policy_period.new_hire.following_DOT.days;
                _this.timePolicy.timerDays = _this.getPolicyData.employee_policy_period.new_hire.following_DOT.timerbased;
                _this.timePolicy.others = _this.getPolicyData.employee_policy_period.new_hire.others;
                _this.lifeEvent.dateOfHire = _this.getPolicyData.employee_policy_period.life_event.hire_date.selected;
                _this.lifeEvent.firstMonth = _this.getPolicyData.employee_policy_period.life_event.first_month.selected;
                _this.lifeEvent.after = _this.getPolicyData.employee_policy_period.life_event.first_month.after_days.selected;
                _this.lifeEvent.afterDays = _this.getPolicyData.employee_policy_period.life_event.first_month.after_days.days;
                _this.lifeEvent.timerBased = _this.getPolicyData.employee_policy_period.life_event.first_month.after_days.timerbased;
                _this.lifeEvent.selectDays = _this.getPolicyData.employee_policy_period.life_event.following_DOT.selected;
                _this.lifeEvent.followingDOH = _this.getPolicyData.employee_policy_period.life_event.first_month.following_DOH;
                _this.lifeEvent.days = _this.getPolicyData.employee_policy_period.life_event.following_DOT.days;
                _this.lifeEvent.timerDays = _this.getPolicyData.employee_policy_period.life_event.following_DOT.timerbased;
                _this.lifeEvent.others = _this.getPolicyData.employee_policy_period.life_event.others;
                _this.timePolicy.policyterm = _this.getPolicyData.employee_policy_period.employee_usage.policyterm;
                _this.timePolicy.policytermdays = _this.getPolicyData.employee_policy_period.employee_usage.policytermdays;
                if (_this.timePolicy.policytermdays === "") {
                    _this.showField = false;
                }
                _this.timePolicy.timeearned = _this.getPolicyData.time_earned.timeearned;
                _this.timePolicy.pay_period_freq = _this.getPolicyData.time_earned.pay_period_freq;
                _this.timePolicy.dayofweek = _this.getPolicyData.time_earned.dayofweek;
                _this.timePolicy.dateofMonth = _this.getPolicyData.time_earned.dateofMonth;
                _this.timePolicy.dayofmonth = _this.getPolicyData.time_earned.dayofmonth;
                _this.timePolicy.yearsofservices = _this.getPolicyData.time_earned.tireslevel.yearsofservices;
                console.log(_this.timePolicy.yearsofservices);
                if (_this.timePolicy.yearsofservices == true) {
                    _this.showField1 = true;
                }
                else if (_this.timePolicy.yearsofservices == false) {
                    _this.showField1 = false;
                }
                if (res.tierLevelData) {
                    _this.allTierLevels = res.tierLevelData[0].tierLevel;
                }
                _this.timePolicy.negativebalance = _this.getPolicyData.time_earned.negativebalance;
                if (_this.getPolicyData.time_earned.negativebalance == true) {
                    _this.showField2 = true;
                }
                else {
                    _this.showField2 = false;
                }
                _this.timePolicy.neg_max_hrs = _this.getPolicyData.time_earned.neg_max_hrs;
                _this.timePolicy.expiretime = _this.getPolicyData.expirationoptions.expiretime;
                if (_this.getPolicyData.expirationoptions.expiretime) {
                    _this.showField3 = true;
                }
                else {
                    _this.showField3 = false;
                }
                _this.timePolicy.expireoptions = _this.getPolicyData.expirationoptions.expireoptions;
                if (_this.getPolicyData.expirationoptions.expireoptions == "Expiration Date") {
                    _this.showField4 = true;
                }
                else {
                    _this.showField4 = false;
                }
                _this.timePolicy.expiredate = _this.getPolicyData.expirationoptions.expiredate;
                _this.eligibleGroupsArray = _this.getPolicyData.leave_eligibility_group;
                _this.tierLevelId = _this.getPolicyData.time_earned.tireslevel.tier_level_id[0];
                console.log(_this.tierLevelId);
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 04/06/19,
    // Delete time policy
    TimePoliciesComponent.prototype.deleteTimeOffPolicy = function () {
        var _this = this;
        this.selectedPolicyType = this.getAllPolicies.filter(function (policyCheck) {
            return policyCheck.isChecked;
        });
        if (this.selectedPolicyType.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select a time off policy on to Proceed", "", 'error');
        }
        else {
            var data = {
                _id: this.selectedPolicyType
            };
            this.leaveManagementService.deleteTimeOffPolicies(data).subscribe(function (res) {
                console.log(res);
                _this.getAllTypePolicies();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.Message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 12/06/19,
    // Upload import balances file
    TimePoliciesComponent.prototype.fileChangeEvent = function (e) {
        this.files = e.target.files[0];
        console.log(this.files.name);
    };
    TimePoliciesComponent.prototype.uploadImportBalance = function () {
        var _this = this;
        var formData = new FormData();
        formData.append('file', this.files);
        formData.append('companyId', this.companyId);
        this.leaveManagementService.uploadImportBalance(formData).subscribe(function (res) {
            console.log(res);
            _this.files = "";
            jQuery("#myModal").modal("hide");
        }, function (err) {
            console.log(err);
        });
    };
    TimePoliciesComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/leave-management']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimePoliciesComponent.prototype, "openEdit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('policy_type'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimePoliciesComponent.prototype, "policy_type", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('time_earned'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimePoliciesComponent.prototype, "time_earned", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('recap'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimePoliciesComponent.prototype, "recap", void 0);
    TimePoliciesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-time-policies',
            template: __webpack_require__(/*! ./time-policies.component.html */ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.html"),
            styles: [__webpack_require__(/*! ./time-policies.component.css */ "./src/app/admin-dashboard/leave-management/time-policies/time-policies.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_4__["LeaveManagementService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_7__["SwalAlertService"]])
    ], TimePoliciesComponent);
    return TimePoliciesComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.css":
/*!********************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.css ***!
  \********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.work-summery {margin:0;}\n.work-summery h2 { font-size: 20px;}\n.work-details { background: #787d7a; padding:15px 10px; margin: 25px 0 0;border-radius: 5px;}\n.work-details-top {margin: 0 0 20px;}\n.work-details-top ul { display: block;}\n.work-details-top ul li {display: inline-block; vertical-align: middle; float: none;}\n.work-details-top ul li label{color: #fff; font-weight: normal; font-size: 17px;}\n.work-details-top ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%;\nbackground:#fff;}\n.work-details-top ul li .form-control:focus{ box-shadow: none;}\n.work-details-mdl { background: #fff; padding:20px; border-radius:5px; margin: 0 0 20px;}\n.work-details-mdl h2 { margin: 0 0 7px; color: #3a3c3b; font-size:20px; font-weight: 500;}\n.work-details-mdl h4 { margin: 0; color: #787d7a; font-size: 16px;}\n.work-days {display: inline-block;width: 100%;margin:0 0 10px;}\n.work-days ul { display: block; text-align: center;}\n.work-days ul li { float: none; display: inline-block;}\n.work-days ul li h5 { color:#737070; padding:0 0 10px; font-size: 18px;}\n.work-days ul li figure img { margin: 0 auto 20px;}\n.vacation { position: relative; text-align: left; padding: 0 0 0 40px;}\n.vacation span { display: inline-block; font-size: 14px; color: #ccc;}\n.vacation span b { display: inline-block; font-size: 22px; color: #bfbcbc; padding: 0 7px 0 0;}\n.vacation small { display:block; font-size: 14px; color: #737070;}\n.vacation:after { content:''; position: absolute; top:5px; left:20px; border-left: #23a4db 5px solid; height:90%;}\n.vacation.sick:after { content:''; position: absolute; top:5px; left:20px; border-left: #fad32b 5px solid; height:90%;}\n.vacation.breave:after { content:''; position: absolute; top:5px; left:20px; border-left: #7ac650 5px solid; height:90%;}\n.vacation.duty:after { content:''; position: absolute; top:5px; left:20px; border-left: #52d3c8 5px solid; height:90%;}\n.working-inform { background: #fff;border-radius:5px; padding:25px 0 20px; margin: 0 0 20px;}\n.work-heading { padding: 0 0 45px;}\n.working-inform .work-days ul li { width: 20%; position: relative;}\n.working-inform .work-days ul li:after{ content: \"\"; position: absolute; top:20%; right:8px;\nborder-right: #cecece 1px solid; height:70%;}\n.working-inform .vacation span {padding:20px 0 0; vertical-align: bottom;}\n.working-inform .work-days ul li:nth-last-child(1):after{ border-right: none;}\n.work-details-btm { display: block;}\n.work-details-btm .table{ background:#fff;margin-bottom:0;border-radius: 5px;}\n.work-details-btm .table>tbody>tr>th, .work-details-btm .table>thead>tr>th {padding:15px; color: #484747; font-weight: normal;\nfont-size:18px;vertical-align: middle; position: relative; background: #edf7ff;border-radius: 5px;}\n.work-details-btm .table>tbody>tr>td, .work-details-btm .table>tfoot>tr>td, .work-details-btm .table>tfoot>tr>th, \n.work-details-btm .table>thead>tr>td {color: #484747;padding:20px 15px;font-size:15px; background: #fff;border-radius: 5px;}\n.edit-work { float: none; margin: 0 auto; padding: 0;height: 680px;overflow-y: auto;}\n.edit-work ul {border-bottom: #e5e5e5 1px solid; padding:25px 0; display: inline-block; width: 100%;}\n.edit-work ul li {display:block; margin: 0 0 10px;}\n.edit-work ul li label{color: #484747; font-weight: normal; font-size: 17px; padding: 0 0 10px; display: block;}\n.edit-work ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%;\nbackground:#f8f8f8;}\n.edit-work ul li .form-control:focus{ box-shadow: none;}\n.edit-work ul li .date { height: auto !important; line-height:inherit !important; width:80% !important;background:#f8f8f8;}\n.edit-work ul li .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.edit-work ul li .date span { border: none !important; padding: 0 !important;}\n.edit-work ul li .date .form-control { width:77%; height: auto; padding: 12px 0 12px 10px;}\n.edit-work ul li .date .form-control:focus { box-shadow: none; border: none;}\n.edit-work ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.circle { margin: 40px 0 0;}\n.circle a { cursor: pointer;}\n.circle a .fa{ color:#5d5d5d; font-size: 25px;}\n.date1 { height: auto !important; line-height:inherit !important; width:66% !important;background:#fff;}\n.edit-work .date1 button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none;\nwidth: 40px;height: 30px; min-width: auto; border-left: #ccc 1px solid; border-radius: 0;}\n.date1 span { border: none !important; padding: 0 !important;}\n.date1 .form-control { width:73%; height: auto; padding: 12px 0 12px 10px; border: none; box-shadow: none; display: inline-block;}\n.date1 .form-control:focus { box-shadow: none; border: none;}\n.date1 span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.edit-work .table{ background:#fff; margin: 0;}\n.edit-work .table>tbody>tr>th, .edit-work .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:17px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.edit-work .table>tbody>tr>td, .edit-work .table>tfoot>tr>td, .edit-work .table>tfoot>tr>th, \n.edit-work .table>thead>tr>td {color: #484747;padding:20px 15px;font-size:15px; background: #f7f7f7;}\n.edit-work button.mat-menu-item a { color:#ccc;}\n.edit-work button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.edit-work .table>tbody>tr>th a, .edit-work .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer; font-size: 15px;}\n.edit-work .table>tbody>tr>th a .fa, .edit-work .table>thead>tr>th a .fa { color: #6f6d6d;}\n.edit-work button { border: none; outline: none; background:transparent;}\n.circle-popup { margin: 40px 0 0; padding: 50px 0; text-align: center;}\n.circle-popup a { cursor: pointer; display: inline-block; padding: 0 0 20px;}\n.circle-popup a .fa{ color:#5d5d5d; font-size: 45px;}\n.circle-popup p { color: #7b7b7b; font-size:20px; line-height:30px;}\n.circle-popup p b { display: inline-block; font-weight: bold;}\n.circle-popup .btn { background: #099247; border: none; border-radius:30px; padding: 8px 35px; color: #fff; font-size:20px; margin: 30px 0 0;}\n.time-sheet-bg {\n    background: #f8f8f8;\n    padding: 10px 0px 24px 18px;\n  }\n.search-bg span { float: right; width: 10%; text-align: center;}\n.search-bg span .fa { color:#777676; font-size: 14px; padding: 12px 0 0; cursor: pointer;}\n.search-bg {\n    margin-left: -35px;\n  }\n.search-box {\n    outline: none;\n    border: none;\n    background: #fff;\n    box-shadow: none;\n  }\n.search-icon {\n    position: absolute;\n    top: 0px;\n    right: 23px;\n  }\n.time { border-right:#9bcbec 1px solid; width:4%; height: 18px; display: inline-block; cursor: pointer; vertical-align: middle;}\n.check { width:20px; height:20px; background: #099247; line-height: 18px; border-radius:50%; cursor: pointer; margin: 0; text-align: center; margin:0 auto;}\n.check .fa { color: #fff; font-size: 14px; line-height: 14px;}\n.check.info {background: #eac907;}\n.check.info .fa {-webkit-transform:rotateX(180deg);transform:rotateX(180deg);}\n.check.wrong {background: #f00;}\n.check.wrong .fa {-webkit-transform:rotateX(180deg);transform:rotateX(180deg);}\n.attendance { display: block; position: relative; cursor: pointer;}\n.attendance span { display:inline-block;}\n.itsAbsolute{position: absolute;background-color:#64c0f5;}\n.tooltip1 {\n      position: relative;\n      display:block; height: 22px;\n      width:100%;\n    }\n/* .tooltip1::after { content:'';position:absolute; top: 0; left: 0;\n      border-top:#f00 1px solid;border-right:transparent 1px solid;border-left:transparent 1px solid;}\n     */\n.tooltip1 .tooltiptext {\n      visibility: hidden;\n      width:200px;\n      background-color: #555;\n      color: #fff;\n      text-align: center;\n      border-radius:4px;\n      padding: 5px 21px;\n      position: absolute;\n      z-index: 1;\n      bottom: 125%;\n      left: 50%;\n      margin-left: -60px;\n      opacity: 0;\n      transition: opacity 0.3s; line-height: 25px;\n    }\n.tooltip1 .tooltiptext::after {\n      content: \"\";\n      position: absolute;\n      top: 100%;\n      left: 50%;\n      margin-left: -5px;\n      border-width: 5px;\n      border-style: solid;\n      border-color: #555 transparent transparent transparent;\n    }\n.tooltip1:hover .tooltiptext {\n      visibility: visible;\n      opacity: 1;\n    }\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.html":
/*!*********************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.html ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"work-schedule-top col-md-12\">\n    <h4>Employee Time Sheet</h4>\n    <div class=\"row time-sheet-bg\">\n      <div class=\"col-xs-6\">\n        <h4 class=\"col-xs-4\">Employee Name:</h4> \n          <span class=\"col-xs-5 search-bg\">\n              <div >\n                  <input type=\"text\" class=\"form-control search-box\" placeholder=\"Search Employee Name\">\n                  <span>\n                    <i class=\"fa fa-search search-icon\" aria-hidden=\"true\"></i>\n                  </span>\n                </div>\n          </span>\n        \n      </div>\n      <div class=\"col-xs-6\">\n        <ul style=\"font-size:16px;\">\n          <li>\n            <p>Work Schedule Name: <span class=\"green\"> &nbsp; &nbsp;<u>Customer Service 1</u></span></p>\n          </li>\n          <li>\n              <p>Work Schedule Type: <span class=\"green\">&nbsp; &nbsp;<u>Standard</u></span></p>\n            </li>\n        </ul>\n      </div>\n    </div>\n   \n  </div>\n  \n  \n  \n  <div class=\"work-summery col-md-12\">\n       <h4>Time Sheet - Summary / Details</h4>\n  \n    <div class=\"work-details\">\n  \n      <div class=\"work-details-top\">\n          <ul class=\"col-md-6\">\n            <li class=\"col-md-4\"><label> Select Date Range:</label></li>\n            <li class=\"col-md-5\">\n                <mat-select class=\"form-control\" placeholder=\"Month\">\n                  <mat-option value=''>Month</mat-option>\n                  <mat-option value=''>Month</mat-option>\n                </mat-select>\n            </li>\n          </ul>\n  \n          <div class=\"work-years pull-right\">\n  \n          </div>\n          <div class=\"clearfix\"></div>\n      </div>\n  \n  \n      <div class=\"work-details-mdl\">\n  \n        <div class=\"work-heading\">\n  \n        <div class=\"pull-left\">\n          <h2>Monday - Friday</h2>\n          <h4>Schedule Work Days</h4>\n        </div>\n  \n        <div class=\"pull-right\">\n            <h2>10:00 AM - 07:00 PM</h2>\n            <h4>Schedule Start Time / Finish Time</h4>\n        </div>\n        <div class=\"clearfix\"></div>\n  \n      </div>\n  \n        <div class=\"work-days\">\n  \n            <ul>\n              <li class=\"col-md-3\">\n              \n                  <figure><img src=\"../../../../assets/images/dashboard/vacation.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation\">\n                    <span><b>{{arrivalDetails.averageHours.hours}}:{{arrivalDetails.averageHours.minutes}}</b></span>\n                    <small>Average Hours Per Day</small>\n                  </div>\n    \n              </li>\n    \n              <li class=\"col-md-3\">\n  \n                  <figure><img src=\"../../../../assets/images/dashboard/breavment.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation breave\">\n                    <span><b>{{arrivalDetails.ontimeArrival}}%</b></span>\n                    <small>% On Time Arrival</small>\n                  </div>\n              </li>\n    \n              <li class=\"col-md-3\">\n  \n                  <figure><img src=\"../../../../assets/images/dashboard/sick.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation sick\">\n                    <span><b>{{arrivalDetails.earlyArrivalCount}}</b></span>\n                    <small>Early Arrival Count</small>\n                  </div>  \n              </li>\n    \n              <li class=\"col-md-3\">\n              \n                  <figure><img src=\"../../../../assets/images/dashboard/duty.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation duty\">\n                    <span><b>{{arrivalDetails.lateArrivalCount}}</b></span>\n                    <small>Late Arrival Count</small>\n                  </div>\n              </li>\n    \n            </ul>\n    \n          </div>\n  \n        </div>\n  \n    <div class=\"working-inform\">\n  \n        <div class=\"work-days\">\n  \n            <ul>\n              <li>\n  \n                <h5>Hours Worked</h5>\n                  <figure><img src=\"../../../../assets/images/dashboard/vacation.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation\">\n                      <span><b>{{arrivalDetails.totalHoursWorked.hours}}:{{arrivalDetails.totalHoursWorked.minutes}}</b>Hours Worked</span>\n                  </div>\n    \n              </li>\n  \n  \n              <li>\n  \n                  <h5>vacation</h5>\n                    <figure><img src=\"../../../../assets/images/dashboard/vacation.png\" alt=\"img\"></figure>\n      \n                    <div class=\"vacation\">\n                      <span><b>0.0</b>Hours Used</span>\n                    </div>\n      \n                </li>\n    \n    \n              <li>\n                \n                  <h5>Sick</h5>\n                  <figure><img src=\"../../../../assets/images/dashboard/breavment.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation breave\">\n                      <span><b>0.0</b>Hours Used</span>\n                  </div>  \n              </li>\n    \n              <li>\n              \n                  <h5>Breavement</h5>\n  \n                  <figure><img src=\"../../../../assets/images/dashboard/sick.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation sick\">\n                      <span><b>0.0</b>Hours Used</span>\n                  </div>\n              </li>\n  \n  \n              <li>\n              \n                  <h5>Over Time</h5>\n  \n                  <figure><img src=\"../../../../assets/images/dashboard/duty.png\" alt=\"img\"></figure>\n    \n                  <div class=\"vacation duty\">\n                      <span><b>0.0</b>Hours Used</span>\n                  </div>\n              </li>\n    \n            </ul>\n    \n          </div>\n    </div>\n  \n      <div class=\"work-details-btm\">\n  \n        <table class=\"table\">\n          <thead>\n            <tr>\n              <th>Day</th>\n              <th>Date</th>\n              <th>Arrival</th>\n              <th>Hours Worked</th>\n              <th>Total Hours</th>\n              <th>Visual Attendance</th>\n              <th>Log</th>\n             </tr>\n          </thead>\n          <tbody> \n            <tr *ngFor=\"let data of timeInsideData\">\n              <td>{{data.key | date:'EEEE'}}</td>\n              <td>{{data.key | date:'MM/dd/yyyy'}}</td>\n              <td>\n                <span *ngIf=\"data.value.lateByTime\">{{data.value.lateByTime.hours}}:{{data.value.lateByTime.minutes}} Late</span>\n              </td>\n              <td>\n                  <span *ngIf=\"data.value.hoursworked\">{{data.value.hoursworked.hours}}:{{data.value.hoursworked.minutes}} </span>\n\n              </td>\n              <td>\n                  <span *ngIf=\"data.value.grosshours\">{{data.value.grosshours.hours}}:{{data.value.grosshours.minutes}} </span>\n\n              </td>\n              <td>\n                  <div class=\"attendance\">\n                      <span *ngFor=\"let time of data.schedules\" class=\"itsAbsolute\" [ngStyle]=\"{'left':time.marginLeft+'%', 'width':time.width+'%'}\">\n        \n                        <div class=\"tooltip1\">\n                          <span class=\"tooltiptext\">{{getToolTimeData(time)}}</span>\n                        </div>\n        \n                      </span>\n        \n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n                    <small class=\"time\"></small>\n        \n                    </div>\n              </td>\n              <td>\n\n            <p class=\"check\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></p>\n              </td>\n            </tr>\n    \n  \n          </tbody>\n      </table>\n  \n  \n      </div>\n  \n    </div>\n  \n  \n  </div>\n  \n  \n  \n  \n  \n  <!-- Work Schedule History -->\n  <div class=\"work-history\">\n  \n      <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n    \n          <div class=\"modal-content\">\n            <div class=\"modal-header\">\n              <h4 class=\"modal-title\">Work Schedule History</h4>\n            </div>\n            <div class=\"modal-body\">\n              \n              <div class=\"history-table col-md-11\">\n  \n                  <table class=\"table\">\n                      <thead>\n                        <tr>\n                          <th>Position Schedule Name</th>\n                          <th>Effective Date</th>\n                          <th>End Date</th>\n                         </tr>\n                      </thead>\n                      <tbody> \n                        <tr>\n                          <td>Suresh M</td>\n                          <td>9/10/2012</td>\n                          <td>9/10/2012</td>\n                        </tr>\n  \n                         <tr>\n                            <td>Suresh M</td>\n                            <td>9/10/2012</td>\n                            <td>9/10/2012</td>\n                          </tr>\n                        \n                      </tbody>\n                  </table>\n  \n  \n                  <button class=\"btn\" data-dismiss=\"modal\">Ok</button>\n  \n              </div>\n    \n            </div>\n    \n          </div>\n    \n        </div>\n      </div>\n    \n    </div>\n  \n  \n  <!-- Edit Work Schedule -->\n    <div class=\"work-history\">\n  \n        <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n          <div class=\"modal-dialog\">\n      \n            <div class=\"modal-content\">\n              <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Edit Work Schedule</h4>\n              </div>\n              <div class=\"modal-body\">\n                \n               <div class=\"edit-work col-md-11\">\n  \n                  <ul>\n                    <li class=\"col-md-3\">\n                      <label>Work Schedule Title</label>\n                      <mat-select class=\"form-control\" placeholder=\"Title\">\n                        <mat-option value=''>Suresh</mat-option>\n                        <mat-option value=''>Clerk</mat-option>\n                      </mat-select>\n                    </li>\n  \n                    <li class=\"col-md-3\">\n                      <label>Work Schedule Start Date</label>\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                        <mat-datepicker #picker2></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker2.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </li>\n  \n                  </ul>\n                  <div class=\"clearfix\"></div>\n  \n  \n                  <ul>\n  \n                    <li><mat-checkbox>Select Days of the Week Applicable</mat-checkbox></li>\n                    <br>\n                    <li><mat-checkbox>Monday - Friday</mat-checkbox></li>\n                    <li><mat-checkbox>Monday</mat-checkbox></li>\n                    <li><mat-checkbox>Tuesday</mat-checkbox></li>\n                    <li><mat-checkbox>Wednesday</mat-checkbox></li>\n                    <li><mat-checkbox>Thursday</mat-checkbox></li>\n                    <li><mat-checkbox>Friday</mat-checkbox></li>\n                    <li><mat-checkbox>Saturday</mat-checkbox></li>\n                    <li><mat-checkbox>Sunday</mat-checkbox></li>\n                  </ul>\n  \n  \n                  <ul>\n                    <li class=\"col-md-2\">\n                      <label>Total Hours per Day</label>\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Total Hours Day\">\n                    </li>\n  \n                    <li class=\"col-md-2\">\n                      <label>Total Hours per Week</label>\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Total Hours Week\">\n                    </li>\n                    <div class=\"clearfix\"></div>\n  \n                    <li class=\"col-md-3\">\n                      <label>Schedule Type</label>\n                      <mat-select class=\"form-control\" placeholder=\"Type\">\n                        <mat-option value='Standard'>Standard</mat-option>\n                        <mat-option value='Employee'>Employee</mat-option>\n                      </mat-select>\n  \n                    \n                    </li>\n  \n                    <li class=\"col-md-1\">\n                      <div class=\"circle\">\n                          <a data-toggle=\"modal\" data-target=\"#myModal2\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i></a>\n                      </div>\n                    </li>\n  \n  \n                  </ul>\n                  <div class=\"clearfix\"></div>\n  \n                  <ul class=\"no-bor\">\n                    \n                      <li class=\"col-md-3\">\n                        <label>Set Lunch Break Time?</label>\n                        <mat-select class=\"form-control\" placeholder=\"Lunch Break\">\n                          <mat-option value='Yes'>Yes</mat-option>\n                          <mat-option value='No'>No</mat-option>\n                        </mat-select>\n    \n                      </li>\n    \n                    </ul>\n                    <div class=\"clearfix\"></div>\n  \n                  \n                    <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>\n                              <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n                            </th>\n  \n                            <th>Day</th>\n                            <th>Start Time</th>\n                            <th>Lunch Start Time</th>\n                            <th>Lunch End Time</th>\n                            <th>End Time</th>\n                          \n                            <th>\n                              <button mat-icon-button [matMenuTriggerFor]=\"menu1\"\n                                aria-label=\"Example icon-button with a menu\">\n                                <mat-icon>more_vert</mat-icon>\n                              </button>\n                              <mat-menu #menu1=\"matMenu\">\n                                \n                                <button mat-menu-item>\n                                    <a>Edit</a>\n                                  </button>\n                                <button mat-menu-item>\n                                    <a>Delete</a>\n                                  </button>\n                              </mat-menu>\n                            </th>\n                          </tr>\n                        </thead>\n                        <tbody>\n                          <tr>\n                            <td>\n                              <mat-checkbox class=\"zenwork-customized-checkbox\" style=\"margin:10px 0 0; display: block;\"></mat-checkbox>\n                            </td>\n                            <td style=\"padding:30px 0 0;\">Monday</td>\n                            <td>\n                              <div class=\"date1\">\n                                  <input matInput [matDatepicker]=\"picker21\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                                  <mat-datepicker #picker21></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker21.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                            </td>\n                            <td>\n  \n                                <div class=\"date1\">\n                                    <input matInput [matDatepicker]=\"picker22\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                                    <mat-datepicker #picker22></mat-datepicker>\n                                    <button mat-raised-button (click)=\"picker22.open()\">\n                                      <span>\n                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                      </span>\n                                    </button>\n                                    <div class=\"clearfix\"></div>\n                                  </div>\n  \n                            </td>\n                            <td>\n                                <div class=\"date1\">\n                                    <input matInput [matDatepicker]=\"picker23\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                                    <mat-datepicker #picker23></mat-datepicker>\n                                    <button mat-raised-button (click)=\"picker23.open()\">\n                                      <span>\n                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                      </span>\n                                    </button>\n                                    <div class=\"clearfix\"></div>\n                                  </div>\n                            </td>\n                            <td>\n                                <div class=\"date1\">\n                                    <input matInput [matDatepicker]=\"picker24\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n                                    <mat-datepicker #picker24></mat-datepicker>\n                                    <button mat-raised-button (click)=\"picker24.open()\">\n                                      <span>\n                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                      </span>\n                                    </button>\n                                    <div class=\"clearfix\"></div>\n                                  </div>\n                            </td>\n                           \n                            <td></td>\n                          </tr>\n    \n    \n                        </tbody>\n                      </table>\n  \n  \n               </div>\n      \n              </div>\n      \n            </div>\n      \n          </div>\n        </div>\n      \n      </div>\n  \n  \n  \n      <!-- Schedule Type -->\n  <div class=\"work-history\">\n  \n      <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n    \n          <div class=\"modal-content\">\n            \n            <div class=\"modal-body\">\n              \n              <div class=\"circle-popup\">\n  \n                 \n                  <a data-toggle=\"modal\" data-target=\"#myModal2\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i></a>\n  \n                  <p>\n                    <b>Standard- </b> Every Employee will be assigned the same days of the week and have the same start and finish time.</p>\n                  \n  \n                    <p>\n                      <b>Employee Specific- </b> Each Employee can have their own Schedule that allows for different work days, start times and finish times.\n                    </p>\n  \n                  <button class=\"btn\" data-dismiss=\"modal\">Ok</button>\n  \n              </div>\n    \n            </div>\n    \n          </div>\n    \n        </div>\n      </div>\n    \n    </div>\n"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.ts":
/*!*******************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.ts ***!
  \*******************************************************************************************************************************************/
/*! exports provided: ReviewHistoricalTimesheetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReviewHistoricalTimesheetComponent", function() { return ReviewHistoricalTimesheetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ReviewHistoricalTimesheetComponent = /** @class */ (function () {
    function ReviewHistoricalTimesheetComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.timeInsideData = [
            {
                lateByTime: '',
                hoursworked: '',
                grosshours: '',
                attendanceDetails: [],
                schedules: [
                    {
                        punchIn: '',
                        marginLeft: ''
                    }
                ]
            }
        ];
    }
    ReviewHistoricalTimesheetComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    ReviewHistoricalTimesheetComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.data);
        if (this.data.ArrivalDetails && this.data.dateWiseAttendanceList) {
            this.arrivalDetails = this.data.ArrivalDetails;
            this.dateWiseAttendanceList = this.data.dateWiseAttendanceList;
            this.timeInsideData = Object.keys(this.data.dateWiseAttendanceList).map(function (key) { return ({
                key: key, value: _this.data.dateWiseAttendanceList[key]
            }); });
            this.timeInsideData.forEach(function (element) {
                var totalMinutes = 1440;
                element.schedules = [];
                if (element.value.attendanceDetails && element.value.attendanceDetails.length) {
                    element.value.attendanceDetails.forEach(function (times) {
                        if (times.punchType === "IN") {
                            console.log(element);
                            var nt = new Date(times.punchTime);
                            var timenow = (nt.getHours() * 60) + nt.getMinutes();
                            element.schedules.push({ punchIn: timenow });
                        }
                        else if (times.punchType === "OUT") {
                            console.log(element);
                            var nt = new Date(times.punchTime);
                            var timenow = (nt.getHours() * 60) + nt.getMinutes();
                            element.schedules[element.schedules.length - 1].punchOut = timenow;
                            element.schedules[element.schedules.length - 1].marginLeft = (element.schedules[element.schedules.length - 1].punchIn / totalMinutes) * 100;
                            element.schedules[element.schedules.length - 1].width = ((element.schedules[element.schedules.length - 1].punchOut - element.schedules[element.schedules.length - 1].punchIn) / totalMinutes) * 100;
                        }
                    });
                }
            });
        }
    };
    // Author: Suresh M, Date: 24/07/19
    // getToolTimeData
    ReviewHistoricalTimesheetComponent.prototype.getToolTimeData = function (data) {
        // console.log(data);
        var punchinTime = this.getTimeFromMinutes(data.punchIn);
        var punchoutTime = this.getTimeFromMinutes(data.punchOut);
        // console.log(punchinTime, punchinTime)
        return 'Punch in ' + punchinTime + ', Punch out ' + punchoutTime;
    };
    // Author: Suresh M, Date: 24/07/19
    // getTimeFromMinutes
    ReviewHistoricalTimesheetComponent.prototype.getTimeFromMinutes = function (mins) {
        var h = Math.floor(mins / 60);
        var m = mins % 60;
        m = m < 10 ? '0' + m : m;
        var a = 'am';
        if (h >= 12)
            a = 'pm';
        if (h > 12)
            h = h - 12;
        return h + ":" + m + " " + a;
    };
    ReviewHistoricalTimesheetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-review-historical-timesheet',
            template: __webpack_require__(/*! ./review-historical-timesheet.component.html */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.html"),
            styles: [__webpack_require__(/*! ./review-historical-timesheet.component.css */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ReviewHistoricalTimesheetComponent);
    return ReviewHistoricalTimesheetComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.css":
/*!********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.css ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".time-sheet-block { margin:40px auto 0; float: none; padding: 0;}\n.zenwork-checked-border:after { \n  content: '';\n  position: absolute;\n  top: 0;\n  left: -50px;\n  border-right: #83c7a1 4px solid;\n  height: 100%;\n  border-radius: 4px;\n  padding: 25px;\n  }\n.box-align {\n    outline: none;\n    border: none;\n    box-shadow: none;\n  }\n.date-cycle {\n  width: 20%;\n  margin: 9px -42px 0 0px;\n  background: #fff;\n}\n.zenwork-currentpage { padding: 0;}\n.zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n.zenwork-currentpage em { font-size: 18px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n.zenwork-currentpage span{ display: inline-block;}\n.mr-7{margin-right: 7px;}\n.time-sheet-in { display: block;}\n.time-sheet-tabs {padding:10px 0 0;}\n.time-sheet-tabs .tab-content { margin:40px 0 0;}\n.time-sheet-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border: none;background: transparent;}\n.time-sheet-tabs .nav-tabs {border-bottom:#dbdbdb 2px solid;}\n.time-sheet-tabs .nav-tabs > li { padding:0 30px;}\n.time-sheet-tabs .nav-tabs > li > a {font-size: 18px;line-height: 18px;font-weight:normal;color:#484747;border: none;padding: 10px 0 15px !important;margin: 0;}\n.time-sheet-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;}\n.time-sheet-tabs .nav-tabs > li.active{border-bottom:#439348 5px solid;padding:0px 30px 0 30px;color:#000;}\n.time-approval { margin:40px 0 0;}\n.time-approval .panel-default>.panel-heading { padding: 0 0 20px; background-color:transparent !important; border: none; position: relative;}\n.time-approval .panel-title { display: inline-block; font-weight: 600; font-size: 17px;}\n.time-approval .panel-title a:hover { text-decoration: none;}\n.time-approval .panel-title>.small, .time-approval .panel-title>.small>a, .time-approval .panel-title>a, .time-approval .panel-title>small, .time-approval .panel-title>small>a { text-decoration:none;}\n.time-approval .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.time-approval .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.time-approval .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.time-approval .panel-group .panel-heading+.panel-collapse>.list-group, .time-approval .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.time-approval .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.time-approval .panel-body { padding: 0 0 5px;}\n.time-approval .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.time-approval .panel-default { border-color:transparent;}\n.time-approval .panel-group .panel+.panel { margin-top: 20px;}\n/* .time-missing-value { position: absolute; top: -10px; right: 0; width: 55%;} */\n.time-missing-value ul { display:block; width: 100%; margin: 0;}\n.time-missing-value ul li { padding: 0;}\n.time-missing-value ul li label { color:#4c4c4c;font-size: 16px; line-height: 16px; font-weight:500;display: inline-block;\nvertical-align: middle; width: 160px; padding:10px 0 10px;}\n.time-missing-value ul li label.label-view { padding: 0 0 0 80px;}\n.time-missing-table {margin:20px 0 0; padding: 0 0 10px;}\n.time-missing-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; \nfont-size: 15px; border-radius:3px;position: relative; line-height: 18px; vertical-align: middle;}\n.time-missing-table .table>tbody>tr>td, .time-missing-table .table>tfoot>tr>td, .time-missing-table .table>thead>tr>td{ padding:15px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#e8e8e8 1px solid; color: #4c4b4b; font-size: 15px;}\n.time-missing-table .checkbox{ margin: 0;}\n.time-missing-table .table>tbody>tr>td.zenwork-border {position: relative;}\n.time-missing-table .table>tbody>tr>td.zenwork-border:after { content:''; position: absolute; top: 2px;left: 2px;border-right: #83c7a1 4px solid;height: 90%;border-radius: 0 5px 5px 0;}\n.time-missing-table .table>thead>tr>th p { text-align: center; margin: 0;}\n.time-missing-table .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.time-missing-table .table>thead>tr>th a.pad-right { padding-right: 0;}\n.time-missing-table .table>thead>tr>th a .fa { color:#777676; font-size: 15px; line-height: 15px;}\n.time-missing-table .table>thead>tr>th .cont-check .checkbox label::before {left: 0;margin-left: -20px;border: 1px solid #cccccc;border-radius: 3px;background-color: #fff; top: 9px;}\n.time-missing-table .cont-check .checkbox label { font-size: 15px;}\n.time-missing-table .table>thead>tr>th .cont-check .checkbox label::after { top: 9px;}\n.cont-check .checkbox label { padding-left: 20px; font-size: 16px; line-height: 18px; position: relative;}\n.time-missing-table .table>thead>tr>th .cont-check .checkbox label span .fa { color:#6d6a6a; position: absolute; top:11px; right:-20px;}\n.time-missing-table .table>thead>tr>th span {font-size: 16px;display: inline-block; cursor: pointer;}\n.time-missing-table .table>thead>tr>th span .fa {color:#6d6a6a;position: absolute; top: 25px; right:65px;}\n.time-missing-table .table>tbody>tr>td .cont-check .checkbox label::before {left: 0;margin-left: -20px;border: 1px solid #cccccc;border-radius: 3px;background-color: #fff;font-size: 15px;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th { padding: 15px 10px;}\n.action { display: inline-block;}\n.time-sheet-summery .table>thead>tr>th small { display: inline-block; vertical-align: middle; padding: 0 0 0 10px;font-size: 15px;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th .cont-check .checkbox label span .fa { color:#6d6a6a; position: absolute; top:6px; right:-20px;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th span {font-size: 15px;display: inline-block; cursor: pointer;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th span .fa {color:#6d6a6a;position: absolute; top: 25px; right:20px;}\n.time-missing-table.time-sheet-summery .cont-check .checkbox label { font-size: 15px;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th .cont-check .checkbox label::before {top: 0;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th .cont-check .checkbox label::after { top:0;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th .dropdown-menu { min-width: 150px; border: none; top: 50px;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th .dropdown-menu li a { display: block; font-size: 14px; padding:8px 0 8px 10px; color: #636363; font-weight: 500;}\n.time-missing-table.time-sheet-summery .table>thead>tr>th .dropdown-menu li a:hover { background-color: #f2f9f4; color:#4c4c4c;}\n.action.status-action { margin: 0 0 0 30px;}\n.time-missing-table tbody tr .start-time .clock-icon{\n  font-size: 20px;\n  color: #c7c4c4;\n  position: absolute;\n  top: 20px;\n  right:33px;\n  border-left: 1px solid #c7c4c4;\n  padding: 0 0 0 5px;\n}\n.manager-search { border-bottom:#e1e1e1 1px solid; padding: 30px 0;}\n.search-block{ padding: 0; background:#fff;}\n.search-block .form-control { float: left; background: transparent; width:90%; color:#636363; border:none; font-size: 15px; box-shadow: none; padding: 20px 15px;}\n.search-block .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#c0c0bf;\n  }\n.search-block .form-control::-moz-placeholder { /* Firefox 19+ */\n    color:#c0c0bf;\n  }\n.search-block .form-control:-ms-input-placeholder { /* IE 10+ */\n    color:#c0c0bf;\n  }\n.search-block .form-control:-moz-placeholder { /* Firefox 18- */\n    color:#c0c0bf;\n  }\n.search-block span { float: right; width: 10%; text-align: center;}\n.search-block span .fa { color:#777676; font-size: 14px; padding: 12px 0 0; cursor: pointer;}\n.search-bg span { float: right; width: 10%; text-align: center;}\n.search-bg span .fa { color:#777676; font-size: 14px; padding: 12px 0 0; cursor: pointer;}\n.search-category { padding: 0;}\n.search-category label { color:#4c4c4c;font-size: 16px; line-height: 16px; font-weight:500;display: inline-block; vertical-align: middle; width: 160px;}\n.emp-time{\n  border: 0;\n  background: #f8f8f8;\n  padding: 7px 0px;\n  outline: none;\n}\n.time-missing-table tbody tr .start-time{\n  width: 25%;\n  position: relative;\n}\n.time-missing-table.manager-employee .table>thead>tr>th .cont-check .checkbox label span .fa { top:4px;}\n.time-missing-table.manager-employee .table>thead>tr>th .cont-check .checkbox label::before {top:0;}\n.time-missing-table.manager-employee .cont-check .checkbox label { font-size: 15px;}\n.time-missing-table.manager-employee .table>thead>tr>th .cont-check .checkbox label::after { top:0;}\n.time-missing-table.manager-employee .table>thead>tr>th span .fa {right:20px; top:20px;}\n.time-approval .time-missing-table .btn {border-radius: 20px;color:#000; font-size: 15px; padding:6px 25px; background:transparent; margin:30px 0 0; float:left; position: static;}\n.time-approval .time-missing-table .btn:hover {background: #008f3d; color:#fff;}\n.time-approval .btn{border-radius: 20px;color:#484848; font-size: 16px; padding:8px 25px; background:#fff;vertical-align: middle; font-weight: 500; position: absolute; top:-10px; right: 0;}\n.time-approval .btn span { display: inline-block; vertical-align: middle; padding:0 10px 0 0;}\n.time-approval .btn span .fa{ color:#008f3d; font-size: 15px;}\n.time-sheet-employee { display: block;}\n.time-sheet-employee h3 { font-size: 18px; font-weight:500; margin: 0 0 10px;}\n.time-sheet-employee ul { padding:30px; background:#fff; margin: 0; display: inline-block; width: 100%;}\n.time-sheet-employee ul li { padding: 0;}\n.time-sheet-employee ul li h4 { font-size: 16px; font-weight: normal; margin: 0 0 10px;}\n.time-sheet-employee ul li small { font-size: 16px; font-weight: normal; margin: 0;}\n.time-missing-table.time-sheet-main .table>thead>tr>th .cont-check .checkbox label span .fa { color:#6d6a6a; position: absolute; top:6px; right:-20px;}\n.time-missing-table.time-sheet-main .table>thead>tr>th span .fa {color:#6d6a6a;position: absolute; top: 20px; right:100px;}\n.time-missing-table.time-sheet-main .table>thead>tr>th span.date-filter .fa {right:50px;}\n.time-main-popup {background: #f8f8f8;}\n.time-main-popup .modal-header { padding:20px 20px 10px; border-bottom:none; position: relative;}\n.time-main-popup .modal-title { color:#008f3d;}\n.time-main-popup .modal-dialog { width:85%;}\n.time-main-popup .modal-body {padding: 0;}\n.time-main-popup .modal-content { border: none; padding:50px 40px; background-color:#f8f8f8;}\n.time-main-popup .close { position: absolute; top: 5px; right:15px; opacity: 1; text-shadow: none; font-size: 30px;}\n.time-main-popup .close:focus { outline: none;}\n.time-main-popup a.btn { display: inline-block; background:#fff; padding:10px 20px; border-radius: 20px; vertical-align: middle; cursor: pointer; color:#565454;}\n.time-main-popup a span {display: inline-block; vertical-align: middle;}\n.time-main-popup a span .fa { color:#008f3d; font-size: 16px; padding: 0 10px 0 0;}\n.time-main-in { display: block;}\n.date { display: block; background:#fff;height:40px; line-height:40px;}\n.date span { display: inline-block; vertical-align: middle; cursor: pointer; border: none !important; }\n.date span .fa { color: #008f3d;font-size:18px;line-height:18px;width: 18px;text-align: center; margin-left: -10px;}\n.date .form-control { display: inline-block;vertical-align: middle; width: 70%; border: none; background:transparent; box-shadow: none; padding: 0 0 0 10px;}\n.date input { border:none; width: 75%; display: inline-block; float: left; padding: 0 0 0 15px;}\n.date button { width: 25%; display: inline-block; float: right; background:transparent; border: none; outline: none; padding: 0 10px 0 0;}\n.time-missing-value ul li h5 { color:#4c4c4c;font-size: 16px; line-height: 16px; font-weight:500;display:block; margin: 0 0 10px;}\n.time-main-popup .time-missing-value { width: 60%;}\n.time-main-popup .time-missing-value ul li { display: inline-block; vertical-align: bottom; float: none; padding: 0 5px;}\n.time-main-popup .time-approval .panel-default>.panel-heading { padding: 0 0 20px;}\n.time-main-popup .time-missing-table { margin: 0;}\n.time-main-popup .time-approval .panel-title { padding: 25px 0 0;}\n.time-main-popup .time-approval { margin: 10px 0 0;}\n.time-main-popup .time-missing-table .table>thead>tr>th .cont-check .checkbox label::before { top: 0;}\n.time-main-popup .time-missing-table .table>thead>tr>th .cont-check .checkbox label::after { top:0;}\n.time-missing-table a.admin {display: inline-block; background:#797d7a;; padding:10px 35px;cursor: pointer; color:#fff; font-size: 15px; font-weight: 600; border-radius: 40px;}\n.time-main-popup .time-approval .panel-group .panel + .panel { margin-top: 0;}\n.education-date {\n  margin: 20px 5px 20px 0;\n}\n.education-date-list {\n  \n  position: relative;\n  display: inline-block;\n  padding: 0px 20px 0 0;\n  margin: 0px;\n}\n.edu-ends-calendar-icon{\n  position: absolute;\n  top: 40px;\n  width: 15px;\n  right: 25px;\n}\n.birthdate{\n  font-size: 14px;\n  padding-left: 15px;\n  /* width: 178px;    */\n  box-shadow: none;\n}\n.address-state-dropodown{\n  width: 100%;\n  border-radius: 0px!important;\n  height: 30px;\n  border: 0;\n  box-shadow: none;\n  background: #fff;\n}\n.mar-top-10 {\n  margin-top: 10px;\n}\n.select-category{\n  background: #fff;\n  height: 40px;\n  padding: 10px;\n  width: 60%;\n  margin-left: 20px;\n}\n.admin-tabs {background: #fff; width: 19%; margin: 0 0 13px;}\n.admin-tabs .tab-content { margin:40px 0 0;}\n.admin-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border: none;background: transparent;}\n.admin-tabs .nav-tabs > li { padding:0 20px;border-right: #e4d8d8 3px solid; margin: 0 10px;}\n.admin-tabs .nav-tabs > li > a {font-size: 18px;line-height: 18px;font-weight:normal;color:#484747;border: none;\npadding: 10px 0 15px !important;margin: 0; border-radius:0;}\n.admin-tabs .nav-tabs > li.active a{border-bottom:#439348 5px solid;padding:0px 30px 0 30px;color:#000; border-radius: 0;}\n.vertical-line {\n  position: absolute;\n  -webkit-transform: rotate;\n          transform: rotate;\n  -webkit-transform: rotate(90deg);\n          transform: rotate(90deg);\n  right: 74px;\n  line-height: 38px;\n  font-size: 40px;\n  font-weight: normal;\n  color: #e4d8d8;\n}\n.admin-tabs .nav-tabs { padding: 5px 0 0; border-bottom: transparent;}\n.admin-tabs .nav>li>a:focus, .admin-tabs .nav>li>a:hover { background-color:transparent;}\n.admin-tabs .nav-tabs > li.right-border { border-right:transparent 5px solid;}\n.time-sheet-bg {\n  background: #fff;\n  padding: 10px 0px 24px 18px;\n}\n.search-box {\n  outline: none;\n  border: none;\n  background: #f8f8f8;\n  box-shadow: none;\n}\n.search-icon {\n  position: absolute;\n  top: 0px;\n  right: 23px;\n}\n.search-bg {\n  margin-left: -35px;\n}\n.approved{ \n  display: inline-block;\n  font-size: 13px;\n  padding: 0 5px 0 0;\n  color: #008f3d;\n  vertical-align: middle;\n}\n.not-approved {\n  display: inline-block; font-size: 13px; padding:0 5px 0 0; color: #ccc;\n}\n.time-missing-value {padding:5px 0 0;}\n.right-select { margin:0;}\n.select-search { margin: 15px 0 0;}\n.select-search1 { margin:0;}\n.time-missing-value.time-sheet-value { margin:0;}\n.employee-time-missing { margin: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"time-sheet-block col-md-11\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n      <small>\n        <img src=\"../../../../assets/images/leave-management/time-approval.png\" width=\"22\" height=\"22\" \n          alt=\"Company-settings icon\">\n      </small>\n      <em>Time Sheet Approval and Maintenance</em>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n  \n  <div class=\"time-sheet-in\">\n\n    <div class=\"time-sheet-tabs\">\n\n      <ul class=\"nav nav-tabs\">\n        <li class=\"active\">\n          <a data-toggle=\"tab\" href=\"#tab1\">Time Sheet Approvals</a>\n        </li>\n        <li>\n          <a data-toggle=\"tab\" href=\"#tab2\">Time Sheet Maintenance</a>\n        </li>\n        <li>\n          <a data-toggle=\"tab\" href=\"#tab3\">Manager & Employee Approvals</a>\n        </li>\n      </ul>\n\n    </div>\n\n\n    <div class=\"tab-content\">\n\n      <div id=\"tab1\" class=\"tab-pane fade in active\">\n\n        <div class=\"time-approval\">\n\n          <div class=\"panel-group\" id=\"accordion1\">\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseOne\">\n                    Administrator Approval\n                  </a>\n                </h4>\n              </div>\n              <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n                  <div class=\"row\">\n                  <div class=\"admin-tabs col-xs-4\">\n\n                    <ul class=\"nav nav-tabs\">\n                      <li>\n                        <a data-toggle=\"tab\" href=\"#tab4\">Summary</a>\n                      </li>\n                                             \n                      <li class=\"active right-border\">\n                        <a data-toggle=\"tab\" href=\"#tab5\">List</a>\n                      </li>\n                    \n                    </ul>\n              \n                  </div>\n\n                  <div class=\"time-missing-value col-xs-8 pull-right\">\n                    <ul>\n                        <li class=\"col-md-6\">\n                            <label>Search by Category</label>\n                            <mat-select placeholder=\"Select Category\" [(ngModel)]=\"searchCategory\" (ngModelChange)=\"searchByCategory($event)\">\n                              <mat-option value=\"AllEmployees\">All Employees</mat-option>\n                              <mat-option value=\"Approved\">Approved Employees Only</mat-option>\n                              <mat-option value=\"NotApproved\">Not Approved Employees Only</mat-option>\n                              <mat-option value=\"DirectReportiesOnly\">My Direct Reports</mat-option>\n                              \n                            </mat-select>\n                          </li>\n                          <li class=\"col-md-6 right-select\">\n                              <label>Pay Cycle</label>\n                            <mat-select [(ngModel)]=\"selectPayFrequecy\" placeholder=\"Select Pay Group\" (ngModelChange)=\"selectPayGroup(selectPayFrequecy)\">\n                                <mat-option value=\"Weekly\">Weekly</mat-option>\n                                  <mat-option value=\"Bi-Weekly\">Bi-Weekly</mat-option>\n                                  <mat-option value=\"Semi-Monthly\">Semi-Monthly</mat-option>\n                                  <mat-option value=\"Monthly\">Monthly</mat-option>\n                            \n                            </mat-select>\n                          </li>\n\n                          <!-- <li class=\"col-md-1\" style=\"margin: 4px 0px 0px -12px;\">\n                            <label>Pay Cycle</label>\n                          </li>\n                          <li class=\"col-md-2\" style=\"width:8%\">\n                            \n                            <input type=\"text\" class=\"form-control box-align\" [(ngModel)]=\"payCycleForTimeSheet\" (ngModelChange)=\"payCycleNumber(payCycleForTimeSheet)\">\n                           \n                          </li> -->\n  \n                      <!-- <li class=\"col-md-2 pull-right date-cycle\"> -->\n                        <!-- <p> -->\n                            <!-- {{payCycleDate.pay_cycle_start | date : 'MM/dd/yyyy'}} - {{payCycleDate.pay_cycle_end | date : 'MM/dd/yyyy'}} -->\n\n                        <!-- </p> -->\n                        <!-- <input type=\"text\" class=\"form-control box-align\"> -->\n                       \n                      <!-- </li> -->\n                    </ul>\n                    <div class=\"clearfix\"></div>\n                  </div>\n                </div>\n                <div class=\"tab-content\">\n\n                    <div id=\"tab4\" class=\"tab-pane fade in\">\n                     </div>\n                      <div id=\"tab5\" class=\"tab-pane fade in active\">\n                          <div class=\"time-missing-table\">\n\n                              <table class=\"table\">\n                                <thead>\n                                  <tr>\n                                    <th></th>\n                                    <th>\n                                     Employee Name\n                                     <!-- <span>\n                                        <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                                      </span> -->\n                                    </th>\n                                    <th>\n                                      Manager Name\n                                      \n                                      <!-- <span>\n                                        <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                                      </span> -->\n                                    </th>\n          \n                                    <th>\n                                      Business Unit\n                                      <!-- <span>\n                                        <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                                      </span> -->\n                                    </th>\n          \n                                    <th>\n                                     Department\n                                      <!-- <span>\n                                        <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                                      </span> -->\n                                    </th>\n          \n                                    <th>\n                                    Status\n                                      <!-- <span>\n                                        <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                                      </span> -->\n                                    </th>\n\n          \n                                    <th>\n                                        <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                                        <mat-menu #menu1=\"matMenu\">\n                                          <button mat-menu-item>\n                                            <a (click)=\"reviewTimeSheet()\"> Review Timesheet</a>\n                                            <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit></a>\n                                          </button>\n                        \n                                        </mat-menu>\n                                    </th>\n          \n                                  </tr>\n                                </thead>\n                                <tbody>\n          \n                                  <tr *ngFor=\"let timeSheet of getTimeSheetList\">\n                                    <td [ngClass]=\"{'zenwork-checked-border': timeSheet.isChecked}\">\n          \n                                        <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"timeSheet.isChecked\" (change)=\"checkRequestTimeSheet(timeSheet.userId, timeSheet.pay_cycle, timeSheet.employee_fullname, timeSheet.pay_cycle_start, timeSheet.pay_cycle_end)\">\n                                        </mat-checkbox>\n          \n                                    </td>\n                                    <td>{{timeSheet.employee_fullname}}</td>\n                                    <td>{{timeSheet.manager_full_name}}</td>\n                                    <td>{{timeSheet.bussiness_unit}}</td>\n                                    <td>{{timeSheet.department}}</td>\n                                    <td>\n                                      <span *ngIf=\"timeSheet.isAdminApproved == true\">\n                                          <em class=\"approved\"><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>Approved</span>\n                                      <span *ngIf=\"timeSheet.isAdminApproved == false\">\n                                          <em class=\"not-approved\"><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>Not Approved</span>\n\n                                    </td>\n                                    <td></td>\n                                  \n                                  </tr>\n          \n\n                              </table>\n          \n                            </div>\n                            <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                              </mat-paginator>\n\n                     </div>\n\n                      </div>\n\n                </div>\n              </div>\n\n            </div>\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseTwo\">\n                    Missing Punches\n                  </a>\n                </h4>\n\n              </div>\n\n              <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n\n                    <div class=\"time-missing-value time-sheet-value\">\n                        <ul>\n                            <li class=\"col-md-4\" style=\"margin-top: 15px;\">\n                                <label>Search by Category</label>\n                                <mat-select [(ngModel)]=\"category\" placeholder=\"Select Category\" (ngModelChange)=\"selectCategory($event)\">\n                                  <mat-option value=\"AllEmployees\">All Employees</mat-option>\n                                  <mat-option value=\"Business Unit\">Business Unit</mat-option>\n                                  <mat-option value=\"Location\">Location</mat-option>\n                                  <mat-option value=\"Department\">Department</mat-option>\n                                  <mat-option value=\"Pay Frequency\">Pay Group</mat-option>\n                                </mat-select>\n                              </li>\n      \n                              <li class=\"col-md-4 select-search\" *ngIf=\"selectSearchCategory\" >\n                                <mat-select>\n                                  <mat-option *ngFor=\"let category of allValues\" value=\"category.name\" (click)=\"selectSubCategory(category.name)\">{{category.name}}</mat-option>\n                                 \n                                </mat-select>\n                              </li>\n    \n                          <!-- <li class=\"col-md-2\">\n                              <label>Select Date Range</label>\n                              <mat-select>\n                                <mat-option value=\"Current Pay Cycle\">Current Pay Cycle</mat-option>\n                                <mat-option value=\"Previous Pay Cycle\">Previous Pay Cycle</mat-option>\n                                <mat-option value=\"Next Pay Cycle\">Next Pay Cycle</mat-option>\n                                <mat-option value=\"Specific Date Range(T1-T2)\">Specific Date Range(T1-T2)</mat-option>\n                              </mat-select>\n                            </li> -->\n                         <li class=\"col-md-4 pull-right\">\n                            <ul class=\"list-unstyled education-date\">\n                                <li class=\"education-date-list padding10-listitems\">\n                                  <label>Start Date Range(T1)</label><br>\n                                  <div class=\"date\">\n                                      <input matInput [matDatepicker]=\"picker3\" [(ngModel)]=\"missingPunchStartDate\" #sdate (dateInput)=\"onChangeStartDate(sdate.value)\" placeholder=\"dd/mm/yy\">\n                                      <mat-datepicker #picker3></mat-datepicker>\n                                      <button mat-raised-button (click)=\"picker3.open()\"><span><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i></span></button>\n                                    <div class=\"clearfix\"></div>\n                                   </div>\n                                </li>\n                                <li class=\"education-date-list padding10-listitems\">\n                                  <label>End Date Range(T2)</label><br>\n                                  <div class=\"date\">\n                                      <input matInput [matDatepicker]=\"picker4\" [(ngModel)]=\"missingPunchEndtDate\" #edate (dateInput)=\"onChangeEndDate(edate.value)\" placeholder=\"dd/mm/yy\">\n                                      <mat-datepicker #picker4></mat-datepicker>\n                                      <button mat-raised-button (click)=\"picker4.open()\"><span><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i></span></button>\n                                    <div class=\"clearfix\"></div>\n                                   </div>\n                                </li>\n                              </ul>\n                         </li>\n                        </ul>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n\n                  <div class=\"time-missing-table\">\n\n                    <table class=\"table\">\n                      <thead>\n                        <tr>\n                          <th></th>\n                          <th>\n                           Employee Name\n                           <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n                          <th>\n                            Manager\n                            <br> Name\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Date Missing\n                            <br> Punch In\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Date Missing\n                            <br> Punch Out\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Schedule\n                            <br> Start Time\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Schedule\n                            <br> Finish Time\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n                              <mat-menu #menu2=\"matMenu\">\n                    \n                                  <button mat-menu-item>\n                                      <a (click)=\"reviewTimeSheetForMissingPunches()\"> Review Timesheet</a>\n                                      <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit3></a>\n                                    </button>\n              \n                              </mat-menu>\n                          </th>\n\n                        </tr>\n                      </thead>\n                      <tbody>\n\n                        <tr *ngFor = \"let missing of missingPunches\">\n                            <td [ngClass]=\"{'zenwork-checked-border': missing.isChecked}\">\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"missing.isChecked\" (change)=\"checkMissingPunches(missing.EmployeeDetails._id)\">\n                                </mat-checkbox>\n  \n                            </td>\n                          <td>{{missing.EmployeeDetails.personal.name.preferredName}}</td>\n                          <td>{{missing.EmployeeDetails.details.name.preferredName}}</td>\n                          <td>{{missing.MissingPunchType == 'IN' ? missing.MissingDate : '--'}}</td>\n                          <td>{{missing.MissingPunchType == 'OUT' ? missing.MissingDate : '--'}}</td>\n                        \n                          <td>08:00</td>\n                          <td>06:00</td>\n                          <td></td>\n                        </tr>\n\n                    </table>\n\n                  </div>\n                  <!-- <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                    </mat-paginator> -->\n                </div>\n              </div>\n            </div>\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\" href=\"#collapseThree\">\n                    Time Sheet Summary\n                  </a>\n                </h4>\n\n              </div>\n\n\n              <div id=\"collapseThree\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n                    <div class=\"time-missing-value time-sheet-value\">\n                        <ul>\n                            <li class=\"col-md-4\">\n                                <label>Search by Category</label>\n                                <mat-select [(ngModel)]=\"category\" (ngModelChange)=\"selectCategory($event)\">\n                                  <mat-option value=\"AllEmployees\">All Employees</mat-option>\n                                  <mat-option value=\"Business Unit\">Business Unit</mat-option>\n                                  <mat-option value=\"Location\">Location</mat-option>\n                                  <mat-option value=\"Department\">Department</mat-option>\n                                  <mat-option value=\"Pay Frequency\">Pay Group</mat-option>\n                                </mat-select>\n                              </li>\n      \n                              <li class=\"col-md-4 select-search1\" *ngIf=\"selectSearchCategory\">\n                                <mat-select>\n                                  <mat-option *ngFor=\"let category of allValues\" value=\"category.name\" (click)=\"selectSubCategory(category.name)\">{{category.name}}</mat-option>\n                                 \n                                </mat-select>\n                              </li>\n                          <!-- <li class=\"col-md-4 pull-right\">\n                            <label>Pay Cycle</label>\n                            <mat-select>\n                              <mat-option value=\"date\">01/01/2019-01/15/2019</mat-option>\n                            </mat-select>\n                          </li> -->\n                        </ul>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n\n                  <div class=\"time-missing-table time-sheet-summery\">\n\n                    <table class=\"table\">\n                      <thead>\n                        <tr>\n                          <th>\n                          </th>\n                          <th> Name\n                              <!-- <span>\n                                  <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                                </span> -->\n                          </th>\n                          <th>\n                            Manager\n                            <br> Name\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Daily Total\n                            <br> Hours\n                          </th>\n\n                          <th>\n                            Weekly Total\n                            <br> Hours\n                          </th>\n                          <th>\n                              Pay Cycle\n                              <br> Total\n                            </th>\n\n                          <th>\n                            Approaching\n                            <br> Over Time\n                          </th>\n\n                        <th>Approval Status\n                          <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span></th>\n                   \n                          <th>\n                              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu3\">more_vert</mat-icon>\n                              <mat-menu #menu3=\"matMenu\">\n                    \n                                  <button mat-menu-item>\n                                      <a (click)=\"reviewTimeSheetForTimesheetSummary()\"> Review Timesheet</a>\n                                      <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit4></a>\n                                    </button>\n              \n                              </mat-menu>\n                          </th>\n\n                        </tr>\n                      </thead>\n                      <tbody>\n\n                        <tr *ngFor=\"let summary of getTimeSheetSummaryList\">\n                          <td [ngClass]=\"{'zenwork-checked-border': summary.isChecked}\">\n\n                              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"summary.isChecked\" (change)=\"checkSummaryTimeSheet(summary.timesheetDetails.userId, summary.timesheetDetails.pay_cycle,summary.timesheetDetails.employee_fullname, summary.timesheetDetails.pay_cycle_start, summary.timesheetDetails.pay_cycle_end)\">\n\n                                </mat-checkbox>\n\n                          </td>\n                          <td>{{summary.timesheetDetails.employee_fullname}}</td>\n                          <td>{{summary.timesheetDetails.manager_full_name}}</td>\n                          <td>{{summary.timedata.dailyTotal.hours}}:{{summary.timedata.dailyTotal.minutes}}</td>\n                          <td>{{summary.timedata.weeklyTotal.hours}}:{{summary.timedata.weeklyTotal.minutes}}</td>\n                          <td>{{summary.timedata.payCyleTotal.hours}}:{{summary.timedata.payCyleTotal.minutes}}</td>\n                          <td>{{summary.timedata.dailyTotal.hours > 8 ? 'Yes':'No'}}</td>\n                          <td>\n                              <span *ngIf=\"summary.timesheetDetails.isAdminApproved == true\">\n                                  <em class=\"approved\"><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>Approved</span>\n                              <span *ngIf=\"summary.timesheetDetails.isAdminApproved == false\">\n                                  <em class=\"not-approved\"><i class=\"fa fa-circle\" aria-hidden=\"true\"></i></em>Not Approved</span>\n                          </td>\n                          <td></td>\n                        </tr>\n                    </table>\n                  \n                  </div>\n                  <mat-paginator [length]=\"total\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                    </mat-paginator>\n\n                </div>\n              </div>\n\n            </div>\n\n          </div>\n<!-- \n          <div class=\"time-sheet-employee\">\n\n            <h3>All Employees - Summary</h3>\n\n            <ul>\n\n              <li class=\"col-md-3\">\n                <h4>Total Number of Employees</h4>\n                <small>5</small>\n              </li>\n\n              <li class=\"col-md-3\">\n                <h4>Total Daily Hours</h4>\n                <small>60:00</small>\n              </li>\n\n              <li class=\"col-md-3\">\n                <h4>Total Weekly Hours</h4>\n                <small>55:00</small>\n              </li>\n\n              <li class=\"col-md-3\">\n                <h4>Total Pay Cycle Hours</h4>\n                <small>55:00</small>\n              </li>\n\n            </ul>\n            <div class=\"clearfix\"></div>\n          </div> -->\n\n        </div>\n\n      </div>\n\n\n      <div id=\"tab2\" class=\"tab-pane fade in\">\n\n        <div class=\"manager-search\">\n\n          <div class=\"search-block col-md-4\">\n            <input type=\"text\" class=\"form-control\" placeholder=\"Search Employee Name\" [(ngModel)]=\"searchString\" (ngModelChange)=\"searchEmployee(searchString)\">\n            <span>\n              <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\n            </span>\n          </div>\n          <div class=\"col-md-8 pull-right\">\n\n          \n          <div class=\"col-md-7\">\n              <label>Search by Category</label>\n              <mat-select class=\"select-category\" [(ngModel)]=\"category\" (ngModelChange)=\"selectCategory($event)\" placeholder=\"Search\">\n                <mat-option value=\"AllEmployees\">All Employees</mat-option>\n                <mat-option value=\"Business Unit\">Business Unit</mat-option>\n                <mat-option value=\"Location\">Location</mat-option>\n                <mat-option value=\"Department\">Department</mat-option>\n                <mat-option value=\"Pay Frequency\">Pay Group</mat-option>\n              </mat-select>\n            </div>\n\n            <div class=\"col-md-5\" *ngIf=\"selectSearchCategory\">\n              <mat-select class=\"select-category\" placeholder=\"Units\">\n                <mat-option *ngFor=\"let category of allValues\" value=\"category.name\" (click)=\"selectSubCategory(category.name)\">{{category.name}}</mat-option>\n               \n              </mat-select>\n            </div>\n\n            </div>\n\n          <div class=\"clearfix\"></div>\n\n        </div>\n\n        <div class=\"time-approval\">\n\n          <div class=\"panel-group\" id=\"accordion3\">\n\n            <div class=\"panel panel-default\">\n              <div class=\"panel-heading\">\n                <h4 class=\"panel-title\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseFive\">\n                    Time Sheet\n                  </a>\n                </h4>\n\n              </div>\n\n\n              <div id=\"collapseFive\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n\n\n                  <div class=\"time-missing-table time-sheet-main\">\n\n                    <table class=\"table\">\n                      <thead>\n                        <tr>\n                          <th>\n         \n                          </th>\n                            <th>\n                            Name\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>Employee Number\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Pay Group\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n\n                          <th>\n                            Pay Type\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n\n                          <th>\n                              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu4\">more_vert</mat-icon>\n                              <mat-menu #menu4=\"matMenu\">\n                                  <button mat-menu-item>\n                                      <a (click)=\"reviewTimeSheetForTimesheetMaintain()\"> Review Timesheet</a>\n                                      <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit1></a>\n                                    </button>\n                                <button mat-menu-item (click)=\"reviewHistoricalTimesheet()\">\n                                 Review Historical Time Sheet  \n                               </button>\n              \n                              </mat-menu>\n                          </th>\n\n                        </tr>\n                      </thead>\n                      <tbody>\n\n                        <tr *ngFor = \"let employee of searchEmployeeList\">\n                          <td [ngClass]=\"{'zenwork-checked-border': employee.isChecked}\">\n          \n                            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"employee.isChecked\" (change)=\"checkTimeSheetMaintain(employee._id)\">\n                            </mat-checkbox>\n\n                        </td>\n\n                          <td>{{employee.personal.name.preferredName}}</td>\n                          <td>{{employee.job.employeeId}}</td>\n                          <td>{{employee.compensation.Pay_frequency}}</td>\n                          <td>{{employee.compensation.current_compensation.payType}}</td>\n                          <td></td>\n                        </tr>\n\n\n                    </table>\n                   \n                  </div>\n                  <mat-paginator [length]=\"totalLength\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                    </mat-paginator>\n\n                </div>\n             \n              </div>\n\n            </div>\n\n          </div>\n<!-- \n          <div class=\"time-sheet-employee\">\n\n            <h3>All Employees</h3>\n\n            <ul>\n\n              <li class=\"col-md-3\">\n                <h4>Total Number of Employees</h4>\n                <small>5</small>\n              </li>\n\n              <li class=\"col-md-3\">\n                <h4>Total Daily Hours</h4>\n                <small>60:00</small>\n              </li>\n\n              <li class=\"col-md-3\">\n                <h4>Total Weekly Hours</h4>\n                <small>55:00</small>\n              </li>\n\n              <li class=\"col-md-3\">\n                <h4>Total Pay Cycle Hours</h4>\n                <small>55:00</small>\n              </li>\n\n            </ul>\n            <div class=\"clearfix\"></div>\n          </div> -->\n\n        </div>\n\n      </div>\n\n\n      <div id=\"tab3\" class=\"tab-pane fade\">\n\n        \n        <div class=\"manager-search\">\n\n          <div class=\"search-block col-md-5\">\n            <input type=\"text\" class=\"form-control\" placeholder=\"Search Employee Name\" [(ngModel)]=\"searchString\" (ngModelChange)=\"searchEmployee(searchString)\">\n            <span>\n              <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\n            </span>\n          </div>\n\n          <div class=\"col-md-6 pull-right\">\n              <label>Search by Category</label>\n              <mat-select class=\"select-category\" [(ngModel)]=\"category\" (ngModelChange)=\"selectEmployeeCategory($event)\">\n                <mat-option value=\"AllEmployees\">All Employees</mat-option>\n                <mat-option value=\"ManagerApprovedOnly\">Manager Approved Only</mat-option>\n                <mat-option value=\"EmployeeApprovedOnly\">Employee Approved Only</mat-option>\n                <mat-option value=\"NoApprovals\">No Approvals</mat-option>\n               \n              </mat-select>\n            </div>\n           \n          <div class=\"clearfix\"></div>\n\n        </div>\n\n        <div class=\"time-approval\">\n\n          <div class=\"panel-group\" id=\"accordion2\">\n\n            <div class=\"panel panel-default\">\n\n              <div class=\"panel-heading\">\n                <div class=\"col-md-4\">\n                    <h4 class=\"panel-title\">\n                        <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#collapseFour\">\n                          Approvals\n                        </a>\n                      </h4>\n                </div>\n<!--              \n                <div class=\"time-missing-value\">\n                    <ul>\n                      <li class=\"col-md-4\">\n                          <label>Select Date Range</label>\n                          <mat-select>\n                            <mat-option value=\"Current Pay Cycle\">Current Pay Cycle</mat-option>\n                            <mat-option value=\"Previous Pay Cycle\">Previous Pay Cycle</mat-option>\n                            <mat-option value=\"Next Pay Cycle\">Next Pay Cycle</mat-option>\n                            <mat-option value=\"Specific Date Range(T1-T2)\">Specific Date Range(T1-T2)</mat-option>\n                          </mat-select>\n                        </li>\n                     <li class=\"col-md-4\">\n                        <ul class=\"list-unstyled education-date\">\n                            <li class=\"education-date-list padding10-listitems\" style=\"margin-right:30px;\">\n                              <label>Start Date Range(T1)</label><br>\n                              <input class=\"form-control birthdate address-state-dropodown mar-top-10\" #dpMDYSdate=\"bsDatepicker\"\n                                bsDatepicker  [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                                placeholder=\"mm/dd/yyyy\" name=\"birth-date\">\n                              <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\"\n                                alt=\"Company-settings icon\" (click)=\"dpMDYSdate.toggle()\" [attr.aria-expanded]=\"dpMDYSdate.isOpen\">\n                            </li>\n                            <li class=\"education-date-list padding10-listitems\">\n                              <label>End Date Range(T2)</label><br>\n                              <input class=\"form-control birthdate address-state-dropodown mar-top-10\" #dpMDYEdate=\"bsDatepicker\"\n                                bsDatepicker  [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                                placeholder=\"mm/dd/yyyy\" name=\"birth-date\">\n                              <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\"\n                                alt=\"Company-settings icon\" (click)=\"dpMDYEdate.toggle()\" [attr.aria-expanded]=\"dpMDYEdate.isOpen\">\n                            </li>\n                          </ul>\n                     </li>\n                    </ul>\n                    <div class=\"clearfix\"></div> -->\n                  <!-- </div> -->\n                <div class=\"clearfix\"></div>\n              </div>\n\n              <div id=\"collapseFour\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body\">\n\n                  <div class=\"time-missing-table manager-employee\">\n\n                    <table class=\"table\">\n                      <thead>\n                        <tr>\n                          <th>\n                          </th>\n                          <th>\n                              Manager Name\n                              <!-- <span>\n                                <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                              </span> -->\n                            </th>\n                          <th>\n                            Manager Approved\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Manager Approval Stamp\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Employee Name\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Employee Approved\n                            <!-- <span>\n                              <i aria-hidden=\"true\" class=\"fa fa-filter\"></i>\n                            </span> -->\n                          </th>\n\n                          <th>\n                            Employee Approved Stamp\n                          </th>\n\n                          <th>\n                              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu5\">more_vert</mat-icon>\n                              <mat-menu #menu5=\"matMenu\">\n                    \n                                  <button mat-menu-item>\n                                      <a (click)=\"reviewTimeSheetForEmployeeManagerApproval()\"> Review Timesheet</a>\n                                      <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit2></a>\n                                    </button>\n                                <button mat-menu-item> Send Email Remainder </button>\n                                <button mat-menu-item (click)=\"employeeMissingApproval('employeeMissingApproval')\"> Email Employee Missing Approval  </button>\n                                <button mat-menu-item (click)=\"managerMissingApproval('managerMissingApproval')\"> Email Managers Missing Approval  </button>\n                                <button mat-menu-item (click)=\"anyOneMissingApproval()\"> Email Anyone Missing Approval  </button>\n              \n                              </mat-menu>\n                          </th>\n\n                        </tr>\n                      </thead>\n                      <tbody>\n\n                        <tr *ngFor = \"let approval of searchEmployeeApprovalList\">\n                          <td [ngClass]=\"{'zenwork-checked-border': approval.isChecked}\">\n          \n                            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"approval.isChecked\" (ngModelChange)=\"checkApproval(approval.userId,approval.pay_cycle,approval.employee_fullname, approval.pay_cycle_start, approval.pay_cycle_end )\">\n                            </mat-checkbox>\n\n                        </td>\n\n                          <td>\n                              {{approval.manager_full_name}}\n                          </td>\n                          <td>\n                              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"approval.isManagerApproved\" [disabled]=\"checkedDisable\">\n                            \n                                </mat-checkbox>\n                          </td>\n                          <td>{{approval.managerApprovalTimestamp | date:'MM/dd/yyyy'}}-{{approval.managerApprovalTimestamp | date:'shortTime'}}</td>\n                          <td> {{approval.employee_fullname}}</td>\n                          <td>\n                              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"approval.isEmployeeApproved\" [disabled]=\"checkedDisable\">\n                              \n                                </mat-checkbox>\n                          </td>\n                          <td>{{approval.employeeApprovalTimestamp | date:'MM/dd/yyyy'}}-{{approval.employeeApprovalTimestamp | date:'shortTime'}}</td>\n\n                          <td></td>\n                        </tr>\n\n                    </table>\n\n                  </div>\n\n                </div>\n              </div>\n            </div>\n\n\n          </div>\n\n        </div>\n\n\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n\n\n<div class=\"time-main-popup\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n    \n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <!-- <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n         \n          <div class=\"manager-search\">\n\n            <div class=\"search-block col-md-6\">\n              <input type=\"text\" class=\"form-control\" placeholder=\"Search Employee Name\">\n              <span>\n                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\n              </span>\n            </div>\n  \n            <div class=\"search-category col-md-3 pull-right\">\n              <label>Search by Category</label>\n              <mat-select placeholder=\"All Employees\">\n                <mat-option *ngFor=\" let category of categorys\" [value]=\"category\">{{category}}</mat-option>\n              </mat-select>\n            </div>\n            <div class=\"clearfix\"></div>  \n          </div>\n        </div> -->\n        <h4>Employee Time Sheet</h4>\n        <div class=\"row time-sheet-bg\">\n          <div class=\"col-xs-6\">\n            <h4 class=\"col-xs-4\">Employee Name:</h4> \n            <p style=\"margin: 7px 0px 0 9px;font-size: 17px;\">\n                <span class=\"green\" style=\"margin-left: -50px;\"> &nbsp; &nbsp; <u> {{employeeName}}</u></span>\n            </p>\n            \n              <!-- <span class=\"col-xs-5 search-bg\">\n                {{employeeName}} -->\n                  <!-- <div>\n                      <input type=\"text\" class=\"form-control search-box\" placeholder=\"Search Employee Name\">\n                      <span>\n                        <i class=\"fa fa-search search-icon\" aria-hidden=\"true\"></i>\n                      </span>\n                    </div> -->\n              <!-- </span> -->\n            \n          </div>\n          <div class=\"col-xs-6\">\n            <ul style=\"font-size:16px;\">\n              <li>\n                <p>Work Schedule Name: \n                  <!-- <span class=\"green\"> &nbsp; &nbsp; <u>Customer Service 1</u></span> -->\n                </p>\n              </li>\n              <li>\n                  <p>Work Schedule Type: \n                    <!-- <span class=\"green\">&nbsp; &nbsp;<u>Standard</u></span> -->\n                  </p>\n                </li>\n            </ul>\n          </div>\n        </div>\n\n        <div class=\"modal-body\">\n          <div class=\"time-approval\">\n\n            <div class=\"panel-group\" id=\"accordion4\">\n  \n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\" style=\"margin-bottom: 20px;\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseSix\">\n                      Time Sheet\n                    </a>\n                  </h4>\n  \n                  <div class=\"time-missing-value employee-time-missing pull-right\">\n                    <ul>\n                    \n                      <li class=\"col-md-6\">\n                        <label>Select Date Range</label>\n                        <mat-select placeholder=\"Specific date range t1-t2\">\n                          <mat-option *ngFor=\"let selectDate of selectDates\" [value]=\"selectDate\">{{selectDate}}</mat-option>\n                        </mat-select>\n                      </li>\n\n                      <li class=\"col-md-3\">\n\n                        <h5>Start Date Range (T1)</h5>\n                        <div class=\"date\">\n                          <input matInput [matDatepicker]=\"picker1\" [(ngModel)]=\"payCycleStartDate\" disabled placeholder=\"dd/mm/yy\">\n                          <mat-datepicker #picker1></mat-datepicker>\n                          <button mat-raised-button (click)=\"picker1.open()\"><span><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i></span></button>\n                        <div class=\"clearfix\"></div>\n                       </div>\n\n                      </li>\n\n                      <li class=\"col-md-3\">\n\n                        <h5>End Date Range (T2)</h5>\n                        <div class=\"date\">\n                          <input matInput [matDatepicker]=\"picker\" [(ngModel)]=\"payCycleEndDate\" disabled placeholder=\"dd/mm/yy\">\n                          <mat-datepicker #picker></mat-datepicker>\n                          <button mat-raised-button (click)=\"picker.open()\"><span><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i></span></button>\n                        <div class=\"clearfix\"></div>\n                       </div>\n\n                      </li>\n\n                    </ul>\n                    <div class=\"clearfix\"></div>\n                  </div>\n  \n  \n                </div>\n  \n                <div id=\"collapseSix\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body\">\n  \n                    <div class=\"time-missing-table\">\n  \n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>\n                          \n                            </th>\n                            <th>Name</th>\n                            <th>Day</th>\n  \n                            <th>Date</th>\n  \n                            <th>Time Type</th>\n  \n                            <th>Time In</th>\n  \n                            <th>Time Out</th>\n  \n                            <th>Dialy Total</th>\n\n                            <th>Weekly Total</th>\n\n                            <th>Pay Cycle</th>\n\n                            <th>Pay Cycle Total</th>\n  \n                            <th>\n                              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu6\">more_vert</mat-icon>\n                              <mat-menu #menu6=\"matMenu\">\n                                <button mat-menu-item (click)=\"addMissingPunches()\">Add row</button>\n                                <button mat-menu-item (click)=\"deletePunch()\">Delete row</button>\n                                <button mat-menu-item (click)=\"adminReject()\">Remove approval</button>\n                              </mat-menu>\n                            </th>\n  \n                          </tr>\n                        </thead>\n                        <tbody>\n  \n                          <tr *ngFor = \"let details of getTimeSheetDetails\">\n                            <td [ngClass]=\"{'zenwork-checked-border': details.isChecked}\">\n          \n                              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"details.isChecked\">\n                              </mat-checkbox>\n\n                          </td>\n                            <td>{{details.employeeName}}</td>\n                            <td> {{details.punchDate | date:'EEEE'}}</td>\n                            <td>{{details.punchDate | date:'MM/dd/yyyy'}}</td>\n                            <td>Hours Worked</td>\n                            <td>{{details.firstIn | date:'shortTime'}}</td>\n                            <td>{{details.lastOut | date:'shortTime'}}</td>\n                            <td>\n                              <span *ngIf=\"details.dailyTotal\">{{details.dailyTotal.hours}}:{{details.dailyTotal.minutes}}</span>\n                              \n                            </td>\n                            <td>\n                              <span *ngIf=\"details.weeklyTotalTime\">{{details.weeklyTotalTime.hours}}:{{details.weeklyTotalTime.minutes}}</span>\n                             \n                            </td>\n                            <td>{{details.pay_frequency}}</td>\n                            <td>\n                              <span *ngIf=\"details.payCycleTotalTime\">{{details.payCycleTotalTime.hours}}:{{details.payCycleTotalTime.minutes}}</span>\n                            </td>\n                            <td></td>\n                          </tr>\n\n                          <tr *ngFor=\"let punch of punchArray\">\n                            <td [ngClass]=\"{'zenwork-checked-border': punch.isChecked}\">\n          \n                              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"punch.isChecked\">\n                              </mat-checkbox>\n\n                          </td>\n                            <td>{{punch.name}}</td>\n                            <td> \n                                {{dayOfWeek}}\n                            </td>\n                            <td>\n                              <div class=\"date\" style=\"background: #f8f8f8;\">\n                                <input matInput [matDatepicker]=\"picker2\" [(ngModel)]=\"missingPunch.punchDate\" #fdate (dateInput)=\"onChangeDateValue(fdate.value)\">\n                                <mat-datepicker #picker2></mat-datepicker>\n                                <button mat-raised-button (click)=\"picker2.open()\"><span><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i></span></button>\n                              <div class=\"clearfix\"></div>\n                             </div>\n                            </td>\n                            <td>Hours Worked</td>\n                            <td class=\"start-time\">\n                              <input class=\"emp-time\" atp-time-picker [(ngModel)]=\"missingPunch.punchIn\">\n                               <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                            </td>\n                            <td class=\"start-time\">\n                              <input class=\"emp-time\" atp-time-picker [(ngModel)]=\"missingPunch.punchOut\">\n                              <i class=\"fa fa-clock-o clock-icon\" aria-hidden=\"true\"></i>\n                            </td>\n                            <td></td>\n                            <td></td>\n                            <td></td>\n                            <td></td>\n                            <td></td>\n                          </tr>\n\n  \n  \n                      </table>\n                      <a class=\"admin pull-left save-btn\" (click)=\"saveMissingPunch()\">Save</a>\n                      <a class=\"admin pull-right approve-btn\" (click)=\"adminApproveOrRemove()\">Administrator Approval</a>\n                      <div class=\"clearfix\"></div>\n                    </div>\n  \n                  </div>\n                </div>\n              </div>\n<!-- \n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion4\" href=\"#collapseSeven\">\n                      Accrual balance details\n                    </a>\n                  </h4>\n  \n                </div>\n  \n                <div id=\"collapseSeven\" class=\"panel-collapse collapse\">\n                  <div class=\"panel-body\">\n  \n                    <div class=\"time-missing-table\">\n  \n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox40\" type=\"checkbox\">\n                                  <label for=\"checkbox40\">Name</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>Balance-Current</th>\n  \n                            <th>Hourse Earned <br>This Cycle</th>\n  \n                            <th>Schedule Hours <br> Not Taken Yet</th>\n  \n                            <th>Project Balance <br> as of 12/31</th>\n  \n                            <th>Carryover Balance</th>                            \n  \n                            <th>\n                              <p>\n                                <a>\n                                  <i aria-hidden=\"true\" class=\"fa fa-pencil\"></i>\n                                </a>\n                                <a class=\"pad-right\">\n                                  <i aria-hidden=\"true\" class=\"fa fa-trash\"></i>\n                                </a>\n                              </p>\n                            </th>\n  \n                          </tr>\n                        </thead>\n                        <tbody>\n  \n                          <tr>\n                            <td class=\"zenwork-border\">\n  \n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox41\" type=\"checkbox\">\n                                  <label for=\"checkbox41\">Vacation</label>\n                                </div>\n                              </div>\n  \n                            </td>\n  \n                           \n                            <td>15:00</td>\n                            <td>05:00</td>\n                            <td>12:50</td>\n                            <td>12:00</td>\n                            <td>04:00</td>\n                            <td></td>\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-border\">\n  \n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox42\" type=\"checkbox\">\n                                  <label for=\"checkbox42\">Sick</label>\n                                </div>\n                              </div>\n  \n                            </td>\n  \n                           \n                            <td>15:00</td>\n                            <td>05:00</td>\n                            <td>12:50</td>\n                            <td>12:00</td>\n                            <td>04:00</td>\n                            <td></td>\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-border\">\n  \n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox43\" type=\"checkbox\">\n                                  <label for=\"checkbox43\">Float</label>\n                                </div>\n                              </div>\n  \n                            </td>\n  \n                           \n                            <td>15:00</td>\n                            <td>05:00</td>\n                            <td>12:50</td>\n                            <td>12:00</td>\n                            <td>04:00</td>\n                            <td></td>\n                          </tr>\n\n\n\n                    \n                      </table>\n  \n                      \n                    </div>\n  \n                  </div>\n                </div>\n              </div> -->\n  \n             \n            </div>\n  \n           \n  \n          </div>\n\n        </div>\n      </div>\n      \n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: TimeSheetApprovalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeSheetApprovalComponent", function() { return TimeSheetApprovalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var amazing_time_picker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! amazing-time-picker */ "./node_modules/amazing-time-picker/amazing-time-picker.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _review_historical_timesheet_review_historical_timesheet_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./review-historical-timesheet/review-historical-timesheet.component */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/review-historical-timesheet/review-historical-timesheet.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var TimeSheetApprovalComponent = /** @class */ (function () {
    function TimeSheetApprovalComponent(router, leaveManagementService, accessLocalStorageService, companySettingsService, myInfoService, swalAlertService, atp, dialog) {
        this.router = router;
        this.leaveManagementService = leaveManagementService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.companySettingsService = companySettingsService;
        this.myInfoService = myInfoService;
        this.swalAlertService = swalAlertService;
        this.atp = atp;
        this.dialog = dialog;
        this.getTimeSheetList = [];
        this.getTimeSheetSummaryList = [];
        this.defaultValues = [];
        this.customValues = [];
        this.allValues = [];
        this.customPayFrequency = [];
        this.defauluPayFrequency = [];
        this.allPayFrequency = [];
        this.selectedTimeSheets = [];
        this.getTimeSheetDetails = [];
        this.punchArray = [];
        this.selectedTimeSheetsDetails = [];
        this.missingPunch = {
            punchDate: '',
            punchIn: '',
            punchOut: ''
        };
        this.searchEmployeeList = [];
        this.searchEmployeeApprovalList = [];
        this.checkedDisable = true;
        this.missingPunches = [];
        this.selectedTimeSheetMaintain = [];
        this.selectedEmployeeManagerSheetApproval = [];
        this.selectedMissingPunchesReview = [];
        this.selectedTimesheetSummaryReview = [];
        this.selectDates = ["Current Cycle", "Previous Cycle", "Next Cycle"];
    }
    TimeSheetApprovalComponent.prototype.ngOnInit = function () {
        console.log(this.missingPunch.punchIn);
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        this.userId = this.accessLocalStorageService.get('employeeId');
        this.addedBy = this.accessLocalStorageService.get('addedBy');
        console.log(this.userId);
        this.typeofselectCategory = "AllEmployees";
        this.searchForCategory = "AllEmployees";
        this.selectedCategoryName = "AllEmployees";
        this.getStandardAndCustomStructureFields();
        this.getTimeSheetApprovals();
        // this.getMissingPunches();
        this.getTimeSheetSummary();
        this.searchEmployeInTimesheetMaintenance();
        this.searchEmployeeAndManagerApproval();
    };
    TimeSheetApprovalComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getTimeSheetSummary();
        this.getTimeSheetApprovals();
        this.searchEmployeInTimesheetMaintenance();
    };
    TimeSheetApprovalComponent.prototype.searchByCategory = function (event) {
        console.log(event);
        this.searchForCategory = event;
        this.getTimeSheetApprovals();
    };
    TimeSheetApprovalComponent.prototype.selectPayGroup = function (event) {
        console.log(event);
        this.selectPayFrequecy = event;
        this.getTimeSheetApprovals();
    };
    TimeSheetApprovalComponent.prototype.payCycleNumber = function (number) {
        console.log(number);
        this.payCycleForTimeSheet = number;
        this.getTimeSheetApprovals();
    };
    // Author:Saiprakash, Date:3/07/2019
    // Get Standard And Custom Structure Fields
    TimeSheetApprovalComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            // this.customPayFrequency = this.getAllStructureFields['Pay Frequency'].custom;
            // this.defauluPayFrequency = this.getAllStructureFields['Pay Frequency'].default;
            // this.allPayFrequency = this.customPayFrequency.concat(this.defauluPayFrequency);
        });
    };
    // Author: Saiprakash G, Date: 01/07/19,
    // Description: Get time sheet approval requests
    TimeSheetApprovalComponent.prototype.getTimeSheetApprovals = function () {
        var _this = this;
        var data = {
            employeeId: this.userId,
            selectedCategory: this.searchForCategory,
            selectedPayGroup: this.selectPayFrequecy,
            selectedPayCycle: this.payCycleForTimeSheet,
            perPageCount: this.perPage,
            currentPageNum: this.pageNo - 1
        };
        this.leaveManagementService.getTimeSheetApprovals(data).subscribe(function (res) {
            console.log(res);
            _this.getTimeSheetList = res.data.timesheetReqList;
            _this.count = res.data.totalCount;
            // this.payCycleDate = res.data[0]
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 02/07/19,
    // Description: Get time sheet details(Review Timesheet)
    TimeSheetApprovalComponent.prototype.reviewTimeSheet = function () {
        var _this = this;
        this.selectedTimeSheets = this.getTimeSheetList.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedTimeSheets);
        if (this.selectedTimeSheets.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed", "", 'error');
        }
        else if (this.selectedTimeSheets.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            this.openEdit.nativeElement.click();
            this.leaveManagementService.getTimeSheetDetails(this.selectedTimeSheets[0].pay_cycle, this.selectedTimeSheets[0].userId).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetDetails = res.data;
                // this.payCycleStartDate = res.data[0].pay_cycle_start;
                // this.payCycleEndDate = res.data[0].pay_cycle_end;
                // this.employeeName = res.data[0].employee_fullname;
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 04/07/19,
    // Description: Add missing punches
    TimeSheetApprovalComponent.prototype.addMissingPunches = function () {
        this.punchArray.push({
            name: this.employeeName,
            day: this.dayOfWeek,
            punchTime: '',
            punchType: 'IN'
        });
    };
    // Author: Saiprakash G, Date: 05/07/19
    // Punch Time and Date values
    TimeSheetApprovalComponent.prototype.onChangeDateValue = function (date) {
        console.log(date);
        var t = new Date(date);
        console.log(t);
        var day = t.getDay();
        console.log(day);
        var allDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        if (day == 0) {
            this.dayOfWeek = allDays[0];
        }
        else if (day == 1) {
            this.dayOfWeek = allDays[1];
        }
        else if (day == 2) {
            this.dayOfWeek = allDays[2];
        }
        else if (day == 3) {
            this.dayOfWeek = allDays[3];
        }
        else if (day == 4) {
            this.dayOfWeek = allDays[4];
        }
        else if (day == 5) {
            this.dayOfWeek = allDays[5];
        }
        else if (day == 6) {
            this.dayOfWeek = allDays[6];
        }
        console.log(this.dayOfWeek);
        this.punchDateTime = t.toISOString();
        console.log(this.punchDateTime);
        console.log(this.missingPunch.punchIn);
        console.log(this.missingPunch.punchOut);
        var timeInSplit = this.missingPunch.punchIn.split(":");
        var timeOutSplit = this.missingPunch.punchOut.split(":");
        console.log(timeInSplit[0], timeInSplit[1]);
        var timeInHours = timeInSplit[0];
        var timeInMinutes = timeInSplit[1];
        var timeOutHours = timeOutSplit[0];
        var timeOutMinutes = timeOutSplit[1];
        this.punchInTime = new Date(t.setHours(timeInHours, timeInMinutes));
        this.punchOutTime = new Date(t.setHours(timeOutHours, timeOutMinutes));
        console.log(this.punchInTime);
        console.log(this.punchOutTime);
    };
    // Author: Saiprakash G, Date: 17/06/19
    // Add Time picker
    TimeSheetApprovalComponent.prototype.open = function (event) {
        console.log(event);
        var amazingTimePicker = this.atp.open();
        amazingTimePicker.afterClose().subscribe(function (time) {
            console.log(time);
            // this.missingPunch.punchIn = time;
        });
    };
    // Author: Saiprakash G, Date: 04/07/19,
    // Description: Save missing punch
    TimeSheetApprovalComponent.prototype.saveMissingPunch = function () {
        var _this = this;
        if (this.requestId) {
            var data1 = {
                addedBy: this.userId,
                userId: this.requestId,
                companyId: this.companyId,
                punchArray: [{
                        day: this.dayOfWeek,
                        punchTime: this.punchInTime,
                        punchType: "IN"
                    },
                    {
                        day: this.dayOfWeek,
                        punchTime: this.punchOutTime,
                        punchType: "OUT"
                    },
                ]
            };
            this.leaveManagementService.addMissingPunches(data1).subscribe(function (res) {
                console.log(res);
                _this.reviewTimeSheet();
                _this.punchArray = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.missingPunchId) {
            var data2 = {
                addedBy: this.userId,
                userId: this.missingPunchId,
                companyId: this.companyId,
                punchArray: [{
                        day: this.dayOfWeek,
                        punchTime: this.punchInTime,
                        punchType: "IN"
                    },
                    {
                        day: this.dayOfWeek,
                        punchTime: this.punchOutTime,
                        punchType: "OUT"
                    },
                ]
            };
            this.leaveManagementService.addMissingPunches(data2).subscribe(function (res) {
                console.log(res);
                _this.reviewTimeSheetForMissingPunches();
                _this.punchArray = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.summaryId) {
            var data3 = {
                addedBy: this.userId,
                userId: this.summaryId,
                companyId: this.companyId,
                punchArray: [{
                        day: this.dayOfWeek,
                        punchTime: this.punchInTime,
                        punchType: "IN"
                    },
                    {
                        day: this.dayOfWeek,
                        punchTime: this.punchOutTime,
                        punchType: "OUT"
                    },
                ]
            };
            this.leaveManagementService.addMissingPunches(data3).subscribe(function (res) {
                console.log(res);
                _this.reviewTimeSheetForTimesheetSummary();
                _this.punchArray = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.timeSheetMaintainId) {
            var data4 = {
                addedBy: this.userId,
                userId: this.timeSheetMaintainId,
                companyId: this.companyId,
                punchArray: [{
                        day: this.dayOfWeek,
                        punchTime: this.punchInTime,
                        punchType: "IN"
                    },
                    {
                        day: this.dayOfWeek,
                        punchTime: this.punchOutTime,
                        punchType: "OUT"
                    },
                ]
            };
            this.leaveManagementService.addMissingPunches(data4).subscribe(function (res) {
                console.log(res);
                _this.reviewTimeSheetForTimesheetMaintain();
                _this.punchArray = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.approvalId) {
            var data5 = {
                addedBy: this.userId,
                userId: this.approvalId,
                companyId: this.companyId,
                punchArray: [{
                        day: this.dayOfWeek,
                        punchTime: this.punchInTime,
                        punchType: "IN"
                    },
                    {
                        day: this.dayOfWeek,
                        punchTime: this.punchOutTime,
                        punchType: "OUT"
                    },
                ]
            };
            this.leaveManagementService.addMissingPunches(data5).subscribe(function (res) {
                console.log(res);
                _this.reviewTimeSheetForEmployeeManagerApproval();
                _this.punchArray = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    TimeSheetApprovalComponent.prototype.checkRequestTimeSheet = function (id, payCycle, name, startDate, endDate) {
        console.log(id, payCycle);
        this.requestId = id;
        this.requestPayCycle = payCycle;
        this.employeeName = name;
        this.payCycleStartDate = startDate;
        this.payCycleEndDate = endDate;
    };
    TimeSheetApprovalComponent.prototype.checkSummaryTimeSheet = function (id1, payCycle1, name, startDate, endDate) {
        console.log(id1, payCycle1);
        this.summaryId = id1;
        this.summaryPayCycle = payCycle1;
        this.employeeName = name;
        this.payCycleStartDate = startDate;
        this.payCycleEndDate = endDate;
    };
    TimeSheetApprovalComponent.prototype.checkMissingPunches = function (id) {
        this.missingPunchId = id;
    };
    TimeSheetApprovalComponent.prototype.checkTimeSheetMaintain = function (id) {
        this.timeSheetMaintainId = id;
    };
    TimeSheetApprovalComponent.prototype.adminReject = function () {
        var _this = this;
        if (this.requestId) {
            var data1 = {
                employeeId: this.requestId,
                payCycle: this.requestPayCycle,
                approveType: "admin",
                isApproved: false,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data1).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetApprovals();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.summaryId) {
            var data3 = {
                employeeId: this.summaryId,
                payCycle: this.summaryPayCycle,
                approveType: "admin",
                isApproved: false,
                comments: this.selectedTimesheetSummaryReview[0].timesheetDetails.comments,
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data3).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetSummary();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.missingPunchId) {
            var data2 = {
                employeeId: this.missingPunchId,
                payCycle: "13",
                approveType: "admin",
                isApproved: false,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data2).subscribe(function (res) {
                console.log(res);
                _this.getMissingPunches();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.timeSheetMaintainId) {
            var data4 = {
                employeeId: this.timeSheetMaintainId,
                payCycle: "13",
                approveType: "admin",
                isApproved: false,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data4).subscribe(function (res) {
                console.log(res);
                _this.searchEmployeInTimesheetMaintenance();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.approvalId) {
            var data5 = {
                employeeId: this.approvalId,
                payCycle: this.approvalPayCycle,
                approveType: "admin",
                isApproved: false,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data5).subscribe(function (res) {
                console.log(res);
                _this.searchEmployeeAndManagerApproval();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 04/07/19,
    // Description: Admin approve or reject timesheet
    TimeSheetApprovalComponent.prototype.adminApproveOrRemove = function () {
        var _this = this;
        if (this.requestId) {
            var data1 = {
                employeeId: this.requestId,
                payCycle: this.requestPayCycle,
                approveType: "admin",
                isApproved: false,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data1).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetApprovals();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.summaryId) {
            var data3 = {
                employeeId: this.summaryId,
                payCycle: this.summaryPayCycle,
                approveType: "admin",
                isApproved: true,
                comments: this.selectedTimesheetSummaryReview[0].timesheetDetails.comments,
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data3).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetSummary();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.missingPunchId) {
            var data2 = {
                employeeId: this.missingPunchId,
                payCycle: "13",
                approveType: "admin",
                isApproved: true,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data2).subscribe(function (res) {
                console.log(res);
                _this.getMissingPunches();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.timeSheetMaintainId) {
            var data4 = {
                employeeId: this.timeSheetMaintainId,
                payCycle: "13",
                approveType: "admin",
                isApproved: true,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data4).subscribe(function (res) {
                console.log(res);
                _this.searchEmployeInTimesheetMaintenance();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else if (this.approvalId) {
            var data5 = {
                employeeId: this.approvalId,
                payCycle: this.approvalPayCycle,
                approveType: "admin",
                isApproved: true,
                comments: "test comment",
                approvedBy: this.userId
            };
            this.leaveManagementService.approveOrRejectTimeSheet(data5).subscribe(function (res) {
                console.log(res);
                _this.searchEmployeeAndManagerApproval();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                $("#myModal").modal("hide");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 04/07/19,
    // Description: Delete punch date
    TimeSheetApprovalComponent.prototype.deletePunch = function () {
        var _this = this;
        this.selectedTimeSheetsDetails = this.getTimeSheetDetails.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedTimeSheetsDetails);
        if (this.selectedTimeSheetsDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            this.leaveManagementService.deletePunch(this.userId, this.selectedTimeSheetsDetails[0].userId, this.selectedTimeSheetsDetails[0].punchDate, this.selectedTimeSheetsDetails[0].pay_cycle).subscribe(function (res) {
                console.log(res);
                _this.reviewTimeSheet();
                _this.reviewTimeSheetForTimesheetSummary();
                _this.reviewTimeSheetForMissingPunches();
                _this.reviewTimeSheetForTimesheetMaintain();
                _this.reviewTimeSheetForEmployeeManagerApproval();
            }, function (err) {
                console.log(err);
            });
        }
    };
    TimeSheetApprovalComponent.prototype.onChangeStartDate = function (date) {
        console.log(date);
        var s = new Date(date);
        this.startDate = s.toISOString().split("T")[0];
        console.log(this.startDate);
    };
    TimeSheetApprovalComponent.prototype.onChangeEndDate = function (date) {
        console.log(date);
        var e = new Date(date);
        this.endDate = e.toISOString().split("T")[0];
        console.log(this.endDate);
        this.getMissingPunches();
    };
    // Author: Saiprakash G, Date: 02/07/19,
    // Description: Get missing punches
    TimeSheetApprovalComponent.prototype.getMissingPunches = function () {
        var _this = this;
        var data = {
            startdate: this.startDate,
            enddate: this.endDate,
            "employeetype": {
                "type": {
                    "businesunit": [],
                    "location": [
                        this.subCategoryName
                    ],
                    "department": [],
                    "paygroup": [],
                    "All": false
                }
            }
        };
        this.leaveManagementService.getMissingPunches(data).subscribe(function (res) {
            console.log(res);
            _this.missingPunches = res.missingpunches;
        }, function (err) {
            console.log(err);
        });
    };
    TimeSheetApprovalComponent.prototype.reviewTimeSheetForMissingPunches = function () {
        var _this = this;
        this.selectedMissingPunchesReview = this.missingPunches.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedMissingPunchesReview);
        if (this.selectedMissingPunchesReview.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed", "", 'error');
        }
        else if (this.selectedMissingPunchesReview.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            this.openEdit3.nativeElement.click();
            this.leaveManagementService.getTimeSheetDetails("25", this.selectedMissingPunchesReview[0].EmployeeDetails._id).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetDetails = res.data;
                // this.payCycleStartDate = res.data[0].pay_cycle_start;
                // this.payCycleEndDate = res.data[0].pay_cycle_end;
                // this.employeeName = res.data[0].employee_fullname;
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 03/07/19,
    // Description: Select Category
    TimeSheetApprovalComponent.prototype.selectCategory = function (event) {
        var _this = this;
        console.log(event);
        var str = event;
        this.selectSearchCategory = event;
        // if(event == "Pay Frequency"){
        //   var payGroupCategory =  str.replace("Pay Frequency", "PayGroup");
        //   console.log(payGroupCategory);
        // }
        this.typeofselectCategory = str.replace(/ +/g, "");
        // this.typeofselectCategory = payGroupCategory
        console.log(this.typeofselectCategory);
        // this.typeofselectCategory = event;
        this.companySettingsService.getStuctureFields(event, 0).subscribe(function (res) {
            console.log(res);
            _this.defaultValues = res.default;
            _this.customValues = res.custom;
            _this.allValues = _this.defaultValues.concat(_this.customValues);
            console.log(_this.allValues);
        }, function (err) {
            console.log(err);
        });
    };
    TimeSheetApprovalComponent.prototype.selectSubCategory = function (name) {
        this.subCategoryName = name;
        this.getTimeSheetSummary();
        this.searchEmployeInTimesheetMaintenance();
    };
    // Author: Saiprakash G, Date: 02/07/19,
    // Description: Get time sheet summary
    TimeSheetApprovalComponent.prototype.getTimeSheetSummary = function () {
        var _this = this;
        var data = {
            selectedCategoryType: this.typeofselectCategory,
            selectedCategory: this.subCategoryName,
            payCycle: "13",
            userId: this.userId,
            perPageCount: this.perPage,
            currentPageNum: this.pageNo - 1
        };
        this.leaveManagementService.getTimeSheetSummary(data).subscribe(function (res) {
            console.log(res);
            _this.getTimeSheetSummaryList = res.data.attendenceList;
            _this.total = res.data.totalCount;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 09/07/19,
    // Description: Get time sheet details(Review Timesheet)
    TimeSheetApprovalComponent.prototype.reviewTimeSheetForTimesheetSummary = function () {
        var _this = this;
        this.selectedTimesheetSummaryReview = this.getTimeSheetSummaryList.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedTimesheetSummaryReview);
        if (this.selectedTimesheetSummaryReview.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed", "", 'error');
        }
        else if (this.selectedTimesheetSummaryReview.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            this.openEdit4.nativeElement.click();
            this.leaveManagementService.getTimeSheetDetails(this.selectedTimesheetSummaryReview[0].timesheetDetails.pay_cycle, this.selectedTimesheetSummaryReview[0].timesheetDetails.userId).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetDetails = res.data;
                // this.payCycleStartDate = res.data[0].pay_cycle_start;
                // this.payCycleEndDate = res.data[0].pay_cycle_end;
                // this.employeeName = res.data[0].employee_fullname;
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 05/07/19,
    // Description: Search for employee string
    TimeSheetApprovalComponent.prototype.searchEmployee = function (string) {
        console.log(string);
        this.searchString = string;
        this.searchEmployeInTimesheetMaintenance();
        this.searchEmployeeAndManagerApproval();
    };
    // Author: Saiprakash G, Date: 05/07/19,
    // Description: Employee timesheet maintenance
    TimeSheetApprovalComponent.prototype.searchEmployeInTimesheetMaintenance = function () {
        var _this = this;
        var data = {
            selectedCategoryType: this.typeofselectCategory,
            selectedCategory: this.subCategoryName,
            searchString: this.searchString,
            userId: this.userId,
            perPageCount: this.perPage,
            currentPageNum: this.pageNo - 1
        };
        this.leaveManagementService.searchEmployeeInTimeSheetMaintenance(data).subscribe(function (res) {
            console.log(res);
            _this.searchEmployeeList = res.data.searchResult;
            _this.totalLength = res.data.totalCount;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 09/07/19,
    // Description: Get time sheet details(Review Timesheet)
    TimeSheetApprovalComponent.prototype.reviewTimeSheetForTimesheetMaintain = function () {
        var _this = this;
        this.selectedTimeSheetMaintain = this.searchEmployeeList.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedTimeSheetMaintain);
        if (this.selectedTimeSheetMaintain.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed", "", 'error');
        }
        else if (this.selectedTimeSheetMaintain.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            this.openEdit1.nativeElement.click();
            this.leaveManagementService.getTimeSheetDetails("25", this.selectedTimeSheetMaintain[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetDetails = res.data;
                // this.payCycleStartDate = res.data[0].pay_cycle_start;
                // this.payCycleEndDate = res.data[0].pay_cycle_end;
                // this.employeeName = res.data[0].employee_fullname;
            }, function (err) {
                console.log(err);
            });
        }
    };
    TimeSheetApprovalComponent.prototype.reviewHistoricalDialog = function () {
        if (this.getHistoricalTimeSheetData) {
            var dialogRef = this.dialog.open(_review_historical_timesheet_review_historical_timesheet_component__WEBPACK_IMPORTED_MODULE_9__["ReviewHistoricalTimesheetComponent"], {
                width: '1300px',
                height: '800px',
                data: this.getHistoricalTimeSheetData
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
            });
        }
    };
    TimeSheetApprovalComponent.prototype.reviewHistoricalTimesheet = function () {
        var _this = this;
        this.selectedTimeSheetMaintain = this.searchEmployeeList.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedTimeSheetMaintain);
        if (this.selectedTimeSheetMaintain.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed", "", 'error');
        }
        else if (this.selectedTimeSheetMaintain.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            var data = {
                userId: "5cd91cc3f308c21c43e5054d",
                timesheetStartDate: "2019-07-21 00:00:00.000Z",
                timesheetEndDate: "2019-07-26 00:00:00.000Z"
            };
            this.leaveManagementService.getHistoricalTimeshhet(data).subscribe(function (res) {
                console.log(res);
                _this.getHistoricalTimeSheetData = res.data;
                _this.reviewHistoricalDialog();
            }, function (err) {
                console.log(err);
            });
        }
    };
    TimeSheetApprovalComponent.prototype.selectEmployeeCategory = function (event) {
        console.log(event);
        this.selectedCategoryName = event;
        this.searchEmployeeAndManagerApproval();
    };
    // Author: Saiprakash G, Date: 08/07/19,
    // Description: Employee and manager approvals
    TimeSheetApprovalComponent.prototype.searchEmployeeAndManagerApproval = function () {
        var _this = this;
        var data = {
            selectedCategory: this.selectedCategoryName,
            searchString: this.searchString,
            userId: this.userId,
            payCycle: 13,
            perPageCount: this.perPage,
            currentPageNum: this.pageNo - 1
        };
        this.leaveManagementService.searchEmployeeForApproval(data).subscribe(function (res) {
            console.log(res);
            _this.searchEmployeeApprovalList = res.data.timesheetList;
            _this.totalLength = res.data.totalCount;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 09/07/19,
    // Description: Get time sheet details(Review Timesheet)
    TimeSheetApprovalComponent.prototype.reviewTimeSheetForEmployeeManagerApproval = function () {
        var _this = this;
        this.selectedEmployeeManagerSheetApproval = this.searchEmployeeApprovalList.filter(function (timeSheetCheck) {
            return timeSheetCheck.isChecked;
        });
        console.log(this.selectedEmployeeManagerSheetApproval);
        if (this.selectedEmployeeManagerSheetApproval.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one timesheet approval to proceed", "", 'error');
        }
        else if (this.selectedEmployeeManagerSheetApproval.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one timesheet approval to proceed", "", 'error');
        }
        else {
            this.openEdit2.nativeElement.click();
            this.leaveManagementService.getTimeSheetDetails(this.selectedEmployeeManagerSheetApproval[0].pay_cycle, this.selectedEmployeeManagerSheetApproval[0].userId).subscribe(function (res) {
                console.log(res);
                _this.getTimeSheetDetails = res.data;
                // this.payCycleStartDate = res.data[0].pay_cycle_start;
                // this.payCycleEndDate = res.data[0].pay_cycle_end;
                // this.employeeName = res.data[0].employee_fullname;
            }, function (err) {
                console.log(err);
            });
        }
    };
    TimeSheetApprovalComponent.prototype.checkApproval = function (id, payCycle, name, startDate, endDate) {
        this.approvalId = id,
            this.approvalPayCycle = payCycle,
            this.employeeName = name;
        this.payCycleStartDate = startDate;
        this.payCycleEndDate = endDate;
    };
    TimeSheetApprovalComponent.prototype.employeeMissingApproval = function (name) {
        var data = {
            userId: this.approvalId,
            payCycle: this.approvalPayCycle,
            missingApprovalType: name
        };
        this.leaveManagementService.sendMissingApproval(data).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    TimeSheetApprovalComponent.prototype.managerMissingApproval = function (name) {
        var data = {
            userId: this.approvalId,
            payCycle: this.approvalPayCycle,
            missingApprovalType: name
        };
        this.leaveManagementService.sendMissingApproval(data).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    TimeSheetApprovalComponent.prototype.anyOneMissingApproval = function () {
        var data = {
            userId: this.approvalId,
            payCycle: this.approvalPayCycle,
            missingApprovalType: ""
        };
        this.leaveManagementService.sendMissingApproval(data).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    TimeSheetApprovalComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/leave-management']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimeSheetApprovalComponent.prototype, "openEdit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit1'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimeSheetApprovalComponent.prototype, "openEdit1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit2'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimeSheetApprovalComponent.prototype, "openEdit2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit3'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimeSheetApprovalComponent.prototype, "openEdit3", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit4'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TimeSheetApprovalComponent.prototype, "openEdit4", void 0);
    TimeSheetApprovalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-time-sheet-approval',
            template: __webpack_require__(/*! ./time-sheet-approval.component.html */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.html"),
            styles: [__webpack_require__(/*! ./time-sheet-approval.component.css */ "./src/app/admin-dashboard/leave-management/time-sheet-approval/time-sheet-approval.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_4__["CompanySettingsService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_5__["MyInfoService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__["SwalAlertService"],
            amazing_time_picker__WEBPACK_IMPORTED_MODULE_7__["AmazingTimePickerService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_8__["MatDialog"]])
    ], TimeSheetApprovalComponent);
    return TimeSheetApprovalComponent;
}());



/***/ })

}]);
//# sourceMappingURL=leave-management-leave-management-module.js.map