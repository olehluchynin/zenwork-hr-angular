(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760"],{

/***/ "./src/app/services/leaveManagement.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/leaveManagement.service.ts ***!
  \*****************************************************/
/*! exports provided: LeaveManagementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementService", function() { return LeaveManagementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LeaveManagementService = /** @class */ (function () {
    function LeaveManagementService(http) {
        this.http = http;
    }
    LeaveManagementService.prototype.createLeaveEligibilityGroup = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leaveEligibilityGroup/create", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getAllLeaveEligibilityGroup = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leaveEligibilityGroup/list")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updateLeaveEligibilityGroup = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leaveEligibilityGroup/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deleteLeaveEligibilityGroup = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leaveEligibilityGroup/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getLeaveEligibilityGroup = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leaveEligibilityGroup/get/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createTimeOffPolicies = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/createpolicy ", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createTierLevels = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/createtier ", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getEmployeeList = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leave-eligibility-group/paginate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getAllPolicies = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/getpolicydetails/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getTierLevel = function (company_id, tier_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/gettierlevel/" + company_id + "/" + tier_id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updateTierLevel = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/updatetierlevel", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getTimeOffPolicy = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/get/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deleteTierLevel = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/deletetierlevel", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updateTimePolicy = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/updatetimepolicy", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deleteTimeOffPolicies = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.uploadCustomFile = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leave-eligibility-group/uploadxlsx", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.downloadCustomFile = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/leave-eligibility-group/exportexcel", data, { responseType: 'blob' }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.uploadImportBalance = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/time-off-policies/upload", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createHoliday = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/addHolidays", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.loadHolidays = function (year) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/loadholidays/" + year)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getHolidayList = function (year) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getHolidayList/" + year)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getHoliday = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getHoliday/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updateHoliday = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/updateHoliday", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deleteHoliday = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/deleteHolidays", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createEmployeeGroup = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/createBlackoutEmployeeGroup", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getEmployeeGroupList = function (company_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getempgrp/" + company_id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createBlackout = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/addBlackoutDates", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getBlackoutList = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getblackout", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updateBlackout = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/updateblackout", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deleteBlackout = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/deleteblackout", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createPosition = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/createPositionSchedule", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getAllPosition = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/listPositionSchedules")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getPosition = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getPositionSchedule/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updatePosition = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/updatePositionSchedule", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deletePosition = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/deleteposition", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.createDepartment = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/createDepartmentSchedule", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getEmployeeWorkTime = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getempworktime", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getAllDepartment = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/listDepartmentSchedules")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getDepartmnet = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/getDepartmentSchedule/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.updateDepartment = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/editDepartmentSchedule", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deleteDepartment = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/scheduler/deletedepartment", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.punchIn = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/punchIn", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.punchOut = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/punchOut", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.timeSheetApproval = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetapproval/requesttimesheetapproval", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getTimeSheetApprovals = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetapproval/gettimesheetapprovalrequests", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getMissingPunches = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetapproval/getmissingpunches", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getTimeSheetSummary = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetapproval/gettimesheetsummary", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getTimeSheetDetails = function (payCycle, id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetapproval/gettimesheetdetails/" + payCycle + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.getHistoricalTimeshhet = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetmaintenance/gethistroricaltimesheetdetails", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.addMissingPunches = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/addmissingpunches", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.deletePunch = function (userId, timesheetEmployee, punchDate, payCycle) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/deleteattendancefordate/" + userId + "/" + timesheetEmployee + "/" + punchDate + "/" + payCycle)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.approveOrRejectTimeSheet = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetapproval/updatetimesheet", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.searchEmployeeInTimeSheetMaintenance = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetmaintenance/searchemployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.searchEmployeeForApproval = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetmaintenance/searchapprovalemployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService.prototype.sendMissingApproval = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/timeSchedule/timesheetmaintenance/sendmissingapproval", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    LeaveManagementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LeaveManagementService);
    return LeaveManagementService;
}());



/***/ })

}]);
//# sourceMappingURL=black-out-dates-black-out-dates-module~dashboard-dashboard-module~leave-management-eligibility-leave~20cf4760.js.map