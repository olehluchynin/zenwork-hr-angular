(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["onboardingtab-onboardingtab-module"],{

/***/ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.css":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.education-date-list {\n    width:23%;\n    position: relative;\n    display: inline-block; vertical-align: top; margin: 0 20px 0 0;\n}\n.education-date-list span { display: inline-block;}\n.education-date-list span.disable { display: inline-block; margin: 0 0 0 20px;}\n.education-date-list:nth-child(3){ margin:20px 0 0;}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 14px;\n    padding: 0 0 0 2px;\n}\n.birthdate{\n    font-size: 12px;\n    padding-left: 15px;\n    width: 178px;   \n    box-shadow: none;\n}\n.address-state-dropodown{\n    width: 100%;\n    border-radius: 0px!important;\n    height: 35px;\n    border: 0;\n    box-shadow: none;\n    background: #fff;\n}\n.main-onboarding .panel-heading{\n    padding:10px 15px!important;\n}\n.main-onboarding{\n    width:100%;\n    margin-top: 40px;\n    padding: 0 30px 0 0;\n}\n.main-onboarding .panel-font{\n    font-weight: 600;\n    margin-top: 5px;\n}\n.main-onboarding .panel{\n    border:0px !important;\n    box-shadow: none;\n}\n.main-onboarding .panel-heading .btn{\n    padding:2px 6px!important;\n    border-radius: 0!important;\n}\n.caret{\n    margin-bottom: 5px!important;\n}\n.hr-tasks{\n    font-size: 13px;\n    font-weight: 600;\n}\n.individual-hr-task{\n    font-size:14px;\n}\n.margin-top-zero{\n    margin-top:0px!important;\n}\n.padding-top-remove{\n    padding-top:0px!important;\n}\n.team-introdution-card{\n    background-color: #fcfcfc;\n    border-radius: 2px;\n    padding: 10px;\n}\n.margin-zero{\n    margin:0px!important;\n}\n.zenwork-documents-tab {\n  padding: 10px 35px;\n}\n.assign-field {\n  outline: none;\n  border: none;\n  height: 35px;\n  box-shadow: none;\n  background: #fff;\n}\n.next-btn {\n  background-color: #439348;\n  border: 1px solid #439348;\n  border-radius: 20px;\n  color: #fff;\n  padding: 0 30px;    \n}\n.btn-run-report{\n  padding: 6px 25px;\n  display: inline-block;\n  border: 1px solid #008f3d;\n  border-radius: 24px;\n  background: #fff;\n  color: #008f3d !important;\n  cursor: pointer;\n\n}\n.btn-run-report span{\n  padding: 0px 0 0 10px;\n  display: inline-block;\n  vertical-align: middle;\n}\n.disable button:disabled {\n  background-color:#aeaeae;\n  border-color: #aeaeae;\n  color: #000 !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.html":
/*!****************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-documents-tab\" style=\"margin-left: 40px;\">\n\n  <div class=\"education-dates\">\n    <ul class=\"list-unstyled education-date\" style=\"margin-bottom: 20px;\">\n\n      <li class=\"education-date-list padding10-listitems\">\n        <label>Onboarding Template Assigned</label><br>\n        <input type=\"text\" class=\"text-field form-control assign-field\" [(ngModel)]=\"template.templateName\"\n          [readOnly]=\"disableField\">\n\n      </li>\n      <li class=\"education-date-list padding10-listitems\">\n        <label>Date</label><br>\n        <p class=\"form-control birthdate address-state-dropodown\">\n          {{assignedDate | date : 'MM/dd/yyyy'}}\n        </p>\n        <!-- <input class=\"form-control birthdate address-state-dropodown\" type=\"date\"\n            placeholder=\"mm/dd/yyyy\" [(ngModel)]=\"assignedDate\" [readOnly]=\"disableField\"> -->\n        <!-- <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\" alt=\"Company-settings icon\"\n            (click)=\"dpMDYTDATE.toggle()\" [attr.aria-expanded]=\"dpMDYTDATE.isOpen\"> -->\n      </li>\n\n\n\n    </ul>\n\n  </div>\n  <hr class=\"zenwork-margin-ten-zero\">\n\n  <div class=\"education-dates\">\n    <ul class=\"list-unstyled education-date\">\n\n      <li class=\"education-date-list\">\n        <label>Onboarding Complete</label><br>\n        <input type=\"text\" class=\"text-field form-control assign-field\" [(ngModel)]=\"template.onboarding_complete\"\n          [readOnly]=\"disableField\">\n\n        <!-- <mat-select class=\"address-city text-field form-control address-state-dropodown\" [(ngModel)]=\"template.onboarding_complete\">\n          <mat-option [value]=\"\">{{template.onboarding_complete}}</mat-option>\n          <mat-option [value]=\"No\">No</mat-option>\n        </mat-select> -->\n      </li>\n      <li class=\"education-date-list\">\n        <label>Date</label><br>\n        <p class=\"form-control birthdate address-state-dropodown\">\n          {{completeDate | date : 'MM/dd/yyyy'}}\n        </p>\n\n        <!-- <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"edu-ends-calendar-icon\" alt=\"Company-settings icon\"\n          (click)=\"dpMDYTDATE.toggle()\" [attr.aria-expanded]=\"dpMDYTDATE.isOpen\"> -->\n      </li>\n      <li *ngIf=\"!onboardingTabView\" class=\"education-date-list\">\n        <span>\n          <mat-checkbox (change)=\"adminOverride($event)\" [(ngModel)]=\"overRide\" class=\"zenwork-customized-checkbox\">\n          </mat-checkbox>\n        </span>\n        <span class=\"disable\">\n          <button (click)=\"adminOverRideOfboarding()\" [disabled]='btnOverRide' class=\"btn-run-report\">\n            Administrator Override\n          </button>\n        </span>\n        <div class=\"clearfix\"></div>\n      </li>\n\n    </ul>\n\n  </div>\n  <!-- <hr class=\"zenwork-margin-ten-zero\"> -->\n  <div class=\"main-onboarding\">\n    <div class=\"panel panel-info\">\n      <div class=\"panel-heading\">\n        <span class=\"panel-title panel-font pull-left\">Onboarding Tasks </span>\n\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"row\" style=\"margin:0px;\">\n        <div class=\"panel-body col-xs-4\"></div>\n        <div class=\"panel-body col-xs-4\">\n          <p class=\"hr-tasks\">Task Status</p>\n        </div>\n        <div class=\"panel-body col-xs-4\">\n          <p class=\"hr-tasks\">Task Completion</p>\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class=\"panel panel-info\" *ngFor=\"let task of getKeys((template['Onboarding Tasks']))\">\n      <p *ngIf=\"template['Onboarding Tasks'][task].length > 0\" class=\"hr-tasks\" style=\"padding: 10px 0px 0px 13px;\">\n        {{task}}</p>\n      <div class=\"row\" style=\"margin:0px; border-bottom: 1px solid #efe0e0;\"\n        *ngFor=\"let data of template['Onboarding Tasks'][task]\">\n        <div class=\"panel-body col-xs-4\">\n\n          <p class=\"individual-hr-task\">{{data.taskName}}<br>\n            <small>{{data.taskAssignee}} - {{data.task_assigned_date | date:'MM/dd/yyyy'}}</small>\n          </p>\n        </div>\n        <div class=\"panel-body col-xs-4\">\n\n          <p class=\"individual-hr-task\">{{data.status}} </p>\n        </div>\n        <div class=\"panel-body col-xs-4\">\n\n          <p class=\"individual-hr-task\">{{data.task_completion_date | date:'MM/dd/yyyy' }} </p>\n        </div>\n        <!-- <hr class=\"zenwork-margin-ten-zero margin-zero\"> -->\n      </div>\n\n\n    </div>\n\n\n    <!-- <div class=\"panel panel-info\">\n        <div class=\"team-introdution-card\">\n          <div class=\"panel-body padding-top-remove\">\n            <div class=\"checkbox checkbox-success\">\n              <input id=\"checkbox3\" type=\"checkbox\" class=\"team-checkbox\">\n              <label for=\"checkbox3\">\n                <span class=\"individual-hr-task\">Team Introduction </span>\n                <small>Ashley Adams -Jul 25,2018</small>\n              </label>\n            </div>\n\n           \n\n          </div>\n          <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n          <p class=\"card-intro\">\n            please introduce the new hire to the team lead,and instruct the team lead to introduce the new hire\n            to\n            other individuals the new hire to other individuals with inthe organization<br>\n            that will have a key role in their job responsibilities\n          </p>\n          <hr class=\"zenwork-margin-ten-zero margin-top-zero\">\n          <div class=\"employee-image-info\">\n            <img src=\"../../../assets/images/Onboarding/empoyee_2.png\" alt=\"employee-icon\" class=\"emp-image\">\n            <div class=\"employee-details borderright\">\n              <span class=\"emp-name\">Crane Andy </span>\n\n              <small class=\"emp-designation\">Manager</small>\n            </div>\n\n            <div class=\"employee-details\">\n              <span class=\"emp-name \">Today</span>\n              <small class=\"emp-designation\">10:07 AM</small>\n            </div>\n          </div>\n          <div class=\"team-text-area\">\n            <div class=\"form-group\">\n              <textarea class=\"form-control rounded-0\" rows=\"3\"></textarea>\n            \n            </div>\n          </div>\n          <button class=\"btn zenwork-customized-back-btn pull-right btn-color\">\n            close\n\n          </button>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div> -->\n    <!-- <div class=\"panel panel-info\">\n        <div class=\"new-emp-paperwork\">\n          <p class=\"emp-paperwork-heading green\">\n            New Employee Paperwork\n          </p>\n          <div class=\"newhire-packets pull-left\">\n            <input type=\"checkbox\" class=\"newhire-checkbox\">\n            <span class=\"panel-body padding-top-remove newhire-content\">\n              <p class=\"individual-hr-task\">New Hire Packet- UK<br>\n                <small>Adam Jones- Jul 25,2018</small>\n              </p>\n            </span>\n            <img src=\"../../../assets/images/Onboarding/ic_insert_drive_file_24px.png\" alt=\"employee-icon\" class=\"insert-drive\">\n\n          </div>\n          <div class=\"padding-right-10\">\n            <ul class=\"list-unstyled pull-right\">\n              <li class=\"image-items\">\n                <img src=\"../../../assets/images/Onboarding/ic_border_color_24px.png\" alt=\"employee-icon\" class=\"insert-drive\">\n              </li>\n              <li class=\"image-items\">\n                <img src=\"../../../assets/images/Onboarding/ic_delete_24px.png\" alt=\"employee-icon\" class=\"insert-drive\">\n              </li>\n            </ul>\n            <div class=\"clearfix\"></div>\n          </div>\n         \n        </div>\n\n      </div> -->\n\n    <div style=\"margin:20px 0px;\">\n\n      <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Save Changes</button>\n      <div class=\"clear-fix\"></div>\n    </div>\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: OnBoardigTabComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnBoardigTabComponent", function() { return OnBoardigTabComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OnBoardigTabComponent = /** @class */ (function () {
    function OnBoardigTabComponent(accessLocalStorageService, myInfoService, router, swalAlertService) {
        this.accessLocalStorageService = accessLocalStorageService;
        this.myInfoService = myInfoService;
        this.router = router;
        this.swalAlertService = swalAlertService;
        this.disableField = true;
        this.overRide = false;
        this.btnOverRide = true;
        this.pagesAccess = [];
        this.onboardingTabView = false;
        this.template = {
            "templateName": "Onboarding Template",
            "templateType": "base",
            "Onboarding Tasks": {}
        };
        this.assignedDate = new Date();
    }
    OnBoardigTabComponent.prototype.ngOnInit = function () {
        this.type = JSON.parse(localStorage.getItem('type'));
        if (this.type != 'company') {
            this.pageAccesslevels();
        }
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        this.templateId = this.employeeDetails.module_settings.onboarding_template;
        console.log(this.templateId);
        this.getOnboardingDetails();
    };
    OnBoardigTabComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - On-Boarding" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.onboardingTabView = true;
                    console.log('loggss', _this.onboardingTabView);
                }
            });
        }, function (err) {
        });
    };
    OnBoardigTabComponent.prototype.getKeys = function (data) {
        // console.log(data);
        return Object.keys(data);
    };
    //  Author: Saiprakash, Date: 18/05/19
    //  Get onboarding details
    OnBoardigTabComponent.prototype.getOnboardingDetails = function () {
        var _this = this;
        this.myInfoService.getOnboardingDetails(this.companyId, this.templateId).subscribe(function (res) {
            console.log(res);
            _this.template = res.data;
            _this.assignedDate = new Date(_this.template.assigned_date);
            if (_this.template.completion_date) {
                _this.completeDate = new Date(_this.template.completion_date);
                // console.log(this.assignedDate);
            }
        }, function (err) {
            console.log(err);
        });
    };
    OnBoardigTabComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/offboardingtab"]);
    };
    OnBoardigTabComponent.prototype.adminOverride = function (event) {
        console.log(event);
        this.btnOverRide = !event.checked;
    };
    OnBoardigTabComponent.prototype.adminOverRideOfboarding = function () {
        var _this = this;
        console.log("vpiin");
        var uId = JSON.parse(localStorage.getItem('employee'));
        this.myInfoService.adminOonBoardingOverRide(uId._id)
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Adminover Ride", res.message, "success");
                _this.getOnboardingDetails();
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Adminover Ride", err.error.message, "error");
        });
    };
    OnBoardigTabComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-on-boardig-tab',
            template: __webpack_require__(/*! ./on-boardig-tab.component.html */ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.html"),
            styles: [__webpack_require__(/*! ./on-boardig-tab.component.css */ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.css")]
        }),
        __metadata("design:paramtypes", [_services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_1__["AccessLocalStorageService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], OnBoardigTabComponent);
    return OnBoardigTabComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/onboardingtab/onboardingtab.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/onboardingtab/onboardingtab.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: OnboardingtabModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingtabModule", function() { return OnboardingtabModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _on_boardig_tab_on_boardig_tab_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./on-boardig-tab/on-boardig-tab.component */ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.ts");
/* harmony import */ var _onboardingtab_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./onboardingtab.router */ "./src/app/admin-dashboard/employee-management/onboardingtab/onboardingtab.router.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var OnboardingtabModule = /** @class */ (function () {
    function OnboardingtabModule() {
    }
    OnboardingtabModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _onboardingtab_router__WEBPACK_IMPORTED_MODULE_3__["OnboardingTabRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModuleModule"]
            ],
            declarations: [_on_boardig_tab_on_boardig_tab_component__WEBPACK_IMPORTED_MODULE_2__["OnBoardigTabComponent"]]
        })
    ], OnboardingtabModule);
    return OnboardingtabModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/onboardingtab/onboardingtab.router.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/onboardingtab/onboardingtab.router.ts ***!
  \*******************************************************************************************/
/*! exports provided: OnboardingTabRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingTabRouting", function() { return OnboardingTabRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _on_boardig_tab_on_boardig_tab_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./on-boardig-tab/on-boardig-tab.component */ "./src/app/admin-dashboard/employee-management/onboardingtab/on-boardig-tab/on-boardig-tab.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/onboardingtab",pathMatch:"full" },
    { path: '', component: _on_boardig_tab_on_boardig_tab_component__WEBPACK_IMPORTED_MODULE_2__["OnBoardigTabComponent"] },
];
var OnboardingTabRouting = /** @class */ (function () {
    function OnboardingTabRouting() {
    }
    OnboardingTabRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OnboardingTabRouting);
    return OnboardingTabRouting;
}());



/***/ })

}]);
//# sourceMappingURL=onboardingtab-onboardingtab-module.js.map