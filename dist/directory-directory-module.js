(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["directory-directory-module"],{

/***/ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.css":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.css ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".directory-settings-heading{\n    padding: 5px 0 15px 15px;\n    border-bottom: 1px solid #ccc;\n}\n.table-outer{\n    border-bottom: 1px solid #ccc;\n}\n.reports-table { display: block; width: 60%;}\n.reports-table .table { margin-bottom: 30px;}\n.reports-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 13px; background: #eef7ff; border-bottom:none; width:20%;}\n.reports-table .table>tbody>tr>td, .reports-table .table>tfoot>tr>td, .reports-table .table>thead>tr>td{ padding: 13px; background:#fff;border-top: none;}\n.bottom-border { width: 100%;border-top:1px solid #ccc;}\n.reports-table tr.bottom-border td{ border-bottom: none;}\n.reports-table .table>tbody>tr>td{background: #f8f8f8;border-bottom:1px solid #ccc;}\n.employees-details{\n    border-top:1px solid #eeeeee;\n}\n.margin-top{\n    margin-top:40px;\n}\n.margin-top-15{\n    margin-top:15px;\n}\n.name-employee{\n    top:14px;\n    font-size: 14px;\n}\n.name-employee a{\n    color: #000;\n    text-decoration: none;\n    cursor: pointer;\n}\n.angle{\n    font-size: 9px;\n}\n.name{\n\n}\n.name b{\n    color:#616161;\n    cursor: pointer;\n}\n.list-details{\n    font-size: 12px;\n    color: #a4a7a9;\n    line-height: 20px;\n}\n.list-personal{\n    margin-top: 26px;\n    font-size: 12px;\n    line-height: 18px;\n    color: #a4a7a9;\n}\n.list-personal li{\n    line-height: 20px;\n}\n.list-profile{\n    margin-left:40px;\n    line-height: 18px;\n}\n.emp-info{\n    margin-left: 10px;\n}\n.img-emp-info{\n    width:12px;\n}\n.img-emp-info-mobile{\n    width:9px;\n}\n.margin-left{\n    margin-left:10px;\n}\n.accordion-toggle{\n    color: #000;\n    padding: 0 0 0 15px\n}\n.employee-individual-dropdown{\n    padding: 20px 0;\n    background: #f8f8f8;\n}\n.preview{\n   border-bottom: 1px solid #ccc; \n}\n.buttons{\n    margin: 20px 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    cursor: pointer;\n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n    /* cursor: pointer; */\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n.emp-img{\n    margin: 0 auto !important;\n    display: block !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.html":
/*!********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"directory-settings-main\">\n  <div class=\"directory-settings-heading\">\n    <h4>Directory Settings</h4>\n  </div>\n  <div class=\"table-outer\">\n    <div class=\"panel-body reports-table\">\n      <table class=\"table\">\n        <thead>\n          <tr>\n\n            <th>Field Name</th>\n\n            <th>Display</th>\n          </tr>\n        </thead>\n        <tbody>\n\n          <tr>\n\n            <td>Photo</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.photo\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n\n\n          <tr>\n\n            <td>Name</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.name\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n          <tr>\n\n            <td>Job Title</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.job_title\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n\n\n          <tr>\n\n            <td>Location</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.location\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n\n          <tr>\n\n            <td>Email</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.email\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n\n          <tr>\n\n            <td>Work Phone</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.work_phone\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n\n\n\n\n          </tr>\n          <tr>\n            <td>Cell Phone</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.cell_phone\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n\n          </tr>\n\n          <tr>\n\n            <td>Department</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.department\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n          <tr>\n\n            <td>Business Unit</td>\n            <td>\n              <mat-select placeholder=\"Yes/No\" [(ngModel)]=\"directorySettings.settings.business_unit\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n            </td>\n\n          </tr>\n\n\n      </table>\n    </div>\n  </div>\n  <div class=\"preview\">\n    <div class=\"field-accordion\" style=\"padding:20px 0 0 0;\">\n      <div class=\"panel-group\" id=\"employee\">\n\n\n        <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#formats\" href=\"#emp\">\n          Preview\n        </a>\n\n        <div id=\"emp\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body \">\n            <div class=\"employees-details\">\n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n\n                    <span>\n                      Employee's Details &nbsp;\n                    </span>\n                  </h4>\n                </div>\n              </div>\n              <div>\n                <div class=\"col-xs-12 individual-employee\">\n                  <div class=\"col-xs-1\">\n                    <div class=\"row\">\n\n                      <!-- <img src=\"../../../assets/images/Directory/empoyee_-1.png\"> -->\n\n                    </div>\n                  </div>\n\n                </div>\n                <div class=\"col-xs-12 employee-individual-dropdown individual-employee\">\n                  <div class='row'>\n                    <a href=\"javascript:void(0);\">\n\n                      <div *ngIf=\"directorySettings.settings.photo\" class=\"col-xs-2 \">\n                        <div class=\"row\">\n                          <img class=\"emp-img\" src=\"../../../assets/images/Directory/empoyee_2.png\">\n                        </div>\n                      </div>\n                      <div class=\"col-xs-6\">\n                        <div class=\"row\">\n                          <ul class=\"list-unstyled list-profile\">\n                            <li class=\"name\">\n                              <a>\n                                <b *ngIf=\"directorySettings.settings.name\">Name\n\n                                </b>\n                              </a>\n                            </li>\n                            <li *ngIf=\"directorySettings.settings.job_title\" class=\"list-details\">Job Title</li>\n                            <li *ngIf=\"directorySettings.settings.location\" class=\"list-details\">Location</li>\n                            <li *ngIf=\"directorySettings.settings.business_unit\" class=\"list-details\">Bussiness Unit</li>\n                            <!-- <li class=\"linked-in symbol\">\n                                <img src=\"../../../assets/images/Directory/layer1_1_.png\">\n                            </li> -->\n                          </ul>\n                          <!-- <img src=\"../../../assets/images/Directory/layer1_1_.png\" class=\"list-profile\"> -->\n                        </div>\n                      </div>\n\n                      <div class=\"col-xs-4\">\n                        <div class=\"row\">\n                          <ul class=\"list-personal list-unstyled\">\n                            <li *ngIf=\"directorySettings.settings.email\" class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/ic_email_24px.png\" class=\"img-emp-info\">\n                              <span class=\"emp-info\">Work Email</span>\n                            </li>\n                            <li *ngIf=\"directorySettings.settings.work_phone\" class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" class=\"img-emp-info\">\n                              <span class=\"emp-info\">Work Phone</span>\n                            </li>\n                            <li *ngIf=\"directorySettings.settings.cell_phone\" class=\"list-emp-detail\">\n                              <img src=\"../../../assets/images/Directory/Group 504.png\" class=\"img-emp-info-mobile\">\n                              <span class=\"emp-info\">&nbsp;Cell Phone</span>\n                            </li>\n                          </ul>\n                        </div>\n                      </div>\n                    </a>\n                  </div>\n                </div>\n              </div>\n\n\n\n\n            </div>\n          </div>\n        </div>\n\n      </div>\n    </div>\n  </div>\n\n  <div class=\"buttons\">\n    <ul class=\"list-unstyled\">\n\n      <li class=\"list-items\">\n        <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n      </li>\n      <li class=\"list-items pull-right\">\n        <button class='save' type=\"submit\" (click)=\"sendSettings()\">\n          Submit\n        </button>\n      </li>\n\n      <div class=\"clearfix\"></div>\n    </ul>\n  </div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: DirectorySettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectorySettingsComponent", function() { return DirectorySettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_directory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/directory.service */ "./src/app/services/directory.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var DirectorySettingsComponent = /** @class */ (function () {
    function DirectorySettingsComponent(dialogRef, data, directoryService, swalAlertService, siteAccessRolesService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.directoryService = directoryService;
        this.swalAlertService = swalAlertService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.directorySettings = {
            "companyId": "",
            "settings": {
                "photo": true,
                "name": true,
                "job_title": true,
                "location": true,
                "email": true,
                "work_phone": true,
                "cell_phone": true,
                "department": true,
                "business_unit": true
            }
        };
    }
    DirectorySettingsComponent.prototype.ngOnInit = function () {
        var cID = localStorage.getItem('companyId');
        console.log("cid", cID);
        cID = cID.replace(/^"|"$/g, "");
        console.log("cin2", cID);
        this.companyId = cID;
        this.directorySettings.companyId = cID;
        this.getDirectorySettings(this.companyId);
    };
    /* Description: getv directory settings fields
   author : vipin reddy */
    DirectorySettingsComponent.prototype.getDirectorySettings = function (companyId) {
        var _this = this;
        this.directoryService.getDirectorySettings(companyId)
            .subscribe(function (res) {
            console.log("get directory settings", res);
            _this.directorySettings.settings = res.data.settings;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: update directory settings fields
  author : vipin reddy */
    DirectorySettingsComponent.prototype.sendSettings = function () {
        var _this = this;
        this.directoryService.updateDirectorySettings(this.directorySettings)
            .subscribe(function (res) {
            console.log("directory settings update", res);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Update directory settings", res.message, "success");
            if (res.status == true) {
                _this.directorySettings.settings = res.data;
                _this.cancel();
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("error", err.error.message, "error");
        });
    };
    DirectorySettingsComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    DirectorySettingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-directory-settings',
            template: __webpack_require__(/*! ./directory-settings.component.html */ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.html"),
            styles: [__webpack_require__(/*! ./directory-settings.component.css */ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _services_directory_service__WEBPACK_IMPORTED_MODULE_2__["DirectoryService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_4__["SiteAccessRolesService"]])
    ], DirectorySettingsComponent);
    return DirectorySettingsComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 6px 15px;\n    font-size: 14px;\n    margin: 3px 15px 0px 0px;\n\n}\n.s3img{\n    margin: 20px 0 0 14px;\n    -o-object-fit: contain;\n       object-fit: contain;\n    border-radius: 50%;\n    width:100%;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.main-directory-tab{\n    float: none;\n    margin: 0 auto;\n    padding: 0\n}\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-currentpage .sub-title b{\n    vertical-align: middle;\n}\n.zenwork-inner-icon{\n    width: 20px;\n    height: auto;\n}\n.zenwork-structure-settings{\n    border-radius: 0px!important;\n    height:30px;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.sub-title{\n    display: inline-block;\n    font-size: 16px; vertical-align:text-top; padding: 0 0 0 10px;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.zenwork-border-bottom{\n    border-bottom: 1px solid #ddd;\n}\n.add-employee{\n    /* margin-top: 40px; */\n    margin-bottom: 20px;\n    padding-bottom: 10px;\n    border-bottom: 1px solid #eeeeee;\n}\n.add-employee .btn-employee{\n    background-color: #797d7a !important;\n    font-size: 12px;\n    padding: 7px 12px !important;\n    border: 1px solid #797d7a;\n}\n.img-plug{\n    width: 12px;\n    margin: 0px 7px;\n}\n.globe-icon{\n    font-size: 17px;\n    vertical-align: top;\n}\n.search-bar input{\n    font-size: 14px; \n    outline: none;\n    border: none;\n    box-shadow: none; height:auto; padding: 13px;\n   \n}\n.down-img{\n    vertical-align: middle;\n    color: #797d7b;\n    font-size: 9px;\n}\n.Employee-filters{\n    margin-bottom: 20px;\n    padding-bottom:35px;\n}\n.input-search{\n    width:100%;\n    position: relative;\n    height: 32px;\n    border: none;\n    padding: 0 0 0 10px;    \n}\n.search-icon{\n    position: absolute;\n    top: 15px;\n    right: 15px;\n    color: #999c9a;\n}\n.group-by{\n    padding-left: 30px;\n}\n.domain-img{\n    padding: 0px 6px;\n    border-right: 1px solid #999c9a;\n}\n.name-dropdown{\n    width: 128px;\n    height: 32px;\n    font-size: 14px;\n    border: none;\n    display: inline-block;\n    border-radius: 0;\n    background-color: #fff;\n    box-shadow: none;\n    \n}\n.employees-dropdown{\n    width: 84.3%;padding:13px 0;\n    border: 0px;\n    font-size: 14px;\n    border-radius: 0;\n    background-color: #fff;\n    display: inline-block;\n    box-shadow: none;\n    vertical-align: middle;\n    height: auto;\n}\n.all-employee-filter{\n    /* border:1px solid #999c9a; */\n    background: #fff;\n}\n.individual-employee{\n    border-bottom: 1px solid #f1f1f1;\n    padding-bottom: 20px;   \n}\n.employees-details{\n    border-top:1px solid #eeeeee;\n}\n.margin-top{\n    margin-top:40px;\n}\n.margin-top-15{\n    margin-top:15px;\n}\n.name-employee{\n    top:14px;\n    font-size: 14px;\n}\n.name-employee a{\n    color: #000;\n    text-decoration: none;\n    cursor: pointer;\n}\n.angle{\n    font-size: 9px;\n}\n.name{\n    font-size: 18px;\n}\n.name b{\n    color:#616161;\n    cursor: pointer;\n}\n.list-details{\n    font-size: 16px;\n    color: #a4a7a9;\n    line-height: 28px\n}\n.list-personal{\n    margin-top: 26px;\n    font-size: 16px;\n    line-height: 28px;\n    color: #a4a7a9;\n}\n.list-profile{\n    margin-left:40px;\n    margin-top: 15px;\n    line-height: 18px;\n    cursor: pointer;\n}\n.emp-info{\n    margin-left: 10px;\n}\n.img-emp-info{\n    width:17px;\n}\n.img-emp-info-mobile{\n    width:14px;\n}\n.margin-left{\n    margin-left:10px;\n}\n.out-date{\n    font-size: 9px;\n    color: #439348;\n}\n/* .panel{\n    border: none!important;  \n} */\n.panel-border .panel{\n    border:0;\n}\n/* modalcss */\n.modal-margintop{\n    margin-top:40px;\n}\n/* side nav css*/\n.tabs-left{\n    margin-top:0;\n}\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n  }\n.tabs-left > .nav-tabs {\n    float: left;\n    border-right: 1px solid #ddd;\n  }\n.tabs-left > .nav-tabs > li > a {\n    font-size: 12px;\n    margin-right: -1px;\n    border-radius: 4px 0 0 4px;\n  }\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee;\n  }\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n/*side nav css ends*/\n.customize-form{\n    margin-top:20px;\n}\n.customize-form .template{\n    height:30px;\n    width:40%;\n    border: 0;\n    background: #f8f8f8;\n}\n.modal-body{\n    background:#f8f8f8;\n    padding: 0;\n}\n.tabs-left .nav-tabs{\n    padding: 20px;\n    border-right: 0;\n}\n.tab-content{\n  margin: 0;\n  background: #fff;\n  padding: 10px 30px;\n  height: 100vh;\n}\n.tabs-customize{\n    background: #f8f8f8;\n    padding:0px!important;\n}\n.modal-body .container{\n    width:100%;\n}\n.caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n.benefits-dropdown{\n    width: 25%;\n    height: 30px;\n    border-radius: 0px!important;\n    border: 0;\n    background: #f8f8f8;\n    box-shadow: none;\n  }\nlabel{\n    font-weight:normal;\n    line-height: 30px;\n    font-size: 12px;\n}\n.question-list{\n    margin: 0 0 20px 0;\n}\n.question-list .list-items{\n    padding:10px 0px;\n}\n.submit-buttons .list-item{\n    display: inline-block;\n    padding: 0px 10px 0px 0px;\n  }\n.submit-buttons .btn-success {\n      background-color: #439348;\n  }\n.btn-style {\n    padding: 2px 12px!important;\n    border-radius: 30px!important;\n}\n.main-customizeform-benefits {\n    margin: 10px 0 20px 0;\n    border-bottom: 2px solid #ededed;\n\n}\n.caret-dropdown {\n    font-size: 10px;\n    vertical-align: middle;\n}\n.date-list .list-items {\n    display: inline-block;\n    padding: 0 20px 0 0;\n    width: 30%;\n}\n.main-customizeform-date{\n    margin: 20px 0 0 0;\n}\n.date-list .list-items .benefits-dropdown {\n    width: 100%;\n}\n.main-filter-rights{\n    \n}\n.emplployee-icon{\n    font-size: 100px;\n    color: #ccc;\n    padding: 20px 0 0 0; \n}\n.label-display { padding:10px 15px 0 0;}\n.label-display label { font-size: 15px;}\n.linked-in{\n    cursor: pointer\n}\n#accordion1 li.panel{\n    margin-bottom: 0px;\n}\n.display-right { padding: 0;}\n.display-right label {font-size: 16px; padding: 10px 20px 0 0; float: right;}\n.display-right .form-control { width:60%;border:none;box-shadow: none; float:right; height: auto; padding: 14px 12px;\nbackground:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAQCAYAAAAbBi9cAAAABHNCSVQICAgIfAhkiAAAAcJJREFUOI2lk81qk1EQhp83zUK7iyHrFsG1uHKRHxutgtcgLpSuCnYhpF+/JKIgSdp6ESLSm7AuWtOChdBUFy7FFncVEwURi36vixxLsbE/OKvhzMxz5sw7R1EtNkeYzfJis3XjqByAFIChb7Nq2A7FW+BXxxUfAmG6i83WhOA5gJTc6+1+njwNSFEtNvYHwzOJMqhkeIr9UdID7PcOF/wTIr9IB29c8HA/AHeQ/mSdPxgbZjbf0sHtStxPrLuC2ziZMSPvJL80dFJyZRggSbgsaR4gPSCqv9BorUS1+Fpoqdvf/bSRyWXB9OYb8yvDQJV6PKqgeWrQPeWwBvVw3M7ksnshdj2qxZ6tzsUAUTV+HdXi739DBx1BH/MGMS4Ys9mS/BVUwu4ZvRWp7VCzCf4yFBTkvxrV4sdAfSB/byOTy+4ZdQ4u5EKzNT3smSOFUvGRsPLFQkZiAjRmpDOjZy9JuhJi5wql/M/19vpOpTo3VSgWbq6319r5UvGC4BawfGL5bX4Aayk0hbgINA52NNhs3JVc3l88JzO2JsP8OpLL+uUlgARNJwmH/t6p5X/SbG4Om5GO+/0nMduzaZvV/waJnd/vO9BvlygKnwAAAABJRU5ErkJggg==) no-repeat 15px center #fff;}\n.display-right .form-control:focus { border-color: transparent;}\n.mat-expansion-indicator::after { display: none !important;}\n.display-settings { margin:0 0 0 15px;}\n.display-settings ul { display: block;}\n.display-settings ul li { display: block;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <a [routerLink]=\"['/admin/admin-dashboard/employee-management']\">\n        <button class=\"btn zenwork-customized-back-btn\">\n          <span class=\"green mr-7\">\n            <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n          </span>Back\n        </button>\n      </a>\n      <span class=\"inner-icon-img\">\n        <img src=\"../../../assets/images/Directory/Group 2202.png\" class=\"zenwork-inner-icon\"\n          alt=\"Company-settings icon\">\n      </span>\n      <small class=\"sub-title\">\n        <b>\n\n          Directory\n\n\n        </b>\n      </small>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero zenwork-border-bottom\">\n  </div>\n  <div class=\"col-md-11 main-directory-tab\">\n    <div class=\"add-employee\">\n      <button class=\"btn btn-success btn-employee\" (click)=\"openModalWithClass(template)\">Demographic Mass Update &nbsp;\n        <i class=\"fa fa-globe globe-icon\" aria-hidden=\"true\"></i>\n      </button>\n      <button class=\"btn btn-success btn-employee pull-right\" (click)=\"openDialog()\">Directory Settings</button>\n      <div class=\"clearfix\"></div>\n      <ng-template #template>\n        <div class=\"modal-header modal-margintop\">\n          <h4 class=\"modal-title pull-left green\">\n            <b>Demographic Mass Update</b>\n          </h4>\n          <div class=\"clearfix\"></div>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"container\">\n            <div class=\"row\">\n              <div class=\"tabbable tabs-left\">\n                <ul class=\"nav nav-tabs col-md-3\">\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'updateBenefits' }\">\n                    <a href=\"#updateBenefits\" data-toggle=\"tab\" (click)=\"chooseFieldss('updateBenefits')\">Update\n                      Benefits?\n                      <span [ngClass]=\"{'caret': selectedNav == 'updateBenefits' }\"></span>\n                    </a>\n                  </li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'chooseFields' }\">\n                    <a href=\"#chooseFields\" data-toggle=\"tab\" (click)=\"chooseFieldss('chooseFields')\">Choose Fields\n                      <span [ngClass]=\"{'caret': selectedNav == 'chooseFields' }\"></span>\n                    </a>\n                  </li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'addEditDelete' }\">\n                    <a href=\"#addEditDelete\" data-toggle=\"tab\" (click)=\"chooseFieldss('addEditDelete')\">Add/Edit/Delete\n                      <span [ngClass]=\"{'caret': selectedNav == 'addEditDelete' }\"></span>\n                    </a>\n                  </li>\n                  <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'summaryReview' }\">\n                    <a href=\"#summaryReview\" data-toggle=\"tab\" (click)=\"chooseFieldss('summaryReview')\">Summary/Review\n                      <span [ngClass]=\"{'caret': selectedNav == 'summaryReview' }\"></span>\n                    </a>\n                  </li>\n                </ul>\n                <div class=\"tab-content col-md-9\">\n                  <div class=\"tab-pane active\" id=\"updateBenefits\">\n                    <div class=\"main-customizeform\">\n                      <ul class=\"list-unstyled question-list\">\n                        <li class=\"list-items\">\n                          <label>Make Eligible for Benefits?</label>\n                          <br>\n                          <select class=\"form-control benefits-dropdown\">\n                            <option>Yes</option>\n                            <option>No</option>\n                          </select>\n                        </li>\n                        <li class=\"list-items\">\n                          <label>Select type</label>\n                          <br>\n                          <select class=\"form-control benefits-dropdown\">\n                            <option>Benefits</option>\n                            <option>Demographic</option>\n                          </select>\n                        </li>\n                      </ul>\n                      <div class=\"margin-top\">\n                        <ul class=\"submit-buttons list-unstyled\">\n                          <li class=\"list-item\">\n                            <button class=\"btn btn-success btn-style\"> Proceed </button>\n                          </li>\n                          <li class=\"list-item\">\n                            <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> Cancel </button>\n                          </li>\n                        </ul>\n\n                      </div>\n\n                    </div>\n                  </div>\n                  <div class=\"tab-pane\" id=\"chooseFields\">\n                    <div class=\"main-customizeform-benefits\">\n                      <label class=\"green\">Benefits</label>&nbsp;\n                      <span class=\"glyphicon glyphicon-triangle-bottom caret-dropdown\"></span>\n                      <ul class=\"list-unstyled question-list\">\n                        <li class=\"list-items\">\n                          <label>Plan Types</label>\n                          <br>\n                          <select class=\"form-control benefits-dropdown\">\n                            <option>Yes</option>\n                            <option>No</option>\n                          </select>\n                        </li>\n                        <li class=\"list-items\">\n                          <label>Plans</label>\n                          <br>\n                          <select class=\"form-control benefits-dropdown\">\n                            <option>Yes</option>\n                            <option>No</option>\n                          </select>\n                        </li>\n                      </ul>\n                    </div>\n                    <div class=\"demogrsphic-section\">\n                      <label class=\"green\">Demographics</label>&nbsp;\n                      <span class=\"glyphicon glyphicon-triangle-bottom caret-dropdown\"></span>\n                      <ul class=\"list-unstyled question-list\">\n                        <li class=\"list-items\">\n                          <label>My Info sections</label>\n                          <br>\n                          <select class=\"form-control benefits-dropdown\">\n                            <option>Yes</option>\n                            <option>No</option>\n                          </select>\n                        </li>\n                        <li class=\"list-items\">\n                          <label>Fields</label>\n                          <br>\n                          <select class=\"form-control benefits-dropdown\">\n                            <option>Yes</option>\n                            <option>No</option>\n                          </select>\n                        </li>\n                      </ul>\n                    </div>\n                    <div class=\"margin-top\">\n                      <ul class=\"submit-buttons list-unstyled\">\n                        <li class=\"list-item\">\n                          <button class=\"btn btn-success btn-style\"> Proceed </button>\n                        </li>\n                        <li class=\"list-item\">\n                          <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> Cancel </button>\n                        </li>\n                      </ul>\n\n                    </div>\n                  </div>\n                  <div class=\"tab-pane\" id=\"addEditDelete\">\n                    <div class=\"main-customizeform-date\">\n                      <div class=\"add-delete-section\">\n                        <span class=\"Effective-date\">\n                          Effective date?\n                        </span>\n                        <br>\n                        <ul class=\"list-unstyled date-list\">\n                          <li class=\"list-items\">\n                            <label>Start Date</label>\n                            <br>\n\n                            <input type=\"text\" class=\"benefits-dropdown\">\n                          </li>\n                          <li class=\"list-items\">\n                            <label>End Date</label>\n                            <br>\n                            <input type=\"text\" class=\"benefits-dropdown\">\n\n                          </li>\n                        </ul>\n\n                        <ul class=\"list-unstyled question-list\">\n                          <li class=\"list-items\">\n                            <label>Make Eligible for Benefits?</label>\n                            <br>\n                            <select class=\"form-control benefits-dropdown\">\n                              <option>Yes</option>\n                              <option>No</option>\n                            </select>\n                          </li>\n                        </ul>\n                      </div>\n                      <div class=\"margin-top\">\n                        <ul class=\"submit-buttons list-unstyled\">\n                          <li class=\"list-item\">\n                            <button class=\"btn btn-success btn-style\"> Proceed </button>\n                          </li>\n                          <li class=\"list-item\">\n                            <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> Cancel </button>\n                          </li>\n                        </ul>\n\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"tab-pane\" id=\"summaryReview\">\n                    <div class=\"main-customizeform\">\n                      <div class=\"main-customizeform\">\n                        <ul class=\"list-unstyled question-list\">\n                          <li class=\"list-items\">\n                            <label>A.Make Eligible for Benefits?</label>\n                            <br>\n                            <select class=\"form-control benefits-dropdown\">\n                              <option>Yes</option>\n                              <option>No</option>\n                            </select>\n                          </li>\n                        </ul>\n                      </div>\n                      <div class=\"margin-top\">\n                        <ul class=\"submit-buttons list-unstyled\">\n                          <li class=\"list-item\">\n                            <button class=\"btn btn-success btn-style\"> Proceed </button>\n                          </li>\n                          <li class=\"list-item\">\n                            <button class=\"btn btn-style\" (click)=\"modalRef.hide()\"> Cancel </button>\n                          </li>\n                        </ul>\n\n                      </div>\n\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n      </ng-template>\n    </div>\n    <div class=\"Employee-filters\">\n      <div class=\"col-xs-12 border-bottom\">\n        <div class=\"row\">\n          <div class=\"col-xs-6\">\n            <div class=\"row\">\n              <div class=search-bar>\n                <form [formGroup]=\"employeeSearch\" class=\"search-form\">\n                  <input type=\"text\" class=\"border-none box-shadow-none form-control\" formControlName=\"search\"\n                    placeholder=\"Search by name,title, etc\">\n                </form>\n                <span>\n                  <span class=\"glyphicon glyphicon-search search-icon\" aria-hidden=\"true\"></span>\n                </span>\n              </div>\n            </div>\n          </div>\n\n\n          <div class=\"display-right col-xs-5 pull-right\">\n\n            <mat-select class=\"form-control\" (click)=\"filterOptionsClear()\" placeholder=\"All Employees\">\n              <mat-option>All Employees</mat-option>\n\n              <mat-accordion>\n\n                <mat-expansion-panel >\n                  <mat-expansion-panel-header>\n                    <mat-panel-title>\n                      Employment Status\n                    </mat-panel-title>\n                  </mat-expansion-panel-header>\n\n                  <div class=\"display-settings\">\n                    <ul>\n                      <li *ngFor=\"let data of StructureValuesEmpStatus\">\n                        <mat-checkbox #checkbox=\"matCheckbox\" [(ngModel)]=\"data.isChecked\" [checked]=\"data.selected\"\n                          (change)=\"filterEmployee($event,'Employment Status',data)\" [value]=\"data._id\">{{data.name}}\n                        </mat-checkbox>\n                      </li>\n\n                    </ul>\n                  </div>\n\n                </mat-expansion-panel>\n\n\n                <mat-expansion-panel >\n                  <mat-expansion-panel-header>\n                    <mat-panel-title>\n                      Location\n                    </mat-panel-title>\n                  </mat-expansion-panel-header>\n\n                  <div class=\"display-settings\">\n                    <ul>\n                      <li *ngFor=\"let data of StructureValuesLocation\">\n                        <mat-checkbox #checkbox=\"matCheckbox\" [(ngModel)]=\"data.isChecked\" [checked]=\"data.selected\"\n                          (change)=\"filterEmployee($event,'Location',data)\" [value]=\"data._id\">{{data.name}}\n                        </mat-checkbox>\n                      </li>\n                    </ul>\n                  </div>\n\n                </mat-expansion-panel>\n\n\n                <mat-expansion-panel >\n                  <mat-expansion-panel-header>\n                    <mat-panel-title>\n                      Employee Type\n                    </mat-panel-title>\n                  </mat-expansion-panel-header>\n\n                  <div class=\"display-settings\">\n                    <ul>\n                        <li *ngFor=\"let data of StructureValuesEmpType\">\n                        <mat-checkbox #checkbox=\"matCheckbox\" [(ngModel)]=\"data.isChecked\" [checked]=\"data.selected\"\n                        (change)=\"filterEmployee($event,'Employee Type',data)\" [value]=\"data._id\">{{data.name}}\n                      </mat-checkbox>\n                      </li>\n                    </ul>\n                  </div>\n\n                </mat-expansion-panel>\n\n                <mat-expansion-panel >\n                  <mat-expansion-panel-header>\n                    <mat-panel-title>\n                      Department\n                    </mat-panel-title>\n                  </mat-expansion-panel-header>\n\n                  <div class=\"display-settings\">\n                    <ul>\n                        <li *ngFor=\"let data of StructureValuesDepartment\">\n                     <mat-checkbox #checkbox=\"matCheckbox\" [(ngModel)]=\"data.isChecked\" [checked]=\"data.selected\"\n                        (change)=\"filterEmployee($event,'Department',data)\" [value]=\"data._id\">{{data.name}}\n                      </mat-checkbox>\n                      </li>\n                    </ul>\n                  </div>\n\n                </mat-expansion-panel>\n\n\n              </mat-accordion>\n\n            </mat-select>\n\n            <label>Display</label>\n\n            <div class=\"clearfix\"></div>\n          </div>\n\n\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n    <div class=\"employees-details\">\n      <accordion class=\"margin-top-15\">\n        <accordion-group #group [isOpen]=true>\n\n          <span accordion-heading class=\"panel-border\">\n            Employee Details &nbsp;\n          </span>\n          <div *ngFor=\"let employee of allEmployeeDetails\">\n            <!-- <div class=\"col-xs-12 individual-employee\" *ngIf=\"individualEmployee\"> \n              <div class=\"col-xs-1\">\n                <div class=\"row\">\n\n\n                </div>\n              </div> \n               <div class=\"col-xs-11 name-employee\">\n                <div class=\"row\">\n                  <a (click)=\"detailEmployee()\">\n                    {{employee.personal.name.firstName}} {{employee.personal.name.middleName}} {{employee.personal.name.lastName}}\n\n                    <i class=\"glyphicon glyphicon-triangle-right angle\"></i>\n                  </a>\n                </div>\n\n              </div> \n             </div> -->\n            <div *ngIf=\"individualEmployeeDropdown\" class=\"col-xs-12 employee-individual-dropdown individual-employee\">\n              <div class='row'>\n                <a>\n\n                  <div class=\"col-xs-1\">\n                    <div class=\"row\">\n                      <!-- <img src=\"../../../assets/images/Directory/empoyee_2.png\"> -->\n                      <span *ngIf=\"!employee.personal.profile_pic_details\">\n                        <i *ngIf=\"directorySettings.settings.photo\" class=\"material-icons emplployee-icon\">\n                          account_circle\n                        </i>\n                      </span>\n                      <span *ngIf=\"directorySettings.settings.photo\">\n                        <img *ngIf=\"employee.personal.profile_pic_details\"\n                          [src]=\"employee.personal.profile_pic_details.url\" class=\"s3img\" alt=\"employee-img\">\n                      </span>\n\n                    </div>\n                  </div>\n                  <div class=\"col-xs-7\">\n                    <div class=\"row\">\n                      <ul class=\"list-unstyled list-profile\" (click)=\"editDetailsOfEmployee(employee)\">\n                        <li class=\"name\">\n                          <a style=\"cursor:pointer;\">\n                            <b><span\n                                *ngIf=\"directorySettings.settings.name\">{{employee.personal.name.firstName}}</span>&nbsp;&nbsp;\n                              <span\n                                *ngIf=\"directorySettings.settings.name\">{{employee.personal.name.middleName}}</span>&nbsp;&nbsp;\n                              <span\n                                *ngIf=\"directorySettings.settings.name\">{{employee.personal.name.lastName}}</span>&nbsp;&nbsp;\n                              <!-- <i class=\"glyphicon glyphicon-triangle-bottom angle\"></i> -->\n                            </b>\n                          </a>\n                        </li>\n                        <li *ngIf=\"directorySettings.settings.job_title\" class=\"list-details\">\n                          <span *ngIf=\"employee.job.jobTitle\">{{employee.job.jobTitle.name}}</span>\n                          <span *ngIf=\"!employee.job.jobTitle\">--</span>\n                         \n                       \n                        </li>\n                        <li *ngIf=\"directorySettings.settings.location\" class=\"list-details\">\n                          {{employee.personal.address.city}},{{employee.personal.address.state}}\n                        </li>\n                        <li *ngIf=\"directorySettings.settings.location\" class=\"list-details\">\n                          {{employee.personal.address.country}}</li>\n                        <!-- <li class=\"linked-in symbol\">\n                    <img src=\"../../../assets/images/Directory/layer1_1_.png\">\n                </li> -->\n                      </ul>\n                      <img src=\"../../../assets/images/Directory/layer1_1_.png\"\n                        (click)=\"linkedInLink(employee.personal.social_links.linkedin)\" class=\"list-profile linked-in\">\n                    </div>\n                  </div>\n\n                  <div class=\"col-xs-4 linked-in\" (click)=\"editDetailsOfEmployee(employee)\">\n                    <div class=\"row\">\n                      <ul class=\"list-personal list-unstyled\">\n                        <li *ngIf=\"directorySettings.settings.email\" class=\"list-emp-detail\">\n                          <img src=\"../../../assets/images/Directory/ic_email_24px.png\" class=\"img-emp-info\">\n                          <span class=\"emp-info\">{{employee.personal.contact.workMail}}</span>\n                        </li>\n                        <li *ngIf=\"directorySettings.settings.work_phone\" class=\"list-emp-detail\">\n                          <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" class=\"img-emp-info\">\n                          <span class=\"emp-info\">{{employee.personal.contact.workPhone}}\n                            Ext.{{employee.personal.contact.extention}}</span>\n                        </li>\n                        <li *ngIf=\"directorySettings.settings.cell_phone\" class=\"list-emp-detail\">\n                          <img src=\"../../../assets/images/Directory/Group 504.png\" class=\"img-emp-info-mobile\">\n                          <span class=\"emp-info\">&nbsp;{{employee.personal.contact.homePhone}}</span>\n                        </li>\n                      </ul>\n                    </div>\n                  </div>\n                </a>\n              </div>\n            </div>\n          </div>\n          <!-- <div *ngIf=\"individualEmployeefly\" class=\"\">\n          <div class=\"col-xs-12 individual-employee margin-top-15\">\n            <div class=\"col-xs-1\">\n              <div class=\"row\">\n                <img src=\"../../../assets/images/Directory/empoyee_-2.png\">\n              </div>\n              <img src=\"../../../assets/images/Directory/ic_flight_takeoff_24px.png\">\n\n            </div>\n            <div class=\"col-xs-11 name-employee\">\n              <div class=\"row\">\n                <a (click)=\"detailEmployeefly()\">\n                  Crane Andy\n\n                  <i class=\"glyphicon glyphicon-triangle-right angle\"></i>\n                </a>\n              </div>\n\n            </div>\n          </div>\n        </div> -->\n          <!-- <div *ngIf=\"individualEmployeeflyDropdown\" class=\"col-xs-12 employee-individual-dropdown individual-employee margin-top\">\n          <div class='row'>\n            <div class=\"col-xs-1\">\n              <div class=\"row\">\n                <img src=\"../../../assets/images/Directory/empoyee_-3.png\">\n              </div>\n\n              <img src=\"../../../assets/images/Directory/ic_flight_takeoff_24px.png\" class=\"margin-left\">\n              <span class=\"out-date\"> OUT JUL 23-28</span>\n            </div>\n            <div class=\"col-xs-7\">\n              <div class=\"row\">\n                <ul class=\"list-unstyled list-profile\">\n                  <li class=\"name\" (click)=\"hideEmployeeflyData()\">\n                    <b>Crane Andy\n                      <i class=\"glyphicon glyphicon-triangle-bottom angle\"></i>\n                    </b>\n                  </li>\n                  <li class=\"list-details\">Sr. HR Administrator in Human Resources</li>\n                  <li class=\"list-details\"> Lindon,Utah</li>\n                  <li class=\"list-details\"> North America</li>\n                  //<li class=\"linked-in symbol\">\n                      //  <img src=\"../../../assets/images/Directory/layer1_1_.png\">\n                    //</li>\n                </ul>\n                <img src=\"../../../assets/images/Directory/layer1_1_.png\" class=\"list-profile\">\n              </div>\n            </div>\n            <div class=\"col-xs-4\">\n              <div class=\"row\">\n                <ul class=\"list-personal list-unstyled\">\n                  <li class=\"list-emp-detail\">\n                    <img src=\"../../../assets/images/Directory/ic_email_24px.png\" class=\"img-emp-info\">\n                    <span class=\"emp-info\">craneandy@efficientoffice.com</span>\n                  </li>\n                  <li class=\"list-emp-detail\">\n                    <img src=\"../../../assets/images/Directory/ic_domain_24px.png\" class=\"img-emp-info\">\n                    <span class=\"emp-info\">801-724-6600 Ext.1272</span>\n                  </li>\n                  <li class=\"list-emp-detail\">\n                    <img src=\"../../../assets/images/Directory/Group 504.png\" class=\"img-emp-info-mobile\">\n                    <span class=\"emp-info\">&nbsp;415-555-8985</span>\n                  </li>\n                </ul>\n              </div>\n            </div>\n          </div>\n        </div> -->\n        </accordion-group>\n\n      </accordion>\n    </div>\n    <mat-paginator [length]=\"allEmpLength\" [pageSize]=\"10\" (page)=\"pageEvent($event)\"\n      [pageSizeOptions]=\"[5, 10, 25, 100]\">\n    </mat-paginator>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory.component.ts ***!
  \**************************************************************************************/
/*! exports provided: DirectoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectoryComponent", function() { return DirectoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _directory_settings_directory_settings_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./directory-settings/directory-settings.component */ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.ts");
/* harmony import */ var _services_directory_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/directory.service */ "./src/app/services/directory.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { Router } from '@angular/router';






var DirectoryComponent = /** @class */ (function () {
    function DirectoryComponent(router, dialog, modalService, employeeOnboardingService, swalAlertService, accessLocalStorageService, directoryService, companySettingsService, loaderService) {
        var _this = this;
        this.router = router;
        this.dialog = dialog;
        this.modalService = modalService;
        this.employeeOnboardingService = employeeOnboardingService;
        this.swalAlertService = swalAlertService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.directoryService = directoryService;
        this.companySettingsService = companySettingsService;
        this.loaderService = loaderService;
        this.individualEmployee = true;
        this.individualEmployeeDropdown = true;
        this.individualEmployeefly = true;
        this.individualEmployeeflyDropdown = false;
        this.updateBenefits = true;
        this.chooseFields = false;
        this.addEditDelete = false;
        this.summaryReview = false;
        this.directorySettings = {
            "companyId": "",
            "settings": {
                "photo": true,
                "name": true,
                "job_title": true,
                "location": true,
                "email": true,
                "work_phone": true,
                "cell_phone": true,
                "department": true,
                "business_unit": true
            }
        };
        this.allEmployeeDetails = [
            {
                personal: {
                    name: {
                        firstName: '',
                        middleName: '',
                        lastName: '',
                    },
                    address: {
                        city: '',
                        state: '',
                        country: ''
                    },
                    contact: {
                        workMail: '',
                        workPhone: '',
                        extention: '',
                        homePhone: ''
                    }
                },
                job: {
                    jobTitle: {
                        name: '',
                    }
                },
            }
        ];
        this.filterData = {
            location: [],
            employment_status: [],
            department: [],
            employment_type: []
        };
        this.StructureValues = [];
        this.StructureValuesEmpStatus = [];
        this.customValuesEmpStatus = [];
        this.StructureValuesLocation = [];
        this.customValuesLocation = [];
        this.StructureValuesEmpType = [];
        this.customValuesEmpType = [];
        this.StructureValuesDepartment = [];
        this.customValuesDepartment = [];
        this.customValues = [];
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
    }
    DirectoryComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_10__["NavigationStart"]) {
            this.loaderService.loader(true);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_10__["NavigationEnd"]) {
            this.loaderService.loader(false);
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_10__["NavigationCancel"]) {
            this.loaderService.loader(false);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_10__["NavigationError"]) {
            this.loaderService.loader(false);
        }
    };
    DirectoryComponent.prototype.ngOnInit = function () {
        this.employeeSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('')
        });
        this.perPage = 10;
        this.pageNo = 1;
        this.onChanges();
        this.companyId = JSON.parse(localStorage.getItem('companyId'));
        this.getDirectorySettings(this.companyId);
        this.getEmployeeDetails();
        // this.getStructureFields()
        this.getStructureFields('Employment Status');
        this.getStructureFields('Location');
        this.getStructureFields('Employee Type');
        this.getStructureFields('Department');
    };
    DirectoryComponent.prototype.getStructureFields = function (fields) {
        var _this = this;
        // this.customValues = []
        // this.StructureValues = []
        this.companySettingsService.getStuctureFields(fields, 0).subscribe(function (res) {
            console.log(res, name);
            if (fields == 'Employment Status') {
                _this.StructureValuesEmpStatus = res.default;
                _this.customValuesEmpStatus = res.custom;
                _this.customValuesEmpStatus.forEach(function (element) {
                    _this.StructureValuesEmpStatus.push(element);
                });
                _this.StructureValuesEmpStatus = _this.StructureValuesEmpStatus.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                });
            }
            else if (fields == 'Location') {
                _this.StructureValuesLocation = res.default;
                _this.customValuesLocation = res.custom;
                _this.customValuesLocation.forEach(function (element) {
                    _this.StructureValuesLocation.push(element);
                });
                _this.StructureValuesLocation = _this.StructureValuesLocation.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                });
            }
            else if (fields == 'Employee Type') {
                _this.StructureValuesEmpType = res.default;
                _this.customValuesEmpType = res.custom;
                _this.customValuesEmpType.forEach(function (element) {
                    _this.StructureValuesEmpType.push(element);
                });
                _this.StructureValuesEmpType = _this.StructureValuesEmpType.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                });
            }
            else if (fields = 'Department') {
                _this.StructureValuesDepartment = res.default;
                _this.customValuesDepartment = res.custom;
                _this.customValuesDepartment.forEach(function (element) {
                    _this.StructureValuesDepartment.push(element);
                });
                _this.StructureValuesDepartment = _this.StructureValuesDepartment.map(function (el) {
                    var o = Object.assign({}, el);
                    o.isChecked = false;
                    return o;
                });
            }
            console.log(_this.StructureValuesDepartment);
        }, function (err) {
            console.log(err);
        });
        // console.log(this.StructureValues);
    };
    DirectoryComponent.prototype.getDirectorySettings = function (id) {
        var _this = this;
        this.directoryService.getDirectorySettings(id)
            .subscribe(function (res) {
            console.log("get directory settings", res);
            _this.directorySettings.settings = res.data.settings;
        }, function (err) {
            console.log(err);
        });
    };
    DirectoryComponent.prototype.onChanges = function () {
        var _this = this;
        this.employeeSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            // console.log(this.zenworkersSearch.get('search').value);
            _this.getEmployeeDetails();
        });
    };
    DirectoryComponent.prototype.openModalWithClass = function (template) {
        this.selectedNav = 'updateBenefits';
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    };
    DirectoryComponent.prototype.detailEmployee = function () {
        this.individualEmployee = !this.individualEmployee;
        this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
    };
    // hideEmployeeData() {
    //   this.individualEmployeeDropdown = !this.individualEmployeeDropdown;
    //   this.individualEmployee = !this.individualEmployee;
    // }
    // detailEmployeefly() {
    //   this.individualEmployeefly = !this.individualEmployeefly;
    //   this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
    // }
    // hideEmployeeflyData() {
    //   this.individualEmployeeflyDropdown = !this.individualEmployeeflyDropdown;
    //   this.individualEmployeefly = !this.individualEmployeefly;
    // }
    DirectoryComponent.prototype.chooseFieldss = function (event) {
        this.selectedNav = event;
        if (event == "updateBenefits") {
            this.updateBenefits = true;
            this.addEditDelete = false;
            this.chooseFields = false;
            this.summaryReview = false;
        }
        else if (event == "chooseFields") {
            this.chooseFields = true;
            this.updateBenefits = false;
            this.addEditDelete = false;
            this.summaryReview = false;
        }
        else if (event == "addEditDelete") {
            this.addEditDelete = true;
            this.updateBenefits = false;
            this.chooseFields = false;
            this.summaryReview = false;
        }
        else if (event == 'summaryReview') {
            this.summaryReview = true;
            this.updateBenefits = false;
            this.addEditDelete = false;
            this.chooseFields = false;
        }
    };
    DirectoryComponent.prototype.pageEvent = function ($event) {
        console.log("pages", $event);
        this.pageNo = $event.pageIndex + 1;
        this.perPage = $event.pageSize;
        this.getEmployeeDetails();
    };
    /* Getting Employee Directory(All Employees) Details from Backend */
    DirectoryComponent.prototype.getEmployeeDetails = function () {
        var _this = this;
        var postData = {
            search_query: this.employeeSearch.get('search').value,
            page_no: this.pageNo,
            per_page: this.perPage,
            sort: {
                // "personal.name.firstName": 1,
                // "personal.name.lastName": 1,
                // "job.businessUnit": 1,
                // "job.department": 1,
                // "job.location": 1,
                updatedAt: -1
            },
            employment_status: [],
            employment_type: [],
            location: [],
            department: []
        };
        if (this.filterData.department.length > 0) {
            postData.department = this.filterData.department;
        }
        else if (this.filterData.employment_status.length > 0) {
            postData.employment_status = this.filterData.employment_status;
        }
        else if (this.filterData.employment_type.length > 0) {
            postData.employment_type = this.filterData.employment_type;
        }
        else if (this.filterData.location.length > 0) {
            postData.location = this.filterData.location;
        }
        this.employeeOnboardingService.getOnboardedEmployeeDetails(postData)
            .subscribe(function (res) {
            console.log("directory res", res);
            if (res.status) {
                // this.swalAlertService.SweetAlertWithoutConfirmation("Employees", res.message, 'success')
                _this.allEmployeeDetails = res.data;
                _this.allEmpLength = res.total_count;
                console.log(_this.allEmployeeDetails.length, "len");
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employees", "Employee Data Not Found", 'info');
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Employees", err.error.message, 'error');
        });
    };
    /* Getting Employee Directory Details from Backend Ending*/
    /* Edit Details of Employee */
    DirectoryComponent.prototype.editDetailsOfEmployee = function (employee) {
        this.accessLocalStorageService.set('employee', employee);
        this.router.navigate(['/admin/admin-dashboard/employee-management/profile/profile-view/personal']);
    };
    DirectoryComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_directory_settings_directory_settings_component__WEBPACK_IMPORTED_MODULE_7__["DirectorySettingsComponent"], {
            width: '1200px',
            height: '700px',
            backdropClass: 'directory-settings-model',
            data: ""
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.getDirectorySettings(_this.companyId);
            _this.getEmployeeDetails();
        });
    };
    DirectoryComponent.prototype.linkedInLink = function (link) {
        console.log(link);
        window.open(link, "_blank");
    };
    DirectoryComponent.prototype.openGroup = function (data) {
        console.log(data);
        this.getStructureFields(data);
    };
    DirectoryComponent.prototype.filterEmployee = function (event, data, value) {
        console.log(event, data, value);
        // event.preventDefault();
        // console.log('onClick this.ref._checked '+ this.ref._checked);
        // this.ref._checked = !this.ref._checked;
        if (data == 'Employee Type' && event.checked == true) {
            this.filterData.employment_type.push(value.name);
            this.StructureValuesEmpStatus.forEach(function (element) {
                if (element.name == value.name) {
                    element.isChecked = true;
                }
            });
        }
        if (data == 'Location' && event.checked == true) {
            this.filterData.location.push(value.name);
            this.StructureValuesLocation.forEach(function (element) {
                if (element.name == value.name) {
                    element.isChecked = true;
                }
            });
        }
        if (data == 'Employment Status' && event.checked == true) {
            this.filterData.employment_status.push(value.name);
            this.StructureValuesEmpStatus.forEach(function (element) {
                if (element.name == value.name) {
                    element.isChecked = true;
                }
            });
        }
        if (data == 'Department' && event.checked == true) {
            this.filterData.department.push(value.name);
            this.StructureValuesLocation.forEach(function (element) {
                if (element.name == value.name) {
                    element.isChecked = true;
                }
            });
        }
        if (data == 'Employee Type' && event.checked == false) {
            var index = this.filterData.employment_type.indexOf(value.name);
            this.filterData.employment_type.splice(index, 1);
        }
        if (data == 'Location' && event.checked == false) {
            var index = this.filterData.employment_type.indexOf(value.name);
            this.filterData.location.splice(index, 1);
        }
        if (data == 'Employment Status' && event.checked == false) {
            var index = this.filterData.employment_type.indexOf(value.name);
            this.filterData.employment_status.splice(index, 1);
        }
        if (data == 'Department' && event.checked == false) {
            var index = this.filterData.employment_type.indexOf(value.name);
            this.filterData.department.splice(index, 1);
        }
        console.log(this.filterData);
        this.getEmployeeDetails();
    };
    DirectoryComponent.prototype.somethingClick = function (checkbox, item) {
        console.log(checkbox, item);
    };
    DirectoryComponent.prototype.filterOptionsClear = function () {
        this.filterData.department = [];
        this.filterData.employment_status = [];
        this.filterData.employment_type = [];
        this.filterData.location = [];
        this.StructureValuesEmpStatus.forEach(function (element) {
            element.isChecked = false;
        });
        this.StructureValuesLocation.forEach(function (element) {
            element.isChecked = false;
        });
        this.StructureValuesEmpType.forEach(function (element) {
            element.isChecked = false;
        });
        this.StructureValuesDepartment.forEach(function (element) {
            element.isChecked = false;
        });
        this.getEmployeeDetails();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('ref'),
        __metadata("design:type", Object)
    ], DirectoryComponent.prototype, "ref", void 0);
    DirectoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-directory',
            template: __webpack_require__(/*! ./directory.component.html */ "./src/app/admin-dashboard/employee-management/directory/directory.component.html"),
            styles: [__webpack_require__(/*! ./directory.component.css */ "./src/app/admin-dashboard/employee-management/directory/directory.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _services_employee_service__WEBPACK_IMPORTED_MODULE_4__["EmployeeService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__["AccessLocalStorageService"],
            _services_directory_service__WEBPACK_IMPORTED_MODULE_8__["DirectoryService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__["CompanySettingsService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_11__["LoaderService"]])
    ], DirectoryComponent);
    return DirectoryComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory.module.ts ***!
  \***********************************************************************************/
/*! exports provided: DirectoryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectoryModule", function() { return DirectoryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _directory_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./directory.router */ "./src/app/admin-dashboard/employee-management/directory/directory.router.ts");
/* harmony import */ var ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/accordion */ "./node_modules/ngx-bootstrap/accordion/fesm5/ngx-bootstrap-accordion.js");
/* harmony import */ var _directory_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./directory.component */ "./src/app/admin-dashboard/employee-management/directory/directory.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _directory_settings_directory_settings_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./directory-settings/directory-settings.component */ "./src/app/admin-dashboard/employee-management/directory/directory-settings/directory-settings.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var DirectoryModule = /** @class */ (function () {
    function DirectoryModule() {
    }
    DirectoryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _directory_router__WEBPACK_IMPORTED_MODULE_2__["DirectoryRouting"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"],
                ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_3__["AccordionModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"], _angular_material_expansion__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"]
            ],
            declarations: [_directory_component__WEBPACK_IMPORTED_MODULE_4__["DirectoryComponent"], _directory_settings_directory_settings_component__WEBPACK_IMPORTED_MODULE_6__["DirectorySettingsComponent"]],
            entryComponents: [
                _directory_settings_directory_settings_component__WEBPACK_IMPORTED_MODULE_6__["DirectorySettingsComponent"]
            ]
        })
    ], DirectoryModule);
    return DirectoryModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/directory/directory.router.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/directory/directory.router.ts ***!
  \***********************************************************************************/
/*! exports provided: DirectoryRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectoryRouting", function() { return DirectoryRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _directory_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./directory.component */ "./src/app/admin-dashboard/employee-management/directory/directory.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', redirectTo: "/directory", pathMatch: "full" },
    { path: 'directory', component: _directory_component__WEBPACK_IMPORTED_MODULE_2__["DirectoryComponent"] },
];
var DirectoryRouting = /** @class */ (function () {
    function DirectoryRouting() {
    }
    DirectoryRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]]
        })
    ], DirectoryRouting);
    return DirectoryRouting;
}());



/***/ }),

/***/ "./src/app/services/directory.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/directory.service.ts ***!
  \***********************************************/
/*! exports provided: DirectoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectoryService", function() { return DirectoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DirectoryService = /** @class */ (function () {
    function DirectoryService(http) {
        this.http = http;
    }
    DirectoryService.prototype.getDirectorySettings = function (companyId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/directory-settings/get/" + companyId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    DirectoryService.prototype.updateDirectorySettings = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/directory-settings/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    DirectoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DirectoryService);
    return DirectoryService;
}());



/***/ }),

/***/ "./src/app/services/site-access-roles-service.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/site-access-roles-service.service.ts ***!
  \***************************************************************/
/*! exports provided: SiteAccessRolesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteAccessRolesService", function() { return SiteAccessRolesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SiteAccessRolesService = /** @class */ (function () {
    function SiteAccessRolesService(http) {
        this.http = http;
    }
    SiteAccessRolesService.prototype.getStandardOnboardingtemplate = function (id) {
        // return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
        // .pipe(map(res => res));
    };
    SiteAccessRolesService.prototype.getAllEmployees = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/searchEmployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getuserRoleEmployees = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/users/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.chooseEmployeesData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/chooseEmployeesForRoles", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.postEmpDatatoRole = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addGroupsToUsers", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAlltabsData = function (cID, rID, category) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/get/" + cID + "/" + rID + "/" + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.UpdateRoleType = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getBaseRoles = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/baseroles")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.createRoleType = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/addUpdate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.allPagesData = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/all/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllPagesForCompany = function (cid, rid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/" + cid + "/" + rid)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getSingleCustomRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/role/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.pagesCreation = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/create/all", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.pagesUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/all/pages", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.personalFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.jobFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.emergencyFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.compensationFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.postpersonalfields = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/create", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.updateRoles = function (data) {
        console.log("seerveice array", data);
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.updateMultipleRoles = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/all/pages", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.companyId = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.deleteRole = function (id) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/delete/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllMyinfoFields = function (roleid, cId, category) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/get/" + cId + "/" + roleid + "/" + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesforRoles = function (cId, Rid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/users-under-group/" + cId + "/" + Rid)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesUnderManager = function (cId, uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/getAllDirectReports/" + cId + "/" + uId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesforCustomRoles = function (rId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/userIds/" + rId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.changeSiteAccessRoleforEmp = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/change-user-role", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SiteAccessRolesService);
    return SiteAccessRolesService;
}());



/***/ })

}]);
//# sourceMappingURL=directory-directory-module.js.map