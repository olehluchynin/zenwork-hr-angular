(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/services/chardgeCreditCard.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/chardgeCreditCard.service.ts ***!
  \*******************************************************/
/*! exports provided: CreditCardChargingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditCardChargingService", function() { return CreditCardChargingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CreditCardChargingService = /** @class */ (function () {
    function CreditCardChargingService(http) {
        this.http = http;
    }
    CreditCardChargingService.prototype.createCharge = function (token, id, amount, orderId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'token': token,
            'amount': amount
        });
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/orders/credit-charge", { id: id, amount: amount, token: token, orderId: orderId });
    };
    CreditCardChargingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CreditCardChargingService);
    return CreditCardChargingService;
}());



/***/ }),

/***/ "./src/app/services/employee-service.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/employee-service.service.ts ***!
  \******************************************************/
/*! exports provided: EmployeeServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeServiceService", function() { return EmployeeServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EmployeeServiceService = /** @class */ (function () {
    function EmployeeServiceService(http) {
        this.http = http;
    }
    EmployeeServiceService.prototype.getCompanyList = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/announcement/list/" + id);
    };
    EmployeeServiceService.prototype.AddCompanyData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/announcement/add", data);
    };
    EmployeeServiceService.prototype.deleteCompanayData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/announcement/delete", data);
    };
    EmployeeServiceService.prototype.companyEditData = function (id) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/announcement/edit", id);
    };
    EmployeeServiceService.prototype.getManagerSettings = function (comp) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/settings/get/" + comp);
    };
    EmployeeServiceService.prototype.managerSettingsAdd = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/settings/addOrUpdate", data);
    };
    EmployeeServiceService.prototype.companyLinkAdding = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/links/add", data);
    };
    EmployeeServiceService.prototype.companyLinksDeleteData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/links/delete", data);
    };
    EmployeeServiceService.prototype.companyLinkEditData = function (id) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-front-page/links/edit", id);
    };
    EmployeeServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EmployeeServiceService);
    return EmployeeServiceService;
}());



/***/ }),

/***/ "./src/app/services/manager-frontpage.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/manager-frontpage.service.ts ***!
  \*******************************************************/
/*! exports provided: ManagerFrontpageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerFrontpageService", function() { return ManagerFrontpageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ManagerFrontpageService = /** @class */ (function () {
    function ManagerFrontpageService(http) {
        this.http = http;
    }
    ManagerFrontpageService.prototype.getAllData = function (cId, epmId) {
        console.log("iddddd", epmId);
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/announcement/list/" + cId);
    };
    ManagerFrontpageService.prototype.submitData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/announcement/add", data);
    };
    ManagerFrontpageService.prototype.editAnnounceData = function (id) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/announcement/edit", id);
    };
    ManagerFrontpageService.prototype.deleteData = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/announcement/delete", id);
    };
    ManagerFrontpageService.prototype.getAllAdditionalData = function (cid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/settings/get/" + cid);
    };
    ManagerFrontpageService.prototype.additionalDataSave = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/settings/addOrUpdate", data);
    };
    ManagerFrontpageService.prototype.managerLinksAdd = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/links/add", data);
    };
    ManagerFrontpageService.prototype.managerLinkEdit = function (id) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/links/edit", id);
    };
    ManagerFrontpageService.prototype.managerLinksDelete = function (id) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-front-page/links/delete", id);
    };
    ManagerFrontpageService.prototype.managercontrols = function (cId) {
    };
    ManagerFrontpageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ManagerFrontpageService);
    return ManagerFrontpageService;
}());



/***/ }),

/***/ "./src/app/services/pay-roll.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/pay-roll.service.ts ***!
  \**********************************************/
/*! exports provided: PayRollService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayRollService", function() { return PayRollService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PayRollService = /** @class */ (function () {
    function PayRollService(http) {
        this.http = http;
    }
    PayRollService.prototype.addPayrollGroup = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-groups/add", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getPayFrequency = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payrollGroup/payfrequency")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getPayrollGroup = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-groups/list", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.deletePaygroups = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-groups/delete", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getSinglePayrollGroup = function (cId, pId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-groups/get/" + cId + '/' + pId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.updatePayrollGroup = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-groups/edit", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.addSalaryGrade = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/add", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.updateSalaryGrade = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/edit", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getSalrayGradeList = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/list", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getSingleSalaryGrade = function (cId, sId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/get/" + cId + '/' + sId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.deleteSalaryGrades = function (data) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/delete/" + data._ids[0])
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.deleteSalaryGradeBands = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/delete-grade-bands", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.previewCalander = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/get-pay-cycle-dates", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.singlePayrollCalanderAdd = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/add", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.pageEvents1 = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/list", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.deleteCalander = function (data) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/delete/" + data.companyId + '/' + data._ids[0])
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getPayrollCalender = function (cId, pcId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/get/" + cId + '/' + pcId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.singlePayrollCalanderUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/edit", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getSingleSalaraygradeBandsPagination = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/salary-grades/list-grade-bands", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.StartNextCycle = function (cId, calendarId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/current-and-next-cycle-details/" + cId + '/' + calendarId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.cycleDataSend = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/start-next-cycle", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.reopenPreviousCycleSubmit = function (postaData) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/reopen-cycle", postaData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.reopenPreviousCycle = function (cId, calendarId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/previous-and-current-cycle-details/" + cId + '/' + calendarId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.copyCalender = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/payroll-calendars/create-copy", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.getJobLinks = function (code) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getJobSalaryLinkDetails/" + code)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService.prototype.compensationRationCalculate = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/cal-compensation-ratio", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    PayRollService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PayRollService);
    return PayRollService;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map