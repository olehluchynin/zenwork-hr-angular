(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["black-out-dates-black-out-dates-module"],{

/***/ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".holidays-main-tab{\n    margin:20px auto 0; float: none; padding: 0;\n}\na{\n    color: #000;\n}\n.resumes-field-heading small{\n    font-size: 15px;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n/* .field-accordion .panel-default>.panel-heading{\n    background: #f8f8f8 !important;\n    position: relative;\n}\n.field-accordion .panel{\n    box-shadow: none;background: #f8f8f8;\n} */\n.field-accordion .panel-title a{\n    display: inline-flex;\n}\n.field-accordion .panel-title { display: inline-block;}\n.field-accordion .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.main-punches-filters {\n   width: 100%;\n}\n.main-punches-filters .search-category .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n    /* margin: 5px; */\n    position: relative;\n    font-size: 12px;\n}\n.main-punches-filters .search-category .list-items span{\n    cursor: pointer;\n    font-size: 15px;\n}\n.custom-tables { margin: 0 auto; float: none; padding: 30px 0 0;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px 10px; background: #eef7ff; border:none; font-size: 14px; border-radius:3px; vertical-align: middle;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 10px; background:#fff;border: none;vertical-align:middle;font-size: 13px;}\n.custom-fields .checkbox{ margin: 0;}\n.group-list .table>tbody>tr>td {\n  background: #f8f8f8;\n  border: none;\n  vertical-align: middle;\n  border-top: none;\n  border-bottom: #c3c3c3 1px solid;\n  color: #948e8e;\n}\n.submit-btn {\n  background-color: #439348;\n  border: 1px solid #439348;\n  border-radius: 35px;\n  color: #fff;\n  padding: 0 25px;    \n}\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n.heade-check .checkbox label::after { color:#616161;width: 20px; height: 20px; padding:2px 0 0 4px; font-size: 12px;}\n.heade-check .checkbox{ margin: 0;}\n.heade-check .checkbox label { padding-left: 0;}\n.cont-check .checkbox label::before {width: 20px; height: 20px; top: -1px;}\n.cont-check .checkbox label::after { color:#fff; background: #439348;width: 20px; height: 20px; padding: 3px 0 0 4px;border-radius: 3px;}\n.cont-check .checkbox{ margin: 0;}\n.cont-check .checkbox label { padding-left: 0;}\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n.custom-tables .table>tbody>tr>td .form-control {    border: none; width: 110px; box-shadow: none; color: #000; background: #f8f8f8;}\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.select-drop select { background:#fff; border: none; width: 80%; height: 34px; color:#948e8e; padding: 6px 10px;}\n.custom-tables .btn { float: right; color:#ec6469; border:#ec6469 1px solid; padding:5px 25px; border-radius: 20px; background: transparent; font-size: 15px; margin: 20px 0 0;}\n.birthdate{\n    font-size: 12px;\n    padding-left: 15px;\n    width: 178px;   \n    box-shadow: none;\n}\n.calendar-icon{\n    position: absolute;\n    top: 24px;\n    width: 15px;\n    right: 30px;\n\n}\n.start-date{\n    position: relative;\n}\n.cont-check .checkbox .checkbox-wrapper{\n    padding: 0 0 0 5px !important;\n}\n.tabs-left{\n    margin-top:0;\n}\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n  }\n.tabs-left > .nav-tabs {\n    float: left;\n    /* border-right: 1px solid #ddd; */\n  }\n.tabs-left > .nav-tabs > li > a {\n    font-size: 12px;\n    margin-right: -1px;\n    border-radius: 4px 0 0 4px;\n  }\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee;\n  }\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n/*side nav css ends*/\n.caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n.modal-body{\n    /* background:#f8f8f8; */\n    padding: 0;\n}\n.tabs-left .nav-tabs{\n    padding: 20px;\n    border-right: 0;\n}\n.tab-content{\n  margin: 0;\n  /* background: #fff; */\n}\n.sidenav-tabcontent{\n    background: #fff !important;\n  padding: 30px;\n\n}\n.tabs-customize{\n    background: #f8f8f8;\n    padding:0px!important;\n}\n.modal-body .container{\n   \n    margin: 20px 40px;\n}\n.text-field{\n  border: none; \n  box-shadow: none; \n  padding: 11px 12px; \n  height: 40px; \n  box-shadow: none;\n  background: #f8f8f8;\n  width: 64%;\n}\n.start-date1{\n  font-size: 14px;\n  padding-left: 15px;\n  box-shadow: none;\n  background: #f8f8f8;\n}\n.margin-btm {\n  border-bottom: 1px solid #fdeded;\n  padding-bottom: 20px;\n}\n.list-width {\n  width: 30%;\n}\n.text-field{\n  border: none; \n  box-shadow: none; \n  padding: 11px 12px; \n  height: 40px; \n  box-shadow: none;\n  background: #f8f8f8;\n}\nlabel{\n  font-weight: normal;\n  line-height: 30px;\n  color:#464444;\n  font-size: 14px;\n  padding: 0 0 0 2px;\n}\n/* .date { height: auto !important; line-height:inherit !important; width:99% !important;     display: block;\n  background: #fff;}\n.date button { display: inline-block; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 40px; min-width: auto;}\n\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:82%; height: auto; padding: 12px 0 12px 10px; display: inline-block;\n  vertical-align: middle; }\n.date .tier-width { width:25%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;} */\n.employees-data{\n  padding: 0px;\n}\n.field-accordion p {color: #565555;font-size: 15px;}\n.field-accordion .panel-default>.panel-heading { padding: 20px 0 20px; background-color:transparent !important; border: none;}\n.field-accordion .panel-title { color: #008f3d; display: inline-block;padding: 15px 0 0 0;}\n.field-accordion .panel-title a:hover { text-decoration: none;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n/* .heading-eligibility{\n  padding: 15px 0 0 30px !important;\n} */\n.field-accordion .eligibilty-border-bottom{\n  padding: 0 0 40px !important;\n}\n/* .employees-data{\n  padding: 30px;\n} */\n.structure-tbl-body {\n  background: #f8f8f8;\n  width: 45%;\n}\n.height-300 {\n  height: 300px;\n  overflow: auto;\n}\n.structure-list .list-items{\n  padding: 15px 15px;\n  /* border-bottom: 2px solid #eee; */\n}\n.structure-list .list-items-values{\n  padding: 17px 37px;\n  /* border-bottom: 2px solid #eee; */\n}\n.person-tables .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top: 0;height: 100%;}\n.person-tables .table>tbody>tr>td.zenwork-checked-border {position: relative;text-align: left}\n.arrow-position {\n  position: relative;\n}\n.arrow-align-one {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  left: 40px;\n  font-weight: lighter;\n  color: #439348; \n}\n.arrow-align-two {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  right: 0;\n  left: 0px;\n  font-weight: lighter;\n  color: #439348;\n}\n.employees-table{\n  margin: 60px 0 0 0;\n}\n.import-date-btn {\n  background-color: #797d7a;\n  border: 1px solid #797d7a;\n  padding: 5px 14px;\n  border-radius: 50px;\n  color: #fff;\n  margin-top: 30px;\n  outline: none;\n}\n.text-report {\n  font-size: 12px;\n\n}\n.date { height: 44px !important; line-height:inherit !important; background: #f8f8f8;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:72%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.submit-buttons .list-item {\n  display: inline-block;\n  }\n.submit-buttons .btn-success {\n  background-color: #439348;\n}\n.btn-style {\n  padding: 6px 20px !important;\n  border-radius: 30px!important;\n  /* font-size: 12px; */\n  }\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -64px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.add-edit-blockout { float:none; margin: 0 auto; padding:20px 0 0;}\n.employee-group {\n  padding: 0 40px;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"holidays-main-tab col-md-11\">\n  <div class=\"field-accordion\">\n\n    <div class=\"panel-group\" id=\"accordion\">\n      <div class=\"panel panel-default\">\n         <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\"\n              href=\"#collapse_currentschedule\">\n              <div class=\"resumes-field-heading\">\n                <p>Blackout Dates</p>\n              </div>\n            </a>\n          </h4>\n         \n             <ng-template #template>\n              <div class=\"modal-header\">\n                <h4 class=\"modal-title pull-left \">Add / Edit Blockout Employee Group</h4>\n                <div class=\"clearfix\"></div>\n              </div>\n              <div class=\"modal-body employee-group\">\n\n               <div class=\"add-edit-blockout\">\n                  \n                    <div class=\"col-xs-4\">\n\n                      <label>Blackout Employee Group Name</label>\n                      <input type=\"text\" class=\"form-control text-field\" [(ngModel)]=\"blackoutGroup.name\">\n\n                    </div>\n\n\n                    <div class=\"col-xs-5\">\n                      <label>Blackout Employee Group Name Effective Date</label>\n\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker\" placeholder=\"mm / dd / yy\"\n                          class=\"form-control text-field\" [(ngModel)]=\"blackoutGroup.effectiveDate\">\n                        <mat-datepicker #picker></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker.open()\" class=\"text-field\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                      </div>\n                    </div>\n\n                    <div class=\"clearfix\"></div>\n                  \n                  <div class=\"employees-data\">\n                    <div class=\"field-accordion\">\n                      <div class=\"panel-group\" id=\"accordion2\">\n                        <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                          <div class=\"panel-heading\">\n                            <h4 class=\"panel-title heading-eligibility\">\n                              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\"\n                                href=\"#eligibility-info\">\n                                Blackout Eligible Criteria\n                              </a>\n                            </h4>\n                          </div>\n                          <div id=\"eligibility-info\" class=\"panel-collapse collapse in\">\n                            <div class=\"panel-body eligibilty-border-bottom\">\n\n                              <div class=\"employees-data\">\n                                <div class=\"col-sm-12 table-body margin-btm\">\n                                  <div class=\"row\">\n                                    <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                                      <ul class=\"list-unstyled structure-list\" *ngFor=\"let fields of structureFields\">\n\n                                        <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                                          <div>\n\n                                            <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"fields.isChecked\"\n                                              [value]=\"fields.name\"\n                                              (ngModelChange)=\"getStructureSubFields(fields.name,fields.isChecked )\">\n                                              {{fields.name}}\n                                            </mat-checkbox>\n                                          </div>\n\n                                        </li>\n\n                                      </ul>\n                                    </div>\n\n                                    <div class=\"col-sm-1 text-center arrow-position\">\n                                      <i class=\"material-icons arrow-align-one\">\n                                        keyboard_arrow_right\n                                      </i>\n                                      <i class=\"material-icons arrow-align-two\">\n                                        keyboard_arrow_right\n                                      </i>\n                                    </div>\n                                    <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                                      <div *ngIf=\"StructureValues\">\n                                        <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of StructureValues\">\n\n                                          <li class=\"list-items-values\">\n                                            <mat-checkbox class=\"checkbox-success\"\n                                              (change)=\"subFieldsStructure($event,values.name)\" [value]=\"values.name\">\n                                              {{values.name}}\n                                            </mat-checkbox>\n\n                                          </li>\n\n                                        </ul>\n\n                                      </div>\n                                      <div *ngIf=\"customValues\">\n                                        <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of customValues\">\n\n                                          <li class=\"list-items-values\">\n                                            <mat-checkbox class=\"checkbox-success\"\n                                              (change)=\"subFieldsStructure($event,values.name)\" [value]=\"values.name\">\n                                              {{values.name}}\n                                            </mat-checkbox>\n                                          </li>\n\n                                        </ul>\n\n                                      </div>\n                                    </div>\n\n                                  </div>\n                                </div>\n                                <div class=\"panel-group margin-btm\" id=\"accordion3\">\n                                  <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                                    <div class=\"panel-heading\">\n                                      <h4 class=\"panel-title heading-eligibility\">\n                                        <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                                          href=\"#specific-employee\">\n                                          Specific Employee\n                                        </a>\n                                      </h4>\n                                    </div>\n                                    <div id=\"specific-employee\" class=\"panel-collapse collapse in\">\n                                      <div class=\"panel-body eligibilty-border-bottom row\">\n                                        <!-- <div class=\"col-xs-6\">\n                                          <h4>Include a Specific Person</h4>\n                                          <ng-multiselect-dropdown [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                                            [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                                            (onSelectAll)=\"onSelectAll($event)\" (onDeSelect)=\"OnItemDeSelect($event)\" (onDeSelectAll)=\"onDeSelectAll($event)\">\n                                          </ng-multiselect-dropdown>\n                                        </div>\n                                        <div class=\"col-xs-6\">\n                                          <h4>Exclude a Specific Person</h4>\n                                          <ng-multiselect-dropdown [placeholder]=\"'custom placeholder'\"\n                                            [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                                            [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                                            (onSelectAll)=\"onSelectAll($event)\">\n                                          </ng-multiselect-dropdown>\n                                        </div> -->\n                                        <div>\n                                          <h4 style=\"float: left;\">Include a Specific Person</h4>\n                                          <h4 style=\"float: right;\">Exclude a Specific Person</h4>\n                                          <div class=\"clearfix\"></div>\n                                          <dual-list [source]=\"dropdownList\" [(destination)]=\"selectedUsers\" [format]=\"format\" filter=true\n                                          height='300px' (click)=\"onSelectUsers(selectedUsers)\">\n                                        </dual-list>\n                                        </div>\n                                      </div>\n                                    </div>\n                                  </div>\n                                </div>\n                                <div class=\"margin-btm row\">\n                                  <h4 style=\"margin-left:15px;\">Assign Initial Group Effective Date</h4>\n                                  <div class=\"col-xs-3\">\n                                    <label>Select Date</label><br>\n                                    <mat-select class=\"form-control text-field\" [(ngModel)]=\"blackoutGroup.hireDate\" (ngModelChange)=\"selectEffectiveDate($event)\">\n                                      <mat-option value=\"date_of_hire\">Date of Hire</mat-option>\n                                      <mat-option value=\"custom_date\">Custom Date</mat-option>\n                                    </mat-select>\n                                  </div>\n                                  <div class=\"col-xs-3\" *ngIf=\"showField1\">\n                                    <label>Select Custom Date Field</label><br>\n                                    <div class=\"date\">\n                                      <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm / dd / yy\"\n                                        class=\"form-control text-field\" [(ngModel)]=\"blackoutGroup.customDate\">\n                                      <mat-datepicker #picker2></mat-datepicker>\n                                      <button mat-raised-button (click)=\"picker2.open()\" class=\"text-field\">\n                                        <span>\n                                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                        </span>\n                                      </button>\n                                    </div>\n                                    <!-- <mat-select class=\"form-control text-field\" [(ngModel)]=\"blackoutGroup.customDate\">\n                                      <mat-option value=\"Custom Date1\">Custom Date1</mat-option>\n                                      <mat-option value=\"Custom Date2\">Custom Date2</mat-option>\n                                    </mat-select> -->\n                                  </div>\n                                  <!-- <div class=\"col-xs-3\">\n                                    <button class=\"import-date-btn\">Import Custom Date</button>\n                                  </div> -->\n\n                                </div>\n\n                                <div class=\"margin-btm\">\n\n                                  <h4>Preview Employee Benefit Eligibility Group List\n                                    <button class=\"import-date-btn pull-right text-report\">Run Report</button>\n\n                                  </h4>\n                                  <div class=\"clear-fix\"></div>\n                                  <span>(Total Employees - {{count}})</span>\n                                  <div class=\"custom-tables group-list\">\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th>\n                                            Employee Name\n                                          </th>\n                                          <th>Employee ID</th>\n                                          <th>Manager Name</th>\n                                          <th>Department</th>\n                                          <th>Effective Date<br><small>(Custom Date1)</small></th>\n                                          <th>Benefit Eligibility Group</th>\n\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n                                        <tr *ngFor=\"let list of previewList\">\n                                          <td>{{list.personal.name.firstName}}</td>\n                                          <td>{{list.job.employeeId}}</td>\n                                          <td>\n                                            <span *ngIf=\"list.job.ReportsTo\">\n                                                {{list.job.ReportsTo.personal.name.firstName}}\n                                            </span>\n                                            \n                                          </td>\n                                          <td>{{list.job.department}}</td>\n                                          <td></td>\n                                          <td></td>\n                                        </tr>\n                                    </table>\n                                    \n                                  </div>\n                                  <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                                  </mat-paginator>\n                                </div>\n                                <div class=\"row\" style=\"margin:30px 0px;\">\n  \n                                  <button mat-button (click)=\"decline()\">Cancel</button>\n          \n                                  <button mat-button class=\"submit-btn pull-right\" (click)=\"createBlackoutEmployeeGroup()\">Submit</button>\n                                </div>\n                                <div class=\"clearfix\"></div>\n\n                              </div>\n\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n\n\n\n                    </div>\n                  </div>\n               \n              </div>\n            </div>\n\n\n            </ng-template>\n          \n        </div>\n\n         <div id=\"collapse_currentschedule\" class=\"panel-collapse collapse in\">\n\n          <div class=\"panel-body\">\n\n            <div class=\"custom-tables\">\n\n              <table class=\"table\">\n                 <thead>\n                  <tr>\n                   <th></th>\n                    <th>Location</th>\n                    <th>Name</th>\n                    <th>Employee Groups</th>\n                    <th>Start Date</th>\n                    <th>End Date</th>\n                    <th>Blockout Type</th>\n\n                    <th>\n                      <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                      <mat-menu #menu1=\"matMenu\">\n                        <button *ngIf=\"!checkBlackoutIds.length >=1\" mat-menu-item (click)=\"pushBlockouts()\"> Add Blackout Date</button>\n                        <button *ngIf=\"!checkBlackoutIds.length >=1\" mat-menu-item (click)=\"openModalWithClass(template)\"> Add Blackout Employee Group\n                        </button>\n                        <button *ngIf=\"(!checkBlackoutIds.length <=0) && (checkBlackoutIds.length ==1)\" mat-menu-item (click)=\"editBlackout()\"> Edit </button>\n                        <button *ngIf=\"(!checkBlackoutIds.length <=0) && (checkBlackoutIds.length >=1)\" mat-menu-item (click)=\"deleteBlackout()\"> Delete </button>\n\n                      </mat-menu>\n                    </th>\n                  </tr>\n                </thead>\n                <tbody>\n\n                  <tr *ngFor = \"let blackout of blackOuts\" style=\"border-bottom: 1px solid #efe6e6;\">\n                    <td [ngClass]=\"{'zenwork-checked-border': blackout.isChecked}\">\n                      <mat-checkbox [(ngModel)]=\"blackout.isChecked\" (change)=\"selectBlackout($event, blackout._id, blackout)\">\n\n                      </mat-checkbox>\n                    </td>\n                    <td>\n                      <mat-select class=\"form-control select-btn\" [(ngModel)]=\"blackout.location_id\" [disabled]=\"blackout.edit\">\n                        <mat-option *ngFor=\"let location of allLocation\" [value]=\"location._id\">{{location.name}}</mat-option>\n                      </mat-select> \n                    </td>\n                    <td>\n                    <input type=\"text\" class=\"form-control text-fields\" [(ngModel)]=\"blackout.blackoutName\" [readOnly]=\"blackout.edit\">\n\n                    </td>\n                    <td>\n                      <mat-select class=\"form-control select-btn\" [(ngModel)]=\"blackout.blackoutEmployeeGroup\" [disabled]=\"blackout.edit\">\n                        <mat-option *ngFor=\"let group of groupNames\" [value]=\"group._id\">{{group.name}}</mat-option>\n                      </mat-select> \n                    </td>\n                    <td>\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"blackout.startDate\"\n                        [max]=\"blackout.endDate\" [disabled]=\"blackout.edit\">\n                        <mat-datepicker #picker></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    <td>\n                      <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"blackout.endDate\"\n                        [min]=\"blackout.startDate\" [disabled]=\"blackout.edit\">\n                        <mat-datepicker #picker1></mat-datepicker>\n                        <button mat-raised-button (click)=\"picker1.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </button>\n                        <div class=\"clearfix\"></div>\n                      </div>\n                    </td>\n\n                    <td>\n                      <mat-select class=\"form-control text-field\" [(ngModel)]=\"blackout.blockType\" [disabled]=\"blackout.edit\">\n                        <mat-option value=\"Block\">Block</mat-option>\n                        <mat-option value=\"Single Day Request Only\">Single Day Request Only</mat-option>\n\n                      </mat-select>\n                    </td>\n                    <td></td>\n                  </tr>\n\n\n              </table>\n\n            </div>\n\n             <div class=\"form-group button-section\">\n                <ul class=\"submit-buttons list-unstyled\">\n                    <li class=\"list-item\">\n                        <button class=\"btn btn-style\" [disabled]=\"disableCancel\" (click)=\"cancelBlackout()\"> cancel </button>\n                      </li>\n                  <li class=\"list-item pull-right\">\n                    <button *ngIf = \"!blackoutId\" class=\"btn btn-success btn-style\" (click)=\"createBlackout()\"> Save Blackout </button>\n                    <button *ngIf = \"blackoutId\" class=\"btn btn-success btn-style\" (click)=\"updateBlackout()\"> Update Blackout </button>\n\n                  </li>\n                 \n                </ul>\n              </div>\n          </div>\n\n        </div>\n      </div>\n    </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: BlackOutDatesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackOutDatesComponent", function() { return BlackOutDatesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var BlackOutDatesComponent = /** @class */ (function () {
    function BlackOutDatesComponent(modalService, companySettingsService, accessLocalStorageService, leaveManagementService, siteAccessRolesService, myInfoService, swalAlertService) {
        this.modalService = modalService;
        this.companySettingsService = companySettingsService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.leaveManagementService = leaveManagementService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.myInfoService = myInfoService;
        this.swalAlertService = swalAlertService;
        this.chooseFields = false;
        this.chooseSteps = true;
        this.jobSalary = false;
        this.StructureValues = [];
        this.customValues = [];
        this.eligibilityArray = [];
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.selectedUsers = [];
        this.groupNames = [];
        this.blackOuts = [
            {
                location_id: '',
                blackoutName: '',
                blackoutEmployeeGroup: '',
                startDate: '',
                endDate: '',
                blockType: '',
                isChecked: false,
                edit: false
            }
        ];
        this.allGroups = [
            { name: 'Business Unit1' },
            { name: 'Business Unit2' },
        ];
        this.customLocation = [];
        this.defaultLocation = [];
        this.allLocation = [];
        this.selectedBlackouts = [];
        this.showField1 = false;
        this.previewList = [];
        this.checkBlackoutIds = [];
        this.disableCancel = true;
        this.structureFields = [
            {
                name: 'Business Unit',
                isChecked: true
            }
        ];
        this.blackoutGroup = {
            name: '',
            effectiveDate: '',
            hireDate: '',
            customDate: ''
        };
        this.format = {
            add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
            direction: angular_dual_listbox__WEBPACK_IMPORTED_MODULE_8__["DualListComponent"].LTR, draggable: true, locale: 'da',
        };
    }
    BlackOutDatesComponent.prototype.openModalWithClass = function (template) {
        this.previewEmployeeList();
        this.selectedNav = 'chooseSteps';
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    };
    BlackOutDatesComponent.prototype.confirm = function () {
        this.message = 'Confirmed!';
        this.modalRef.hide();
    };
    BlackOutDatesComponent.prototype.decline = function () {
        this.message = 'Declined!';
        this.modalRef.hide();
    };
    BlackOutDatesComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.getFieldsForRoles();
        this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
        this.getStandardAndCustomStructureFields();
        this.getEmployeeGroupList();
        this.getBlackoutList();
        // this.dropdownSettings = {
        //   singleSelection: false,
        //   idField: 'item_id',
        //   textField: 'item_text',
        //   selectAllText: 'Select All',
        //   unSelectAllText: 'UnSelect All',
        //   itemsShowLimit: 0,
        //   maxHeight: 150,
        //   defaultOpen: true,
        //   allowSearchFilter: true
        // };
    };
    // Author: Saiprakash G, Date: 14/06/19
    // Get standard and custom structure fields
    BlackOutDatesComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customLocation = _this.getAllStructureFields['Location'].custom;
            _this.defaultLocation = _this.getAllStructureFields['Location'].default;
            _this.allLocation = _this.customLocation.concat(_this.allLocation);
        });
    };
    BlackOutDatesComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.previewEmployeeList();
    };
    BlackOutDatesComponent.prototype.previewEmployeeList = function () {
        var _this = this;
        var finalPreviewUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalPreviewUsers.push(element._id);
        });
        var data = {
            per_page: this.perPage,
            page_no: this.pageNo,
            _ids: finalPreviewUsers
        };
        this.companySettingsService.prviewEmployeeList(data).subscribe(function (res) {
            console.log(res);
            _this.previewList = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 15/06/19,
    // Slect and deselect employees for include and exclude
    // onItemSelect(item: any) {
    //   this.selectedUsers = []
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   for (var i = 0; i < this.selectedItems.length; i++) {
    //     this.selectedUsers.push(this.selectedItems[i].item_id);
    //   }
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // OnItemDeSelect(item: any) {
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // onSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   for (var i = 0; i < items.length; i++) {
    //     this.selectedUsers.push(items[i].item_id);
    //   }
    //   console.log("users", this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // onDeSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    BlackOutDatesComponent.prototype.getFieldsForRoles = function () {
        var _this = this;
        this.companySettingsService.getFieldsForRoles()
            .subscribe(function (res) {
            console.log("12122123", res);
            _this.structureFields = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    BlackOutDatesComponent.prototype.getStructureSubFields = function (fields, isChecked) {
        var _this = this;
        console.log(fields, isChecked);
        this.fieldsName = fields;
        this.checked = isChecked;
        if (isChecked == true) {
            this.eligibilityArray = [];
            this.structureFields.forEach(function (element) {
                if (element.name !== fields) {
                    element.isChecked = false;
                }
            });
            this.companySettingsService.getStuctureFields(fields, 0)
                .subscribe(function (res) {
                console.log(res, name);
                _this.StructureValues = res.default;
                _this.customValues = res.custom;
                console.log(_this.StructureValues);
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Sub structure fields
    BlackOutDatesComponent.prototype.subFieldsStructure = function ($event, value) {
        var _this = this;
        console.log($event, value);
        // this.eligibilityArray = [];
        if ($event.checked == true) {
            this.eligibilityArray.push(value);
        }
        console.log(this.eligibilityArray, "123321");
        if ($event.checked == false) {
            var index = this.eligibilityArray.indexOf(value);
            if (index > -1) {
                this.eligibilityArray.splice(index, 1);
            }
            console.log(this.eligibilityArray);
        }
        var postdata = {
            field: this.fieldsName,
            chooseEmployeesForRoles: this.eligibilityArray
        };
        console.log(postdata);
        this.siteAccessRolesService.chooseEmployeesData(postdata)
            .subscribe(function (res) {
            console.log("emp data", res);
            _this.dropdownList = [];
            for (var i = 0; i < res.data.length; i++) {
                if ($event.checked == true)
                    _this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName });
            }
            console.log(_this.dropdownList, "employee dropdown");
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 17/06/19
    // Get employee group list
    BlackOutDatesComponent.prototype.getEmployeeGroupList = function () {
        var _this = this;
        this.leaveManagementService.getEmployeeGroupList(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.groupNames = res.result;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Create blackout employee group
    BlackOutDatesComponent.prototype.createBlackoutEmployeeGroup = function () {
        var _this = this;
        var finalSelectedUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalSelectedUsers.push(element._id);
        });
        var data = {
            companyId: this.companyId,
            name: this.blackoutGroup.name,
            effectiveDate: this.blackoutGroup.effectiveDate,
            initial_group_effective_date_details: {
                date_selection: this.blackoutGroup.hireDate
            },
            employee_ids: finalSelectedUsers
        };
        this.leaveManagementService.createEmployeeGroup(data).subscribe(function (res) {
            console.log(res);
            _this.getEmployeeGroupList();
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.message = 'Confirmed!';
            _this.modalRef.hide();
            _this.blackoutGroup = "";
            _this.dropdownList = [];
            _this.selectedUsers = [];
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Push empty blackouts
    BlackOutDatesComponent.prototype.pushBlockouts = function () {
        this.blackoutDetails = {
            location_id: '',
            blackoutName: '',
            blackoutEmployeeGroup: '',
            startDate: '',
            endDate: '',
            blockType: ''
        };
        if (!this.blackOuts) {
            this.blackOuts = [];
        }
        this.blackOuts.push(this.blackoutDetails);
        console.log(this.blackOuts);
        this.disableCancel = false;
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Select blackout 
    BlackOutDatesComponent.prototype.selectBlackout = function (event, id, blackout) {
        console.log(event, id, blackout);
        this.updateDetails = blackout;
        this.blackoutId = id;
        if (event.checked == true) {
            this.checkBlackoutIds.push(id);
        }
        console.log(this.checkBlackoutIds);
        if (event.checked == false) {
            var index = this.checkBlackoutIds.indexOf(id);
            if (index > -1) {
                this.checkBlackoutIds.splice(index, 1);
            }
            console.log(this.checkBlackoutIds);
        }
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Create blackout date
    BlackOutDatesComponent.prototype.createBlackout = function () {
        var _this = this;
        var blackoutAdd = [];
        for (var i = 0; i < this.blackOuts.length; i++) {
            if (!this.blackOuts[i]._id) {
                blackoutAdd.push(this.blackOuts[i]);
            }
        }
        // var data: any = {
        //   country: "India",
        //   companyId: this.companyId,
        //   location_id: this.blackoutDetails.location_id,
        //   blackoutName: this.blackoutDetails.blackoutName,
        //   employeeGrpName: this.blackoutDetails.employeeGrpName,
        //   startDate:this.blackoutDetails.startDate,
        //   endDate: this.blackoutDetails.endDate,
        //   blockType: this.blackoutDetails.blockType
        // }
        if (blackoutAdd.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Please add blackout date", "", "error");
        }
        else {
            this.leaveManagementService.createBlackout(blackoutAdd).subscribe(function (res) {
                console.log(res);
                _this.getBlackoutList();
                _this.blackoutDetails = "";
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Update blackout day
    BlackOutDatesComponent.prototype.updateBlackout = function () {
        var _this = this;
        var data1 = {
            _id: this.blackoutId,
            companyId: this.companyId,
            location_id: this.updateDetails.location_id,
            blackoutName: this.updateDetails.blackoutName,
            blackoutEmployeeGroup: this.updateDetails.blackoutEmployeeGroup,
            startDate: this.updateDetails.startDate,
            endDate: this.updateDetails.endDate,
            blockType: this.updateDetails.blockType
        };
        if (this.blackoutId) {
            this.leaveManagementService.updateBlackout(data1).subscribe(function (res) {
                console.log(res);
                _this.getBlackoutList();
                _this.checkBlackoutIds = [];
                _this.blackoutId = "";
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Get blackout list
    BlackOutDatesComponent.prototype.getBlackoutList = function () {
        var _this = this;
        var data = {
            country: "India"
        };
        this.leaveManagementService.getBlackoutList(data).subscribe(function (res) {
            console.log(res);
            _this.blackOuts = res.result;
            _this.blackOuts.filter(function (item) {
                item.edit = true;
            });
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 15/06/19
    // Edit blackout
    BlackOutDatesComponent.prototype.editBlackout = function () {
        this.selectedBlackouts = this.blackOuts.filter(function (checkVisa) {
            return checkVisa.isChecked;
        });
        if (this.selectedBlackouts.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one blackout to proceed", "", 'error');
        }
        else if (this.selectedBlackouts.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one blackout to proceed", "", 'error');
        }
        else {
            if (this.selectedBlackouts[0]._id) {
                this.selectedBlackouts[0].edit = false;
            }
        }
    };
    // Author: Saiprakash G, Date: 15/06/19
    //  Delete blackout
    BlackOutDatesComponent.prototype.deleteBlackout = function () {
        var _this = this;
        this.selectedBlackouts = this.blackOuts.filter(function (checkVisa) {
            return checkVisa.isChecked;
        });
        if (this.selectedBlackouts.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one blackout to proceed", "", 'error');
        }
        else {
            var data = {
                _id: this.selectedBlackouts
            };
            this.leaveManagementService.deleteBlackout(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getBlackoutList();
                _this.checkBlackoutIds = [];
                _this.blackoutId = "";
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    BlackOutDatesComponent.prototype.selectEffectiveDate = function (event) {
        if (event === "date_of_hire") {
            this.showField1 = false;
        }
        else if (event === "custom_date") {
            this.showField1 = true;
        }
    };
    BlackOutDatesComponent.prototype.cancelBlackout = function () {
        this.getBlackoutList();
        this.disableCancel = true;
    };
    BlackOutDatesComponent.prototype.onSelectUsers = function (selectSpecificPerson) {
        console.log("users", selectSpecificPerson);
        console.log('selcetd users', this.selectedUsers);
        this.previewEmployeeList();
    };
    BlackOutDatesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-black-out-dates',
            template: __webpack_require__(/*! ./black-out-dates.component.html */ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.html"),
            styles: [__webpack_require__(/*! ./black-out-dates.component.css */ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_4__["LeaveManagementService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__["SiteAccessRolesService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_6__["MyInfoService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_7__["SwalAlertService"]])
    ], BlackOutDatesComponent);
    return BlackOutDatesComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.module.ts ***!
  \********************************************************************************************/
/*! exports provided: BlackOutDatesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackOutDatesModule", function() { return BlackOutDatesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _black_out_dates_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./black-out-dates.routing */ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.routing.ts");
/* harmony import */ var _black_out_dates_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./black-out-dates.component */ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var BlackOutDatesModule = /** @class */ (function () {
    function BlackOutDatesModule() {
    }
    BlackOutDatesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _black_out_dates_routing__WEBPACK_IMPORTED_MODULE_2__["BlackOutDatesRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_6__["MatSlideToggleModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_7__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_10__["NgMultiSelectDropDownModule"].forRoot(),
                angular_dual_listbox__WEBPACK_IMPORTED_MODULE_11__["AngularDualListBoxModule"]
            ],
            declarations: [_black_out_dates_component__WEBPACK_IMPORTED_MODULE_3__["BlackOutDatesComponent"]],
            providers: [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_8__["CompanySettingsService"]]
        })
    ], BlackOutDatesModule);
    return BlackOutDatesModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.routing.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.routing.ts ***!
  \*********************************************************************************************/
/*! exports provided: BlackOutDatesRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackOutDatesRouting", function() { return BlackOutDatesRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _black_out_dates_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./black-out-dates.component */ "./src/app/admin-dashboard/leave-management/black-out-dates/black-out-dates.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: _black_out_dates_component__WEBPACK_IMPORTED_MODULE_2__["BlackOutDatesComponent"] }
];
var BlackOutDatesRouting = /** @class */ (function () {
    function BlackOutDatesRouting() {
    }
    BlackOutDatesRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BlackOutDatesRouting);
    return BlackOutDatesRouting;
}());



/***/ })

}]);
//# sourceMappingURL=black-out-dates-black-out-dates-module.js.map