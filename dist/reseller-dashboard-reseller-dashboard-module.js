(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reseller-dashboard-reseller-dashboard-module"],{

/***/ "./src/app/reseller-dashboard/administrator/administrator.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/reseller-dashboard/administrator/administrator.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenworkers-wrapper{\n    background: #f8f8f8;\n    height: 100vh;\n}\n\n.zenwork-currentpage {\n    padding: 40px 0 0 0;\n}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.zenworkers-wrapper{\n    padding: 0px; float: none; margin: 0 auto;\n}\n\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 15px;\n    width:22px;\n    color:#9a9898;\n    font-size: 13px; cursor: pointer;\n}\n\n.zenwork-margin-ten-zero {\n    margin: 20px 0px!important;\n}\n\n.zenwork-margin-zero { font-size: 17px; line-height: 15px; vertical-align: middle; color:#008f3d;}\n\n.zenwork-margin-zero small{ display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n\n.zenwork-inner-icon { vertical-align: text-top;}\n\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n\n.zenwork-custom-select{\n    background: #fff!important;\n}\n\n.border-none{\n    border:0 none !important;\n}\n\n.box-shadow-none{\n    box-shadow: none !important;\n}\n\n.zenwork-table-customized{\n    background: #fff;\n}\n\n.zenwork-table-customized.table-condensed>tbody>tr>td{\n    padding: 20px; border-top: none;border-bottom: #efefef 1px solid; color: #807d7d;\n}\n\n.zenwork-table-customized.table-condensed>thead>tr>th{\n    padding: 15px 20px; background:#eef7ff;border-bottom:none; color: #3e3c3c; font-weight: normal;\n}\n\n.search-form .form-control {padding:12px 40px 12px 20px; height: auto;}\n\n.zenwork-customized-green-btn{\n    background: #797d7b!important;\n    color: #fff!important;\n    padding:3px 17px!important;\n    font-size:15px!important;\n}\n\n.zenwork-customized-green-btn small { display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n\n.zenwork-customized-green-btn small .fa { color:#fff; font-size: 15px;}\n\n.zenwork-customized-transparent-btn:focus, .zenwork-customized-transparent-btn:active{ outline: none;}"

/***/ }),

/***/ "./src/app/reseller-dashboard/administrator/administrator.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/reseller-dashboard/administrator/administrator.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper col-md-11\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <small>\n        <img src=\"../../../assets/images/company-settings/site-access-hover.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n      </small>\n      Administrator\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"zenwork-clients-wrapper\">\n    <div class=\"col-xs-4 padding-left-zero\">\n      <div class=\"search-border text-left\">\n        <form [formGroup]=\"zenworkersSearch\" class=\"search-form\">\n          <input type=\"text\" class=\"border-none box-shadow-none form-control\" formControlName=\"search\" placeholder=\"Search by name,title, etc\">\n        </form>\n        <span>\n          <span class=\"glyphicon glyphicon-search search-icon\" aria-hidden=\"true\"></span>\n        </span>\n      </div>\n    </div>\n    <div class=\"col-xs-6 col-xs-offset-2\">\n      <div class=\"col-xs-8\">\n        <div class=\"input-group\">\n          <span class=\"input-group-addon zenwork-client-group-addon zenwork-input-group-addon\">\n            <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group zenwork-company-group\">\n          </span>\n          <mat-select class=\"form-control zenwork-input zenwork-custom-select\" placeholder=\"All Clients\" [(ngModel)]=\"selectedZenworkRole\" (ngModelChange)=\"getZenworkersData();\">\n            <mat-option value=\"all\">\n              All\n            </mat-option>\n         \n          </mat-select>\n        </div>\n      </div>\n      <div class=\"col-xs-4\">\n \n        <button mat-button (click)=\"addZenworker('')\" class=\"btn zenwork-customized-green-btn\">\n            <small><i aria-hidden=\"true\" class=\"fa fa-plus\"></i></small> Add Administrator\n        </button>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n    <div class=\"clearfix\"></div>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"clients-tablle-wrapper\">\n    <table class=\"table table-responsive table-condensed zenwork-table-customized\">\n      <thead>\n        <tr class=\"info\">\n          \n          <th>\n            <div class=\"cont-check\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"allZenworkers\" (change)=\"selectAllZenworkers()\"></mat-checkbox>\n            </div>\n          </th>\n          <th>Name</th>\n          <th>\n            Email\n          </th>\n          <th>\n            Profile\n          </th>\n          <th>\n            Action\n            <!-- <button class=\"zenwork-customized-transparent-btn\">\n              <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n            </button> -->\n            <!-- <button class=\"zenwork-customized-transparent-btn\">\n              <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n            </button> -->\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let zenworker of zenworkerDetails\">\n          <td [ngClass]=\"{'zenwork-checked-border':zenworker.isChecked}\">\n            <div class=\"cont-check\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"zenworker.isChecked\"></mat-checkbox>\n          </div>\n          </td>\n          <td>{{zenworker.name}}</td>\n          <td>\n            {{zenworker.email}}\n          </td>\n          <td>\n              {{zenworker.roleId.name}}\n          </td>\n          <td>\n            <button class=\"zenwork-customized-transparent-btn\" (click)=\"addZenworker(zenworker)\">\n              <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n            </button>\n            <!-- <button class=\"zenwork-customized-transparent-btn\" (click)=\"deleteZenworker(zenworker)\">\n              <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n            </button> -->\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/reseller-dashboard/administrator/administrator.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/reseller-dashboard/administrator/administrator.component.ts ***!
  \*****************************************************************************/
/*! exports provided: AdministratorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministratorComponent", function() { return AdministratorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _create_administrator_template_create_administrator_template_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../create-administrator-template/create-administrator-template.component */ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { CreateZenworkerTemplateComponent } from '../../super-admin-dashboard/create-zenworker-template-component/create-zenworker-template-component.component';

var AdministratorComponent = /** @class */ (function () {
    function AdministratorComponent(dialog, zenworkersService, swalAlertService, loaderService, route) {
        this.dialog = dialog;
        this.zenworkersService = zenworkersService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.route = route;
        this.allZenworkers = false;
    }
    AdministratorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log("iddddd", _this.id, params);
        });
        this.loaderService.loader(true);
        this.zenworkersSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('')
        });
        this.selectedZenworkRole = "all";
        this.perPage = 10;
        this.pages = [10, 25, 50];
        this.pageNo = 1;
        // this.selectedZenworkerDetails = { name: "Venkatesh", email: "venkatesh.g@mtwlabs.com", profile: "---", isChecked: false }
        this.onChanges();
        this.getZenworkersData();
    };
    AdministratorComponent.prototype.onChanges = function () {
        var _this = this;
        this.zenworkersSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            // console.log(this.zenworkersSearch.get('search').value);
            _this.getZenworkersData();
        });
    };
    AdministratorComponent.prototype.getZenworkersData = function () {
        var _this = this;
        console.log("change");
        this.loaderService.loader(true);
        var zenworker;
        if (this.selectedZenworkRole == "all") {
            zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage };
        }
        else {
            this.selectedRole = this.selectedZenworkRole;
            zenworker = { searchQuery: this.zenworkersSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, role: this.selectedRole };
        }
        var postData = zenworker;
        postData['companyId'] = this.id;
        console.log("admindtrator create", postData);
        this.zenworkersService.getZenworkersDetails(zenworker)
            .subscribe(function (response) {
            console.log(response);
            _this.loaderService.loader(false);
            if (response.status) {
                // this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success')
                _this.zenworkerDetails = response.data;
                _this.zenworkerDetails.forEach(function (zenworker) {
                    zenworker.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Zenworkers", "Data Not Found", 'info');
            }
        }, function (err) {
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    AdministratorComponent.prototype.selectAllZenworkers = function () {
        var _this = this;
        // console.log("Selecting all", this.allZenworkers);
        this.zenworkerDetails.forEach(function (zenworker) {
            zenworker.isChecked = _this.allZenworkers;
        });
    };
    AdministratorComponent.prototype.changePage = function (p) {
        this.pageNo = p;
        this.getZenworkersData();
    };
    AdministratorComponent.prototype.addZenworker = function (data) {
        var _this = this;
        localStorage.setItem('resellerId', this.id);
        var dialogRef = this.dialog.open(_create_administrator_template_create_administrator_template_component__WEBPACK_IMPORTED_MODULE_6__["CreateAdministratorTemplateComponent"], {
            height: '700px',
            width: '890px',
            data: data
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Result", result);
            _this.getZenworkersData();
        });
    };
    AdministratorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-administrator',
            template: __webpack_require__(/*! ./administrator.component.html */ "./src/app/reseller-dashboard/administrator/administrator.component.html"),
            styles: [__webpack_require__(/*! ./administrator.component.css */ "./src/app/reseller-dashboard/administrator/administrator.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__["ZenworkersService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]])
    ], AdministratorComponent);
    return AdministratorComponent;
}());



/***/ }),

/***/ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.css":
/*!**************************************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-create-client-wrapper .modal-header {\n    padding: 0 0 24px 0;\n    border-bottom: 1px solid #e5e5e5;\n}\n\n.zenwork-custom-proceed-btn{\n    background: green !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 25px; margin:0 0 0 30px;\n}\n\n.create-client-form-wrapper { padding: 30px 0 45px;}\n\n.create-client-form-wrapper label { font-weight: normal; color:#524f4f; padding: 0 0 10px; font-size: 16px;}\n\n.create-client-form-wrapper .form-control { padding:10px 15px; height: auto; border-radius: 3px; color:#524f4f;}\n\n.create-client-form-wrapper .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #5e5e5e;\n  }"

/***/ }),

/***/ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.html":
/*!***************************************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-create-client-wrapper\">\n  <div class=\"modal-header\">\n    <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button> -->\n    <h4 class=\"modal-title green\">New Administrator</h4>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"create-client-form-wrapper\">\n\n      <div class=\"col-xs-5\">\n        <div class=\"form-group\">\n          <label>\n            Name<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <input type=\"text\" class=\"form-control zenwork-input\" placeholder=\"Enter Name\"\n            [(ngModel)]=\"createZenworker.name\" maxlength=\"24\" name=\"name\" required #adminName=\"ngModel\">\n          <!-- <span *ngIf=\"createZenworker.name && adminName.invalid\" style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span> -->\n          <span *ngIf=\"!createZenworker.name && adminName.touched || (!createZenworker.name && isValid)\"\n            style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span>\n        </div>\n        <div class=\"form-group\">\n          <label>\n            Email<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <input type=\"email\" class=\"form-control zenwork-input\" placeholder=\"Enter Email\"\n            [(ngModel)]=\"createZenworker.email\" name=\"email\" required #adminEmail=\"ngModel\">\n          <span *ngIf=\"!createZenworker.email && adminEmail.touched || (!createZenworker.email && isValid)\"\n            style=\"color:#e44a49; padding:6px 0 0;\">Enter email</span>\n        </div>\n        <div class=\"form-group\">\n          <label>\n            Profile<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <mat-select class=\"form-control zenwork-input\" placeholder=\"Profile\" [(ngModel)]=\"createZenworker.roleId\"\n            name=\"profile\" required #adminProfile=\"ngModel\">\n            <mat-option value={{profile._id}} *ngFor=\"let profile of userProfile\">\n              {{profile.name}}\n            </mat-option>\n\n          </mat-select>\n          <span *ngIf=\"!createZenworker.roleId && adminProfile.touched || (!createZenworker.roleId && isValid)\"\n            style=\"color:#e44a49; padding:6px 0 0;\">select profile</span>\n        </div>\n        <div class=\"form-group\">\n          <label>\n            Location<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <mat-select class=\"form-control zenwork-input\" placeholder=\"Location\"\n            [(ngModel)]=\"createZenworker.address.state\" name=\"location\" required #adminLocation=\"ngModel\">\n            <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n              {{data.name}}\n            </mat-option>\n            <!-- <mat-option value=\"New york\">\n              New york\n            </mat-option>\n            <mat-option value=\"Texas\">\n              Texas\n            </mat-option> -->\n          </mat-select>\n          <span\n            *ngIf=\"!createZenworker.address.state && adminLocation.touched || (!createZenworker.address.state && isValid)\"\n            style=\"color:#e44a49; padding:6px 0 0;\">select location</span>\n\n        </div>\n        <div class=\"form-group\">\n          <label>\n            Status<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <mat-select class=\"form-control zenwork-input\" placeholder=\"Status\" [(ngModel)]=\"createZenworker.status\"\n            required #adminStatus=\"ngModel\">\n            <mat-option value=\"active\">\n              Active\n            </mat-option>\n            <mat-option value=\"inactive\">\n              InActive\n            </mat-option>\n          </mat-select>\n          <span *ngIf=\"!createZenworker.status && adminStatus.touched || (!createZenworker.status && isValid)\"\n            style=\"color:#e44a49; padding:6px 0 0;\">select status</span>\n\n        </div>\n      </div>\n      <div class=\"clearfix\"></div>\n\n    </div>\n  </div>\n  <div>\n    <button type=\"button\" class=\"btn btn-success zenwork-custom-proceed-btn\" (click)=\"addZenworker()\">Proceed</button>\n    <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\"\n      (click)=\"this.dialogRef.close();\">Cancel</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: CreateAdministratorTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateAdministratorTemplateComponent", function() { return CreateAdministratorTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var CreateAdministratorTemplateComponent = /** @class */ (function () {
    function CreateAdministratorTemplateComponent(dialogRef, data, zenworkersService, swalAlertService, loaderService, route, companySettingsService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.zenworkersService = zenworkersService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.route = route;
        this.companySettingsService = companySettingsService;
        this.isValid = false;
        this.createZenworker = {
            name: '',
            email: '',
            roleId: '',
            address: { state: '' },
            status: ''
        };
        this.allStates = [];
    }
    CreateAdministratorTemplateComponent.prototype.ngOnInit = function () {
        this.id = localStorage.getItem('resellerId');
        console.log(this.id);
        if (this.data) {
            this.createZenworker = this.data;
        }
        this.getAllRoles();
        this.getAllUSstates();
    };
    CreateAdministratorTemplateComponent.prototype.getAllUSstates = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('State', 0)
            .subscribe(function (res) {
            console.log(res);
            if (res.custom.length > 0) {
                res.custom.forEach(function (element) {
                    _this.allStates.push(element);
                    console.log("22222111111", _this.allStates);
                });
            }
            if (res.default.length > 0) {
                res.default.forEach(function (element) {
                    _this.allStates.push(element);
                });
                console.log("22222", _this.allStates);
            }
        }, function (err) {
            console.log(err);
        });
    };
    CreateAdministratorTemplateComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.zenworkersService.getAllroles()
            .subscribe(function (res) {
            console.log("111111111", res);
            if (res.status == true) {
                // this.loaderService.loader(false)
                _this.userProfile = res.data;
                _this.userProfile.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    CreateAdministratorTemplateComponent.prototype.addZenworker = function () {
        var _this = this;
        // this.loaderService.loader(true);
        /* if (!this.createZenworker.name) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Name", 'error')
        } else if (!this.createZenworker.email) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Enter Valid Email", 'error')
        } else if (!this.createZenworker.type) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Select Valid Zenworker Role", 'error')
        } else if (!this.createZenworker.address.state) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Add Address", 'error')
        } else if (!this.createZenworker.status) {
          this.swalAlertService.SweetAlertWithoutConfirmation("Zenworker", "Please Select Status", 'error')
        } else { */
        this.isValid = true;
        if (this.createZenworker.name && this.createZenworker.email && this.createZenworker.address.state &&
            this.createZenworker.roleId && this.createZenworker.status) {
            if (this.createZenworker.hasOwnProperty('_id')) {
                this.zenworkersService.editZenworker(this.createZenworker)
                    .subscribe(function (res) {
                    console.log("res", res);
                    _this.dialogRef.close();
                }, function (err) {
                    console.log("err", err);
                });
            }
            else {
                var postData = this.createZenworker;
                postData['companyId'] = this.id;
                console.log("admindtrator create", postData);
                this.zenworkersService.addZenworker(postData)
                    .subscribe(function (res) {
                    console.log("res", res);
                    _this.dialogRef.close();
                    _this.loaderService.loader(false);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                }, function (err) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
                });
            }
        }
    };
    CreateAdministratorTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-administrator-template',
            template: __webpack_require__(/*! ./create-administrator-template.component.html */ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.html"),
            styles: [__webpack_require__(/*! ./create-administrator-template.component.css */ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_2__["ZenworkersService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_6__["CompanySettingsService"]])
    ], CreateAdministratorTemplateComponent);
    return CreateAdministratorTemplateComponent;
}());



/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".company-details-wrapper , .contact-wrapper{\n    padding:30px 0px;\n}\n.company-details-wrapper{\n    height: 100vh;\n}\n.company-details{\n    border-bottom: 1px dotted;\n\n}\n.company-details h2 { color:#008f3d; font-size: 18px; line-height: 18px; display: inline-block; margin: 0 0 20px;}\n.company-details label { color:#5f5c5c; font-size: 15px; padding: 0 0 10px; font-weight: normal;}\n.zenwork-custom-input{\n    background: #fff !important; padding: 13px 15px; height: auto;\n}\n.zenwork-custom-select{\n    background: #fff!important;\n}\n.buttons{\n    margin: 20px 0 0 0;\n}\n.buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    cursor: pointer;\n}\n.disabled{\n    background: grey !important; \n}\n.buttons ul li .save{\n\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n    /* cursor: pointer; */\n}\n.buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n}\n/* package&billing css starts */\n.package-billing-main{\n    margin: 40px 0 0 0;\n    border-bottom: 1px dotted;\n}\n.package-billing-main .billing-block h2 { padding: 20px 20px 5px;}\n.discount-block{\n    padding: 0;\n}\n.billing-package-block{\n    float: none;\n}\n.border-bottom-green{\n    border-bottom: 2px solid #439348;\n    padding: 0 0px 5px 0;\n}\n.discount-heading .list-items{\n    display: inline-block;\n    padding: 0px 20px 5px 0;\n    font-size: 16px; float: left;\n}\n.discount-heading .list-items span{\n    display: block;\n}\n.package-table { margin: 0 auto; float: none; padding: 0 30px 0 0px;}\n.package-table h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.package-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;text-align: center; width: 34%;}\n.package-table .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.package-table .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.package-table .table>tbody>tr>td, .package-table .table>tfoot>tr>td, .package-table .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-bottom: #e0dada 1px solid;font-size: 15px;text-align: center;}\n.package-table .table>tbody>tr>td .mat-select-placeholder{\n    font-size: 15px;\n    text-align: left;\n}\n.package-table .table>tbody>tr>td .package-table-select{\n    background: #f8f8f8;\n}\n.add-on-cancel{\n    float: left;\n    padding:5px 3px 0 0;\n    cursor: pointer;\n}\n.package-table .table>tbody>tr>td .add-on-select{\n    float: right;\n    width: 87% !important;\n}\n.package-table .table>tbody>tr .add-on-type{\n    width:40%;\n}\n.package-table .table>tbody>tr .package-name{\n    width: 21%;\n}\n.payment { margin: 50px 0 0;}\n.payment-lft { padding:30px 15px 0 0;}\n.payment-lft h2 {color:#008f3d; font-size: 17px; line-height: 17px;margin: 0; float: left; padding: 5px 0 0; font-weight: 600;}\n.payment-lft > a {color:#008f3d; font-size: 16px; line-height: 17px;margin: 0; cursor: pointer; float: right; border-radius: 30px; padding: 10px 20px; border:#008f3d 1px solid;}\n.payment-method { margin: 30px 0 0;}\n.payment-method .panel-default>.panel-heading {background-color:#fff !important; border: none; position: relative; padding:0; box-shadow: 0 0 10px #ccc; display: block;}\n.payment-method .panel-title { color:#808080; display:block; font-size: 15px; font-weight: 600;}\n.payment-method .panel-title span { display: inline-block; padding:0 10px 0 0; margin: 0 10px 0 0; border-right:#ccc 1px solid;}\n.payment-method .panel-title a:hover { text-decoration: none;}\n.payment-method .panel-title>.small, .payment-method .panel-title>.small>a, .payment-method .panel-title>a, .payment-method .panel-title>small, .payment-method .panel-title>small>a { text-decoration:none; display: block; padding: 15px;}\n.payment-method .panel { background: none; box-shadow: none; border-radius: 0;}\n.payment-method .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color:#798993;\n}\n.payment-method .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.payment-method .panel-group .panel-heading+.panel-collapse>.list-group, .payment-method .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.payment-method .panel-heading .accordion-toggle:after { margin:4px 0 0; font-size: 12px; line-height: 10px;}\n.payment-method .panel-body { padding: 0 0 5px;}\n.payment-method .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.payment-method .panel-default { border-color:transparent;}\n.payment-method .panel-group .panel+.panel { margin-top: 20px;}\n.ach-payment { margin: 20px 0 0 0;}\n.credit-payment { margin: 20px 0 0;}\n.paypal-payment { margin: 20px 0 0;}\n.payment-rgt { padding:50px 0 0 20px;}\n.payment-rgt h3 { color:#008f3d; font-size: 16px; line-height: 17px; display: inline-block; margin: 0 0 20px;}\n.billing-block { background:#fff;}\n.billing-block h2 { background:#f0be19; padding: 20px; border-radius:5px 5px 0 0;margin:0;}\n.billing-block h2 span { float: left; color:#3e3e3e; font-size: 16px; line-height: 16px;}\n.billing-block h2 small { float:right; color:#3e3e3e; font-size: 16px; line-height: 16px;}\n.annualy-block { float: right;}\n.annualy-block p { float:left; color:#3e3e3e; font-size: 16px; line-height: 16px; border-bottom:#3e3e3e 1px solid; display:inline-block; margin: 0 30px 0 0;}\n.billing-block > ul { padding:10px 0; margin: 0;}\n.billing-block > ul > li { border-bottom:#b8c0c6 1px dashed; padding:13px 0 10px;}\n.billing-price { padding: 0 20px;}\n.billing-price > ul {  padding:0;}\n.billing-price > ul > li { margin: 0 0 7px; border-bottom:none;}\n.billing-price ul li h5 { color:#008f3d; font-size: 15px; line-height: 15px; margin: 0 0 3px; border-bottom:#008f3d 1px solid; display: inline-block;}\n.billing-price ul li span { float: left; color:#777; font-size: 15px; line-height: 15px;}\n.billing-price ul li small { float:right; color:#008f3d; font-size: 15px; line-height: 15px;}\n.billing-price ul li a { float: left; cursor: pointer; display: inline-block; background:#b8c0c6; font-size: 13px; line-height: 13px; padding:4px 10px; color:#fff;\nborder-radius: 3px; margin: -3px 0 0 15px;}\n.billing-price ul li var { float: left;; color:#008f3d; font-size: 16px; line-height: 15px; margin: 0 0 0 10px; font-style: normal;}\n.billing-btm { margin:0 20px;}\n.billing-btm .form-control { float: left; border:#d2cbcb 1px solid; width: 30%; padding: 15px 10px; border-radius: 5px; box-shadow: none;text-transform: uppercase;}\n.billing-btm a.apply { float: left; color:#92b8ce; margin:6px 0 0 10px; font-size: 14px; cursor: pointer;}\n.billing-btm em { float: right; color:#777; font-size: 15px; line-height: 15px; font-style: normal; padding: 10px 0 0;}\n.billing-block h4 { background:#eef7ff; padding: 20px;margin:0;}\n.billing-block h4 h1 { float: left; color:#008f3d; font-size: 18px; line-height: 16px; font-weight: bold; margin: 0;}\n.billing-block h4 p { float:right; color:#008f3d; font-size: 18px; line-height: 16px;font-weight: bold; margin: 0;}\n.payment-rgt a.btn {border-radius: 20px;color:#616060; font-size: 15px; padding:6px 25px; background:transparent; margin:40px 0 0; float:left; position: static;}\n.payment-rgt a.btn:hover {background: #008f3d; color:#fff;}\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n.heade-check .checkbox label::after { color:#008f3d;width: 20px; height: 20px; padding:0 0 0 4px; font-size: 12px;}\n.heade-check .checkbox{ margin: 0;}\n.heade-check .checkbox label { padding-left: 0; color:#948e8e; font-size: 13px; line-height: 20px; padding: 0 0 0 10px;}\n.payment-rgt .cont-check { margin:20px 20px;}\n.payment-rgt .cont-check .checkbox label::after { padding:2px 0 0 4px;border-radius: 3px;}\n.payment-rgt .cont-check .checkbox label { padding-left:15px;color:#6d6d6d; font-size: 15px;line-height: 17px;}\n.payment-rgt .cont-check .checkbox label small { color:#008f3d;font-size: 15px; }\n.date { height: auto !important; line-height:inherit !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width: 70% !important;}\n.date .form-control:focus { box-shadow: none; border: none;}\n/* package&billing css ends */\n/* contact-detail css starts */\n.address-wrapper { padding: 0 0 20px; border-bottom: 1px dotted;}\n.address-wrapper ul { display: inline-block; width: 100%;}\n.address-wrapper ul li { margin: 0 0px 20px 0; padding: 0;}\n.address-wrapper ul li.details-width{ width: 100%;}\n.address-wrapper ul li .form-control { font-size: 15px; line-height:15px;border: none; box-shadow: none; height: auto;padding:13px 0 13px 10px;}\n.address-wrapper ul li select { background:#fff; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n.address-wrapper ul li.united-state { margin: 0; clear: left;}\n.address-wrapper ul li.state, .address-wrapper ul li.city { padding: 0 10px 0 0;}\n.contact-wrapper { display: block; padding: 0 0 20px;}\n.contact-wrapper ul { margin: 0 0 30px;}\n.contact-wrapper ul li {margin: 0 0 15px 0; padding: 0;}\n.contact-wrapper ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n.contact-wrapper ul li .phone { display: block; background:#fff; padding: 0 10px;clear: left;}\n.contact-wrapper ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n.contact-wrapper ul li .phone span .fa { color: #636060;\nfont-size: 23px;\nline-height: 20px;\nwidth: 18px;\ntext-align: center;}\n.contact-wrapper ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 88%; border: none; background:transparent; box-shadow: none; padding:13px 0 13px 10px;}\n.contact-wrapper ul li .form-control { display: inline-block;vertical-align: middle; width:100%; border: none; background:#fff; box-shadow: none;padding:13px 10px;\nheight: auto;}\n.contact-wrapper ul li.phone-width { float: none; clear:left;}\n.contact-wrapper ul li.phone-width .phone .form-control { width: 100%; padding: 13px 10px 13px 10px;}\n.zenwork-custom-input{\n    background: #fff !important; padding: 13px 15px; height: auto;\n}\n.company-details-wrapper , .contact-wrapper{\n    padding:30px 0px;\n    border-bottom: 1px dotted;\n}\n.billing-company-contact{\n    padding: 60px 0 0 60px;\n}\n.address-wrapper{\n    padding: 40px 0px;\n}\n.zenwork-custom-proceed-btn{\n    background:#008f3d !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 30px;\n    margin: 0 0 0 40px;\n}\n.zenwork-customized-transparent-btn { margin: 0 0 0 20px;}\n.zenwork-margin-zero small { display:inline-block; vertical-align: middle; font-size: 17px; margin: 0 10px 0 0;}\n.zenwork-margin-zero{ font-size: 18px; color:#585858;}\n.company-details h2 { color:#008f3d; font-size: 18px; line-height: 18px; display: inline-block; margin: 0 0 20px;}\n.company-details label { color:#5f5c5c; font-size: 15px; padding: 0 0 10px; font-weight: normal;}\n.right-text { margin: 0 0 0 20px !important;}\n.contact-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n.contact-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n.address-wrapper h2 { color:#636060; font-size: 17px; line-height: 17px; display: inline-block; margin: 0 0 20px;}\n.address-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n.address-wrapper .form-group { margin: 15px 0 0;}\n.address-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n/* contact-details css ends */\n.payment-method .panel-group .credit-card-payment {\n    background: #fff;\n    box-shadow: -2px 0px 21px -1px rgba(0,0,0,0.75);\n    margin: 10px 0 0 0;\n}\n.card-buttons {\n    display: block;\n    padding: 10px 20px;\n}\n.wrapper{\n    padding: 10px 25px;\n}\n.card-buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.card-buttons ul li .my-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #777d7a;\n    padding: 8px 30px; \n}\n.card-buttons ul li .my-cards-btn:hover,.card-buttons ul li .my-cards-btn:active,.card-buttons ul li .my-cards-btn:visited{\n    border-radius: 20px;\n    background: #777d7a;\n    color: #fff;\n    padding: 8px 30px;\n\n}\n.card-buttons ul li .secure-cards-btn:hover,.card-buttons ul li .secure-cards-btn:active,.card-buttons ul li .secure-cards-btn:visited{\n    border-radius: 20px;\n    background: #439348;\n    color: #fff;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n.card-buttons ul li .secure-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #439348;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n.credit-card-details-date ul li{\n    display: inline-block;\n    padding: 0px 10px 0 0;\n    width: 29.5%;\n}\n.agree-policy{\n    padding: 25px 40px;\n}\n.coupon-code{\n    padding: 7px;\n    border: 1px solid gray;\n    border-radius: 6px;\n}\n.coupons-block span .coupon-code a{\n    margin: 5px 0 0 20px !important;\n}\n.coupons-block{\n    margin: 20px 0 30px 0 !important;\n}\n.coupons-code-check{\n    margin: 0 0 50px !important;\n}\n.payment{\n    border-bottom: 1px dotted;\n}\n.menu-dropdown{\n    background: #fff;\n    border: none;\n}\n.menu-dropdown-list{\n    right: 0px !important;\n    left: auto;\n}\n.menu-dropdown-list>li>a:focus, .menu-dropdown-list>li>a:hover{\n    background-color: #008f3d; \n    color: #fff;\n}\n.menu-dropdown-list{\n    right: 0px !important;\n    left: auto;\n}\n.menu-dropdown-list>li>a:focus, .menu-dropdown-list>li>a:hover{\n    background-color: #008f3d; \n    color: #fff;\n}"

/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"company-details-wrapper\">\n    <div class=\"stepper\">\n  \n      <mat-horizontal-stepper [linear]=\"true\" #stepper>\n        <ng-template matStepperIcon=\"edit\">\n          <mat-icon>check</mat-icon>\n        </ng-template>\n  \n        <mat-step label=\"Client Details\">\n\n          <!-- <form #loginForm=\"ngForm\">\n            <div class=\"company-details\">\n              <div class=\"col-xs-4\">\n  \n                <div class=\"form-group\">\n                  <label>Company Legal Name*</label>\n                  <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Company Name\"\n                    [(ngModel)]=\"companyDetails.name\" name=\"name\" required>\n                </div>\n                <div class=\"form-group\">\n                  <label>Trade Name*</label>\n                  <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Trade Name\"\n                    [(ngModel)]=\"companyDetails.tradeName\" name=\"tradeName\" required>\n                </div>\n                <div class=\"form-group\">\n                  <label>Email*</label>\n                  <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Email\"\n                    [(ngModel)]=\"companyDetails.email\" name=\"email\" required>\n                </div>\n                <div class=\"form-group\">\n                  <label>Estimated Number of Employees*</label>\n                  <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"No. of Employees\"\n                    [(ngModel)]=\"companyDetails.employeCount\" name=\"employeCount\" required>\n                </div>\n                <div class=\"form-group\">\n                  <label>Referral By</label>\n                  <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Referral Company Name\"\n                    [(ngModel)]=\"companyDetails.referralcompany\" name=\"referral\">\n                </div>\n                <div class=\"form-group\">\n                  <div class=\"col-xs-6 padding-left-zero\">\n                    <label>Add client as a Reseller?</label>\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Client / Reseller\"\n                      [(ngModel)]=\"companyDetails.type\" name=\"type\" required>\n                      <mat-option value=\"reseller\">\n                        Yes\n                      </mat-option>\n                      <mat-option value=\"company\">\n                        No\n                      </mat-option>\n                    </mat-select>\n                  </div>\n                  <div class=\"col-xs-6 padding-right-zero\" *ngIf=\"companyDetails.type == 'company'\">\n                    <div class=\"form-group\">\n                      <label>Client Belongs to Reseller?</label>\n                      <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                        placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.belongsToReseller\" [ngModelOptions]=\"{standalone:true}\">\n                        <mat-option value=\"true\">\n                          Yes\n                        </mat-option>\n                        <mat-option value=\"false\">\n                          No\n                        </mat-option>\n                      </mat-select>\n                    </div>\n                    <div class=\"form-group\" *ngIf=\"companyDetails.belongsToReseller == 'true'\">\n                      <label>Select Reseller</label>\n                      <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                        placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.resellerId\" (ngModelChange)=\"selectedReseller($event)\"\n                        [ngModelOptions]=\"{standalone:true}\">\n                        <mat-option value=\"{{reseller._id}}\" *ngFor=\"let reseller of resellersList\">\n                          {{reseller.name}}\n                        </mat-option>\n  \n                      </mat-select>\n                    </div>\n                  </div>\n                  <div class=\"clearfix\"></div>\n                </div>\n  \n              </div>\n              <div class=\"clearfix\"></div>\n  \n            </div>\n            <div class=\"buttons\">\n              <ul class=\"list-unstyled\">\n                <li class=\"list-items\">\n                  <a class=\"back\" [routerLink]=\"['/super-admin/super-admin-dashboard/clients']\">Back</a>\n                </li>\n                <li class=\"list-items\">\n                  <a class=\"cancel\" [routerLink]=\"['/super-admin/super-admin-dashboard/clients']\">Cancel</a>\n                </li>\n                <li class=\"list-items pull-right\">\n                  <button class='save' (click)=\"createCompany(stepper)\" [class.disabled]=\"loginForm.form.invalid\"\n                    [disabled]=\"loginForm.form.invalid\" mdStepperNext>\n                    save&continue\n                  </button>\n                </li>\n  \n                <div class=\"clearfix\"></div>\n              </ul>\n            </div>\n          </form> -->\n\n          <form #loginForm=\"ngForm\" (ngSubmit)=\"f.form.valid && createCompany(stepper)\" #f=\"ngForm\">\n              <div class=\"company-details\">\n                <div class=\"col-xs-4\">\n                  \n                  <div class=\"form-group\">\n                    <label>Company Legal Name*</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Company Name\"\n                      [(ngModel)]=\"companyDetails.name\" name=\"name\" required #companyName=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && companyName.invalid }\">\n                      <span *ngIf=\"(f.submitted && companyName.invalid) || (companyName.touched && companyName.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter company name</span>\n                  </div>\n    \n                  \n                  <div class=\"form-group\">\n                    <label>Trade Name*</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Trade Name\"\n                      [(ngModel)]=\"companyDetails.tradeName\" name=\"tradeName\" required #companyTrade=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && companyTrade.invalid }\">\n                      <span *ngIf=\"(f.submitted && companyTrade.invalid) || (companyTrade.touched && companyTrade.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter trade name</span>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Email*</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Email\"\n                      [(ngModel)]=\"companyDetails.email\" name=\"email\" required #emailer=\"ngModel\" type=\"email\" pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\"\n                      [ngClass]=\"{ 'is-invalid': f.submitted && emailer.invalid }\">\n                      <span *ngIf=\"(f.submitted && emailer.invalid) || (emailer.touched && emailer.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter email</span>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Estimated Number of Employees*</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"No. of Employees\"\n                      [(ngModel)]=\"companyDetails.employeCount\" name=\"employeCount\" required #count=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && count.invalid }\">\n                      <span *ngIf=\"(f.submitted && count.invalid) || (count.touched && count.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter employee</span>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Referral By</label>\n                    <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Referral Company Name\"\n                      [(ngModel)]=\"companyDetails.referralcompany\" name=\"referral\">\n                      <!-- <span *ngIf=\"(f.submitted && referal.invalid) || (referal.touched && referal.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter referal name</span> -->\n                  </div>\n                  <div class=\"form-group\">\n                    <div class=\"col-xs-6 padding-left-zero\">\n                      <label>Add client as a Reseller?</label>\n                      <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Client / Reseller\"\n                        [(ngModel)]=\"companyDetails.type\" name=\"type\" required #reseller=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && reseller.invalid }\">\n                        <mat-option value=\"reseller\">\n                          Yes\n                        </mat-option>\n                        <mat-option value=\"company\">\n                          No\n                        </mat-option>\n                      </mat-select>\n                      <span *ngIf=\"(f.submitted && reseller.invalid) || (reseller.touched && reseller.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter reseller</span>\n                    </div>\n    \n                    <div class=\"col-xs-6 padding-right-zero\" *ngIf=\"companyDetails.type == 'company'\">\n                      <div class=\"form-group\">\n                        <label>Client Belongs to Reseller?</label>\n                        <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                          placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.belongsToReseller\" [ngModelOptions]=\"{standalone:true}\" \n                          required #belongReseller=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && belongReseller.invalid }\">\n                          <mat-option value=\"true\">\n                            Yes\n                          </mat-option>\n                          <mat-option value=\"false\">\n                            No\n                          </mat-option>\n                        </mat-select>\n                        <span *ngIf=\"(f.submitted && belongReseller.invalid) || (belongReseller.touched && belongReseller.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Enter client reseller</span>\n                      </div>\n    \n                      <div class=\"form-group\" *ngIf=\"companyDetails.belongsToReseller == 'true'\">\n                        <label>Select Reseller</label>\n                        <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                          placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.resellerId\" (ngModelChange)=\"selectedReseller($event)\"\n                          [ngModelOptions]=\"{standalone:true}\" required #resellerId=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && resellerId.invalid }\">\n                          <mat-option value=\"{{reseller._id}}\" *ngFor=\"let reseller of resellersList\">\n                            {{reseller.name}}\n                          </mat-option>\n    \n                        </mat-select>\n                      <span *ngIf=\"(f.submitted && resellerId.invalid) || (resellerId.touched && resellerId.invalid)\" style=\"color:#e44a49; padding:6px 0 0;\">Select reseller</span>\n                      </div>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                  </div>\n    \n                </div>\n                <div class=\"clearfix\"></div>\n    \n              </div>\n              <div class=\"buttons\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items\">\n                    <a class=\"back\" [routerLink]=\"['/super-admin/super-admin-dashboard/clients']\">Back</a>\n                  </li>\n                  <li class=\"list-items\">\n                    <a class=\"cancel\" [routerLink]=\"['/super-admin/super-admin-dashboard/clients']\">Cancel</a>\n                  </li>\n                  <li class=\"list-items pull-right\">\n                    <button class='save' mdStepperNext type=\"submit\">\n                      save&continue\n                    </button>\n                  </li>\n    \n                  <div class=\"clearfix\"></div>\n                </ul>\n              </div>\n            </form>\n\n        </mat-step>\n        <mat-step label=\"Packages & Billing\">\n  \n          <div class=\"package-billing-main\">\n  \n            <div class=\"col-md-7 discount-block\">\n              <ul class=\"list-unstyled discount-heading\">\n                <li class=\"list-items\">\n                  <span [ngClass]=\"{'border-bottom-green':!selectedPackage.wholeSaleDiscount}\">Whole sale discount\n                    <br>\n                    <small>(Reseller is billed)</small>\n                  </span>\n  \n                </li>\n                <li class=\"list-items\">\n                  <mat-slide-toggle [(ngModel)]=\"selectedPackage.wholeSaleDiscount\" (change)=\"wholesaleDiscountChange($event)\"></mat-slide-toggle>\n                </li>\n                <li class=\"list-items\">\n                  <span [ngClass]=\"{'border-bottom-green':selectedPackage.wholeSaleDiscount}\">Direct discount\n                    <br>\n                    <small>(Client is billed)</small>\n                  </span>\n  \n                </li>\n                <li class=\"list-items\">\n                  <button class=\"btn\" (click)=\"addAnotherAddOn()\">\n                    <small>\n                      <i aria-hidden=\"true\" class=\"fa fa-plus\"></i>\n                    </small> Add-On\n                  </button>\n                </li>\n              </ul>\n              <div class=\"package-table\">\n                <table class=\"table\">\n                  <thead>\n                    <tr>\n                      <th>Package</th>\n                      <th>Add-On\n                        <span class=\"green\">+</span>\n                      </th>\n                      <th>\n                        Discount\n                      </th>\n                      <th>Start Date</th>\n                      <th>End Date</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr>\n                      <td class=\"package-name\">\n                        <mat-select class=\"form-control zenwork-input package-table-select\" [(ngModel)]=\"selectedPackage.name\"\n                          placeholder=\"Select Package\" (ngModelChange)=\"getSpecificPackageDetails()\">\n                          <mat-option [value]=\"package.name\" *ngFor=\"let package of packages\">\n                            {{package.name}}\n                          </mat-option>\n                        </mat-select>\n                      </td>\n                      <td>\n  \n                      </td>\n                      <td>\n                        <!-- <mat-select class=\"form-control zenwork-input package-table-select\" [disabled]=\"discountEnable\"\n                          [(ngModel)]=\"discountValue\" placeholder=\"Select Package\" (ngModelChange)=\"discountBasePrice()\">\n                          <mat-option [value]=\"discount.value\" *ngFor=\"let discount of dicountArray\">\n                            {{discount.viewValue}}\n                          </mat-option>\n                        </mat-select> -->\n                        <input type=\"text\" (keypress)=\"keyPress($event)\"\n                        class=\"form-control zenwork-input package-table-select\" [disabled]=\"discountEnable\"\n                        [(ngModel)]=\"discountValue\" placeholder=\"discount\" (ngModelChange)=\"discountBasePrice()\">\n                      </td>\n                      <td>\n                        <div class=\"date\">\n                          {{selectedPackage.startDate | date}}\n                          <div class=\"clearfix\"></div>\n                        </div>\n                      </td>\n                      <td>\n  \n                        <div class=\"date\">\n                          <!-- {{selectedPackage.endDate | date}} -->\n                          --\n                          <div class=\"clearfix\"></div>\n                        </div>\n  \n                      </td>\n                    </tr>\n                    <tr *ngFor=\"let packageAddon of selectedPackage.addOns\">\n                      <td>\n  \n                      </td>\n                      <td class=\"add-on-type\">\n                        <span class=\"add-on-cancel\" (click)=\"addonRemove(packageAddon.selectedAddOn)\">\n                          <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                        </span>\n                        <mat-select class=\"form-control zenwork-input package-table-select add-on-select\" placeholder=\"Select Add-on\"\n                          [(ngModel)]=\"packageAddon.selectedAddOn\" (ngModelChange)=\"addOnChange(packageAddon.selectedAddOn)\">\n                          <mat-option [value]=\"addOn.name\" *ngFor=\"let addOn of allAddOns\">\n                            {{addOn.name}}\n                          </mat-option>\n                        </mat-select>\n                      </td>\n                      <td>\n  \n                      </td>\n                      <td>\n                        {{selectedPackage.startDate | date }}\n                      </td>\n                      <td>\n                        {{selectedPackage.endDate | date}}\n                      </td>\n                    </tr>\n                    <!-- <tr>\n                        <td>\n    \n                        </td>\n                        <td class=\"add-on-type\">\n                          <span class=\"add-on-cancel\">\n                            <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                          </span>\n                          <mat-select class=\"form-control zenwork-input package-table-select add-on-select\" placeholder=\"BioMetric Access/Time Stamp\">\n                            <mat-option value=\"ACA\">\n                              BioMetric Access/Time Stamp\n                            </mat-option>\n                            <mat-option value=\"BioMetric\">\n                              ACA Comliance\n                            </mat-option>\n                            <mat-option value=\"Basic\">\n                              Advanced Training\n                            </mat-option>\n                          </mat-select>\n                        </td>\n                        <td>\n                          12/12/2018\n                        </td>\n                        <td>\n                          12/12/2019\n                        </td>\n                      </tr> -->\n                  </tbody>\n                </table>\n              </div>\n            </div>\n  \n  \n  \n             \n  \n             \n            <div class=\"clearfix\"></div>\n          </div>\n          <div class=\"buttons\">\n            <ul class=\"list-unstyled\">\n              <li class=\"list-items\">\n                <a class=\"back\" (click)=\"goBack(stepper)\">Back</a>\n              </li>\n              <li class=\"list-items\">\n                <a class=\"cancel\" [routerLink]=\"['/super-admin/super-admin-dashboard/clients']\">Cancel</a>\n              </li>\n              <li class=\"list-items pull-right\">\n                <button class='save' (click)=\"savePackageDetails(stepper)\" mdStepperNext>\n                  save&continue\n                </button>\n              </li>\n  \n              <div class=\"clearfix\"></div>\n            </ul>\n          </div>\n        </mat-step>\n  \n        <mat-step label=\"Contact Details\">\n  \n          <!-- <div class=\"contact-wrapper\">\n            <div class=\"col-xs-4\">\n              <ul class=\"zenwork-margin-bottom-zero\">\n                <li>\n                  <h3>Contact Details</h3>\n                </li>\n                <li>\n                  <h4>Primary Company Contact*</h4>\n                </li>\n                <li class=\"phone-width\">\n                  <div class=\"phone\">\n                    \n                    <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\" [(ngModel)]=\"companyContact.name\">\n                  </div>\n                </li>\n                <li>\n                  <h4>Email</h4>\n                </li>\n                <li class=\"col-md-12\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyContact.email\">\n                  </div>\n                </li>\n                <div class=\"clearfix\"></div>\n                <li>\n                  <h4>Phone</h4>\n                </li>\n                <li class=\"col-md-9\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input type=\"number\" class=\"form-control\" placeholder=\"Work Phone\" [(ngModel)]=\"companyContact.phone\">\n                  </div>\n                </li>\n                <li class=\"col-md-2 right-text\">\n  \n                  <input type=\"text\" class=\"form-control\" placeholder=\"Ext\" [(ngModel)]=\"companyContact.extention \">\n  \n                </li>\n  \n                <div class=\"clearfix\"></div>\n  \n              </ul>\n              <div class=\"form-group\">\n                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"companyContact.isPrimaryContactIsBillingContact\"></mat-checkbox>\n                <small>Is Primary Contact same as billing Contact ?</small>\n              </div>\n            </div>\n            <div class=\"col-xs-4 billing-company-contact\">\n              <ul *ngIf=\"!companyContact.isPrimaryContactIsBillingContact \">\n                <li>\n                  <h4>Billing Company Contact</h4>\n                </li>\n                <li class=\"phone-width\">\n                  <div class=\"phone\">\n                    \n                    <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\" [(ngModel)]=\"billingContact.name\">\n                  </div>\n                </li>\n                <li>\n                  <h4>Email</h4>\n                </li>\n                <li class=\"col-md-9\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"billingContact.email\">\n                  </div>\n                </li>\n                <div class=\"clearfix\"></div>\n                <li>\n                  <h4>Phone</h4>\n                </li>\n                <li class=\"col-md-8\">\n                  <div class=\"phone\">\n                    <span>\n                      <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                        height=\"16\" alt=\"img\">\n                    </span>\n                    <input type=\"number\" class=\"form-control\" placeholder=\"Work Phone\" [(ngModel)]=\"billingContact.phone\">\n                  </div>\n                </li>\n                <li class=\"col-md-2 right-text\">\n  \n                  <input type=\"number\" class=\"form-control\" placeholder=\"Ext\" [(ngModel)]=\"billingContact.extention \">\n  \n                </li>\n  \n                <div class=\"clearfix\"></div>\n  \n              </ul>\n            </div>\n            <div class=\"clearfix\"></div>\n          </div>\n  \n  \n          <div class=\"address-wrapper\">\n            <div class=\"clearfix\"></div>\n            <div class=\"col-xs-5\">\n              <h3>\n                Address Details\n              </h3>\n              <h4>\n                Company Address*\n              </h4>\n              <ul class=\"zenwork-margin-bottom-zero\">\n                <li class=\"col-md-12\">\n                  <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" [(ngModel)]=\"address.address1\">\n                </li>\n                <li class=\"col-md-12\">\n                  <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" [(ngModel)]=\"address.address2\">\n                </li>\n              </ul>\n  \n              <ul class=\"zenwork-margin-bottom-zero\">\n                <li class=\"state col-md-4\">\n                  <input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]=\"address.city\">\n                </li>\n                <li class=\"city col-md-4\">\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"State\"\n                    [(ngModel)]=\"address.state\">\n                    <mat-option value=\"newyork\">\n                      New York\n                    </mat-option>\n                    <mat-option value=\"washington\">\n                      Washington\n                    </mat-option>\n                    <mat-option value=\"florida\">\n                      Florida\n                    </mat-option>\n                  </mat-select>\n                </li>\n                <li class=\"zip col-md-4\">\n                  <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" [(ngModel)]=\"address.zipcode\">\n                </li>\n  \n                <li class=\"col-md-4 united-state\">\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\" Country\"\n                    [(ngModel)]=\"address.country\">\n                    <mat-option value=\"United States\">\n                      USA\n                    </mat-option>\n                  </mat-select>\n                </li>\n                <div class=\"clearfix\"></div>\n              </ul>\n              <div class=\"clearfix\"></div>\n              <div class=\"form-group\">\n                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"address.isPrimaryAddressIsBillingAddress\"></mat-checkbox>\n                <small>Is Primary Address same as billing address ?</small>\n              </div>\n            </div>\n  \n            <div class=\"col-xs-5\" *ngIf=\"!address.isPrimaryAddressIsBillingAddress\">\n              <h2>\n                Billing Company Address*\n              </h2>\n              <ul class=\"zenwork-margin-bottom-zero\">\n                <li class=\"col-md-12\">\n                  <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" [(ngModel)]=\"billingAddress.address1\">\n                </li>\n                <li class=\"col-md-12\">\n                  <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" [(ngModel)]=\"billingAddress.address2\">\n                </li>\n              </ul>\n  \n              <ul class=\"zenwork-margin-bottom-zero\">\n                <li class=\"state col-md-4\">\n                  <input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]=\"billingAddress.city\">\n                </li>\n                <li class=\"city col-md-4\">\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"State\"\n                    [(ngModel)]=\"billingAddress.state\">\n                    <mat-option value=\"newyork\">\n                      New York\n                    </mat-option>\n                    <mat-option value=\"washington\">\n                      Washington\n                    </mat-option>\n                    <mat-option value=\"florida\">\n                      Florida\n                    </mat-option>\n                  </mat-select>\n                </li>\n                <li class=\"zip col-md-4\">\n                  <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" [(ngModel)]=\"billingAddress.zipcode\">\n                </li>\n  \n                <li class=\"col-md-4 united-state\">\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Country\"\n                    [(ngModel)]=\"billingAddress.country\">\n                    <mat-option value=\"United States\">\n                      USA\n                    </mat-option>\n                  </mat-select>\n                </li>\n                <div class=\"clearfix\"></div>\n              </ul>\n              <div class=\"clearfix\"></div>\n            </div>\n            <div class=\"clearfix\"></div>\n          </div>\n          <div class=\"buttons\">\n            <ul class=\"list-unstyled\">\n              <li class=\"list-items\">\n                <a class=\"back\" (click)=\"goBack(stepper)\">Back</a>\n              </li>\n              <li class=\"list-items\">\n                <a class=\"cancel\">Cancel</a>\n              </li>\n              <li class=\"list-items pull-right\">\n                <button class='save' (click)=\"saveAddressDetails()\" mdStepperNext>\n                  save&continue\n                </button>\n              </li>\n              <div class=\"clearfix\"></div>\n            </ul>\n          </div> -->\n          \n          <div class=\"contact-wrapper\">\n              <div class=\"col-xs-4\">\n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li>\n                    <h3>Contact Details</h3>\n                  </li>\n                  <li>\n                    <h4>Primary Company Contact*</h4>\n                  </li>\n                  <li class=\"phone-width\">\n                    <div class=\"phone\">\n                      <!-- <span>\n                          <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                        </span> -->\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\" [(ngModel)]=\"companyContact.name\" required #contact=\"ngModel\">\n    \n                    </div>\n                    <span *ngIf=\"contact.invalid && contact.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter contact name</span>\n                  </li>\n                  <li>\n                    <h4>Email</h4>\n                  </li>\n                  <li class=\"col-md-12\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyContact.email\" required\n                        #workEmail=\"ngModel\" pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n    \n                    </div>\n                    <span *ngIf=\"workEmail.invalid && workEmail.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter work email</span>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                  <li>\n                    <h4>Phone</h4>\n                  </li>\n                  <li class=\"col-md-9\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input class=\"form-control\" placeholder=\"Work Phone\" [(ngModel)]=\"companyContact.phone\" required #workPhone=\"ngModel\" (keypress)=\"keyPress($event)\"\n                        minlength=10 maxlength=10>\n                    </div>\n                    <span *ngIf=\"workPhone.invalid && workPhone.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter work phone</span>\n                  </li>\n                  <li class=\"col-md-2 right-text\">\n    \n                    <input type=\"text\" class=\"form-control\" placeholder=\"Ext\" (keypress)=\"keyPress($event)\" [(ngModel)]=\"companyContact.extention\" maxlength=\"4\" required #ext=\"ngModel\">\n                    <span *ngIf=\"ext.invalid && ext.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Extention</span>\n                  </li>\n    \n                  <div class=\"clearfix\"></div>\n    \n                </ul>\n                <div class=\"form-group\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"companyContact.isPrimaryContactIsBillingContact\"></mat-checkbox>\n                  <small>Is Primary Contact same as billing Contact ?</small>\n                </div>\n              </div>\n              <div class=\"col-xs-4 billing-company-contact\">\n                <ul *ngIf=\"!companyContact.isPrimaryContactIsBillingContact \">\n                  <li>\n                    <h4>Billing Company Contact</h4>\n                  </li>\n                  <li class=\"phone-width\">\n                    <div class=\"phone\">\n                      <!-- <span>\n                          <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                        </span> -->\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\" [(ngModel)]=\"billingContact.name\" required #billingName=\"ngModel\">\n                    </div>\n                    <span *ngIf=\"billingName.invalid && billingName.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter billing name</span>\n                  </li>\n                  <li>\n                    <h4>Email</h4>\n                  </li>\n                  <li class=\"col-md-9\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"billingContact.email\" required\n                        #billingEmail=\"ngModel\" pattern=\"[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}\">\n                    </div>\n                    <span *ngIf=\"billingEmail.invalid && billingEmail.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter work email</span>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                  <li>\n                    <h4>Phone</h4>\n                  </li>\n                  <li class=\"col-md-8\">\n                    <div class=\"phone\">\n                      <span>\n                        <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\" height=\"16\" alt=\"img\">\n                      </span>\n                      <input class=\"form-control\" placeholder=\"Work Phone\" [(ngModel)]=\"billingContact.phone\" required #billingPhone=\"ngModel\"\n                        (keypress)=\"keyPress($event)\" minlength=10 maxlength=10>\n                    </div>\n                    <span *ngIf=\"billingPhone.invalid && billingPhone.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter work phone</span>\n                  </li>\n                  <li class=\"col-md-2 right-text\">\n    \n                    <input type=\"text\" class=\"form-control\" placeholder=\"Ext\" (keypress)=\"keyPress($event)\" [(ngModel)]=\"billingContact.extention \" maxlength=\"4\" required #extn=\"ngModel\">\n                    <span *ngIf=\"extn.invalid && extn.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Extention</span>\n                  </li>\n    \n                  <div class=\"clearfix\"></div>\n    \n                </ul>\n              </div>\n              <div class=\"clearfix\"></div>\n            </div>\n    \n    \n            <div class=\"address-wrapper\">\n              <div class=\"clearfix\"></div>\n              <div class=\"col-xs-5\">\n                <h3>\n                  Address Details\n                </h3>\n                <h4>\n                  Company Address*\n                </h4>\n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li class=\"col-md-12\">\n                    <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" [(ngModel)]=\"address.address1\" required #companyAddress=\"ngModel\">\n                    <span *ngIf=\"companyAddress.invalid && companyAddress.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter company address</span>\n                  </li>\n                  <li class=\"col-md-12\">\n                    <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" [(ngModel)]=\"address.address2\" required #companyAddress1=\"ngModel\">\n                    <span *ngIf=\"companyAddress1.invalid && companyAddress1.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter company address</span>\n                  </li>\n                </ul>\n    \n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li class=\"state col-md-4\">\n                    <input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]=\"address.city\" required #companyCity=\"ngModel\">\n                    <span *ngIf=\"companyCity.invalid && companyCity.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter city</span>\n                  </li>\n                  <li class=\"city col-md-4\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"State\" [(ngModel)]=\"address.state\"\n                      required #companyState=\"ngModel\">\n                      <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                        {{data.name}}\n                      </mat-option>\n                      <!-- <mat-option value=\"washington\">\n                        Washington\n                      </mat-option>\n                      <mat-option value=\"florida\">\n                        Florida\n                      </mat-option> -->\n                    </mat-select>\n                    <span *ngIf=\"companyState.invalid && companyState.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter state</span>\n                  </li>\n                  <li class=\"zip col-md-4\">\n                    <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" (keypress)=\"keyPress($event)\" [(ngModel)]=\"address.zipcode\" minlength=\"5\" required #companyZip=\"ngModel\">\n                    <span *ngIf=\"companyZip.invalid && companyZip.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter zip</span>\n                  </li>\n    \n                  <li class=\"col-md-4 united-state\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\" Country\" [(ngModel)]=\"address.country\"\n                      required #companyCountry=\"ngModel\">\n                      <mat-option value=\"United States\">\n                        USA\n                      </mat-option>\n                    </mat-select>\n                    <span *ngIf=\"companyCountry.invalid && companyCountry.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter country</span>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                </ul>\n                <div class=\"clearfix\"></div>\n                <div class=\"form-group\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"address.isPrimaryAddressIsBillingAddress\"></mat-checkbox>\n                  <small>Is Primary Address same as billing address ?</small>\n                </div>\n              </div>\n    \n              <div class=\"col-xs-5\" *ngIf=\"!address.isPrimaryAddressIsBillingAddress\">\n                <h2>\n                  Billing Company Address*\n                </h2>\n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li class=\"col-md-12\">\n                    <input type=\"text\" placeholder=\"Street1\" class=\"form-control\" [(ngModel)]=\"billingAddress.address1\" required #billingAddress=\"ngModel\">\n                    <span *ngIf=\"billingAddress.invalid && billingAddress.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter company address</span>\n                  </li>\n                  <li class=\"col-md-12\">\n                    <input type=\"text\" placeholder=\"Street2\" class=\"form-control\" [(ngModel)]=\"billingAddress.address2\" required #billingAddress1=\"ngModel\">\n                    <span *ngIf=\"billingAddress1.invalid && billingAddress1.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Please enter company address</span>\n                  </li>\n                </ul>\n    \n                <ul class=\"zenwork-margin-bottom-zero\">\n                  <li class=\"state col-md-4\">\n                    <input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]=\"billingAddress.city\" required #billingCity=\"ngModel\">\n                    <span *ngIf=\"billingCity.invalid && billingCity.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter city</span>\n                  </li>\n                  <li class=\"city col-md-4\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"State\" [(ngModel)]=\"billingAddress.state\"\n                      required #billingState=\"ngModel\">\n                      <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                        {{data.name}}\n                      </mat-option>\n                      <!-- <mat-option value=\"washington\">\n                        Washington\n                      </mat-option>\n                      <mat-option value=\"florida\">\n                        Florida\n                      </mat-option> -->\n                    </mat-select>\n                    <span *ngIf=\"billingState.invalid && billingState.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter state</span>\n                  </li>\n                  <li class=\"zip col-md-4\">\n                    <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" (keypress)=\"keyPress($event)\" [(ngModel)]=\"billingAddress.zipcode\" minlength=\"5\" required #billingZip=\"ngModel\">\n                    <span *ngIf=\"billingZip.invalid && billingZip.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter zip</span>\n                  </li>\n    \n                  <li class=\"col-md-4 united-state\">\n                    <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Country\" [(ngModel)]=\"billingAddress.country\"\n                      required #billingCountry=\"ngModel\">\n                      <mat-option value=\"United States\">\n                        USA\n                      </mat-option>\n                    </mat-select>\n                    <span *ngIf=\"billingCountry.invalid && billingCountry.touched\" style=\"color:#e44a49; padding:6px 0 0;\">Enter country</span>\n                  </li>\n                  <div class=\"clearfix\"></div>\n                </ul>\n                <div class=\"clearfix\"></div>\n              </div>\n              <div class=\"clearfix\"></div>\n            </div>\n            <div class=\"buttons\">\n              <ul class=\"list-unstyled\">\n                <li class=\"list-items\">\n                  <a class=\"back\" (click)=\"goBack(stepper)\">Back</a>\n                </li>\n                <li class=\"list-items\">\n                  <a class=\"cancel\">Cancel</a>\n                </li>\n                <li class=\"list-items pull-right\">\n                  <button class='save' (click)=\"saveAddressDetails(stepper)\" mdStepperNext>\n                    save&continue\n                  </button>\n                </li>\n                <div class=\"clearfix\"></div>\n              </ul>\n            </div>\n        </mat-step>\n  \n     \n      </mat-horizontal-stepper>\n    </div>\n  \n  \n  </div>"

/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ResellerAddClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResellerAddClientComponent", function() { return ResellerAddClientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/purchasePackage.service */ "./src/app/services/purchasePackage.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/chardgeCreditCard.service */ "./src/app/services/chardgeCreditCard.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import * as jspdf from 'jspdf';


var ResellerAddClientComponent = /** @class */ (function () {
    function ResellerAddClientComponent(companyService, swalAlertService, router, packageAndAddOnService, purchasePackageService, accessLocalStorageService, creditCardChargingService, loaderService, route, companySettingsService) {
        this.companyService = companyService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.packageAndAddOnService = packageAndAddOnService;
        this.purchasePackageService = purchasePackageService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.creditCardChargingService = creditCardChargingService;
        this.loaderService = loaderService;
        this.route = route;
        this.companySettingsService = companySettingsService;
        this.validity = false;
        this.isLinear = true;
        this.invoiceData1 = {
            startDate: '',
            endDate: ''
        };
        this.selectedPackage = {
            clientId: '',
            name: '',
            wholeSaleDiscount: false,
            discount: 0,
            costPerEmployee: 0,
            price: 0,
            startDate: new Date(),
            endDate: new Date(new Date().setMonth(new Date().getMonth() + 1)),
            addOns: [],
            totalPrice: 0,
            packages: [],
            billingFrequency: 'monthly'
        };
        this.companyContact = {
            name: '',
            email: '',
            phone: '',
            extention: '',
            isPrimaryContactIsBillingContact: true,
        };
        this.billingContact = {
            name: '',
            email: '',
            phone: '',
            extention: ''
        };
        this.address = {
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipcode: '',
            country: '',
            isPrimaryAddressIsBillingAddress: true
        };
        this.billingAddress = {
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipcode: '',
            country: ''
        };
        this.dicountArray = [
            { value: 10, viewValue: '10%' },
            { value: 20, viewValue: '20%' },
            { value: 30, viewValue: '30%' },
            { value: 40, viewValue: '40%' },
            { value: 50, viewValue: '50%' },
            { value: 60, viewValue: '60%' },
            { value: 70, viewValue: '70%' },
            { value: 80, viewValue: '80%' },
            { value: 90, viewValue: '90%' },
        ];
        this.selectedPackageData = {};
        this.addedCompanyData = {};
        this.discountEnable = true;
        this.invoiceData = {
            email: '',
            name: '',
            type: '',
            primaryContact: { phone: '', extention: '' },
            createdDate: ''
        };
        this.invoiceBillingCompanyAddress = {
            address1: '',
            city: '',
            state: '',
            zipcode: '',
            country: '',
        };
        this.resellersList = [];
        this.allStates = [];
        this.cardDetails = {
            cardNumber: '',
            expMonth: '',
            expYear: '',
            cvv: '',
            cardName: ''
        };
    }
    ResellerAddClientComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log("iddddd", _this.id, params);
        });
        this.isLinear = true;
        // this.discountEnable = true;
        this.discountValue = 0;
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: '',
            referralcompany: '',
            email: '',
            type: '',
            belongsToReseller: '',
            resellerId: ''
        };
        this.companyContact = {
            name: '',
            email: '',
            phone: '',
            extention: '',
            isPrimaryContactIsBillingContact: true,
        };
        this.billingContact = {
            name: '',
            email: '',
            phone: '',
            extention: ''
        };
        this.address = {
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipcode: '',
            country: '',
            isPrimaryAddressIsBillingAddress: true
        };
        this.billingAddress = {
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipcode: '',
            country: ''
        },
            this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        if (this.addedCompanyData == null) {
            this.addedCompanyData = {};
        }
        this.getAllPackages();
        this.getAllAddons();
        this.getAllresellers();
        this.getAllUSstates();
    };
    ResellerAddClientComponent.prototype.getAllUSstates = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('State', 0)
            .subscribe(function (res) {
            console.log(res);
            if (res.custom.length > 0) {
                res.custom.forEach(function (element) {
                    _this.allStates.push(element);
                    console.log("22222111111", _this.allStates);
                });
            }
            if (res.default.length > 0) {
                res.default.forEach(function (element) {
                    _this.allStates.push(element);
                });
                console.log("22222", _this.allStates);
            }
        }, function (err) {
            console.log(err);
        });
    };
    ResellerAddClientComponent.prototype.createCompany = function (stepper) {
        var _this = this;
        console.log(this.companyDetails);
        if (this.companyDetails.belongsToReseller == 'true') {
            this.companyDetails.type = 'reseller-client';
        }
        console.log(this.companyDetails);
        this.companyService.createCompany(this.companyDetails)
            .subscribe(function (res) {
            console.log("response-client", res);
            if (res.status == true) {
                _this.addedCompanyData = res.data;
                if (_this.addedCompanyData == null) {
                    _this.addedCompanyData = {};
                }
                _this.accessLocalStorageService.set('addedCompany', _this.addedCompanyData);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                _this.selectedPackage.clientId = _this.addedCompanyData._id;
                _this.goForward(stepper);
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error');
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerAddClientComponent.prototype.goForward = function (stepper) {
        stepper.next();
    };
    ResellerAddClientComponent.prototype.goBack = function (stepper) {
        stepper.previous();
    };
    ResellerAddClientComponent.prototype.getAllPackages = function () {
        var _this = this;
        this.packageAndAddOnService.getAllPackages()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.packages = res.data;
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerAddClientComponent.prototype.discountBasePrice = function () {
        console.log(this.discountValue);
        this.basePriceDiscount = (this.selectedPackage.price * this.discountValue) / 100;
        this.basePrice = this.selectedPackage.price - this.basePriceDiscount;
        this.getSpecificPackageDetails();
    };
    ResellerAddClientComponent.prototype.getAllAddons = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                console.log(_this.allAddOns);
                _this.allAddOns.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerAddClientComponent.prototype.getAllAddOnDetails = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                _this.allAddOns.forEach(function (addon) {
                    addon.isSelected = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerAddClientComponent.prototype.addAnotherAddOn = function () {
        this.selectedPackage.addOns.push({
            id: '',
            selectedAddOn: '',
            price: 0,
            index: this.selectedPackage.addOns.length + 1
        });
    };
    ResellerAddClientComponent.prototype.addOnChange = function (addOn) {
        var _this = this;
        console.log(addOn);
        console.log("before", this.selectedPackage.addOns);
        // for(let ref of this.selectedPackage.addOns){
        //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
        // }
        // console.log("after",this.selectedPackage.addOns);
        this.selectedPackage.totalPrice = 0;
        var _loop_1 = function (i) {
            this_1.selectedPackage.addOns.forEach(function (addon) {
                console.log(addon, _this.allAddOns[i]);
                if (addon.selectedAddOn == _this.allAddOns[i].name) {
                    addon.price = _this.allAddOns[i].price;
                    addon.id = _this.allAddOns[i]._id;
                    console.log(addon.id);
                    _this.selectedAddons = _this.selectedPackage.addOns;
                    _this.selectedAddons['id'] = addon.id;
                    console.log(_this.selectedAddons);
                }
            });
        };
        var this_1 = this;
        for (var i = 0; i < this.allAddOns.length; i++) {
            _loop_1(i);
        }
        var price = 0;
        this.selectedPackage.addOns.forEach(function (data) {
            price += data.price;
        });
        console.log(price);
        this.selectedPackage.totalPrice = this.dummy + price;
    };
    ResellerAddClientComponent.prototype.addonRemove = function (addon) {
        var _this = this;
        // if (addon == '') {
        //   this.selectedPackage.addOns = []
        // }
        console.log("addon", addon);
        // for (let i = 0; i < this.allAddOns.length; i++) {
        //   this.selectedPackage.addOns.forEach(addon => {
        //     console.log(addon, this.allAddOns[i])
        //     if (addon == this.allAddOns[i].name) {
        //    }
        //   });
        // }
        this.selectedPackage.addOns.forEach(function (data) {
            if (data.selectedAddOn == addon) {
                _this.selectedPackage.totalPrice -= data.price;
            }
        });
        this.selectedPackage.addOns = this.selectedPackage.addOns.filter(function (data) { return addon != data.selectedAddOn; });
        console.log(this.selectedPackage.addOns);
    };
    ResellerAddClientComponent.prototype.getAllresellers = function () {
        var _this = this;
        this.companyService.getresellers()
            .subscribe(function (res) {
            console.log("reselerrsList", res);
            _this.resellersList = res.data;
        }, function (err) {
        });
    };
    ResellerAddClientComponent.prototype.selectedReseller = function (id) {
        console.log(id);
    };
    ResellerAddClientComponent.prototype.getSpecificPackageDetails = function () {
        var _this = this;
        this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        console.log(this.selectedPackage.name, this.packages);
        this.packages.forEach(function (packageData) {
            if (packageData.name == _this.selectedPackage.name) {
                _this.selectedPackage.packages = _this.packages.filter(function (data) { return data.name == _this.selectedPackage.name; });
                console.log(_this.selectedPackage.packages);
                if (_this.validity == false) {
                    _this.selectedPackage.price = packageData.price;
                }
                else {
                    _this.selectedPackage.price = (packageData.price * 12);
                }
                // this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
            }
        });
        // this.selectedPackage.totalPrice = this.selectedPackage.price + this.selectedPackage.costPerEmployee * this.addedCompanyData.employeCount;
        // if (this.basePriceDiscount > 0) {
        // this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
        // }
        this.discountEnable = false;
        /* this.selectedPackageData = packageData; */
        /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              if (res.status) {
                this.selectedPackageData = res.data;
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err)
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          ) */
        // this.dummy = this.selectedPackage.totalPrice;
    };
    ResellerAddClientComponent.prototype.toggleDateChange = function (event) {
        console.log(event.checked);
        if (event.checked == true) {
            this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
            this.selectedPackage.billingFrequency = 'annually';
            this.validity = true;
        }
        else {
            this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
            this.validity = false;
            this.selectedPackage.billingFrequency = 'monthly';
        }
        this.getSpecificPackageDetails();
    };
    ResellerAddClientComponent.prototype.wholesaleDiscountChange = function (event) {
    };
    ResellerAddClientComponent.prototype.saveAddressDetails = function (stepper) {
        var _this = this;
        var postData = {
            _id: this.addedCompanyData._id,
            primaryContact: this.companyContact,
            isPrimaryContactIsBillingContact: this.companyContact.isPrimaryContactIsBillingContact,
            billingContact: this.billingContact,
            primaryAddress: this.address,
            isPrimaryAddressIsBillingAddress: this.address.isPrimaryAddressIsBillingAddress,
            billingAddress: this.billingAddress,
        };
        this.companyService.addContactDetails(postData)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                _this.companyContact = {
                    name: '',
                    email: '',
                    phone: '',
                    extention: '',
                    isPrimaryContactIsBillingContact: true,
                };
                _this.billingContact = {
                    name: '',
                    email: '',
                    phone: '',
                    extention: ''
                };
                _this.address = {
                    address1: '',
                    address2: '',
                    city: '',
                    state: '',
                    zipcode: '',
                    country: '',
                    isPrimaryAddressIsBillingAddress: true
                };
                _this.billingAddress = {
                    address1: '',
                    address2: '',
                    city: '',
                    state: '',
                    zipcode: '',
                    country: ''
                };
                _this.companyDetails = {
                    name: '',
                    tradeName: '',
                    employeCount: '',
                    referralcompany: '',
                    email: '',
                    type: '',
                    belongsToReseller: '',
                    resellerId: ''
                };
            }
            _this.router.navigate(['super-admin/super-admin-dashboard/reseller/' + _this.id]);
        }, function (err) {
            console.log(err);
        });
    };
    ResellerAddClientComponent.prototype.savePackageDetails = function (stepper) {
        var _this = this;
        console.log("this.selectedPackage", this.selectedPackage);
        this.invoiceadons = this.selectedPackage.addOns;
        var postData = this.selectedPackage;
        delete postData.addOns;
        postData['adons'] = this.invoiceadons;
        console.log(postData);
        this.purchasePackageService.purchasePackage(postData)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.orderId = res.data.id;
                _this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success');
                // this.activeTab = 'payment';
                _this.goForward(stepper);
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error');
        });
    };
    ResellerAddClientComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    ResellerAddClientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reseller-add-client',
            template: __webpack_require__(/*! ./reseller-add-client.component.html */ "./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.html"),
            styles: [__webpack_require__(/*! ./reseller-add-client.component.css */ "./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.css")]
        }),
        __metadata("design:paramtypes", [_services_company_service__WEBPACK_IMPORTED_MODULE_1__["CompanyService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_4__["PackageAndAddOnService"],
            _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_5__["PurchasePackageService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__["AccessLocalStorageService"],
            _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_7__["CreditCardChargingService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_9__["CompanySettingsService"]])
    ], ResellerAddClientComponent);
    return ResellerAddClientComponent;
}());



/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-dashboard.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-dashboard.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-template { padding: 0;}\n\n.zenwork-create-client-wrapper .modal-header {\n    padding: 0 0 24px 0;\n    border-bottom: 1px solid #e5e5e5;\n    height: 100vh;\n}\n\n.zenwork-custom-proceed-btn{\n    background: green !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 25px;\n}\n\n.zenworkers-wrapper{\n    background: #f8f8f8; margin:0 auto; float: none; padding: 0;height: 100vh;\n}\n\n.zenwork-margin-zero { font-size: 20px; line-height: 20px; vertical-align: middle; color:#008f3d;}\n\n.zenwork-currentpage{\n    padding:40px 0 0 0;\n}\n\n.zenwork-margin-ten-zero {\n    margin:30px 0px!important;\n}\n\n.zenwork-currentpage small { display: inline-block; vertical-align:text-top; margin: 0 15px 0 0;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 15px;\n    width:22px;\n    color:#9a9898;\n    font-size: 13px; cursor: pointer;\n}\n\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n\n.zenwork-customized-green-btn{\n    background: #797d7b!important;\n    color: #fff!important;\n    padding:3px 35px!important;\n    font-size:15px!important;\n}\n\n.zenwork-customized-green-btn small { display: inline-block; vertical-align: middle; padding: 0 10px 0 0;}\n\n.zenwork-customized-green-btn small .fa { color:#fff; font-size: 15px;}\n\n.zenwork-custom-select{\n    background: #fff!important;\n}\n\n.border-none{\n    border:0 none !important;\n}\n\n.box-shadow-none{\n    box-shadow: none !important;\n}\n\n.zenwork-custom-progress-bar{\n    width: 30% !important;\n    height: 7px !important;\n    margin-bottom: 0px !important;\n    display: inline-block !important;\n}\n\n.client-details{\n    font-size: 10px;\n    /* float: none; */\n    border-right: 1px solid gray;\n}\n\n.client-details-wrapper{\n    background: #fff;\n    padding: 20px 20px;\n    position: relative;\n}\n\n.client-details-margin{\n    margin: 6px 0px;\n    display: inline-block;\n    /* float: none; */\n}\n\n.menu-icon{\n    font-size: 19px !important;\n    cursor: pointer;\n}\n\n.menu-dropdown{\n    background: #fff;\n    border: none;\n}\n\n.menu-dropdown-list{\n    right: 0px !important;\n    left: auto;\n}\n\n.menu-dropdown-list>li>a:focus, .menu-dropdown-list>li>a:hover{\n    background-color: #008f3d; \n    color: #fff;\n}\n\n.client-details .media-body ul li{\n    display: inline-block;\n}\n\n.client-details .media-body ul{\n    margin: 0;                                      \n}\n\n.client-details .media-body ul li .reseller-btn{\n    background: #f3616c;\n    width: 30px;\n    padding: 5px 0px;\n    display: inline-block;\n    font-size: 15px;\n    margin: 0px 10px 0 0;\n    text-align: center;\n}\n\n.client-details .media-body ul li .direct-client-btn{\n    background: #1aa4db;\n    width: 30px;\n    padding: 5px 0px;\n    display: inline-block;\n    font-size: 15px;\n    margin: 0px 10px 0 0;\n    text-align: center;\n}\n\n.client-details .media-body ul li .reseller-direct-client-btn{\n    background: #fbd437;\n    width: 30px;\n    padding: 5px 0px;\n    display: inline-block;\n    font-size: 15px;\n    margin: 0px 10px 0 0;\n    text-align: center;\n}\n\n.right-divide{\n    border-right: 1px solid gray;\n    height: 5px;\n}\n\n.media-left{\n    padding-right: 25px;\n}\n\n.zenwork-client-type-position-bottom{\n    position: absolute;\n    bottom: 10px;\n    right: 10px;\n}\n\n.zenwork-client-type-position-top{\n    position: absolute;\n    top: 10px;\n    right: 10px;\n}\n\n.zenwork-client-action-button-position-middle{\n    position: absolute;\n    right: 10px;\n    top: 38%;\n}\n\n.zenwork-client-detail-icon{\n    width: 17px !important;\n    height: auto;\n    margin-right: 6px;\n}\n\n.client-type{\n    padding: 10px 0px 0px 45px; \n    text-align: right;\n}\n\n.search-form .form-control {padding:12px 40px 12px 20px; height: auto;}\n\n.media-heading  { font-size: 20px;margin-bottom: 10px;\n    color: #676565;}\n\n.media-body { vertical-align: top;}\n\n.media-right { margin: 10px 0 0;}\n\n.zenwork-client-type-position-top .btn, .zenwork-client-type-position-bottom .btn { color:#008f3d !important; font-size:17px;}\n\n.client-details-margin span { font-size: 12px; display: inline-block; margin: 0 5px 0 0; color: #5a5959;}\n\n.zenwork-custom-progress-bar .green { font-size:12px; display: inline-block;}\n\n.client-details-margin small { font-size: 14px; display: inline-block; color: #797979; padding: 0 0 0 10px; vertical-align: middle;}\n\n.client-details-margin em { font-style: normal; display: inline-block; color:#008f3d; font-size: 12px; margin: 0 0 0 6px;}\n\n"

/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-dashboard.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-dashboard.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper col-md-11\">\n    <div class=\"zenwork-currentpage\">\n      <p class=\"zenwork-margin-zero\">\n        <small>\n          <img src=\"../../../assets/images/Directory/ic_domain_24px.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n        </small>\n      {{resellerName}} Clients ({{zenworkClients}})\n      </p>\n      <hr class=\"zenwork-margin-ten-zero\">\n    </div>\n  \n    <div class=\"zenwork-clients-wrapper\">\n  \n      <div class=\"col-xs-4 padding-left-zero\">\n        <div class=\"search-border text-left\">\n          <form [formGroup]=\"clientsSearch\" class=\"search-form\">\n            <input type=\"text\" class=\"border-none box-shadow-none form-control\" formControlName=\"search\" placeholder=\"Search by name,title, etc\">\n          </form>\n          <span>\n            <span class=\"glyphicon glyphicon-search search-icon\" aria-hidden=\"true\"></span>\n          </span>\n        </div>\n      </div>\n  \n      <div class=\"col-xs-7 pull-right\">\n        <span class=\"col-xs-3 client-type\"></span>\n        <div class=\"col-xs-6\">\n          <!-- <div class=\"input-group\">\n  \n            <span class=\"input-group-addon zenwork-client-group-addon zenwork-input-group-addon\">\n              <img src=\"../../../assets/images/Directory/ic_domain_24px1.png\" alt=\"First Name Icon\" class=\"zenwork-demo-input-image-group zenwork-company-group\">\n            </span>\n            <mat-select class=\"form-control zenwork-input zenwork-custom-select\" placeholder=\"All Clients\" [(ngModel)]=\"selectedClient\"\n              (ngModelChange)=\"filterForClient()\">\n              <mat-option value=\"all\">\n                All\n              </mat-option>\n              <mat-option value=\"company\">\n                Direct-client\n              </mat-option>\n              <mat-option value=\"reseller\">\n                Reseller\n              </mat-option>\n            </mat-select>\n          </div> -->\n        </div>\n        <div class=\"col-xs-3\">\n          <a (click)=\"addclient()\" mat-button class=\"btn zenwork-customized-green-btn\">\n            <small><i aria-hidden=\"true\" class=\"fa fa-plus\"></i></small> Add Client\n          </a>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n  \n      <div class=\"clearfix\"></div>\n      <hr class=\"zenwork-margin-ten-zero\">\n    </div>\n    <div class=\"client-details-wrapper form-group\" *ngFor=\"let company of companyDetails\">\n      <div class=\"client-details col-xs-4\">\n        <div class=\"media-body\">\n          <ul class=\"list-unstyled\">\n            <li *ngIf=\"company.type =='reseller'\" class=\"list-items\">\n              <span class=\"reseller-btn\">R</span>\n            </li>\n            <li *ngIf=\"company.type =='company'\" class=\"list-items\">\n              <span class=\"direct-client-btn\">DC</span>\n            </li>\n            <li *ngIf=\"company.type =='reseller-client'\" class=\"list-items\">\n              <span class=\"reseller-direct-client-btn\">RC</span>\n            </li>\n            <li class=\"list-items\">\n              <h4> {{company.companyId.name}} </h4>\n            </li>\n          </ul>\n  \n        </div>\n      </div>\n  \n      <div class=\"client-details-margin col-xs-8\">\n        <span>Info</span>\n        <div class=\"progress zenwork-custom-progress-bar\">\n          <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\"\n            aria-valuemax=\"100\" style=\"width: 40%\">\n            <span class=\"sr-only\"><em>40%</em> Completed</span>\n          </div>\n        </div>\n        <span><em>40%</em> Completed</span>\n        <span class=\"menu-icon pull-right\">\n  \n          <div class=\"dropdown\">\n            <button class=\"menu-dropdown dropdown-toggle\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n              aria-expanded=\"true\">\n              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n  \n            </button>\n            <ul class=\"dropdown-menu menu-dropdown-list\" aria-labelledby=\"dropdownMenu1\">\n              <!-- <li><a (click)=\"resellerDashboard(company._id,company.name)\">view as Reseller Admin</a></li>\n              <li><a>Billing History</a></li> -->\n              <li><a (click)=\"editResellerClient(company,company.companyId._id)\">Edit</a></li>\n              <li><a>Archive</a></li>\n            </ul>\n          </div>\n        </span>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n    <mat-paginator [length]=\"zenworkClients\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n    </mat-paginator>\n  </div>\n  \n  \n  <!-- <div class=\"client-details-wrapper form-group\" *ngFor=\"let company of companyDetails\">\n      <div class=\"client-details\">\n        <div class=\"media\">\n          <div class=\"media-left\">\n            <img class=\"media-object\" src=\"../../../assets/images/Directory/ic_domain_24px.png\" alt=\"Company Logo\">\n          </div>\n          <div class=\"media-body\">\n            <h4 class=\"media-heading\">\n              {{company.name}}\n            </h4>\n            <div>\n              <small>\n                {{company.city}}\n              </small>\n            </div>\n            <div class=\"client-details-margin\">\n              <span>Info</span>\n              <div class=\"progress zenwork-custom-progress-bar\">\n                <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\"\n                  aria-valuemax=\"100\" style=\"width: 40%\">\n                  <span class=\"sr-only\"><em>40%</em> Completed</span>\n                </div>\n              </div>\n              <span><em>40%</em> Completed</span>\n            </div>\n            <div class=\"client-details-margin\">\n              <img src=\"../../../assets/images/Side Menu  - Sick/ic_email_24px.png\" class=\"zenwork-inner-icon zenwork-client-detail-icon\"\n                alt=\"Company-settings icon\"> <small> {{company.email}}</small>\n            </div>\n            <div class=\"client-details-margin\">\n              <img src=\"../../../assets/images/Side Menu  - Sick/ic_domain_24px.png\" class=\"zenwork-inner-icon zenwork-client-detail-icon\"\n                alt=\"Company-settings icon\"> <small>{{company.primaryContact.phone}}\n                Extn-{{company.primaryContact.extention}}</small>\n            </div>\n            <div class=\"client-details-margin\">\n              <img src=\"../../../assets/images/Side Menu  - Sick/ic_smartphone_24px.png\" class=\"zenwork-inner-icon zenwork-client-detail-icon\"\n                alt=\"Company-settings icon\"> <small> {{company.billingContact.phone || \"NA\"}}\n                Extn-{{company.billingContact .extention || \"NA\"}}</small>\n            </div>\n          </div>\n          <div class=\"media-right\">\n            <div class=\"zenwork-client-type-position-top\">\n              <button class=\"btn zenwork-customized-transparent-btn green\">\n                View as Reseller Admin\n              </button>\n            </div>\n            <div class=\"clearfix\"></div>\n            <div class=\"zenwork-client-action-button-position-middle\">\n              <a href=\"javascript:void(0)\" class=\"btn zenwork-customized-transparent-btn\">\n                <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n              </a>\n              <button class=\"btn zenwork-customized-transparent-btn\" (click)=\"editCompany(company)\">\n                <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n              </button>\n              <button class=\"btn zenwork-customized-transparent-btn\">\n                <i class=\"fa fa-archive\" aria-hidden=\"true\"></i>\n              </button>\n            </div>\n            <div class=\"clearfix\"></div>\n            <div class=\"zenwork-client-type-position-bottom\">\n              <button class=\"btn zenwork-customized-transparent-btn green\">\n                Client Type : {{company.type}}\n              </button>\n            </div>\n            <div class=\"clearfix\"></div>\n          </div>\n        </div>\n      </div>\n    </div> -->"

/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-dashboard.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-dashboard.component.ts ***!
  \********************************************************************/
/*! exports provided: ResellerDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResellerDashboardComponent", function() { return ResellerDashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_reseller_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/reseller.service */ "./src/app/services/reseller.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// import { MessageService } from '../services/message-service.service';
var ResellerDashboardComponent = /** @class */ (function () {
    function ResellerDashboardComponent(_location, route, resellerService, companyService, accessLocalStorageService, router) {
        this._location = _location;
        this.route = route;
        this.resellerService = resellerService;
        this.companyService = companyService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.router = router;
        this.pageNo = 1;
        this.perPage = 10;
    }
    ResellerDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.clientsSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('')
        });
        this.resellerName = localStorage.getItem('resellerName');
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log("iddddd", _this.id, params);
        });
        // this.resellerService.getreseller(this.id)
        //   .subscribe(
        //     (res: any) => {
        //     })
        // this.clientsSearch = new FormGroup({
        //   search: new FormControl('')
        // })
        // this.messageService.sendMessage('super-admin');
        // this.onChanges(this.id);
        this.getClientsData();
    };
    ResellerDashboardComponent.prototype.backClicked = function () {
        console.log('backclickk');
        this._location.back();
    };
    // filterForClient(){
    //   console.log(this.selectedClient);
    //   this.getClientsData()
    // }
    ResellerDashboardComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getClientsData();
    };
    // onChanges(id) {
    //   this.clientsSearch.controls['search'].valueChanges
    //     .subscribe(val => {
    //       // console.log(this.zenworkersSearch.get('search').value);
    //       this.getClientsData();
    //     });
    // }
    ResellerDashboardComponent.prototype.getClientsData = function () {
        var _this = this;
        this.resellerService.getAllCompanies({ searchQuery: this.clientsSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage, resellerId: this.id })
            .subscribe(function (response) {
            if (response.status) {
                _this.zenworkClients = response.data.length;
                _this.companyDetails = response.data;
            }
            console.log("Response", response);
        });
    };
    ResellerDashboardComponent.prototype.editCompany = function (company) {
        this.accessLocalStorageService.set('editCompany', company);
        this.router.navigate(['/super-admin/super-admin-dashboard/edit-company']);
    };
    // sendMessage(){
    //   this.messageService.sendMessage('add-client')
    // }
    ResellerDashboardComponent.prototype.resellerDashboard = function (id, comapanyName) {
        localStorage.setItem('resellerName', comapanyName);
        this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + id]);
        // this.messageService.sendMessage('reseller')
    };
    ResellerDashboardComponent.prototype.addclient = function () {
        this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.id + '/add-client']);
    };
    ResellerDashboardComponent.prototype.editResellerClient = function (company, id) {
        console.log(company, id);
        this.accessLocalStorageService.set('editCompany', company);
        this.router.navigate(['/super-admin/super-admin-dashboard/reseller/' + this.id + '/edit-client/' + id]);
    };
    ResellerDashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reseller-dashboard',
            template: __webpack_require__(/*! ./reseller-dashboard.component.html */ "./src/app/reseller-dashboard/reseller-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./reseller-dashboard.component.css */ "./src/app/reseller-dashboard/reseller-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_reseller_service__WEBPACK_IMPORTED_MODULE_3__["ResellerService"],
            _services_company_service__WEBPACK_IMPORTED_MODULE_5__["CompanyService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__["AccessLocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ResellerDashboardComponent);
    return ResellerDashboardComponent;
}());



/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-dashboard.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-dashboard.module.ts ***!
  \*****************************************************************/
/*! exports provided: ResellerDashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResellerDashboardModule", function() { return ResellerDashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _reseller_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reseller-dashboard.component */ "./src/app/reseller-dashboard/reseller-dashboard.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _administrator_administrator_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./administrator/administrator.component */ "./src/app/reseller-dashboard/administrator/administrator.component.ts");
/* harmony import */ var _create_administrator_template_create_administrator_template_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./create-administrator-template/create-administrator-template.component */ "./src/app/reseller-dashboard/create-administrator-template/create-administrator-template.component.ts");
/* harmony import */ var _reseller_add_client_reseller_add_client_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./reseller-add-client/reseller-add-client.component */ "./src/app/reseller-dashboard/reseller-add-client/reseller-add-client.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _reseller_edit_client_reseller_edit_client_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./reseller-edit-client/reseller-edit-client.component */ "./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var router = [
    { path: '', redirectTo: 'ResellerDashboardComponent', pathMatch: 'full' },
    {
        path: '', component: _reseller_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["ResellerDashboardComponent"],
    },
    {
        path: 'reseller-dashboard', component: _reseller_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["ResellerDashboardComponent"],
    },
    { path: 'add-client', component: _reseller_add_client_reseller_add_client_component__WEBPACK_IMPORTED_MODULE_8__["ResellerAddClientComponent"] },
    { path: 'edit-client/:id', component: _reseller_edit_client_reseller_edit_client_component__WEBPACK_IMPORTED_MODULE_10__["ResellerEditClientComponent"] },
    { path: 'administrator', component: _administrator_administrator_component__WEBPACK_IMPORTED_MODULE_6__["AdministratorComponent"] }
];
var ResellerDashboardModule = /** @class */ (function () {
    function ResellerDashboardModule() {
    }
    ResellerDashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(router),
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"]
            ],
            entryComponents: [
                _create_administrator_template_create_administrator_template_component__WEBPACK_IMPORTED_MODULE_7__["CreateAdministratorTemplateComponent"]
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ],
            declarations: [_create_administrator_template_create_administrator_template_component__WEBPACK_IMPORTED_MODULE_7__["CreateAdministratorTemplateComponent"],
                _reseller_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["ResellerDashboardComponent"],
                _administrator_administrator_component__WEBPACK_IMPORTED_MODULE_6__["AdministratorComponent"],
                _reseller_add_client_reseller_add_client_component__WEBPACK_IMPORTED_MODULE_8__["ResellerAddClientComponent"],
                _reseller_edit_client_reseller_edit_client_component__WEBPACK_IMPORTED_MODULE_10__["ResellerEditClientComponent"]]
        })
    ], ResellerDashboardModule);
    return ResellerDashboardModule;
}());



/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.css":
/*!********************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-currentpage {\n    padding: 40px 0 0 0;\n}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-padding-25-zero{\n    padding: 25px 0!important;\n}\n\n.zenworkers-wrapper{\n    padding:0; float: none;margin: 0 auto 30px;\n}\n\n.search-border{\n    border-radius: 3px;\n    height: 25px;\n    position: relative;\n}\n\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 12px;\n    width: 13px;\n    color: #000;\n    font-size: 10px;\n}\n\n.padding-left-zero{\n    padding-left: 0 !important;\n}\n\n.zenwork-client-group-addon{\n    background: #fff;\n    border: 1px transparent;\n    border-right: 1px solid #ccc;\n}\n\n.zenwork-customized-green-btn{\n    background: #439348!important;\n    color: #fff!important;\n    padding: 7px 12px!important;\n    font-size: 12px!important;\n    border-radius: 0px!important;\n}\n\n.zenwork-custom-select{\n    background: #fff!important;\n}\n\n.border-none{\n    border:0 none !important;\n}\n\n.address-wrapper { padding: 0 0 20px;}\n\n.address-wrapper ul { display: inline-block; width: 100%;}\n\n.address-wrapper ul li { margin: 0 0px 20px 0; padding: 0;}\n\n.address-wrapper ul li.details-width{ width: 100%;}\n\n.address-wrapper ul li .form-control { font-size: 15px; line-height:15px;border: none; box-shadow: none; height: auto;padding:13px 0 13px 10px;}\n\n.address-wrapper ul li select { background:#fff; border: none; width: 100%; height: 34px; color:#948e8e; padding: 6px 10px; margin: 0;}\n\n.address-wrapper ul li.united-state { margin: 0; clear: left;}\n\n.address-wrapper ul li.state, .address-wrapper ul li.city { padding: 0 10px 0 0;}\n\n.contact-wrapper { display: block; padding: 0 0 20px;}\n\n.contact-wrapper ul { margin: 0 0 30px;}\n\n.contact-wrapper ul li {margin: 0 0 15px 0; padding: 0;}\n\n.contact-wrapper ul li h4 { color:#636060; font-size: 16px; line-height: 16px; margin: 0 0 15px;}\n\n.contact-wrapper ul li .phone { display: block; background:#fff; padding: 0 10px;clear: left;}\n\n.contact-wrapper ul li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;    border-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n\n.contact-wrapper ul li .phone span .fa { color: #636060;\nfont-size: 23px;\nline-height: 20px;\nwidth: 18px;\ntext-align: center;}\n\n.contact-wrapper ul li .phone .form-control { display: inline-block;vertical-align: middle; width: 88%; border: none; background:transparent; box-shadow: none; padding:13px 0 13px 10px;}\n\n.contact-wrapper ul li .form-control { display: inline-block;vertical-align: middle; width:100%; border: none; background:#fff; box-shadow: none;padding:13px 10px;\nheight: auto;}\n\n.contact-wrapper ul li.phone-width { float: none; clear:left;}\n\n.contact-wrapper ul li.phone-width .phone .form-control { width: 100%; padding: 13px 10px 13px 10px;}\n\n.zenwork-custom-input{\n    background: #fff !important; padding: 13px 15px; height: auto;\n}\n\n.company-details-wrapper , .contact-wrapper{\n    padding:30px 0px;\n    border-bottom: 1px dotted;\n}\n\n.address-wrapper{\n    padding: 40px 0px;\n}\n\n.zenwork-custom-proceed-btn{\n    background:#008f3d !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 30px;\n    margin: 0 0 0 40px;\n}\n\n.zenwork-customized-transparent-btn { margin: 0 0 0 20px;}\n\n.zenwork-margin-zero small { display:inline-block; vertical-align: middle; font-size: 17px; margin: 0 10px 0 0;}\n\n.zenwork-margin-zero{ font-size: 18px; color:#585858;}\n\n.company-details h2 { color:#008f3d; font-size: 18px; line-height: 18px; display: inline-block; margin: 0 0 20px;}\n\n.company-details label { color:#5f5c5c; font-size: 15px; padding: 0 0 10px; font-weight: normal;}\n\n.right-text { margin: 0 0 0 20px !important;}\n\n.contact-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n\n.contact-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n\n.address-wrapper h2 { color:#636060; font-size: 17px; line-height: 17px; display: inline-block; margin: 0 0 20px;}\n\n.address-wrapper .zenwork-customized-checkbox { vertical-align: top; display: inline-block;}\n\n.address-wrapper .form-group { margin: 15px 0 0;}\n\n.address-wrapper small { display: inline-block; vertical-align: middle;font-size: 16px; color:#585858; line-height: 23px;}\n\n.client-tabs {padding:10px 0 0;}\n\n.client-tabs .tab-content { margin:40px 0 0;}\n\n.client-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border: none;background: transparent;}\n\n.client-tabs .nav-tabs {border-bottom:#dbdbdb 2px solid;}\n\n.client-tabs .nav-tabs > li { padding:0 30px;}\n\n.client-tabs .nav-tabs > li > a {font-size: 18px;line-height: 18px;font-weight:normal;color:#484747;border: none;padding: 10px 0 15px !important;margin: 0;}\n\n.client-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;}\n\n.client-tabs .nav-tabs > li.active{border-bottom:#439348 5px solid;padding:0px 30px 0 30px;color:#000;}\n\n/* package-billing css */\n\n.package-billing-main{\n    margin: 40px 0 0 0;\n}\n\n.package-billing-main .billing-block h2 { padding: 20px 20px 5px;}\n\n.discount-block{\n    padding: 0;\n}\n\n.billing-package-block{\n    float: none;\n}\n\n.border-bottom-green{\n    border-bottom: 2px solid #439348;\n    padding: 0 0px 5px 0;\n}\n\n.discount-heading .list-items{\n    display: inline-block;\n    padding: 0px 20px 5px 0;\n    font-size: 16px; float: left;\n}\n\n.discount-heading .list-items span{\n    display: block;\n}\n\n.package-table { margin: 0 auto; float: none; padding: 0 30px 0 0px;}\n\n.package-table h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n\n.package-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;text-align: center; width: 34%;}\n\n.package-table .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n\n.package-table .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n\n.package-table .table>tbody>tr>td, .package-table .table>tfoot>tr>td, .package-table .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-bottom: #e0dada 1px solid;font-size: 15px;text-align: center;}\n\n.package-table .table>tbody>tr>td .mat-select-placeholder{\n    font-size: 15px;\n    text-align: left;\n}\n\n.package-table .table>tbody>tr>td .package-table-select{\n    background: #f8f8f8;\n}\n\n.add-on-cancel{\n    float: left;\n    padding:5px 3px 0 0;\n}\n\n.package-table .table>tbody>tr>td .add-on-select{\n    float: right;\n    width: 87% !important;\n}\n\n.package-table .table>tbody>tr .add-on-type{\n    width:40%;\n}\n\n.package-table .table>tbody>tr .package-name{\n    width: 21%;\n}\n\n.payment { margin: 50px 0 0;}\n\n.payment-lft { padding:30px 15px 0 0;}\n\n.payment-lft h2 {color:#008f3d; font-size: 17px; line-height: 17px;margin: 0; float: left; padding: 5px 0 0; font-weight: 600;}\n\n.payment-lft > a {color:#008f3d; font-size: 16px; line-height: 17px;margin: 0; cursor: pointer; float: right; border-radius: 30px; padding: 10px 20px; border:#008f3d 1px solid;}\n\n.payment-method { margin: 30px 0 0;}\n\n.payment-method .panel-default>.panel-heading {background-color:#fff !important; border: none; position: relative; padding:0; box-shadow: 0 0 10px #ccc; display: block;}\n\n.payment-method .panel-title { color:#808080; display:block; font-size: 15px; font-weight: 600;}\n\n.payment-method .panel-title span { display: inline-block; padding:0 10px 0 0; margin: 0 10px 0 0; border-right:#ccc 1px solid;}\n\n.payment-method .panel-title a:hover { text-decoration: none;}\n\n.payment-method .panel-title>.small, .payment-method .panel-title>.small>a, .payment-method .panel-title>a, .payment-method .panel-title>small, .payment-method .panel-title>small>a { text-decoration:none; display: block; padding: 15px;}\n\n.payment-method .panel { background: none; box-shadow: none; border-radius: 0;}\n\n.payment-method .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color:#798993;\n}\n\n.payment-method .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.payment-method .panel-group .panel-heading+.panel-collapse>.list-group, .payment-method .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.payment-method .panel-heading .accordion-toggle:after { margin:4px 0 0; font-size: 12px; line-height: 10px;}\n\n.payment-method .panel-body { padding: 0 0 5px;}\n\n.payment-method .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.payment-method .panel-default { border-color:transparent;}\n\n.payment-method .panel-group .panel+.panel { margin-top: 20px;}\n\n.ach-payment { margin: 20px 0 0 0;}\n\n.credit-payment { margin: 20px 0 0;}\n\n.paypal-payment { margin: 20px 0 0;}\n\n.payment-rgt { padding:50px 0 0 20px;}\n\n.payment-rgt h3 { color:#008f3d; font-size: 16px; line-height: 17px; display: inline-block; margin: 0 0 20px;}\n\n.billing-block { background:#fff;}\n\n.billing-block h2 { background:#f0be19; padding: 20px; border-radius:5px 5px 0 0;margin:0;}\n\n.billing-block h2 span { float: left; color:#3e3e3e; font-size: 16px; line-height: 16px;}\n\n.billing-block h2 small { float:right; color:#3e3e3e; font-size: 16px; line-height: 16px;}\n\n.annualy-block { float: right;}\n\n.annualy-block p { float:left; color:#3e3e3e; font-size: 16px; line-height: 16px; border-bottom:#3e3e3e 1px solid; display:inline-block; margin: 0 30px 0 0;}\n\n.billing-block > ul { padding:10px 0; margin: 0;}\n\n.billing-block > ul > li { border-bottom:#b8c0c6 1px dashed; padding:13px 0 10px;}\n\n.billing-price { padding: 0 20px;}\n\n.billing-price > ul {  padding:0;}\n\n.billing-price > ul > li { margin: 0 0 7px; border-bottom:none;}\n\n.billing-price ul li h5 { color:#008f3d; font-size: 15px; line-height: 15px; margin: 0 0 3px; border-bottom:#008f3d 1px solid; display: inline-block;}\n\n.billing-price ul li span { float: left; color:#777; font-size: 15px; line-height: 15px;}\n\n.billing-price ul li small { float:right; color:#008f3d; font-size: 15px; line-height: 15px;}\n\n.billing-price ul li a { float: left; cursor: pointer; display: inline-block; background:#b8c0c6; font-size: 13px; line-height: 13px; padding:4px 10px; color:#fff;\nborder-radius: 3px; margin: -3px 0 0 15px;}\n\n.billing-price ul li var { float: left;; color:#008f3d; font-size: 16px; line-height: 15px; margin: 0 0 0 10px; font-style: normal;}\n\n.billing-btm { margin:0 20px;}\n\n.billing-btm .form-control { float: left; border:#d2cbcb 1px solid; width: 30%; padding: 15px 10px; border-radius: 5px; box-shadow: none;text-transform: uppercase;}\n\n.billing-btm a.apply { float: left; color:#92b8ce; margin:6px 0 0 10px; font-size: 14px; cursor: pointer;}\n\n.billing-btm em { float: right; color:#777; font-size: 15px; line-height: 15px; font-style: normal; padding: 10px 0 0;}\n\n.billing-block h4 { background:#eef7ff; padding: 20px;margin:0;}\n\n.billing-block h4 h1 { float: left; color:#008f3d; font-size: 18px; line-height: 16px; font-weight: bold; margin: 0;}\n\n.billing-block h4 p { float:right; color:#008f3d; font-size: 18px; line-height: 16px;font-weight: bold; margin: 0;}\n\n.payment-rgt a.btn {border-radius: 20px;color:#616060; font-size: 15px; padding:6px 25px; background:transparent; margin:40px 0 0; float:left; position: static;}\n\n.payment-rgt a.btn:hover {background: #008f3d; color:#fff;}\n\n.heade-check .checkbox label::before {width: 20px; height: 20px;}\n\n.heade-check .checkbox label::after { color:#008f3d;width: 20px; height: 20px; padding:0 0 0 4px; font-size: 12px;}\n\n.heade-check .checkbox{ margin: 0;}\n\n.heade-check .checkbox label { padding-left: 0; color:#948e8e; font-size: 13px; line-height: 20px; padding: 0 0 0 10px;}\n\n.payment-rgt .cont-check { margin:20px 20px 0;}\n\n.payment-rgt .cont-check .checkbox label::after { padding:2px 0 0 4px;border-radius: 3px;}\n\n.payment-rgt .cont-check .checkbox label { padding-left:15px;color:#6d6d6d; font-size: 15px;line-height: 17px;}\n\n.payment-rgt .cont-check .checkbox label small { color:#008f3d;font-size: 15px; }\n\n.date { height: auto !important; line-height:inherit !important;}\n\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0;}\n\n.date span { border: none !important; padding: 0 !important;}\n\n.date .form-control { width: 70% !important;}\n\n.date .form-control:focus { box-shadow: none; border: none;}\n\n/* packahe-billing-css-ends */\n\n.discount-block .btn {\n    background: #008f3d!important;\n    color: #fff!important;\n    padding: 8px 20px!important;\n    font-size: 15px!important; margin: 0 0 0 20px;\n}\n\n.discount-block small { padding:0 10px 0 0;}\n\n.discount-block small .fa { font-size: 14px;}\n\n/* ==========================edit-company-starts========================== */\n\n.setting-table{height: 100vh;}\n\n.setting-table .table>thead>tr>th { padding: 18px 15px;background: #eec700}\n\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td { padding: 17px 15px;}\n\n.payment-method .panel-group .credit-card-payment {\n    background: #fff;\n    box-shadow: -2px 0px 21px -1px rgba(0,0,0,0.75);\n    margin: 10px 0 0 0;\n}\n\n.card-buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n}\n\n.card-buttons ul li .my-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #777d7a;\n    padding: 8px 30px; \n}\n\n.card-buttons ul li .my-cards-btn:hover,.card-buttons ul li .my-cards-btn:active,.card-buttons ul li .my-cards-btn:visited{\n    border-radius: 20px;\n    background: #777d7a;\n    color: #fff;\n    padding: 8px 30px;\n\n}\n\n.card-buttons ul li .secure-cards-btn:hover,.card-buttons ul li .secure-cards-btn:active,.card-buttons ul li .secure-cards-btn:visited{\n    border-radius: 20px;\n    background: #439348;\n    color: #fff;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n\n.card-buttons ul li .secure-cards-btn{\n    border-radius: 20px;\n    background: #fff;\n    color: #439348;\n    padding: 8px 30px;\n    border: 1px solid #439348;\n}\n\n.credit-card-details-date ul li{\n    display: inline-block;\n    padding: 0px 10px 0 0;\n    width: 29.5%;\n}\n\n.wrapper{\n    padding: 10px 25px;\n}"

/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenworkers-wrapper col-md-11\">\n  <div class=\"client-tabs\">\n\n    <ul class=\"nav nav-tabs\">\n      <li [ngClass]=\"{ 'active':activeTab==='create'}\">\n        <a data-toggle=\"tab\" href=\"#tab1\">Client Details</a>\n      </li>\n      <li [ngClass]=\"{ 'active':activeTab==='package'}\">\n        <a data-toggle=\"tab\" href=\"#tab2\">Billing History</a>\n      </li>\n      <li [ngClass]=\"{ 'active':activeTab==='payment'}\">\n        <a data-toggle=\"tab\" href=\"#tab3\">Upgrade/Downgrade Package</a>\n      </li>\n\n    </ul>\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n  <div class=\"tab-content\">\n\n    <div id=\"tab1\" class=\"tab-pane fade\" [ngClass]=\"{ 'active':activeTab==='create','in':activeTab==='create'}\">\n\n      <div class=\"company-details-wrapper\">\n        <div class=\"company-details\">\n          <div class=\"col-xs-4\">\n\n            <div class=\"form-group\">\n              <label>Company Name*</label>\n              <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Company Name\"\n                [(ngModel)]=\"companyDetails.name\" maxlength=\"28\" name=\"name\" required #name=\"ngModel\">\n              <span *ngIf=\"!companyDetails.name && name.touched || (!companyDetails.name && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter company name</span>\n            </div>\n            <div class=\"form-group\">\n              <label>Trade Name*</label>\n              <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"Trade Name\"\n                [(ngModel)]=\"companyDetails.tradeName\" maxlength=\"28\" name=\"tradeName\" required #tradeName=\"ngModel\">\n              <span *ngIf=\"!companyDetails.tradeName && tradeName.touched || (!companyDetails.tradeName && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter tradename name</span>\n            </div>\n            <div class=\"form-group\">\n              <label>No. of employees</label>\n              <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" (keypress)=\"keyPress($event)\"\n                placeholder=\"No. of Employees\" [(ngModel)]=\"companyDetails.employeCount\" maxlength=\"5\"\n                name=\"employeCount\" required #employeCount=\"ngModel\">\n              <span\n                *ngIf=\"!companyDetails.employeCount && employeCount.touched || (!companyDetails.employeCount && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter employee count</span>\n            </div>\n            <div class=\"form-group\">\n              <div class=\"col-xs-6 padding-left-zero\">\n                <label>Is the Client Reseller?</label>\n                <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                  placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.type\" name=\"type\" required\n                  #type=\"ngModel\">\n                  <mat-option value=\"reseller\">\n                    Yes\n                  </mat-option>\n                  <mat-option value=\"company\">\n                    No\n                  </mat-option>\n                </mat-select>\n                <span *ngIf=\"!companyDetails.type && type.touched || (!companyDetails.type && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter employee count</span>\n              </div>\n              <div class=\"col-xs-6 padding-right-zero\" *ngIf=\"companyDetails.type == 'company'\">\n                <div class=\"form-group\">\n                  <label>Client belongs to Reseller?</label>\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                    placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.belongsToReseller\">\n                    <mat-option value=\"true\">\n                      Yes\n                    </mat-option>\n                    <mat-option value=\"false\">\n                      No\n                    </mat-option>\n                  </mat-select>\n                </div>\n                <div class=\"form-group\" *ngIf=\"companyDetails.belongsToReseller != 'false'\">\n                  <label>Select Reseller</label>\n                  <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                    placeholder=\"Client / Reseller\" [(ngModel)]=\"companyDetails.resellerId\"\n                    (ngModelChange)=\"selectedReseller($event)\" [ngModelOptions]=\"{standalone:true}\">\n                    <mat-option value=\"{{reseller._id}}\" *ngFor=\"let reseller of resellersList\">\n                      {{reseller.name}}\n                    </mat-option>\n\n                  </mat-select>\n                </div>\n              </div>\n              <div class=\"clearfix\"></div>\n            </div>\n          </div>\n          <div class=\"col-xs-4\">\n            <div class=\"form-group col-md-6\">\n              <label>Client Status</label>\n              <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"active status\"\n                [(ngModel)]=\"clientStatus\" readonly >\n            </div>\n            <div class=\"form-group col-md-6\">\n              <label>Inactive Date</label>\n              <input type=\"text\" class=\"form-control zenwork-input zenwork-custom-input\" placeholder=\"End Date\"\n                [(ngModel)]=\"comapnyInactiveDate\" readonly >\n            </div>\n          </div>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n\n      <div class=\"contact-wrapper\">\n        <div class=\"col-xs-4\">\n          <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('primaryContact')\">\n            <li>\n              <h4>Primary Company Contact*</h4>\n            </li>\n            <li class=\"phone-width\">\n              <div class=\"phone\">\n                <!-- <span>\n                <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n              </span> -->\n                <input type=\"text\" class=\"form-control\" maxlength=\"28\" placeholder=\"Contact Name\"\n                  [(ngModel)]=\"companyDetails.primaryContact.name\" name=\"contactname\" required #contactname=\"ngModel\">\n\n              </div>\n              <span\n                *ngIf=\"!companyDetails.primaryContact.name && contactname.touched || (!companyDetails.primaryContact.name && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter contact person name</span>\n            </li>\n            <li>\n              <h4>Email</h4>\n            </li>\n            <li class=\"col-md-9\">\n              <div class=\"phone\">\n                <span>\n                  <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                    height=\"16\" alt=\"img\">\n                </span>\n                <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\" [(ngModel)]=\"companyDetails.email\"\n                  name=\"email\" required #email=\"ngModel\">\n                <span *ngIf=\"!companyDetails.email && email.touched || (!companyDetails.email && isValid)\"\n                  style=\"color:#e44a49; padding:6px 0 0;\">Enter contact person name</span>\n              </div>\n            </li>\n            <div class=\"clearfix\"></div>\n            <li>\n              <h4>Phone</h4>\n            </li>\n            <li class=\"col-md-8\">\n              <div class=\"phone\">\n                <span>\n                  <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                    height=\"16\" alt=\"img\">\n                </span>\n                <input type=\"text\" class=\"form-control\" placeholder=\"Work Phone\"\n                  [(ngModel)]=\"companyDetails.primaryContact.phone\" (keypress)=\"keyPress($event)\" name=\"phone\"\n                  maxlength=\"10\" minlength=\"10\" required #phone=\"ngModel\">\n\n              </div>\n              <span\n                *ngIf=\"!companyDetails.primaryContact.phone && phone.touched || (!companyDetails.primaryContact.phone && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter phone number</span>\n            </li>\n            <li class=\"col-md-2 right-text\">\n\n              <input type=\"text\" class=\"form-control\" placeholder=\"Ext\"\n                [(ngModel)]=\"companyDetails.primaryContact.extention\" (keypress)=\"keyPress($event)\" maxlength=\"3\"\n                name=\"extention\" required #extention=\"ngModel\">\n\n\n            </li>\n            <span\n              *ngIf=\"!companyDetails.primaryContact.extention && extention.touched || (!companyDetails.primaryContact.extention && isValid)\"\n              style=\"color:#e44a49; padding:6px 0 0;\">Enter extension number</span>\n\n            <div class=\"clearfix\"></div>\n\n          </ul>\n          <div class=\"form-group\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\"\n              [(ngModel)]=\"companyDetails.isPrimaryContactIsBillingContact\"></mat-checkbox>\n            <small>Is Primary Contact same as billing Contact ?</small>\n          </div>\n        </div>\n        <div class=\"col-xs-4\">\n          <ul\n            *ngIf=\"!companyDetails.isPrimaryContactIsBillingContact && companyDetails.hasOwnProperty('billingContact')\">\n            <li>\n              <h4>Billing Company Contact</h4>\n            </li>\n            <li class=\"phone-width\">\n              <div class=\"phone\">\n                <!-- <span>\n                <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n              </span> -->\n                <input type=\"text\" class=\"form-control\" placeholder=\"Contact Name\"\n                  [(ngModel)]=\"companyDetails.billingContact.name\">\n              </div>\n            </li>\n            <li>\n              <h4>Email</h4>\n            </li>\n            <li class=\"col-md-9\">\n              <div class=\"phone\">\n                <span>\n                  <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                    height=\"16\" alt=\"img\">\n                </span>\n                <input type=\"email\" class=\"form-control\" placeholder=\"Work Email\"\n                  [(ngModel)]=\"companyDetails.billingContact.email\">\n              </div>\n            </li>\n            <div class=\"clearfix\"></div>\n            <li>\n              <h4>Phone</h4>\n            </li>\n            <li class=\"col-md-8\">\n              <div class=\"phone\">\n                <span>\n                  <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n                    height=\"16\" alt=\"img\">\n                </span>\n                <input type=\"text\" class=\"form-control\" (keypress)=\"keyPress($event)\" placeholder=\"Work Phone\"\n                  [(ngModel)]=\"companyDetails.billingContact.phone\">\n              </div>\n            </li>\n            <li class=\"col-md-2 right-text\">\n\n              <input type=\"text\" class=\"form-control\" (keypress)=\"keyPress($event)\" placeholder=\"Ext\"\n                [(ngModel)]=\"companyDetails.billingContact.extention \">\n\n            </li>\n\n            <div class=\"clearfix\"></div>\n\n          </ul>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <div class=\"address-wrapper\">\n        <div class=\"clearfix\"></div>\n        <div class=\"col-xs-5\">\n          <h2>\n            Company Address*\n          </h2>\n          <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('address')\">\n            <li class=\"col-md-12\">\n              <input type=\"text\" placeholder=\"Street1\" class=\"form-control\"\n                [(ngModel)]=\"companyDetails.address.address1\" maxlength=\"28\" name=\"address1\" required\n                #address1=\"ngModel\">\n              <span\n                *ngIf=\"!companyDetails.address.address1 && address1.touched || (!companyDetails.address.address1 && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter street name</span>\n            </li>\n            <li class=\"col-md-12\">\n              <input type=\"text\" placeholder=\"Street2\" class=\"form-control\"\n                [(ngModel)]=\"companyDetails.address.address2\" maxlength=\"28\" name=\"address2\" required\n                #address2=\"ngModel\">\n              <span\n                *ngIf=\"!companyDetails.address.address2 && address2.touched || (!companyDetails.address.address2 && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter address</span>\n            </li>\n          </ul>\n\n          <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('address')\">\n            <li class=\"state col-md-4\">\n              <input type=\"text\" placeholder=\"City\" maxlength=\"22\" class=\"form-control\"\n                [(ngModel)]=\"companyDetails.address.city\" name=\"city\" required #city=\"ngModel\">\n              <span *ngIf=\"!companyDetails.address.city && city.touched || (!companyDetails.address.city && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter city</span>\n            </li>\n            <li class=\"city col-md-4\">\n              <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                placeholder=\"State\" [(ngModel)]=\"companyDetails.address.state\" name=\"state\" required #state=\"ngModel\">\n                <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                  {{data.name}}\n                </mat-option>\n                <!-- <mat-option value=\"washington\">\n                  Washington\n                </mat-option>\n                <mat-option value=\"florida\">\n                  Florida\n                </mat-option> -->\n              </mat-select>\n              <span *ngIf=\"!companyDetails.address.state && state.touched || (!companyDetails.address.state && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter state</span>\n            </li>\n            <li class=\"zip col-md-4\">\n              <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" (keypress)=\"keyPress($event)\" minlength=\"5\"\n                [(ngModel)]=\"companyDetails.address.zipcode\" name=\"zipcode\" required #zipcode=\"ngModel\">\n              <span\n                *ngIf=\"!companyDetails.address.zipcode && zipcode.touched || (!companyDetails.address.zipcode && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter zipcode</span>\n            </li>\n\n            <li class=\"col-md-4 united-state\">\n              <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                placeholder=\" Country\" [(ngModel)]=\"companyDetails.address.country\" name=\"country\" required\n                #country=\"ngModel\">\n                <mat-option value=\"United States\">\n                  USA\n                </mat-option>\n              </mat-select>\n              <span\n                *ngIf=\"!companyDetails.address.country && country.touched || (!companyDetails.address.country && isValid)\"\n                style=\"color:#e44a49; padding:6px 0 0;\">Enter country</span>\n            </li>\n            <div class=\"clearfix\"></div>\n          </ul>\n          <div class=\"clearfix\"></div>\n          <div class=\"form-group\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\"\n              [(ngModel)]=\"companyDetails.isPrimaryAddressIsBillingAddress\"></mat-checkbox>\n            <small>Is Primary Address same as billing address ?</small>\n          </div>\n        </div>\n\n        <div class=\"col-xs-5\" *ngIf=\"!companyDetails.isPrimaryAddressIsBillingAddress\">\n          <h2>\n            Billing Company Address*\n          </h2>\n          <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('billingAddress')\">\n            <li class=\"col-md-12\">\n              <input type=\"text\" placeholder=\"Street1\" class=\"form-control\"\n                [(ngModel)]=\"companyDetails.billingAddress.address1\">\n            </li>\n            <li class=\"col-md-12\">\n              <input type=\"text\" placeholder=\"Street2\" class=\"form-control\"\n                [(ngModel)]=\"companyDetails.billingAddress.address2\">\n            </li>\n          </ul>\n\n          <ul class=\"zenwork-margin-bottom-zero\" *ngIf=\"companyDetails.hasOwnProperty('billingAddress')\">\n            <li class=\"state col-md-4\">\n              <input type=\"text\" placeholder=\"City\" class=\"form-control\"\n                [(ngModel)]=\"companyDetails.billingAddress.city\">\n            </li>\n            <li class=\"city col-md-4\">\n              <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                placeholder=\"State\" [(ngModel)]=\"companyDetails.billingAddress.state\">\n                <mat-option *ngFor=\"let data of allStates\" [value]=\"data.name\">\n                  {{data.name}}\n                </mat-option>\n                <!-- <mat-option value=\"washington\">\n                  Washington\n                </mat-option>\n                <mat-option value=\"florida\">\n                  Florida\n                </mat-option> -->\n              </mat-select>\n            </li>\n            <li class=\"zip col-md-4\">\n              <input type=\"text\" placeholder=\"Zip\" class=\"form-control\" (keypress)=\"keyPress($event)\" minlength=\"5\"\n                [(ngModel)]=\"companyDetails.billingAddress.zipcode\">\n            </li>\n\n            <li class=\"col-md-4 united-state\">\n              <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\"\n                placeholder=\"Country\" [(ngModel)]=\"companyDetails.billingAddress.country\">\n                <mat-option value=\"United States\">\n                  USA\n                </mat-option>\n              </mat-select>\n            </li>\n            <div class=\"clearfix\"></div>\n          </ul>\n          <div class=\"clearfix\"></div>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <div class=\"col-xs-12\">\n\n        <button type=\"button\" class=\"btn btn-success zenwork-custom-proceed-btn\" (click)=\"saveEditDetails()\">Save &\n          continue\n        </button>\n        <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\" (click)=\"cancel()\">Cancel</button>\n\n      </div>\n      <div class=\"clearfix\"></div>\n\n    </div>\n\n\n    <div id=\"tab2\" class=\"tab-pane fade\" [ngClass]=\"{ 'active':activeTab==='package','in':activeTab==='package'}\">\n\n      <div class=\"package-billing-main\">\n\n\n        <div class=\"package-billing-history\">\n          Biling History of {{companyNameValue}}\n          <div class=\"setting-table\">\n            <table class=\"table\">\n              <thead>\n                <tr>\n                  <th>\n                    Features/Package\n                  </th>\n                  <th>Add-Ons</th>\n                  <th>Start Date</th>\n                  <th>Price</th>\n                  <th>Status</th>\n                  <th>End Date</th>\n\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let package of billingHistoryPackages\">\n                  <td>{{package.name}}</td>\n                  <td></td>\n                  <td>{{package.startDate | date}}</td>\n                  <td>{{package.price}}</td>\n                  <td>{{package.status}}</td>\n                  <td>{{package.endDate | date}}</td>\n                </tr>\n                <tr *ngFor=\"let adons of billingHistoryAddons\">\n                  <td></td>\n                  <td>{{adons.id.name}}</td>\n                  <td>{{adons.startDate | date}}</td>\n                  <td>{{adons.price}}</td>\n                  <td>{{adons.status}}</td>\n                  <!-- <td *ngIf=\"clientStatus == ''\"> Inactive</td> -->\n                  <td>{{adons.endDate | date}}</td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n\n      </div>\n    </div>\n\n\n    <div id=\"tab3\" class=\"tab-pane fade\" [ngClass]=\"{ 'active':activeTab==='payment','in':activeTab==='payment'}\">\n\n      <div class=\"package-billing-main\">\n\n        <div class=\"col-md-7 discount-block\">\n          <ul class=\"list-unstyled discount-heading\">\n            <li class=\"list-items\">\n              <span [ngClass]=\"{'border-bottom-green':!selectedPackage.wholeSaleDiscount}\">Whole sale discount\n                <br>\n                <small>(Reseller is billed)</small>\n              </span>\n\n            </li>\n            <li class=\"list-items\">\n              <mat-slide-toggle [(ngModel)]=\"selectedPackage.wholeSaleDiscount\"></mat-slide-toggle>\n            </li>\n            <li class=\"list-items\">\n              <span [ngClass]=\"{'border-bottom-green':selectedPackage.wholeSaleDiscount}\">Direct discount\n                <br>\n                <small>(Client is billed)</small>\n              </span>\n\n            </li>\n            <li class=\"list-items\">\n              <button class=\"btn\" (click)=\"addAnotherAddOn()\">\n                <small>\n                  <i aria-hidden=\"true\" class=\"fa fa-plus\"></i>\n                </small> Add-On\n              </button>\n            </li>\n          </ul>\n          <div class=\"package-table\">\n            <table class=\"table\">\n              <thead>\n                <tr>\n                  <th>Package</th>\n                  <th>Add-On\n                    <span class=\"green\">+</span>\n                  </th>\n                  <th>\n                    Discount\n                  </th>\n                  <th>Start Date</th>\n                  <th>End Date</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let package of currentPlanPackage\">\n                  <td>\n                    {{package.name}}\n                  </td>\n                  <td>\n\n                  </td>\n                  <td>\n\n                  </td>\n                  <td>\n                    {{package.startDate | date}}\n                  </td>\n                  <td>\n                    {{package.endDate | date}}\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"package-name\">\n                    <mat-select class=\"form-control zenwork-input package-table-select\"\n                      [(ngModel)]=\"selectedPackage.name\" placeholder=\"Select Package\"\n                      (ngModelChange)=\"getSpecificPackageDetails()\">\n                      <mat-option [value]=\"package.name\" *ngFor=\"let package of packages\">\n                        {{package.name}}\n                      </mat-option>\n                    </mat-select>\n                  </td>\n                  <td>\n\n                  </td>\n                  <td>\n                    <!-- <mat-select class=\"form-control zenwork-input package-table-select\" [disabled]=\"discountEnable\"\n                      [(ngModel)]=\"discountValue\" placeholder=\"Select Package\" (ngModelChange)=\"discountBasePrice()\">\n                      <mat-option [value]=\"discount.value\" *ngFor=\"let discount of dicountArray\">\n                        {{discount.viewValue}}\n                      </mat-option>\n                    </mat-select> -->\n                    <input type=\"text\" (keypress)=\"keyPress($event)\"\n                      class=\"form-control zenwork-input package-table-select\" [disabled]=\"discountEnable\"\n                      [(ngModel)]=\"discountValue\" placeholder=\"discount\" (ngModelChange)=\"discountBasePrice()\">\n                  </td>\n                  <td>\n                    <div class=\"date\">\n                      {{selectedPackage.startDate | date}}\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </td>\n                  <td>\n\n                    <div class=\"date\">\n                      {{selectedPackage.endDate | date}}\n                      <div class=\"clearfix\"></div>\n                    </div>\n\n                  </td>\n                </tr>\n                <tr *ngFor=\"let packageAddon of selectedPackage.addOns\">\n                  <td>\n\n                  </td>\n                  <td class=\"add-on-type\">\n                    <span class=\"add-on-cancel\" (click)=\"addonRemove(packageAddon.selectedAddOn)\">\n                      <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                    </span>\n                    <mat-select class=\"form-control zenwork-input package-table-select add-on-select\"\n                      placeholder=\"Select Add-on\" [(ngModel)]=\"packageAddon.selectedAddOn\"\n                      (ngModelChange)=\"addOnChange(packageAddon.selectedAddOn)\">\n                      <mat-option [value]=\"addOn.name\" *ngFor=\"let addOn of allAddOns\">\n                        {{addOn.name}}\n                      </mat-option>\n                    </mat-select>\n                  </td>\n                  <td>\n\n                  </td>\n                  <td>\n                    {{selectedPackage.startDate | date }}\n                  </td>\n                  <td>\n                    {{selectedPackage.endDate | date}}\n                  </td>\n                </tr>\n                <!-- <tr>\n                  <td>\n\n                  </td>\n                  <td class=\"add-on-type\">\n                    <span class=\"add-on-cancel\">\n                      <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                    </span>\n                    <mat-select class=\"form-control zenwork-input package-table-select add-on-select\" placeholder=\"BioMetric Access/Time Stamp\">\n                      <mat-option value=\"ACA\">\n                        BioMetric Access/Time Stamp\n                      </mat-option>\n                      <mat-option value=\"BioMetric\">\n                        ACA Comliance\n                      </mat-option>\n                      <mat-option value=\"Basic\">\n                        Advanced Training\n                      </mat-option>\n                    </mat-select>\n                  </td>\n                  <td>\n                    12/12/2018\n                  </td>\n                  <td>\n                    12/12/2019\n                  </td>\n                </tr> -->\n              </tbody>\n            </table>\n          </div>\n        </div>\n\n        <!-- <div class=\"payment-rgt col-md-5\">\n\n\n          <div class=\"billing-block\">\n            <h2>\n              <span>Billing</span>\n              <div class=\"annualy-block\">\n                <p>Monthly</p>\n                <mat-slide-toggle (change)=\"toggleDateChange($event)\"></mat-slide-toggle>\n                <small>Annually</small>\n                <div class=\"clearfix\"></div>\n              </div>\n              <div class=\"clearfix\"></div>\n            </h2>\n\n            <ul>\n\n              <li>\n                <div class=\"billing-price\">\n                  <ul>\n                    <li>\n                      <h5>Package</h5>\n                    </li>\n                    <li>\n                      <span>{{selectedPackage.name}}</span>\n                      <small>$ {{selectedPackage.price}}</small>\n\n                      <div class=\"clearfix\"></div>\n                    </li>\n                    <li>\n                      <span>Discount</span>\n                      <small>{{discountValue}}%</small>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                    <li>\n                      <span>Base Price</span>\n                      <small>$ {{basePrice}}</small>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                  </ul>\n                </div>\n              </li>\n\n              <li>\n                <div class=\"billing-price\">\n                  <ul>\n\n                    <li>\n                      <span>Number of Employees</span>\n                      <var>{{addedCompanyData.employeCount}}</var>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                    <li>\n                      <span>Total Employee Cost</span>\n                      <small>$ {{selectedPackage.costPerEmployee * addedCompanyData.employeCount}}</small>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                  </ul>\n                </div>\n              </li>\n\n              <li>\n                <div class=\"billing-price\">\n                  <ul>\n                    <li>\n                      <h5>Add-on</h5>\n                    </li>\n                    <li *ngFor=\"let adon of selectedPackage.addOns\">\n                      <span>{{adon.selectedAddOn}}</span>\n                      <small>{{adon.price}}</small>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                  </ul>\n                </div>\n              </li>\n\n              <li class=\"no-bor\">\n                <div class=\"billing-price\">\n                  <ul>\n                    <li>\n                      <span>Total Price</span>\n                      <small>$ {{selectedPackage.totalPrice}}</small>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                    <li>\n                      <span>Tax Rate @ 8%</span>\n                      <small>$ {{(selectedPackage.totalPrice * 0.08).toFixed(2)}}</small>\n                      <div class=\"clearfix\"></div>\n                    </li>\n                  </ul>\n                </div>\n              </li>\n\n            </ul>\n\n            <h4>\n              <h1>Grand Price</h1>\n              <p>$ {{ selectedPackage.totalPrice + selectedPackage.totalPrice*0.08}}</p>\n              <div class=\"clearfix\"></div>\n            </h4>\n\n          </div>\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox2\" type=\"checkbox\">\n              <label for=\"checkbox2\">Make me the\n                <small> Master Admin</small> of this client's company.</label>\n            </div>\n          </div>\n\n          \n          <div class=\"clearfix\"></div>\n        </div> -->\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"col-xs-12\">\n\n        <button type=\"button\" class=\"btn btn-success zenwork-custom-proceed-btn\" (click)=\"savePackageDetails()\">Save &\n          continue\n        </button>\n        <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\" (click)=\"cancel()\">Cancel</button>\n\n      </div>\n      <div class=\"clearfix\"></div>\n\n\n\n    </div>\n\n\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ResellerEditClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResellerEditClientComponent", function() { return ResellerEditClientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_company_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/company.service */ "./src/app/services/company.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/purchasePackage.service */ "./src/app/services/purchasePackage.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/chardgeCreditCard.service */ "./src/app/services/chardgeCreditCard.service.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ResellerEditClientComponent = /** @class */ (function () {
    function ResellerEditClientComponent(companyService, swalAlertService, router, activatedRoute, packageAndAddOnService, purchasePackageService, accessLocalStorageService, creditCardChargingService, companySettingsService) {
        this.companyService = companyService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.packageAndAddOnService = packageAndAddOnService;
        this.purchasePackageService = purchasePackageService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.creditCardChargingService = creditCardChargingService;
        this.companySettingsService = companySettingsService;
        this.activeTab = 'create';
        this.isValid = false;
        this.selectedPackage = {
            clientId: '',
            name: '',
            wholeSaleDiscount: false,
            discount: 0,
            costPerEmployee: 0,
            price: 0,
            startDate: new Date(),
            endDate: new Date(),
            addOns: [],
            packages: [],
            totalPrice: 0,
            billingFrequency: 'monthly'
        };
        this.selectedPackageData = {};
        this.addedCompanyData = {};
        this.billingHistory = {};
        this.currentPlan = {};
        this.discountEnable = true;
        this.validity = false;
        this.billingHistoryAddons = [];
        this.billingHistoryPackages = [];
        this.currentPlanAddons = [];
        this.currentPlanPackage = [];
        this.resellersList = [];
        this.dicountArray = [
            { value: 10, viewValue: '10%' },
            { value: 20, viewValue: '20%' },
            { value: 30, viewValue: '30%' },
            { value: 40, viewValue: '40%' },
            { value: 50, viewValue: '50%' },
            { value: 60, viewValue: '60%' },
            { value: 70, viewValue: '70%' },
            { value: 80, viewValue: '80%' },
            { value: 90, viewValue: '90%' },
        ];
        this.allStates = [];
        this.cardDetails = {
            cardNumber: '',
            expMonth: '',
            expYear: '',
            cvv: '',
            cardName: ''
        };
    }
    ResellerEditClientComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.discountValue = 0;
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: '',
            email: '',
            type: '',
            belongsToReseller: '',
            reseller: '',
            primaryContact: { name: '', phone: '', extention: '' },
            billingContact: { name: '', email: '', phone: '', extention: '' },
            address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            isPrimaryContactIsBillingContact: true,
            isPrimaryAddressIsBillingAddress: true
        };
        this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        if (this.addedCompanyData == null) {
            this.addedCompanyData = {};
        }
        this.activatedRoute.params.subscribe(function (params) {
            _this.id = params['id'];
            console.log("iddddd", _this.id, params);
        });
        this.getSingleUser();
        this.getAllPackages();
        this.getAllAddons();
        this.getSingleCompanyBillingHistory();
        this.getSingleComapnyCurrentPlan();
        this.getAllresellers();
        this.getAllUSstates();
    };
    ResellerEditClientComponent.prototype.getAllUSstates = function () {
        var _this = this;
        this.companySettingsService.getStuctureFields('State', 0)
            .subscribe(function (res) {
            console.log(res);
            if (res.custom.length > 0) {
                res.custom.forEach(function (element) {
                    _this.allStates.push(element);
                    console.log("22222111111", _this.allStates);
                });
            }
            if (res.default.length > 0) {
                res.default.forEach(function (element) {
                    _this.allStates.push(element);
                });
                console.log("22222", _this.allStates);
            }
        }, function (err) {
            console.log(err);
        });
    };
    ResellerEditClientComponent.prototype.getSingleUser = function () {
        var _this = this;
        var comapnyData = this.accessLocalStorageService.get('editCompany');
        this.id = comapnyData._id;
        this.companyService.editClient(this.id)
            .subscribe(function (res) {
            console.log("editres", res);
            _this.companyDetails = res.data;
            if (_this.companyDetails.type == 'reseller-client') {
                var x = _this.companyDetails.type;
                _this.companyDetails.type = 'company';
                _this.companyDetails.belongsToReseller = true;
            }
            if (_this.companyDetails.type == "company" && x != 'reseller-client') {
                _this.companyDetails.type = 'company';
                _this.companyDetails.belongsToReseller = false;
            }
            _this.companyDetails.primaryContact = res.data.primaryContact;
            _this.companyDetails.billingContact = res.data.billingAddress;
            _this.companyDetails.address = res.data.primaryAddress;
            _this.companyDetails.billingAddress = res.data.billingAddress;
            if (res.data.orders) {
                _this.comapnyInactiveDate = res.order.endDate;
                var date = new Date(_this.comapnyInactiveDate);
                console.log(date);
                _this.comapnyInactiveDate = date.toLocaleDateString();
                console.log(_this.comapnyInactiveDate);
                var today = new Date().toLocaleDateString();
                if (today < _this.comapnyInactiveDate) {
                    _this.clientStatus = "Active";
                }
                else {
                    _this.clientStatus = "Inactive";
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ResellerEditClientComponent.prototype.getSingleCompanyBillingHistory = function () {
        var _this = this;
        this.addedCompanyData = this.accessLocalStorageService.get('editCompany');
        console.log(this.addedCompanyData);
        this.companyNameValue = this.addedCompanyData.companyId.name;
        console.log(this.companyNameValue);
        this.id = this.addedCompanyData.companyId._id;
        this.companyService.getBillingHistory(this.id)
            .subscribe(function (res) {
            console.log("billinghistory", res);
            if (res.data) {
                _this.billingHistory = res.data;
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].adons.length >= 1) {
                        if (res.data[i].adons.length == 1) {
                            _this.billingHistoryAddons.push(res.data[i].adons);
                        }
                        if (res.data[i].adons.length > 1) {
                            console.log(res.data[i].adons);
                            for (var j = 0; j < res.data[i].adons.length; j++) {
                                _this.billingHistoryAddons.push([res.data[i].adons[j]]);
                            }
                        }
                    }
                    if (res.data[i].packages.length >= 1) {
                        _this.billingHistoryPackages.push(res.data[i].packages[0]);
                    }
                    console.log("adins,packages", _this.billingHistoryAddons, _this.billingHistoryPackages);
                }
            }
            else {
                _this.billingHistoryPackages = [];
                _this.billingHistoryAddons = [];
            }
        }, function (err) {
        });
    };
    ResellerEditClientComponent.prototype.saveEditDetails = function () {
        var _this = this;
        var comapnyData = this.accessLocalStorageService.get('editCompany');
        this.isValid = true;
        this.id = comapnyData.companyId._id;
        console.log(this.id);
        console.log("edit and update data", this.companyDetails);
        if (this.companyDetails.name && this.companyDetails.tradeName && this.companyDetails.employeCount &&
            this.companyDetails.type && this.companyDetails.primaryContact.name && this.companyDetails.primaryContact.phone &&
            this.companyDetails.email && this.companyDetails.primaryContact.extention && this.companyDetails.address.address1 &&
            this.companyDetails.address.address2 && this.companyDetails.address.city && this.companyDetails.address.state &&
            this.companyDetails.address.zipcode && this.companyDetails.address.country) {
            this.companyService.editDataSave(this.companyDetails, this.id)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.activeTab = 'package';
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Client Details", res.message, 'success');
                }
            }, function (err) {
            });
        }
    };
    ResellerEditClientComponent.prototype.getSingleComapnyCurrentPlan = function () {
        var _this = this;
        var comapnyData = this.accessLocalStorageService.get('editCompany');
        console.log(comapnyData);
        this.id = comapnyData.companyId._id;
        console.log(this.id);
        this.companyService.currentPlan(this.id)
            .subscribe(function (res) {
            console.log("currentPlan", res);
            _this.currentPlan = res.data;
            if (_this.currentPlan) {
                _this.currentPlanAddons = res.data.adons;
                _this.currentPlanPackage = res.data.packages;
            }
        }, function (err) {
        });
    };
    ResellerEditClientComponent.prototype.discountBasePrice = function () {
        console.log(this.discountValue);
        if (this.discountValue <= 100) {
            var x = (this.tempforDiscountChange * this.discountValue / 100);
            this.selectedPackage.totalPrice = this.tempforDiscountChange - x;
            // this.getSpecificPackageDetails();
        }
        else {
            this.swalAlertService.SweetAlertWithoutConfirmation("Error", "Discount is more than totalPrice.Please proceed lessthan totalprice", 'error');
            this.discountValue = '';
            this.getSpecificPackageDetails();
            this.addOnChange();
        }
    };
    ResellerEditClientComponent.prototype.addOnChange = function () {
        // console.log(addOn);
        var _this = this;
        console.log("before", this.selectedPackage.addOns);
        // for(let ref of this.selectedPackage.addOns){
        //   this.selectedPackage.addOns.push(this.allAddOns.filter(data=>data.selectedAddOn==ref.selectedAddOn))
        // }
        // console.log("after",this.selectedPackage.addOns);
        this.selectedPackage.totalPrice = 0;
        var _loop_1 = function (i) {
            this_1.selectedPackage.addOns.forEach(function (addon) {
                console.log(addon, _this.allAddOns[i]);
                if (addon.selectedAddOn == _this.allAddOns[i].name) {
                    addon.price = _this.allAddOns[i].price;
                    addon.id = _this.allAddOns[i]._id;
                    console.log(addon.id);
                    _this.selectedAddons = _this.selectedPackage.addOns;
                    _this.selectedAddons['id'] = addon.id;
                    console.log(_this.selectedAddons);
                }
            });
        };
        var this_1 = this;
        for (var i = 0; i < this.allAddOns.length; i++) {
            _loop_1(i);
        }
        var price = 0;
        this.selectedPackage.addOns.forEach(function (data) {
            price += data.price;
        });
        console.log(price);
        this.selectedPackage.totalPrice = this.dummy + price;
        this.tempforDiscountChange = this.selectedPackage.totalPrice;
        this.discountBasePrice();
    };
    ResellerEditClientComponent.prototype.toggleDateChange = function (event) {
        console.log(event.checked);
        if (event.checked == true) {
            this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 12));
            this.selectedPackage.billingFrequency = 'annually';
            this.validity = true;
        }
        else {
            this.selectedPackage.endDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
            this.validity = false;
            this.selectedPackage.billingFrequency = 'monthly';
        }
        this.getSpecificPackageDetails();
    };
    ResellerEditClientComponent.prototype.chargeCreditCard = function () {
        var _this = this;
        window.Stripe.card.createToken({
            number: this.cardDetails.cardNumber,
            exp_month: this.cardDetails.expMonth,
            exp_year: this.cardDetails.expYear,
            cvc: this.cardDetails.cvv
        }, function (status, response) {
            if (status === 200) {
                var token = response.id;
                console.log(token);
                // this.chargeCard(token);
                var id = JSON.parse(localStorage.getItem("addedCompany"));
                console.log(id);
                var amount = _this.selectedPackage.totalPrice + _this.selectedPackage.totalPrice * 0.08;
                _this.creditCardChargingService.createCharge(token, id._id, amount, _this.orderId)
                    .subscribe(function (response) {
                    console.log(response);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Success", response.message, 'success');
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                console.log(response.error.message);
            }
            _this.cardDetails.cardNumber = '';
            _this.cardDetails.expMonth = '';
            _this.cardDetails.expYear = '';
            _this.cardDetails.cvv = '';
        });
    };
    ResellerEditClientComponent.prototype.editCompany = function () {
        var _this = this;
        this.companyService.createCompany(this.companyDetails)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.addedCompanyData = res.data;
                if (_this.addedCompanyData == null) {
                    _this.addedCompanyData = {};
                }
                _this.accessLocalStorageService.set('addedCompany', _this.addedCompanyData);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                _this.companyDetails = {
                    name: '',
                    tradeName: '',
                    employeCount: '',
                    email: '',
                    type: '',
                    belongsToReseller: '',
                    reseller: '',
                    primaryContact: { name: '', phone: '', extention: '' },
                    billingContact: { name: '', email: '', phone: '', extention: '' },
                    address: {
                        address1: '',
                        address2: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        country: ''
                    },
                    billingAddress: {
                        address1: '',
                        address2: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        country: ''
                    },
                    isPrimaryContactIsBillingContact: true,
                    isPrimaryAddressIsBillingAddress: true
                };
                _this.selectedPackage.clientId = _this.addedCompanyData._id;
                _this.activeTab = 'package';
                // this.router.navigate(['/super-admin/super-admin-dashboard/clients']);
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Error", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerEditClientComponent.prototype.cancel = function () {
        this.companyDetails = {
            name: '',
            tradeName: '',
            employeCount: '',
            email: '',
            type: '',
            belongsToReseller: '',
            reseller: '',
            primaryContact: { name: '', phone: '', extention: '' },
            billingContact: { name: '', email: '', phone: '', extention: '' },
            address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            billingAddress: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
            isPrimaryContactIsBillingContact: true,
            isPrimaryAddressIsBillingAddress: true
        };
    };
    ResellerEditClientComponent.prototype.getAllPackages = function () {
        var _this = this;
        this.packageAndAddOnService.getAllPackages()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.packages = res.data;
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerEditClientComponent.prototype.getSpecificPackageDetails = function () {
        var _this = this;
        // this.addedCompanyData = this.accessLocalStorageService.get('addedCompany');
        console.log(this.selectedPackage.name, this.packages);
        this.packages.forEach(function (packageData) {
            if (packageData.name == _this.selectedPackage.name) {
                _this.selectedPackage.packages = _this.packages.filter(function (data) { return data.name == _this.selectedPackage.name; });
                console.log(_this.selectedPackage.packages);
                if (_this.validity == false) {
                    _this.selectedPackage.price = packageData.price;
                }
                else {
                    _this.selectedPackage.price = (packageData.price * 12);
                }
                _this.selectedPackage.costPerEmployee = packageData.costPerEmployee;
            }
        });
        this.selectedPackage.totalPrice = this.selectedPackage.price + this.selectedPackage.costPerEmployee * this.companyDetails.employeCount;
        // if (this.basePriceDiscount > 0) {
        //   this.selectedPackage.totalPrice = this.selectedPackage.totalPrice - this.basePriceDiscount;
        // }
        this.discountEnable = false;
        /* this.selectedPackageData = packageData; */
        /* this.packageAndAddOnService.getSpecificPackageDetails(packageData)
          .subscribe(
            (res: any) => {
              console.log("Response", res);
              if (res.status) {
                this.selectedPackageData = res.data;
              } else {
                this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error')
              }
            },
            (err: HttpErrorResponse) => {
              console.log('Err', err)
              this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error')
            }
          ) */
        this.dummy = this.selectedPackage.totalPrice;
        this.tempforDiscountChange = this.selectedPackage.totalPrice;
    };
    ResellerEditClientComponent.prototype.getAllAddons = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                console.log(_this.allAddOns);
                _this.allAddOns.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerEditClientComponent.prototype.selectedReseller = function (id) {
        console.log(id);
    };
    ResellerEditClientComponent.prototype.getAllresellers = function () {
        var _this = this;
        this.companyService.getresellers()
            .subscribe(function (res) {
            console.log("reselerrsList", res);
            _this.resellersList = res.data;
        }, function (err) {
        });
    };
    ResellerEditClientComponent.prototype.getAllAddOnDetails = function () {
        var _this = this;
        this.packageAndAddOnService.getAllAddOns()
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.allAddOns = res.data;
                _this.allAddOns.forEach(function (addon) {
                    addon.isSelected = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    ResellerEditClientComponent.prototype.addAnotherAddOn = function () {
        this.selectedPackage.addOns.push({
            id: '',
            selectedAddOn: '',
            price: 0,
            index: this.selectedPackage.addOns.length + 1
        });
    };
    ResellerEditClientComponent.prototype.addonRemove = function (addon) {
        var _this = this;
        // if (addon == '') {
        //   this.selectedPackage.addOns = []
        // }
        console.log("addon", addon);
        // for (let i = 0; i < this.allAddOns.length; i++) {
        //   this.selectedPackage.addOns.forEach(addon => {
        //     console.log(addon, this.allAddOns[i])
        //     if (addon == this.allAddOns[i].name) {
        //    }
        //   });
        // }
        this.selectedPackage.addOns.forEach(function (data) {
            if (data.selectedAddOn == addon) {
                _this.selectedPackage.totalPrice -= data.price;
            }
        });
        this.selectedPackage.addOns = this.selectedPackage.addOns.filter(function (data) { return addon != data.selectedAddOn; });
        console.log(this.selectedPackage.addOns);
    };
    ResellerEditClientComponent.prototype.savePackageDetails = function () {
        var _this = this;
        console.log(postData);
        console.log("this.selectedPackage", this.selectedPackage);
        var id = localStorage.getItem('resellercmpnyID');
        var cmpnyId = JSON.parse(localStorage.getItem('editCompany'));
        console.log(cmpnyId);
        this.selectedPackage.clientId = cmpnyId.companyId._id;
        console.log("this.selectedPackage", this.selectedPackage);
        this.invoiceadons = this.selectedPackage.addOns;
        var postData = this.selectedPackage;
        delete postData.addOns;
        postData['adons'] = this.invoiceadons;
        this.purchasePackageService.purchasePackage(postData)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status) {
                _this.orderId = res.data.id;
                _this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'success');
                _this.router.navigate(['super-admin/super-admin-dashboard/reseller/' + id]);
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Purchase Package", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message._message, 'error');
        });
    };
    ResellerEditClientComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    ResellerEditClientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reseller-edit-client',
            template: __webpack_require__(/*! ./reseller-edit-client.component.html */ "./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.html"),
            styles: [__webpack_require__(/*! ./reseller-edit-client.component.css */ "./src/app/reseller-dashboard/reseller-edit-client/reseller-edit-client.component.css")]
        }),
        __metadata("design:paramtypes", [_services_company_service__WEBPACK_IMPORTED_MODULE_1__["CompanyService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_4__["PackageAndAddOnService"],
            _services_purchasePackage_service__WEBPACK_IMPORTED_MODULE_5__["PurchasePackageService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__["AccessLocalStorageService"],
            _services_chardgeCreditCard_service__WEBPACK_IMPORTED_MODULE_7__["CreditCardChargingService"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_8__["CompanySettingsService"]])
    ], ResellerEditClientComponent);
    return ResellerEditClientComponent;
}());



/***/ })

}]);
//# sourceMappingURL=reseller-dashboard-reseller-dashboard-module.js.map