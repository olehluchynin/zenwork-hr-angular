(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["employee-management-employee-management-module"],{

/***/ "./src/app/admin-dashboard/employee-management/employee-management.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-management.module.ts ***!
  \***********************************************************************************/
/*! exports provided: EmployeeManagementModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeManagementModule", function() { return EmployeeManagementModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _employee_management_employee_management_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-management/employee-management.component */ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.ts");
/* harmony import */ var _employee_management_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-management.routing */ "./src/app/admin-dashboard/employee-management/employee-management.routing.ts");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/accordion */ "./node_modules/ngx-bootstrap/accordion/fesm5/ngx-bootstrap-accordion.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm5/button-toggle.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-mask */ "./node_modules/ngx-mask/fesm5/ngx-mask.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { FormsModule } from '@angular/forms';



// import { DirectoryComponent } from './directory/directory.component';
// import { OnBoardingComponent } from './on-boarding/on-boarding.component';
// import { ManagerSelfServiceComponent } from './manager-self-service/manager-self-service.component';
// import { EmployeeSelfServiceComponent } from './employee-self-service/employee-self-service.component';
// import { NewHireHrComponent } from './new-hire-hr/new-hire-hr.component';
// import { NewHireHrTimeComponent } from './new-hire-hr-time/new-hire-hr-time.component';
// import { NewHireWizardComponent } from './new-hire-wizard/new-hire-wizard.component';
var EmployeeManagementModule = /** @class */ (function () {
    function EmployeeManagementModule() {
    }
    EmployeeManagementModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _employee_management_routing__WEBPACK_IMPORTED_MODULE_3__["employeeManagementRouting"],
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_4__["TabsModule"].forRoot(),
                ngx_bootstrap_accordion__WEBPACK_IMPORTED_MODULE_5__["AccordionModule"].forRoot(),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_6__["BsDatepickerModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__["ModalModule"].forRoot(),
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_8__["MatSlideToggleModule"],
                _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_9__["MatButtonToggleModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                ngx_mask__WEBPACK_IMPORTED_MODULE_12__["NgxMaskModule"].forRoot({
                    showMaskTyped: true
                })
                // FormsModule
            ],
            declarations: [_employee_management_employee_management_component__WEBPACK_IMPORTED_MODULE_2__["EmployeeManagementComponent"],
            ],
            providers: [
                _services_my_info_service__WEBPACK_IMPORTED_MODULE_11__["MyInfoService"]
            ]
        })
    ], EmployeeManagementModule);
    return EmployeeManagementModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-management.routing.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-management.routing.ts ***!
  \************************************************************************************/
/*! exports provided: employeeManagementRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employeeManagementRouting", function() { return employeeManagementRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _employee_management_employee_management_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-management/employee-management.component */ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { DirectoryComponent } from './directory/directory.component';
// import { OnBoardingComponent } from './on-boarding/on-boarding.component';
// import { ManagerSelfServiceComponent } from './manager-self-service/manager-self-service.component';
// import { EmployeeSelfServiceComponent } from './employee-self-service/employee-self-service.component';
// import { NewHireHrComponent } from './new-hire-hr/new-hire-hr.component';
// import { NewHireHrTimeComponent } from './new-hire-hr-time/new-hire-hr-time.component';
// import { NewHireWizardComponent } from './new-hire-wizard/new-hire-wizard.component';
var routes = [
    { path: '', component: _employee_management_employee_management_component__WEBPACK_IMPORTED_MODULE_2__["EmployeeManagementComponent"] },
    { path: 'directory', loadChildren: "./directory/directory.module#DirectoryModule" },
    { path: 'profile', loadChildren: "./employee-profile-view/employee-profile-view.module#EmployeeProfileViewModule" },
    { path: 'off-boarding-landing', loadChildren: "./off-boarding-landing/off-boarding-landing.module#OffBoardingLandingModule" },
    { path: 'on-boarding', loadChildren: "./on-boarding/on-boarding.module#OnBoardingModule" },
    { path: 'new-hire-hr', loadChildren: "./new-hire-hr/new-hire-hr.module#NewHireHrModule" },
    { path: 'new-hire-hr-time', loadChildren: "./new-hire-hr-time/new-hire-hr-time.module#NewHireHrTimeModule" },
    { path: 'new-hire-wizard', loadChildren: "./new-hire-wizard/new-hire-wizard.module#NewHireWizardModule" },
    { path: 'manager-self-service', loadChildren: "./manager-self-service/manager-self-service.module#ManagerSelfServiceModule" },
    { path: 'employee-self-service', loadChildren: "./employee-self-service/employee-self-service.module#EmployeeSelfServiceModule" },
];
var employeeManagementRouting = /** @class */ (function () {
    function employeeManagementRouting() {
    }
    employeeManagementRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], employeeManagementRouting);
    return employeeManagementRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.css":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.css ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".employee-management-landing{\n    padding: 0 0px 0 0;\n}\n.zenwork-jumbotron-bg{\n    background-color: #fff;\n    box-shadow: -1px 1px 37px -5px rgba(0,0,0,0.37);\n    float: none;\n    margin: 0 auto;\n}\n.jumbotron{\n    padding-top: 55px;\n    padding-bottom: 55px;\n    /* margin: 0 30px; */\n}\n.zenwork-currentpage{\n    padding-top: 40px;\n}\n.zenwork-currentpage span{\n    padding: 0 0 0 10px;\n    font-size: 16px; vertical-align: middle;\n}\n.main-employee{\n    width: 30px\n}\n.margin-bottom-20{\n    margin: 0 0 20px 0;\n}\n.margin-top-20{\n    margin: 20px 0 0 0;\n}\n.zenwork-custom-company-settings{\n    padding-top: 60px;\n    padding-bottom: 40px;\n    border-bottom: 1px solid #e4eae7;\n\n}\n.zenwork-custom-company-settings a{\n    color : #3e3e3ea3;\n    text-decoration: none;\n}\n.jumbotron p{\n    font-size: 18px !important;\n    color: #3f3e3f;\n    font-weight: normal !important; padding: 0 0 5px;\n}\n.settings-icon{\n    width: 40px;\n}\n.zenwork-settings-wrapper{\n    padding: 20px; \n}\nsmall{\n    font-size: 12px;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n/* css :starts */\n.employee-details{\n    margin:40px 0 0 0;\n    border-bottom: 1px solid #e4eae7;\n    padding: 0px 0 20px;\n    float: none;\n    margin: 40px auto;\n}\n.content-total-employ>.number-employees > span{\n    color:#c0c1c0;\n}\n.img-employee{\n    /* width: 85px;\n    float: left; */\n    display: inline-block;\n    vertical-align: middle;\n}\n.total-employee{\n    margin: 0 auto;\n}\n.tab-content{\n    padding: 40px 20px;\n    background: #fff;\n}\n.nav-tabs>li>a {\n    border:none! important;\n}\n.content-total-employee{\n    width: 207px;\n    display: inline-block;\n    vertical-align: middle;\n}\n.content-of-employee{\n    /* margin: 0 30px; */\n}\n.content-total-employee .number-employees{\n    color:#3bafba;\n    font-weight: 600;\n    font-size: 35px;\n}\n.report-table{\n    /* border: 1px solid #bebebe; */\n    background: #fff;   \n}\n.table-heading{\n    padding: 5px 5px 5px 0px;\n    /* border-bottom: 1px solid #bebebe; */\n    background-color:#edf7fe;\n}\n.today-report{\n    margin:20px auto;\n    float: none;\n}\n.table-data{\n    padding-top:40px;\n    background: #fff;\n}\n.list-items-carousel{\n    display: inline-block;\n    padding: 0px 35px;  \n}\n.list-images{\n    margin: 0 auto;\n}\n.employees-content{\n    font-size:14px;\n    display: inline-block;\n}\n.employees-content span{\n    display: inline-block;\n    vertical-align: middle;\n}\n.employe-reports{\n    text-align: center;\n    margin:0;\n    position: relative;\n\n}\n.employe-reports .fa-caret-left{\n    position: absolute;\n    top: 20px;\n    left: 25px\n}\n.employe-reports .fa-caret-right{\n    position: absolute;\n    top: 20px;\n    right: 50px;\n}\n.working{\n    /* padding-left: 12px; */\n}\n.medical-leave{\n    padding-left:30px;\n}\n.employees-number{\n    font-weight: 400;\n    font-size: 30px;\n    color: #bebebe;\n}\n.border-top{\n    border-top: 1px solid #e4eae7;\n}\n.border-right{\n    border-right: 1px solid #e4eae7;\n}\n.custom-height-mss{\n    height: 157px;\n}\n.name-dropdown{\n    width: 75px;\n    height: 30px;\n    font-size: 14px;\n    padding: 6px 12px 6px 3px;\n    border: none;\n    display: inline-block;\n    border-radius: 3px;\n    background-color: #fff;\n    box-shadow: none;   \n}\n.filter-list .list-items{\n    display: inline-block;\n    padding: 0px 5px;\n}\n.filter-list .list-items label{\n    display: inline-block;\n    font-weight: normal;\n    font-size: 14px;\n    padding: 0px 15px;\n}\n.heading{\n    color:#3f4143;\n}\n.filter-list {\n    margin: 0;\n}\n.calender-icon{\n    width: 15px;\n}\n.onboarding-tabs li a:active,.onboarding-tabs li a:visited,.onboarding-tabs li a:focus{\n    /* border: 1px solid transparent!important; */\n    border-top: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n}\n.onboarding-tabs>li.active>a, .onboarding-tabs>li.active>a:focus, .onboarding-tabs>li.active>a:hover{\n    /* border: 1px solid transparent!important; */\n    border-top: 4px solid #439348!important;\n    border-radius: 0px!important;\n    color: #353131!important;\n    background: #f8f8f8;\n}\n.onboarding-tabs{\n    border-bottom: 0;\n}\n.onboarding-tabs>li>a{\n    margin:0 30px;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"employee-management-landing\">\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <img src=\"../../../assets/images/employee-management/employee.png\" alt=\"employee icon\" class=\"main-employee \">\n      <span>Employee Management</span>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n  <div class=\"zenwork-custom-company-settings\">\n    <div class=\"jumbotron zenwork-jumbotron-bg col-md-11\">\n\n    <div class=\"col-xs-4 text-center border-right margin-bottom-20\">\n        <div class=\"zenwork-settings-wrapper\" style=\"cursor: pointer;\">\n          <a (click) = \"empManagement()\" >\n            <img src=\"../../../assets/images/employee-management/onboard.png\" alt=\"on-boarding icon\" class=\"center-block\">\n            <div class=\"margin-top-20\">\n              <p class=\"zenwork-margin-zero\">\n                Onboarding\n              </p>\n              <small>Hire new employees, Onboard templates, <br> New Hire Wizard</small>\n            </div>\n          </a>\n        </div>\n      </div>\n\n\n      <div class=\"col-xs-4 text-center border-right margin-bottom-20\">\n        <div class=\"zenwork-settings-wrapper\">\n          <a [routerLink]=\"['/admin/admin-dashboard/employee-management/directory/directory']\">\n            <img src=\"../../../assets/images/employee-management/directory.png\" alt=\"Directory icon\" class=\" center-block\">\n            <div class=\"margin-top-20\">\n              <p class=\"zenwork-margin-zero\">\n                Directory\n              </p>\n              <small>Search for coworkers and <br> mass updates</small>\n            </div>\n          </a>\n        </div>\n      </div>\n     \n      <div class=\"col-xs-4 text-center margin-bottom-20\">\n        <div class=\"zenwork-settings-wrapper\">\n          <a [routerLink]=\"['/admin/admin-dashboard/employee-management/off-boarding-landing/off-boarding-landing']\">\n            <img src=\"../../../assets/images/employee-management/off-board.png\" alt=\"off-boarding icon\" class=\"center-block\">\n            <div class=\"margin-top-20\">\n              <p class=\"zenwork-margin-zero\">\n                Offboarding\n              </p>\n              <small>Terminate employees,<br> Offboard Templates</small>\n            </div>\n          </a>\n        </div>\n      </div>\n      <div class=\"clearfix\"></div>\n      <div class=\"border-top\">\n        <div class=\"col-xs-6 text-center border-right margin-top-20\">\n          <div class=\"zenwork-settings-wrapper\">\n            <a [routerLink]=\"['/admin/admin-dashboard/employee-management/manager-self-service']\">\n              <img src=\"../../../assets/images/employee-management/ms-service.png\" alt=\"manager-self icon\" class=\" center-block\">\n              <div class=\"margin-top-20\">\n                <p class=\"zenwork-margin-zero\">\n                  <span class=\"heading\"> Manager self Service</span>\n                </p>\n                <small>Manager Reports, Approvals <br> and Requests</small>\n              </div>\n            </a>\n          </div>\n        </div>\n        <div class=\"col-xs-6 text-center custom-height-mss margin-top-20\">\n          <div class=\"zenwork-settings-wrapper\">\n            <a [routerLink]=\"['/admin/admin-dashboard/employee-management/employee-self-service/employee-self-service']\">\n              <img src=\"../../../assets/images/employee-management/em-service.png\" alt=\"employee-self icon\" class=\"center-block\">\n              <div class=\"margin-top-20\">\n                <p class=\"zenwork-margin-zero\">\n                  Employee Self Service\n                </p>\n                <small>\n                  Employee Frontpage\n                </small>\n              </div>\n            </a>\n          </div>\n        </div>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n\n  <div class=\"employee-details col-md-11\">\n\n    <div class=\"tab-content\">\n\n      <div id=\"total\" class=\"tab-pane fade in active employee-display text-center content-of-employee\">\n        <div class=\"img-employee\">\n          <img src=\"../../../assets/images/employee-management/employee-detail.png\" alt=\"employee-total icon\" class=\"img-responsive total-employee\">\n        </div>\n        <div class=\"content-total-employee\">\n          <span class=\"number-employees\">1334</span><br>\n          <span>Total Number of Employees</span>\n        </div>\n      </div>\n\n      <div id=\"employeeStatus\" class=\"tab-pane fade\">\n\n      </div>\n      <div id=\"location\" class=\"tab-pane fade\">\n\n      </div>\n      <div id=\"employeeType\" class=\"tab-pane fade\">\n      </div>\n      <div id=\"department\" class=\"tab-pane fade\">\n\n      </div>\n    </div>\n\n    <ul class=\"nav nav-tabs onboarding-tabs\">\n      <li class=\"active\"><a data-toggle=\"tab\" href=\"#total\">Total Number of Employees</a></li>\n      <li><a data-toggle=\"tab\" href=\"#employeeStatus\">Employee Status</a></li>\n      <li><a data-toggle=\"tab\" href=\"#location\">Location</a></li>\n      <li><a data-toggle=\"tab\" href=\"#employeeType\">Employee Type</a></li>\n      <li><a data-toggle=\"tab\" href=\"#department\">Department</a></li>\n    </ul>\n  </div>\n  <div class=\"today-report col-md-11\">\n    <h4 class=\"employees-content green\">\n      <span class=\"calender-icon\">\n        <img src=\"../../../assets/images/employee-management/event_icon.png\" alt=\"calender-icon\" class=\"img-responsive\">\n      </span>&nbsp;\n      <span class=\"today-report-text\">Today's Report</span>\n\n    </h4>\n    <div class=\"report-table\">\n      <div class=\"table-heading\">\n        <div class=\"main-filter pull-left\">\n          <ul class=\"list-unstyled filter-list\">\n\n            <li class=\"list-items\">\n              <label>Location</label>\n              <select class=\"form-control name-dropdown\">\n                <option>All</option>\n                <option>No</option>\n              </select>\n            </li>\n            <li class=\"list-items\">\n              <label>Department</label>\n              <select class=\"form-control name-dropdown\">\n                <option>All</option>\n                <option>No</option>\n              </select>\n            </li>\n            <li class=\"list-items\">\n              <label>Shifts</label>\n              <select class=\"form-control name-dropdown\">\n                <option>All</option>\n                <option>No</option>\n              </select>\n            </li>\n          </ul>\n\n        </div>\n        <div class=\"main-filter pull-right\">\n          <ul class=\"list-unstyled filter-list\">\n            <li class=\"list-items\">\n              <select class=\"form-control name-dropdown\">\n                <option>All</option>\n                <option>No</option>\n              </select>\n            </li>\n            <li class=\"list-items\">\n              <select class=\"form-control name-dropdown\">\n                <option>All</option>\n                <option>No</option>\n              </select>\n            </li>\n\n          </ul>\n\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <!-- <ul class=\"employe-reports list-unstyled \">\n          <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>\n        <li class=\"lisy-items\">\n          <img src=\"../../../assets/images/employee-management/Artboard-1_1_.png\" alt=\"employee-total icon\" class=\"img-responsive list-images\">\n          <h4 class=\"employees-content working\"> Working </h4>\n          <p class=\"employees-number\">1218</p>\n        </li>\n        <li class=\"lisy-items\">\n          <img src=\"../../../assets/images/employee-management/car1.png\" alt=\"car icon\" class=\"img-responsive list-images\">\n          <h4 class=\"employees-content\"> On Leave </h4>\n          <p class=\"employees-number\">65</p>\n        </li>\n        <li class=\"lisy-items\">\n          <img src=\"../../../assets/images/employee-management/watch.png\" alt=\"early-birds icon\" class=\"img-responsive list-images\">\n          <h4 class=\"employees-content\"> Early Birds </h4>\n          <p class=\"employees-number\">245</p>\n        </li>\n        <li class=\"lisy-items\">\n          <img src=\"../../../assets/images/employee-management/Path 19.png\" alt=\"late-arriavals icon\" class=\"img-responsive list-images\">\n          <h4 class=\"employees-content\"> Late Arrivals </h4>\n          <p class=\"employees-number\">213</p>\n        </li>\n        <li class=\"lisy-items\">\n          <img src=\"../../../assets/images/employee-management/wfh2.png\" alt=\"wfh-outdoor icon\" class=\"img-responsive list-images\">\n          <h4 class=\"employees-content\"> WFH/outdoor </h4>\n          <p class=\"employees-number\">165</p>\n        </li>\n        <li class=\"lisy-items\">\n          <img src=\"../../../assets/images/employee-management/Path 20.png\" alt=\"Medical icon\" class=\"img-responsive list-images\">\n          <h4 class=\"employees-content\"> On Medical Leave</h4>\n          <p class=\"employees-number medical-leave\">51</p>\n        </li>\n \n        <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>\n\n      </ul> -->\n    </div>\n    <div id=\"myCarousel\" class=\"carousel slide table-data\" data-ride=\"carousel\">\n      <div class=\"carousel-inner\">\n\n        <ul class=\"item active  employe-reports list-unstyled\">\n          <a href=\"#myCarousel\" data-slide=\"prev\">\n            <i class=\"fa fa-caret-left\" aria-hidden=\"true\">\n            </i>\n          </a>\n          <li class=\"list-items-carousel\">\n            <img src=\"../../../assets/images/employee-management/Artboard-1_1_.png\" alt=\"employee-total icon\" class=\"img-responsive list-images\">\n            <h4 class=\"employees-content working\"> Working </h4>\n            <p class=\"employees-number\">1218</p>\n          </li>\n          <li class=\"list-items-carousel\">\n            <img src=\"../../../assets/images/employee-management/car1.png\" alt=\"car icon\" class=\"img-responsive list-images\">\n            <h4 class=\"employees-content\"> On Leave </h4>\n            <p class=\"employees-number\">65</p>\n          </li>\n          <li class=\"list-items-carousel\">\n            <img src=\"../../../assets/images/employee-management/watch.png\" alt=\"early-birds icon\" class=\"img-responsive list-images\">\n            <h4 class=\"employees-content\"> Early Birds </h4>\n            <p class=\"employees-number\">245</p>\n          </li>\n          <li class=\"list-items-carousel\">\n            <img src=\"../../../assets/images/employee-management/Path 19.png\" alt=\"late-arriavals icon\" class=\"img-responsive list-images\">\n            <h4 class=\"employees-content\"> Late Arrivals </h4>\n            <p class=\"employees-number\">213</p>\n          </li>\n          <li class=\"list-items-carousel\">\n            <img src=\"../../../assets/images/employee-management/wfh2.png\" alt=\"wfh-outdoor icon\" class=\"img-responsive list-images\">\n            <h4 class=\"employees-content\">WFH/outdoor</h4>\n            <p class=\"employees-number\">165</p>\n          </li>\n          <li class=\"list-items-carousel\">\n            <img src=\"../../../assets/images/employee-management/Path 20.png\" alt=\"Medical icon\" class=\"img-responsive list-images\">\n            <h4 class=\"employees-content\"> On Medical Leave</h4>\n            <p class=\"employees-number medical-leave\">51</p>\n          </li>\n          <a href=\"#myCarousel\" data-slide=\"next\">\n            <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>\n          </a>\n        </ul>\n\n\n      </div>\n      <!-- <a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">\n        <span class=\"glyphicon glyphicon-chevron-left\"></span>\n        <span class=\"sr-only\">Previous</span>\n      </a>\n      <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">\n        <span class=\"glyphicon glyphicon-chevron-right\"></span>\n        <span class=\"sr-only\">Next</span>\n      </a> -->\n    </div>\n    <!-- </div> -->\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: EmployeeManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeManagementComponent", function() { return EmployeeManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { Router } from '@angular/router';
// import { LoaderService } from '../../../services/loader.service';



var EmployeeManagementComponent = /** @class */ (function () {
    function EmployeeManagementComponent(router, loaderService, myInfoService) {
        var _this = this;
        this.router = router;
        this.loaderService = loaderService;
        this.myInfoService = myInfoService;
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
    }
    EmployeeManagementComponent.prototype.ngOnInit = function () {
    };
    EmployeeManagementComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
            this.loaderService.loader(true);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
            this.loaderService.loader(false);
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationCancel"]) {
            this.loaderService.loader(false);
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationError"]) {
            this.loaderService.loader(false);
        }
    };
    EmployeeManagementComponent.prototype.empManagement = function () {
        this.router.navigate(['/admin/admin-dashboard/employee-management/on-boarding']);
    };
    EmployeeManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-management',
            template: __webpack_require__(/*! ./employee-management.component.html */ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.html"),
            styles: [__webpack_require__(/*! ./employee-management.component.css */ "./src/app/admin-dashboard/employee-management/employee-management/employee-management.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_3__["MyInfoService"]])
    ], EmployeeManagementComponent);
    return EmployeeManagementComponent;
}());



/***/ })

}]);
//# sourceMappingURL=employee-management-employee-management-module.js.map