(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["package-creation-package-creation-module"],{

/***/ "./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".settings-add { margin: 40px 0 0;}\n.settings-add > ul { margin: 0 0 30px; width: 100%; display: inline-block;}\n.settings-add > ul > li { padding: 0 10px 0 0; margin: 0 20px 0 0;}\n.settings-add > ul > li label { font-size: 17px; line-height: 17px; color:#484747; margin: 0; font-weight: normal; padding: 0 0 14px;}\n.settings-add > ul > li .form-control { background:#fff; height: auto; border: none; color:#484747; box-shadow: none; padding: 12px; font-size: 15px;}\n.settings-add ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #484747;\n  }\n.settings-add ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484747;\n  }\n.settings-add ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.settings-add ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.setting-table {margin:10px auto 30px; padding: 0;}\n.setting-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 10px; background: #eef7ff; border:none; font-size: 16px; border-radius:3px; width: 45%;}\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td{ padding:15px 10px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#484848;}\n.setting-table .cont-check .checkbox label{padding-left:30px;color:#484848;}\n.settings-add a { cursor: pointer;}\n.settings-add a.btn {border-radius: 20px;color:#fff; font-size: 15px; padding:6px 35px; background:transparent;float:left; position: static;background: #008f3d; margin: 0 15px 0 0;}\n.settings-add a.btn:hover {background: #008f3d; color:#fff;}\n.settings-add button:focus{ border: none; outline: none;}\n.date { width: auto !important; height: auto !important; line-height:inherit !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0;}\n.date span { border: none !important; padding: 0 0 0 5px !important;}\n.date .form-control { width: 72% !important;}\n.setting-table .cont-check.add-check .checkbox { width: 13%; display: inline-block;}\n.settings-add > ul.add-btm-block { margin: 50px 0;} "

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"settings-add\">\n\n\n  <ul>\n    <li class=\"col-md-2\">\n      <label>Add-On Name</label>\n      <input type=\"text\" placeholder=\"Add-On Name\" class=\"form-control\" maxlength=\"32\" [(ngModel)]=\"addOn.name\" name=\"name\" required\n        #name=\"ngModel\">\n      <span *ngIf=\"!addOn.name && name.touched || (!addOn.name && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter addon name</span>\n    </li>\n    <li class=\"col-md-2\">\n      <label>Status</label>\n      <mat-select placeholder=\"Status\" [(ngModel)]=\"addOn.status\" name=\"status\" required #status=\"ngModel\">\n\n        <mat-option value=\"active\">Active</mat-option>\n        <mat-option value=\"inactive\">InActive</mat-option>\n      </mat-select>\n\n      <span *ngIf=\"!addOn.status && status.touched || (!addOn.status && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter status</span>\n\n    </li>\n  </ul>\n  <div class=\"clearfix\"></div>\n\n\n  <!-- <ul>\n    <li class=\"col-md-2\">\n      <label>Start Date</label>\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker\" placeholder=\"dd / mm / yyyy\" class=\"form-control\"\n          [(ngModel)]=\"addOn.startDate\" [max]=\"addOn.endDate\" name=\"date\" required #startDate=\"ngModel\">\n        <mat-datepicker #picker></mat-datepicker>\n        <button mat-raised-button (click)=\"picker.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <span *ngIf=\"!addOn.startDate && startDate.touched || (!addOn.startDate && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter startdate</span>\n\n\n    </li>\n    <li class=\"col-md-2\">\n      <label>End Date</label>\n\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker1\" placeholder=\"dd / mm / yyyy\" class=\"form-control\"\n          [(ngModel)]=\"addOn.endDate\" [min]=\"addOn.startDate\" name=\"end\" required #endDate=\"ngModel\">\n        <mat-datepicker #picker1></mat-datepicker>\n        <button mat-raised-button (click)=\"picker1.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <span *ngIf=\"!addOn.endDate && endDate.touched || (!addOn.endDate && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter enddate</span>\n\n    </li>\n  </ul> -->\n  <div class=\"clearfix\"></div>\n\n  <ul class=\"add-btm-block\">\n    <li class=\"col-md-2\">\n      <label>Cost</label>\n      <input type=\"text\" placeholder=\"$\" class=\"form-control\"  (keypress)=\"keyPress($event)\" maxlength=\"6\" [(ngModel)]=\"addOn.price\" name=\"price\" required\n        #price=\"ngModel\">\n\n      <span *ngIf=\"!addOn.price && price.touched || (!addOn.price && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter addon cost</span>\n\n    </li>\n    <li class=\"col-md-2\">\n      <label>Frequency</label>\n      <mat-select placeholder=\"Frequency\" [(ngModel)]=\"addOn.subscriptionType\" name=\"subscriptionType\" required\n        #subscriptionType=\"ngModel\">\n        <mat-option value=\"monthly\">Monthly</mat-option>\n        <mat-option value=\"yearly\">Year</mat-option>\n      </mat-select>\n\n      <span *ngIf=\"!addOn.subscriptionType && subscriptionType.touched || (!addOn.subscriptionType && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter addon frequency</span>\n\n    </li>\n    <!-- <li class=\"col-md-2\">\n      <label>Employee Cost</label>\n      <input type=\"text\" placeholder=\"$\" class=\"form-control\" [(ngModel)]=\"addOn.price\">\n    </li> -->\n  </ul>\n  <div class=\"clearfix\"></div>\n\n\n  <button style=\"background: transparent; border:none;\"><a class=\"btn\" (click)=\"createAddOn()\"\n      type=\"Submit\">Submit</a></button>\n  <button style=\"background: transparent; border:none;\"><a class=\"btn\" (click)=\"cancel()\">Cancel</a></button>\n  <div class=\"clearfix\"></div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: AddOnsAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOnsAddComponent", function() { return AddOnsAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddOnsAddComponent = /** @class */ (function () {
    function AddOnsAddComponent(packageAndAddOnService, swalAlertService, loaderService, router, activatedRoute) {
        this.packageAndAddOnService = packageAndAddOnService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.addOn = {
            name: '',
            price: 0,
            status: '',
            subscriptionType: '',
            startDate: '',
            endDate: '',
        };
        this.isValid = false;
        this.selectedaddOnId = '';
    }
    AddOnsAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.selectedaddOnId = params['id'];
        });
        if (this.selectedaddOnId) {
            this.getSpecifiedAddOnDetails();
        }
    };
    /* Description: create adon submit
author : vipin reddy */
    AddOnsAddComponent.prototype.createAddOn = function () {
        var _this = this;
        // this.loaderService.loader(true);
        this.isValid = true;
        if (this.addOn.name && this.addOn.status && this.addOn.startDate &&
            this.addOn.endDate && this.addOn.price && this.addOn.subscriptionType) {
            if (this.selectedaddOnId) {
                this.editAddOnDetails();
            }
            else {
                this.packageAndAddOnService.createAddOn(this.addOn)
                    .subscribe(function (res) {
                    console.log("Response", res);
                    _this.loaderService.loader(false);
                    if (res.status) {
                        _this.router.navigate(['/super-admin/super-admin-dashboard/settings/add-ons']);
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'success');
                    }
                    else {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error');
                    }
                }, function (err) {
                    console.log('Err', err);
                    _this.loaderService.loader(false);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
                });
            }
        }
    };
    AddOnsAddComponent.prototype.cancel = function () {
        this.addOn = {
            name: '',
            price: 0,
            status: '',
            subscriptionType: '',
            startDate: '',
            endDate: '',
        };
    };
    /* Description: get single addon detail
author : vipin reddy */
    AddOnsAddComponent.prototype.getSpecifiedAddOnDetails = function () {
        var _this = this;
        this.packageAndAddOnService.getSpecificAddOnDetails({ _id: this.selectedaddOnId })
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.addOn = res.data;
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: edit single addon detail
 author : vipin reddy */
    AddOnsAddComponent.prototype.editAddOnDetails = function () {
        var _this = this;
        this.packageAndAddOnService.editAddOn(this.addOn)
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.router.navigate(['/super-admin/super-admin-dashboard/settings/add-ons']);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'success');
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: accept only numbers
author : vipin reddy */
    AddOnsAddComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    AddOnsAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-ons-add',
            template: __webpack_require__(/*! ./add-ons-add.component.html */ "./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.html"),
            styles: [__webpack_require__(/*! ./add-ons-add.component.css */ "./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.css")]
        }),
        __metadata("design:paramtypes", [_services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__["PackageAndAddOnService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], AddOnsAddComponent);
    return AddOnsAddComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".setting-table{\n    height: 100vh;\n}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"setting-table\">\n\n  <table class=\"table\">\n    <thead>\n      <tr>\n        <th>\n          <div class=\"cont-check\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"checkAllAddOns\" (change)=\"selectAll()\">\n              Add-on Title\n            </mat-checkbox>\n          </div>\n        </th>\n        <th>Cost</th>\n        <th>Frequency</th>\n        <th>Start Date</th>\n        <th>End Date</th>\n        <th>\n          <div class=\"setting-drop\">\n            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </a>\n            <ul class=\"dropdown-menu\">\n              <li>\n                <a routerLink=\"/super-admin/super-admin-dashboard/settings/add-ons-add\">Add</a>\n              </li>\n              <li>\n                <a (click)=\"editAddOns()\">Edit</a>\n              </li>\n              <li>\n                <a (click)=\"deleteAddOns()\">Delete</a>\n              </li>\n              <!-- <li>\n                <a>Copy</a>\n              </li> -->\n            </ul>\n          </div>\n        </th>\n      </tr>\n    </thead>\n    <tbody>\n\n      <tr *ngFor=\"let addOn of addons\">\n        <td [ngClass]=\"{'zenwork-checked-border':addOn.isChecked}\">\n\n          <div class=\"cont-check\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"addOn.isChecked\">\n              {{addOn.name}}\n            </mat-checkbox>\n          </div>\n\n        </td>\n        <td>{{addOn.price}}</td>\n        <td>\n          <div class=\"include packages text-capitalize\">\n            {{addOn.subscriptionType}}\n          </div>\n        </td>\n        <!-- <td>{{addOn.startDate | date}}</td>\n        <td>{{addOn.endDate | date}}</td> -->\n        <td>--</td>\n        <td>--</td>\n        <td></td>\n      </tr>\n  </table>\n\n\n  <mat-paginator [length]=\"addonsCount\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n    (page)=\"pageEventsforAddons($event)\">\n  </mat-paginator>\n\n</div>\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.ts ***!
  \*************************************************************************************/
/*! exports provided: AddOnsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOnsComponent", function() { return AddOnsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddOnsComponent = /** @class */ (function () {
    function AddOnsComponent(packageAndAddOnService, swalAlertService, loaderService, router) {
        this.packageAndAddOnService = packageAndAddOnService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.router = router;
        this.checkAllAddOns = false;
    }
    AddOnsComponent.prototype.ngOnInit = function () {
        this.perPageforaddons = 10;
        this.pageNoforaddons = 1;
        this.getAllAddons();
    };
    /* Description: mat pagination for adons list
   author : vipin reddy */
    AddOnsComponent.prototype.pageEventsforAddons = function (event) {
        console.log(event);
        this.pageNoforaddons = event.pageIndex + 1;
        this.perPageforaddons = event.pageSize;
        this.getAllAddons();
    };
    /* Description: get all adons list
 author : vipin reddy */
    AddOnsComponent.prototype.getAllAddons = function () {
        var _this = this;
        this.loaderService.loader(true);
        var postData = {
            page_no: this.pageNoforaddons,
            per_page: this.perPageforaddons
        };
        this.packageAndAddOnService.getAllAddOnslist(postData)
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.addons = res.data;
                _this.addonsCount = res.count;
                _this.addons.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add Ons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: edit single addon
 author : vipin reddy */
    AddOnsComponent.prototype.editAddOns = function () {
        this.selectedAddOnDetails = this.addons.filter(function (addonsData) {
            return addonsData.isChecked;
        });
        if (this.selectedAddOnDetails.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Add-on", "Select only one add-on to Proceed", 'error');
        }
        else if (this.selectedAddOnDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Add-on", "Select a add-on to Proceed", 'error');
        }
        else {
            this.router.navigate(['/super-admin/super-admin-dashboard/settings/add-ons-add/' + this.selectedAddOnDetails[0]._id]);
        }
    };
    /* Description: delete multiple adons
author : vipin reddy */
    AddOnsComponent.prototype.deleteAddOns = function () {
        var _this = this;
        this.selectedAddOnDetails = this.addons.filter(function (addonsData) {
            return addonsData.isChecked;
        });
        var deletedIds = [];
        if (deletedIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete Addons", "Select a Addon to Proceed", 'error');
        }
        if (deletedIds.length >= 1) {
            this.selectedAddOnDetails.forEach(function (element) {
                deletedIds.push(element._id);
            });
            var postData = {
                _ids: deletedIds
            };
            console.log(deletedIds);
            this.packageAndAddOnService.deleteAddons(postData)
                .subscribe(function (res) {
                console.log("Response", res);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Addons', res.message, "success");
                _this.getAllAddons();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation('Addons', err.error.message, "error");
            });
        }
    };
    /* Description: select all adons
author : vipin reddy */
    AddOnsComponent.prototype.selectAll = function () {
        var _this = this;
        this.addons.forEach(function (addonsData) {
            addonsData.isChecked = _this.checkAllAddOns;
        });
    };
    AddOnsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-ons',
            template: __webpack_require__(/*! ./add-ons.component.html */ "./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.html"),
            styles: [__webpack_require__(/*! ./add-ons.component.css */ "./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.css")]
        }),
        __metadata("design:paramtypes", [_services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__["PackageAndAddOnService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], AddOnsComponent);
    return AddOnsComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.css":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.css ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-create-client-wrapper .modal-header {\n    padding: 0 0 24px 0;\n    border-bottom: 1px solid #e5e5e5;\n}\n\n.zenwork-custom-proceed-btn{\n    background: green !important;\n    border-color: transparent !important;\n    border-radius: 25px !important;\n    padding: 6px 25px; margin:0 0 0 30px;\n}\n\n.create-client-form-wrapper { padding: 30px 0 45px;}\n\n.create-client-form-wrapper label { font-weight: normal; color:#524f4f; padding: 0 0 10px; font-size: 16px;}\n\n.create-client-form-wrapper .form-control { padding:10px 15px; height: auto; border-radius: 3px; color:#524f4f;}\n\n.create-client-form-wrapper .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #5e5e5e;\n  }\n\n.create-client-form-wrapper .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #5e5e5e;\n  }"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.html":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.html ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-create-client-wrapper\">\n  <div class=\"modal-header\">\n    <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button> -->\n    <h4 class=\"modal-title green\">Add/Edit-User Profile</h4>\n  </div>\n\n  <form #loginForm=\"ngForm\" (ngSubmit)=\"addProfile()\" #f=\"ngForm\">\n\n  <div class=\"modal-body\">\n    <div class=\"create-client-form-wrapper\">\n      <div class=\"col-xs-5\">\n\n        <div class=\"form-group\">\n          <label>\n            Profile Name<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <input type=\"text\" class=\"form-control zenwork-input\" placeholder=\"Enter Name\" [(ngModel)]=\"createZenworker.name\"\n          name=\"name\" required #profileName =\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && profileName.invalid }\">\n          <span *ngIf=\"(f.submitted && profileName.invalid) || (profileName.touched && profileName.invalid)\"\n          style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span>\n        </div>\n\n        <div class=\"form-group\">\n          <label>\n            Profile Description<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <input type=\"email\" class=\"form-control zenwork-input\" placeholder=\"Profle Desciprtion\" [(ngModel)]=\"createZenworker.description\"\n          name=\"description\" required #profileDescription =\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && profileDescription.invalid }\">\n          <span *ngIf=\"(f.submitted && profileDescription.invalid) || (profileDescription.touched && profileDescription.invalid)\"\n          style=\"color:#e44a49; padding:6px 0 0;\">Enter Description</span>\n        </div>\n\n        <div class=\"form-group\">\n          <label>\n          Level of Access<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <mat-select class=\"form-control zenwork-input\" placeholder=\"level of access\" [(ngModel)]=\"createZenworker.access\"\n          name=\"access\" required #LevelAccess =\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && LevelAccess.invalid }\">\n            <mat-option value=\"view\">\n              View Only\n            </mat-option>\n            <mat-option value=\"full\">\n              Full Access\n            </mat-option>\n          </mat-select>\n          <span *ngIf=\"(f.submitted && LevelAccess.invalid) || (LevelAccess.touched && LevelAccess.invalid)\"\n          style=\"color:#e44a49; padding:6px 0 0;\">Enter Access</span>\n        </div>\n      \n        <div class=\"form-group\">\n          <label>\n            Select Clients<sup class=\"mandatory-red\">*</sup>\n          </label>\n          <mat-select class=\"form-control zenwork-input\" placeholder=\"select client\" \n          >\n            <mat-option value=\"active\">\n              All Clients\n            </mat-option>\n            <mat-option value=\"inactive\">\n              Resellers\n            </mat-option>\n          </mat-select>\n\n          <!-- <span *ngIf=\"(f.submitted && SelectClients.invalid) || (SelectClients.touched && LevelAccess.invalid)\"\n          style=\"color:#e44a49; padding:6px 0 0;\">Enter Access</span> -->\n\n        </div>\n      </div>\n      <div class=\"clearfix\"></div>\n    </div>\n  </div>\n  <div>\n    <button type=\"submit\" class=\"btn btn-success zenwork-custom-proceed-btn\" (click)=\"addProfile()\">Proceed</button>\n    <button type=\"button\" class=\"btn zenwork-customized-transparent-btn\" (click)=\"this.dialogRef.close();\">Cancel</button>\n  </div>\n</form>\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.ts":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.ts ***!
  \*******************************************************************************************************************************/
/*! exports provided: CreateUserProfileTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateUserProfileTemplateComponent", function() { return CreateUserProfileTemplateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var CreateUserProfileTemplateComponent = /** @class */ (function () {
    function CreateUserProfileTemplateComponent(dialogRef, data, zenworkersService, swalAlertService, loaderService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.zenworkersService = zenworkersService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.createZenworker = {
            name: '',
            description: '',
            access: '',
        };
    }
    CreateUserProfileTemplateComponent.prototype.ngOnInit = function () {
        if (this.data) {
            this.editId = this.data;
            this.getSingleRole(this.editId);
        }
    };
    /* Description: get single level
author : vipin reddy */
    CreateUserProfileTemplateComponent.prototype.getSingleRole = function (id) {
        var _this = this;
        this.zenworkersService.editProfile(id)
            .subscribe(function (res) {
            if (res.status == true) {
                console.log(res);
                _this.createZenworker = res.data;
                // this.dialogRef.close();
                _this.loaderService.loader(false);
                // this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success')
                // this.getAllRoles()
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: add single level
 author : vipin reddy */
    CreateUserProfileTemplateComponent.prototype.addProfile = function () {
        var _this = this;
        // this.loaderService.loader(true);
        this.zenworkersService.addProfile(this.createZenworker)
            .subscribe(function (res) {
            if (res.status == true) {
                _this.dialogRef.close();
                _this.loaderService.loader(false);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Success", res.message, 'success');
                // this.getAllRoles()
            }
        }, function (err) {
            console.log(err);
        });
    };
    CreateUserProfileTemplateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-user-profile-template',
            template: __webpack_require__(/*! ./create-user-profile-template.component.html */ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.html"),
            styles: [__webpack_require__(/*! ./create-user-profile-template.component.css */ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_2__["ZenworkersService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]])
    ], CreateUserProfileTemplateComponent);
    return CreateUserProfileTemplateComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.css":
/*!********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".settings-add { margin: 40px 0 0;}\n.settings-add > ul { margin: 0 0 30px; width: 100%; display: inline-block;}\n.settings-add > ul > li { padding: 0 10px 0 0; margin: 0 20px 0 0;}\n.settings-add > ul > li label { font-size: 17px; line-height: 17px; color:#484747; margin: 0; font-weight: normal; padding: 0 0 14px;}\n.settings-add > ul > li .form-control { background:#fff; height: auto; border: none; color:#484747; box-shadow: none; padding: 12px; font-size: 15px;}\n.settings-add ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #484747;\n  }\n.settings-add ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484747;\n  }\n.settings-add ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.settings-add ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.setting-table {margin:10px auto 30px; padding: 0;}\n.setting-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 10px; background: #eef7ff; border:none; font-size: 16px; border-radius:3px; width: 45%;}\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td{ padding:15px 10px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#484848;}\n.setting-table .cont-check .checkbox label{padding-left:30px;color:#484848;}\n.settings-add a { cursor: pointer;}\n.settings-add a.btn {border-radius: 20px;color:#fff; font-size: 15px; padding:6px 35px; background:transparent;float:left; position: static; background:#008f3d; margin: 0 10px 0 ;}\n.settings-add a.btn:hover {background: #008f3d; color:#fff;}\n.date { width: auto !important; height: auto !important; line-height:inherit !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0 10px 0 0;}\n.date span { border: none !important;}\n.date .form-control { width: 70%;}\n.setting-table .cont-check.add-check .checkbox { width: 13%; display: inline-block;}\n.settings-add > ul.add-btm-block { margin: 50px 0;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"settings-add\">\n\n  <ul>\n    <li class=\"col-md-2\">\n      <label>Coupon Name</label>\n      <input type=\"text\" placeholder=\"coupon Name\" class=\"form-control\" maxlength=\"24\" [(ngModel)]=\"coupon.name\" name=\"name\" required\n        #name=\"ngModel\">\n      <span *ngIf=\"!coupon.name && name.touched || (!coupon.name && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter coupon name</span>\n    </li>\n  </ul>\n  <div class=\"clearfix\"></div>\n\n\n  <ul>\n    <li class=\"col-md-2\">\n      <label>Start Date</label>\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker\" [max] = \"coupon.endDate\" placeholder=\"dd / mm / yyyy\" class=\"form-control\"\n          [(ngModel)]=\"coupon.startDate\" name=\"startDate\" required #startDate=\"ngModel\">\n\n        <mat-datepicker #picker></mat-datepicker>\n        <button mat-raised-button (click)=\"picker.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <span *ngIf=\"!coupon.startDate && startDate.touched || (!coupon.startDate && isValid)\"\n          style=\"color:#e44a49; padding:6px 0 0;\">Enter Startdate</span>\n\n    </li>\n    <li class=\"col-md-2\">\n      <label>End Date</label>\n\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker1\" placeholder=\"dd / mm / yyyy\" class=\"form-control\"\n          [(ngModel)]=\"coupon.endDate\" [min] = \"coupon.startDate\" name=\"endDate\" required #endDate=\"ngModel\">       \n\n        <mat-datepicker #picker1></mat-datepicker>\n        <button mat-raised-button (click)=\"picker1.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <span *ngIf=\"!coupon.endDate && endDate.touched || (!coupon.endDate && isValid)\"\n      style=\"color:#e44a49; padding:6px 0 0;\">Enter Enddate</span>\n\n    </li>\n  </ul>\n  <div class=\"clearfix\"></div>\n\n  <ul class=\"add-btm-block\">\n    <li class=\"col-md-2\">\n      <label>Select Coupon Type</label>\n      <mat-select placeholder=\"Coupon Type\" [(ngModel)]=\"coupon.couponType\" (ngModelChange)=\"couponTypeChange()\" name=\"coupontype\" required #coupontype=\"ngModel\">\n        <mat-option value=\"Percentage\">Percentage-%</mat-option>\n        <mat-option value=\"Value\">Value-$</mat-option>\n      </mat-select>\n\n      <span *ngIf=\"!coupon.couponType && coupontype.touched || (!coupon.couponType && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Select coupon type</span>\n    </li>\n    <li *ngIf = \"coupon.couponType == 'Percentage'\"class=\"col-md-2\">\n      <label>Coupon %</label>\n      <input type=\"text\" placeholder=\"%\" class=\"form-control\" (keypress)=\"keyPress($event)\"\n        [(ngModel)]=\"coupon.percentage\" name=\"percentage\" maxlength=\"6\"  required #percentage=\"ngModel\">\n\n      <span *ngIf=\"!coupon.percentage && percentage.touched || (!coupon.percentage && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter coupon percentage</span>\n    </li>\n    <li  *ngIf = \"coupon.couponType == 'Value'\" class=\"col-md-2\">\n      <label>Coupon $</label>\n      <input type=\"text\" placeholder=\"$\" class=\"form-control\" (keypress)=\"keyPress($event)\"\n        [(ngModel)]=\"coupon.flatDiscountAmount\" maxlength=\"6\" name=\"flatDiscountAmount\" required #flatDiscountAmount=\"ngModel\">\n      <span *ngIf=\"!coupon.flatDiscountAmount && flatDiscountAmount.touched || (!coupon.flatDiscountAmount && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter coupon value</span>\n    </li>\n\n  </ul>\n  <div class=\"clearfix\"></div>\n\n\n  <a class=\"btn\" type=\"Submit\" (click)=\"createCoupon()\">Submit</a>\n  <a class=\"btn\" (click)=\"cancel()\">Cancel</a>\n  <div class=\"clearfix\"></div>\n\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: CuponsAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CuponsAddComponent", function() { return CuponsAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CuponsAddComponent = /** @class */ (function () {
    function CuponsAddComponent(couponAddService, router, activated, loaderService, swalAlertService) {
        this.couponAddService = couponAddService;
        this.router = router;
        this.activated = activated;
        this.loaderService = loaderService;
        this.swalAlertService = swalAlertService;
        this.isValid = false;
        this.coupon = {
            name: '',
            startDate: '',
            endDate: '',
            percentage: '',
            flatDiscountAmount: '',
            couponType: ''
        };
        this.selectCouponId = '';
    }
    CuponsAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activated.params.subscribe(function (params) {
            _this.selectCouponId = params['id'];
        });
        if (this.selectCouponId) {
            this.getCouponDetails();
        }
    };
    /* Description: coupons create
   author : vipin reddy */
    CuponsAddComponent.prototype.createCoupon = function () {
        var _this = this;
        this.isValid = true;
        if (this.coupon.name && this.coupon.startDate && this.coupon.endDate) {
            this.loaderService.loader(true);
            console.log(this.coupon);
            if (this.selectCouponId) {
                this.editCouponDetails();
            }
            else {
                this.couponAddService.createCoupon(this.coupon)
                    .subscribe(function (res) {
                    console.log("Response", res);
                    _this.loaderService.loader(false);
                    if (res.status) {
                        _this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'success');
                    }
                    else {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error');
                    }
                }, function (err) {
                    console.log('Err', err);
                    _this.loaderService.loader(false);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
                });
            }
        }
    };
    CuponsAddComponent.prototype.cancel = function () {
        this.coupon = {
            name: '',
            startDate: '',
            endDate: '',
            percentage: '',
            flatDiscountAmount: '',
            couponType: ''
        };
    };
    /* Description: get single coupon
 author : vipin reddy */
    CuponsAddComponent.prototype.getCouponDetails = function () {
        var _this = this;
        this.couponAddService.getSepcificCouponDetails({ _id: this.selectCouponId })
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.coupon = res.data;
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add-On", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: update coupon detail
   author : vipin reddy */
    CuponsAddComponent.prototype.editCouponDetails = function () {
        var _this = this;
        console.log(this.coupon);
        this.couponAddService.editCouponsData(this.coupon)
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
                _this.swalAlertService.SweetAlertWithoutConfirmation("coupons-add", res.message, 'success');
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("coupons-add", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: coupon type percentage to value change
   author : vipin reddy */
    CuponsAddComponent.prototype.couponTypeChange = function () {
        console.log(this.coupon.couponType);
        if (this.coupon.couponType == 'Value') {
            delete this.coupon.percentage;
        }
        if (this.coupon.couponType == 'Percentage') {
            delete this.coupon.flatDiscountAmount;
        }
    };
    /* Description: accept only numbers
 author : vipin reddy */
    CuponsAddComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    CuponsAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cupons-add',
            template: __webpack_require__(/*! ./cupons-add.component.html */ "./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.html"),
            styles: [__webpack_require__(/*! ./cupons-add.component.css */ "./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.css")]
        }),
        __metadata("design:paramtypes", [_services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__["PackageAndAddOnService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"]])
    ], CuponsAddComponent);
    return CuponsAddComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modules-add { margin: 40px 0 0;}\n.modules-add > ul { margin: 0 0 30px; width: 100%; display: inline-block;}\n.modules-add > ul > li { padding: 0 10px 0 0; margin: 0 20px 0 0;}\n.modules-add > ul > li label { font-size: 17px; line-height: 17px; color:#484747; margin: 0; font-weight: normal; padding: 0 0 14px;}\n.modules-add > ul > li .form-control { background:#fff; height: auto; border: none; color:#484747; box-shadow: none; padding: 12px; font-size: 15px;}\n.modules-add ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #484747;\n  }\n.modules-add ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484747;\n  }\n.modules-add ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.modules-add ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.modules-table {margin:0 0 50px; padding: 0; float: none;}\n.modules-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px 30px; background: #eef7ff; border:none; font-size: 16px; border-radius:3px; width: 45%;}\n.modules-table .table>tbody>tr>td, .modules-table .table>tfoot>tr>td, .modules-table .table>thead>tr>td{ padding:15px 30px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#484848;}\n.modules-add a { cursor: pointer;}\n.modules-add a.btn {border-radius: 20px;color:#484848; font-size: 15px; padding:6px 35px; background:transparent;float:left; position: static;}\n.modules-add a.btn:hover {background: #008f3d; color:#fff;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modules-add\">\n\n  <ul>\n    <li class=\"col-md-2\">\n      <label>Module Name</label>\n      <input type=\"text\" placeholder=\"Modules Name\" class=\"form-control\">\n    </li>\n  </ul>\n  <div class=\"clearfix\"></div>\n\n\n\n  <div class=\"modules-table col-md-8\">\n\n    <table class=\"table\">\n      <thead>\n        <tr>\n          <th>Sub Module</th>\n          <th>Include ?</th>\n        </tr>\n      </thead>\n      <tbody>\n\n        <tr>\n          <td>Company Setup</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n        <tr>\n          <td>Theme</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n        <tr>\n          <td>Site Access / Rights Managements</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n        <tr>\n          <td>Workflow Approvals</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n        <tr>\n          <td>Mobile Access</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n        <tr>\n          <td>Integration & API Partners</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n\n        <tr>\n          <td>Onboarding</td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\">\n                <mat-option value=\"Yes\">Yes</mat-option>\n                <mat-option value=\"No\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n        </tr>\n\n        \n\n    </table>\n\n  </div>\n\n\n  <a class=\"btn\">Submit</a>\n  <a class=\"btn\">Cancel</a>\n  <div class=\"clearfix\"></div>\n\n\n\n\n\n  </div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: ModulesAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModulesAddComponent", function() { return ModulesAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ModulesAddComponent = /** @class */ (function () {
    function ModulesAddComponent() {
    }
    ModulesAddComponent.prototype.ngOnInit = function () {
    };
    ModulesAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modules-add',
            template: __webpack_require__(/*! ./modules-add.component.html */ "./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.html"),
            styles: [__webpack_require__(/*! ./modules-add.component.css */ "./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ModulesAddComponent);
    return ModulesAddComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/modules/modules.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/modules/modules.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".setting-table { padding: 0; float: none;height:100vh;}\n\n.setting-table a.btn {border-radius: 20px;color:#484848; font-size: 15px; padding:6px 40px; background:#008f3d; margin: 35px 0 0; color:#fff;}\n\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td{ padding:20px 10px;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/modules/modules.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/modules/modules.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"setting-table col-md-6\">\n\n  <table class=\"table\">\n    <thead>\n      <tr>\n        <th>\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox1\" type=\"checkbox\">\n              <label for=\"checkbox1\">Module</label>\n            </div>\n          </div>\n        </th>\n       \n         <th>\n           <div class=\"setting-drop\">\n            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></a>\n            <ul class=\"dropdown-menu\">\n              <li><a routerLink=\"/super-admin/super-admin-dashboard/settings/modules-add\">Add</a></li>\n              <li><a>Edit</a></li>\n              <!-- <li><a>Delete</a></li>\n              <li><a>Copy</a></li> -->\n             </ul>\n            </div>\n         </th>\n      </tr>\n    </thead>\n    <tbody>\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox2\" type=\"checkbox\">\n              <label for=\"checkbox2\">Company Settings</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox3\" type=\"checkbox\">\n              <label for=\"checkbox3\">Employee Management</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox4\" type=\"checkbox\">\n              <label for=\"checkbox4\">Recruitment</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox5\" type=\"checkbox\">\n              <label for=\"checkbox5\">Benifits Administration</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox6\" type=\"checkbox\">\n              <label for=\"checkbox6\">Leave Management</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox7\" type=\"checkbox\">\n              <label for=\"checkbox7\">Performance Management</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n\n      <tr>\n        <td class=\"zenwork-checked-border\">\n\n          <div class=\"cont-check\">\n            <div class=\"checkbox\">\n              <input id=\"checkbox8\" type=\"checkbox\">\n              <label for=\"checkbox8\">Reports</label>\n            </div>\n          </div>\n\n        </td>\n        \n        <td></td>\n      </tr>\n\n\n  </table>\n\n\n  <a class=\"btn\">Submit</a>\n\n</div>\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/modules/modules.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/modules/modules.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ModulesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModulesComponent", function() { return ModulesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ModulesComponent = /** @class */ (function () {
    function ModulesComponent() {
    }
    ModulesComponent.prototype.ngOnInit = function () {
    };
    ModulesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modules',
            template: __webpack_require__(/*! ./modules.component.html */ "./src/app/super-admin-dashboard/package-creation/modules/modules.component.html"),
            styles: [__webpack_require__(/*! ./modules.component.css */ "./src/app/super-admin-dashboard/package-creation/modules/modules.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ModulesComponent);
    return ModulesComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/package-creation.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/package-creation.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".setting-panel { margin:50px auto 0; float: none; padding: 0;}\n\n.zenwork-currentpage { padding:0;}\n\n.zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 18px;line-height: 17px; color:#008f3d; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{margin-right: 7px;}\n\n.zenwork-margin-ten-zero {\n    margin: 15px 0px!important;\n}\n\n.setting-panel-in { display: block;}\n\n.setting-tabs {padding:15px 0 0;}\n\n.setting-tabs .tab-content { margin:40px 0 0;}\n\n.setting-tabs ul {border-bottom:#dbdbdb 2px solid; padding: 0 0 2px 60px;}\n\n.setting-tabs ul li { padding:0 30px 10px;display: inline-block;}\n\n.setting-tabs ul li a {font-size: 19px;line-height: 19px;font-weight:normal;color:#484747;border: none;cursor: pointer;padding:0 30px;}\n\n.setting-tabs ul > li > a:hover, .ul > li > a:focus { background: none;}\n\n.setting-tabs ul li.active a{border-bottom:#439348 5px solid;padding:0px 30px 9px;color:#484747;}\n\n.setting-tabs ul li a:focus, .setting-tabs ul li a:hover{ outline: none;}\n\n\n"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/package-creation.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/package-creation.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"setting-panel col-md-11\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n\n      <small>\n        <img src=\"../../../assets/images/Side Menu/Green/green-client.png\" width=\"18\" height=\"16\"\n          alt=\"Company-settings icon\">\n      </small>\n      <em>Settings - Super Admin Panel</em>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"setting-tabs\">\n\n    <ul>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/super-admin/super-admin-dashboard/settings/packages\">Packages</a>\n        <!-- <a routerLink=\"/super-admin/super-admin-dashboard/settings/packages-add\"></a> -->\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/super-admin/super-admin-dashboard/settings/modules\">Modules</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/super-admin/super-admin-dashboard/settings/add-ons\">Add-ons</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/super-admin/super-admin-dashboard/settings/user-profile\">User Profile</a>\n      </li>\n    </ul>\n\n  </div>\n\n  <router-outlet></router-outlet>\n\n</div>\n\n\n<!-- <div class=\"package-wrapper\">\n  <div class=\"zenwork-package-creation\">\n    <mat-tab-group>\n      <mat-tab label=\"Create Package\">\n        <div class=\"form-group\">\n          <input class=\"form-control zenwork-input zenwork-custom-input\" type=\"text\" [(ngModel)]=\"package.name\" placeholder=\"Package Name\">\n        </div>\n        <div class=\"form-group\">\n          <input class=\"form-control zenwork-input zenwork-custom-input\" type=\"number\" [(ngModel)]=\"package.price\" placeholder=\"Package Price\">\n        </div>\n        <div class=\"form-group\">\n          <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Package Status\" [(ngModel)]=\"package.status\">\n            <mat-option value=\"active\">\n              Active\n            </mat-option>\n            <mat-option value=\"inactive\">\n              In Active\n            </mat-option>\n          </mat-select>\n        </div>\n        <div class=\"form-group\">\n          <button type=\"submit\" (click)=\"createPackage()\" class=\"btn btn-success\">Create Package</button>\n        </div>\n      </mat-tab>\n      <mat-tab label=\"Create Add On\">\n        <div class=\"form-group\">\n          <input class=\"form-control zenwork-input zenwork-custom-input\" type=\"text\" [(ngModel)]=\"addOn.name\" placeholder=\"Add-on Name\">\n        </div>\n        <div class=\"form-group\">\n          <input class=\"form-control zenwork-input zenwork-custom-input\" type=\"number\" [(ngModel)]=\"addOn.price\" placeholder=\"Add-on Price\">\n        </div>\n        <div class=\"form-group\">\n          <mat-select class=\"form-control zenwork-input zenwork-custom-input zenwork-custom-select\" placeholder=\"Add-on Status\" [(ngModel)]=\"addOn.status\">\n            <mat-option value=\"active\">\n              Active\n            </mat-option>\n            <mat-option value=\"inactive\">\n              In Active\n            </mat-option>\n          </mat-select>\n        </div>\n        <div class=\"form-group\">\n          <button type=\"submit\" (click)=\"createAddOn()\" class=\"btn btn-success\">Create Add-on</button>\n        </div>\n      </mat-tab>\n    </mat-tab-group>\n  </div>\n</div> -->"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/package-creation.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/package-creation.component.ts ***!
  \**************************************************************************************/
/*! exports provided: PackageCreationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackageCreationComponent", function() { return PackageCreationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PackageCreationComponent = /** @class */ (function () {
    function PackageCreationComponent(packageAndAddOnService, loaderService, swalAlertService) {
        this.packageAndAddOnService = packageAndAddOnService;
        this.loaderService = loaderService;
        this.swalAlertService = swalAlertService;
        this.package = {
            name: '',
            price: 0,
            status: 'active'
        };
        this.addOn = {
            name: '',
            price: 0,
            status: 'active'
        };
    }
    PackageCreationComponent.prototype.ngOnInit = function () {
        // this.loaderService.loader(true);
    };
    PackageCreationComponent.prototype.createPakage = function () {
        var _this = this;
        this.packageAndAddOnService.createPackage(this.package)
            .subscribe(function (res) {
            if (res.status) {
                _this.loaderService.loader(false);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Package", res.message, 'success');
            }
            else {
                _this.loaderService.loader(false);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Package", res.message, 'info');
            }
        }, function (err) {
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Package", err.message, 'error');
        });
    };
    PackageCreationComponent.prototype.createAddOn = function () {
        var _this = this;
        this.packageAndAddOnService.createAddOn(this.addOn)
            .subscribe(function (res) {
            if (res.status) {
                _this.loaderService.loader(false);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add On", res.message, 'success');
            }
            else {
                _this.loaderService.loader(false);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Add On", res.message, 'info');
            }
        }, function (err) {
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Add On", err.message, 'error');
        });
    };
    PackageCreationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-package-creation',
            template: __webpack_require__(/*! ./package-creation.component.html */ "./src/app/super-admin-dashboard/package-creation/package-creation.component.html"),
            styles: [__webpack_require__(/*! ./package-creation.component.css */ "./src/app/super-admin-dashboard/package-creation/package-creation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__["PackageAndAddOnService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"]])
    ], PackageCreationComponent);
    return PackageCreationComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/package-creation.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/package-creation.module.ts ***!
  \***********************************************************************************/
/*! exports provided: PackageCreationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackageCreationModule", function() { return PackageCreationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _package_creation_package_creation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../package-creation/package-creation.component */ "./src/app/super-admin-dashboard/package-creation/package-creation.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _packages_packages_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./packages/packages.component */ "./src/app/super-admin-dashboard/package-creation/packages/packages.component.ts");
/* harmony import */ var _packages_add_packages_add_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./packages-add/packages-add.component */ "./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _modules_modules_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/modules.component */ "./src/app/super-admin-dashboard/package-creation/modules/modules.component.ts");
/* harmony import */ var _modules_add_modules_add_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modules-add/modules-add.component */ "./src/app/super-admin-dashboard/package-creation/modules-add/modules-add.component.ts");
/* harmony import */ var _add_ons_add_ons_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./add-ons/add-ons.component */ "./src/app/super-admin-dashboard/package-creation/add-ons/add-ons.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _add_ons_add_add_ons_add_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./add-ons-add/add-ons-add.component */ "./src/app/super-admin-dashboard/package-creation/add-ons-add/add-ons-add.component.ts");
/* harmony import */ var _cupons_add_cupons_add_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./cupons-add/cupons-add.component */ "./src/app/super-admin-dashboard/package-creation/cupons-add/cupons-add.component.ts");
/* harmony import */ var _user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./user-profile/user-profile.component */ "./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.ts");
/* harmony import */ var _create_user_profile_template_create_user_profile_template_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./create-user-profile-template/create-user-profile-template.component */ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var router = [
    // { path: '', redirectTo: 'setting', pathMatch: 'full' },
    {
        path: '', component: _package_creation_package_creation_component__WEBPACK_IMPORTED_MODULE_2__["PackageCreationComponent"], children: [
            { path: '', redirectTo: 'packages', pathMatch: 'full' },
            { path: 'packages', component: _packages_packages_component__WEBPACK_IMPORTED_MODULE_4__["PackagesComponent"] },
            { path: 'packages-add', component: _packages_add_packages_add_component__WEBPACK_IMPORTED_MODULE_5__["PackagesAddComponent"] },
            // { path: '', redirectTo: 'modules', pathMatch: 'full' },
            { path: 'modules', component: _modules_modules_component__WEBPACK_IMPORTED_MODULE_7__["ModulesComponent"] },
            { path: 'modules-add', component: _modules_add_modules_add_component__WEBPACK_IMPORTED_MODULE_8__["ModulesAddComponent"] },
            // { path: '', redirectTo: 'add-ons', pathMatch: 'full' },
            { path: 'add-ons', component: _add_ons_add_ons_component__WEBPACK_IMPORTED_MODULE_9__["AddOnsComponent"] },
            { path: 'add-ons-add', component: _add_ons_add_add_ons_add_component__WEBPACK_IMPORTED_MODULE_12__["AddOnsAddComponent"] },
            { path: 'add-ons-add/:id', component: _add_ons_add_add_ons_add_component__WEBPACK_IMPORTED_MODULE_12__["AddOnsAddComponent"] },
            { path: 'packages-add', component: _packages_add_packages_add_component__WEBPACK_IMPORTED_MODULE_5__["PackagesAddComponent"] },
            { path: 'packages-add/:id', component: _packages_add_packages_add_component__WEBPACK_IMPORTED_MODULE_5__["PackagesAddComponent"] },
            { path: 'coupons-add', component: _cupons_add_cupons_add_component__WEBPACK_IMPORTED_MODULE_13__["CuponsAddComponent"] },
            { path: 'coupons-add/:id', component: _cupons_add_cupons_add_component__WEBPACK_IMPORTED_MODULE_13__["CuponsAddComponent"] },
            { path: "user-profile", component: _user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_14__["UserProfileComponent"] }
        ]
    },
];
var PackageCreationModule = /** @class */ (function () {
    function PackageCreationModule() {
    }
    PackageCreationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(router),
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_11__["MatCheckboxModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_16__["MaterialModuleModule"],
            ],
            entryComponents: [
                _create_user_profile_template_create_user_profile_template_component__WEBPACK_IMPORTED_MODULE_15__["CreateUserProfileTemplateComponent"]
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]
            ],
            declarations: [_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_14__["UserProfileComponent"],
                _package_creation_package_creation_component__WEBPACK_IMPORTED_MODULE_2__["PackageCreationComponent"],
                _packages_packages_component__WEBPACK_IMPORTED_MODULE_4__["PackagesComponent"],
                _packages_add_packages_add_component__WEBPACK_IMPORTED_MODULE_5__["PackagesAddComponent"],
                _modules_modules_component__WEBPACK_IMPORTED_MODULE_7__["ModulesComponent"],
                _modules_add_modules_add_component__WEBPACK_IMPORTED_MODULE_8__["ModulesAddComponent"],
                _add_ons_add_ons_component__WEBPACK_IMPORTED_MODULE_9__["AddOnsComponent"],
                _add_ons_add_add_ons_add_component__WEBPACK_IMPORTED_MODULE_12__["AddOnsAddComponent"],
                _cupons_add_cupons_add_component__WEBPACK_IMPORTED_MODULE_13__["CuponsAddComponent"],
                _create_user_profile_template_create_user_profile_template_component__WEBPACK_IMPORTED_MODULE_15__["CreateUserProfileTemplateComponent"]],
        })
    ], PackageCreationModule);
    return PackageCreationModule;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".settings-add { margin: 40px 0 0;}\n.settings-add > ul { margin: 0 0 30px; width: 100%; display: inline-block;}\n.settings-add > ul > li { padding: 0 10px 0 0; margin: 0 20px 0 0;}\n.settings-add > ul > li label { font-size: 17px; line-height: 17px; color:#484747; margin: 0; font-weight: normal; padding: 0 0 14px;}\n.settings-add > ul > li .form-control { background:#fff; height: auto; border: none; color:#484747; box-shadow: none; padding: 12px; font-size: 15px;}\n.settings-add ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #484747;\n  }\n.settings-add ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484747;\n  }\n.settings-add ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484747;\n  }\n.settings-add ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484747;\n  }\n.setting-table {margin:30px 0 30px; padding: 0; float: none;}\n.setting-table .table>thead>tr>th { color: #484848; font-weight: normal; padding: 14px 10px; background: #eef7ff; border:none; font-size: 16px; border-radius:3px; width: 45%;}\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td{ padding:15px 10px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color:#484848;}\n.setting-table .cont-check .checkbox label{padding-left:30px;color:#484848;}\n.settings-add a { cursor: pointer;}\n.settings-add a.btn {border-radius: 20px;color:#fff; font-size: 15px; padding:6px 35px; background:transparent; margin:30px 10px 0; float:left; position: static;background: #008f3d;}\n.settings-add a.btn:hover {background: #008f3d; color:#fff;}\n.date { width: auto !important; height: auto !important; line-height:inherit !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0;}\n.date span { border: none !important;}\n.date .form-control { width: 62% !important;}\n.setting-table .cont-check.add-check .checkbox { width: 13%; display: inline-block;}\n.settings-add button:focus{ outline: none; border: none;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"settings-add\">\n\n  <ul>\n    <li class=\"col-md-2\">\n      <label>Package Name</label>\n      <input type=\"text\" placeholder=\"Package Name\" class=\"form-control\" maxlength=\"24\" [(ngModel)]=\"package.name\"\n        name=\"PackageName\" required #PackageName=\"ngModel\">\n      <span *ngIf=\"!package.name && PackageName.touched || (!package.name && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span>\n    </li>\n    <li class=\"col-md-2\">\n      <label>Status</label>\n      <mat-select placeholder=\"Status\" [(ngModel)]=\"package.status\" name=\"status\" required #status=\"ngModel\">\n        <mat-option value=\"active\">Active</mat-option>\n        <mat-option value=\"inactive\">InActive</mat-option>\n      </mat-select>\n      <span *ngIf=\"!package.status && status.touched || (!package.status && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter name</span>\n    </li>\n  </ul>\n  <div class=\"clearfix\"></div>\n\n\n  <!-- <ul>\n    <li class=\"col-md-2\">\n      <label>Start Date</label>\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker\" placeholder=\"dd / mm / yyyy\" class=\"form-control\"\n          [(ngModel)]=\"package.startDate\" [max]=\"package.endDate\" name=\"StartDate\" required #StartDate=\"ngModel\">\n        <mat-datepicker #picker></mat-datepicker>\n        <button mat-raised-button (click)=\"picker.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <span *ngIf=\"!package.startDate && StartDate.touched || (!package.startDate && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter date</span>\n\n    </li>\n    <li class=\"col-md-2\">\n      <label>End Date</label>\n\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker1\" placeholder=\"dd / mm / yyyy\" class=\"form-control\"\n          [(ngModel)]=\"package.endDate\" [min]=\"package.startDate\" name=\"endDate\" required #endDate=\"ngModel\">\n        <mat-datepicker #picker1></mat-datepicker>\n        <button mat-raised-button (click)=\"picker1.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n\n      <span *ngIf=\"!package.endDate && endDate.touched || (!package.endDate && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter date</span>\n\n\n\n    </li>\n  </ul> -->\n  <div class=\"clearfix\"></div>\n\n\n  <div class=\"setting-table col-md-8\">\n\n    <table class=\"table\">\n      <thead>\n        <tr>\n          <th>\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox1\" type=\"checkbox\">\n                <label for=\"checkbox1\">Module</label>\n              </div>\n            </div>\n          </th>\n          <th>Include ?</th>\n\n          <th>\n            <div class=\"setting-drop\">\n              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n              </a>\n              <ul class=\"dropdown-menu\">\n                <li>\n                  <a>Add</a>\n                </li>\n                <li>\n                  <a>Edit</a>\n                </li>\n                <li>\n                  <a>Delete</a>\n                </li>\n              </ul>\n            </div>\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox2\" type=\"checkbox\">\n                <label for=\"checkbox2\">Company Settings</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.companySettings\" name=\"work\" required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox3\" type=\"checkbox\">\n                <label for=\"checkbox3\">Employee Management</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.employeeManagement\" name=\"work1\" required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox4\" type=\"checkbox\">\n                <label for=\"checkbox4\">Requirements</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.requirements\" name=\"work2\" required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox4\" type=\"checkbox\">\n                <label for=\"checkbox4\">Benifits Management</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.benifitsManagement\" name=\"work\" required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox5\" type=\"checkbox\">\n                <label for=\"checkbox5\">Leave Management</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.leaveManagement\" name=\"work\" required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox5\" type=\"checkbox\">\n                <label for=\"checkbox5\">Performance Management</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.performanceManagement\" name=\"work\"\n                required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n        <tr>\n          <td class=\"zenwork-checked-border\">\n\n            <div class=\"cont-check\">\n              <div class=\"checkbox\">\n                <input id=\"checkbox6\" type=\"checkbox\">\n                <label for=\"checkbox6\">Reports</label>\n              </div>\n            </div>\n\n          </td>\n          <td>\n            <div class=\"include\">\n              <mat-select placeholder=\"Include\" [(ngModel)]=\"package.modules.reports\" name=\"work\" required>\n                <mat-option value=\"true\">Yes</mat-option>\n                <mat-option value=\"false\">No</mat-option>\n              </mat-select>\n            </div>\n          </td>\n          <td></td>\n        </tr>\n\n    </table>\n\n  </div>\n\n  <ul>\n    <li class=\"col-md-2\">\n      <label>Package Cost</label>\n      <input type=\"text\" (keypress)=\"keyPress($event)\" placeholder=\"$\" maxlength=\"6\" class=\"form-control\" [(ngModel)]=\"package.price\"\n        name=\"cost\" required #price=\"ngModel\">\n\n      <span *ngIf=\"!package.price && price.touched || (!package.price && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter cost</span>\n\n    </li>\n    <li class=\"col-md-2\">\n      <label>Per Employee Cost</label>\n      <input type=\"text\" (keypress)=\"keyPress($event)\" maxlength=\"6\" placeholder=\"$\" class=\"form-control\"\n        [(ngModel)]=\"package.costPerEmployee\" name=\"cost\" required #costPerEmployee=\"ngModel\">\n\n      <span *ngIf=\"!package.costPerEmployee && costPerEmployee.touched || (!package.costPerEmployee && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter cost</span>\n\n    </li>\n\n    <li class=\"col-md-2\">\n      <label>Frequency</label>\n      <mat-select placeholder=\"Frequency\" [(ngModel)]=\"package.subscriptionType\" name=\"subscriptionType\" required\n        #subscriptionType=\"ngModel\">\n        <mat-option value=\"Monthly\">Monthly</mat-option>\n        <mat-option value=\"Yearly\">Year</mat-option>\n      </mat-select>\n\n      <span *ngIf=\"!package.subscriptionType && subscriptionType.touched || (!package.subscriptionType && isValid)\"\n        style=\"color:#e44a49; padding:6px 0 0;\">Enter cost</span>\n\n    </li>\n  </ul>\n\n  <div class=\"clearfix\"></div>\n\n\n  <button style=\"background: transparent; border:none;\"> <a class=\"btn\" (click)=\"addPackage()\"\n      type=\"Submit\">Submit</a></button>\n  <a class=\"btn\" (click)=\"cancel()\">Cancel</a>\n  <div class=\"clearfix\"></div>\n\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: PackagesAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackagesAddComponent", function() { return PackagesAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PackagesAddComponent = /** @class */ (function () {
    function PackagesAddComponent(packageAndAddOnService, swalAlertService, loaderService, router, activatedRoute) {
        this.packageAndAddOnService = packageAndAddOnService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.isValid = false;
        this.package = {
            name: '',
            price: 0,
            subscriptionType: '',
            startDate: '',
            endDate: '',
            costPerEmployee: 0,
            status: '',
            modules: {
                companySettings: false,
                employeeManagement: false,
                requirements: false,
                benifitsManagement: false,
                performanceManagement: false,
                leaveManagement: false,
                reports: false
            }
        };
        this.selectedPackageId = '';
    }
    PackagesAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.selectedPackageId = params['id'];
        });
        if (this.selectedPackageId) {
            this.getSpecifiedPackageDetails();
        }
    };
    /* Description: package creation
   author : vipin reddy */
    PackagesAddComponent.prototype.addPackage = function () {
        var _this = this;
        this.isValid = true;
        // this.loaderService.loader(true);
        if (this.package.name && this.package.status && this.package.startDate &&
            this.package.endDate && this.package.price && this.package.subscriptionType && this.package.costPerEmployee)
            if (this.selectedPackageId) {
                this.editPackageDetails();
            }
            else {
                this.packageAndAddOnService.createPackage(this.package)
                    .subscribe(function (res) {
                    console.log("Response", res);
                    _this.loaderService.loader(false);
                    if (res.status) {
                        _this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Package", res.message, 'success');
                    }
                    else {
                        _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
                    }
                }, function (err) {
                    console.log('Err', err);
                    _this.loaderService.loader(false);
                    _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
                });
            }
    };
    PackagesAddComponent.prototype.cancel = function () {
        this.package = {
            name: '',
            price: 0,
            subscriptionType: '',
            startDate: '',
            endDate: '',
            costPerEmployee: 0,
            status: "active",
            modules: {
                companySettings: false,
                employeeManagement: false,
                requirements: false,
                benifitsManagement: false,
                performanceManagement: false,
                leaveManagement: false,
                reports: false
            }
        };
    };
    /* Description: get single package detail
   author : vipin reddy */
    PackagesAddComponent.prototype.getSpecifiedPackageDetails = function () {
        var _this = this;
        this.packageAndAddOnService.getSpecificPackageDetails({ _id: this.selectedPackageId })
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.package = res.data;
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: edit single package details
   author : vipin reddy */
    PackagesAddComponent.prototype.editPackageDetails = function () {
        var _this = this;
        this.packageAndAddOnService.editPackage(this.package)
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages']);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Package", res.message, 'success');
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: accept only numbers
   author : vipin reddy */
    PackagesAddComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9 ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    PackagesAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-packages-add',
            template: __webpack_require__(/*! ./packages-add.component.html */ "./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.html"),
            styles: [__webpack_require__(/*! ./packages-add.component.css */ "./src/app/super-admin-dashboard/package-creation/packages-add/packages-add.component.css")]
        }),
        __metadata("design:paramtypes", [_services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__["PackageAndAddOnService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], PackagesAddComponent);
    return PackagesAddComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/packages/packages.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/packages/packages.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .setting-table{height: 100vh;} */\n.setting-table .table>thead>tr>th { padding: 18px 15px;}\n.setting-table .table>tbody>tr>td, .setting-table .table>tfoot>tr>td, .setting-table .table>thead>tr>td { padding: 17px 15px;}\n.tax-rate { margin: 40px 0 0;}\n.tax-rate h2 {font-size: 18px; color:#008f3d; margin: 0 0 20px;}\n.tax-rate > ul { margin: 0 0 30px; width: 100%; display: inline-block;}\n.tax-rate > ul > li { padding: 0 10px 0 0; margin: 0 20px 0 0;}\n.tax-rate > ul > li label { font-size: 17px; line-height: 17px; color:#484747; margin: 0; font-weight: normal; padding: 0 0 14px;}\n.tax-rate > ul > li .form-control { background:#fff; height: auto; border: none; color:#484747; box-shadow: none; padding: 12px 16px; font-size: 15px;}\n.tax-rate ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\ncolor: #484747;\n}\n.tax-rate ul li .form-control::-moz-placeholder { /* Firefox 19+ */\ncolor: #484747;\n}\n.tax-rate ul li .form-control:-ms-input-placeholder { /* IE 10+ */\ncolor: #484747;\n}\n.tax-rate ul li .form-control:-moz-placeholder { /* Firefox 18- */\ncolor: #484747;\n}\n.tax-rate a.btn {border-radius: 20px;color:#fff; font-size: 15px; padding:6px 35px; background:#008f3d; margin: 35px 0 0;}\n.setting-table h5 {font-size: 18px; color:#008f3d; margin: 0 0 20px;}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/packages/packages.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/packages/packages.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"setting-table\">\n\n  <table class=\"table\">\n    <thead>\n      <tr>\n        <th>\n          <div class=\"cont-check\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"selectPackages\" (change)=\"selectAllPackages()\">\n              Package Name\n            </mat-checkbox>\n          </div>\n        </th>\n        <th>Package Cost</th>\n        <th>Per Employee Cost</th>\n        <th>Frequency</th>\n        <th>Status</th>\n        <th>Start Date</th>\n        <th>End Date</th>\n        <th>\n          <div class=\"setting-drop\">\n            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </a>\n            <ul class=\"dropdown-menu\">\n              <li>\n                <a routerLink=\"/super-admin/super-admin-dashboard/settings/packages-add\">Add</a>\n              </li>\n              <li>\n                <a (click)=\"editPackage()\">Edit</a>\n              </li>\n              <li >\n                <a (click)=\"deletePackages()\">Delete</a>\n              </li>\n              <!-- <li>\n                <a>Copy</a>\n              </li> -->\n            </ul>\n          </div>\n        </th>\n      </tr>\n    </thead>\n    <tbody>\n\n\n      <tr *ngFor=\"let package of packages\">\n        <td [ngClass]=\"{'zenwork-checked-border':package.isChecked}\">\n\n          <div class=\"cont-check\">\n            <div>\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"package.isChecked\">\n                {{package.name}}\n              </mat-checkbox>\n\n            </div>\n          </div>\n\n        </td>\n        <td>{{package.price}}</td>\n        <td>{{package.costPerEmployee}}</td>\n        <td>{{package.subscriptionType}}</td>\n        <td>{{package.status}}</td>\n        <!-- <td>{{package.startDate | date}}</td>\n        <td>{{package.endDate | date}}</td> -->\n        <td>--</td>\n        <td>--</td>\n        <td></td>\n      </tr>\n\n  </table>\n  <mat-paginator [length]=\"packagesCount\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\" (page)=\"pageEventsforPackages($event)\">\n  </mat-paginator>\n\n  <div class=\"tax-rate\">\n    <h2>Zenwork Tax</h2>\n    <ul>\n      <li class=\"col-md-2\">\n        <label>Tax Rate</label>\n        <input type=\"text\" placeholder=\"tax-rate\" maxlength=\"3\" (keypress)=\"keyPress($event)\" [(ngModel)]=\"taxrate.tax_rate\"\n          class=\"form-control\">\n      </li>\n      <li class=\"col-md-2\">\n        <a class=\"btn\" (click)=\"taxrateSend()\" type=\"submit\">Submit</a>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n\n  </div>\n\n  <h5>coupons</h5>\n\n  <table class=\"table\">\n    <thead>\n      <tr>\n        <th>\n          <div class=\"cont-check\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"selectAllCoupons\" (change)=\"selectAll()\">\n              Coupon Name\n            </mat-checkbox>\n          </div>\n        </th>\n        <th>Coupon %</th>\n        <th>Coupon $</th>\n        <th>Start Date</th>\n        <th>End Date</th>\n        <th>\n          <div class=\"setting-drop\">\n            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </a>\n            <ul class=\"dropdown-menu\">\n              <li>\n                <a routerLink=\"/super-admin/super-admin-dashboard/settings/coupons-add\">Add</a>\n              </li>\n              <li>\n                <a (click)=\"editCoupon()\">Edit</a>\n              </li>\n              <li>\n                <a (click)=\"deleteAdons()\">Delete</a>\n              </li>\n              <!-- <li>\n              <a>Delete</a>\n            </li>\n            <li>\n              <a>Copy</a>\n            </li> -->\n            </ul>\n          </div>\n        </th>\n      </tr>\n    </thead>\n    <tbody>\n\n\n      <tr *ngFor=\"let coupon of coupons\">\n        <td [ngClass]=\"{'zenwork-checked-border':coupon.isChecked}\">\n\n          <div class=\"cont-check\">\n            <div>\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"coupon.isChecked\">\n                {{coupon.name}}\n              </mat-checkbox>\n\n            </div>\n          </div>\n\n        </td>\n        <td *ngIf=\"coupon.couponType == 'Percentage'\">{{coupon.percentage}} % </td>\n        <td *ngIf=\"coupon.couponType != 'Percentage'\">-- </td>\n        <td *ngIf=\"coupon.couponType == 'Value'\">$ {{coupon.flatDiscountAmount}}</td>\n        <td *ngIf=\"coupon.couponType != 'Value'\">-- </td>\n        <td>{{coupon.startDate | date}}</td>\n        <td>{{coupon.endDate | date}}</td>\n        \n        <td></td>\n      </tr>\n\n  </table>\n\n  <mat-paginator [length]=\"countforCoupon\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\" (page)=\"pageEventsforCoupons($event)\">\n  </mat-paginator>\n\n\n\n</div>\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/packages/packages.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/packages/packages.component.ts ***!
  \***************************************************************************************/
/*! exports provided: PackagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackagesComponent", function() { return PackagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/packageAndAddon.service */ "./src/app/services/packageAndAddon.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PackagesComponent = /** @class */ (function () {
    function PackagesComponent(packageAndAddOnService, swalAlertService, loaderService, router) {
        this.packageAndAddOnService = packageAndAddOnService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.router = router;
        this.selectedPackageDetails = [];
        this.selectAllCoupons = false;
        this.selectPackages = false;
        this.deleteCouponsArray = [];
        this.taxrate = {
            tax_rate: '',
            _id: ''
        };
        this.deletedIds = [];
    }
    PackagesComponent.prototype.ngOnInit = function () {
        this.pageNoforPackage = 1;
        this.perPageforPackage = 5;
        this.pageNoforCoupon = 1;
        this.perPageforCoupon = 5;
        this.getAllPackages();
        this.getAllCoupons();
        this.getTaxRate();
    };
    /* Description: mat pagination for packages
   author : vipin reddy */
    PackagesComponent.prototype.pageEventsforPackages = function (event) {
        console.log(event);
        this.pageNoforPackage = event.pageIndex + 1;
        this.perPageforPackage = event.pageSize;
        this.getAllPackages();
    };
    /* Description: mat pagination for coupons
 author : vipin reddy */
    PackagesComponent.prototype.pageEventsforCoupons = function (event) {
        console.log(event);
        this.pageNoforCoupon = event.pageIndex + 1;
        this.perPageforCoupon = event.pageSize;
        this.getAllCoupons();
    };
    /* Description: get all packages list view
   author : vipin reddy */
    PackagesComponent.prototype.getAllPackages = function () {
        var _this = this;
        this.loaderService.loader(true);
        var postData = {
            page_no: this.pageNoforPackage,
            per_page: this.perPageforPackage
        };
        this.packageAndAddOnService.getAllPackagesforSettings(postData)
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.packages = res.data;
                _this.packagesCount = res.count;
                _this.packages.forEach(function (packageData) {
                    packageData.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Packages", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: eidt Update package
 author : vipin reddy */
    PackagesComponent.prototype.editPackage = function () {
        this.selectedPackageDetails = this.packages.filter(function (packageData) {
            return packageData.isChecked;
        });
        if (this.selectedPackageDetails.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select only one package to Proceed", 'error');
        }
        else if (this.selectedPackageDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select a package to Proceed", 'error');
        }
        else {
            this.router.navigate(['/super-admin/super-admin-dashboard/settings/packages-add/' + this.selectedPackageDetails[0]._id]);
        }
    };
    PackagesComponent.prototype.packagesLenght = function () {
        this.selectedPackageDetails = this.packages.filter(function (packageData) {
            return packageData.isChecked;
        });
    };
    /* Description: delete package
 author : vipin reddy */
    PackagesComponent.prototype.deletePackages = function () {
        var _this = this;
        this.selectedPackageDetails = this.packages.filter(function (packageData) {
            return packageData.isChecked;
        });
        console.log("all packges ids", this.selectedPackageDetails);
        this.selectedPackageDetails.forEach(function (element) {
            _this.deletedIds.push(element._id);
        });
        if (this.deletedIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete Package", "Select a package to Proceed", 'error');
        }
        if (this.deletedIds.length >= 1) {
            console.log(this.deletedIds);
            var postData = {
                "_ids": this.deletedIds
            };
            this.packageAndAddOnService.deletePackages(postData)
                .subscribe(function (res) {
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation('Package', res.message, 'success');
                    _this.getAllPackages();
                }
            }, function (err) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Package', err.error.message, 'error');
            });
        }
    };
    /* Description: delete multiple addons
   author : vipin reddy */
    PackagesComponent.prototype.deleteAdons = function () {
        var _this = this;
        this.deleteCouponsArray = this.coupons.filter(function (adonData) {
            return adonData.isChecked;
        });
        console.log("all packges ids", this.deleteCouponsArray);
        var deletedIds = [];
        this.deleteCouponsArray.forEach(function (element) {
            deletedIds.push(element._id);
        });
        console.log(deletedIds);
        if (deletedIds.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Delete Coupons", "Select a Coupon to Proceed", 'error');
        }
        if (deletedIds.length >= 1) {
            var postData = {
                "_ids": deletedIds
            };
            this.packageAndAddOnService.deleteCoupons(postData)
                .subscribe(function (res) {
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation('Coupons', res.message, 'success');
                    _this.getAllCoupons();
                }
            }, function (err) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Coupons', err.error.message, 'error');
            });
        }
    };
    /* Description: taxrate sending
 author : vipin reddy */
    PackagesComponent.prototype.taxrateSend = function () {
        var _this = this;
        console.log("taxrate", this.taxrate);
        var postdata = {
            tax_rate: this.taxrate.tax_rate,
            _id: this.taxrate._id
        };
        this.packageAndAddOnService.updateTaxRate(postdata)
            .subscribe(function (res) {
            console.log("Response", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('Taxrate', res.message, 'success');
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation('Taxrate', err.error.message, 'error');
        });
    };
    /* Description: accept only numbers
 author : vipin reddy */
    PackagesComponent.prototype.keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    /* Description: get all coupons for list view
   author : vipin reddy */
    PackagesComponent.prototype.getAllCoupons = function () {
        var _this = this;
        this.loaderService.loader(true);
        var postData = {
            page_no: this.pageNoforCoupon,
            per_page: this.perPageforCoupon
        };
        this.packageAndAddOnService.getAllCouponsforAll(postData)
            .subscribe(function (res) {
            console.log("Response", res);
            _this.loaderService.loader(false);
            if (res.status) {
                _this.coupons = res.data;
                _this.countforCoupon = res.count;
                _this.coupons.forEach(function (couponData) {
                    couponData.isChecked = false;
                });
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Coupons", res.message, 'error');
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    /* Description: edit update coupon
   author : vipin reddy */
    PackagesComponent.prototype.editCoupon = function () {
        this.selectCouponDetails = this.coupons.filter(function (couponData) {
            return couponData.isChecked;
        });
        if (this.selectCouponDetails.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Coupons", "Select only one coupon to Proceed", 'error');
        }
        else if (this.selectCouponDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Coupons", "Select a coupon to Proceed", 'error');
        }
        else {
            this.router.navigate(['/super-admin/super-admin-dashboard/settings/coupons-add/' + this.selectCouponDetails[0]._id]);
        }
    };
    /* Description: select all packages
   author : vipin reddy */
    PackagesComponent.prototype.selectAllPackages = function () {
        var _this = this;
        this.packages.forEach(function (packageData) {
            packageData.isChecked = _this.selectPackages;
        });
    };
    /* Description: select all coupons
   author : vipin reddy */
    PackagesComponent.prototype.selectAll = function () {
        var _this = this;
        this.coupons.forEach(function (couponData) {
            couponData.isChecked = _this.selectAllCoupons;
        });
    };
    /* Description: get tax rate
   author : vipin reddy */
    PackagesComponent.prototype.getTaxRate = function () {
        var _this = this;
        this.packageAndAddOnService.getTaxRate()
            .subscribe(function (res) {
            console.log("Response", res);
            _this.taxrate = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    PackagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-packages',
            template: __webpack_require__(/*! ./packages.component.html */ "./src/app/super-admin-dashboard/package-creation/packages/packages.component.html"),
            styles: [__webpack_require__(/*! ./packages.component.css */ "./src/app/super-admin-dashboard/package-creation/packages/packages.component.css")]
        }),
        __metadata("design:paramtypes", [_services_packageAndAddon_service__WEBPACK_IMPORTED_MODULE_1__["PackageAndAddOnService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], PackagesComponent);
    return PackagesComponent;
}());



/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".setting-table{\n    height: 100vh;\n}"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"setting-table\">\n\n  <table class=\"table\">\n    <thead>\n      <tr>\n        <th>\n          <div class=\"cont-check\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"checkAllAddOns\" (change)=\"selectAll()\">\n                  Profile Name\n              </mat-checkbox>\n          </div>\n        </th>\n        <th>Profile Description</th>\n        \n        <th>\n          <div class=\"setting-drop\">\n            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n              <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </a>\n            <ul class=\"dropdown-menu\">\n              <li>\n                <a (click)=\"addEditUser('data')\">Add</a>\n              </li>\n              <li>\n                <a (click)=\"editUserProfile()\">Edit</a>\n              </li>\n              <!-- <li>\n                <a>Delete</a>\n              </li>\n              <li>\n                <a>Copy</a>\n              </li> -->\n            </ul>\n          </div>\n        </th>\n      </tr>\n    </thead>\n    <tbody>\n\n      <tr *ngFor=\"let profile of userProfile\">\n        <td [ngClass]=\"{'zenwork-checked-border':profile.isChecked}\">\n\n          <div class=\"cont-check\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"profile.isChecked\">\n                  {{profile.name}}\n              </mat-checkbox>\n          </div>\n\n        </td>\n        <td>{{profile.description}}</td>\n        \n        <td></td>\n      </tr>\n  </table>\n\n  \n\n</div>"

/***/ }),

/***/ "./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: UserProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileComponent", function() { return UserProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _create_user_profile_template_create_user_profile_template_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../create-user-profile-template/create-user-profile-template.component */ "./src/app/super-admin-dashboard/package-creation/create-user-profile-template/create-user-profile-template.component.ts");
/* harmony import */ var _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/zenworkers.service */ "./src/app/services/zenworkers.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/loader.service */ "./src/app/services/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(dialog, zenworkersService, swalAlertService, loaderService) {
        this.dialog = dialog;
        this.zenworkersService = zenworkersService;
        this.swalAlertService = swalAlertService;
        this.loaderService = loaderService;
        this.checkAllAddOns = false;
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        this.loaderService.loader(true);
        this.getAllRoles();
    };
    /* Description: get all levels
   author : vipin reddy */
    UserProfileComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.zenworkersService.getAllroles()
            .subscribe(function (res) {
            console.log(res);
            if (res.status == true) {
                _this.loaderService.loader(false);
                _this.userProfile = res.data;
                _this.userProfile.forEach(function (addonsData) {
                    addonsData.isChecked = false;
                });
            }
        }, function (err) {
            console.log('Err', err);
            _this.loaderService.loader(false);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Error", err.error.message, 'error');
        });
    };
    // this.selectedAddOnDetails = this.addons.filter(addonsData => {
    //   return addonsData.isChecked;
    // })
    /* Description: addedit level popup
   author : vipin reddy */
    UserProfileComponent.prototype.addEditUser = function (data) {
        var _this = this;
        var dialogRef = this.dialog.open(_create_user_profile_template_create_user_profile_template_component__WEBPACK_IMPORTED_MODULE_2__["CreateUserProfileTemplateComponent"], {
            height: '700px',
            width: '890px',
            data: data,
            autoFocus: false
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Result", result);
            _this.getAllRoles();
        });
    };
    /* Description: single edit for level
   author : vipin reddy */
    UserProfileComponent.prototype.editUserProfile = function () {
        this.selectedAddOnDetails = this.userProfile.filter(function (addonsData) {
            console.log(addonsData.isChecked);
            // this.selectedAddOnDetails[0]._id]
            return addonsData.isChecked;
        });
        if (this.selectedAddOnDetails.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select only one profile to Proceed", 'error');
        }
        else if (this.selectedAddOnDetails.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Edit Package", "Select one profile to Proceed", 'error');
        }
        else {
            console.log(this.selectedAddOnDetails[0]._id);
            this.addEditUser(this.selectedAddOnDetails[0]._id);
        }
    };
    /* Description: select all all levels
   author : vipin reddy */
    UserProfileComponent.prototype.selectAll = function () {
        var _this = this;
        this.userProfile.forEach(function (addonsData) {
            addonsData.isChecked = _this.checkAllAddOns;
        });
    };
    UserProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-profile',
            template: __webpack_require__(/*! ./user-profile.component.html */ "./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.html"),
            styles: [__webpack_require__(/*! ./user-profile.component.css */ "./src/app/super-admin-dashboard/package-creation/user-profile/user-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_zenworkers_service__WEBPACK_IMPORTED_MODULE_3__["ZenworkersService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]])
    ], UserProfileComponent);
    return UserProfileComponent;
}());



/***/ })

}]);
//# sourceMappingURL=package-creation-package-creation-module.js.map