(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["manager-self-service-manager-self-service-module"],{

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.css":
/*!********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.css ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".manager-front { margin: 0 auto; float: none; padding: 0;}\n\n\n.text-area{\n    margin:15px 30px;\n}\n\n\n.zenwork-buttons{\n    margin: 10px 25px 0;\n}\n\n\n.submit-btn{\n    font-size: 14px;\n    padding: 0px 22px !important;\n    border-radius: 25px;\n    margin: 0 10px 0 0;\n    border: 1px solid #439348;\n    color: #439348;background: #fff;\n}\n\n\n.cancel-btn{\n    font-size: 12px;\n    padding: 5px 12px !important;\n    border-radius: 25px;\n}\n\n\n.personal { margin: 30px auto 40px; float: none;}\n\n\n.personal .panel-default>.panel-heading { background-color:transparent !important; border: none;}\n\n\n.personal .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n\n\n.personal .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n\n\n.personal .panel-title>.small, .personal .panel-title>.small>a, .personal .panel-title>a, .personal .panel-title>small, .personal .panel-title>small>a { text-decoration:none;}\n\n\n.personal .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:none;}\n\n\n.personal .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n\n\n.personal .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n\n.personal .panel-group .panel-heading+.panel-collapse>.list-group, .personal .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n\n.personal .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n\n\n.personal .panel-body { padding: 0 0 5px;padding: 15px;}\n\n\n.personal .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n\n.personal .panel-default { border-color:transparent; background: #fff; \n    padding: 15px 25px;}\n\n\n.personal .panel-group .panel+.panel { margin-top: 20px;}\n\n\n.employee-mission ul { display: block;}\n\n\n.employee-mission ul.list-inline { margin-left: 0;}\n\n\n.employee-mission ul li {\n    margin: 0px 30px 0 30px;\n}\n\n\n.employee-mission ul li .date span { border: none !important;}\n\n\n.employee-mission ul li small { display: inline-block; margin: 0 0 0 15px;}\n\n\n.employee-mission ul li small .form-control { border: none; font-size: 16px;box-shadow: none; height: auto; padding: 8px 12px;\nbackground-color:#fff;}\n\n\n.employee-mission ul li small .form-control:focus{ box-shadow: none; border-color:transparent;}\n\n\n.date { height: 38px !important; line-height:inherit !important;}\n\n\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n\n.employee-mission ul li .date .form-control { width:70%; height: auto;}\n\n\n.employee-mission ul li .date .form-control:focus { box-shadow: none; border: none;}\n\n\n.employee-mission ul li .date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n\n\n.manager-link-main {\n    border-bottom: 1px solid #e8dddd;\n    padding-bottom: 15px;\n    margin: 20px 0 0;\n}\n\n\n.manager-link-main label { padding: 0 0 15px; display: block; font-size: 17px;}\n\n\n.content-field {\n    margin-top: 10px;\n    border: none;\n    box-shadow: none;\n}\n\n\n.custom-tables { margin: 0 auto; float: none; padding: 0 30px 0 5px;}\n\n\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n\n\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n\n\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n\n\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n\n\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 13px;}\n\n\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n\n\n.custom-tables .table>tbody>tr>td .form-control { border: none; width: 80%; box-shadow: none;background: #f8f8f8;\nheight: auto; padding:12px; font-size: 15px;}\n\n\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n\n\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n\n\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n\n\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n\n\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n\n\n.save-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;\n    margin: 0 0 0 10px; \n}\n\n\n.label-font {\n    font-size: 15px;\n}\n\n\n.cancel { color: #e44a49;}\n\n\n.text-area .form-control { font-size: 15px; line-height:25px; padding: 8px 30px 8px; background: #fff;height: auto;\n    resize: none;\n    border: none;\n    box-shadow: none;}\n\n\n.announce-top { margin: 0 0 20px;background: #f8f8f8; padding: 20px 0 15px;}\n\n\n.zenwork-checked-border:after {\n    top:-11px;\n    left: -80px;\n    border-right: #83c7a1 4px solid;\n    right: auto;\n    height:55px;\n    border-radius: 4px;\n    padding: 25px;\n}\n\n\n.zenwork-customized-ccheckbox { margin: 0 0 0 30px;}\n\n\n.zenwork-checked { position: relative;}\n\n\n.zenwork-checked:after {\n    content:'';\n    position: absolute;\n    top:8px;\n    left: -48px;\n    border-right: #83c7a1 4px solid;\n    right: auto;\n    height:55px;\n    border-radius: 4px;\n    padding: 25px;\n}\n\n\n.error { color: #e44a49;}\n\n\n.show { margin: 20px 0;}\n\n\n.back { color: #e44a49; font-size: 14px;}\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"manager-front col-md-11\">\n\n  <div class=\"personal\">\n    <div class=\"panel-group\" id=\"accordion10\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h4 class=\"panel-title\">\n            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion10\" href=\"#collapseTen\">\n              Manager Announcement\n            </a>\n            <span class=\"pull-right\">\n              <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n              <mat-menu #menu=\"matMenu\">\n                <button mat-menu-item (click)=\"addFields()\" >Add</button>\n                <button mat-menu-item (click)=\"editFields()\">Edit</button>\n                <button mat-menu-item (click)=\"delete()\" >Delete</button>\n              </mat-menu>\n            </span>\n          </h4>\n        </div>\n        <div id=\"collapseTen\" class=\"panel-collapse collapse in\">\n          <div class=\"panel-body\">\n\n            <div class=\"announce-top\" *ngFor=\"let announce of addList ; let i=index;\">\n\n              <div class=\"employee-mission\">\n                <ul class=\"list-unstyled list-inline border-bottom\">\n                  <li class=\"col-md-3\" [ngClass]=\"{'zenwork-checked-border':announce.isChecked}\">\n                    <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"announce.isChecked\"\n                      style=\"margin-top:10px;display:inline-block;\" (change)=\"managerSelectId($event,announce._id)\">\n                    </mat-checkbox>\n                    <small>\n                      <input class=\"form-control\" placeholder=\"Title\" [(ngModel)]=\"announce.title\"\n                        [readOnly]=\"announce.editValue\">\n                    </small>\n                  </li>\n\n                  <li>\n                    <label class=\"label-font\">Schedule Date</label>\n                  </li>\n\n                  <li style=\"margin:0px\">\n                    <div class=\"date\">\n                      <input matInput [matDatepicker]=\"picker21\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                        [(ngModel)]=\"announce.scheduled_date\" [disabled]=\"announce.editValue\" [min]=\"pastDate\">\n                      <mat-datepicker #picker21></mat-datepicker>\n                      <button mat-raised-button (click)=\"picker21.open()\">\n                        <span>\n                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                        </span>\n                      </button>\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n\n                  <li>\n                    <label class=\"label-font\">Expiration Date</label>\n                  </li>\n                  <li style=\"margin:0px\">\n                    <div class=\"date\">\n                      <input matInput [matDatepicker]=\"picker22\" placeholder=\"mm / dd / yy\" class=\"form-control\"\n                        [(ngModel)]=\"announce.expiration_date\" [disabled]=\"announce.editValue\"  [min]=\"announce.scheduled_date\">\n                      <mat-datepicker #picker22></mat-datepicker>\n                      <button mat-raised-button (click)=\"picker22.open()\">\n                        <span>\n                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                        </span>\n                      </button>\n                      <div class=\"clearfix\"></div>\n                    </div>\n                  </li>\n\n                </ul>\n              </div>\n\n              <div class=\"text-area\">\n                <textarea class=\"form-control\" rows=\"4\" placeholder=\"Description...\" [(ngModel)]=\"announce.description\"\n                  [readOnly]=\"announce.editValue\"></textarea>\n\n              </div>\n\n              <div class=\"zenwork-buttons pull-right\" *ngIf=\"!announce.disabledButtons\">\n                  <button mat-button class=\"cancel\" *ngIf=\"announce._id\" (click)=\"resetClick()\"  [disabled]=\"announce.disabledButtons\" >Reset</button>\n                <button mat-button class=\"cancel\" *ngIf=\"!announce._id\" (click)=\"cancelData(i)\"  [disabled]=\"announce.disabledButtons\" >Cancel</button>\n                <button mat-button class=\"submit-btn\" (click)=\"announceSubmit(announce)\" [disabled]=\"announce.disabledButtons\" >Submit</button>\n\n              </div>\n              <div class=\"clearfix\"></div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n  </div>\n\n  <div>\n    <h4>Additional Content</h4>\n    <div class=\"row border-bottom\" style=\"margin-top: 20px;\">\n      <div class=\"col-xs-2\">\n        <label class=\"label-font\">Show Team Summary?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_team_summary\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n      <div class=\"col-xs-2\">\n        <label class=\"label-font\">Show To-Do List?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_to_do_list\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n      <div class=\"col-xs-2\">\n        <label class=\"label-font\">Show Today's Report?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_todays_reports\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n      <div class=\"col-xs-3\">\n        <label class=\"label-font\">Show Employement Type?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_employment_type\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n    </div>\n    <div class=\"row border-bottom\" style=\"margin-top: 20px;\">\n      <div class=\"col-xs-2\">\n        <label class=\"label-font\">Show Birthdays?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_birthdays\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n      <div class=\"col-xs-3\">\n        <label class=\"label-font\">Select Employees-Birthday</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.birthday_settings\">\n          <mat-option value=\"My Team Only\">My Team Only</mat-option>\n          <mat-option value=\"My Department Only\">My Department Only</mat-option>\n          <mat-option value=\"Entire Company\">Entire Company</mat-option>\n        </mat-select>\n      </div>\n\n      <div class=\"col-xs-3\">\n        <label class=\"label-font\">Show Work Anniversary?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_work_anniversary\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n      <div class=\"col-xs-3\">\n        <label class=\"label-font\">Select Employees-Work Anniversary</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.work_anniversary_settings\">\n          <mat-option value=\"My Team Only\">My Team Only</mat-option>\n          <mat-option value=\"My Department Only\">My Department Only</mat-option>\n          <mat-option value=\"Entire Company\">Entire Company</mat-option>\n        </mat-select>\n      </div>\n\n\n\n      <div class=\"col-xs-2 show\">\n        \n        <label class=\"label-font\">Show Manager Links?</label>\n        <mat-select class=\"form-control content-field\" [(ngModel)]=\"additionContents.show_manager_links\">\n          <mat-option [value]=\"true\">Yes</mat-option>\n          <mat-option [value]=\"false\">No</mat-option>\n        </mat-select>\n      </div>\n\n      <div class=\"clearfix\"></div>\n      \n      <button mat-button class=\"save-btn\"  (click)=\"additionalSave()\">Save</button>\n\n    </div>\n\n    <div class=\"row manager-link-main\">\n\n        <label>Manager Links</label>\n\n        <div class=\"custom-tables\">\n          \n          <table class=\"table\">\n            <thead>\n              <tr>\n                <th>\n\n                </th>\n                <th>Site Name</th>\n                <th>Site Link</th>\n                <th>\n                  <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu2\">more_vert</mat-icon>\n                  <mat-menu #menu2=\"matMenu\">\n                    <button mat-menu-item (click)=\"managerAdd()\">Add</button>\n                    <button mat-menu-item (click)=\"managerEdit()\">Edit</button>\n                    <button mat-menu-item (click)=\"managerDelete()\">Delete</button>\n                  </mat-menu>\n                </th>\n              </tr>\n            </thead>\n            <tbody>\n\n              <tr *ngFor=\"let links of additionalData.manager_links\">\n                <td [ngClass]=\"{'zenwork-checked':links.isChecked}\">\n                  <mat-checkbox class=\"zenwork-customized-ccheckbox\" [(ngModel)]=\"links.isChecked\"\n                    (ngModelChange)=\"managerID(links._id,links)\"></mat-checkbox>\n                </td>\n                <td>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Site name\" [(ngModel)]=\"links.site_name\"\n                    [readOnly]=\"links.edit\" name=\"siteName\" required #siteName=\"ngModel\">\n                    <p *ngIf=\"!links.site_name && siteName.touched ||(!links.site_name && isValid)\" class=\"error\">Please enter Sitename</p>\n                </td>\n                <td>\n                  <input type=\"url\" placeholder=\"Enter site URL\" name=\"site\" #site=\"ngModel\" required\n                    pattern=\"https?://.+\" class=\"form-control\" [(ngModel)]=\"links.site_link\" [readOnly]=\"links.edit\">\n                  <p *ngIf=\"!links.site_link && site.touched || (!links.site_link && isValid)\" class=\"error\"> Please\n                    enter Sitelink</p>\n                </td>\n                <td></td>\n              </tr>\n\n          </table>\n\n        </div>\n      \n    </div>\n\n    <div class=\"row col-md-12\" style=\"margin:20px 0px;\">\n\n      <button mat-button (click)=\"mssBackClick()\" class=\"back\">Back</button>\n      <button mat-button class=\"save-btn pull-right\" (click)=\"managerSave()\">Save</button>\n      <div class=\"clear-fix\"></div>\n    </div>\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: ManagerFrontComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerFrontComponent", function() { return ManagerFrontComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_frontpage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/manager-frontpage.service */ "./src/app/services/manager-frontpage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ManagerFrontComponent = /** @class */ (function () {
    function ManagerFrontComponent(managerFrontPage, swalService, router) {
        this.managerFrontPage = managerFrontPage;
        this.swalService = swalService;
        this.router = router;
        this.addList = [
            {
                editValue: false,
                disabledButtons: false
            }
        ];
        this.managerAnnounceID = [];
        this.deleteSelectId = [];
        this.selectData = [];
        this.additionalData = [
            {
                edit: false
            },
        ];
        this.isValid = false;
        this.additionContents = {
            show_team_summary: '',
            show_to_do_list: '',
            show_todays_reports: '',
            show_employment_type: '',
            show_birthdays: '',
            birthday_settings: '',
            show_work_anniversary: '',
            work_anniversary_settings: '',
            show_manager_links: ''
        };
        this.managerLinkID = [];
    }
    ManagerFrontComponent.prototype.ngOnInit = function () {
        this.pastDate = new Date();
        // this.managerLinks = [
        //   {
        //     site_name: '',
        //     site_link: ''
        //   }
        // ]
        this.cID = localStorage.getItem('companyId');
        console.log("IDDDDDDD", this.cID);
        this.cID = this.cID.replace(/^"|"$/g, "");
        console.log("cin222222222", this.cID);
        this.userID = localStorage.getItem('employeeId');
        console.log("userrr iddd", this.userID);
        this.userID = this.userID.replace(/^"|"$/g, "");
        console.log("cin222222222", this.userID);
        this.getAllAnnouncement();
        this.getAllAdditionalContent();
    };
    // Author:Suresh M, Date:25-06-19
    // All Announcements
    ManagerFrontComponent.prototype.getAllAnnouncement = function () {
        var _this = this;
        console.log("Userrrrrrr", this.userID);
        this.managerFrontPage.getAllData(this.cID, this.userID)
            .subscribe(function (res) {
            console.log("get dataa", res);
            _this.addList = res.data;
            console.log("responsivee get data", _this.addList);
            _this.addList.filter(function (item) {
                item.editValue = true;
                item.disabledButtons = true;
            });
            console.log("responsivee get data", _this.addList);
        });
    };
    // Author:Suresh M, Date:25-06-19
    // add data
    ManagerFrontComponent.prototype.addFields = function () {
        this.addList.push({ scheduled_date: '', expiration_date: '', description: '', disabledButtons: false });
    };
    // Author:Suresh M, Date:25-06-19
    // Announcements submit
    ManagerFrontComponent.prototype.announceSubmit = function (announce) {
        var _this = this;
        console.log("anounce", announce);
        var postData = {
            companyId: this.cID,
            description: announce.description,
            title: announce.title,
            expiration_date: announce.expiration_date,
            scheduled_date: announce.scheduled_date
        };
        if (announce._id) {
            postData._id = announce._id;
            this.managerFrontPage.editAnnounceData(postData)
                .subscribe(function (res) {
                console.log("Editt data", res);
                _this.getAllAnnouncement();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Edit", "Successfull Edit Data", "success");
                }
            });
        }
        else {
            this.managerFrontPage.submitData(postData)
                .subscribe(function (res) {
                console.log("submit dataaa", res);
                _this.getAllAnnouncement();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Added", "Successfully Added", "success");
                }
            }, (function (err) {
                _this.swalService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            }));
        }
    };
    // Author:Suresh M, Date:25-06-19
    // Edit data
    ManagerFrontComponent.prototype.editFields = function () {
        this.selectData = this.addList.filter(function (announceCheck) {
            return announceCheck.isChecked;
        });
        if (this.selectData.length > 1) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select only one data", "error");
        }
        else if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one", "error");
        }
        else {
            if (this.selectData[0]._id) {
                this.selectData[0].editValue = false;
                this.selectData[0].disabledButtons = false;
            }
        }
    };
    // Author:Suresh M, Date:25-06-19
    // Delete
    ManagerFrontComponent.prototype.delete = function () {
        var _this = this;
        this.selectData = this.addList.filter(function (announceCheck) {
            return announceCheck.isChecked;
        });
        console.log("selecttt", this.selectData);
        if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Select any data", "error");
        }
        else {
            var deleteData = {
                companyId: this.cID,
                userId: this.userID,
                _ids: this.deleteSelectId
            };
            this.managerFrontPage.deleteData(deleteData)
                .subscribe(function (res) {
                console.log("deletee", res);
                _this.getAllAnnouncement();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully Deleted", "success");
                }
                (function (err) {
                });
            });
        }
    };
    // Author:Suresh M, Date:25-06-19
    // select id
    ManagerFrontComponent.prototype.changeSelect = function (announce_id) {
        console.log("Changeeeeeeee", announce_id);
        this.checkID = announce_id;
        console.log("Chnage IDDDDDD", this.checkID);
    };
    // Author:Suresh M, Date:25-06-19
    // cancel data 
    ManagerFrontComponent.prototype.cancelData = function (i) {
        this.addList.splice(i, 1);
        console.log("Cancelll", i);
    };
    ManagerFrontComponent.prototype.resetClick = function () {
        this.getAllAnnouncement();
    };
    ManagerFrontComponent.prototype.managerSelectId = function (event, id) {
        console.log("managerrr", event, id);
        if (event.checked == true) {
            this.deleteSelectId.push(id);
        }
        console.log("pushh idd", this.deleteSelectId);
        if (event.checked == false) {
            var index = this.deleteSelectId.indexOf(id);
            if (index > -1) {
                this.deleteSelectId.splice(index, 1);
                this.addList[0].disabledButtons = true;
                this.addList[0].editValue = true;
            }
            console.log("uncheckkkk value", this.deleteSelectId);
        }
    };
    // Author:Suresh M, Date:25-06-19
    // All Additional contents
    ManagerFrontComponent.prototype.getAllAdditionalContent = function () {
        var _this = this;
        this.managerFrontPage.getAllAdditionalData(this.cID)
            .subscribe(function (res) {
            console.log("get Additionals dataa", res);
            _this.additionalData = res.data;
            _this.additionContents = _this.additionalData;
            if (_this.additionalData.manager_links) {
                _this.additionalData.manager_links.filter(function (item) {
                    item.edit = true;
                });
            }
        });
    };
    // Author:Suresh M, Date:25-06-19
    // additional submit
    ManagerFrontComponent.prototype.additionalSave = function () {
        var _this = this;
        var postAdditional = {
            companyId: this.cID,
            show_team_summary: this.additionContents.show_team_summary,
            show_to_do_list: this.additionContents.show_to_do_list,
            show_todays_reports: this.additionContents.show_todays_reports,
            show_employment_type: this.additionContents.show_employment_type,
            show_birthdays: this.additionContents.show_birthdays,
            birthday_settings: this.additionContents.birthday_settings,
            show_work_anniversary: this.additionContents.show_work_anniversary,
            work_anniversary_settings: this.additionContents.work_anniversary_settings,
            show_manager_links: this.additionContents.show_manager_links
        };
        this.managerFrontPage.additionalDataSave(postAdditional)
            .subscribe(function (res) {
            console.log("additional submit", res);
            if (res.status == true) {
                _this.swalService.SweetAlertWithoutConfirmation("Successfully Updates Manager Settings", "", "");
            }
        });
    };
    // Author:Suresh M, Date:25-06-19
    // manager links Add
    ManagerFrontComponent.prototype.managerAdd = function () {
        this.isValid = false;
        this.managerLinks = {
            site_link: '',
            site_name: '',
        };
        if (!this.additionalData.manager_links) {
            this.additionalData.manager_links = [];
        }
        this.additionalData.manager_links.push(this.managerLinks);
    };
    // Author:Suresh M, Date:25-06-19
    // manager links submit
    ManagerFrontComponent.prototype.managerSave = function () {
        var _this = this;
        this.isValid = true;
        if (this.selectData && this.selectData.length && this.selectData[0]._id) {
            var managerEditData = {
                companyId: this.cID,
                site_name: this.managerEditLinks.site_name,
                site_link: this.managerEditLinks.site_link,
                linkId: this.managerLinkID
            };
            this.managerFrontPage.managerLinkEdit(managerEditData)
                .subscribe(function (res) {
                console.log("manager links edit", res);
                _this.getAllAdditionalContent();
                if (res.status == true) {
                    _this.selectData = [];
                    _this.swalService.SweetAlertWithoutConfirmation("Edited", "Successfully Edit", "success");
                }
            });
        }
        else {
            var postData = [];
            for (var i = 0; i < this.additionalData.manager_links.length; i++) {
                if (!this.additionalData.manager_links[i]._id) {
                    postData.push(this.additionalData.manager_links[i]);
                }
            }
            console.log(postData, "sendingdata");
            if (postData.length == 0) {
                this.swalService.SweetAlertWithoutConfirmation("Add", "Please Add data", "error");
            }
            else {
                if (!this.managerLinks.site_name) {
                    this.isValid = true;
                }
                else if (!this.managerLinks.site_link) {
                    this.isValid = true;
                }
                else {
                    this.managerFrontPage.managerLinksAdd(postData)
                        .subscribe(function (res) {
                        console.log("managee links add", res);
                        _this.additionalData.manager_links = [];
                        postData = [];
                        _this.getAllAdditionalContent();
                        _this.isValid = true;
                        if (res.status == true) {
                            _this.swalService.SweetAlertWithoutConfirmation("Added", "Successfully Added", "success");
                        }
                    });
                }
            }
        }
    };
    // Author:Suresh M, Date:25-06-19
    // manager links edit
    ManagerFrontComponent.prototype.managerEdit = function () {
        this.selectData = this.additionalData.manager_links.filter(function (managerCheck) {
            return managerCheck.isChecked;
        });
        if (this.selectData.length > 1) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select only one", "error");
        }
        else if (this.selectData == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Edit", "Select any one data", "error");
        }
        else {
            if (this.selectData[0]._id) {
                this.selectData[0].edit = false;
            }
        }
    };
    // Author:Suresh M, Date:25-06-19
    // manager id
    ManagerFrontComponent.prototype.managerID = function (id, links) {
        console.log("Manager idd", id);
        this.managerLinkID = id;
        this.managerEditLinks = links;
        console.log("new manager id", this.managerLinkID);
    };
    // Author:Suresh M, Date:25-06-19
    // manager links delete
    ManagerFrontComponent.prototype.managerDelete = function () {
        var _this = this;
        this.selectData = this.additionalData.manager_links.filter(function (managerCheck) {
            return managerCheck.isChecked;
        });
        if (this.selectData.length == 0) {
            this.swalService.SweetAlertWithoutConfirmation("Delete", "Please select data", "error");
        }
        else {
            var daleteData = {
                companyId: this.cID,
                userId: this.userID,
                _ids: this.selectData
            };
            this.managerFrontPage.managerLinksDelete(daleteData)
                .subscribe(function (res) {
                console.log("manager dalete", res);
                _this.getAllAdditionalContent();
                if (res.status == true) {
                    _this.swalService.SweetAlertWithoutConfirmation("Delete", "Successfully Deleted", "success");
                }
            });
        }
    };
    ManagerFrontComponent.prototype.mssBackClick = function () {
        this.router.navigate(['/admin/admin-dashboard/employee-management']);
        window.scroll(0, 0);
    };
    ManagerFrontComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manager-front',
            template: __webpack_require__(/*! ./manager-front.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.html"),
            styles: [__webpack_require__(/*! ./manager-front.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_frontpage_service__WEBPACK_IMPORTED_MODULE_1__["ManagerFrontpageService"], _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_2__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ManagerFrontComponent);
    return ManagerFrontComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.css":
/*!************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".manager-report { margin: 0 auto; float: none;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"manager-report col-md-11\">\n\n  <p>\n    manager-reports works!\n  </p>\n\n  \n</div>\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: ManagerReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerReportsComponent", function() { return ManagerReportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_reports_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/manager-reports.service */ "./src/app/services/manager-reports.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ManagerReportsComponent = /** @class */ (function () {
    function ManagerReportsComponent(managerRepostService) {
        this.managerRepostService = managerRepostService;
    }
    ManagerReportsComponent.prototype.ngOnInit = function () {
    };
    ManagerReportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manager-reports',
            template: __webpack_require__(/*! ./manager-reports.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.html"),
            styles: [__webpack_require__(/*! ./manager-reports.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_reports_service__WEBPACK_IMPORTED_MODULE_1__["ManagerReportsService"]])
    ], ManagerReportsComponent);
    return ManagerReportsComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.css":
/*!************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".manager-request { margin: 0 auto; float: none;}\n\n.request { padding: 30px 0 0;}\n\n.request a { padding:10px 15px; background: #fff; border-radius: 4px; color:#484747;margin: 20px 15px 0 0;\n    font-size: 16px;\n    font-weight: 500;}\n\n.request a:hover{ background: #787d7a; color:#fff;}\n\n.work-flow-lft { margin:30px 0;}\n\n.work-flow-lft ul { background:#f8f8f8; padding:20px 20px 0; margin: 30px 0 0;}\n\n.work-flow-lft ul li { border-bottom:#3a3a3a dashed 1px; margin: 0 0 15px; padding: 0 0 15px;}\n\n.work-flow-lft ul li h6{ margin: 0 0 5px; font-size: 15px;}\n\n.work-flow-lft ul li.border{ border: none;}\n\n.work-flow-lft ul li .form-control{ border: none; box-shadow: none; padding: 11px 12px; height: auto; width:95%; background:#fff;}\n\n.work-flow-lft ul li .form-control:focus{ box-shadow: none;}\n\n.work-flow-lft ul li span { vertical-align:middle; display: inline-block; padding: 0 25px 0 0;}\n\n.work-flow-lft ul li span .fa{ color:#c1c1c1; font-size:20px; line-height:20px; vertical-align:text-bottom;}\n\n.errors {color:#e44a49; padding:10px 0 0; font-size: 16px; margin: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"manager-request col-md-11\">\n\n  <div class=\"request\">\n    <h4>Change Request</h4>\n\n    <a href=\"#\" class=\"pull-left\" data-toggle=\"modal\" (click)=\"jobchangePopup()\" data-target=\"#myModal\">Job Change</a>\n    <a href=\"#\" class=\"pull-left\" data-toggle=\"modal\" (click)=\"jobchangePopup()\" data-target=\"#myModal1\">Salary\n      Change</a>\n\n  </div>\n\n</div>\n\n\n<!-- Job chnage Popup-->\n\n<div class=\"job-change-popup\">\n\n  <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Job Change Request</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"job-main\">\n\n            <form [formGroup]=\"jobChangeForm\">\n\n              <div class=\"job-change\">\n\n                <div class=\"job-change-in col-md-11\">\n\n                  <ul>\n\n                    <li>\n                      <label>Select Employee Name</label>\n                      <mat-select class=\"form-control\" placeholder=\"Employee Name\"\n                        (selectionChange)=\"selectEmployee($event)\">\n                        <mat-option *ngFor=\"let emp of employeeData\" [value]=\"emp._id\">{{emp.personal.name.preferredName}}\n                        </mat-option>\n                      </mat-select>\n\n                    </li>\n\n                    <li class=\"col-md-6\">\n                      <label>Current Status</label>\n                      <div class=\"job-status\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>Job Title</th>\n                              <th>Current Value</th>\n                              <th>Effective Date</th>\n                            </tr>\n                          </thead>\n                          <tbody>\n\n                            <tr>\n                              <td>Job Title</td>\n                              <td>\n\n                                <span class=\"form-control\" readonly>{{empStatus.job.jobTitle}}</span>\n\n                                <!-- <mat-select class=\"form-control\">\n                                  <mat-option [value]=\"empStatus.job.jobTitle\" >{{empStatus.job.jobTitle}}</mat-option>\n                                  \n                                </mat-select> -->\n\n                              </td>\n                              <td>\n                                <div class=\"date\" readonly>\n                                  <span class=\"form-control\">{{empStatus.personal.effectiveDate | date}}</span>\n\n                                  <mat-datepicker #picker></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n                            <tr>\n                              <td>Department</td>\n                              <td>\n                                <span class=\"form-control\" readonly>{{empStatus.job.department}}</span>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <span class=\"form-control\">{{empStatus.personal.effectiveDate | date}}</span>\n                                  <mat-datepicker #picker2></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                            <tr>\n                              <td>Location</td>\n                              <td>\n                                <span class=\"form-control\" readonly>{{empStatus.job.location}}</span>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <span class=\"form-control\">{{empStatus.personal.effectiveDate | date}}</span>\n                                  <mat-datepicker #picker3></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n                          </tbody>\n                        </table>\n\n                      </div>\n                    </li>\n\n\n                    <li class=\"col-md-6\">\n                      <label>New Status</label>\n                      <div class=\"job-status new\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>Filed Name</th>\n                              <th>&nbsp; New Value &nbsp; &nbsp;</th>\n                              <th>Effective Date</th>\n                            </tr>\n                          </thead>\n                          <tbody>\n                            <tr>\n                              <td>Job Title</td>\n                              <td>\n                                <mat-select class=\"form-control\" placeholder=\"Job title\" formControlName=\"new_value\">\n                                  <mat-option *ngFor=\"let title of allJobTitleFields\" [value]=\"title.name\">\n                                    {{title.name}}\n                                  </mat-option>\n                                </mat-select>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker4\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" formControlName=\"effective_date\" [min]=\"pastDate\">\n                                  <mat-datepicker #picker4></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker4.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                            <tr>\n                              <td>Department</td>\n                              <td>\n                                <mat-select class=\"form-control\" placeholder=\"Department\" formControlName=\"new_value1\">\n                                  <mat-option *ngFor=\"let depart of allDepartments\" [value]=\"depart.name\">\n                                    {{depart.name}}\n                                  </mat-option>\n\n                                </mat-select>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker5\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" formControlName=\"effective_date1\" [min]=\"pastDate\">\n                                  <mat-datepicker #picker5></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker5.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                            <tr>\n                              <td>Location</td>\n                              <td>\n                                <mat-select class=\"form-control\" placeholder=\"Location\" formControlName=\"new_value2\">\n                                  <mat-option *ngFor=\"let location of allLocations\" [value]=\"location.name\">\n                                    {{location.name}}</mat-option>\n\n                                </mat-select>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker6\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" formControlName=\"effective_date2\" [min]=\"pastDate\">\n                                  <mat-datepicker #picker6></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker6.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n                           \n                          </tbody>\n                        </table>\n\n                        <p class=\"errors\" *ngIf=\"jobChangeForm.get('new_value').invalid && jobChangeForm.get('new_value').touched || jobChangeForm.get('effective_date').invalid && jobChangeForm.get('effective_date').touched \n                        || jobChangeForm.get('new_value1').invalid && jobChangeForm.get('new_value1').touched || jobChangeForm.get('effective_date1').invalid && jobChangeForm.get('effective_date1').touched \n                        || jobChangeForm.get('new_value2').invalid && jobChangeForm.get('new_value2').touched || jobChangeForm.get('effective_date2').invalid && jobChangeForm.get('effective_date2').touched\">Please fill the all new status values</p>\n                        \n                      </div>\n                    </li>\n                    <div class=\"clearfix\"></div>\n\n\n                    <li>\n                      <label>Does the change impact salary</label>\n                      <mat-select class=\"form-control\" formControlName=\"impactsSalary\" placeholder=\"Salary change\">\n                        <mat-option value=\"true\">Yes</mat-option>\n                        <mat-option value=\"false\">No</mat-option>\n                      </mat-select>\n                    </li>\n\n                    <li>\n                      <label>Change Reason</label>\n                      <mat-select class=\"form-control\" formControlName=\"changeReason\" placeholder=\"Change reason\">\n                        <mat-option [value]=\"reason.name\" *ngFor=\"let reason of allJobChanges\">{{reason.name}}\n                        </mat-option>\n                      </mat-select>\n                    </li>\n\n                    <li class=\"text-area\">\n                      <label>Note *</label>\n                      <textarea class=\"form-control\" rows=\"5\" id=\"comment\" formControlName=\"notes\" placeholder=\"Note\" maxlength=\"600\"></textarea>\n                    </li>\n\n                    <div class=\"work-flow-lft col-md-3\">\n                      <label>Approval</label>\n                      <ul>\n                        <span *ngIf=\"updateJobWorkflow\">\n                          <li *ngFor=\"let data of usersOrder\">\n                            <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Reports To\"\n                               [readOnly]=\"disableField\"> -->\n                            <span class=\"form-control\">{{data}}</span>\n                            <!-- \n                          <mat-select class=\"form-control\" formControlName =\"jobManagerName\" placeholder=\"Reports To\" [disabled]=\"disableField\">\n                            <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n                            </mat-option>\n                          </mat-select> -->\n                          </li>\n                        </span>\n                      </ul>\n                    </div>\n                    <!-- <div class=\"approval\">\n                          <h4>Approval Priority</h4>\n                          <div cdkDropList class=\"example-list\" (cdkDropListDropped)=\"drop($event)\">\n                            <div class=\"example-box\" *ngFor=\"let timePeriod of timePeriods\" cdkDrag><span><i\n                                  class=\"fa fa-list-ul\" aria-hidden=\"true\"></i></span>{{timePeriod}}</div>\n                          </div>\n  \n                        </div> -->\n\n\n\n                  </ul>\n\n                </div>\n\n              </div>\n\n              <div class=\"termination-border col-md-11\">\n\n                <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                <button class=\"btn pull-right save\" type=\"submit\" (click)=\"jobChangeSubmit()\">Submit</button>\n                <div class=\"clearfix\"></div>\n              </div>\n\n            </form>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n\n\n<!-- Salary chnage Popup-->\n\n<div class=\"job-change-popup\">\n\n  <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Salary Change Request</h4>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"job-main\">\n\n            <form [formGroup]=\"salaryChangeForm\">\n\n              <div class=\"job-change\">\n\n                <div class=\"job-change-in col-md-11\">\n\n                  <ul>\n\n                    <li>\n                      <label>Select Employee Name</label>\n                      <mat-select class=\"form-control\" placeholder=\"Employee Name\"\n                        (selectionChange)=\"selectEmployee($event)\">\n                        <mat-option *ngFor=\"let emp of employeeData\" [value]=\"emp._id\">{{emp.personal.name.preferredName}}\n                        </mat-option>\n                      </mat-select>\n\n                    </li>\n\n                    <li class=\"col-md-6\">\n                      <label>Current Status</label>\n                      <div class=\"job-status\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>Job Title</th>\n                              <th>Current Value</th>\n                              <th>Effective Date</th>\n                            </tr>\n                          </thead>\n\n                          <tbody>\n\n                            <tr>\n                              <td>Pay Type</td>\n                              <td>\n                                <mat-select class=\"form-control\" placeholder=\"Pay Type\"\n                                  (selectionChange)=\"selectPayType($event)\">\n                                  <mat-option *ngFor=\"let pay of allPayType\" [value]=\"pay.payType\">{{pay.payType}}\n                                  </mat-option>\n                                </mat-select>\n                              </td>\n                              <td>\n                                <div class=\"date\" readonly>\n                                  <span class=\"form-control\">{{newEmpStatus.effective_date | date}}</span>\n                                  <mat-datepicker #picker9></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n                            <tr>\n                              <td>Pay Rate</td>\n                              <td>\n                                <span class=\"form-control\" readonly>{{newEmpStatus.payRate.amount}}</span>\n                              </td>\n                              <td>\n                                <div class=\"date\" readonly>\n                                  <span class=\"form-control\">{{newEmpStatus.effective_date | date}}</span>\n                                  <mat-datepicker #picker7></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n                            <tr>\n                              <td>Pay Frequency</td>\n                              <td>\n                                <span class=\"form-control\" readonly>{{newEmpStatus.Pay_frequency}}</span>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <span class=\"form-control\">{{newEmpStatus.effective_date | date}}</span>\n                                  <mat-datepicker #picker8></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                            <tr>\n                              <td>% Change Difference</td>\n                              <td>\n                                <input placeholder=\"% Change\" class=\"form-control\" formControlName=\"old_value3\">\n\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <span class=\"form-control\">{{newEmpStatus.effective_date | date}}</span>\n                                  <mat-datepicker #picker9></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n                          </tbody>\n                        </table>\n\n                      </div>\n                    </li>\n\n\n\n                    <li class=\"col-md-6\">\n                      <label>New Status</label>\n                      <div class=\"job-status new\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>Job Title</th>\n                              <th>&nbsp; New Value &nbsp; &nbsp;</th>\n                              <th>Effective Date</th>\n                            </tr>\n                          </thead>\n                          <tbody>\n\n\n                            <tr>\n                              <td>Pay Type</td>\n                              <td>\n                                <mat-select class=\"form-control\" placeholder=\"Pay Type\"\n                                  (selectionChange)=\"selectPayType($event)\">\n                                  <mat-option *ngFor=\"let pay of allPayTypenew\" [value]=\"pay.name\">{{pay.name}}\n                                  </mat-option>\n                                </mat-select>\n                              </td>\n                              <td>\n                                <div class=\"date\" readonly>\n                                  <span class=\"form-control\">{{newEmpStatus.effective_date | date}}</span>\n                                  <mat-datepicker #picker9></mat-datepicker>\n                                  <button mat-raised-button>\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                            <tr>\n                              <td>Pay Rate</td>\n                              <td>\n                                <input placeholder=\"Pay Rate\" class=\"form-control\" formControlName=\"new_value1\">\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker121\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" formControlName=\"effective_date1\" [min]=\"pastDate\">\n                                  <mat-datepicker #picker121></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker121.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n                            <tr>\n                              <td>Pay Frequency</td>\n                              <td>\n                                <mat-select class=\"form-control\" placeholder=\"Frequency\" formControlName=\"new_value2\">\n                                  <mat-option [value]=\"frequen.name\" *ngFor=\"let frequen of allPayFrequency\">\n                                    {{frequen.name}}</mat-option>\n                                </mat-select>\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker113\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" formControlName=\"effective_date2\" [min]=\"pastDate\">\n                                  <mat-datepicker #picker113></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker113.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                            <tr>\n                              <td>% Change Difference</td>\n                              <td>\n                                <input placeholder=\"% Change\" class=\"form-control\" formControlName=\"new_value3\">\n                              </td>\n                              <td>\n                                <div class=\"date\">\n                                  <input matInput [matDatepicker]=\"picker012\" placeholder=\"mm / dd / yy\"\n                                    class=\"form-control\" formControlName=\"effective_date3\" [min]=\"pastDate\">\n                                  <mat-datepicker #picker012></mat-datepicker>\n                                  <button mat-raised-button (click)=\"picker012.open()\">\n                                    <span>\n                                      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                    </span>\n                                  </button>\n                                  <div class=\"clearfix\"></div>\n                                </div>\n                              </td>\n                            </tr>\n\n\n                          </tbody>\n                        </table>\n\n\n                        <p *ngIf=\"salaryChangeForm.get('new_value1').invalid && salaryChangeForm.get('new_value1').touched || salaryChangeForm.get('effective_date1').invalid && salaryChangeForm.get('effective_date1').touched \n                        || salaryChangeForm.get('new_value2').invalid && salaryChangeForm.get('new_value2').touched || salaryChangeForm.get('effective_date2').invalid && salaryChangeForm.get('effective_date2').touched \n                        || salaryChangeForm.get('new_value3').invalid && salaryChangeForm.get('new_value3').touched || salaryChangeForm.get('effective_date3').invalid && salaryChangeForm.get('effective_date3').touched\" class=\"errors\">Please fill the all new status values</p>\n\n                      </div>\n\n                    </li>\n                    <div class=\"clearfix\"></div>\n\n                    <li>\n                      <label>Change Reason</label>\n                      <mat-select class=\"form-control\" placeholder=\"Reason\" formControlName=\"changeReason\">\n                        <mat-option [value]=\"reason.name\" *ngFor=\"let reason of allJobChanges\">{{reason.name}}\n                        </mat-option>\n                      </mat-select>\n                    </li>\n\n                    <li class=\"text-area\">\n                      <label>Note *</label>\n                      <textarea class=\"form-control\" rows=\"5\" id=\"comment\" formControlName=\"notes\" placeholder=\"Note\" maxlength=\"600\"></textarea>\n                    </li>\n\n                    <div class=\"work-flow-lft col-md-3\">\n                      <label>Approval</label>\n                      <ul>\n\n\n\n                        <span *ngIf=\"updateCompensationWorkflow\">\n                          <li *ngFor=\"let data of usersCompensationOrder\">\n                            <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Reports To\"\n                               [readOnly]=\"disableField\"> -->\n                            <span class=\"form-control\">{{data}}</span>\n                            <!-- \n                          <mat-select class=\"form-control\" formControlName =\"jobManagerName\" placeholder=\"Reports To\" [disabled]=\"disableField\">\n                            <mat-option *ngFor=\"let data of managerData\" [value]=\"data._id\">{{data.levelName}}\n                            </mat-option>\n                          </mat-select> -->\n                          </li>\n                        </span>\n\n\n                      </ul>\n                    </div>\n\n                  </ul>\n\n                </div>\n\n              </div>\n\n              <div class=\"termination-border col-md-11\">\n\n                <button class=\"btn pull-left cancel\" data-dismiss=\"modal\">Cancel</button>\n                <button class=\"btn pull-right save\" type=\"submit\" (click)=\"salaryChangeSubmit()\">Submit</button>\n                <div class=\"clearfix\"></div>\n              </div>\n\n            </form>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: ManagerRequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerRequestComponent", function() { return ManagerRequestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_manager_self_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/manager-self-service */ "./src/app/services/manager-self-service.ts");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_workflow_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/workflow.service */ "./src/app/services/workflow.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ManagerRequestComponent = /** @class */ (function () {
    function ManagerRequestComponent(managerRequestService, workflowService, siteAccessRolesService, accessLocalStorageService, myInfoService) {
        this.managerRequestService = managerRequestService;
        this.workflowService = workflowService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.myInfoService = myInfoService;
        this.timePeriods = [
            'Bronze age',
            'Iron age',
            'Middle ages',
            'Early modern period',
            'Long nineteenth century'
        ];
        this.empStatus = {
            job: '',
            jobTitle: '',
            personal: '',
            effectiveDate: '',
            compensation: {
                current_compensation: [
                    {
                        payType: '',
                        effective_date: '',
                        payRate: {
                            amount: ''
                        },
                        Pay_frequency: ''
                    }
                ]
            }
        };
        this.allJobTitleFields = [];
        this.allDepartments = [];
        this.allLocations = [];
        this.allJobChanges = [];
        this.allPayType = [];
        this.allPayTypenew = [];
        this.allPayFrequency = [];
        this.updateJobWorkflow = false;
        this.priorityUsersJob = [];
        this.employeeData = [];
        this.updateCompensationWorkflow = false;
        this.priorityUsersCompensation = [];
        this.disableField = true;
        this.newEmpStatus = {
            payType: '',
            effective_date: '',
            payRate: {
                amount: ''
            },
            Pay_frequency: ''
        };
        this.usersOrder = [];
        this.usersCompensationOrder = [];
    }
    ManagerRequestComponent.prototype.drop = function (event) {
        Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__["moveItemInArray"])(this.timePeriods, event.previousIndex, event.currentIndex);
    };
    ManagerRequestComponent.prototype.ngOnInit = function () {
        // Author:Suresh M, Date:28-05-19
        // Job Change request Form  
        this.jobChangeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            new_value: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            effective_date: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            new_value1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            effective_date1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            new_value2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            effective_date2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            impactsSalary: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            changeReason: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            notes: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            jobManagerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            jobHrName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            jobSplRole: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            jobEmpName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
        });
        // Author:Suresh M, Date:28-05-19
        // Salary Change request Form  
        this.salaryChangeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            new_value: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            effective_date: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            new_value1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            effective_date1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            new_value2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            effective_date2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            new_value3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            effective_date3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            old_value: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            effective_date4: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            old_value1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            effective_date5: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            old_value2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            effective_date6: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            old_value3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            effective_date7: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            changeReason: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            notes: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            compensationManagerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            compensationHrName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            compensationSplRole: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
            compensationEmpName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](""),
        });
        // this.employeeList();
        var cID = localStorage.getItem('companyId');
        console.log("cidddddddddd", cID);
        cID = cID.replace(/^"|"$/g, "");
        this.companyId = cID;
        this.userId = this.accessLocalStorageService.get('employeeId');
        console.log("cin222222222", cID);
        this.getStandardFields();
        this.jobChangeReasonFields();
        this.employeeSearch();
        this.getAllBaseRoles();
        this.getManagerRoles();
        this.gettingLevelsHr();
        this.gettingLevelsManager();
        this.sysAdminRoles();
        this.getJobWorkFlow();
        this.getCompensationWorkFlow();
    };
    ManagerRequestComponent.prototype.jobchangePopup = function () {
        // this.employeeList();
        this.getStandardFields();
        this.jobChangeReasonFields();
        this.employeeSearch();
        this.getAllBaseRoles();
        this.getManagerRoles();
        this.gettingLevelsHr();
        this.gettingLevelsManager();
        this.sysAdminRoles();
        this.getJobWorkFlow();
        this.getCompensationWorkFlow();
    };
    ManagerRequestComponent.prototype.employeeSearch = function () {
        var _this = this;
        this.siteAccessRolesService.getAllEmployeesUnderManager(this.companyId, this.userId)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    ManagerRequestComponent.prototype.getAllBaseRoles = function () {
        var _this = this;
        this.siteAccessRolesService.getBaseRoles()
            .subscribe(function (res) {
            console.log("roles", res);
            _this.resRolesObj = res.data;
            for (var i = 0; i < _this.resRolesObj.length; i++) {
                console.log(_this.resRolesObj, "rolesssss");
                if (_this.resRolesObj[i].name == 'Manager') {
                    _this.managerId = _this.resRolesObj[i]._id;
                    console.log(_this.managerId);
                    _this.getManagerRoles();
                }
                if (_this.resRolesObj[i].name == "System Admin") {
                    _this.sysAdminId = _this.resRolesObj[i]._id;
                    _this.sysAdminRoles();
                }
                if (_this.resRolesObj[i].name == "Employee") {
                    _this.employeeBaseId = _this.resRolesObj[i]._id;
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ManagerRequestComponent.prototype.getManagerRoles = function () {
        var _this = this;
        console.log("managerId", this.managerId);
        // this.managerId = "5cbe98a3561562212689f747"
        this.workflowService.getManagerEmployees(this.companyId, this.managerId)
            .subscribe(function (res) {
            console.log("manager roles", res);
            _this.managerData1 = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    ManagerRequestComponent.prototype.gettingLevelsManager = function () {
        var _this = this;
        var cmpnyId = JSON.parse(localStorage.getItem('companyId'));
        this.workflowService.gettingofManagerlevels(cmpnyId)
            .subscribe(function (res) {
            console.log("roles", res);
            _this.managerData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    ManagerRequestComponent.prototype.gettingLevelsHr = function () {
        var _this = this;
        var cmpnyId = JSON.parse(localStorage.getItem('companyId'));
        this.workflowService.gettingofHrlevels(cmpnyId)
            .subscribe(function (res) {
            console.log("roles", res);
            _this.hrData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    ManagerRequestComponent.prototype.sysAdminRoles = function () {
        var _this = this;
        console.log("vippip");
        // this.sysAdminId = "5cbe9922561562212689f74a",
        this.workflowService.getManagerEmployees(this.companyId, this.sysAdminId)
            .subscribe(function (res) {
            console.log("sys admin roles", res);
            _this.splRoleData = res.data;
            _this.managerData1.forEach(function (element) {
                _this.splRoleData.push(element);
            });
            console.log(_this.splRoleData, 'splroledata');
        }, function (err) {
            console.log(err);
        });
    };
    ManagerRequestComponent.prototype.getJobWorkFlow = function () {
        var _this = this;
        this.workflowService.getWorkflowservice(this.companyId, "job")
            .subscribe(function (res) {
            console.log("get job workflow", res);
            _this.jobSelectedWorkflowId = res.data[0]._id;
            _this.hrPriority = res.data[0].hr.priority;
            _this.hrLevelId = res.data[0].hr.levelId;
            _this.reportsToLevelId = res.data[0].reportsTo.levelId;
            _this.specialPersonPriority = res.data[0].specialPerson.priority;
            _this.specialPersonId = res.data[0].specialPerson.personId;
            _this.specialRolePriority = res.data[0].specialRole.priority;
            _this.specialRoleId = res.data[0].specialRole.roleId;
            _this.reportsToPriority = res.data[0].reportsTo.priority;
            console.log(_this.hrPriority, _this.reportsToPriority, _this.specialRolePriority, _this.specialPersonPriority, _this.userId);
            if (_this.hrPriority) {
                _this.workflowService.findCorrespondingPersonId(_this.companyId, 'job', _this.userId, _this.hrPriority).subscribe(function (res) {
                    console.log(res);
                    _this.jobHrPersonId = res.data;
                    console.log(_this.jobHrPersonId);
                    if (res.resultType == 'user') {
                        _this.myInfoService.getOtherUser(_this.companyId, _this.jobHrPersonId).subscribe(function (res) {
                            console.log(res);
                            _this.usersOrder[(_this.hrPriority - 1)] = res.data.personal.name.preferredName;
                            _this.jobChangeForm.get('jobSplRole').patchValue(res.data.personal.name.preferredName);
                            console.log(_this.usersOrder);
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            if (_this.reportsToPriority) {
                _this.workflowService.findCorrespondingPersonId(_this.companyId, 'job', _this.userId, _this.reportsToPriority).subscribe(function (res) {
                    console.log(res);
                    _this.jobReportsToPersonId = res.data;
                    console.log(_this.jobReportsToPersonId);
                    if (res.resultType == 'user') {
                        _this.myInfoService.getOtherUser(_this.companyId, _this.jobReportsToPersonId).subscribe(function (res) {
                            console.log(res);
                            // this.timePeriods[(this.compensationRes.approvalPriority[i].priority - 1)]
                            _this.usersOrder[(_this.reportsToPriority - 1)] = res.data.personal.name.preferredName;
                            _this.jobChangeForm.get('jobSplRole').patchValue(res.data.personal.name.preferredName);
                            console.log(_this.usersOrder);
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            if (res.data.length >= 1) {
                _this.jobWorkflowId = res.data[0]._id;
                _this.updateJobWorkflow = true;
                // this.jobChangeForm.get('jobHrName').patchValue(res.data[0].hr.levelId);
                // console.log(this.jobChangeForm.get('jobHrName').value);
                var index = _this.hrData.findIndex(function (userData) { return userData._id === res.data[0].hr.levelId; });
                var user = _this.hrData[index];
                // user['userType'] = 'hr';
                var name = user.levelName;
                var _id = user._id;
                _this.priorityUsersJob[(res.data[0].hr.priority) - 1] = { name: name, _id: _id, userType: 'hr' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                console.log("res get of workflow", user);
                // this.jobChangeForm.get('jobManagerName').patchValue(res.data[0].reportsTo.levelId);
                // console.log(this.jobChangeForm.get('jobManagerName').value);
                var indexManager = _this.managerData.findIndex(function (userData) { return userData._id === res.data[0].reportsTo.levelId; });
                var userManager = _this.managerData[indexManager];
                // userManager['userType'] = 'manager';
                var name = userManager.levelName;
                var _id = userManager._id;
                _this.priorityUsersJob[(res.data[0].reportsTo.priority) - 1] = { name: name, _id: _id, userType: 'manager' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                console.log("res get of workflow", userManager);
                // this.jobChangeForm.get('jobEmpName').patchValue(res.data[0].specialPerson.personId);
                // console.log(this.jobChangeForm.get('jobEmpName').value);
                var indexSpecialPerson = _this.employeeData.findIndex(function (userData) { return userData._id === res.data[0].specialPerson.personId; });
                var userSpecialManager = _this.employeeData[indexSpecialPerson];
                console.log(userSpecialManager, 'newqqqqqqqqqqqqq1');
                var name = userSpecialManager.personal.name.preferredName;
                _this.usersOrder[(_this.specialPersonPriority - 1)] = userSpecialManager.personal.name.preferredName;
                _this.jobChangeForm.get('jobSplRole').patchValue(userSpecialManager.personal.name.preferredName);
                console.log(name);
                var _id = userSpecialManager._id;
                _this.priorityUsersJob[(res.data[0].specialPerson.priority) - 1] = { name: name, _id: _id, userType: 'specialPerson' };
                console.log("res get of workflow", { name: name, _id: _id, userType: 'specialPerson' });
                // this.jobChangeForm.get('jobSplRole').patchValue(res.data[0].specialRole.roleId);
                // console.log(this.jobChangeForm.get('jobSplRole').value);
                console.log(_this.splRoleData);
                var indexSpecialRole = _this.splRoleData.findIndex(function (userData) { return userData._id === res.data[0].specialRole.roleId; });
                console.log("indexx value", indexSpecialRole);
                var userSpecialManagerole = _this.splRoleData[indexSpecialRole];
                console.log(userSpecialManagerole, "11111");
                var name = userSpecialManagerole.name;
                _this.usersOrder[(_this.specialRolePriority - 1)] = userSpecialManagerole.name;
                _this.jobChangeForm.get('jobSplRole').patchValue(userSpecialManagerole.name);
                var _id = userSpecialManagerole._id;
                _this.priorityUsersJob[(res.data[0].specialRole.priority) - 1] = { name: name, _id: _id, userType: 'specialRole' };
                console.log("res get of workflow", { name: name, _id: _id });
                console.log(_this.priorityUsersJob, _this.usersOrder);
            }
        }, function (err) {
            console.log("error", err);
        });
        console.log(this.usersOrder, 'userdorders');
    };
    ManagerRequestComponent.prototype.getCompensationWorkFlow = function () {
        var _this = this;
        this.workflowService.getWorkflowservice(this.companyId, "compensation")
            .subscribe(function (res) {
            console.log("get workflow", res);
            _this.salarySelectedWorkflowId = res.data[0]._id;
            _this.salaryHrPriority = res.data[0].hr.priority;
            _this.salaryHrLevelId = res.data[0].hr.levelId;
            _this.salaryReportsToLevelId = res.data[0].reportsTo.levelId;
            _this.salarySpecialPersonPriority = res.data[0].specialPerson.priority;
            _this.salarySpecialPersonId = res.data[0].specialPerson.personId;
            _this.salarySpecialRolePriority = res.data[0].specialRole.priority;
            _this.salarySpecialRoleId = res.data[0].specialRole.roleId;
            _this.salaryReportsPriority = res.data[0].reportsTo.priority;
            console.log(_this.salaryHrPriority, 'vipin');
            if (_this.salaryHrPriority) {
                _this.workflowService.findCorrespondingPersonId(_this.companyId, 'compensation', _this.userId, _this.salaryHrPriority).subscribe(function (res) {
                    console.log(res);
                    _this.salaryHrPersonId = res.data;
                    if (res.resultType == 'user') {
                        _this.myInfoService.getOtherUser(_this.companyId, _this.salaryHrPersonId).subscribe(function (res) {
                            console.log(res);
                            _this.usersCompensationOrder[(_this.salaryHrPriority - 1)] = res.data.personal.name.preferredName;
                            _this.salaryChangeForm.get('compensationHrName').patchValue(res.data.personal.name.preferredName);
                            console.log(_this.salaryChangeForm.get('compensationHrName').value);
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            if (_this.salaryReportsPriority) {
                _this.workflowService.findCorrespondingPersonId(_this.companyId, 'compensation', _this.userId, _this.salaryReportsPriority).subscribe(function (res) {
                    console.log(res);
                    _this.salaryReportsPersonId = res.data;
                    if (res.resultType == 'user') {
                        _this.myInfoService.getOtherUser(_this.companyId, _this.salaryReportsPersonId).subscribe(function (res) {
                            console.log(res);
                            _this.usersCompensationOrder[(_this.salaryReportsPriority - 1)] = res.data.personal.name.preferredName;
                            _this.salaryChangeForm.get('compensationHrName').patchValue(res.data.personal.name.preferredName);
                            console.log(_this.salaryChangeForm.get('compensationManagerName').value);
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            if (res.data.length >= 1) {
                _this.compensationWorkflowId = res.data[0]._id;
                _this.updateCompensationWorkflow = true;
                // this.salaryChangeForm.get('compensationHrName').patchValue(res.data[0].hr.levelId);
                var index = _this.hrData.findIndex(function (userData) { return userData._id === res.data[0].hr.levelId; });
                var user = _this.hrData[index];
                // user['userType'] = 'hr';
                var name = user.levelName;
                var _id = user._id;
                _this.priorityUsersCompensation[(res.data[0].hr.priority) - 1] = { name: name, _id: _id, userType: 'hr' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                console.log("res get of workflow", user);
                // this.salaryChangeForm.get('compensationManagerName').patchValue(res.data[0].reportsTo.levelId);
                var indexManager = _this.managerData.findIndex(function (userData) { return userData._id === res.data[0].reportsTo.levelId; });
                var userManager = _this.managerData[indexManager];
                // userManager['userType'] = 'manager';
                var name = userManager.levelName;
                var _id = userManager._id;
                _this.priorityUsersCompensation[(res.data[0].reportsTo.priority) - 1] = { name: name, _id: _id, userType: 'manager' }; // temp ===>>>>> in 0 place (res.data[2].hr.priority) - 1
                console.log("res get of workflow", userManager);
                // this.salaryChangeForm.get('compensationEmpName').patchValue(res.data[0].specialPerson.personId);
                var indexSpecialPerson = _this.employeeData.findIndex(function (userData) { return userData._id === res.data[0].specialPerson.personId; });
                var userSpecialManager = _this.employeeData[indexSpecialPerson];
                console.log(userSpecialManager);
                var name = userSpecialManager.personal.name.preferredName;
                _this.usersCompensationOrder[(_this.salarySpecialPersonPriority - 1)] = userSpecialManager.personal.name.preferredName;
                _this.salaryChangeForm.get('compensationHrName').patchValue(userSpecialManager.personal.name.preferredName);
                console.log("name", name);
                var _id = userSpecialManager._id;
                _this.priorityUsersCompensation[(res.data[0].specialPerson.priority) - 1] = { name: name, _id: _id, userType: 'specialPerson' };
                console.log("res get of workflow", { name: name, _id: _id, userType: 'specialPerson' });
                // this.salaryChangeForm.get('compensationSplRole').patchValue(res.data[0].specialRole.roleId);
                // console.log(this.salaryChangeForm.get('compensationSplRole').value);
                console.log(_this.splRoleData);
                var indexSpecialRole = _this.splRoleData.findIndex(function (userData) { return userData._id === res.data[0].specialRole.roleId; });
                console.log("indexx value", indexSpecialRole);
                var userSpecialManagerole = _this.splRoleData[indexSpecialRole];
                console.log(userSpecialManagerole, "11111");
                var name = userSpecialManagerole.name;
                _this.usersCompensationOrder[(_this.salarySpecialRolePriority - 1)] = userSpecialManagerole.name;
                _this.salaryChangeForm.get('compensationHrName').patchValue(userSpecialManagerole.name);
                var _id = userSpecialManagerole._id;
                _this.priorityUsersCompensation[(res.data[0].specialRole.priority) - 1] = { name: name, _id: _id, userType: 'specialRole' };
                console.log("res get of workflow", { name: name, _id: _id });
                console.log(_this.priorityUsersJob);
            }
        }, function (err) {
            console.log("error", err);
        });
    };
    // Author:Suresh M, Date:28-05-19
    // Employee List  
    // employeeList() {
    //   var data: any = {
    //     name: ""
    //   };
    //   this.managerRequestService.jobEmployeeList(data)
    //     .subscribe((res: any) => {
    //       console.log("Employeees", res)
    //       this.employees = res.data
    //     });
    // }
    // Author:Suresh M, Date:28-05-19
    // Select Employee ID
    ManagerRequestComponent.prototype.selectEmployee = function ($event) {
        var _this = this;
        this.newEmpStatus = {
            payType: '',
            effective_date: '',
            payRate: {
                amount: ''
            },
            Pay_frequency: ''
        };
        this.allPayType = [];
        console.log("SelectEmppppp", $event);
        this.empId = $event.value;
        console.log("EmployeeIDDDD", this.empId);
        var cID = JSON.parse(localStorage.getItem('companyId'));
        console.log("Company IDDDDD", cID);
        this.managerRequestService.selectUserId(cID, this.empId)
            .subscribe(function (res) {
            console.log("Comapny & User", res);
            // if(res.data.)
            _this.empStatus = res.data;
            _this.pastDate = _this.empStatus.personal.effectiveDate;
            console.log("past dateeee", _this.pastDate);
            if (_this.empStatus.compensation.current_compensation) {
                _this.empStatus.compensation.current_compensation.forEach(function (element) {
                    if (element.payType) {
                        _this.allPayType.push(element);
                    }
                });
            }
            console.log(_this.allPayType);
        });
        console.log(this.empStatus);
    };
    // Author:Suresh M, Date:30-05-19
    // selectPayType Name
    ManagerRequestComponent.prototype.selectPayType = function ($event) {
        var _this = this;
        console.log("Select Pay typess", $event, this.empStatus);
        this.payTypeID = $event.value;
        this.empStatus.compensation.current_compensation.forEach(function (element) {
            console.log(element.payType, _this.payTypeID);
            if (_this.payTypeID == element.payType) {
                _this.newEmpStatus = element;
            }
            console.log(_this.newEmpStatus, "AAAAAAAAA");
            _this.pastDate = _this.newEmpStatus.effective_date;
            console.log("salary dateeeee", _this.pastDate);
        });
    };
    // Author:Suresh M, Date:28-05-19
    // New Status Fieldss
    ManagerRequestComponent.prototype.getStandardFields = function () {
        var _this = this;
        var cID = JSON.parse(localStorage.getItem('companyId'));
        this.managerRequestService.getNewStatusFields(cID)
            .subscribe(function (res) {
            console.log("New Statusss", res);
            _this.getStandardData = res.data;
            if (_this.getStandardData['Job Title'].custom.length > 0) {
                _this.getStandardData['Job Title'].custom.forEach(function (element) {
                    _this.allJobTitleFields.push(element);
                });
            }
            if (_this.getStandardData['Job Title'].default.length > 0) {
                _this.getStandardData['Job Title'].default.forEach(function (element) {
                    _this.allJobTitleFields.push(element);
                });
            }
            console.log("All Jobbbb", _this.allJobTitleFields);
            if (_this.getStandardData['Department'].custom.length > 0) {
                _this.getStandardData['Department'].custom.forEach(function (element) {
                    _this.allDepartments.push(element);
                });
            }
            if (_this.getStandardData['Department'].default.length > 0) {
                _this.getStandardData['Department'].default.forEach(function (element) {
                    _this.allDepartments.push(element);
                });
            }
            console.log("All Departt", _this.allDepartments);
            if (_this.getStandardData['Location'].custom.length > 0) {
                _this.getStandardData['Location'].custom.forEach(function (element) {
                    _this.allLocations.push(element);
                });
            }
            if (_this.getStandardData['Location'].default.length > 0) {
                _this.getStandardData['Location'].default.forEach(function (element) {
                    _this.allLocations.push(element);
                });
            }
            console.log("All locations", _this.allLocations);
            if (_this.getStandardData['Pay Type'].default.length > 0) {
                _this.getStandardData['Pay Type'].default.forEach(function (element) {
                    _this.allPayTypenew.push(element);
                });
            }
            if (_this.getStandardData['Pay Type'].custom.length > 0) {
                _this.getStandardData['Pay Type'].custom.forEach(function (element) {
                    _this.allPayTypenew.push(element);
                });
            }
            console.log("All Pay Typess", _this.allPayTypenew);
            if (_this.getStandardData['Pay Frequency'].custom.length > 0) {
                _this.getStandardData['Pay Frequency'].custom.forEach(function (element) {
                    _this.allPayFrequency.push(element);
                });
            }
            if (_this.getStandardData['Pay Frequency'].default.length > 0) {
                _this.getStandardData['Pay Frequency'].default.forEach(function (element) {
                    _this.allPayFrequency.push(element);
                });
            }
            console.log("All Pay Frequency", _this.allPayFrequency);
        });
    };
    // Author:Suresh M, Date:28-05-19
    // Job Change Request Submit
    ManagerRequestComponent.prototype.jobChangeSubmit = function () {
        var _this = this;
        var cID = JSON.parse(localStorage.getItem('companyId'));
        var newData = {
            companyId: cID,
            changeRequestType: "Job",
            requestedBy: this.userId,
            requestedFor: this.empId,
            newStatus: {
                jobTitle: {
                    new_value: this.jobChangeForm.get('new_value').value,
                    effective_date: this.jobChangeForm.get('effective_date').value
                },
                department: {
                    new_value: this.jobChangeForm.get('new_value1').value,
                    effective_date: this.jobChangeForm.get('effective_date1').value,
                },
                location: {
                    new_value: this.jobChangeForm.get('new_value2').value,
                    effective_date: this.jobChangeForm.get('effective_date2').value,
                },
            },
            oldStatus: {
                jobTitle: {
                    old_value: this.empStatus.job.jobTitle,
                    effective_date: this.empStatus.personal.effectiveDate
                },
                department: {
                    old_value: this.empStatus.job.department,
                    effective_date: this.empStatus.personal.effectiveDate,
                },
                location: {
                    old_value: this.empStatus.job.location,
                    effective_date: this.empStatus.personal.effectiveDate,
                },
            },
            changeReason: this.jobChangeForm.get('changeReason').value,
            notes: this.jobChangeForm.get('notes').value,
            impactsSalary: this.jobChangeForm.get('impactsSalary').value,
            selected_workflow: this.jobSelectedWorkflowId,
            approvalPriority: [
                {
                    priority: this.reportsToPriority,
                    key: "reportsTo",
                    levelId: this.reportsToLevelId,
                    personId: this.jobReportsToPersonId
                },
                {
                    priority: this.hrPriority,
                    key: "hr",
                    levelId: this.hrLevelId,
                    personId: this.jobHrPersonId
                },
                {
                    priority: this.specialPersonPriority,
                    key: "specialPerson",
                    personId: this.specialPersonId
                },
                {
                    priority: this.specialRolePriority,
                    key: "specialRole",
                    roleId: this.specialRoleId
                }
            ]
        };
        if (this.jobChangeForm.valid) {
            this.managerRequestService.jobChangeRequest(newData)
                .subscribe(function (res) {
                console.log("Job Chnage Request", res);
                _this.jobChangeForm.reset();
                _this.empId = '';
                jQuery("#myModal").modal("hide");
            });
        }
        else {
            this.jobChangeForm.get('new_value').markAsTouched();
            this.jobChangeForm.get('new_value1').markAsTouched();
            this.jobChangeForm.get('new_value2').markAsTouched();
            this.jobChangeForm.get('effectiveDate').markAsTouched();
            this.jobChangeForm.get('effectiveDate1').markAsTouched();
            this.jobChangeForm.get('effectiveDate2').markAsTouched();
        }
    };
    // Author:Suresh M, Date:28-05-19
    // Job chnage Reasons
    ManagerRequestComponent.prototype.jobChangeReasonFields = function () {
        var _this = this;
        var cID = JSON.parse(localStorage.getItem('companyId'));
        this.managerRequestService.getJobChangeReason(cID)
            .subscribe(function (res) {
            console.log("Change Reasonssss", res);
            _this.jobChangeDetails = res.data;
            if (_this.jobChangeDetails['Job Change Reason'].custom.length > 0) {
                _this.jobChangeDetails['Job Change Reason'].custoom.forEach(function (element) {
                    _this.allJobChanges.push(element);
                });
            }
            if (_this.jobChangeDetails['Job Change Reason'].default.length > 0) {
                _this.jobChangeDetails['Job Change Reason'].default.forEach(function (element) {
                    _this.allJobChanges.push(element);
                });
            }
            console.log("All Job Change Reasonss", _this.allJobChanges);
        });
    };
    // Author:Suresh M, Date:29-05-19
    // Salary Change request
    ManagerRequestComponent.prototype.salaryChangeSubmit = function () {
        var _this = this;
        var cID = JSON.parse(localStorage.getItem('companyId'));
        console.log("Salary Company IDDDD", cID);
        var salaryData = {
            companyId: cID,
            changeRequestType: "Salary",
            requestedBy: cID,
            requestedFor: this.empId,
            newStatus: {
                payType: {
                    new_value: this.salaryChangeForm.get('new_value').value,
                    effective_date: this.salaryChangeForm.get('effective_date').value
                },
                payRate: {
                    new_value: {
                        amount: this.salaryChangeForm.get('new_value1').value,
                        unit: "USD"
                    },
                    effective_date: this.salaryChangeForm.get('effective_date1').value
                },
                Pay_frequency: {
                    new_value: this.salaryChangeForm.get('new_value2').value,
                    effective_date: this.salaryChangeForm.get('effective_date2').value
                },
                percentage_change: {
                    new_value: this.salaryChangeForm.get('new_value3').value,
                    effective_date: this.salaryChangeForm.get('effective_date3').value
                }
            },
            oldStatus: {
                payType: {
                    old_value: this.payTypeID,
                    effective_date: this.newEmpStatus.effective_date
                },
                payRate: {
                    old_value: {
                        amount: this.newEmpStatus.payRate.amount,
                        unit: "USD"
                    },
                    effective_date: this.newEmpStatus.effective_date
                },
                Pay_frequency: {
                    old_value: this.newEmpStatus.Pay_frequency,
                    effective_date: this.newEmpStatus.effective_date
                },
                percentage_change: {
                    old_value: this.salaryChangeForm.get('old_value3').value,
                    effective_date: this.newEmpStatus.effective_date
                }
            },
            changeReason: this.salaryChangeForm.get('changeReason').value,
            notes: this.salaryChangeForm.get('notes').value,
            selected_workflow: this.salarySelectedWorkflowId,
            approvalPriority: [
                {
                    priority: this.salaryReportsPriority,
                    key: "reportsTo",
                    levelId: this.salaryReportsToLevelId,
                    personId: this.salaryReportsPersonId
                },
                {
                    priority: this.salaryHrPriority,
                    key: "hr",
                    levelId: this.salaryHrLevelId,
                    personId: this.salaryHrPersonId
                },
                {
                    priority: this.salarySpecialPersonPriority,
                    key: "specialPerson",
                    personId: this.salarySpecialPersonId
                },
                {
                    priority: this.salarySpecialRolePriority,
                    key: "specialRole",
                    roleId: this.salarySpecialRoleId
                }
            ]
        };
        console.log('newstatus oldstatus', salaryData);
        if (this.salaryChangeForm.valid) {
            this.managerRequestService.salaryChangeRequestData(salaryData)
                .subscribe(function (res) {
                console.log("Salary Changeee", res);
                _this.salaryChangeForm.reset();
                jQuery("#myModal1").modal("hide");
            });
        }
        else {
            this.salaryChangeForm.get('new_value1').markAsTouched();
            this.salaryChangeForm.get('new_value2').markAsTouched();
            this.salaryChangeForm.get('new_value3').markAsTouched();
            this.salaryChangeForm.get('effective_date1').markAsTouched();
            this.salaryChangeForm.get('effective_date2').markAsTouched();
            this.salaryChangeForm.get('effective_date3').markAsTouched();
        }
    };
    ManagerRequestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manager-request',
            template: __webpack_require__(/*! ./manager-request.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.html"),
            styles: [__webpack_require__(/*! ./manager-request.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.css")]
        }),
        __metadata("design:paramtypes", [_services_manager_self_service__WEBPACK_IMPORTED_MODULE_1__["ManagerSelfService"],
            _services_workflow_service__WEBPACK_IMPORTED_MODULE_4__["WorkflowService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__["SiteAccessRolesService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_6__["AccessLocalStorageService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_7__["MyInfoService"]])
    ], ManagerRequestComponent);
    return ManagerRequestComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.css":
/*!*************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.css ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 6px 15px;\n    font-size: 14px;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 16px!important; vertical-align: middle;\n    padding: 0 0 0 5px;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-inner-icon{\n    width: 33px;\n    height: auto;\n    vertical-align: text-bottom;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px 0 10px;\n}\n.zenwork-margin-zero a { margin:  0 10px 0 0; color: #000;}\n/*-- Author:Suresh M, Date:21-05-19  --*/\n/*-- Manager self service codee  --*/\n.manager-self-service { margin: 0 auto; float: none; padding: 0;}\n.manager-self-tabs {display: block;padding: 20px 0 0;}\n.manager-self-tabs .tab-content { margin:40px 0 0;}\n.manager-self-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus\n{ border-bottom:#439348 4px solid;background: transparent;color: #484747 !important;font-size: 18px;}\n.manager-self-tabs .nav-tabs {border-bottom:none; padding: 15px 0 0; background: #fff;}\n.manager-self-tabs .nav-tabs > li { margin: 0 50px;}\n.manager-self-tabs .nav-tabs > li > a {font-size: 18px;line-height: 17px;font-weight:normal;color:#484747 !important;border: none;padding: 10px 0 18px !important;\nmargin: 0; cursor: pointer;}\n.manager-self-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;color: #484747 !important;font-size: 18px;}\n.manager-self-tabs .nav-tabs > li.active a{border-bottom:#439348 4px solid;color: #484747 !important;font-size: 18px;}\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.html":
/*!**************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management']\">\n      <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n    </a>\n    <span class=\"inner-icon-img\">\n      <img src=\"../../../assets/images/Manager Self Service/mss.png\" class=\"zenwork-inner-icon\"\n        alt=\"Company-settings icon\">\n    </span>\n    <small class=\"sub-title\"><b>Manager Self Service</b></small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n\n\n<div class=\"manager-self-service  col-md-11\">\n\n  <div class=\"manager-self-tabs\">\n\n    <ul class=\"nav nav-tabs\">\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-reports\">Manager Reports</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-approval\">Manager Approvals</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-request\">Manager Requests</a>\n      </li>\n      <li routerLinkActive=\"active\">\n        <a routerLink=\"/admin/admin-dashboard/employee-management/manager-self-service/manager-front\">Manager Front Page</a>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n\n  </div>\n\n</div>\n\n<router-outlet></router-outlet>\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: ManagerSelfServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerSelfServiceComponent", function() { return ManagerSelfServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ManagerSelfServiceComponent = /** @class */ (function () {
    function ManagerSelfServiceComponent() {
    }
    ManagerSelfServiceComponent.prototype.ngOnInit = function () {
    };
    ManagerSelfServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manager-self-service',
            template: __webpack_require__(/*! ./manager-self-service.component.html */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.html"),
            styles: [__webpack_require__(/*! ./manager-self-service.component.css */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ManagerSelfServiceComponent);
    return ManagerSelfServiceComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.module.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.module.ts ***!
  \*********************************************************************************************************/
/*! exports provided: ManagerSelfServiceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerSelfServiceModule", function() { return ManagerSelfServiceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _manager_self_service_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./manager-self-service.routing */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.routing.ts");
/* harmony import */ var _manager_self_service_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./manager-self-service.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _services_manager_self_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../services/manager-self-service */ "./src/app/services/manager-self-service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _manager_reports_manager_reports_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./manager-reports/manager-reports.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.ts");
/* harmony import */ var _manager_request_manager_request_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./manager-request/manager-request.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.ts");
/* harmony import */ var _manager_front_manager_front_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./manager-front/manager-front.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.ts");
/* harmony import */ var _services_manager_frontpage_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../services/manager-frontpage.service */ "./src/app/services/manager-frontpage.service.ts");
/* harmony import */ var _services_employee_service_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../services/employee-service.service */ "./src/app/services/employee-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var ManagerSelfServiceModule = /** @class */ (function () {
    function ManagerSelfServiceModule() {
    }
    ManagerSelfServiceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _manager_self_service_routing__WEBPACK_IMPORTED_MODULE_2__["ManagerselfserviceRouting"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"],
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_6__["TabsModule"].forRoot(), _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_11__["DragDropModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_12__["MatCheckboxModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_15__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_15__["FormsModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_13__["MatAutocompleteModule"]
            ],
            declarations: [_manager_self_service_component__WEBPACK_IMPORTED_MODULE_3__["ManagerSelfServiceComponent"], _manager_reports_manager_reports_component__WEBPACK_IMPORTED_MODULE_16__["ManagerReportsComponent"], _manager_request_manager_request_component__WEBPACK_IMPORTED_MODULE_17__["ManagerRequestComponent"], _manager_front_manager_front_component__WEBPACK_IMPORTED_MODULE_18__["ManagerFrontComponent"]],
            providers: [_services_manager_self_service__WEBPACK_IMPORTED_MODULE_14__["ManagerSelfService"], _services_manager_frontpage_service__WEBPACK_IMPORTED_MODULE_19__["ManagerFrontpageService"], _services_employee_service_service__WEBPACK_IMPORTED_MODULE_20__["EmployeeServiceService"]]
        })
    ], ManagerSelfServiceModule);
    return ManagerSelfServiceModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.routing.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.routing.ts ***!
  \**********************************************************************************************************/
/*! exports provided: ManagerselfserviceRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerselfserviceRouting", function() { return ManagerselfserviceRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _manager_self_service_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./manager-self-service.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-self-service.component.ts");
/* harmony import */ var _manager_reports_manager_reports_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./manager-reports/manager-reports.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-reports/manager-reports.component.ts");
/* harmony import */ var _manager_request_manager_request_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./manager-request/manager-request.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-request/manager-request.component.ts");
/* harmony import */ var _manager_front_manager_front_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./manager-front/manager-front.component */ "./src/app/admin-dashboard/employee-management/manager-self-service/manager-front/manager-front.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: '', component: _manager_self_service_component__WEBPACK_IMPORTED_MODULE_2__["ManagerSelfServiceComponent"], children: [
            { path: '', redirectTo: 'manager-reports', pathMatch: 'full' },
            { path: 'manager-reports', component: _manager_reports_manager_reports_component__WEBPACK_IMPORTED_MODULE_3__["ManagerReportsComponent"] },
            { path: 'manager-request', component: _manager_request_manager_request_component__WEBPACK_IMPORTED_MODULE_4__["ManagerRequestComponent"] },
            { path: 'manager-front', component: _manager_front_manager_front_component__WEBPACK_IMPORTED_MODULE_5__["ManagerFrontComponent"] },
            { path: 'manager-approval', loadChildren: './manager-approval/manager-approval.module#ManagerApprovalModule' }
        ] },
];
var ManagerselfserviceRouting = /** @class */ (function () {
    function ManagerselfserviceRouting() {
    }
    ManagerselfserviceRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ManagerselfserviceRouting);
    return ManagerselfserviceRouting;
}());



/***/ }),

/***/ "./src/app/services/manager-reports.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/manager-reports.service.ts ***!
  \*****************************************************/
/*! exports provided: ManagerReportsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerReportsService", function() { return ManagerReportsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ManagerReportsService = /** @class */ (function () {
    function ManagerReportsService(http) {
        this.http = http;
    }
    ManagerReportsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ManagerReportsService);
    return ManagerReportsService;
}());



/***/ }),

/***/ "./src/app/services/manager-self-service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/manager-self-service.ts ***!
  \**************************************************/
/*! exports provided: ManagerSelfService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerSelfService", function() { return ManagerSelfService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ManagerSelfService = /** @class */ (function () {
    function ManagerSelfService(http) {
        this.http = http;
    }
    ManagerSelfService.prototype.jobEmployeeList = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/searchEmployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    ManagerSelfService.prototype.selectUserId = function (cid, id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/getOtherUser/" + cid + '/' + id);
    };
    ManagerSelfService.prototype.getNewStatusFields = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + id);
    };
    ManagerSelfService.prototype.jobChangeRequest = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-requests/submitRequest", data);
    };
    ManagerSelfService.prototype.getJobChangeReason = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + id);
    };
    ManagerSelfService.prototype.salaryChangeRequestData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/manager-requests/submitRequest", data);
    };
    ManagerSelfService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ManagerSelfService);
    return ManagerSelfService;
}());



/***/ })

}]);
//# sourceMappingURL=manager-self-service-manager-self-service-module.js.map