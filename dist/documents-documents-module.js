(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["documents-documents-module"],{

/***/ "./src/app/admin-dashboard/employee-management/documents/documents.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents.module.ts ***!
  \***********************************************************************************/
/*! exports provided: DocumentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentsModule", function() { return DocumentsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _documents_documents_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./documents/documents.component */ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.ts");
/* harmony import */ var _documents_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./documents.routing */ "./src/app/admin-dashboard/employee-management/documents/documents.routing.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _documents_document_dialog_document_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./documents/document-dialog/document-dialog.component */ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DocumentsModule = /** @class */ (function () {
    function DocumentsModule() {
    }
    DocumentsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _documents_routing__WEBPACK_IMPORTED_MODULE_3__["DocumentsRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"]
            ],
            declarations: [_documents_documents_component__WEBPACK_IMPORTED_MODULE_2__["DocumentsComponent"], _documents_document_dialog_document_dialog_component__WEBPACK_IMPORTED_MODULE_6__["DocumentDialogComponent"]],
            entryComponents: [_documents_document_dialog_document_dialog_component__WEBPACK_IMPORTED_MODULE_6__["DocumentDialogComponent"]]
        })
    ], DocumentsModule);
    return DocumentsModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents.routing.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents.routing.ts ***!
  \************************************************************************************/
/*! exports provided: DocumentsRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentsRouting", function() { return DocumentsRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _documents_documents_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./documents/documents.component */ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/documents",pathMatch:"full" },
    { path: '', component: _documents_documents_component__WEBPACK_IMPORTED_MODULE_2__["DocumentsComponent"] },
];
var DocumentsRouting = /** @class */ (function () {
    function DocumentsRouting() {
    }
    DocumentsRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DocumentsRouting);
    return DocumentsRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.css":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".training-modal .modal-dialog { width: 50%;}\n.training-modal .modal-content { border-radius: 5px !important;}\n.training-modal .modal-header {padding: 15px 15px 20px;}\n.training-modal .modal-header h4 { padding: 0 0 0 30px;}\n.training-class { margin:30px 0 40px 50px;}\n.training-class ul { display: block; border-bottom:#ccc 1px solid; padding: 0 0 30px; margin: 0 0 30px;}\n.training-class ul li { margin: 0 0 40px; width:35%;}\n.training-class ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-class ul li .form-control{ border: none; box-shadow: none; background: #f8f8f8;padding: 10px 12px; height: auto;}\n.training-class ul li .form-control:focus{ box-shadow: none;}\n.training-class ul li .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color:#484848;\n  }\n.training-class ul li .form-control::-moz-placeholder { /* Firefox 19+ */\n    color: #484848;\n  }\n.training-class ul li .form-control:-ms-input-placeholder { /* IE 10+ */\n    color: #484848;\n  }\n.training-class ul li .form-control:-moz-placeholder { /* Firefox 18- */\n    color: #484848;\n  }\n.training-class .cancel { color: #E85453; font-size: 15px; float: left; border: none; outline: none; background: transparent; margin: 6px 0 0;}\n.training-class .save { color: #fff; font-size: 15px; float: right;background: #099247; padding: 10px 40px; border-radius: 30px;border: none; outline: none;}\n#fileUpload {\n  height: 0;\n  display: none;\n  width: 0;\n}\n.browse-btn {\n  line-height: 10px;\n}\n.upload {\n  color: #fff;\n  background: #099247;\n  outline: none;\n}\n.error {\n  color: #e85e5e;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.html":
/*!************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n  <h4>Upload Documents</h4>\n </div>\n\n<div class=\"training-class\">\n             \n  <ul>\n\n    <li style=\"width:15%\">\n        <label>Select File*</label>\n        <input type=\"file\" id=\"fileUpload\" accept=\".doc,.docx,application/msword,.xlsx, .xls,application/pdf\" (change)=\"fileChangeEvent($event)\" required>\n        <label for=\"fileUpload\" class=\"browse-btn\">\n      <a mat-menu-item class=\"edit-color upload\">Browse</a>   \n       </label>\n       <span *ngIf=\"fileName\">{{fileName}}</span>\n       <p *ngIf=\"!fileName && isValid\" class=\"error\">Please select one file to upload</p>\n    </li>\n    <li>\n      <label>Document Name*</label>\n      <input type=\"text\" class=\"form-control\" [(ngModel)] =\"documentName\" name=\"document_Name\" #document_Name=\"ngModel\" maxlength=\"40\" minlength=\"40\" required>\n      <p *ngIf=\"!documentName && document_Name.touched || (!documentName && isValid)\" class=\"error\">Please provide document name</p>\n  </li>\n    <li>\n        <label>Document Type*</label>\n        <mat-select class=\"form-control zenwork-general-form-settings\" placeholder=\"Select Type\" [(ngModel)]=\"documentType\" name=\"document_Type\" #document_Type=\"ngModel\" required>\n          <mat-option *ngFor=\"let documentType of allDocumentType\" [value]=\"documentType.name\">{{documentType.name}}</mat-option>\n\n        </mat-select>\n      <p *ngIf=\"!documentType && document_Type.touched || (!documentType && isValid)\" class=\"error\">Please select one document type</p>\n\n    </li>\n    \n  \n    \n  </ul>\n  \n\n  <button type=\"submit\" class=\"cancel\" (click)=\"cancel()\" >Cancel</button>\n  <button *ngIf=\"!data._id\" type=\"submit\" class=\"save\" (click)=\"uploadDocument()\">Submit</button>\n  <button *ngIf=\"data._id\" type=\"submit\" class=\"save\" (click)=\"updateDocument()\">Update</button>\n  <div class=\"clearfix\"></div>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: DocumentDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentDialogComponent", function() { return DocumentDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var DocumentDialogComponent = /** @class */ (function () {
    function DocumentDialogComponent(myInfoService, accessLocalStorageService, swalAlertService, dialogRef, data) {
        this.myInfoService = myInfoService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.customDocumentType = [];
        this.defaultDocumentType = [];
        this.allDocumentType = [];
        this.isValid = false;
    }
    DocumentDialogComponent.prototype.ngOnInit = function () {
        console.log(this.data);
        if (this.data) {
            this.documentName = this.data.documentName;
            this.documentType = this.data.documentType;
            this.fileName = this.data.originalFilename;
        }
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        this.userId = this.employeeDetails._id;
        console.log(this.userId);
        this.getStandardAndCustomStructureFields();
    };
    DocumentDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    // Author:Saiprakash G, Date:21-05-19
    // File upload
    DocumentDialogComponent.prototype.fileChangeEvent = function (e) {
        this.files = e.target.files[0];
        console.log(this.files);
        this.fileName = this.files.name;
    };
    // Author:Saiprakash, Date:24/05/2019
    // Get Standard And Custom Structure Fields
    DocumentDialogComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customDocumentType = _this.getAllStructureFields['Document Type'].custom;
            _this.defaultDocumentType = _this.getAllStructureFields['Document Type'].default;
            _this.allDocumentType = _this.customDocumentType.concat(_this.defaultDocumentType);
        });
    };
    // Author:Saiprakash G, Date:21-05-19
    // Upload Document
    DocumentDialogComponent.prototype.uploadDocument = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            userId: this.userId,
            documentName: this.documentName.trim(),
            documentType: this.documentType
        };
        console.log(data);
        var formData = new FormData();
        formData.append('file', this.files);
        formData.append('data', JSON.stringify(data));
        //   if(this.data._id){
        //     formData.append('data', JSON.stringify(this.data._id));
        //     formData.append('data', JSON.stringify(this.data.originalFilename));
        //     formData.append('data', JSON.stringify(this.data.s3Path));
        //  this.myInfoService.editDocuments(formData).subscribe((res:any)=>{
        //   console.log(res);
        //   this.documentName =null;
        //   this.documentType = null;
        //   this.files = null
        //   this.dialogRef.close(res);
        //  this.swalAlertService.SweetAlertWithoutConfirmation( res.message,"", "success");
        // },(err)=>{
        //   console.log(err);
        //  this.swalAlertService.SweetAlertWithoutConfirmation( err.error.message,"", "error");
        // })
        //   }
        this.isValid = true;
        if (this.fileName && this.documentName && this.documentType) {
            this.myInfoService.uploadDocument(formData).subscribe(function (res) {
                console.log(res);
                _this.dialogRef.close(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.documentName = '';
                _this.documentType = '';
                _this.files = '';
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author:Saiprakash G, Date:21-05-19
    // Update Document
    DocumentDialogComponent.prototype.updateDocument = function () {
        var _this = this;
        console.log(this.data);
        var data = {
            documentId: this.data._id,
            companyId: this.companyId,
            userId: this.userId,
            originalFilename: this.data.originalFilename,
            documentName: this.documentName.trim(),
            documentType: this.documentType,
            s3Path: this.data.s3Path
        };
        console.log(data, this.fileName, this.documentName);
        var formData = new FormData();
        formData.append('file', this.files);
        formData.append('data', JSON.stringify(data));
        this.isValid = true;
        if (this.fileName && this.documentName && this.documentType) {
            this.myInfoService.editDocuments(formData).subscribe(function (res) {
                console.log(res);
                _this.documentName = null;
                _this.documentType = null;
                _this.files = null;
                _this.data._id = "";
                console.log(_this.data.checkDocumentIds);
                _this.dialogRef.close(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    DocumentDialogComponent.prototype.cancel = function () {
        this.onNoClick();
    };
    DocumentDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-document-dialog',
            template: __webpack_require__(/*! ./document-dialog.component.html */ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.html"),
            styles: [__webpack_require__(/*! ./document-dialog.component.css */ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.css")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_3__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], DocumentDialogComponent);
    return DocumentDialogComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.css":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents/documents.component.css ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-documents-tab {\n    padding: 10px 30px;\n}\n.personal-head{\n    margin: 10px;\n}\n.main-head .list-items {\n    display: inline-block;\n    padding: 0px 5px;\n}\n.main-head .list-items span{\n    vertical-align: middle;\n    font-size: 16px;\n}\na:focus {\n    outline: none\n}\na, a:hover {\n  color: black;\n}\n.custom-tables { margin: 0 auto; float: none; padding: 0 30px 0 5px;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px; position: relative;}\n.custom-tables .table>thead>tr>th a { display: inline-block; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 13px;}\n.custom-fields .checkbox{ margin: 0;}\n.custom-tables.table>tbody>tr>th a, .custom-tables .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.custom-tables .table>tbody>tr>th a .fa, .custom-tables .table>thead>tr>th a .fa { color: #6f6d6d;}\n.custom-tables button { border: none; outline: none; background:transparent;}\n.custom-tables .table>tbody>tr>th .dropdown-menu, .custom-tables .table>thead>tr>th .dropdown-menu { top:50px; border:none; margin: 0; padding: 0;}\n.list>li>a { padding: 6px 20px; color:#ccc; display: block !important;}\n.list>li>a:hover {color:#fff !important; background: #099247;}\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.select-drop select { background:#fff; border: none; width: 80%; height: 34px; color:#948e8e; padding: 6px 10px;}\n.custom-tables .btn { float: right; color:#ec6469; border:#ec6469 1px solid;  border-radius: 20px; background: transparent; font-size: 15px; margin: 5px 0 0;}\n.assets-table{\n    padding: 0px 20px;\n  }\n.zenwork-checked-border:after { \ncontent: '';\nposition: absolute;\ntop: 0;\nleft: -50px;\nborder-right: #83c7a1 4px solid;\nheight: 100%;\nborder-radius: 4px;\npadding: 25px;\n}\n.date { height: 38px !important; line-height:inherit !important; width:42% !important; background: #f8f8f8;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date span { border: none !important; padding: 0 !important; margin-left: 15px;}\n.date .form-control { width:82%; height: auto; padding: 12px 0 12px 10px; margin-right: 20px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.document-field {\n    background: #f8f8f8;\n}\n.save {border-radius: 20px; height:40px; line-height:38px;font-size: 15px; padding: 0 24px;margin:30px 0 0;background: #008f3d; color:#fff;}\n.arrow-up {\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n    margin-left: 12px;\n    position: absolute;\n    top: 18px;\n    font-size: 20px;\n    color: gray;\n    cursor: pointer;\n}\n.arrow-down {\n    -webkit-transform: rotate(90deg);\n            transform: rotate(90deg);\n    position: absolute;\n    top: 18px;\n    font-size: 20px;\n    color: gray;\n    cursor: pointer;\n}\n.next-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 20px;\n    color: #fff;\n    padding: 0 30px;    \n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents/documents.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-documents-tab\" style=\"margin-left: 35px;\">\n  <div class=\"personal-head\">\n    <ul class=\"list-unstyled main-head\">\n      <li class=\"list-items\">\n        <span>Documents</span>\n      </li>\n    </ul>\n  </div>\n\n  <div class=\"custom-tables\">\n    <table class=\"table\">\n      <thead>\n        <tr>\n          <th>\n\n          </th>\n          <th>Document Name\n            <i class=\"material-icons arrow-down\" (click)=\"documentSort(1)\">\n              arrow_right_alt\n            </i>\n            <i class=\"material-icons arrow-up\" (click)=\"documentSort(-1)\">\n              arrow_right_alt\n            </i>\n          </th>\n          <th>Document Type\n            <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\"\n                class=\"fa fa-filter\"></i></a>\n            <ul class=\"dropdown-menu list\">\n              <li *ngFor=\"let documentType of allDocumentType\"><a class=\"documents\"\n                  (click)=\"filter(documentType.name)\">{{documentType.name}}</a></li>\n            </ul>\n          </th>\n          <th>Original File Name</th>\n          <th *ngIf=\"!documentsTabView\">\n            <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\" style=\"float:right\">more_vert</mat-icon>\n            <mat-menu #menu=\"matMenu\">\n              <button *ngIf=\"!checkDocumentIds.length >=1\" mat-menu-item (click)=\"openDocumentDialog()\">Add</button>\n              <button *ngIf=\"(!checkDocumentIds.length <=0) && (checkDocumentIds.length ==1)\" mat-menu-item\n                (click)=\"editDocument()\">Edit</button>\n              <button *ngIf=\"(!checkDocumentIds.length <=0) && (checkDocumentIds.length >=1)\" mat-menu-item\n                (click)=\"deleteDocument()\">Delete</button>\n            </mat-menu>\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n\n        <tr *ngFor=\"let document of allDocuments\">\n          <td [ngClass]=\"{'zenwork-checked-border': document.isChecked}\">\n\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"document.isChecked\"\n              (change)=\"checkDocument($event, document._id)\">\n\n            </mat-checkbox>\n          </td>\n          <td>\n            {{document.documentName}}\n          </td>\n          <td>\n            {{document.documentType}}\n          </td>\n          <td>\n            <a href=\"{{document.url}}\" target=\"_blank\">\n              {{document.originalFilename}}\n            </a>\n          </td>\n          <td>\n\n          </td>\n\n        </tr>\n\n\n    </table>\n    <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n    </mat-paginator>\n\n\n\n  </div>\n\n  <div *ngIf=\"!documentsTabView\" style=\"margin:20px 28px;\">\n\n    <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Save Changes</button>\n    <div class=\"clear-fix\"></div>\n  </div>\n\n  <!-- <hr class=\"zenwork-margin-ten-zero\">\n\n  <div class=\"personal-head\">\n      <ul class=\"list-unstyled main-head\">\n        <li class=\"list-items\">\n          <span>Company Policies</span>\n        </li>\n      </ul>\n    </div>\n  <div class=\"custom-tables\">\n      <table class=\"table\">\n        <thead>\n          <tr>\n            <th>\n            \n            </th>\n            <th>Policy Name</th>\n            <th>Acknowledgment Date</th>\n            <th>Renewal Date</th>\n          \n          </tr>\n        </thead>\n        <tbody>\n  \n          <tr>\n         \n              <td>\n  \n            </td>\n            <td>Company Handbook</td>\n            <td>\n                <div class=\"date\">\n                    <input matInput [matDatepicker]=\"picker\" placeholder=\"mm/dd/yy\" class=\"form-control \">\n                    <mat-datepicker #picker></mat-datepicker>\n                    <button mat-raised-button (click)=\"picker.open()\">\n                      <span>\n                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                      </span>\n                    </button>\n                    <div class=\"clearfix\"></div>\n                  </div>\n            </td>\n            <td>\n                <div class=\"date\">\n                    <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm/dd/yy\" class=\"form-control\" >\n                    <mat-datepicker #picker1></mat-datepicker>\n                    <button mat-raised-button (click)=\"picker1.open()\">\n                      <span>\n                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                      </span>\n                    </button>\n                    <div class=\"clearfix\"></div>\n                  </div>\n            </td>\n           \n          </tr>\n  \n  \n      </table>\n      <mat-paginator [length]=\"\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\">\n      </mat-paginator>\n\n      <button class=\"btn pull-right save\" type=\"submit\">Save Changes</button>\n\n      <div class=\"clearfix\"></div>\n    </div>\n   -->"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/documents/documents/documents.component.ts ***!
  \************************************************************************************************/
/*! exports provided: DocumentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentsComponent", function() { return DocumentsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _document_dialog_document_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./document-dialog/document-dialog.component */ "./src/app/admin-dashboard/employee-management/documents/documents/document-dialog/document-dialog.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DocumentsComponent = /** @class */ (function () {
    function DocumentsComponent(dialog, myInfoService, accessLocalStorageService, swalAlertService, router) {
        this.dialog = dialog;
        this.myInfoService = myInfoService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.allDocuments = [];
        this.selectedDocuments = [];
        this.customDocumentType = [];
        this.defaultDocumentType = [];
        this.allDocumentType = [];
        this.checkDocumentIds = [];
        this.pagesAccess = [];
        this.documentsTabView = false;
    }
    DocumentsComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
            this.pageAccesslevels();
        }
        console.log(this.userId);
        this.getStandardAndCustomStructureFields();
        this.getAllDocuments();
    };
    DocumentsComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Documents" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.documentsTabView = true;
                    console.log('loggss', _this.documentsTabView);
                }
            });
        }, function (err) {
        });
    };
    DocumentsComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllDocuments();
    };
    // Author:Saiprakash G, Date:21-05-19
    // Filter Documents
    DocumentsComponent.prototype.filter = function (type) {
        this.filterDocumentType = type;
        this.getAllDocuments();
    };
    // Author:Saiprakash G, Date:21-05-19
    // Sorting Document
    DocumentsComponent.prototype.documentSort = function (sort) {
        this.sortDocument = sort;
        this.getAllDocuments();
    };
    // Author:Saiprakash, Date:24/05/2019
    // Get Standard And Custom Structure Fields
    DocumentsComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            _this.customDocumentType = _this.getAllStructureFields['Document Type'].custom;
            _this.defaultDocumentType = _this.getAllStructureFields['Document Type'].default;
            _this.allDocumentType = _this.customDocumentType.concat(_this.defaultDocumentType);
        });
    };
    // Author:Saiprakash G, Date:21-05-19
    // Open document dialog
    DocumentsComponent.prototype.openDocumentDialog = function () {
        var _this = this;
        if (this.getSingleDocument && this.getSingleDocument._id) {
            var dialogRef = this.dialog.open(_document_dialog_document_dialog_component__WEBPACK_IMPORTED_MODULE_5__["DocumentDialogComponent"], {
                width: '900px',
                height: '700px',
                backdropClass: 'structure-model',
                data: this.getSingleDocument,
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.getAllDocuments();
                _this.checkDocumentIds = [];
                _this.getSingleDocument = '';
            });
        }
        else {
            var dialogRef = this.dialog.open(_document_dialog_document_dialog_component__WEBPACK_IMPORTED_MODULE_5__["DocumentDialogComponent"], {
                width: '900px',
                backdropClass: 'structure-model',
                data: {},
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.getAllDocuments();
                _this.checkDocumentIds = [];
            });
        }
    };
    // Author:Saiprakash G, Date:21-05-19
    // Get all documents
    DocumentsComponent.prototype.getAllDocuments = function () {
        var _this = this;
        var postData = {
            companyId: this.companyId,
            userId: this.userId,
            per_page: this.perPage,
            page_no: this.pageNo,
            filter: {
                documentType: this.filterDocumentType
            },
            sort: {
                documentName: this.sortDocument
            }
        };
        this.myInfoService.listDocuments(postData).subscribe(function (res) {
            console.log(res);
            _this.allDocuments = res.data;
            // if(this.allDocuments.length){
            //   this.documentName = this.allDocuments[0].documentName;
            //   this.documentType = this.allDocuments[0].documentType
            // }
            _this.count = res.count;
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash G, Date:21-05-19
    // Single get document
    DocumentsComponent.prototype.editDocument = function () {
        var _this = this;
        this.selectedDocuments = this.allDocuments.filter(function (documentCheck) {
            return documentCheck.isChecked;
        });
        if (this.selectedDocuments.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one document to proceed", "", 'error');
        }
        else if (this.selectedDocuments.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one document to proceed", "", 'error');
        }
        else {
            this.myInfoService.getDocumentDetails(this.companyId, this.userId, this.selectedDocuments[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getSingleDocument = res.data;
                _this.openDocumentDialog();
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author:Saiprakash G, Date:21-05-19
    // Delete Documents
    DocumentsComponent.prototype.deleteDocument = function () {
        var _this = this;
        this.selectedDocuments = this.allDocuments.filter(function (documentCheck) {
            return documentCheck.isChecked;
        });
        if (this.selectedDocuments.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one document to proceed", "", 'error');
        }
        else {
            var data = {
                companyId: this.companyId,
                userId: this.userId,
                _ids: this.selectedDocuments
            };
            this.myInfoService.deleteDocuments(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation("Deleted Successfully", "", "success");
                _this.checkDocumentIds = [];
                _this.getAllDocuments();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    DocumentsComponent.prototype.checkDocument = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkDocumentIds.push(id);
        }
        console.log(this.checkDocumentIds);
        if (event.checked == false) {
            var index = this.checkDocumentIds.indexOf(id);
            if (index > -1) {
                this.checkDocumentIds.splice(index, 1);
            }
            console.log(this.checkDocumentIds);
        }
    };
    DocumentsComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/assets"]);
    };
    DocumentsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-documents',
            template: __webpack_require__(/*! ./documents.component.html */ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.html"),
            styles: [__webpack_require__(/*! ./documents.component.css */ "./src/app/admin-dashboard/employee-management/documents/documents/documents.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_1__["MyInfoService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_2__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], DocumentsComponent);
    return DocumentsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=documents-documents-module.js.map