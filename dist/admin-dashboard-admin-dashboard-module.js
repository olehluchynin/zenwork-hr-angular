(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin-dashboard-admin-dashboard-module"],{

/***/ "./src/app/admin-dashboard/admin-dashboard.component.css":
/*!***************************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar-right{\n    padding-top: 9px!important;\n\n}\n.zenwork-full{\n    width:100%;\n    height: 100vh;\n    display: table;\n\n}\n.search-border{\n    border : 1px solid gray;\n    border-radius: 25px;\n    height: 25px;\n    position: relative;\n    bottom:5px\n}\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 5px;\n    width: 13px;\n}\n.border-none{\n    border: none;\n    background: transparent;\n}\n.dropdown{\n    padding-top: 14px !important;\n}\n.dropdown a{\n    color: #fff !important;\n}\n.caret{\n    display: inline !important;\n    margin-left: 18px !important;\n    padding-top: 6px !important;\n}\n.main-nav-image{\n    width:13px!important;\n}\n.main-nav-image-notification{\n    width:11px!important;\n    margin-right: 10px !important;\n}\n.zenwork-container {\n    width: 500px;\n    height: 300px;\n    border: 1px solid rgba(0, 0, 0, 0.5);\n  }\n.zenwork-sidenav-content {\n    display: flex;\n    height: 100%;\n    align-items: center;\n    justify-content: center;\n  }\n.zenwork-sidenav {\n    padding: 20px;\n  }\n.toggle-side-nav-inner{\n    box-shadow: 0 0 1px rgba(0,0,0,0.4)!important;\n  }\n.toggle-side-nav-inner .nav>li>a{\n    padding: 15px 33px!important;\n    color: #3e3e3ea3\n  }\n.toggle-side-nav-inner .nav>li>a:focus, .toggle-side-nav-inner .nav>li>a:hover {\n    background-color: #f1f9f4!important; border:none; outline: none;\n  }\n.dashboard-sub-menu-main-wrapper{\n    padding: 20px!important;\n    border-bottom: 2px dotted #3e3e3e2b!important;\n  }\n.zenwork-customized-sidebar-menu{\n      width:21.666667%!important;\n      padding-right: 0px;\n      display: table-cell;\n      vertical-align: top;\n      height: 100%;\n      float:none;    \n  }\n.zenwork-main-submenu{\n    padding: 25px 0px!important; text-align: left !important;\n  }\n.zenwork-customized-template{\n    width: 78.3%!important; \n    /* height: 100vh; */\n    background: #f8f8f8;\n    display: table-cell;\n    float: none;\n    vertical-align: top;\n    padding: 0;\n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/admin-dashboard.component.html":
/*!****************************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Main Navigation Starts-->\n<div>\n  <app-navbar></app-navbar>\n</div>\n<!-- Main Navigation Ends-->\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"zenwork-full\">\n      <!-- Side Navigation Starts -->\n      <div class=\"col-sm-3 zenwork-customized-sidebar-menu sidebar zenwork-pl-zero\">\n        <app-side-nav [getUserData]=\"userData\"></app-side-nav>\n      </div>\n      <!-- Side Navigation Ends -->\n      <div class=\"col-sm-9 zenwork-customized-template\">\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/admin-dashboard/admin-dashboard.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.component.ts ***!
  \**************************************************************/
/*! exports provided: AdminDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminDashboardComponent", function() { return AdminDashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdminDashboardComponent = /** @class */ (function () {
    function AdminDashboardComponent() {
    }
    AdminDashboardComponent.prototype.ngOnInit = function () {
        this.userData = {
            role: "admin"
        };
    };
    AdminDashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-dashboard',
            template: __webpack_require__(/*! ./admin-dashboard.component.html */ "./src/app/admin-dashboard/admin-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./admin-dashboard.component.css */ "./src/app/admin-dashboard/admin-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AdminDashboardComponent);
    return AdminDashboardComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/admin-dashboard.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/admin-dashboard/admin-dashboard.module.ts ***!
  \***********************************************************/
/*! exports provided: AdminDashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminDashboardModule", function() { return AdminDashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _admin_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin.routes */ "./src/app/admin-dashboard/admin.routes.ts");
/* harmony import */ var _admin_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin-dashboard.component */ "./src/app/admin-dashboard/admin-dashboard.component.ts");
/* harmony import */ var _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-module/shared.module */ "./src/app/shared-module/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



/* External Routing */

/* Importing Components */


var AdminDashboardModule = /** @class */ (function () {
    function AdminDashboardModule() {
    }
    AdminDashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]],
            declarations: [
                _admin_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["AdminDashboardComponent"],
            ],
            providers: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_routes__WEBPACK_IMPORTED_MODULE_3__["AdminRouting"],
                _shared_module_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_2__["BsDatepickerModule"].forRoot()
            ]
        })
    ], AdminDashboardModule);
    return AdminDashboardModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/admin.routes.ts":
/*!*************************************************!*\
  !*** ./src/app/admin-dashboard/admin.routes.ts ***!
  \*************************************************/
/*! exports provided: AdminRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRouting", function() { return AdminRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-dashboard.component */ "./src/app/admin-dashboard/admin-dashboard.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




/* import { SharedModule } from '../shared-module/shared.module'; */

var routes = [
    { path: '', redirectTo: 'admin-dashboard', pathMatch: "full" },
    {
        path: 'admin-dashboard', component: _admin_dashboard_component__WEBPACK_IMPORTED_MODULE_2__["AdminDashboardComponent"], children: [
            { path: 'company-settings', loadChildren: "./company-settings/company-settings.module#CompanySettingsModule" },
            { path: 'employee-management', loadChildren: "./employee-management/employee-management.module#EmployeeManagementModule" },
            { path: 'leave-management', loadChildren: './leave-management/leave-management.module#LeaveManagementModule' },
            { path: 'inbox', loadChildren: './inbox/inbox.module#InboxModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'recruitment', loadChildren: './recruitment/recruitment.module#RecruitmentModule' }
        ]
    },
];
var AdminRouting = /** @class */ (function () {
    function AdminRouting() {
    }
    AdminRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes), _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            declarations: []
        })
    ], AdminRouting);
    return AdminRouting;
}());



/***/ })

}]);
//# sourceMappingURL=admin-dashboard-admin-dashboard-module.js.map