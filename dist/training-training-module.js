(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["training-training-module"],{

/***/ "./src/app/admin-dashboard/employee-management/training/training.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TrainingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainingModule", function() { return TrainingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _training_training_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./training/training.component */ "./src/app/admin-dashboard/employee-management/training/training/training.component.ts");
/* harmony import */ var _training_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./training.routing */ "./src/app/admin-dashboard/employee-management/training/training.routing.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _training_find_training_dialog_find_training_dialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./training/find-training-dialog/find-training-dialog.component */ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.ts");
/* harmony import */ var _training_training_history_dialog_training_history_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./training/training-history-dialog/training-history-dialog.component */ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var TrainingModule = /** @class */ (function () {
    function TrainingModule() {
    }
    TrainingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _training_routing__WEBPACK_IMPORTED_MODULE_3__["TrainingRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModuleModule"]
            ],
            declarations: [_training_training_component__WEBPACK_IMPORTED_MODULE_2__["TrainingComponent"], _training_find_training_dialog_find_training_dialog_component__WEBPACK_IMPORTED_MODULE_7__["FindTrainingDialogComponent"], _training_training_history_dialog_training_history_dialog_component__WEBPACK_IMPORTED_MODULE_8__["TrainingHistoryDialogComponent"]],
            providers: [_services_my_info_service__WEBPACK_IMPORTED_MODULE_4__["MyInfoService"]],
            entryComponents: [_training_find_training_dialog_find_training_dialog_component__WEBPACK_IMPORTED_MODULE_7__["FindTrainingDialogComponent"], _training_training_history_dialog_training_history_dialog_component__WEBPACK_IMPORTED_MODULE_8__["TrainingHistoryDialogComponent"]]
        })
    ], TrainingModule);
    return TrainingModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training.routing.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training.routing.ts ***!
  \**********************************************************************************/
/*! exports provided: TrainingRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainingRouting", function() { return TrainingRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _training_training_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./training/training.component */ "./src/app/admin-dashboard/employee-management/training/training/training.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/training",pathMatch:"full" },
    { path: '', component: _training_training_component__WEBPACK_IMPORTED_MODULE_2__["TrainingComponent"] },
];
var TrainingRouting = /** @class */ (function () {
    function TrainingRouting() {
    }
    TrainingRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], TrainingRouting);
    return TrainingRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.css":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".training-cont { display: block;}\n.training-cont .form-group { position: relative; background:#fff; padding: 0;}\n.training-cont .form-control { height: auto; border: none; box-shadow: none; padding: 10px 12px; width:100%; font-size: 15px; background: #f8f8f8;\n    margin-top: 20px;}\n.training-cont .form-control:focus { box-shadow: none;}\n.training-cont span { position: absolute; top:30px; right:10px; cursor: pointer; width: 5%;}\n.training-cont span .material-icons { color: #8e8e8e; font-size:20px; font-weight: normal;}\n.training-cont .table{ background:#fff;}\n.training-cont .table>tbody>tr>th, .training-cont .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:18px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n.training-cont .table>tbody>tr>td, .training-cont .table>tfoot>tr>td, .training-cont .table>tfoot>tr>th, \n.training-cont .table>thead>tr>td {color: #484747;padding:15px;font-size:15px;}\n.training-cont button.mat-menu-item a { color:#ccc;}\n.training-cont button.mat-menu-item a:hover { color:#fff; background: #099247;}\n.training-cont .table>tbody>tr>th a, .training-cont .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n.training-cont .table>tbody>tr>th a .fa, .training-cont .table>thead>tr>th a .fa { color: #6f6d6d;}\n.training-cont button { border: none; outline: none; background:transparent;}\n.training-cont .table>tbody>tr>th .dropdown-menu, .training-cont .table>thead>tr>th .dropdown-menu { top:50px; border:none; margin: 0; padding: 0;}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n.border-top {\n    border-top: 1px solid #e6dfdf;\n    padding-top: 20px;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n    <h4>Find / Add Employee Training</h4>\n   </div>\n\n   <div class=\"training-cont\">\n\n      <div class=\"form-group col-md-4\">\n        <form [formGroup]=\"trainingSearch\">\n        <input type=\"mail\" class=\"form-control\" placeholder=\"Search by training name, subject, type etc..\" formControlName=\"search\">\n        <span><i class=\"material-icons\">search</i></span>\n      </form>\n      </div>\n      <div class=\"clearfix\"></div>\n\n\n      <table class=\"table\">\n          <thead>\n            <tr>\n              <th></th>\n              <th>Training Name</th>\n              \n              <th>Training Subject\n\n                  <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n\n                  <ul class=\"dropdown-menu\">\n                      <li *ngFor=\"let subjectField of trainingSubjectData\"><a (click)=\"filter(subjectField.name)\">{{subjectField.name}}</a></li>\n                  </ul>\n\n              </th>\n              <th>Training Type\n                  <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n                  <ul class=\"dropdown-menu\">\n                      <li *ngFor=\"let subjectField of trainingTypeData\"><a (click)=\"filterType(subjectField.name)\">{{subjectField.name}}</a></li>\n\n                    </ul>\n              </th>\n              <th>Average Time to Complete\n\n                  <a href=\"#\" class=\"dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"><i aria-hidden=\"true\" class=\"fa fa-filter\"></i></a>\n                  <ul class=\"dropdown-menu\">\n                      <li *ngFor=\"let subjectField of trainingAverageTime\"><a (click)=\"filterTime(subjectField.name)\">{{subjectField.name}}</a></li>\n\n                    </ul>\n\n              </th>\n             \n            </tr>\n          </thead>\n          <tbody> \n            <tr *ngFor=\"let training of trainingData\">\n              <td [ngClass]=\"{'zenwork-checked-border':training.isChecked}\"><mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"training.isChecked\" (ngModelChange)=\"selctTrainingId(training.isChecked, training._id)\"></mat-checkbox></td>\n              <td>{{training.trainingName}}</td>\n              <td>{{training.trainingSubject}}</td>\n              <td>{{training.trainingType}}</td>\n              <td>{{training.timeToComplete}}</td>\n             \n            </tr>\n            \n          </tbody>\n      </table>\n\n        <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\"></mat-paginator>\n\n    </div>\n    <div class=\"row border-top\">\n\n        <button mat-button (click)=\"onNoClick()\">Cancel</button>\n  \n        <button mat-button class=\"submit-btn pull-right\" (click)=\"assignTraining()\">Submit</button>\n  \n      </div>\n\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.ts":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: FindTrainingDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FindTrainingDialogComponent", function() { return FindTrainingDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var FindTrainingDialogComponent = /** @class */ (function () {
    function FindTrainingDialogComponent(dialogRef, companySettingsService, accessLocalStorageService, swalAlertService, data) {
        this.dialogRef = dialogRef;
        this.companySettingsService = companySettingsService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.data = data;
        this.trainingData = [
            {
                isChecked: true
            }
        ];
        this.activeTrainings = [];
    }
    FindTrainingDialogComponent.prototype.ngOnInit = function () {
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
        }
        console.log(this.userId);
        this.trainingSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            search: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("")
        });
        this.pageNo = 1;
        this.perPage = 10;
        this.filterName = '';
        this.filterTypeName = '';
        this.filterTimeName = '';
        this.onChanges();
        this.getAllData();
        this.getSubjectData();
    };
    FindTrainingDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    FindTrainingDialogComponent.prototype.onChanges = function () {
        var _this = this;
        this.trainingSearch.controls['search'].valueChanges
            .subscribe(function (val) {
            _this.getAllData();
        });
    };
    FindTrainingDialogComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.getAllData();
    };
    //Filter Training Subject
    FindTrainingDialogComponent.prototype.filter = function (event) {
        this.filterName = event;
        console.log("Eventttt", event);
        this.getAllData();
    };
    //Filter Training Type
    FindTrainingDialogComponent.prototype.filterType = function (event) {
        this.filterTypeName = event;
        this.getAllData();
    };
    //Filter Training Time to Complete
    FindTrainingDialogComponent.prototype.filterTime = function (event) {
        this.filterTimeName = event;
        this.getAllData();
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Get all trainings
    FindTrainingDialogComponent.prototype.getAllData = function () {
        var _this = this;
        console.log("Pagesss", this.pageNo, this.perPage);
        this.companySettingsService.viewTrainingData({
            searchQuery: this.trainingSearch.get('search').value, pageNumber: this.pageNo, perPage: this.perPage,
            trainingSubject: this.filterName, trainingType: this.filterTypeName, timeToComplete: this.filterTimeName
        })
            .subscribe(function (res) {
            console.log("Get the Training Data", res);
            _this.count = res.count;
            _this.trainingData = res.data;
            // this.trainingData.forEach(element => {
            // if(element.status == 1){
            //   this.activeTrainings.push(element)
            // }
            //   element.isChecked = false;
            // });
            // console.log(this.activeTrainings);
        });
    };
    FindTrainingDialogComponent.prototype.getSubjectData = function () {
        var _this = this;
        this.companySettingsService.getAllTraniningData()
            .subscribe(function (res) {
            console.log("Get All Subjects Dataa", res);
            _this.subjectFields = res.temp;
            _this.trainingSubjectData = _this.subjectFields['Training Subject'],
                _this.trainingTypeData = _this.subjectFields['Training Type'],
                _this.trainingAverageTime = _this.subjectFields['Training - Average Time to Complete'],
                console.log("Get All Subjects 21345768", _this.subjectFields);
        });
    };
    FindTrainingDialogComponent.prototype.selctTrainingId = function (isChecked, id) {
        console.log(isChecked, id);
        this.trainingData.forEach(function (element) {
            if (element._id !== id) {
                element.isChecked = false;
            }
        });
        this.selectId = id;
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Self Assign training to employees
    FindTrainingDialogComponent.prototype.assignTraining = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            trainingId: this.selectId,
            _ids: [this.userId],
            origin: "FindTraining"
        };
        this.companySettingsService.assignTrainingToEmployees(data).subscribe(function (res) {
            console.log(res);
            _this.dialogRef.close(res);
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    FindTrainingDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-find-training-dialog',
            template: __webpack_require__(/*! ./find-training-dialog.component.html */ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.html"),
            styles: [__webpack_require__(/*! ./find-training-dialog.component.css */ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.css")]
        }),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__["CompanySettingsService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"], Object])
    ], FindTrainingDialogComponent);
    return FindTrainingDialogComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.css":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.css ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".training-class { margin:10px 0 0 50px;}\n.training-class ul { display: block; border-bottom:#ccc 1px solid; padding: 0 0 30px; margin: 0 0 30px;}\n.training-class ul li { margin: 0 0 20px;}\n.training-class ul li label{color: #484747; font-weight: normal; font-size: 16px; padding: 0 0 15px; display: block;}\n.training-class ul li .form-control{ border: none; box-shadow: none; background: #f8f8f8;padding: 10px 12px; height: auto;}\n.training-class ul li .form-control:focus{ box-shadow: none;}\n.training-class ul li.instructor { padding: 0 15px;}\n.training-class ul li.instructor .form-control { width:31%;}\n.training-class .cancel { color: #E85453; font-size: 15px; float: left; border: none; outline: none; background: transparent; margin: 6px 0 0;}\n.training-class .save { color: #fff; font-size: 15px; float: right;background: #099247; padding: 10px 40px; border-radius: 30px;border: none; outline: none;}\n.errors {color:#e44a49; padding: 5px 0 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.html":
/*!**************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.html ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"training-modal\">\n\n    <div class=\"modal-header\">\n     <h4>Edit / View Training Classes</h4>\n    </div>\n \n      <div class=\"training-class\">\n        <form [formGroup]=\"trainingForm\" (ngSubmit)=\"editTraining()\">\n        <ul>\n\n          <li class=\"col-md-4\">\n            <label>Training Name</label>\n            <input type=\"text\" class=\"form-control\" formControlName=\"trainingName\" [readOnly]=\"disableField\">\n            <span *ngIf=\"trainingForm.get('trainingName').invalid && trainingForm.get('trainingName').touched\" class=\"errors\">Enter name</span>\n          </li>\n          <li class=\"col-md-4\">\n              <label>Training ID Number</label>\n              <input type=\"number\" class=\"form-control\" formControlName=\"trainingId\" [readOnly]=\"disableField\">\n              <span *ngIf=\"trainingForm.get('trainingId').invalid && trainingForm.get('trainingId').touched\" class=\"errors\">Enter number</span>\n            </li>\n          <div class=\"clearfix\"></div>  \n\n          <li class=\"instructor\">\n              <label>Instructor Name</label>\n              <mat-select class=\"form-control zenwork-general-form-settings\" formControlName=\"instructorName\" [disabled]=\"disableField\">\n                      \n                    <mat-option *ngFor=\"let employess of employeeData\" [value]=\"employess._id\" >\n                    {{employess.personal.name.preferredName}}</mat-option>\n\n                </mat-select>\n\n                <span *ngIf=\"trainingForm.get('instructorName').invalid && trainingForm.get('instructorName').touched\" class=\"errors\">Select Name</span>\n\n          </li>\n         <div class=\"row\" style=\"margin:0px\">\n          <li class=\"col-md-4\">\n              <label>Training Subject</label>\n              <mat-select class=\"form-control zenwork-general-form-settings\" formControlName=\"trainingSubject\" [disabled]=\"disableField\">\n                  <mat-option value=\"\">Subject</mat-option>\n                  <mat-option *ngFor=\"let subjectField of trainingSubjectData\" [value]=\"subjectField.name\">{{subjectField.name}}</mat-option>\n\n              </mat-select>\n              <span *ngIf=\"trainingForm.get('trainingSubject').invalid && trainingForm.get('trainingSubject').touched\" class=\"errors\">Enter subject</span>\n          </li>\n          \n          <li class=\"col-md-4\">\n              <label>Training Type</label>\n              <mat-select class=\"form-control zenwork-general-form-settings\" formControlName=\"trainingType\" [disabled]=\"disableField\">\n                  <mat-option value=\"\">Type</mat-option>\n                  <mat-option *ngFor=\"let subjectField of trainingTypeData\" [value]=\"subjectField.name\">{{subjectField.name}}</mat-option>\n                  \n              </mat-select>\n              <span *ngIf=\"trainingForm.get('trainingType').invalid && trainingForm.get('trainingType').touched\" class=\"errors\">Enter type</span>\n          </li>\n        </div>\n        <div class=\"row\" style=\"margin:0px\">\n            <li class=\"col-md-4\">\n                <label>Score</label>\n                <input type=\"number\" class=\"form-control\" formControlName=\"score\">\n\n                <span *ngIf=\"trainingForm.get('score').invalid && trainingForm.get('score').touched\" class=\"errors\">Enter score</span>\n            </li>\n            \n            <li class=\"col-md-4\">\n                <label>Pass / Fail?</label>\n                <mat-select class=\"form-control zenwork-general-form-settings\" formControlName=\"result\">\n                    <mat-option value=\"Pass\">Pass</mat-option>\n                    <mat-option value=\"Fail\">Fail</mat-option>\n                    <mat-option value=\"N/A\">N/A</mat-option>\n                </mat-select>\n                <span *ngIf=\"trainingForm.get('result').invalid && trainingForm.get('result').touched\" class=\"errors\">Select result</span>\n            </li>\n          </div>\n          <li class=\"instructor\">\n              <label>Average Time to Complete</label>\n              <mat-select class=\"form-control zenwork-general-form-settings\" formControlName=\"timeToComplete\" [disabled]=\"disableField\">\n                  <mat-option value=\"\">Average</mat-option>\n                  <mat-option *ngFor=\"let subjectField of trainingAverageTime\" [value]=\"subjectField.name\">{{subjectField.name}}</mat-option>\n           \n              </mat-select>\n              <span *ngIf=\"trainingForm.get('timeToComplete').invalid && trainingForm.get('timeToComplete').touched\" class=\"errors\">Enter average</span>\n          </li>\n          <li class=\"col-md-6\">\n              <label>Training URL</label>\n              <input type=\"text\" class=\"form-control\" formControlName=\"trainingUrl\" [readOnly]=\"disableField\">\n              <span *ngIf=\"trainingForm.get('trainingUrl').invalid && trainingForm.get('trainingUrl').touched\" class=\"errors\">Enter url</span>\n          </li>\n          <div class=\"clearfix\"></div>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n\n        <button type=\"submit\" class=\"cancel\" (click)=\"onNoClick()\">Cancel</button>\n        <button type=\"submit\" class=\"save\" >Submit</button>\n        <div class=\"clearfix\"></div>\n      </form>\n      </div>\n  \n  \n  </div>\n"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.ts":
/*!************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.ts ***!
  \************************************************************************************************************************************/
/*! exports provided: TrainingHistoryDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainingHistoryDialogComponent", function() { return TrainingHistoryDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};








var TrainingHistoryDialogComponent = /** @class */ (function () {
    function TrainingHistoryDialogComponent(dialogRef, trainingService, myInfoService, accessLocalStorageService, swalAlertService, siteAccessRolesService, data) {
        this.dialogRef = dialogRef;
        this.trainingService = trainingService;
        this.myInfoService = myInfoService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.data = data;
        this.disableField = true;
        this.employeeData = [];
    }
    TrainingHistoryDialogComponent.prototype.ngOnInit = function () {
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
        }
        console.log(this.userId);
        this.trainingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            trainingName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            instructorName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingSubject: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            timeToComplete: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            trainingUrl: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            score: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            result: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
        });
        this.getSubjectData();
        this.employeeSearch();
        console.log(this.data);
        this.id = this.data._id;
        this.trainingForm.patchValue(this.data.training);
        this.trainingForm.get('score').patchValue(this.data.score);
        this.trainingForm.get('result').patchValue(this.data.result);
    };
    TrainingHistoryDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Edit assigned trainings
    TrainingHistoryDialogComponent.prototype.editTraining = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
            userId: this.userId,
            _id: this.id,
            score: this.trainingForm.get('score').value,
            result: this.trainingForm.get('result').value,
        };
        if (this.trainingForm.valid) {
            this.myInfoService.editAssignedTraining(data).subscribe(function (res) {
                console.log(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.dialogRef.close(res);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Get all employees
    TrainingHistoryDialogComponent.prototype.employeeSearch = function () {
        var _this = this;
        var data = {
            name: ''
        };
        this.siteAccessRolesService.getAllEmployees(data)
            .subscribe(function (res) {
            console.log("employees", res);
            _this.employeeData = res.data;
            console.log(_this.employeeData);
        }, function (err) {
            console.log(err);
        });
    };
    TrainingHistoryDialogComponent.prototype.getSubjectData = function () {
        var _this = this;
        this.trainingService.getAllTraniningData()
            .subscribe(function (res) {
            console.log("Get All Subjects Dataa", res);
            _this.subjectFields = res.temp;
            _this.trainingSubjectData = _this.subjectFields['Training Subject'],
                _this.trainingTypeData = _this.subjectFields['Training Type'],
                _this.trainingAverageTime = _this.subjectFields['Training - Average Time to Complete'],
                console.log("Get All Subjects 21345768", _this.subjectFields);
        });
    };
    TrainingHistoryDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-training-history-dialog',
            template: __webpack_require__(/*! ./training-history-dialog.component.html */ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.html"),
            styles: [__webpack_require__(/*! ./training-history-dialog.component.css */ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.css")]
        }),
        __param(6, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_3__["CompanySettingsService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__["MyInfoService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_5__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__["SwalAlertService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_7__["SiteAccessRolesService"], Object])
    ], TrainingHistoryDialogComponent);
    return TrainingHistoryDialogComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/training.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/training.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.personal-head{\n    margin: 10px;\n}\n.main-head .list-items {\n    display: inline-block;\n    padding: 0px 5px;\n}\n.zenwork-padding-20-zero{\n    padding:10px 0px!important;\n}\n.img-personal-icon{\n    width: 20px;\n}\n/* green checkbox end */\n.upcoming-training-table{\n      padding: 0 15px;\n  }\n.table-heading {\n    padding: 10px 0px 10px 10px;\n    background-color: #edf7fe;\n  }\n.table-body{\n      background: #fff;\n  }\n.upcoming-training-list .list-items{\n      padding: 15px 0;\n      border-bottom: 2px solid #eee;\n  }\n.table-body .upcoming-training-list .training-date{\n      display: block;\n      color: #3e3e3ea3;\n  }\n.table-history-list{\n      margin-bottom: 20px;\n  }\n.table-history-list .list-items{\n      display: inline-block;\n      padding: 0px 14px;\n      font-size: 14px;\n  }\n.search-icon{\n    right: 7px;\n    position: absolute;\n    top: 5px;\n    width: 13px;\n    color: #d3dbe1;\n}\n/* .filter-training-dropdown ,.filter-training-dropdown:active{\n    background-color: #fff;\n    border: none;\n    font-size: 12px;\n}\n.edit-delete-icon{\n    padding: 0 0 0 35px;\n}\n.icons-list .edit-icon{\n    width: 12px;\n}\n.icons-list .delete-icon{\n    width: 10px;\n}\n.icons-list .list-items{\n    padding: 0px 10px;\n    display: inline-block;\n}\n.complete-icon{\n    width: 15px;\n}\n.completed-table{\n    background: #fff;\n    margin: 15px;\n    padding: 15px;\n}\n.complete-img{\n    padding: 15px;\n}\n.complete-history{\n    margin: 0;\n    padding: 0 0 10px;\n    border-bottom: 1px solid #eee;\n}\n.complete-history .list-items{\n    display: inline-block;\n    padding: 0px 10px;\n}\n.employee-history{\n    padding: 0 0 0 30px;\n    border-bottom: 1px solid #eee;\n}\n.employee-history .list-items{\n    width: 15%;\n    padding: 10px 20px;\n    display: inline-block;\n}\n\n.employee-history .list-items span{\n    display: block;\n    font-size: 12px;\n}\n.zenwork-customized-back-btn{\n    background: #fff;\n    border-radius: 25px;\n    padding: 3px 10px;\n    font-size: 10px;\n}\n.btn-color{\n    border: 1px solid #EF8086;\n    color: #ef8086;\n    cursor: auto;\n  }\n  .new-hire-training{\n      padding: 0 0 15px 50px;\n      border-bottom: 1px solid #eee;\n  }\n  .new-hire-checkbox{\n    padding: 0 20px !important;\n  }\n  .new-hire-checkbox label{\n      padding: 0 0 0 10px;\n  }\n  .new-hire-checkbox label::before,.new-hire-checkbox label::after{\n      top: 3px !important;\n  }\n  .complete-summary{\n      padding: 10px 0px;\n  }\n  .visa-list-entries .list-items{\n    padding: 10px 0px;\n}\n.visa-info{\n    color:#9a9696;\n    padding: 0px 0 0 25px;\n    font-size: 12px;\n} */\n.training-tab { margin: 0 auto; float:none; padding: 0;}\n.date { height: 38px !important; line-height:inherit !important; width:80% !important;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:73%; height: auto; padding: 10px 0 10px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.find-btn {\n    background: #fff;\n    border: 1px solid #eadede;\n    padding: 10px 40px;\n    outline: none;\n}\n.apply-btn {\n    background: #439348;\n    border:none;\n    padding:6px 30px 8px; font-size: 15px; border-radius:3px;\n    outline: none;\n    color: #fff;\n}\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border {position: relative;}\n.custom-tables .table>tbody>tr>td .form-control { border: none; width: 80%; box-shadow: none; color:#000;}\n.custom-tables .table>tbody>tr>td .form-control ::-webkit-input-placeholder { /* Chrome/Opera/Safari */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control ::-moz-placeholder { /* Firefox 19+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-ms-input-placeholder { /* IE 10+ */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td .form-control :-moz-placeholder { /* Firefox 18- */\n    color: #000;\n  }\n.custom-tables .table>tbody>tr>td.zenwork-assets-checked-border:after { content: ''; position: absolute; top: 5px; left: 0px; border-left: #439348 4px solid; height: 79%; border-radius: 0px 10px 10px 0px;}\n.select-drop select { background:#fff; border: none; width: 80%; height: 34px; color:#948e8e; padding: 6px 10px;}\n.custom-tables .btn { float: right; color:#ec6469; border:#ec6469 1px solid; padding:5px 25px; border-radius: 20px; background: transparent; font-size: 15px; margin: 20px 0 0;}\n.assets-table{\n    padding: 0px 20px;\n  }\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.custom-tables { margin: 0 auto; float: none; padding: 0 15px 0 5px;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;border-top: #e0dada 1px solid;border-bottom: #e0dada 1px solid;font-size: 13px;}\n.zenwork-padding-20-zero{\n    padding:10px !important;\n}\n.search-field {\n    height: auto; border: none; box-shadow: none; padding: 10px 12px; width:100%; font-size: 15px; \n    margin-top: 20px;\n}\n.search-field:focus {\n    box-shadow: none;\n}\n.search-icon {\n    position: absolute; top:12px; right:10px; cursor: pointer; width: 5%;\n}\n.material-icons {\n    color: #8e8e8e; font-size:20px; font-weight: normal;\n}\n.form-group {\n    position: relative;\n}\n.next-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 20px;\n    color: #fff;\n    padding: 0 30px;    \n}\n.personal-head label { display: block; padding: 0 0 10px;}\n.training-history-table label {display: block; padding: 0 0 10px;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/training.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/training.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"training-tab col-md-11\">\n  <div class=\"personal-head\">\n    <ul class=\"list-unstyled main-head pull-right\">\n      <li class=\"list-items\">\n        <label>Completion Date</label>\n          <div class=\"date\">\n              <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"completion_date\" [min]=\"assignedDate\">\n              <mat-datepicker #picker1></mat-datepicker>\n              <button mat-raised-button (click)=\"picker1.open()\">\n                <span>\n                  <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                </span>\n              </button>\n              <div class=\"clearfix\"></div>\n            </div>\n      </li>\n      <li class=\"list-items\">\n         <button class=\"find-btn\" (click)=\"findTrainingDialog()\">Find Training</button>\n      </li>\n    </ul>\n    <div class=\"clearfix\"></div>\n  </div>\n  \n  <div class=\"custom-tables\">\n      <table class=\"table\">\n        <thead>\n          <tr>\n            <th>\n            \n            </th>\n            <th>Active Training</th>\n            <th>Assigned Date</th>\n            <th>Due Date</th>\n            <th>Status</th>\n            \n            <th>\n              <mat-icon *ngIf=\"checkAssignTrainingIds.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n              <mat-menu #menu=\"matMenu\">\n                <button *ngIf=\"(!checkAssignTrainingIds.length <=0) && (checkAssignTrainingIds.length ==1)\"  mat-menu-item (click)=\"markAsCompleteTraining()\">Mark as Complete</button>\n                <button *ngIf=\"(!checkAssignTrainingIds.length <=0) && (checkAssignTrainingIds.length >=1)\"  mat-menu-item (click)=\"deleteAssignedTrainings()\">Delete</button>\n              </mat-menu>\n            </th>\n          </tr>\n        </thead>\n        <tbody>\n\n          <tr *ngFor=\"let trainings of assignedTrainings\">\n            <td [ngClass]=\"{'zenwork-checked-border': trainings.isChecked}\">\n\n                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"trainings.isChecked\" (change)=\"checkAssignTrainings($event,trainings._id)\">\n                      \n                </mat-checkbox>\n            </td>\n            <td>\n              <span *ngIf=\"trainings.training\"> {{trainings.training.trainingName}}</span>\n             \n            </td>\n            <td>{{trainings.assigned_date | date : 'MM/dd/yyyy'}}</td>\n            <td>{{trainings.due_date | date : 'MM/dd/yyyy'}}</td>\n            <td>{{trainings.status}}</td>\n            <td></td>\n            \n          </tr>\n\n\n      </table>\n      <hr class=\"zenwork-margin-ten-zero\">\n    </div>\n  <hr class=\"zenwork-margin-ten-zero\">\n  <div class=\"training-history-table\">\n    <div>\n        <div class=\"zenwork-padding-20-zero pull-left\">\n            <p style=\"font-size:17px;\">Training History</p> \n           </div>\n           <div class=\"clearfix\"></div>\n      <ul class=\"list-unstyled table-history-list\">\n      \n        <li class=\"list-items\">\n         <label>From Date</label>\n         <div class=\"date\">\n            <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"from_date\"\n            [max]=\"to_date\" #fdate (dateInput)=\"onChangeFromValue(fdate.value)\">\n            <mat-datepicker #picker2></mat-datepicker>\n            <button mat-raised-button (click)=\"picker2.open()\">\n              <span>\n                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n              </span>\n            </button>\n            <div class=\"clearfix\"></div>\n          </div>\n        </li>\n        <li class=\"list-items\">\n             <label>To Date</label>\n             <div class=\"date\">\n                <input matInput [matDatepicker]=\"picker3\" placeholder=\"mm/dd/yy\" class=\"form-control\" [(ngModel)]=\"to_date\"\n                [min]=\"from_date\" #tdate (dateInput)=\"onChangeToValue(tdate.value)\">\n                <mat-datepicker #picker3></mat-datepicker>\n                <button mat-raised-button (click)=\"picker3.open()\">\n                  <span>\n                    <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                  </span>\n                </button>\n                <div class=\"clearfix\"></div>\n              </div>\n        </li>\n        <!-- <li class=\"list-items\">\n          <button class=\"apply-btn\" (click)=\"applyDateRange()\">Apply</button>\n        </li>  -->\n        <li class=\"list-items pull-right\" style=\"width:32%\">\n            <div class=\"form-group\">\n           \n                <input type=\"mail\" class=\"form-control search-field\" placeholder=\"Search by training name\" [(ngModel)]=\"searchString\" (ngModelChange)=\"searchFilter()\">\n                <span class=\"search-icon\"><i class=\"material-icons\">search</i></span>\n            \n            </div>\n        </li>\n    \n      </ul>\n    </div>\n    <div class=\"custom-tables\">\n        <table class=\"table\">\n          <thead>\n            <tr>\n              <th>\n              \n              </th>\n              <th>Training Name</th>\n              <th>Start Date</th>\n              <th>Completion Date</th>\n              <th>Status</th>\n              <th>Score</th>\n              <th>Pass/Fail</th>\n              \n              <th>\n                <mat-icon *ngIf=\"checkHistoryTrainingIds.length\" class=\"cursor\" [matMenuTriggerFor]=\"menu1\">more_vert</mat-icon>\n                <mat-menu #menu1=\"matMenu\">\n                    <button *ngIf=\"(!checkHistoryTrainingIds.length <=0) && (checkHistoryTrainingIds.length ==1)\" mat-menu-item (click)=\"editTraining()\">View</button>\n                  <button *ngIf=\"(!checkHistoryTrainingIds.length <=0) && (checkHistoryTrainingIds.length ==1)\" mat-menu-item (click)=\"editTraining()\">Edit</button>\n                  <button *ngIf=\"(!checkHistoryTrainingIds.length <=0) && (checkHistoryTrainingIds.length >=1)\" mat-menu-item (click)=\"deleteHistoryTraining()\">Delete</button>\n                </mat-menu>\n              </th>\n            </tr>\n          </thead>\n          <tbody>\n  \n            <tr *ngFor=\"let traininHistory of allAssignedHistoryTrainings\">\n              <td [ngClass]=\"{'zenwork-checked-border': traininHistory.isChecked}\">\n  \n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"traininHistory.isChecked\" (change)=\"checkTrainingsHistory($event,traininHistory._id)\">\n                        \n                  </mat-checkbox>\n              </td>\n              <td>{{traininHistory.training.trainingName}}</td>\n              <td>{{traininHistory.start_date | date : 'MM/dd/yyyy'}}</td>\n              <td>{{traininHistory.completion_date | date : 'MM/dd/yyyy'}}</td>\n              <td>{{traininHistory.status}}</td>\n              <td>{{traininHistory.score}}</td>\n              <td>{{traininHistory.result}}</td>\n              <td></td>\n            </tr>\n  \n  \n        </table>\n        <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n        </mat-paginator>\n      </div>\n  </div>\n\n  <hr class=\"zenwork-margin-ten-zero\">\n  <div style=\"margin:20px 0px;\">\n  \n      <button mat-button class=\"next-btn pull-right\" (click)=\"nextTab()\">Save Changes</button>\n  <div class=\"clear-fix\"></div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/training/training/training.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/training/training/training.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: TrainingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainingComponent", function() { return TrainingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _find_training_dialog_find_training_dialog_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./find-training-dialog/find-training-dialog.component */ "./src/app/admin-dashboard/employee-management/training/training/find-training-dialog/find-training-dialog.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _training_history_dialog_training_history_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./training-history-dialog/training-history-dialog.component */ "./src/app/admin-dashboard/employee-management/training/training/training-history-dialog/training-history-dialog.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TrainingComponent = /** @class */ (function () {
    function TrainingComponent(dialog, myInfoService, accessLocalStorageService, swalAlertService, router) {
        this.dialog = dialog;
        this.myInfoService = myInfoService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.allAssignedTrainings = [];
        this.assignedTrainings = [];
        this.allAssignedHistoryTrainings = [];
        this.selectedMarkAsCompleted = [];
        this.selectedTrainingHistories = [];
        this.checkAssignTrainingIds = [];
        this.checkHistoryTrainingIds = [];
        this.pagesAccess = [];
        this.trainingTabView = false;
    }
    TrainingComponent.prototype.ngOnInit = function () {
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.employeeDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.employeeDetails) {
            this.userId = this.employeeDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.userId = JSON.parse(localStorage.getItem('employeeId'));
            this.pageAccesslevels();
        }
        console.log(this.userId);
        this.listAssignedTrainings();
        this.listAssignedHistoryTrainings();
    };
    TrainingComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Training" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.trainingTabView = true;
                    console.log('loggss', _this.trainingTabView);
                }
            });
        }, function (err) {
        });
    };
    TrainingComponent.prototype.searchFilter = function () {
        console.log(this.searchString);
        this.listAssignedHistoryTrainings();
    };
    TrainingComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.listAssignedHistoryTrainings();
    };
    // selectTraining(assignDate){
    //   console.log(assignDate);
    //    this.assignedDate = assignDate;
    // }
    //  Author: Saiprakash, Date: 17/05/19
    //  Find training open dialog
    TrainingComponent.prototype.findTrainingDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_find_training_dialog_find_training_dialog_component__WEBPACK_IMPORTED_MODULE_1__["FindTrainingDialogComponent"], {
            width: '1300px',
            height: '800px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed', result);
            _this.listAssignedTrainings();
        });
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Training history open dialog
    TrainingComponent.prototype.trainingHistoryOpen = function () {
        var _this = this;
        if (this.getSingleTraining) {
            var dialogRef = this.dialog.open(_training_history_dialog_training_history_dialog_component__WEBPACK_IMPORTED_MODULE_6__["TrainingHistoryDialogComponent"], {
                width: '1000px',
                height: '700px',
                data: this.getSingleTraining
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.listAssignedHistoryTrainings();
                _this.checkHistoryTrainingIds = [];
            });
        }
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Get all list assigned trainings
    TrainingComponent.prototype.listAssignedTrainings = function () {
        var _this = this;
        var data1 = {
            companyId: this.companyId,
            userId: this.userId,
            onlyActive: 1
        };
        this.myInfoService.listAssignedTrainings(data1).subscribe(function (res) {
            console.log(res);
            _this.assignedTrainings = res.data;
            _this.assignedTrainings.forEach(function (element) {
                if (element.training !== null) {
                    console.log(element.training);
                    _this.allAssignedTrainings = element;
                }
            });
        }, function (err) {
            console.log(err);
        });
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Mark as completed trainings
    TrainingComponent.prototype.markAsCompleteTraining = function () {
        var _this = this;
        this.selectedMarkAsCompleted = this.assignedTrainings.filter(function (trainingCheck) {
            return trainingCheck.isChecked;
        });
        if (this.selectedMarkAsCompleted.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one training to proceed", "", 'error');
        }
        else if (this.selectedMarkAsCompleted.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one training to proceed", "", 'error');
        }
        else {
            var data3 = {
                companyId: this.companyId,
                userId: this.userId,
                _id: this.selectedMarkAsCompleted[0]._id,
                completion_date: this.completion_date
            };
            this.myInfoService.markAsComplete(data3).subscribe(function (res) {
                console.log(res);
                _this.completion_date = '';
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.checkAssignTrainingIds = [];
                _this.listAssignedTrainings();
                _this.listAssignedHistoryTrainings();
            }, function (err) {
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    TrainingComponent.prototype.onChangeFromValue = function (value) {
        // console.log('fromDate', value);
        var f = new Date(value);
        this.fromDate = f.toISOString();
        console.log(this.fromDate);
        this.listAssignedHistoryTrainings();
    };
    TrainingComponent.prototype.onChangeToValue = function (value) {
        console.log('toDate', value);
        var t = new Date(value);
        this.toDate = t.toISOString();
        console.log(this.toDate);
        this.listAssignedHistoryTrainings();
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Get all list assigned history trainings
    TrainingComponent.prototype.listAssignedHistoryTrainings = function () {
        var _this = this;
        var data2 = {
            companyId: this.companyId,
            userId: this.userId,
            onlyActive: 0,
            per_page: this.perPage,
            page_no: this.pageNo,
            from_date: this.fromDate,
            to_date: this.toDate,
            search_query: this.searchString
        };
        this.myInfoService.listAssignedTrainings(data2).subscribe(function (res) {
            console.log(res);
            _this.allAssignedHistoryTrainings = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // applyDateRange(){
    //   this.listAssignedHistoryTrainings();
    // }
    //  Author: Saiprakash, Date: 17/05/19
    //  Get single assigned training
    TrainingComponent.prototype.editTraining = function () {
        var _this = this;
        this.selectedTrainingHistories = this.allAssignedHistoryTrainings.filter(function (trainingCheck) {
            return trainingCheck.isChecked;
        });
        if (this.selectedTrainingHistories.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one training to proceed", "", 'error');
        }
        else if (this.selectedTrainingHistories.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Please select one training to proceed", "", 'error');
        }
        else {
            this.myInfoService.getAssignedTraining(this.companyId, this.userId, this.selectedTrainingHistories[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getSingleTraining = res.data;
                _this.trainingHistoryOpen();
            }, function (err) {
                console.log(err);
            });
        }
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Delete assigned trainings
    TrainingComponent.prototype.deleteAssignedTrainings = function () {
        var _this = this;
        this.selectedMarkAsCompleted = this.assignedTrainings.filter(function (trainingCheck) {
            return trainingCheck.isChecked;
        });
        var deleteTrainings = {
            companyId: this.companyId,
            userId: this.userId,
            _ids: this.selectedMarkAsCompleted
        };
        this.myInfoService.deleteAssignedTrainings(deleteTrainings).subscribe(function (res) {
            console.log(res);
            _this.checkAssignTrainingIds = [];
            _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            _this.listAssignedTrainings();
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
        });
    };
    //  Author: Saiprakash, Date: 17/05/19
    //  Delete assigned history trainings
    TrainingComponent.prototype.deleteHistoryTraining = function () {
        var _this = this;
        this.selectedTrainingHistories = this.allAssignedHistoryTrainings.filter(function (trainingCheck) {
            return trainingCheck.isChecked;
        });
        if (this.selectedTrainingHistories.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select assigned training to proceed", "", 'error');
        }
        else {
            var deleteTrainings = {
                companyId: this.companyId,
                userId: this.userId,
                _ids: this.selectedTrainingHistories
            };
            this.myInfoService.deleteAssignedTrainings(deleteTrainings).subscribe(function (res) {
                console.log(res);
                _this.checkHistoryTrainingIds = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.listAssignedHistoryTrainings();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    TrainingComponent.prototype.checkAssignTrainings = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkAssignTrainingIds.push(id);
        }
        console.log(this.checkAssignTrainingIds);
        if (event.checked == false) {
            var index = this.checkAssignTrainingIds.indexOf(id);
            if (index > -1) {
                this.checkAssignTrainingIds.splice(index, 1);
            }
            console.log(this.checkAssignTrainingIds);
        }
    };
    TrainingComponent.prototype.checkTrainingsHistory = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkHistoryTrainingIds.push(id);
        }
        console.log(this.checkHistoryTrainingIds);
        if (event.checked == false) {
            var index = this.checkHistoryTrainingIds.indexOf(id);
            if (index > -1) {
                this.checkHistoryTrainingIds.splice(index, 1);
            }
            console.log(this.checkHistoryTrainingIds);
        }
    };
    TrainingComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/documents"]);
    };
    TrainingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-training',
            template: __webpack_require__(/*! ./training.component.html */ "./src/app/admin-dashboard/employee-management/training/training/training.component.html"),
            styles: [__webpack_require__(/*! ./training.component.css */ "./src/app/admin-dashboard/employee-management/training/training/training.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_3__["MyInfoService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], TrainingComponent);
    return TrainingComponent;
}());



/***/ }),

/***/ "./src/app/services/site-access-roles-service.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/site-access-roles-service.service.ts ***!
  \***************************************************************/
/*! exports provided: SiteAccessRolesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteAccessRolesService", function() { return SiteAccessRolesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SiteAccessRolesService = /** @class */ (function () {
    function SiteAccessRolesService(http) {
        this.http = http;
    }
    SiteAccessRolesService.prototype.getStandardOnboardingtemplate = function (id) {
        // return this.http.get(environment.url + "/api" + environment.version + "/employee-management/onboarding/onboard-template/getTemplate/" + id)
        // .pipe(map(res => res));
    };
    SiteAccessRolesService.prototype.getAllEmployees = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/searchEmployee", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getuserRoleEmployees = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/users/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.chooseEmployeesData = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/chooseEmployeesForRoles", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.postEmpDatatoRole = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/addGroupsToUsers", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAlltabsData = function (cID, rID, category) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/get/" + cID + "/" + rID + "/" + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.UpdateRoleType = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getBaseRoles = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/baseroles")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.createRoleType = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/addUpdate", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.allPagesData = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/all/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllPagesForCompany = function (cid, rid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/" + cid + "/" + rid)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getSingleCustomRole = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/role/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.pagesCreation = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/create/all", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.pagesUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/all/pages", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.personalFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.jobFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.emergencyFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.compensationFieldsUpdate = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/update", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.postpersonalfields = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/create", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.updateRoles = function (data) {
        console.log("seerveice array", data);
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/" + data._id, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.updateMultipleRoles = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/pages/update/all/pages", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.companyId = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/company/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.deleteRole = function (id) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/delete/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllMyinfoFields = function (roleid, cId, category) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/fields/get/" + cId + "/" + roleid + "/" + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesforRoles = function (cId, Rid) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/employee-management/onboarding/hire-wizard/users-under-group/" + cId + "/" + Rid)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesUnderManager = function (cId, uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/offboarding/getAllDirectReports/" + cId + "/" + uId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.getAllEmployeesforCustomRoles = function (rId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/userIds/" + rId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService.prototype.changeSiteAccessRoleforEmp = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/change-user-role", data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    SiteAccessRolesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SiteAccessRolesService);
    return SiteAccessRolesService;
}());



/***/ })

}]);
//# sourceMappingURL=training-training-module.js.map