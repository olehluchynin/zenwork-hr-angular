(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-hire-hr-new-hire-hr-module"],{

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background: #fff;\n    /* border: 1px solid #ccc; */\n    border-radius: 25px;\n    padding: 3px 10px;\n    font-size: 12px;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 14px!important;   \n}\n.mr-7{\n    margin-right: 7px;\n}\n.main-newhirehr{\n    margin: 0 auto;\n    float: none;\n    padding: 0;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0 25px 10px!important;\n}\n.zenwork-inner-icon{\n    width: 20px;\n    height: auto;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n/* input select{\n    border:0!important;\n} */\n.zenwork-general-form-settings{\n    width: 18%!important;\n    border-radius: 0px!important;\n}\n.name-list .list-name-field{\n    display:inline-block;\n    padding:0px 10px;\n}\n.address, .contact{\n    margin: 0 0 0 10px;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n}\n.birth-date .padding10-listitems{\n    padding: 0px 10px;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\n/* input {\n    height:30px;\n} */\n/* .birth-date{\n    position:relative;\n} */\n.birth-date-list{\n    position: relative;\n    display: inline-block;\n}\n.birthdate{\n    font-size: 12px;\n    padding-left: 15px;\n    width: 178px;   \n    box-shadow: none;\n}\n.calendar-icon{\n    position: absolute;\n    top: 38px;\n    width: 15px;\n    right: 15px;\n}\n.zenwork-structure-settings{\n    width: 178px!important;\n    border-radius: 0px!important;\n    height:34px;\n    border: 0;\n    box-shadow: none;\n}\n.gender-list{\n    padding:0px 10px;\n}\n.gender .gender-list label,.ethnicity .ethnicity-list label{\n    display: block;\n}\n.list-martial-field{\n    display: inline-block;\n    padding: 0px 10px;\n    /* width: 22%; */\n}\n/* .martial-status{\n    position:relative;\n} */\n.list-martial-field-date{\n   /* width:18%!important; */\n   display:inline-block;\n   position: relative;\n   padding: 0px 10px;\n}\n.martial-dropdown{\n    border-radius:0px!important;\n    border:0px!important;\n    height:34px;\n    width:178px!important;\n    box-shadow: none;\n}\n.effectivedate{\n\n}\n.calendar-icon-martial{\n    width: 15px;\n    position: absolute;\n    top: 40px;\n    right: 10px;\n}\n.ethnicity{\n    margin-bottom: 20px;\n}\n.ethnicity-list select{\n    width:39%!important;\n}\n.zenwork-padding-20-zero{\n    padding:20px 0px!important;\n}\n.list-items-address{\n    padding: 10px 0px;\n}\n.list-items-address .text-field{\n    font-size: 14px;\n    padding-left: 15px;\n}\n.zipcode-input{\n    width:auto;\n}\n.list-items-addresslocal .text-field{\n    font-size: 14px;\n    padding-left: 15px;\n    width: 100%;\n}\ninput{\n    height:34px;\n    border:0;\n    width: 178px;\n    outline: none;\n}\nselect{\n    font-size: 12px;\n}\n.address1{\n    width:41%;\n    /* border:1px solid #e5e5e5; */\n}\n.list-items-addresslocal{\n    display: inline-block;\n    margin: 0px 10px 0px 0px;\n    width: 20%;\n}\n.city{\n    margin: 0px 10px 0px 0px;\n}\n.country-items{\n    margin-top:20px;\n}\n.zenwork-margin-twenty-zero{\n    margin: 20px 0px!important;\n}\n.ext{\n    width: 70px;\n    margin-left: 15px!important;\n}\n.text-field-ext{\n    width:8%;\n    font-size: 14px;\n    padding-left:25px;\n}\n.contact-details .text-field{\n    font-size: 14px;\n    padding-left:35px;\n}\n.contact-details{\n    position: relative;\n}\n.landphone-img{\n    position: absolute;\n    width: 15px;\n    top: 37px;\n    left: 10px;\n}\n.mobile-img{\n    position: absolute;\n    width: 12px;\n    top: 6px;\n    left: 11px;\n}\n.home-img{\n    position: absolute;\n    width: 15px;\n    top: 7px;\n    left: 8px;\n}\n.email-item{\n    padding: 10px 0px;\n    width: 28%;\n}\n.workingemail-img{\n    position: absolute;\n    width: 15px;\n    top: 47px;\n    left: 10px;\n}\n.homeemail-img{\n    position: absolute;\n    width: 15px;\n    top: 94px;\n    left: 8px;\n}\n.email-item .workphone{\n    width: 100%;\n}\n.zenwork-setting .btn {border-radius: 20px; height: 34px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:20px 0 0;}\n.zenwork-setting .btn:hover {background: #008f3d; color:#fff;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management/on-boarding/on-boarding']\">\n      <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n    </a>\n    <span class=\"inner-icon-img\">\n      <img src=\"../../../assets/images/Onboarding/Group 828.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n    </span>\n    <small class=\"sub-title\">\n      <b>New Hire HR</b>\n    </small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n<div class=\"col-md-11 main-newhirehr\">\n  <div class=\"green zenwork-padding-25-zero\">\n    Details\n    <span class=\"caret\"></span>\n  </div>\n  <div class=\"form-group\">\n    <div class=\"detail-section\">\n      <ul class=\"list-unstyled name-list\">\n        <li class=\"list-name-field\">\n          <label>First Name</label>\n          <br>\n          <input type=\"text\" name=\"fname\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.name.firstName\">\n        </li>\n        <li class=\"list-name-field\">\n          <label>Middle Name</label>\n          <br>\n          <input type=\"text\" name=\"fname\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.name.middleName\">\n        </li>\n        <li class=\"list-name-field\">\n          <label>Last Name</label>\n          <br>\n          <input type=\"text\" name=\"fname\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.name.lastName\">\n        </li>\n        <li class=\"list-name-field\">\n          <label>Prefered Name</label>\n          <br>\n          <input type=\"text\" name=\"fname\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.name.prefferedName\">\n        </li>\n      </ul>\n      <ul class=\"list-unstyled birth-date\">\n        <li class=\"birth-date-list padding10-listitems\">\n          <label>Birth Date</label>\n          <br>\n          <input class=\"form-control birthdate\" #dpMDY=\"bsDatepicker\" bsDatepicker [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" placeholder=\"mm/dd/yyyy\"\n            name=\"birth-date\" [(ngModel)]=\"newEmployeeDetails.birth\">\n          <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon\" alt=\"Company-settings icon\" (click)=\"dpMDY.toggle()\"\n            [attr.aria-expanded]=\"dpMDY.isOpen\">\n\n        </li>\n      </ul>\n      <ul class=\"list-unstyled gender\">\n        <li class=\"gender-list padding10-listitems\">\n          <label>Gender</label>\n          <mat-select class=\"form-control zenwork-structure-settings\" [(ngModel)]=\"newEmployeeDetails.gender\">\n            <mat-option value=\"Male\">Male</mat-option>\n            <mat-option value=\"Female\">Female</mat-option>\n            <mat-option value=\"Others\">Others</mat-option>\n          </mat-select>\n        </li>\n      </ul>\n      <ul class=\"list-unstyled martial-status\">\n        <li class=\"list-martial-field\">\n          <label>Marital Status</label>\n          <br>\n          <mat-select class=\"form-control martial-dropdown\" [(ngModel)]=\"newEmployeeDetails.maritalStatus\">\n            <mat-option value=\"Single\">Single</mat-option>\n            <mat-option value=\"Married\">Married</mat-option>\n            <mat-option value=\"Divorced\">Divorced</mat-option>\n            <mat-option value=\"Widowed\">Widowed</mat-option>\n          </mat-select>\n        </li>\n        <li class=\"list-martial-field-date\">\n          <label>Effective Date</label>\n          <br>\n          <input class=\"form-control effectivedate\" #dpMDY1=\"bsDatepicker\" bsDatepicker [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n            name=\"martial-date\" class=\"\" [(ngModel)]=\"newEmployeeDetails.effectiveDate\">\n          <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon-martial\" alt=\"Company-settings icon\" (click)=\"dpMDY1.toggle()\"\n            [attr.aria-expanded]=\"dpMDY1.isOpen\">\n        </li>\n      </ul>\n      <ul class=\"list-unstyled gender\">\n        <li class=\"gender-list padding10-listitems\">\n          <label>Veteran Status</label>\n          <mat-select class=\"form-control zenwork-structure-settings\" [(ngModel)]=\"newEmployeeDetails.veteranStatus\">\n            <mat-option value=\"true\">\n              Yes\n            </mat-option>\n            <mat-option value=\"false\">\n              No\n            </mat-option>\n          </mat-select>\n\n        </li>\n      </ul>\n      <ul class=\"list-unstyled name-list\">\n        <li class=\"list-name-field\">\n          <label>SSN</label>\n          <br>\n          <input type=\"text\" name=\"ssn\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.ssn\">\n        </li>\n        <li class=\"list-name-field\">\n          <label>SIN</label>\n          <br>\n          <input type=\"text\" name=\"sin\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.sin\">\n        </li>\n        <li class=\"list-name-field\">\n          <label>NIN</label>\n          <br>\n          <input type=\"text\" name=\"nin\" class=\"names\" [(ngModel)]=\"newEmployeeDetails.nin\">\n        </li>\n      </ul>\n      <ul class=\"list-unstyled ethnicity\">\n        <li class=\"gender-list padding10-listitems ethnicity-list\">\n          <label>Ethnicity</label>\n          <mat-select class=\"form-control zenwork-structure-settings \" [(ngModel)]=\"newEmployeeDetails.ethnicity\">\n            <mat-option value=\"American\">American</mat-option>\n            <mat-option value=\"Asian\">Asian</mat-option>\n            <mat-option value=\"Indian\">Indian</mat-option>\n            <mat-option value=\"Japanese\">Japanese</mat-option>\n            <mat-option value=\"Chinese\">Chinese</mat-option>\n          </mat-select>\n        </li>\n      </ul>\n      <hr class=\"zenwork-margin-ten-zero\">\n    </div>\n\n    <div class=\"address\">\n      <div class=\"green zenwork-padding-20-zero\">\n        Address\n        <span class=\"caret\"></span>\n      </div>\n      <ul class=\"list-unstyled \">\n        <li class=\"list-items-address\">\n          <input type=\"text\" class=\"address1 text-field\" placeholder=\"Street1\" [(ngModel)]=\"newEmployeeDetails.address.address1\">\n        </li>\n        <li class=\"list-items-address\">\n          <input type=\"text\" class=\"address1 text-field\" placeholder=\"Street2\" [(ngModel)]=\"newEmployeeDetails.address.address2\">\n        </li>\n      </ul>\n      <ul class=\"list-unstyled\">\n        <li class=\"list-items-addresslocal city\">\n          <input type=\"text\" class=\"address-city text-field\" placeholder=\"City\" [(ngModel)]=\"newEmployeeDetails.address.city\">\n        </li>\n        <li class=\"list-items-addresslocal\">\n          <input type=\"text\" class=\"address-city text-field\" placeholder=\"State\" [(ngModel)]=\"newEmployeeDetails.address.state\">\n        </li>\n        <li class=\"list-items-addresslocal\">\n          <input type=\"text\" class=\"address-city text-field zipcode-input\" placeholder=\"ZIP\" [(ngModel)]=\"newEmployeeDetails.address.zipcode\">\n        </li>\n        <br>\n      </ul>\n      <ul class=\"list-unstyled\">\n        <li class=\"country-items\">\n          <mat-select class=\"form-control zenwork-structure-settings \" placeholder=\"Country\" [(ngModel)]=\"newEmployeeDetails.address.country\">\n            <mat-option value=\"USA\">United States</mat-option>\n            <mat-option value=\"Asia\">Asia</mat-option>\n            <mat-option value=\"India\">India</mat-option>\n            <mat-option value=\"Japan\">Japan</mat-option>\n            <mat-option value=\"China\">China</mat-option>\n          </mat-select>\n        </li>\n      </ul>\n      <hr class=\"zenwork-margin-twenty-zero\">\n\n    </div>\n    <div class=\"contact\">\n      <div class=\"green zenwork-padding-20-zero\">\n        Contact\n        <span class=\"caret\"></span>\n      </div>\n      <ul class=\"list-unstyled contact-details working-phone\">\n        <li class=\"phone-items\">\n          <label>Phone</label>\n          <br>\n          <img src=\"../../../assets/images/Onboarding/ic_domain_24px.png\" class=\"landphone-img\" alt=\"land-phone-icon\">\n\n          <input type=\"number\" class=\"workphone text-field\" placeholder=\"Work phone\" [(ngModel)]=\"newEmployeeDetails.contact.workPhone\">\n          <input type=\"number\" class=\"ext text-field-ext\" placeholder=\"Ext\" [(ngModel)]=\"newEmployeeDetails.contact.extention\">\n        </li>\n      </ul>\n      <ul class=\"list-unstyled contact-details mobile-phone\">\n        <li class=\"mobile\">\n          <img src=\"../../../assets/images/Onboarding/Group 1127.png\" class=\"mobile-img\" alt=\"mobile-phone-icon\">\n          <input type=\"number\" class=\"mobile-phone text-field\" placeholder=\"Mobile Phone\" [(ngModel)]=\"newEmployeeDetails.contact.mobilePhone\">\n\n        </li>\n      </ul>\n      <ul class=\"list-unstyled contact-details home-phone\">\n        <li class=\"Home-phone\">\n          <img src=\"../../../assets/images/Onboarding/ic_home_24px.png\" class=\"home-img\" alt=\"home-phone-icon\">\n          <input type=\"number\" class=\"home-phone text-field\" placeholder=\"Home Phone\" [(ngModel)]=\"newEmployeeDetails.contact.homePhone\">\n\n        </li>\n      </ul>\n      <ul class=\"list-unstyled contact-details mobile-phone\">\n        <label>Email</label>\n        <br>\n        <li class=\"email-item\">\n          <img src=\"../../../assets/images/Onboarding/ic_domain_24px.png\" class=\"workingemail-img\" alt=\"land-phone-icon\">\n          <input type=\"email\" class=\"workphone text-field\" placeholder=\"Work Email\" [(ngModel)]=\"newEmployeeDetails.contact.workMail\">\n        </li>\n        <li class=\"Home-email\">\n          <img src=\"../../../assets/images/Onboarding/ic_home_24px.png\" class=\"homeemail-img\" alt=\"home-phone-icon\">\n          <input type=\"email\" class=\"home-phone text-field\" placeholder=\"Home Email\" [(ngModel)]=\"newEmployeeDetails.contact.personalMail\">\n        </li>\n      </ul>\n    </div>\n    <hr class=\"zenwork-margin-twenty-zero\">\n  </div>\n  <div class=\"zenwork-setting\">\n    <button class=\"btn\" (click)=\"hireNewEmployee()\">Save Changes</button>\n    <button class=\"btn\">Cancel</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.ts ***!
  \******************************************************************************************/
/*! exports provided: NewHireHrComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewHireHrComponent", function() { return NewHireHrComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/employee.service */ "./src/app/services/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NewHireHrComponent = /** @class */ (function () {
    function NewHireHrComponent(employeeOnboardingService, swalAlertService) {
        this.employeeOnboardingService = employeeOnboardingService;
        this.swalAlertService = swalAlertService;
        /* minDate = new Date(100, 5, 10);
        maxDate = new Date(4000, 9, 15); */
        this.newEmployeeDetails = {
            name: { firstName: '', lastName: '', middleName: '', prefferedName: '' },
            birth: '',
            gender: '',
            maritalStatus: '',
            effectiveDate: '',
            veteranStatus: '',
            ssn: '',
            sin: '',
            nin: '',
            ethnicity: '',
            type: '',
            contact: {
                workPhone: '',
                extention: '',
                mobilePhone: '',
                homePhone: '',
                workMail: '',
                personalMail: ''
            },
            address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                zipcode: '',
                country: ''
            },
        };
    }
    NewHireHrComponent.prototype.ngOnInit = function () {
    };
    /* myForm = new FormGroup({
      myDateYMD: new FormControl(new Date()),
      myDateFull: new FormControl(new Date()),
      myDateMDY: new FormControl(new Date())
    }); */
    NewHireHrComponent.prototype.hireNewEmployee = function () {
        var _this = this;
        this.employeeOnboardingService.addNewEmployee(this.newEmployeeDetails)
            .subscribe(function (res) {
            if (res.status) {
                _this.newEmployeeDetails = {
                    name: { firstName: '', lastName: '', middleName: '', prefferedName: '' },
                    birth: '',
                    gender: '',
                    maritalStatus: '',
                    effectiveDate: '',
                    veteranStatus: '',
                    ssn: '',
                    sin: '',
                    nin: '',
                    ethnicity: '',
                    type: '',
                    contact: {
                        workPhone: '',
                        extention: '',
                        mobilePhone: '',
                        homePhone: '',
                        workMail: '',
                        personalMail: ''
                    },
                    address: {
                        address1: '',
                        address2: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        country: ''
                    },
                };
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employees Onboarding", "Employee onboarded successfully", 'success');
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employees Onboarding", "Data Not Found", 'info');
            }
        }, function (err) {
            _this.swalAlertService.SweetAlertWithoutConfirmation("Employees Onboarding", err.error.message, 'error');
        });
    };
    NewHireHrComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-hire-hr',
            template: __webpack_require__(/*! ./new-hire-hr.component.html */ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.html"),
            styles: [__webpack_require__(/*! ./new-hire-hr.component.css */ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.css")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_1__["SwalAlertService"]])
    ], NewHireHrComponent);
    return NewHireHrComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.module.ts ***!
  \***************************************************************************************/
/*! exports provided: NewHireHrModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewHireHrModule", function() { return NewHireHrModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _new_hire_hr_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-hire-hr.routing */ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.routing.ts");
/* harmony import */ var _new_hire_hr_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-hire-hr.component */ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var NewHireHrModule = /** @class */ (function () {
    function NewHireHrModule() {
    }
    NewHireHrModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _new_hire_hr_routing__WEBPACK_IMPORTED_MODULE_2__["NewhirehrRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSelectModule"]
            ],
            declarations: [_new_hire_hr_component__WEBPACK_IMPORTED_MODULE_3__["NewHireHrComponent"]]
        })
    ], NewHireHrModule);
    return NewHireHrModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.routing.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.routing.ts ***!
  \****************************************************************************************/
/*! exports provided: NewhirehrRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewhirehrRouting", function() { return NewhirehrRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _new_hire_hr_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-hire-hr.component */ "./src/app/admin-dashboard/employee-management/new-hire-hr/new-hire-hr.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: '', redirectTo: "/new-hire-hr", pathMatch: "full" },
    { path: 'new-hire-hr', component: _new_hire_hr_component__WEBPACK_IMPORTED_MODULE_2__["NewHireHrComponent"] },
];
var NewhirehrRouting = /** @class */ (function () {
    function NewhirehrRouting() {
    }
    NewhirehrRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NewhirehrRouting);
    return NewhirehrRouting;
}());



/***/ })

}]);
//# sourceMappingURL=new-hire-hr-new-hire-hr-module.js.map