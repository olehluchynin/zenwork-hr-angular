(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375"],{

/***/ "./src/app/services/my-info.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/my-info.service.ts ***!
  \*********************************************/
/*! exports provided: MyInfoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyInfoService", function() { return MyInfoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyInfoService = /** @class */ (function () {
    function MyInfoService(http) {
        this.http = http;
    }
    MyInfoService.prototype.uploadPhoto = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/personal/upload", data);
    };
    MyInfoService.prototype.getSingleTabSettingForMyinfo = function (tab) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/settingsOfMyInfoTab/" + tab)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    MyInfoService.prototype.basicDetailsGetAll = function (data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/settingsOfMyInfoTab/" + data);
    };
    MyInfoService.prototype.updateUSer = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/update", data);
    };
    MyInfoService.prototype.getOtherUser = function (company_id, user_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/getOtherUser" + "/" + company_id + "/" + user_id);
    };
    MyInfoService.prototype.updateCurrentCompensation = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/compensation/updateCurrentCompensation", data);
    };
    MyInfoService.prototype.getStandardAndCustomStructureFields = function (company_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/structure/getStandardAndCustomFields/" + company_id);
    };
    MyInfoService.prototype.addEmergenctContact = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/emergencyContact/add", data);
    };
    MyInfoService.prototype.getAllEmergencyContacts = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/emergencyContact/getAll", data);
    };
    MyInfoService.prototype.getEmergencyContact = function (company_id, user_id, ec_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/emergencyContact/get" + "/" + company_id + "/" + user_id + "/" + ec_id);
    };
    MyInfoService.prototype.editEmergencyContact = function (ec_id, data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/emergencyContact/edit" + "/" + ec_id, data);
    };
    MyInfoService.prototype.deleteEmergencyContact = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/emergencyContact/delete", data);
    };
    MyInfoService.prototype.addNotes = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/notes/add", data);
    };
    MyInfoService.prototype.getAllNotes = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/notes/getAll", data);
    };
    MyInfoService.prototype.getNote = function (company_id, user_id, note_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/notes/get" + "/" + company_id + "/" + user_id + "/" + note_id);
    };
    MyInfoService.prototype.editNote = function (note_id, data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/notes/edit" + "/" + note_id, data);
    };
    MyInfoService.prototype.deleteNote = function (company_id, user_id, note_id) {
        return this.http.delete(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/notes/delete" + "/" + company_id + "/" + user_id + "/" + note_id);
    };
    MyInfoService.prototype.changeStatus = function (note_id, data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/notes/change-status" + "/" + note_id, data);
    };
    MyInfoService.prototype.addAssets = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/assets/add", data);
    };
    MyInfoService.prototype.getAllAssets = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/assets/getAll", data);
    };
    MyInfoService.prototype.getAsset = function (company_id, user_id, asset_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/assets/get" + "/" + company_id + "/" + user_id + "/" + asset_id);
    };
    MyInfoService.prototype.editAsset = function (asset_id, data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/assets/edit" + "/" + asset_id, data);
    };
    MyInfoService.prototype.deleteAsset = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/assets/delete", data);
    };
    MyInfoService.prototype.listAssignedTrainings = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/assigned-trainings/listTrainings", data);
    };
    MyInfoService.prototype.markAsComplete = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/assigned-trainings/mark-as-complete", data);
    };
    MyInfoService.prototype.getAssignedTraining = function (company_id, user_id, id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/assigned-trainings/get" + "/" + company_id + "/" + user_id + "/" + id);
    };
    MyInfoService.prototype.editAssignedTraining = function (data) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/assigned-trainings/edit", data);
    };
    MyInfoService.prototype.deleteAssignedTrainings = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/assigned-trainings/deleteMany", data);
    };
    MyInfoService.prototype.getOnboardingDetails = function (company_id, template_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/onboarding/get-onboarding-details" + "/" + company_id + "/" + template_id);
    };
    MyInfoService.prototype.getOffboardingDetails = function (company_id, template_id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/offboarding/get-offboarding-details" + "/" + company_id + "/" + template_id);
    };
    MyInfoService.prototype.uploadDocument = function (formData) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/documents/upload", formData);
    };
    MyInfoService.prototype.listDocuments = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/documents/list", data);
    };
    MyInfoService.prototype.getDocumentDetails = function (company_id, user_id, id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/documents/get" + "/" + company_id + "/" + user_id + "/" + id);
    };
    MyInfoService.prototype.editDocuments = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/documents/edit", data);
    };
    MyInfoService.prototype.deleteDocuments = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/documents/delete", data);
    };
    MyInfoService.prototype.deleteEducation = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/personal/deleteEducation", data);
    };
    MyInfoService.prototype.deleteLanguages = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/personal/deleteLanguages", data);
    };
    MyInfoService.prototype.deleteVisaInformation = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/personal/deleteVisaInformation", data);
    };
    MyInfoService.prototype.updateEmployeInfoHistory = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/job/updateEmploymentStatus", data);
    };
    MyInfoService.prototype.updateJobInfoHistory = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/job/updateJobInformation", data);
    };
    MyInfoService.prototype.getEmpJobInfo = function (cId, uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/users/getOtherUser/" + cId + "/" + uId);
    };
    MyInfoService.prototype.getPersonalSSNData = function (id) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/companies/settings/" + id);
    };
    MyInfoService.prototype.sendingCustomService = function (data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/custom/addOrUpdate", data);
    };
    MyInfoService.prototype.getcustomfieldsData = function (cId, uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/custom/" + cId + '/' + uId);
    };
    MyInfoService.prototype.adminOffBoardingOverRide = function (uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/offboarding/adminOverride/" + uId);
    };
    MyInfoService.prototype.adminOonBoardingOverRide = function (uId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/myinfo/onboarding/adminOverride/" + uId);
    };
    MyInfoService.prototype.getPageACLS = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url + "/api" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version + "/roles/userAccessControls");
    };
    MyInfoService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], MyInfoService);
    return MyInfoService;
}());



/***/ })

}]);
//# sourceMappingURL=assets-assets-module~black-out-dates-black-out-dates-module~compensation-compensation-module~custom-~756f7375.js.map