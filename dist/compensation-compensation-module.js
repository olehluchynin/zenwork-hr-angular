(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["compensation-compensation-module"],{

/***/ "./src/app/admin-dashboard/employee-management/compensation/compensation.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/compensation/compensation.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: CompensationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompensationModule", function() { return CompensationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _compensation_compensation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./compensation/compensation.component */ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.ts");
/* harmony import */ var _compensation_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./compensation.routing */ "./src/app/admin-dashboard/employee-management/compensation/compensation.routing.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/pay-roll.service */ "./src/app/services/pay-roll.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var CompensationModule = /** @class */ (function () {
    function CompensationModule() {
    }
    CompensationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _compensation_routing__WEBPACK_IMPORTED_MODULE_3__["CompensationRouting"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModuleModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5__["BsDatepickerModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"]
            ],
            declarations: [_compensation_compensation_component__WEBPACK_IMPORTED_MODULE_2__["CompensationComponent"]],
            providers: [_services_my_info_service__WEBPACK_IMPORTED_MODULE_6__["MyInfoService"], _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_8__["PayRollService"]]
        })
    ], CompensationModule);
    return CompensationModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/compensation/compensation.routing.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/compensation/compensation.routing.ts ***!
  \******************************************************************************************/
/*! exports provided: CompensationRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompensationRouting", function() { return CompensationRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _compensation_compensation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./compensation/compensation.component */ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/compensation",pathMatch:"full" },
    { path: '', component: _compensation_compensation_component__WEBPACK_IMPORTED_MODULE_2__["CompensationComponent"] },
];
var CompensationRouting = /** @class */ (function () {
    function CompensationRouting() {
    }
    CompensationRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CompensationRouting);
    return CompensationRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.css":
/*!**********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.css ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal { margin: 30px auto 0; float: none;}\n.personal .panel-default>.panel-heading { padding: 0 0 25px; background-color:transparent !important; border: none;}\n.personal .panel-title a { color: #3c3c3c; display: inline-block; font-size: 19px;}\n.personal .panel-title a:hover { text-decoration: none;color: #3c3c3c;}\n.personal .panel-title>.small, .personal .panel-title>.small>a, .personal .panel-title>a, .personal .panel-title>small, .personal .panel-title>small>a { text-decoration:none;}\n.personal .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.personal .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.personal .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.personal .panel-group .panel-heading+.panel-collapse>.list-group, .personal .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.personal .panel-heading .accordion-toggle:after { margin:4px 15px 0; font-size: 13px; line-height: 13px;}\n.personal .panel-body { padding: 0 0 5px;}\n.personal .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.personal .panel-default { border-color:transparent;}\n.personal .panel-group .panel+.panel { margin-top: 20px;}\n.list-items-addresslocal{\n    display: inline-block;\n    width: 25%;\n}\n.save {border-radius: 20px;font-size: 15px; padding: 6px 24px;margin:30px 0 0;background: #008f3d; color:#fff;}\n.cancel { color:#e44a49; background:transparent; padding: 30px 0 0;}\n.zenwork-checked-border:after { \n    content: '';\n    position: absolute;\n    top: 0;\n    left: -50px;\n    border-right: #83c7a1 4px solid;\n    height: 100%;\n    border-radius: 4px;\n    padding: 25px;\n    }\n.next-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 20px;\n    color: #fff;\n    padding: 0 30px;    \n}\n.date { height: auto; line-height:inherit; width:100%;background:#f8f8f8;}\n.date a { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date a:focus { border: none; outline: none;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:75%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.error{ color: #e44a49;}\n.padding-left-15{\n    padding: 0 0 0 15px;\n}\n.historical-tablerow{\n    background: #fff;\n}\n.historical-tablerow td{\n    padding: 20px 0px;\n    text-align: center;\n    border-bottom: 1px solid #edf7fe;\n}\n.list-items-employement{\n    display: inline-block;\n    width: 24%;\n    padding: 0px 7px;\n    vertical-align: top;\n}\n.employement-details .list-job-employement {\n    display: inline-block;\n    width: 50%;\n    padding: 0px 7px;\n}\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: auto; \n    box-shadow: none;\n    background: #f8f8f8;\n}\n.text-area1 { width: 80%; resize: none;}\n.employement-details{\n    margin: 20px 0 0 0;\n}\ninput{\n    height:30px;\n    border:0;\n    width: 178px;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 15px;\n    padding: 0 0 8px;\n}\n.education-date-list {\n    width: 25%;\n    position: relative;\n    display: inline-block;\n}\n.grade-band span{\n    height: 40px;\n    background: #f8f8f8;\n    display: block;\n    padding: 15px;\n}\n.education-date-list span {\n    height: 49px;\n    background: #fff;\n    display: block;\n    padding: 15px;\n}\n.education-date .padding10-listitems{\n    margin: 0px 5px 0 0;\n}\n.historical-table thead td{\n    padding: 10px 30px;\n    background: #edf7fe;\n    text-align: center;\n    font-size: 15px;\n}\n.select-btn {\n    border: none;\n    box-shadow: none;\n    height: auto;\n    width: 95%;\n    padding: 15px;\n}\n.employement-status-modal .modal-dialog { width: 60%;}\n.employement-status-modal .modal-content { border-radius: 5px !important;}\n.employement-status-modal .modal-header {padding: 15px 15px 20px;}\n.employement-status-modal .modal-header h4 { padding: 0 0 0 30px;}\n.model-bg {\n    background: #fff;\n    padding: 30px;\n}\n.zenwork-margin-ten-zero {\n    margin: 10px 0px!important;\n    border-top: 2px solid #eee;\n}\n.employment-date-list {   \n    position: relative;\n    display: inline-block;\n    padding: 0px 23px; \n    width: 23%;   \n}\n.job-date-list {   \n    position: relative;\n    display: inline-block; \n    width: 23%;  \n    margin-left: 13px; \n}\n.start-date{\n    font-size: 14px;\n    padding-left: 15px;\n    box-shadow: none;\n    background: #f8f8f8;\n}\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"personal col-md-11\">\n\n  <div class=\"panel-group\" id=\"accordion3\">\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseOne\">\n            Compensation Details\n          </a>\n        </h4>\n      </div>\n      <form  [formGroup]=\"compensationDetailsForm\">\n      <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n        <div class=\"panel-body\">\n          <div class=\"compensation-details padding-left-15\">  \n            <div class=\"row mar-btm-15\">\n              <div class=\"col-xs-2 list-items-addresslocal city\" *ngIf=\"fieldsShow.Nonpaid_Record.hide !== true\">\n                <label>Nonpaid Record? <span *ngIf=\"fieldsShow.Nonpaid_Record.required\">*</span></label><br>\n                <mat-select class=\"form-control select-btn\" formControlName=\"NonpaidPosition\" [required]=\"fieldsShow.Nonpaid_Record.required\" (ngModelChange)=\"nonPaidRecordsFields($event)\" [disabled]=\"compensationTabView\">\n                  <mat-option [value]=\"true\">Yes</mat-option>\n                  <mat-option [value]=\"false\">No</mat-option>\n                </mat-select>\n                <span *ngIf=\"compensationDetailsForm.get('NonpaidPosition').invalid && compensationDetailsForm.get('NonpaidPosition').touched\" class=\"error\">Enter nonpaid record</span>\n              </div>\n              <div class=\"col-xs-4 list-items-addresslocal city\" *ngIf=\"(fieldsShow.Highly_compensated_Employee_type.hide !== true) && showFilelds\">\n                <label>Highly Compensated Employee? <span *ngIf=\"fieldsShow.Highly_compensated_Employee_type.required\">*</span></label><br>\n                <mat-select class=\"form-control select-btn\" formControlName=\"HighlyCompensatedEmployee\" [required]=\"fieldsShow.Highly_compensated_Employee_type.required\"\n                placeholder=\"Highly compensated\" [disabled]=\"compensationTabView\">\n                  <mat-option [value]=\"true\">Yes</mat-option>\n                  <mat-option [value]=\"false\">No</mat-option>\n                </mat-select>\n                <span *ngIf=\"compensationDetailsForm.get('HighlyCompensatedEmployee').invalid && compensationDetailsForm.get('HighlyCompensatedEmployee').touched\" class=\"error\">Enter compensated employee</span>\n\n              </div>\n            </div>\n            <!-- <div class=\"row mar-btm-15\" *ngIf=\"showFilelds\">\n              <div class=\"education-date\">\n                <div class=\"col-xs-4 education-date-list\" *ngIf=\"fieldsShow.Pay_group.hide !== true\">\n                  <label>Pay Group </label><br>\n                  <mat-select class=\"form-control select-btn\" formControlName=\"Pay_group\" [(ngModel)]=\"Pay_group\" [required]=\"fieldsShow.Pay_group.required\"\n                  placeholder=\"Pay group\" (ngModelChange)=\"payGroupChange()\" disabled>\n                    <mat-option *ngFor=\"let group of payGroupList\" [value]=\"group._id\">{{group.payroll_group_name}}</mat-option>\n                  </mat-select>\n                <span *ngIf=\"compensationDetailsForm.get('Pay_group').invalid && compensationDetailsForm.get('Pay_group').touched\" class=\"error\">You must select a value</span>\n\n                </div>\n                <div class=\"col-xs-4 education-date-list\" *ngIf=\"fieldsShow.Pay_frequency.hide !== true\">\n                  <label>Pay Frequency </label><br>\n                  <mat-select class=\"form-control select-btn\" formControlName=\"Pay_frequency\" [required]=\"fieldsShow.Pay_frequency.required\"\n                  placeholder=\"Pay Frequency\" disabled>\n                  <mat-option value=\"Weekly\">Weekly</mat-option>\n                  <mat-option value=\"Bi-Weekly\">Bi-Weekly</mat-option>\n                  <mat-option value=\"Semi-Monthly\">Semi-Monthly</mat-option>\n                  <mat-option value=\"Monthly\">Monthly</mat-option>\n\n                   \n                  </mat-select>\n                <span *ngIf=\"compensationDetailsForm.get('Pay_frequency').invalid && compensationDetailsForm.get('Pay_frequency').touched\" class=\"error\">You must select a value</span>\n\n                </div>\n              </div>\n            </div> -->\n            <div class=\"row mar-btm-15\" *ngIf=\"showFilelds\">\n              <div class=\"education-date\">\n                <div class=\"col-xs-4 education-date-list\"  *ngIf=\"fieldsShow.Salary_Grade.hide !== true\">\n                  <label>Salary Grade</label><br>\n                  <mat-select class=\"form-control select-btn\" formControlName=\"Salary_Grade\" [required]=\"fieldsShow.Salary_Grade.required\"\n                  placeholder=\"Salary Grade\" [disabled]=true>\n                    <mat-option *ngFor=\"let grade of salaryGradeList\" [value]=\"grade._id\">{{grade.salary_grade_name}}</mat-option>\n                  </mat-select>\n                \n                <span *ngIf=\"compensationDetailsForm.get('Salary_Grade').invalid && compensationDetailsForm.get('Salary_Grade').touched\" class=\"error\">Enter salary grade</span>\n\n                </div>\n                <div class=\"col-xs-4 education-date-list\">\n                    <label>Salary Grade Band</label>\n                    <span>{{salaryGradeValue}}</span>\n                    \n                  <!-- <input type=\"text\"  class=\"form-control ratio-field\" placeholder=\"Grade Band\" formControlName=\"grade_band\" disabled> -->\n\n                 \n                </div>\n                <!-- <div class=\"col-xs-4 education-date-list\" *ngIf=\"fieldsShow.Compensation_ratio.hide !== true\">\n                  <label>Compensation Ratio </label><br>\n                  <input type=\"text\" (keypress)=\"keyPress($event)\" class=\"form-control ratio-field\" placeholder=\"\" formControlName=\"compensationRatio\" [required]=\"fieldsShow.Compensation_ratio.required\"\n                  placeholder=\"Ratio\" disabled>\n\n                <span *ngIf=\"compensationDetailsForm.get('compensationRatio').invalid && compensationDetailsForm.get('compensationRatio').touched\" class=\"error\">You must select a value</span>\n\n                </div> -->\n              </div>\n            </div>\n            <!-- <div class=\"row mar-btm-15\">\n              <div class=\"education-date\">\n                  <div class=\"col-xs-4 education-date-list\">\n                      <label>Pay Type</label>\n                      <mat-select class=\"form-control select-btn height-30\" formControlName=\"payType\" placeholder=\"Pay type\" disabled>\n                        <mat-option value=\"Salary\">Salary</mat-option>\n                        <mat-option value=\"Hourly\">Hourly</mat-option>\n                        <mat-option value=\"Bonus\">Bonus</mat-option>\n                      </mat-select>\n                  </div>\n                  <div class=\"col-xs-4 education-date-list\">\n                      <label>Pay Rate</label>\n                    <input type=\"text\" class=\"form-control ratio-field\" placeholder=\"Pay rate\" formControlName=\"payRate\" disabled>\n                    </div>\n                </div>\n            </div> -->\n            \n          <!-- <button class=\"btn pull-left cancel\" (click)=\"cancelDetails()\">Cancel</button> -->\n          <button *ngIf=\"!compensationTabView\" class=\"btn pull-right save\" type=\"submit\" (click)=\"saveCompensationDetails()\">Save Changes</button>\n          </div>\n        </div>\n      </div>\n      </form>\n    </div>\n\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        <h4 class=\"panel-title\">\n          <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#collapseTwo\">\n            Compensation\n          </a>\n        </h4>\n      </div>\n      <div id=\"collapseTwo\" class=\"panel-collapse collapse\">\n        <div class=\"panel-body\">\n          <div class=\"employee-status  padding-left-15\">\n\n            <table class=\"historical-table\" width=\"100%\">\n              <thead>\n                <td></td>\n                <td>Effective Date</td>\n                <td>Pay Rate</td>\n                <td>Pay Type</td>\n                <td>Pay Frequency</td>\n                <td>Change Reason</td>\n                <td>Notes</td>\n                <td>\n                  <span *ngIf=\"showFilelds\">\n                    <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n                    <mat-menu #menu=\"matMenu\">\n                      <button *ngIf=\"!checkCompensationIds.length >=1\" mat-menu-item data-toggle=\"modal\" data-target=\"#myModal\" (click)=\"openDialog()\">Add</button>\n                      <button *ngIf=\"(!checkCompensationIds.length <=0) && (checkCompensationIds.length ==1)\" mat-menu-item>\n                          <a (click)=\"edit()\" >Edit</a>\n                          <a data-toggle=\"modal\" data-target=\"#myModal\" #openEdit></a>\n                      </button>\n                    </mat-menu>\n                  </span>\n                </td>\n              </thead>\n              <tbody>\n                <tr class=\"historical-tablerow\" *ngFor=\"let compensation of currentCompensation\">\n                  <td [ngClass]=\"{'zenwork-checked-border': compensation.isChecked}\">\n                    <div class=\"cont-check\">\n                      <mat-checkbox class=\"zenwork-customized-checkbox\" (change)=\"checkCurrentCompensation($event, compensation._id)\" [(ngModel)]=\"compensation.isChecked\">\n\n                      </mat-checkbox>\n                    </div>\n\n                  </td>\n                  <td>{{compensation.effective_date | date : 'MM/dd/yyyy'}}</td>\n                  <td>$ {{compensation.payRate}}</td>\n                  <td>{{compensation.payType}}</td>\n                  <td *ngIf=\"compensation.Pay_group\">{{compensation.Pay_group.pay_frequency}}</td>\n                  <td *ngIf=\"compensation.Pay_frequency\">{{compensation.Pay_frequency}}</td>\n                  <td>{{compensation.changeReason}}</td>\n                  <td>{{compensation.notes}}</td>\n                  <td></td>\n                </tr>\n         \n\n              </tbody>\n            </table>\n            \n          </div>\n        </div>\n      </div>\n    </div>\n\n\n\n   \n\n  </div>\n</div>\n\n<!-- Add/Edit Compensation -->\n\n<div class=\"employement-status-modal\">\n  <div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"model-bg\">\n          <h4 class=\"mar-btm-30\">Add / Edit - Compensation</h4>\n\n          <hr class=\"zenwork-margin-ten-zero\">\n          <form [formGroup]=\"compensationForm\" >\n          \n            \n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                \n                  <li class=\"list-items-employement\">\n                    <label>Pay Type*</label>\n                    <mat-select class=\"form-control text-field height-30\" formControlName=\"payType\" [(ngModel)]=\"payType\" placeholder=\"Pay type\" (ngModelChange)=\"payTypeSelect($event)\">\n                      <mat-option value=\"Salary\">Salary</mat-option>\n                      <mat-option value=\"Hourly\">Hourly</mat-option>\n                      <mat-option value=\"Bonus\">Bonus</mat-option>\n                    </mat-select>\n                  <span *ngIf=\"compensationForm.get('payType').invalid && compensationForm.get('payType').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Enter pay type</span>\n\n                  </li>\n          \n                  <li class=\"list-items-employement\" *ngIf=\"payTypeFields\">\n                      <label>Pay Group* </label><br>\n                      <mat-select class=\"form-control text-field\" formControlName=\"Pay_group\" [(ngModel)]=\"Pay_group\" [required]=\"fieldsShow.Pay_group.required\"\n                      placeholder=\"Pay group\" (ngModelChange)=\"payGroupChange()\">\n                        <mat-option *ngFor=\"let group of payGroupList\" [value]=\"group._id\">{{group.payroll_group_name}}</mat-option>\n                      </mat-select>\n                    <span *ngIf=\"compensationForm.get('Pay_group').invalid && compensationForm.get('Pay_group').touched\" class=\"error\">Enter pay group</span>\n                  </li>\n                  <li class=\"list-items-employement\" *ngIf=\"payTypeFields\">\n                    <label>Pay Frequency</label><br>\n                    <mat-select class=\"form-control text-field height-30\" formControlName=\"Pay_frequency\" placeholder=\"Pay Frequency\" disabled>\n                      <mat-option value=\"Weekly\">Weekly</mat-option>\n                      <mat-option value=\"Bi-Weekly\">Bi-Weekly</mat-option>\n                      <mat-option value=\"Semi-Monthly\">Semi-Monthly</mat-option>\n                      <mat-option value=\"Monthly\">Monthly</mat-option>\n               \n                    </mat-select>\n                  <!-- <span *ngIf=\"compensationForm.get('Pay_frequency').invalid && compensationForm.get('Pay_frequency').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">You must select a field</span> -->\n\n                  </li>\n                  <li class=\"list-items-employement\" *ngIf=\"payTypeBonus == 'Bonus'\">\n                      <label>Pay Frequency*</label><br>\n                      <mat-select class=\"form-control text-field height-30\" formControlName=\"Pay_frequency\" placeholder=\"Pay Frequency\">\n                        <mat-option value=\"Weekly\">Weekly</mat-option>\n                        <mat-option value=\"Bi-Weekly\">Bi-Weekly</mat-option>\n                        <mat-option value=\"Semi-Monthly\">Semi-Monthly</mat-option>\n                        <mat-option value=\"Monthly\">Monthly</mat-option>\n                        <mat-option value=\"Annually\">Annually</mat-option>\n  \n  \n                      </mat-select>\n                    <span *ngIf=\"compensationForm.get('Pay_frequency').invalid && compensationForm.get('Pay_frequency').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Enter pay frequency</span>\n  \n                    </li>\n\n                    <li class=\"list-items-employement\" *ngIf=\"payTypeBonus == 'Bonus'\">\n                      <label>Pay Rate*</label>\n                      <input type=\"number\" class=\"form-control text-field\" placeholder=\"Pay Rate\" formControlName=\"payRate\"\n                      (keypress)=\"keyPress($event)\" maxlength=\"4\">\n                    <span *ngIf=\"compensationForm.get('payRate').invalid && compensationForm.get('payRate').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Enter pay rate</span>\n                   \n                    </li>\n\n                </ul>\n              </div>\n           \n           \n              <div class=\"employement-details padding-left-15\" *ngIf=\"payTypeFields\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                      <label>Salary Grade</label><br>\n                      <mat-select class=\"form-control text-field\" formControlName=\"Salary_Grade\" [required]=\"fieldsShow.Salary_Grade.required\"\n                      placeholder=\"Salary Grade\" disabled>\n                        <mat-option *ngFor=\"let grade of salaryGradeList\" [value]=\"grade._id\">{{grade.salary_grade_name}}</mat-option>\n                      </mat-select>\n                    \n                    <span *ngIf=\"compensationForm.get('Salary_Grade').invalid && compensationForm.get('Salary_Grade').touched\" class=\"error\">You must enter a value</span>\n\n                  </li>\n                  <li class=\"list-items-employement grade-band\">\n                      <label>Salary Grade Band</label>\n                      \n                      <span>{{salaryGradeValue}}</span>\n                   \n                  </li>\n                  <li class=\"list-items-employement\" *ngIf=\"payTypeFields\">\n                      <label>Pay Rate*</label>\n                      <input type=\"text\" class=\"form-control text-field\" placeholder=\"Pay Rate\" formControlName=\"payRate\"\n                      (keypress)=\"keyPress($event)\" (ngModelChange)=\"payRateChangeforCompensation()\">\n                    <span *ngIf=\"compensationForm.get('payRate').invalid && compensationForm.get('payRate').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Enter pay rate</span>\n                    <span *ngIf=\"compensationError \" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">Compensation does not fall\n                        under the specified grade band </span>\n                    </li>\n\n                  <li class=\"list-items-employement\">\n                    <label>Compensation Ratio</label><br>\n                    <input type=\"text\" (keypress)=\"keyPress($event)\" class=\"form-control text-field\" placeholder=\"Ratio\" formControlName=\"compensationRatio\"\n                    [(ngModel)]=\"compensationRatio\" disabled>\n                  <!-- <span *ngIf=\"compensationForm.get('compensationRatio').invalid && compensationForm.get('compensationRatio').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">You must enter a field</span> -->\n\n                  </li>\n                </ul>\n              </div>\n           \n            \n              <div class=\"employement-details padding-left-15\">\n                <ul class=\"list-unstyled\">\n                  <li class=\"list-items-employement\">\n                    <label>Change Reason</label><br>\n                    <mat-select class=\"form-control text-field height-30\" formControlName=\"changeReason\" placeholder=\"Change reason\">\n                      \n                      <mat-option *ngFor=\"let reason of allChangeReason\" [value]='reason.name'>{{reason.name}}</mat-option>\n                    </mat-select>\n                  <!-- <span *ngIf=\"compensationForm.get('changeReason').invalid && compensationForm.get('changeReason').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">You must select a field</span> -->\n\n                  </li>\n                  <li class=\"job-date-list padding10-listitems list-items-addresslocal\">\n                    <label>Effective Date</label>\n\n                    <div class=\"date\">\n                        <input matInput [matDatepicker]=\"picker5\" placeholder=\"mm / dd / yy\" class=\"form-control\" formControlName=\"effective_date\">\n                        <mat-datepicker #picker5></mat-datepicker>\n                        <a mat-raised-button (click)=\"picker5.open()\">\n                          <span>\n                            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                          </span>\n                        </a>\n                        <div class=\"clearfix\"></div>\n                      </div>\n\n                    <!-- <input class=\"form-control text-field start-date address-state-dropodown data-picker-align\"\n                      #dpMDYSdate=\"bsDatepicker\" bsDatepicker [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" placeholder=\"mm/dd/yyyy\" name=\"birth-date\" >\n                    <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"emp-ends-calendar-icon\"\n                      alt=\"Company-settings icon\" (click)=\"dpMDYSdate.toggle()\"\n                      [attr.aria-expanded]=\"dpMDYSdate.isOpen\">\n                      <span *ngIf=\"compensationForm.get('effective_date').invalid && compensationForm.get('effective_date').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">You must select a field</span> -->\n\n                  </li>\n                </ul>\n              </div>\n           \n            <div class=\"employement-details padding-left-15 mar-btm-50\">\n            \n                  <label>Notes</label><br>\n                  <textarea class=\"form-control text-field text-area1\" rows=\"5\" formControlName=\"notes\" placeholder=\"Notes\"></textarea>\n                  <!-- <span *ngIf=\"compensationForm.get('notes').invalid && compensationForm.get('notes').touched\" style=\"color:#e44a49; padding: 5px 0 0; display: block;\">You must enter a field</span> -->\n\n              \n            </div>\n\n            <hr class=\"zenwork-margin-ten-zero\">\n            <div class=\"row mar-top-40\">\n\n              <button mat-button data-dismiss=\"modal\" (click)=\"cancelForm()\">Cancel</button>\n\n              <button *ngIf=\"!checkCompensationIds.length >=1\" mat-button class=\"submit-btn pull-right\" (click)=\"addCompensation()\">Submit</button>\n              <button *ngIf=\"(!checkCompensationIds.length <=0) && (checkCompensationIds.length ==1)\" mat-button class=\"submit-btn pull-right\" (click)=\"editCompensation()\">Update</button>\n\n            </div>\n          \n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: CompensationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompensationComponent", function() { return CompensationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/pay-roll.service */ "./src/app/services/pay-roll.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CompensationComponent = /** @class */ (function () {
    function CompensationComponent(myInfoService, swalAlertService, accessLocalStorageService, router, payRollService) {
        this.myInfoService = myInfoService;
        this.swalAlertService = swalAlertService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.router = router;
        this.payRollService = payRollService;
        this.tabName = "Compensation";
        this.fieldsShow = {
            Compensation_ratio: {
                hide: '',
                required: '',
                format: ''
            },
            Highly_compensated_Employee_type: {
                hide: '',
                required: '',
                format: ''
            },
            Nonpaid_Record: {
                hide: '',
                required: '',
                format: ''
            },
            Pay_frequency: {
                hide: '',
                required: '',
                format: ''
            },
            Pay_group: {
                hide: '',
                required: '',
                format: ''
            },
            Salary_Grade: {
                hide: '',
                required: '',
                format: ''
            },
        };
        this.showFilelds = true;
        this.data2 = {};
        this.tepmCF = {
            payRate: Number,
            effective_date: '',
            payType: '',
            Pay_frequency: '',
            notes: '',
            changeReason: '',
        };
        this.currentCompensation = [];
        this.selectedCurrentCompensation = [];
        this.customPayFrequency = [];
        this.defaultPayFrewuency = [];
        this.allPayFrequency = [];
        this.customPayType = [];
        this.defaultPayType = [];
        this.allPayType = [];
        this.customChangeReason = [];
        this.defaultChangeReason = [];
        this.allChangeReason = [];
        this.payGroupList = [];
        this.salaryGradeList = [];
        this.payTypeFields = false;
        this.compensationError = false;
        this.checkCompensationIds = [];
        this.pagesAccess = [];
    }
    CompensationComponent.prototype.ngOnInit = function () {
        this.roletype = JSON.parse(localStorage.getItem('type'));
        if (this.roletype != 'company') {
            this.pageAccesslevels();
        }
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        this.compensationDetails = JSON.parse(localStorage.getItem('employee'));
        if (this.compensationDetails) {
            this.id = this.compensationDetails._id;
        }
        this.roleType = JSON.parse(localStorage.getItem('type'));
        if (this.roleType != 'company') {
            this.id = JSON.parse(localStorage.getItem('employeeId'));
        }
        // console.log(this.compensationDetails.compensation);
        // this.currentCompensation = this.compensationDetails.compensation.current_compensation;
        // console.log(this.currentCompensation);
        this.listSalaryGrades();
        this.payRollGroupList();
        this.getUserCompensation();
        this.getSingleTabSettingForCompensation();
        this.getStandardAndCustomStructureFields();
        this.initCompensationDetailsForm();
        this.compensationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            payRate: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            payType: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            Pay_frequency: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            compensationRatio: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            changeReason: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            effective_date: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            notes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            Pay_group: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            Salary_Grade: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            grade_band1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    };
    CompensationComponent.prototype.pageAccesslevels = function () {
        var _this = this;
        console.log("shfjksdh");
        this.myInfoService.getPageACLS()
            .subscribe(function (res) {
            console.log(res);
            _this.pagesAccess = res.data;
            _this.pagesAccess.forEach(function (element) {
                console.log('data comes in1');
                if (element.page == "My Info (EE's) - Compensation" && element.access == 'view') {
                    console.log('data comes in2');
                    _this.compensationTabView = true;
                    console.log('loggss', _this.compensationTabView);
                }
            });
        }, function (err) {
        });
    };
    CompensationComponent.prototype.initCompensationDetailsForm = function () {
        this.compensationDetailsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            NonpaidPosition: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            HighlyCompensatedEmployee: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            // Pay_group: new FormControl(""),
            // Pay_frequency: new FormControl(""),
            Salary_Grade: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            grade_band: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
        });
    };
    CompensationComponent.prototype.nonPaidRecordsFields = function (event) {
        console.log(event);
        this.nonPaidRecord = event;
        if (event == true) {
            this.showFilelds = false;
        }
        else {
            this.showFilelds = true;
        }
    };
    // Author:Saiprakash, Date:15/05/2019
    // Get Standard And Custom Structure Fields
    CompensationComponent.prototype.getStandardAndCustomStructureFields = function () {
        var _this = this;
        this.myInfoService.getStandardAndCustomStructureFields(this.companyId).subscribe(function (res) {
            console.log(res);
            _this.getAllStructureFields = res.data;
            // this.customPayFrequency = this.getAllStructureFields['Pay Frequency'].custom;
            // this.defaultPayFrewuency = this.getAllStructureFields['Pay Frequency'].default;
            // this.allPayFrequency = this.customPayFrequency.concat(this.defaultPayFrewuency);
            // this.customPayType = this.getAllStructureFields['Pay Type'].custom;
            // this.defaultPayType = this.getAllStructureFields['Pay Type'].default;
            // this.allPayType = this.customPayType.concat(this.defaultPayType); 
            _this.customChangeReason = _this.getAllStructureFields['Job Change Reason'].custom;
            _this.defaultChangeReason = _this.getAllStructureFields['Job Change Reason'].default;
            _this.allChangeReason = _this.customChangeReason.concat(_this.defaultChangeReason);
            console.log(_this.allChangeReason);
        });
    };
    // Author:Saiprakash, Date:12/04/2019
    // Get single tab settings for compensation details
    CompensationComponent.prototype.getSingleTabSettingForCompensation = function () {
        var _this = this;
        this.myInfoService.getSingleTabSettingForMyinfo(this.tabName).subscribe(function (res) {
            console.log(res);
            _this.fieldsShow = res.data.fields;
            console.log(_this.fieldsShow);
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash G, Date:09-05-19
    // save employee compensation details
    CompensationComponent.prototype.saveCompensationDetails = function () {
        var _this = this;
        var data1 = {
            _id: this.id,
            compensation: {
                NonpaidPosition: this.compensationDetailsForm.get('NonpaidPosition').value,
                HighlyCompensatedEmployee: this.compensationDetailsForm.get('HighlyCompensatedEmployee').value,
                Salary_Grade: this.compensationDetailsForm.get('Salary_Grade').value,
                grade_band: this.salaryGradeId,
                current_compensation: this.currentCompensation
            }
        };
        if (this.compensationDetailsForm.valid) {
            this.myInfoService.updateUSer(data1).subscribe(function (res) {
                console.log(res);
                _this.getUserCompensation();
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/notes"]);
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.compensationDetailsForm.get('NonpaidPosition').markAsTouched();
            this.compensationDetailsForm.get('HighlyCompensatedEmployee').markAsTouched();
            // this.compensationDetailsForm.get('Pay_group').markAsTouched();
            // this.compensationDetailsForm.get('Pay_frequency').markAsTouched();
            // this.compensationDetailsForm.get('Salary_Grade').markAsTouched();
            // this.compensationDetailsForm.get('compensationRatio').markAsTouched();
        }
    };
    // Author:Saiprakash, Date:14/05/2019
    // Get user compensation
    CompensationComponent.prototype.getUserCompensation = function () {
        var _this = this;
        this.myInfoService.getOtherUser(this.companyId, this.id).subscribe(function (res) {
            console.log(res);
            _this.compensationData = res.data;
            // console.log(this.compensationData.compensation.Pay_group);
            // if(this.compensationData.compensation.Pay_group){
            //   this.payRollService.getSinglePayrollGroup(this.companyId ,this.compensationData.compensation.Pay_group).subscribe((res:any)=>{
            //     console.log(res);
            //     this.compensationForm.get('Pay_frequency').patchValue(res.data.pay_frequency)
            //   },(err)=>{
            //     console.log(err);
            //   })
            // }
            _this.compensationDetailsForm.get('Salary_Grade').patchValue(_this.compensationData.compensation.Salary_Grade);
            _this.compensationForm.get('Salary_Grade').patchValue(_this.compensationData.compensation.Salary_Grade);
            if (_this.compensationData.compensation.grade_band) {
                _this.compensationDetailsForm.get('grade_band').patchValue(_this.compensationData.compensation.grade_band.grade);
                _this.salaryGradeValue = _this.compensationData.compensation.grade_band.grade;
                _this.salaryGradeId = _this.compensationData.compensation.grade_band._id;
            }
            if (_this.compensationData.job.workHours) {
                _this.workHours = _this.compensationData.job.workHours;
            }
            _this.compensationDetailsForm.patchValue(_this.compensationData.compensation);
            if (_this.compensationData.compensation.NonpaidPosition == false) {
                _this.showFilelds = true;
            }
            else if (_this.compensationData.compensation.NonpaidPosition == true) {
                _this.showFilelds = false;
            }
            if (res.data.compensation.current_compensation && res.data.compensation.current_compensation.length) {
                _this.currentCompensation = res.data.compensation.current_compensation;
            }
        }, function (err) {
            console.log(err);
        });
    };
    // Author:Saiprakash, Date:14/05/2019
    // Add and edit user compensation
    CompensationComponent.prototype.addCompensation = function () {
        var _this = this;
        if (this.payTypeBonus == "Bonus") {
            var bonusCompensation = {
                payRate: this.compensationForm.get('payRate').value,
                effective_date: this.compensationForm.get('effective_date').value,
                payType: this.compensationForm.get('payType').value,
                Pay_frequency: this.compensationForm.get('Pay_frequency').value,
                notes: this.compensationForm.get('notes').value,
                changeReason: this.compensationForm.get('changeReason').value,
            };
            this.currentCompensation.push(bonusCompensation);
            console.log(this.currentCompensation);
            var bonusData = {
                _id: this.id,
                compensation: {
                    NonpaidPosition: this.compensationDetailsForm.get('NonpaidPosition').value,
                    HighlyCompensatedEmployee: this.compensationDetailsForm.get('HighlyCompensatedEmployee').value,
                    Salary_Grade: this.compensationDetailsForm.get('Salary_Grade').value,
                    grade_band: this.compensationData.compensation.grade_band,
                    current_compensation: this.currentCompensation
                }
            };
            if (this.compensationForm.get('payRate').value && this.compensationForm.get('Pay_frequency').value && this.compensationForm.get('payType').value) {
                this.myInfoService.updateUSer(bonusData).subscribe(function (res) {
                    console.log(res);
                    _this.compensationForm.reset();
                    $("#myModal").modal("hide");
                    _this.getUserCompensation();
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.compensationForm.get('Pay_frequency').markAsTouched();
                this.compensationForm.get('payRate').markAsTouched();
                this.compensationForm.get('payType').markAsTouched();
            }
        }
        else {
            this.tepmCF =
                {
                    payRate: this.compensationForm.get('payRate').value,
                    effective_date: this.compensationForm.get('effective_date').value,
                    payType: this.compensationForm.get('payType').value,
                    Pay_group: this.compensationForm.get('Pay_group').value,
                    // Pay_frequency: this.compensationForm.get('Pay_frequency').value,
                    notes: this.compensationForm.get('notes').value,
                    changeReason: this.compensationForm.get('changeReason').value,
                    compensationRatio: this.compensationForm.get('compensationRatio').value
                };
            console.log(this.tepmCF, this.currentCompensation);
            this.currentCompensation.push(this.tepmCF);
            console.log(this.currentCompensation);
            this.data2 = {
                _id: this.id,
                compensation: {
                    NonpaidPosition: this.compensationDetailsForm.get('NonpaidPosition').value,
                    HighlyCompensatedEmployee: this.compensationDetailsForm.get('HighlyCompensatedEmployee').value,
                    Salary_Grade: this.compensationDetailsForm.get('Salary_Grade').value,
                    grade_band: this.compensationData.compensation.grade_band,
                    current_compensation: this.currentCompensation
                }
            };
            if (this.compensationForm.get('payRate').value && this.compensationForm.get('Pay_group').value && this.compensationForm.get('payType').value) {
                this.myInfoService.updateUSer(this.data2).subscribe(function (res) {
                    console.log(res);
                    _this.compensationForm.reset();
                    $("#myModal").modal("hide");
                    _this.getUserCompensation();
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.compensationForm.get('Pay_group').markAsTouched();
                this.compensationForm.get('payRate').markAsTouched();
                this.compensationForm.get('payType').markAsTouched();
            }
        }
    };
    CompensationComponent.prototype.editCompensation = function () {
        var _this = this;
        var data3 = {
            companyId: this.companyId,
            userId: this.id,
            cur_comp_id: this.currentCompensationId,
            payRate: this.compensationForm.get('payRate').value,
            effective_date: this.compensationForm.get('effective_date').value,
            payType: this.compensationForm.get('payType').value,
            notes: this.compensationForm.get('notes').value,
            changeReason: this.compensationForm.get('changeReason').value,
            compensationRatio: this.compensationForm.get('compensationRatio').value
        };
        if (this.payTypeBonus == "Bonus") {
            data3.Pay_frequency = this.compensationForm.get('Pay_frequency').value;
            if (data3.payRate) {
                this.myInfoService.updateCurrentCompensation(data3).subscribe(function (res) {
                    console.log(res);
                    _this.compensationForm.reset();
                    $("#myModal").modal("hide");
                    _this.currentCompensationId = "";
                    _this.checkCompensationIds = [];
                    _this.getUserCompensation();
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.compensationForm.get('payRate').markAsTouched();
            }
        }
        else {
            data3.Pay_group = this.compensationForm.get('Pay_group').value;
            if (data3.payRate) {
                this.myInfoService.updateCurrentCompensation(data3).subscribe(function (res) {
                    console.log(res);
                    _this.compensationForm.reset();
                    $("#myModal").modal("hide");
                    _this.currentCompensationId = "";
                    _this.checkCompensationIds = [];
                    _this.getUserCompensation();
                    _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                }, function (err) {
                    console.log(err);
                    _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
                });
            }
            else {
                this.compensationForm.get('payRate').markAsTouched();
            }
        }
    };
    CompensationComponent.prototype.checkCurrentCompensation = function (event, id) {
        this.currentCompensationId = id;
        console.log(event, id);
        if (event.checked == true) {
            this.checkCompensationIds.push(id);
        }
        console.log(this.checkCompensationIds);
        if (event.checked == false) {
            var index = this.checkCompensationIds.indexOf(id);
            if (index > -1) {
                this.checkCompensationIds.splice(index, 1);
            }
            console.log(this.checkCompensationIds);
        }
    };
    // Author:Saiprakash, Date:14/05/2019
    // get single comensation details
    CompensationComponent.prototype.edit = function () {
        this.selectedCurrentCompensation = this.currentCompensation.filter(function (compensationCheck) {
            return compensationCheck.isChecked;
        });
        if (this.selectedCurrentCompensation.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one compensation to proceed", "", 'error');
        }
        else if (this.selectedCurrentCompensation.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one compensation to proceed", "", 'error');
        }
        else {
            this.openEdit.nativeElement.click();
            this.compensationForm.patchValue(this.selectedCurrentCompensation[0]);
            console.log(this.selectedCurrentCompensation[0]);
            this.Pay_group = this.selectedCurrentCompensation[0].Pay_group._id;
            this.compensationForm.get('Pay_frequency').patchValue(this.selectedCurrentCompensation[0].Pay_group.pay_frequency);
            this.compensationForm.get('payRate').patchValue(this.selectedCurrentCompensation[0].payRate);
            this.compensationForm.get('effective_date').patchValue(new Date(this.selectedCurrentCompensation[0].effective_date));
            this.compensationForm.get('compensationRatio').patchValue(this.selectedCurrentCompensation[0].compensationRatio);
            this.payRateChangeforCompensation();
        }
    };
    CompensationComponent.prototype.listSalaryGrades = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
        };
        this.payRollService.getSalrayGradeList(data).subscribe(function (res) {
            console.log(res);
            _this.salaryGradeList = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    CompensationComponent.prototype.payRollGroupList = function () {
        var _this = this;
        var data = {
            companyId: this.companyId,
        };
        this.payRollService.getPayrollGroup(data).subscribe(function (res) {
            console.log(res);
            _this.payGroupList = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    CompensationComponent.prototype.payGroupChange = function () {
        var _this = this;
        console.log(this.Pay_group);
        if (this.Pay_group) {
            this.payRollService.getSinglePayrollGroup(this.companyId, this.Pay_group)
                .subscribe(function (res) {
                console.log(res);
                _this.compensationForm.get('Pay_frequency').patchValue(res.data.pay_frequency);
            }, function (err) {
                console.log(err);
            });
        }
    };
    CompensationComponent.prototype.payTypeSelect = function (type) {
        console.log(type);
        this.payTypeBonus = type;
        if (type == "Salary" || type == "Hourly") {
            this.payTypeFields = true;
        }
        else if (type == "Bonus") {
            this.payTypeFields = false;
        }
    };
    CompensationComponent.prototype.payRateChangeforCompensation = function () {
        var _this = this;
        this.compensationRatio = '';
        this.compensationForm.get('compensationRatio').patchValue('');
        var postData = {
            payRate: this.compensationForm.get('payRate').value,
            payType: this.compensationForm.get('payType').value,
            pay_frequency: this.compensationForm.get('Pay_frequency').value,
            grade_band: "",
            workHours: this.workHours
        };
        if (this.salaryGradeId) {
            postData.grade_band = this.salaryGradeId;
        }
        if (postData.payRate && postData.payType && postData.pay_frequency) {
            this.payRollService.compensationRationCalculate(postData)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.compensationError = false;
                    _this.calculateCompensation = res.data;
                    _this.compensationForm.get('compensationRatio').patchValue(_this.calculateCompensation.compensationRatio);
                    _this.compensationRatio = _this.calculateCompensation.compensationRatio;
                }
            }, function (err) {
                console.log(err);
                _this.compensationError = true;
                // this.swalAlertService.SweetAlertWithoutConfirmation('Compensation Ratio',err.error.message,'error');
            });
        }
    };
    CompensationComponent.prototype.nextTab = function () {
        this.router.navigate(["/admin/admin-dashboard/employee-management/profile/profile-view/notes"]);
    };
    CompensationComponent.prototype.keyPress = function (event) {
        var pattern = /^[0-9#$%^&*()@!]*$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    CompensationComponent.prototype.cancelForm = function () {
        this.compensationForm.reset();
        this.getUserCompensation();
        this.checkCompensationIds = [];
    };
    CompensationComponent.prototype.openDialog = function () {
        this.compensationForm.reset();
        this.getUserCompensation();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('openEdit'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CompensationComponent.prototype, "openEdit", void 0);
    CompensationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-compensation',
            template: __webpack_require__(/*! ./compensation.component.html */ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.html"),
            styles: [__webpack_require__(/*! ./compensation.component.css */ "./src/app/admin-dashboard/employee-management/compensation/compensation/compensation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_my_info_service__WEBPACK_IMPORTED_MODULE_1__["MyInfoService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_pay_roll_service__WEBPACK_IMPORTED_MODULE_6__["PayRollService"]])
    ], CompensationComponent);
    return CompensationComponent;
}());



/***/ })

}]);
//# sourceMappingURL=compensation-compensation-module.js.map