(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["site-access-site-access-module"],{

/***/ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.css":
/*!**************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-template{\n    /* padding: 0 !important; */\n    padding: 40px;\n  }\n  .main-template .modal-header{\n    padding: 20px 0;\n    border-bottom: 1px solid #ccc;\n  \n  }\n  .type-lebel{\n      display: inline-block;\n  }\n  .emp-name{\n      display: inline-block;\n      padding: 0 0 0 40px;\n      width: 49%;\n  }\n  .main-empdata{\n      padding: 40px;\n  }\n  .company-legal-name{\n    border: none;\n    padding: 10px;\n    background: #f8f8f8;\n    box-shadow: none;\n    height: 40px;\n  }\n  .text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\n  .select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n    /* width: 95%; */\n}\n  .buttons{\n    margin: 35px 30px 0;\n  }\n  .buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n  }\n  .buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    cursor: pointer;\n  }\n  .buttons ul li .save{\n  \n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n    /* cursor: pointer; */\n  }\n  .buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.html":
/*!***************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-template\">\n  <div class=\"modal-header\">\n    <span class=\"\">Assign User Role</span>\n  </div>\n\n  <div class=\"main-empdata\">\n    <div class=\"user-type col-md-6\">\n      <div class=\"form-group\">\n        <label class=\"type-lebel\">Employee Name</label>\n        <span class=\"emp-name\">\n          <input type=\"text\" [(ngModel)]=\"data.personal.name.preferredName\" placeholder=\"Employee Name\"\n            class=\"form-control company-legal-name\" readonly>\n        </span>\n      </div>\n      <div class=\"form-group \">\n        <label class=\"type-lebel\">Current User Role</label>\n        <span class=\"emp-name\">\n          <mat-select [disabled]=true class=\"form-control text-field select-btn\" [(ngModel)]=\"data.job.Site_AccessRole\"\n            placeholder=\"Select user role\">\n            <mat-option *ngFor=\"let data of resRolesObj\" [value]=\"data._id\"> {{data.name}}</mat-option>\n\n          </mat-select>\n        </span>\n      </div>\n      <div class=\"form-group\">\n        <label class=\"type-lebel\">New User Role</label>\n        <span class=\"emp-name\">\n          <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"newRole\" placeholder=\"Select user role\">\n\n            <mat-option *ngFor=\"let data of resRolesObj\" [value]=\"data._id\"> {{data.name}}</mat-option>\n          </mat-select>\n        </span>\n      </div>\n    </div>\n  </div>\n  <div class=\"clearfix\"></div>\n  <div class=\"buttons\">\n    <ul class=\"list-unstyled\">\n\n      <li class=\"list-items\">\n        <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n      </li>\n      <li class=\"list-items pull-right\">\n        <button class='save' type=\"submit\" (click)=\"submitChangeRole()\">\n          Save Changes\n        </button>\n      </li>\n\n      <div class=\"clearfix\"></div>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: ChangeUserRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeUserRoleComponent", function() { return ChangeUserRoleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ChangeUserRoleComponent = /** @class */ (function () {
    function ChangeUserRoleComponent(dialogRef, data, siteAccessRolesService, swalAlertService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.siteAccessRolesService = siteAccessRolesService;
        this.swalAlertService = swalAlertService;
        this.companyRoles = [];
        this.resRolesObj = [];
    }
    ChangeUserRoleComponent.prototype.ngOnInit = function () {
        console.log(this.data);
        this.getAllroles();
        this.getBaseRoles();
    };
    ChangeUserRoleComponent.prototype.getAllroles = function () {
        var _this = this;
        var cID = localStorage.getItem('companyId');
        console.log("cid", cID);
        cID = cID.replace(/^"|"$/g, "");
        console.log("cin2", cID);
        this.siteAccessRolesService.companyId(cID)
            .subscribe(function (res) {
            console.log("get ROles", res);
            _this.companyRoles = res.data;
            var result = _this.companyRoles.map(function (el) {
                var o = Object.assign({}, el);
                o.isActive = false;
                return o;
            });
            _this.companyRoles = result;
            console.log(_this.companyRoles, 'custom create');
        }, function (err) {
            console.log(err);
        });
    };
    ChangeUserRoleComponent.prototype.getBaseRoles = function () {
        var _this = this;
        this.siteAccessRolesService.getBaseRoles()
            .subscribe(function (res) {
            console.log("roles", res);
            _this.resRolesObj = res.data;
            _this.companyRoles.forEach(function (element) {
                _this.resRolesObj.push(element);
            });
            console.log(_this.resRolesObj, "all roles");
        }, function (err) {
        });
    };
    ChangeUserRoleComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    ChangeUserRoleComponent.prototype.submitChangeRole = function () {
        var _this = this;
        var postData = {
            "userId": this.data._id,
            "newRoleId": this.newRole,
        };
        console.log("sendingdata", postData);
        this.siteAccessRolesService.changeSiteAccessRoleforEmp(postData)
            .subscribe(function (res) {
            console.log("roles", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation('User Role', res.message, 'success');
                _this.cancel();
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation('User Role', err.error.message, 'error');
        });
    };
    ChangeUserRoleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-change-user-role',
            template: __webpack_require__(/*! ./change-user-role.component.html */ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.html"),
            styles: [__webpack_require__(/*! ./change-user-role.component.css */ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_2__["SiteAccessRolesService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"]])
    ], ChangeUserRoleComponent);
    return ChangeUserRoleComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-access.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-access.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".site-access-main { margin:30px auto 0; float: none; padding: 0;}\n\n.zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.zenwork-currentpage{\n    padding: 0;\n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.site-access-tabs {display: block;padding: 20px 0 0;margin: 0 0 30px;}\n\n.site-access-tabs .tab-content { margin:40px 0 0;}\n\n.site-access-tabs .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border: none;background: transparent;}\n\n.site-access-tabs .nav-tabs {border-bottom:#dbdbdb 2px solid;}\n\n.site-access-tabs .nav-tabs > li { margin: 0 30px;}\n\n.site-access-tabs .nav-tabs > li > a {font-size: 17px;line-height: 17px;font-weight:normal;color:#484747;border: none;padding: 10px 0 15px;margin: 0;}\n\n.site-access-tabs .nav > li > a:hover, .nav > li > a:focus { background: none;}\n\n.site-access-tabs .nav-tabs > li.active{border-bottom:#439348 5px solid;margin: 0px 30px -1px 30px;color:#000;}\n\n.employee-role { margin: 0 10px 100px;}\n\n.employee-role-lft {padding: 0;}\n\n.employee-role-lft label {display: block; color:#5a5959; font-weight: normal; padding: 0 0 15px; font-size: 17px; line-height: 17px;}\n\n.employee-role-rgt { padding: 0;}\n\n.add-fields { display: block; margin: 0 0 15px;}\n\n.add-fields a { display: inline-block; vertical-align: middle; cursor: pointer; color:#676767; font-size: 13px; line-height: 13px;}\n\n.add-fields a small { display: inline-block; padding: 0 10px 0 0;}\n\n.add-fields a small .fa { color:#008f3d; font-size: 12px; line-height: 12px;}\n\n.employee-role-rgt a { display: block;}\n\n.employee-role-rgt a.btn { background:#008f3d; height: 40px;line-height: 28px; color:#fff; text-align: center;}\n\n.user-type-heading{\n    font-size: 16px;\n}\n\n.custom-user {margin: 20px 0;}\n\n.custom-user .table>thead>tr>th { color: #484848; font-weight: normal; padding: 13px 20px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n\n.custom-user .table>tbody>tr>td, .custom-user .table>tfoot>tr>td, .custom-user .table>thead>tr>td{ padding: 15px 20px; background:#fff;border: none;vertical-align:middle; border-top:none;border-bottom:#c3c3c3 1px solid; color: #6d6a6a;}\n\n.custom-user .checkbox{ margin: 0;}\n\n.custom-user .table>tbody>tr>td.zenwork-border {position: relative;}\n\n.custom-user .table>tbody>tr>td.zenwork-border:after { content:''; position: absolute; top: 2px;left: 2px;border-right: #83c7a1 4px solid;height: 90%;border-radius: 0 5px 5px 0;}\n\n.table-icons { float: right;}\n\n.table-icons ul { display:block; margin: 0;}\n\n.table-icons ul li { display:inline-block; padding: 0 0 0 15px;}\n\n.table-icons ul li a { display: inline-block; cursor: pointer;}\n\n.table-icons ul li a .fa { color:#868686; font-size: 15px; line-height: 15px;}\n\n.custom-user-top { margin: 0 0 30px;}\n\n.custom-user-top h3 { color:#008f3d; font-size: 17px; line-height: 17px; font-weight: normal;}\n\n.custom-user-top a { display: block;}\n\n.custom-user-top a.btn { background:#008f3d; height: 40px;line-height: 28px; color:#fff; text-align: center; padding: 6px 20px;}\n\n.view-rights-popup {background:#fff;}\n\n.view-rights-popup .modal-dialog { width: 80%;}\n\n.view-rights-popup .modal-content { box-shadow: none;border: none;}\n\n.view-rights-popup .modal-body { padding:50px 40px 30px;}\n\n.view-right-cont { padding:0;}\n\n.view-right-cont .panel-default>.panel-heading { padding: 0 0 20px; background-color:transparent !important; border: none;}\n\n.view-right-cont .panel-title { color: #008f3d; display: inline-block;}\n\n.view-right-cont .panel-title a:hover { text-decoration: none;}\n\n.view-right-cont .panel-title>.small, .view-right-cont .panel-title>.small>a, .view-right-cont .panel-title>a, .view-right-cont .panel-title>small, .view-right-cont .panel-title>small>a { text-decoration:none;}\n\n.view-right-cont .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n\n.view-right-cont .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n\n.view-right-cont .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.view-right-cont .panel-group .panel-heading+.panel-collapse>.list-group, .view-right-cont .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.view-right-cont .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n\n.view-right-cont .panel-body { padding: 0 0 20px;}\n\n.view-right-cont .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.view-right-cont .panel-default { border-color:transparent;}\n\n.view-right-cont .panel-group .panel+.panel { margin-top:30px;}\n\n.person-fields { display: block;}\n\n.person-fields .modal-header { width: 90%; margin:0 auto; padding:0 50px 10px; border-bottom: 1px solid #c5c3c3;}\n\n.person-fields .modal-title { color:#008f3d;}\n\n.person-fields .modal-dialog { width:70%;}\n\n.person-fields .modal-content { border: none; padding:30px 0 50px;}\n\n.person-tables { margin: 0 auto; float: none; padding: 0;}\n\n.person-tables h3 { font-size: 15px; line-height: 15px; color:#5d5d5d; margin: 0 0 30px;}\n\n.person-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 16px; border-radius:3px;}\n\n.person-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n\n.person-tables .table>thead>tr>th a .fa { color:#c3c3c3; font-size: 15px; line-height: 15px;}\n\n.person-tables .table>tbody>tr>td, .person-tables .table>tfoot>tr>td, .person-tables .table>thead>tr>td{ padding: 15px; background:#f8f8f8;border: none;vertical-align:middle; border-top:none;border-bottom:#e6e6e6 1px solid; color: #656464;}\n\n.person-fields .checkbox{ margin: 0;}\n\n.person-tables .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top: 0;height: 100%;}\n\n.person-tables .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n\n.person-tables .cont-check .checkbox label { padding-left:20px;}\n\n.person-tables .cont-check.label-pad .checkbox label { padding-left: 0;}\n\n.view-right-cont .btn {border-radius: 20px;color:#000; font-size: 15px; padding:6px 25px; background:transparent; margin:30px 0 0; float:left;}\n\n.view-right-cont .btn:hover {background: #008f3d; color:#fff;}\n\n.main-onboarding .setting-drop .dropdown-menu{\n    left: -76px !important;\n    min-width: 109px !important;\n}\n\n.list-of-current-users{\n    margin: 40px 0;\n}\n\n.select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n    width: 95%;\n}\n\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #fff;\n}\n\n.list-of-current-users label{\n    line-height: 40px;\n}\n\n.employee-list .list-items{\n    border-bottom: 1px #c3c3c3 solid;\n    background: #fff;\n    display: block;\n    padding: 15px 10px;\n}\n\n.employe-list-table{\n    padding: 39px 0 0 44px;\n}\n\n.employee-table-headings{\n    padding: 10px 0 10px 10px;\n    background: #eef7ff;\n}\n\n.run-report-btn{\n    padding:6px 20px 8px 25px;\n    border-radius: 25px;\n    font-size: 12px;\n    border: 0;\n    color: #fff;\n    box-shadow: none;\n    background: #797d7b;\n    /* background: url(\"/assets/images/Manager Self Service/Group -1.png\") no-repeat 10px center #797d7b; */\n}\n\n.role-description{\n    \n}\n\n.zenwork-margin-zero em { font-size: 15px; padding: 2px 0 0 15px; display: inline-block; font-style: normal; vertical-align: middle;}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-access.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-access.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"site-access-main col-md-11\">\n\n  <p class=\"zenwork-margin-zero\">\n    <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n      <span class=\"green mr-7\">\n        <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n      </span>Back\n    </button>\n    <small>\n      <img src=\"../../../assets/images/company-settings/site-access/site-access-icon.png\" alt=\"Company-settings icon\">\n    </small>\n    <em>Site Access / Rights Management </em>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n\n\n  <div class=\"site-access-tabs\">\n\n    <span class=\"user-type-heading\">User Types</span>\n    <div class=\"custom-user\">\n\n      <!-- <div class=\"custom-user-top\">\n                <h3 class=\"pull-left\">Custom User Types</h3>\n\n                <a routerLink=\"/admin/admin-dashboard/company-settings/site-access-management/site-user\" class=\"btn pull-right\">Preview Site as User</a>\n                <div class=\"clearfix\"></div>\n              </div>  -->\n\n      <table class=\"table\">\n        <thead>\n          <tr>\n            <th>\n\n            </th>\n            <th>User Type Role</th>\n            <th>Role Name</th>\n            <th>Description of Role</th>\n\n            <th>\n              <div class=\"main-onboarding \">\n                <div class=\"setting-drop\">\n                  <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                    <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                  </a>\n                  <ul class=\"dropdown-menu\">\n\n                    <li *ngIf=\"!addShowHide\">\n                      <a (click)=\"openDialog('add')\">Add</a>\n                    </li>\n                    <li *ngIf=\"addShowHide\">\n                      <a (click)=\"openDialog('edit')\">Edit</a>\n                    </li>\n                    <li *ngIf=\"!addShowHide\">\n                      <a (click)=\"openDialog1()\"> Configure</a>\n                    </li>\n                    <li *ngIf=\"addShowHide\">\n                      <a (click)=\"copyData()\">Copy</a>\n                    </li>\n                    <!-- <li>\n                      <a>Preview</a>\n                    </li> -->\n                    <li *ngIf=\"addShowHide\">\n                      <a (click)=\"deleteCustomUserRole()\">Delete</a>\n                    </li>\n\n                  </ul>\n                </div>\n              </div>\n            </th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let data of resRolesObj\">\n            <td>\n              <mat-checkbox [disabled]=\"true\" class=\"zenwork-customized-checkbox\"></mat-checkbox>\n            </td>\n            <td>\n              Standard\n            </td>\n            <td>\n              {{data.name}}\n            </td>\n            <td>\n              {{data.description}}\n            </td>\n            <td>\n\n            </td>\n          </tr>\n          <tr *ngFor=\"let data of companyRoles\">\n            <td [ngClass]=\"{'zenwork-checked-border': data.isActive}\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isActive\"\n                (change)=singleEditDelete(data)></mat-checkbox>\n            </td>\n            <td>Custom-{{data.baseRoleId.name}}</td>\n            <td>{{data.name}}</td>\n            <td class=\"role-description\">{{data.description}}</td>\n            <td> </td>\n          </tr>\n\n\n\n          <!-- <tr>\n            <td [ngClass]=\"{'zenwork-checked-border': true}\">\n              <mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox>\n            </td>\n            <td>Custom 4</td>\n            <td>US System Admin</td>\n            <td>Standard</td>\n            <td> </td>\n          </tr> -->\n\n\n\n      </table>\n\n    </div>\n    <div class=\"list-of-current-users col-xs-12\">\n      <div class=\"row\">\n        <div class=\"col-xs-3\">\n          <div class=\"row\">\n            <h4>List of Current Users</h4>\n            <div class=\"form-group\">\n              <label>Select User Role Name</label>\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"slectedUserType\"\n                (ngModelChange)=\"postIdforEmployees($event)\" placeholder=\"Select user role\">\n                <mat-option *ngFor=\"let data of companyRoles\" [value]=\"data._id\">{{data.name}}</mat-option>\n\n              </mat-select>\n\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-9 employe-list-table\">\n          <div class=\"employee-table-headings\">\n            <span>Employee Name</span>\n            <!-- <span class=\"pull-right\">\n              <button class=\"btn btn-default run-report-btn\">\n                Run Report</button>\n            </span> -->\n            <span class=\"pull-right\">\n                <div class=\"main-onboarding \">\n                    <div class=\"setting-drop\">\n                      <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                        <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                      </a>\n                      <ul class=\"dropdown-menu\">\n    \n                        <li>\n                          <a (click)=\"openDialogforChangeRole()\">Change User Role</a>\n                        </li>\n                        \n    \n                      </ul>\n                    </div>\n                  </div>\n            </span>\n            <div class=\"clearfix\"></div>\n          </div>\n          <ul class=\"list-unstyled employee-list\">\n            <span *ngFor=\"let data of selectedEmployees\">\n              <!-- <li>\n               \n              </li> -->\n              <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': data.isActive}\">\n                  <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.isActive\"\n                  (change)=singleEmpEdit(data)></mat-checkbox>\n                {{data.personal.name.preferredName}}\n              </li>\n            </span>\n            <li *ngIf=\"selectedEmployees.length == 0\" class=\"list-items\">\n              No employees assigned to role\n            </li>\n            <!-- <li *ngIf =\"!showEmployees\" class=\"list-items\">\n              No employees assigned to role\n            </li> -->\n          </ul>\n          <mat-paginator [length]=\"count\" [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 25, 100]\"\n            (page)=\"pageEvents($event)\">\n          </mat-paginator>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n<div class=\"add-custom-popup\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n\n          <div class=\"add-custom-cont\">\n\n            <div class=\"panel-group\" id=\"accordion7\">\n\n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion7\"\n                      href=\"#collapseTwentyFive\">\n                      Personal Access Level\n                    </a>\n                  </h4>\n                </div>\n                <div id=\"collapseTwentyFive\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body\">\n\n\n                  </div>\n                </div>\n\n              </div>\n\n\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n\n\n\n</div>\n\n\n<div class=\"view-rights-popup\">\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n\n        <div class=\"modal-body\">\n\n          <div class=\"view-right-cont\">\n\n            <div class=\"panel-group\" id=\"accordion6\">\n\n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion6\"\n                      href=\"#collapseTwentyOne\">\n                      Personal Access Level\n                    </a>\n                  </h4>\n                </div>\n                <div id=\"collapseTwentyOne\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body\">\n\n                    <div class=\"person-tables\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox1\" type=\"checkbox\">\n                                  <label for=\"checkbox1\">Field Name</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox2\" type=\"checkbox\">\n                                  <label for=\"checkbox2\">No Access</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox3\" type=\"checkbox\">\n                                  <label for=\"checkbox3\">View Only</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox4\" type=\"checkbox\">\n                                  <label for=\"checkbox4\">Change Requires Approval</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox5\" type=\"checkbox\">\n                                  <label for=\"checkbox5\">Change Without Approval</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <a>\n                                <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n                              </a>\n                            </th>\n                          </tr>\n                        </thead>\n                        <tbody>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox6\" type=\"checkbox\">\n                                  <label for=\"checkbox6\">First Name</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox7\" type=\"checkbox\">\n                                  <label for=\"checkbox7\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox8\" type=\"checkbox\">\n                                  <label for=\"checkbox8\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox9\" type=\"checkbox\">\n                                  <label for=\"checkbox9\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox10\" type=\"checkbox\">\n                                  <label for=\"checkbox10\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox11\" type=\"checkbox\">\n                                  <label for=\"checkbox11\">Middle Name</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox12\" type=\"checkbox\">\n                                  <label for=\"checkbox12\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox13\" type=\"checkbox\">\n                                  <label for=\"checkbox13\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox14\" type=\"checkbox\">\n                                  <label for=\"checkbox14\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox15\" type=\"checkbox\">\n                                  <label for=\"checkbox15\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox16\" type=\"checkbox\">\n                                  <label for=\"checkbox16\">Last Name</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox17\" type=\"checkbox\">\n                                  <label for=\"checkbox17\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox18\" type=\"checkbox\">\n                                  <label for=\"checkbox18\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox19\" type=\"checkbox\">\n                                  <label for=\"checkbox19\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox20\" type=\"checkbox\">\n                                  <label for=\"checkbox20\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox21\" type=\"checkbox\">\n                                  <label for=\"checkbox21\">Preferred Name</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox22\" type=\"checkbox\">\n                                  <label for=\"checkbox22\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox23\" type=\"checkbox\">\n                                  <label for=\"checkbox23\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox24\" type=\"checkbox\">\n                                  <label for=\"checkbox24\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox25\" type=\"checkbox\">\n                                  <label for=\"checkbox25\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox26\" type=\"checkbox\">\n                                  <label for=\"checkbox26\">Phone Number</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox27\" type=\"checkbox\">\n                                  <label for=\"checkbox27\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox28\" type=\"checkbox\">\n                                  <label for=\"checkbox28\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox29\" type=\"checkbox\">\n                                  <label for=\"checkbox29\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox30\" type=\"checkbox\">\n                                  <label for=\"checkbox30\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox31\" type=\"checkbox\">\n                                  <label for=\"checkbox31\">State</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox32\" type=\"checkbox\">\n                                  <label for=\"checkbox32\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox33\" type=\"checkbox\">\n                                  <label for=\"checkbox33\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox34\" type=\"checkbox\">\n                                  <label for=\"checkbox34\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox35\" type=\"checkbox\">\n                                  <label for=\"checkbox35\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox36\" type=\"checkbox\">\n                                  <label for=\"checkbox36\">Location</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox37\" type=\"checkbox\">\n                                  <label for=\"checkbox37\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox38\" type=\"checkbox\">\n                                  <label for=\"checkbox38\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox39\" type=\"checkbox\">\n                                  <label for=\"checkbox39\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox40\" type=\"checkbox\">\n                                  <label for=\"checkbox40\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox41\" type=\"checkbox\">\n                                  <label for=\"checkbox41\">Marital Status</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox42\" type=\"checkbox\">\n                                  <label for=\"checkbox42\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox43\" type=\"checkbox\">\n                                  <label for=\"checkbox43\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox44\" type=\"checkbox\">\n                                  <label for=\"checkbox44\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox45\" type=\"checkbox\">\n                                  <label for=\"checkbox45\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox46\" type=\"checkbox\">\n                                  <label for=\"checkbox46\">Date</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox47\" type=\"checkbox\">\n                                  <label for=\"checkbox47\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox48\" type=\"checkbox\">\n                                  <label for=\"checkbox48\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox49\" type=\"checkbox\">\n                                  <label for=\"checkbox49\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox50\" type=\"checkbox\">\n                                  <label for=\"checkbox50\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                      </table>\n\n\n                    </div>\n\n\n                  </div>\n                </div>\n\n              </div>\n\n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion6\"\n                      href=\"#collapseTwentyTwo\">\n                      Job Access Level\n                    </a>\n                  </h4>\n\n                </div>\n\n                <div id=\"collapseTwentyTwo\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body\">\n\n                    <div class=\"person-tables\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox51\" type=\"checkbox\">\n                                  <label for=\"checkbox51\">Job Title</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox52\" type=\"checkbox\">\n                                  <label for=\"checkbox52\">No Access</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox53\" type=\"checkbox\">\n                                  <label for=\"checkbox53\">View Only</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox54\" type=\"checkbox\">\n                                  <label for=\"checkbox54\">Change Requires Approval</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox55\" type=\"checkbox\">\n                                  <label for=\"checkbox55\">Change Without Approval</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <a>\n                                <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n                              </a>\n                            </th>\n                          </tr>\n                        </thead>\n                        <tbody>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox56\" type=\"checkbox\">\n                                  <label for=\"checkbox56\">Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox57\" type=\"checkbox\">\n                                  <label for=\"checkbox57\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox58\" type=\"checkbox\">\n                                  <label for=\"checkbox58\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox59\" type=\"checkbox\">\n                                  <label for=\"checkbox59\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox60\" type=\"checkbox\">\n                                  <label for=\"checkbox60\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox61\" type=\"checkbox\">\n                                  <label for=\"checkbox61\">Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox62\" type=\"checkbox\">\n                                  <label for=\"checkbox62\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox63\" type=\"checkbox\">\n                                  <label for=\"checkbox63\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox64\" type=\"checkbox\">\n                                  <label for=\"checkbox64\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox65\" type=\"checkbox\">\n                                  <label for=\"checkbox65\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox66\" type=\"checkbox\">\n                                  <label for=\"checkbox66\">Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox67\" type=\"checkbox\">\n                                  <label for=\"checkbox67\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox68\" type=\"checkbox\">\n                                  <label for=\"checkbox68\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox69\" type=\"checkbox\">\n                                  <label for=\"checkbox69\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox70\" type=\"checkbox\">\n                                  <label for=\"checkbox70\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox71\" type=\"checkbox\">\n                                  <label for=\"checkbox71\">Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox72\" type=\"checkbox\">\n                                  <label for=\"checkbox72\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox73\" type=\"checkbox\">\n                                  <label for=\"checkbox73\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox74\" type=\"checkbox\">\n                                  <label for=\"checkbox74\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox75\" type=\"checkbox\">\n                                  <label for=\"checkbox75\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox76\" type=\"checkbox\">\n                                  <label for=\"checkbox76\">Sale Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox77\" type=\"checkbox\">\n                                  <label for=\"checkbox77\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox78\" type=\"checkbox\">\n                                  <label for=\"checkbox78\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox79\" type=\"checkbox\">\n                                  <label for=\"checkbox79\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox80\" type=\"checkbox\">\n                                  <label for=\"checkbox80\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox81\" type=\"checkbox\">\n                                  <label for=\"checkbox81\">Sale Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox82\" type=\"checkbox\">\n                                  <label for=\"checkbox82\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox83\" type=\"checkbox\">\n                                  <label for=\"checkbox83\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox84\" type=\"checkbox\">\n                                  <label for=\"checkbox84\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox85\" type=\"checkbox\">\n                                  <label for=\"checkbox85\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox86\" type=\"checkbox\">\n                                  <label for=\"checkbox86\">Marketing Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox87\" type=\"checkbox\">\n                                  <label for=\"checkbox87\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox88\" type=\"checkbox\">\n                                  <label for=\"checkbox88\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox89\" type=\"checkbox\">\n                                  <label for=\"checkbox89\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox90\" type=\"checkbox\">\n                                  <label for=\"checkbox90\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox91\" type=\"checkbox\">\n                                  <label for=\"checkbox91\">Marketing Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox92\" type=\"checkbox\">\n                                  <label for=\"checkbox92\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox93\" type=\"checkbox\">\n                                  <label for=\"checkbox93\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox94\" type=\"checkbox\">\n                                  <label for=\"checkbox94\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox95\" type=\"checkbox\">\n                                  <label for=\"checkbox95\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox96\" type=\"checkbox\">\n                                  <label for=\"checkbox96\">Marketing Manager</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox97\" type=\"checkbox\">\n                                  <label for=\"checkbox97\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox98\" type=\"checkbox\">\n                                  <label for=\"checkbox98\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox99\" type=\"checkbox\">\n                                  <label for=\"checkbox99\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox100\" type=\"checkbox\">\n                                  <label for=\"checkbox100\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                      </table>\n\n\n                    </div>\n\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                  <h4 class=\"panel-title\">\n                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion6\"\n                      href=\"#collapseTwentyThree\">\n                      Emergency Access Level\n                    </a>\n                  </h4>\n\n                </div>\n\n                <div id=\"collapseTwentyThree\" class=\"panel-collapse collapse in\">\n                  <div class=\"panel-body\">\n\n                    <div class=\"person-tables\">\n\n                      <table class=\"table\">\n                        <thead>\n                          <tr>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox101\" type=\"checkbox\">\n                                  <label for=\"checkbox101\">Name</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox102\" type=\"checkbox\">\n                                  <label for=\"checkbox102\">No Access</label>\n                                </div>\n                              </div>\n                            </th>\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox103\" type=\"checkbox\">\n                                  <label for=\"checkbox103\">View Only</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox104\" type=\"checkbox\">\n                                  <label for=\"checkbox104\">Change Requires Approval</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox105\" type=\"checkbox\">\n                                  <label for=\"checkbox105\">Change Without Approval</label>\n                                </div>\n                              </div>\n                            </th>\n\n                            <th>\n                              <a>\n                                <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n                              </a>\n                            </th>\n                          </tr>\n                        </thead>\n                        <tbody>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox106\" type=\"checkbox\">\n                                  <label for=\"checkbox106\">Name</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox107\" type=\"checkbox\">\n                                  <label for=\"checkbox107\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox108\" type=\"checkbox\">\n                                  <label for=\"checkbox108\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox109\" type=\"checkbox\">\n                                  <label for=\"checkbox109\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox110\" type=\"checkbox\">\n                                  <label for=\"checkbox110\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox111\" type=\"checkbox\">\n                                  <label for=\"checkbox111\">Relation Ship</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox112\" type=\"checkbox\">\n                                  <label for=\"checkbox112\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox113\" type=\"checkbox\">\n                                  <label for=\"checkbox113\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox114\" type=\"checkbox\">\n                                  <label for=\"checkbox114\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox115\" type=\"checkbox\">\n                                  <label for=\"checkbox115\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox116\" type=\"checkbox\">\n                                  <label for=\"checkbox116\">Phone</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox117\" type=\"checkbox\">\n                                  <label for=\"checkbox117\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox118\" type=\"checkbox\">\n                                  <label for=\"checkbox118\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox119\" type=\"checkbox\">\n                                  <label for=\"checkbox119\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox120\" type=\"checkbox\">\n                                  <label for=\"checkbox120\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox120\" type=\"checkbox\">\n                                  <label for=\"checkbox120\">Email</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox121\" type=\"checkbox\">\n                                  <label for=\"checkbox121\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox122\" type=\"checkbox\">\n                                  <label for=\"checkbox122\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox123\" type=\"checkbox\">\n                                  <label for=\"checkbox123\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox124\" type=\"checkbox\">\n                                  <label for=\"checkbox124\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                          <tr>\n                            <td class=\"zenwork-checked-border\">\n\n                              <div class=\"cont-check\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox125\" type=\"checkbox\">\n                                  <label for=\"checkbox125\">Address</label>\n                                </div>\n                              </div>\n\n                            </td>\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox126\" type=\"checkbox\">\n                                  <label for=\"checkbox126\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox127\" type=\"checkbox\">\n                                  <label for=\"checkbox127\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox128\" type=\"checkbox\">\n                                  <label for=\"checkbox128\"></label>\n                                </div>\n                              </div>\n                            </td>\n\n                            <td>\n                              <div class=\"cont-check label-pad\">\n                                <div class=\"checkbox\">\n                                  <input id=\"checkbox129\" type=\"checkbox\">\n                                  <label for=\"checkbox129\"></label>\n                                </div>\n                              </div>\n                            </td>\n                            <td></td>\n\n                          </tr>\n\n                      </table>\n\n\n                    </div>\n\n                  </div>\n                </div>\n              </div>\n\n              <button class=\"btn\" type=\"submit\">Submit</button>\n              <button class=\"btn\" type=\"submit\">close</button>\n              <div class=\"clearfix\"></div>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n\n\n\n</div>\n\n\n\n\n<!-- <router-outlet></router-outlet> -->"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-access.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-access.component.ts ***!
  \***************************************************************************************/
/*! exports provided: SiteAccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteAccessComponent", function() { return SiteAccessComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _site_user_create_custom_user_role_model_create_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./site-user/create-custom-user-role-model/create-custom-user-role-model.component */ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.ts");
/* harmony import */ var _site_user_edit_custom_user_role_model_edit_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./site-user/edit-custom-user-role-model/edit-custom-user-role-model.component */ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _change_user_role_change_user_role_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./change-user-role/change-user-role.component */ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SiteAccessComponent = /** @class */ (function () {
    function SiteAccessComponent(router, dialog, siteAccessRolesService, swalAlertService, employeeService) {
        this.router = router;
        this.dialog = dialog;
        this.siteAccessRolesService = siteAccessRolesService;
        this.swalAlertService = swalAlertService;
        this.employeeService = employeeService;
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.selectedEmployees = [];
        this.showEmployees = false;
        this.addShowHide = false;
        this.standards = ["standards1", "standards2", "standards3"];
    }
    SiteAccessComponent.prototype.ngOnInit = function () {
        //npm multi select dropdown sample start
        this.perPage = 5;
        this.pageNo = 1;
        this.getAllroles();
        this.getBaseRoles();
    };
    /* Description: get custom roles
     author : vipin reddy */
    SiteAccessComponent.prototype.getAllroles = function () {
        var _this = this;
        var cID = localStorage.getItem('companyId');
        console.log("cid", cID);
        cID = cID.replace(/^"|"$/g, "");
        console.log("cin2", cID);
        this.siteAccessRolesService.companyId(cID)
            .subscribe(function (res) {
            console.log("get ROles", res);
            _this.companyRoles = res.data;
            var result = _this.companyRoles.map(function (el) {
                var o = Object.assign({}, el);
                o.isActive = false;
                return o;
            });
            _this.companyRoles = result;
            console.log(_this.companyRoles, 'custom create');
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: get baseroles
     author : vipin reddy */
    SiteAccessComponent.prototype.getBaseRoles = function () {
        var _this = this;
        this.siteAccessRolesService.getBaseRoles()
            .subscribe(function (res) {
            console.log("roles", res);
            _this.resRolesObj = res.data;
        }, function (err) {
        });
    };
    /* Description: single edit delete for checkbox
       author : vipin reddy */
    SiteAccessComponent.prototype.singleEditDelete = function (data) {
        var _this = this;
        console.log(data);
        if (data.isActive == false) {
            this.popupEditData = '';
            this.addShowHide = false;
        }
        if (data.isActive == true) {
            this.companyRoles.forEach(function (element) {
                if (element._id === data._id) {
                    element.isActive = true;
                    _this.addShowHide = true;
                    _this.popupEditData = element;
                    _this.selectedId = data._id;
                }
                else {
                    element.isActive = false;
                }
                // console.log(element._id, data._id, element.isActive);
            });
        }
    };
    /* Description: getting users for custom role wise
     author : vipin reddy */
    SiteAccessComponent.prototype.postIdforEmployees = function (event) {
        var _this = this;
        console.log(event);
        var cID = JSON.parse(localStorage.getItem('companyId'));
        this.roleID = event;
        this.siteAccessRolesService.getAllEmployeesforCustomRoles(this.roleID)
            .subscribe(function (res) {
            console.log("get ROles", res);
            _this.tempEmpIds = res.data;
            _this.getEmpNames(_this.tempEmpIds);
        }, function (err) {
            console.log(err);
        });
    };
    SiteAccessComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.postIdforEmployees(this.roleID);
    };
    SiteAccessComponent.prototype.getEmpNames = function (empIds) {
        var _this = this;
        var postData = {
            "per_page": this.perPage,
            "page_no": this.pageNo,
            "_ids": empIds
        };
        this.employeeService.getOnboardedEmployeeDetails(postData)
            .subscribe(function (res) {
            console.log("get ROle of employees", res);
            _this.selectedEmployees = res.data;
            var result = _this.selectedEmployees.map(function (el) {
                var o = Object.assign({}, el);
                o.isActive = false;
                return o;
            });
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: before page redirect
       author : vipin reddy */
    SiteAccessComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/company-settings']);
    };
    /* Description: create/add the popup
       author : vipin reddy */
    SiteAccessComponent.prototype.openDialog = function (event) {
        var _this = this;
        if (event == 'add') {
            var data = {
                data: 'CreateCustomUserRole'
            };
            var dialogRef = this.dialog.open(_site_user_create_custom_user_role_model_create_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_3__["CreateCustomUserRoleModelComponent"], {
                width: '2000px',
                height: '700px',
                backdropClass: 'custom-user-role-model',
                data: data,
            });
            dialogRef.afterClosed().subscribe(function (result) {
                _this.getAllroles();
            });
        }
        if (event == 'edit') {
            var data1 = {
                data: this.popupEditData
            };
            var dialogRef = this.dialog.open(_site_user_create_custom_user_role_model_create_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_3__["CreateCustomUserRoleModelComponent"], {
                width: '2000px',
                height: '700px',
                backdropClass: 'custom-user-role-model',
                data: data1,
            });
            dialogRef.afterClosed().subscribe(function (result) {
                _this.getAllroles();
                _this.addShowHide = false;
            });
        }
    };
    /* Description: configure base roles for pages ACLS
       author : vipin reddy */
    SiteAccessComponent.prototype.openDialog1 = function () {
        var data = {
            data: 'CreateCustomUserRole'
        };
        var dialogRef = this.dialog.open(_site_user_edit_custom_user_role_model_edit_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_4__["EditCustomUserRoleModelComponent"], {
            width: '1600px',
            height: '700px',
            backdropClass: 'custom-user-role-model',
            data: data,
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("resultttt", result);
        });
    };
    SiteAccessComponent.prototype.openDialogforChangeRole = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_change_user_role_change_user_role_component__WEBPACK_IMPORTED_MODULE_8__["ChangeUserRoleComponent"], {
            width: '1300px',
            height: '600px',
            backdropClass: 'custom-user-role-model',
            data: this.popupRoleData,
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("resultttt", result);
            _this.slectedUserType = '';
            _this.selectedEmployees = [];
        });
    };
    /* Description: delete custom user role
     author : vipin reddy */
    SiteAccessComponent.prototype.deleteCustomUserRole = function () {
        var _this = this;
        this.siteAccessRolesService.deleteRole(this.popupEditData._id)
            .subscribe(function (res) {
            console.log("get ROles", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Delete Role", res.message, "success");
                _this.addShowHide = false;
                _this.getAllroles();
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Delete Role", err.error.message, "error");
        });
    };
    SiteAccessComponent.prototype.copyData = function () {
        var cID = JSON.parse(localStorage.getItem('companyId'));
        var copyData = {
            companyId: cID,
            _id: this.selectedId
        };
        this.employeeService.copyService(copyData)
            .subscribe(function (res) {
            console.log("copyy data", res);
        }, function (err) {
            console.log(err);
        });
    };
    SiteAccessComponent.prototype.singleEmpEdit = function (data) {
        var _this = this;
        console.log(data);
        // this.changeUserRoleData = data;
        if (data.isActive == false) {
            this.popupRoleData = '';
            // this.addShowHide = false;
        }
        if (data.isActive == true) {
            this.selectedEmployees.forEach(function (element) {
                if (element._id === data._id) {
                    element.isActive = true;
                    // this.addShowHide = true;
                    _this.popupRoleData = element;
                    _this.selectedRoleId = data._id;
                }
                else {
                    element.isActive = false;
                }
                // console.log(element._id, data._id, element.isActive);
            });
        }
    };
    SiteAccessComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-site-access',
            template: __webpack_require__(/*! ./site-access.component.html */ "./src/app/admin-dashboard/company-settings/site-access/site-access.component.html"),
            styles: [__webpack_require__(/*! ./site-access.component.css */ "./src/app/admin-dashboard/company-settings/site-access/site-access.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_5__["SiteAccessRolesService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_6__["SwalAlertService"],
            _services_employee_service__WEBPACK_IMPORTED_MODULE_7__["EmployeeService"]])
    ], SiteAccessComponent);
    return SiteAccessComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-access.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-access.module.ts ***!
  \************************************************************************************/
/*! exports provided: SiteAccessModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteAccessModule", function() { return SiteAccessModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _site_access_site_access_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../site-access/site-access.component */ "./src/app/admin-dashboard/company-settings/site-access/site-access.component.ts");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _site_user_create_custom_user_role_model_create_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./site-user/create-custom-user-role-model/create-custom-user-role-model.component */ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _site_user_edit_custom_user_role_model_edit_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./site-user/edit-custom-user-role-model/edit-custom-user-role-model.component */ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
/* harmony import */ var _change_user_role_change_user_role_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./change-user-role/change-user-role.component */ "./src/app/admin-dashboard/company-settings/site-access/change-user-role/change-user-role.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';



var router = [
    // {path:'', redirectTo:'site', pathMatch:'full'},
    { path: 'site', component: _site_access_site_access_component__WEBPACK_IMPORTED_MODULE_4__["SiteAccessComponent"] },
    // {path:'', redirectTo:'site-user', pathMatch:'full'},
    { path: 'site-user', loadChildren: './site-user/site-user.module#SiteUserModule' },
];
var SiteAccessModule = /** @class */ (function () {
    function SiteAccessModule() {
    }
    SiteAccessModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                // AngularMultiSelectModule,
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_9__["NgMultiSelectDropDownModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(router), _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                angular_dual_listbox__WEBPACK_IMPORTED_MODULE_10__["AngularDualListBoxModule"]
            ],
            declarations: [_site_access_site_access_component__WEBPACK_IMPORTED_MODULE_4__["SiteAccessComponent"], _site_user_create_custom_user_role_model_create_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_6__["CreateCustomUserRoleModelComponent"], _site_user_edit_custom_user_role_model_edit_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_8__["EditCustomUserRoleModelComponent"], _change_user_role_change_user_role_component__WEBPACK_IMPORTED_MODULE_11__["ChangeUserRoleComponent"]],
            entryComponents: [
                _site_user_create_custom_user_role_model_create_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_6__["CreateCustomUserRoleModelComponent"], _site_user_edit_custom_user_role_model_edit_custom_user_role_model_component__WEBPACK_IMPORTED_MODULE_8__["EditCustomUserRoleModelComponent"], _change_user_role_change_user_role_component__WEBPACK_IMPORTED_MODULE_11__["ChangeUserRoleComponent"]
            ]
        })
    ], SiteAccessModule);
    return SiteAccessModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.css":
/*!**************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.css ***!
  \**************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* side nav css*/\n.tabs-left{\n    margin-top:0;\n}\n.tabs-left > .nav-tabs {\n    border-bottom: 0;\n  }\n.tab-content > .tab-pane,\n  .pill-content > .pill-pane {\n    display: none;\n  }\n.tab-content > .active,\n  .pill-content > .active {\n    display: block;\n  }\n.tabs-left > .nav-tabs > li {\n    float: none;\n  }\n.tabs-left > .nav-tabs > li > a {\n    min-width: 74px;\n    margin-right: 0;\n    margin-bottom: 3px;\n    color: #000;\n    border: none;\n    background: #f8f8f8;\n  }\n.tabs-left > .nav-tabs {\n    /* float: left; */\n    /* border-right: 1px solid #ddd; */\n    border-right: 1px solid #ddd;\n    padding: 20px 20px 0 20px;\n    height: 700px;\n  }\n.tabs-left > .nav-tabs > li > a {\n    font-size: 16px;\n    margin-right: -1px;\n    outline: none;\n    border-radius: 4px 0 0 4px;\n  }\n.tabs-left > .nav-tabs > li > a:hover,\n  .tabs-left > .nav-tabs > li > a:focus {\n    border-color: #eeeeee #dddddd #eeeeee #eeeeee;\n  }\n.tabs-left > .nav-tabs .active > a,\n  .tabs-left > .nav-tabs .active > a:hover,\n  .tabs-left > .nav-tabs .active > a:focus {\n    border-color: #ddd transparent #ddd #ddd;\n    *border-right-color: #ffffff;\n  }\n/*side nav css ends*/\n.caret{\n    margin-left: 0px !important;\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n  }\n.create-custom-sidenav{\n    background: #f8f8f8;\n    padding: 20px 0;\n  }\n.main-template{\n    padding: 0 !important;\n  }\n.main-template .modal-header{\n    padding: 20px 0;\n    border-bottom: 1px solid #ccc;\n  }\n.select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n    /* width: 95%; */\n}\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\n.tab-content{\n    padding: 20px 0;\n}\n.user-type label{\n  line-height: 40px;\n\n}\n.company-legal-name{\n  border: none;\n  padding: 10px;\n  background: #f8f8f8;\n  box-shadow: none;\n  height: 40px;\n}\n.custom-scroll{\n  height: 700px;\n  overflow-y: auto;\n}\ntextarea{\n  resize: none;\n  border: 0;\n  box-shadow: none;\n  background: #f8f8f8;\n}\n.buttons{\n  margin: 35px 30px 0;\n}\n.buttons ul li{\n  display: inline-block;\n  padding: 0px 10px;\n}\n.buttons ul li .back{\n  border: 1px solid gray;\n  padding: 7px 30px;\n  border-radius: 31px;\n  color: #000;\n  cursor: pointer;\n}\n.buttons ul li .save{\n\n  border: 1px solid gray;\n  padding: 7px 30px;\n  border-radius: 31px;\n  color: #fff;\n  background: green;\n  /* cursor: pointer; */\n}\n.buttons ul li .cancel{\n  color:#e5423d;\n  cursor: pointer;\n}\n.system-admin{\n  padding: 40px 0 0 40px;\n}\n.modules-fields{\n  padding: 20px;\n}\n.module-table table{\n  width: 100%;\n}\n.module-table table thead th{\n  padding: 20px 40px 20px 15px;\n  font-size: 16px;\n  background: #f8f8f8;\n  border-right: 1px solid #ccc;\n  border-bottom: 1px solid #ccc;\n}\n.module-table table tbody tr td{\n  padding: 20px 40px 20px 15px;\n  font-size: 16px;\n  background: #f8f8f8;\n  border-right: 1px solid #ccc;\n  border-bottom: 1px solid #ccc;\n}\n.module-table-padding{\n  padding: 15px 30px 30px 15px;\n  border-bottom: 1px solid #ccc;\n}\n.dashboard-table-dropdown{\n  background: #fff !important;\n}\n.module-table table thead th:last-child, .module-table table tbody tr td:last-child {\n  border-right: 0;\n  \n}\n.module-table table tbody tr:last-child td:last-child,.module-table table tbody tr:last-child td:first-child{\n  border-bottom: 0;\n}\n/* accrodian css*/\n.field-accordion p {color: #565555;font-size: 15px;}\n.field-accordion .panel-default>.panel-heading { padding: 0 0 20px; background-color:transparent !important; border: none;}\n.field-accordion .panel-title { color: #008f3d; display: inline-block;padding: 15px 0 0 0;}\n.field-accordion .panel-title a:hover { text-decoration: none;}\n.field-accordion .panel-title>.small, .field-accordion .panel-title>.small>a, .field-accordion .panel-title>a, .field-accordion .panel-title>small, .field-accordion .panel-title>small>a { text-decoration:none;}\n.field-accordion .panel { background: none; box-shadow: none; border-radius: 0; border-bottom:#ccc 1px solid !important;}\n.field-accordion .panel-heading .accordion-toggle:after {font-family: 'Glyphicons Halflings';content: \"\\e114\";float: right;color: grey;}\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n/* accrodian css ends */\n/* personal table data */\n.person-tables { margin: 0 auto; float: none; padding: 0;}\n.person-tables h3 { font-size: 15px; line-height: 15px; color:#5d5d5d; margin: 0 0 30px;}\n.person-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; font-size: 16px; border-radius:3px;}\n.person-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.person-tables .table>thead>tr>th a .fa {  font-size: 15px; line-height: 15px;}\n.person-tables .table>tbody>tr>td, .person-tables .table>tfoot>tr>td, .person-tables .table>thead>tr>td{ padding: 15px; background:#f8f8f8;vertical-align:middle; border-right:#e6e6e6 1px solid;border-bottom:#e6e6e6 1px solid; color: #656464;text-align: center;}\n.person-fields .checkbox{ margin: 0;}\n.person-tables .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top: 0;height: 100%;}\n.person-tables .table>tbody>tr>td.zenwork-checked-border {position: relative;text-align: left}\n.person-tables .cont-check .checkbox label { padding-left:5px;}\n.person-tables .cont-check.label-pad .checkbox label { padding-left: 0;}\n.person-tables table thead tr th{\n  border-right: 1px solid #ccc;\n  /* border-bottom: 1px solid #ccc; */\n}\n.person-tables table thead tr th:last-child{\n  border-right: none;\n}\n.person-tables table tbody tr td:last-child{\n  border-right: none; \n}\n.person-tables table tbody tr:last-child td{\n  border-bottom: none; \n}\n.myinfo-fields-controll{\n  padding: 15px 30px 30px 15px;\n}\n.main-onboarding .setting-drop .dropdown-menu{\n  left: -76px !important;\n  min-width: 109px !important;\n}\n.setting-drop .dropdown-menu>li>a{\n    font-size: 14px !important;\n    line-height: 14px !important;\n    padding: 8px 20px !important;\n    display:block;\n}\n/* structure-field css */\n.structure-tbl-body {\n  background: #f8f8f8;\n  width: 45%;\n}\n.bg-color {\n    background: #ecf7ff;\n    padding: 15px 10px;\n}\n.mar-left-40 {\n    margin-left: 40px;\n}\n.active-structure {\n    border-left: 4px solid #5ddb97;\n    border-radius: 5px;\n}\n.arrow-position {\n  position: relative;\n}\n.arrow-align-one {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  left: 30px;\n  font-weight: lighter;\n  color: #439348;\n  \n  \n}\n.arrow-align-two {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  right: 0;\n  left: 0px;\n  font-weight: lighter;\n  color: #439348;\n}\n#fileUpload {\n  height: 0;\n  display: none;\n  width: 0;\n}\n.upload{\n  text-decoration: none;\n}\n.zenwork-checked-border:after { \n  content: '';\n  position: absolute;\n  top: 0;\n  left: -50px;\n  border-right: #83c7a1 4px solid;\n  height: 100%;\n  border-radius: 4px;\n  padding: 25px;\n  }\n.height-300 {\n  height: 300px;\n  overflow: auto;\n}\n/* structure-fieldds css ends */\n.structure-list .list-items{\n  padding: 15px 15px;\n  border-bottom: 2px solid #eee;\n}\n.structure-list .list-items-values{\n  padding: 17px 37px;\n  border-bottom: 2px solid #eee;\n}\n.arrow-position {\n  position: relative;\n}\n.structure-tbl-body {\n  background: #f8f8f8;\n  width: 45%;\n}\n.height-300 {\n  height: 300px;\n  overflow: auto;\n}\n.bg-color {\n  background: #ecf7ff;\n  padding: 15px 10px;\n}\n.mar-left-40 {\n  margin-left: 40px;\n}\n.heading-eligibility{\n  padding: 15px 0 0 30px !important;\n}\n.field-accordion .eligibilty-border-bottom{\n  padding: 0 0 40px !important;\n}\n.employees-data{\n  padding: 0 20px;\n}\n.error{\n  font-size:12px !important;\n  color: #f44336 !important;\n}\n.employees-table{\n  margin: 60px 0 0 0;\n}\n.recap-tab{\n  padding: 0 30px;\n}\n.dual-list .listbox button{\n  background: #008f3d !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.html":
/*!***************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.html ***!
  \***************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-template\">\n  <div class=\"modal-header\">\n    <span class=\"\">Create Custom User Role - Define User Type</span>\n  </div>\n  <div class=\"custom-modal-body\">\n    <div class=\"tabbable tabs-left\">\n\n      <ul class=\"nav nav-tabs col-md-3 create-custom-sidenav\">\n        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'chooseSteps' }\">\n          <a href=\"#chooseSteps\" data-toggle=\"tab\" (click)=\"chooseFieldss('chooseSteps')\">Define User Type<span\n              [ngClass]=\"{'caret': selectedNav == 'chooseSteps' }\"></span>\n          </a>\n        </li>\n        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'chooseFields' }\">\n          <a href=\"#chooseFields\" data-toggle=\"tab\" (click)=\"chooseFieldss('chooseFields')\">Choose Modules & Fields\n            <span [ngClass]=\"{'caret': selectedNav == 'chooseFields' }\"></span>\n          </a>\n        </li>\n        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'chooseEmployees' }\">\n          <a href=\"#chooseEmployees\" data-toggle=\"tab\" (click)=\"chooseFieldss('chooseEmployees')\">Choose Employees\n            <span [ngClass]=\"{'caret': selectedNav == 'chooseEmployees' }\"></span>\n          </a>\n        </li>\n        <li [ngClass]=\"{'zenwork-sidenav-activetab': selectedNav == 'summaryRecap' }\">\n          <a href=\"#summaryRecap\" data-toggle=\"tab\" (click)=\"chooseFieldss('summaryRecap')\">Summary / Recap\n            <span [ngClass]=\"{'caret': selectedNav == 'summaryRecap' }\"></span>\n          </a>\n        </li>\n\n\n      </ul>\n      <div class=\"tab-content col-md-9\">\n        <div class=\"tab-pane\" [ngClass]=\"{'active':selectedNav === 'chooseSteps','in':selectedNav==='chooseSteps'}\"\n          id=\"chooseSteps\">\n          <div class=\"user-type col-md-4\">\n            <div class=\"form-group\">\n              <label>User Type Level</label>\n              <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"userRoleData.baseRoleId\"\n                (ngModelChange)=\"roleChange(userRoleData.baseRoleId)\" placeholder=\"Select user role\" name=\"baseRoleId\"\n                #baseRoleId=\"ngModel\" required>\n                <mat-option *ngFor=\"let data of resRolesObj.data\" [value]=\"data._id\"> {{data.name}}</mat-option>\n\n                <!-- <mat-option value=\"Job2\">Job2</mat-option> -->\n              </mat-select>\n              <p *ngIf=\"!userRoleData.baseRoleId && baseRoleId.touched || (!userRoleData.baseRoleId && isValid)\"\n                class=\"error\">Please fill\n                this field</p>\n            </div>\n            <div class=\"form-group\">\n              <label>Name Of The Custom Type Role</label>\n              <input type=\"text\" placeholder=\"Role Type\" [(ngModel)]=\"userRoleData.name\"\n                class=\"form-control company-legal-name\" name=\"name\" #name=\"ngModel\" required>\n              <p *ngIf=\"!userRoleData.name && name.touched || (!userRoleData.name && isValid)\" class=\"error\">Please fill\n                this field</p>\n            </div>\n\n          </div>\n          <div *ngIf=\"userRoleData.name == 'System Admin'\" class=\"system-admin col-xs-5\">\n            <mat-checkbox class=\"zenwork-customized-checkbox\">\n              Allow System Administrator to also be designated as HR Contact\n            </mat-checkbox>\n          </div>\n          <div class=\"col-xs-12\"></div>\n          <div class=\"form-group col-xs-5\">\n            <label>Description</label>\n            <textarea class=\"form-control rounded-0\" [(ngModel)]=\"userRoleData.description\" rows=\"4\" name=\"description\"\n              #description=\"ngModel\" required></textarea>\n            <p *ngIf=\"!userRoleData.description && description.touched || (!userRoleData.description && isValid)\"\n              class=\"error\">Please\n              fill this field</p>\n\n          </div>\n\n          <div class=\"clearfix\"></div>\n\n\n\n          <div class=\"buttons\">\n            <ul class=\"list-unstyled\">\n              <li class=\"list-items\">\n                <a class=\"back\" (click)=\"cancel()\">Back</a>\n              </li>\n              <li class=\"list-items\">\n                <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n              </li>\n              <li *ngIf=\"!editCustomRole\" class=\"list-items pull-right\">\n                <button class='save' type=\"submit\" (click)=\"createUserRole()\">\n                  Proceed\n                </button>\n              </li>\n              <li *ngIf=\"editCustomRole\" class=\"list-items pull-right\">\n                <button class='save' type=\"submit\" (click)=\"UpdateUserRole()\">\n                  Proceed\n                </button>\n              </li>\n\n              <div class=\"clearfix\"></div>\n            </ul>\n          </div>\n        </div>\n        <div class=\"tab-pane\" [ngClass]=\"{'active':selectedNav === 'chooseFields','in':selectedNav==='chooseFields'}\"\n          id=\"chooseFields\">\n          <div class=\"col-xs-12 \">\n            <div class=\"row\">\n              <div class=\"col-md-4\">\n                <h4>Select the modules</h4>\n                <div class=\"form-group\">\n                  <input type=\"text\" placeholder=\"Role Name\" [(ngModel)]=\"userRoleData.name\"\n                    class=\"form-control company-legal-name\" name=\"roletype\" #roletype=\"ngModel\" readonly required>\n                  <p *ngIf=\"!userRoleData.roletype && roletype.touched || (!userRoleData.roletype && isValid)\"\n                    class=\"error\">Please fill this field</p>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-12 module-table-padding\">\n            <div class=\"row\">\n              <div class=\"col-md-6 module-table\">\n                <table>\n                  <thead>\n                    <th>Page Name</th>\n                    <th>Custom {{userRoleData.name}}</th>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let data of pagesAllObj.data; let i = index\" >\n                      <td>{{data.name}}</td>\n                      <td>\n                        <mat-select *ngIf=\"roleName == 'HR'\"\n                          class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                          placeholder=\"Select access level\" (change)=\"accessLevelChange($event)\" [(ngModel)]=\"data.hr\">\n                          <mat-option *ngIf=\"data.baseHr=='full' || data.baseHr=='view'\" value=\"view\">View Only</mat-option>\n                          <mat-option *ngIf=\"data.baseHr=='full'\" value=\"full\">Full Access</mat-option>\n                          <mat-option *ngIf=\"data.baseHr=='full' || data.baseHr=='view' || data.baseHr=='no'\" value=\"no\">No access\n                          </mat-option>\n                        </mat-select>\n                        <mat-select *ngIf=\"roleName == 'Manager'\"\n                          class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                          placeholder=\"Select access level\" [(ngModel)]=\"data.manager\" (ngModelChange)=\"accessLevelChange(roleName,$event,i)\">\n                          <mat-option *ngIf=\"data.baseManager=='full' || data.baseManager=='view'\" value=\"view\">View Only</mat-option>\n                          <mat-option *ngIf=\"data.baseManager=='full'\" value=\"full\">Full Access</mat-option>\n                          <mat-option *ngIf=\"data.baseManager=='full' || data.baseManager=='view' || data.baseManager=='no'\" value=\"no\">No access\n                          </mat-option>\n                        </mat-select>\n                        <mat-select *ngIf=\"roleName == 'Employee'\"\n                          class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                          placeholder=\"Select access level\" [(ngModel)]=\"data.employee\">\n                          <mat-option *ngIf=\"data.baseEmployee=='full' || data.baseEmployee=='view'\" value=\"view\">View Only</mat-option>\n                          <mat-option *ngIf=\"data.baseEmployee=='full'\" value=\"full\">Full Access</mat-option>\n                          <mat-option *ngIf=\"data.baseEmployee=='full' || data.baseEmployee=='view' || data.baseEmployee=='no'\" value=\"no\">No access\n                          </mat-option>\n                        </mat-select>\n                        <mat-select *ngIf=\"roleName == 'System Admin'\"\n                          class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                          placeholder=\"Select access level\" [(ngModel)]=\"data.sysadmin\">\n                          <mat-option *ngIf=\"data.sysadmin=='full' || data.sysadmin=='view'\" value=\"view\">View Only</mat-option>\n                          <mat-option *ngIf=\"data.sysadmin=='full'\" value=\"full\">Full Access</mat-option>\n                          <mat-option *ngIf=\"data.sysadmin=='full' || data.sysadmin=='view' || data.sysadmin=='no'\" value=\"no\">No access\n                          </mat-option>\n                        </mat-select>\n                      </td>\n                    </tr>\n\n                  </tbody>\n                </table>\n              </div>\n            </div>\n          </div>\n          <div class=\"myinfo-fields-controll\">\n            <div class=\"field-accordion\">\n\n              <div class=\"panel-group\" id=\"accordion1\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n                      <a *ngIf=\"!editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#personal-info\" (click)=\"getMyInfoTabsData('Personal')\">\n                        Personal Access Level\n                      </a>\n                      <a *ngIf=\"editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#personal-info\">\n                        Personal Access Level\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"personal-info\" class=\"panel-collapse collapse in\">\n                    <div class=\"panel-body\">\n                      <!-- personaltable -->\n\n                      <div class=\"person-tables\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>\n                                Field Name\n                              </th>\n                              <th>\n                                No Access\n                              </th>\n                              <th>\n                                View Only\n                              </th>\n\n                              <th>\n                                Change Requires Approval\n\n                              </th>\n\n                              <th>\n                                Change Without Approval\n                              </th>\n\n                            </tr>\n                          </thead>\n                          <tbody>\n                            <tr *ngFor=\"let data of personalFields\">\n                              <td style=\"text-align: left\">\n                                {{data.field_name}}\n                              </td>\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.no_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.full_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.not_req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n                            </tr>\n\n                          </tbody>\n                        </table>\n\n\n                      </div>\n                      <!-- personaltable ends -->\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"panel-group\" id=\"accordion1\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n\n                      <a *ngIf=\"!editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#job-info\" (click)=\"getMyInfoTabsData('Job')\">\n                        Job Access Level\n                      </a>\n                      <a *ngIf=\"editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#job-info\">\n                        Job Access Level\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"job-info\" class=\"panel-collapse collapse\">\n                    <div class=\"panel-body\">\n                      <!-- personaltable -->\n\n                      <div class=\"person-tables\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>\n                                Field Name\n                              </th>\n                              <th>\n                                No Access\n                              </th>\n                              <th>\n                                View Only\n                              </th>\n\n                              <th>\n                                Change Requires Approval\n\n                              </th>\n\n                              <th>\n                                Change Without Approval\n                              </th>\n\n                              <!-- <th>\n                                <div class=\"setting-drop\">\n                                  <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                    <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                  </a>\n                                  <ul class=\"dropdown-menu\">\n\n                                    <li>\n                                      <a>Edit</a>\n                                    </li>\n\n                                  </ul>\n                                </div> \n                              </th> -->\n                            </tr>\n                          </thead>\n                          <tbody>\n\n                            <tr *ngFor=\"let data of JobFields\">\n                              <td class=\"text-left\">\n                                {{data.field_name}}\n\n\n                              </td>\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.no_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.full_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.not_req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n                            </tr>\n\n                          </tbody>\n                        </table>\n\n\n                      </div>\n                      <!-- personaltable ends -->\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"panel-group\" id=\"accordion1\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n\n                      <a *ngIf=\"!editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#emergency-info\"\n                        (click)=\"getMyInfoTabsData('Emergency_Contact')\">\n                        Emergency Contact Access Level\n                      </a>\n                      <a *ngIf=\"editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#emergency-info\">\n                        Emergency Contact Access Level\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"emergency-info\" class=\"panel-collapse collapse\">\n                    <div class=\"panel-body\">\n                      <!-- personaltable -->\n\n                      <div class=\"person-tables\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>\n                                Field Name\n                              </th>\n                              <th>\n                                No Access\n                              </th>\n                              <th>\n                                View Only\n                              </th>\n\n                              <th>\n                                Change Requires Approval</th>\n\n                              <th>\n                                Change Without Approval\n                              </th>\n\n                              <!-- <th> -->\n                              <!-- <div class=\"setting-drop\">\n                                  <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                    <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                  </a>\n                                  <ul class=\"dropdown-menu\">\n\n                                    <li>\n                                      <a>Edit</a>\n                                    </li>\n\n                                  </ul>\n                                </div> -->\n                              <!-- </th> -->\n                            </tr>\n                          </thead>\n                          <tbody>\n\n                            <tr *ngFor=\"let data of emergency\">\n                              <td class=\"text-left\">\n                                {{data.field_name}}\n\n\n                              </td>\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.no_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.full_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.not_req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n\n                            </tr>\n\n                          </tbody>\n                        </table>\n\n\n                      </div>\n                      <!-- personaltable ends -->\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"panel-group\" id=\"accordion1\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n\n                      <a *ngIf=\"!editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#compensation-info\" (click)=\"getMyInfoTabsData('Compensation')\">\n                        Compensation Access Level\n                      </a>\n                      <a *ngIf=\"editCustomRole\" class=\"accordion-toggle\" data-toggle=\"collapse\"\n                        data-parent=\"#accordion1\" href=\"#compensation-info\">\n                        Compensation Access Level\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"compensation-info\" class=\"panel-collapse collapse\">\n                    <div class=\"panel-body\">\n                      <!-- personaltable -->\n\n                      <div class=\"person-tables\">\n\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>\n                                Field Name\n                              </th>\n                              <th>\n                                No Access\n                              </th>\n                              <th>\n                                View Only\n                              </th>\n\n                              <th>\n                                Change Requires Approval\n\n                              </th>\n\n                              <th>\n                                Change Without Approval\n                              </th>\n\n                              <!--<th>\n                                 <div class=\"setting-drop\">\n                                  <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                    <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                  </a>\n                                  <ul class=\"dropdown-menu\">\n\n                                    <li>\n                                      <a>Edit</a>\n                                    </li>\n\n                                  </ul>\n                                </div> \n                              </th>-->\n                            </tr>\n                          </thead>\n                          <tbody>\n\n                            <tr *ngFor=\"let data of compensationFields\">\n                              <td class=\"text-left\">\n                                {{data.field_name}}\n\n\n                              </td>\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.no_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.full_access\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n                              <td>\n                                <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"data.not_req_approval\">\n                                </mat-checkbox>\n\n                              </td>\n\n                            </tr>\n\n                          </tbody>\n                        </table>\n\n\n                      </div>\n                      <!-- personaltable ends -->\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"buttons\">\n            <ul class=\"list-unstyled\">\n              <li class=\"list-items\">\n                <a class=\"back\" (click)=\"backModulesandFields('chooseSteps')\">Back</a>\n              </li>\n              <li class=\"list-items\">\n                <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n              </li>\n              <li *ngIf=\"!editCustomRole\" class=\"list-items pull-right\">\n                <button class='save' type=\"submit\" (click)=\"postShowFields()\">\n                  Save Changes\n                </button>\n              </li>\n              <li *ngIf=\"editCustomRole\" class=\"list-items pull-right\">\n                <button class='save' type=\"submit\" (click)=\"postUpdateShowFields()\">\n                  Save Changes\n                </button>\n              </li>\n\n              <div class=\"clearfix\"></div>\n            </ul>\n          </div>\n\n        </div>\n        <div class=\"tab-pane\"\n          [ngClass]=\"{'active':selectedNav === 'chooseEmployees','in':selectedNav==='chooseEmployees'}\"\n          id=\"chooseEmployees\">\n\n          <div class=\"employees-data\">\n            <div class=\"field-accordion\">\n              <div class=\"panel-group\" id=\"accordion2\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title heading-eligibility\">\n                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\"\n                        href=\"#eligibility-info\">\n                        Eligibility Criteria\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"eligibility-info\" class=\"panel-collapse collapse in\">\n                    <div class=\"panel-body eligibilty-border-bottom\">\n\n                      <div class=\"employees-data\">\n                        <div class=\"col-sm-12 table-body\">\n                          <div class=\"row\">\n                            <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                              <ul class=\"list-unstyled structure-list\" *ngFor=\"let fields of structureFields\">\n\n                                <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                                  <div>\n\n                                    <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"fields.isChecked\"\n                                      [value]=\"fields.name\"\n                                      (ngModelChange)=\"getStructureSubFields(fields.name,fields.isChecked )\">\n                                      {{fields.name}}\n                                    </mat-checkbox>\n                                  </div>\n\n                                </li>\n\n                              </ul>\n                            </div>\n\n                            <div class=\"col-sm-1 text-center arrow-position\">\n                              <i class=\"material-icons arrow-align-one\">\n                                keyboard_arrow_right\n                              </i>\n                              <i class=\"material-icons arrow-align-two\">\n                                keyboard_arrow_right\n                              </i>\n                            </div>\n                            <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                              <div *ngIf=\"StructureValues\">\n                                <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of StructureValues\">\n\n                                  <li class=\"list-items-values\">\n                                    <mat-checkbox class=\"checkbox-success\"\n                                      (change)=\"subFieldsStructure($event,values.name)\" [value]=\"values.name\">\n                                      {{values.name}}\n                                    </mat-checkbox>\n\n                                  </li>\n\n                                </ul>\n\n                              </div>\n                              <div *ngIf=\"customValues\">\n                                <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of customValues\">\n\n                                  <li class=\"list-items-values\">\n                                    <mat-checkbox class=\"checkbox-success\"\n                                      (change)=\"subFieldsStructure($event,values.name)\" [value]=\"values.name\">\n                                      {{values.name}}\n                                    </mat-checkbox>\n                                  </li>\n\n                                </ul>\n\n                              </div>\n                            </div>\n\n                          </div>\n                        </div>\n\n\n                      </div>\n\n                    </div>\n                  </div>\n                </div>\n                <div class=\"employees-table\">\n                  <div>\n                    <h4 style=\"float: left;\">Include a Specific Person</h4>\n                    <h4 style=\"float: right;\">Exclude a Specific Person</h4>\n                    <div class=\"clearfix\"></div>\n                    <!-- <ng-multiselect-dropdown [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                      [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                      (onDeSelect)=\"OnItemDeSelect($event)\" (onSelectAll)=\"onSelectAll($event)\"\n                      (onDeSelectAll)=\"onDeSelectAll($event)\">\n                    </ng-multiselect-dropdown> -->\n\n                    <dual-list [source]=\"dropdownList\" [(destination)]=\"selectedUsers\" [format]=\"format\" filter=true\n                      height='300px'>\n                    </dual-list>\n                  </div>\n\n                </div>\n\n\n              </div>\n            </div>\n\n          </div>\n          <div class=\"buttons\">\n            <ul class=\"list-unstyled\">\n              <li class=\"list-items\">\n                <a class=\"back\" (click)=\"backModulesandFields('chooseFields')\">Back</a>\n              </li>\n              <li class=\"list-items\">\n                <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n              </li>\n              <li class=\"list-items pull-right\">\n                <button class='save' type=\"submit\" (click)=\"postEmpData()\">\n                  Save Changes\n                </button>\n              </li>\n\n              <div class=\"clearfix\"></div>\n            </ul>\n          </div>\n        </div>\n        <div class=\"tab-pane\" [ngClass]=\"{'active':selectedNav === 'summaryRecap','in':selectedNav==='summaryRecap'}\"\n          id=\"summaryRecap\">\n          <div class=\"recap-tab\">\n            <!-- ===============================================first tab============================================================ -->\n\n            <div class=\"field-accordion\">\n\n              <div class=\"panel-group\" id=\"recap\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#recap\" href=\"#define-user\">\n                        Define User Type\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"define-user\" class=\"panel-collapse collapse in\">\n                    <div class=\"panel-body\">\n                      <div class=\"user-type col-md-4\">\n                        <div class=\"form-group\">\n                          <label>User Type Level</label>\n                          <mat-select class=\"form-control text-field select-btn\" [(ngModel)]=\"userRoleData.name\"\n                            (ngModelChange)=\"roleChange(userRoleData.name)\" placeholder=\"Select user role\" name=\"name\"\n                            #name=\"ngModel\" required [disabled]=true>\n                            <mat-option *ngFor=\"let data of resRolesObj.data\" [value]=\"data.name\"> {{data.name}}\n                            </mat-option>\n\n                            <!-- <mat-option value=\"Job2\">Job2</mat-option> -->\n                          </mat-select>\n                          <p *ngIf=\"!userRoleData.name && name.touched || (!userRoleData.name && isValid)\"\n                            class=\"error\">Please fill\n                            this field</p>\n                        </div>\n                        <div class=\"form-group\">\n                          <label>Name Of The custom Type Role</label>\n                          <input type=\"text\" placeholder=\"Role Type\" [(ngModel)]=\"userRoleData.roletype\"\n                            class=\"form-control company-legal-name\" name=\"roletype\" #roletype=\"ngModel\" required\n                            readonly>\n                          <p *ngIf=\"!userRoleData.roletype && roletype.touched || (!userRoleData.roletype && isValid)\"\n                            class=\"error\">Please fill this field</p>\n                        </div>\n\n                      </div>\n                      <div *ngIf=\"userRoleData.name == 'System Admin'\" class=\"system-admin col-xs-5\">\n                        <mat-checkbox class=\"zenwork-customized-checkbox\">\n                          Allow System Administrator to also be designated as HR Contact\n                        </mat-checkbox>\n                      </div>\n                      <div class=\"col-xs-12\"></div>\n                      <div class=\"form-group col-xs-5\">\n                        <label>Description</label>\n                        <textarea class=\"form-control rounded-0\" [(ngModel)]=\"userRoleData.description\" rows=\"4\"\n                          name=\"description\" #description=\"ngModel\" required readonly></textarea>\n                        <p *ngIf=\"!userRoleData.description && description.touched || (!userRoleData.description && isValid)\"\n                          class=\"error\">Please fill this field</p>\n\n                      </div>\n\n                      <div class=\"clearfix\"></div>\n\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"panel-group\" id=\"recap\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#recap\" href=\"#modules\">\n                        Modules & Fields\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"modules\" class=\"panel-collapse collapse\">\n                    <div class=\"panel-body\">\n                      <!-- =======================================secondtab ==============================================-->\n\n                      <div class=\"col-xs-12 \">\n                        <div class=\"row\">\n                          <div class=\"col-md-4\">\n                            <h4>Select the modules</h4>\n                            <div class=\"form-group\">\n                              <input type=\"text\" placeholder=\"Role Name\" [(ngModel)]=\"userRoleData.roletype\"\n                                class=\"form-control company-legal-name\" name=\"roletype\" #roletype=\"ngModel\" readonly\n                                required readonly>\n                              <p *ngIf=\"!userRoleData.roletype && roletype.touched || (!userRoleData.roletype && isValid)\"\n                                class=\"error\">Please fill this field</p>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"col-md-12 module-table-padding\">\n                        <div class=\"row\">\n                          <div class=\"col-md-6 module-table\">\n                            <table>\n                              <thead>\n                                <th>Page Name</th>\n                                <th>Custom {{userRoleData.name}}</th>\n                              </thead>\n                              <tbody>\n                                <tr *ngFor=\"let data of pagesAllObj.data\">\n                                  <td>{{data.name}}</td>\n                                  <td>\n                                    <mat-select *ngIf=\"userRoleData.baseRoleId == '5cbe98f8561562212689f748'\"\n                                      class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                                      placeholder=\"Select access level\" [(ngModel)]=\"data.hr\">\n                                      <mat-option value=\"view\">View Only</mat-option>\n                                      <mat-option value=\"full\">Full Access</mat-option>\n                                      <mat-option value=\"no\">No access</mat-option>\n                                    </mat-select>\n                                    <mat-select *ngIf=\"userRoleData.baseRoleId == '5cbe98a3561562212689f747'\"\n                                      class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                                      placeholder=\"Select access level\" [(ngModel)]=\"data.manager\">\n                                      <mat-option value=\"view\">View Only</mat-option>\n                                      <mat-option value=\"full\">Full Access</mat-option>\n                                      <mat-option value=\"no\">No access</mat-option>\n                                    </mat-select>\n                                    <mat-select *ngIf=\"userRoleData.baseRoleId == '5cbe990e561562212689f749'\"\n                                      class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                                      placeholder=\"Select access level\" [(ngModel)]=\"data.employee\">\n                                      <mat-option value=\"view\">View Only</mat-option>\n                                      <mat-option value=\"full\">Full Access</mat-option>\n                                      <mat-option value=\"no\">No access</mat-option>\n                                    </mat-select>\n                                    <mat-select *ngIf=\"userRoleData.baseRoleId == '5cbe9922561562212689f74a'\"\n                                      class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                                      placeholder=\"Select access level\" [(ngModel)]=\"data.sysadmin\">\n                                      <mat-option value=\"view\">View Only</mat-option>\n                                      <mat-option value=\"full\">Full Access</mat-option>\n                                      <mat-option value=\"no\">No access</mat-option>\n                                    </mat-select>\n                                  </td>\n                                </tr>\n\n                              </tbody>\n                            </table>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"myinfo-fields-controll\">\n                        <div class=\"field-accordion\">\n\n                          <div class=\"panel-group\" id=\"accordion1\">\n\n                            <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                              <div class=\"panel-heading\">\n                                <h4 class=\"panel-title\">\n                                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\"\n                                    href=\"#personal\" (click)=\"getMyInfoTabsData('Personal')\">\n                                    Personal Access Level\n                                  </a>\n                                </h4>\n                              </div>\n                              <div id=\"personal-info\" class=\"panel-collapse collapse in\">\n                                <div class=\"panel-body\">\n                                  <!-- personaltable -->\n\n                                  <div class=\"person-tables\">\n\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Field Name\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>No Access\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>View Only\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Requires Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Without Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a>Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div>\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr *ngFor=\"let data of personalFields\">\n                                          <td class=\"zenwork-checked-border\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>\n                                              {{data.field_name}}\n                                            </mat-checkbox>\n\n\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.no_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.full_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.not_req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n                                          <td>\n\n                                          </td>\n\n                                        </tr>\n\n                                      </tbody>\n                                    </table>\n\n\n                                  </div>\n                                  <!-- personaltable ends -->\n                                </div>\n                              </div>\n                            </div>\n                          </div>\n                          <div class=\"panel-group\" id=\"accordion1\">\n\n                            <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                              <div class=\"panel-heading\">\n                                <h4 class=\"panel-title\">\n                                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\"\n                                    href=\"#job\" (click)=\"getMyInfoTabsData('Job')\">\n                                    Job Access Level\n                                  </a>\n                                </h4>\n                              </div>\n                              <div id=\"job\" class=\"panel-collapse collapse in\">\n                                <div class=\"panel-body\">\n                                  <!-- personaltable -->\n\n                                  <div class=\"person-tables\">\n\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Field Name\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>No Access\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>View Only\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Requires Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Without Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a>Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div>\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr *ngFor=\"let data of JobFields\">\n                                          <td class=\"zenwork-checked-border\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>\n                                              {{data.field_name}}\n                                            </mat-checkbox>\n\n\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.no_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.full_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.not_req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n                                          <td>\n\n                                          </td>\n\n                                        </tr>\n\n                                      </tbody>\n                                    </table>\n\n\n                                  </div>\n                                  <!-- personaltable ends -->\n                                </div>\n                              </div>\n                            </div>\n                          </div>\n                          <div class=\"panel-group\" id=\"accordion1\">\n\n                            <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                              <div class=\"panel-heading\">\n                                <h4 class=\"panel-title\">\n                                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\"\n                                    href=\"#emergency\" (click)=\"getMyInfoTabsData('Emergency_Contact')\">\n                                    Emergency Contact Access Level\n                                  </a>\n                                </h4>\n                              </div>\n                              <div id=\"emergency\" class=\"panel-collapse collapse in\">\n                                <div class=\"panel-body\">\n                                  <!-- personaltable -->\n\n                                  <div class=\"person-tables\">\n\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Field Name\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>No Access\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>View Only\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Requires Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Without Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a>Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div>\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr *ngFor=\"let data of emergency\">\n                                          <td class=\"zenwork-checked-border\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>\n                                              {{data.field_name}}\n                                            </mat-checkbox>\n\n\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.no_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.full_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.not_req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n                                          <td>\n\n                                          </td>\n\n                                        </tr>\n\n                                      </tbody>\n                                    </table>\n\n\n                                  </div>\n                                  <!-- personaltable ends -->\n                                </div>\n                              </div>\n                            </div>\n                          </div>\n                          <div class=\"panel-group\" id=\"accordion1\">\n\n                            <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                              <div class=\"panel-heading\">\n                                <h4 class=\"panel-title\">\n                                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion1\"\n                                    href=\"#compensation\" (click)=\"getMyInfoTabsData('Compensation')\">\n                                    Compensation Access Level\n                                  </a>\n                                </h4>\n                              </div>\n                              <div id=\"compensation\" class=\"panel-collapse collapse in\">\n                                <div class=\"panel-body\">\n                                  <!-- personaltable -->\n\n                                  <div class=\"person-tables\">\n\n                                    <table class=\"table\">\n                                      <thead>\n                                        <tr>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Field Name\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>No Access\n                                            </mat-checkbox>\n                                          </th>\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>View Only\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Requires Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>Change\n                                              Without Approval\n                                            </mat-checkbox>\n                                          </th>\n\n                                          <th>\n                                            <div class=\"setting-drop\">\n                                              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                                                <i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i>\n                                              </a>\n                                              <ul class=\"dropdown-menu\">\n\n                                                <li>\n                                                  <a>Edit</a>\n                                                </li>\n\n                                              </ul>\n                                            </div>\n                                          </th>\n                                        </tr>\n                                      </thead>\n                                      <tbody>\n\n                                        <tr *ngFor=\"let data of compensationFields\">\n                                          <td class=\"zenwork-checked-border\">\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\" [disabled]=true>\n                                              {{data.field_name}}\n                                            </mat-checkbox>\n\n\n                                          </td>\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.no_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.full_access\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n\n                                          <td>\n                                            <mat-checkbox class=\"zenwork-customized-checkbox\"\n                                              [(ngModel)]=\"data.not_req_approval\" [disabled]=true>\n                                            </mat-checkbox>\n\n                                          </td>\n                                          <td>\n\n                                          </td>\n\n                                        </tr>\n\n                                      </tbody>\n                                    </table>\n\n\n                                  </div>\n                                  <!-- personaltable ends -->\n                                </div>\n                              </div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"panel-group\" id=\"recap\">\n\n                <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"panel-title\">\n                      <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#recap\" href=\"#employees\">\n                        Employees\n                      </a>\n                    </h4>\n                  </div>\n                  <div id=\"employees\" class=\"panel-collapse collapse\">\n                    <div class=\"panel-body\">\n\n                      <div class=\"employees-data\">\n                        <div class=\"field-accordion\">\n                          <div class=\"panel-group\" id=\"accordion2\">\n\n                            <div class=\"panel panel-default panel-border-remove\" style=\"border-bottom:none;\">\n                              <div class=\"panel-heading\">\n                                <h4 class=\"panel-title heading-eligibility\">\n                                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\"\n                                    href=\"#eligibility-info\">\n                                    Eligibility Criteria\n                                  </a>\n                                </h4>\n                              </div>\n                              <div id=\"eligibility-info\" class=\"panel-collapse collapse in\">\n                                <div class=\"panel-body eligibilty-border-bottom\">\n\n                                  <div class=\"employees-data\">\n                                    <div class=\"col-sm-12 table-body\">\n                                      <div class=\"row\">\n                                        <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                                          <ul class=\"list-unstyled structure-list\"\n                                            *ngFor=\"let fields of structureFields\">\n\n                                            <li class=\"list-items\"\n                                              [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                                              <div>\n\n                                                <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"fields.isChecked\"\n                                                  [value]=\"fields.name\"\n                                                  (ngModelChange)=\"getStructureSubFields(fields.name,fields.isChecked )\"\n                                                  [disabled]=true>\n                                                  {{fields.name}}\n                                                </mat-checkbox>\n                                              </div>\n\n                                            </li>\n\n                                          </ul>\n                                        </div>\n\n                                        <div class=\"col-sm-1 text-center arrow-position\">\n                                          <i class=\"material-icons arrow-align-one\">\n                                            keyboard_arrow_right\n                                          </i>\n                                          <i class=\"material-icons arrow-align-two\">\n                                            keyboard_arrow_right\n                                          </i>\n                                        </div>\n                                        <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                                          <div *ngIf=\"StructureValues\">\n                                            <ul class=\"list-unstyled structure-list\"\n                                              *ngFor=\"let values of StructureValues\">\n\n                                              <li class=\"list-items-values\">\n                                                <mat-checkbox class=\"checkbox-success\"\n                                                  (change)=\"subFieldsStructure($event,values.name)\"\n                                                  [value]=\"values.name\" [disabled]=true>\n                                                  {{values.name}}\n                                                </mat-checkbox>\n\n                                              </li>\n\n                                            </ul>\n\n                                          </div>\n                                          <div *ngIf=\"customValues\">\n                                            <ul class=\"list-unstyled structure-list\"\n                                              *ngFor=\"let values of customValues\">\n\n                                              <li class=\"list-items-values\">\n                                                <mat-checkbox class=\"checkbox-success\"\n                                                  (change)=\"subFieldsStructure($event,values.name)\"\n                                                  [value]=\"values.name\" [disabled]=true>\n                                                  {{values.name}}\n                                                </mat-checkbox>\n                                              </li>\n\n                                            </ul>\n\n                                          </div>\n                                        </div>\n\n                                      </div>\n                                    </div>\n\n\n                                  </div>\n\n                                </div>\n                              </div>\n                            </div>\n                            <div class=\"employees-table\">\n                              <div class=\"col-xs-12\">\n                                <dual-list [source]=\"dropdownList\" [(destination)]=\"selectedUsers\" [format]=\"format\"\n                                  filter=true height='300px'>\n                                </dual-list>\n                              </div>\n                            </div>\n\n\n                          </div>\n                        </div>\n\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n              </div>\n\n            </div>\n            <div class=\"buttons\">\n              <ul class=\"list-unstyled\">\n                <li class=\"list-items\">\n                  <a class=\"back\" (click)=\"backModulesandFields('chooseEmployees')\">Back</a>\n                </li>\n                <li class=\"list-items\">\n                  <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n                </li>\n                <li class=\"list-items pull-right\">\n                  <button class='save' type=\"submit\" (click)=\"closeRecapPopup()\">\n                    Save Changes\n                  </button>\n                </li>\n\n                <div class=\"clearfix\"></div>\n              </ul>\n            </div>\n\n\n            <!-- ====================================third tab ========================================-->\n\n\n          </div>\n        </div>\n      </div>\n    </div>\n\n\n  </div>\n\n\n\n\n\n</div>\n<!-- <div class=\"clearfix\"></div> -->"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.ts":
/*!*************************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.ts ***!
  \*************************************************************************************************************************************************/
/*! exports provided: CreateCustomUserRoleModelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCustomUserRoleModelComponent", function() { return CreateCustomUserRoleModelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/employee.service */ "./src/app/services/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};








var CreateCustomUserRoleModelComponent = /** @class */ (function () {
    function CreateCustomUserRoleModelComponent(dialogRef, data, employeeService, siteAccessRolesService, companySettingsService, myInfoService, swalAlertService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.employeeService = employeeService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.companySettingsService = companySettingsService;
        this.myInfoService = myInfoService;
        this.swalAlertService = swalAlertService;
        this.chooseFields = false;
        this.chooseSteps = false;
        this.chooseEmployees = false;
        this.summaryRecap = false;
        this.isValid = false;
        this.editCustomRole = false;
        this.StructureValues = [];
        this.customValues = [];
        this.structureFields = [
            {
                name: 'Business Unit',
                isChecked: true
            }
        ];
        this.resRolesObj = {
            "data": []
        };
        this.pagesAllObj = {
            "data": []
        };
        this.userRoleData = {
            baseRoleId: '',
            name: "",
            description: "",
            access: "",
            roletype: 1,
            companyId: '',
            createdBy: '',
            updatedBy: '',
        };
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.fields = [];
        this.result = [];
        this.personalFields = [];
        this.JobFields = [];
        this.emergency = [];
        this.compensationFields = [];
        this.eligibilityArray = [];
        this.selectedUsers = [];
        this.format = {
            add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
            direction: angular_dual_listbox__WEBPACK_IMPORTED_MODULE_6__["DualListComponent"].LTR, draggable: true, locale: 'da',
        };
    }
    CreateCustomUserRoleModelComponent.prototype.ngOnInit = function () {
        this.selectedNav = 'chooseSteps';
        console.log("data from other component", this.data);
        if (this.data.data != 'CreateCustomUserRole') {
            this.editCustomRole = (this.data.data != 'CreateCustomUserRole');
            console.log(this.editCustomRole, "edit");
            this.roleName = this.data.data.baseRoleId.name;
            this.allPagesData();
            this.getAllEmp();
            this.getAllsingleRole(this.data.data);
            this.getAllpagesForUpdate(this.data.data);
            this.getAllMyInfoTabFieldsUpdate(this.data.data);
            this.getEmployeesUnderRole(this.data.data);
        }
        if (this.editCustomRole == false) {
            this.allPagesData();
            this.getMyInfoTabsData('Personal');
            this.getMyInfoTabsData('Job');
            this.getMyInfoTabsData('Emergency_Contact');
            this.getMyInfoTabsData('Compensation');
        }
        this.getFieldsForRoles();
        this.getStructureSubFields(this.structureFields[0].name, this.structureFields[0].isChecked);
        this.getBaseRoles();
        // this.getMyInfoTabsData('Job')
        // this.getMyInfoTabsData('Emergency_Contact')
        // this.getMyInfoTabsData('Compensation')
        //npm multi select dropdown sample start
        this.dropdownList = [
        // { item_id: 1, item_text: 'Mumbai' },
        // { item_id: 2, item_text: 'Bangaluru' },
        // { item_id: 3, item_text: 'Pune' },
        // { item_id: 4, item_text: 'Navsari' },
        // { item_id: 5, item_text: 'New Delhi' },
        // { item_id: 6, item_text: 'New Delhi' },
        // { item_id: 7, item_text: 'New Delhi' },
        // { item_id: 8, item_text: 'New Delhi' },
        // { item_id: 9, item_text: 'New Delhi' },
        ];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 0,
            maxHeight: 150,
            defaultOpen: true,
            allowSearchFilter: true,
        };
    };
    CreateCustomUserRoleModelComponent.prototype.onItemSelect = function (item) {
        this.selectedUsers = [];
        console.log(item);
        console.log(this.selectedItems);
        for (var i = 0; i < this.selectedItems.length; i++) {
            this.selectedUsers.push(this.selectedItems[i].item_id);
        }
        console.log('selcetd users', this.selectedUsers);
    };
    CreateCustomUserRoleModelComponent.prototype.OnItemDeSelect = function (item) {
        console.log(item);
        console.log(this.selectedItems);
        // for (var i = 0; i < this.selectedUsers.length; i++) {
        //   if(item.item_id == this.selectedUsers[i])
        //   delete this.selectedUsers[i].item_id;
        // }
        this.selectedUsers = this.selectedUsers.filter(function (user) { return user != item.item_id; });
        console.log('selcetd users', this.selectedUsers);
    };
    CreateCustomUserRoleModelComponent.prototype.onSelectAll = function (items) {
        console.log(items);
        this.selectedUsers = [];
        for (var i = 0; i < items.length; i++) {
            this.selectedUsers.push(items[i].item_id);
        }
        console.log("users", this.selectedUsers);
    };
    CreateCustomUserRoleModelComponent.prototype.onDeSelectAll = function (items) {
        console.log(items);
        this.selectedUsers = [];
        console.log('selcetd users', this.selectedUsers);
    };
    /* Description: getting all pages data
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getAllsingleRole = function (data) {
        var _this = this;
        console.log(data);
        this.siteAccessRolesService.getSingleCustomRole(data._id)
            .subscribe(function (res) {
            console.log("single role", res);
            _this.userRoleData = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    CreateCustomUserRoleModelComponent.prototype.getAllEmp = function () {
        var _this = this;
        var listallUsers;
        var postData = {
            name: ''
        };
        console.log("data comes");
        this.siteAccessRolesService.getAllEmployees(postData)
            .subscribe(function (res) {
            console.log("resonse for all employees", res);
            listallUsers = res.data;
            for (var i = 0; i < listallUsers.length; i++) {
                _this.dropdownList.push({ _id: listallUsers[i]._id, _name: listallUsers[i].personal.name.preferredName });
                console.log(_this.selectedUsers, _this.dropdownList, "selectedUsers");
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: update all pages data
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getEmployeesUnderRole = function (data) {
        var _this = this;
        console.log(data, "emplyes");
        var listUsers = [];
        this.siteAccessRolesService.getAllEmployeesforRoles(data.companyId, data._id)
            .subscribe(function (res) {
            console.log("resonse for after edit pages", res);
            listUsers = res.data;
            _this.getEmpNames(listUsers);
        }, function (err) {
            console.log(err);
        });
    };
    CreateCustomUserRoleModelComponent.prototype.getEmpNames = function (empIds) {
        var _this = this;
        var postData = {
            "_ids": empIds
        };
        this.employeeService.getOnboardedEmployeeDetails(postData)
            .subscribe(function (res) {
            console.log("get ROle of employees", res);
            _this.selectedEmployees = res.data;
            for (var i = 0; i < _this.selectedEmployees.length; i++) {
                _this.selectedUsers.push({ _id: _this.selectedEmployees[i]._id, _name: _this.selectedEmployees[i].personal.name.preferredName });
                console.log(_this.selectedUsers, _this.dropdownList, "selectedUsers");
            }
            // this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    CreateCustomUserRoleModelComponent.prototype.getAllpagesForUpdate = function (data) {
        var _this = this;
        console.log(data.companyId, data._id, 'comanyidroleid,');
        this.siteAccessRolesService.getAllPagesForCompany(data.companyId, data._id)
            .subscribe(function (res) {
            console.log("resonse for after edit pages", res);
            _this.pagesAllObj = res;
            // this.tempPagesControlls
            for (var i = 0; i < _this.pagesAllObj.data.length; i++) {
                _this.pagesAllObj.data[i]['baseEmployee'] = _this.tempPagesControlls.data[i].employee;
                _this.pagesAllObj.data[i]['baseHr'] = _this.tempPagesControlls.data[i].hr;
                _this.pagesAllObj.data[i]['baseManager'] = _this.tempPagesControlls.data[i].manager;
                _this.pagesAllObj.data[i]['baseSysAdmin'] = _this.tempPagesControlls.data[i].sysadmin;
            }
            console.log(_this.pagesAllObj, "new data pages ACLs");
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: update all pages data
  author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getAllMyInfoTabFieldsUpdate = function (data) {
        var _this = this;
        this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Personal')
            .subscribe(function (res) {
            console.log("myinfo field data for personal", res);
            _this.personalFields = res.data;
            console.log("viiiiiiiiiiii perosnal", _this.personalFields);
            if (res.data.length == 0) {
                _this.getMyInfoTabsData('Personal');
            }
        }, function (err) {
            console.log(err);
        });
        this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Job')
            .subscribe(function (res) {
            console.log("myinfo field data for Job", res);
            _this.JobFields = res.data;
            if (res.data.length == 0) {
                _this.getMyInfoTabsData('Job');
            }
            console.log("viiiiiiiiiiii Job", _this.JobFields);
        }, function (err) {
            console.log(err);
        });
        this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Emergency_Contact')
            .subscribe(function (res) {
            console.log("myinfo field data for Emergency_Contact", res);
            _this.emergency = res.data;
            console.log("viiiiiiiiiiii Emergency_Contact", _this.emergency);
            if (res.data.length == 0) {
                _this.getMyInfoTabsData('Emergency_Contact');
            }
        }, function (err) {
            console.log(err);
        });
        this.siteAccessRolesService.getAlltabsData(data.companyId, data._id, 'Compensation')
            .subscribe(function (res) {
            console.log("myinfo field data for Compensation", res);
            _this.compensationFields = res.data;
            console.log("viiiiiiiiiiii Compensation", _this.compensationFields);
            if (res.data.length == 0) {
                _this.getMyInfoTabsData('Compensation');
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: getting my-info tabs key names from companies serivce
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getMyInfoTabsData = function (data) {
        var _this = this;
        this.fields = [];
        this.result = [];
        console.log(this.fields, "fields data");
        this.myInfoService.basicDetailsGetAll(data)
            .subscribe(function (res) {
            console.log("myinfo field data", res);
            console.log(res.data.fields);
            _this.fields = [];
            for (var k in res.data.fields)
                _this.fields.push({ "field_name": k });
            console.log(_this.fields);
            _this.result = _this.fields.map(function (el) {
                var o = Object.assign({}, el);
                o.full_access = false;
                o.no_access = false;
                o.not_req_approval = false;
                o.req_approval = false;
                return o;
            });
            console.log("viiiiii", _this.result);
            if (data == 'Personal') {
                _this.personalFields = _this.result;
                console.log('111111', _this.personalFields);
            }
            if (data == 'Job') {
                _this.JobFields = _this.result;
                console.log('2222222', _this.JobFields);
            }
            if (data == 'Emergency_Contact') {
                _this.emergency = _this.result;
                console.log('2222222', _this.emergency);
            }
            if (data == 'Compensation') {
                _this.compensationFields = _this.result;
                console.log('2222222', _this.compensationFields);
            }
            // this.personalfields = res.data.fields.keys
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: add popup/side nav tabs active tabI
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.chooseFieldss = function (event) {
        this.selectedNav = event;
        if (event == "chooseFields") {
            this.chooseFields = true;
            this.chooseSteps = false;
            this.chooseEmployees = false;
            this.summaryRecap = false;
        }
        else if (event == 'chooseSteps') {
            this.chooseFields = false;
            this.chooseSteps = true;
            this.chooseEmployees = false;
            this.summaryRecap = false;
        }
        else if (event == 'chooseEmployees') {
            this.chooseFields = false;
            this.chooseSteps = false;
            this.chooseEmployees = true;
            this.summaryRecap = false;
        }
        else if (event == 'summaryRecap') {
            this.chooseFields = false;
            this.chooseSteps = false;
            this.chooseEmployees = false;
            this.summaryRecap = true;
        }
    };
    /* Description: getBaseRoles
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getBaseRoles = function () {
        var _this = this;
        this.siteAccessRolesService.getBaseRoles()
            .subscribe(function (res) {
            console.log("roles", res);
            _this.resRolesObj = res;
        }, function (err) {
        });
    };
    /* Description: roleChange for API
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.roleChange = function (access) {
        console.log(access, this.resRolesObj, "121212");
        for (var i = 0; i < this.resRolesObj.data.length; i++) {
            if (access == this.resRolesObj.data[i]._id) {
                this.userRoleData.access = this.resRolesObj.data[i].access;
                this.roleName = this.resRolesObj.data[i].name;
            }
        }
        console.log("access", this.userRoleData.access, this.roleName);
    };
    /* Description: create user role for (Add popup first popup)
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.createUserRole = function () {
        var _this = this;
        this.isValid = true;
        if (this.userRoleData.name && this.userRoleData.roletype && this.userRoleData.description) {
            var cID = localStorage.getItem('companyId');
            console.log("cid", cID);
            cID = cID.replace(/^"|"$/g, "");
            console.log("cin2", cID);
            this.userRoleData.companyId = cID;
            this.userRoleData.createdBy = cID;
            this.userRoleData.updatedBy = cID;
            var postData = this.userRoleData;
            console.log(postData);
            // delete this.roleName;
            this.siteAccessRolesService.createRoleType(postData)
                .subscribe(function (res) {
                console.log("userrole", res);
                if (res.status == true) {
                    // this.roleId = res.data._id
                    _this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", res.message, "success");
                    _this.selectedNav = 'chooseFields';
                    _this.roleId = res.role._id;
                    console.log(_this.roleId, "11111111111111111111111111111111111111");
                    // this.getAllMyInfoTabFields()
                }
                else {
                    _this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", res.message, "error");
                }
            }, function (err) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", err.error.message, "error");
            });
        }
    };
    /* Description: update single user role
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.UpdateUserRole = function () {
        var _this = this;
        this.siteAccessRolesService.UpdateRoleType(this.userRoleData)
            .subscribe(function (res) {
            console.log("update custom role res", res);
            if (res.status == true) {
                _this.swalAlertService.SweetAlertWithoutConfirmation("User Role Type", res.message, "success");
                _this.selectedNav = 'chooseFields';
            }
        }, function (err) {
            console.log(err);
        });
    };
    CreateCustomUserRoleModelComponent.prototype.getAllMyInfoTabFields = function () {
        var _this = this;
        var cID = localStorage.getItem('companyId');
        console.log("cid", cID);
        cID = cID.replace(/^"|"$/g, "");
        console.log("cin2", cID);
        this.siteAccessRolesService.getAllMyinfoFields(this.roleId, cID, 'personal')
            .subscribe(function (res) {
            console.log("personal res", res);
            if (res.data.length == 0) {
                // this.getMyInfoTabsData('Personal')
            }
            else {
                _this.personalFields = res.data;
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: getting all pages data
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.allPagesData = function () {
        var _this = this;
        var companyId = JSON.parse(localStorage.getItem('companyId'));
        this.siteAccessRolesService.allPagesData(companyId)
            .subscribe(function (res) {
            console.log("respages", res);
            _this.pagesAllObj = res;
            _this.tempPagesControlls = _this.pagesAllObj;
            console.log(_this.tempPagesControlls, _this.pagesAllObj);
        }, function (err) {
            console.log("err");
        });
    };
    /* Description: first time creating pages (ACLS) and perosnal,job,emergency,compensation fields
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postShowFields = function () {
        console.log("create", this.pagesAllObj);
        // for (var i = 0; i < this.resRolesObj.data.length; i++) {
        //   if (this.userRoleData.name == this.resRolesObj.data[i].name) {
        //     console.log("121212", this.userRoleData.name, this.resRolesObj.data[i].name);
        //     var catname = this.resRolesObj.data[i].name;
        //     console.log("catname", catname);
        //   }
        // }
        if (this.userRoleData.name == 'HR' || this.roleName == 'HR') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.employee;
                delete item.manager;
                delete item.sysadmin;
                delete item.status;
                delete item._id;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                delete item.updatedBy;
                return item;
            });
        }
        if (this.userRoleData.name == 'Manager' || this.roleName == 'Manager') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.employee;
                delete item.hr;
                delete item.sysadmin;
                delete item.status;
                delete item._id;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                delete item.updatedBy;
                return item;
            });
        }
        if (this.userRoleData.name == 'Employee' || this.roleName == 'Employee') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.hr;
                delete item.manager;
                delete item.sysadmin;
                delete item.status;
                delete item._id;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                delete item.updatedBy;
                return item;
            });
        }
        if (this.userRoleData.name == 'System Admin' || this.roleName == 'System Admin') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.employee;
                delete item.manager;
                delete item.hr;
                delete item.status;
                delete item._id;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                delete item.updatedBy;
                return item;
            });
        }
        console.log("postdata of modules", this.pagesAllObj.data);
        var postData = {
            company_id: this.userRoleData.companyId,
            role_id: this.roleId,
            pages: this.pagesAllObj.data
        };
        postData['page_type'] = 'derived';
        console.log(postData, "vipin");
        this.pagesCreation(postData);
        var postData1 = {
            company_id: this.userRoleData.companyId,
            role_id: this.roleId,
            category: 'Personal',
            fields: this.personalFields
        };
        this.postPersonalFields(postData1);
        var postdata2 = {
            company_id: this.userRoleData.companyId,
            role_id: this.roleId,
            category: 'Job',
            fields: this.JobFields
        };
        this.postJobFields(postdata2);
        var postdata3 = {
            company_id: this.userRoleData.companyId,
            role_id: this.roleId,
            category: 'Emergency_Contact',
            fields: this.emergency
        };
        this.postEmergencyFields(postdata3);
        var postdata4 = {
            company_id: this.userRoleData.companyId,
            role_id: this.roleId,
            category: 'Compensation',
            fields: this.compensationFields
        };
        this.postCompensationFields(postdata4);
    };
    /* Description: sending updated pages(ACL) when custom role created
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postUpdateShowFields = function () {
        console.log("123456789", this.pagesAllObj);
        if (this.userRoleData.name == 'HR') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.employee;
                delete item.manager;
                delete item.roleId;
                delete item.sysadmin;
                delete item.status;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                return item;
            });
        }
        if (this.userRoleData.name == 'Manager') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.employee;
                delete item.hr;
                delete item.roleId;
                delete item.sysadmin;
                delete item.status;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                return item;
            });
        }
        if (this.userRoleData.name == 'Employee') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.hr;
                delete item.manager;
                delete item.roleId;
                delete item.sysadmin;
                delete item.status;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                return item;
            });
        }
        if (this.userRoleData.name == 'System Admin') {
            this.pagesAllObj.data.map(function (item) {
                delete item.pageType;
                delete item.employee;
                delete item.roleId;
                delete item.manager;
                delete item.hr;
                delete item.status;
                delete item.companyId;
                delete item.createdAt;
                delete item.updatedAt;
                delete item.pageid;
                return item;
            });
        }
        console.log(this.pagesAllObj.data, "post pages data");
        this.pagesUpdation(this.pagesAllObj.data);
        this.updatePersonalFields(this.personalFields);
        this.updateJobFields(this.JobFields);
        this.updateEmergencyFields(this.emergency);
        this.updateCompensationFields(this.compensationFields);
    };
    /* Description: update pages fields post
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.pagesUpdation = function (data) {
        var _this = this;
        this.siteAccessRolesService.pagesUpdate(data)
            .subscribe(function (res) {
            console.log("reges aftyer update", res);
            _this.selectedNav = 'chooseEmployees';
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time update personal fields post
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.updatePersonalFields = function (personalFields) {
        console.log(personalFields, "personalFields");
        this.siteAccessRolesService.personalFieldsUpdate(personalFields)
            .subscribe(function (res) {
            console.log("update res of personal", res);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time update job fields post
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.updateJobFields = function (jobfields) {
        console.log("jobfields", jobfields);
        this.siteAccessRolesService.jobFieldsUpdate(jobfields)
            .subscribe(function (res) {
            console.log("update res of job", res);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time update Emergency fields post
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.updateEmergencyFields = function (emergencyfields) {
        console.log("jobfields", emergencyfields);
        this.siteAccessRolesService.emergencyFieldsUpdate(emergencyfields)
            .subscribe(function (res) {
            console.log("update res of emergency", res);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: update compensation fields
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.updateCompensationFields = function (compensationfields) {
        this.siteAccessRolesService.compensationFieldsUpdate(compensationfields)
            .subscribe(function (res) {
            console.log("update res of compensation", res);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time create Pages first time cretaion fields post
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.pagesCreation = function (postData) {
        var _this = this;
        this.siteAccessRolesService.pagesCreation(postData)
            .subscribe(function (res) {
            console.log(res, 'pages creation');
            if (res.status == true) {
                _this.selectedNav = 'chooseEmployees';
            }
        }, function (err) {
            console.log(err);
        });
        console.log(postData);
    };
    /* Description: first time create personal fields post
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postPersonalFields = function (personalFields) {
        var _this = this;
        console.log('personal fields post', personalFields);
        this.siteAccessRolesService.postpersonalfields(personalFields)
            .subscribe(function (res) {
            console.log(res, 'personal fields create');
            if (res.status == true) {
                _this.selectedNav = 'chooseEmployees';
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time create job fields post
   author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postJobFields = function (jobfields) {
        console.log('personal fields post', jobfields);
        this.siteAccessRolesService.postpersonalfields(jobfields)
            .subscribe(function (res) {
            console.log(res, name);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time create Emergency fields post
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postEmergencyFields = function (emergency) {
        console.log('personal fields post', emergency);
        this.siteAccessRolesService.postpersonalfields(emergency)
            .subscribe(function (res) {
            console.log(res, name);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: first time create compensation fields post
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postCompensationFields = function (compensation) {
        console.log('personal fields post', compensation);
        this.siteAccessRolesService.postpersonalfields(compensation)
            .subscribe(function (res) {
            console.log(res, name);
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: post employee selection for specific task
     author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.postEmpData = function () {
        var _this = this;
        var postData = {
            roleId: this.roleId,
            users: this.selectedUsers
        };
        console.log(this.selectedUsers);
        this.siteAccessRolesService.postEmpDatatoRole(postData)
            .subscribe(function (res) {
            console.log('rwspone emp', res);
            if (res.status == true) {
                _this.selectedNav = 'summaryRecap';
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employee", res.message, "success");
            }
            else {
                _this.swalAlertService.SweetAlertWithoutConfirmation("Employee", res.message, "error");
            }
        }, function (err) {
            console.log(err);
            _this.swalAlertService.SweetAlertWithoutConfirmation("Employee", err.error.message, "error");
        });
    };
    /* Description: in model moving back tab
      author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.backModulesandFields = function (tabName) {
        this.selectedNav = tabName;
    };
    /* Description: get structure fields data for choose employee tab
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getStructureSubFields = function (fields, isChecked) {
        var _this = this;
        console.log(fields, isChecked);
        this.fieldsName = fields;
        this.checked = isChecked;
        if (isChecked == true) {
            this.eligibilityArray = [];
            this.structureFields.forEach(function (element) {
                if (element.name !== fields) {
                    element.isChecked = false;
                }
            });
            this.companySettingsService.getStuctureFields(fields, 0)
                .subscribe(function (res) {
                console.log(res, name);
                _this.StructureValues = res.default;
                _this.customValues = res.custom;
                console.log(_this.StructureValues);
            }, function (err) {
                console.log(err);
            });
        }
    };
    /* Description: select multiple subfields to get employee data
  author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.subFieldsStructure = function ($event, value) {
        var _this = this;
        console.log($event, value);
        // this.eligibilityArray = [];
        if ($event.checked == true) {
            this.eligibilityArray.push(value);
        }
        console.log(this.eligibilityArray, "123321");
        if ($event.checked == false) {
            var index = this.eligibilityArray.indexOf(value);
            if (index > -1) {
                this.eligibilityArray.splice(index, 1);
            }
            console.log(this.eligibilityArray);
        }
        var postdata = {
            field: this.fieldsName,
            chooseEmployeesForRoles: this.eligibilityArray
        };
        console.log(postdata);
        this.siteAccessRolesService.chooseEmployeesData(postdata)
            .subscribe(function (res) {
            console.log("emp data", res);
            _this.dropdownList = [];
            for (var i = 0; i < res.data.length; i++) {
                if ($event.checked == true)
                    _this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName });
            }
            console.log(_this.dropdownList, "employee dropdown");
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: select single structure values
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.getFieldsForRoles = function () {
        var _this = this;
        this.companySettingsService.getFieldsForRoles()
            .subscribe(function (res) {
            console.log("12122123", res);
            _this.structureFields = res.data;
            // this.dropdownList =  { item_id: 1, item_text: 'Mumbai' }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description: summary recap last submit button model close
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.closeRecapPopup = function () {
        this.dialogRef.close();
    };
    /* Description: model close
    author : vipin reddy */
    CreateCustomUserRoleModelComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    CreateCustomUserRoleModelComponent.prototype.accessLevelChange = function (roleName, event, i) {
        console.log(roleName, event, i);
    };
    CreateCustomUserRoleModelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-custom-user-role-model',
            template: __webpack_require__(/*! ./create-custom-user-role-model.component.html */ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.html"),
            styles: [__webpack_require__(/*! ./create-custom-user-role-model.component.css */ "./src/app/admin-dashboard/company-settings/site-access/site-user/create-custom-user-role-model/create-custom-user-role-model.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object, _services_employee_service__WEBPACK_IMPORTED_MODULE_7__["EmployeeService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_3__["SiteAccessRolesService"], _services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__["CompanySettingsService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_4__["MyInfoService"], _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"]])
    ], CreateCustomUserRoleModelComponent);
    return CreateCustomUserRoleModelComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.css":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.css ***!
  \**********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-template{\n    padding: 0 !important;\n  }\n  .main-template .modal-header{\n    padding: 20px 0;\n    border-bottom: 1px solid #ccc;\n  }\n  /* personal table data */\n  .person-tables { margin: 0 auto; float: none; padding: 0;}\n  .person-tables h3 { font-size: 15px; line-height: 15px; color:#5d5d5d; margin: 0 0 30px;}\n  .person-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #f8f8f8; font-size: 16px; border-radius:3px;}\n  .person-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n  .person-tables .table>thead>tr>th a .fa {  font-size: 15px; line-height: 15px;}\n  .person-tables .table>tbody>tr>td, .person-tables .table>tfoot>tr>td, .person-tables .table>thead>tr>td{ padding: 15px; background:#f8f8f8;vertical-align:middle; border-right:#e6e6e6 1px solid;border-bottom:#e6e6e6 1px solid; color: #656464;text-align: center;}\n  .person-fields .checkbox{ margin: 0;}\n  .person-tables .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top: 0;height: 100%;}\n  .person-tables .table>tbody>tr>td.zenwork-checked-border {position: relative;}\n  .person-tables .cont-check .checkbox label { padding-left:5px;}\n  .person-tables .cont-check.label-pad .checkbox label { padding-left: 0;}\n  .person-tables table{\n    width: 100%;\n}\n  .person-tables table thead tr th{\n    padding: 20px 40px 20px 15px;\n    font-size: 16px;\n    background: #f8f8f8;\n    /* border-right: 1px solid #ccc; */\n    border-bottom: 1px solid #ccc;\n}\n  .person-tables table tbody tr td{\n    padding: 20px 40px 20px 15px;\n    font-size: 16px;\n    background: #f8f8f8;\n    border-bottom: 1px solid #ccc;\n}\n  .person-tables table thead tr th:first-child{\n    border-right: 1px solid #ccc;\n    /* border-right: none; */\n}\n  .person-tables table tbody tr td:first-child{\n    border-right: 1px solid #ccc;\n    /* border-right: none;  */\n}\n  .person-tables table tbody tr:last-child td{\n  border-bottom: none; \n}\n  .custom-modal-body{\n    padding: 60px;\n}\n  .text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n}\n  .select-btn {\n    border: none;\n    box-shadow: none;\n    height: 40px;\n    /* width: 95%; */\n}\n  .dashboard-table-dropdown{\n    background: #fff !important;\n  }\n  .buttons{\n    margin: 20px 0;\n    padding: 60px;\n  }\n  .buttons ul li{\n    display: inline-block;\n    padding: 0px 10px;\n  }\n  .buttons ul li .back{\n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #000;\n    cursor: pointer;\n  }\n  .buttons ul li .save{\n  \n    border: 1px solid gray;\n    padding: 7px 30px;\n    border-radius: 31px;\n    color: #fff;\n    background: green;\n    /* cursor: pointer; */\n  }\n  .buttons ul li .cancel{\n    color:#e5423d;\n    cursor: pointer;\n  }"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.html":
/*!***********************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.html ***!
  \***********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-template\">\n  <div class=\"modal-header\">\n    <span class=\"\">Create Custom User Role - Define User Type</span>\n  </div>\n  <div class=\"custom-modal-body\">\n    <div class=\"person-tables\">\n      <table>\n        <thead>\n          <tr>\n            <th>Page Name</th>\n            <th>Employee</th>\n            <th>Manager</th>\n            <th>HR</th>\n            <th>System Administrator</th>\n          </tr>\n        </thead>\n        <tbody>\n\n          <tr *ngFor=\"let data of sampleArray\">\n            <td>\n              {{data.name}}\n            </td>\n            <td>\n              <mat-select class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                placeholder=\"Select access level\" (ngModelChange)=\"changeaccessLevel(data)\" [(ngModel)]=\"data.employee\">\n                <mat-option value=\"view\">View Only</mat-option>\n                <mat-option value=\"no\">No Access</mat-option>\n                <mat-option value=\"full\">Full Access</mat-option>\n              </mat-select>\n            </td>\n            <td>\n              <mat-select class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                placeholder=\"Select access level\" (ngModelChange)=\"changeaccessLevel(data)\" [(ngModel)]=\"data.manager\">\n                <mat-option value=\"view\">View Only</mat-option>\n                <mat-option value=\"no\">No Access</mat-option>\n                <mat-option value=\"full\">Full Access</mat-option>\n              </mat-select>\n            </td>\n            <td>\n              <mat-select class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                placeholder=\"Select access level\" (ngModelChange)=\"changeaccessLevel(data)\" [(ngModel)]=\"data.hr\">\n                <mat-option value=\"view\">View Only</mat-option>\n                <mat-option value=\"no\">No Access</mat-option>\n                <!-- <mat-option value=\"none\">No Access</mat-option> -->\n                <mat-option value=\"full\">Full Access</mat-option>\n              </mat-select>\n            </td>\n            <td>\n              <mat-select class=\"form-control text-field select-btn dashboard-table-dropdown\"\n                placeholder=\"Select access level\" (ngModelChange)=\"changeaccessLevel(data)\" [(ngModel)]=\"data.sysadmin\">\n                <mat-option value=\"view\">View Only</mat-option>\n                <mat-option value=\"no\">No Access</mat-option>\n                <mat-option value=\"full\">Full Access</mat-option>\n              </mat-select>\n            </td>\n\n\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n  <div class=\"buttons\">\n    <ul class=\"list-unstyled\">\n\n      <li class=\"list-items\">\n        <a class=\"cancel\" (click)=\"cancel()\">Cancel</a>\n      </li>\n      <li class=\"list-items pull-right\">\n        <button class='save' type=\"submit\" (click)=\"updatePageRoles()\">\n          Save\n        </button>\n      </li>\n\n      <div class=\"clearfix\"></div>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.ts":
/*!*********************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.ts ***!
  \*********************************************************************************************************************************************/
/*! exports provided: EditCustomUserRoleModelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCustomUserRoleModelComponent", function() { return EditCustomUserRoleModelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditCustomUserRoleModelComponent = /** @class */ (function () {
    function EditCustomUserRoleModelComponent(dialogRef, siteAccessRolesService) {
        this.dialogRef = dialogRef;
        this.siteAccessRolesService = siteAccessRolesService;
        this.updateDeleteArray = [];
        this.postData = {};
        this.sampleArray = [];
    }
    EditCustomUserRoleModelComponent.prototype.ngOnInit = function () {
        this.companyId = JSON.parse(localStorage.getItem('companyId'));
        this.allPagesData();
    };
    /* Description: getting all pages data
     author : vipin reddy */
    EditCustomUserRoleModelComponent.prototype.allPagesData = function () {
        var _this = this;
        this.siteAccessRolesService.allPagesData(this.companyId)
            .subscribe(function (res) {
            console.log("respages", res);
            _this.sampleArray = res.data;
        }, function (err) {
            console.log("err");
        });
    };
    /* Description: update pagewise access with baseroles
     author : vipin reddy */
    EditCustomUserRoleModelComponent.prototype.updatePageRoles = function () {
        var _this = this;
        console.log('deleteArray', this.updateDeleteArray);
        if (this.updateDeleteArray.length == 1) {
            console.log(this.postData);
            delete this.postData['createdAt'];
            delete this.postData['name'];
            delete this.postData['pageid'];
            delete this.postData['status'];
            delete this.postData['updatedAt'];
            console.log(this.postData);
            this.siteAccessRolesService.updateRoles(this.postData)
                .subscribe(function (res) {
                console.log("respages", res);
                if (res.status == true) {
                    _this.cancel();
                }
                // this.sampleArray = res.data;
                _this.allPagesData();
            }, function (err) {
                console.log("err");
            });
        }
        if (this.updateDeleteArray.length > 1) {
            for (var i = 0; i < this.updateDeleteArray.length; i++) {
                delete this.updateDeleteArray[i]['createdAt'];
                delete this.updateDeleteArray[i]['name'];
                delete this.updateDeleteArray[i]['pageid'];
                delete this.updateDeleteArray[i]['status'];
                delete this.updateDeleteArray[i]['updatedAt'];
                delete this.updateDeleteArray[i]['pageType'];
            }
            // var result = this.updateDeleteArray.map(function(el) {
            //   var o = Object.assign({}, el);
            //   o.pageType = 'derived';
            //   return o;
            // })
            // console.log("asjkdhjkasdh123",result,this.updateDeleteArray);
            // this.updateDeleteArray = result;
            console.log(this.updateDeleteArray, 'final array');
            console.log('1q2w3e', this.updateDeleteArray);
            this.siteAccessRolesService.updateMultipleRoles(this.updateDeleteArray)
                .subscribe(function (res) {
                console.log("respages multi", res);
                _this.allPagesData();
                if (res.status == true) {
                    _this.cancel();
                }
                // this.sampleArray = res.data;
            }, function (err) {
                console.log(err);
            });
        }
    };
    /* Description: change access level of page wise with baseroles
     author : vipin reddy */
    EditCustomUserRoleModelComponent.prototype.changeaccessLevel = function (data) {
        console.log(data);
        // if(data._id ==
        var index = -1;
        for (var i = 0; i < this.updateDeleteArray.length; i++) {
            if (data._id === this.updateDeleteArray[i]._id)
                index = i;
        }
        if (index >= 0) {
            this.updateDeleteArray[index] = data;
            this.postData = data;
            // this.updatePagesRoles(data)
        }
        else {
            this.updateDeleteArray.push(data);
            // this.updataMultipleRoles(data)
        }
        console.log("111111111", this.updateDeleteArray);
    };
    /* Description: close the popup
     author : vipin reddy */
    EditCustomUserRoleModelComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    EditCustomUserRoleModelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-custom-user-role-model',
            template: __webpack_require__(/*! ./edit-custom-user-role-model.component.html */ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.html"),
            styles: [__webpack_require__(/*! ./edit-custom-user-role-model.component.css */ "./src/app/admin-dashboard/company-settings/site-access/site-user/edit-custom-user-role-model/edit-custom-user-role-model.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_2__["SiteAccessRolesService"]])
    ], EditCustomUserRoleModelComponent);
    return EditCustomUserRoleModelComponent;
}());



/***/ })

}]);
//# sourceMappingURL=site-access-site-access-module.js.map