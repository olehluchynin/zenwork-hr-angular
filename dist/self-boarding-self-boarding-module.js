(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["self-boarding-self-boarding-module"],{

/***/ "./src/app/self-boarding/self-boarding.component.css":
/*!***********************************************************!*\
  !*** ./src/app/self-boarding/self-boarding.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".self-header { background:#3e3e3e; padding:20px 0 16px;}\n\n.self-header a { display: inline-block;}\n\n.self-header span { display: inline-block; color:#fff; font-size: 20px;}\n\n.self-cont { display: block;}\n\n.self-cont-lft { padding: 0; position: relative;}\n\n.copy { position: absolute; bottom: 0; left: 0; text-align: center;width: 95%;\n    border-top: #cecece 1px solid;\n    margin: 0 auto;\n    right: 0; padding: 10px 0 0;}\n\n.copy p { color:#818583; font-size: 15px; line-height: 25px;}\n\n.copy p a { color:#2ab3e1;}\n\n.self-cont-rgt { padding: 0; background: #f8f8f8;}"

/***/ }),

/***/ "./src/app/self-boarding/self-boarding.component.html":
/*!************************************************************!*\
  !*** ./src/app/self-boarding/self-boarding.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"self-header\">\n  <div class=\"container-fluid\">\n\n    <a href=\"#\" class=\"col-md-3\">\n      <img alt=\"Zenwork Hr\" src=\"../../assets/images//main-nav/whitelogo.png\">\n    </a>\n\n    <span class=\"col-md-3\">Package & Biliing</span>\n    <div class=\"clearfix\"></div>\n  </div>\n</div>\n\n\n<div class=\"self-cont\">\n\n  <!-- <div class=\"self-cont-lft col-md-3\">\n\n    <button mat-raised-button (click)=\"isLinear = !isLinear\" id=\"toggle-linear\">\n      {{!isLinear ? 'Enable linear mode' : 'Disable linear mode'}}\n    </button>\n\n    <mat-vertical-stepper [linear]=\"isLinear\" #stepper>\n\n      <mat-step [stepControl]=\"firstFormGroup\">\n\n        <form [formGroup]=\"firstFormGroup\">\n          <ng-template matStepLabel>Fill out your name</ng-template>\n          <mat-form-field>\n            <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\n          </mat-form-field>\n          <div>\n            <button mat-button matStepperNext>Next</button>\n          </div>\n        </form>\n      </mat-step>\n      \n      <mat-step [stepControl]=\"secondFormGroup\">\n        <form [formGroup]=\"secondFormGroup\">\n          <ng-template matStepLabel>Fill out your address</ng-template>\n          <mat-form-field>\n            <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\n          </mat-form-field>\n          <div>\n            <button mat-button matStepperPrevious>Back</button>\n            <button mat-button matStepperNext>Next</button>\n          </div>\n        </form>\n      </mat-step>\n\n      <mat-step>\n        <ng-template matStepLabel>Done</ng-template>\n        You are now done.\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button (click)=\"stepper.reset()\">Reset</button>\n        </div>\n      </mat-step>\n\n    </mat-vertical-stepper>\n\n\n    <div class=\"copy\">\n      <p>2019 &copy; Copyright | Powered by Zenwork, Inc. <br>\n      <a href=\"#\">Privacy Policy</a> | <a href=\"#\"> Terms of Use </a></p>\n    </div>\n\n\n   </div> -->\n\n\n  <!-- <div class=\"self-cont-rgt col-md-9\">\n\n  </div> -->\n  <router-outlet></router-outlet>\n  <div class=\"clearfix\"></div>\n</div>"

/***/ }),

/***/ "./src/app/self-boarding/self-boarding.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/self-boarding/self-boarding.component.ts ***!
  \**********************************************************/
/*! exports provided: SelfBoardingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfBoardingComponent", function() { return SelfBoardingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SelfBoardingComponent = /** @class */ (function () {
    function SelfBoardingComponent(_formBuilder) {
        this._formBuilder = _formBuilder;
        this.isLinear = false;
    }
    SelfBoardingComponent.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    SelfBoardingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-self-boarding',
            template: __webpack_require__(/*! ./self-boarding.component.html */ "./src/app/self-boarding/self-boarding.component.html"),
            styles: [__webpack_require__(/*! ./self-boarding.component.css */ "./src/app/self-boarding/self-boarding.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], SelfBoardingComponent);
    return SelfBoardingComponent;
}());



/***/ }),

/***/ "./src/app/self-boarding/self-boarding.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/self-boarding/self-boarding.module.ts ***!
  \*******************************************************/
/*! exports provided: SelfBoardingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfBoardingModule", function() { return SelfBoardingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _self_boarding_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./self-boarding.component */ "./src/app/self-boarding/self-boarding.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { ClientDetailsModule } from '../super-admin-dashboard/client-details/client-details.module';
var routes = [
    {
        path: '', component: _self_boarding_component__WEBPACK_IMPORTED_MODULE_3__["SelfBoardingComponent"], children: [
            { path: 'client-details', loadChildren: '../super-admin-dashboard/client-details/client-details.module#ClientDetailsModule' }
        ]
    }
    // { path: '', redirectTo: 'admin-dashboard', pathMatch: "full" },
    // {
    //   path: 'admin-dashboard', component: AdminDashboardComponent, children: [
    //     { path: 'company-settings', loadChildren: "./company-settings/company-settings.module#CompanySettingsModule" },
    //     { path: 'employee-management', loadChildren: "./employee-management/employee-management.module#EmployeeManagementModule" },
    //     { path: 'leave-management', loadChildren: './leave-management/leave-management.module#LeaveManagementModule' }
    //   ]
    // },
];
var SelfBoardingModule = /** @class */ (function () {
    function SelfBoardingModule() {
    }
    SelfBoardingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            declarations: [_self_boarding_component__WEBPACK_IMPORTED_MODULE_3__["SelfBoardingComponent"]]
        })
    ], SelfBoardingModule);
    return SelfBoardingModule;
}());



/***/ })

}]);
//# sourceMappingURL=self-boarding-self-boarding-module.js.map