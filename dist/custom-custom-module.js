(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["custom-custom-module"],{

/***/ "./src/app/admin-dashboard/employee-management/custom/custom.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/custom/custom.module.ts ***!
  \*****************************************************************************/
/*! exports provided: CustomModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomModule", function() { return CustomModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _custom_custom_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom/custom.component */ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.ts");
/* harmony import */ var _custom_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./custom.routing */ "./src/app/admin-dashboard/employee-management/custom/custom.routing.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var CustomModule = /** @class */ (function () {
    function CustomModule() {
    }
    CustomModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _custom_routing__WEBPACK_IMPORTED_MODULE_3__["CustomRouting"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"]
            ],
            declarations: [_custom_custom_component__WEBPACK_IMPORTED_MODULE_2__["CustomComponent"]]
        })
    ], CustomModule);
    return CustomModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/custom/custom.routing.ts":
/*!******************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/custom/custom.routing.ts ***!
  \******************************************************************************/
/*! exports provided: CustomRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomRouting", function() { return CustomRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _custom_custom_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom/custom.component */ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    // { path:'',redirectTo:"/custom",pathMatch:"full" },
    { path: '', component: _custom_custom_component__WEBPACK_IMPORTED_MODULE_2__["CustomComponent"] },
];
var CustomRouting = /** @class */ (function () {
    function CustomRouting() {
    }
    CustomRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CustomRouting);
    return CustomRouting;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/custom/custom/custom.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".personal { margin: 30px auto 0; float: none;}\n\n.personal-head{\n    margin: 10px;\n}\n\n.main-head .list-items {\n    display: inline-block;\n    padding: 0px 5px;\n}\n\n.main-head .list-items span{\n    vertical-align: middle; font-size: 18px;\n}\n\n.zenwork-padding-20-zero{\n    padding:10px !important;\n}\n\n.img-personal-icon{\n    width: 15px;\n}\n\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n\n.field-accordion .panel-body { padding: 0 0 5px;}\n\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n\n.field-accordion .panel-default { border-color:transparent;}\n\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n\n.field-accordion .panel-default>.panel-heading{\n    background: #f8f8f8 !important;\n}\n\n.field-accordion .panel{\n    box-shadow: none;background: #f8f8f8;\n}\n\n.field-accordion .panel-title a{\n    display: inline-flex;\n}\n\n.field-accordion .panel-title { display: inline-block;}\n\n.field-accordion .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n\n.padding-left-15{\n    padding: 0 0 0 15px;\n}\n\n.list-items-addresslocal{\n    display: block;\n    margin: 0px 5px 0px 0px;\n    width: 17%;\n}\n\n.list-items-addresslocal .text-field{\n    font-size: 10px;\n    padding-left: 15px;\n    width: 100%;\n}\n\ninput{\n    /* height:30px; */\n    border:0;\n    /* width: 178px; */\n}\n\nselect{\n    font-size: 12px;\n}\n\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n    color:#464444;\n    font-size: 12px;\n    padding: 0 0 0 2px;\n}\n\n.education-date-list {\n    width: 17%;\n    position: relative;\n    display: inline-block;\n}\n\n.birthdate{\n    font-size: 12px;\n    padding-left: 15px;\n    width: 178px;   \n    box-shadow: none;\n}\n\n.address-state-dropodown{\n    width: 100%;\n    border-radius: 0px!important;\n    height: 30px;\n    border: 0;\n    box-shadow: none;\n}\n\n.edu-ends-calendar-icon{\n    position: absolute;\n    top: 37px;\n    width: 15px;\n    right: 10px;\n}\n\n.mat-radio-button ~ .mat-radio-button {\n    margin-left: 16px;\n  }\n\n.radio-btn{\n    height: 65px;\n    padding: 10px 15px 0 0;  \n  }\n\n.submit-btn {\n    background-color: #439348;\n    border: 1px solid #439348;\n    border-radius: 35px;\n    color: #fff;\n    padding: 0 25px;    \n}\n\n.mar-top-40 {\n    margin-top: 40px;\n}\n\n.error {\n    color:#e44a49;\n}\n\n.text-captilize { font-size: 15px; line-height: 15px; padding: 0 0 10px; display: block;}\n\n.custom-fields { padding: 10px 0 0;}\n\n.custom-fields .form-control { height: auto; padding: 10px 20px; border: none; box-shadow: none;}\n\n.custom-fields .form-control:focus { border-color:transparent; outline: none; box-shadow: none;}\n\n.date { height: 38px !important; line-height:inherit !important; width:80% !important;}\n\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n\n.date span { border: none !important; padding: 0 !important;}\n\n.date .form-control { width:73%; height: auto; padding: 10px 0 10px 10px;}\n\n.date .form-control:focus { box-shadow: none; border: none;}\n\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/custom/custom/custom.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"personal col-md-11\">\n  <div class=\"zenwork-documents-tab\">\n    <div class=\"personal-head\">\n      <ul class=\"list-unstyled main-head\">\n        <li class=\"list-items\">\n          <img src=\"../../../assets/images/directory_tabs/Onboarding/onboarding.png\" class=\"img-personal-icon\"\n            alt=\"documents-icon\">\n        </li>\n        <li class=\"list-items\">\n          <span>Custom</span>\n        </li>\n      </ul>\n    </div>\n    <hr class=\"zenwork-margin-ten-zero\">\n    <div class=\"custom-fields\">\n      <div *ngFor=\"let field of customFieldsData\" class=\"col-xs-4\" style=\"height:94px\">\n        <label class=\"text-captilize\">{{field.fieldName}}</label>\n        <div *ngIf=\"field.fieldType == 'Dropdown'\" class=\"dropdown-field\">\n          <mat-select placeholder=\"type\" [(ngModel)]=\"field.text\" name=\"dropdown\" #dropdown=\"ngModel\"\n            [required]=\"field.required\">\n            <mat-option *ngFor=\"let dropdownoption of field.dropDownOptions\" [value]=\"dropdownoption.value\">\n              {{dropdownoption.value}}</mat-option>\n\n          </mat-select>\n          <span *ngIf=\"field.required\">\n            <p *ngIf=\"!field.text && dropdown.touched || (!field.text && isValid)\" class=\"error\">\n              Please fill this field</p>\n          </span>\n        </div>\n\n        <div *ngIf=\"field.fieldType == 'Text'\" class=\"input-filed\">\n          <input type=\"text\" [(ngModel)]=\"field.text\" (keypress)=\"alphabets($event)\"\n            class=\"form-control company-legal-name\" name=\"alpa\" #alpa=\"ngModel\" [required]=\"field.required\">\n\n          <span *ngIf=\"field.required\">\n            <p *ngIf=\"!field.text && alpa.touched || (!field.text && isValid)\" class=\"error\">\n              Please fill this field</p>\n          </span>\n\n        </div>\n        <div *ngIf=\"field.fieldType == 'Numeric'\" class=\"input-filed\">\n          <input type=\"number\" class=\"form-control company-legal-name\" [(ngModel)]=\"field.text\" name=\"numeric\"\n            #numeric=\"ngModel\" [required]=\"field.required\">\n          <span *ngIf=\"field.required\">\n            <p *ngIf=\"!field.text && numeric.touched || (!field.text && isValid)\" class=\"error\">\n              Please fill this field</p>\n          </span>\n        </div>\n        <div *ngIf=\"field.fieldType == 'Yes/No'\" class=\"input-filed radio-btn\">\n          <!-- <input type=\"number\" class=\"form-control company-legal-name\" [(ngModel)]=\"field.text\" > -->\n          <mat-radio-group aria-label=\"Select an option\" [(ngModel)]=\"field.text\" name=\"radio\" #radio=\"ngModel\"\n            [required]=\"field.required\">\n            <mat-radio-button value=\"Yes\">Yes</mat-radio-button>\n            <mat-radio-button value=\"No\">No</mat-radio-button>\n          </mat-radio-group>\n\n          <span *ngIf=\"field.required\">\n            <p *ngIf=\"!field.text && radio.touched || (!field.text && isValid)\" class=\"error\">\n              Please fill this field</p>\n          </span>\n        </div>\n\n        <div *ngIf=\"field.fieldType == 'Date'\" class=\"input-filed radio-btn\">\n          <div class=\"date\">\n            <!-- <input type=\"number\" class=\"form-control company-legal-name\" [(ngModel)]=\"field.text\" > -->\n            <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm/dd/yy\" class=\"form-control\"\n              [(ngModel)]=\"field.text\" name=\"date\" #date=\"ngModel\"\n              [required]=\"field.required\">\n            <mat-datepicker #picker1></mat-datepicker>\n            <button mat-raised-button (click)=\"picker1.open()\">\n              <span>\n                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n              </span>\n            </button>\n            <div class=\"clearfix\"></div>\n\n          </div>\n          <span *ngIf=\"field.required\">\n              <p *ngIf=\"!field.text && date.touched || (!field.text && isValid)\" class=\"error\">\n                Please fill this field</p>\n            </span>\n        </div>\n\n      </div>\n\n    </div>\n    <div class=\"clearfix\"></div>\n    <hr class=\"zenwork-margin-ten-zero\">\n    <div class=\"row mar-top-40\">\n\n      <button mat-button data-dismiss=\"modal\">Cancel</button>\n      <button mat-button class=\"submit-btn pull-right\" (click)=\"saveCompanyContactInfo()\">Save Changes</button>\n\n\n\n    </div>\n\n    <!-- <button class=\"btn pull-left cancel\">Cancel</button> -->\n    <!-- <button class=\"btn pull-right save\" (click)=\"saveCompanyContactInfo()\">Save Changes</button> -->\n\n    <div class=\"clearfix\"></div>\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/custom/custom/custom.component.ts ***!
  \***************************************************************************************/
/*! exports provided: CustomComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomComponent", function() { return CustomComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/my-info.service */ "./src/app/services/my-info.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CustomComponent = /** @class */ (function () {
    function CustomComponent(companySettingsService, myInfoService, swalAlertService) {
        this.companySettingsService = companySettingsService;
        this.myInfoService = myInfoService;
        this.swalAlertService = swalAlertService;
        this.show = false;
        this.isValid = false;
        this.customFieldsData = [
            {
                text: '',
                fieldName: '',
                fieldType: '',
                required: false,
            },
        ];
        this.fieldData = [];
        this.fieldName = [];
        this.sendingObj = {};
        this.responseKeys = [];
        this.customFieldValidate = false;
    }
    CustomComponent.prototype.ngOnInit = function () {
        this.companyId = JSON.parse(localStorage.getItem('companyId'));
        this.userId = JSON.parse(localStorage.getItem('employeeId'));
        this.getAllCustomFields();
        this.getcustomfieldsData();
    };
    /* Description:get all custom fields
    author : vipin reddy */
    CustomComponent.prototype.getAllCustomFields = function () {
        var _this = this;
        this.companySettingsService.getAllCustomFields()
            .subscribe(function (res) {
            console.log("res of custom", res);
            _this.customFieldsData = res.data.customFields;
            _this.customFieldsData = _this.customFieldsData.map(function (el) {
                var o = Object.assign({}, el);
                o.text = '';
                return o;
            });
        }, function (err) {
            console.log("err res", err);
        });
    };
    CustomComponent.prototype.getcustomfieldsData = function () {
        var _this = this;
        this.myInfoService.getcustomfieldsData(this.companyId, this.userId)
            .subscribe(function (res) {
            console.log(res);
            if (res.data) {
                Object.assign(_this.sendingObj, res.data);
                console.log(_this.sendingObj);
                _this.responseKeys = Object.keys(_this.sendingObj);
                for (var i = 0; i < _this.customFieldsData.length; i++) {
                    _this.responseKeys.forEach(function (element1) {
                        if (_this.customFieldsData[i].fieldName == element1) {
                            _this.customFieldsData[i].text = _this.sendingObj[_this.customFieldsData[i].fieldName];
                        }
                        console.log(_this.customFieldsData);
                    });
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    /* Description:accept only alphabets
  author : vipin reddy */
    CustomComponent.prototype.alphabets = function (event) {
        return (event.charCode > 64 &&
            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123);
    };
    CustomComponent.prototype.saveCompanyContactInfo = function () {
        var _this = this;
        this.isValid = true;
        this.customFieldValidate = false;
        console.log(this.customFieldsData);
        for (var i = 0; i < this.customFieldsData.length; i++) {
            this.sendingObj[this.customFieldsData[i].fieldName] = this.customFieldsData[i].text;
            console.log(this.sendingObj);
            if (!this.customFieldsData[i].text && this.customFieldsData[i].required) {
                this.customFieldValidate = true;
            }
        }
        var postData = {
            companyId: this.companyId,
            userId: this.userId
        };
        Object.assign(this.sendingObj, postData);
        console.log(this.sendingObj, 'sendingObj');
        if (!this.customFieldValidate) {
            this.myInfoService.sendingCustomService(this.sendingObj)
                .subscribe(function (res) {
                console.log(res);
                if (res.status == true) {
                    _this.swalAlertService.SweetAlertWithoutConfirmation('custom fields', res.message, 'success');
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    CustomComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-custom',
            template: __webpack_require__(/*! ./custom.component.html */ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.html"),
            styles: [__webpack_require__(/*! ./custom.component.css */ "./src/app/admin-dashboard/employee-management/custom/custom/custom.component.css")]
        }),
        __metadata("design:paramtypes", [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_1__["CompanySettingsService"],
            _services_my_info_service__WEBPACK_IMPORTED_MODULE_2__["MyInfoService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_3__["SwalAlertService"]])
    ], CustomComponent);
    return CustomComponent;
}());



/***/ })

}]);
//# sourceMappingURL=custom-custom-module.js.map