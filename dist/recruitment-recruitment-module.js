(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["recruitment-recruitment-module"],{

/***/ "./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-currentpage { border-bottom:#ccc 1px solid; padding:30px 0 20px; margin: 0 auto; float: none;}\n\n.application-candidates { padding: 0; float: none; margin: 0 auto;}\n\n.search-candidates { border-bottom: #ccc 1px solid; padding: 0 0 20px; margin:30px 0;}\n\n.search-candidates .form-group { position: relative; background:#fff; padding: 0; border-radius:5px;}\n\n.search-candidates .form-control { height: auto; border: none; box-shadow: none; padding:12px; width:94%; font-size: 15px;}\n\n.search-candidates .form-control:focus { box-shadow: none;}\n\n.search-candidates span { position: absolute; top:15px; right:10px; cursor: pointer;}\n\n.search-candidates span .material-icons { color: #8e8e8e; font-size:20px; font-weight: normal;}\n\n.search-candidates a { background:#787d7a; color: #fff; border-radius:4px; padding: 7px 25px; cursor: pointer; margin:10px 0 0;}\n\n.candidates-cont { display: block;}\n\n.candidates-cont h3 { margin: 0 0 20px; font-size:18px;}\n\n.candidates-block {background: #fff;}\n\n.candidates-top {background:#edf7fe; padding:12px 10px;}\n\n.candidates-top ul{ margin: 0;}\n\n.candidates-top ul li{padding: 0; float: left;}\n\n.candidates-top ul li h4 { margin: 0; font-size: 16px; position: relative; display: inline-block;}\n\n.candidates-top ul li.applicant { width:35%;}\n\n.candidates-top ul li.applicant1 { width:25%;}\n\n.candidates-top ul li.applicant2 { width: 13%;}\n\n.candidates-top ul li.applicant3 { width: 14%;}\n\n.candidates-top ul li.applicant4 { width: 13%;}\n\n.dropdown { position: absolute; top: 0; right:-30px;}\n\n.dropdown a { color:#787d7a; cursor: pointer;}\n\n.dropdown > ul > li { float: none;}\n\n.dropdown > ul > li a { padding: 8px 10px;}\n\n.dropdown > ul > li a:hover { background:#008f3d; color: #fff;}\n\n.dropdown-menu { padding: 0; border: none;}\n\n.candidates-mdl {padding: 0 10px;}\n\n.candidates-mdl ul { display: block;}\n\n.candidates-mdl ul li {border-bottom: #ccc 1px solid;}\n\n.applications {margin: 15px 0;}\n\n.applications ul { display: block;}\n\n.applications ul li { float: left; border: none;}\n\n.applications ul li.recent-data {width: 35%;}\n\n.applications ul li.recent-data1 {width: 25%;}\n\n.applications ul li.recent-data2 {width: 13%;}\n\n.applications ul li.recent-data3 {width: 14%;}\n\n.applications ul li.recent-data4 {width: 13%;}\n\n.applications ul li.recent-data1 p { display: block;}\n\n.applications ul li.recent-data1 p small { display: inline-block; vertical-align: middle; width:30px;}\n\n.applications ul li.recent-data1 p span { display: inline-block; vertical-align: middle; width:230px; font-size: 15px;\ncolor:#8e8e8e;}\n\n.applications ul li.recent-data2 var { font-style: normal;color:#8e8e8e;}\n\n.details-data{ display: block;}\n\n.details-data figure{ display: inline-block; width: 110px; vertical-align: middle;}\n\n.details-cont { display: inline-block; vertical-align: middle; width: 250px;}\n\n.details-cont h4{ color:#444; font-size:17px; margin: 0 0 10px;}\n\n.details-cont p{ color:#787d7a; font-size:14px; margin: 0 0 2px;}\n\n.details-cont p span{ color:#787d7a; font-size:14px; display: inline-block;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"zenwork-currentpage col-md-11\">\n  <p class=\"zenwork-margin-zero\">\n    <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n      <span class=\"green mr-7\">\n        <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n      </span>Back\n    </button>\n    <small>\n      <img alt=\"Company-settings icon\" src=\"../../../assets/images/company-settings/site-access/site-access-icon.png\">\n    </small>\n    <em>Applicant Candidates</em>\n  </p>\n</div>\n\n\n<div class=\"application-candidates col-md-11\">\n\n  <div class=\"search-candidates\">\n    <div class=\"form-group col-md-6\">\n      <input type=\"mail\" class=\"form-control\" placeholder=\"Search by name, Job Titles, etc..\">\n      <span><i class=\"material-icons\">search</i></span>\n    </div>\n\n    <a class=\"pull-right\" (click)=\"manuallyClick()\">Manually Add Applicant</a>\n\n    <div class=\"clearfix\"></div>\n  </div>\n\n\n  <div class=\"candidates-cont\">\n\n    <h3>Recently submitted applications</h3>\n\n     <div class=\"candidates-block\">\n        \n        <div class=\"candidates-top\">\n\n          <ul>\n            <li class=\"applicant\"><h4>Applicant Details\n\n              <div class=\"dropdown\">\n                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"material-icons icon-image-preview\">swap_vert</i></a>\n                <ul class=\"dropdown-menu\">\n                  <li><a>Naukari.com</a></li>\n              \n                </ul>\n              </div>              \n            </h4> \n          \n            </li>\n            <li class=\"applicant1\"> <h4>Contact Info</h4> </li>\n            <li class=\"applicant2\"> <h4>Date Applied</h4> </li>\n            <li class=\"applicant3\"> <h4>Source</h4> </li>\n            <li class=\"applicant4\"> <h4>Status</h4></li>\n          </ul>\n           <div class=\"clearfix\"></div>\n        </div>\n\n        <div class=\"candidates-mdl\">\n          \n          <ul>\n            <li>\n              <div class=\"applications\">\n                <ul>\n                  <li class=\"recent-data\">\n                    \n                    <div class=\"details-data\">\n\n                      <figure>\n                        <img src=\"../../../../assets/images/Directory/empoyee_2.png\">\n                      </figure>\n\n                      <div class=\"details-cont\">\n                        <h4>Suresh M</h4>\n                        <p>Job Title: <span>SR Angular Developer</span></p>\n                        <p>Job Number: <span>2354235</span></p>\n                        <p>Department / Location / Business Unit: <span>USA</span></p>\n                      </div>\n\n                    </div>\n\n                  </li>\n                  <li class=\"recent-data1\">\n                    <p>\n                      <small><img src=\"../../../assets/images/Directory/ic_email_24px.png\"></small>\n                      <span>suresh@ghgjkl.com</span>\n                    </p>\n                    <p>\n                      <small><img src=\"../../../assets/images/Directory/ic_domain_24px1.png\"></small>\n                      <span>457688789797 Ext, 1272</span>\n                    </p>\n                    <p>\n                      <small><img src=\"../../../assets/images/Directory/Group 504.png\"></small>\n                      <span>98794856887</span>\n                    </p>\n                  </li>\n                  <li class=\"recent-data2\">\n                    <var>10/10/2000</var>\n                  </li>\n                  <li class=\"recent-data3\">\n                    ssssssssssssss\n                  </li>\n                  <li class=\"recent-data4\">\n                    <mat-select class=\"form-control\" placeholder=\"Status\">\n                      <mat-option value=\"option1\">Option 1</mat-option>                \n                    </mat-select>\n                  </li>\n                </ul>\n                <div class=\"clearfix\"></div>\n              </div>\n            </li>\n          </ul>\n\n        </div>\n\n     </div>\n\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: ApplicantCandidatesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicantCandidatesComponent", function() { return ApplicantCandidatesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _manually_applicant_manually_applicant_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../manually-applicant/manually-applicant.component */ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApplicantCandidatesComponent = /** @class */ (function () {
    function ApplicantCandidatesComponent(dialog, router) {
        this.dialog = dialog;
        this.router = router;
    }
    ApplicantCandidatesComponent.prototype.ngOnInit = function () {
    };
    ApplicantCandidatesComponent.prototype.manuallyClick = function () {
        var dialogRef = this.dialog.open(_manually_applicant_manually_applicant_component__WEBPACK_IMPORTED_MODULE_2__["ManuallyApplicantComponent"], {
            width: '900px',
            height: '800px'
        });
        dialogRef.afterClosed().subscribe(function (data) {
        });
    };
    ApplicantCandidatesComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/recruitment']);
    };
    ApplicantCandidatesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-applicant-candidates',
            template: __webpack_require__(/*! ./applicant-candidates.component.html */ "./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.html"),
            styles: [__webpack_require__(/*! ./applicant-candidates.component.css */ "./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ApplicantCandidatesComponent);
    return ApplicantCandidatesComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.css":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.css ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".schedule-main { display: block;}\n\n.schedule-main h3 { border-bottom:#ccc 1px solid; font-size:18px; margin: 0 0 20px; padding: 0 0 20px;color: #696666;}\n\n.schedule-in { margin: 0 auto; float: none; padding: 0;}\n\n.schedule-in h4 {font-size:18px; margin: 0; padding: 0 0 20px;color: #696666;}\n\n.schedule-in > ul { display: inline-block; width: 100%; margin: 0 0 15px;}\n\n.schedule-in > ul > li { padding: 0; margin: 0 20px 0 0;}\n\n.schedule-in > ul > li label{color:#808080;font-size: 17px; padding: 0 0 10px;}\n\n.schedule-in > ul > li .form-control{ border: none; box-shadow: none; padding:12px; height: auto;\nbackground:#f8f8f8;}\n\n.schedule-in > ul > li .form-control:focus{ box-shadow: none;}\n\n.interview-details{border-top:#ccc 1px solid; padding: 20px 0 0; margin: 20px 0 0;}\n\n.interview-details-in { margin: 0 auto; float: none; padding: 0;}\n\n.interview-details-in li a {color:#808080; font-size: 16px; text-decoration: underline;}\n\n.interview-details-in li { margin: 0 0 20px;}\n\n.schedule-main .btn.save {border-radius: 20px;font-size: 16px; padding:6px 30px;\nbackground: #008f3d; color:#fff;}\n\n.schedule-main .btn.cancel {color:#e5423d; background:transparent;font-size: 16px;}\n\n.border-top { border-top:#ccc 1px solid; padding: 20px 0 0;}\n\n.border-mdl { margin: 0 auto; float: none; padding: 0;}\n\n.date { height: auto !important; line-height:inherit !important; width:100%;background:#f8f8f8;}\n\n.edit-work .date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none;\nwidth: 40px;height: 30px; min-width: auto; border-left: #ccc 1px solid; border-radius: 0;}\n\n.date span { border: none !important; padding: 0 !important;}\n\n.date .form-control { width:65%; height: auto; padding: 13px 0 13px 10px; border: none; box-shadow: none; display: inline-block;}\n\n.date .form-control:focus { box-shadow: none; border: none;}\n\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"schedule-main\">\n  <h3>Schedule / Score Interview </h3>\n\n <div class=\"schedule-in col-md-11\">\n\n    <h4>Interview Follow Up</h4>\n\n   <ul>\n     <li class=\"col-md-4\">\n       <label>Candidate Name</label>\n       <input type=\"text\" class=\"form-control\" placeholder=\"Candidate name\">\n     </li>\n   </ul>    \n   <div class=\"clearfix\"></div>\n\n   <ul>\n    <li class=\"col-md-3\">\n      <label>Job Title</label>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Job title\">\n    </li>\n\n    <li class=\"col-md-3\">\n      <label>Job Requisition Number</label>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Job number\">\n    </li>\n  </ul>    \n  <div class=\"clearfix\"></div>\n\n\n  <ul>\n    <li class=\"col-md-3\">\n      <label>Interviewer Name</label>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Interviewer name\">\n    </li>\n\n    <li class=\"col-md-3\">\n      <label>Interviewer Role</label>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Interviewer role\">\n    </li>\n  </ul>    \n  <div class=\"clearfix\"></div>\n\n\n  <ul>\n    <li class=\"col-md-3\">\n      <label>Interview Date</label>\n      <div class=\"date\">\n        <input matInput [matDatepicker]=\"picker1\" placeholder=\"mm / dd / yy\" class=\"form-control\">\n        <mat-datepicker #picker1></mat-datepicker>\n        <button mat-raised-button (click)=\"picker1.open()\">\n          <span>\n            <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n          </span>\n        </button>\n        <div class=\"clearfix\"></div>\n      </div>\n    </li>\n\n    <li class=\"col-md-3\">\n      <label>Interview Round</label>\n      <mat-select class=\"form-control\" placeholder=\"Interview round\">        \n        <mat-option value=\"option1\">Option 1</mat-option>\n      </mat-select>\n    </li>\n\n    <li class=\"col-md-3\">\n      <label>Interview Score</label>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Interview score\">\n    </li>\n  </ul>     \n  <div class=\"clearfix\"></div>\n\n </div>\n\n <div class=\"schedule-in interview-details\">\n    <div class=\"interview-details-in col-md-11\">\n\n  <h4>Interview Details</h4>\n\n  <ul>\n    <li><a href=\"#\">Decline</a></li>\n    <li><a href=\"#\">Schedule Another Interview</a></li>\n    <li><a href=\"#\">Recommend for Another Position</a></li>\n    <li><a href=\"#\">Make Offer</a></li>\n  </ul>    \n  <div class=\"clearfix\"></div>\n\n</div>\n</div>\n   \n  <div class=\"border-top\">\n     <div class=\"border-mdl col-md-11\">\n       <button class=\"btn pull-left cancel\" (click)=\"onClick()\">Cancel</button>\n       <button class=\"btn pull-right save\">Submit</button>\n       <div class=\"clearfix\"></div>\n\n     </div>\n  </div>\n   \n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.ts ***!
  \************************************************************************************************/
/*! exports provided: InterviewScheduleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterviewScheduleComponent", function() { return InterviewScheduleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var InterviewScheduleComponent = /** @class */ (function () {
    function InterviewScheduleComponent(dialog, data) {
        this.dialog = dialog;
        this.data = data;
    }
    InterviewScheduleComponent.prototype.ngOnInit = function () {
    };
    InterviewScheduleComponent.prototype.onClick = function () {
        this.dialog.close();
    };
    InterviewScheduleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-interview-schedule',
            template: __webpack_require__(/*! ./interview-schedule.component.html */ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.html"),
            styles: [__webpack_require__(/*! ./interview-schedule.component.css */ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], InterviewScheduleComponent);
    return InterviewScheduleComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/interviews/interviews.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/interviews/interviews.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 17px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{\n    margin-right: 7px;\n}\n\n.zenwork-currentpage { border-bottom:#ccc 1px solid; padding:30px 0 20px; margin: 0 auto; float: none;}\n\n.interviews { margin:20px auto 0; float: none; padding: 0;}\n\n.interviews h2 { margin: 0 0 30px; font-size: 20px; color:#444;}\n\n.interviews { display: block;}\n\n.interviews .table{ background:#fff;}\n\n.interviews .table>tbody>tr>th, .interviews .table>thead>tr>th {padding: 10px 15px; color: #484747; font-weight: normal; font-size:17px;\nvertical-align: middle; position: relative; background: #edf7ff;}\n\n.interviews .table>tbody>tr>td, .interviews .table>tfoot>tr>td, .interviews .table>tfoot>tr>th, \n.interviews .table>thead>tr>td {color: #484747;padding:15px;font-size:15px; vertical-align: middle;}\n\n.interviews .table>tbody>tr>th a, .interviews .table>thead>tr>th a { position: relative;color: #929191; cursor: pointer;}\n\n.interviews .table>tbody>tr>th a .fa, .interviews .table>thead>tr>th a .fa { color: #6f6d6d;}\n\n.interview-data{ display: block;}\n\n.interview-data figure{ display: inline-block; width:60px; vertical-align: middle;}\n\n.interview-data h4{ color:#444; font-size:17px;display: inline-block; vertical-align: middle;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/interviews/interviews.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/interviews/interviews.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"zenwork-currentpage col-md-11\">\n  <p class=\"zenwork-margin-zero\">\n    <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n      <span class=\"green mr-7\">\n        <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n      </span>Back\n    </button>\n    <small>\n      <img alt=\"Company-settings icon\" src=\"../../../assets/images/company-settings/site-access/site-access-icon.png\">\n    </small>\n    <em>Interviews</em>\n  </p>\n  \n</div>\n\n\n<div class=\"interviews col-md-11\">\n\n <h2>Interviews</h2>\n\n\n <table class=\"table\">\n  <thead>\n    <tr>\n      <th><mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox></th>\n      <th>Candidates</th>\n      <th>Job Title</th>\n      <th>Job Number</th>\n      <th>Scheduled Date</th>\n      <th>Location</th>\n     <th>Interview Score / Status</th>\n      <th>\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\" aria-label=\"Example icon-button with a menu\">\n          <mat-icon>more_vert</mat-icon>\n        </button>\n        <mat-menu #menu=\"matMenu\">\n          <button mat-menu-item>\n            <a>Reschedule</a>\n          </button>\n         \n          <button mat-menu-item>\n              <a>Review Candidates</a>\n          </button>\n\n          <button mat-menu-item>\n            <a (click)=\"scheduleClick()\">schedule / Score Interview</a>\n          </button>\n\n        </mat-menu>\n      </th>\n    </tr>\n  </thead>\n\n  <tbody> \n    <tr>\n      <td><mat-checkbox class=\"zenwork-customized-checkbox\"></mat-checkbox></td>\n      <td>\n        <div class=\"interview-data\">\n\n          <figure>\n            <img src=\"../../../../assets/images/Directory/empoyee_-1.png\">\n          </figure>\n\n            <h4>Candidates</h4>\n\n        </div>\n      </td>\n      <td>Job Title</td>\n      <td>5345345</td>\n      <td>June 20</td>\n      <td>Hyd</td>\n     <td>sfsdfsfsdfsd</td>\n      <td></td>\n    </tr>\n    \n  </tbody>\n</table>\n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/interviews/interviews.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/interviews/interviews.component.ts ***!
  \********************************************************************************/
/*! exports provided: InterviewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterviewsComponent", function() { return InterviewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _interview_schedule_interview_schedule_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../interview-schedule/interview-schedule.component */ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InterviewsComponent = /** @class */ (function () {
    function InterviewsComponent(router, dialog) {
        this.router = router;
        this.dialog = dialog;
    }
    InterviewsComponent.prototype.ngOnInit = function () {
    };
    InterviewsComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/recruitment/']);
    };
    InterviewsComponent.prototype.scheduleClick = function () {
        var scheduleOpen = this.dialog.open(_interview_schedule_interview_schedule_component__WEBPACK_IMPORTED_MODULE_3__["InterviewScheduleComponent"], {
            width: '1000px',
            height: '800px'
        });
        scheduleOpen.afterClosed().subscribe(function (data) {
        });
    };
    InterviewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-interviews',
            template: __webpack_require__(/*! ./interviews.component.html */ "./src/app/admin-dashboard/recruitment/interviews/interviews.component.html"),
            styles: [__webpack_require__(/*! ./interviews.component.css */ "./src/app/admin-dashboard/recruitment/interviews/interviews.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], InterviewsComponent);
    return InterviewsComponent;
}());

// @Component({
//   selector:'app-interview-schedule',
//   templateUrl:'./interview-schedule.component.html',
// })
// export class InterviewScheduleComponent {
//   constructor( private dialog :MatDialogRef <InterviewScheduleComponent>, @Inject(MAT_DIALOG_DATA) public data) {
//   }
// }


/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.css":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.css ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".manually-add { display: block;}\n\n.manually-add h3 { border-bottom:#ccc 1px solid; font-size:18px; margin: 0 0 20px; padding: 0 0 20px;}\n\n.manually-in { margin: 0 auto; float: none; padding: 0;}\n\n.manually-in > ul { display: inline-block; width: 100%; margin: 0 0 15px;}\n\n.manually-in > ul > li { padding: 0; margin: 0 20px 0 0;}\n\n.manually-in > ul > li label{color:#808080;font-size: 17px; padding: 0 0 10px;}\n\n.manually-in > ul > li .form-control{ border: none; box-shadow: none; padding:12px; height: auto;\nbackground:#f8f8f8;}\n\n.manually-in > ul > li .form-control:focus{ box-shadow: none;}\n\n.manually-in > ul.manually-add > li { display: block; margin: 0 0 20px; float: none;}\n\n.manually-in > ul.manually-add > li .phone { display: block; background:#f8f8f8; padding:3px 10px;}\n\n.manually-in > ul.manually-add > li .phone span { display: inline-block; vertical-align: middle; cursor: pointer;\nborder-right: #bdbdbd 1px solid;padding: 0 10px 0 0;}\n\n.manually-in > ul.manually-add > li .phone span .fa { color: #636060;\n    font-size: 23px;\n    line-height: 23px;\n    width: 18px;\n    text-align: center;}\n\n.manually-in > ul.manually-add > li .phone .form-control { display: inline-block;vertical-align: middle; width: 85%;\nborder: none; background:transparent; box-shadow: none; padding:10px;line-height:inherit;}\n\n.manually-add .btn.save {border-radius: 20px;font-size: 16px; padding:8px 30px;\nbackground: #008f3d; color:#fff;}\n\n.manually-add .btn.cancel {color:#e5423d; background:transparent;font-size: 16px;}\n\n.border-top { border-top:#ccc 1px solid; padding: 20px 0 0;}\n\n.border-mdl { margin: 0 auto; float: none; padding: 0;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"manually-add\">\n   <h3>Manually Add Applicant</h3>\n\n  <div class=\"manually-in col-md-10\">\n\n    <ul>\n      <li class=\"col-md-4\">\n        <label>First Name</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"First Name\">\n      </li>\n  \n      <li class=\"col-md-4\">\n        <label>Last Name</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"Last Name\">\n      </li>\n\n    </ul>    \n    <div class=\"clearfix\"></div>\n\n    <ul class=\"manually-add\">\n      <li class=\"col-md-5\">\n        <label>Phone Number</label>\n        <div class=\"phone\">\n          <span>\n            <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n          </span>\n          <input type=\"text\" class=\"form-control\" placeholder=\"Phone Number\">          \n        </div>\n      </li>\n  \n      <li class=\"col-md-5\">\n        <label>Work Email</label>\n        <div class=\"phone\">\n          <span>\n            <img src=\"../../../../assets/images/company-settings/company-setup/work-phone.png\" width=\"18\"\n              height=\"16\" alt=\"img\">\n          </span>\n          <input type=\"text\" class=\"form-control\" placeholder=\"Work Email\">          \n        </div>\n      </li>\n\n      <li class=\"col-md-5\">\n        <label>Job Title Interest</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"Job Title\">\n      </li>\n  \n      <li class=\"col-md-5\">\n        <label>Job Requisition Number</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"Job Number\">\n      </li>\n\n      <li class=\"col-md-5\">\n        <label>Area of Interest</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"Area of Interest\">\n      </li>\n\n    </ul>    \n\n    <div class=\"clearfix\"></div>\n\n  </div>\n    \n   <div class=\"border-top\">\n      <div class=\"border-mdl col-md-10\">\n\n        <button class=\"btn pull-left cancel\" (click)=\"onClick()\">Cancel</button>\n        <button class=\"btn pull-right save\">Submit</button>\n        <div class=\"clearfix\"></div>\n\n      </div>\n   </div>\n    \n\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.ts ***!
  \************************************************************************************************/
/*! exports provided: ManuallyApplicantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManuallyApplicantComponent", function() { return ManuallyApplicantComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ManuallyApplicantComponent = /** @class */ (function () {
    function ManuallyApplicantComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ManuallyApplicantComponent.prototype.ngOnInit = function () {
    };
    ManuallyApplicantComponent.prototype.onClick = function () {
        this.dialogRef.close();
    };
    ManuallyApplicantComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manually-applicant',
            template: __webpack_require__(/*! ./manually-applicant.component.html */ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.html"),
            styles: [__webpack_require__(/*! ./manually-applicant.component.css */ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ManuallyApplicantComponent);
    return ManuallyApplicantComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/recruitment.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/recruitment.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-recruit {float: none; margin:70px auto 0;}\n\n.recruitment-block {text-align: center; background: #fff; border-radius:5px; padding: 60px 0; box-shadow: 0 0 30px #ada5a5;}\n\n.recruitment-block ul { display: block;}\n\n.recruitment-block ul li { display: inline-block; float: none; border-right:#ccc 1px solid; padding: 0 10px 0 0;\nvertical-align: top;height: 165px;}\n\n.recruitment-block ul li a { display: inline-block;}\n\n.recruitment-block ul li a img { margin: 0 auto;}\n\n.recruitment-block ul li a h2 { color: #232323; font-size:18px; font-weight:500; line-height: 30px;}\n\n.recruitment-block ul li a small { color: #444; font-size:15px; font-weight: normal;}\n\n.reports-block{ display: block;}"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/recruitment.component.html":
/*!************************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/recruitment.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"main-recruit col-md-11\">\n\n  <div class=\"recruitment-block\">\n\n    <ul>\n\n      <li class=\"col-md-3\">\n         <a routerLink=\"/admin/admin-dashboard/recruitment/post-jobs\">\n           <img src=\"../../../assets/images/recruitments/post-jobs.png\">\n          <h2>Post Jobs</h2>\n          <small>Post Jobs to website</small>\n         </a>\n      </li>\n\n      <li class=\"col-md-3\">\n          <a routerLink=\"/admin/admin-dashboard/recruitment/job-description\">\n            <img src=\"../../../assets/images/recruitments/job-description.png\">\n           <h2>Jobs Descriptions & Applications</h2>\n           <small>Create job description <br> Job Bank</small>\n          </a>\n       </li>\n\n\n       <li class=\"col-md-3\">\n          <a routerLink=\"/admin/admin-dashboard/recruitment/candidates\">\n            <img src=\"../../../assets/images/recruitments/site-access.png\">\n           <h2>Applicants & Candidates</h2>\n           <small>Search existing candidates</small>\n          </a>\n       </li>\n\n       <li class=\"col-md-3 no-bor\">\n          <a routerLink=\"/admin/admin-dashboard/recruitment/interviews\">\n            <img src=\"../../../assets/images/recruitments/interviews.png\">\n           <h2>Interviews</h2>\n           <small>Coordinate interview schedule</small>\n          </a>\n       </li>\n\n    </ul>\n\n  </div>\n\n\n  <div class=\"reports-block\">\n\n\n\n  </div>\n\n</div>\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/recruitment.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/recruitment.component.ts ***!
  \**********************************************************************/
/*! exports provided: RecruitmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecruitmentComponent", function() { return RecruitmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RecruitmentComponent = /** @class */ (function () {
    function RecruitmentComponent() {
    }
    RecruitmentComponent.prototype.ngOnInit = function () {
    };
    RecruitmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-recruitment',
            template: __webpack_require__(/*! ./recruitment.component.html */ "./src/app/admin-dashboard/recruitment/recruitment.component.html"),
            styles: [__webpack_require__(/*! ./recruitment.component.css */ "./src/app/admin-dashboard/recruitment/recruitment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RecruitmentComponent);
    return RecruitmentComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/recruitment/recruitment.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin-dashboard/recruitment/recruitment.module.ts ***!
  \*******************************************************************/
/*! exports provided: RecruitmentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecruitmentModule", function() { return RecruitmentModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _recruitment_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recruitment.component */ "./src/app/admin-dashboard/recruitment/recruitment.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _applicant_candidates_applicant_candidates_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./applicant-candidates/applicant-candidates.component */ "./src/app/admin-dashboard/recruitment/applicant-candidates/applicant-candidates.component.ts");
/* harmony import */ var _interviews_interviews_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./interviews/interviews.component */ "./src/app/admin-dashboard/recruitment/interviews/interviews.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _manually_applicant_manually_applicant_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./manually-applicant/manually-applicant.component */ "./src/app/admin-dashboard/recruitment/manually-applicant/manually-applicant.component.ts");
/* harmony import */ var _interview_schedule_interview_schedule_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./interview-schedule/interview-schedule.component */ "./src/app/admin-dashboard/recruitment/interview-schedule/interview-schedule.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    { path: '', component: _recruitment_component__WEBPACK_IMPORTED_MODULE_2__["RecruitmentComponent"] },
    { path: 'post-jobs', loadChildren: './post-jobs/post-jobs.module#PostJobsModule' },
    { path: 'job-description', loadChildren: './job-description/job-description.module#JobDescriptionModule' },
    { path: 'candidates', component: _applicant_candidates_applicant_candidates_component__WEBPACK_IMPORTED_MODULE_4__["ApplicantCandidatesComponent"] },
    { path: 'interviews', component: _interviews_interviews_component__WEBPACK_IMPORTED_MODULE_5__["InterviewsComponent"] }
];
var RecruitmentModule = /** @class */ (function () {
    function RecruitmentModule() {
    }
    RecruitmentModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_recruitment_component__WEBPACK_IMPORTED_MODULE_2__["RecruitmentComponent"], _applicant_candidates_applicant_candidates_component__WEBPACK_IMPORTED_MODULE_4__["ApplicantCandidatesComponent"], _interviews_interviews_component__WEBPACK_IMPORTED_MODULE_5__["InterviewsComponent"], _manually_applicant_manually_applicant_component__WEBPACK_IMPORTED_MODULE_13__["ManuallyApplicantComponent"], _interview_schedule_interview_schedule_component__WEBPACK_IMPORTED_MODULE_14__["InterviewScheduleComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _angular_material_select__WEBPACK_IMPORTED_MODULE_6__["MatSelectModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_11__["MatCheckboxModule"], _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__["MatPaginatorModule"]
            ],
            entryComponents: [_manually_applicant_manually_applicant_component__WEBPACK_IMPORTED_MODULE_13__["ManuallyApplicantComponent"], _interview_schedule_interview_schedule_component__WEBPACK_IMPORTED_MODULE_14__["InterviewScheduleComponent"]]
        })
    ], RecruitmentModule);
    return RecruitmentModule;
}());



/***/ })

}]);
//# sourceMappingURL=recruitment-recruitment-module.js.map