(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-hire-hr-time-new-hire-hr-time-module"],{

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".zenwork-customized-back-btn{\n    background: #fff;\n    /* border: 1px solid #ccc; */\n    border-radius: 25px;\n    padding: 3px 10px;\n    font-size: 10px;\n}\n.zenwork-custom-height{\n    height: 100vh;\n}\n.zenwork-currentpage{\n    padding-top: 25px;\n}\n.mr-7{\n    margin-right: 7px;\n}\n.zenwork-inner-icon{\n    width: 20px;\n    height: auto;\n}\n.inner-icon-img{\n    vertical-align: middle;\n    display: inline-block;\n    margin: 0px 5px;\n}\n.main-newhirehrtime{\n    float: none;\n    padding: 0;\n    margin: 0 auto;\n}\n.zenwork-padding-25-zero{\n    padding: 25px 0 10px 10px!important;\n}\n.zenwork-general-form-settings{\n    width: 18%!important;\n    border-radius: 0px!important;\n}\n.name-list .list-name-field{\n    display:inline-block;\n    padding:0px 10px;\n}\na{\n    color: #000;\n}\na:hover{\n    text-decoration: none;\n    color: #000;\n}\nlabel{\n    font-weight: normal;\n    line-height: 30px;\n}\n.zenwork-currentpage .sub-title b{\n    font-size: 14px!important;      \n}\n.birth-date .padding10-listitems{\n    padding: 0px 10px;\n    display: inline-block;\n    position: relative;\n}\n.birth-date-list{\n    width: 21%;\n}\n.birthdate{\n    width: 180px;\n    font-size: 12px;\n    padding: 0 0 0 10px;\n}\n/* input {\n    height:30px;\n} */\n.birth-date{\n    position:relative;\n}\n.calendar-icon{\n    position: absolute;\n    top: 38px;\n    width: 15px;\n    right: 18px;\n}\n.calendar-icon-time{\n    position: absolute;\n    top: 8px;\n    width: 15px;\n    right: 18px;\n}\n.zenwork-structure-settings{\n    width: 180px!important;\n    border-radius: 0px!important;\n    height:30px;\n    border: 0;\n    box-shadow: none;\n}\n.gender-list{\n    padding:0px 10px;\n}\ninput{\n    height:30px;\n    border:0;\n    width: 180px;\n}\nselect{\n    font-size: 10px;\n}\n.zenwork-margin-twenty-zero{\n    margin: 20px 0px!important;\n}\n.time-off-policy{\n    font-weight: 600;\n    padding-left:10px;\n}\n.zenwork-setting .btn {border-radius: 20px; height: 33px; line-height: 30px; color:#000; font-size: 15px; padding: 0 24px; background:transparent; margin:20px 0 0;}\n.zenwork-setting .btn:hover {background: #008f3d; color:#fff;}"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid zenwork-custom-height\">\n<div class=\"zenwork-currentpage\">\n  <p class=\"zenwork-margin-zero\">\n    <a [routerLink]=\"['/admin/admin-dashboard/employee-management/on-boarding/on-boarding']\">    \n    <button class=\"btn zenwork-customized-back-btn\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back  \n    </button>\n  </a>\n  <span class=\"inner-icon-img\">\n    <img src=\"../../../assets/images/Onboarding/Group 828.png\" class=\"zenwork-inner-icon\" alt=\"Company-settings icon\">\n   </span>\n    <small class=\"sub-title\"><b>New Hire HR + Time</b></small>\n  </p>\n  <hr class=\"zenwork-margin-ten-zero\">\n</div>\n<div class=\"main-newhirehrtime col-md-11\">\n<div class=\"green zenwork-padding-25-zero\">\n  Details\n  <span class=\"caret\"></span>\n</div>\n<div class=\"form-group\">\n  <ul class=\"list-unstyled name-list\">\n    <li class=\"list-name-field\">\n      <label>First Name</label><br>\n      <input type=\"text\" name=\"fname\" class=\"names\">\n    </li>\n    <li class=\"list-name-field\">\n      <label>Middle Name</label><br>\n      <input type=\"text\" name=\"fname\" class=\"names\">\n    </li>\n    <li class=\"list-name-field\">\n      <label>Last Name</label><br>\n      <input type=\"text\" name=\"fname\" class=\"names\">\n    </li>\n    <li class=\"list-name-field\">\n      <label>Prefered Name</label><br>\n      <input type=\"text\" name=\"fname\" class=\"names\">\n    </li>\n  </ul>\n  <ul class=\"list-unstyled birth-date\">\n    <li class=\"birth-date-list padding10-listitems\">\n      <label>Birth Date</label><br>\n      <input class=\"form-control\" \n      #dpMDY=\"bsDatepicker\" \n      bsDatepicker\n      formControlName=\"myDateMDY\" \n      [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" \n      placeholder=\"mm/dd/yyyy\" \n      name=\"birth-date\"\n      class=\"birthdate\">\n      <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon\" alt=\"Company-settings icon\"\n        (click)=\"dpMDY.toggle()\" [attr.aria-expanded]=\"dpMDY.isOpen\">\n\n    </li>\n  </ul>\n  <ul class=\"list-unstyled gender\">\n    <li class=\"gender-list padding10-listitems\">\n      <label>Gender</label>\n      <select class=\"form-control zenwork-structure-settings\">\n        <option>male</option>\n        <option>female</option>\n      </select>\n    </li>\n  </ul>\n  <hr class=\"zenwork-margin-twenty-zero\">\n  <div class=\"address\">\n    <div class=\"green zenwork-padding-20-zero\">\n      <label class=\"time-off-policy\">Time off policy</label>\n      <span class=\"caret\"></span>\n    </div>\n      <ul class=\"list-unstyled birth-date\">\n        <li class=\"birth-date-list padding10-listitems\">\n        \n          <input class=\"form-control\"  \n          #dpMDY1=\"bsDatepicker\" \n          bsDatepicker\n          formControlName=\"myDateMDY\" \n          [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" \n          placeholder=\"mm/dd/yyyy\" \n          name=\"birth-date\"\n          class=\"birthdate\">\n          <img src=\"../../../assets/images/Onboarding/calendar_2_.png\" class=\"calendar-icon-time\" alt=\"Company-settings icon\"\n            (click)=\"dpMDY1.toggle()\" [attr.aria-expanded]=\"dpMDY1.isOpen\">\n    \n        </li>\n        </ul>\n   \n  </div>\n</div>\n<div class=\"zenwork-setting\">\n    <button class=\"btn\">Save Changes</button>\n    <button class=\"btn\">Cancel</button>\n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: NewHireHrTimeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewHireHrTimeComponent", function() { return NewHireHrTimeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewHireHrTimeComponent = /** @class */ (function () {
    function NewHireHrTimeComponent() {
    }
    NewHireHrTimeComponent.prototype.ngOnInit = function () {
    };
    NewHireHrTimeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-hire-hr-time',
            template: __webpack_require__(/*! ./new-hire-hr-time.component.html */ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.html"),
            styles: [__webpack_require__(/*! ./new-hire-hr-time.component.css */ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewHireHrTimeComponent);
    return NewHireHrTimeComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.module.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: NewHireHrTimeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewHireHrTimeModule", function() { return NewHireHrTimeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _new_hire_hr_time_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-hire-hr-time.routing */ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.routing.ts");
/* harmony import */ var _new_hire_hr_time_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-hire-hr-time.component */ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NewHireHrTimeModule = /** @class */ (function () {
    function NewHireHrTimeModule() {
    }
    NewHireHrTimeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _new_hire_hr_time_routing__WEBPACK_IMPORTED_MODULE_2__["NewhirehrtimeRouting"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsDatepickerModule"].forRoot(),
            ],
            declarations: [_new_hire_hr_time_component__WEBPACK_IMPORTED_MODULE_3__["NewHireHrTimeComponent"]]
        })
    ], NewHireHrTimeModule);
    return NewHireHrTimeModule;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.routing.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.routing.ts ***!
  \**************************************************************************************************/
/*! exports provided: NewhirehrtimeRouting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewhirehrtimeRouting", function() { return NewhirehrtimeRouting; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _new_hire_hr_time_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-hire-hr-time.component */ "./src/app/admin-dashboard/employee-management/new-hire-hr-time/new-hire-hr-time.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', redirectTo: "/new-hire-hr-time", pathMatch: "full" },
    { path: 'new-hire-hr-time', component: _new_hire_hr_time_component__WEBPACK_IMPORTED_MODULE_2__["NewHireHrTimeComponent"] },
];
var NewhirehrtimeRouting = /** @class */ (function () {
    function NewhirehrtimeRouting() {
    }
    NewhirehrtimeRouting = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NewhirehrtimeRouting);
    return NewhirehrtimeRouting;
}());



/***/ })

}]);
//# sourceMappingURL=new-hire-hr-time-new-hire-hr-time-module.js.map