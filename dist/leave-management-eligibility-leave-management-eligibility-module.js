(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["leave-management-eligibility-leave-management-eligibility-module"],{

/***/ "./node_modules/file-saver/dist/FileSaver.min.js":
/*!*******************************************************!*\
  !*** ./node_modules/file-saver/dist/FileSaver.min.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function(a,b){if(true)!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (b),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {}})(this,function(){"use strict";function b(a,b){return"undefined"==typeof b?b={autoBom:!1}:"object"!=typeof b&&(console.warn("Deprecated: Expected third argument to be a object"),b={autoBom:!b}),b.autoBom&&/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(a.type)?new Blob(["\uFEFF",a],{type:a.type}):a}function c(b,c,d){var e=new XMLHttpRequest;e.open("GET",b),e.responseType="blob",e.onload=function(){a(e.response,c,d)},e.onerror=function(){console.error("could not download file")},e.send()}function d(a){var b=new XMLHttpRequest;b.open("HEAD",a,!1);try{b.send()}catch(a){}return 200<=b.status&&299>=b.status}function e(a){try{a.dispatchEvent(new MouseEvent("click"))}catch(c){var b=document.createEvent("MouseEvents");b.initMouseEvent("click",!0,!0,window,0,0,0,80,20,!1,!1,!1,!1,0,null),a.dispatchEvent(b)}}var f="object"==typeof window&&window.window===window?window:"object"==typeof self&&self.self===self?self:"object"==typeof global&&global.global===global?global:void 0,a=f.saveAs||("object"!=typeof window||window!==f?function(){}:"download"in HTMLAnchorElement.prototype?function(b,g,h){var i=f.URL||f.webkitURL,j=document.createElement("a");g=g||b.name||"download",j.download=g,j.rel="noopener","string"==typeof b?(j.href=b,j.origin===location.origin?e(j):d(j.href)?c(b,g,h):e(j,j.target="_blank")):(j.href=i.createObjectURL(b),setTimeout(function(){i.revokeObjectURL(j.href)},4E4),setTimeout(function(){e(j)},0))}:"msSaveOrOpenBlob"in navigator?function(f,g,h){if(g=g||f.name||"download","string"!=typeof f)navigator.msSaveOrOpenBlob(b(f,h),g);else if(d(f))c(f,g,h);else{var i=document.createElement("a");i.href=f,i.target="_blank",setTimeout(function(){e(i)})}}:function(a,b,d,e){if(e=e||open("","_blank"),e&&(e.document.title=e.document.body.innerText="downloading..."),"string"==typeof a)return c(a,b,d);var g="application/octet-stream"===a.type,h=/constructor/i.test(f.HTMLElement)||f.safari,i=/CriOS\/[\d]+/.test(navigator.userAgent);if((i||g&&h)&&"object"==typeof FileReader){var j=new FileReader;j.onloadend=function(){var a=j.result;a=i?a:a.replace(/^data:[^;]*;/,"data:attachment/file;"),e?e.location.href=a:location=a,e=null},j.readAsDataURL(a)}else{var k=f.URL||f.webkitURL,l=k.createObjectURL(a);e?e.location=l:location.href=l,e=null,setTimeout(function(){k.revokeObjectURL(l)},4E4)}});f.saveAs=a.saveAs=a,"undefined"!=typeof module&&(module.exports=a)});

//# sourceMappingURL=FileSaver.min.js.map

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.css":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.css ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal-body{\n    padding: 0;\n}\n.modal-body .container{\n      margin: 20px 40px;\n}\n.margin-btm {\n    border-bottom: 1px solid #fdeded;\n    padding: 20px 0;\n  }\n.text-field{\n    border: none; \n    box-shadow: none; \n    padding: 11px 12px; \n    height: 40px; \n    box-shadow: none;\n    background: #f8f8f8;\n    width: 85%;\n  }\n.date { height: 40px !important; line-height:inherit !important; width:66% !important; background: #f8f8f8;}\n.date button { display: inline-block;background:transparent; border: none; outline: none; padding: 0; box-shadow:none; width: 40px;\nheight: 30px; min-width: auto;}\n.date span { border: none !important; padding: 0 !important;}\n.date .form-control { width:80%; height: auto; padding: 12px 0 12px 10px;}\n.date .form-control:focus { box-shadow: none; border: none;}\n.date span .fa { color: #a0a0a0;font-size:18px;line-height:18px;width: 18px;text-align: center;}\n.employees-data{\n    padding: 0px 30px;\n  }\n.field-accordion .panel-heading .accordion-toggle.collapsed:after {content: \"\\e080\";}\n.field-accordion .panel-group .panel-heading+.panel-collapse>.list-group, .field-accordion .panel-group .panel-heading+.panel-collapse>.panel-body { border: none;}\n.field-accordion .panel-heading .accordion-toggle:after { margin:4px 10px 0; font-size: 10px; line-height: 10px;}\n.field-accordion .panel-body { padding: 0 0 5px;}\n.field-accordion .panel-default>.panel-heading+.panel-collapse>.panel-body { min-height: auto !important;}\n.field-accordion .panel-default { border-color:transparent;}\n.field-accordion .panel-group .panel+.panel { margin-top: 20px;}\n/* .field-accordion .panel-default>.panel-heading{\n    background: #f8f8f8 !important;\n    position: relative;\n}\n.field-accordion .panel{\n    box-shadow: none;background: #f8f8f8;\n} */\n.field-accordion .panel-title a{\n    display: inline-flex;\n}\n.structure-tbl-body {\n    background: #f8f8f8;\n    width: 45%;\n  }\n.height-300 {\n    height: 300px;\n    overflow: auto;\n  }\n.structure-list .list-items{\n    padding: 15px 15px;\n    border-bottom: 2px solid #eee;\n  }\n.structure-list .list-items-values{\n    padding: 17px 37px;\n    border-bottom: 2px solid #eee;\n  }\n.person-tables .table>tbody>tr>td.zenwork-checked-border:after { content:''; position: absolute; top: 0;height: 100%;}\n.person-tables .table>tbody>tr>td.zenwork-checked-border {position: relative;text-align: left}\n.arrow-position {\n  position: relative;\n}\n.arrow-align-one {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  left: 30px;\n  font-weight: lighter;\n  color: #439348; \n}\n.arrow-align-two {\n  font-size: 40px;\n  position: absolute;\n  top: 145px;\n  right: 0;\n  left: -15px;\n  font-weight: lighter;\n  color: #439348;\n}\n.employees-table{\n  margin: 60px 0 0 0;\n}\n.field-accordion .panel-title { display: inline-block;}\n.field-accordion .panel-heading .accordion-toggle:after {\n    font-family: 'Glyphicons Halflings';  \n    content: \"\\e114\";    \n    float: right;       \n    color: grey;\n}\n.import-date-btn {\n    background-color: #797d7a;\n    border: 1px solid #797d7a;\n    border-radius: 50px;\n    color: #fff;\n    margin-top: 30px;\n    outline: none;\n    margin-right: 30px;\n  }\n.text-report {\n    font-size: 12px;\n  \n  }\n.custom-tables { margin: 0 auto; float: none; padding: 30px 30px 0 5px;}\n.custom-tables h3 { font-size: 15px; line-height: 15px; color:#000; margin: 0 0 30px;}\n.custom-tables .table>thead>tr>th { color: #484848; font-weight: normal; padding: 15px; background: #eef7ff; border:none; font-size: 15px; border-radius:3px;}\n.custom-tables .table>thead>tr>th a { display: inline-block; padding: 0 15px 0 0; cursor: pointer;}\n.custom-tables .table>thead>tr>th a .fa { color:#000; font-size: 15px; line-height: 15px;}\n.custom-tables .table>tbody>tr>td, .custom-tables .table>tfoot>tr>td, .custom-tables .table>thead>tr>td{ padding: 15px; background:#fff;border: none;vertical-align:middle;font-size: 13px;}\n.custom-fields .checkbox{ margin: 0;}\n.group-list .table>tbody>tr>td {\n  background: #f8f8f8;\n  border: none;\n  vertical-align: middle;\n  border-top: none;\n  border-bottom: #c3c3c3 1px solid;\n  color: #948e8e;\n}\n.submit-btn {\n  background-color: #439348;\n  border: 1px solid #439348;\n  border-radius: 35px;\n  color: #fff;\n  padding: 0 25px;    \n}\nlabel{\n  font-weight: normal;\n  line-height: 30px;\n  color:#464444;\n  font-size: 15px;\n  padding: 0 0 0 2px;\n}\n.blackout-head {\n  margin: 20px 0px 0 -10px;\n}\n.specific-head {\n  margin: 20px 0px 20px 10px;\n}\n#fileUpload {\n  height: 0;\n  display: none;\n  width: 0;\n}\n.upload{\n  text-decoration: none;\n}\n.dual-list .listbox button{\n  background: #008f3d !important;\n}"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.html":
/*!******************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.html ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header modal-margintop margin-btm\">\n    <h4 class=\"modal-title pull-left \">Add / Edit Leave Eligibility Group</h4>\n    <div class=\"clearfix\"></div>\n  </div>\n\n<div class=\"modal-body\">\n\n    <div style=\"width: 100%\">\n      <div class=\"margin-btm\">\n      <div class=\"row\" style=\"margin-left: 50px;\">\n        <div class=\"col-xs-4\">\n\n          <label>Name the Leave Eligibility Group</label><br>\n          <input type=\"text\" class=\"form-control text-field\" [(ngModel)]=\"eligibilityGroup.name\">\n\n        </div>\n\n\n        <div class=\"col-xs-4\">\n          <label>Leave Eligibility Group Effective Date</label><br>\n\n          <div class=\"date\">\n            <input matInput [matDatepicker]=\"picker\" placeholder=\"mm / dd / yy\"\n              class=\"form-control\" [(ngModel)]=\"eligibilityGroup.effectiveDate\">\n            <mat-datepicker #picker></mat-datepicker>\n            <button mat-raised-button (click)=\"picker.open()\" class=\"text-field\">\n              <span>\n                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n              </span>\n            </button>\n          </div>\n        </div>\n\n        <div class=\"clearfix\"></div>\n      </div>\n      <!-- <div class=\"row\" style=\"margin-left: 50px; margin-top: 15px;\">\n          <div class=\"col-xs-4\">\n  \n            <label>Use Benefit Eligibility Group Rules?</label><br>\n            <mat-select class=\"form-control text-field\" [(ngModel)]=\"eligibilityGroup.useBenefitGroupRules\" (ngModelChange)=\"selectBenefitEligibilityGroup($event)\">\n                <mat-option [value]=\"true\">Yes</mat-option>\n                <mat-option [value]=\"false\">No</mat-option>\n\n              </mat-select>\n  \n          </div>\n  \n  \n          <div class=\"col-xs-4\" *ngIf=\"showField1\">\n            <label>Benefit Eligibility Group List</label><br>\n            <mat-select class=\"form-control text-field\" [(ngModel)]=\"eligibilityGroup.benifitGroup\">\n                <mat-option value=\"5d78b5aa585e7760e3397cc1\">Full Time</mat-option>\n                <mat-option value=\"5d78b5aa585e7760e3397cc1\">Part Time</mat-option>\n\n              </mat-select>\n        \n          </div>\n  \n          <div class=\"clearfix\"></div>\n        </div> -->\n      </div>\n      <div class=\"employees-data\">\n        <div class=\"field-accordion\">\n          <div class=\"panel-group\" id=\"accordion2\">\n            <!-- <div class=\"panel panel-border-remove\" style=\"border-bottom:none;\"> -->\n              <div class=\"panel-heading blackout-head\">\n                <h4 class=\"panel-title heading-eligibility\" style=\"margin-left:30px\">\n                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\"\n                    href=\"#eligibility-info\">\n                     Eligibility Criteria\n                  </a>\n                </h4>\n              </div>\n              <div id=\"eligibility-info\" class=\"panel-collapse collapse in\">\n                <div class=\"panel-body eligibilty-border-bottom\">\n\n                  <div class=\"employees-data\">\n                    <div class=\"col-sm-12 table-body margin-btm\">\n                      <div class=\"row\">\n                        <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                          <ul class=\"list-unstyled structure-list\" *ngFor=\"let fields of structureFields\">\n\n                            <li class=\"list-items\" [ngClass]=\"{'zenwork-checked-border': fields.isChecked}\">\n                              <div>\n\n                                <mat-checkbox class=\"checkbox-success\" [(ngModel)]=\"fields.isChecked\"\n                                  [value]=\"fields.name\"\n                                  (ngModelChange)=\"eligibilityArray = [];getStructureSubFields(fields.name,fields.isChecked )\">\n                                  {{fields.name}}\n                                </mat-checkbox>\n                              </div>\n\n                            </li>\n\n                          </ul>\n                        </div>\n\n                        <div class=\"col-sm-1 text-center arrow-position\">\n                          <i class=\"material-icons arrow-align-one\">\n                            keyboard_arrow_right\n                          </i>\n                          <i class=\"material-icons arrow-align-two\">\n                            keyboard_arrow_right\n                          </i>\n                        </div>\n                        <div class=\"col-sm-6 structure-tbl-body height-300\">\n\n                          <div *ngIf=\"StructureValues\">\n                            <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of StructureValues\">\n\n                              <li class=\"list-items-values\">\n                                <mat-checkbox class=\"checkbox-success\" \n                                  (change)=\"subFieldsStructure($event,values.name)\" [(ngModel)]=\"values.isChecked\" [value]=\"values.name\">\n                                  {{values.name}}\n                                </mat-checkbox>\n\n                              </li>\n\n                            </ul>\n\n                          </div>\n                          <div *ngIf=\"customValues\">\n                            <ul class=\"list-unstyled structure-list\" *ngFor=\"let values of customValues\">\n\n                              <li class=\"list-items-values\">\n                                <mat-checkbox class=\"checkbox-success\"\n                                  (change)=\"subFieldsStructure($event,values.name)\" [(ngModel)]=\"values.isChecked\" [value]=\"values.name\">\n                                  {{values.name}}\n                                </mat-checkbox>\n                              </li>\n\n                            </ul>\n\n                          </div>\n                        </div>\n\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n                    <div class=\"panel-group\" id=\"accordion3\">\n                      <div class=\"panel panel-border-remove\" style=\"border-bottom:none;\">\n                        <div class=\"panel-heading specific-head\">\n                          <h4 class=\"panel-title heading-eligibility\">\n                            <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion3\"\n                              href=\"#specific-employee\">\n                              Specific Employee\n                            </a>\n                          </h4>\n                        </div>\n                        <div id=\"specific-employee\" class=\"panel-collapse collapse in\" style=\"margin-left:25px;\">\n                          <div class=\"panel-body eligibilty-border-bottom row\" style=\"padding-bottom:30px;\">\n                            <!-- <div class=\"col-xs-6\">\n                              <h4>Include a Specific Person</h4>\n                              <ng-multiselect-dropdown [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                                [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                                (onSelectAll)=\"onSelectAll($event)\" (onDeSelect)=\"OnItemDeSelect($event)\" (onDeSelectAll)=\"onDeSelectAll($event)\">\n                              </ng-multiselect-dropdown>\n                            </div>\n                            <div class=\"col-xs-6\">\n                              <h4>Exclude a Specific Person</h4>\n                              <ng-multiselect-dropdown [placeholder]=\"'custom placeholder'\"\n                                [data]=\"dropdownList\" [(ngModel)]=\"selectedItems\"\n                                [settings]=\"dropdownSettings\" (onSelect)=\"onItemSelect($event)\"\n                                (onSelectAll)=\"onSelectAll($event)\">\n                              </ng-multiselect-dropdown>\n                            </div> -->\n                            <div>\n                              <h4 style=\"float: left;\">Include a Specific Person</h4>\n                              <h4 style=\"float: right;\">Exclude a Specific Person</h4>\n                              <div class=\"clearfix\"></div>\n                              <dual-list [source]=\"dropdownList\" [(destination)]=\"selectedUsers\" [format]=\"format\" filter=true\n                              height='300px' (click)=\"onSelectUsers(selectedUsers)\">\n                            </dual-list>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <div class=\"margin-btm row\" style=\"margin-left:10px;\">\n                      <h4 style=\"margin-left:15px;\">Assign Initial Group Effective Date</h4>\n                      <div class=\"col-xs-3\">\n                        <label>Select Date</label>\n                        <mat-select class=\"form-control text-field\" [(ngModel)]=\"eligibilityGroup.date_selection\" (ngModelChange)=\"selectEffectiveDate($event)\"\n                        placeholder=\"Select date\">\n                          <mat-option value=\"date_of_hire\">Date of Hire</mat-option>\n                          <mat-option value=\"custom_date\">Custom Date</mat-option>\n                        </mat-select>\n                      </div>\n                      <div class=\"col-xs-3\" *ngIf=\"showField2\">\n                        <label>Select Custom Date Field</label>\n\n                        <div class=\"date\">\n                          <input matInput [matDatepicker]=\"picker2\" placeholder=\"mm / dd / yy\"\n                            class=\"form-control\" [(ngModel)]=\"eligibilityGroup.custom_date_field\">\n                          <mat-datepicker #picker2></mat-datepicker>\n                          <button mat-raised-button (click)=\"picker2.open()\" class=\"text-field\">\n                            <span>\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                            </span>\n                          </button>\n                        </div>\n                        <!-- <mat-select class=\"form-control text-field\" [(ngModel)]=\"eligibilityGroup.customDate\" placeholder=\"Select custom date\">\n                          <mat-option value=\"Custom Date1\">Custom Date1</mat-option>\n                          <mat-option value=\"Custom Date2\">Custom Date2</mat-option>\n                        </mat-select> -->\n                      </div>\n                      <!-- <div class=\"col-xs-6\" *ngIf=\"showField2\">\n                          <input type=\"file\" id=\"fileUpload\" (change)=\"fileChangeEvent($event)\">\n                          <label for=\"fileUpload\">\n                              <a mat-raised-button class=\"import-date-btn upload\">Import Custom Date</a>\n                              \n                            </label>\n                        <span class=\"mar-left-25\">\n                          <a target=\"_blank\" class=\"green\" style=\"cursor:pointer;\" (click)=\"downloadExcelFile()\">Click here to Download.</a>\n                        </span>\n                      </div> -->\n\n                    </div>\n\n                    <div class=\"margin-btm\" style=\"margin-left:20px;\">\n\n                      <h4>Preview Employee Benefit Eligibility Group List\n                       <!-- <button class=\"import-date-btn pull-right text-report\">Run Report</button>  -->\n\n                      </h4>\n                      <div class=\"clear-fix\"></div>\n                      <span>(Total Employees - {{count}})</span>\n                      <div class=\"custom-tables group-list\">\n                        <table class=\"table\">\n                          <thead>\n                            <tr>\n                              <th>\n                                Employee Name\n                              </th>\n                              <th>Employee ID</th>\n                              <th>Manager Name</th>\n                              <th>Department</th>\n                              <th>Effective Date<br><small>(Custom Date1)</small></th>\n                              <th>Benefit Eligibility Group</th>\n\n                            </tr>\n                          </thead>\n                          <tbody>\n                            <tr *ngFor=\"let list of previewList\">\n                              <td>{{list.personal.name.firstName}}</td>\n                              <td>{{list.job.employeeId}}</td>\n                              <td>\n                                <span *ngIf=\"list.job.ReportsTo\">\n                                    {{list.job.ReportsTo.personal.name.firstName}}\n                                </span>\n                                \n                              </td>\n                              <td>{{list.job.department}}</td>\n                              <td></td>\n                              <td></td>\n                            </tr>\n\n                        </table>\n                        \n                      </div>\n                      <mat-paginator [length]=\"count\" [pageSize]=\"10\" [pageSizeOptions]=\"[2,5, 10, 25, 100]\" (page)=\"pageEvents($event)\">\n                      </mat-paginator>\n                    </div>\n                    <div class=\"row\" style=\"margin:30px 0px;\">\n\n                      <button mat-button (click)=\"onNoClick()\">Cancel</button>\n\n                      <button mat-button class=\"submit-btn pull-right\" (click)=\"createAndUpdateLeaveEligibilityGroup()\">Submit</button>\n                    </div>\n                    <div class=\"clearfix\"></div>\n\n                 \n\n                </div>\n              <!-- </div> -->\n           \n          \n\n\n\n        </div>\n      </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.ts":
/*!****************************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.ts ***!
  \****************************************************************************************************************************************/
/*! exports provided: AddLeaveEligibilityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddLeaveEligibilityComponent", function() { return AddLeaveEligibilityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/site-access-roles-service.service */ "./src/app/services/site-access-roles-service.service.ts");
/* harmony import */ var _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/accessLocalStorage.service */ "./src/app/services/accessLocalStorage.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../services/employee.service */ "./src/app/services/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};










var AddLeaveEligibilityComponent = /** @class */ (function () {
    function AddLeaveEligibilityComponent(dialogRef, companySettingsService, siteAccessRolesService, accessLocalStorageService, swalAlertService, leaveManagementService, employeeService, data) {
        this.dialogRef = dialogRef;
        this.companySettingsService = companySettingsService;
        this.siteAccessRolesService = siteAccessRolesService;
        this.accessLocalStorageService = accessLocalStorageService;
        this.swalAlertService = swalAlertService;
        this.leaveManagementService = leaveManagementService;
        this.employeeService = employeeService;
        this.data = data;
        this.StructureValues = [];
        this.customValues = [];
        this.eligibilityArray = [];
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.selectedUsers = [];
        this.showField1 = false;
        this.showField2 = false;
        this.editField = true;
        this.structureFields = [
            {
                name: 'Business Unit',
                isChecked: true
            }
        ];
        this.eligibilityGroup = {
            name: '',
            effectiveDate: '',
            // useBenefitGroupRules: '',
            // benifitGroup: '',
            date_selection: '',
            custom_date_field: ''
        };
        this.previewList = [];
        this.selectedEmployees = [];
        this.format = {
            add: 'Add', remove: 'Remove', all: 'Select All', none: 'Deselect All',
            direction: angular_dual_listbox__WEBPACK_IMPORTED_MODULE_8__["DualListComponent"].LTR, draggable: true, locale: 'da',
        };
    }
    AddLeaveEligibilityComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    AddLeaveEligibilityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageNo = 1;
        this.perPage = 10;
        this.companyId = this.accessLocalStorageService.get('companyId');
        console.log(this.companyId);
        console.log(this.data);
        this.getAllEmp();
        this.getFieldsForRoles();
        if (this.data) {
            this.eligibilityGroup = this.data;
            // if (this.data.useBenefitGroupRules === true) {
            //   this.showField1 = false;
            // }
            if (this.data.userIds) {
                var data = {
                    per_page: this.perPage,
                    page_no: this.pageNo,
                    _ids: this.data.userIds
                };
                this.companySettingsService.prviewEmployeeList(data).subscribe(function (res) {
                    console.log(res);
                    _this.previewList = res.data;
                    _this.count = res.total_count;
                    for (var i = 0; i < res.data.length; i++) {
                        _this.selectedUsers.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            if (this.data.initial_group_effective_date_details) {
                this.eligibilityGroup.date_selection = this.data.initial_group_effective_date_details.date_selection;
                if (this.data.initial_group_effective_date_details.date_selection === "date_of_hire") {
                    this.showField2 = false;
                }
                this.eligibilityGroup.custom_date_field = this.data.initial_group_effective_date_details.custom_date_field;
                console.log(this.eligibilityGroup.date_selection);
            }
        }
        // if (this.data.eligibility_criteria && this.data.eligibility_criteria.sub_fields.length) {
        //   this.eligibilityArray = this.data.eligibility_criteria.sub_fields;
        //   this.getStructureSubFields(this.data.eligibility_criteria.selected_field, true, this.data.eligibility_criteria.sub_fields);
        // }
        // this.dropdownSettings = {
        //   singleSelection: false,
        //   idField: 'item_id',
        //   textField: 'item_text',
        //   selectAllText: 'Select All',
        //   unSelectAllText: 'UnSelect All',
        //   itemsShowLimit: 0,
        //   maxHeight: 150,
        //   defaultOpen: true,
        //   allowSearchFilter: true
        // };
    };
    // Author: Saiprakash G, Date: 05/06/19,
    // Restrict fields
    AddLeaveEligibilityComponent.prototype.selectBenefitEligibilityGroup = function (event) {
        console.log(event);
        if (event === true) {
            this.showField1 = true;
        }
        else {
            this.showField1 = false;
        }
    };
    AddLeaveEligibilityComponent.prototype.selectEffectiveDate = function (event) {
        if (event === "date_of_hire") {
            this.showField2 = false;
        }
        else if (event === "custom_date") {
            this.showField2 = true;
        }
    };
    // Author: Saiprakash G, Date: 05/06/19,
    // Upload custom date file
    AddLeaveEligibilityComponent.prototype.fileChangeEvent = function (e) {
        var files = e.target.files[0];
        console.log(files.name);
        var formData = new FormData();
        formData.append('file', files);
        this.leaveManagementService.uploadCustomFile(formData).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 05/06/19,
    // Download sample custom date xlxs file
    AddLeaveEligibilityComponent.prototype.downloadExcelFile = function () {
        var downloadData = [
            {
                "empId": "2432534",
                "empName": "Alex",
                "groupName": "Team Titans",
                "effectiveDate": "2019-03-29(yyyy-mm-dd)"
            }
        ];
        this.leaveManagementService.downloadCustomFile(downloadData).subscribe(function (res) {
            console.log(res);
            var file = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            var fileURL = URL.createObjectURL(file);
            file_saver__WEBPACK_IMPORTED_MODULE_7__["saveAs"](file, 'file.xlsx');
            // window.open(fileURL)
        }, function (err) {
            console.log(err);
        });
    };
    AddLeaveEligibilityComponent.prototype.pageEvents = function (event) {
        console.log(event);
        this.pageNo = event.pageIndex + 1;
        this.perPage = event.pageSize;
        this.previewEmployeeList();
    };
    AddLeaveEligibilityComponent.prototype.previewEmployeeList = function () {
        var _this = this;
        var finalPreviewUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalPreviewUsers.push(element._id);
        });
        var data = {
            per_page: this.perPage,
            page_no: this.pageNo,
            _ids: finalPreviewUsers
        };
        this.companySettingsService.prviewEmployeeList(data).subscribe(function (res) {
            console.log(res);
            _this.previewList = res.data;
            _this.count = res.total_count;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 04/06/19,
    // Slect and deselect employees for include and exclude
    // onItemSelect(item: any) {
    //   this.selectedUsers = []
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   for (var i = 0; i < this.selectedItems.length; i++) {
    //     this.selectedUsers.push(this.selectedItems[i].item_id);
    //   }
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // OnItemDeSelect(item: any) {
    //   console.log(item);
    //   console.log(this.selectedItems);
    //   this.selectedUsers = this.selectedUsers.filter(user => user != item.item_id)
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // onSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   for (var i = 0; i < items.length; i++) {
    //     this.selectedUsers.push(items[i].item_id);
    //   }
    //   console.log("users", this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // onDeSelectAll(items: any) {
    //   console.log(items);
    //   this.selectedUsers = [];
    //   console.log('selcetd users', this.selectedUsers);
    //   this.previewEmployeeList();
    // }
    // Author: Saiprakash , Date: 27/05/19
    // Get fields for roles
    AddLeaveEligibilityComponent.prototype.getFieldsForRoles = function () {
        var _this = this;
        this.companySettingsService.getFieldsForRoles()
            .subscribe(function (res) {
            console.log("12122123", res);
            _this.structureFields = res.data;
            _this.structureFields[0].isChecked = false;
            // if (this.data.eligibility_criteria && this.data.eligibility_criteria.selected_field) {
            //   let index = this.structureFields.findIndex(e => e.name === this.data.eligibility_criteria.selected_field)
            //   console.log(index, 'index')
            //   if (index >= 0) {
            //     this.structureFields[index].isChecked = true;
            //   }
            // }
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash , Date: 27/05/19
    // Get Structure Fields
    AddLeaveEligibilityComponent.prototype.getStructureSubFields = function (fields, isChecked, sub_fields) {
        var _this = this;
        console.log(fields, isChecked);
        this.fieldsName = fields;
        // this.eligibilityArray = [];
        this.checked = isChecked;
        if (isChecked == true) {
            this.structureFields.forEach(function (element) {
                if (element.name !== fields) {
                    element.isChecked = false;
                }
            });
            this.companySettingsService.getStuctureFields(fields, 0)
                .subscribe(function (res) {
                _this.EmployeeListSummary();
                console.log(res, name);
                _this.StructureValues = res.default;
                _this.customValues = res.custom;
                console.log(_this.StructureValues);
                if (sub_fields && sub_fields.length) {
                    sub_fields.forEach(function (element) {
                        var structureIndex = _this.StructureValues.findIndex(function (e) { return e.name === element; });
                        var customValuesIndex = _this.customValues.findIndex(function (e) { return e.name === element; });
                        if (structureIndex >= 0) {
                            _this.StructureValues[structureIndex].isChecked = true;
                        }
                        if (customValuesIndex >= 0) {
                            _this.customValues[customValuesIndex].isChecked = true;
                        }
                    });
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash , Date: 27/05/19
    // Get sub fields structure 
    AddLeaveEligibilityComponent.prototype.subFieldsStructure = function ($event, value) {
        console.log($event, value);
        this.eventName = $event;
        // this.eligibilityArray = [];
        if ($event.checked == true) {
            this.eligibilityArray.push(value);
        }
        console.log(this.eligibilityArray, "123321");
        if ($event.checked == false) {
            var index = this.eligibilityArray.indexOf(value);
            if (index > -1) {
                this.eligibilityArray.splice(index, 1);
            }
            console.log(this.eligibilityArray);
        }
        this.EmployeeListSummary();
    };
    AddLeaveEligibilityComponent.prototype.EmployeeListSummary = function () {
        var _this = this;
        var postdata = {
            field: this.fieldsName,
            chooseEmployeesForRoles: this.eligibilityArray
        };
        console.log(postdata);
        this.siteAccessRolesService.chooseEmployeesData(postdata)
            .subscribe(function (res) {
            console.log("emp data", res);
            _this.dropdownList = [];
            for (var i = 0; i < res.data.length; i++) {
                _this.dropdownList.push({ _id: res.data[i]._id, _name: res.data[i].personal.name.preferredName });
            }
            console.log(_this.dropdownList, "employee dropdown");
            var result = _this.selectedUsers.filter(function (item) { return (_this.dropdownList.findIndex(function (e) { return e.item_id === item; })) >= 0; });
            _this.selectedUsers = result;
            console.log(result, _this.selectedUsers);
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 28/05/19,
    // Create and update leave eligibility group
    AddLeaveEligibilityComponent.prototype.createAndUpdateLeaveEligibilityGroup = function () {
        var _this = this;
        var finalSelectedUsers = [];
        this.selectedUsers.forEach(function (element) {
            finalSelectedUsers.push(element._id);
        });
        var data1 = {
            name: this.eligibilityGroup.name,
            effectiveDate: this.eligibilityGroup.effectiveDate,
            // useBenefitGroupRules: this.eligibilityGroup.useBenefitGroupRules,
            // benifitGroup: this.eligibilityGroup.benifitGroup,
            initial_group_effective_date_details: {
                date_selection: this.eligibilityGroup.date_selection,
                custom_date_field: this.eligibilityGroup.custom_date_field
            },
            empIds: finalSelectedUsers
        };
        if (this.data._id) {
            data1._id = this.data._id;
            this.leaveManagementService.updateLeaveEligibilityGroup(data1).subscribe(function (res) {
                console.log(res);
                _this.dialogRef.close(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
        else {
            this.leaveManagementService.createLeaveEligibilityGroup(data1).subscribe(function (res) {
                console.log(res);
                _this.dialogRef.close(res);
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    // Author: Saiprakash G, Date:29/08/19,
    // Description: Preview employee list
    AddLeaveEligibilityComponent.prototype.onSelectUsers = function (selectSpecificPerson) {
        console.log("users", selectSpecificPerson);
        console.log('selcetd users', this.selectedUsers);
        this.previewEmployeeList();
    };
    // Author: Saiprakash G, Date:29/08/19,
    // Description: Get all employees in include box for edit
    AddLeaveEligibilityComponent.prototype.getAllEmp = function () {
        var _this = this;
        var listallUsers;
        var postData = {
            name: ''
        };
        console.log("data comes");
        this.siteAccessRolesService.getAllEmployees(postData)
            .subscribe(function (res) {
            console.log("resonse for all employees", res);
            listallUsers = res.data;
            for (var i = 0; i < listallUsers.length; i++) {
                _this.dropdownList.push({ _id: listallUsers[i]._id, _name: listallUsers[i].personal.name.preferredName });
            }
            console.log("DropDownList", _this.dropdownList);
        }, function (err) {
            console.log(err);
        });
    };
    AddLeaveEligibilityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-leave-eligibility',
            template: __webpack_require__(/*! ./add-leave-eligibility.component.html */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.html"),
            styles: [__webpack_require__(/*! ./add-leave-eligibility.component.css */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.css")]
        }),
        __param(7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"],
            _services_companySettings_service__WEBPACK_IMPORTED_MODULE_2__["CompanySettingsService"],
            _services_site_access_roles_service_service__WEBPACK_IMPORTED_MODULE_3__["SiteAccessRolesService"],
            _services_accessLocalStorage_service__WEBPACK_IMPORTED_MODULE_4__["AccessLocalStorageService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_5__["SwalAlertService"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_6__["LeaveManagementService"],
            _services_employee_service__WEBPACK_IMPORTED_MODULE_9__["EmployeeService"], Object])
    ], AddLeaveEligibilityComponent);
    return AddLeaveEligibilityComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.css":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".leave-main { margin:40px auto 0; float: none; padding: 0;}\n\n.zenwork-currentpage { padding: 0;}\n\n.zenwork-customized-back-btn{\n    background:#fff;\n    border-radius: 25px;\n    padding:8px 20px;\n    font-size: 10px;\n    color:#525151;\n    font-size: 13px; line-height: 13px; border: 1px solid transparent;\n    margin: 0 20px 0 0; vertical-align: middle;\n}\n\n.zenwork-currentpage small { margin: 0 20px 0 0; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage em { font-size: 18px;line-height: 17px; color:#484747; font-style: normal; display: inline-block; vertical-align: middle;}\n\n.zenwork-currentpage span .fa{ color:#439348; font-size: 15px; line-height: 15px;}\n\n.zenwork-currentpage span{ display: inline-block;}\n\n.mr-7{margin-right: 7px;}\n\n.leave-dashboard { background:#fff; border-radius:5px; box-shadow: 0 0 10px #ccc; float: none; margin:50px auto 100px; padding: 0 35px; text-align: center;position: relative;}\n\n.leave-dashboard ul { display:inline-block;  width: 100%; padding:0 0 35px; margin: 0;}\n\n.leave-dashboard ul li { padding: 40px 0 0; border-right:#ccc 1px solid; margin-bottom: 30px;}\n\n.leave-dashboard ul li a { display: inline-block; cursor: pointer;}\n\n/* .leave-dashboard ul li a img { margin: 0 0 20px;} */\n\n.leave-dashboard ul li a h2 { margin: 0 0 10px; font-size: 20px; line-height: 20px; color:#403e3e; font-weight: normal; }\n\n.leave-dashboard ul li h1 { margin: 0 0 60px; font-size:30px; line-height: 30px; color:#403e3e; font-weight: 600; }\n\n.leave-dashboard h2 { font-weight: normal; text-align: center;font-size: 26px;padding: 30px 0 40px;}\n\n.leave-btn { position: absolute; top:25px; right:25px;}\n\n.leave-dashboard ul li:nth-child(even) { border-right: none;}"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"leave-main col-md-11\">\n\n  <div class=\"zenwork-currentpage\">\n    <p class=\"zenwork-margin-zero\">\n      <button class=\"btn zenwork-customized-back-btn\" (click)=\"previousPage()\">\n        <span class=\"green mr-7\">\n          <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n        </span>Back\n      </button>\n      <small>\n        <img src=\"../../../../assets/images/leave-management/leave-icon.png\" width=\"31\" height=\"25\"\n          alt=\"Company-settings icon\">\n      </small>\n      <em>Leave Management Eligibility Groups</em>\n    </p>\n    <hr class=\"zenwork-margin-ten-zero\">\n  </div>\n\n\n  <div class=\"leave-dashboard\">\n\n\n    <h2>Leave Eligibility Groups</h2>\n\n    <div class=\"leave-btn\">\n       \n            <mat-icon class=\"cursor\" [matMenuTriggerFor]=\"menu\">more_vert</mat-icon>\n            <mat-menu #menu=\"matMenu\">\n    \n              <button *ngIf=\"!checkEligibilityIds.length >=1\" mat-menu-item (click)=\"openDialog()\"> Add </button>\n              <button *ngIf=\"(!checkEligibilityIds.length <=0) && (checkEligibilityIds.length ==1)\" mat-menu-item (click)=\"editEligibilityGroup()\"> Edit </button>\n              <button *ngIf=\"(!checkEligibilityIds.length <=0) && (checkEligibilityIds.length >=1)\" mat-menu-item (click)=\"deleteEligibilityGroup()\"> Delete </button>\n            </mat-menu>\n          \n    </div>\n\n    <ul>\n\n      <li class=\"col-md-6\" *ngFor=\"let eligibility of leaveEligibilityGroups\">\n        <a>\n          <img src=\"../../../../assets/images/leave-management/full_time.png\" width=\"43\" height=\"48\" alt=\"img\"\n            *ngIf=\"eligibility.benefitType == 'Full Time'\">\n          <img src=\"../../../../assets/images/leave-management/part_time.png\" width=\"43\" height=\"48\" alt=\"img\"\n            *ngIf=\"eligibility.benefitType == 'Part Time'\">\n          <h2>\n            <mat-checkbox class=\"zenwork-customized-checkbox\" [(ngModel)]=\"eligibility.isChecked\" (change)=\"selectLeaveEligibility($event, eligibility._id)\">\n              {{eligibility.name}}\n            </mat-checkbox>\n\n          </h2>\n\n        </a>\n      </li>\n\n    </ul>\n\n    <div class=\"clearfix\"></div>\n\n  </div>\n\n\n\n</div>\n\n\n\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: LeaveManagementEligibilityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementEligibilityComponent", function() { return LeaveManagementEligibilityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _add_leave_eligibility_add_leave_eligibility_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-leave-eligibility/add-leave-eligibility.component */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/leaveManagement.service */ "./src/app/services/leaveManagement.service.ts");
/* harmony import */ var _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/swalAlert.service */ "./src/app/services/swalAlert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LeaveManagementEligibilityComponent = /** @class */ (function () {
    function LeaveManagementEligibilityComponent(dialog, leaveManagementService, swalAlertService, router) {
        this.dialog = dialog;
        this.leaveManagementService = leaveManagementService;
        this.swalAlertService = swalAlertService;
        this.router = router;
        this.leaveEligibilityGroups = [];
        this.selectedEligibilityGroups = [];
        this.checkEligibilityIds = [];
    }
    LeaveManagementEligibilityComponent.prototype.ngOnInit = function () {
        this.getAllLeaveEligibilityGroups();
    };
    // Author: Saiprakash G, Date: 27/05/19,
    // Open dialog for add and edit leave eligibility group
    LeaveManagementEligibilityComponent.prototype.openDialog = function () {
        var _this = this;
        if (this.getSingleEligibilityGroup) {
            var dialogRef = this.dialog.open(_add_leave_eligibility_add_leave_eligibility_component__WEBPACK_IMPORTED_MODULE_1__["AddLeaveEligibilityComponent"], {
                width: '1200px',
                height: '700px',
                backdropClass: 'structure-model',
                data: this.getSingleEligibilityGroup,
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.getAllLeaveEligibilityGroups();
                _this.getSingleEligibilityGroup = "";
                _this.checkEligibilityIds = [];
            });
        }
        else {
            var dialogRef = this.dialog.open(_add_leave_eligibility_add_leave_eligibility_component__WEBPACK_IMPORTED_MODULE_1__["AddLeaveEligibilityComponent"], {
                width: '1300px',
                height: '700px',
                backdropClass: 'structure-model',
                data: {},
            });
            dialogRef.afterClosed().subscribe(function (result) {
                console.log('The dialog was closed', result);
                _this.getAllLeaveEligibilityGroups();
            });
        }
    };
    // Author: Saiprakash G, Date: 29/05/19,
    // Get all leave eligibility groups
    LeaveManagementEligibilityComponent.prototype.getAllLeaveEligibilityGroups = function () {
        var _this = this;
        this.leaveManagementService.getAllLeaveEligibilityGroup().subscribe(function (res) {
            console.log(res);
            _this.leaveEligibilityGroups = res.data;
        }, function (err) {
            console.log(err);
        });
    };
    // Author: Saiprakash G, Date: 30/05/19,
    // Edit leave eligibility group
    LeaveManagementEligibilityComponent.prototype.editEligibilityGroup = function () {
        var _this = this;
        this.selectedEligibilityGroups = this.leaveEligibilityGroups.filter(function (checkGroups) {
            return checkGroups.isChecked;
        });
        if (this.selectedEligibilityGroups.length > 1) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select only one leave eligibility group on to proceed", "", 'error');
        }
        else if (this.selectedEligibilityGroups.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select one leave eligibility group on to proceed", "", 'error');
        }
        else {
            this.leaveManagementService.getLeaveEligibilityGroup(this.selectedEligibilityGroups[0]._id).subscribe(function (res) {
                console.log(res);
                _this.getSingleEligibilityGroup = res.data;
                _this.openDialog();
            }, function (err) {
                console.log(err);
            });
        }
    };
    // Author: Saiprakash G, Date: 30/05/19,
    // Delete leave eligibility group
    LeaveManagementEligibilityComponent.prototype.deleteEligibilityGroup = function () {
        var _this = this;
        this.selectedEligibilityGroups = this.leaveEligibilityGroups.filter(function (checkGroups) {
            return checkGroups.isChecked;
        });
        if (this.selectedEligibilityGroups.length == 0) {
            this.swalAlertService.SweetAlertWithoutConfirmation("Select leave eligibility group on to proceed", "", 'error');
        }
        else {
            var deleteGroup = {
                _ids: this.selectedEligibilityGroups
            };
            this.leaveManagementService.deleteLeaveEligibilityGroup(deleteGroup).subscribe(function (res) {
                console.log(res);
                _this.checkEligibilityIds = [];
                _this.swalAlertService.SweetAlertWithoutConfirmation(res.message, "", "success");
                _this.getAllLeaveEligibilityGroups();
            }, function (err) {
                console.log(err);
                _this.swalAlertService.SweetAlertWithoutConfirmation(err.error.message, "", "error");
            });
        }
    };
    LeaveManagementEligibilityComponent.prototype.selectLeaveEligibility = function (event, id) {
        console.log(event, id);
        if (event.checked == true) {
            this.checkEligibilityIds.push(id);
        }
        console.log(this.checkEligibilityIds);
        if (event.checked == false) {
            var index = this.checkEligibilityIds.indexOf(id);
            if (index > -1) {
                this.checkEligibilityIds.splice(index, 1);
            }
            console.log(this.checkEligibilityIds);
        }
    };
    LeaveManagementEligibilityComponent.prototype.previousPage = function () {
        this.router.navigate(['/admin/admin-dashboard/leave-management']);
    };
    LeaveManagementEligibilityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-leave-management-eligibility',
            template: __webpack_require__(/*! ./leave-management-eligibility.component.html */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.html"),
            styles: [__webpack_require__(/*! ./leave-management-eligibility.component.css */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_leaveManagement_service__WEBPACK_IMPORTED_MODULE_3__["LeaveManagementService"],
            _services_swalAlert_service__WEBPACK_IMPORTED_MODULE_4__["SwalAlertService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], LeaveManagementEligibilityComponent);
    return LeaveManagementEligibilityComponent;
}());



/***/ }),

/***/ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.module.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.module.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: LeaveManagementEligibilityModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeaveManagementEligibilityModule", function() { return LeaveManagementEligibilityModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _leave_management_eligibility_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leave-management-eligibility.component */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/leave-management-eligibility.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../material-module/material-module.module */ "./src/app/material-module/material-module.module.ts");
/* harmony import */ var _add_leave_eligibility_add_leave_eligibility_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-leave-eligibility/add-leave-eligibility.component */ "./src/app/admin-dashboard/leave-management/leave-management-eligibility/add-leave-eligibility/add-leave-eligibility.component.ts");
/* harmony import */ var _services_companySettings_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/companySettings.service */ "./src/app/services/companySettings.service.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_dual_listbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-dual-listbox */ "./node_modules/angular-dual-listbox/fesm5/angular-dual-listbox.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var router = [
    { path: '', redirectTo: 'eligibility', pathMatch: 'full' },
    { path: 'eligibility', component: _leave_management_eligibility_component__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementEligibilityComponent"] }
];
var LeaveManagementEligibilityModule = /** @class */ (function () {
    function LeaveManagementEligibilityModule() {
    }
    LeaveManagementEligibilityModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(router), _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _material_module_material_module_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModuleModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["NgMultiSelectDropDownModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                angular_dual_listbox__WEBPACK_IMPORTED_MODULE_10__["AngularDualListBoxModule"]
            ],
            declarations: [_leave_management_eligibility_component__WEBPACK_IMPORTED_MODULE_2__["LeaveManagementEligibilityComponent"], _add_leave_eligibility_add_leave_eligibility_component__WEBPACK_IMPORTED_MODULE_6__["AddLeaveEligibilityComponent"]],
            entryComponents: [_add_leave_eligibility_add_leave_eligibility_component__WEBPACK_IMPORTED_MODULE_6__["AddLeaveEligibilityComponent"]],
            providers: [_services_companySettings_service__WEBPACK_IMPORTED_MODULE_7__["CompanySettingsService"]]
        })
    ], LeaveManagementEligibilityModule);
    return LeaveManagementEligibilityModule;
}());



/***/ })

}]);
//# sourceMappingURL=leave-management-eligibility-leave-management-eligibility-module.js.map